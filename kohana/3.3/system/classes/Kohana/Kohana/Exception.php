<?php defined('SYSPATH') OR die('No direct access');
/**
 * Kohana exception class. Translates exceptions using the [I18n] class.
 *
 * @package    Kohana
 * @category   Exceptions
 * @author     Kohana Team
 * @copyright  (c) 2008-2012 Kohana Team
 * @license    http://kohanaframework.org/license
 */
class Kohana_Kohana_Exception extends Exception {

	/**
	 * @var  array  PHP error code => human readable name
	 */
	public static $php_errors = array(
		E_ERROR              => 'Fatal Error',
		E_USER_ERROR         => 'User Error',
		E_PARSE              => 'Parse Error',
		E_WARNING            => 'Warning',
		E_USER_WARNING       => 'User Warning',
		E_STRICT             => 'Strict',
		E_NOTICE             => 'Notice',
		E_RECOVERABLE_ERROR  => 'Recoverable Error',
		E_DEPRECATED         => 'Deprecated',
	);

	/**
	 * @var  string  error rendering view
	 */
	public static $error_view = 'kohana/error';

	/**
	 * @var  string  error view content type
	 */
	public static $error_view_content_type = 'text/html';

	/**
	 * Creates a new translated exception.
	 *
	 *     throw new Kohana_Exception('Something went terrible wrong, :user',
	 *         array(':user' => $user));
	 *
	 * @param   string          $message    error message
	 * @param   array           $variables  translation variables
	 * @param   integer|string  $code       the exception code
	 * @param   Exception       $previous   Previous exception
	 * @return  void
	 */
	public function __construct($message = "", array $variables = NULL, $code = 0, Exception $previous = NULL)
	{

		// Set the message
		$message = __($message, $variables);

		// Pass the message and integer code to the parent
		parent::__construct($message, (int) $code, $previous);

		// Save the unmodified code
		// @link http://bugs.php.net/39615
		$this->code = $code;
	}

	/**
	 * Magic object-to-string method.
	 *
	 *     echo $exception;
	 *
	 * @uses    Kohana_Exception::text
	 * @return  string
	 */
	public function __toString()
	{
		return Kohana_Exception::text($this);
	}

	/**
	 * Inline exception handler, displays the error message, source of the
	 * exception, and the stack trace of the error.
	 *
	 * @uses    Kohana_Exception::response
	 * @param   Exception  $e
	 * @return  boolean
	 */
	public static function handler(Exception $e)
	{
		$response = Kohana_Exception::_handler($e);

		// Send the response to the browser
		echo $response->send_headers()->body();

		exit(1);
	}

	/**
	 * Exception handler, logs the exception and generates a Response object
	 * for display.
	 *
	 * @uses    Kohana_Exception::response
	 * @param   Exception  $e
	 * @return  boolean
	 */
	public static function _handler(Exception $e)
	{
		try
		{
			// Log the exception
			Kohana_Exception::log($e);

			// Generate the response
                        
			$response = Kohana_Exception::response($e);
			return $response;
		}
		catch (Exception $e)
		{
			/**
			 * Things are going *really* badly for us, We now have no choice
			 * but to bail. Hard.
			 */
			// Clean the output buffer if one exists
			ob_get_level() AND ob_clean();

			// Set the Status code to 500, and Content-Type to text/plain.
			header('Content-Type: text/plain; charset='.Kohana::$charset, TRUE, 500);

			echo Kohana_Exception::text($e);

			exit(1);
		}
	}

	/**
	 * Logs an exception.
	 *
	 * @uses    Kohana_Exception::text
	 * @param   Exception  $e
	 * @param   int        $level
	 * @return  void
	 */
	public static function log(Exception $e, $level = Log::EMERGENCY)
	{
		if (is_object(Kohana::$log))
		{
			// Create a text version of the exception
                        if($e->getCode() != 441){
                            $error = Kohana_Exception::text($e);

			// Add this exception to the log
			Kohana::$log->add($level, $error, NULL, array('exception' => $e));

			// Make sure the logs are written
			Kohana::$log->write();
                        }else{
                            $error = Kohana_Exception::text($e);

			// Add this exception to the log
			Kohana::$log->add(Log::LOG_USER, $error, NULL, array('exception' => $e));

			// Make sure the logs are written
			Kohana::$log->write();
                        }			
		}
	}

	/**
	 * Get a single line of text representing the exception:
	 *
	 * Error [ Code ]: Message ~ File [ Line ]
	 *
	 * @param   Exception  $e
	 * @return  string
	 */
	public static function text(Exception $e)
	{
            $remove = array("\n", "\r\n", "\n\r", "\r");
            $params = str_replace($remove, '', Debug::dump($_REQUEST));        
            $moreMessage = '<b>Error en: </b> ' . $_SERVER['HTTP_HOST'] . '<br/><br/>';
            if (Session::instance('database')->get('SESSION_ID') != NULL) {
                        $moreMessage .= '<b>Id Usuario:</b> ' . Session::instance('database')->get('USER_ID') . '<br/>';
                        $moreMessage .= '<b>Usuario:</b > ' . Session::instance('database')->get('USER_NAME') . '<br/>';
                        $moreMessage .= '<b>Nombre Completo: </b > ' . Session::instance('database')->get('USER_FULL_NAME') . '<br/>';
                        $moreMessage .= '<b>Id Oficina:</b> ' . Session::instance('database')->get('OFFICE_ID') . '<br/>';
                        $moreMessage .= '<b>oficina:</b> ' . Session::instance('database')->get('OFFICE_NAME') . '<br/>';
                        $moreMessage .= '<b>URI:</b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
                        $moreMessage .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
                    }
            $moreMessage .= '<b>URI: </b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
            $moreMessage .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
            if (isset($_SERVER['REMOTE_ADDR'])) {
                $moreMessage .= '<b>REMOTE_ADDR:</b> ' . $_SERVER['REMOTE_ADDR'] . '<br/>';
            }
            if (isset($_SERVER['HTTP_USER_AGENT'])) {
                $moreMessage .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>';
            }
            if (isset($_SERVER['HTTP_REFERER'])) {
                $moreMessage .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_REFERER'] . '<br/>';
            }
            if (isset($_SERVER['REDIRECT_URL'])) {
                $moreMessage .= '<b>REDIRECT URL:</b> ' . $_SERVER['REDIRECT_URL'] . '<br/>';
            }
            if (isset($_SERVER['PATH_INFO'])) {
                $moreMessage .= '<b>PATH INFO:</b> ' . $_SERVER['PATH_INFO'] . '<br/>';
            }
            if (isset($_SERVER['PATH_TRANSLATED'])) {
                $moreMessage .= '<b>PATH TRANSLATED:</b> ' . $_SERVER['PATH_TRANSLATED'] . '<br/>';
            }
            if (isset($_SERVER['SCRIPT_FILENAME'])) {
                $moreMessage .= '<b>SCRIPT FILENAMEL:</b> ' . $_SERVER['SCRIPT_FILENAME'] . '<br/>';
            }
            if (isset($_SERVER['ORIG_PATH_INFO'])) {
                $moreMessage .= '<b>ORIG_PATH INFO:</b> ' . $_SERVER['ORIG_PATH_INFO'] . '<br/>';
            }
            $moreMessage .= '<b>PARAMETROS: </b><br/><pre>' . $params . '</pre><br/><br/>';
            return sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($e), $e->getCode(), '<div style="word-wrap:break-word;">' . $moreMessage . strip_tags($e->getMessage()) . '</div>', Debug::path($e->getFile()), $e->getLine());
	}

	/**
	 * Get a Response object representing the exception
	 *
	 * @uses    Kohana_Exception::text
	 * @param   Exception  $e
	 * @return  Response
	 */
	public static function response(Exception $e)
	{
		try
		{                    
			// Get the exception information
			$class   = get_class($e);
			$code    = $e->getCode();
			$message = $e->getMessage();
			$file    = $e->getFile();
			$line    = $e->getLine();
			$trace   = $e->getTrace();

			if ( ! headers_sent())
			{
				// Make sure the proper http header is sent
				$http_header_status = ($e instanceof HTTP_Exception) ? $code : 500;
			}

			/**
			 * HTTP_Exceptions are constructed in the HTTP_Exception::factory()
			 * method. We need to remove that entry from the trace and overwrite
			 * the variables from above.
			 */
			if ($e instanceof HTTP_Exception AND $trace[0]['function'] == 'factory')
			{
				extract(array_shift($trace));
			}


			if ($e instanceof ErrorException)
			{
				/**
				 * If XDebug is installed, and this is a fatal error,
				 * use XDebug to generate the stack trace
				 */
				if (function_exists('xdebug_get_function_stack') AND $code == E_ERROR)
				{
					$trace = array_slice(array_reverse(xdebug_get_function_stack()), 4);

					foreach ($trace as & $frame)
					{
						/**
						 * XDebug pre 2.1.1 doesn't currently set the call type key
						 * http://bugs.xdebug.org/view.php?id=695
						 */
						if ( ! isset($frame['type']))
						{
							$frame['type'] = '??';
						}

						// XDebug also has a different name for the parameters array
						if (isset($frame['params']) AND ! isset($frame['args']))
						{
							$frame['args'] = $frame['params'];
						}
					}
				}
				
				if (isset(Kohana_Exception::$php_errors[$code]))
				{
					// Use the human-readable error name
					$code = Kohana_Exception::$php_errors[$code];
				}
			}

			/**
			 * The stack trace becomes unmanageable inside PHPUnit.
			 *
			 * The error view ends up several GB in size, taking
			 * serveral minutes to render.
			 */
			if (defined('PHPUnit_MAIN_METHOD'))
			{
				$trace = array_slice($trace, 0, 2);
			}

			// Instantiate the error view.
			$view = View::factory(Kohana_Exception::$error_view, get_defined_vars());
                    
                        if (Kohana::$environment == Kohana::PRODUCTION) {
                        $view = View::factory('errors/_default', get_defined_vars());
                        }
			// Prepare the response object.
			$response = Response::factory();

			// Set the response status
			$response->status(($e instanceof HTTP_Exception) ? $e->getCode() : 500);
			
			// Set the response headers
			$response->headers('Content-Type', Kohana_Exception::$error_view_content_type.'; charset='.Kohana::$charset);

			// Set the response body
			$response->body($view->render());
		}
		catch (Exception $e)
		{
			/**
			 * Things are going badly for us, Lets try to keep things under control by
			 * generating a simpler response object.
			 */
			$response = Response::factory();
			$response->status(500);
			$response->headers('Content-Type', 'text/plain');
			$response->body(Kohana_Exception::text($e));
		}

		return $response;
	}

} // End Kohana_Exception
