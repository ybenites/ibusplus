<?php

defined('SYSPATH') or die('No direct script access.');

/**
 * Base paypal requester class
 *
 * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_NVPAPIBasics
 * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_NVPAPIOverview
 * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/howto_api_reference

 * @author Alexey Geno <alexeygeno@gmail.com>
 * @package Payment
 */
abstract class Payment_Requester_Paypal extends Payment_Paypal {
                
    /**
     * Returns the Requester API URL for the current environment
     * @return string
     */
    protected function _api_url() {
        if ($this->_environment === 'live') {
            // Live environment does not use a sub-domain
            $env = '';
        } else {
            // Use the environment sub-domain
            $env = $this->_environment . '.';
        }

        return 'https://api-3t.' . $env . 'paypal.com/nvp';
    }

    /**
     * Makes a POST request to PayPal NVP for the given method and parameters
     * @link  https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_Requester_RequesterAPIOverview
     * @param string API method to call
     * @param array API POST parameters
     * @param string interface base name that will be log
     * @param string action name that will be log
     * @throws Kohana_Exception
     * @return array API response
     */
    protected function _post($method, array $params, $interface, $action) {              // Create POST data

        $post = array(
            'METHOD' => $method,
            'VERSION' => 51.0,
            'USER' => $this->_username,
            'PWD' => $this->_password,
            'SIGNATURE' => $this->_signature,
                ) + $params;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->_api_url(),
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => http_build_query($post, NULL, '&'),
            CURLOPT_SSL_VERIFYPEER => FALSE,
            CURLOPT_SSL_VERIFYHOST => FALSE,
            CURLOPT_RETURNTRANSFER => TRUE,
        ));
        
        if (($response_str = curl_exec($curl)) === FALSE) {
            $code = curl_errno($curl);
            $error = curl_error($curl);
            curl_close($curl);
            throw new Kohana_Exception('PayPal API request for :method failed: :error (:code)',
                    array(':method' => $method, ':error' => $error, ':code' => $code));
        }
        curl_close($curl);
        parse_str($response_str, $response);
        if (isset($params['AMT']))
        {
            $this->_after_request($params['AMT'], $post, $response, $interface, $action,$params);
        }
        else
        {
            $this->_after_request('0.00', $post, $response, $interface, $action,$params);
        }
        return $response;
    }

    /**
     * Calls directly after request done. Does Logging and checks success of API operation
     * @param array API request
     * @param array API response
     * @param string  interface base name that should be log
     * @param string  action name that should be log
     * @throws Payment_Exception throws when API operation has failed
     */
    protected function _after_request($amount, array $request, array $response, $interface, $action,$infoClient) {     

        $success = (isset($response['ACK']) AND strpos($response['ACK'], 'Success') !== FALSE);
        $txn_id = ((isset($response['TRANSACTIONID']) AND $response['TRANSACTIONID']) ? $response['TRANSACTIONID'] : FALSE);
        $custom = (isset($request['CUSTOM']) ? $request['CUSTOM'] : FALSE);
        $date = (isset($response['TIMESTAMP']) ? strtotime($response['TIMESTAMP']) : FALSE);
        $payment_log = new Model_Payment_Log('requester', 'paypal');
        $payment_log->create($amount, $interface, $action, $success, $response, $request, $txn_id, $date, $custom,$infoClient);
        Database::instance()->commit();
        if (!$success) {
            $keys = array_keys($response);
            if (in_array('L_LONGMESSAGE1', $keys) AND in_array('L_ERRORCODE1', $keys))
                throw new Kohana_Exception ('PayPal API request for :method failed: :error1 <b>(:code1)</b>. :error0 <b>(:code0)</b>',
                array (
                ':method' => $request['METHOD'],
                ':error0' => $response['L_LONGMESSAGE0'],
                ':code0' => $response['L_ERRORCODE0'],
                ':error1' => $response['L_LONGMESSAGE1'],
                ':code1' => $response['L_ERRORCODE1']),'99999'
            );
            else
            throw new Kohana_Exception('PayPal API request for :method failed :error0 <b>(:code0)</b>',
                    array(
                        ':method' => $request['METHOD'],
                        ':error0' => $response['L_LONGMESSAGE0'],
                        ':code0' => $response['L_ERRORCODE0']),'99999');
        }
    }

}