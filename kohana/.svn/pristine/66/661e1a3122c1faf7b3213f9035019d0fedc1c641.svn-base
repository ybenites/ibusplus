<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Privilege extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('privilege', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('privilege', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $privilege_view = new View('private/privilege');
        if ($this->getSessionParameter(self::USER_ROOT_OPTION)) {
            $groups = ORM::factory("Group")->where('status', '=', self::STATUS_ACTIVE)->find_all();
        } else {
            $groups = ORM::factory("Group")->where('status', '=', self::STATUS_ACTIVE)->and_where('rootOption', '=', self::STATUS_DEACTIVE)->find_all();
        }
        $privilege_view->groups = $groups;
        //$privilege_view->privilegeRows = $this->createMenuArray(NULL, false, $groups);
        $this->template->content = $privilege_view;
    }

    public function walk_menu($arr, &$a_gg) {
        foreach ($arr as $value) {
            array_push($a_gg, $value->id);
            $has_child = (!empty($value->items));
            if ($has_child) {
                $this->walk_menu($value->items, $a_gg);
            }
        }
    }

    public function action_createOrDeleteAccess() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {

            $action = $this->request->post('act');
            $idGroup = $this->request->post('gid');
            $idSuperMenu = $this->request->post('mid');
            Database::instance()->begin();
            $group = ORM::factory('Group')->where('group.idGroup', '=', $idGroup)->find_all();
            $menuStructure = $this->createMenuArray($idSuperMenu, false, $group);
            $menusIDs = array();
            $this->walk_menu($menuStructure, $menusIDs);
            array_push($menusIDs, $idSuperMenu);
            if (!(bool) $action) {
                $parentsMenuIds = explode(",", $this->fnGetSuperMenus($idSuperMenu));
                foreach ($parentsMenuIds as $pIdMenu) {
                    array_push($menusIDs, $pIdMenu);
                }
            }
            foreach ($menusIDs as $idMenu) {
                $privilege = new Model_Privilege();
                $privilege = ORM::factory('Privilege')
                                ->where('idMenu', '=', $idMenu)
                                ->where('idGroup', '=', $idGroup)->find();
                if ($privilege->loaded()) {
                    if ((bool) $action) {
                        $privilege->delete();
                    }
                } else {
                    if (!(bool) $action) {
                        $privilege->idMenu = $idMenu;
                        $privilege->idGroup = $idGroup;
                        $privilege->grantDate = Date::formatted_time();
                        $privilege->idUser = $this->getSessionParameter(self::USER_ID);
                        $privilege->save();
                    }
                }
            }
            Database::instance()->commit();
            $aResponse['data'] = $menusIDs;
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function fnGetMenuGroupPrivileges($idMenu = NULL) {

        $subQuery = DB::select(
                        'menu.idMenu'
                        , DB::expr('GROUP_CONCAT(`group`.`idGroup` ORDER BY `group`.`idGroup`) AS fGroup'))
                ->from(array('bts_menu','menu'), array('bts_group','group'))
                ->where('menu.status', '=', self::STATUS_ACTIVE)
                ->and_where('group.status', '=', self::STATUS_ACTIVE);
        if (is_null($idMenu))
            $subQuery->and_where('menu.idSuperMenu', 'IS', NULL);
        else
            $subQuery->and_where('menu.idSuperMenu', '=', $idMenu);
        if (!$this->getSessionParameter(self::USER_ROOT_OPTION))
            $subQuery->and_where('group.rootOption', '=', self::STATUS_DEACTIVE);
        $subQuery->group_by('menu.idMenu');

        $result = DB::select(
                        'menu.idMenu'
                        , 'menu.name'
                        , 'menu.type'
                        , 'menu.rootOption'
                        , 'menu.idSuperMenu'
                        , DB::expr('GROUP_CONCAT(`privilege`.`idGroup` ORDER BY `privilege`.`idGroup`) AS cGroup')
                        , 'fGroup.fGroup')
                ->from(array('bts_menu','menu'))
                ->join(array('bts_privilege','privilege'), 'LEFT')->on('menu.idMenu', '=', 'privilege.idMenu')
                ->join(array('bts_group','group'), 'LEFT')->on('group.idGroup', '=', 'privilege.idGroup')
                ->join(array($subQuery, 'fGroup'), 'LEFT')->on('menu.idMenu', '=', 'fGroup.idMenu')
                ->where('menu.status', '=', self::STATUS_ACTIVE)
                ->group_by('menu.idMenu')
                ->order_by('menu.order')
                ->where('menu.status', '=', self::STATUS_ACTIVE);

        if (is_null($idMenu))
            $result->and_where('menu.idSuperMenu', 'IS', NULL);
        else
            $result->and_where('menu.idSuperMenu', '=', $idMenu);
        if (!$this->getSessionParameter(self::USER_ROOT_OPTION))
            $result->and_where('group.rootOption', '=', self::STATUS_DEACTIVE);
        return $result->execute()->as_array();
    }

    public function action_listPrivilegies() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idMenu = $this->request->post('idm');
            $aResponse['data'] = $this->fnGetMenuGroupPrivileges($idMenu);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse);
    }
    
    public function fnGetSuperMenus($idMenu) {
        try {
            $menu = DB::select('bts_menu.idMenu', 'bts_menu.idSuperMenu')
                            ->from('bts_menu')->where('bts_menu.idMenu', '=', $idMenu)
                            ->as_object()->execute()->current();
            if ($menu != null) {
                if ($menu->idSuperMenu != null) {
                    return $idMenu . "," . $this->fnGetSuperMenus($menu->idSuperMenu);
                } else {
                    return $idMenu;
                }
            }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

}