<style>
    *{
        margin: 0px;
        padding: 0px;
        font-family: 'Calibri','Tahoma','Times New Roman';
    }
    .c-border-radius{
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }
    .c-margin{
        margin: 5px;
    }
    .c-text-justify{
        text-align: justify;
    }
    .c-text-right{
        text-align: right;
    }
    .c-text-left{
        text-align: left;
    }
    .c-text-center{
        text-align: center;
    }
    .c-fLeft{
        float: left;
    }
    .c-clear{
        clear: both;
    }
    .websale-oBorder{
        border: #CCCCCC dashed 2px;
    }

    .websale-mail-content{
        min-width: 100%;
    }
    .websale-title{
        font-size: 1.2em;
        text-align: center;
        background-color: #99CCFF;
        font-weight: bold;
    }
    .websale-header-info label{
        font-weight: bold;
    }
    .websale-customer-side{
        background-color: #3DCC60;
        color: white;
    }
    .websale-transaction-side{
        background-color: #FFB2B2;
    }
    .websale-table{
        font-size: smaller;
    }
    .websale-table th{
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
        background-color: #BADCFF;
    }
    .websale-table th div{
        margin: 4px 5px 4px 5px;
    }
</style>
<div class="c-margin websale-oBorder c-border-radius" style="margin: 5px; padding: 4px;">
    <div class="c-margin" style="margin: 5px; padding: 4px;">
        <div class="c-border-radius websale-title" 
             style="
             margin: 5px; 
             padding: 4px;
             font-size: 1.2em;
             text-align: center;
             background-color: #99CCFF;
             font-weight: bold;
             -moz-border-radius: 4px;
             -webkit-border-radius: 4px;
             border-radius: 4px;
             ">
            <p class="c-margin" style="margin: 5px; padding: 4px;"><? echo __("Información de Compra On-line desde "); ?>:company</p>
        </div>
    </div>
    <div class="c-margin" style="margin: 5px; padding: 4px;">
        <p class="c-text-justify">
            <?php echo __("Gracias por adquirir sus boletos de viaje desde nuestra página web."); ?>
            <?php echo __("Para adquirir nuevos boletos visítenos nuevamente a nuestra dirección en internet "); ?>
            <a href=":website">:website</a>
        </p>
        <center>
            <div class="websale-header-info" style="font-weight: bold; display: table;">
                <div class="c-margin c-border-radius websale-customer-side c-fLeft" 
                     style="
                     margin: 5px;
                     padding: 4px;
                     background-color: #3DCC60;
                     color: white;
                     float: left;
                     -moz-border-radius: 4px;
                     -webkit-border-radius: 4px;
                     border-radius: 4px;
                     ">
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label style="color: white;"><b><?php echo __("Cliente"); ?></b></b></label>
                        <p style="text-align: left">:client</p>
                    </div>
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label style="color: white;"><b><?php echo __("Dirección"); ?></b></label>
                        <p style="text-align: left">:address</p>
                    </div>
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label style="color: white;"><b><?php echo __("Ciudad"); ?></b></label>
                        <p style="text-align: left">:city</p>
                    </div>
                </div>
                <div style=" float: left; padding: 4px;"></div>
                <div class="c-margin c-border-radius websale-transaction-side c-fLeft"
                     style="
                     margin: 5px;
                     padding: 4px;
                     background-color: #FFB2B2;
                     float: left;
                     -moz-border-radius: 4px;
                     -webkit-border-radius: 4px;
                     border-radius: 4px;
                     ">
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label><b><?php echo __("Dirección de Email"); ?></b></label>
                        <p class="c-text-center" style="text-align: center;">:email</p>
                    </div>
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label><b><?php echo __("Número de Transacción Paypal"); ?></b></label>
                        <p class="c-text-center" style="text-align: center;">:transactionId</p>
                    </div>
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label><b><?php echo __("Número de Transacción Venta"); ?></b></label>
                        <p class="c-text-center" style="text-align: center;">:codeWebSale</p>
                    </div>
                    <div class="c-margin" style="margin: 5px; padding: 4px; text-align: left;">
                        <label><b><?php echo __("Monto de la Transacción"); ?></b></label>
                        <p class="c-text-right" style="text-align: right;">:currency :totalAmount</p>
                    </div>
                </div>
            </div>
        </center>
        <div class="c-clear" style="clear: both;"></div>
    </div>

    <div class="c-margin" style="margin: 5px; padding: 4px;">
        <div class="c-border-radius websale-title"
             style="
             margin: 5px; 
             padding: 4px;
             font-size: 1.2em;
             text-align: center;
             background-color: #99CCFF;
             font-weight: bold;
             -moz-border-radius: 4px;
             -webkit-border-radius: 4px;
             border-radius: 4px;
             ">
            <p class="c-margin" style="margin: 5px; padding: 4px;"><? echo __("Información de Viaje"); ?></p>
        </div>
    </div>
    <center>
        <div class="c-margin" style="margin: 5px; padding: 4px;">
            <table class="websale-table" style="font-size: smaller;">
                <thead>
                <th style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Origen"); ?></div></th>
                <th style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Destino"); ?></div></th>
                <th style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Fecha de Viaje"); ?></div></th>
                <th style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Asiento"); ?></div></th>
                <th style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Pasajero"); ?></div></th>
                <th colspan="2" style="-moz-border-radius: 4px;-webkit-border-radius: 4px;border-radius: 4px;background-color: #BADCFF;"><div style="margin: 4px 5px 4px 5px;"><?php echo __("Precio"); ?></div></th>
                </thead>
                <tbody>
                    <?php
                    $total = 0;
                    $symbol = NULL;
                    foreach ($tickets as $ticket) {
                        $total = (float) $total + (float) $ticket->amount;
                        if (is_null($symbol))
                            $symbol = $ticket->symbol;
                        ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $ticket->departureCity; ?></td>
                            <td style="text-align: center;"><?php echo $ticket->arrivalCity; ?></td>
                            <td><?php echo $ticket->travelDateFormat; ?></td>
                            <td style="text-align: center;"><?php echo $ticket->numberSeat; ?></td>
                            <td><?php echo $ticket->passenger; ?></td>
                            <td style="text-align: right;"><?php echo $ticket->symbol; ?></td>
                            <td style="text-align: right;"><?php echo $ticket->amount; ?></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="5"><div class="c-text-right" style="text-align: right;"><b><?php echo __("Total"); ?></b></div></td>
                        <td><div class="c-text-right" style="text-align: right;"><?php echo $symbol ?></div></td>
                        <td><div class="c-text-right" style="text-align: right;"><?php echo number_format($total, 2); ?></div></td>
                    </tr>
                    <tr>
                        <td colspan="5"><div class="c-text-right" style="text-align: right;"><b><?php echo __("Total Abonado"); ?></b></div></td>
                        <td><div class="c-text-right" style="text-align: right;">:currency</div></td>
                        <td><div class="c-text-right" style="text-align: right;">:totalAmount</div></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </center>
    <div style="font-size: x-small; color: gray; padding: 4px;">
        <p style="text-align: right;">
            <?php echo __("Para contactar con nuestro servicio de atención al cliente, visite nuestro Centro de Asistencia o llame al 877-815-1531 (dentro de los EE.UU.)"); ?>
            <br/><?php echo __("Atentamente"); ?>
            <br/>:contactCompany
            <br/>:contactAddress
            <br/>:contactPhone
            <br/>:website
        </p>
    </div>
</div>
