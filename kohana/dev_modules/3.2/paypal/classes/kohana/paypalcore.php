<?php

/* * ******************************************
  PayPal API Module

  Defines all the global variables and the wrapper functions
 * ****************************************** */

class Kohana_PaypalCore {

    protected $PROXY_HOST = '127.0.0.1';
    protected $PROXY_PORT = '808';
    protected $SandboxFlag = true;
//'------------------------------------
//' PayPal API Credentials
//' Replace <API_USERNAME> with your API Username
//' Replace <API_PASSWORD> with your API Password
//' Replace <API_SIGNATURE> with your Signature
//'------------------------------------
//$API_UserName = "hcm_1334359889_biz_api1.ibusplus.com";
//$API_Password = "1334359927";
//$API_Signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31ATWnRBQ4Ga-VTixHA0stz6q2WyMu";
    protected $API_UserName = '';
    protected $API_Password = '';
    protected $API_Signature = '';
// BN Code 	is only applicable for partners
    protected $sBNCode = "PP-ECWizard";
    protected $USE_PROXY = false;
    protected $version = "64";
    protected $API_Endpoint = '';
    protected $PAYPAL_URL = '';

    public function __construct() {
        $this->API_UserName = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['paypal_username'];
        $this->API_Password = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['paypal_password'];
        $this->API_Signature = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['paypal_signature'];
        $paypal_environment = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['paypal_environment'];
        if ($paypal_environment == 'sandbox') {
            $this->SandboxFlag = true;
        } else {
            $this->SandboxFlag = false;
        }
        if (isset($config['sand_box_flag'])) {
            $this->SandboxFlag = $config['sand_box_flag'];
        }
        if (isset($config['use_proxy'])) {
            $this->USE_PROXY = $config['use_proxy'];
        }
        if (isset($config['proxy_host'])) {
            $this->PROXY_HOST = $config['proxy_host'];
        }
        if (isset($config['proxy_port'])) {
            $this->PROXY_PORT = $config['proxy_port'];
        }

        if ($this->SandboxFlag) {
            $this->API_Endpoint = "https://api-3t.sandbox.paypal.com/nvp";
            $this->PAYPAL_URL = "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=";
        } else {
            $this->API_Endpoint = "https://api-3t.paypal.com/nvp";
            $this->PAYPAL_URL = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";
        }
    }

    public function CallShortcutExpressCheckout($codeFormat, $currencyCodeType, $paymentType, $returnURL, $cancelURL) {
        //------------------------------------------------------------------------------------------------------------------------------------
        // Construct the parameter string that describes the SetExpressCheckout API call in the shortcut implementation
        Session::instance()->set("paypal_currencyCodeType", $currencyCodeType);
        Session::instance()->set("paypal_PaymentType", $paymentType);
        //'--------------------------------------------------------------------------------------------------------------- 
        //' Make the API call to PayPal
        //' If the API call succeded, then redirect the buyer to PayPal to begin to authorize payment.  
        //' If an error occured, show the resulting errors
        //'---------------------------------------------------------------------------------------------------------------
        $resArray = $this->hash_call("SetExpressCheckout", $codeFormat);
        $ack = strtoupper($resArray["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            $token = urldecode($resArray["TOKEN"]);
            Session::instance()->set("paypal_TOKEN", $token);
        }

        return $resArray;
    }

    /*
      '-------------------------------------------------------------------------------------------------------------------------------------------
      ' Purpose: 	Prepares the parameters for the SetExpressCheckout API Call.
      ' Inputs:
      '		paymentAmount:  	Total value of the shopping cart
      '		currencyCodeType: 	Currency code value the PayPal API
      '		paymentType: 		paymentType has to be one of the following values: Sale or Order or Authorization
      '		returnURL:			the page where buyers return to after they are done with the payment review on PayPal
      '		cancelURL:			the page where buyers return to when they cancel the payment review on PayPal
      '		shipToName:		the Ship to name entered on the merchant's site
      '		shipToStreet:		the Ship to Street entered on the merchant's site
      '		shipToCity:			the Ship to City entered on the merchant's site
      '		shipToState:		the Ship to State entered on the merchant's site
      '		shipToCountryCode:	the Code for Ship to Country entered on the merchant's site
      '		shipToZip:			the Ship to ZipCode entered on the merchant's site
      '		shipToStreet2:		the Ship to Street2 entered on the merchant's site
      '		phoneNum:			the phoneNum  entered on the merchant's site
      '--------------------------------------------------------------------------------------------------------------------------------------------
     */

    public function CallMarkExpressCheckout($paymentAmount, $currencyCodeType, $paymentType, $returnURL, $cancelURL, $shipToName, $shipToStreet, $shipToCity, $shipToState, $shipToCountryCode, $shipToZip, $shipToStreet2, $phoneNum
    ) {
        //------------------------------------------------------------------------------------------------------------------------------------
        // Construct the parameter string that describes the SetExpressCheckout API call in the shortcut implementation

        $nvpstr = "&PAYMENTREQUEST_0_AMT=" . $paymentAmount;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $paymentType;
        $nvpstr = $nvpstr . "&RETURNURL=" . $returnURL;
        $nvpstr = $nvpstr . "&CANCELURL=" . $cancelURL;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $currencyCodeType;
        $nvpstr = $nvpstr . "&ADDROVERRIDE=1";
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTONAME=" . $shipToName;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET=" . $shipToStreet;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTREET2=" . $shipToStreet2;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCITY=" . $shipToCity;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOSTATE=" . $shipToState;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE=" . $shipToCountryCode;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOZIP=" . $shipToZip;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_SHIPTOPHONENUM=" . $phoneNum;


        Session::instance()->set("paypal_currencyCodeType", $currencyCodeType);
        Session::instance()->set("paypal_PaymentType", $paymentType);

        //'--------------------------------------------------------------------------------------------------------------- 
        //' Make the API call to PayPal
        //' If the API call succeded, then redirect the buyer to PayPal to begin to authorize payment.  
        //' If an error occured, show the resulting errors
        //'---------------------------------------------------------------------------------------------------------------
        $resArray = $this->hash_call("SetExpressCheckout", $nvpstr);
        $ack = strtoupper($resArray["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            $token = urldecode($resArray["TOKEN"]);
            Session::instance()->set("paypal_TOKEN", $token);
        }

        return $resArray;
    }

    /*
      '-------------------------------------------------------------------------------------------
      ' Purpose: 	Prepares the parameters for the GetExpressCheckoutDetails API Call.
      '
      ' Inputs:
      '		None
      ' Returns:
      '		The NVP Collection object of the GetExpressCheckoutDetails Call Response.
      '-------------------------------------------------------------------------------------------
     */

    public function GetShippingDetails($token, $typeProcess) {
        //'--------------------------------------------------------------
        //' At this point, the buyer has completed authorizing the payment
        //' at PayPal.  The function will call PayPal to obtain the details
        //' of the authorization, incuding any shipping information of the
        //' buyer.  Remember, the authorization is not a completed transaction
        //' at this state - the buyer still needs an additional step to finalize
        //' the transaction
        //'--------------------------------------------------------------
        //'---------------------------------------------------------------------------
        //' Build a second API request to PayPal, using the token as the
        //'  ID to get the details on the payment authorization
        //'---------------------------------------------------------------------------
        $nvpstr = "&TOKEN=" . $token;
        //'---------------------------------------------------------------------------
        //' Make the API call and store the results in an array.  
        //'	If the call was a success, show the authorization details, and provide
        //' 	an action to complete the payment.  
        //'	If failed, show the error
        //'---------------------------------------------------------------------------
        $resArray = $this->hash_call("GetExpressCheckoutDetails", $nvpstr);
        $data['type'] = '';
        $data['gateway'] = 'paypal';
        $data['interface'] = '';
        $data['action'] = 'pay';
        $data['checkstatus'] = $resArray['CHECKOUTSTATUS'];
        $data['token'] = $resArray['TOKEN'];
        $data['payerId'] = $resArray['PAYERID'];
        $data['date'] = DB::expr('NOW()');
        $data['custom'] = $resArray['PAYMENTREQUEST_0_CUSTOM'];
        $data['created_date'] = DB::expr('NOW()');
        $data['amount'] = $resArray['PAYMENTREQUEST_0_AMT'];
        $idCurrency = Session::instance()->get('DEFAULT_CURRENCY');
        $dollarEquivalency = Session::instance()->get('DOLLAR_EQUIVALENCY');

        if ($idCurrency == NULL) {
            $idCurrency = Session::instance('database')->get('DEFAULT_CURRENCY');
        }
        $data['normalAmount'] = round($resArray['PAYMENTREQUEST_0_AMT'] * $dollarEquivalency, 2);
        $data['firstName'] = $resArray['FIRSTNAME'];
        $data['lastName'] = $resArray['LASTNAME'];
        $data['email'] = $resArray['EMAIL'];
        $data['address'] = $resArray['SHIPTOSTREET'];
        $data['city'] = $resArray['SHIPTOCITY'];
        $data['country'] = $resArray['SHIPTOCOUNTRYNAME'];
        $data['currencyCode'] = $resArray['CURRENCYCODE'];
        $data['tc'] = $dollarEquivalency;
        $data['paymentType'] = $typeProcess;
        $data['ip'] = Request::$client_ip;
        if ($resArray['ACK'] == "Success") {
            $success = 1;
        } ELSE {
            $success = 0;
        }
        $data['success'] = (bool) $success;
        $resultLog = DB::insert('bx_payment_logs', array_keys($data))->values(array_values($data))->execute();
//        echo $resultLog ;
//        die();
        $resArray['idPaymentLog'] = $resultLog[0];

        $ack = strtoupper($resArray["ACK"]);
        if ($ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING") {
            Session::instance()->set("paypal_payer_id", $resArray['PAYERID']);
        }
        return $resArray;
    }

    /*
      '-------------------------------------------------------------------------------------------------------------------------------------------
      ' Purpose: 	Prepares the parameters for the GetExpressCheckoutDetails API Call.
      '
      ' Inputs:
      '		sBNCode:	The BN code used by PayPal to track the transactions from a given shopping cart.
      ' Returns:
      '		The NVP Collection object of the GetExpressCheckoutDetails Call Response.
      '--------------------------------------------------------------------------------------------------------------------------------------------
     */

    public function ConfirmPayment($FinalPaymentAmt, $idPaymentLog) {
        /* Gather the information to make the final call to
          finalize the PayPal payment.  The variable nvpstr
          holds the name value pairs
         */

        //Format the other parameters that were stored in the session from the previous calls	
        $token = urlencode(Session::instance()->get("paypal_TOKEN"));
        $paymentType = urlencode(Session::instance()->get("paypal_PaymentType"));
        $currencyCodeType = urlencode(Session::instance()->get("paypal_currencyCodeType"));
        $payerID = urlencode(Session::instance()->get("paypal_payer_id"));
        $serverName = urlencode($_SERVER['SERVER_NAME']);

        $nvpstr = '&TOKEN=' . $token . '&PAYERID=' . $payerID . '&PAYMENTREQUEST_0_PAYMENTACTION=' . $paymentType . '&PAYMENTREQUEST_0_AMT=' . $FinalPaymentAmt;
        $nvpstr .= '&PAYMENTREQUEST_0_CURRENCYCODE=' . $currencyCodeType . '&IPADDRESS=' . $serverName;

        /* Make the call to PayPal to finalize payment
          If an error occured, show the resulting errors
         */

        $resArray = $this->hash_call("DoExpressCheckoutPayment", $nvpstr);
        /* Display the API response back to the browser.
          If the response from PayPal was a success, display the response parameters'
          If the response was an error, display the errors received using APIError.php.
         */
        $paymentLog = new Model_PaymentPaypalLog($idPaymentLog);
        Session::instance()->set("paypalLogID", $idPaymentLog);
        if ($resArray['ACK'] == "Success") {
            $paymentLog->txn_id = $resArray['PAYMENTINFO_0_TRANSACTIONID'];
            $paymentLog->date = DateHelper::getFechaFormateadaActual();
            $paymentLog->type = $resArray['PAYMENTINFO_0_TRANSACTIONTYPE'];
            $paymentLog->interface = $resArray['PAYMENTINFO_0_PAYMENTTYPE'];
            $paymentLog->txn_id = $resArray['PAYMENTINFO_0_TRANSACTIONID'];
            $success = 1;
        } ELSE {
            $paymentLog->recieved_data = $resArray['L_SHORTMESSAGE0'];
            $success = 0;
        }
        $paymentLog->success = $success;
        $paymentLog->update();
        return $resArray;
    }

    /*
      '-------------------------------------------------------------------------------------------------------------------------------------------
      ' Purpose: 	This function makes a DoDirectPayment API call
      '
      ' Inputs:
      '		paymentType:		paymentType has to be one of the following values: Sale or Order or Authorization
      '		paymentAmount:  	total value of the shopping cart
      '		currencyCode:	 	currency code value the PayPal API
      '		firstName:			first name as it appears on credit card
      '		lastName:			last name as it appears on credit card
      '		street:				buyer's street address line as it appears on credit card
      '		city:				buyer's city
      '		state:				buyer's state
      '		countryCode:		buyer's country code
      '		zip:				buyer's zip
      '		creditCardType:		buyer's credit card type (i.e. Visa, MasterCard ... )
      '		creditCardNumber:	buyers credit card number without any spaces, dashes or any other characters
      '		expDate:			credit card expiration date
      '		cvv2:				Card Verification Value
      '
      '-------------------------------------------------------------------------------------------
      '
      ' Returns:
      '		The NVP Collection object of the DoDirectPayment Call Response.
      '--------------------------------------------------------------------------------------------------------------------------------------------
     */

    public function DirectPayment($paymentType, $paymentAmount, $creditCardType, $creditCardNumber, $expDate, $cvv2, $firstName, $lastName, $street, $city, $state, $zip, $countryCode, $currencyCode) {
        //Construct the parameter string that describes DoDirectPayment
        $nvpstr = "&AMT=" . $paymentAmount;
        $nvpstr = $nvpstr . "&CURRENCYCODE=" . $currencyCode;
        $nvpstr = $nvpstr . "&PAYMENTACTION=" . $paymentType;
        $nvpstr = $nvpstr . "&CREDITCARDTYPE=" . $creditCardType;
        $nvpstr = $nvpstr . "&ACCT=" . $creditCardNumber;
        $nvpstr = $nvpstr . "&EXPDATE=" . $expDate;
        $nvpstr = $nvpstr . "&CVV2=" . $cvv2;
        $nvpstr = $nvpstr . "&FIRSTNAME=" . $firstName;
        $nvpstr = $nvpstr . "&LASTNAME=" . $lastName;
        $nvpstr = $nvpstr . "&STREET=" . $street;
        $nvpstr = $nvpstr . "&CITY=" . $city;
        $nvpstr = $nvpstr . "&STATE=" . $state;
        $nvpstr = $nvpstr . "&COUNTRYCODE=" . $countryCode;
        $nvpstr = $nvpstr . "&IPADDRESS=" . $_SERVER['REMOTE_ADDR'];

        $resArray = $this->hash_call("DoDirectPayment", $nvpstr);

        return $resArray;
    }

    /**
      '-------------------------------------------------------------------------------------------------------------------------------------------
     * hash_call: Function to perform the API call to PayPal using API signature
     * @methodName is name of API  method.
     * @nvpStr is nvp string.
     * returns an associtive array containing the response from the server.
      '-------------------------------------------------------------------------------------------------------------------------------------------
     */
    public function hash_call($methodName, $nvpStr) {

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        //if USE_PROXY constant set to TRUE in Constants.php, then only proxy will be enabled.
        //Set proxy name to PROXY_HOST and port number to PROXY_PORT in constants.php 
        if ($this->USE_PROXY)
            curl_setopt($ch, CURLOPT_PROXY, $this->PROXY_HOST . ":" . $this->PROXY_PORT);

        //NVPRequest for submitting to server
        $nvpreq = "METHOD=" . urlencode($methodName) . "&VERSION=" . urlencode($this->version) . "&PWD=" . urlencode($this->API_Password) . "&USER=" . urlencode($this->API_UserName) . "&SIGNATURE=" . urlencode($this->API_Signature) . $nvpStr . "&BUTTONSOURCE=" . urlencode($this->sBNCode);

        //setting the nvpreq as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        //getting response from server
        $response = curl_exec($ch);

        //convrting NVPResponse to an Associative Array
        $nvpResArray = $this->deformatNVP($response);
        $nvpReqArray = $this->deformatNVP($nvpreq);

        Session::instance()->get("paypal_nvpReqArray", $nvpReqArray);

        if (curl_errno($ch)) {
            // moving to display page to display curl errors            
            Session::instance()->get("paypal_curl_error_no", curl_errno($ch));
            Session::instance()->get("paypal_curl_error_msg", curl_error($ch));

            //Execute the Error handling module to display errors. 
        } else {
            //closing the curl
            curl_close($ch);
        }

        return $nvpResArray;
    }

    /* '----------------------------------------------------------------------------------
      Purpose: Redirects to PayPal.com site.
      Inputs:  NVP string.
      Returns:
      ----------------------------------------------------------------------------------
     */

    public function RedirectToPayPal($token) {

        // Redirect to paypal.com here
        $payPalURL = $this->PAYPAL_URL . $token;
        Request::initial()->redirect($payPalURL);
    }

    /* '----------------------------------------------------------------------------------
     * This function will take NVPString and convert it to an Associative Array and it will decode the response.
     * It is usefull to search for a particular key and displaying arrays.
     * @nvpstr is NVPString.
     * @nvpArray is Associative Array.
      ----------------------------------------------------------------------------------
     */

    public function deformatNVP($nvpstr) {
        $intial = 0;
        $nvpArray = array();

        while (strlen($nvpstr)) {
            //postion of Key
            $keypos = strpos($nvpstr, '=');
            //position of value
            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

            /* getting the Key and Value values and storing in a Associative Array */
            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
            //decoding the respose
            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }

}
