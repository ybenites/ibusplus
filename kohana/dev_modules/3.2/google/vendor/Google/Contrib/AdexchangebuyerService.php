<?php

/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

namespace Google\Contrib;

use Google\Service\Service as Service;

use Google\Client as Client;

use Google\Contrib\DirectDealsServiceResource as DirectDealsServiceResource;

use Google\Contrib\AccountsServiceResource as AccountsServiceResource;

use Google\Contrib\CreativesServiceResource as CreativesServiceResource;

/**
 * Service definition for Adexchangebuyer (v1).
 *
 * <p>
 * Lets you manage your Ad Exchange Buyer account.
 * </p>
 *
 * <p>
 * For more information about this service, see the
 * <a href="https://developers.google.com/ad-exchange/buyer-rest" target="_blank">API Documentation</a>
 * </p>
 *
 * @author Google, Inc.
 */
class AdexchangebuyerService extends Service {

    public $directDeals;
    public $accounts;
    public $creatives;

    /**
     * Constructs the internal representation of the Adexchangebuyer service.
     *
     * @param Client $client
     */
    public function __construct(Client $client) {
        $this->servicePath = 'adexchangebuyer/v1/';
        $this->version = 'v1';
        $this->serviceName = 'adexchangebuyer';

        $client->addService($this->serviceName, $this->version);
        $this->directDeals = new DirectDealsServiceResource($this, $this->serviceName, 'directDeals', json_decode('{"methods": {"list": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "path": "directdeals", "response": {"$ref": "DirectDealsList"}, "id": "adexchangebuyer.directDeals.list", "httpMethod": "GET"}, "get": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"id": {"required": true, "type": "string", "location": "path", "format": "int64"}}, "id": "adexchangebuyer.directDeals.get", "httpMethod": "GET", "path": "directdeals/{id}", "response": {"$ref": "DirectDeal"}}}}', true));
        $this->accounts = new AccountsServiceResource($this, $this->serviceName, 'accounts', json_decode('{"methods": {"patch": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"id": {"required": true, "type": "integer", "location": "path", "format": "int32"}}, "request": {"$ref": "Account"}, "response": {"$ref": "Account"}, "httpMethod": "PATCH", "path": "accounts/{id}", "id": "adexchangebuyer.accounts.patch"}, "list": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "path": "accounts", "response": {"$ref": "AccountsList"}, "id": "adexchangebuyer.accounts.list", "httpMethod": "GET"}, "update": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"id": {"required": true, "type": "integer", "location": "path", "format": "int32"}}, "request": {"$ref": "Account"}, "response": {"$ref": "Account"}, "httpMethod": "PUT", "path": "accounts/{id}", "id": "adexchangebuyer.accounts.update"}, "get": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"id": {"required": true, "type": "integer", "location": "path", "format": "int32"}}, "id": "adexchangebuyer.accounts.get", "httpMethod": "GET", "path": "accounts/{id}", "response": {"$ref": "Account"}}}}', true));
        $this->creatives = new CreativesServiceResource($this, $this->serviceName, 'creatives', json_decode('{"methods": {"insert": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "request": {"$ref": "Creative"}, "response": {"$ref": "Creative"}, "httpMethod": "POST", "path": "creatives", "id": "adexchangebuyer.creatives.insert"}, "list": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"pageToken": {"type": "string", "location": "query"}, "maxResults": {"location": "query", "minimum": "1", "type": "integer", "maximum": "1000", "format": "uint32"}}, "response": {"$ref": "CreativesList"}, "httpMethod": "GET", "path": "creatives", "id": "adexchangebuyer.creatives.list"}, "get": {"scopes": ["https://www.googleapis.com/auth/adexchange.buyer"], "parameters": {"adgroupId": {"required": true, "type": "string", "location": "query", "format": "int64"}, "buyerCreativeId": {"required": true, "type": "string", "location": "path"}, "accountId": {"required": true, "type": "integer", "location": "path", "format": "int32"}}, "id": "adexchangebuyer.creatives.get", "httpMethod": "GET", "path": "creatives/{accountId}/{buyerCreativeId}", "response": {"$ref": "Creative"}}}}', true));
    }

}
