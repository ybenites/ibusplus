<?php 

spl_autoload_register(function($class){
        if (strpos($class, 'Google') !== 0) return;
    
	$class = str_replace('\\', '/', $class);
	require_once('vendor/' . $class . '.php');
});