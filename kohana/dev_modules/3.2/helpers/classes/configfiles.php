<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of config_files
 *
 * @author Pepe , Walter
 */
class ConfigFiles {

    /**
     * Retorna un array con todos los js | css, del modulo especificado
     *
     * @param string $sPageName Modulo
     * @param string $sTypeFile JS | css
     * @return array|string
     */
    public static function fnGetFiles($sPageName, $sTypeFile) {
        $mediaPath = realpath('../../../../kohana/dev_modules/3.2/media');

        try {
//            if ($sPageName == 'private_zone') {
//                $repofile = fopen("media/$sTypeFile/ibusplus.repo.code.$sTypeFile", 'cb');
//            } else {
//                $repofile = fopen("media/$sTypeFile/ibusplus.repo.code.$sTypeFile", 'ab');
//            }

            $aMediaFiles = DB::select(
                                    DB::expr("CONCAT(bts_file.fileName,'.',bts_file.fileType) AS mediaFile_fileName")
                                    , array('bts_file.fileType', 'mediaFile_fileType')
                            )
                            ->from('bts_file')
                            ->join('bts_section')->on('bts_file.idFile', '=', 'bts_section.idFile')
                            ->join('bts_page')->on('bts_page.idPage', '=', 'bts_section.idPage')
                            ->and_where('bts_page.pageName', '=', $sPageName)
                            ->and_where('bts_file.fileType', '=', $sTypeFile)
                            ->and_where('bts_page.status', '=', 1)
                            ->and_where('bts_file.status', '=', 1)
                            ->order_by('bts_file.fileType')
                            ->order_by('bts_section.fileOrder')
                            ->execute()->as_array();
            foreach ($aMediaFiles as $key => $aMediaFile) {
//                if (file_exists("media/$sTypeFile/" . $aMediaFile['mediaFile_fileName'])) {
                    $aMediaFiles[$key]['mediaFile_fileName'] = "media/$sTypeFile/" . $aMediaFile['mediaFile_fileName'];
//                } else {
//
//                    $filename = "$mediaPath/$sTypeFile/" . $aMediaFile['mediaFile_fileName'];
//                    $fichero_url = fopen(trim($filename), "r");
//                    fwrite($repofile, fread($fichero_url, filesize($filename)));
//                    fclose($fichero_url);
//                    unset($aMediaFiles[$key]);
//                }
            }

//            fclose($repofile);


            return $aMediaFiles;
        } catch (Database_Exception $e) {
            echo $e->getTraceAsString();
        }
    }

//
//    public static function fnGetBtsFiles($sPageName, $sTypeFile) {
//        
//        try {
//            $aMediaFiles = DB::select(DB::expr("CONCAT(bts_file.fileName,'.',bts_file.fileType) AS mediaFile_fileName"), array('bts_file.fileType', 'mediaFile_fileType'))
//                            ->from('bts_file')
//                            ->join('bts_section')->on('bts_file.idFile', '=', 'bts_section.idFile')
//                            ->join('bts_page')->on('bts_page.idPage', '=', 'bts_section.idPage')
//                            ->and_where('bts_page.pageName', '=', $sPageName)
//                            ->and_where('bts_file.fileType', '=', $sTypeFile)
//                            ->and_where('bts_page.status', '=', 1)
//                            ->and_where('bts_file.status', '=', 1)
//                            ->order_by('bts_file.fileType')
//                            ->order_by('bts_section.fileOrder')
//                            ->execute()->as_array();
//            foreach ($aMediaFiles as $key => $aMediaFile) {
//                $aMediaFiles[$key]['mediaFile_fileName'] = "$mediaPath/$sTypeFile/" . $aMediaFile['mediaFile_fileName'];
//            }
//
//            return $aMediaFiles;
//        } catch (Database_Exception $e) {
//            echo $e->getMessage();
//        }
//    }
}