<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StringHelper
 *
 * @author Emmanuel
 */
class StringHelper {

    public static function cleanEmptyString4NULL($string, $isNumber=FALSE) {
        if (strcmp(trim($string), '') == 0) {
            if ($isNumber) {
                return 0;
            } else {
                return NULL;
            }
        } else {
            return trim($string);
        }
    }

    /**
     * Devuelve TRUE si la cadena inicia con la palabra indicada
     * @param type $Haystack Cadena
     * @param type $Needle Palabra
     * @return type
     */
    public static function stringStartsWith($Haystack, $Needle) {
        return strpos($Haystack, $Needle) === 0;
    }

    public static function realUpperCase($str) {
        return strtr(strtoupper($str), "àèìòùáéíóúçñäëïöü", "ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
    }

}