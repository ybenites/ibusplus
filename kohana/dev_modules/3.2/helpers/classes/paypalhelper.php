<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * Funciones especiales

 */

/**
 * Funciones especiales para uso domestico
 *
 * @author Hardlick
 */
class PaypalHelper {

    static function fnSetPreSalePaypal($items,$customHtml,$subtotal,$total,$returnURL,$cancelURL,$currencyCodeType,$paymentType){
        $nvpstr = "&SOLUTIONTYPE=Sole";
        $nvpstr = $nvpstr . "&BUYEREMAILOPTINENABLE=1";    
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_AMT=$total";        
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CUSTOM=$customHtml";
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_ITEMAMT=$subtotal";
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_PAYMENTACTION=" . $paymentType;
        $nvpstr = $nvpstr . "&RETURNURL=" . $returnURL;
        $nvpstr = $nvpstr . "&CANCELURL=" . $cancelURL;
        $nvpstr = $nvpstr . "&PAYMENTREQUEST_0_CURRENCYCODE=" . $currencyCodeType;
        
        foreach ($items as $key => $value) {
            $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NUMBER$key=" . $value['L_PAYMENTREQUEST_0_NUMBERn'];
            $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_NAME$key=" . $value['L_PAYMENTREQUEST_0_NAMEn'];
            $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_DESC$key=" . $value['L_PAYMENTREQUEST_0_DESCn'];
            $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_AMT$key=" . $value['L_PAYMENTREQUEST_0_AMTn'];
            $nvpstr = $nvpstr . "&L_PAYMENTREQUEST_0_QTY$key=" . $value['L_PAYMENTREQUEST_0_QTYn'];
        }
        return $nvpstr;
    }

}