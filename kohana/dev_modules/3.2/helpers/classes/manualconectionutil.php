<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mconection
 *
 * @author Emmanuel
 */
include_once (implode(Kohana::find_file('config', $_SERVER['HTTP_HOST']. '_db')));

class ManualConectionUtil {

    public static function conection() {
        $host_name =db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['serverDBName'];
        $port =db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['serverPort'];
        if (!(strcmp(trim($port), '') == 0)) {
            $host_name = $host_name . ":" . $port;
        }
        $user_name =db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['dbUser'];
        $password =db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['dbPassword'];
        $datbase_name =db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['dbName'];

        if (!($conection = mysql_connect($host_name, $user_name, $password))) {
            echo "Error al conectarse a la base de datos.";
            exit();
        }
        if (!mysql_select_db($datbase_name, $conection)) {
            echo "Error al seleccionar la base de datos.";
            exit();
        }
        return $conection;
    }

}