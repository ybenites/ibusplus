<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * Funciones especiales

 */

/**
 * Funciones especiales para uso domestico
 *
 * @author Hardlick
 */
class CacheFile {

    /**
     * Funcion deleteAllCacheSesion
     * @author Hardlick
     */
    static function deleteAllCacheSesion($files) {
      
        try {
            foreach ($files as $value) {
                unlink($value);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Funcion deleteAllCache
     * @author Hardlick
     */
    static function deleteAllCache($cacheDir=null) {        
        if ($cacheDir==null)
        {
        FileHelper::deleteFileOrFolderRecursive(APPPATH.'cache/',FALSE);    
        }
        else
        {
        FileHelper::deleteFileOrFolderRecursive(APPPATH.'cache/'.$cacheDir.'/',FALSE);    
        }        
    }

}