<?php

class UserHelper {

    public static function fnGetAvailableWriteMessage() {

        $a_response = self::fnGetPrivilegesMessages();

        if ($a_response->allow_write == 1) {
            return true;
        }
        return false;
    }

    public static function fnGetAvailableReplyMessage() {

        $a_response = self::fnGetPrivilegesMessages();

        if ($a_response->allow_reply == 1) {
            return true;
        }
        return false;
    }

    public static function fnGetLastDateNotification() {
        try {
            $iUserId = Session::instance('database')->get('USER_ID');

            $oSelectUser = DB::select(array(DB::expr("UNIX_TIMESTAMP(COALESCE(u.lastDateMessage, '0000-00-00 00:00:00'))"), 'lastNotification'))
                            ->from(array('bts_user', 'u'))
                            ->where('u.idUser', '=', $iUserId)
                            ->as_object()
                            ->execute()->current();
            if ($oSelectUser) {
                return $oSelectUser->lastNotification;
            } else {
                return '';
            }
        } catch (Database_Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Total de mensajes que faltan mostrarse apartir de (Pagina)
     * 
     * @param int $iStart
     * @return object Totales 
     */
    public static function fnGetCountPreviousNotification($iPage = 0, $i_last_notification = 0) {
        try {
            $iCityId = Session::instance('database')->get('USER_CITY_ID');
            $iOfficeId = Session::instance('database')->get('USER_OFFICE_ID');
            $iUserId = Session::instance('database')->get('USER_ID');



            $oSelectCountPreviousMessages = DB::select(
                            array(DB::expr('count(DISTINCT me.idMessage)')
                                , 'totalPrevious')
                    )
                    ->from(array('bts_messages', 'me'))
                    ->limit(Controller_Private_Privatezone::MAX_ROW_LIMIT)
                    ->where(
                            DB::expr('UNIX_TIMESTAMP(me.creationDate)'), '<=', intval(self::fnGetLastDateNotification())
                    )
                    ->and_where(
                            'me.userCreate', '<>', intval($iUserId)
                    )
                    ->and_where(
                            DB::expr("
                        CASE
                            WHEN me.toIdCity = 0 AND me.toIdOffice = 0 AND me.toIdUser = 0 THEN TRUE
                        ")
                            , 'ELSE', DB::expr("me.toIdCity = :ID_CITY OR me.toIdOffice = :ID_OFFICE OR me.toIdUser = :ID_USER 
                        END", array(':ID_CITY' => intval($iCityId), ':ID_OFFICE' => intval($iOfficeId), ':ID_USER' => intval($iUserId))))
                    ->and_where('me.state', '=', 1)
                    ->as_object()
                    ->execute()
                    ->current();
            //echo Database::instance()->last_query;die();
            if ($oSelectCountPreviousMessages) {
                $iCountReturn = (($oSelectCountPreviousMessages->totalPrevious)
                        - (($iPage + 1) * Controller_Private_Privatezone::MSG_COUNT_DISPLAY) - (self::fnGetCountAfterNotification($i_last_notification)));
                return $iCountReturn < 0 ? 0 : $iCountReturn;
            } else {
                return 0;
            }
        } catch (Database_Exception $e) {
            return $e->getMessage();
        }
    }

    public static function fnGetCountAfterNotification($i_last_notification) {
        try {
            $iCityId = Session::instance('database')->get('USER_CITY_ID');
            $iOfficeId = Session::instance('database')->get('USER_OFFICE_ID');
            $iUserId = Session::instance('database')->get('USER_ID');

            $oSelectCountPreviousMessages = DB::select(
                            array(DB::expr('count(DISTINCT me.idMessage)')
                                , 'totalAfter')
                    )
                    ->from(array('bts_messages', 'me'))
                    ->limit(Controller_Private_Privatezone::MAX_ROW_LIMIT)
                    ->where(
                            DB::expr('UNIX_TIMESTAMP(me.creationDate)'), '>', $i_last_notification
                    )
                    ->and_where(
                            'me.userCreate', '<>', intval($iUserId)
                    )
                    ->and_where(
                            DB::expr("
                        CASE
                            WHEN me.toIdCity = 0 AND me.toIdOffice = 0 AND me.toIdUser = 0 THEN TRUE
                        ")
                            , 'ELSE', DB::expr(" 
                                    me.toIdCity = :ID_CITY OR me.toIdOffice = :ID_OFFICE OR me.toIdUser = :ID_USER 
                        END", array(':ID_CITY' => intval($iCityId), ':ID_OFFICE' => intval($iOfficeId), ':ID_USER' => intval($iUserId))))
                    ->and_where('me.state', '=', 1)
                    ->as_object()
                    ->execute()
                    ->current();
            //echo Database::instance()->last_query;die();
            if ($oSelectCountPreviousMessages) {
                return $oSelectCountPreviousMessages->totalAfter < 0 ? 0 : $oSelectCountPreviousMessages->totalAfter;
            } else {
                return 0;
            }
        } catch (Database_Exception $e) {
            return $e->getMessage();
        }
    }

    public static function fnGetLastNotifications($iPageOffset = 0, $i_last_notification = 0) {
        try {
            $iCityId = Session::instance('database')->get('USER_CITY_ID');
            $iOfficeId = Session::instance('database')->get('USER_OFFICE_ID');
            $iUserId = Session::instance('database')->get('USER_ID');

            $iCountPageOffset = $iPageOffset * Controller_Private_Privatezone::MSG_COUNT_DISPLAY;

            if ($i_last_notification == 0)
                $i_last_notification = self::fnGetLastDateNotification();

            $iCountPageOffset += ( intval(self::fnGetCountAfterNotification($i_last_notification)));

            $oSelectMessages = DB::select(
                                    array(
                                'me.idMessage'
                                , 'idOut'
                                    )
                                    , array(
                                'me.message'
                                , 'messageOut'
                                    )
                                    , array(
                                'ur.fullName'
                                , 'userRegistrationOut'
                                    )
                                    , array(
                                DB::expr("REPLACE(us.imgProfile, '/128/', '/64/')")
                                , 'user_image'
                                    )
                                    , array(
                                DB::expr('UNIX_TIMESTAMP(me.creationDate)')
                                , 'updateNotification'
                                    )
                                    , array(
                                DB::expr("DATE_FORMAT(me.creationDate, '%d/%m/%Y %h:%i %p')")
                                , 'dateCreationOut'
                                    )
                                    , array(
                                "me.toIdCity"
                                , 'cityOut'
                                    )
                                    , array(
                                "me.toIdOffice"
                                , 'officeOut'
                                    )
                                    , array(
                                "me.toIdUser"
                                , 'userOut'
                                    )
                                    , array(
                                "me.userCreate"
                                , 'userCreateOut'
                                    ), array(
                                'me.idResponse'
                                , 'responseOut'
                                    )
                            )
                            ->from(array('bts_messages', 'me'))
                            ->limit(Controller_Private_Privatezone::MSG_COUNT_DISPLAY)
                            ->offset($iCountPageOffset)
                            ->order_by('me.creationDate', 'DESC')
                            ->join(array('bts_user', 'us'))
                            ->on('me.userCreate', '=', 'us.idUser')
                            ->join(array('bts_person', 'ur'))
                            ->on('me.userCreate', '=', 'ur.idPerson')
                            ->join(array('bts_person', 'u'), 'LEFT')
                            ->on('me.toIdUser', '=', 'u.idPerson')
                            ->join(array('bts_office', 'o'), 'LEFT')
                            ->on('me.toIdOffice', '=', 'o.idOffice')
                            ->join(array('bts_city', 'c'), 'LEFT')
                            ->on('me.toIdCity', '=', 'c.idCity')
                            ->where(
                                    DB::expr('UNIX_TIMESTAMP(me.creationDate)'), '<=', intval(self::fnGetLastDateNotification())
                            )
                            ->and_where(
                                    'me.userCreate', '<>', intval($iUserId)
                            )
                            ->and_where(
                                    DB::expr("
                                    CASE
                                        WHEN me.toIdCity = 0 AND me.toIdOffice = 0 AND me.toIdUser = 0 THEN TRUE
                                    ")
                                    , 'ELSE', DB::expr("
                                        me.toIdCity = :ID_CITY OR me.toIdOffice = :ID_OFFICE OR me.toIdUser = :ID_USER 
                                    END"
                                            , array(':ID_CITY' => intval($iCityId), ':ID_OFFICE' => intval($iOfficeId), ':ID_USER' => intval($iUserId))))
                            ->and_where('me.state', '=', 1)
                            ->execute()->as_array();

            return array_merge(
                            $oSelectMessages
                            , array(
                        'previousCount' => self::fnGetCountPreviousNotification($iPageOffset, $i_last_notification)
                        , 'date' => self::fnGetCurrentDate()
                            )
            );
        } catch (Database_Exception $e) {
            return $e->getMessage();
        }
    }

    public static function fnGetCurrentDate() {
        return array(
            'current_date' => date('Y-m-d H:i:s'),
            'current_unix_date' => time()
        );
    }

    public static function fnGetPrivilegesMessages() {
        try {
            $i_group_id = Session::instance('database')->get(Controller_Private_Privatezone::USER_GROUP_ID);
            $o_select = DB::select(
                            array('gr.allowWriteMessages', 'allow_write')
                            , array('gr.allowReplyMessages', 'allow_reply')
                    )
                    ->from(array('bts_group', 'gr'))
                    ->where('gr.idGroup', '=', $i_group_id)
                    ->as_object()
                    ->execute()
                    ->current();
            return $o_select;
        } catch (Exception $exc) {
            return array();
        }
    }

//mensajes
    public static function fnGetLastDateNotificationMessage($i_user_id = null) {
        try {
            $o_select_user = DB::select(array(DB::expr("UNIX_TIMESTAMP(COALESCE(m.creationDate, '0000-00-00 00:00:00'))"), 'lastNotification'))
                    ->from(array('bts_messages', 'm'));

            if ($i_user_id != null) {
                $o_select_user = $o_select_user->where('m.userCreate', '=', $i_user_id);
            }
            $o_select_user = $o_select_user->order_by('m.creationDate', 'DESC')
                            ->as_object()
                            ->execute()->current();

            if ($o_select_user) {
                return $o_select_user->lastNotification;
            } else {
                return '';
            }
        } catch (Database_Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public static function fngetInfoMessages($i_page_offset = 0, $i_last_notification = 0, $i_user_id = null, $s_date_format = '', $s_time_format= '') {

        try {

            $i_count_page_offset = $i_page_offset * Kohana_Private_Icontroller::MSG_COUNT_DISPLAY_TOTAL;

            if ($i_user_id != null) {
                if ($i_last_notification == 0)
                    $i_last_notification = self::fnGetLastDateNotificationMessage($i_user_id);
            }else {
                if ($i_last_notification == 0)
                    $i_last_notification = self::fnGetLastDateNotificationMessage();
            }


            $o_select_messages = DB::select(
                            array('m.idMessage', 'idMessage')
                            , array('pr.fullName', 'sender')
                            , array('ur.idUser', 'idUserSender')
                            , DB::expr("IF(m.toIdUser > 0,ud.idUser,0) AS idUserDestination")
                            , array('ur.imgProfile', 'img')
                            , DB::expr("DATE_FORMAT(m.creationDate,'$s_date_format $s_time_format') as creation_dates")
                            , array(DB::expr('UNIX_TIMESTAMP(m.creationDate)'), 'lastNotification')
                            , array('m.creationDate', 'creation_date')
                            , array(DB::expr(" 
                    CASE
                     WHEN m.toIdCity = 0 AND m.toIdOffice = 0 AND m.toIdUser = 0
                     THEN 'todos'
                     WHEN m.toIdCity > 0 AND m.toIdOffice = 0 AND m.toIdUser = 0
                     THEN CONCAT('ciudad: ', c.name)
                     WHEN m.toIdCity = 0 AND m.toIdOffice > 0 AND m.toIdUser = 0
                     THEN CONCAT('oficina: ', o.name)
                     ELSE 
                     pd.fullName END"), 'destination')
                            , array('m.message', 'message')
                    )
                    ->from(array('bts_messages', 'm'))
                    ->limit(Kohana_Private_Icontroller::MSG_COUNT_DISPLAY_TOTAL)
                    ->offset($i_count_page_offset)
                    ->order_by('m.creationDate', 'Desc')
                    ->join(array('bts_user', 'ur'))->on('m.userCreate', '=', 'ur.idUser')
                    ->join(array('bts_person', 'pr'))->on('ur.idUser', '=', 'pr.idPerson')
                    ->join(array('bts_user', 'ud'), 'LEFT')->on('m.toIdUser', '=', 'ud.idUser')
                    ->join(array('bts_person', 'pd'), 'LEFT')->on('ud.idUser', '=', 'pd.idPerson')
                    ->join(array('bts_city', 'c'), 'LEFT')->on('m.toIdCity', '=', 'c.idCity')
                    ->join(array('bts_office', 'o'), 'LEFT')->on('m.toIdOffice', '=', 'o.idOffice')
                    ->where(DB::expr('UNIX_TIMESTAMP(m.creationDate)'), '<=', intval($i_last_notification));
            if ($i_user_id != null) {
                $o_select_messages = $o_select_messages->where('m.userCreate', '=', $i_user_id);
            }
            $o_select_messages = $o_select_messages->execute()->as_array();
            return array_merge($o_select_messages, array('previousCount' => self::fnGetCountMessages($i_page_offset, $i_last_notification, $i_user_id)), array('lastNotification' => self::fnGetLastDateNotificationMessage($i_user_id)));
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

    public static function fnGetCountMessages($i_page = 0, $i_last_notification = 0, $i_user_id = null) {
        try {

            $i_page_num = ($i_page + 1) * Kohana_Private_Icontroller::MSG_COUNT_DISPLAY_TOTAL;

            $o_selectCount_previous_messages = DB::select(
                            array(DB::expr('count(DISTINCT me.idMessage)'), 'totalPrevious')
                    )
                    ->from(array('bts_messages', 'me'))
                    ->where(
                    DB::expr('UNIX_TIMESTAMP(me.creationDate)'), '<=', intval($i_last_notification)
            );
            if ($i_user_id != null) {
                $o_selectCount_previous_messages = $o_selectCount_previous_messages->and_where('me.userCreate', '=', $i_user_id);
            }

            $o_selectCount_previous_messages = $o_selectCount_previous_messages->as_object()
                    ->execute()
                    ->current();

            if ($o_selectCount_previous_messages) {
                return $o_selectCount_previous_messages->totalPrevious - $i_page_num;
            } else {
                return 0;
            }
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

}
