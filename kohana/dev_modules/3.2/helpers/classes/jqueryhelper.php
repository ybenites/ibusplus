<?php

defined('SYSPATH') or die('No direct script access.');

class jQueryHelper {

    public static function JSON_jQuery_encode($array_json, $isArray=true) {        
        if ($isArray)
            return (isset($_REQUEST["jsoncallback"])?$_REQUEST["jsoncallback"].'(':'' ) . json_encode($array_json) . (isset($_REQUEST["jsoncallback"])?')':'');
        else
            return (isset($_REQUEST["jsoncallback"])?$_REQUEST["jsoncallback"].'(':'' ) . ($array_json) . (isset($_REQUEST["jsoncallback"])?')':'');
    }

}