<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * Funciones especiales

 */

/**
 * Funciones especiales para uso domestico
 *
 * @author Hardlick
 */
class DateHelper {

    /**
     * Funcion getFechaActual
     * Sirve para formatear la fecha de forma predeterminada para el insertado en la base de datos mysql format
     * Formato Y-m-d
     * @author Hardlick
     */
    static function getfechaActual() {
        //formateado para el insertado en la bd
        $fechaA = date('Y-m-d');
        return $fechaA;
    }
    /**
     * Funcion getAddDaysToActualDate
     *Sirve para sumar dias a una fecha
     * Formato Y-m-d
     * @author Hardlick
     */
   static function getAddDaysToActualDate($dias) {

        $calculo = strtotime("$dias days");
        return date("Y-m-d", $calculo);
    }

    /**
     * Funcion getFechaFormateadaActual
     * Sirve para formatear la fecha para la vista del usuario
     * Formato Y-m-d H-M-S
     * @author Hardlick
     */
    static function getFechaFormateadaActual() {
        $fechaA = strftime("%Y-%m-%d %H:%M:%S", time());
        return $fechaA;
    }

    /**
     * Funcion getFechaAnterior
     * Sirve para obtener la fecha del dia anterior basandose en la fecha actual,
     * formatear la fecha de forma predeterminada para el insertado en la base de datos mysql format
     * Formato Y-m-d
     * @author Hardlick
     */
    static function getfechaAnterioraHoy() {
        //formateado para el insertado en la bd
        $dia = date("Y-m-d", time() - 86400);
        return $dia;
    }

    /**
     * Convertir el formato 00/00/0000 mes/dia/año, a formato de MYSQL
     * Fecha en Nuestro Formato
     * Creamos una lista de variables a la cual le asignamos los valores parciales de $dtm_fechainicial, divididos por el signo "/"
     * @author Hardlick
     */
    static function cambiafmysqlre($dtm_fechainicial) {
        //El List depende del formato usado Ahora es Dia / Mes / Anhio        
        list($dia, $mes, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    /**
     * Convertir el formato 0000-00-00 año-mes-dia a formato: mes/dia/año.
     * Fecha en Nuestro Formato
     * Creamos una lista de variables a la cual le asignamos los valores parciales de $dtm_fechainicial, divididos por el signo "/"
     * @author -JD-
     */
    static function fnFormatTimeStamp($sTimeStamp, $sFormat) {
        //list($sFormatoDate, $sFormatoTime) = explode(' ', $sTimeStamp);        
        $cTimeStamp = strtotime($sTimeStamp);
        return date($sFormat, $cTimeStamp);        
    }
    
    /**
     * Convertir el formato 0000-00-00 año-mes-dia a formato: mes/dia/año.
     * Fecha en Nuestro Formato
     * Creamos una lista de variables a la cual le asignamos los valores parciales de $dtm_fechainicial, divididos por el signo "/"
     * @author Hardlick
     */
    static function cambiafmysql($fecha) {
        ereg("([0-9]{1,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
        $lafecha = $mifecha[2] . "/" . $mifecha[3] . "/" . $mifecha[1];
        return $lafecha;
    }

    /**
     * Convertir el formato de Hora Militar a 12 Horas      
     * @author Hardlick
     */
    static function datetimemilitarto12timehour($time) {

        $time = date('d/m/Y h:i A', strtotime($time));
        return $time;
    }

    static function fnChangeFormatJsMysql($sDateFormat, $bReverse = false) {

        $aMysql = array(
            '%W' //Dia nombre completo
            , '%a' //Dia Nombre Abreviado 3 letras
            , '%d' //Dia 2 digitos
            , '%e' //Dia 1 digito
            , '%M' //Mes Nombre Completo
            , '%b' //Mes Nombre Abreviado 3 letras
            , '%m' //Mes 2 Digitos
            , '%c' //Mes 1 Digito
            , '%Y' //Anhio completo
            , '%y' //Anhio 2 ultimos digitos
            , '%h' //12 Horas agregado 0
            , '%l' //12 Horas sin 0
            , '%H' //24 horas agregado 0
            , '%k' //24 horas sin 0
            , '%i' //Minutos agregado 0
            , '%i' //Minutos sin 0 //No Hay
            , '%s' //Segundo agregado 0
            , '%s' //Segundos sin 0 //No hay
        );

        $aJs = array(
            'dddd' //Dia nombre completo
            , 'ddd' //Dia Nombre Abreviado 3 letras
            , 'dd' //Dia 2 digitos
            , 'd' //Dia 1 digito
            , 'mmmm' //Mes Nombre Completo
            , 'mmm' //Mes Nombre Abreviado 3 letras
            , 'mm' //Mes 2 Digitos
            , 'm' //Mes 1 Digito
            , 'yyyy' //Anhio completo
            , 'yy' //Anhio 2 ultimos digitos
            , 'hh' //12 Horas agregado 0
            , 'h' //12 Horas sin 0
            , 'HH' //24 horas agregado 0
            , 'H' //24 horas sin 0
            , 'MM' //Minutos agregado 0
            , 'M' //Minutos sin 0
            , 'ss' //Segundo agregado 0
            , 's' //Segundos sin 0
        );

        if ($bReverse) {
            /*$aSearch = array_map(function($sValue) {
                        return "/([^%]|^)" . $sValue . "(?!" . substr($sValue, 0, 1) . ")/";
                    }, $aMysql);*/
            $aSearch = $aMysql;
            $aReplace = $aJs;
        } else {
            /*$aSearch = array_map(function($sValue) {
                        return "/([^%]|^)" . $sValue . "(?!" . substr($sValue, 0, 1) . ")/";
                    }, $aJs);*/
            $aSearch = $aJs;
            $aReplace = $aMysql;
        }

        $iCount = 3;
        echo str_replace($aSearch,$aReplace, $sDateFormat);

        //echo $iCount;

        //return preg_replace($aSearch, $aReplace, $sDateFormat);
    }
	static function fnPHPDate2MySQLDatetime($php_date) {
        //  print_r( get_class_methods($php_date));
        //print_r($php_date->date_format('Y-m-d H:i:s'));
        // die();
        return $php_date->format('Y-m-d H:i:s');
    }

    static function convertHourToFormatSql($date) {
        $e = explode('/', $date);
        if(count($e)==1){
            list($m, $d, $a) = explode('-', $date);  
        }else{
          list($d, $m, $a) = explode('/', $date);  
        }
        $array = array($a, $m, $d);
        return implode('-', $array);
    }

    static function getHoraFormato($hora) {
        $explo1 = explode('.', $hora);
        if (count($explo1) > 1) {
            list($a, $b) = array($explo1[0], $explo1[1]);
        } else {
            $a = $explo1[0];
            $b = -1;
        }
        if ($b != -1) {
            $b = '0.' . $b;
            $b = DateHelper::getConvertNumber($b);
            $b = $b * 60;
            $explo2 = explode('.', $b);
            if (count($explo2) > 1) {
                list($x, $y) = array($explo2[0], $explo2[1]);
                $y = '0.' . $y;
                $y = DateHelper::getConvertNumber($y);
                $y = (int) ($y * 60);
            } else {
                $x = $explo2[0];
                $y = 0;
            }
        } else {
            $x = 0;
            $y = 0;
        }
        $horaConvertida = array($a, $x, $y);
        return implode(':', $horaConvertida);
    }

    static function getHoraActual() {

        $today = date("H:i:s");
        return $today;
    }

    static function getDiferenciaentredosHoras($hora1, $hora2) {
        $fechaIni = $hora1;
        $fechaFin = $hora2;
        $fecha1 = strtotime($hora1);
        $fecha2 = strtotime($hora2);
        $valor = $fecha2 - $fecha1;
        $valor = round($valor / 3600, 4);
        return $valor;
    }

    static function getConvertNumber($dato) {
        $caracteres = "%[^0-9 \. \- ]%";
        $str = preg_replace($caracteres, "", $dato);
        return $str;
    }

    static function getPrimerDiaDelMes($date) {
        list($a, $m, $d) = explode('-', $date);
        $array = array($a, $m, '01');
        return implode('-', $array);
    }

    static function getDaysWorked($horas) {
        $dias = $horas / 8;
        $explo1 = explode('.', $dias);
        if (count($explo1) > 1) {
            list($d, $h) = array($explo1[0], $explo1[1]);
            $h = '0.' . $h;
            $h = DateHelper::getConvertNumber($h);
            $h = $h * 8;
            $horas2 = DateHelper::getHoraFormato($h);
            $horas3 = explode(':', $horas2);
            $h = $horas3[0];
            $m = $horas3[1];
            $s = $horas3[2];
        } else {
            $d = $explo1[0];
            $h = 0;
            $m = 0;
            $s = 0;
        }
        $valor = array($d, $h, $m, $s);
        return implode(':', $valor);
    }
	
    
    
}
