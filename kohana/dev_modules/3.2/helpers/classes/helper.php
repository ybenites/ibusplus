<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of function
 *
 * @author Pepe
 */
class Helper {

    public static function comboSimple(
    $objectModel, $attrId, $attrValue, $idCombo, $class = null,$msg = null) {
        if ($class != null) {
            $class = ' class="' . $class . '" ';
        }
        if ($msg != null) {
            $msg = __($msg);
        } else {
            $msg = __("Seleccione una Opción....");
        }
        echo '<select ' . $class . 'style="width : 185px" id="' . $idCombo . '" name="' . $idCombo . '">';
        echo '<option value="">' . $msg . '</option>';
        if (is_array($objectModel)) {
            foreach ($objectModel as $key => $item) {
                echo '<option value="' . $key . '">' . __($item) . '</option>';
            }
        } else {
            foreach ($objectModel as $item) {
                echo '<option value="' . $item->$attrId . '">' . $item->$attrValue . '</option>';
            }
        }
        echo '</select>';
    }

    public static function selectAll($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL) {
        $object_list = ORM::factory($tableName)->order_by($tableValue, 'ASC')->find_all();
        echo '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        echo '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($object_list as $object) {
            echo '<option value="' . $object->$tableId . '">' . $object->$tableValue . '</option>';
        }
        echo '</select>';
    }
    public static function selectAllNotEcho($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL) {
        $object_list = ORM::factory($tableName)->order_by($tableValue, 'ASC')->find_all();
        $html =  '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        $html .=  '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($object_list as $object) {
            $html .=  '<option value="' . $object->$tableId . '">' . $object->$tableValue . '</option>';
        }
        $html .=  '</select>';
        return $html;
    }

    public static function selectAllByStatus($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL, $status=null) {
        $object_list = ORM::factory($tableName)->where('status', '=', $status)->order_by($tableValue, 'ASC')->find_all();
        echo '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        echo '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($object_list as $object) {
            echo '<option value="' . $object->$tableId . '">' . $object->$tableValue . '</option>';
        }
        echo '</select>';
    }

    public static function selectTicketTypeByStatus($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL, $status=null) {
        $object_list = ORM::factory($tableName)->where('status', '=', $status)->order_by($tableValue, 'ASC')->find_all();
        echo '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        echo '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($object_list as $object) {
                if ($object->$tableValue == 'Normal')
                {
                    echo '<option id="' . $object->discount . '" formatDiscount="' . $object->type . '"  value="' . $object->$tableId . '" selected="selected">' . $object->$tableValue . '</option>';
                }
                else
                {
                    echo '<option id="' . $object->discount . '" formatDiscount="' . $object->type . '" value="' . $object->$tableId . '">' . $object->$tableValue . '</option>';
                }
            
        }
        echo '</select>';
    }

    public static function selectCurrency($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL) {
        $tipousuarios = ORM::factory($tableName)->order_by($tableValue, 'ASC')->where('currency.endLife', 'IS', NULL)->find_all();
        echo '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        echo '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($tipousuarios as $tipoUsuario) {
            echo '<option value="' . $tipoUsuario->$tableId . '">' . $tipoUsuario->$tableValue . '</option>';
        }
        echo '</select>';
    }
    public static function selectCurrencyNotEcho($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL) {
        $tipousuarios = ORM::factory($tableName)->order_by($tableValue, 'ASC')->where('currency.endLife', 'IS', NULL)->find_all();
        $html = '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        $html .= '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($tipousuarios as $tipoUsuario) {
            $html .= '<option value="' . $tipoUsuario->$tableId . '">' . $tipoUsuario->$tableValue . '</option>';
        }
        $html .= '</select>';
        return $html;
    }

    public static function selectBus($tableName, $tableId, $tableValue, $id, $multiple=NULL, $class =NULL) {
        $tipousuarios = ORM::factory($tableName)->order_by($tableValue, 'ASC')->where('bus.defaultchoose', '=', 1)->find_all();
        echo '<select style="width : 185px" class="' . $class . '" id="' . $id . '" ' . (isset($onChange) ? $onChange : "") . ' name="' . $tableName . '">';
        echo '<option value="">' . __("Seleccione una Opción....") . '</option>';
        foreach ($tipousuarios as $tipoUsuario) {
            echo '<option value="' . $tipoUsuario->$tableId . '">' . $tipoUsuario->$tableValue . '</option>';
        }
        echo '</select>';
    }

    public static function dateremove($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0) {

        $date_r = getdate(strtotime($date));
        $date_result = date("Y-m-d H:i:s", mktime(($date_r["hours"] - $hh), ($date_r["minutes"] - $mn), ($date_r["seconds"] - $ss), ($date_r["mon"] - $mm), ($date_r["mday"] - $dd), ($date_r["year"] - $yy)));
        return $date_result;
    }

    public static function dateadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0) {

        $date_r = getdate(strtotime($date));
        $date_result = date("Y-m-d H:i:s", mktime(($date_r["hours"] + $hh), ($date_r["minutes"] + $mn), ($date_r["seconds"] + $ss), ($date_r["mon"] + $mm), ($date_r["mday"] + $dd), ($date_r["year"] + $yy)));

        return $date_result;
    }
    public static function dateformatremove($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0) {

        $date_r = getdate(strtotime($date));
        $date_result = date("d/m/Y H:i:s", mktime(($date_r["hours"] - $hh), ($date_r["minutes"] - $mn), ($date_r["seconds"] - $ss), ($date_r["mon"] - $mm), ($date_r["mday"] - $dd), ($date_r["year"] - $yy)));
        return $date_result;
    }

    public static function dateformatadd($date, $dd=0, $mm=0, $yy=0, $hh=0, $mn=0, $ss=0) {

        $date_r = getdate(strtotime($date));
        $date_result = date("d/m/Y H:i:s", mktime(($date_r["hours"] + $hh), ($date_r["minutes"] + $mn), ($date_r["seconds"] + $ss), ($date_r["mon"] + $mm), ($date_r["mday"] + $dd), ($date_r["year"] + $yy)));

        return $date_result;
    }

    public static function selectNumbers($ini, $end, $name) {

        echo '<select class="ui-corner-all ui-toolbar ui-widget-content" id="id' . $name . '" name="' . $name . '">';
        for ($i = $ini; $i <= $end; $i++) {
           $ia = str_pad($i, 2, "0", STR_PAD_LEFT); 
            echo '<option value="' . $ia . '">' . $ia . '</option>';
        }
        echo '</select>';
    }

    public static function textFieldValue($tableName, $tableValue, $tableIdValue, $fieldType) {
        $tipousuarios = ORM::factory($tableName)->find_all($tableIdValue);

        foreach ($tipousuarios as $tipoUsuario) {

            $select = +'<input name="txtUsuario" id="txt' . $tableIdValue . '" value="' . $tipoUsuario->$tableValue . '" type="' . $fieldType . '"/>';
        }

        return $select;
    }

    public static function getImages($dir) {
        try {
            $array = scandir($dir);
            $cnt = sizeof($array);
            $j = 0;
            for ($i = 0; $i < $cnt; $i++) {

                if (substr($array[$i], -3) == 'png') {
                    $newArray[$j] = $array[$i];
                    $j++;
                }
            }
            $size = sizeof($newArray);
            for ($x = 0; $x < $size; $x++) {
                $xi = $x;
                echo '<tr>';
                echo "<td width='20' align='center'><img onClick=icon('" . $newArray[$xi] . "') style='cursor: pointer;' width='16px' height='16px' src='/" . $dir . "/" . $newArray[$xi] . "' /></td> ";
                echo ($xi + 1 < $size) ? "<td width='20' align='center'><img onClick=icon('" . $newArray[$xi + 1] . "') style='cursor: pointer;' width='16px' height='16px' src='/" . $dir . "/" . $newArray[$xi + 1] . "' /></td> " : '<td></td>';
                $x = $x + 1;
                echo ($xi + 2 < $size) ? "<td width='20' align='center'><img onClick=icon('" . $newArray[$xi + 2] . "') style='cursor: pointer;' width='16px' height='16px' src='/" . $dir . "/" . $newArray[$xi + 2] . "' /></td> " : '<td></td>';
                $x = $x + 1;
                echo ($xi + 3 < $size) ? "<td width='20' align='center'><img onClick=icon('" . $newArray[$xi + 3] . "') style='cursor: pointer;' width='16px' height='16px' src='/" . $dir . "/" . $newArray[$xi + 3] . "' /></td> " : '<td></td>';
                $x = $x + 1;
                echo '</tr>';
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public static function getHtmlStructure($Pagina, $htmlstructureType) {
        if ($Pagina == '') {
            die();
        } else {
            try {
                $query = DB::select('htmlstructure.htmlCode')
                                ->from('htmlstructure')
                                ->where('dominio.serverName', '=', $_SERVER['HTTP_HOST'])
                                ->and_where('pagina.nombre', '=', $Pagina)
                                ->and_where('htmlstructure.htmlStructureType', '=', $htmlstructureType)
                                ->join('dominio')
                                ->on('dominio.idDominio', '=', 'htmlstructure.idDominio')
                                ->join('pagina')
                                ->on('pagina.idPagina', '=', 'htmlstructure.idPagina')
                                ->execute()->as_array();

                foreach ($query as $salida) {
                    echo $salida['htmlCode'];
                }
            } catch (Database_Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public static function getOptionRoot($table, $column) {
        try {
            $query = DB::select($column)
                            ->from($table)
                            ->where('defaultChoose', '=', '1')
                            ->execute()->as_array();

            foreach ($query as $salida) {
                return $salida[$column];
            }
        } catch (Database_Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function getOptionValue($key) {
        try {
            $var = Controller_Private_Privatezone::getSessionParameter('system_options');
            return $var[$key]['value'];
        } catch (Database_Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function getOptionKeys() {
        try {
            $options = DB::select('key', 'value')
                            ->from(array('bts_options','options'))->where('options.status', '=', 1)
                            ->execute()->as_array('key');

            return $options;
        } catch (Database_Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function randomKeys($in, $to_num = false, $pad_up = false, $passKey = null)
{
  $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  if ($passKey !== null) {
    // Although this function's purpose is to just make the
    // ID short - and not so much secure,
    // with this patch by Simon Franz (http://blog.snaky.org/)
    // you can optionally supply a password to make it harder
    // to calculate the corresponding numeric ID
 
    for ($n = 0; $n<strlen($index); $n++) {
      $i[] = substr( $index,$n ,1);
    }
 
    $passhash = hash('sha256',$passKey);
    $passhash = (strlen($passhash) < strlen($index))
      ? hash('sha512',$passKey)
      : $passhash;
 
    for ($n=0; $n < strlen($index); $n++) {
      $p[] =  substr($passhash, $n ,1);
    }
 
    array_multisort($p,  SORT_DESC, $i);
    $index = implode($i);
  }
 
  $base  = strlen($index);
 
  if ($to_num) {
    // Digital number  <<--  alphabet letter code
    $in  = strrev($in);
    $out = 0;
    $len = strlen($in) - 1;
    for ($t = 0; $t <= $len; $t++) {
      $bcpow = bcpow($base, $len - $t);
      $out   = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
    }
 
    if (is_numeric($pad_up)) {
      $pad_up--;
      if ($pad_up > 0) {
        $out -= pow($base, $pad_up);
      }
    }
    $out = sprintf('%F', $out);
    $out = substr($out, 0, strpos($out, '.'));
  } else {
    // Digital number  -->>  alphabet letter code
    if (is_numeric($pad_up)) {
      $pad_up--;
      if ($pad_up > 0) {
        $in += pow($base, $pad_up);
      }
    }
 
    $out = "";
    for ($t = floor(log($in, $base)); $t >= 0; $t--) {
      $bcp = bcpow($base, $t);
      $a   = floor($in / $bcp) % $base;
      $out = $out . substr($index, $a, 1);
      $in  = $in - ($a * $bcp);
    }
    $out = strrev($out); // reverse
  }
 
  return $out;
}

public static function fnGetDrawJqueryButton($a_object, $s_name, $s_type = 'radio', $a_extra_params = array()) {

        $s_extra_params = '';
        if (is_array($a_extra_params)) {
            foreach ($a_extra_params as $a_param) {
                if (is_array($a_param)) {
                    if (
                            array_key_exists(Rms_Constants::ARRAY_KEY_NAME, $a_param)
                            && array_key_exists(Rms_Constants::ARRAY_KEY_VALUE, $a_param)
                    ) {
                        $s_extra_params.= ' data-' . $a_param[Rms_Constants::ARRAY_KEY_NAME];
                        $s_extra_params.= '="' . $a_param[Rms_Constants::ARRAY_KEY_VALUE] . '"';
                    }
                }
            }
        }

        return "<input 
                    type='" . $s_type . "' 
                    id='" . $s_name . $a_object['short'] . "' 
                    name='" . $s_name . ($s_type=='checkbox'?'[]':''). "'
                    value='" . $a_object['value'] . "'
                    " . (array_key_exists('default', $a_object)?($a_object['default'] ? 'checked' : '') :''). "
                        " . $s_extra_params . "
                />
                <label for='" . $s_name . $a_object['short'] . "'>
                    " . __($a_object['display']) . "
                </label>";
    }
    static function checkEmail( $email ){
    return filter_var( $email, FILTER_VALIDATE_EMAIL );
    }
}