<?php 

spl_autoload_register(function($className){
	if(strpos($className,'Dropbox_')===0) {

        include dirname(__FILE__) . '/' . str_replace('_','/',substr($className,8)) . '.php';

    }
});
?>