<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of moduleitem
 *
 * @author Pepe
 */
class ModuleItem {

    public $name;
    public $desc;
    public $id;
    public $items = array();

    function __construct($name, $desc, $items, $id=NULL) {
        $this->name = $name;
        $this->items = $items;
        $this->id = $id;
        $this->desc = $desc;
    }

    public static function printModule($objMI,$guion=null) {

        $out = '';
//        print_r($objMI);
//        die();
        foreach($objMI as $obj) {
            if (empty($obj['items'])) {
                $out = $out . '<option value="' . $obj['id']. '">-'.$guion . $obj['name'] . '</option>';
            } else {
                $out = $out . '<option value="' . $obj['id']. '">-'.$guion . $obj['name'] . '</option>';
                foreach ($obj['items']as $mitem) {
                    if (empty($mitem['items'])) {
                        $out = $out . '<option value="' . $mitem['id'] . '">--'.$guion . $mitem['name'] . '</option>';
                    } else {
                        $out = $out . '--<option value="' . $mitem['id'] . '">--'.$guion . $mitem['name'] . '</option>';
                        $out = $out . ModuleItem::printModule($mitem['items'],'--'.$guion);
                    }
                }
            }
        }
        return $out;
    }

    /**
     *
     * @param ModuleItem $moduleItemRow
     * @param int $margin_left
     * @param array<GroupPrivilege> $groups
     * @return string la fila de la tabla tblPrivileges (view/private/privilege)
     */
    public function printModuleItemRow($moduleItemRow, $margin_left=0, $groups=NULL) {

        $has_child = (!empty($moduleItemRow->items));
        if ($has_child) {
            $class_name = "close_node";
        } else {
            $class_name = $this->getNodeClassName($moduleItemRow->type);
        }
        $html_row = "";
        $html_row = $html_row . str_ireplace("module_id", $moduleItemRow->id, str_ireplace("class_name", $class_name, str_ireplace("margin_left_value", $margin_left, str_ireplace("module_name", __($moduleItemRow->name), str_ireplace("new_row", "", str_ireplace("checkbox_group", $this->getCheckBoxRow($moduleItemRow->groups, $moduleItemRow, $moduleItemRow->user_privilege), $html_row_template))))));
        if ($has_child) {
            $margin_left = $margin_left + 25;
            foreach ($moduleItemRow->items as $row) {
                if (empty($row->items)) {
                    $class_name = $this->getNodeClassName($row->type);
                    $html_row = $html_row . str_ireplace("module_id", $row->id, str_ireplace("class_name", $class_name, str_ireplace("margin_left_value", $margin_left, str_ireplace("module_name", __($row->name), str_ireplace("new_row", "", str_ireplace("checkbox_group", $this->getCheckBoxRow($row->groups, $row, $row->user_privilege), $html_row_template))))));
                } else {
                    $class_name = "close_node";
                    $html_row = $html_row . $row->printModuleItemRow($row, $margin_left, $row->groups);
                }
            }
        }
        return $html_row;
    }

}

?>
