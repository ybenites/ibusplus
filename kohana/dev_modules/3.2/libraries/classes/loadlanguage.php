<?php

defined('SYSPATH') or die('No direct script access.');

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Clase loadLanguage
 *  ------------ Crea archivos de traduccion a partir de la Base de datos, tabla traduccion.
 * Funcion loadData($controllersArray,$language,$modoTraduccion)
 * $controllersArray: Es el arreglo de controladores a generar
 * $language : el idioma que quieres que genere es,fr,br,en
 * $modoTraduccion : 0 si es se desea el idioma base (es) ó 1 se desea el idioma base traducido (en)
 * $deleteFiles : si es 1 entonces elimina los archivos, antes de crearlos.
 * Author: Emmanuel Walter Cumpa Niño.
 * Fecha : 11/02/2011
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class LoadLanguage {

    public function loadData($language_file_name, $language_base, $language_target, $deleteFiles = false) {
        $language_file_name = APPPATH . "i18n" . DIRECTORY_SEPARATOR . $language_target . DIRECTORY_SEPARATOR . $language_file_name;
        if ($deleteFiles)
            FileHelper::deleteFileOrFolderRecursive($language_file_name . EXT);

        if (file_exists($language_file_name . EXT) == false) {
            $conection = ManualConectionUtil::conection();
            mysql_query("SET NAMES utf8");
            $translate_condition = "";
            $modoTraduccion = 1;
            if ($language_base != $language_target) {
                $modoTraduccion = 0;
                $translate_condition = "AND `language_target`='" . $language_target . "'";
            }
            $result = mysql_query("SELECT * FROM `bts_translate` WHERE `language_base`='" . $language_base . "'", $conection);

            $nfilas = mysql_num_rows($result);

            if ($nfilas > 0) {
                $controllerFile = fopen($language_file_name . EXT, 'a') or die("problemas al crear archivo");
                fputs($controllerFile, "<?php \n /*" . $language_file_name . ".php -- archivo de idiomas Kohana " . strtoupper($language_base) . " \n");
                fputs($controllerFile, " Author: Emmanuel Walter Cumpa Niño \n");
                fputs($controllerFile, " Kohana Framework 3.1 Todos los derecho reservados */ \n\n");
                fputs($controllerFile, "defined('SYSPATH') or die('No direct script access.'); \n\n");
                fputs($controllerFile, "return array\n");
                fputs($controllerFile, "(\n");

                for ($i = 0; $i < $nfilas - 1; $i++) {
                    $row = mysql_fetch_array($result);
                    if ($modoTraduccion == 0) {
                        $text = $row["text"];
                        $textTranslate = $row["textTranslate"];
                    } else {
                        $text = $row["textTranslate"];
                        $textTranslate = $row["text"];
                    }
                    fputs($controllerFile, "\t'" . str_ireplace("'", "\'", $text) . "' => '" . str_ireplace("'", "\'", $textTranslate) . "', \n");
                }
                $row = mysql_fetch_array($result);
                if ($modoTraduccion == 0) {
                    $text = $row["text"];
                    $textTranslate = $row["textTranslate"];
                } else {
                    $text = $row["textTranslate"];
                    $textTranslate = $row["text"];
                }
                fputs($controllerFile, "\t'" . str_ireplace("'", "\'", $text) . "' => '" . str_ireplace("'", "\'", $textTranslate) . "' \n");

                fputs($controllerFile, " \n); \n");
                fputs($controllerFile, " \n ");
                fclose($controllerFile);
            }
            mysql_close($conection);
        }
    }

}

?>
