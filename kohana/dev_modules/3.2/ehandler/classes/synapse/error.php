<?php

class Synapse_Error {

	public $type    = NULL;
	public $code    = NULL;
	public $message = NULL;
	public $file    = NULL;
	public $line    = NULL;
	public $text    = NULL;
	public $trace   = array();
	public $traceAsString   =null;
	public $display = NULL;
        public $skin = NULL;
        public $exception = NULL;

	/**
	 * Replaces Kohana's `Kohana::exception_handler()` method. This does the
	 * same thing, but also adds email functionality and the ability to perform
	 * an action in response to the exception. These actions and emails are
	 * customizable per type in the config file for this module.
	 *
	 * @uses    Kohana::exception_text
	 * @param   object   exception object
	 * @return  boolean
	 */
	public static function handle_exception(Exception $e)
	{ 
		try
		{
			$error = new Error();

			// Get the exception information
			$error->type    = get_class($e);
			$error->code    = $e->getCode();
			$error->message = $e->getMessage();
			$error->file    = $e->getFile();
			$error->line    = $e->getLine();
                       
                        if (!db_config::$db_conection_config[SITE_DOMAIN]['skin']) {
                        $error->skin = 'quatrobus';
                    } else {                      
                        $error->skin = db_config::$db_conection_config[SITE_DOMAIN]['skin'];
                    }
			// Create a text version of the exception
			$error->text = Kohana_Exception::text($e);

			
			// Get the exception backtrace
			$error->trace = $e->getTrace();
			$error->traceAsString = $e->getTraceAsString();
			$error->exception = $e;

			if ($e instanceof ErrorException)
			{
				if (isset(Kohana_Exception::$php_errors[$error->code]))
				{
					// Use the human-readable error name
					$error->code = Kohana_Exception::$php_errors[$error->code];
				}
			}

			if ( ! headers_sent())
			{
				// Make sure the proper content type is sent with a 500 status
				header('Content-Type: text/html; charset='.Kohana::$charset, TRUE, 500);
			}

			// Get the contents of the output buffer
			$error->display = $error->render();
                         $a_jquery_variables = Kohana::$config->load('jquery_version');    
                        $error->jquery= $a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_VERSION];
                            $error->jqueryui= $a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_UI_VERSION];        

        
        
			// Log the error
			$error->log();

			// Email the error
			if ($error->config('email'))
			{
				$error->email();
			}

			// Respond to the error
			$error->action();

			return TRUE;
		}
		catch (Exception $e)
		{  
			// Clean the output buffer if one exists
			ob_get_level() and ob_clean();

			// Display the exception text
			if (Kohana::$environment === Kohana::PRODUCTION)
			{
				ob_start();
				include Kohana::find_file('views', 'errors/_default');
				echo ob_get_clean();
			}
			else
			{
                        $remove = array("\n", "\r\n", "\n\r", "\r");
                        $params = str_replace($remove, '', Debug::dump($_REQUEST));                 
                        $moreMessage ='<b>Error en: </b> ' . $_SERVER['HTTP_HOST'] . '<br/><br/>';                   
                        $moreMessage .= '<b>URI:</b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
                        $moreMessage .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
                        $moreMessage .= '<b>HTTP USER AGENT:</b> ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>';
                         if (isset($_SERVER['REMOTE_ADDR'])) {
                            $moreMessage .= '<b>REMOTE_ADDR:</b> ' . $_SERVER['REMOTE_ADDR'] . '<br/>';
                        }
                        if (isset($_SERVER['HTTP_REFERER'])) {
                            $moreMessage .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_REFERER'] . '<br/>';
                        }
                        if (isset($_SERVER['REDIRECT_URL'])) {
                            $moreMessage .= '<b>REDIRECT URL:</b> ' . $_SERVER['REDIRECT_URL'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_INFO'])) {
                            $moreMessage .= '<b>PATH INFO:</b> ' . $_SERVER['PATH_INFO'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_TRANSLATED'])) {
                            $moreMessage .= '<b>PATH TRANSLATED:</b> ' . $_SERVER['PATH_TRANSLATED'] . '<br/>';
                        }
                        if (isset($_SERVER['SCRIPT_FILENAME'])) {
                            $moreMessage .= '<b>SCRIPT FILENAMEL:</b> ' . $_SERVER['SCRIPT_FILENAME'] . '<br/>';
                        }
                        $moreMessage .= '<b>PARAMETROS: </b><br/><pre>' . $params . '</pre><br/><br/>';                  
            
                    $info = sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($e), $e->getCode(), '<div style="width: 100%;word-wrap:break-word;">' . $moreMessage . strip_tags($e->getMessage()) . '</div>', Debug::path($e->getFile()), $e->getLine());
                    
                    Kohana::$log->add(LOG::CRITICAL
                            , ':info'
                            , array(
                        ':info' => $info
                    ));
                    $strace = $info . "\n--\n" . $e->getTraceAsString();
                    Kohana::$log->add(Log::STRACE, $strace);
			}

			// Exit with an error status
			exit(1);
		}
	}

	/**
	 * PHP error handler, converts all errors into ErrorExceptions. This handler
	 * respects error_reporting settings.
	 *
	 * @throws  ErrorException
	 * @return  TRUE
	 */
	public static function handle_error($code, $error, $file = NULL, $line = NULL)
	{
		if (error_reporting() & $code)
		{
			// This error is not suppressed by current error reporting settings
			// Convert the error into an ErrorException
			throw new ErrorException($error, $code, 0, $file, $line);
		}

		// Do not execute the PHP error handler
		return TRUE;
	}

	/**
	 * Replace Kohana's `Kohana::shutdown_handler()` method with one that will
	 * use our error handler. This is to catch errors that are not normally
	 * caught by the error handler, such as E_PARSE.
	 *
	 * @uses    Error::handler
	 * @return  void
	 */
	public static function handle_shutdown()
	{
		if ( ! Kohana::$_init)
		{
			// Do not execute when not active
			return;
		}

		try
		{
			if (Kohana::$caching === TRUE AND Kohana::$_files_changed === TRUE)
			{
				// Write the file path cache
				Kohana::cache('Kohana::find_file()', Kohana::$_files);
			}
		}
		catch (Exception $e)
		{
			// Pass the exception to the handler
			Kohana::exception_handler($e);
		}

		if (Kohana_Exception::$errors AND $error = error_get_last() AND in_array($error['type'], Kohana_Exception::$shutdown_errors))
		{
			// Clean the output buffer
			ob_get_level() and ob_clean();
			// Fake an exception for nice debugging
			Error::handle_exception(new ErrorException($error['message'], $error['type'], 0, $error['file'], $error['line']));

			// Shutdown now to avoid a "death loop"
			exit(1);
		}
	}

	/**
	 * Retrieves the config settings for the exception type, and cascades down
	 * to the _default settings if there is nothing relavant to the type.
	 *
	 * @param   string  $key      The config key
	 * @param   mixed   $default  A default value to return
	 * @return  mixed
	 */
	public function config($key, $default = NULL)
	{           
	 $config = Kohana::$config->load('error.' . $this->type . ':' . $this->code . '.' . $key);
        if (Kohana::$environment != Kohana::DEVELOPMENT) {
            $config = !is_null($config) ? $config : Kohana::$config->load('error.' . $this->type . '.' . $key);
            $config = !is_null($config) ? $config : Kohana::$config->load('error._default.' . $key);
        }
        return!is_null($config) ? $config : $default;
	}

	/**
	 * Renders an error with a view file. The view library is not used because
	 * there is a chance that it will fail within this context.
	 *
	 * @param   string  $view  The view file
	 * @return  string
	 */
	public function render($view = 'kohana/error')
	{
		// Start an output buffer
		ob_start();

		// Import error variables into View's scope
		$error = get_object_vars($this);
		unset($error['display']);
		extract($error);

		// Include the exception HTML
		include Kohana::find_file('views', $view);

		// Get the contents of the output buffer
		return ob_get_clean();
	}

	/**
	 * Performs the logging is enabled
	 */
	public function log()
	{
		if ($this->config('log', TRUE) AND is_object(Kohana::$log))
		{
			
                        $remove = array("\n", "\r\n", "\n\r", "\r");
                        $params = str_replace($remove, '', Debug::dump($_REQUEST));                 
                        $moreMessage ='<b>Error en: </b> ' . $_SERVER['HTTP_HOST'] . '<br/><br/>';                   
                        $moreMessage .= '<b>URI:</b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
                        $moreMessage .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
                        $moreMessage .= '<b>HTTP USER AGENT:</b> ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>';
                         if (isset($_SERVER['REMOTE_ADDR'])) {
                            $moreMessage .= '<b>REMOTE_ADDR:</b> ' . $_SERVER['REMOTE_ADDR'] . '<br/>';
                        }
                        if (isset($_SERVER['HTTP_REFERER'])) {
                            $moreMessage .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_REFERER'] . '<br/>';
                        }
                        if (isset($_SERVER['REDIRECT_URL'])) {
                            $moreMessage .= '<b>REDIRECT URL:</b> ' . $_SERVER['REDIRECT_URL'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_INFO'])) {
                            $moreMessage .= '<b>PATH INFO:</b> ' . $_SERVER['PATH_INFO'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_TRANSLATED'])) {
                            $moreMessage .= '<b>PATH TRANSLATED:</b> ' . $_SERVER['PATH_TRANSLATED'] . '<br/>';
                        }
                        if (isset($_SERVER['SCRIPT_FILENAME'])) {
                            $moreMessage .= '<b>SCRIPT FILENAMEL:</b> ' . $_SERVER['SCRIPT_FILENAME'] . '<br/>';
                        }
                        $moreMessage .= '<b>PARAMETROS: </b><br/><pre>' . $params . '</pre><br/><br/>';                  
            
                    $info = sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($this->exception), $this->exception->getCode(), '<div style="width: 100%;word-wrap:break-word;">' . $moreMessage . strip_tags($this->exception->getMessage()) . '</div>', Debug::path($this->exception->getFile()), $this->exception->getLine());
                    
                    Kohana::$log->add(LOG::CRITICAL
                            , ':info'
                            , array(
                        ':info' => $info
                    ));
                    $strace = $info . "\n--\n" . $this->exception->getTraceAsString();
                    Kohana::$log->add(Log::STRACE, $strace);
     
		}
	}

	/**
	 * Sends the email if enabled
	 *
	 * @return  void
	 */
	public function email()
	{
		$content = $this->display;

		$config = $this->config('email', FALSE);
		if (is_string($config))
		{
			$content = $this->render($config);
		}

		$email_available = (class_exists('Email') AND method_exists('Email', 'send'));
		if ( ! $email_available)
		{
			throw new Exception('The email functionality of the Synapse Studios Error module requires the Synapse Studios Email module.');
		}

		ob_start();
		$error = $this;
		include Kohana::find_file('views', 'errors/email');
		$content = ob_get_clean();

		$success = Email::send(
			Kohana::config('error.email.to'),
			Kohana::config('error.email.from'),
			'Error: '.$this->type,
			$content,
			TRUE
		);

		if ( ! $success)
		{
			throw new Kohana_Exception('The error email failed to be sent.');
		}
	}

	/**
	 * Performs the action set in configuration
	 *
	 * @return  boolean
	 */
	public function action()
	{
		$type = '_action_'.$this->config('action.type', NULL);
		$options = $this->config('action.options', array());
		$this->$type($options);
		return TRUE;
	}

	/**
	 * Redirects the user upon error
	 *
	 * @param   array  $options  Options from config
	 * @return  void
	 */
	protected function _action_redirect(array $options = array())
	{
		if ($this->code === 'Parse Error')
		{
			echo '<p><strong>NOTE:</strong> Cannot redirect on a parse error, because it might cause a redirect loop.</p>';
			echo $this->display;
			return;
		}

		$notices_available = (class_exists('Notices') AND method_exists('Notices', 'add'));
		$message = Arr::get($options, 'message', FALSE);
		if ($notices_available AND $message)
		{
			Notices::add('error', $message);
		}

		$url = Arr::get($options, 'url');
		if (strpos($url, '://') === FALSE)
		{
			// Make the URI into a URL
			$url = URL::site($url, TRUE);
		}
		header("Location: $url", TRUE);
		exit;
	}

	/**
	 * Displays the error
	 *
	 * @param   array  $options  Options from config
	 * @return  void
	 */
	protected function _action_display(array $options = array())
	{
		$view = Arr::get($options, 'view', 'errors/_default');                 
		$this->display = $this->render($view);

		echo $this->display;
	}

	/**
	 * Performs a callback on the error before displaying
	 *
	 * @param   array  $options  Options from config
	 * @return  void
	 */
	protected function _action_callback(array $options = array())
	{
		$callback = Arr::get($options, 'callback');
		@list($method,) = Arr::callback($callback);
		if (is_callable($method))
		{
			call_user_func($method, $this);
		}

		echo $this->display;
	}

	/**
	 * CatchAll for actions. Just displays the error.
	 *
	 * @param  string  $method
	 * @param  array   $args
	 */
	public function __call($method, $args)
	{
		echo $this->display;
	}

	/**
	 * This is a demo callback that serves an example for how to use the
	 * callback action type.
	 *
	 * @param  object  $error  The error object
	 */
	public static function demo_callback($error)
	{
		$error->display = '<p>THERE WAS AN ERROR!</p>';
	}

}