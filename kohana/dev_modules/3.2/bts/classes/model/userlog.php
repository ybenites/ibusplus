<?php

defined('SYSPATH') or die('No direct script access.');

class Model_UserLog extends ORM {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_userlog';
    protected $_primary_key = 'idUserLog';
    protected $_belongs_to = array(
        'user' => array('model','User','foreign_key' => 'idUser')  
        );

}