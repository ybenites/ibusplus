<?php
defined('SYSPATH') or die('No direct script access.');
class Model_City extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_city';
    protected $_primary_key = 'idCity';
    protected $_has_many = array(
        'offices' => array(
            'model' => 'Office',
            'foreign_key' => 'idCity',
        ),
    );

}