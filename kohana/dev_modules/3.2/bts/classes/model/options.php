<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Options extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_options';
    protected $_primary_key = 'key';
    static protected $options = array();

    static public function getKey($key) {

        if (count(Model_Options::$options) == 0)
            Model_Options::getKeys();

        if (array_key_exists($key, Model_Options::$options)) {
            $opt = Model_Options::$options[$key];

            switch ($opt['dataType']) {
                case 'bollean':
                    return (boolean) $opt['value'];
                case 'seconds':
                case 'integer':
                    return (int) $opt['value'];
                case 'decimal':
                case 'percentage':
                    return (float) $opt['value'];

                default:
                    return $opt['value'];
            }
        }
    }

    static public function getKeys() {
        $opt = new Model_Options();
        $opt = $opt->find_all();
        $response = array();
        foreach ($opt as $item) {
            $response[$item->key]['value'] = $item->value;
            $response[$item->key]['dataType'] = $item->dataType;
        }
        Model_Options::$options = $response;
        return $response;
    }

}