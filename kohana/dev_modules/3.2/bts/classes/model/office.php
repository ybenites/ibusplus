<?php

class Model_Office extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'bts_office';
    protected $_primary_key ='idOffice';
    protected $_belongs_to = array(
        'city' => array('foreign_key' => 'idCity')
        );
}