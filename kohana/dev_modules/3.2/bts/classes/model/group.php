<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Group extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_group';
    protected $_primary_key = 'idGroup';
    protected $_has_many = array('menus' => array('through' => 'privilege', 'foreign_key' => 'idGroup', 'far_key' => 'idMenu'));

}
