<?php

defined('SYSPATH') or die('No direct script access.');

class Model_TerminalSelection extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_terminalselection';
    protected $_primary_key = 'idTerminalSelection';
    protected $_belongs_to = array(
        'office' => array('model' => 'Office', 'foreign_key' => 'idOffice'),
        'terminal' => array('model' => 'Terminal', 'foreign_key' => 'idTerminal'),
        'user' => array('model' => 'User', 'foreign_key' => 'idUser')
    );

}