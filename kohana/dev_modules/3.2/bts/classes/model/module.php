<?php
defined('SYSPATH') or die('No direct script access.');

class Model_Module extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_table_name = 'bts_module';
    protected $_primary_key = 'idModule';
    protected $_sorting = array('name' => 'asc', 'desc' => 'asc');
    protected $_belongs_to = array('superModule' => array('foreign_key' => 'idModule'));

}