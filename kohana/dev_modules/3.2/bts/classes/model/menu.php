<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Menu extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_menu';
    protected $_primary_key = 'idMenu';
    protected $_sorting = array('name' => 'asc', 'type' => 'desc');
    protected $_belongs_to = array('superMenu' => array('foreign_key' => 'idMenu'));
    protected $_has_many = array(
        'groups' => array('through' => 'privilege', 'foreign_key' => 'idMenu', 'far_key' => 'idGroup'),
        'users' => array('through' => 'customPrivilege', 'foreign_key' => 'idMenu', 'far_key' => 'idUser'),
        'submenus' => array('model' => 'Menu', 'foreign_key' => 'idSuperMenu',)
    );
    
}