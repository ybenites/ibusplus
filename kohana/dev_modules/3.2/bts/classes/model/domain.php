<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Domain extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_domain';
    protected $_primary_key = 'idDomain';

}

?>
