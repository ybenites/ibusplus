<?php

defined('SYSPATH') or die('No direct script access.');

class Model_User extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_user';
    protected $_primary_key = 'idUser';
    protected $_belongs_to = array(
        'group' => array('foreign_key' => 'idGroup'),         
        'office' => array('foreign_key' => 'idOffice')
        );
    protected $_has_one = array(
        'person' => array('foreign_key' => 'idPerson'),
        'userwork' => array('foreign_key' => 'idWorkUser')
    );
    protected $_has_many = array('customPrivileges' => array('through' => 'customprivilege', 'foreign_key' => 'idUser', 'far_key' => 'idMenu'));

}