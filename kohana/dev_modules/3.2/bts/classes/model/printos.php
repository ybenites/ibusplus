<?php

defined('SYSPATH') or die('No direct script access.');

class Model_PrintOS extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_printos';
    protected $_primary_key = 'idPrintOS';

    protected $_has_many = array(
        'terminal' => array(
            'model' => 'Terminal',
            'foreign_key' => 'idPrintOS',
        ),
        'printdriver' => array(
            'model' => 'idPrintDriver',
            'foreign_key' => 'idPrintOS',
        )
    );
}
