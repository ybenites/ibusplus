<?php

defined('SYSPATH') or die('No direct script access.');

class Model_PrintModel extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_printmodel';
    protected $_primary_key = 'idPrintModel';
    protected $_belongs_to = array(
        'printBrand' => array('model' => 'PrintBrand', 'foreign_key' => 'idPrintBrand')
    );
    protected $_has_many = array(
        'printer' => array(
            'model' => 'Printer',
            'foreign_key' => 'idPrintModel'),
    );

}