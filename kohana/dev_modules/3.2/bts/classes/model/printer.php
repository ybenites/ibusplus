<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Printer extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_printer';
    protected $_primary_key = 'idPrinter';
    protected $_belongs_to = array(
        'printModel' => array('model' => 'PrintModel', 'foreign_key' => 'idPrintModel')
    );
    protected $_has_many = array(
        'printDriver' => array(
            'model' => 'PrintDriver',
            'foreign_key' => 'idPrinter',
        ),
        'terminal' => array(
            'model' => 'Terminal',
            'foreign_key' => 'idPrinter',
        )
    );

}