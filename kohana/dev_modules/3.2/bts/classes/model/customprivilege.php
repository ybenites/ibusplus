<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Customprivilege extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_customprivilege';
    protected $_primary_key = 'idCustomPrivilege';
    protected $_belongs_to = array('user' => array(), 'menu' => array());

}

?>