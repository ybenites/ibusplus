<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Privilege
 *
 * @author Emmanuel
 */
class Model_Privilege extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_privilege';
    protected $_primary_key = 'idPrivilege';
    protected $_belongs_to = array('Group' => array(), 'Menu' => array());

}