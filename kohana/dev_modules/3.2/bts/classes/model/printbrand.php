<?php

defined('SYSPATH') or die('No direct script access.');

class Model_PrintBrand extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_printbrand';
    protected $_primary_key = 'idPrintBrand';

    protected $_has_many = array(
        'printmodel' => array(
            'model' => 'PrinModel',
            'foreign_key' => 'idPrintBrand',
        )
    );
}
