<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Terminal extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_terminal';
    protected $_primary_key = 'idTerminal';
    protected $_belongs_to = array(
        'office' => array('model' => 'Office', 'foreign_key' => 'idOffice'),
        'printOS' => array('model' => 'PrintOS', 'foreign_key' => 'idPrintOS'),
        'printer' => array('model' => 'Printer', 'foreign_key' => 'idPrinter')
    );
   protected $_has_many = array(
        'terminalsection' => array(
            'model' => 'TerminalSelection',
            'foreign_key' => 'idTerminal',
        )
     );
   
}