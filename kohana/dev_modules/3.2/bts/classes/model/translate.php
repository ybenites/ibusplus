<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Translate extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_translate';
    protected $_primary_key = 'idTranslate';
 
}