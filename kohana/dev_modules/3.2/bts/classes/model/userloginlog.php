<?php

defined('SYSPATH') or die('No direct script access.');

class Model_UserLoginLog extends ORM {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_userloginlog';
    protected $_primary_key = 'idUserLogin';
    
}