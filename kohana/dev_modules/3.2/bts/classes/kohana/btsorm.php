<?php
defined('SYSPATH') or die('No direct script access.');
class Kohana_Btsorm extends ORM {

    public function create(Validation $o_validation = NULL) {
        $a_columns = $this->_table_columns;

        if (isset($a_columns['userCreate'])) {
            if ($this->userCreate === NULL)
                $this->userCreate = Session::instance('database')->get("USER_ID");
        }
        if (isset($a_columns['creationDate'])) {
            $this->creationDate = date("Y-m-d H:i:s");
        }
        if (isset($a_columns['userLastChange'])) {
            if ($this->userLastChange === NULL)
                $this->userLastChange = Session::instance('database')->get("USER_ID");
        }
        if (isset($a_columns['lastChangeDate'])) {
            $this->lastChangeDate = date("Y-m-d H:i:s");
        }
        return parent::create($o_validation);
    }

    public function update(Validation $o_validation = NULL) {
        $a_columns = $this->_table_columns;
        if (isset($a_columns['userLastChange'])) {
            if ($this->userLastChange === NULL)
                $this->userLastChange = Session::instance('database')->get("USER_ID");
        }
        parent::update($o_validation);
    }

}