<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of install
 *
 * @author Emmanuel
 */
abstract class Kohana_Public_Install extends Controller_Template {

    public $template = 'public/install';
    public $language = 'install_en';
    const CODE_SUCCESS = 441;
    const CODE_ERROR = 443;
    public $json_array_return = array(
        'code' => self::CODE_SUCCESS,
        'msg' => 'OK'
    );

    public function before() {
        Cookie::$salt = 'install_cookie';
        return parent::before();
    }

    public function action_index() {
        if (implode(Kohana::find_file('config', $_SERVER['HTTP_HOST'] . '_db'))) {
            Request::initial()->redirect('public/login');
        }
        $this->template->CODE_SUCCESS = self::CODE_SUCCESS;
        $this->template->CODE_ERROR = self::CODE_ERROR;
        I18n::lang(Cookie::get('lang', NULL));
    }

    public function action_change2Es() {
        $this->auto_render = FALSE;
        $this->changeLanguage("es");
        Cookie::set('lang', 'es-install');
        echo '1';
    }

    public function action_change2En() {
        $this->auto_render = FALSE;
        $this->changeLanguage("en");
        Cookie::set('lang', 'en-install');
        echo '1';
    }

    private function deleteConfigDBFile($serverName) {
        FileHelper::deleteFileOrFolderRecursive(APPPATH . 'config' . DIRECTORY_SEPARATOR . $serverName . '_db' . EXT);
    }

    private function createConfigDBFile($domainName, $siteName, $serverDBName, $serverPort, $dbName, $dbUser, $dbPassword, $skin, $timeZone='America/Lima', $sessionLifeTime=1800, $email="quatrobus@ibusplus.com") {
        //FileHelper::deleteFileOrFolderRecursive(APPPATH . 'config' . DIRECTORY_SEPARATOR . $serverName . '_db' . EXT);
        $this->deleteConfigDBFile($domainName);
        $fp = fopen(APPPATH . 'config' . DIRECTORY_SEPARATOR . $domainName . '_db' . EXT, "c");

        $string = "<?php\n\nclass db_config {\n\n\tpublic static \$db_conection_config = array('$domainName' => array(
            'siteName' => '$siteName',
            'serverDBName' => '$serverDBName',
            'serverPort' => '$serverPort',
            'dbName' => '$dbName',
            'dbUser' => '$dbUser',
            'dbPassword' => '$dbPassword',
            'skin' => '$skin',
            'website' => '$domainName',
            'timeZone' => '$timeZone',
            'defaultIndex' => 'public/login/index',
            'email' => '$email',
            'sessionLifeTime' => $sessionLifeTime));}";

        $write = fputs($fp, $string);
        fclose($fp);
        chmod(APPPATH . 'config' . DIRECTORY_SEPARATOR . $domainName . '_db' . EXT, 0777);
    }

    public function createErrorResponse(&$aResponse, $msg) {
        $aResponse['code'] = self::CODE_ERROR;
        $aResponse['msg'] = $msg;
    }

    public function getPostData(&$domainName, &$siteName, &$skin, &$timeZone, &$sessionLifeTime, &$serverDBName, &$serverPort, &$dbName, &$dbUser, &$dbPassword, &$rootPassword,&$sName) {
        $domainName = $this->request->post('website');
        $siteName = $this->request->post('siteName');
        $skin = $this->request->post('skin');
        $timeZone = $this->request->post('skin');
        $sessionLifeTime = $this->request->post('sessionLifeTime');
        $serverDBName = $this->request->post('serverDBName');
        $serverPort = $this->request->post('serverPort');
        $dbName = $this->request->post('dbName');
        $dbUser = $this->request->post('dbUser');
        $dbPassword = $this->request->post('dbPassword');
        $rootPassword = $this->request->post('rp');
        if ($rootPassword == NULL)
            $rootPassword = '';
        if ($serverPort == NULL)
            $serverPort = '3306';
        $sName = $serverDBName . ':' . $serverPort;
    }

    public function action_installBTS() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $this->getPostData($domainName, $siteName, $skin, $timeZone, $sessionLifeTime, $serverDBName, $serverPort, $dbName, $dbUser, $dbPassword, $rootPassword,$sName);
            if (!($root_db_link = mysql_connect($sName, 'root', $rootPassword))) {
                $this->createErrorResponse($aResponse, __('Error al intentar contectarse como usuario root'));
            } elseif (!mysql_select_db('mysql', $root_db_link)) {
                $this->createErrorResponse($aResponse, __('Error al seleccionar la base de datos mysql para el usuario root'));
            } else {
                if ($this->createDatabase($sName, $dbName, $root_db_link, $aResponse)) {
                    $this->fnResponseFormat($aResponse);
                }
            }
        } catch (Exception $exc) {
            $this->createErrorResponse($aResponse, $exc->getMessage());
            $this->fnResponseFormat($aResponse);
        }
    }

    public function action_installDataBase() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $this->getPostData($domainName, $siteName, $skin, $timeZone, $sessionLifeTime, $serverDBName, $serverPort, $dbName, $dbUser, $dbPassword, $rootPassword,$sName);
            if (!($root_db_link = mysql_connect($sName, 'root', $rootPassword))) {
                $this->createErrorResponse($aResponse, __('Error al intentar contectarse como usuario root'));
            } elseif (!mysql_select_db('mysql', $root_db_link)) {
                $this->createErrorResponse($aResponse, __('Error al seleccionar la base de datos mysql para el usuario root'));
            } else {
                if ($this->createUser($domainName, $dbName, $dbUser, $dbPassword, $root_db_link, $aResponse)) {
                    $this->createConfigDBFile($domainName, $siteName, $serverDBName, $serverPort, $dbName, $dbUser, $dbPassword, $skin);
                    $simple_user_link = ManualConectionUtil::conection();
                    mysql_select_db($dbName, $simple_user_link);
                    $SQL_CREATE_SCRIPT = file_get_contents(APPPATH . 'config' . DIRECTORY_SEPARATOR . 'install' . DIRECTORY_SEPARATOR . 'database_install.sql');
                    $SQL_CREATE_SCRIPT = str_replace('[DATA_BASE_NAME]', $dbName, $SQL_CREATE_SCRIPT);
                    $querys = explode(";", $SQL_CREATE_SCRIPT);
                    $success = TRUE;
                    $query_count = 0;
                    foreach ($querys as $q) {
                        if (!empty($q)) {
                            if (mysql_query($q, $simple_user_link)) {
                                $query_count = $query_count + 1;
                            } else {
                                $success = FALSE;
                                $this->createErrorResponse($aResponse, __('Error al crear las tablas de la base de datos') . ' ' . $query_count . ' ' . $q . ' ' . mysql_error());
                                break;
                            }
                        }
                    }
                    $this->fnResponseFormat($aResponse);
                    mysql_close($simple_user_link);
                }
            }
        } catch (Exception $exc) {
            $this->createErrorResponse($aResponse, $exc->getMessage());
            $this->fnResponseFormat($aResponse);
        }
    }

    public function createDatabase($domainName, $dbName, $root_db_link, &$aResponse) {
        $db_drop = 'DROP DATABASE IF EXISTS ' . $dbName;
        $db_create = 'CREATE DATABASE ' . $dbName;
        if (!mysql_query($db_drop, $root_db_link)) {
            $this->createErrorResponse($aResponse, str_replace('[DATA_BASE]', $dbName, __('Error al eliminar la base de datos [DATA_BASE]')));
            mysql_close($root_db_link);
            $this->deleteConfigDBFile($domainName);
        } elseif (!mysql_query($db_create, $root_db_link)) {
            $this->createErrorResponse($aResponse, str_replace('[DATA_BASE]', $dbName, __('Error al crear la base de datos [DATA_BASE]')));
            mysql_close($root_db_link);
            $this->deleteConfigDBFile($domainName);
        } else {
            return TRUE;
        }
    }

    public function createUser($domainName, $dbName, $dbUser, $dbPassword, $root_db_link, &$aResponse) {
        $db_user_privileges = "GRANT ALL ON " . $dbName . ".* TO '" . $dbUser . "'@'localhost' IDENTIFIED BY '" . $dbPassword . "'";
        if (!mysql_query($db_user_privileges, $root_db_link)) {
            $this->createErrorResponse($aResponse, str_replace('[USER]', $dbUser, __('Error al crear usuario [USER] y privilegios')));
            mysql_close($root_db_link);
            $this->deleteConfigDBFile($domainName);
        } else {
            mysql_close($root_db_link);
            return TRUE;
        }
    }
}