<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Public_Accessdenied extends Kohana_Private_Privatezone {

    
    public function action_noAccess(){
        $access_denied_view = new View('/public/access_denied');
        $this->template->content = $access_denied_view;
    }
}