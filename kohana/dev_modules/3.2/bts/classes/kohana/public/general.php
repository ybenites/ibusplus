<?php
defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Public_General extends Kohana_Private_Privatezone {    
  
    public function action_cleaningCacheBasic()
    {    
        try {
        Cache::instance('apc')->delete_all();
        CacheFile::deleteAllCache();
        apc_clear_cache();
        echo "<h1>All Cache APC, Files Was Deleted</h1>";     
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        die();    
    }
    
    public function action_cleaningCacheAdvanced()
    {         
         try {
        Cache::instance('apc')->delete_all();
        apc_clear_cache();
        CacheFile::deleteAllCache();
        $this->destroySession();
        $this->destroyNativeSession();        
        echo "<h1>All Cache APC, Files, Session Native, Session Database was Deleted</h1>";     
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        die();
    }
    public function action_regenerateLanguages() {
        $translation_file = new LoadLanguage();
        $language_sufix_file_name = 'bts';
        $translation_file->loadData($language_sufix_file_name, 'es', 'es', true);
        $translation_file->loadData($language_sufix_file_name, 'es', 'en', true);
        echo "<h1>All language was Regenerate </h1>";
        die();
    }
    
}