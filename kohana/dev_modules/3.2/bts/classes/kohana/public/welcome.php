<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Public_Welcome extends Kohana_Private_Privatezone {

    public function action_index() {
        $welcome_view_file = DIRECTORY_SEPARATOR.'public' . DIRECTORY_SEPARATOR . db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'] . DIRECTORY_SEPARATOR . 'index';
        $welcome_view = new View($welcome_view_file);
        $welcome_view->skin = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
        $this->template = $welcome_view;
        $this->template->saleWeb = new View('/public/saleWeb');
    }

}