<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Public_Internationalization extends Kohana_Private_Privatezone {

    public function action_change2Es() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        Cookie::set($this->getSystemSkin().'_lang', self::LANG_ES);
        $this->fnResponseFormat($aResponse);
    }

    public function action_change2En() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        Cookie::set($this->getSystemSkin().'_lang', self::LANG_EN);
        $this->fnResponseFormat($aResponse);
    }

}