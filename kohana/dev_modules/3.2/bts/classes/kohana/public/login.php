<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Public_Login extends Kohana_Private_Privatezone {

    public function action_index() {
        $login_view = new View('/public/login');
        $login_view->skin = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];   
        $login_view->website = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['website'];
        $login_view->siteName = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['siteName'];
        $this->template->content = $login_view;
        if ($this->getSessionParameter(self::USER_SESSION_ID) == NULL) {
            $this->destroySession();
        } else {
            Request::initial()->redirect($this->getSessionParameter(self::USER_DEFAULT_HOME));
        }
    }

    public function action_recoveryPass() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $uname = $this->request->post('uname');
            if ($uname == NULL) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __('El nombre de usuario es necesario para recuperar la contraseña');
            } else {
                $result = DB::select(
                                array('bts_user.idUser', 'user_id'), array('bts_user.userName', 'user_name'), array('bts_user.password', 'user_password'), array('bts_person.email', 'person_email'), array('bts_person.fName', 'person_fname'), array('bts_person.lName', 'person_lname'))
                                ->from('bts_user')
                                ->join('bts_person')
                                ->on('bts_user.idUser', '=', 'bts_person.idPerson')
                                ->where('bts_user.userName', 'LIKE', $uname)
                                ->and_where('bts_user.status', '=', self::STATUS_ACTIVE)
                                ->execute()->current();

                if (!empty($result)) {
                    if ($result['person_email'] == NULL) {
                        $aResponse['code'] = self::CODE_ERROR;
                        $aResponse['msg'] = __("Su cuenta de usuario no tiene asociado un correo electrónico por favor contéctese con el administrador del sistema para recuperar su contraseña");
                    } else {
                        $this->fnaddNewModule('swift', 'swift');
                        $new_pass = strtoupper(base_convert((date('s') . $result['user_id'] . date('s') . $result['user_id'] . date('s') . date('His')), 10, 36));
                        $user = new Model_User($result['user_id']);
                        $user->password = md5($new_pass);
                        $user->update();
                        $userLog = new Model_UserLog();
                        $userLog->idUser =$result['user_id'];
                        $userLog->ip = Request::$client_ip;;
                        $userLog->password =$new_pass;
                        $userLog->changeDate = DateHelper::getFechaFormateadaActual(); 
                        $userLog->save();
                        Email::instance(__("Recuperación de Contraseña"))
                                ->to(array($result['person_email'] => $result['person_fname'] . ' ' . $result['person_lname']))
                                ->message(Email::template(View::factory('password/password_recovery'), array(":user_name" => $result['user_name'], ":user_password" => $new_pass)))
                                ->from('no_reply@ibusplus.com')->send(TRUE);
                        $emailSendArray =explode('@',$result['person_email']);
                        $emailReplace = substr_replace($emailSendArray[0],'***', 4);
                        $emailSend=  $emailReplace.'@'.$emailSendArray[1]; 
                        $aResponse['msg'] = __('La contraseña se ha enviado a la dirección de correo electrónico') . " " . $emailSend;
                    }
                } else {
                    $aResponse['code'] = self::CODE_NO_AUTHORIZED;
                    $aResponse['msg'] = __("El nombre de usuario no se encuentra registrado en el sistema o ha sido dado de baja");
                }
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getKeepAlive(){
        $this->auto_render = FALSE;
        echo jQueryHelper::JSON_jQuery_encode($this->json_array_return);
    }
}
