<?php

defined('SYSPATH') or die('No direct script access.');
interface Kohana_Uconstants {
    
    const SO_EMAIL_LOG_SYSTEM = 'log@ibusplus.com';
    const SO_EMAIL_HCASANOVA = 'hugo.casanova@ibusplus.com';
    
    const VAR_JQUERY_VERSION = 'JQUERY_VERSION';
    const VAR_JQUERY_UI_VERSION = 'JQUERY_UI_VERSION';
    
        /*
     * OPTIONS
     */

    const SO_CUSTOMER_FULL_INFO = 'CUSTOMER_FULL_INFO';
    const SO_DATE_FORMAT = 'DATE_FORMAT';
    const SO_PAYMENT_PAYPAL = 'PAYMENT_PAYPAL';
    const SO_COUNTRY_DEFAULT = 'COUNTRY_DEFAULT';

    /*
     * LOGIN EVENTS
     */
    const EVENT_LOG_IN = "LOGIN";
    const EVENT_LOG_OUT = "LOGOUT";

    /*
     * DATE FORMAT ACCESS CONSTANTS
     */
    const DF_PHP_DATE = 'PHP_DATE';
    const DF_SQL_TIME = 'SQL_TIME';
    const DF_SQL_TIME_RAW = 'SQL_TIME_RAW';
    const DF_SQL_TIME_LIMIT = 'SQL_TIME_LIMIT';
    const DF_SQL_DATE = 'SQL_DATE';
    const DF_SQL_DATE_TIME = 'SQL_DATE_TIME';
    const DF_JQUERY_DATE = 'JQUERY_DATE';
    const DF_PHP_TIME_INPUT = 'PHP_TIME_INPUT';
    const DF_SHOW24HOUR = 'JQUERY_TIME_ENTRY_24_HORAS';


    /**
     * MESSAGE OPTIONS AND CONSTANTS
     */
    const MSG_COUNT_DISPLAY_TOTAL = 20;
    const MSG_COUNT_DISPLAY = 5;
    const GLOBAL_ACTION_OK = 'OK';
    const GLOBAL_ACTION_NEW = 'NEW';
    const GLOBAL_ACTION_EDIT = 'EDIT';
    const GLOBAL_ACTION_SAVE = 'SAVE';
    const GLOBAL_ACTION_DELETE = 'DELETE';
    const GLOBAL_ACTION_CANCEL = 'CANCEL';
    const GLOBAL_ACTION_SEARCH = 'SEARCH';
    const GLOBAL_ACTION_SELECT = 'SELECT';
    const GLOBAL_ACTION_CLEAR = 'CLEAR';
    const GLOBAL_ACTION_COMMENT = 'COMMENT';


    /*
     * USER SESSION CONSTANTS
     */
    const USER_SESSION_SECOND_BEFORE_DISCONECT = 20;
    const USER_SESSION_ID = 'USER_SESSION_ID';
    const USER_NAME = 'USER_NAME';    
    const USER_FULL_NAME = 'USER_FULL_NAME';
    const USER_ID = 'USER_ID';
    const USER_GROUP_ID = 'USER_GROUP_ID';
    const USER_CUSTOM_PRIVILEGE = 'USER_CUSTOM_PRIVILEGE';
    const USER_OFFICE_ID = 'USER_OFFICE_ID';
    const USER_OFFICE_NAME = 'USER_OFFICE_NAME';
    const USER_CITY_ID = 'USER_CITY_ID';
    const USER_CITY_NAME = 'USER_CITY_NAME';
    const USER_ROOT_OPTION = 'USER_ROOT_OPTION';
    const USER_TERMINAL_ID = 'USER_TERMINAL_ID';
    const USER_ROOT_OPTION_MIMIC = 'USER_ROOT_OPTION_MIMIC';
    const USER_ROOT_OPTION_MIMIC_REAL_ID = 'USER_ROOT_OPTION_MIMIC_REAL_ID';
    const USER_TYPE = 'USER_TYPE';
    const USER_DEFAULT_HOME = 'USER_DEFAULT_HOME';
    const USER_TYPE_EMPLOYEE = 1;
    const USER_TYPE_CUSTOMERS = 2;



    /**
     *  PUBLIC SESSION NAME 
     */
    const USER_PUBLIC_SESSION_DATA = 'USER_PUBLIC_SESSION_DATA';

    /*
     * GROUP CONSTANTS
     */
    const GROUP_ROOT = 0;
    const GROUP_ADMIN = 1;
    const USER_SYSTEM_ROOT_ID = 0;

    /*
     * SESSION SCREEN MODE CONSTANTS
     */
    const VIEW_OPTION = 'FULL_SCREEN_MODE';
    const VIEW_TRUE_OPTION = 'FULL_TRUE_SCREEN_MODE';
    const FULL_SCREEN_OFF = 0;
    const FULL_SCREEN_ON = 1;
    const FULL_TRUE_SCREEN_OFF = 0;
    const FULL_TRUE_SCREEN_ON = 1;

    /*
     * SECURITY SESSION CONSTANTS
     */
    const SECUTIRY_ACTIONS = 'SECUTIRY_ACTIONS';
    const SYSTEM_OPTIONS = 'SYSTEM_OPTIONS';
    const MENU_OPTIONS = 'MENU_OPTIONS';
    const CODE_SUCCESS = 441;
    const CODE_NO_AUTHORIZED = 442;
    const CODE_ERROR = 443;
    const CRUD_READ = 'READ';
    const CRUD_CREATE = 'CREATE';
    const CRUD_UPDATE = 'UPDATE';
    const CRUD_DELETE = 'DELETE';

    /**
     * WORKMODE
     */
    const USER_WORKMODE_NAME = 'USER_WORKMODE_NAME';
    const USER_WORKMODE_HOURS = 'USER_WORKMODE_HOURS';
    /*
     * UPLOAD FILES CONSTANTS
     */
    const FOLDER_UPLOAD_TMP = "tmp";
    const FOLDER_UPLOAD = "upload";
    const PHOTO_UPLOAD_PROFILE = "profile";
    const PHOTO_UPLOAD_128 = "128";
    const PHOTO_UPLOAD_64 = "64";

    /*
     * FILE TYPE CONSTANTS
     */
    const FILE_TYPE_CSS = 'css';
    const FILE_TYPE_JS = 'js';

    /*
     * REPORT CONSTANTS
     */
    const RQ_LANG = "qlang";
    const RQ_DATE_FORMAT = "qdf";
    const REPORT_RESPONSE_TYPE_JSON_JQGRID = "JSON_JQGRID";
    const REPORT_RESPONSE_TYPE_JSON = "JSON";
    const REPORT_RESPONSE_TYPE_XML = "XML";
    const REPORT_RESPONSE_TYPE_ARRAY = "ARRAY";
    const REPORT_RESPONSE_TYPE_HTML = "HTML";

    /**
     * CONFIG FILE CONSTANTS  
     */
    const REG_SERVER_SITE_NAME = "siteName";
    const REG_SERVER_DIR_DB = "serverDBName";
    const REG_SERVER_PORT = "serverPort";
    const REG_SERVER_DB_NAME = "dbName";
    const REG_SERVER_DB_USER = "dbUser";
    const REG_SERVER_DB_PASSWORD = "dbPassword";
    const REG_SERVER_SKIN = "skin";
    const REG_SERVER_WEB_SITE = "website";
    const REG_SERVER_TIME_ZONE = "timeZone";
    const REG_SERVER_DEFAULT_INDEX = "defaultIndex";
    const REG_SERVER_EMAIL = "email";
    const REG_SERVER_CONTACT_EMAIL = "contactEmail";
    const REG_SERVER_CONTACT_COMPANY = "contactCompany";
    const REG_SERVER_CONTACT_PHONE = "contactPhone";
    const REG_SERVER_CONTACT_ADDRESS = "contactAddress";
    const REG_SERVER_SESION_TIME = "sessionLifeTime";
    const REG_SERVER_SESION_DB_TIME = "sessionDBLifeTime";
    const REG_PAYPAL_USER_NAME = "paypal_username";
    const REG_PAYPAL_PASSW0RD = "paypal_password";
    const REG_PAYPAL_SIGNATURE = "paypal_signature";
    const REG_PAYPAL_MERCHANT_EMAIL = "paypal_merchant_email";
    const REG_ENVIRONMENT = "environment";

    /**
     * HELP DESK CONSTANTS
     */
    const MAIL_NEW = 1;
    const MAIL_DELETE = 0;
    const MAIL_READ = 2;

    /*
     * OTHER CONSTANTS
     */
    const ZERO = 0;
    const MAX_ROW_LIMIT = '18446744073709551615';
    const DEFAULT_HOME_PAGE = '/private/welcome/index';
    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;
    const LANG_ES = 'es-bts';
    const LANG_EN = 'en-bts';
    const MENU_TYPE_MENU = 'M';
    const MENU_TYPE_ACTION = 'A';
    
    
   
}
