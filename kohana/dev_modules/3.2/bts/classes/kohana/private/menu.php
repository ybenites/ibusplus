<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Menu extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('menu', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('menu', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $menu_view = new View('private/menu_jqgrid');
        $menu_view->options = ORM::factory('Options')->order_by('key')->find_all();
        $menu_view->menu_type = self::MENU_TYPE_MENU;
        $this->template->content = $menu_view;
    }

    public function action_createOrUpdateMenu() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $post = Validation::factory($_POST)
                    ->rule('menu_name', 'not_empty')
                    ->rule('menu_url', 'not_empty');
            if (!$post->check()) {
                throw new Exception(__('Empty Data Post'), self::CODE_SUCCESS);
            } else {
                $menu_name = $this->request->post('menu_name');
                $menu_url = $this->request->post('menu_url');
                $super_menu_id = $this->request->post('idSuperMenu');
                $menu_id = $this->request->post('idMenu');
                $menu_icon = $this->request->post('menu_icon');
                $menu_module = $this->request->post('module');
                $menu_isSubMenu = $this->request->post('isSubMenu');
                $this->request->post('idOption') == '' ? $menu_option = NULL : $menu_option = $this->request->post('idOption');

                Database::instance()->begin();
                $menu = new Model_Menu();
                if ($menu_id != 0) {
                    $menu = ORM::factory('Menu', $menu_id);
                }
                $menu->name = trim($menu_name);
                $menu->url = trim($menu_url);
                
                if ($this->request->post('menu_type') == self::MENU_TYPE_MENU AND strcmp(trim($menu_icon), '') == 0) {
                    $menu->icon = 'ico_help.png';
                }
                if ($this->request->post('menu_type') == self::MENU_TYPE_MENU) {
                    $menu->type = self::MENU_TYPE_MENU;
                } else {
                    $menu->type = self::MENU_TYPE_ACTION;
                    $menu_isSubMenu = self::STATUS_DEACTIVE;
                }
                if ($super_menu_id != 0) {
                    $menu->idSuperMenu = $super_menu_id;
                }
                $menu->idModule = $menu_module;
                $menu->optionKey = $menu_option;
                $menu->status = self::STATUS_ACTIVE;
                $menu->isSubMenu = (int) $menu_isSubMenu;
                $menu->save();

                if ($this->request->post('menu_type') == self::MENU_TYPE_MENU AND $menu->idSuperMenu != NULL AND !((bool) $menu->isSubMenu)) {
                    if ($menu_id == 0) {
                        $url = explode('/', $menu->url);
                        if (count($url) >= 2) {
                            foreach ($this->getCRUDOptionsAsArray() as $value) {
                                $oCRUD_MENU_OPTION = new Model_Menu();
                                $oCRUD_MENU_OPTION->name = $value;
                                $oCRUD_MENU_OPTION->url = '/' . $url[1] . '/' . $url[2] . '/' . $value;
                                $oCRUD_MENU_OPTION->type = self::MENU_TYPE_ACTION;
                                $oCRUD_MENU_OPTION->status = self::STATUS_ACTIVE;
                                $oCRUD_MENU_OPTION->idModule = $menu->idModule;
                                $oCRUD_MENU_OPTION->idSuperMenu = $menu->idMenu;
                                $oCRUD_MENU_OPTION->icon = 'ico_shield.png';
                                $oCRUD_MENU_OPTION->save();
                            }
                        } else {
                            Database::instance()->rollback();
                            throw new Exception(__('No es una URL valida'), self::CODE_SUCCESS);
                        }
                    }
                }
                Database::instance()->commit();
            }
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listMenus() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $menu_id = NULL;
            if (isset($_REQUEST['menu_id'])) {
                $menu_id = $_REQUEST['menu_id'];
            }
            $db = DB::select()->from(array('bts_menu', 'menu'))
                            ->and_where('menu.status', '=', self::STATUS_ACTIVE)
                            ->and_where('menu.idSuperMenu', '=', $menu_id)
                            ->order_by('menu.order', 'asc')
                            ->execute()->as_array();

            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    private function searchSubMenu($superMenu_id = NULL) {
        try {
            $subMenus = new Model_Menu();
            $subMenus = $subMenus->and_where('menu.idSuperMenu', '=', $superMenu_id)->find_all();
            $ids_subMenus = "";
            if (count($subMenus) > 0) {
                foreach ($subMenus as $subMenus) {
                    $ids_subMenus .= $subMenus;
                    $ids_subMenus .="-";
                }
                return $ids_subMenus;
            } else {
                return $ids_subMenus;
            }
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    private function removeSubMenu($idSuperMenu = NULL) {
        $ids = $this->searchSubMenu($idSuperMenu);
        if (!(empty($ids))) {
            $subIds = explode("-", $ids);
            foreach ($subIds as $subId) {
                if (!empty($subId)) {
                    $this->removeSubMenu($subId);
                    DB::update('bts_menu')->set(array('status' => self::STATUS_DEACTIVE))->and_where('idMenu', '=', $subId)->execute();
                }
            }
        }
    }

    public function action_removeMenu() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            Database::instance()->begin();
            $menu_id = $this->request->post('idu');
            $menu = ORM::factory('Menu', $menu_id);
            $menu->status = self::STATUS_DEACTIVE;
            $this->removeSubMenu($menu_id);
            $menu->save();
            Database::instance()->commit();
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listSubMenu() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $superMenu_id = $this->request->post('idMenu');
            $menus = DB::select()
                            ->from(array('bts_menu', 'menu'))
                            ->and_where('menu.idSuperMenu', '=', $superMenu_id)
                            ->and_where('menu.status', '=', self::STATUS_ACTIVE)
                            ->order_by('menu.name', 'desc')
                            ->order_by('menu.type', 'desc')
                            ->execute()->as_array();
            $aResponse['data'] = $menus;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_isSuperMenu() {
        $this->auto_render = FALSE;
        $menu_id = $this->request->post('idMenu');
        $childs = ORM::factory('Menu')
                        ->where('idSuperMenu', '=', $menu_id)
                        ->where('status', '=', self::STATUS_ACTIVE)->count_all();
        if ($childs > 0) {
            echo 1;
        }
    }

    public function action_getMenuById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $menu_id = $this->request->post('idMenu');
            $menu = ORM::factory('Menu', $menu_id)->as_array();
            $aResponse['data'] = $menu;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listMenuXML() {
        $this->auto_render = FALSE;
        try {
            $node = (integer) $_REQUEST['nodeid'];
            $n_lvl = (integer) $_REQUEST['n_level'];
            $SQL_LEAF_NODES = 'SELECT l.idSuperMenu FROM bts_menu AS m LEFT JOIN bts_menu AS l ON m.idMenu = l.idSuperMenu WHERE l.idMenu IS NOT NULL AND l.status=' . self::STATUS_ACTIVE . ' ORDER BY l.order';
            $results = DB::query(Database::SELECT, $SQL_LEAF_NODES)->execute()->as_array();
            $leaf_nodes = array();
            foreach ($results as $r) {
                $leaf_nodes[$r['idSuperMenu']] = $r['idSuperMenu'];
            }

            //if (stristr($_SERVER["HTTP_ACCEPT"], "application/xhtml+xml")) {
            //    header("Content-type: application/xhtml+xml;charset=utf-8");
            //} else {
            header("Content-type: text/xml;charset=utf-8");
            //}
            $et = ">";
            echo "<?xml version='1.0' encoding='utf-8'?$et\n";
            echo "<rows>";
            echo "<page>1</page>";
            echo "<total>1</total>";
            echo "<records>1</records>";
            if ($node > 0) {
                $wh = 'menu.idSuperMenu=' . $node . ' AND menu.status=' . self::STATUS_ACTIVE;
                $n_lvl = $n_lvl + 1;
            } else {
                $wh = 'ISNULL(menu.idSuperMenu) AND menu.status=' . self::STATUS_ACTIVE;
            }
            $NEXT_NODE = "SELECT menu.idMenu, menu.name, menu.url, menu.idSuperMenu, menu.icon, menu.type, menu.order FROM bts_menu menu WHERE " . $wh . " ORDER BY menu.order";
            $results = DB::query(Database::SELECT, $NEXT_NODE)->execute()->as_array();
            foreach ($results as $row) {
                #VALIDACIONES
                if (!$row['idSuperMenu'])
                    $valp = 'NULL';
                else
                    $valp = $row['idSuperMenu'];
                if (array_search($row['idMenu'], $leaf_nodes) > 0)
                    $leaf = 'false';
                else
                    $leaf = 'true';
                $ico_cell = htmlspecialchars(HTML::image('media/ico/' . $row['icon'], array('title' => $row['name'], 'alt' => $row['icon'], 'width' => '16px')));

                $edit_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-edit" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __('Editar') . '" ></a>');
                $delete_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-trash" style="cursor: pointer;" href="/private/menu/removeMenu" rel="' . $row['idMenu'] . '" title="' . __("Eliminar") . '" ></a>');
                $submenu_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-new-submenu" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Nuevo Submenu") . '" ></a>');
                $php_action_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-php-action" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Nueva Acción PHP") . '" ></a>');
                $order_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-order" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Ordenar Menú") . '" ></a>');
                $level_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-order-level" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Mover Menú") . '" ></a>');

                switch ($row['type']) {
                    case self::MENU_TYPE_MENU:
                        if ($leaf == 'true')
                            $img = 'media/images/folder_disabled.png';
                        else
                            $img = 'media/images/folder_closed.png';
                        $ta = __('Menú');
                        $options = $edit_option . $delete_option . $submenu_option . $php_action_option . $order_option . $level_option;
                        break;
                    case self::MENU_TYPE_ACTION:
                        $img = 'media/ico/php.png';
                        $ta = __('Acción PHP');
                        $options = $edit_option . $delete_option;
                        break;
                }
                $type_cell = htmlspecialchars(HTML::image($img, array('title' => $ta, 'alt' => $ta)));


                echo "<row>";
                echo "<cell>" . $row['idMenu'] . "</cell>";
                echo "<cell>" . __($row['name']) . "</cell>";
                echo "<cell>" . $row['url'] . "</cell>";
                echo "<cell>" . $ico_cell . "</cell>";
                echo "<cell>" . $row['type'] . "</cell>";
                echo "<cell>" . $type_cell . "</cell>";
                echo "<cell>" . $options . "</cell>";
                echo "<cell>" . $n_lvl . "</cell>";
                echo "<cell><![CDATA[" . $valp . "]]></cell>";
                echo "<cell>" . $leaf . "</cell>";
                echo "<cell>false</cell>";
                echo "</row>";
            } echo "</rows>";
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_saveOrderMenu() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $menu_keys = $_REQUEST['menu'];
            Database::instance()->begin();
            $count = count($menu_keys);
            for ($i = 0; $i < $count; $i++) {
                DB::update('bts_menu')
                        ->set(array('order' => ($i + 1)))
                        ->where('idMenu', '=', $menu_keys[$i])->execute();
            }

            Database::instance()->commit();
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_changeMenuLevel() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idSource = $this->request->post('idsm');
            $idDestination = $this->request->post('iddm');
            $menu = new Model_Menu($idSource);
            $menu->idSuperMenu = $idDestination;
            $menu->update();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}