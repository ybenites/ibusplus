<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_File extends Kohana_Private_Admin {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('file', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('file', self::FILE_TYPE_JS));
        $file_view = new View('private/file');
        $file_view->file_type = $this->getFileTypeAsArray();
        $this->template->content = $file_view;
    }

    public function saveUpdateFile(&$aResponse) {
        $idFile = $this->request->post('fileId');
        $fileType = $this->request->post('cmbFileType');
        $fileName = trim($this->request->post('fileName'));
        $ext = strrchr($fileName, '.');
        if ($ext != "") {
            $ext = substr($ext, 1);
            if (array_search($ext, $this->getFileTypeAsArray()) === $ext) {
                $fileName = (substr($fileName, 0, strpos($fileName, strrchr($fileName, '.'))));
            }
        }
        $tmp = DB::select(db::expr('COUNT(file.idFile) AS c'))->from(array('bts_file','file'))->where('file.fileName', 'LIKE', $fileName)->and_where('file.fileType', 'LIKE', $fileType)->and_where('file.status', '=', self::STATUS_ACTIVE);
        if ($idFile == NULL) {
            $file = new Model_File();
        } else {
            $file = new Model_File($idFile);
            $tmp = $tmp->and_where('file.idFile', "<>", $idFile);
        }
        $tmp = $tmp->as_object()->execute()->current();
        if ($tmp->c > 0) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = __("El archivo ya existe");
        } else {
            $file->fileType = $fileType;
            $file->fileName = $fileName;

            if ($idFile == NULL) {
                $file->save();
            } else {
                $file->update();
            }
        }
    }

    public function action_saveFile() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $this->saveUpdateFile($aResponse);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getFileById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_File($this->request->post('id'));
            $aResponse['data'] = $file->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }
    
    public function action_removeFile() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_File($this->request->post('id'));
            $file->status=self::STATUS_DEACTIVE;
            $file->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listFiles() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "f.idFile",
                'cDate' => "f.`creationDate`",
                'lcDate' => "f.`lastChangeDate`",
                'ft' => "f.fileType",
                'fileName' => "CONCAT(f.fileName,'.',f.fileType)",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(f.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(f.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_file` f ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = f.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = f.`userLastChange` ';

            $where_conditions = " f.`status` = " . self::STATUS_ACTIVE;

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

}
