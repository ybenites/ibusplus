<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Group extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('group', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('group', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $group_view = new View('private/group_jqgrid');
        $group_view->user_rootOption = $this->getSessionParameter(self::USER_ROOT_OPTION);
        $this->template->content = $group_view;
    }

    public function action_createOrUpdateGroup() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {

            $group_name = $this->request->post('group_name');
            $group_id = $this->request->post('group_id');
            $root_option = $this->request->post('rootOption');
            $messages_privileges_write = $this->request->post('messages_privileges_write');
            $messages_privileges_reply = $this->request->post('messages_privileges_reply');

            $tmp = DB::select(db::expr('COUNT(group.idGroup) AS c'))->from(array('bts_group','group'))->where('group.name', 'LIKE', $group_name)->and_where('group.status', '=', self::STATUS_ACTIVE);
            if ($group_id == NULL) {
                $group = new Model_Group();
            } else {
                $group = new Model_Group($group_id);
                $tmp = $tmp->and_where('group.idGroup', "<>", $group_id);
            }
            $tmp = $tmp->as_object()->execute()->current();

            if ($tmp->c > 0) {
                throw new Exception(__("El grupo ya existe"),self::CODE_SUCCESS);
            } else {
                $group->defaultHomePage = self::DEFAULT_HOME_PAGE;
                $group->name = trim($group_name);
                $group->status = self::STATUS_ACTIVE;
                if ($this->getSessionParameter(self::USER_ROOT_OPTION) AND $this->request->post('group_home_page') != NULL) {
                    $group->defaultHomePage = $this->request->post('group_home_page');
                }
                if ($root_option != NULL) {
                    $group->rootOption = $root_option;
                }
                $group->allowWriteMessages = $messages_privileges_write;
                $group->allowReplyMessages = $messages_privileges_reply;
                if ($group_id == NULL) {
                    $group->save();
                } else {
                    $group->update();
                }
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listGroups() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "g.idGroup",
                'cDate' => "g.`creationDate`",
                'lcDate' => "g.`lastChangeDate`",
                'name' => "g.name",
                'rootOption' => "g.rootOption",
                'systemDefault' => "g.systemDefault",
                'allowWriteMessages' => "g.allowWriteMessages",
                'allowReplyMessages' => "g.allowReplyMessages",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(g.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(g.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_group` g ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = g.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = g.`userLastChange` ';

            $where_conditions = " g.`status` = " . self::STATUS_ACTIVE;
            if (!(bool) $this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $where_conditions .=" AND g.rootOption IN (" . $this->getSessionParameter(self::USER_ROOT_OPTION) . ") ";
            }

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_removeGroup() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $group_id = $this->request->post('id');
            $group = ORM::factory('Group', $group_id);
            $group->status = self::STATUS_DEACTIVE;
            $group->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getGroupById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $group_id = $this->request->post('id');
            $group = DB::select('group.idGroup', 'group.name', 'group.defaultHomePage', 'group.systemDefault', 'group.allowWriteMessages', 'group.allowReplyMessages', 'group.rootOption')->from(array('bts_group','group'))->where('group.idGroup', '=', $group_id)->execute()->as_array();
            $aResponse['data'] = $group[0];
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}

?>
