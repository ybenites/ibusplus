<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_User extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('user', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('user', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $user_view = new View('private/user_jqgrid');
        $user_view->user_rootOption = $this->getSessionParameter(self::USER_ROOT_OPTION);
        $this->template->content = $user_view;
    }

    public function action_createOrUpdateUser() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $u_id = $this->request->post('u_id');
            $u_name = $this->request->post('u_name');
            $u_password = $this->request->post('u_password');
            $u_userCode = $this->request->post('u_userCode');
            $u_group = $this->request->post('u_group');
            $u_office = $this->request->post('u_office');
            $root_option = $this->request->post('rootOption');
            $p_fname = $this->request->post('p_fname');
            $p_lname = $this->request->post('p_lname');
            $p_phone1 = $this->request->post('p_phone1');
            $p_phone2 = $this->request->post('p_phone2');
            $p_cellphone = $this->request->post('p_cellphone');
            $p_address1 = $this->request->post('p_address1');
            $p_address2 = $this->request->post('p_address2');
            $p_email = $this->request->post('p_email');
            $u_imgProfile = $this->request->post('u_photo');
            $event = $this->request->post('e');

            $tmp = DB::select(db::expr('COUNT(user.idUser) AS c'))->from(array('bts_user','user'))->where('user.userName', 'LIKE', $u_name)->and_where('user.status', '=', self::STATUS_ACTIVE);
            Database::instance()->begin();
            if ($u_id == NULL) {
                $person = new Model_Person();
                $user = new Model_User();
            } else {
                $person = new Model_Person($u_id);
                $user = new Model_User($u_id);
                $tmp = $tmp->and_where('user.idUser', "<>", $u_id);
            }
            $tmp = $tmp->as_object()->execute()->current();
            if ($tmp->c > 0) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __("Ya existe un usuario con este nombre");
                Database::instance()->rollback();
            } else {

                $person->fName = $p_fname;
                $person->lName = $p_lname;
                $person->fullName = $p_fname . ' ' . $p_lname;
                $person->phone1 = $p_phone1;
                $person->phone2 = $p_phone2;
                $person->type = self::USER_TYPE_EMPLOYEE;
                $person->cellPhone = $p_cellphone;
                $person->address1 = $p_address1;
                $person->address2 = $p_address2;
                $person->email = $p_email;
                if ($u_id == NULL) {
                    $person->save();
                    $user->idUser = $person->idPerson;
                } else {
                    $person->update();
                }
                $user->userName = $u_name;                
                if ((boolean) $event) {
                    $user->password = md5($u_password);
                }
                $user->userCode = $u_userCode;
                $user->idGroup = $u_group;
                $user->idOffice = $u_office;
                $user->status = self::STATUS_ACTIVE;
                if ($u_imgProfile != NULL) {
                    if ($user->imgProfile != NULL) {
                        FileHelper::deleteFileOrFolderRecursive($user->imgProfile);
                    }
                    $source_dir = self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_128;
                    $source_url = realpath(self::FOLDER_UPLOAD_TMP . DIRECTORY_SEPARATOR . $u_imgProfile);
                    $destination_url = realpath(self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_128) . DIRECTORY_SEPARATOR . $u_imgProfile;
                    $destination_url_prev = realpath(self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_64) . DIRECTORY_SEPARATOR . $u_imgProfile;
                    copy($source_url, $destination_url);
                    copy($source_url, $destination_url_prev);
                    $img_prev = Kohana_Image::factory($destination_url_prev);
                    $img_prev->resize(64, 64, Image::HEIGHT);
                    $img_prev->save($destination_url_prev);
                    $user->imgProfile = str_ireplace(DIRECTORY_SEPARATOR, '/', DIRECTORY_SEPARATOR . $source_dir . DIRECTORY_SEPARATOR . $u_imgProfile);
                    FileHelper::deleteFileOrFolderRecursive($source_url);
                } else {
                    if ($user->imgProfile == NULL) {
                        $user->imgProfile = '/upload/profile/128/02.png';
                    }
                }
                if ($root_option != NULL) {
                    $user->rootOption = $root_option;
                }
                if ($u_id == NULL) {
                    $user->save();
                } else {
                    $user->update();
                }

                Database::instance()->commit();
            }
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getUserById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $user_id = $this->request->post('id');
            $person = ORM::factory('Person', $user_id)->as_array();
            $user = ORM::factory('User', $user_id);
            $systemDefault = $user->group->systemDefault;
            $user = $user->as_array();
            $user['systemDefault'] = $systemDefault;
            $full_data_array = array_merge($person, $user);
            $aResponse['data'] = $full_data_array;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listUsers() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "u.idUser",
                'cDate' => "u.`creationDate`",
                'lcDate' => "u.`lastChangeDate`",
                'name' => "u.userName",
                'fName' => "p.fullName",
                'gname' => "g.name",
                'gsDefault' => "g.systemDefault",
                'office' => "o.name",
                'rootOption' => "u.rootOption",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(u.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(u.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_user` u ' .
                    'INNER JOIN bts_office o ON u.`idOffice` = o.`idOffice` ' .
                    'INNER JOIN `bts_group` g ON u.`idGroup` = g.`idGroup` ' .
                    'INNER JOIN bts_person p ON u.`idUser` = p.`idPerson` ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = u.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = u.`userLastChange` ';

            $where_conditions = " u.`status` = " . self::STATUS_ACTIVE;
            if (!(bool) $this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $where_conditions .=" AND u.rootOption IN (" . $this->getSessionParameter(self::USER_ROOT_OPTION) . ") ";
            }

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_removeUser() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $user_id = $this->request->post('id');
            $user = ORM::factory('User', $user_id);
            $user->status = self::STATUS_DEACTIVE;
            $user->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}