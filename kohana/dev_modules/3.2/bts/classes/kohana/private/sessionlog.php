<?php
defined('SYSPATH') or die('No direct script access.');
abstract class Kohana_Private_Sessionlog extends Kohana_Private_Admin {    
    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('sessionlog', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('sessionlog', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $sessionlog_view = new View('private/sessionlog');
        $sessionlog_view->today = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE));
        $sessionlog_view->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $sessionlog_view->sql_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE );
        $this->template->content = $sessionlog_view;
    }
    
    
    public function action_listSessionLog() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "slog.idSessionLog",
                'eventDate' => "slog.`eventDate`",
                'event' => "slog.`event`",
                'user' => "u.fullName",
                'eDate' => "DATE_FORMAT(slog.`eventDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_sessionlog` slog ' .
                    'INNER JOIN bts_person u ON u.`idPerson` = slog.`idUser` ';
            
            $where_conditions = " slog.`sessionId` IS NOT NULL ";
             if (isset($_REQUEST['ed'])) {
                $where_conditions .= " AND DATE_FORMAT(slog.`eventDate`,'$date_format') LIKE '" . $_REQUEST['ed'] . "' ";
            }

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

}

?>
