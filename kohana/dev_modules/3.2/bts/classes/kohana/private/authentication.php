<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Authentication extends Kohana_Private_Privatezone {

    public $controller = 'private/private_zone';

    public function action_login() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $uname = $this->request->post('uname');
        $upass = $this->request->post('upass');
        if ($uname == NULL OR $upass == NULL) {
            $this->action_logout();
        } else {
            try {
                $user = new Model_User();
                $user = $user->where('userName', '=', $uname)
                        ->where('password', '=', md5($upass))
                        ->and_where('status', '=', self::STATUS_ACTIVE)
                        ->find();
                if ($user->loaded()) {
                    Session::instance('database');                    
                    $this->setSessionParameter(self::USER_CUSTOM_PRIVILEGE, $user->userPrivilegeActive);
                    $this->setSessionParameter(self::USER_GROUP_ID, $user->group->idGroup);
                    $permited_actions_array = array();
                    //LOS PRIVILEGIOS DE USUARIO ESTÁN ACTIVADOS
                    if ($this->getSessionParameter(self::USER_CUSTOM_PRIVILEGE) == 1) {
                        $actions = $this->createCustomUserPrivileges();
                        foreach ($actions as $a) {
                            array_push($permited_actions_array, $a['url']);
                        }
                    } else {
                        //LOS PRIVILEGIOS DE USUARIO NO ESTAN ACTIVADOS, SE TOMA LOS PRIVILEGIOS DE GRUPO
                        $actions = $this->createRestrictedMenuArray(NULL, TRUE);
                        foreach ($actions as $a) {
                            array_push($permited_actions_array, $a->url);
                        }
                    }

                    $this->setSessionParameter(self::SECUTIRY_ACTIONS, $permited_actions_array);
                    $actions = $this->getSessionParameter(self::SECUTIRY_ACTIONS);
                    #MUESTRA LAS URL DE LOS PERMISOS DEL USUARIO QUE INICIA SESIÓN
                    /*
                      print_r($actions);
                      die();
                     */
                    if (empty($actions)) {
                        throw new Exception(__("Su cuenta no tiene privilegios en el sistema"), self::CODE_SUCCESS);
                        $this->destroySession();
                    }
                    $rootMimic = 0;
                    $rootRealUserID = $user->idUser;
                    if ((bool) $user->rootOption AND $user->idUserMimic != NULL) {
                        $user_mimic = DB::select(DB::expr('um.idUser as idUser'))
                                        ->from(array('bts_user', 'u'))
                                        ->join(array('bts_user', 'um'))->on('u.idUserMimic', '=', 'um.idUser')
                                        ->where('u.idUser', '=', $user->idUser)
                                        ->and_where('um.status', '=', self::STATUS_ACTIVE)
                                        ->as_object()->execute()->current();
                        if ($user_mimic != NULL) {
                            $rootMimic = $user->rootOption;
                            $user = ORM::factory('User', $user_mimic->idUser);
                        }
                    }
                    $this->setSessionParameter(self::USER_SESSION_ID, md5(Session::instance('database')->id()));
                    $this->setSessionParameter('sessionKeyId', $this->getSystemWebSiteURL());
                    $this->setSessionParameter(self::USER_NAME, $user->userName);
                    $this->setSessionParameter(self::USER_FULL_NAME, $user->person->fullName);
                    $this->setSessionParameter(self::USER_ID, $user->idUser);
                    $this->setSessionParameter(self::USER_OFFICE_ID, $user->office->idOffice);
                    $this->setSessionParameter(self::USER_OFFICE_NAME, $user->office->name);
                    $this->setSessionParameter(self::USER_CITY_ID, $user->office->idCity);
                    $this->setSessionParameter(self::USER_TYPE, $user->person->type);
                    $this->setSessionParameter(self::USER_CITY_NAME, $user->office->city->aliasName);
                    $this->setSessionParameter(self::USER_ROOT_OPTION, $user->rootOption);
                    $this->setSessionParameter(self::USER_ROOT_OPTION_MIMIC, $rootMimic);
                    $this->setSessionParameter(self::USER_ROOT_OPTION_MIMIC_REAL_ID, $rootRealUserID);
                    $this->setSessionParameter(self::VIEW_OPTION, self::FULL_SCREEN_OFF);
                    $this->setSessionParameter(self::VIEW_TRUE_OPTION, self::FULL_TRUE_SCREEN_OFF);
                    $this->setSessionParameter(self::SYSTEM_OPTIONS, Helper::getOptionKeys());
                    $this->setSessionParameter(self::USER_DEFAULT_HOME, $user->group->defaultHomePage);
                    $this->createSessionEvent(self::EVENT_LOG_IN);
                    
                    
                    if(array_key_exists('loadExtraDataSession', $this->imported_functions)){                        
                        $data = $this->loadExtraDataSession();
                        foreach ($data as $key=>$value){
                            $this->setSessionParameter($key, $value);
                        }
                    }
                    $aResponse['data'] = $user->group->defaultHomePage;        
                } else {
                    $ip = Request::$client_ip;
                    $o_userLoginF  = new Model_UserLoginLog();
                    $o_userLoginF->where('ip', '=', $ip)->find();
                    if ($o_userLoginF->loaded()) {
                    $o_userLoginF->count=$o_userLoginF->count+1;
                    $o_userLoginF->update();
                    }
                    
                    $o_userLogin  = new Model_UserLoginLog();
                    $o_userLogin->where('ip', '=', $ip)
                    ->and_where('count', '>', 3)  
                            ->find();

                    if ($o_userLogin->loaded()) {
                    $this->fnaddNewModule('swift', 'swift'); 
                    $system = $_SERVER['HTTP_HOST'];
                    Email::instance('Ingreso no autorizado')
                                ->to(array('hugo.casanova@ibusplus.com' =>'Hugo Casanova M'))
                                ->message(Email::template(View::factory('mail/password_retry'), array(":userName" => $uname, ":password" => $upass, ":ip" => $ip, ":system" => $system)))
                                ->from('no_reply@ibusplus.com')->send(FALSE);
                    throw new Exception(__("Haz superado los numeros de intento de ingreso al sistema, seras un robot?."), self::CODE_SUCCESS);
                    }else{
                    $browser = get_browser(null, true);
                    $string = preg_replace('/[^(\x20-\x7F)]*/','', $browser);
                    $browser = json_encode($string);  
                    $loginLog = new Model_UserLoginLog();
                    $loginLog->username=$uname;
                    $loginLog->password=$upass;
                    $loginLog->ip=$ip;
                    $loginLog->dateAccess=  DateHelper::getFechaFormateadaActual();
                    $loginLog->agent=Request::$user_agent;
                    $loginLog->moreInfo=$browser;
                    //$loginLog->host=$_SERVER['REMOTE_HOST'];
                    $loginLog->count=1;
                    $loginLog->create();
                    throw new Exception(__("Usuario o contraseña no válidos"), self::CODE_SUCCESS);
                    }
                }
            } catch (Exception $exc) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = $this->errorHandling($exc);
            }
        }
        $this->fnResponseFormat($aResponse);
    }

    public function isAccessGranted() {
        $permited_actions = $this->getSessionParameter(self::SECUTIRY_ACTIONS);
        return in_array($this->getRequestURI(), $permited_actions);
    }
    public function redirectAccessDenied() {
        if (Request::initial()->is_ajax()) {
            $aResponse = $this->json_array_return;
            $aResponse['code'] = self::CODE_NO_AUTHORIZED;
            $aResponse['msg'] = __('Usted no tiene acceso a esta opción');
            $this->fnResponseFormat($aResponse);
            die();
        } else {
            Request::initial()->redirect('public/accessdenied/noAccess');
        }
    }

    public function createCustomUserPrivileges() {
        try {
            $result = DB::select()
                    ->from('bts_customprivilege')
                    ->and_where('bts_customprivilege.idUser', '=', $this->getSessionParameter(self::USER_ID))
                    ->join('bts_menu')->on('bts_menu.idMenu', '=', 'bts_customprivilege.idMenu')
                    ->join('bts_user')->on('bts_user.idUser', '=', 'bts_customprivilege.idUser')
                    ->join('bts_options', 'LEFT')->on('options.key', '=', 'bts_menu.optionKey')
                    ->and_where('bts_menu.idSuperMenu', 'IS NOT', NULL)
                    ->and_where_open()
                    ->and_where('bts_options.value', '=', DB::expr('bts_options.menuActiveValue OR bts_options.menuActiveValue IS NULL'))
                    ->and_where_close()
                    ->order_by('bts_menu.name', 'asc');
            if (!$this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $result = $result->where('menu.rootOption', '=', self::STATUS_DEACTIVE);
            }
            $result = $result->execute()->as_array();
            return $result;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     *
     * @param int $idSuperMenu
     * @param boolean $all_type si es TRUE recupera Actions y MenÃºs en un Ã¡rbol jerÃ¡rquico, si es FALSE sÃ³lo MenÃºs
     * @return array de MenuItem para iterar y mostrar el menu del usuario acutal
     */
    public function createRestrictedMenuArray($idSuperMenu = NULL, $all_type = false) {
        try {
            $menuArray = array();
            $menus = DB::select('menu.name'
                            , 'menu.icon'
                            , 'menu.url'
                            , 'menu.idMenu'
                            , 'menu.type'
                            , 'menu.isSubMenu')
                    ->from(array('bts_menu', 'menu'))
                    ->join(array('bts_module', 'module'))->on('menu.idModule', '=', 'module.idModule')
                    ->join(array('bts_privilege', 'privilege'))->on('menu.idMenu', '=', 'privilege.idMenu')
                    ->join(array('bts_options', 'options'), 'LEFT')->on('options.key', '=', 'menu.optionKey')
                    ->where('privilege.idGroup', '=', $this->getSessionParameter(self::USER_GROUP_ID))
                    ->and_where('module.status', '=', self::STATUS_ACTIVE)
                    ->and_where('menu.status', '=', self::STATUS_ACTIVE)
                    ->and_where_open()
                    ->and_where('options.value', '=', db::expr('options.menuActiveValue'))->or_where('options.menuActiveValue', 'IS', NULL)
                    ->and_where_close()
                    ->order_by('menu.order', 'asc');
            if (!$this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $menus = $menus->where('menu.rootOption', '=', self::STATUS_DEACTIVE);
            }
            if ($all_type) {
                $menus = $menus->and_where('menu.idSuperMenu', 'IS NOT', $idSuperMenu);
            } else {
                $menus = $menus->and_where('menu.idSuperMenu', '=', $idSuperMenu)->and_where('menu.type', '=', self::MENU_TYPE_MENU);
            }

            $menus = $menus->execute()->as_array();

            foreach ($menus as $m) {
                array_push($menuArray, new MenuItem($m['name'], $m['icon'], $m['url'], $this->createRestrictedMenuArray($m['idMenu']), $m['idMenu'], $m['type'], NULL, NULL, $m['isSubMenu']));
            }

            return $menuArray;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
