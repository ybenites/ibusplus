<?php

defined('SYSPATH') or die('No direct script access.');
abstract class Kohana_Private_Managerpages extends Kohana_Private_Admin{
    public function action_index(){
        $listLinks=DB::select(
                'idCmscontent',
                'titleTableCms'
                )
                ->from('bts_cmscontent')
                ->where('titleTableCms', 'IS NOT',NULL)
                ->execute()->as_array();       
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('managerPages', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('managerPages', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        
        $managerPages_view = new View('private/managerpages');          
        $managerPages_view->listLinks=$listLinks;
        $this->template->content = $managerPages_view;
    }
    public function action_saveContent(){
        $this->auto_render=false;
        $aResponse = $this->json_array_return;
        try {
            $objFrmData=$this->request->post('frmData');           
            $managerpages=new Model_Managerpages($objFrmData['idCont']);                        
            $managerpages->contentTableCms=$objFrmData['elm1'];
            $managerpages->save();
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $this->fnResponseFormat($aResponse,'json');
    }
    public function action_listContent(){
        $this->auto_render=false;
        $aResponse = $this->json_array_return;
        try {
            $valCont=$this->request->post('valCont');
            $content=new Model_Managerpages($valCont);
            $content=$content->as_array();
            $aResponse['data']=$content;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $this->fnResponseFormat($aResponse,'json');
    }
    public function action_saveNewLink(){
        $this->auto_render=false;
        $aResponse = $this->json_array_return;
        try {
            $valNewLink=$this->request->post('valNewLink');   
            
            $content=new Model_Managerpages();
            $content=$content->where('titleTableCms', '=', trim($valNewLink))->find();
            if(!$content->loaded()){
                $content->titleTableCms=trim($valNewLink);
                $content->save();
                $aResponse['respuesta']="Link Creado";   
            }else{
                $aResponse['respuesta']="Link Ya Existe";                
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $this->fnResponseFormat($aResponse,'json');
    }
    public function action_deleteLink(){
        $this->auto_render=false;
        $aResponse = $this->json_array_return;
        try {
            $idLink=$this->request->post('idLink');             
            if($idLink!=''){
                $content=new Model_Managerpages($idLink);
                $content->delete();
                $aResponse['respuesta']="Link Eliminado";  
            }
            else{
                $aResponse['respuesta']="No Selecciono Link";
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $this->fnResponseFormat($aResponse,'json');
    }
}