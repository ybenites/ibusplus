<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Options extends Kohana_Private_Admin {

    public function action_index() {
      $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('option', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('option', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $option_view = new View('private/options_jqgrid');
        $option_view->user_rootOption = (bool) $this->getSessionParameter(self::USER_ROOT_OPTION);
        $this->template->content = $option_view;
    }

    public function action_updateEnumValuesComponent() {
        $this->alterEnumValues('bts_options', 'component');
    }

    public function action_updateEnumValuesDataType() {
        $this->alterEnumValues('bts_options', 'dataType');
    }

    public function action_listValuesComponent() {
        $this->listEnumValues('bts_options', 'component');
    }

    public function action_listValuesDataType() {
        $this->listEnumValues('bts_options', 'dataType');
    }

    public function listEnumValues($tableName, $columName) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $aResponse['data'] = $this->getEnumValuesFromTable($tableName, $columName);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function alterEnumValues($tableName, $columName) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $values = $this->request->post('values');
            $this->alterEnumValuesForTable($tableName, $columName, $values);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_createOrUpdateOption() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $validate = Validation::factory($this->request->post())
                    ->rule('opt_key', 'not_empty')
                    ->rule('opt_cmp', 'not_empty')
                    ->rule('opt_alias', 'not_empty')
                    ->rule('opt_desc', 'not_empty')
                    ->rule('opt_dt', 'not_empty');
            if (!$validate->check())
                throw new Exception(__("Información Incorrecta"), self::CODE_SUCCESS);
            $key = $this->request->post('opt_key');
            $component = $this->request->post('opt_cmp');
            $dataType = $this->request->post('opt_dt');
            if ($dataType == 'boolean') {
                $value = $this->request->post($dataType . 'Pick');
            } else {
                $value = $this->request->post($dataType);
            }
            $rootOption = $this->request->post('rootOption');
            $arrayOption = $this->request->post('arrayOption');
            $systemDefault = $this->request->post('sdf');
            $suffix = $this->request->post('opt_suffix');
            $description = $this->request->post('opt_desc');
            $descriptionShort = $this->request->post('opt_desc_short');
            $menuActiveValue = $this->request->post('opt_mav');
            $groupName = $this->request->post('opt_gn');
            $groupParent = $this->request->post('opt_sg_val');

            $option = new Model_Options($key);
            $option->component = $component;
            $option->aliasComponent = str_replace('_', ' ', $component);
            $option->dataType = $dataType;
            $option->value = $value;
            $option->hasArrayValues = $arrayOption;
            $option->rootOption = $rootOption;
            $option->systemDefault = $systemDefault;
            $option->suffix = $suffix;
            $option->description = $description;
            $option->descriptionShort = $descriptionShort;
            $option->menuActiveValue = $menuActiveValue;
            $option->groupName = $groupName;
            $option->groupParent = $groupParent;
            if ($option->loaded()) {
                $option->update();
            } else {
                $option->key = $key;
                $option->save();
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listOptions() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "o.key",
                'cDate' => "o.`creationDate`",
                'lcDate' => "o.`lastChangeDate`",
                'component' => "o.component",
                'keyName' => "o.key",
                'dataType' => "o.dataType",
                'val' => "o.value",
                'description' => "o.description",
                'suffix' => "o.suffix",
                'mav' => "o.menuActiveValue",
                'arrayOption' => "o.hasArrayValues",
                'gsDefault' => "o.systemDefault",
                'rootOption' => "o.rootOption",
                'systemDefault' => "o.systemDefault",
                'status' => "o.status",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(o.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(o.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_options` o ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = o.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = o.`userLastChange` ';

            $where_conditions = " o.`key` IS NOT NULL ";
            if (!(bool) $this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $where_conditions .=" AND o.rootOption IN (" . $this->getSessionParameter(self::USER_ROOT_OPTION) . ") ";
            }

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_getOptionById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $option_id = $this->request->post('id');
            $option = ORM::factory('Options', $option_id)->as_array();
            $aResponse['data'] = $option;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_removeOption() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $option_id = $this->request->post('id');
            $option = ORM::factory('Options', $option_id);
            $option->status = self::STATUS_DEACTIVE;
            $option->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_removeUndoOption() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $option_id = $this->request->post('id');
            $option = ORM::factory('Options', $option_id);
            $option->status = self::STATUS_ACTIVE;
            $option->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listOptionsByComponent() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $chars_search = $this->request->post('term');
        $component = $this->request->post('cmp');
        $type = $this->request->post('t');

        $cols = array(
            array('options.key', 'id'),
            array('options.key', 'value')
        );
        if ($type != NULL) {
            if ($type == 'f') {
                $cols = array(
                    array('options.key', 'key'),
                    array('options.dataType', 'dataType'),
                    array('options.value', 'value'),
                    array('options.rootOption', 'rootOption'),
                    array('options.suffix', 'suffix'),
                    array('options.description', 'description'),
                    array('options.descriptionShort', 'descriptionShort'),
                    array('options.hasArrayValues', 'hasArrayValues'),
                );
            }
        }
        try {
            $city = DB::select_array($cols)
                    ->from(array('bts_options', 'options'))
                    ->where('options.status', '=', self::STATUS_ACTIVE)
                    ->order_by('options.order');

            if ($chars_search != NULL) {
                $city->where('options.key', 'LIKE', '%' . $chars_search . '%');
            }

            if ($component != NULL) {
                $city->where('options.component', '=', $component);
            }
            $aResponse['data'] = $city->execute()->as_array();
        } catch (Exception $e) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['data'] = $this->errorHandling($e);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_saveOptionsOrder() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $options = $this->request->post('options');
            Database::instance()->begin();
            $success = FALSE;
            $count = count($options);
            for ($i = 0; $i < $count; $i++) {
                $option_update = DB::update('bts_options')
                                ->set(array('order' => ($i + 1)))
                                ->where('key', '=', $options[$i])->execute();
            }

            Database::instance()->commit();
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getgenerateSalesColor() {
        $this->auto_render = FALSE;
        try {
            $this->action_findgenerateFile($_POST['color_reserved'], $_POST['color_sold'], $_POST['color_canceled'], $_POST['color_expired'], $_POST['color_rexpired'], $_POST['color_trManualTicketSale'], $_POST['color_trManualAgentTicketSale'], $_POST['color_trManualVaucherSale']);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_indexPanel() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('option_panel', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('option_panel', self::FILE_TYPE_JS));
        $option_view = new View('private/options_panel');
        $option_view->user_rootOption = $this->getSessionParameter(self::USER_ROOT_OPTION);
        $this->template->content = $option_view;
    }

    /*
      public function action_getValuesDATE_FORMAT() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;
      $aResponse['data'] = array_keys($this->DATE_FORMAT);
      $this->fnResponseFormat($aResponse);
      } */

    public function action_updateOptionValue() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $key = $this->request->post('key');
            $value = $this->request->post('value');
            $option = new Model_Options($key);
            if ($option->loaded()) {
                $option->value = $value;
                $option->update();
                $aResponse['data'] = array(
                    "key" => $option->key
                    , "value" => $option->value
                    , "suffix" => $option->suffix
                    , "dataType" => $option->dataType
                );
            } else {
                throw new Exception(__("La opción que desea cambiar no existe"), self::CODE_SUCCESS);
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getArrayData() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $validate = Validation::factory($this->request->post())
                    ->rule('key', 'not_empty');
            if (!$validate->check())
                throw new Exception(__("Información Incorrecta"), self::CODE_SUCCESS);
            $key = $this->request->post('key');
            switch ($key) {
                case self::SO_DATE_FORMAT:
                    $aResponse['data'] = array(
                        array("id" => "MX", "value" => "MX")
                        , array("id" => "PE", "value" => "PE")
                        , array("id" => "US", "value" => "US")
                    );
                    break;
            }
        } catch (Exception $exc) {
            $aResponse['code'] = $exc->getCode();
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getAutoCompleteOptions() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $validate = Validation::factory($this->request->post())
                    ->rule('term', 'not_empty');
            if (!$validate->check())
                throw new Exception(__("Información Incorrecta"), self::CODE_SUCCESS);
            $term = $this->request->post('term');
            $key = $this->request->post('key');
            $aResponse['data'] = DB::select(
                                    array('options.key', 'value')
                                    , array('options.key', 'id'))
                            ->from(array('bts_options', 'options'))
                            ->where('key', 'LIKE', DB::expr("UPPER(:TERM)"))
                            ->param(':TERM', '%' . $term . '%')->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listComponents() {
        $aResponse = $this->json_array_return;
        $this->auto_render = FALSE;
        try {
            $op = DB::select('component', array('aliasComponent', 'aliasComponent'))
                            ->from(array('bts_options', 'options'))
                            //->and_where('rootOption', '=', 0)
                            ->group_by('component')
                            ->execute()->as_array();


            foreach ($op as $key => $value) {
                $op[$key]['aliasComponent'] = __($value['aliasComponent']);
            }
            $aResponse['data'] = $op;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getOptions() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $component = $this->request->post('cmp');
            $key = $this->request->post('key');
            $subOptionsCount =
                    DB::select('options.key'
                            , DB::expr("SUM(IF(`options`.`key`=`opt`.`groupParent`,1,0)) AS subOptions"))
                    ->from(array('bts_options', 'options'), (array('bts_options', 'opt')))
                    ->order_by('options.order')
                    ->group_by('options.key');
            if ($key != NULL)
                $subOptionsCount->and_where('options.groupParent', '=', $key);
            else
                $subOptionsCount->and_where('options.groupParent', 'IS', NULL);
            $options = DB::select(
                            //'options.currencyDependence'
                            'options.dataType'
                            , array('options.description', 'longDscription')
                            , 'options.groupParent'
                            //, 'options.idCurrency'
                            , 'options.key'
                            , 'options.menuActiveValue'
                            //, 'options.order'
                            //,'options.root'
                            , 'options.suffix'
                            , array('options.descriptionShort', 'shortDescription')
                            , 'options.value'
                            , 'options.hasArrayValues'
                            , 'options.groupName'
                            , 'subOpt.subOptions'
                    )
                    ->from(array('bts_options', 'options'))
                    ->join(array($subOptionsCount, 'subOpt'))->on('subOpt.key', '=', 'options.key')
                    //->where('options.component', '=', $component)
                    ->order_by('options.order');
            if ($component != NULL)
                $options->and_where('options.component', '=', $component);
            if ($key != NULL)
                $options->and_where('options.groupParent', '=', $key);
            else
                $options->and_where('options.groupParent', 'IS', NULL);
            $aResponse['data'] = $options->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}