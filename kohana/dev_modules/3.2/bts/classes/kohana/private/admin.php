<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Admin extends Kohana_Private_Authentication {

    public $template = 'private/admin';
    public $view_user_info_extra = 'private/user_info_extra';
    public $view_user_info_extra_params = array();
    public $auto_render = TRUE;

    function __construct(Request $request, Response $response) {        
        parent::__construct($request, $response);
        if (!$this->request->is_ajax()) {
        $this->cacheSessionDynamic = Session::instance('database')->get('sessionKeyId') . Request::initial()->controller() . Session::instance('database')->get('user_name') . Request::initial()->action();
        $this->cacheSessionDynamicIndex = Session::instance('database')->get('sessionKeyId') . Request::initial()->controller() . Session::instance('database')->get('user_name') . 'index';
        $actions = $this->getSessionParameter(self::SECUTIRY_ACTIONS);
        if ($this->isReportDataResponse($request->action())) {            
        } else {
            if ($this->getSessionParameter(self::USER_SESSION_ID) != $this->getSessionId()) {
                $this->action_logout();
            } elseif (empty($actions)) {
                
            } elseif (strcmp($this->isAccessGranted($this->getRequestURI()), "") == 0) {
                $this->redirectAccessDenied();
            }
        }
        }else{
            $this->cacheSessionDynamic = Session::instance('database')->get('sessionKeyId') . Request::initial()->controller() . Session::instance('database')->get('user_name') . Request::initial()->action();
            $this->cacheSessionDynamicIndex = Session::instance('database')->get('sessionKeyId') . Request::initial()->controller() . Session::instance('database')->get('user_name') . 'index';
             if ($this->getSessionParameter('sessionKeyId') == NULL) {
                $aResponse = $this->json_array_return;
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __("Vuelva a Iniciar Sesion en el Sistema, presione F5");
                $this->fnResponseFormat($aResponse);
                die();
            }
             if (strcmp($this->isAccessGranted($this->getRequestURI()), "") == 0) {
                $aResponse = $this->json_array_return;
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __("Ud. No tiene privilegios para ejecutar esta acción");
                 $this->fnResponseFormat($aResponse);
                die();
            }
        }
    }

    public function isReportDataResponse($action) {
        $success = TRUE;
        $session_id = $this->request->query('sid');
        if (!StringHelper::stringStartsWith($action, 'report')) {
            $success = FALSE;
        } elseif ($session_id == NULL) {
            $success = FALSE;
        } else {
            $response = DB::select(DB::expr('IF( FROM_UNIXTIME(last_active) <= DATE_SUB(NOW(), INTERVAL 10 MINUTE) , 0, 1) AS IS_ACTIVE'))
                            ->from('bts_sessions')
                            ->where(DB::expr('MD5(bts_sessions.session_id)'), 'LIKE', $session_id)->as_object()->execute()->current();
            if ($response) {
                if (!$response->IS_ACTIVE)
                    $success = FALSE;
            } else {
                $success = FALSE;
            }
        }
        return $success;
    }
    
    public function before() {
        parent::before();
        if (!$this->request->is_ajax()) {
        $this->sesion_options = $this->getSessionParameter(self::SYSTEM_OPTIONS);
        $this->template->full_screen_mode = $this->getSessionParameter(self::VIEW_OPTION);
        $this->template->full_true_screen_mode = $this->getSessionParameter(self::VIEW_TRUE_OPTION);
        $this->template->user_fullName = $this->getSessionParameter(self::USER_FULL_NAME);
        $this->template->user_officeName = $this->getSessionParameter(self::USER_OFFICE_NAME);
        $this->template->user_cityName = $this->getSessionParameter(self::USER_CITY_NAME);
        $this->template->user_idCity = $this->getSessionParameter(self::USER_CITY_ID);
        $this->template->SESSION_ID = $this->getSessionParameter(self::USER_SESSION_ID);
        $this->template->username = $this->getSessionParameter(self::USER_NAME);
        $this->template->user_rootOption = $this->getSessionParameter(self::USER_ROOT_OPTION);
        if (!($this->isReportDataResponse($this->request->action()))) {
            $this->template->DATE_FORMAT = $this->getOptionValue(self::SO_DATE_FORMAT);
        }
        $this->template->ROOT_MIMIC_OPTION = ($this->getSessionParameter(self::USER_ROOT_OPTION) OR $this->getSessionParameter(self::USER_ROOT_OPTION_MIMIC));
        
        try{
            $this->view_user_info_extra_params = $this->loadUserInfoExtraParameters($this->view_user_info_extra_params);
        }catch(Exception $e){         
           
        }
        $view_user_info_extra = new View($this->view_user_info_extra, $this->view_user_info_extra_params);        
        $this->template->view_user_info_extra = $view_user_info_extra;
        $this->template->QLANG = self::RQ_LANG . "=";
        $this->template->LANG = __("es");
        $this->template->QDATE_FORMAT = self::RQ_DATE_FORMAT . "=";
        
         $cacheInstance = Cache::instance('apc');
            $cacheKey = $this->getSessionParameter('sessionKeyId') . 'menus' . $this->getSessionParameter(self::USER_ID);
            $incacheObject = $cacheInstance->get($cacheKey);
            if ($incacheObject === null) {
                if ((bool) $this->getSessionParameter(self::USER_CUSTOM_PRIVILEGE)) {
                    $actions = $this->createCustomRestrictedMenuArray();
                } else {
             $actions = $this->createRestrictedMenuArray();
                }
                $cacheInstance->set($cacheKey, $actions);
                $incacheObject = $cacheInstance->get($cacheKey);
            }
        
        $this->template->menu = $incacheObject;
    }
    }

    /**
     *
     * @param int $idSuperMenu cÃ³digo del menu padre
     * @param boolean $only_menu true si sÃ³lo se requiere items de tipo M o false si se requiere ademÃ¡s los php actions
     * @param array<GroupPrivilege> $groups
     * @return array<MenuItem> de menÃº y/o php actions en Ã¡rbol jerÃ¡rquico
     */
    public function createMenuArray($idSuperMenu = NULL, $only_menu = true, $groups = NULL, $user = NULL) {
        try {

            $menuArray = array();
            $menus = DB::select()->from('bts_menu')
                    ->and_where('bts_menu.status', '=', self::STATUS_ACTIVE)
                    ->and_where('bts_menu.idSuperMenu', '=', $idSuperMenu)
                    ->order_by('bts_menu.order', 'asc');

            if (!$this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $menus = $menus->where('bts_menu.rootOption', '=', self::STATUS_DEACTIVE);
            }
            if ($only_menu) {
                $menus = $menus->and_where('bts_menu.type', '=', self::MENU_TYPE_MENU);
            } else {
                $menus = $menus->order_by('bts_menu.type', 'desc');
            }
            $menus = $menus->execute()->as_array();
            foreach ($menus as $m) {
                $privilege_array_group = array();
                $privilege_array_user = array();
                if ($groups != NULL) {
                    foreach ($groups as $gp) {
                        $privilege = DB::select()->from('bts_privilege')
                                        ->and_where('bts_privilege.idGroup', '=', $gp->idGroup)
                                        ->and_where('bts_privilege.idMenu', '=', $m['idMenu'])->execute()->as_array();
                        array_push($privilege_array_group, new GroupPrivilege($gp, $privilege));
                    }
                }
                if ($user != NULL) {
                    $user_privilege = DB::select()->from('bts_customprivilege')
                                    ->and_where('bts_customprivilege.idUser', '=', $user->idUser)
                                    ->and_where('bts_customprivilege.idMenu', '=', $m['idMenu'])->execute()->as_array();
                    array_push($privilege_array_user, new UserPrivilege($user, $user_privilege));
                }
                array_push($menuArray, new MenuItem($m['name'], $m['icon'], $m['url'], $this->createMenuArray($m['idMenu'], $only_menu, $groups, $user), $m['idMenu'], $m['type'], $privilege_array_group, $privilege_array_user));
            }
            return $menuArray;
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_listCountries() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $db = DB::select()->from('bts_country')->and_where('idSuperCountry', '=', NULL)->execute()->as_array();
            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getFullScreenMode() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            if ($this->getSessionParameter(self::VIEW_OPTION) == self::FULL_SCREEN_ON)
                $this->setSessionParameter(self::VIEW_OPTION, self::FULL_SCREEN_OFF);
            else
                $this->setSessionParameter(self::VIEW_OPTION, self::FULL_SCREEN_ON);
            $aResponse['data'] = array('value' => $this->getSessionParameter(self::VIEW_OPTION));
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getTrueFullScreenMode() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            if ($this->getSessionParameter(self::VIEW_TRUE_OPTION) == self::FULL_TRUE_SCREEN_ON)
                $this->setSessionParameter(self::VIEW_TRUE_OPTION, self::FULL_TRUE_SCREEN_OFF);
            else
                $this->setSessionParameter(self::VIEW_TRUE_OPTION, self::FULL_TRUE_SCREEN_ON);
            $aResponse['data'] = array('value' => $this->getSessionParameter(self::VIEW_TRUE_OPTION));
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_changeOffice() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $id = $this->getSessionParameter(self::USER_ID);
            $user = ORM::factory('User', $id);
            $user->idOffice = $this->request->post('office_id');
            $user->update();
            $this->setSessionParameter(self::USER_OFFICE_ID, $user->idOffice);
            $this->cleaningCache();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }

        $this->fnResponseFormat($aResponse);
    }

    /**
     * pago via Paypal
     * @param string gateway
     */
    public function action_requester_instant_pay($cardNumber, $cardExpYear, $cardExpMonth, $amount, $cardCvv, $userId, $itemId, $billingFname, $billingLname, $billingState, $billingCity, $billingAddress, $billingCountry, $billingZip, $billingCompany, $billingEmail) {
        $this->auto_render = FALSE;

        $gateway = 'paypal';
        try {
            $payment = Payment_Instant::factory();
            $payment->card_number = $cardNumber;
            $payment->card_exp_year = $cardExpYear;
            $payment->card_exp_month = $cardExpMonth;
            $payment->amount = $amount;
            $payment->card_cvv = $cardCvv;
            $payment->item_id = 'item_id';
            $payment->billing_fname = $billingFname;
            $payment->billing_lname = $billingLname;
            $payment->billing_state = $billingState;
            $payment->billing_city = $billingCity;
            $payment->billing_address = $billingAddress;
            $payment->billing_country = $billingCountry;
            $payment->billing_zip = $billingZip;
            $payment->billing_company = $billingCompany;
            $payment->billing_email = $billingEmail;
            $payment->custom = 'Transaction for:' . ' ' . db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['siteName'];
            Payment_Instance::Requester($gateway, 'instant')->pay($payment);
        } catch (Payment_Exception $payment_exception) {
            //gateway did not apply operation
            Kohana::$log->add(Kohana_Log::ERROR, 'Requester instant pay operation failed: ":message" gateway: :gateway :payment', array(':payment' => $payment, ':gateway' => $gateway, ':message' => $payment_exception->getMessage()));
            //here you can apply failed operation business logic
        }
        return $payment->as_array();
    }

    public function action_getMimicUser() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            if (($this->getSessionParameter(self::USER_ROOT_OPTION) OR $this->getSessionParameter(self::USER_ROOT_OPTION_MIMIC))) {
                if ($this->request->post('reset') != NULL) {
                    $realUserId = $this->getSessionParameter(self::USER_ROOT_OPTION_MIMIC_REAL_ID);
                    if ($realUserId != NULL) {
                        $realUser = new Model_User($realUserId);
                        $realUser->idUserMimic = NULL;
                        $realUser->update();
//                        $this->cleaningCache();
                        $aResponse['msg'] = __("Ahora puede iniciar sesión con su propio usuario");
                    }
                } else {
                    $targetUserId = $this->request->post('uid');
                    $targetUserName = $this->request->post('ufname');
                    $realUserId = $this->getSessionParameter(self::USER_ROOT_OPTION_MIMIC_REAL_ID);
                    $realUser = new Model_User($realUserId);
                    if ($realUserId == $targetUserId) {
                        $realUser->idUserMimic = NULL;
                        $aResponse['msg'] = __("Ahora puede iniciar sesión con su propio usuario");
                    } else {
                        $realUser->idUserMimic = $targetUserId;
                        $aResponse['msg'] = __("Debe cerrar e iniciar sesión nuevamente para iniciar sesión como el usuario <b>" . $targetUserName . "</b>");
                    }
                    $realUser->update();
//                    $this->cleaningCache();
                }
            } else {
                throw new Exception(__("No tiene los privilegios necesarios para realizar esta acción"), self::CODE_SUCCESS);
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listCities() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $chars_search = $this->request->post('term');
        $country_id = $this->request->post('idCoutnry');
        $type = $this->request->post('t');

        $cols = array(
            array('bts_city.idCity', 'id'),
            DB::expr('IF(bts_city.aliasName IS NULL,bts_city.name,bts_city.aliasName) AS value')
        );
        if ($type != NULL) {
            if ($type == 'f') {
                $cols = array('city.*');
            }
        }
        try {
            $city = DB::select_array($cols)
                    ->from('bts_city')
                    ->where('bts_city.status', '=', self::STATUS_ACTIVE)
                    ->order_by('bts_city.name');

            if ($chars_search != NULL) {
                $city->where('bts_city.name', 'LIKE', '%' . $chars_search . '%');
            }

            if ($country_id != NULL) {
                $city->where('bts_city.idCountry', '=', $country_id);
            }
            $aResponse['data'] = $city->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listGroups() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $chars_search = $this->request->post('term');
        $type = $this->request->post('t');

        $cols = array(
            array('bts_group.idGroup', 'id'),
            DB::expr('bts_group.name AS value')
        );
        if ($type != NULL) {
            if ($type == 'f') {
                $cols = array('group.*');
            }
        }
        try {
            $city = DB::select_array($cols)
                    ->from('bts_group')
                    ->where('bts_group.status', '=', self::STATUS_ACTIVE)
                    ->order_by('bts_group.name');

            if ($chars_search != NULL) {
                $city->where('bts_group.name', 'LIKE', '%' . $chars_search . '%');
            }
            if (!(bool) $this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $city->and_where('bts_group.rootOption', '=', $this->getSessionParameter(self::USER_ROOT_OPTION));
            }
            $aResponse['data'] = $city->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getCityById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $city_id = $_POST['city_id'];
            $city = DB::select(array('bts_country.name', 'countryName'), array('bts_country.code', 'countryCode'), 'bts_city.*')
                            ->from('bts_country')
                            ->join('bts_city')->on('bts_country.idCountry', '=', 'bts_city.idCountry')
                            ->where('bts_city.idCity', '=', $city_id)
                            ->order_by('bts_city.name', 'ASC')
                            ->execute()->as_array();
            $aResponse['data'] = $city;
        } catch (Database_Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listOffices() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $chars_search = $this->request->post('term');
        $city_id = $this->request->post('idCity');
        $type = $this->request->post('t');

        $cols = array(
            array('bts_office.idOffice', 'id'),
            array('bts_office.name', 'value')
        );
        if ($type != NULL) {
            if ($type == 'f') {
                $cols = array('bts_office.*');
            }
        }
        try {
            $city = DB::select_array($cols)
                    ->from('bts_office')
                    ->where('bts_office.status', '=', self::STATUS_ACTIVE)
                    ->order_by('bts_office.name');

            if ($chars_search != NULL) {
                $city->where('bts_office.name', 'LIKE', '%' . $chars_search . '%');
            }

            if ($city_id != NULL) {
                $city->where('bts_office.idCity', '=', $city_id);
            }
            $aResponse['data'] = $city->execute()->as_array();
        } catch (Exception $e) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($e);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function getCurrentUniqueSequence($type) {

        return $this->getNextUniqueSequence($type, FALSE);
    }

    public function getNextUniqueSequence($type, $next = TRUE) {
        try {

            if ($next) {

                $db = DB::query(Database::SELECT, "SELECT NEXT_UNIQUE_SEQUENCE_NUMBER('" . $type . "') AS next_sequence")->execute()->as_array();
                $value = (integer) $db[0]['next_sequence'];
            } else {

                $db = DB::select('rms_uniquesequence.sequence_cur_value')->from('rms_uniquesequence')->where('rms_uniquesequence.sequence_name', '=', $type)->execute()->as_array();
                $value = (integer) $db[0]['sequence_cur_value'];
            }
            return $value;
        } catch (Exception $exc) {
            return NULL;
        }
    }

    public function printDocument($idDocument, $documentType) {
        $this->auto_render = TRUE;
        $print_error_view = new View('/printer/printError');
        $print_error_view->skin = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
        $this->template = new View('/printer/emptyTemplate');
        if ($this->getSessionParameter(self::USER_TERMINAL_ID) == NULL) {
            $print_error_view->title_dialog = __('Seleccione Impresora');
            $print_error_view->content_dialog = __('No ha seleccionado la configuración adecuada para imprimir sus documentos. Por favor selecciónela.');
            $this->template->printable_document = $print_error_view;
        } else {
            try {

                $terminal = new Model_Terminal($this->getSessionParameter(self::USER_TERMINAL_ID));
                $BROWSER_FOLDER = $terminal->browser;
                switch ($terminal->browser) {
                    case 'Mozilla Firefox':$BROWSER_FOLDER = 'FF';
                        break;
                    case 'Chrome':$BROWSER_FOLDER = 'CHR';
                        break;
                    case 'Opera':$BROWSER_FOLDER = 'OPE';
                        break;
                    case 'Safari':$BROWSER_FOLDER = 'SAF';
                        break;
                    case 'Internet Explorer':$BROWSER_FOLDER = 'IE';
                        break;
                }
                $OS_FOLDER = $terminal->printOS->code;
                $PRINTER_FOLDER = $terminal->printer->code;
                $FOLDER_TEMPLATE = 'printer' . DIRECTORY_SEPARATOR . $BROWSER_FOLDER . DIRECTORY_SEPARATOR . $OS_FOLDER . DIRECTORY_SEPARATOR . $PRINTER_FOLDER . DIRECTORY_SEPARATOR . strtoupper(db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin']);
                $HTML_TEMPLATE = $FOLDER_TEMPLATE . DIRECTORY_SEPARATOR . $documentType;
                if (implode(Kohana::find_file('views', $HTML_TEMPLATE, NULL, TRUE)) != '') {
                    $documentPrint_view = new View($HTML_TEMPLATE);
                    switch ($documentType) {
                        case self::DT_BOLETO :
                            $ticket = new Model_Ticket($idDocument);
                            $documentPrint_view->PASSENGER = $ticket->passenger->completeName;
                            $documentPrint_view->DEPARTURE = $ticket->tempBusCel->cityDeparture->aliasName;
                            $documentPrint_view->ARRIVAL = $ticket->tempBusCel->cityArrival->aliasName;
                            $documentPrint_view->DEPARTURE_T = $ticket->cityDepartureT->aliasName;
                            $documentPrint_view->ARRIVAL_T = $ticket->cityArrivalT->aliasName;
                            $documentPrint_view->PURCHASE_DATE = DateHelper::fnFormatTimeStamp($ticket->listingDate, 'd/m/Y');
                            $documentPrint_view->SELLER = $ticket->seller->person->fullName;
                            $documentPrint_view->TICKET_TYPE = $ticket->ticketType->name;
                            $documentPrint_view->DEPARURE_TIME = DateHelper::fnFormatTimeStamp($ticket->listingDate, 'h:i A');
                            $documentPrint_view->SEAT_NUMBER = $ticket->tempBusCel->busCel->numberSeat;
                            $documentPrint_view->TICKET_PRICE = $ticket->amount;
                            $documentPrint_view->TICKET_ID = $ticket->idTicket;
                            $documentPrint_view->OFFICE_INFO = $ticket->seller->office->name;
                            $this->template->printable_document = $documentPrint_view;
                            break;
                        case self::DP_PAQUETE :
                            $package = new Model_Package($idDocument);
                            $documentPrint_view->SITE_NAME = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['website'];
                            $documentPrint_view->SKIN = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
                            $documentPrint_view->SENDER_NAME = $package->passangerSender->person->fullName;
                            $documentPrint_view->SENDER_ADDRESS = $package->passangerSender->person->address1;
                            $documentPrint_view->SENDER_CITY = $package->passangerSender->person->city;
                            $documentPrint_view->SENDER_ZIPCODE = $package->passangerSender->person->zipcode;
                            $documentPrint_view->SENDER_STATE = '';
                            $documentPrint_view->SENDER_COUNTRY = $package->passangerSender->person->country->name;
                            if ($package->haveSerie) {
                                $documentPrint_view->SERIE_CORRELATIVE = ($package->serie == null ? '' : $package->serie . '-') . $package->correlative;
                            } else {
                                $documentPrint_view->SERIE_CORRELATIVE = $package->idPackageAlias;
                            }
                            $documentPrint_view->AGENCY_SOURCE = $package->officeSource->name;
                            $documentPrint_view->AGENCY_SOURCE_DETAILS = $package->officeSource->address . ' - ' . $package->officeSource->phone;

                            $documentPrint_view->RECIPIENT_NAME = $package->passangerRecipient->person->fullName;
                            $documentPrint_view->RECIPIENT_ADDRESS = $package->passangerRecipient->person->address1;
                            $documentPrint_view->RECIPIENT_CITY = $package->passangerRecipient->person->city;
                            $documentPrint_view->RECIPIENT_ZIPCODE = $package->passangerRecipient->person->zipcode;
                            $documentPrint_view->RECIPIENT_STATE = '';
                            $documentPrint_view->RECIPIENT_COUNTRY = $package->passangerRecipient->person->country->name;
                            $details = $package->packagedetail->find_all();
                            $documentPrint_view->ITEMS = count($details);
                            $documentPrint_view->TOTAL_VALUE = $package->totalAmountWithTax;
                            $documentPrint_view->TOTAL_DISCOUNT = $package->totalDiscount;
                            $documentPrint_view->WEIGHT_VALUE = 0;
                            $documentPrint_view->TOTAL_PAY = $package->totalAmount;
                            $documentPrint_view->DETAIL = $details;
                            $this->template->printable_document = $documentPrint_view;
                            break;
                        default:
                            break;
                    }
                } else {

                    $print_template_no_exist_view = new View('printer/printTemplateNoExist');
                    $print_template_no_exist_view->ERROR_MESSAGE = __("No existe un formato para la configuración de su terminal y el documento que desea imprimir. Su Configuración es la siguiente:");
                    $terminal = new Model_Terminal($this->getSessionParameter(self::USER_TERMINAL_ID));
                    $print_template_no_exist_view->office = $terminal->office->name;
                    $print_template_no_exist_view->os = $terminal->printOS->name;
                    $print_template_no_exist_view->printer = $terminal->printer->printModel->name;
                    $print_template_no_exist_view->browser = $terminal->browser;
                    $print_error_view->title_dialog = __('No Existe Formato');
                    $print_error_view->content_dialog = $print_template_no_exist_view;
                    $this->template->printable_document = $print_error_view;
                }
            } catch (Exception $exc) {
                $this->template->printable_document = $exc->getTraceAsString();
            }
        }
    }

    public function action_getgenerateFile($color_reserved, $color_sold, $color_canceled, $color_expired, $color_rexpired, $color_trManualTicketSale, $color_trManualAgentTicketSale, $color_trManualVaucherSale) {
        try {
            $folder = getcwd();
            $skin = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
            $archivo = "/media/" . self::FILE_TYPE_CSS . "/$skin/salesColors.css"; // el nombre de tu archivo
            $contenido =
                    'tr.reserved
{
    background-color:' . $color_reserved . ';
}
tr.sold
{
    background-color:' . $color_sold . ';
}
tr.canceled
{
    background-color:' . $color_canceled . ';
}
tr.rexpired
{
    background-color:' . $color_rexpired . ';
}
tr.expired
{
    background-color:' . $color_expired . '; 
}
tr.trManualTicketSale
{
    background-color:' . $color_trManualTicketSale . '; 
}
tr.trManualVaucherSale
{
    background-color:' . $color_trManualVaucherSale . '; 
}
tr.trManualAgentTicketSale
{
    background-color:' . $color_trManualAgentTicketSale . '; 
}';
            $fch = fopen($folder . $archivo, "w");
            fwrite($fch, $contenido);
            fclose($fch);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     *
     * @return array<String> retorna un arreglo con las rutas de los menÃºs a las cuales
     * el usuario tiene acceso a traves de los permisos de usuario.
     */
    public function action_getUserInfo() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $id = $this->getSessionParameter(self::USER_ID);
            $user = ORM::factory('User', $id);
            $array = array(
                'user_fName' => $user->person->fName,
                'user_lName' => $user->person->lName,
                'user_name_info' => $user->userName,
                'user_email' => $user->person->email,
                'user_group_name' => $user->group->name,
                'user_office' => $user->office->idOffice,
                'user_img' => $user->imgProfile);
            if ($this->getSessionParameter(self::USER_TERMINAL_ID) != NULL) {
                $db = DB::select(
                                        array('bts_printos.name', 'os')
                                        , array('bts_printmodel.name', 'printer')
                                        , array('bts_terminal.browser', 'browser')
                                )
                                ->from('bts_terminal')
                                ->join('bts_office')->on('bts_office.idOffice', '=', 'bts_terminal.idOffice')
                                ->join('bts_printer')->on('bts_printer.idPrinter', '=', 'bts_terminal.idPrinter')
                                ->join('bts_printmodel')->on('bts_printmodel.idPrintModel', '=', 'bts_printer.idPrintModel')
                                ->join('bts_printbrand')->on('bts_printbrand.idPrintBrand', '=', 'bts_printmodel.idPrintBrand')
                                ->join('bts_printos')->on('bts_printos.idPrintOS', '=', 'bts_terminal.idPrintOS')
                                ->where('bts_terminal.idTerminal', '=', $this->getSessionParameter(self::USER_TERMINAL_ID))->execute()->as_array();
                $array['user_os'] = $db[0]['os'];
                $array['user_printer'] = $db[0]['printer'];
                $array['user_browser'] = $db[0]['browser'];
            }
            $aResponse['data'] = $array;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_updateSelectedTerminal() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $idTerminal = $this->request->post('idt');
        try {
            $oTerminalSelection = new Model_TerminalSelection();
            $oTerminalSelection = $oTerminalSelection
                            ->where('idUser', '=', $this->getSessionParameter(self::USER_ID))
                            ->where('idOffice', '=', $this->getSessionParameter(self::USER_OFFICE_ID))->find();
            if ($oTerminalSelection->loaded()) {
                $oTerminalSelection->idTerminal = $idTerminal;
                $oTerminalSelection->update();
            } else {
                $terminalSelection = new Model_TerminalSelection();
                $terminalSelection->idUser = $this->getSessionParameter(self::USER_ID);
                $terminalSelection->idOffice = $this->getSessionParameter(self::USER_OFFICE_ID);
                $terminalSelection->idTerminal = $idTerminal;
                $terminalSelection->save();
            }
            $this->setSessionParameter(self::USER_TERMINAL_ID, $idTerminal);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listAvaiblePrintersByUserOffice() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $idOffice = $this->getSessionParameter(self::USER_OFFICE_ID);

            $terminal = 'NULL';
            if ($this->getSessionParameter(self::USER_TERMINAL_ID) != NULL) {
                $terminal = $this->getSessionParameter(self::USER_TERMINAL_ID);
            }
            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $array_cols = array(
                'id' => "t.`idTerminal`",
                'os' => "pos.`name`",
                'brand' => 'pb.`name`',
                'model' => 'pm.`name`',
                'browser' => 't.`browser`',
                'acrive' => "IF(t.`idTerminal`= " . $terminal . ",1,0)"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'bts_terminal t ' .
                    'INNER JOIN bts_office o ON o.idOffice = t.idOffice ' .
                    'INNER JOIN bts_printer p ON t.idPrinter = p.idPrinter ' .
                    'INNER JOIN bts_printmodel pm ON pm.idPrintModel = p.idPrintModel ' .
                    'INNER JOIN bts_printbrand pb ON pb.idPrintBrand = pm.idPrintBrand ' .
                    'INNER JOIN bts_printos pos ON t.idPrintOS=pos.idPrintOS ';

            $where_conditions = "t.`state` =" . self::STATUS_ACTIVE . " AND t.`idOffice` = " . $idOffice . " ";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_getUsersByGroup() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idGroup = $this->request->post('idg');
            if ($idGroup == 'A') {
                $idGroup = self::GROUP_ADMIN;
            }
            $aResponse['data'] = $db = DB::select(array('bts_person.fullName', 'name'), array('bts_user.idUser', 'id'))
                            ->from('bts_person')->join('bts_user')->on('bts_person.idPerson', '=', 'bts_user.idUser')
                            ->where('bts_user.idGroup', '=', $idGroup)
                            ->order_by('name')
                            ->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function getEnumValuesFromTable($tableName, $columName) {
        $db = DB::query(Database::SELECT, "SHOW COLUMNS FROM `$tableName` LIKE '$columName'")->as_object()->execute()->current();
        if ($db != NULL) {
            $raw_values = str_replace("enum(", "", $db->Type);
            $raw_values = str_replace(")", "", $raw_values);
            $raw_values = str_replace("'", "", $raw_values);
            $raw_values = explode(',', $raw_values);
            return $raw_values;
        } else {
            return array('NO_DATA');
        }
    }

    public function action_getUsers() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $term = $this->request->post('term');
            $type = $this->request->post('t');

            $cols = array(
                array('pe.idPerson', 'id'),
                array('pe.fullname', 'value'),
                array('o.name', 'office')
            );
            if ($type != NULL) {
                if ($type == 'f') {
                    $cols = array('pe.*', 'u.*');
                }
            }
            $users = DB::select_array($cols)
                    ->from(array('bts_user', 'u'))
                    ->join(array('bts_person', 'pe'))->on('u.idUser', '=', 'pe.idPerson')
                    ->join(array('bts_office', 'o'))->on('o.idOffice', '=', 'u.idOffice')
                    ->where('u.status', '=', self::STATUS_ACTIVE);
            if ($term != NULL OR $type == 'c') {
                $users =
                        $users
                        ->and_where('pe.fullname', 'LIKE', '%' . $term . '%');
            }
            $aResponse['data'] = $users->execute()->as_array();

            $this->fnResponseFormat($aResponse);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function alterEnumValuesForTable($tableName, $columName, $values) {
        if ($columName == 'component') {
            $string_values = "GENERAL";
            $default_value = "GENERAL";
        } elseif ($columName == 'dataType') {
            $default_values = array('boolean', 'decimal', 'percentage', 'color', 'integer', 'string');
            if ($values[0] == '') {
                $values = $default_values;
            }
        }
        if (is_array($values) && $values[0] != '') {
            sort($values, SORT_STRING);
            $default_value = "'" . $values[0] . "'";
            $string_values = implode(',', $values);
            $string_values = "'" . str_replace(',', "','", $string_values) . "'";
        } else {
            $default_value = "'" . $string_values . "'";
            $string_values = $default_value;
        }
       DB::query(Database::UPDATE, "ALTER TABLE `$tableName` CHANGE `$columName` `$columName` enum($string_values) DEFAULT $default_value NOT NULL")->execute();
    }
     public function action_cleaningCache()
    {   $this->auto_render = FALSE;        
        $aResponse = $this->json_array_return;
        $this->cleaningAllCache();
        $this->fnResponseFormat($aResponse, 'json');
        die();
    }
    public function action_cleaningLanguages()
    {   $this->auto_render = FALSE;        
        $aResponse = $this->json_array_return;
        $this->cleaningLanguages();
        $this->fnResponseFormat($aResponse, 'json');
        die();
    }
    public function action_cleaningSessionsUsers()
    {   $this->auto_render = FALSE;        
        $aResponse = $this->json_array_return;
        $this->cleaningSessionsUsers();
        $this->fnResponseFormat($aResponse, 'json');
        die();
    }
}
