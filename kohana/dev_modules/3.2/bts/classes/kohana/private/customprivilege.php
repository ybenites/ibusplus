<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Customprivilege extends Kohana_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('custom_privilege', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('custom_privilege', self::FILE_TYPE_JS));
        $custom_privilege_view = new View('private/custom_privilege');
        $user_list = new Model_User();
        if ($this->getSessionParameter(self::USER_ROOT_OPTION)) {
            $user_list = $user_list->where('status', '=', self::STATUS_ACTIVE)->find_all();
        } else {
            $user_list = $user_list->where('status', '=', self::STATUS_ACTIVE)->and_where('rootOption', '=', self::STATUS_DEACTIVE)->find_all();
        }

        $custom_privilege_view->user_list = $user_list;
        $custom_privilege_view->privilegeRows = array();
        $this->template->content = $custom_privilege_view;
    }

    public function action_getSelectUser() {
        try {
            $this->auto_render = false;
            $user = ORM::factory("User", $_POST['idUser']);
            $groups = $this->getGroupByUser($user);
            $privilegeRows = $this->createMenuArray(NULL, false, $groups, $user);
            foreach ($privilegeRows as $pr) {
                echo $pr->printMenuItemRow($pr, 0, $pr->groups);
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    private function getGroupByUser($user) {
        return ORM::factory("Group")->where('idGroup', '=', $user->group->idGroup)->find_all();
    }

    public function action_createOrDeleteCustomAccess() {
        try {
            $this->auto_render = FALSE;
            $aResponse = $this->json_array_return;
            $post = Validation::factory($_POST)
                    ->rule('menu_id', 'not_empty')
                    ->rule('user_id', 'not_empty');
            if (!$post->check()) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
            }
            $menu_id = $_POST['menu_id'];
            $user_id = $_POST['user_id'];
            $custom_privilege = new Model_Customprivilege();
            $custom_privilege = ORM::factory('CustomPrivilege')
                            ->where('idMenu', '=', $menu_id)
                            ->where('idUser', '=', $user_id)->find();
            if ($custom_privilege->loaded() == TRUE) {
                $custom_privilege->delete();
            } else {
                $custom_privilege->idMenu = $menu_id;
                $custom_privilege->idUser = $user_id;
                $custom_privilege->grantDate = Date::formatted_time();
                $custom_privilege->guilty = $this->getSessionParameter(self::USER_ID);
                $custom_privilege->save();
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_createActivateDeactiveUserPrivilege() {
        try {
            $this->auto_render = false;
            $aResponse = $this->json_array_return;
            $post = Validation::factory($_POST)
                    ->rule('user_id', 'not_empty');
            if (!$post->check()) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
            }
            $user_id = $_POST['user_id'];
            $user = ORM::factory('User', $user_id);
            if ($user->userPrivilegeActive == 1)
                $user->userPrivilegeActive = self::STATUS_DEACTIVE;
            else
                $user->userPrivilegeActive = self::STATUS_ACTIVE;
            $user->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}