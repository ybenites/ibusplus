<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Module extends Kohana_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('module', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('module', self::FILE_TYPE_JS));
        $module_view = new View('private/module');
        $this->template->content = $module_view;
    }

    public function saveUpdateModule() {
        $idModule = $this->request->post('moduleId');
        $idParent = $this->request->post('parentId');
        $moduleName = trim($this->request->post('moduleName'));
        $moduleCode = trim($this->request->post('moduleCode'));

        //$tmp = DB::select(db::expr('COUNT(module.idModule) AS c'))->from('module')->where('module.name', 'LIKE', $moduleName)->and_where('module.status', '=', self::STATUS_ACTIVE);
        $tmpName = DB::select(db::expr('COUNT(module.idModule) AS c'))->from(array('bts_module', 'module'))->where('module.name', 'LIKE', $moduleName);
        $tmpCode = DB::select(db::expr('COUNT(module.idModule) AS c'))->from(array('bts_module', 'module'))->where('module.code', 'LIKE', $moduleCode);
        
        if ($idModule == NULL) {
            $module = new Model_Module();
        } else {
            $module = new Model_Module($idModule);
            $tmpName = $tmpName->and_where('module.idModule', "<>", $idModule);
            $tmpCode = $tmpCode->and_where('module.idModule', "<>", $idModule);
        }
        $tmpName = $tmpName->as_object()->execute()->current();
        $tmpCode = $tmpCode->as_object()->execute()->current();
        if ($tmpCode->c > 0) {
            throw new Exception(__("El código del módulo ya existe en alguno de los niveles"), self::CODE_SUCCESS);
        } elseif($tmpName->c > 0) {
            throw new Exception(__("El módulo ya existe en alguno de los niveles"), self::CODE_SUCCESS);
        } else {
            $module->code = $moduleCode;
            $module->name = $moduleName;
            $module->status = self::STATUS_ACTIVE;
            if ($idParent != null) {
                $module->idSuperModule = $idParent;
            }
            if ($idModule == NULL) {
                $module->save();
            } else {
                $module->update();
            }
        }
    }

    public function action_saveModule() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $this->saveUpdateModule();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getModuleById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_Module($this->request->post('id'));
            $aResponse['data'] = $file->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_removeModule() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_Module($this->request->post('id'));
            if ($file->status == self::STATUS_DEACTIVE)
                $file->status = self::STATUS_ACTIVE;
            else {
                $file->status = self::STATUS_DEACTIVE;
            }
            $file->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['data'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listModulesXML() {
        $this->auto_render = FALSE;
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
        try {
            $node = (integer) $_REQUEST['nodeid'];
            $n_lvl = (integer) $_REQUEST['n_level'];
            $SQL_LEAF_NODES = 'SELECT l.idSuperModule FROM bts_module AS p LEFT JOIN bts_module AS l ON p.idModule = l.idSuperModule WHERE l.idModule IS NOT NULL ORDER BY l.name';
            $results = DB::query(Database::SELECT, $SQL_LEAF_NODES)->execute()->as_array();
            $leaf_nodes = array();
            foreach ($results as $r) {
                $leaf_nodes[$r['idSuperModule']] = $r['idSuperModule'];
            }

            header("Content-type: text/xml;charset=utf-8");

            $et = ">";
            echo "<?xml version='1.0' encoding='utf-8'?$et\n";
            echo "<rows>";
            echo "<page>1</page>";
            echo "<total>1</total>";
            echo "<records>1</records>";
            if ($node > 0) {
                $wh = 'bts_module.idSuperModule=' . $node; //. ' AND module.status=' . self::STATUS_ACTIVE;
                $n_lvl = $n_lvl + 1;
            } else {
                $wh = 'ISNULL(bts_module.idSuperModule)'; //AND module.status=' . self::STATUS_ACTIVE;
            }
            $NEXT_NODE =
                    "SELECT bts_module.idModule, bts_module.code,bts_module.name, bts_module.idSuperModule, bts_module.`creationDate` as cDate ,bts_module.`lastChangeDate` as lcDate, bts_module.`status` as `status`" .
                    ", pc.fullName as uc " .
                    ", DATE_FORMAT(bts_module.creationDate,'$date_format $time_format') as regDate " .
                    ", plc.fullName as ulc " .
                    ", DATE_FORMAT(bts_module.lastChangeDate,'$date_format $time_format') as lastDate " .
                    "FROM bts_module INNER JOIN bts_person pc ON pc.`idPerson` = bts_module.`userCreate` INNER JOIN bts_person plc ON plc.`idPerson` = bts_module.`userLastChange` " .
                    "WHERE " . $wh . " ORDER BY bts_module.name";
            
            $results = DB::query(Database::SELECT, $NEXT_NODE)->execute()->as_array();
            foreach ($results as $row) {
                #VALIDACIONES
                if (!$row['idSuperModule'])
                    $valp = 'NULL';
                else
                    $valp = $row['idSuperModule'];
                if (array_search($row['idModule'], $leaf_nodes) > 0)
                    $leaf = 'false';
                else
                    $leaf = 'true';
                $ico_cell = ""; //htmlspecialchars(HTML::image('media/ico/' . $row['icon'], array('title' => $row['name'], 'alt' => $row['icon'], 'width' => '16px')));
                $edit_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-edit" data-ido="' . $row['idModule'] . '" title="' . __('Editar') . '" ></a>');
                if ((int) $row['status'])
                    $active_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-on" data-ido="' . $row['idModule'] . '" title="' . __("Activar") . '" ></a>');
                else
                    $active_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-off" data-ido="' . $row['idModule'] . '" title="' . __("Activar") . '" ></a>');

                $submenu_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-new-subfile"  data-ido="' . $row['idModule'] . '" title="' . __("Nuevo SubModulo") . '" ></a>');
                $php_action_option = ""; //htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-php-action" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Nueva Acción PHP") . '" ></a>');
                $order_option = ""; //htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-order" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Ordenar Menú") . '" ></a>');
                $options = $edit_option . $active_option . $submenu_option;


                echo "<row>";
                echo "<cell>" . $row['idModule'] . "</cell>";
                echo "<cell>" . $row['cDate'] . "</cell>";
                echo "<cell>" . $row['lcDate'] . "</cell>";
                echo "<cell>" . $row['code'] . "</cell>";
                echo "<cell>" . $row['name'] . "</cell>";
                echo "<cell>" . $row['uc'] . "</cell>";
                echo "<cell>" . $row['regDate'] . "</cell>";
                echo "<cell>" . $row['ulc'] . "</cell>";
                echo "<cell>" . $row['lastDate'] . "</cell>";
                echo "<cell>" . $row['status'] . "</cell>";
                echo "<cell>" . $options . "</cell>";
                echo "<cell>" . $n_lvl . "</cell>";
                echo "<cell><![CDATA[" . $valp . "]]></cell>";
                echo "<cell>" . $leaf . "</cell>";
                echo "<cell>false</cell>";
                echo "</row>";
            } echo "</rows>";
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_listModulesForMenu() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $ids = $this->request->post('id');
            if ($ids == NULL) {
                $db = DB::select(array('module.idModule', 'id'), array('module.name', 'value'))->from(array('bts_module', 'module'))->where('module.idSuperModule', '=', NULL)->execute()->as_array();
            } else {
                $db = DB::select(array('module.idModule', 'id'), array('module.name', 'value'))->from(array('bts_module', 'module'))->join(array('bts_menu', 'menu'))->on('module.idModule', '=', 'menu.idModule')->where('menu.idMenu', '=', $ids[0])->execute()->as_array();
            }
            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    /*
      public function action_registerModule() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;
      try {
      $post = Validation::factory($_POST)
      ->rule('module_name', 'not_empty');
      if (!$post->check()) {
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
      }
      $module_name = $this->request->post('module_name');
      $module_desc = $this->request->post('module_desc');
      $super_module_id = $this->request->post('idSuperModule');

      $module_id = $this->request->post('idModule');
      $module = new Model_Module();
      if ($module_id != 0)
      $module = ORM::factory('Module', $module_id);
      $module->name = trim($module_name);
      $module->desc = trim($module_desc);

      if ($super_module_id != 0) {
      $module->idSuperModule = $super_module_id;
      }
      $module->status = self::STATUS_ACTIVE;

      $module->save();
      } catch (Exception $exc) {
      DB::query(NULL, "ROLLBACK")->execute();
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = 'ERROR - ROLLBACK ACTION SUCCESS ' . $this->errorHandling($exc);
      }
      echo json_encode($aResponse);
      }

      public function action_listModules() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;

      try {
      $db = DB::select()->from('module')
      ->where('module.idSuperModule', '=', NULL)
      ->order_by('module.name', 'asc')
      ->execute()->as_array();
      $aResponse['data'] = $db;
      } catch (Database_Exception $e) {
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = $this->errorHandling($exc);
      }
      echo json_encode($aResponse);
      }

      private function searchSubModule($superModule = NULL) {
      try {
      $subModule = new Model_Module();
      $subModule = $subModule->and_where('module.idSuperModule', '=', $superModule)->find_all();
      $ids_subModules = "";
      if (count($subModule) > 0) {
      foreach ($subModule as $subModule) {
      $ids_subModules .= $subModule;
      $ids_subModules .=";";
      }
      return $ids_subModules;
      } else {
      return $ids_subModules;
      }
      } catch (Exception $exc) {
      echo $this->errorHandling($exc);
      }
      }

      private function removeSubModule($idSuperModule = NULL) {
      $ids = $this->searchSubModule($idSuperModule);
      if (!(empty($ids))) {
      $subIds = explode(";", $ids);
      foreach ($subIds as $subId) {
      if (!empty($subId)) {
      $this->removeSubModule($subId);
      DB::delete('Module')->and_where('idModule', '=', $subId)->execute();
      }
      }
      }
      return "";
      }

      private function offSubModule($idSuperModule = NULL) {
      $ids = $this->searchSubModule($idSuperModule);
      if (!(empty($ids))) {
      $subIds = explode(";", $ids);
      foreach ($subIds as $subId) {
      if (!empty($subId)) {
      $this->removeSubModule($subId);
      DB::update('Module')->set(array('status' => self::STATUS_DEACTIVE))->and_where('idModule', '=', $subId)->execute();
      }
      }
      }
      return "";
      }

      private function onSubModule($idSuperModule = NULL) {
      $ids = $this->searchSubModule($idSuperModule);
      if (!(empty($ids))) {
      $subIds = explode(";", $ids);
      foreach ($subIds as $subId) {
      if (!empty($subId)) {
      $this->removeSubModule($subId);
      DB::update('Module')->set(array('status' => self::STATUS_ACTIVE))->and_where('idModule', '=', $subId)->execute();
      }
      }
      }
      return "";
      }


      public function action_listSubModule() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;
      try {
      $superModule_id = $this->request->post('idModule');
      $modules = DB::select()
      ->from('Module')
      ->and_where('module.idSuperModule', '=', $superModule_id)
      ->order_by('module.name', 'desc')
      ->execute()->as_array();
      $aResponse['data'] = $modules;
      } catch (Exception $exc) {
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = $this->errorHandling($exc);
      }
      echo json_encode($aResponse);
      }

      public function action_isSuperModule() {
      $this->auto_render=FALSE;
      $module_id = $this->request->post('idModule');
      $childs = ORM::factory('Module')
      ->where('idSuperModule', '=', $module_id)
      ->where('status', '=', 1)->count_all();
      if ($childs > 0) {
      echo 1;
      }
      }

      public function action_onModule() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;
      try {
      $module_id = $_POST['idModule'];
      $module = ORM::factory('Module', $module_id);
      $module->status = self::STATUS_ACTIVE;
      $this->onSubModule($module_id);
      $module->save();
      } catch (Exception $exc) {
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = $this->errorHandling($exc);
      }
      echo json_encode($aResponse);
      }

      public function action_offModule() {
      $this->auto_render = FALSE;
      $aResponse = $this->json_array_return;
      try {

      $module_id = $_POST['idModule'];
      $module = ORM::factory('Module', $module_id);
      $module->status = self::STATUS_DEACTIVE;
      $this->offSubModule($module_id);
      $module->save();

      } catch (Exception $exc) {
      $aResponse['code'] = self::CODE_ERROR;
      $aResponse['msg'] = $this->errorHandling($exc);
      }
      echo json_encode($aResponse);
      }

      public function createModuleList($idSuperModule) {
      try {
      $moduleArray = array();

      $menus = DB::select()->from('module')
      ->and_where('module.status', '=', self::STATUS_ACTIVE)
      ->where('module.idSuperModule', '=', $idSuperModule)
      ->execute()->as_array();


      foreach ($menus as $m) {
      array_push($moduleArray, array('name' => $m['name'], 'desc' => $m['desc'], 'items' => $this->createModuleList($m['idModule']), 'id' => $m['idModule']));
      }

      return $moduleArray;
      } catch (Exception $exc) {
      echo $this->errorHandling($exc);
      }
      } */
}