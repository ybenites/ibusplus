<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Printers extends Kohana_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('printer', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('printer', self::FILE_TYPE_JS));
        $printer_view = new View('private/printers');
        $this->template->content = $printer_view;
    }

    public function action_listPrintBrands() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $db = DB::select(array('printbrand.idPrintBrand', 'id')
                                    , array('printbrand.name', 'value'))
                            ->from(array('bts_printbrand', 'printbrand'))
                            ->order_by('printbrand.name', 'asc')
                            ->execute()->as_array();

            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    private function createUpdateBrand() {
        $aResponse = $this->json_array_return;
        try {
            $brand_id = $this->request->post('brand_id');
            $brand_name = $this->request->post('brand_name');
            if ($brand_id == NULL) {
                $pb = new Model_PrintBrand();
            } else {
                $pb = new Model_PrintBrand($brand_id);
            }
            $pb->name = $brand_name;
            if ($brand_id == NULL) {
                $pb->save();
            } else {
                $pb->update();
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        return $aResponse;
    }

    public function action_createBrand() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateBrand());
    }

    public function action_updateBrand() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateBrand());
    }

    private function createUpdateModel() {
        $aResponse = $this->json_array_return;
        try {
            $model_id = $this->request->post('model_id');
            $brand_id = $this->request->post('brandModel_id');
            $model_name = $this->request->post('model_name');
            if ($model_id == NULL) {
                $pm = new Model_PrintModel();
            } else {
                $pm = new Model_PrintModel($model_id);
            }
            $pm->idPrintBrand = $brand_id;
            $pm->name = $model_name;
            if ($model_id == NULL) {
                $pm->save();
            } else {
                $pm->update();
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        return $aResponse;
    }

    public function action_createModel() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateModel());
    }

    public function action_updateModel() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateModel());
    }

    public function action_listPrintModelsByBrand() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idBrand = $this->request->post('idb');
            $db = DB::select(array('printmodel.idPrintModel', 'id')
                                    , array('printmodel.name', 'value'))
                            ->from(array('bts_printmodel', 'printmodel'))
                            ->where('printmodel.idPrintBrand', '=', $idBrand)
                            ->order_by('printmodel.name', 'asc')
                            ->execute()->as_array();

            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function createUpdatePrinter() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $printer_id = $this->request->post('printer_id');
            $model_id = $this->request->post('print_model');
            $notes = $this->request->post('print_notes');
            $printer_code = $this->request->post('printer_code');
            if ($printer_id == NULL) {
                $printer = new Model_Printer();
            } else {
                $printer = new Model_Printer($printer_id);
            }
            $db = DB::select(DB::expr('COUNT(printer.idPrinter) AS c'))
                            ->from(array('bts_printer', 'printer'))
                            ->where('printer.idPrintModel', '=', $model_id)
                            ->execute()->get('c');
            if ($db == 0) {
                $printer->idPrintModel = $model_id;
                $printer->notes = $notes;
                $printer->code = $printer_code;
                if ($printer_id == NULL) {
                    $printer->save();
                } else {
                    $printer->update();
                }
            } else {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __('Este modelo de impresora ya se encuentra registrado');
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        return $aResponse;
    }

    public function action_createPrinter() {
        echo json_encode($this->createUpdatePrinter());
    }

    public function action_listPrinters() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);

            $array_cols = array(
                'id' => "p.`idPrinter`",
                'model' => "pm.`name`",
                'brand' => 'pb.`name`',
                'code' => 'p.`code`'
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'bts_printer p ' .
                    'INNER JOIN bts_printmodel pm ON pm.idPrintModel = p.idPrintModel ' .
                    'INNER JOIN bts_printbrand pb ON pm.idPrintBrand = pb.idPrintBrand ';

            $where_conditions = "p.state = " . self::STATUS_ACTIVE . " ";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_getPrinterByBrandModel() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idBrand = $this->request->post('idb');
            $db = DB::select(array('printer.idPrinter', 'id')
                                    , array('printmodel.name', 'value'))
                            ->from(array('bts_printer', 'printer'))->join(array('bts_printmodel', 'printmodel'))->on('printer.idPrintModel', '=', 'printmodel.idPrintModel')
                            ->where('printmodel.idPrintBrand', '=', $idBrand)
                            ->and_where('printer.state', '=', self::STATUS_ACTIVE)
                            ->order_by('printmodel.name', 'asc')
                            ->execute()->as_array();
            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

}