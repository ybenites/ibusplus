<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Messages extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('messages', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('messages', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
        $oModel_City = new Model_City;

        $aCitys = $oModel_City
                ->select('city.idCity', 'city.name')
                ->where('city.status', '=', self::STATUS_ACTIVE)
                ->order_by('city.name')
                ->find_all();

        $oMessage_View = new View('private/messages');
        $oMessage_View->aCitys = $aCitys;
        $oMessage_View->date_format = $date_format;
        $oMessage_View->time_format = $time_format;
        $this->template->content = $oMessage_View;
    }

    public function action_getPreviousMessages() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $oPostPage = Validation::factory($_POST)
                    ->rule('lastNotification', 'not_empty')
                    ->rule('n_page', 'not_empty');
            if (!$oPostPage->check()) {
                $aResponse['msg'] = 'Invalid Data';
            } else {
                $iPage = $this->request->post('n_page');
                $i_last_notification = $this->request->post('lastNotification');
                $aResponse['data'] = UserHelper::fnGetLastNotifications($iPage, $i_last_notification);
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getMessageById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $oPost_Message = Validation::factory($_POST)
                    ->rule('i_response', 'not_empty');
            if (!$oPost_Message->check()) {
                $aResponse['msg'] = 'Invalid Data';
            } else {
                $i_response = $this->request->post('i_response');

                $o_select = DB::select(
                                        array(
                                    'me.idMessage'
                                    , 'idOut'
                                        )
                                        , array(
                                    'me.message'
                                    , 'messageOut'
                                        )
                                        , array(
                                    'ur.fullName'
                                    , 'userRegistrationOut'
                                        )
                                        , array(
                                    DB::expr('UNIX_TIMESTAMP(me.creationDate)')
                                    , 'updateNotification'
                                        )
                                        , array(
                                    DB::expr("DATE_FORMAT(me.creationDate, '%d/%m/%Y %h:%i %p')")
                                    , 'dateCreationOut'
                                        )
                                )->as_object()
                                ->from(array('bts_messages', 'me'))
                                ->order_by('me.creationDate', 'ASC')
                                ->join(array('person', 'ur'))
                                ->on('me.userCreate', '=', 'ur.idPerson')
                                ->where('me.idMessage', '=', $i_response)
                                ->execute()->current();

                $aResponse['data'] = $o_select;
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getMessages() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
                $iCityId = $this->getSessionParameter(self::USER_CITY_ID);
                $iOfficeId = $this->getSessionParameter(self::USER_OFFICE_ID);
                $iUserId = $this->getSessionParameter(self::USER_ID);
                $iLastNotification = $this->request->post('lastNotification');

                $o_select_messages = DB::select(
                                        array(
                                    'me.idMessage'
                                    , 'idOut'
                                        )
                                        , array(
                                    'me.message'
                                    , 'messageOut'
                                        )
                                        , array(
                                    'ur.fullName'
                                    , 'userRegistrationOut'
                                        )
                                        , array(
                                    DB::expr("REPLACE(us.imgProfile, '/128/', '/64/')")
                                    , 'user_image'
                                        )
                                        , array(
                                    DB::expr('UNIX_TIMESTAMP(me.creationDate)')
                                    , 'updateNotification'
                                        )
                                        , array(
                                    DB::expr("DATE_FORMAT(me.creationDate, '%d/%m/%Y %h:%i %p')")
                                    , 'dateCreationOut'
                                        )
                                        , array(
                                    "me.toIdCity"
                                    , 'cityOut'
                                        )
                                        , array(
                                    "me.toIdOffice"
                                    , 'officeOut'
                                        )
                                        , array(
                                    "me.toIdUser"
                                    , 'userOut'
                                        )
                                        , array(
                                    "me.userCreate"
                                    , 'userCreateOut'
                                        )
                                        , array(
                                    'me.idResponse'
                                    , 'responseOut'
                                        )
                                )
                                ->from(array('bts_messages', 'me'))
                                ->order_by('me.creationDate', 'ASC')
                                ->join(array('bts_user', 'us'))
                                ->on('me.userCreate', '=', 'us.idUser')
                                ->join(array('bts_person', 'ur'))
                                ->on('me.userCreate', '=', 'ur.idPerson')
                                ->join(array('bts_person', 'u'), 'LEFT')
                                ->on('me.toIdUser', '=', 'u.idPerson')
                                ->join(array('bts_office', 'o'), 'LEFT')
                                ->on('me.toIdOffice', '=', 'o.idOffice')
                                ->join(array('bts_city', 'c'), 'LEFT')
                                ->on('me.toIdCity', '=', 'c.idCity')
                                ->where(
                                        DB::expr('UNIX_TIMESTAMP(me.creationDate)'), '>', (int) $iLastNotification
                                )
                                ->and_where(
                                        'me.userCreate', '<>', intval($iUserId)
                                )
                                ->and_where(
                                        DB::expr("
                                        CASE
                                           WHEN me.toIdCity = 0 AND me.toIdOffice = 0 AND me.toIdUser = 0 THEN TRUE
                                        ")
                                        , 'ELSE'
                                        , DB::expr("
                                                me.toIdCity = :ID_CITY OR me.toIdOffice = :ID_OFFICE OR me.toIdUser = :ID_USER 
                                            END"
                                                , array(
                                            ':ID_CITY' => intval($iCityId)
                                            , ':ID_OFFICE' => intval($iOfficeId)
                                            , ':ID_USER' => intval($iUserId)
                                                )
                                        )
                                )
                                ->and_where('me.state', '=', 1)
                                ->execute()->as_array();
                $a_response['data'] = array_merge(
                        $o_select_messages
                        , array(
                    'date' => UserHelper::fnGetCurrentDate()
                        )
                );
           
        } catch (Exception $exc) {
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response, 'json');
        die();
    }

    public function action_updateNotification() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $oPostDate = Validation::factory($_POST)
                    ->rule('lastNotification', 'not_empty');
            if (!$oPostDate->check()) {
                $aResponse['msg'] = 'Invalid Data';
            } else {

                $iUserId = $this->getSessionParameter(self::USER_ID);
                $iLastNotification = $this->request->post('lastNotification');

                $oUpdateNotification = DB::update('bts_user')
                                ->value('lastDateMessage', DB::expr("FROM_UNIXTIME(:LAST_NOTIFICATION)", array(':LAST_NOTIFICATION' => $iLastNotification)))
                                ->where('idUser', '=', intval($iUserId))->execute();

                $aResponse['data'] = $oUpdateNotification;
                $this->fncleaningFragmentCache(true);
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_saveNotification() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $o_post_type_notification = Validation::factory($_POST)
                    ->rule('t_type_notification', 'not_empty');
            if (!$o_post_type_notification->check()) {
                $aResponse['msg'] = 'Invalid Data';
            } else {
                $i_type_notification = $this->request->post('t_type_notification');

                if ($i_type_notification == 1) {
                    $oPostMessage = Validation::factory($_POST)
                            ->rule('dlg_form_to_city', 'not_empty')
                            ->rule('dlg_form_to_office', 'not_empty')
                            ->rule('dlg_form_to_user', 'not_empty')
                            ->rule('dlg_form_message_content', 'not_empty');
                    if (!$oPostMessage->check()) {
                        $aResponse['msg'] = 'Invalid Data';
                    } else {

                        $iUserId = $this->getSessionParameter(self::USER_ID);

                        $iTo_City = $this->request->post('dlg_form_to_city');
                        $iTo_Office = $this->request->post('dlg_form_to_office');
                        $iTo_User = $this->request->post('dlg_form_to_user');
                        $sMessage = $this->request->post('dlg_form_message_content');

                        $oModel_Message = new Model_Messages;
                        $oModel_Message->toIdCity = $iTo_City;
                        $oModel_Message->toIdOffice = $iTo_Office;
                        $oModel_Message->toIdUser = $iTo_User;
                        $oModel_Message->message = $sMessage;
                        $oModel_Message->state = 1;
                        $this->fncleaningFragmentCache(true);
                        if ($oModel_Message->save()) {
                            $aResponse['data'] = 1;
                        } else {
                            $aResponse['data'] = 0;
                        }
                    }
                } elseif ($i_type_notification == 2) {
                    $oPostMessage = Validation::factory($_POST)
                            ->rule('f_instant_message_response', 'not_empty')
                            ->rule('f_instant_to_id', 'not_empty')
                            ->rule('f_instant_msg_cont', 'not_empty');
                    if (!$oPostMessage->check()) {
                        $aResponse['msg'] = 'Invalid Data';
                    } else {

                        $iUserId = $this->getSessionParameter(self::USER_ID);

                        $iTo_City = 0;
                        $iTo_Office = 0;
                        $iTo_User = $this->request->post('f_instant_to_id');
                        $sMessage = $this->request->post('f_instant_msg_cont');
                        $i_response_id = $this->request->post('f_instant_message_response');

                        $oModel_Message = new Model_Messages;
                        $oModel_Message->toIdCity = $iTo_City;
                        $oModel_Message->toIdOffice = $iTo_Office;
                        $oModel_Message->toIdUser = $iTo_User;
                        $oModel_Message->message = $sMessage;
                        $oModel_Message->idResponse = $i_response_id;
                        $oModel_Message->state = 1;
                        $this->fncleaningFragmentCache(true);
                        if ($oModel_Message->save()) {                           
                            $aResponse['data'] = 1;
                        } else {
                            $aResponse['data'] = 0;
                        }
                    }
                }
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getUsersMessage() {

        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $oPostSearch = Validation::factory($_POST)
                    ->rule('term', 'not_empty');
            if (!$oPostSearch->check()) {
                $aResponse['msg'] = 'Invalid Data';
            } else {

                $sSearch = $this->request->post('term');
                $sSearch = '%' . $sSearch . '%';
                $iUserId = $this->getSessionParameter(self::USER_ID);

                $oSelectCitysMessages = DB::select(
                                        array(
                                    'c.idCity'
                                    , 'to_city'
                                        )
                                        , array(
                                    DB::expr(0),
                                    DB::expr('to_office')
                                        )
                                        , array(
                                    DB::expr(0),
                                    DB::expr('to_user')
                                        )
                                        , array(
                                    'c.name',
                                    DB::expr('value_search')
                                        )
                                        , array(
                                    DB::expr("'" . __("Ciudad") . "'"),
                                    DB::expr('prefix')
                                        )
                                )
                                ->from(
                                        array('bts_city', 'c')
                                )
                                ->where('c.name', 'LIKE', DB::expr(":SEARCH", array(':SEARCH' => $sSearch)))
                                ->and_where('c.status', '=', 1)->order_by('value_search')
                ;
                $oSelectOfficesMessages = DB::select(
                                array(
                            DB::expr(0)
                            , 'to_city'
                                )
                                , array(
                            'o.idOffice',
                            DB::expr('to_office')
                                )
                                , array(
                            DB::expr(0),
                            DB::expr('to_user')
                                )
                                , array(
                            'o.name',
                            DB::expr('value_search')
                                ), array(
                            DB::expr("'" . __("Oficina") . "'"),
                            DB::expr('prefix')
                                )
                        )
                        ->from(
                                array('bts_city', 'c')
                        )
                        ->join(
                                array('bts_office', 'o')
                        )
                        ->on('c.idCity', '=', 'o.idCity')
                        ->where('o.name', 'LIKE', DB::expr(":SEARCH", array(':SEARCH' => $sSearch)))
                        ->and_where('c.status', '=', 1)
                        ->and_where('o.status', '=', 1);

                $oSelectUsersMessages = DB::select(
                                array(
                            DB::expr(0),
                            DB::expr('to_city')
                                )
                                , array(
                            DB::expr(0),
                            DB::expr('to_office')
                                )
                                , array(
                            'u.idUser',
                            DB::expr('to_user')
                                )
                                , array(
                            'p.fullName',
                            DB::expr('value_search')
                                ), array(
                            DB::expr("'" . __("Usuario") . "'"),
                            DB::expr('prefix')
                                )
                        )
                        ->from(
                                array('bts_city', 'c')
                        )
                        ->join(
                                array('bts_office', 'o')
                        )
                        ->on('c.idCity', '=', 'o.idCity')
                        ->join(
                                array('bts_user', 'u')
                        )
                        ->on('o.idOffice', '=', 'u.idOffice')
                        ->join(
                                array('bts_person', 'p')
                        )
                        ->on('u.idUser', '=', 'p.idPerson')
                        ->where('p.fullName', 'LIKE', DB::expr(":SEARCH", array(':SEARCH' => $sSearch)))
                        ->and_where('c.status', '=', 1)
                        ->and_where('o.status', '=', 1)
                        ->and_where('u.status', '=', 1)
                        ->and_where('u.idUser', '<>', intval($iUserId));

                $oSelectAllMessages = DB::select(
                                array(
                            DB::expr(0),
                            DB::expr('to_city')
                                )
                                , array(
                            DB::expr(0),
                            DB::expr('to_office')
                                )
                                , array(
                            DB::expr(0),
                            DB::expr('to_user')
                                )
                                , array(
                            DB::expr("'Todos'"),
                            DB::expr('value_search')
                                ), array(
                            DB::expr("'" . __("Todos") . "'"),
                            DB::expr('prefix')
                                )
                        )->from(DB::expr('DUAL'))
                        //->where(DB::expr("'".__("Todos")."'"), 'LIKE', DB::expr(":SEARCH", array(':SEARCH' => $sSearch)))
                        ->union($oSelectUsersMessages, true)
                        ->union($oSelectOfficesMessages, true)
                        ->union($oSelectCitysMessages, true)

                ;

                //->order_by('countTotal');

                $aResult = $oSelectAllMessages->execute()->as_array();
                //  echo Database::instance()->last_query;die();
                $aResponse['data'] = $aResult;
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getListMessages() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);

        try {
            $idUser = $this->request->post('idUser');
            $d_creation_dateCant = $this->request->post('creation_dateCant') == '' ? 'TRUE' : "'" . $this->request->post('creation_dateCant') . "'";
            $d_creation_date = $this->request->post('creation_date');
            $selectMessages = DB::select(
                            array('ur.idUser', 'idUserSender')
                            , DB::expr("IF(m.toIdUser > 0,ud.idUser,0) AS idUserDestination"), array('pr.fullName', 'sender'), array('ur.imgProfile', 'img'), DB::expr("DATE_FORMAT(m.creationDate,' $date_format $time_format ') as creation_dates"), array('m.creationDate', 'creation_date'), array(DB::expr(" 
                    CASE
                     WHEN m.toIdCity = 0 AND m.toIdOffice = 0 AND m.toIdUser = 0
                     THEN 'todos'
                     WHEN m.toIdCity > 0 AND m.toIdOffice = 0 AND m.toIdUser = 0
                     THEN CONCAT('ciudad: ', c.name)
                     WHEN m.toIdCity = 0 AND m.toIdOffice > 0 AND m.toIdUser = 0
                     THEN CONCAT('oficina: ', o.name)
                     ELSE 
                     pd.fullName END"), 'destination'), array(DB::expr('UNIX_TIMESTAMP(m.creationDate)'), 'lastNotification'), array('m.message', 'message'))
                    ->from(array('bts_messages', 'm'))
                    ->join(array('bts_user', 'ur'))->on('m.userCreate', '=', 'ur.idUser')
                    ->join(array('bts_person', 'pr'))->on('ur.idUser', '=', 'pr.idPerson')
                    ->join(array('bts_user', 'ud'), 'LEFT')->on('m.toIdUser', '=', 'ud.idUser')
                    ->join(array('bts_person', 'pd'), 'LEFT')->on('ud.idUser', '=', 'pd.idPerson')
                    ->join(array('bts_city', 'c'), 'LEFT')->on('m.toIdCity', '=', 'c.idCity')
                    ->join(array('bts_office', 'o'), 'LEFT')->on('m.toIdOffice', '=', 'o.idOffice')
                    ->where(DB::expr('UNIX_TIMESTAMP(m.creationDate)'), '>', (int) $d_creation_date)
                    ->where('m.creationDate', '>', DB::expr($d_creation_dateCant));
            if ($idUser != '') {
                $selectMessages->and_where('m.userCreate', '=', $idUser);
            }
            $selectMessages->order_by('m.creationDate', 'ASC');
            // ->limit('3');
            $aResponse['data'] = $selectMessages->execute()->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_getListCantMessages() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            $array_cols = array(
                'ID' => "u.idUser",
                'user' => "p.fullName",
                'numberMessages' => "COUNT(m.userCreate)"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'bts_messages m
                            INNER JOIN bts_user u ON m.userCreate = u.idUser
                            INNER JOIN bts_person p ON u.`idUser` = p.idPerson';
            $s_group = 'GROUP BY m.userCreate, u.idUser';

            $count = jqGridHelper::getCount($cols, $tables_join, 'TRUE', $where_search, $s_group);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, 'TRUE', $where_search, $sidx, $sord, $limit, $start, $s_group);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_getPreviousinfoMessages() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
        try {
            $oPostPage = Validation::factory($_POST)
                    ->rule('lastNotification', 'not_empty')
                    ->rule('n_page', 'not_empty');
            if (!$oPostPage->check()) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
                echo jQueryHelper::JSON_jQuery_encode($aResponse);
                die();
            } else {
                $iPage = $this->request->post('n_page');
                $i_last_notification = $this->request->post('lastNotification');
                $aResponse['data'] = UserHelper::fngetInfoMessages($iPage, $i_last_notification, null, $date_format, $time_format);
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getPreviousinfoMessagesUser() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
        try {
            $oPostPage = Validation::factory($_POST)
                    ->rule('lastNotification', 'not_empty')
                    ->rule('n_page', 'not_empty');
            if (!$oPostPage->check()) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
                echo jQueryHelper::JSON_jQuery_encode($aResponse);
                die();
            } else {
                $iPage = $this->request->post('n_page');
                $i_last_notification = $this->request->post('lastNotification');
                $IdUser = $this->request->post('idUser');
                $aResponse['data'] = UserHelper::fngetInfoMessages($iPage, $i_last_notification, $IdUser, $date_format, $time_format);
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getLastNotificationUser() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $oPostPage = Validation::factory($_POST)
                    ->rule('idUser', 'not_empty');
            if (!$oPostPage->check()) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = 'ERROR -' . __('Empty Data Post');
                echo jQueryHelper::JSON_jQuery_encode($aResponse);
                die();
            } else {
                $IdUser = $this->request->post('idUser');
                $aResponse['data'] = UserHelper::fnGetLastDateNotificationMessage($IdUser);
            }
        } catch (Exception $exc) {
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_getInfoUser() {
        $idUser = $this->request->post('idUser');
        $aResponse = $this->json_array_return;

        try {
            $this->auto_render = FALSE;
            $sql = array('u.idUser',
                'u.idGroup', 'u.idOffice', 'u.userName', 'u.imgProfile', 'p.sex', 'p.fullName', array('o.name', 'officeName'), array('g.name', 'groupName'), 'p.email', 'p.phone1', 'p.cellPhone'
            );
            $sqldata = DB::select_array($sql)
                            ->from(array('bts_user', 'u'))
                            ->join(array('bts_person', 'p'))->on('u.idUser', '=', 'p.idPerson')
                            ->join(array('bts_office', 'o'))->on('u.idOffice', '=', 'o.idOffice')
                            ->join(array('bts_group', 'g'))->on('u.idGroup', '=', 'g.idGroup')
                            ->where('u.iduser', '=', $idUser)
                            ->as_object()
                            ->execute()->current();

            $aResponse['data'] = $sqldata;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

}