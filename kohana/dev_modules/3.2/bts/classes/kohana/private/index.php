<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Index extends Kohana_Private_Admin {

    function __construct(Request $request, Response $response) {
        parent::__construct($request, $response);
    }

    public function action_index() {
        $this->template->content = new View('private/index');
    }

    public function action_aboutUs() {
        $v = new View('public/about');
        $this->template->content = $v;
        $this->template->content->hola = "Hola";
    }
}
?>
