<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Terminal extends Kohana_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('terminal', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('terminal', self::FILE_TYPE_JS));
        $terminal_view = new View('private/terminal');
        $oOffice = new Model_Office();
        $aOffice = $oOffice->select('idOffice', 'name')->where('state', '=', self::STATUS_ACTIVE)->order_by('name')
                ->find_all();
        $terminal_view->aOffice = $aOffice;
        $this->template->content = $terminal_view;
    }

    public function action_listAllBrowsers() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $db = DB::query(Database::SELECT, "SHOW COLUMNS FROM `bts_terminal` LIKE 'browser'")->execute()->as_array('Field', 'Type');
            foreach ($db as $k => $enum) {
                $enum = str_replace("'", "", str_replace(")", "", str_replace("enum(", "", $enum)));
                $aResponse['data'] = array_map(create_function('$value', 'return array("id"=>$value,"value"=>$value);'), explode(',', $enum));
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_listAllOS() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $db = DB::select(array('printos.idPrintOS', 'id')
                                    , array('printos.name', 'value'))
                            ->from(array('bts_printos', 'printos'))
                            ->order_by('printos.name', 'asc')
                            ->execute()->as_array();
            $aResponse['data'] = $db;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_createTerminal() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateTerminal());
    }

    public function action_updateTerminal() {
        $this->auto_render = FALSE;
        echo json_encode($this->createUpdateTerminal());
    }

    public function createUpdateTerminal() {
        $aResponse = $this->json_array_return;
        try {
            $terminal_id = $this->request->post('terminal_id');
            $terminal_office = $this->request->post('terminal_office');
            $terminal_browser = $this->request->post('terminal_browser');
            $terminal_os = $this->request->post('terminal_os');
            $terminal_printer = $this->request->post('terminal_printer');
            $print_url = $this->request->post('print_url');
            if ($terminal_id == NULL) {
                $terminal = new Model_Terminal();
            } else {
                $terminal = new Model_Terminal($terminal_id);
            }
            $terminal->idOffice = $terminal_office;
            $terminal->browser = $terminal_browser;
            $terminal->idPrintOS = $terminal_os;
            $terminal->idPrinter = $terminal_printer;
            $terminal->state = self::STATUS_ACTIVE;
            if ($terminal_id == NULL) {
                $terminal->save();
            } else {
                $terminal->update();
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        return $aResponse;
    }

    public function action_listTerminals() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);

            $array_cols = array(
                'id' => "t.`idTerminal`",
                'office' => "o.`name`",
                'so' => "pos.`name`",
                'brower' => "t.`browser`",
                'printer' => "CONCAT(pb.`name`,' ',pm.`name`)"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'bts_terminal t ' .
                    'INNER JOIN bts_Office o ON o.`idOffice` = t.`idOffice` ' .
                    'INNER JOIN bts_printos pos ON t.`idPrintOS` = pos.`idPrintOS` ' .
                    'INNER JOIN bts_printer p ON p.`idPrinter` = t.`idPrinter` ' .
                    'INNER JOIN `bts_printmodel` pm ON pm.`idPrintModel` = p.`idPrintModel` ' .
                    'INNER JOIN `bts_printbrand` pb ON pb.`idPrintBrand` = pm.`idPrintBrand`  ';

            $where_conditions = "t.state = " . self::STATUS_ACTIVE . " ";
            if (isset($_REQUEST['selectOffice'])) {
                $where_conditions .= "AND o.idOffice='" . $_REQUEST['selectOffice'] . "'";
            }
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}