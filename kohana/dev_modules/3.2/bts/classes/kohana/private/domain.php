<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Domain extends Kohana_Private_Admin {
    
    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('domain', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('domain', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $domain_view = new View('private/domain');
        $domain = ORM::factory('Domain')->find();
        $domain_view->domainName = $domain->domainName;
        $domain_view->skin = $domain->skin;
        $domain_view->siteName = $domain->siteName;
        $domain_view->serverNameIP = $domain->serverNameIP;
        $domain_view->dbPort = $domain->dbPort;
        $domain_view->dbName = $domain->dbName;
        $domain_view->dbUser = $domain->dbUser;
        $domain_view->dbPassword = $domain->dbPassword;
        $this->template->content = $domain_view;
    }
}
