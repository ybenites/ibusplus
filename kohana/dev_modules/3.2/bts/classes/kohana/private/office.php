<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Office extends Kohana_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('office', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('office', self::FILE_TYPE_JS));
        $office_view = new View('private/office');
        $office_view->user_rootOption = $this->getSessionParameter(self::USER_ROOT_OPTION);
        $this->template->content = $office_view;
    }

    public function action_createOrUpdateOffice() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {

            $office_name = $this->request->post('office_name');
            $office_city = $this->request->post('office_city');
            $office_address = $this->request->post('office_address');
            $office_phone = $this->request->post('office_phone');
            $office_id = $this->request->post('office_id');
            $root_option = $this->request->post('rootOption');

            $tmp = DB::select(db::expr('COUNT(office.idOffice) AS c'))->from(array('bts_office','office'))->where('office.name', 'LIKE', $office_name)->and_where('office.status', '=', self::STATUS_ACTIVE);
            if ($office_id == NULL) {
                $office = new Model_Office();
            } else {
                $office = new Model_Office($office_id);
                $tmp = $tmp->and_where('office.idOffice', "<>", $office_id);
            }
            $tmp = $tmp->as_object()->execute()->current();

            if ($tmp->c > 0) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __("La oficina ya existe");
            } else {
                $office->name = trim($office_name);
                $office->idCity = $office_city;
                $office->address = trim($office_address);
                $office->phone = trim($office_phone);
                $office->status = self::STATUS_ACTIVE;
                if ($root_option != NULL) {
                    $office->rootOption = $root_option;
                }
                if ($office_id == NULL) {
                    $office->save();
                } else {
                    $office->update();
                }
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listOffices() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "o.idOffice",
                'cDate' => "o.`creationDate`",
                'lcDate' => "o.`lastChangeDate`",
                'name' => "o.name",
                'address' => "o.address",
                'phone' => "o.phone",
                'city' => "IF(c.aliasName IS NULL,c.name,c.aliasName)",
                'rootOption' => "o.rootOption",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(o.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(o.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_office` o ' .
                    'INNER JOIN bts_city c ON c.`idCity` = o.`idCity` ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = o.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = o.`userLastChange` ';

            $where_conditions = " o.`status` = " . self::STATUS_ACTIVE;
            if (!(bool) $this->getSessionParameter(self::USER_ROOT_OPTION)) {
                $where_conditions .=" AND o.rootOption IN (" . $this->getSessionParameter(self::USER_ROOT_OPTION) . ") ";
            }

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_removeOffice() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $office_id = $this->request->post('id');
            $office = ORM::factory('Office', $office_id);
            $office->status = self::STATUS_DEACTIVE;
            $office->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getOfficeById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $office_id = $this->request->post('id');
            $office = DB::select('office.idOffice', 'office.name', 'office.address', 'office.phone', 'office.idCity', 'office.rootOption')->from(array('bts_office','office'))->where('office.idOffice', '=', $office_id)->execute()->as_array();
            $aResponse['data'] = $office[0];
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}