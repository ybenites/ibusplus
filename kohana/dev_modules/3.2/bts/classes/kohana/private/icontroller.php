<?php

defined('SYSPATH') or die('No direct script access.');

class Kohana_Private_Icontroller extends Controller_Template implements Kohana_Uconstants {

    /**
     * Global Variables 
     */
    public $styles = array();
    public $scripts = array();
    public $json_array_return = array(
        'code' => self::CODE_SUCCESS,
        'msg' => 'OK'
    );
    public $FILE_TYPE = array(
        'css' => self::FILE_TYPE_CSS,
        'js' => self::FILE_TYPE_JS
    );
    public $CRUD_OPTIONS =
            array(
        'READ' => self::CRUD_READ,
        'CREATE' => self::CRUD_CREATE,
        'UPDATE' => self::CRUD_UPDATE,
        'DELETE' => self::CRUD_DELETE
    );
    public $DATE_FORMAT = array(
        'US' => array(
	    'VALUE' => 'US',
            'JQUERY_DATE' => 'mm-dd-yy',
            'SQL_DATE' => '%m-%d-%Y',
            'SQL_TIME' => '%H:%i:%s',
            'PHP_DATE' => 'm-d-Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%m-%d-%Y %H:%i',
            'JQUERY_TIME_ENTRY_24_HORAS' => self::STATUS_ACTIVE,
            'PHP_TIME_INPUT' => 'H:i',
            'SQL_TIME_OUTPUT' => '%H:%i'
        ),
        'MX' => array(
	    'VALUE' => 'MX',
            'JQUERY_DATE' => 'dd/mm/yy',
            'SQL_DATE' => '%d/%m/%Y',
            'SQL_TIME' => '%h:%i %p',
            'PHP_DATE' => 'd/m/Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%d/%m/%Y %h:%i %p',
            'JQUERY_TIME_ENTRY_24_HORAS' => self::STATUS_ACTIVE,
            'PHP_TIME_INPUT' => 'h:i A',
            'SQL_TIME_OUTPUT' => '%h:%i %p'
        ),
        'PE' => array(
	    'VALUE' => 'PE',
            'JQUERY_DATE' => 'dd/mm/yy',
            'SQL_DATE' => '%d/%m/%Y',
            'SQL_TIME' => '%h:%i %p',
            'PHP_DATE' => 'd/m/Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%d/%m/%Y %h:%i %p',
            'JQUERY_TIME_ENTRY_24_HORAS' => self::STATUS_DEACTIVE,
            'PHP_TIME_INPUT' => 'h:i A',
            'SQL_TIME_OUTPUT' => '%h:%i %p'
        ),
    );
    public $REGISTER_CONFIGURATION = ARRAY(
        self::REG_SERVER_SITE_NAME => array(
            'display' => 'Nombre Sitio',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_SITE_NAME
        ),
        self::REG_SERVER_DIR_DB => array(
            'display' => 'Dir. Server DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_DIR_DB
        ),
        self::REG_SERVER_PORT => array(
            'display' => 'Puerto DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_PORT
        ),
        self::REG_SERVER_DB_NAME => array(
            'display' => 'Nombre DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_DB_NAME
        ),
        self::REG_SERVER_DB_USER => array(
            'display' => 'Usuario DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_DB_USER
        ),
        self::REG_SERVER_DB_PASSWORD => array(
            'display' => 'Contraseña DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_DB_PASSWORD
        ),
        self::REG_SERVER_SKIN => array(
            'display' => 'Skin Web',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_SKIN
        ),
        self::REG_SERVER_WEB_SITE => array(
            'display' => 'Sitio Web',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_WEB_SITE
        ),
        self::REG_SERVER_TIME_ZONE => array(
            'display' => 'Zona Tiempo',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_TIME_ZONE
        ),
        self::REG_SERVER_DEFAULT_INDEX => array(
            'display' => 'Indice Defecto',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_DEFAULT_INDEX
        ),
        self::REG_SERVER_EMAIL => array(
            'display' => 'Email Servidor',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_EMAIL
        ),
        self::REG_SERVER_CONTACT_EMAIL => array(
            'display' => 'Email Contacto',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_CONTACT_EMAIL
        ),
        self::REG_SERVER_CONTACT_COMPANY => array(
            'display' => 'Compañia Contacto',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_CONTACT_COMPANY
        ),
        self::REG_SERVER_CONTACT_PHONE => array(
            'display' => 'Telefono Contacto',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_CONTACT_PHONE
        ),
        self::REG_SERVER_CONTACT_ADDRESS => array(
            'display' => 'Direccion Contacto',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_SERVER_CONTACT_ADDRESS
        ),
        self::REG_SERVER_SESION_TIME => array(
            'display' => 'Tiempo Sesion',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_SESION_TIME
        ),
        self::REG_SERVER_SESION_DB_TIME => array(
            'display' => 'Tiempo Sesion DB',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_SERVER_SESION_DB_TIME
        ),
        self::REG_PAYPAL_USER_NAME => array(
            'display' => 'Usuario Paypal',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_PAYPAL_USER_NAME
        ),
        self::REG_PAYPAL_PASSW0RD => array(
            'display' => 'Contraseña Paypal',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_PAYPAL_PASSW0RD
        ),
        self::REG_PAYPAL_SIGNATURE => array(
            'display' => 'Firma Paypal',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_PAYPAL_SIGNATURE
        ),
        self::REG_PAYPAL_MERCHANT_EMAIL => array(
            'display' => 'Email Com. Paypal',
            'rootOption' => self::STATUS_DEACTIVE,
            'name' => self::REG_PAYPAL_MERCHANT_EMAIL
        ),
        self::REG_ENVIRONMENT => array(
            'display' => 'Entorno de Desarrollo',
            'rootOption' => self::STATUS_ACTIVE,
            'name' => self::REG_ENVIRONMENT
        )
    );
    public $a_GLOBAL_ACTIONS = array(
        self::GLOBAL_ACTION_OK => array(
            'display' => 'Aceptar'
            , 'value' => self::GLOBAL_ACTION_OK
            , 'short' => 'OK'
        ),
        self::GLOBAL_ACTION_NEW => array(
            'display' => 'Nuevo'
            , 'value' => self::GLOBAL_ACTION_NEW
            , 'short' => 'NEW'
        )
        , self::GLOBAL_ACTION_EDIT => array(
            'display' => 'Editar'
            , 'value' => self::GLOBAL_ACTION_EDIT
            , 'short' => 'EDT'
        )
        , self::GLOBAL_ACTION_SAVE => array(
            'display' => 'Guardar'
            , 'value' => self::GLOBAL_ACTION_SAVE
            , 'short' => 'SAV'
        )
        , self::GLOBAL_ACTION_DELETE => array(
            'display' => 'Eliminar'
            , 'value' => self::GLOBAL_ACTION_DELETE
            , 'short' => 'DEL'
        )
        , self::GLOBAL_ACTION_SEARCH => array(
            'display' => 'Buscar'
            , 'value' => self::GLOBAL_ACTION_SEARCH
            , 'short' => 'SRH'
        )
        , self::GLOBAL_ACTION_SELECT => array(
            'display' => 'Seleccionar'
            , 'value' => self::GLOBAL_ACTION_SELECT
            , 'short' => 'SEL'
        )
        , self::GLOBAL_ACTION_CLEAR => array(
            'display' => 'Limpiar'
            , 'value' => self::GLOBAL_ACTION_CLEAR
            , 'short' => 'CLR'
        )
        , self::GLOBAL_ACTION_COMMENT => array(
            'display' => 'Comentario'
            , 'value' => self::GLOBAL_ACTION_COMMENT
            , 'short' => 'CMM'
        )
        , self::GLOBAL_ACTION_CANCEL => array(
            'display' => 'Cancelar'
            , 'value' => self::GLOBAL_ACTION_CANCEL
            , 'short' => 'CAN'
        )
    );
    public $READ_ALIAS = array('index', 'get', 'list');
    public $UPDATE_ALIAS = array('update', 'change', 'find');
    public $CREATE_ALIAS = array('create', 'save');
    public $DELETE_ALIAS = array('remove', 'delete', 'cleaning');

    public function fnGetCRUDGeneralOption($action_name) {
        $action_name = strtolower($action_name);
        foreach ($this->READ_ALIAS as $value)
            if (StringHelper::stringStartsWith($action_name, $value)) {
                return self::CRUD_READ;
            }
        foreach ($this->UPDATE_ALIAS as $value)
            if (StringHelper::stringStartsWith($action_name, $value)) {
                $this->fncleaningFragmentCache();
                return self::CRUD_UPDATE;
            }
        foreach ($this->CREATE_ALIAS as $value)
            if (StringHelper::stringStartsWith($action_name, $value)) {                
                $this->fncleaningFragmentCache();
                return self::CRUD_CREATE;
            }
        foreach ($this->DELETE_ALIAS as $value)
            if (StringHelper::stringStartsWith($action_name, $value)) {
                $this->fncleaningFragmentCache();
                return self::CRUD_DELETE;
            }
        return self::CRUD_READ;
    }
   public $support_email_contacts = array(
                    'log' => 'log@ibusplus.com',
                    'hugo.casanova' => 'hugo.casanova@ibusplus.com',
                    'wcumpa' => 'walter.cumpa@ibusplus.com',                    
                    'ybenites' => 'yime.benites@ibusplus.com',
                    'hlara' => 'hamilton.lara@ibusplus.com',                    
                    'haguirre' =>'harby.aguirre@ibusplus.com',
                    'gnurenia' =>'gabriela.nurenia@ibusplus.com'
                );
    /*
     * COMMON FUNCTIONS
     */

    public function getConfigFileName() {
        return SITE_DOMAIN . '_db';
    }

    public function getSystemSkin() {
        return db_config::$db_conection_config[SITE_DOMAIN]['skin'];
    }

    public function getSystemSiteName() {
        return db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
    }

    public function getSystemWebSiteURL() {
        return db_config::$db_conection_config[SITE_DOMAIN]['website'];
    }

    public function getSystemTimeZone() {
        return db_config::$db_conection_config[SITE_DOMAIN]['timeZone'];
    }

    public function getSystemDefaultIndex() {
        return db_config::$db_conection_config[SITE_DOMAIN]['defaultIndex'];
    }

    public function getSystemEmail() {
        return db_config::$db_conection_config[SITE_DOMAIN]['email'];
    }

    public function getSystemContactEmail() {
        return db_config::$db_conection_config[SITE_DOMAIN]['contactEmail'];
    }

    public function getSystemContactCompanyName() {
        return db_config::$db_conection_config[SITE_DOMAIN]['contactCompany'];
    }

    public function getSystemContactPhone() {
        return db_config::$db_conection_config[SITE_DOMAIN]['contactPhone'];
    }

    public function getSystemContactAddress() {
        return db_config::$db_conection_config[SITE_DOMAIN]['contactAddress'];
    }

    public function getSystemEnviroment() {
        return db_config::$db_conection_config[SITE_DOMAIN]['environment'];
    }

    public function getSystemSessionLifeTimet() {
        return db_config::$db_conection_config[SITE_DOMAIN]['sessionLifeTime'];
    }

    /**
     * ERROR HANDLING
     */
    public function errorHandling($exc, $extraParams = NULL) {
        $remove = array("\n", "\r\n", "\n\r", "\r");
        $params = str_replace($remove, '', Debug::dump($_REQUEST));
        if ($extraParams != null) {
            $remove = array("\n", "\r\n", "\n\r", "\r");
            $extraParams = str_replace($remove, '', Debug::dump($extraParams));
        }
        $error = '';
        if ($exc->getCode() != self::CODE_SUCCESS) {
            if (Kohana::$environment == Kohana::DEVELOPMENT) {
                if ($exc->getCode() == '1062') {
                   $error['eText'] = ("Se ha violado una restricción de base de datos UNIQUE.");
                } elseif ($exc->getCode() == '99999') {
                       $error['eText'] = $exc->getMessage();
                } elseif ($exc->getCode() == '1451') {
                     $error['eText'] = __("No es posible eliminar esta información. Ya está siendo utilizada en el sistema");                
                } elseif ($exc->getCode() == '1406') {
                    $error = __("El valor que intenta ingresar es demasiado largo. Intente un valor mas corto.");     
                } elseif ($exc->getCode() == self::CODE_NO_AUTHORIZED) {            
                        $error['eText'] = __("Usted no tiene acceso a esta opción");            
                } else {
                        $error['eCode'] = $exc->getCode();
                        $error['eEnviroment'] = Kohana::$environment;
                        $error['eLine'] = $exc->getLine();
                        $error['eFile'] = $exc->getFile();
                        $error['eText'] = $exc->getMessage();
                        $error['eTrace'] = $exc->getTraceAsString();
                        $error['rParams'] = $extraParams;
                }
            } else {
                $errorLog = Log::ERROR;
                $codeHumanError = $exc->getCode();
                if (isset(Kohana_Exception::$php_errors[$codeHumanError])) {
                    $codeHumanError = Kohana_Exception::$php_errors[$codeHumanError];
                    if ($codeHumanError == 'Notice') {
                        $errorLog = 5;
                    } elseif ($codeHumanError == 'Fatal Error') {
                        $errorLog = 3;
                    } elseif ($codeHumanError == 'User Error') {
                        $errorLog = 3;
                    } elseif ($codeHumanError == 'Parse Error') {
                        $errorLog = 3;
                    } elseif ($codeHumanError == 'Warning') {
                        $errorLog = 4;
                    } elseif ($codeHumanError == 'User Warning') {
                        $errorLog = 4;
                    } elseif ($codeHumanError == 'Strict') {
                        $errorLog = 3;
                    } elseif ($codeHumanError == 'Alert') {
                        $errorLog = 1;
                    } elseif ($codeHumanError == 'Recoverable Error') {
                        $errorLog = 3;
                    } else {
                        $errorLog = Log::ERROR;
                    }
                } else {
                    $errorLog = Log::ERROR;
                }
                if ($exc->getCode() == '1062') {
                    $error = __("La Serie y Correlativo que se ha ingresado ya existe.");
                } elseif ($exc->getCode() == '99999') {
                    $error = $exc->getMessage();
                } elseif ($exc->getCode() == '1451') {
                    $error = __("Existen Valores ingresados que no pueden ser borrados.");                
                } elseif ($exc->getCode() == '1406') {
                    $error = __("El valor que intenta ingresar es demasiado largo. Intente un valor mas corto.");    
                } else {
                    $code = strtoupper(base_convert($this->getSessionParameter($this->getSessionParameter(self::USER_OFFICE_ID) . $this->getSessionParameter(self::USER_ID)) . rand() . (date('mdHis') . rand()), 10, 36));
                    $code = 'EH_' . $code;
                    $moreMessage = '<b>Nro Codigo:  ' . $code . '</b><br/><br/>';
                    $moreMessage .='<b>Error en: </b> ' . SITE_DOMAIN . '<br/><br/>';
                     if ($this->getSessionParameter('sessionKeyId') != NULL) {
                        $moreMessage .= '<b>Id Usuario:</b> ' . Session::instance('database')->get(self::USER_ID) . '<br/>';
                        $moreMessage .= '<b>Usuario:</b > ' . Session::instance('database')->get(self::USER_NAME) . '<br/>';
                        $moreMessage .= '<b>Nombre Completo: </b > ' . Session::instance('database')->get(self::USER_FULL_NAME) . '<br/>';
                        $moreMessage .= '<b>Id Oficina:</b> ' . Session::instance('database')->get(self::USER_OFFICE_ID) . '<br/>';
                        $moreMessage .= '<b>oficina:</b> ' . Session::instance('database')->get(self::USER_OFFICE_NAME) . '<br/>';
                        $moreMessage .= '<b>URI:</b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
                        $moreMessage .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
                        }
                        if (isset($_SERVER['REMOTE_ADDR'])) {
                            $moreMessage .= '<b>REMOTE_ADDR:</b> ' . $_SERVER['REMOTE_ADDR'] . '<br/>';
                        }
                        if (isset($_SERVER['HTTP_USER_AGENT'])) {
                            $moreMessage .= '<b>HTTP USER AGENT:</b> ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>';
                        }
                        if (isset($_SERVER['HTTP_REFERER'])) {
                            $moreMessage .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_REFERER'] . '<br/>';
                        }
                        if (isset($_SERVER['REDIRECT_URL'])) {
                            $moreMessage .= '<b>REDIRECT URL:</b> ' . $_SERVER['REDIRECT_URL'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_INFO'])) {
                            $moreMessage .= '<b>PATH INFO:</b> ' . $_SERVER['PATH_INFO'] . '<br/>';
                        }
                        if (isset($_SERVER['PATH_TRANSLATED'])) {
                            $moreMessage .= '<b>PATH TRANSLATED:</b> ' . $_SERVER['PATH_TRANSLATED'] . '<br/>';
                        }
                        if (isset($_SERVER['SCRIPT_FILENAME'])) {
                            $moreMessage .= '<b>SCRIPT FILENAMEL:</b> ' . $_SERVER['SCRIPT_FILENAME'] . '<br/>';
                        }
                        $moreMessage .= '<b>PARAMETROS: </b><br/><pre>' . $params . '</pre><br/><br/>';
                        $moreMessage .= '<b>Extra Params:</b><pre>' . $extraParams . '</pre><br/><br/>';
                 
                    $remove = array("\n", "\r\n", "\n\r", "\r");
                    $getMessage = str_replace($remove, '', strip_tags($exc->getMessage()));
                    $info = sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($exc), $exc->getCode(), '<div style="width: 1100px;word-wrap:break-word;">' . $moreMessage . $getMessage . '</div>', Debug::path($exc->getFile()), $exc->getLine());

                    Kohana::$log->add($errorLog
                            , ':info'
                            , array(
                        ':info' => $info
                    ));
                    $strace = $info . "\n--\n" . $exc->getTraceAsString();
                    Kohana::$log->add(Log::STRACE, $strace);
                    $errorHtml = '<b>Error:</b><br/>';
                    $errorHtml .= '<b>Codigo:</b>' . $code . '<br/><br/>';
                    $errorHtml .= '<b>Hora:</b>' . DateHelper::getFechaFormateadaActual() . '<br/><br/>';
                    $errorHtml .= '<b>Sistema:</b>' . $_SERVER['HTTP_HOST'] . '<br/><br/>';
                    $errorHtml .= '<b>Id Usuario:</b>' . $this->getSessionParameter(self::USER_ID) . '<br/><br/>';
                    $errorHtml .= '<b>Usuario:</b>' . $this->getSessionParameter(self::USER_NAME) . '<br/><br/>';                    
                $error['eCode'] = $code;
                $error['eLine'] = $exc->getLine();
                $error['eFile'] = $exc->getFile();
                $error['eEnviroment'] = Kohana::$environment;
                $error['eTime'] = DateHelper::getFechaFormateadaActual();
                $error['eDomain'] = SITE_DOMAIN;
                $error['eUserId'] = $this->getSessionParameter(self::USER_ID);
                $error['eUserName'] = $this->getSessionParameter(self::USER_NAME);
                $error['eText'] = __("Se produjo un error inesperado en el sistema. Por favor contactarse con el area de soporte para solucionar el problema. Codigo de Error: ") . $code;                
                $error['eTextMsg'] = $exc->getMessage();
                $error['eTrace'] = $exc->getTraceAsString();
                $error['rParams'] = $extraParams;
                }
            }
        } else {
            $error = $exc->getMessage();
        }
        return $error;
    }

}
