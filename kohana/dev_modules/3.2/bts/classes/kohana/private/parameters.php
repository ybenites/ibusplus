<?php
defined('SYSPATH') or die('No direct script access.');
abstract class Kohana_Private_Parameters extends Kohana_Private_Admin{
    //put your code here
    public function action_index(){ 
        $this->mergeStyles(ConfigFiles::fnGetFiles('adminParameters', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('adminParameters', self::FILE_TYPE_JS));        
        
        $fileName=$_SERVER['SERVER_NAME'].'_db.php ';        
        $urlFile=APPPATH.'config';
        $file=$urlFile.DIRECTORY_SEPARATOR.$fileName; 

        $fichero_url = fopen (trim($file), "r+");
        $num=1;
        
        $dataRegister=$this->REGISTER_CONFIGURATION;                
        $isRoot=$this->getSessionParameter(self::USER_ROOT_OPTION); 
        
        if(!$isRoot){
            foreach ($dataRegister as $key => $v) {
                if($v['rootOption']){                    
                    unset ($dataRegister[$key]);
                }
            }
        }
        $dataCompare=array_keys($dataRegister);
        
//        $data=array();
        while (!feof($fichero_url))
        {    
            $pos=ftell($fichero_url);            
            $linea = fgets($fichero_url) ;                         
            foreach ($dataCompare as $value) {                
                if(strpos($linea,$value)!=false ){                                        
                    $quitData=array($value,',','=>',"'");                    
                    $newLinea=str_replace($quitData,'',$linea);                                      
//                    $data[$value]=array($pos,$value,$newLinea);
                    $dataRegister[$value]['value']=$newLinea;
                    $dataRegister[$value]['position']=$pos;
                }                
            }             
            $num++;            
        }                   
        fclose($fichero_url);          
        
//        $listDirectory=opendir($urlFile);
//        while ($archivo = readdir($listFile)){
//            if(trim($_SERVER['SERVER_NAME'].'_db.php ')==trim($archivo)){
//                echo "$archivo<br>";
//            }
//        }
//        closedir($listFile);       
        
        $parameters_view = new View('private/parameters');
        $parameters_view->dataRegisterTotal=$dataRegister;                
        $this->template->content = $parameters_view;
    }
    
    public function action_updateDataRegister(){       
        $this->auto_render=false;   
        $aResponse = $this->json_array_return;
        try {
            $valueRegister=$this->request->post('valueRegister');
            $datai=$this->request->post('datai');
            
            $fileName=$_SERVER['SERVER_NAME'].'_db.php ';        
            $urlFile=APPPATH.'config';
            $file=$urlFile.DIRECTORY_SEPARATOR.$fileName; 
            
            $archivo = file(trim($file));
            $en='';
            
            foreach ($archivo as $key => $linea) {
                if(strpos($linea,$datai)!=false){ 
                    $pos=strpos($linea,$datai);
                    for($a=1;$a<$pos;$a++){
                        $e=' ';
                        $en=$en.$e;
                    }                       
                    $archivo[$key]=$en."'".$datai."'"." => '".$valueRegister."',\n";                     
                    $cadena=file_put_contents(trim($file),$archivo);                    
                    $aResponse['data']='SE MODIFICO';
                }
            }                 
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        $this->fnResponseFormat($aResponse,'json');
    }
}

?>
