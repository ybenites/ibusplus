<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Welcome extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('welcome', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('welcome', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $browser_version = Browser::getBrowser();
        $default_welcome_view = new View('private/welcome');
        $default_welcome_view->user_full_name = $this->getSessionParameter(self::USER_FULL_NAME);
        $default_welcome_view->browser_name = $browser_version['browser_name'];
        $default_welcome_view->browser_version = $browser_version['browser_version'];
        $this->template->content = $default_welcome_view;
    }

    public function action_changePass() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $old_pass = $this->request->post('chpass_current');
            $new_pass = $this->request->post('chpass_new');
            $user = new Model_User($this->getSessionParameter(self::USER_ID));
            if ($user->password == md5($old_pass)) {
                $user->password = md5($new_pass);
                $user->update();
                $aResponse['msg'] = __('¡Su contraseña ha sido actualizada!');
            } else {
                throw new Exception(__('La contraseña que intenta cambiar es incorrecta', self::CODE_ERROR));
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_updateImageProfile() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $u_imgProfile = $this->request->post('img_p');
            $user = new Model_User($this->getSessionParameter(self::USER_ID));
            if ($user->imgProfile != NULL) {
                FileHelper::deleteFileOrFolderRecursive($user->imgProfile);
            }
            $source_dir = self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_128;
            $source_url = realpath(self::FOLDER_UPLOAD_TMP . DIRECTORY_SEPARATOR . $u_imgProfile);
            $destination_url = realpath(self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_128) . DIRECTORY_SEPARATOR . $u_imgProfile;
            $destination_url_prev = realpath(self::FOLDER_UPLOAD . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_PROFILE . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_64) . DIRECTORY_SEPARATOR . $u_imgProfile;
            copy($source_url, $destination_url);
            copy($source_url, $destination_url_prev);
            $img_prev = Kohana_Image::factory($destination_url_prev);
            $img_prev->resize(64, 64, Image::HEIGHT);
            $img_prev->save($destination_url_prev);
            $user->imgProfile = str_ireplace(DIRECTORY_SEPARATOR, '/', DIRECTORY_SEPARATOR . $source_dir . DIRECTORY_SEPARATOR . $u_imgProfile);
            $user->update();
            FileHelper::deleteFileOrFolderRecursive($source_url);
            
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

}