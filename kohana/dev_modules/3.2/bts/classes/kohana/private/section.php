<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Section extends Kohana_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('section', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('section', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $section_vew = new View('private/section_jqgrid');
        $this->template->content = $section_vew;
    }

    public function action_getFilesByPage() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idPage = $this->request->post('id');
            $sql = "(SELECT `file`.`idFile` FROM `bts_section` as section 
                    INNER JOIN `bts_page` as page ON `page`.`idPage` = `section`.`idPage`
                    INNER JOIN `bts_file` as file ON `file`.`idFile` = `section`.`idFile` 
                    WHERE `section`.`idPage` = '[PAGE_ID]')";
            $sql = str_replace('[PAGE_ID]', $idPage, $sql);
            $orphan_files = DB::select(array('file.idFile', 'idf'), DB::expr("CONCAT(file.fileName,'.',file.fileType) as fname"), array('file.fileType', 'ft'))
                            ->from(array('bts_file','file'))
                            ->where('file.idFile', 'NOT IN ', DB::expr($sql))
                            ->order_by('file.fileType')
                            ->order_by('file.fileName')
                            ->execute()->as_array();

            $section_files = DB::select(array('file.idFile', 'idf'), DB::expr("CONCAT(file.fileName,'.',file.fileType) as fname"), array('file.fileType', 'ft'))
                            ->from(array('bts_section','section'))
                            ->join(array('bts_page','page'))->on('page.idPage', '=', 'section.idPage')
                            ->join(array('bts_file','file'))->on('file.idFile', '=', 'section.idFile')
                            ->where('page.idPage', '=', $idPage)
                            ->order_by('file.fileType')
                            ->order_by('section.fileOrder')
                            ->execute()->as_array();

            $aResponse ['data'] = array(
                'oFiles' => $orphan_files,
                'sFiles' => $section_files
            );
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_updateSection() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idPage = $this->request->post('pid');
            $aNewElements = $this->request->post('ne');
            $aTrashElements = $this->request->post('te');
            $css_order = $this->request->post('csso');
            $js_order = $this->request->post('jso');
            Database::instance()->begin();
            if ($aNewElements != NULL) {
                foreach ($aNewElements as $idFile) {
                    $section = new Model_Section();
                    $section->idFile = $idFile;
                    $section->idPage = $idPage;
                    $section->save();
                }
            }

            if ($aTrashElements != NULL) {
                $delete_query = "DELETE FROM bts_section WHERE bts_section.idPage = :PAGE AND bts_section.idFile IN (" . join(',', $aTrashElements) . ")";
                DB::query(Database::DELETE, $delete_query)->param(":PAGE", $idPage)->execute();
            }
            $aIdFiles = array();
            if ($css_order != NULL) {
                array_push($aIdFiles, $css_order);
            }
            if ($js_order != NULL) {
                array_push($aIdFiles, $js_order);
            }

            foreach ($aIdFiles as $aElement) {
                $order = 1;
                foreach ($aElement as $idFile) {
                    $update_query = "UPDATE bts_section section SET section.fileOrder = $order  WHERE section.idPage = '$idPage' AND section.idFile = '$idFile'";
                    DB::query(Database::UPDATE, $update_query)->execute();
                    $order++;
                }
            }

            Database::instance()->commit();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

//    public function action_getFiles() {
//        try {
//            $this->auto_render = FALSE;
//            //NRO PÁGINA
//            $page = $_REQUEST['page'];
//            //FILAS POR PÁGINA
//            $limit = $_REQUEST['rows'];
//            //CRITERIO DE ORDENAMIENTO
//            $sidx = $_REQUEST['sidx'];
//            //DIRECCIÓN DEL ORDENAMIENTO
//            $sord = $_REQUEST['sord'];
//            //MEDIAFILE_TYPE
//            $mediaFileType = $_REQUEST['type'];
//            //DOMAIN ID
//            $idDomain = 0;
//            if (isset($_REQUEST['idDomain']))
//                $idDomain = $_REQUEST['idDomain'];
//            //PAGE ID
//            $idPHPPage = 0;
//            if (isset($_REQUEST['idPHPPage']))
//                $idPHPPage = $_REQUEST['idPHPPage'];
//
//            $direction_inner_join = "";
//            if (isset($_REQUEST['dir']))
//                if ($_REQUEST['dir'] == 'l')
//                    $direction_inner_join = 'LEFT';
//                else
//                    $direction_inner_join = 'RIGHT';
//            if (!$sidx)
//                $sidx = 1;
//
//            $where_search = "";
//            //VERIFICACIÓN SI ES UNA BÚSQUEDA
//            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
//            //SI ES UNA BÚSQUEDA SE ARMA EL SEARCH WHERE
//            //COLUMNAS con su respectivo ALIAS
//            $array_cols = array(
//                'mediafile.idMediaFile' => 'mediaFile_id',
//                'mediafile.mediaFileType' => 'mediaFile_type',
//                'mediafile.fileName' => 'mediaFile_name',
//                'mediafile.customPathActive' => 'mediaFile_customPathActive'
//            );
//
//            if ($_REQUEST['dir'] == 'r') {
//                $array_cols['k.order'] = 'mediaFile_order';
//            }
//
//            //COLUMNAS DE BUSQUEDA SIMPLE
//            $searchable_cols = array_keys($array_cols);
//
//            if ($searchOn == 'true') {
//                if (isset($_REQUEST['filters'])) {
//                    //BÚSQUEDA AVANZADA
//                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
//                    $where_search = jqGridHelper::constructWhere($searchstr);
//                } else {
//                    //BÚSQUEDA SIMPLE POR COLUMNA
//                    foreach ($_REQUEST as $k => $v) {
//
//                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
//                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
//                        }
//                    }
//                }
//            }
//
//
//            $cols = jqGridHelper::arrayColsToSQL($array_cols);
//            //RELACIONES
//            $tables_join = 'mediaFile ' .
//                    $direction_inner_join . ' JOIN (' .
//                    'SELECT mediaFile.idMediaFile, mediafilephppage.order FROM mediaFile ' .
//                    'INNER JOIN domain ON mediaFile.idDomain = domain.idDomain ' .
//                    'INNER JOIN mediafilephppage ON mediafilephppage.idMediaFile = mediafile.idMediaFile ' .
//                    'INNER JOIN phppage ON phppage.idPHPPage = mediafilephppage.idPHPPage ' .
//                    "WHERE domain.idDomain = " . $idDomain . " AND phppage.idPHPPage = " . $idPHPPage . " AND mediaFile.mediaFileType = '" . $mediaFileType . "') AS k ON mediaFile.idMediaFile = k.idMediaFile";
//
//            //CONDICIONES WHERE
//            $aditional_condition = '';
//            if ($direction_inner_join == 'LEFT') {
//                $aditional_condition = ' AND k.idMediaFile IS NULL';
//            }
//            $where_conditions = "mediaFile.idDomain = " . $idDomain . " AND mediaFile.mediaFileType = '" . $mediaFileType . "'" . $aditional_condition;
//            //Calulamos el número de filas
//            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);
//            //Calulamos el offset para la consulta
//            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
//            //Generamos los resultados
//            $products = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
//            //IMPRIMIMOS EL RESULTADO EN JSON para el jqGrid
//            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $products, $array_cols, true), false);
//        } catch (Exception $exc) {
//            echo $this->errorHandling($exc);
//        }
//    }

    public function action_changeMediaFileOrder() {
        $this->auto_render = FALSE;
        try {
            $file_id = $_REQUEST['id'];
            $order = $_REQUEST['mediaFile_order'];
            $page_id = $_REQUEST['p'];

            $mfphp = ORM::factory('Mediafilephppage')
                    ->where('idMediaFile', '=', $file_id)
                    ->and_where('idPHPPage', '=', $page_id)
                    ->find();
            $mfphp->order = $order;
            $mfphp->save();
            echo '1|OK';
        } catch (Exception $exc) {
            echo '0|' . $this->errorHandling($exc);
        }
    }

}
?>

