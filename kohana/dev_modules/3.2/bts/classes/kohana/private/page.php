<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Private_Page extends Kohana_Private_Admin {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('page', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('page', self::FILE_TYPE_JS));
        $page_view = new View('private/page');
        $this->template->content = $page_view;
    }

    public function saveUpdatePage(&$aResponse) {
        $idPage = $this->request->post('pageId');
        $idParent = $this->request->post('parentId');
        $pageName = trim($this->request->post('pageName'));
        $tmp = DB::select(db::expr('COUNT(page.idPage) AS c'))->from(array('bts_page','page'))->where('page.pageName', 'LIKE', $pageName)->and_where('page.status', '=', self::STATUS_ACTIVE);
        if ($idPage == NULL) {
            $page = new Model_Page();
        } else {
            $page = new Model_Page($idPage);
            $tmp = $tmp->and_where('page.idPage', "<>", $idPage);
        }
        $tmp = $tmp->as_object()->execute()->current();
        if ($tmp->c > 0) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = __("La página ya existe en alguno de los niveles");
        } else {
            $page->pageName = $pageName;
            if($idParent!=null){
                $page->idSuperPage = $idParent;
            }
            if ($idPage == NULL) {
                $page->save();
            } else {
                $page->update();
            }
        }
    }

    public function action_savePage() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $this->saveUpdatePage($aResponse);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_getPageById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_Page($this->request->post('id'));
            $aResponse['data'] = $file->as_array();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_removePage() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $file = new Model_Page($this->request->post('id'));
            $file->status = self::STATUS_DEACTIVE;
            $file->save();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['data'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function action_listPages_() {
        $this->auto_render = FALSE;
        try {
            $page = $_REQUEST['page'];
            $limit = $_REQUEST['rows'];
            $sidx = $_REQUEST['sidx'];
            $sord = $_REQUEST['sord'];
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $array_cols = array(
                'id' => "p.idPage",
                'cDate' => "p.`creationDate`",
                'lcDate' => "p.`lastChangeDate`",
                'pageName' => "p.pageName",
                'uc' => "pc.fullName",
                'regDate' => "DATE_FORMAT(p.`creationDate`,'$date_format $time_format')",
                'ulc' => "plc.fullName",
                'lastDate' => "DATE_FORMAT(p.`lastChangeDate`,'$date_format $time_format')",
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = '`bts_page` p ' .
                    'INNER JOIN bts_person pc ON pc.`idPerson` = p.`userCreate` ' .
                    'INNER JOIN bts_person plc ON plc.`idPerson` = p.`userLastChange` ';

            $where_conditions = " p.`status` = " . self::STATUS_ACTIVE;

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $result = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_listPagesXML() {
        $this->auto_render = FALSE;
        $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
        try {
            $node = (integer) $_REQUEST['nodeid'];
            $n_lvl = (integer) $_REQUEST['n_level'];
            $SQL_LEAF_NODES = 'SELECT l.idSuperPage FROM bts_page AS p LEFT JOIN bts_page AS l ON p.idPage = l.idSuperPage WHERE l.idPage IS NOT NULL AND l.status=' . self::STATUS_ACTIVE . ' ORDER BY l.pageName';
            $results = DB::query(Database::SELECT, $SQL_LEAF_NODES)->execute()->as_array();
            $leaf_nodes = array();
            foreach ($results as $r) {
                $leaf_nodes[$r['idSuperPage']] = $r['idSuperPage'];
            }

            header("Content-type: text/xml;charset=utf-8");

            $et = ">";
            echo "<?xml version='1.0' encoding='utf-8'?$et\n";
            echo "<rows>";
            echo "<page>1</page>";
            echo "<total>1</total>";
            echo "<records>1</records>";
            if ($node > 0) {
                $wh = 'page.idSuperPage=' . $node . ' AND page.status=' . self::STATUS_ACTIVE;
                $n_lvl = $n_lvl + 1;
            } else {
                $wh = 'ISNULL(page.idSuperPage) AND page.status=' . self::STATUS_ACTIVE;
            }
            $NEXT_NODE =
                    "SELECT page.idPage, page.pageName, page.idSuperPage, page.`creationDate` as cDate ,page.`lastChangeDate` as lcDate" .
                    ", pc.fullName as uc " .
                    ", DATE_FORMAT(page.creationDate,'$date_format $time_format') as regDate " .
                    ", plc.fullName as ulc " .
                    ", DATE_FORMAT(page.lastChangeDate,'$date_format $time_format') as lastDate " .
                    "FROM bts_page page INNER JOIN bts_person pc ON pc.`idPerson` = page.`userCreate` INNER JOIN bts_person plc ON plc.`idPerson` = page.`userLastChange` " .
                    "WHERE " . $wh . " ORDER BY page.pageName";
            $results = DB::query(Database::SELECT, $NEXT_NODE)->execute()->as_array();
            foreach ($results as $row) {
                #VALIDACIONES
                if (!$row['idSuperPage'])
                    $valp = 'NULL';
                else
                    $valp = $row['idSuperPage'];
                if (array_search($row['idPage'], $leaf_nodes) > 0)
                    $leaf = 'false';
                else
                    $leaf = 'true';
                $ico_cell = ""; //htmlspecialchars(HTML::image('media/ico/' . $row['icon'], array('title' => $row['name'], 'alt' => $row['icon'], 'width' => '16px')));
                $edit_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-edit" data-ido="' . $row['idPage'] . '" title="' . __('Editar') . '" ></a>');
                $delete_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-trash" data-ido="' . $row['idPage'] . '" title="' . __("Eliminar") . '" ></a>');
                $submenu_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-new-subfile"  data-ido="' . $row['idPage'] . '" title="' . __("Nueva Página heredada") . '" ></a>');
                $php_action_option = ""; //htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-php-action" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Nueva Acción PHP") . '" ></a>');
                $order_option = ""; //htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-order" style="cursor: pointer;" rel="' . $row['idMenu'] . '" title="' . __("Ordenar Menú") . '" ></a>');
                $options = $edit_option . $delete_option . $submenu_option;


                echo "<row>";
                echo "<cell>" . $row['idPage'] . "</cell>";
                echo "<cell>" . $row['cDate'] . "</cell>";
                echo "<cell>" . $row['lcDate'] . "</cell>";
                echo "<cell>" . $row['pageName'] . "</cell>";
                echo "<cell>" . $row['uc'] . "</cell>";
                echo "<cell>" . $row['regDate'] . "</cell>";
                echo "<cell>" . $row['ulc'] . "</cell>";
                echo "<cell>" . $row['lastDate'] . "</cell>";
                echo "<cell>" . $options . "</cell>";
                echo "<cell>" . $n_lvl . "</cell>";
                echo "<cell><![CDATA[" . $valp . "]]></cell>";
                echo "<cell>" . $leaf . "</cell>";
                echo "<cell>false</cell>";
                echo "</row>";
            } echo "</rows>";
        } catch (Exception $exc) {
            echo $this->errorHandling($exc);
        }
    }

    public function action_listPages() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $chars_search = $this->request->post('term');
        $idSuperPage = $this->request->post('idsp');
        $type = $this->request->post('t');

        $cols = array(
            array('page.idPage', 'id'),
            DB::expr('page.pageName AS value')
        );
        if ($type != NULL) {
            if ($type == 'f') {
                $cols = array('page.*');
            }
        }
        try {
            $city = DB::select_array($cols)
                    ->from(array('bts_page','page'))
                    ->where('page.status', '=', self::STATUS_ACTIVE)
                    ->order_by('page.pageName');

            if ($chars_search != NULL) {
                $city->where('page.pageName', 'LIKE', '%' . $chars_search . '%');
            }

            if ($idSuperPage != NULL) {
                $city->where('page.idSuperPage', '=', $idSuperPage);
            }
            $aResponse['data'] = $city->execute()->as_array();
        } catch (Exception $e) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($e);
        }
        $this->fnResponseFormat($aResponse);
    }
}
