<?php
defined('SYSPATH') or die('No direct script access.');
abstract class Kohana_Private_Privatezone extends Kohana_Private_Icontroller {
    public $template = 'private/private_zone';
    private $imported;
    protected $imported_functions;
    
    public static $JQUERY_VERSION = '1.8.0';
    public static $JQUERY_UI_VERSION = '1.8.23';

    protected function imports($object) {        
        // the new object to import
        $new_import = new $object();
        // the name of the new object (class name)
        $import_name = get_class($new_import);
        // the new functions to import
        $import_functions = get_class_methods($new_import);

        // add the object to the registry
        array_push($this->imported, array($import_name, $new_import));

        // add teh methods to the registry
        foreach ($import_functions as $key => $function_name) {
            $this->imported_functions[$function_name] = &$new_import;
        }
    }

    public function __call($method, $args = array()) {
        if (array_key_exists($method, $this->imported_functions)) {
            return call_user_func_array(array($this->imported_functions[$method], $method), $args);
        }

        throw new Exception('Call to undefined method/class function: ' . $method);
    }

    function __construct(Request $request, Response $response) {
        parent::__construct($request, $response);
        $this->imported = array();
        $this->imported_functions = array();
        $aInterface = Kohana::$config->load('uInterface');
        foreach ($aInterface as $className) {
            $this->imports(get_class($className));
        }
        if (!$this->request->is_ajax()) {
        $this->loadClientLanguage(db_config::$db_conection_config[SITE_DOMAIN]['skin']);
        $a_jquery_variables = Kohana::$config->load('jquery_version');
            if (isset($a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_VERSION]))
                self::$JQUERY_VERSION = $a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_VERSION];
            if (isset($a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_UI_VERSION]))
                self::$JQUERY_UI_VERSION = $a_jquery_variables[Kohana::$environment][Kohana_Uconstants::VAR_JQUERY_UI_VERSION];          
       
    }
    }

    public function before() {        
        parent::before();
         if (!$this->request->is_ajax()) {
            $this->template->title = $this->getSystemSiteName();
            $this->template->skin = $this->getSystemSkin();
            $this->template->{Kohana_Uconstants::VAR_JQUERY_VERSION} = self::$JQUERY_VERSION;
            $this->template->{Kohana_Uconstants::VAR_JQUERY_UI_VERSION} = self::$JQUERY_UI_VERSION;
            $this->template->siteName = $this->getSystemSiteName();
            $this->template->meta_keywords = '';
            $this->template->meta_description = '';
            $this->template->meta_copyright = '';
            $this->template->header_css_class = '';
            $this->template->header_full_css_class = '';
            $this->template->content_style = '';
            $this->template->username = '';
            $this->template->CODE_SUCCESS = self::CODE_SUCCESS;
            $this->template->CODE_NO_AUTHORIZED = self::CODE_NO_AUTHORIZED;
            $this->template->CODE_ERROR = self::CODE_ERROR;
            $this->template->DOMAIN = SITE_DOMAIN;
            $this->template->TIMEOUT_MILISECONDS = (int) $this->getSystemSessionLifeTimet() * 1000 - self::USER_SESSION_SECOND_BEFORE_DISCONECT * 1000;
            $this->template->TIMEOUT_SECONDS = self::USER_SESSION_SECOND_BEFORE_DISCONECT;
            $this->template->content = '';
            $this->template->username = '';
            $SO_DATE_FORMAT = $this->getOptionValueFromDatabase(self::SO_DATE_FORMAT);
            $this->template->DF_SHOW24HOUR = $this->getDATE_FORMAT_FOR(self::DF_SHOW24HOUR,$SO_DATE_FORMAT->value);             
            $this->template->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE,$SO_DATE_FORMAT->value);
                $cacheInstance = Cache::instance('apc');
                $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'pz_css';
                $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'pz_js';
                $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
                $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
                if ($incacheObjectCss === null && $incacheObjectJs === null) {
                    $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('private_zone', self::FILE_TYPE_CSS));
                    $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('private_zone', self::FILE_TYPE_JS));
                    $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
                    $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
                }
                if (empty($this->styles)) {
                    $this->template->styles = $incacheObjectCss;
                }
                if (empty($this->scripts)) {
                    $this->template->scripts = $incacheObjectJs;
                }
            
    }
    }
     public function after() {
        parent::after();
         if (!$this->request->is_ajax()) {
        if ($this->isKohanaModuleActive('profilertoolbar') AND Kohana::$environment == Kohana::DEVELOPMENT) {
            ProfilerToolbar::firebug();
        }
         }
    }

    public function getRequestURI() {
        $directory = Request::initial()->directory();
        $controller = Request::initial()->controller();
        $action = Request::initial()->action();
        $CRUD_REQUEST = '/' . $directory . '/' . $controller . '/' . $this->fnGetCRUDGeneralOption($action);
        return $CRUD_REQUEST;
    }

    public function mergeStyles($new_styles_array) {
        $this->template->styles = array_merge($this->template->styles, $new_styles_array);
    }

    public function mergeScripts($new_scripts_array) {
        $this->template->scripts = array_merge($this->template->scripts, $new_scripts_array);
    }

    public function loadClientLanguage($skin) {
        I18n::lang(Cookie::get($skin . '_lang', NULL));
    }

    public function action_logout() {
        if ($this->createSessionEvent(self::EVENT_LOG_OUT)) {                        
            Session::instance('database')->destroy();
            Request::initial()->redirect('/public/login');            
        }
    }
    
    public function cleaningAllCache() {
        try {
            Cache::instance('apc')->delete_all();
            $website = Session::instance('database')->get('sessionKeyId');
            $cacheDir = "/$website/";
            CacheFile::deleteAllCache($cacheDir);
            apc_clear_cache();
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
    public function cleaningLanguages() {
        try {
           $translation_file = new LoadLanguage();
           $language_sufix_file_name = 'bts';
           $translation_file->loadData($language_sufix_file_name, 'es', 'es', true);
           $translation_file->loadData($language_sufix_file_name, 'es', 'en', true);        
           return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
    public function cleaningSessionsUsers() {
        try {
           DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessions`")->execute();
           DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessionlog`")->execute();
           return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
    public function fncleaningFragmentCache($header=false,$footer=false) {
        try {
            Fragment::delete($this->cacheSessionDynamicIndex, true);
             if ($header==true){
                $fragmentKeyHeader = Session::instance('database')->get('sessionKeyId') . Session::instance('database')->get('user_name') . 'admin_header';
                Fragment::delete($fragmentKeyHeader, true);
            }
             if ($footer==true){
                $fragmentKeyHeader = Session::instance('database')->get('sessionKeyId') . Session::instance('database')->get('user_name') . 'admin_footer';
                Fragment::delete($fragmentKeyHeader, true);
            }

            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function cleaningCache() {
        try {
            $website = Session::instance('database')->get('sessionKeyId');
            $user_name = Session::instance('database')->get('user_name');
            $cacheDir = "/$website/$user_name/";
            CacheFile::deleteAllCache($cacheDir);
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
    public function cleaningCacheApc($cacheKey = null) {
        try {
            $cacheInstance = Cache::instance('apc');
            $cacheKey = $this->getSessionParameter('sessionKeyId') . $cacheKey;
            $incacheObject = $cacheInstance->get($cacheKey);
            if ($incacheObject != null) {
                $cacheInstance->delete($incacheObject);
            } else {
                Cache::instance('apc')->delete_all();
            }
            $website = Session::instance('database')->get('sessionKeyId');
            $user_name = Session::instance('database')->get('user_name');
            $cacheDir = "/$website/$user_name/";
            CacheFile::deleteAllCache($cacheDir);
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }    
    public function action_cleaningAllCache() {
        try {
            Cache::instance('apc')->delete_all();
            $website = Session::instance('database')->get('sessionKeyId');
            $cacheDir = "/$website/";
            CacheFile::deleteAllCache($cacheDir);
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }
    
    public function destroySession() {
        Session::instance('database')->destroy();
    }
    public function getSessionParameter($parameterId = NULL) {        
        return Session::instance('database')->get($parameterId);
    }

    public function setSessionParameter($parameterId = NULL, $parameterValue = NULL) {
        Session::instance('database')->set($parameterId, $parameterValue);
    }

    public function getSessionId() {
        return md5(Session::instance('database')->id());
    }

    public function deleteVariableSession($key) {
        return Session::instance('database')->delete($key);
    }

    public function getOptionValue($key) {
        try {
            $var = $this->getSessionParameter(self::SYSTEM_OPTIONS);
            return $var[$key]['value'];
        } catch (Database_Exception $e) {
            echo $this->errorHandling($e);
        }
    }

    public function getCRUDOptionsAsArray() {
        $CRUD_Options = array();
        foreach ($this->CRUD_OPTIONS as $key => $value) {
            array_push($CRUD_Options, $value);
        }
        return $CRUD_Options;
    }

    public function fnResponseFormat($a_return, $s_format = self::REPORT_RESPONSE_TYPE_JSON, $a_header_xml = array()) {
        switch (strtoupper($s_format)) {
            case self::REPORT_RESPONSE_TYPE_JSON:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'application/json');
                echo jQueryHelper::JSON_jQuery_encode($a_return, true);
                break;
            case self::REPORT_RESPONSE_TYPE_JSON_JQGRID:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'application/json');
                echo jQueryHelper::JSON_jQuery_encode($a_return, false);
                break;
            case self::REPORT_RESPONSE_TYPE_XML:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'text/xml');
                $o_dom = XMLHelper::toXML($a_return, $a_header_xml);
                echo $o_dom->saveXML();
                break;
            case self::REPORT_RESPONSE_TYPE_HTML:
                echo $a_return;
                break;
            case self::REPORT_RESPONSE_TYPE_ARRAY:
                return $a_return;
                break;
            default:

                break;
        }
    }

    public function getFileTypeAsArray() {
        $fileType = array();
        foreach ($this->FILE_TYPE as $key => $value) {
            $fileType[$key] = $value;
        }
        return $fileType;
    }

    public function getDATE_FORMAT_FOR($DF, $SO_DATE_FORMAT = NULL) {        
         if ($SO_DATE_FORMAT == NULL) {
            $SO_DATE_FORMAT = $this->getOptionValue(self::SO_DATE_FORMAT);
            if ($SO_DATE_FORMAT == NULL) {
                $SO_DATE_FORMAT = $this->getOptionValueNativeSession(self::SO_DATE_FORMAT);
            }
            if ($SO_DATE_FORMAT == NULL) {
                $optionValue = $this->getOptionValueFromDatabase(self::SO_DATE_FORMAT);
                $SO_DATE_FORMAT = $optionValue->value;
            }
        }
        return $this->DATE_FORMAT[$SO_DATE_FORMAT][$DF];
    }

    public function createSessionEvent($EVENT) {
        try {
            $session_log = ORM::factory('SessionLog');
            $session_log->idUser = $this->getSessionParameter(self::USER_ID);
            $session_log->sessionId = Session::instance('database')->id();
            $session_log->event = $EVENT;
            $session_log->ip = Request::$client_ip;
            $session_log->agent = Request::$user_agent;
            $session_log->save();
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public function uploadFileTMP($DIR_TMP, $FILE_NAME = NULL, $RESIZE_VAL = 128, $b_response = true, $s_resize = Image::HEIGHT) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $this->fnaddNewModule('qqFileUploader', 'qqFileUploader');
        if (Fileupload::exists()) {
            $pathinfo = pathinfo(Fileupload::name());            
            $FILE_NAME = time();
            $EXT = $pathinfo['extension'];
            $FILE_NAME .= '.' . $EXT;
            $FILE_PATH = $DIR_TMP . DIRECTORY_SEPARATOR . $FILE_NAME;
            if (Fileupload::save($FILE_PATH)) {
                $FILE_IMG_RESIZE = Kohana_Image::factory($FILE_PATH);
                $FILE_IMG_RESIZE->resize($RESIZE_VAL, $RESIZE_VAL, $s_resize);
                $FILE_IMG_RESIZE->save($FILE_PATH);
                $aResponse['success'] = 'true';
                $aResponse['data'] = array(
                    'path' => str_replace(DIRECTORY_SEPARATOR, '/', DIRECTORY_SEPARATOR . $FILE_PATH),
                    'tmpfn' => $FILE_NAME);
            } else {
                throw new Exception(__('Error al crear el archivo'), self::CODE_SUCCESS);
            }
        } else {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        if ($b_response) {
            $this->fnResponseFormat($aResponse);
        } else {
            return $aResponse;
        }
        
    }

    public function action_getUploadPhotoTMP() {
        $this->uploadFileTMP(self::FOLDER_UPLOAD_TMP);
    }

    public function action_getSendEmailToSuppportLog() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $client = $this->request->post('eDomain');
            $code = $this->request->post('eCode');
            $user = $this->request->post('eUserName');
            $user_id = $this->request->post('eUserId');
            $time = $this->request->post('eTime');
            $msg = $this->request->post('eTextMsg');
            $trace = $this->request->post('eTrace');
            $eParams = $this->request->post('rParams');
            $line = $this->request->post('eLine');
            $file = $this->request->post('eFile');
            $this->sendEmailToSuppportLog($client, $code, $user, $user_id, $time, $msg, $trace, $eParams, $line, $file);
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse);
    }

    public function sendEmailToSuppportLog($client, $code, $user, $user_id, $time, $msg, $trace, $eParams, $line, $file) {

        try {
            $this->fnaddNewModule('swift', 'swift');
            $company = $this->getSystemSiteName();
            $systemBase = db_config::$db_conection_config[SITE_DOMAIN]['systemBase'];            
            $emailContacts = $this->support_email_contacts;
            if ($systemBase == 'rms')
            {
                unset($emailContacts['wcumpa']);                
                unset($emailContacts['ybenites']);                
            }
            $emailContactsFilter = array_values($emailContacts);
            Email::instance(__("Error en el sistema del cliente") . " " . $company)
                    ->to($emailContactsFilter, Email::TO)
                    ->message(Email::template(View::factory('mail/mail_logerror'), array(
                                ":CLIENT" => $client
                                , ":CODE" => $code
                                , ":USER" => $user
                                , ":USER_ID" => $user_id
                                , ":TIME" => $time
                                , ":MSG" => $msg
                                , ":TRACE" => $trace
                                , ":PARAMS" => $eParams
                                , ":LINE" => $line
                                , ":FILE" => $file
                            )))->from('no_reply@ibusplus.com')->send(FALSE);
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

    public function isModuleActive($module_code) {
        try {
            $modules = $db = DB::select('mo.code', 'mo.status')->from(array('bts_module', 'mo'))->where('mo.code', '=', $module_code)->as_object()->execute()->current();
            if ($modules == NULL) {
                return TRUE;
            } else {
                return (bool) $modules->status ? TRUE : FALSE;
            }
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->code, $exc);
        }
    }

    public function sendEmail($asunto, $mailRecipient, $nameRecipient, $view, $arrayMsj, $emailSend, $nameSend) {        
        $this->loadClientLanguage(db_config::$db_conection_config[SITE_DOMAIN]['skin']);
        $this->fnaddNewModule('swift', 'swift');
        Email::instance(__($asunto))
                ->to(array($mailRecipient => $nameRecipient))
                ->message(Email::template(View::factory($view), $arrayMsj))
                ->from(array($emailSend => $nameSend))->send(TRUE);
    }
    
    public function fnsendMessage($sMessage, $iTo_City = 0, $toIdOffice = 0, $toIdUSer = 0, $toIdGroup = 0) {
        try {
            $oModel_Message = new Model_Messages;
            $oModel_Message->toIdGroup = $toIdGroup;
            $oModel_Message->toIdCity = $iTo_City;
            $oModel_Message->userCreate = $this->getROOT_USER_ID();
            $oModel_Message->userLastChange = $this->getROOT_USER_ID();
            $oModel_Message->toIdOffice = $toIdOffice;
            $oModel_Message->toIdUser = $toIdUSer;
            $oModel_Message->message = $sMessage;
            $oModel_Message->state = self::STATUS_ACTIVE;
            $this->cleaningAllCache();
            $oModel_Message->save();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode(), Exception);
        }
    }
    
    /**
     * Obtener y asignar valores en la sesión nativa PHP
     */
    public function destroyNativeSession() {
        Session::instance()->destroy();
    }

    public function getNativeSessionParameter($parameterId = NULL) {
        return Session::instance()->get($parameterId);
    }

    public function setNativeSessionParameter($parameterId = NULL, $parameterValue = NULL) {
        Session::instance()->set($parameterId, $parameterValue);
    }

    public function getNativeSessionId() {
        return md5(Session::instance()->id());
    }

    public function deleteNativeVariableSession($key) {
        return Session::instance()->delete($key);
    }

    public function setOptionValue4NativeSession($keys) {
        $options = DB::select('options.key', 'options.value')
                ->from(array('bts_options', 'options'));
        if (is_array($keys)) {
            $keys = "'" . str_replace(',', "','", implode(',', $keys)) . "'";
            $options = $options->where('options.key', 'IN', DB::expr('(' . $keys . ')'));
        } else {
            $options = $options->where('options.key', '=', $keys);
        }
        $options = $options->execute()->as_array('key');
        $this->setNativeSessionParameter(self::USER_PUBLIC_SESSION_DATA, $options);
    }

    public function getOptionValueNativeSession($key) {
        try {
            $option = $this->getNativeSessionParameter(self::USER_PUBLIC_SESSION_DATA);
            return $option[$key]['value'];
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getOptionValueFromDatabase($key) {
        try {
            $db = DB::select('options.value', 'options.suffix')->from(array('bts_options','options'))->where('options.key', '=', $key)->as_object()->execute()->current();
            return $db;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }
    public function fnaddNewModule($moduleName, $folderName) {
        $modules = array("$moduleName" => DEVMODPATH. '3.2/' . $folderName);
        Kohana::modules(Kohana::modules() + $modules);
    }

    public static function fnaddNewModuleStatic($moduleName, $folderName) {
        $modules = array("$moduleName" => DEVMODPATH. '3.2/' . $folderName);
        Kohana::modules(Kohana::modules() + $modules);
    }
     public function isKohanaModuleActive($moduleName) {
        $modules = Kohana::modules();
        return array_key_exists($moduleName, $modules);
    }
}