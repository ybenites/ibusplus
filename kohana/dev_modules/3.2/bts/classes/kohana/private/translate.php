<?php
defined('SYSPATH') or die('No direct script access.');
abstract class Kohana_Private_Translate extends Kohana_Private_Admin{

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('translate', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('translate', self::FILE_TYPE_JS));
        $oTranslateView = new View('private/translate');
        $this->template->content = $oTranslateView;
    }

    public function action_listTranslate() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            if (!$sidx)
                $sidx = 1;
            $where_search = "";
            $searchOn = jqGridHelper::Strip($_REQUEST['_search']);

            $array_cols = array(
                'ID' => 'translate.idTranslate',
                'texto_e' => 'translate.text',
                'texto_t' => 'translate.textTranslate',
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'bts_translate translate';

            $where_conditions = "translate.text IS NOT NULL";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_saveTranslate() {
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($_POST)
                    ->rule('t_text', 'not_empty')
                    ->rule('t_textTranslate', 'not_empty');
            if (!$post->check()) {
               throw new Exception("Error en la validación de la Información.", self::CODE_SUCCESS);
            }else{
                $texto = $this->request->post('t_text');
                $traduccion = $this->request->post('t_textTranslate');
                $translate_id = $this->request->post('idTranslate');
                
                $translate = new Model_Translate();
                if ($translate_id != 0) {
                    $translate = ORM::factory('translate', $translate_id);
                }
                $traduccionOriginal = trim($texto);
                $translate->text = $traduccionOriginal;
                $translate->text_hash =sha1($traduccionOriginal); 
                $translate->textTranslate = trim($traduccion);
                $translate->save();
                Database::instance()->commit();
            }                
        } catch (Exception $exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
    }
    public function action_getTranslate() {
        $a_response = $this->json_array_return;
        try {
            $translate_id = $this->request->post('idt');
            $translate = ORM::factory('translate', $translate_id)->as_array();
            $a_response['data'] = $translate;
        } catch (Exception $exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response, 'json');
    }

    public function action_removeTranslate() {
        $a_response = $this->json_array_return;
        try {
            $translate_id = $this->request->post('idt');
            $translate = ORM::factory('translate', $translate_id);
            $translate->delete();
        } catch (Exception $exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $this->errorHandling($exc);
        }

        $this->fnResponseFormat($a_response, 'json');
    }
}
?>
