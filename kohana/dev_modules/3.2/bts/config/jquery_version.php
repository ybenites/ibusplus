<?php defined('SYSPATH') or die('No direct script access.');
return array(
    Kohana::DEVELOPMENT => array(        
      Kohana_Uconstants::VAR_JQUERY_VERSION => '1.8.3'
    , Kohana_Uconstants::VAR_JQUERY_UI_VERSION => '1.9.2'
    ),
    Kohana::PRODUCTION => array(        
      Kohana_Uconstants::VAR_JQUERY_VERSION => '1.8.3'
    , Kohana_Uconstants::VAR_JQUERY_UI_VERSION => '1.9.2'
    )
);