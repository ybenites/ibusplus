<?php 
defined('SYSPATH') or die('No direct script access.'); 

return array
(
	'Seleccione su idioma' => 'Select your language', 
	'Dominio' => 'Domain', 
	'Nuevo Dominio' => 'New Domain', 
	'Configuración de la Base de Datos' => 'Database Configuration', 
	'Nombre del Dominio' => 'Domain', 
	'Tema' => 'Skin', 
	'Nombre del sitio' => 'Site Name', 
	'Usuario' => 'User', 
	'Contraseña' => 'Password', 
	'Nombre del Host o IP' => 'Host name or IP', 
	'Puerto' => 'Port', 
	'Base de Datos' => 'Database Name', 
	'Información de cuenta root' => 'Root Account Info', 
	'Aceptar' => 'Ok', 
	'Cancelar' => 'Cancel', 
	'Procesando...' => 'Processing ...', 
	'IBusPlusPeru Todos los derechoa reservados. 2010' => 'IBusPlusPeru All rights Reserved. 2010', 
	'Error de Validación' => 'Validation Error', 
	'<li>La Base de Datos db_name se eliminó con éxito.</li>' => '<li>The Database db_name was droped successfully.</li>', 
	'<li>La Base de Datos db_name se creó con éxito.</li>' => '<li>The Database db_name was created successfully.</li>', 
	'<li>El usuario db_user y sus privilegios para la Base de Datos db_name se crearon con éxito.</li>' => '<li>The User db_user and privileges for the Database db_name was create successfully.</li>', 
	'<li>Las Tablas e información para la Base de Datos db_name se crearon con éxito.</li>' => '<li>The Tables and Data for the Database db_name was create successfully.</li>', 
	'Creación de Dominio' => 'Domain Creation', 
	'Instalación' => 'Install Step', 
 
); 
 
 ?> 