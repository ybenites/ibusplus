<?php 
defined('SYSPATH') or die('No direct script access.'); 

return array
(
	'Select your language' => 'Seleccione su idioma', 
	'Domain' => 'Dominio', 
	'New Domain' => 'Nuevo Dominio', 
	'Database Configuration' => 'Configuración de la Base de Datos', 
	'Domain' => 'Nombre del Dominio', 
	'Skin' => 'Tema', 
	'Site Name' => 'Nombre del sitio', 
	'User' => 'Usuario', 
	'Password' => 'Contraseña', 
	'Host name or IP' => 'Nombre del Host o IP', 
	'Port' => 'Puerto', 
	'Database Name' => 'Base de Datos', 
	'Root Account Info' => 'Información de cuenta root', 
	'Ok' => 'Aceptar', 
	'Cancel' => 'Cancelar', 
	'Processing ...' => 'Procesando...', 
	'IBusPlusPeru All rights Reserved. 2010' => 'IBusPlusPeru Todos los derechoa reservados. 2010', 
	'Validation Error' => 'Error de Validación', 
	'<li>The Database db_name was droped successfully.</li>' => '<li>La Base de Datos db_name se eliminó con éxito.</li>', 
	'<li>The Database db_name was created successfully.</li>' => '<li>La Base de Datos db_name se creó con éxito.</li>', 
	'<li>The User db_user and privileges for the Database db_name was create successfully.</li>' => '<li>El usuario db_user y sus privilegios para la Base de Datos db_name se crearon con éxito.</li>', 
	'<li>The Tables and Data for the Database db_name was create successfully.</li>' => '<li>Las Tablas e información para la Base de Datos db_name se crearon con éxito.</li>', 
	'Domain Creation' => 'Creación de Dominio', 
	'Install Step' => 'Instalación', 
 
); 
 
 ?> 