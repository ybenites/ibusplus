<script type="text/javascript">
    var draggable_element='<div class="ui-corner-all ui-state-highlight" data-ido="[VALUE]"><span class="ui-button-icon-primary ui-icon ui-icon-close"></span><span class="ui-button-text">[LABEL]</span></div>';
    var draggable_element_js='<div class="ui-corner-all ui-state-error" data-ido="[VALUE]"><span class="ui-button-icon-primary"></span><span class="ui-button-text">[LABEL]</span></div>';
    var draggable_element_css='<div class="ui-corner-all ui-state-hover" data-ido="[VALUE]"><span class="ui-button-icon-primary"></span><span class="ui-button-text">[LABEL]</span></div>';
    var remove_event="re";
    var add_event="ae";
    var originalElements = new Array();
    var newElements = new Array();
    var trashElements = new Array();
    var js_order_change = false;
    var css_order_change = false;
    var b_ajax_form_rate = true;
    (function ($) {
        $.fn.liveDraggable = function (opts) {
            this.live("mouseover", function() {
                if (!$(this).data("init")) {
                    $(this).data("init", true).draggable(opts);
                }
            });
            return $();
        };
    }(jQuery));

    $(document).ready(function(){
        function listPages(){
            $.ajax({
                url:'/private/page/listPages'+jquery_params,
                data:{t:'s'},
                async:false,
                success:function(response){
                    $('option', '#cmbPages').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#cmbPages').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#cmbPages').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                            });
                        } else {
                            $('#cmbPages').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                    $('#cmbPages').combobox({
                        selected:function(event,ui){
                            showLoading=1;
                            selectedPage($(this).val());
                        }
                    });
                }
            });
        }
        function selectedPage(idp){
            $.ajax({
                url:'/private/section/getFilesByPage'+jquery_params,
                data:{id:idp},
                success:function(response){
                    $('#ojs').html('');
                    $('#ocss').html('');
                    $('#js').html('');
                    $('#css').html('');
                    if(evalResponse(response)){
                        originalElements = new Array();
                        newElements = new Array();
                        trashElements = new Array();
                        if(response.data.sFiles.length>0){
                            var js=""
                            var css=""
                            $.each(response.data.sFiles,function(){
                                originalElements.push(eval(this.idf));
                                if(this.ft=='js'){
                                    js+=draggable_element.replace('[VALUE]', this.idf, 'g').replace('[LABEL]', this.fname, 'g')
                                } else{
                                    css+=draggable_element.replace('[VALUE]', this.idf, 'g').replace('[LABEL]', this.fname, 'g')
                                } 
                            });
                            $('#js').append(js);
                            $('#css').append(css);
                        }
                        if(response.data.oFiles.length>0){
                            var js=""
                            var css=""
                            $.each(response.data.oFiles,function(){
                                                
                                if(this.ft=='js'){
                                    js+=draggable_element_js.replace('[VALUE]', this.idf, 'g').replace('[LABEL]', this.fname, 'g')
                                } else{
                                    css+=draggable_element_css.replace('[VALUE]', this.idf, 'g').replace('[LABEL]', this.fname, 'g')
                                } 
                            });
                            $('#ojs').append(js);
                            $('#ocss').append(css);
                        }
                    }
                }
            });
        }
        listPages();
        
        $('#js,#ojs').sortable({
            connectWith:".jsconector",
            items:'div',
            remove:function(event,ui){
                if(ui.item.parent().attr('id')=='ojs'){
                    removeIcon(ui.item);
                    addRemoveItem(remove_event,ui.item);
                }
            },
            change: function(event, ui){
                if(ui.item.parent().attr('id')=='js'){
                    js_order_change=true;
                }  
            }
        }).disableSelection();
        $('#js').droppable({
            accept: "#ojs > div",
            activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                addRemoveItem(add_event,ui.draggable);
                addFile(ui.draggable,this);
            }
        });
        
        $( "#ojs div" ).live('dblclick',(function(){
            addRemoveItem(add_event,this);
            addFile(this,$('#js'));
        }));
        
        $( "#js div" ).live('dblclick',(function(){
            addRemoveItem(remove_event,this);
            removeFile(this,$('#ojs'));
        }));
        
        $('#css,#ocss').sortable({
            connectWith:".cssconector",
            items:'div',
            remove:function(event,ui){
                if(ui.item.parent().attr('id')=='ocss'){
                    removeIcon(ui.item);
                    addRemoveItem(remove_event,ui.item);
                }
            },
            change: function(event, ui){
                if(ui.item.parent().attr('id')=='css'){
                    css_order_change=true;
                }  
            }
        }).disableSelection();
        $('#css').droppable({
            accept: "#ocss > div",
            activeClass: "ui-state-highlight",
            drop: function( event, ui ) {
                addRemoveItem(add_event,ui.draggable);
                addFile(ui.draggable,this);
            }
        });
        
        $( "#ocss div" ).live('dblclick',(function(){
            addRemoveItem(add_event,this);
            addFile(this,$('#css'));
        }));
        
        $( "#css div" ).live('dblclick',(function(){
            addRemoveItem(remove_event,this);
            removeFile(this,$('#ocss'));
        }));

        $('.ui-icon-close').live('click', function(){
            addRemoveItem(remove_event,$(this).parent());
            removeFile($(this).parent(),$('#o'+$(this).parent().parent().attr('id')));
        });
        function hideIcon(item){
            $(item).find('.ui-button-icon-primary').hide();
        }
        function showIcon(item){
            $(item).find('.ui-button-icon-primary').show();
        }
        function addFile(item,type){
            $(item).fadeOut(function(){
                $(this).find('.ui-button-icon-primary').addClass('ui-icon ui-icon-close').end().appendTo($(type)).fadeIn();
            });
        }
        function removeFile(item,type){
            $(item).fadeOut(function(){
                $(this).find('.ui-button-icon-primary').removeClass('ui-icon ui-icon-close').end().appendTo($(type)).fadeIn();
            });
        }
        function removeIcon(item){
            item.find('.ui-button-icon-primary').removeClass('ui-icon ui-icon-close');
        }
        function addRemoveItem(option,item){
            var ido = eval($(item).data('ido'));
            if($(item).hasClass('ui-state-highlight')){
                if($.inArray(ido,trashElements)<0 && option==remove_event){
                    trashElements.push(ido);
                }
                if($.inArray(ido,trashElements)>=0 && option==add_event){
                    trashElements = $.removeFromArray(ido,trashElements);
                }
            }
            if($(item).hasClass('ui-state-hover') || $(item).hasClass('ui-state-error')){
                if($.inArray(ido,newElements)<0 && option==add_event){
                    newElements.push(ido);
                }
                if($.inArray(ido,newElements)>=0 && option==remove_event){
                    newElements = $.removeFromArray(ido,newElements);
                }
            }
        }
        $('.img_save').click(function(){
            if(b_ajax_form_rate){
                if($('#cmbPages').val()!='' && (newElements.length>0 || trashElements.length>0 || js_order_change || css_order_change)){
                    showLoading=1;
                    var css_ordr = new Array();
                    $.each($('#css div'),function(){
                        css_ordr.push($(this).data('ido'));
                    });
                    var js_ordr = new Array();
                    $.each($('#js div'),function(){
                        js_ordr.push($(this).data('ido'));
                    });
                    $.ajax({
                    url:'/private/section/updateSection'+jquery_params,
                        data:{pid:$('#cmbPages').val(),ne:newElements,te:trashElements,csso:css_ordr,jso:js_ordr},
                        beforeSend: function(){
                            b_ajax_form_rate = false;
                        },
                        success:function(response){
                            if(evalResponse(response)){
                                js_order_change = false;
                                css_order_change = false;
                                selectedPage($('#cmbPages').val());
                            } 
                        },
                        complete: function(){
                            b_ajax_form_rate = true;
                        }
                    });
                }
            }
        });
        $('#pageLink').click(function(){
            window.location.href = "/private/page";
        });
        $('.fileLink').click(function(){
            window.location.href = "/private/file";
        });
    });
</script>
<style type="text/css">
    #files_content{
        width:1000px;
        margin: 0 auto;
        margin-top: 15px;
    }
    #js_files,#page_files,#css_files{
        width: 300px;
        height: 400px;
        float: left;
        padding: 10px;
        margin-right: 10px;
    }
    .img_file{
        margin-top: -21px;
    }
    .img_save{
        float: right;
        margin-top: -10px;
        cursor: pointer;
    }
    .title_content,.subtitle_content{
        padding: 4px;
    }
    .subtitle_content,#css,#js{
        margin-top: 5px;
    }
    #css,#js{
        height: 145px;
        overflow: auto;
    }
    #ocss,#ojs{
        margin-top: 5px;
        height: 363px;
        overflow: auto;
    }
    #css div,#js div,#ocss div,#ojs div,.ui-draggable-dragging,.ui-sortable-helper{
        padding: 2px;
        display: block;
        margin: 1px;
        font-size: 12px;
        min-height: 18px;

    }
    #css .ui-button-text,#js .ui-button-text, #ocss .ui-button-text,#ojs .ui-button-text,#pageLink,.fileLink{
        cursor: pointer;

    }
    #css .ui-button-icon-primary,#js .ui-button-icon-primary,#ocss .ui-button-icon-primary,#ojs .ui-button-icon-primary{
        float: left;
        cursor: pointer;
    }
    .ui-state-error,.ui-state-hover{
        cursor: pointer;
    }
</style>

<div id="files_content">
    <div  style="width: 350px; margin: 0 auto; padding: 4px; text-align: center; line-height: 20px;">
        <div>
            <label style="color: black; font-weight: bold;"><?php echo __("Seleccione la Página"); ?></label>
            <select id="cmbPages" style="width: 180px;"></select>
        </div>
        <br/>
    </div>
    <div class="clear"></div>
    <div id="js_files" class="ui-corner-all ui-widget-content "> 
        <div class="title_content ui-state-active ui-corner-all">
            <img src="/media/images/js_code.png" class="img_file fileLink"/>
            <?php echo __("Archivos Javascript"); ?>
        </div>
        <div id="ojs" class="ui-corner-all ui-widget-content jsconector title_content">

        </div>
    </div>
    <div id="page_files" class="ui-corner-all ui-widget-content">
        <div class="title_content ui-state-active ui-corner-all">
            <img src="/media/images/php_page.png" class="img_file" id="pageLink"/>
            <?php echo __("Páginas"); ?>
            <img src="/media/ico/ico_save.png" class="img_save"/>
        </div>

        <div class="subtitle_content ui-state-active ui-corner-all">
            <?php echo __("CSS"); ?>
        </div>

        <div id="css" class="ui-corner-all ui-widget-content cssconector title_content">

        </div>

        <div class="subtitle_content ui-state-active ui-corner-all">
            <?php echo __("JS"); ?>
        </div>

        <div id="js" class="ui-corner-all ui-widget-content jsconector title_content ">

        </div>
    </div>
    <div id="css_files" class="ui-corner-all ui-widget-content">
        <div class="title_content ui-state-active ui-corner-all">
            <img src="/media/images/css_code.png" class="img_file fileLink"/>
            <?php echo __("Archivos CSS"); ?>
        </div>
        <div id="ocss" class="ui-corner-all ui-widget-content cssconector title_content">

        </div>
    </div>

</div>
