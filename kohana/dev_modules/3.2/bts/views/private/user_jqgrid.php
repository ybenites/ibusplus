<script type="text/javascript">
    var user_form_title = '<?php echo __("Usuario"); ?>';
    var user_name_required = '<?php echo __('El nombre de la oficina es obligatorio'); ?>';
    var user_name_min_required = '<?php echo __('Se requiere al menos {0} caracteres para el nombre del usuario'); ?>';
    var user_pass_required = '<?php echo __('La contraseña del usuario es obligatoria'); ?>';
    var user_passv_required = '<?php echo __('Debe escribir la contraseña nuevamente'); ?>';
    var user_pass_eq_required = '<?php echo __('Las contraseñas escritas no coinciden'); ?>';
    var user_group_required = '<?php echo __('Debe seleccionar un grupo'); ?>';
    var user_office_required = '<?php echo __('Debe seleccionar la oficina'); ?>';
    var user_fName_required = '<?php echo __('El nombre real del usuario es obligatorio'); ?>';
    var user_lName_required = '<?php echo __('El apellido del usuario es obligatorio'); ?>';
    var user_email_format = '<?php echo __('El email no tiene formato válido'); ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Usuario"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este usuario?"); ?>';
    var root_option = Boolean(eval('<?php echo $user_rootOption ?>'));
    var url_user = '/private/user/listUsers'+jquery_params;
    var url_trash = '/private/user/removeUser'+jquery_params;
    var url_find = '/private/user/getUserById'+jquery_params;
    var ico_on = '<img src="/media/ico/ico_power_on.png" />';
    var ico_off = '<img src="/media/ico/ico_power_off.png" />';
    var pvalidate = true;
    $.validator.methods.equal = function(value, element, param) {
        return value == $('#u_password').val();
    };
    $(document).ready(function(){
        $("#frmUser").validate({
            rules: {
                u_name:{required:true,minlength:4},
                u_group:{required:true},
                u_office:{required:true},
                u_password:{required: function(){return pvalidate;}},
                u_password_validator:{required: function(){return pvalidate;},equal: true},
                p_fname:{required:true},
                p_lname:{required:true},
                p_email:{email:true}
            },
            messages:{
                u_name:{required:user_name_required,minlength:user_name_min_required},
                u_group:{required:user_group_required},
                u_office:{required:user_office_required},
                u_password:{required: user_pass_required},
                u_password_validator:{required: user_passv_required,equal: user_pass_eq_required},
                p_fname:{required:user_fName_required},
                p_lname:{required:user_lName_required},
                p_email:{email:user_email_format}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                var event = 0;
                if(pvalidate) event = 1;
                $.ajax({
                    url: '/private/user/createOrUpdateUser'+jquery_params,
                    data:$(form).serialize()+'&e='+event,
                    success: function(response){
                        if(evalResponse(response)){
                            $('#user_jqGrid').trigger('reloadGrid');
                            $('#frmPopup').dialog('close');
                        } 
                    }
                });
            }
        });
        
        function listGroups(){
            $.ajax({
                url:'/private/user/listGroups'+jquery_params,
                data:{t:'A'},
                async:false,
                success:function(response){
                    $('option', '#u_group').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#u_group').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#u_group').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                            });
                        } else {
                            $('#u_group').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listGroups();
        function listOffices(){
            $.ajax({
                url:'/private/user/listOffices'+jquery_params,
                data:{t:'A'},
                async:false,
                success:function(response){
                    $('option', '#u_office').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#u_office').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#u_office').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                            });
                        } else {
                            $('#u_office').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listOffices();
        $("#rootOption").buttonset();
        var objOkBtn = {
            id:"btn-accept",
            text: okButtonText,
            click: function() {
                $("#frmUser").submit();
            }
        };
        var objCancelBtn = {
            id:"btn-cancel",
            text: cancelButtonText,
            click: function() {
                $(this).dialog("close");
            }
        };
        var objChpBtn = {
            id:"btn-cpass",
            text: chPassText,
            click: function() {
                pvalidate = !pvalidate;
                if(pvalidate){
                    $('.pvalidate').fadeIn(500, function(){
                        $('input#u_password,input#u_password_validator').removeAttr('disabled');
                    });
                } else{
                    $('.pvalidate').fadeOut(500,function(){
                        $('input#u_password,input#u_password_validator').attr('disabled',"");
                    });
                }
            }
        };
        var newArray =[objOkBtn,objCancelBtn];
        var editArray =[objChpBtn,objOkBtn,objCancelBtn];
        $('#frmPopup').dialog({
            title:user_form_title,
            width:650,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmUser").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $("#btnNewUser").click(function(){
            $('#u_name').val('');
            $('#u_userCode').val('');
            $('#u_group').val('');
            $('#u_group').removeAttr('disabled');
            $('#u_office').val('');
            $('#u_password').val('');
            $('#u_password_validator').val('');
            $('#p_fname').val('');
            $('#p_lname').val('');
            $('#p_phone1').val('');
            $('#p_phone2').val('');
            $('#p_cellphone').val('');
            $('#p_address1').val('');
            $('#p_address2').val('');
            $('#p_email').val('');
            $('#u_id').val('');
            $('#u_photo').val('');
            $('.profileImg').attr('src','/upload/profile/128/02.png');
            clearList();
            $('.pvalidate').show();
            pvalidate=true;
            $('input#u_password,input#u_password_validator').removeAttr('disabled');
            $('#frmPopup').dialog({buttons:newArray});
            $('#frmPopup').dialog('open');
        });
        $('#user_jqGrid').jqGrid({
            url:url_user,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:300,
            rowList: [30,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Nombre Real") ?>',
                '<?php echo __("Grupo") ?>',
                'gsDefault',
                '<?php echo __("Oficina") ?>',
                '<?php echo __("Opción ROOT") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'name',index:'u.userName',sortable:true,width:120,search:true,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'fName',index:'p.fullName',sortable:true,width:160,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'gname',index:'gname',sortable:true,width:120,search:false},
                {name:'gsDefault',index:'gsDefault',hidden:true,hidedlg:true},
                {name:'office',index:'office',sortable:true,width:100,search:false},
                {name:'rootOption',index:'rootOption',sortable:false,width:70,hidden:(!root_option),search:false,align:'center'},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,search:false}
            ],
            pager: "#user_jqGrid_pager",
            sortname: 'name',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(Boolean(eval(rowdata.rootOption))){
                    $("#user_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_on});
                } else {
                    $("#user_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_off});
                }
            },
            gridComplete:function(){
                var ids = $('#user_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = edit_template.replace('[ID]',ids[i],'g');
                    trash="";
                    rowData = $('#user_jqGrid').jqGrid('getRowData',ids[i]);
                    if(!Boolean(eval(rowData.gsDefault))){
                        trash = trash_template.replace('[ID]',ids[i],'g');
                    }
                    $("#user_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $("#u_id").val(response.data.idUser);
                                $("#u_name").val(response.data.userName);
                                $("#u_userCode").val(response.data.userCode);
                                $("#u_group").val(response.data.idGroup);
                                if(Boolean(eval(response.data.systemDefault))){
                                    $('#u_group').attr('disabled', true);
                                }else{
                                    $('#u_group').removeAttr('disabled');
                                }
                                $("#u_office").val(response.data.idOffice);
                                $("#p_fname").val(response.data.fName);
                                $("#p_lname").val(response.data.lName);
                                $("#p_phone1").val(response.data.phone1);
                                $("#p_phone2").val(response.data.phone1);
                                $("#p_cellphone").val(response.data.cellphone);
                                $("#p_address1").val(response.data.address1);
                                $("#p_address2").val(response.data.address2);
                                $("#p_email").val(response.data.email);
                                $('#u_photo').val('');
                                clearList();
                                if(response.data.imgProfile!=null){
                                    $('.profileImg').attr('src',response.data.imgProfile);
                                } else{
                                    $('#u_photo').val('');
                                    $('.profileImg').attr('src','/upload/profile/128/02.png');
                                }
<?php if ((bool) $user_rootOption) { ?>
                                    if(Boolean(eval(response.data.rootOption))){
                                        setRadio('rootOptionYES');
                                    } else {
                                        setRadio('rootOptionNO');
                                    }
<?php } ?>
                                $('.pvalidate').hide();
                                $('input#u_password,input#u_password_validator').attr('disabled',"");
                                pvalidate = false;
                                $('#frmPopup').dialog({buttons:editArray});
                                $('#frmPopup').dialog('open');
                            } 
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                        if(response){
                            $.ajax({
                                url:url_trash,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#user_jqGrid').trigger('reloadGrid');
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });

        $("#user_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
    });
    function createUploader(){
        var uploader = new qq.FileUploader({
            element:document.getElementById('btnImgUpload'),
            action: '/private/user/getUploadPhotoTMP',
            allowedExtensions: ['jpg','jpeg','png'],
            onSubmit: function(id, fileName){
                $('.profileImg').fadeOut();
            },
            onComplete: function(id, fileName, responseJSON){
                if(responseJSON.code==code_success && responseJSON.success=='true'){
                    $('#u_photo').val(responseJSON.data.tmpfn);
                    var src = responseJSON.data.path;
                    $('.profileImg').attr('src',src.toString()).fadeIn(2000);
                } else{
                    msgBox(responseJSON.msg, responseJSON.code);
                }
            }
        });
        setCSSUploader();
    }
    function clearList(){
        $('li', '.qq-upload-list').remove();
    }
    window.onload = createUploader;  
</script>
<style type="text/css">
    #jqgh_user_jqGrid_rootOption{
        height: 30px;
        white-space: pre-wrap;
    }
    .label_output div label {
        min-width: 160px;
        max-width: 160px;
    }
    #user_jqGrid tbody tr td{
        white-space: normal;
    }
    .profileImg{
        margin-top: 5px;
    }
</style>
<button id="btnNewUser" class="btn" ><?php echo __("Nuevo Usuario"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Usuarios"); ?></div></center>
<center>
    <table id="user_jqGrid"></table>
    <div id="user_jqGrid_pager"></div>
</center>

<div id="frmPopup" class="dialog-modal">
    <form id="frmUser" method="post" enctype="multipart/form-data">
        <div class="left label_output">
            <div>
                <label><?php echo __("Nombre de usuario"); ?>:</label>
                <input id="u_name" name="u_name" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Codigo de usuario"); ?>:</label>
                <input id="u_userCode" name="u_userCode" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Grupo"); ?>:</label>
                <select id="u_group" name="u_group" style="width: 200px;"></select>
            </div>
            <div>
                <label><?php echo __("Oficina"); ?>:</label>
                <select id="u_office" name="u_office" style="width: 200px;"></select>
            </div>
            <div class="pvalidate">
                <label><?php echo __("Contraseña"); ?>:</label>
                <input id="u_password" name="u_password" value="" type="password" maxlength="150" size="30" />
            </div>
            <div class="pvalidate">
                <label><?php echo __("Reingrese contraseña"); ?>:</label>
                <input id="u_password_validator" name="u_password_validator" value="" type="password" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Nombre(s)"); ?>:</label>
                <input id="p_fname" name="p_fname" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Apellidos"); ?>:</label>
                <input id="p_lname" name="p_lname" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Teléfono"); ?>&nbsp;1:</label>
                <input id="p_phone1" name="p_phone1" value="" type="text" maxlength="150" size="30" alt="number" />
            </div>
            <div>
                <label><?php echo __("Teléfono"); ?>&nbsp;2:</label>
                <input id="p_phone2" name="p_phone2" value="" type="text" maxlength="150" size="30" alt="number" />
            </div>
            <div>
                <label><?php echo __("Celular"); ?>:</label>
                <input id="p_cellphone" name="p_cellphone" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Dirección"); ?>&nbsp;1:</label>
                <input id="p_address1" name="p_address1" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Dirección"); ?>&nbsp;2:</label>
                <input id="p_address2" name="p_address2" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Correo-e"); ?>:</label>
                <input id="p_email" name="p_email" value="" type="text" maxlength="150" size="30" />
            </div>

            <?php if ((bool) $user_rootOption) { ?>
                <div>
                    <label style="margin-top: 7px;"><?php echo __("Opción ROOT"); ?>:</label>
                    <div id="rootOption">
                        <input type="radio" id="rootOptionYES" name="rootOption" value="1"/><label for="rootOptionYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                        <input type="radio" id="rootOptionNO" name="rootOption" checked="checked" value="0"/><label for="rootOptionNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                    </div>
                </div>
            <?php } ?>
        </div>
        <input type="hidden" name="u_id" id="u_id"/>
        <input type="hidden" name="p_type" id="p_type"/>
        <input type="hidden" name="u_photo" id="u_photo"/>
    </form>

    <div id="imgUpload" class="left" style="margin-left: 15px; max-width: 200px;min-width: 200px;">
        <form id="frmUserImg" method="post" enctype="multipart/form-data" >
            <div class="ui-corner-all ui-widget-header" style="height: 138px;">
                <center>
                    <img class="profileImg ui-corner-all" src="/upload/profile/128/02.png"/>
                </center>
            </div>
            <br/>
            <center>
                <div id="btnImgUpload">
                    <noscript>			
                    <p>Please enable JavaScript to use file uploader.</p>
                    <!-- or put a simple form for upload here -->
                    </noscript>
                </div>
            </center>
        </form>
    </div>

</div>