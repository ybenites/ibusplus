<script type="text/javascript">
    var url_location_tinyMCE='/media/js/tinymce/tiny_mce.js';
    var url_location_cssTinyMCE="/media/js/tinymce/css/content.css";
    var url_fnSaveContent="/private/managerpages/saveContent";
    var url_fnListContent="/private/managerpages/listContent";
    var url_fnSaveNewLink="/private/managerpages/saveNewLink";
    var url_fnDeleteLink="/private/managerpages/deleteLink";
    var msg_required_links='<?php echo __('Seleccione un link'); ?>';
    var msg_required_elm1='<?php echo __('Ingrese Contenido') ;?>';
    var title_dialogNewLink='<?php echo __('Crear Nuevo Link') ?>';
    var msg_required_newLink='<?php echo __('Ingrese un Link') ?>';
    var msgBox_saveTrue='<?php echo __('Se Guardo Correctamente el contenido..!!') ?>';
    var msgBoxTitle_saveTrue='<?php echo __('Respuesta del Proceso') ?>';
    var titleConfirmBox='<?php echo __('Confirma el Proceso') ?>';
    var messageConfirmBoxNewLink='<?php echo __('Esta Seguro de Crear un nuevo Link') ?>';
    var messageConfirmBoxDeleteLink='<?php echo __('Esta Seguro de Eliminar Link') ?>';
    $(document).ready(function(){
        function reloadPage(){
            location.reload();
        }
        $("#links").combobox({
            selected:function(event,ui){
                $('#elm1').tinymce().setContent("");
                var valCont=ui.item.value;
                $.ajax({                    
                    url:url_fnListContent+jquery_params,
                    data:{
                        valCont: valCont                        
                    },
                    type:"POST",
                    dataType:"jsonp",
                    beforeSend:function(){
                        $("#loading").dialog("open");
                    },
                    complete:function(){
                        $("#loading").dialog("close");
                    },
                    success:function(r){
                        if(r.code==code_success){
                            var data=r.data;
                            $("#idCont").val(data.idTableCms);
                            var content=data.contentTableCms;  
                            if(content!=null){
                                $('#elm1').tinymce().execCommand('mceInsertContent',false,content);
                            }                            
                        }
                    }
                });
            }
        });
        $("#diagNewLink").dialog({
            autoOpen: false,
            title:title_dialogNewLink,
            height:100
        });
        $("#addlink").button({
            text: false,
            icons: {
                primary:'ui-icon-plusthick'
            }
        });
        $("#lesslink").button({
            text: false,
            icons: {
                primary:'ui-icon-minusthick'
            }
        });
        $("#addlink").on('click',function(e){
            e.preventDefault();
            frmNewLink.currentForm.reset();
            $("#diagNewLink").dialog('open');
        });
        $("#lesslink").on('click',function(e){
            e.preventDefault();
            var idLink=$("#links").val();
            confirmBox(messageConfirmBoxDeleteLink, titleConfirmBox,function(rpta){
                if(rpta){
                    $.ajax({
                        url:url_fnDeleteLink+jquery_params,
                        data:{
                            idLink:idLink                       
                        },
                        type:"POST",
                        dataType:"jsonp",
                        beforeSend:function(){
                            $("#loading").dialog("open");
                        },
                        complete:function(){
                            $("#loading").dialog("close");
                        },
                        success:function(r){
                            if(r.code==code_success){
                                msgBox(r.respuesta,msgBoxTitle_saveTrue,reloadPage());                                
                            }
                        }
                    });
                }
            });            
        });
        $("input#newLink").keyup(function(e){            
            if(e.keyCode==13){
                if(!$("#frmNewLink").valid())
                    return false;
                var valNewLink=$(this).val();
                confirmBox(messageConfirmBoxNewLink, titleConfirmBox,function(rpta){
                    if(rpta){
                        $.ajax({
                            url:url_fnSaveNewLink+jquery_params,
                            data:{
                                valNewLink:valNewLink                       
                            },
                            type:"POST",
                            dataType:"jsonp",
                            beforeSend:function(){
                                $("#loading").dialog("open");
                            },
                            complete:function(){
                                $("#loading").dialog("close");
                            },
                            success:function(r){
                                if(r.code==code_success){
                                    $("#diagNewLink").dialog('close');
                                    msgBox(r.respuesta,msgBoxTitle_saveTrue,reloadPage());
                                }
                            }
                        });
                    }
                });                
            }
        });
        var frmNewLink=$("#frmNewLink").validate({
            rules:{
                newLink:{
                    required:true
                }
            },
            messages:{
                newLink:{
                    required:msg_required_newLink
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){                
            }
        });
        $("#frmCms").validate({
            rules:{
                'links-combobox':{
                    required:true
                },
                elm1:{
                    required:true
                }
            },
            messages:{
                'links-combobox':{
                    required:msg_required_links
                },
                elm1:{
                    required:msg_required_elm1
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){
                
            }
        });
        
        $("textarea.tinymce").tinymce({
            // Location of TinyMCE script   
            relative_urls:false,
            script_url : url_location_tinyMCE,
            
            // General options
            theme : "advanced",
            skin : "o2k7",
            
            plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,imgsurfer,phpimage",
            
            // Theme options
            theme_advanced_buttons1 : "save,newdocument,cancel,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
            theme_advanced_buttons2 : "link,unlink,anchor,imgsurfer,phpimage,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons3 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",            

            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            auto_focus : "elm1",
            
            // Example content CSS (should be your site CSS)
            content_css :url_location_cssTinyMCE,

            // Drop lists for link/image/media/template dialogs
//            template_external_list_url : "/lists/template_list.js",
//            external_link_list_url : "/lists/link_list.js",
//            external_image_list_url : "/lists/image_list.js",
//            media_external_list_url : "/lists/media_list.js",
            
            save_enablewhendirty : true,
            save_onsavecallback:function(q){
                if(!$("form#frmCms").valid())
                    return false;
                $.ajax({                    
                    url:url_fnSaveContent+jquery_params,
                    data:{
                        frmData: $("#frmCms").serializeObject()                        
                    },
                    type:"POST",
                    dataType:"jsonp",
                    beforeSend:function(){
                        $("#loading").dialog("open");
                    },
                    complete:function(){
                        $("#loading").dialog("close");
                    },
                    success:function(r){
                        if(r.code==code_success){
                            msgBox(msgBox_saveTrue, msgBoxTitle_saveTrue)
                        }
                    }
                });
            },                       
            // Replace values for the template plugin
            template_replace_values : {
                    username : "Jack Black",
                    staffid : "991234"
            }
        });
    });
</script>
<style type="text/css">
    #links-combobox{
        height: 18px;
        width: 150px;
    }
    .cnn > div{
        padding: 5px;
    }

    #frmNewLink > div >input{
        height: 20px;
    }
    #frmNewLink > div>label{
        font-weight: bold;
    }
    #frmNewLink > div{
        padding: 10px;
    }  
    
</style>
<div class="titleTables"><center><?php echo __('Administrador De Contenido De CMS'); ?></center></div>

<div>    
    <div>        
        <form id="frmCms" method="post" action="#" style="width:95%;margin-left: auto;margin-right: auto;">
            <div class="cnn">
                <div>
                    <label><?php echo __("Pestaña");?>: </label>
                    <select name="links" id="links">
                        <option value=""></option>
                        <?php
                        foreach ($listLinks as $v) {
                            ?>
                        <option value="<?php echo $v['idCmscontent']?>"><?php echo $v['titleTableCms']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <button id="addlink"><?php echo __('Agregar Pestañas Principal');?></button>
                    <button id="lesslink"><?php echo __('Agregar Pestañas Principal');?></button>                    
                </div>
                <!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
                <div>
                    <input type="hidden" name="idCont" id="idCont" />
                    <textarea id="elm1" name="elm1" rows="25" cols="300" class="tinymce required none" style="width:100%;height: 500px;">
                    </textarea>
                </div>
<!--                <div>
                     Some integration calls 
                    <a href="javascript:;" onclick="$('#elm1').tinymce().show();return false;">[Show]</a>
                    <a href="javascript:;" onclick="$('#elm1').tinymce().hide();return false;">[Hide]</a>
                    <a href="javascript:;" onclick="$('#elm1').tinymce().execCommand('Bold');return false;">[Bold]</a>
                    <a href="javascript:;" onclick="alert($('#elm1').html());return false;">[Get contents]</a>
                    <a href="javascript:;" onclick="alert($('#elm1').tinymce().selection.getContent());return false;">[Get selected HTML]</a>
                    <a href="javascript:;" onclick="alert($('#elm1').tinymce().selection.getContent({format : 'text'}));return false;">[Get selected text]</a>
                    <a href="javascript:;" onclick="alert($('#elm1').tinymce().selection.getNode().nodeName);return false;">[Get selected element]</a>
                    <a href="javascript:;" onclick="$('#elm1').tinymce().execCommand('mceInsertContent',false,'<b>Hello world!!</b>');return false;">[Insert HTML]</a>
                    <a href="javascript:;" onclick="$('#elm1').tinymce().execCommand('mceReplaceContent',false,'<b>{$selection}</b>');return false;">[Replace selection]</a>
                </div>                -->
            </div>
        </form>
    </div>
</div>

<div id="diagNewLink" class="none">
    <div>
        <form id="frmNewLink">
            <div>
                <label><?php echo __('Nombre Pestaña');?>: </label>
                <input type="text" name="newLink" id="newLink" size="25px" />
            </div>
        </form>
    </div>
</div>

