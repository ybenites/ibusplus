<script type="text/javascript">
    var popupTitle = '<?php echo __("Página"); ?>';
    var newSubTitle = '<?php echo __("Nueva Página"); ?>';
    var editSubTitle = '<?php echo __("Editar Página"); ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Página"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar esta página?"); ?>';
    var pageNameRequired = '<?php echo __("El nombre de la página es necesaria"); ?>'
    var urlSave = '/private/page/savePage'+jquery_params;
    var url_pages = '/private/page/listPagesXML'+jquery_params;
    var url_find = '/private/page/getPageById'+jquery_params;
    var url_trash = '/private/page/removePage'+jquery_params;
    $(document).ready(function(){
        $('.parent_group').hide();
        $("#frmPage").validate({
            rules: {
                pageName:{required: true}
            },
            messages:{
                pageName: {required: pageNameRequired}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element) {$(element).addClass('ui-state-error');},
            unhighlight:function(element){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                $.ajax({
                    url:urlSave,
                    data:$(form).serialize(),
                    success:function(response){
                        if(evalResponse(response)){
                            $('#dlgNewPage').dialog('close');
                            $('#page_jqGrid').trigger('reloadGrid');
                        } 
                    }
                });
            }
        });
        
        $('#dlgNewPage').dialog({
            title:popupTitle,
            width:400,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmPage").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $('#btnNewPage').click(function(){
            $('#pageName').val('').focus();
            $('#pageId').val('');
            $('#pageParent').val('');
            $('#parentId').val('');
            $('.parent_group').hide();
            $('#dlgNewPage').dialog('open');
        })
        
        $('#page_jqGrid').jqGrid({
            treeGrid:true,
            treeGridModel : 'adjacency',
            ExpandColumn:'pageName',
            url:'/private/page/listPagesXML?nodeid=0&n_level=0',
            datatype:'xml',
            mtype:'POST',
            colNames:[
                'idPage',
                'cDate',
                'lcDate',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'pageName',index:'pageName',sortable:true,width:160},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,align:'center',search:false}
            ],
            height:'auto',
            pager : "#page_jqGrid_pager",
            caption: "<?php echo __("Páginas") ?>"
            /*url:url_pages,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:280,
            rowList: [20,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'pageName',index:'pageName',sortable:true,width:160},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,align:'center',search:false}
            ],
            pager: "#page_jqGrid_pager",
            sortname: 'pageName',
            gridComplete:function(){
                var ids = $('#page_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = "<a class=\"edit\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#page_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $('#pageName').val(response.data.pageName);
                                $('#pageId').val(response.data.idPage);
                                $('#dlgNewPage').dialog('open');
                            } else {
                                msgBox(response.msg,response.code);
                            }
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_trash,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $('#page_jqGrid').trigger('reloadGrid');
                            } else {
                                msgBox(response.msg,response.code);
                            }
                        }
                    });
                });
            }*/
        });

        //$("#page_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        
        $('.ui-jqgrid-ico-edit').live('click',function(){
            var idPage = $(this).data("ido");
            var node = $("#page_jqGrid").getRowData(idPage);
            var parent = $("#page_jqGrid").getNodeParent(node);
            if(parent!=null){
                $('#pageParent').val(parent.pageName);
                $('#parentId').val(parent.id);
                $('.parent_group').show();
                
            } else{
                $('#pageParent').val('');
                $('#parentId').val('');
                $('.parent_group').hide();
            }
            $('#pageName').val(node.pageName).focus();
            $('#pageId').val(node.id);
            $('#dlgNewPage').dialog('open');
        });
        $('.ui-jqgrid-ico-new-subfile').live('click',function(){
            var idPage = $(this).data("ido");
            var node = $("#page_jqGrid").getRowData(idPage);
            $('.parent_group').show();
            $('#pageParent').val(node.pageName);
            $('#parentId').val(node.id);
            $('#pageName').val('').focus();
            $('#pageId').val('');
            $('#dlgNewPage').dialog('open');
        });
        
        $(".ui-jqgrid-ico-trash").live("click",function(){
            var id = $(this).data("ido");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response == true)
                {
                    $.ajax(
                    {
                        type: "POST",
                        url: url_trash,
                        data:{id:id},
                        dataType:'json',
                        success: function(response)
                        {
                            if(evalResponse(response)){
                                $('#page_jqGrid').trigger('reloadGrid');
                            } 
                        }
                    });
                }
            });
            return false;
        });
        $('.sectionLink').button({
            icons:{primary: 'ui-icon-extlink'},
            text: true
        }).click(function(){
             window.location.href = "/private/section";
        });
    });
</script>
<style type="text/css">
    .label_output div label{
        min-width: 70px;
    }
</style>
<button id="btnNewPage" class="btn"><?php echo __("Nueva Página"); ?></button>
<button  class="sectionLink" ><?php echo __("Ir a Secciones"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Páginas"); ?></div></center>
<center>
    <table id="page_jqGrid"></table>
    <div id="page_jqGrid_pager"></div>
</center>

<div id="dlgNewPage" class="dialog-modal">
    <form id="frmPage" method="post">
        <div class="label_output">
            <div class="parent_group">
                <label><?php echo __("Padre"); ?></label>
                <input id="pageParent" name="pageParent" size="30" type="text" readonly=""/>
            </div>
            <div>
                <label><?php echo __("Nombre"); ?></label>
                <input id="pageName" name="pageName" size="30" type="text"/>
            </div>
            <input id="pageId" name="pageId" type="hidden"/>
            <input id="parentId" name="parentId" type="hidden"/>
        </div>
    </form>
</div>