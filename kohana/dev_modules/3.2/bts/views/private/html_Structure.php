<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
                
    });
</script>
<script type="text/javascript">
    //click en el boton guardar dominio
    $(document).ready(function(){

        //click en el baton eliminar de cada dominio
        msgConfirmBox = $("span.msgConfirmBox").html();
        titleConfirmBox = $("span.titleConfirmBox").html();
        $('#deleteHtmlStructure').live('click',function(){
            //            var idHtmlStructure=$("input[name=idDominio]");
            var idhtmlstructure=$(this).parent().children('input[name=idHtmlStructure]').attr('value');
            confirmBox(msgConfirmBox,titleConfirmBox,function(response){
                if(response == true)
                {
                    $(this).parent().parent('tr').remove();
                    $.post('/private/htmlstructure/deleteHtmlStructure', {idhtmlstructure: idhtmlstructure}, function(data) {
                        
                        if(data.code == code_success)
                        {
                            location.reload();
                        }else{
                            msgBox(data.msg,data.code);
                        }
                    },'json');
                }
            });
        });
        //click en el boton que busca a los HtmlStructures que se encuentran en la base de datos
        $('#findHtmlStructure').live('click',function(){
            var idhtmlstructure=$(this).parent().children('input[name=idHtmlStructure]').attr('value');
            $.post('/private/htmlstructure/getHtmlStructure', {idhtmlstructure:idhtmlstructure}, function(data) {
                if(data.code == code_success)
                {
                    data=data.data;
                    $('.frmSubtitle').text('<?php echo __("Editar Estructura HTML"); ?>');
                    $('#htmlStructureType').val(data['htmlStructureType']);
                    $('#idHtmlStructurePrincipal').val(data['idHtmlStructure']);
                    $('#width').val(data['width']);
                    $('#height').val(data['height']);
                    tinyMCE.activeEditor.setContent(data['htmlCode']);
                    $('#idPagina').val(data['idPagina']);
                    $('#idDominio').val(data['idDominio']);
                    $('#frmPopup').dialog('open');
                }else{
                    msgBox(data.msg,data.code);
                }
                
            },'json');
        });

        //INICIO CLICK NUEVO MENU
        $("#btnNewHtmlStructure").click(function(){
            validator.resetForm();
            $('.frmSubtitle').text('<?php echo __("Nueva Estructura HTML"); ?>');
            $('#htmlStructureType').val('');
            $('#idHtmlStructurePrincipal').val('0');
            $('#width').val('');
            $('#height').val('');
            $('#idPagina').val('1');
            $('#dominio').val('1');
            $('#elm1_save').hide();
            $('#elm1').text('');
            $('#frmPopup').dialog('open');

        });


        //INICIO DATATABLE MENU
        var tblHtmlStructure = $('#tblHtmlStructure').dataTable({
            "bJQueryUI": true,
            "bSort": false,
            "iDisplayLength": 20,
            "bScrollInfinite": true,
            "bScrollCollapse": true,
            "sScrollY": "200px",
            "bFilter":false
        });
        //FIN DATATABLE MENU
        //FIN CLICK MENU NUEVO
        var titlePopup = $('.titleForm').text();
        //INICIO POPUP NUEVA OPCION DE MENU
        $('#frmPopup').dialog({autoOpen:false,width:800,height:350,autoSize:true,modal:true,resizable:false,closeOnEscape:true,title:titlePopup,
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){
                    if(!($('#frmHtmlStructure').valid())){
                        return false;
                    }
                    var htmlStructureType=$("input[name=htmlStructureType]").attr('value');
                    var htmlCode=tinyMCE.activeEditor.getContent();
                    var width=$("input[name=width]").attr('value');
                    var height=$("input[name=height]").attr('value');
                    var idPagina=$("select[name=pagina]").attr('value');
                    var idDominio=$("select[name=dominio]").attr('value');
                    var idHtmlStructure=$("input[name=idHtmlStructurePrincipal]").attr('value');
                    $.post('/private/htmlstructure/registerHtmlStructure',
                    {idHtmlStructure:idHtmlStructure, htmlCode: htmlCode, width: width,height:height,idPagina:idPagina,idDominio:idDominio,htmlStructureType:htmlStructureType}
                    , function(r) {
                        
                        if(r.code == code_success){
                            location.href='/private/htmlstructure/index';

                        } else {
                            msgBox(r.msg,r.code);
                        }
                    },'json');
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }

        });
        //FIN POPUP NUEVA OPCION DE MENU
        var validator = $("#frmHtmlStructure").validate({
            debug:true,
            rules: {
                htmlStructureType: {
                    required: true,
                    minlength:3
                },
                width:{
                    required: true
                },
                height:{
                    required: true
                },
                pagina:{

                    required: true
                },
                dominio:{
                    required: true
                }
            },
            messages:{
                htmlStructureType: {
                    required: 'El nombre del HtmlStructure es obligatorio',
                    minlength:jQuery.format('Se requiere al menos {0} caracteres para el nombre del HtmlStructure')
                },
                width:{
                    minlength:jQuery.format('Se requiere al menos {0} caracteres para la ruta del de la mascara'),
                    required: 'La ruta de la mascara es obligatoria'
                },
                height:{
                    minlength:jQuery.format('Se requiere al menos {0} caracteres para el nombre del sitio'),
                    required: 'El nombre del sitio web es obligatorio'
                },
                pagina:{
                    
                    required: 'El nombre del sitio web es obligatorio'
                },
                dominio:{
                    required: 'El nombre del sitio web es obligatorio'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $("#"+element.id).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $("#"+element.id).removeClass('ui-state-error');
            },
            success: function(label) {
                //label.text("ok!").addClass("success");
            }
        });
        //FIN VALIDADOR

    })

</script>

<input id="btnNewHtmlStructure" type="button" class="btn" value="<?php echo __("Nueva Estructura HTML"); ?>"/>
<center><div class="titleTables"><?php echo __("Administración de HtmlStructures"); ?></div></center>

<span class="titleForm"><?php echo __("Estructura HTML"); ?></span>
<span class="titleConfirmBox"><?php echo __("Eliminar Estructura HTML"); ?></span>
<span class="msgConfirmBox"><?php echo __("¿Está seguro de eliminar este Estructura HTML?"); ?></span>
<div id="frmPopup" class="none">
    <form id="frmHtmlStructure" method="post" action="" class="frm" >
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div class="superMenu">
                    <label><?php echo __("Nombre"); ?>:</label>
                    <input id="htmlStructureType" name="htmlStructureType" value="" class="ui-widget ui-widget-content ui-corner-all" type="text" maxlength="20" size="30"/>
                </div>
                <div>
                    <label><?php echo __("Ancho"); ?>:</label>
                    <input id="width" name="width" value="" type="text" maxlength="20" class="ui-widget ui-widget-content ui-corner-all" size="30" />
                </div>
                <div>
                    <label><?php echo __("Alto"); ?>:</label>
                    <input id="height" name="height" value="" type="text" maxlength="50" class="ui-widget ui-widget-content ui-corner-all" size="40" />
                </div>
                <div>
                    <label><?php echo __("Página"); ?>:</label>
                    <?php echo Helper::selectAll('pagina', 'idPagina', 'nombre', 'idPagina', NULL, "ui-widget ui-widget-content ui-corner-all") ?>
                </div>
                <div>
                    <label><?php echo __("Dominio"); ?>:</label>
                    <?php echo Helper::selectAll('dominio', 'idDominio', 'serverName', 'idDominio', NULL, "ui-widget ui-widget-content ui-corner-all") ?>
                </div>

            </div>
            <div>
                <textarea class="ui-widget ui-widget-content ui-corner-all"  id="elm1" name="elm1" rows="15" cols="80" style="width: 90%">
				
                </textarea>
            </div>
        </fieldset>
        <input type="hidden" name="idHtmlStructurePrincipal" id="idHtmlStructurePrincipal" value="0" />

    </form>
</div>