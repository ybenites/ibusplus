<script type="text/javascript">
    var menu_name_req_msg = '<?php echo __("El nombre del menú es obligatorio"); ?>';
    var menu_name_min_msg = '<?php echo __("Se requiere al menos {0} caracteres para el nombre del menú"); ?>';
    var menu_route_req_msg = '<?php echo __("La ruta del menú es obligatoria"); ?>';
    var menu_route_min_msg = '<?php echo __("Se requiere al menos {0} caracteres para la ruta del menú"); ?>';
    var menu_module_msg = '<?php echo __("Debe seleccionar el módulo"); ?>';
    var menu_only_msg = '<?php echo __("Debe seleccionar un item de tipo menú"); ?>';
    var menu_lvl_confirm = '<?php echo __("¿Desea mover el menú [S] al menú [D]?"); ?>';
    var menu_lvl_title = '<?php echo __("Cambio de Nivel del Menú"); ?>';
    var menu_list_menu = '/private/menu/listMenuXML?nodeid=0&n_level=0';
    var menu_save_menu = '/private/menu/createOrUpdateMenu'+jquery_params;
    var menu_order_menu = '/private/menu/listMenus'+jquery_params;
    var menu_save_order_menu = '/private/menu/saveOrderMenu'+jquery_params;
    var menu_find_menu = '/private/menu/getMenuById'+jquery_params;
    var menu_modules_menu = '/private/module/listModulesForMenu'+jquery_params;
    var menu_change_level= '/private/menu/changeMenuLevel'+jquery_params;
    $(document).ready(function(){
        $("#isSubMenu").buttonset();
        $('#btnHelp').show();    
        //INICIO VALIDADOR
        var validator = $("#frmMenu").validate({
            debug:true,
            rules: {
                menu_name: {
                    required: true,
                    minlength:3
                },
                menu_url:{
                    minlength:3,
                    required: true
                },
                module:{
                    required: true
                }
            },
            messages:{
                menu_name: {
                    required: menu_name_req_msg,
                    minlength:jQuery.format(menu_name_min_msg)
                },
                menu_url:{
                    minlength:jQuery.format(menu_route_min_msg),
                    required: menu_route_req_msg
                },
                module:{
                    required: menu_module_msg
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            }
        });
        //FIN VALIDADOR
        
        function listModules(){
            //var data = new Array();
            /*if(idMenu!=null){
                data.push(idMenu);
            }*/
            $.ajax({
                url:menu_modules_menu,
                //data:{id:data},
                async:false,
                success:function(response){
                    $('option', '#module').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#module').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#module').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                            });
                            /*if(idModule!=null){
                                $('#module').val(idModule);
                            }*/
                        } else {
                            $('#module').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listModules();
        
        var tbl_menu_jqGrid = $('#menu_jqGrid').jqGrid({
            treeGrid:true,
            treeGridModel : 'adjacency',
            ExpandColumn:'name',
            url:menu_list_menu,
            datatype:'xml',
            mtype:'POST',
            colNames:['ID','<?php echo __("Nombre"); ?>','<?php echo __("Ruta"); ?>','ico','rtype','<?php echo __("Tipo"); ?>'
                ,'<?php echo __("Acciones"); ?>'
            ],
            colModel:[
                {name:'idMenu',index:'idMenu', width:1,hidden:true,key:true},
                {name:'name',index:'name',width:350},
                {name:'url',index:'url',width:350},
                {name:'ico',index:'ico',width:50},
                {name:'rtype',index:'rtype',width:50,hidden:true},
                {name:'type',index:'type',width:50},
                {name:'<?php echo __("Acciones"); ?>', index:'<?php echo __("Acciones"); ?>', width:120, sortable:false}
            ],
            height:'auto',
            pager : "#menu_jqGrid_pager",
            caption: "Menu",
            onSelectRow:function(rowid,status){
                if(change_level){
                    var rowDataD = $('#menu_jqGrid').jqGrid('getRowData',rowid);
                    var dId = eval($('#clMenuSelected').data('idr'));
                    var rowDataS = $('#menu_jqGrid').jqGrid('getRowData',dId);
                    if(rowDataD.rtype=='M'){
                        var msg = menu_lvl_confirm.replace('[S]','<b>'+rowDataS.name+'</b>','g').replace('[D]','<b>'+rowDataD.name+'</b>','g');
                        confirmBox(msg, menu_lvl_title, function(uResponse){
                            if(uResponse){
                                changeMenuLevel(dId,rowid);
                            }
                        });

                    }else{
                        msgBox(menu_only_msg);
                    }
                }
                //console.log($('#menu_jqGrid').jqGrid('getRowData',rowid));
            }
        });

        //INICIO CLICK NUEVO MENU
        $("#btnNewMenu").click(function(){
            validator.resetForm();
            $('.frmSubtitle').text('<?php echo __("Nuevo Menú"); ?>');
            $('.superMenu').hide();
            $('.icon_section').show();
            $('.option_group').show();
            $("#menu_name").val('');
            $('#idMenu').val(0);
            $('#idSuperMenu').val(0);
            $('#menu_url').attr("readonly", true);
            $('#menu_url').val("***");
            $('#menu_icon').attr("readonly", true);
            $('#menu_icon').val('');
            $('#idModule').val('');
            $('#idOption').val('');
            listModules();
            $('#frmPopup').dialog('open');
            setRadio('isSubMenuNO');
        });
        //FIN CLICK MENU NUEVO

        var titlePopup = $('.titleForm').text();

        //INICIO POPUP NUEVA OPCION DE MENU
        $('#frmPopup').dialog({autoOpen:false,width:500,autoSize:true,modal:true,resizable:false,closeOnEscape:true,title:titlePopup,
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){
                    if(!($('#frmMenu').valid())){
                        return false;
                    }
                    var frm = $('#frmMenu').serialize();
                    $.ajax({
                        url: menu_save_menu,
                        data:frm,
                        success: function(r){
                            if(evalResponse(r)){
                                tbl_menu_jqGrid.trigger('reloadGrid');
                                $('#frmPopup').dialog('close');
                            } 
                        }
                    });
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }

        });
        //FIN POPUP NUEVA OPCION DE MENU

        //INICIO CLICK EDITAR MENU
        $('.ui-jqgrid-ico-edit').live('click',function(){
            var idMenu = $(this).attr("rel");
            var readOnly = ($('tr[id="'+idMenu.toString()+'"] > td[aria-describedby*="menu_jqGrid_parent"]').attr('title')=='NULL');
            setSuperMenu(idMenu,readOnly);
            $.ajax({
                url:menu_find_menu,
                data:{idMenu:idMenu},
                success: function(response){
                    if(evalResponse(response)){
                        listModules();
                        $('.frmSubtitle').text('<?php echo __("Editar Menú"); ?>');
                        $('#menu_name').val(response.data.name);
                        $('#idMenu').val(response.data.idMenu);
                        $('#menu_url').attr("readonly",readOnly);
                        $('#menu_url').val(response.data.url);
                        $('#menu_icon').val(response.data.icon);
                        $('#menu_icon').attr("readonly",true);
                        $('#menu_type').val(response.data.type);
                        $('#idOption').val(response.data.optionKey);
                        $('#module').val(response.data.idModule);
                        if(response.data.type=='A'){
                            $('.icon_section').hide();
                        } else{
                            $('.icon_section').show();
                        }
                        if(Boolean(eval(response.data.isSubMenu))){
                            setRadio('isSubMenuYES');  
                        }else{
                            setRadio('isSubMenuNO');
                        }
                        $('.option_group').show();
                        $('#frmPopup').dialog('open');
                    } 
                }
            });
        });
        //FIN CLICK EDITAR MENU

        function setSuperMenu(idMenu,isRootMenu){
            if(!isRootMenu){
                var menu_parent_id = $('tr[id="'+idMenu.toString()+'"] > td[aria-describedby*="menu_jqGrid_parent"]').attr('title');
                var manu_parent_text = $('tr[id="'+menu_parent_id.toString()+'"] > td[aria-describedby*="menu_jqGrid_name"]').attr('title');
                $('.superMenu').show();
                $('#idSuperMenu').val(menu_parent_id);
                $('.superMenu input').val(jQuery.trim(manu_parent_text));
            } else {
                $('#idSuperMenu').val(0);
                $('.superMenu').hide();
            }
        }

        //INICIO ELIMINAR
        msgConfirmBox = $("span.msgConfirmBox").html();
        titleConfirmBox = $("span.titleConfirmBox").html();
        $(".ui-jqgrid-ico-trash").live("click",function(){
            var url =  $(this).attr("href")+jquery_params;
            var idMenu = $(this).attr("rel");
            confirmBox(msgConfirmBox,titleConfirmBox,function(response){
                if(response == true)
                {
                    $.ajax(
                    {
                        url: url,
                        data:{idu:idMenu},
                        success: function(response)
                        {
                            if(evalResponse(response)){
                                tbl_menu_jqGrid.trigger('reloadGrid');
                            }
                        }
                    });
                }
            });
            return false;
        });
        //FIN ELIMINAR

        //INICIO CLICK NUEVO SUBMENU
        $('.ui-jqgrid-ico-new-submenu').live('click',function(){
            validator.resetForm();
            var idMenu = $(this).attr("rel");
            var text_superMenu = $('tr[id="'+idMenu.toString()+'"] > td[aria-describedby*="menu_jqGrid_name"]').attr('title');
            $('.frmSubtitle').text('<?php echo __("Nuevo Submenu"); ?>');
            $('.superMenu').show();
            $('.option_group').show();
            $('#idSuperMenu').val(idMenu);
            $('.superMenu input').attr('value',jQuery.trim(text_superMenu));
            $("#menu_name").val('');
            $('#idMenu').val(0);
            $('#menu_url').attr("readonly", false);
            $('#menu_url').val('');
            $('#menu_type').val('M');
            $('.icon_section').show();
            $('#menu_icon').attr("readonly", true);
            $('#menu_icon').val('');
            $('#idOption').val('');
            listModules();
            setRadio('isSubMenuNO');
            $('.submenu_group').show();
            $('#frmPopup').dialog('open');
        });
        //FIN CLICK NUEVO SUBMENU

        //INICIO CLICK NUEVA ACCION PHP
        $('.ui-jqgrid-ico-php-action').live('click',function(){
            validator.resetForm();
            var idMenu = $(this).attr("rel");
            var text_superMenu = $('tr[id="'+idMenu.toString()+'"] > td[aria-describedby*="menu_jqGrid_name"]').attr('title');
            $('.frmSubtitle').text('<?php echo __("Nueva Acción PHP"); ?>');
            /*$('.superMenu').show();*/
            $('.superMenu').hide();
            $('.option_group').hide();
            $('#idSuperMenu').val(idMenu);
            $('.superMenu input').attr('value',jQuery.trim(text_superMenu));
            $("#menu_name").val('');
            $('#menu_url').attr("readonly", false);
            $('#idMenu').val(0);
            $('#menu_url').val("");
            $('#menu_type').val("A");
            $('.icon_section').hide();
            $('#frmPopup').dialog('open');
        });
        //FIN CLICK NUEVA ACCION PHP

        $('#btnOrderMenu, .ui-jqgrid-ico-order').live('click',(function(){
            var dataString='';
            if($(this).attr('id')!='btnOrderMenu'){
                dataString ='menu_id='+$(this).attr('rel');
            }
            $.ajax({
                url:menu_order_menu,
                data:dataString,
                success: function(r){
                    var str="";
                    $('#menu_list').html('');
                    if(r.data.length>0){
                        $.each(r.data,function(){
                            str += '<li class="ui-state-default ui-corner-all" id="menu_'+this.idMenu+'" title="'+this.name+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+this.name+'</li>';
                            $('#menu_list').html(str);
                        });

                        $( "#menu_list" ).sortable({
                            placeholder: "ui-state-highlight"
                        });
                    } else{
                        $('#menu_list').html('<center><?php echo __("Este menú no tiene submenús para ordernar"); ?></center>');
                    }
                    $('#orderMenuPopup').dialog('open');
                }
            });
        }));

        $('#orderMenuPopup').dialog({autoOpen:false,width:500,height:300,autoSize:true,modal:true,resizable:false,closeOnEscape:true,title:'<?php echo __("Ordenar Menú"); ?>',
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){
                    var order = $("#menu_list").sortable("serialize");
                    $.ajax({
                        url:menu_save_order_menu,
                        data:order,
                        success: function(response)
                        {
                            if(evalResponse(response)){
                                tbl_menu_jqGrid.trigger('reloadGrid');
                                $('#orderMenuPopup').dialog('close');
                            } 
                        }
                    });
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                }
            }
        });
        var change_level = false;
        $(".ui-jqgrid-ico-order-level").live("click",function(){
            //var idMenu = $(this).attr("rel");
            var idRow  = $(this).parents('tr').attr('id');
            activeReorderMenu(idRow);
        });
        
        function activeReorderMenu(idRow){
            $('.ui-jqgrid-ico-order-level').hide();
            $('td[title="'+idRow+'"]').parent().fadeOut('fast');
            var rowData = $('#menu_jqGrid').jqGrid('getRowData',idRow);
            $('#clMenuSelected div.ui-state-highlight').text(rowData.name);
            $('#menu_jqGrid').jqGrid('hideCol','<?php echo __("Acciones"); ?>');
            $('button#btnCancelCL').data('idr',idRow);
            $('button#btnTopLevel').data('idr',idRow);
            $('#clMenuSelected').data('idr',idRow);
            $('#clMenuSelected').fadeIn('fast');
            change_level = true;
        }
        
        function deactiveReorderMenu(idRow){
            $('.ui-jqgrid-ico-order-level').show();
            if(idRow!=null)
                $('td[title="'+idRow+'"]').parent().fadeIn('fast');
            $('#clMenuSelected div.ui-state-highlight').text('');
            $('#menu_jqGrid').jqGrid('showCol','<?php echo __("Acciones"); ?>');
            $('button#btnCancelCL').data('idr','');
            $('button#btnTopLevel').data('idr','');
            $('#clMenuSelected').data('idr','');
            $('#clMenuSelected').fadeOut('fast');
            change_level = false;
        }
        
        $('button#btnCancelCL').button({
            icons:{
                primary:'ui-icon-circle-close'
            }
        }).click(function(){
            deactiveReorderMenu($(this).data('idr'));
        });
        
        $('button#btnTopLevel').button({
            icons:{
                primary:'ui-icon-circle-arrow-n'
            }
        }).click(function(){
            changeMenuLevel($(this).data('idr'),null);
        });
        
        function changeMenuLevel(idSourceMenu,idDestinationMenu){
            showLoading = 1;
            var obj = new Object();
            obj.idsm = idSourceMenu;
            if(idDestinationMenu!=null){obj.iddm = idDestinationMenu;}
            $.ajax({
                url:menu_change_level,
                data:obj,
                success:function(response){
                    if(evalResponse(response)){
                        tbl_menu_jqGrid.trigger('reloadGrid');
                        deactiveReorderMenu();
                    }
                }
            });
        }
    });
    function icon(url_img){
        $('#menu_icon').val(url_img);
    }
</script>

<style type="text/css">


    #menu_list { list-style-type: none; margin: 0; padding: 2px; width: auto; text-align: left;}

    #menu_list li { margin: 5px 5px 5px 5px; padding: 5px; font-size: 1.2em; min-height: 15px; padding-left: 1.5em;}

    #menu_list li span { display: block; float: left; margin-left: -1.3em; }

    #clMenuSelected{
        width: 240px;
        min-height: 50px;
        position: absolute;
        top: 0;
        right: 10px;
        padding: 4px;
        text-align: center;
    }
    #clMenuSelected div.ui-state-highlight{
        line-height: 14px;
        font-size: 1.7em;
        padding-bottom: 8px;
        padding-top: 8px;
        margin-top: 8px;
        margin-bottom: 8px;
        margin-left: 5px;
        margin-right: 5px;
    }
    #clMenuSelected div.menu_info{

        font-size: 0.9em;
        padding: 4px;
        padding-top: 0px;
    }


</style>

<input id="btnNewMenu" type="button" class="btn" value="<?php echo __("Nuevo Menú"); ?>"/>
<input id="btnOrderMenu" type="button" class="btn" value="<?php echo __("Ordenar Menú"); ?>"/>
<center><div class="titleTables"><?php echo __("Administración de Menús"); ?></div></center>
<center>
    <table id="menu_jqGrid"></table>
    <div id="menu_jqGrid_pager"></div>
</center>

<span class="titleForm"><?php echo __("Menú"); ?></span>
<span class="titleConfirmBox"><?php echo __("Eliminar Menú"); ?></span>
<span class="msgConfirmBox"><?php echo __("¿Está seguro de eliminar este menú?"); ?></span>

<div id="frmPopup" class="none">
    <form id="frmMenu" method="post" action="" class="frm" >
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div class="superMenu">
                    <label><?php echo __("Menu padre"); ?>:</label>
                    <input id="super_menu_name" name="super_menu_name" value="" type="text" maxlength="150" size="30" readonly="true" disabled="true"/>
                </div>
                <div>
                    <label><?php echo __("Nombre"); ?>:</label>
                    <input id="menu_name" name="menu_name" value="" type="text" maxlength="150" size="30" />
                </div>
                <div>
                    <label><?php echo __("Ruta"); ?>:</label>
                    <input id="menu_url" name="menu_url" value="" type="text" maxlength="250" size="35" />
                </div>
                <div>
                    <label><?php echo __("Módulo"); ?>:</label>
                    <select id="module" name="module"></select>
                </div>
                <div class="option_group">
                    <label><?php echo __("Asociar a la Opción"); ?>:</label>
                    <?php echo Helper::comboSimple($options, 'key', 'key', 'idOption') ?>
                </div>
                <div class="submenu_group">
                    <label><?php echo __("¿Es Sub Menú?"); ?>:</label>
                    <div id="isSubMenu">
                        <input type="radio" id="isSubMenuYES" name="isSubMenu" value="1"/><label for="isSubMenuYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                        <input type="radio" id="isSubMenuNO" name="isSubMenu" checked="checked" value="0"/><label for="isSubMenuNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                    </div>
                </div>
                <!--div class="icon_section">
                    <label><?php echo __("Icono"); ?>:</label>
                    <input id="menu_icon" name="menu_icon" value="" type="text" size="30"/>
                </div-->
            </div>
            <br/>
            <!--div  align="center" style="overflow:auto; height:80%; width:100%;" class="icon_section">
                <table width="50%" style="" class="ui-widget-content ui-corner-all">
            <?php Helper::getImages('media/ico') ?>
                </table>
            </div-->
        </fieldset>
        <input type="hidden" name="idMenu" id="idMenu" value="0" />
        <input type="hidden" name="idSuperMenu" id="idSuperMenu" value="0" />
        <input type="hidden" name="menu_type" id="menu_type" value="<?php echo $menu_type; ?>" />
    </form>
</div>

<div id="orderMenuPopup">
    <form dir="frmOrderMenu">
        <ul id="menu_list">

        </ul>
    </form>
</div>

<div id="clMenuSelected" class="ui-corner-all ui-widget-content ui-helper-hidden">
    <div><?php echo __("Menú para cambio de nivel"); ?></div>
    <div class="ui-corner-all ui-state-highlight"></div>
    <div class="menu_info"><?php echo __("Seleccione el menú de destino"); ?></div>
    <div>
        <button id="btnTopLevel" class="btn"><?php echo __("Subir al primer Nivel"); ?></button>
        <button id="btnCancelCL" class="btn"><?php echo __("Cancelar"); ?></button>
    </div>
</div>