<script type="text/javascript">
    var group_form_title = '<?php echo __("Grupo"); ?>';
    var group_name_required = '<?php echo __('El nombre del grupo es obligatorio'); ?>';
    var group_name_min_required = '<?php echo __('Se requiere al menos {0} caracteres para el nombre del grupo'); ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Grupo"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este grupo?"); ?>';
    var root_option = Boolean(eval('<?php echo $user_rootOption ?>'));
    var url_group = '/private/group/listGroups'+jquery_params;
    var url_trash = '/private/group/removeGroup'+jquery_params;
    var url_find = '/private/group/getGroupById'+jquery_params;
    var ico_on = '<img src="/media/ico/ico_power_on.png" />';
    var ico_off = '<img src="/media/ico/ico_power_off.png" />';
    $(document).ready(function(){
        $("#rootOption,#messages_privileges_write,#messages_privileges_reply").buttonset();
        $("#frmGroup").validate({
            rules: {
                group_name: {
                    required: true,
                    minlength:3
                }
            },
            messages:{
                group_name: {
                    required: group_name_required,
                    minlength:jQuery.format(group_name_min_required)
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                $.ajax({
                    type: "POST",
                    url: '/private/group/createOrUpdateGroup'+jquery_params,
                    data:$(form).serialize(),
                    dataType:'json',
                    success: function(response){
                        if(evalResponse(response)){
                            $('#group_jqGrid').trigger('reloadGrid');
                            $('#frmPopup').dialog('close');
                        }
                    }
                });
            }
        });
        
        $('#frmPopup').dialog({
            title:group_form_title,
            width:500,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmGroup").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        
        $('#btnNewGroup').click(function(){
            $("#group_name").val('');
            $('#group_id').val('');
<?php if ((bool) $user_rootOption) { ?>
            $('#group_home_page').val('');    
<?php } ?>
            $('#frmPopup').dialog('open');
        })
        
        $('#group_jqGrid').jqGrid({
            url:url_group,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:300,
            rowList: [30,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Opción ROOT") ?>',
                'systemDefault',
                '<?php echo __("Escribir Mensajes") ?>',
                '<?php echo __("Responder Mensajes") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'name',index:'name',sortable:true,width:160,searchoptions:{sopt:['cn']}},
                {name:'rootOption',index:'rootOption',sortable:false,width:70,hidden:(!root_option),search:false,align:'center'},
                {name:'systemDefault',index:'systemDefault',hidden:true},
                {name:'allowWriteMessages',index:'allowWriteMessages',sortable:false,width:70,search:false,align:'center'},
                {name:'allowReplyMessages',index:'allowReplyMessages',sortable:false,width:70,search:false,align:'center'},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,search:false}
            ],
            pager: "#group_jqGrid_pager",
            sortname: 'name',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(Boolean(eval(rowdata.rootOption))){
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_on});
                } else {
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_off});
                }
                if(Boolean(eval(rowdata.allowWriteMessages))){
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{allowWriteMessages:ico_on});
                } else {
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{allowWriteMessages:ico_off});
                }
                if(Boolean(eval(rowdata.allowReplyMessages))){
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{allowReplyMessages:ico_on});
                } else {
                    $("#group_jqGrid").jqGrid('setRowData',rowid,{allowReplyMessages:ico_off});
                }
            },
            gridComplete:function(){
                var ids = $('#group_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = "<a class=\"edit\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash="";
                    rowData = $('#group_jqGrid').jqGrid('getRowData',ids[i]);
                    if(!Boolean(eval(rowData.systemDefault))){
                        trash = "<a class=\"trash\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    }
                    $("#group_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $("#group_name").val(response.data.name);
                                $('#group_id').val(response.data.idGroup);
<?php if ((bool) $user_rootOption) { ?>
                                if(Boolean(eval(response.data.rootOption))){
                                    setRadio('rootOptionYES');
                                } else {
                                    setRadio('rootOptionNO');
                                }
                                 $('#group_home_page').val(response.data.defaultHomePage);
<?php } ?>
                                if(Boolean(eval(response.data.allowWriteMessages))){
                                    setRadio('messages_privileges_write_yes');
                                } else {
                                    setRadio('messages_privileges_write_no');
                                }
                                if(Boolean(eval(response.data.allowReplyMessages))){
                                    setRadio('messages_privileges_reply_yes');
                                } else {
                                    setRadio('messages_privileges_reply_no');
                                }
                                $('#frmPopup').dialog('open');
                            }
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                        if(response == true){
                            $.ajax({
                                url:url_trash,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#group_jqGrid').trigger('reloadGrid');
                                    } 
                                }
                            });
                        }
                    });
                });
                return false;
            }
        });

        $("#group_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
    });
</script>
<style type="text/css">
    #jqgh_group_jqGrid_rootOption,#jqgh_group_jqGrid_allowWriteMessages,#jqgh_group_jqGrid_allowReplyMessages{
        height: 30px;
        white-space: pre-wrap;
    }
</style>
<button id="btnNewGroup" class="btn" ><?php echo __("Nuevo Grupo"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Grupos"); ?></div></center>
<center>
    <table id="group_jqGrid"></table>
    <div id="group_jqGrid_pager"></div>
</center>

<div id="frmPopup" class="dialog-modal">
    <form id="frmGroup" method="post">

        <div class="label_output">
            <div>
                <label><?php echo __("Nombre"); ?>:</label>
                <input id="group_name"  name="group_name" value="" type="text" maxlength="150" size="30" />
            </div>
            <?php if ((bool)$user_rootOption) { ?>
                <div>
                    <label><?php echo __("Página de Inicio"); ?>:</label>
                    <input id="group_home_page" name="group_home_page" value="" type="text" maxlength="256" size="30" />
                </div>
                <div>
                    <label style="margin-top: 7px;"><?php echo __("Opción ROOT"); ?>:</label>
                    <div id="rootOption">
                        <input type="radio" id="rootOptionYES" name="rootOption" value="1"/><label for="rootOptionYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                        <input type="radio" id="rootOptionNO" name="rootOption" checked="checked" value="0"/><label for="rootOptionNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                    </div>
                </div>
            <?php } ?>

            <div>
                <label style="margin-top: 7px;"><?php echo __("Escribir Mensajes"); ?>:</label>
                <div id="messages_privileges_write">
                    <input type="radio" id="messages_privileges_write_yes" name="messages_privileges_write" value="1"/><label for="messages_privileges_write_yes" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="messages_privileges_write_no" name="messages_privileges_write" checked="checked" value="0"/><label for="messages_privileges_write_no" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
            </div>
            <div>
                <label style="margin-top: 7px;"><?php echo __("Responder Mensajes"); ?>:</label>
                <div id="messages_privileges_reply">
                    <input type="radio" id="messages_privileges_reply_yes" name="messages_privileges_reply" value="1"/><label for="messages_privileges_reply_yes" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="messages_privileges_reply_no" name="messages_privileges_reply" checked="checked" value="0"/><label for="messages_privileges_reply_no" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
            </div>
        </div>

        <input type="hidden" name="group_id" id="group_id" value="" />
    </form>
</div>