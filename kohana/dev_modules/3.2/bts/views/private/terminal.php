<script  type="text/javascript">
    $(document).ready(function(){
        var urlBrands = '/private/printers/listPrintBrands'+jquery_params;
        var urlModelsByBrand = '/private/printers/getPrinterByBrandModel'+jquery_params;
        var urlPrintOS = '/private/terminal/listAllOS'+jquery_params;
        var urlBrowsers = '/private/terminal/listAllBrowsers'+jquery_params;
        var urlOffices = '/private/terminal/listOffices'+jquery_params;
        var msgSelectDefaultOption='<?php echo __("Seleccione una opción"); ?>';
        var msgSelectEmpty='<?php echo __("No se encontró información"); ?>';
        var frmPopupTerminalTitle='<?php echo __("Terminal"); ?>';
        var frmPopupNewTerminal='<?php echo __("Nuevo Terminal"); ?>';
        var msg_terminal_office='<?php echo __("Debe Seleccionar una Oficina"); ?>';
        var msg_terminal_os='<?php echo __("Debe Seleccionar un Sistema Operativo"); ?>';
        var msg_terminal_browser='<?php echo __("Debe Seleccionar un Navegador Web"); ?>';
        var msg_print_url='<?php echo __("Debe ingresar la ruta de impresión    "); ?>';
        var msgSelectABrand='<?php echo __("Seleccione una Marca"); ?>';
        var msgSelectAModel='<?php echo __("Seleccione un Modelo"); ?>';
        var option_template = '<option value="[VALUE]">[LABEL]</option>';
        var url_terminals = '/private/terminal/listTerminals'+jquery_params;
        $("#frmTerminal").validate({
            rules: {terminal_office:{required:true},terminal_os: {required: true},terminal_browser:{required:true},print_brand:{required:true},terminal_printer:{required:true},print_url:{required:true}},
            messages:{terminal_office:msg_terminal_office,terminal_os:msg_terminal_os,terminal_browser:msg_terminal_browser,print_brand:msgSelectABrand,terminal_printer:msgSelectAModel,print_url:msg_print_url},
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');}
        });
        
        $('#frmPopupTerminal').dialog({
            autoOpen:false,
            height:330,
            width:410,
            modal:true,
            resizable:false,
            title:frmPopupTerminalTitle,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    if(!$("#frmTerminal").valid()) return false;
                    var data = $("#frmTerminal").serialize();
                    loadingcarga = 1;
                    $.ajax({
                        url:'/private/terminal/createTerminal',
                        data:data,
                        success:function(r){
                            if(r.code==code_success){
                                $('#terminals_jqGrid').trigger('reloadGrid');
                                $('#frmPopupTerminal').dialog('close');
                            }else {
                                msgBox(r.msg,r.code);
                            }
                        }
                    });
                },
                '<?php echo __('Cancelar') ?>':function(){
                    $('#errorMessages').hide();
                    $(this).dialog('close');
                }
            }
        });
        
        $('#btnNewTerminal').click(function(){
            $('.rCombo[itemtype="b"]').trigger('click', {rt:'b'});
            loadBrowsers();
            loadOS();
            loadOffices();
            $('.frmSubtitle').text(frmPopupNewTerminal);
            $('#terminal_id').val('');
            $('#terminal_os').val('');
            $('#terminal_browser').val('');
            $('#print_brand').val('');
            $('#terminal_printer').val('');
            $('#frmPopupTerminal').dialog('open');
        });
        
        $('#print_brand').change(function(){
            $('option', '#terminal_printer').remove();
            if($(this).val()!=''){
                loadingcarga=1;
                var data = {idb:$(this).val()};
                $.ajax({
                    url:urlModelsByBrand,
                    data:data,
                    success:function(r){
                        if(r.code==code_success){
                            if(r.data.length>0){
                                $('#terminal_printer').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                                $.each(r.data, function(index, array) {
                                    $('#terminal_printer').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['value'], 'g'));
                                });
                            } else {
                                $('#terminal_printer').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                            }
                        }else {
                            msgBox(r.msg,r.code);
                        }
                    }
                });
            } else{
                $('#terminal_printer').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectABrand, 'g'));
            }
        });
        
        function loadOffices(){
            $.ajax({
                url:urlOffices,
                success:function(r){
                    if(r.code == code_success){
                        $('option',$('#terminal_office')).remove();
                        $('#terminal_office').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));  
                        $.each(r.data, function(index, array) {
                            $('#terminal_office').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['value'], 'g'));
                        });
                    }else{
                        msgBox(r.msg,r.code);
                    }
                }
            });
        }
        
        function loadOS(){
            $.ajax({
                url:urlPrintOS,
                success:function(r){
                    if(r.code == code_success){
                        $('option',$('#terminal_os')).remove();
                        $('#terminal_os').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));  
                        $.each(r.data, function(index, array) {
                            $('#terminal_os').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['value'], 'g'));
                        });
                    }else{
                        msgBox(r.msg,r.code);
                    }
                }
            });
        }
        
        function loadBrowsers(){
            $.ajax({
                url:urlBrowsers,
                success:function(r){
                    if(r.code == code_success){
                        $('option',$('#terminal_browser')).remove();
                        $('#terminal_browser').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));  
                        $.each(r.data, function(index, array) {
                            $('#terminal_browser').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['value'], 'g'));
                        });
                    }else{
                        msgBox(r.msg,r.code);
                    }
                }
            });
        }
        
        $('.rCombo').click(function(event,params){
            var type = $(this).attr('itemtype');
            var select = $(this).prev('select');
            var dMessage;
            var url = '';
            var data = '';
            if(type=='b' || (params!=null && params.rt=='b')){
                url = urlBrands
                dMessage = msgSelectDefaultOption;
                data = {t:type}
                $('option', $('#terminal_printer')).remove();
            } else {
                url = urlModelsByBrand
                dMessage = msgSelectABrand;
                var id = $('#print_brand').val();
                data = {t:type,idb:id}
            }
            $('option', select).remove();
            $(select).append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',dMessage, 'g'));                
            loadingcarga=1;
            $.ajax({
                url:url,
                data:data,
                success:function(r){
                    if(r.code==code_success){
                        if(r.data.length>0){
                            $('option', select).remove();
                            $(select).append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g')); 
                            $.each(r.data, function(index, array) {
                                $(select).append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['value'], 'g'));
                            });
                        } else {
                            $('option', select).remove();
                            $(select).append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));                
                        }
                    }else {
                        msgBox(r.msg,r.code);
                    }
                }
            });
        });
        
        $('#selectOffice').change(function(){
            if($(this).val()!=''){
                $("#terminals_jqGrid").setGridParam({url:url_terminals+'&selectOffice='+$(this).val()}).trigger("reloadGrid");
            } else {
                $("#terminals_jqGrid").setGridParam({url:url_terminals}).trigger("reloadGrid");
            }
        });
        
        $('#terminals_jqGrid').jqGrid({
            url:url_terminals,
            datatype: 'json',
            mtype: 'POST',
            rowNum: 20,
            width:600,
            height:280,
            rowList: [20,40,60,80,100],
            colNames:[
                'id',
                '<?php echo __("Oficina") ?>',
                '<?php echo __("Sistema Operativo") ?>',
                '<?php echo __("Navegador") ?>',
                '<?php echo __("Impresora") ?>'
            ],
            colModel:[
                {name:'id',index:'id', hidden:true},
                {name:'office',index:'office'},
                {name:'so',index:'so'},
                {name:'browser',index:'browser'},
                {name:'printer',index:'printer'}
            ],
            pager: "#terminals_jqGrid_pager",
            sortname: 'office',
            sortable: true,
            grouping: true,
            rownumbers: true,       
            rownumWidth: 40,
            groupingView : {
                groupField : ['office'],
                groupColumnShow : [false],
                groupText : ['<b>{0} - {1} Item(s)</b>'],
                groupCollapse : false,
                groupOrder: ['asc'] },
            caption: '<?php echo __("Terminales en Oficinas"); ?>'
        });
    });
</script>
<style type="text/css" >
    .rCombo{
        cursor: pointer; 
        padding-left: 1px;
    }

    .label_output div label{
        min-width: 140px;
    }
    
</style>
<input id="btnNewTerminal" type="button" class="btn" value="<?php echo __("Nuevo Terminal"); ?>"/>
<center><div class="titleTables"><?php echo __("Administración de Terminales"); ?></div></center>

<div class="ui-corner-all ui-widget-content" style="margin: 5px; width: 880px; margin: 0 auto;">

    <div class="label_output">
        <div>
            <label for="selectOffice"><?php echo __("Oficina"); ?>:</label>
            <?php echo Helper::comboSimple($aOffice, 'idOffice', 'name', 'selectOffice', null, __('Todas')); ?>
        </div>
    </div>
</div>
<br/>
<center>
    <table id="terminals_jqGrid"></table>
    <div id="terminals_jqGrid_pager"></div>
</center>

<div id="frmPopupTerminal" class="none">
    <form id="frmTerminal" method="post" action="" class="frm">
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div>
                    <label><?php echo __("Oficina"); ?>:</label>
                    <select id="terminal_office" name="terminal_office" style="width: 170px;">
                        <option value=""></option>
                    </select>
                </div>
                <div>
                    <label><?php echo __("Sistema Operativo"); ?>:</label>
                    <select id="terminal_os" name="terminal_os" style="width: 170px;">
                        <option value=""></option>
                    </select>
                </div>
                <div>
                    <label><?php echo __("Navegador"); ?>:</label>
                    <select id="terminal_browser" name="terminal_browser" style="width: 170px;">
                        <option value=""></option>
                    </select>
                </div>
                <div>
                    <label><?php echo __("Marca de Impresora"); ?>:</label>
                    <select id="print_brand" name="print_brand" style="width: 170px;">
                        <option value=""></option>
                    </select>
                    <img src="/media/ico/refresh.png" alt="[<?php echo __('Actualizar') ?>]" class="rCombo" itemtype="b" title="[<?php echo __('Actualizar') ?>]"/>
                </div>
                <div>
                    <label><?php echo __("Impresora"); ?>:</label>
                    <select id="terminal_printer" name="terminal_printer" style="width: 170px;">
                        <option value=""></option>
                    </select>
                    <img src="/media/ico/refresh.png" alt="[<?php echo __('Actualizar') ?>]" class="rCombo" itemtype="m" title="[<?php echo __('Actualizar') ?>]"/>
                </div>
            </div>
            <input id="terminal_id" name="terminal_id" type="hidden"/>
        </fieldset>
    </form>
</div>