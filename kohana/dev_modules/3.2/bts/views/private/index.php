<script type="text/javascript">
    $(document).ready(function(){
        var url_avaible_printers = '/private/welcome/listAvaiblePrintersByUserOffice'+jquery_params;
        $('#printers_jqGrid').jqGrid({
            url:url_avaible_printers,
            datatype: 'json',
            mtype: 'POST',
            rowNum: 10,
            
            rowList: [5,10],
            colNames:[
                'id',
                '<?php echo __("Sistema Operativo") ?>',
                '<?php echo __("Marca") ?>',
                '<?php echo __("Modelo") ?>',
                '<?php echo __("Navegador") ?>',
                'selected'
            ],
            colModel:[
                {name:'id',index:'id', hidden:true,key:true},
                {name:'os',index:'os',width:120},
                {name:'brand',index:'brand',width:100},
                {name:'model',index:'model',width:100},
                {name:'browser',index:'browser',width:100},
                {name:'active',index:'active',hidden:true}
            ],
            pager: "#printers_jqGrid_pager",
            sortname: 'model',
            sortable: true,
            gridview: false,
            rownumbers: true,       
            rownumWidth: 40,
            caption: '<?php echo __("Impresoras Disponibles"); ?>',
            onselectrow :true,
            onSelectRow :function(rowid){
                $.ajax({
                    url:'/private/welcome/updateSelectedTerminal'+jquery_params,
                    data:{idt:rowid},
                    success:function(r){
                        if(r.code == code_success){
                            msgBox(r.msg,r.code);
                            $('#printers_jqGrid').trigger("reloadGrid");
                        } else{
                            msgBox(r.msg,r.code);
                        }
                    }
                });
            },
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(Boolean(parseInt(rowdata.active))){
                    $('tr#'+rowid).addClass('selected_terminal');
                }
            }
        });
    });
</script>
<style type="text/css">
    .selected_terminal td{
        background-color: #2EFE64;
    }
</style>
<p style="padding: 5px; margin-bottom: 5px; font-weight: bolder; width: 150px;" class="ui-corner-all ui-widget-content">
    <?php echo __('Bienvenido Sr(a).') . ' '; ?>
</p>

<div style="padding: 5px; margin-top: 10px; font-weight: bolder;" class="ui-corner-all ui-state-highlight">
    <h2><?php echo $user_full_name; ?></h2>
</div>
<br/>
<center>
    <table id="printers_jqGrid"></table>
    <div id="printers_jqGrid_pager"></div>
</center>
