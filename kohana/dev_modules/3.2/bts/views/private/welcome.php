<p style="padding: 5px; margin-bottom: 5px; font-weight: bolder; width: 150px;" class="ui-corner-all ui-widget-content">
    <?php echo __('Bienvenido Sr(a).') . ' '; ?>
</p>
<div style="padding: 5px; margin-top: 10px; font-weight: bolder;font-size: 12px;" class="ui-corner-all ui-state-highlight">
    <h2><?php echo $user_full_name; ?></h2>
    </div>
     <?php
    if ($browser_name == 'Firefox' && $browser_version != '22.0') {
        ?>
<div style="padding: 5px; margin-top: 5px; font-weight: bolder;font-size: 12px;" class="ui-corner-all ui-state-error">
        <div style="padding: 5px; margin-top: 5px; font-weight: bolder;" class="ui-corner-all">
            <h3><?php echo __('La version de su Navegador Firefox no esta Actualizada: ') . ' '; ?> <br/>
                <?php echo __('La Ultima version es: ') . ' '; ?>
                <?php echo $browser_name; ?>
                <?php echo $browser_version; ?>
            </h3>
             <br/>
             <?php echo __('Comuniquese con el area de soporte para poder realizar la actualización') . ' '; ?>
        </div>
        <div style="padding: 5px; margin-top:5px; font-weight: bolder;" class="ui-corner-all">
            <h4><a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Español ') . ' '; ?></a> </h4>
            <h4><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Ingles ') . ' '; ?></a></h4>
            <h4><a href="http://www.google.com/Chrome" target="_blank"><?php echo __('Descargar Google Chrome') . ' '; ?></a></h4>
        </div>
        </div>
        <?php
    } elseif ($browser_name != 'Chrome' && $browser_name != 'Firefox') {
        ?>
<div style="padding: 5px; margin-top: 5px; font-weight: bolder;font-size: 12px;" class="ui-corner-all ui-state-error">
        <div style="padding: 5px; margin-top: 5px; font-weight: bolder;" class="ui-corner-all">
            <h3><?php echo __('El Navegador que esta usando no es compatible con el Sistema. Debe Cambiar a Firefox o Google Chrome. Su Navegador es:') . ' '; ?>
                <?php echo $browser_name; ?>
                <?php echo $browser_version; ?>        
            </h3>
        </div>
        <div style="padding: 5px; margin-top: 5px; font-weight: bolder;" class="ui-corner-all ui-state-highlight">
            <h4><a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Español ') . ' '; ?></a> </h4>
            <h4><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Ingles ') . ' '; ?></a></h4>
            <h4><a href="http://www.google.com/Chrome" target="_blank"><?php echo __('Descargar Google Chrome') . ' '; ?></a></h4>
        </div>
        </div>
        <?php
    }
    ?>
</br>
<div id="container"></div>