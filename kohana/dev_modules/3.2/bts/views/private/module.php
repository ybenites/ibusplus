<script type="text/javascript">
    var popupTitle = '<?php echo __("Módulo"); ?>';
    var newSubTitle = '<?php echo __("Nueva Módulo"); ?>';
    var editSubTitle = '<?php echo __("Editar Módulo"); ?>';
    var deleteSubTitle = '<?php echo __("Desactivar Módulo"); ?>';
    var activeSubTitle = '<?php echo __("Activar Módulo"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de desactivar este módulo?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Está seguro de activar este módulo?"); ?>';
    var moduleNameRequired = '<?php echo __("El nombre del módulo es necesario"); ?>';
    var moduleCodeRequired = '<?php echo __("El código del módulo es necesario"); ?>';
    var url_save = '/private/module/saveModule'+jquery_params;
    var url_list = '/private/module/listModulesXML';
    var url_find = '/private/module/getModuleById'+jquery_params;
    var url_trash = '/private/module/removeModule'+jquery_params;
    $(document).ready(function(){
        //$.mask.rules.r = /[0-9A-Z_]/;
        //$.mask.masks.mod_code = {mask: 'r', type:'repeat'};
        //$('#moduleCode').setMask();
        $('.parent_group').hide();
        $("#frmModule").validate({
            rules: {
                moduleName:{required: true},
                moduleCode:{required: true}
            },
            messages:{
                moduleName: {required: moduleNameRequired},
                moduleCode: {required: moduleCodeRequired}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element) {$(element).addClass('ui-state-error');},
            unhighlight:function(element){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                $.ajax({
                    url:url_save,
                    data:$(form).serialize(),
                    success:function(response){
                        if(evalResponse(response)){
                            $('#dlgNewModule').dialog('close');
                            $('#module_jqGrid').trigger('reloadGrid');
                        } 
                    }
                });
            }
        });
        
        $('#dlgNewModule').dialog({
            title:popupTitle,
            width:400,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmModule").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $('#btnNewModule').click(function(){
            $('#moduleName').val('');
            $('#moduleCode').val('');
            $('#moduleId').val('');
            $('#moduleParent').val('');
            $('#parentId').val('');
            $('.parent_group').hide();
            $('#dlgNewModule').dialog('open');
        })
        
        $('#module_jqGrid').jqGrid({
            treeGrid:true,
            treeGridModel : 'adjacency',
            ExpandColumn:'moduleName',
            url:url_list+'?nodeid=0&n_level=0',
            datatype:'xml',
            mtype:'POST',
            colNames:[
                'idPage',
                'cDate',
                'lcDate',
                '<?php echo __("Código") ?>',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                'status',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'moduleCode',index:'moduleCode',sortable:true,width:100},
                {name:'moduleName',index:'moduleName',sortable:true,width:160},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'status',index:'status',hidden:true,hidden:true,hidedlg:true},
                {name:'actions',index:'actions',sortable:false,width:70,align:'center',search:false}
            ],
            height:'auto',
            pager : "#module_jqGrid_pager",
            caption: "<?php echo __("Módulos") ?>"
        });

        
        $('.ui-jqgrid-ico-edit').live('click',function(){
            var id = $(this).data("ido");
            var node = $("#module_jqGrid").getRowData(id);
            var parent = $("#module_jqGrid").getNodeParent(node);
            if(parent!=null){
                $('#moduleParent').val(parent.moduleName);
                $('#parentId').val(parent.id);
                $('.parent_group').show();
                
            } else{
                $('#moduleParent').val('');
                $('#parentId').val('');
                $('.parent_group').hide();
            }
            $('#moduleName').val(node.moduleName);
            $('#moduleCode').val(node.moduleCode);
            $('#moduleId').val(node.id);
            $('#dlgNewModule').dialog('open');
        });
        $('.ui-jqgrid-ico-new-subfile').live('click',function(){
            var idPage = $(this).data("ido");
            var node = $("#module_jqGrid").getRowData(idPage);
            $('.parent_group').show();
            $('#moduleParent').val(node.moduleName);
            $('#parentId').val(node.id);
            $('#moduleCode').val('');
            $('#moduleName').val('');
            $('#moduleId').val('');
            $('#dlgNewModule').dialog('open');
        });
        
        $(".ui-jqgrid-ico-on,.ui-jqgrid-ico-off").live("click",function(){
            var id = $(this).data("ido");
            var data = $('#module_jqGrid').jqGrid('getRowData',parseInt(id));
            var msg = confirmActiveMessage;
            var tit = activeSubTitle;
            if(Boolean(eval(data.status))){
                msg = confirmDeleteMessage;
                tit = deleteSubTitle;
            }
            confirmBox(msg,tit,function(response){
                if(response == true)
                {
                    $.ajax(
                    {
                        type: "POST",
                        url: url_trash,
                        data:{id:id},
                        dataType:'json',
                        success: function(response)
                        {
                            if(evalResponse(response)){
                                $('#module_jqGrid').trigger('reloadGrid');
                            } 
                        }
                    });
                }
            });
            return false;
        });
    });
</script>
<style type="text/css">
    .label_output div label{
        min-width: 70px;
    }
</style>
<button id="btnNewModule" class="btn"><?php echo __("Nueva Módulo"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Módulos"); ?></div></center>
<center>
    <table id="module_jqGrid"></table>
    <div id="module_jqGrid_pager"></div>
</center>

<div id="dlgNewModule" class="dialog-modal">
    <form id="frmModule" method="post">
        <div class="label_output">
            <div class="parent_group">
                <label><?php echo __("Padre"); ?></label>
                <input id="moduleParent" name="moduleParent" size="30" type="text" readonly=""/>
            </div>
            <div>
                <label><?php echo __("Código"); ?></label>
                <input id="moduleCode" name="moduleCode" size="30" type="text" alt="mod_code"/>
            </div>
            <div>
                <label><?php echo __("Nombre"); ?></label>
                <input id="moduleName" name="moduleName" size="30" type="text"/>
            </div>
            <input id="moduleId" name="moduleId" type="hidden"/>
            <input id="parentId" name="parentId" type="hidden"/>
        </div>
    </form>
</div>