<script type="text/javascript">
    var url_options = '/private/options/listOptions'+jquery_params ;
    var url_find = '/private/options/getOptionById'+jquery_params ;
    var url_trash = '/private/options/removeOption'+jquery_params ;
    var url_undo = '/private/options/removeUndoOption'+jquery_params ;
    var url_autocomplete = '/private/options/getAutoCompleteOptions'+jquery_params ;
    var opt_title = '<?php echo __("Opción"); ?>';
    var cmp_title = '<?php echo __("Editar Componentes"); ?>';
    var cmp_list = '/private/options/listValuesComponent'+jquery_params ;
    var cmp_edit = '/private/options/updateEnumValuesComponent'+jquery_params ;
    var dt_title = '<?php echo __("Editar Tipo de Dato"); ?>';
    var dt_list = '/private/options/listValuesDataType'+jquery_params ;
    var dt_edit = '/private/options/updateEnumValuesDataType'+jquery_params ;
    var iList = '<li class="ui-state-default ui-corner-all" id="menu_[ID]" data-value="[VALUE]"><span class="ui-icon ui-icon-circle-close"></span>[VALUE]</li>';
    var opt_cmp_req = '<?php echo __("Seleccione un componente"); ?>';
    var opt_key_req = '<?php echo __("KEY es necesario"); ?>';
    var opt_alias_req = '<?php echo __("Ingrese el Alias"); ?>';
    var opt_dt_req = '<?php echo __("El tipo de dato es requerido"); ?>';
    var opt_desc_req = '<?php echo __("La descripción es requerida"); ?>';
    var opt_val_req = '<?php echo __("El valor es requerido"); ?>'
    var root_option = Boolean(eval('<?php echo $user_rootOption ?>'));
    var ico_on = '<img src="/media/ico/ico_power_on.png" />';
    var ico_off = '<img src="/media/ico/ico_power_off.png" />';
    var deleteSubTitle = '<?php echo __("Eliminar Opción"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar esta Opción?"); ?>';
    var undoSubTitle = '<?php echo __("Recuperar Opción"); ?>';
    var confirmUndoMessage = '<?php echo __("¿Está seguro de recuperar esta Opción?"); ?>';
    var option_order_title = '<?php echo __("Ordenar Opciones"); ?>';
    var option_order_list = '/private/options/listOptionsByComponent'+jquery_params;
    var option_order_save = '/private/options/saveOptionsOrder'+jquery_params;
    $(document).ready(function(){
        $('#dlgComponent').dialog({
            title:cmp_title,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        var data = new Array();
                        $.each($('#cmpList li'),function(){
                            data.push($(this).data('value'));
                        });
                        $.ajax({
                            url:cmp_edit,
                            data:{values:data},
                            success:function(response){
                                if(evalResponse(response)){
                                    $('#dlgComponent').dialog('close');
                                } 
                            }
                        });
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $('#btnAddCmp').button({
            icons:{primary: 'ui-icon-plusthick'},
            text: false
        }).click(function(){
            var name = $('#cmpName').val().toUpperCase().replace(' ','_','g');
            name = $.trim(name);
            if(name!=''){
                $('#cmpList').append(iList.replace('[ID]',name, 'g').replace('[VALUE]',name, 'g'));
                $('#cmpName').val('');
            }
        });
        $('.ui-icon-circle-close').live('click',function(){
            switch($(this).parent().data('value').toString()){
                case 'GENERAL':break;
                case 'boolean':break;
                case 'color':break;
                case 'decimal':break;
                case 'integer':break;
                case 'string':break;
                case 'percentage':break;
                default:
                    $(this).parent().fadeOut().remove();
                    break;
            }
        });
        $('#btnEditComponent').click(function(){
            $.ajax({
                url:cmp_list,
                success:function(response){
                    if(evalResponse(response)){
                        var str="";
                        $('#cmpList').html('');
                        if(response.data.length>0){
                            $.each(response.data,function(){
                                str += iList.replace('[ID]', this, 'g').replace('[VALUE]', this, 'g');
                                $('#cmpList').html(str);
                            });

                        } else{
                            $('#cmpList').html('<center><?php echo __("No existen Componentes"); ?></center>');
                        }
                        $('#cmpName').val('');
                        $('#dlgComponent').dialog('open');
                    }
                }
            });
        });
        
        $('#dlgDataType').dialog({
            title:dt_title,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        var data = new Array();
                        $.each($('#dtList li'),function(){
                            data.push($(this).data('value'));
                        });
                        $.ajax({
                            url:dt_edit,
                            data:{values:data},
                            success:function(response){
                                if(evalResponse(response)){
                                    $('#dlgDataType').dialog('close');
                                } 
                            }
                        });
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $('#btnAddDt').button({
            icons:{primary: 'ui-icon-plusthick'},
            text: false
        }).click(function(){
            var name = $('#dtName').val().toLowerCase().replace(' ','_','g');
            name = $.trim(name);
            if(name!=''){
                $('#dtList').append(iList.replace('[ID]',name, 'g').replace('[VALUE]',name, 'g'));
                $('#dtName').val('');
            }
        });
        
        $('#btnEditDataType').click(function(){
            $.ajax({
                url:dt_list,
                success:function(response){
                    if(evalResponse(response)){
                        var str="";
                        $('#dtList').html('');
                        if(response.data.length>0){
                            $.each(response.data,function(){
                                str += iList.replace('[ID]', this, 'g').replace('[VALUE]', this, 'g');
                                $('#dtList').html(str);
                            });

                        } else{
                            $('#dtList').html('<center><?php echo __("No existen tipos de dato"); ?></center>');
                        }
                        $('#dtName').val('');
                        $('#dlgDataType').dialog('open');
                    } 
                }
            });
        });
        
        $('#dlgOption').dialog({
            title:opt_title,
            width:500,
            buttons:[
                {
                    id:"btnAccept",
                    text: okButtonText,
                    click: function() {
                        $("#frmOption").submit();
                    }
                },
                {
                    id:"btnCancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $("#rootOption,#booleanPick,#sdfOption,#arrayOption").buttonset();
        $('#opt_cmp').change(function(){
            $('#opt_alias').val($(this).val().toString().replace("_"," ",'g'));
        });
        $('#btnNewOption').click(function(){
            $('#opt_cmp').val('');
            $('#opt_key').val('');
            $('.vars input').val('').hide();
            $('#opt_dt').val('').trigger('change');
            $('#opt_desc').val('');
            $('#opt_desc_short').val('');
            $('#opt_suffix').val('');
            $('#opt_mav').val('');
            $('#opt_sg').val('');
            $('#opt_sg_val').val('');
            $('#opt_gn').val('');
            setRadio('arrayOptionNO');
            setRadio('rootOptionNO');
            setRadio('sdfNO');
            $('#dlgOption').dialog('open');
        });
        
        function listComponents(){
            $.ajax({
                url:cmp_list,
                async:false,
                success:function(response){
                    $('option', '#opt_cmp').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#opt_cmp').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#opt_cmp').append(option_template.replace('[VALUE]', array,'g').replace('[LABEL]',array, 'g'));
                            });
                        } else {
                            $('#opt_cmp').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listComponents();
        
        function listDataType(){
            $.ajax({
                url:dt_list,
                async:false,
                success:function(response){
                    $('option', '#opt_dt').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#opt_dt').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#opt_dt').append(option_template.replace('[VALUE]', array,'g').replace('[LABEL]',array, 'g'));
                            });
                        } else {
                            $('#opt_dt').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listDataType();
        $.mask.rules.r = /[0-9A-Z_]/;
        $.mask.masks.opt_key = {mask: 'r', type:'repeat'};
        $.mask.masks.opt_int = {mask: '9', type:'repeat'};
        $.mask.masks.opt_dec = {mask: '99.99999999999999999999', type:'reverse'};
        $.mask.masks.opt_per = {mask: '99.99'};
        $('#opt_key').setMask();
        $('#color').setMask();
        $('#integer').setMask();
        $('#decimal,#integer,#percentage').setMask();
        $("#opt_dt").change(function(){
            var str = $(this).val().toString();
            if(str!=''){
                $('.vars input').hide();
                $('#booleanPick').hide();
                if(str=='boolean'){
                    str = str+'Pick';
                }
                $('#'+str).show();
                $('#lbl_val').show();
                $('#lbl_val').text(str);
            } else{
                $('#lbl_val').hide();
                $('.vars input').hide();
                $('#booleanPick').hide();
            }
        });
        
        $('#color').ColorPicker({
            onSubmit: function(hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function(){
            $(this).ColorPickerSetColor(this.value);
        });
        
        
        $("#frmOption").validate({
            rules: {
                opt_cmp:{required:true},
                opt_alias:{required:true},
                opt_key:{required:true},
                opt_dt:{required:true},
                opt_desc:{required: true},
                color:{required:function(){
                        return ($('#opt_dt').val()=='color');
                    }},
                decimal:{required:function(){
                        return ($('#opt_dt').val()=='decimal');
                    }},
                integer:{required:function(){
                        return ($('#opt_dt').val()=='integer');
                    }},
                percentage:{required:function(){
                        return ($('#opt_dt').val()=='percentage');
                    }},
                string:{required:function(){
                        return ($('#opt_dt').val()=='string');
                    }}
            },
            messages:{
                opt_cmp:{required:opt_cmp_req},
                opt_alias:{required:opt_alias_req},
                opt_key:{required:opt_key_req},
                opt_dt:{required:opt_dt_req},
                opt_desc:{required: opt_desc_req},
                color:opt_val_req,
                decimal:opt_val_req,
                integer:opt_val_req,
                percentage:opt_val_req,
                string:opt_val_req
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                var params = $(form).serializeObject();
                if(params.opt_sg=='') delete params.opt_sg;
                if(params.opt_sg_val=='') delete params.opt_sg_val;
                if(params.opt_gn=='') delete params.opt_gn;
                if(params.opt_mav=='') delete params.opt_mav;
                $.ajax({
                    url: '/private/options/createOrUpdateOption'+jquery_params,
                    data:params,
                    success: function(response){
                        if(evalResponse(response)){
                            $('#options_jqGrid').trigger('reloadGrid');
                            $('#dlgOption').dialog('close');
                        } 
                    }
                });
            }
        });
        
        $('#options_jqGrid').jqGrid({
            url:url_options,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:300,
            rowList: [30,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Componente") ?>',
                '<?php echo __("KEY") ?>',
                '<?php echo __("Tipo de Dato") ?>',
                '<?php echo __("Valor") ?>',
                '<?php echo __("Descripción") ?>',
                '<?php echo __("Sufijo") ?>',
                '<?php echo __("Activar Menu con el valor") ?>',
                '<?php echo __("Opción ARRAY") ?>',
                '<?php echo __("Integrada al Sistema") ?>',
                '<?php echo __("Opción ROOT") ?>',
                'sysDef',
                'status',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'component',index:'component',sortable:false,search:false},
                {name:'keyName',index:'o.`key`',sortable:true,width:160,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'dataType',index:'dataType',sortable:true,width:80,search:false,align:'center'},
                {name:'val',index:'val',width:80,sortable:false,search:false,align:'center'},
                {name:'description',index:'description',width:120,sortable:false,search:false,hidden:true,hidedlg:true},
                {name:'suffix',index:'suffix',width:50,sortable:false,search:false,align:'center'},
                {name:'mav',index:'mav',width:90,sortable:false,search:false},
                {name:'arrayOption',index:'rootOption',sortable:false,width:70,search:false,align:'center'},
                {name:'gsDefault',index:'gsDefault',sortable:false,width:70,hidden:(!root_option),search:false,align:'center'},
                {name:'rootOption',index:'rootOption',sortable:false,width:70,hidden:(!root_option),search:false,align:'center'},
                {name:'systemDefault',index:'systemDefault',sortable:false,hidden:true,hidedlg:true},
                {name:'status',index:'status',sortable:false,hidden:true,hidedlg:true},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:120,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,search:false}
            ],
            pager: "#options_jqGrid_pager",
            sortname: 'keyName',
            grouping: true,
            groupingView : {
                groupField : ['component'],
                groupColumnShow : [false],
                groupText : ['<b>{0} - ({1})<img src="/media/ico/ico_order_opt.png" class="ico_order_opt"/></b>'],
                groupCollapse : false,
                groupOrder: ['asc'],
                groupSummary : [false], 
                showSummaryOnHide: false,
                groupDataSorted : true
            },
            gridComplete:function(){
                var ids = $('#options_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    var rowData = $('#options_jqGrid').jqGrid('getRowData',ids[i]);
                    if(Boolean(eval(rowData.rootOption))){
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{rootOption:ico_on});
                    } else {
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{rootOption:ico_off});
                    }
                    if(Boolean(eval(rowData.gsDefault))){
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{gsDefault:ico_on});
                    } else {
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{gsDefault:ico_off});
                    }
                    if(Boolean(eval(rowData.arrayOption))){
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{arrayOption:ico_on});
                    } else {
                        $("#options_jqGrid").jqGrid('setRowData',ids[i],{arrayOption:ico_off});
                    }
                    
                    edit = "";
                    trash="";
                    undo="";
                    
                    if(!Boolean(eval(rowData.status))){
                        $('tr#'+ids[i]).addClass('ui-state-error');
                        undo = undo_template.replace('[ID]',ids[i],'g');
                    }else{
                        edit = edit_template.replace('[ID]',ids[i],'g');
                        if(!Boolean(eval(rowData.systemDefault))){
                            trash = trash_template.replace('[ID]',ids[i],'g');
                        }
                    }
                    $("#options_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash+undo});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $("#opt_dt").val(response.data.dataType).trigger('change');
                                $("#opt_key").val(response.data.key);
                                $("#opt_cmp").val(response.data.component);
                                $("#opt_alias").val(response.data.aliasComponent);
                                $("#opt_suffix").val(response.data.suffix);
                                $("#opt_mav").val(response.data.menuActiveValue);
                                $("#opt_desc").val(response.data.description);
                                $("#opt_desc_short").val(response.data.descriptionShort);
                                $('#opt_sg').val(response.data.groupParent);
                                $('#opt_sg_val').val(response.data.groupParent);
                                $('#opt_gn').val(response.data.groupName);
                                $("#"+response.data.dataType).val(response.data.value);
                                if(response.data.dataType=='boolean'){
                                    if(Boolean(eval(response.data.value))){
                                        setRadio('booleanPickYES');  
                                    }else{
                                        setRadio('booleanPickNO');
                                    }    
                                }
                                if(Boolean(eval(response.data.systemDefault))){
                                    setRadio('sdfYES');  
                                }else{
                                    setRadio('sdfNO');
                                }
                                if(Boolean(eval(response.data.hasArrayValues))){
                                    setRadio('arrayOptionYES');  
                                }else{
                                    setRadio('arrayOptionNO');
                                }
                                if(Boolean(eval(response.data.rootOption))){
                                    setRadio('rootOptionYES');  
                                }else{
                                    setRadio('rootOptionNO');
                                }
                                $('#dlgOption').dialog('open');
                            } 
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                        if(response == true){
                            $.ajax({
                                url:url_trash,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#options_jqGrid').trigger('reloadGrid');
                                    }
                                }
                            });
                        }
                    });
                });
                
                $('.undo').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                });
                $('.undo').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(confirmUndoMessage,undoSubTitle,function(response){
                        if(response == true){
                            $.ajax({
                                url:url_undo,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#options_jqGrid').trigger('reloadGrid');
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });

        $("#options_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        
        $('.rCombo').click(function(event,params){
            showLoading=1;
            switch($(this).data('type')){
                case 'cmp':listComponents();break;
                case 'dt':listDataType();break;
            }
        });
        $('.ico_order_opt').live('click',function(){
            $.ajax({
                url:option_order_list,
                data:{cmp:$(this).parent().text().split(' ', 1)},
                success: function(r){
                    var str="";
                    $('#options_list').html('');
                    var i=0
                    $.each(r.data,function(){
                        str += '<li class="ui-state-default ui-corner-all" id="option_'+i+'" data-ido="'+this.id+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+this.value+'</li>';
                        i++;
                        $('#options_list').html(str);
                    });

                    $( "#options_list" ).sortable({
                        placeholder: "ui-state-highlight"
                    });
                    
                    $('#orderOptionsPopup').dialog('open');
                }
            });
        });
        
        $('#orderOptionsPopup').dialog({
            title:option_order_title,
            width:500,
            buttons:[
                {
                    id:"btnAccept",
                    text: okButtonText,
                    click: function() {
                        var order = new Array();
                        $.each($('#options_list li'),function(){
                            order.push($(this).data('ido'));
                        });
                        $.ajax({
                            url:option_order_save,
                            data:{options:order},
                            success: function(response)
                            {
                                if(evalResponse(response)){
                                    $('#options_jqGrid').trigger('reloadGrid');
                                    $('#orderOptionsPopup').dialog('close');
                                } 
                            }
                        });
                    }
                },
                {
                    id:"btnCancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        
        function createAutocompleteKeys(url,input_id){
         if(typeof $('#'+input_id).data('autocomplete') != 'undefined')
            $('#'+input_id).autocomplete('destroy');
            $('#'+input_id).autocomplete({
                autoFocus: true
                ,dataType:'jsonp'
                ,select:function(event,ui){
                    $(this).data('autocomplete').text_value = ui.item.value;
                    $('#opt_sg_val').val(ui.item.id);
                }
                ,change:function(event,ui){
                    if(!ui.item){$('#opt_sg_val').val('');}
                }
                ,source:function(request,response){
                    var params = new Object();
                    params.term = request.term;
                    params.key = $('#opt_key').val();
                    $.ajax({
                        url:url,
                        data:params,
                        success: function(r) {
                            if(r.code==code_success){
                                //response( $.map( r.data, function( item ) {return { label: item.value,value: item.id}}));
                                response( $.map( r.data, function( item ) {
                                    var text = "<span class='spanSearchLeft'>" + item.value + "</span>";                                
                                    text += "<div class='clear'></div>";
                                    return {
                                        label:  text.replace(
                                        new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(request.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>" ),
                                        value:  item.value,
                                        id : item.id                                   
                                    }
                                }));
                            } else{
                                msgBox(r.msg,r.code);
                            }
                        }
                    });
                }
            }).bind('blur', function(){        
                $('#pseudo_dlg_sender_dir').css({'display':'none'});
                if($($(this).data('autocomplete').element).val() != $(this).data('autocomplete').text_value){
                    $( this ).val('');  
                }
            }).data( "autocomplete" )._renderItem = function( ul, item ) {            
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul ).css({width:'250px'});
            };
        }
        createAutocompleteKeys(url_autocomplete,'opt_sg');
    });
</script>
<style type="text/css">
    #jqgh_options_jqGrid_rootOption,#jqgh_options_jqGrid_mav,#jqgh_options_jqGrid_gsDefault,#jqgh_options_jqGrid_arrayOption{
        height: 30px;
        white-space: pre-wrap;
    }
    .ico_order_opt{
        padding-left: 12px;
        cursor: pointer;
    }
    .ui-icon-circle-close{cursor: pointer;}
    .list { list-style-type: none; margin: 0; padding: 2px; width: auto; text-align: left;}

    .list li { margin: 5px 5px 5px 5px; padding: 5px; font-size: 1.2em; min-height: 15px; padding-left: 1.5em;}

    .list li span { display: block; float: left; margin-left: -1.3em; }

    #lbl_val{font-variant: small-caps;}
    .label_output div label{
        min-width: 199px;
        max-width: 199px;
    }
    #options_list li {
        padding: 3px;
        margin: 3px;
    }
    #options_list li span{
        float: left;
    }
</style>
<button id="btnEditComponent" class="btn" ><?php echo __("Editar Componente"); ?></button>
<div id="dlgComponent" class="dialog-modal">
    <div>
        <label><b/><?php echo __("Nombre"); ?><b/></label>
        <input id="cmpName" name="cmpName" type="text"/>
        <button id="btnAddCmp"><?php echo __("Nombre"); ?></button>
    </div>
    <form id="frmComponent" method="post">
        <ul id="cmpList" class="list">

        </ul>
    </form>
</div>
<button id="btnEditDataType" class="btn" ><?php echo __("Editar Tipo de Dato"); ?></button>
<div id="dlgDataType" class="dialog-modal">
    <div>
        <label><b><?php echo __("Nombre"); ?></b></label>
        <input id="dtName" name="dtName" type="text"/>
        <button id="btnAddDt"><?php echo __("Nombre"); ?></button>
    </div>
    <form id="frmDataType" method="post">
        <ul id="dtList" class="list">

        </ul>
    </form>
</div>

<button id="btnNewOption" class="btn" ><?php echo __("Nueva Opción"); ?></button>
<center><br/><div class="titleTables"><?php echo __("Administración de Opciones"); ?></div></center>
<br/>
<center>
    <table id="options_jqGrid"></table>
    <div id="options_jqGrid_pager"></div>
</center>

<div id="dlgOption" class="dialog-modal">
    <form id="frmOption" method="post">
        <div class="label_output">
            <div>
                <label><?php echo __("Componente"); ?></label>
                <select id="opt_cmp" name="opt_cmp" style="width: 200px;"></select>
                <img src="/media/ico/refresh.png" alt="[<?php echo __('Actualizar') ?>]" class="rCombo" data-type="cmp" title="[<?php echo __('Actualizar') ?>]"/>
            </div>
            <div>
                <label><?php echo __("Alias"); ?></label>
                <input id="opt_alias" name="opt_alias" type="text" maxlength="45"/>
            </div>
            <div>
                <label><?php echo __("KEY"); ?></label>
                <input id="opt_key" name="opt_key" type="text" alt="opt_key" maxlength="45" style="width: 200px;" />
            </div>
            <div>
                <label><?php echo __("KEY Grupo Padre"); ?></label>
                <input id="opt_sg" name="opt_sg" type="text"  maxlength="45" style="width: 200px;"/>
                <input id="opt_sg_val" name="opt_sg_val" type="hidden" />
            </div>
            <div>
                <label><?php echo __("Nombre Grupo"); ?></label>
                <input id="opt_gn" name="opt_gn" type="text" maxlength="45"/>
            </div>
            <div>
                <label><?php echo __("Tipo de Dato"); ?></label>
                <select id="opt_dt" name="opt_dt" style="width: 200px;"></select>
                <img src="/media/ico/refresh.png" alt="[<?php echo __('Actualizar') ?>]" class="rCombo" data-type="dt" title="[<?php echo __('Actualizar') ?>]"/>
            </div>
            <div class="vars">
                <label id="lbl_val"></label>
                <input id="color" name="color" type="text" class="ui-helper-hidden" maxlength="6" readonly/>

                <div id="booleanPick" style="display: none;">
                    <input type="radio" id="booleanPickYES" name="booleanPick" value="1"/><label for="booleanPickYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="booleanPickNO" name="booleanPick" checked="checked" value="0"/><label for="booleanPickNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
                <input id="decimal" name="decimal" type="text" alt="opt_dec" class="ui-helper-hidden"/>
                <input id="integer" name="integer" type="text" alt="opt_int" class="ui-helper-hidden" maxlength="10"/>
                <input id="percentage" name="percentage" type="text" alt="opt_per" class="ui-helper-hidden"/>
                <input id="string" name="string" type="text" class="ui-helper-hidden"/>
            </div>
            <div>
                <label><?php echo __("Descripción"); ?></label>
                <textarea id="opt_desc" name="opt_desc" cols="20" rows="3"></textarea>
            </div>
            <div>
                <label><?php echo __("Descripción Corta"); ?></label>
                <textarea id="opt_desc_short" name="opt_desc_short" cols="20" rows="3"></textarea>
            </div>
            <div>
                <label><?php echo __("Símbolo"); ?></label>
                <input id="opt_suffix" name="opt_suffix" type="text" maxlength="5" size="7"/>
            </div>
            <div>
                <label><?php echo __("Valor para acivar menú"); ?></label>
                <input id="opt_mav" name="opt_mav" type="text"/>
            </div>
            <div>
                <label style="margin-top: 7px;"><?php echo __("Valores en ARRAY"); ?>:</label>
                <div id="arrayOption">
                    <input type="radio" id="arrayOptionYES" name="arrayOption" value="1"/><label for="arrayOptionYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="arrayOptionNO" name="arrayOption" checked="checked" value="0"/><label for="arrayOptionNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
            </div>
            <div>
                <label style="margin-top: 7px;"><?php echo __("Opción ROOT"); ?>:</label>
                <div id="rootOption">
                    <input type="radio" id="rootOptionYES" name="rootOption" value="1"/><label for="rootOptionYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="rootOptionNO" name="rootOption" checked="checked" value="0"/><label for="rootOptionNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
            </div>
            <div>
                <label style="margin-top: 7px;"><?php echo __("Opción Integrada al Sistema"); ?>:</label>
                <div id="sdfOption">
                    <input type="radio" id="sdfYES" name="sdf" value="1"/><label for="sdfYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                    <input type="radio" id="sdfNO" name="sdf" checked="checked" value="0"/><label for="sdfNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="orderOptionsPopup" class="dialog-modal">
    <ul id="options_list">

    </ul>
</div>