<script type="text/javascript">

    
    //oficinas
    var url_messages_list='/private/messages/getListMessages'+jquery_params;
    var url_cantmessages_list='/private/messages/getListCantMessages'+jquery_params;
    var url_cantmessagesuser_list='/private/messages/getPreviousinfoMessagesUser'+jquery_params;
    var url_LastNotificationUser='/private/messages/getLastNotificationUser'+jquery_params;
    var lastNotificationMessage = '<?php echo UserHelper::fnGetLastDateNotificationMessage() ?>';

    var m_user_last_notification_new = 0;

    var clear_all = null;
    $(document).ready(function(){
        
        
        // cantidad mensajes
        //cantidad de mensajes que envian los usuarios
        
        $('#messages_List_jqGrid').jqGrid({
            url:url_cantmessages_list,
            datatype:'json',
            mtype:'POST',
            rowNum: 20,
            height:300,
            rowList: [10, 20, 30],

            colNames:[
                'ID',
                '<?php echo __("Usuario"); ?>',
                '<?php echo __("Nro de Mensajes"); ?>',
            ],
            colModel:[
                {name:'id',index:'id', width:35,key:true,hidden:true,align:'center'},
                {name:'user',index:'p.fullName',align:'center',width:200, search:true,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'numberMessages',index:'numberMessages',align:'center',width:100, search:false}
            ],
           
            pager: "#messages_List_jqGrid_pager",
            caption: "<?php echo __("Lista de Mensajes"); ?>",
            sortname: 'numberMessages',
            sortorder: 'desc',
//            sortable: true,
//            viewrecords: true,
//            scroll: true,
            onSelectRow:function(rowid,status){
                $('#displaysCantMessages').html('');
                numPagUser = 0;
                numbAvailableMoreMessageUserAjax = true;
                m_user_last_notification = 0;
                clearTimeout(clear_all);
                $.ajax({
                    url:url_LastNotificationUser,
                    // async: false,
                    data:{
                        idUser: rowid
                        
                    },
                    success:function(responsecant){
                        
                        if(responsecant.code==code_success){
                            m_user_last_notification = responsecant.data;
                            fnGetMsgByUser(rowid, numPagUser ,  m_user_last_notification, numbAvailableMoreMessageUserAjax );
                            
                        }
                    }
                });
                                    
                
            }

        });
        $("#messages_List_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        
        //setInterval(function() {$('#messages_List_jqGrid').trigger("reloadGrid"); },25000);
              
        // fin cantidad mensajes
        
        
        
        function fnGetMsgByUser(idUser, numPagUser, m_user_last_notification, numbAvailableMoreMessageUserAjax){
            
            
            if(numbAvailableMoreMessageUserAjax){
                
                    
                $.ajax({
                    url:url_cantmessagesuser_list,
                    beforeSend: function(){
                        $('div.more_cantmessagesuser').append($('<img/>').attr('src', '/media/images/ajax-loading.gif').css('vertical-align', 'middle'));
                    },
                    data:{
                        n_page:numPagUser,
                        idUser: idUser,
                        lastNotification:m_user_last_notification
                    },
                    success:function(responsecant){
                        
                        if(responsecant.code==code_success){
                            
                            $('#displaysCantMessages').fadeIn();
                            oResponse = responsecant.data; 
                            m_user_last_notification_new = oResponse.lastNotification;
                            numPagUser ++;
                            
                            $('.more_cantmessagesuser').data('numPagUser', numPagUser);
                            $('.more_cantmessagesuser').data('user_id', idUser);
                            //$('.more_cantmessagesuser').data('m_user_last_notification', m_user_last_notification);
                            $('.more_cantmessagesuser').data('m_user_last_notification_new', m_user_last_notification_new);
                            
                            numbAvailableMoreMessageUserAjax = true;
                            ob_ajax_user.ajax(); 
                            $.each(responsecant.data, function(i, valor){  
                                if(this.hasOwnProperty('idMessage')){ 
                                    c_html = '<div class="cantMessageUser" id="messageUser_id_'+valor.idMessage+'" unix_time="'+valor.lastNotification+'">';
                                    c_html += '<div class="contMessage">';
                                    c_html += '<div class="cont_cantmessageuser">';              
                                    c_html += '<span class="imgTo ui-icon ui-icon-circle-arrow-e"></span>';
                                    c_html += '<span class="nameBold">'+valor.destination+'</span>';
                                    c_html += '<div>';
                                    c_html += '<span class="message_text">'+valor.message+'</span>';
                                    c_html += '</div>';
                                    c_html += '<div>';
                                    c_html += '<span class="aligntext">'+valor.creation_dates+'</span>';
                                    c_html += '</div>';
                                    c_html += '</div>';
                                    c_html += '<div class="clear"></div>';
                                    c_html += '</div>';
                                    c_html += '</div>';
                                    c_html += '<div class="clear"></div>';
        
                                    $('#displaysCantMessages').append(c_html);
                                }
                            });
                                
                            if(oResponse.previousCount>0){
                                $('div.more_cantmessagesuser').fadeIn();
                                $('div.more_cantmessagesuser').html("["+oResponse.previousCount+"]<?php echo __("Mensajes Anteriores") ?>");
                            }else{
                                $('div.more_cantmessagesuser').fadeOut();
                            }
                        
                        
                        }else{
                            msgBox(responsecant.msg,responsecant.code);
                        }
                        
                    }
                });
                    
            }   
        }

        $('div.more_cantmessagesuser').click(function(){
            fnGetMsgByUser($('.more_cantmessagesuser').data('user_id')
            , $('.more_cantmessagesuser').data('numPagUser')
            ,  m_user_last_notification, true);
            clearTimeout(clear_all);
        });
        
        var ob_ajax_user = { 
            ajax: function (){
                clear_all =  setTimeout( function(){ob_ajax_user.ajax();},25000);
                $.ajax({
                    url:url_messages_list,
                    data:{
                        idUser: $('.more_cantmessagesuser').data('user_id'),
                        creation_date: $('.more_cantmessagesuser').data('m_user_last_notification_new')
                        
                    },
                    //                async:false,
                    success:function(response){
                        if(response.code==code_success){
                            $.each(response.data, function(i, val){
                                
                                c_html = '<div class="cantMessageUser" id="messageUser_id_'+val.idMessage+'">';
                                c_html += '<div class="contMessage">';
                                c_html += '<div class="cont_cantmessageuser">';              
                                c_html += '<span class="imgTo ui-icon ui-icon-circle-arrow-e"></span>';
                                c_html += '<span class="nameBold">'+val.destination+'</span>';
                                c_html += '<div>';
                                c_html += '<span class="message_text">'+val.message+'</span>';
                                c_html += '</div>';
                                c_html += '<div>';
                                c_html += '<span class="aligntext">'+val.creation_dates+'</span>';
                                c_html += '</div>';
                                c_html += '</div>';
                                c_html += '<div class="clear"></div>';
                                c_html += '</div>';
                                c_html += '</div>';
                                c_html += '<div class="clear"></div>';
                                $('#displaysCantMessages').prepend(c_html);
                                $('.more_cantmessagesuser').data('m_user_last_notification_new', val.lastNotification);
                            });
                        
                        }else{
                            msgBox(response.msg,response.code);
                        }
                    
                      
                    
                    
                    }
                });
            }
        };

        //oficinas
        
        var ob_ajax = { 
            ajax: function (){
                
                $.ajax({
                    url:url_messages_list,
                    data:{
                        creation_date: lastNotificationMessage
                    },
                    //                async:false,
                    success:function(response){
                        if(response.code==code_success){
                            $.each(response.data, function(i, val){
                                
                                var $img = "/upload/profile/128/02.png"
                                if(val.img!=""){
                                    $img = val.img;
                                }
                                dates=''
                                    if(val.idUserDestination>0){
                                        dates='dates'
                                }
                                console.log(val.idUserDestination);
                                s_html = '<div class="messageUser">';
                                s_html +='<div class="contMessage">';
                                s_html +='<img class="imgUser" alt="Imagen" src="'+$img+'"/>';
                                s_html +='<div class="cont_user">';                
                                s_html +='<span class="nameBold dates" id="'+val.idUserSender+'">'+val.sender+' </span>';
                                s_html +='<span class="imgTo ui-icon ui-icon-circle-arrow-e"></span>';
                                s_html +='<span class="nameBold '+ dates +'" id="'+val.idUserDestination+'"> '+val.destination+'</span>';
                                s_html +='<div>';
                                s_html +='<span class="message_text">'+val.message+'</span>';
                                s_html +='</div>';
                                s_html +='<div>';
                                s_html += '<span class="aligntext">'+val.creation_dates+'</span>';
                                s_html +='</div>';
                                s_html +='</div>';
                                s_html +='<div class="clear"></div>';
                                s_html +='</div>';
                                s_html +='</div> ';
                                s_html +='<div class="clear"></div>';
                                $('#displaysMessages').prepend(s_html);
                                lastNotificationMessage = val.lastNotification;
                                $('#messages_List_jqGrid').trigger("reloadGrid");
                            });
                        
                        }else{
                            msgBox(response.msg,response.code);
                        }
                    
                        setTimeout( function(){ob_ajax.ajax();},25000);
                    
                    
                    }
                });
            }
        };
        ob_ajax.ajax();
        
        
        var numbAvailableMoreMessageAjax = true;
        var num_page = 1;
<?php
$m_user_notifications = UserHelper::fngetInfoMessages(0 , 0, null, $date_format , $time_format);
$m_last_notification = 0;


if (array_key_exists(0, $m_user_notifications))
    $m_last_notification = $m_user_notifications[0]['lastNotification'];
?>

        $('div.more_messagesuser').click(function(){
            if(numbAvailableMoreMessageAjax){
                $.ajax({
                    url: "/private/messages/getPreviousInfoMessages"+jquery_params,
                    data: {n_page: num_page, lastNotification: <?php echo $m_last_notification ?>},
                    beforeSend: function(){
                        $('div.more_messagesuser').append($('<img/>').attr('src', '/media/images/ajax-loading.gif').css('vertical-align', 'middle'));
                    },
                    error: function(){
                        $('div.more_messagesuser img').remove();
                        
                    } ,
                    success: function(oResponse){
                        $('div.more_messagesuser img').remove();
                        if(oResponse.code == code_success){
                            oData = oResponse.data;
                            numbAvailableMoreMessageAjax = true;
                            num_page ++;
                            oData = oResponse.data;
                            
                            $.each(oData, function(){
                                
                                if(this.hasOwnProperty('idMessage')){
                                    dates=''
                                    if(this.idUserDestination>0){
                                        dates='dates'
                                    }
                                    s_html = '<div id="messageUser_id_'+this.idMessage+'" unix_time="'+this.lastNotification+'" class="messageUser">';
                                    s_html +='<div class="contMessage">';
                                    s_html +='<img class="imgUser" alt="Imagen" src="'+this.img+'"/>';
                                    s_html +='<div class="cont_user">';                
                                    s_html +='<span class="nameBold dates" id="'+this.idUserSender+'">'+this.sender+' </span>';
                                    s_html +='<span class="imgTo ui-icon ui-icon-circle-arrow-e"></span>';
                                    s_html +='<span class="nameBold '+ dates +'" id="'+this.idUserDestination+'"> '+this.destination+'</span>';
                                       
                                    s_html +='<div>';
                                    s_html +='<span class="message_text">'+this.message+'</span>';
                                    s_html +='</div>';
                                    s_html +='<div>';
                                    s_html += '<span class="aligntext">'+this.creation_dates+'</span>';
                                    s_html +='</div>';
                                    s_html +='</div>';
                                    s_html +='<div class="clear"></div>';
                                    s_html +='</div>';
                                    s_html +='</div> ';
                                    s_html +='<div class="clear"></div>';
                                    $('div.more_messagesuser').before(s_html);
                                }
                            });
                            if(oData.previousCount>0){
                                $('div.more_messagesuser').html("<?php echo __("Mensajes Anteriores") ?>");
                            }else{
                                $('div.more_messagesuser').fadeOut();
                            }
                            
                        }
                    }
                    
                });
            }
        });
        
        $('#search').live('click',function(){
            if ($('#showmessages').is(':hidden')){
                $("#showmessages").show(1000);
            }else{
                $("#showmessages").hide("slow");
            }
        });
        
        $('.dates').live('click',function()
            {

                idUser = $(this).attr('id');                
//                loadingcarga=1;        
                $.ajax({
                    type:'POST',
                    url:'/private/messages/getInfoUser'+jquery_params,
                    data: {'idUser':idUser
                    },
                    dataType:'json',
                    success: function(r){
                        
                        //console.log(datas.fullName);
                        if(r.code==code_success){
                            r = r.data;
                            
                            $('i#nombreCompleto').text(r.fullName);
                            $('i#usuario').text(r.userName);
                            $('i#idUser').text(r.idUser);
                            $('i#oficina').text(r.officeName);
                            $('i#grupo').text(r.groupName);
                            $('i#email').text(r.email);
                            $('i#phone1').text(r.phone1);
                            $('i#cellPhone').text(r.cellPhone);
                            
                                
                            $("div#infoUserOnline" ).dialog( "option", "width",300);
                            $('div#infoUserOnline').dialog('open');
                        } else {
                            msgBox(r.msg,r.code);
                        }
                    }
               });               
                        
            });
        
    });
    
</script>
<style type="text/css">
    .cont_user span, .cont_cantmessageuser span{display: inline-block;}
    .cantmessagesuser{float: left; margin-left:  10px;}
    .nameBold{
        font-weight: bold;
        margin-right: 5px;
        font-size: 12px;
    }
    .dates{
        cursor: pointer;
    }
    .aligntext{
        /*        float: right;*/
        color: #999999;
        font-size: 12px;
    }

    .message_text{
        font-family: Arial,"Helvetica Neue",sans-serif;
        line-height: 19px;
        padding: 0;
        word-wrap: break-word;
        color: #0099B9;

    }
    .imgTo{
        margin-right: 5px;  
    }

    #help{
        float: right; 
        margin: 3px 4px 0 0;
        cursor: pointer;
    }
    #imgUser{
        float: left; 
        margin: 3px 4px 0 0;
        cursor: pointer;
        background: #0063DC;
    }
    #displaysMessages{
        background-color: #F5F8F9;
        border: 1px solid #DDDDDD;
        width: 435px;
    }
    #displaysCantMessages{
        background-color: #F5F8F9;
        border: 1px solid #DDDDDD;
        width: 429px;

    }

    .messageUser{
        background-color: #F5F8F9;
        border-bottom: 1px solid #DDDDDD;
        width: 430px;
        padding-top:5px;
        padding-bottom:5px;
        padding-right:5px;

    }

    .cantMessageUser{
        background-color: #F5F8F9;
        border-bottom: 1px solid #DDDDDD;
        width: 424px;
        padding-top:5px;
        padding-bottom:5px;
        padding-right:5px;

    }


    img.imgUser {
        height: 60px;
        margin-left: 3px;
        width: 60px;
        float:left;
    }

    .cont_user{
        float: left;
        /*        background: #3399FF;*/
        margin-left: 10px;
        width: 357px;
    }
    .cont_cantmessageuser{

        /*        background: #3399FF;*/
        margin-left: 10px;
        width: 414px;
    }

    .icon_luggage_search{
        /*        margin: 6px 5px 6px 0;*/
    }

    #messages{width:1200px;
              margin: 0 auto;
    }


    .b_help{padding: 4px;display: block; cursor: pointer;}

    .more_messagesuser, .more_cantmessagesuser{
        background: none repeat scroll 0 0 #E3F1FA;
        border: 1px solid #C6E4F2;
        border-radius: 3px 3px 3px 3px;
        cursor: pointer;
        display: block;
        font-size: 13px;
        margin: 10px 15px;
        padding: 5px 1px;
        text-align: center;
        text-shadow: 0 1px 0 #FFFFFF;
        z-index: 2;
    }
    
        #infoUserOnline
    {
        padding: 3px;
    }
    #infoUserOnline form fieldset
    {
        padding-left: 5px;
        padding-top: 10px;
        padding-bottom: 10px;
    }

</style>
<center>
    <div class="titleTables"><?php echo __("Historial de Mensajes"); ?>
        <span id="search">
            <ul class="menu_right">
                <li class="ui-state-default ui-corner-all" title="<?php echo __("Buscar");?>">
                    <span class="b_help">
                        <span class="ui-icon ui-icon-search"></span>
                    </span>
                </li>
            </ul>
        </span>
    </div>
</center>

<div id="messages">   
    <div id="displaysMessages" style="float:left;">
        <!--        primeros mensajes-->
        <?php foreach (UserHelper::fngetInfoMessages(0,0,null,$date_format , $time_format) as $iCount => $aNotification): ?>                        
            <?php if (is_numeric($iCount)): ?>
                <div id="messageUser_id_<?php echo $aNotification['idMessage'] ?>" unix_time="<?php echo $aNotification['lastNotification'] ?>" class="messageUser">
                    <div class="contMessage">
                        <img class="imgUser" alt="Imagen" src="<?php echo $aNotification['img'] ?>"/>
                        <div class="cont_user">                
                            <span class="nameBold dates" id="<?php echo $aNotification['idUserSender'] ?>"><?php echo $aNotification['sender'] ?></span>
                            <span class="imgTo ui-icon ui-icon-circle-arrow-e"></span>
                            <span class="nameBold <?php if($aNotification['idUserDestination']>0){ echo 'dates';} ?>" id="<?php echo $aNotification['idUserDestination'] ?>"><?php echo $aNotification['destination'] ?></span>

                            <div>
                                <span class="message_text"><?php echo $aNotification['message'] ?></span>
                            </div>
                            <div>
                                <span class="aligntext"><?php echo $aNotification['creation_dates'] ?></span>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>

            <?php endif; ?>
        <?php endforeach; ?> 

        <div class="more_messagesuser" <?php
        if (UserHelper::fnGetCountMessages(0, $m_last_notification, null) == 0) {
            echo "class='none'";
        }
        ?>>
                 <?php echo __("Mensajes Anteriores") ?>
        </div>



    </div>
    <div style="display: none;" id="showmessages">
        <div class="cantmessagesuser">
            <table id="messages_List_jqGrid"></table>
            <div id="messages_List_jqGrid_pager"></div>
        </div>

        <div class="left">
            <div class="cantmessagesuser" id="displaysCantMessages" style="display: none;">


            </div>
            <div class="clear"></div>
            <div class="more_cantmessagesuser" style="display: none;">

            </div>
        </div>


    </div>
</div>

    <div id="infoUserOnline" class="dialog none">
        <form>        
            <fieldset>                
                <legend class="frmSubtitle"><?php echo __("Información de Usuario"); ?></legend>
                <div class="">
                    <div><label class="frmlblmedium"><?php echo __("Nombre"); ?>:</label><i id="nombreCompleto"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Usuario"); ?>:</label><i id="usuario"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Id Usuario"); ?>:</label><i id="idUser"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Oficina"); ?>:</label><i id="oficina"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Grupo"); ?>:</label><i id="grupo"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("E-mail"); ?>:</label><i id="email"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Teléfono"); ?>:</label><i id="phone1"></i></div><div class="clear"></div>
                    <div><label class="frmlblmedium"><?php echo __("Celular"); ?>:</label><i id="cellPhone"></i></div><div class="clear"></div>
                    
                </div>
            </fieldset>
        </form>
    </div>

