<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Content-Language" content="en-us" />
        <title><?php echo $title; ?></title>
        <meta name="keywords" content="<?php echo $meta_keywords; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta name="copyright" content="<?php echo $meta_copyright; ?>" />
        <link rel="shortcut icon" href="/media/images/<?php echo $skin; ?>/favicon.png"/>
        <!--Estilos Obligatorios-->        
        <link type="text/css" href="/media/css/<?php echo $skin.'/'.${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}; ?>/jquery-ui-<?php echo ${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}?>.custom.min.css" rel="stylesheet" />
        <?php
        foreach ($styles as $file) {
            echo HTML::style($file['mediaFile_fileName'], NULL, NULL), "\n" . "\t";
        }
        ?>
        <script type="text/javascript">
            /*TIMEOUT VARS*/
            var idleTime = <?php echo $TIMEOUT_MILISECONDS; ?>;
            var redirectAfter = <?php echo $TIMEOUT_SECONDS; ?>;
            var initialSessionTimeoutMessage_1 = '<?php echo __("Tu sesión var a expirar en ") ?>';
            var initialSessionTimeoutMessage_2 = '<?php echo __(" segundos. Por favor dar un click en el botón Aceptar para continuar en la sesión.") ?>';
            var expiredMessage = '<?php echo __('Su sesión ha expirado. Será desconectado del sistema por motivos de seguridad.'); ?>';
            var titleSessionTimeout = '<?php echo __('Advertencia de Expiración de la Sesión.'); ?>';//'Session Expiration Warning'
             var txtprocessOk = '<?php echo __('Proceso Satisfactorio'); ?>';
            /*TIMEOUT VARS*/
            /*ACTION VARS*/
            var option_template = '<option value="[VALUE]">[LABEL]</option>';
            var option_template_selected = '<option value="[VALUE]" [SELECTED]>[LABEL]</option>';
            var edit_template = "<a class=\"edit\" data-oid=\"[ID]\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
            var trash_template = "<a class=\"trash\" data-oid=\"[ID]\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
            var undo_template = "<a class=\"undo\" data-oid=\"[ID]\" title=\"<?php echo __('Recuperar'); ?>\" ><?php echo __('Recuperar'); ?></a>";
            /*ACTION VARS*/
            var jquery_params='?jsoncallback=?';
            var jquery_df = '<?php echo $jquery_date_format; ?>';
            var dfShow24Hour = Boolean(eval(<?php echo $DF_SHOW24HOUR; ?>));
            var rParams = new Object();
            rParams.sid='<?php echo $SESSION_ID; ?>';
            rParams.qlang = '<?php echo $LANG; ?>';
            rParams.qdf = '<?php echo $DATE_FORMAT; ?>';
            var full_screen_mode=parseInt('<?php echo $full_screen_mode; ?>');
            var full_true_screen_mode=parseInt('<?php echo $full_true_screen_mode; ?>');
            var showLoading = 0;
            var imgLoading = '/media/images/<?php echo $skin; ?>/wait_loading.gif';
            var txtLoading = '<?php echo __("Por favor, espere un momento..."); ?>';
            var skin = '<?php echo $skin; ?>';
            var msgSelectEmpty='<?php echo __("No se encontró información"); ?>';
            var msgSelectDefaultOption='<?php echo __("Seleccione una opción"); ?>';
            var msgUploadFile='<?php echo __("Subir Imagen"); ?>';
            var errorMessage = '<?php echo __("Hubo un error al procesar la Informacion Solicitada"); ?>';
            var okButtonText = '<?php echo __("Aceptar"); ?>';
            var chPassText = '<?php echo __("Cambiar Password"); ?>';
            var cancelButtonText = '<?php echo __("Cancelar"); ?>';
            var closeButtonText = '<?php echo __("Cerrar"); ?>';
            var enabledButtonText = '<?php echo __("Activo"); ?>';
            var disabledButtonText = '<?php echo __("Inactivo"); ?>';
            var createCssText = '<?php echo __("Generar Estilos"); ?>';
            var saveColorText = '<?php echo __("Para guardar un color, presionar enter en cada caja de texto."); ?>';
            var printButtonText = '<?php echo __("Imprimir Ticket"); ?>';
            var ticketManual = '<?php echo __("Venta Manual Boleto"); ?>';
            var receiptTicket = '<?php echo __("Venta Manual Recibo"); ?>';
            var agentManual = '<?php echo __("Venta Manual Agente"); ?>';
            var confirmationText = '<?php echo __("Confirmación"); ?>';
            var changeOfficeText = '<?php echo __("¿Esta seguro que desea Cambiar de Oficina?"); ?>';
            var observationtext = '<?php echo __("Observaciones"); ?>';
            var recordSuccessfulText = '<?php echo __("Registro Exitoso"); ?>';
            var reservationText = '<?php echo __("Reservar Ticket"); ?>';
            var returnText = '<?php echo __("Retorno"); ?>';
            var fieldRequired = '<?php echo __("Campo Requerido"); ?>';
            var routeText = '<?php echo __("Ruta"); ?>';
            var itineraryText = '<?php echo __("Itinerario"); ?>';
            var toText = '<?php echo __("a"); ?>';
            var removeText = '<?php echo __("Eliminar"); ?>';
            var selectOptionText = '<?php echo __('Seleccione una opción....'); ?>';
            var selectOptionEmptyText = '<?php echo __('Sin resultados....'); ?>';
            var userInfoText = '<?php echo __("Información del Usuario"); ?>';       
            var sLengthMenu = '<?php echo __("Mostrar _MENU_ registros por pagina"); ?>';       
            var sZeroRecords = '<?php echo __("No hay Registros"); ?>';       
            var sEmptyTable = '<?php echo __("Sin Registros"); ?>';      
            var sInfo = '<?php echo __("Mostrando de _START_ a _END_ de _TOTAL_ registros."); ?>';       
            var sInfoEmpty = '<?php echo __("Mostrando de 0 a 0 de 0 Registros"); ?>';       
            var sInfoFiltered = '<?php echo __("Registros filtrados de _MAX_ registros"); ?>';                 
            var sProcessing = '<?php echo __("Procesando..."); ?>';                             
            var sLoadingRecords = '<?php echo __("Cargando..."); ?>';                 
            var sSearch = '<?php echo __("Buscar"); ?>';  
            var code_success = '<?php echo $CODE_SUCCESS; ?>';  
            var code_no_authorized = '<?php echo $CODE_NO_AUTHORIZED; ?>';  
            var code_error= '<?php echo $CODE_ERROR; ?>';
            var msg_unknown = '<?php echo __("Desconocido"); ?>';
            var msg_warning ='<?php echo __("Advertencia"); ?>';
            var msg_no_correlative ='<?php echo __("No se ha encontrado el correlativo para el documento"); ?>';
            
            var pass_blank = '<?php echo __('Todos los campos son necesarios'); ?>'
            var pass_confirm = '<?php echo __('La nueva contraseña no coincide con su cofirmación'); ?>'
        
            var voucher_success = '<?php echo __('Su vale se ha registrado con éxito'); ?>';
            var domain = '<?php echo $DOMAIN; ?>';
            var sendSupportEmail = '<?php echo __('Enviar Email a Soporte'); ?>';
            var sendSupportEmailSuccess = '<?php echo __('Se ha enviado el informe de error al sistema de soporte. Gracias por reportar el problema.'); ?>';
            var supportURL = '/private/welcome/getsendEmailToSuppportLog'+jquery_params;
           
        </script>
        <?php echo HTML::script('media/js/jquery-'.${Kohana_Uconstants::VAR_JQUERY_VERSION}.'.min.js', array('language' => 'javascript')) ?>
        <?php echo HTML::script('media/js/jquery-ui-'.${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}.'.custom.min.js', array('language' => 'javascript')) ?>
        <?php
        foreach ($scripts as $file) {
            if (!(strpos($file['mediaFile_fileName'], 'jquery.jqGrid.min.js') === false)) {
                echo '<script type="text/javascript" src="/media/js/jquery.jqGrid.i18n/grid.locale-' . __("es") . '.js" language="javascript" ></script>' . "\n" . "\t";
            }
            echo HTML::script($file['mediaFile_fileName'], array('language' => 'javascript')), "\n" . "\t";
        }
        ?>
        
        <?php if($LANG == 'es'):?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME'] ?>/media/js/ui/i18n/jquery.ui.datepicker-es.js"></script>
        <?php else:?>
        <script type="text/javascript" src="http://<?php echo $_SERVER['SERVER_NAME'] ?>/media/js/ui/i18n/jquery.ui.datepicker-en-GB.js"></script>
        <?php endif?>
        
        <script type="text/javascript">            
            var lastNotification = '<?php echo UserHelper::fnGetLastDateNotification() ?>';
            var msgData = {};            
            var messages_ids = [];
            var iCount = 0;
            
            var month_names = new Array ( );
            month_names[month_names.length] = "<?php echo __("Ene") ?>";
            month_names[month_names.length] = "<?php echo __("Feb") ?>";
            month_names[month_names.length] = "<?php echo __("Mar") ?>";
            month_names[month_names.length] = "<?php echo __("Abr") ?>";
            month_names[month_names.length] = "<?php echo __("May") ?>";
            month_names[month_names.length] = "<?php echo __("Jun") ?>";
            month_names[month_names.length] = "<?php echo __("Jul") ?>";
            month_names[month_names.length] = "<?php echo __("Ago") ?>";
            month_names[month_names.length] = "<?php echo __("Set") ?>";
            month_names[month_names.length] = "<?php echo __("Oct") ?>";
            month_names[month_names.length] = "<?php echo __("Nov") ?>";
            month_names[month_names.length] = "<?php echo __("Dic") ?>";
            

<?php $d_current_date = UserHelper::fnGetCurrentDate() ?>
            
    var t_current_unix_date = <?php echo $d_current_date['current_unix_date'] ?>;
            
    function fnGetDateUpdate(t_time){                
                
        t_time = new Date(t_time*1000);
                
        t_current_unix_date_f = new Date(t_current_unix_date*1000);
                
        d_diff_date = t_current_unix_date_f.getTime()-t_time.getTime() ;    

        d_sec = 1000;
        d_min = 60*d_sec;
        d_hour = d_min * 60;
        d_day = 24 * d_hour;
                
        if(d_diff_date/d_day >= 1){                    
            var d_date_update = t_time;
            return d_date_update.getDate() + ' '+month_names[d_date_update.getMonth()] ;
        }
                
        if(d_diff_date/d_hour >= 1){                    
            var d_hour_update = d_diff_date / d_hour;
                    
            d_hour_update = parseInt(d_hour_update);
                    
            if(d_hour_update > 1){
                return '<?php echo __('hace') ?> '+ d_hour_update + ' <?php echo __('horas') ?>';
            }else{
                return '<?php echo __('hace') ?> '+ d_hour_update + ' <?php echo __('hora') ?>';
            }                    
        }
                
        if(d_diff_date/d_min >= 1){                    
            var d_min_update = d_diff_date / d_min;
                    
            d_min_update = parseInt(d_min_update);
                    
            if(d_min_update > 1){
                return '<?php echo __('hace') ?> '+ d_min_update + ' <?php echo __('minutos') ?>';
            }else{                        
                return '<?php echo __('hace') ?> '+ d_min_update + ' <?php echo __('minuto') ?>';
            }                    
        }
                
        if(d_diff_date/d_sec >= 1){                    
            var d_sec_update = d_diff_date / d_sec;
                    
            d_sec_update = parseInt(d_sec_update);
                    
            if(d_sec_update > 1){
                return '<?php echo __('hace') ?> '+ d_sec_update + ' <?php echo __('segundos') ?>';
            }else{                        
                return 'ahora';
            }                    
        }        
    }            
            
    $(document).ready(function(){
        $('#btnCleaningCache').button({
            text:false,
            icons:{
                primary:'ui-icon-power'
            }
        });
        $('#btnRegenerateLanguages').button({
            text:false,
            icons:{
                primary:'ui-icon-flag'
            }
        });
        $('#btncleaningSessionUsers').button({
            text:false,
            icons:{
                primary:'ui-icon-person'
            }
        });
        uploaderUserInfo();
        $('#btnImgUpload_ui div ul').hide();
        var bAvailableSaveMessage = true;
        $li_msg_response = null;
        var notification = {
            request_url: "/private/messages/getMessages"+jquery_params,
            do_request: function() {
                $.ajax({
                    url: notification.request_url,
                    data: {lastNotification: lastNotification},
                    error: function(){
                        //setTimeout( function(){notification.do_request();}, 5000);
                    } ,
                    success: function(oResponse){
                        if(oResponse.code == code_success){
                            oData = oResponse.data;
                                    
                            $.each(oData, function(i, value){
                                if(value.hasOwnProperty('idOut')){
                                    iCount++;
                                    $('#sound_message').get(0).play();
                                    li = '<li id="message_id_'+value.idOut+'" unix_time="'+value.updateNotification+'" class="message_new notification">';                                            
                                    li += ' <div class="div_img_action_msg'+(value.responseOut>0?' div_img_action_response':'')+'"></div>';
                                    li += ' <div class="div_message">';
                                    li += '     <div class="general_msg_image ui-corner-all ui-widget-header" style="background: url('+value.user_image+') center center no-repeat;backgroud-size: 100%;">';                                    
                                    li += '     </div>';
                                    li += '     <div class="general_msg_content">';                                            
                                    li += '         <div class="row"><span class="user_name">'+value.userRegistrationOut+'</span> <span class="message_date">'+value.dateCreationOut +'</span></div>';
                                    li += '         <div class="row"><div class="message_text">'+value.messageOut+'</div></div>';
                                    li += '     </div>';
                                    li += '     <div class="clear"></div>';
                                    li += '     <div class="general_msg_footer">';
                                    li += '         <span class="general_time"></span>';
<?php if (UserHelper::fnGetAvailableReplyMessage()): ?>
                                        li += '         <span class="msg_response_btn" r_to_user="'+value.userCreateOut+'" r_to_message="'+value.idOut+'">';
                                        li += '         <i class="icon_response"></i>';
                                        li += '         <?php echo __("Responder") ?>';
                                        li += '         </span>';
                                        if(value.responseOut>0){
                                            li += '         <span class="msg_response_view" r_to_message="'+value.responseOut+'">';
                                            li += '         <i class="icon_view_msg"></i>';
                                            li += '         <?php echo __("Mostrar") ?>';
                                            li += '         </span>';
                                        }
<?php endif; ?>
                                    li += '         <div class="clear"></div>';
                                    li += '     </div>';
                                    li += ' </div>';
                                    li +='</li>';
                                    $('ul#ulMessages').prepend(li);      
                                    lastNotification = value.updateNotification;
                                    messages_ids.push(value.idOut);
                                }
                            });
                                    
                            if(oData.hasOwnProperty('date')){
                                t_current_unix_date = oData.date.current_unix_date;
                                
                                $.each($('ul#ulMessages li.notification'), function(){
                                    t_temp_unix_time = $(this).attr('unix_time');
                                    $(this).find('span.general_time').text(fnGetDateUpdate(t_temp_unix_time));
                                });
                            }
                                    
                                    
                            if(iCount>0){
                                $('ul.menu_right li.commentStatus').addClass('ui-state-error');
                                $('ul.menu_right li.commentStatus span b').html('<?php echo __("Mensajes Nuevos") ?> ('+iCount+')');
                            }else{
                                $('ul.menu_right li.commentStatus').removeClass('ui-state-error');
                                $('ul.menu_right li.commentStatus span b').html('<?php echo __("Mensajes Nuevos") ?> (0)');
                            }
                        }             
                        //Refresh TIME
                        setTimeout( function(){notification.do_request();},15000);
                    }
                });                                
            }
        }
        notification.do_request();
         
        var bAvailableViewMessageAjax = true;
                
        $('#viewMessages').click(function(){
            if(iCount>0){
                if(bAvailableViewMessageAjax){
                    bAvailableViewMessageAjax =false;
                    $.ajax({
                        url: "/private/messages/updateNotification"+jquery_params,
                        data: {lastNotification: lastNotification},
                        error: function(){
                            //setTimeout( function(){notification.do_request();}, 5000);
                        } ,
                        success: function(oResponse){
                            bAvailableViewMessageAjax = true;
                        }
                    });
                }
            }
            iCount = 0;
            $('ul.menu_right li.commentStatus span b').html('<?php echo __("Mensajes Nuevos") ?> (0)');
            $('#ulMessages').show();
            $.each(messages_ids, function(){
                $("#message_id_"+this).show('highlight'); 
            });
            messages_ids = [];
            $(this).parent().removeClass('ui-state-error').addClass('ui-state-default');
        });
                
        click_ulMessages = false;
                
        $('#ulMessages, #viewMessages, #msg_response_div, #msg_response_view_div').hover(function(){ 
            click_ulMessages=true;                    
        }, function(){ 
            click_ulMessages=false;
        });

        $("html").click(function(){ 
            if(! click_ulMessages) {
                $('#ulMessages').hide();
                $('#msg_response_view_div').hide();
                if($li_msg_response!=null)
                    $li_msg_response.removeClass('li_post_message');
                $('#msg_response_div').hide();
                //f_instant_response.currentForm.reset();
                //f_instant_response.resetForm();
                //$('input, select, textarea', f_instant_response.currentForm).removeClass('ui-state-error');                        
            }
        });
                
        var bAvailableMoreMessageAjax = true;
        var n_page = 1;
<?php
$a_user_notifications = UserHelper::fnGetLastNotifications();
$i_last_notification = 0;
?>
        $('#f_instant_close').click(function(){
            $li_msg_response.removeClass('li_post_message');
            $('#msg_response_div').hide();
            f_instant_response.currentForm.reset();
            f_instant_response.resetForm();
            $('input, select, textarea', f_instant_response.currentForm).removeClass('ui-state-error');
        });
        $('div.more_messages').click(function(){
            if(bAvailableMoreMessageAjax){
                $.ajax({
                    url: "/private/messages/getetPreviousMessages"+jquery_params,
                    data: {n_page: n_page, lastNotification: <?php echo $i_last_notification ?>},
                    beforeSend: function(){
                        $('div.more_messages').append($('<img/>').attr('src', '/media/images/ajax-loading.gif').css('vertical-align', 'middle'));
                    },
                    error: function(){
                        $('div.more_messages img').remove();
                        //setTimeout( function(){notification.do_request();}, 5000);
                    } ,
                    success: function(oResponse){
                        $('div.more_messages img').remove();
                        if(oResponse.code == code_success){
                            oData = oResponse.data;
                            bAvailableViewMessageAjax = true;
                            n_page ++;
                                    
                            oData = oResponse.data;
                                    
                            $.each(oData, function(){
                                if(this.hasOwnProperty('idOut')){                                            
                                    li = '<li id="message_id_'+this.idOut+'" unix_time="'+this.updateNotification+'" class="notification">';                        
                                    li += ' <div class="div_img_action_msg'+(this.responseOut>0?' div_img_action_response':'')+'"></div>';
                                    li += ' <div class="div_message">';
                                    li += '     <div class="general_msg_image ui-corner-all ui-widget-header" style="background: url('+this.user_image+') center center no-repeat;backgroud-size: 100%;">';                                    
                                    li += '     </div>';
                                    li += '     <div class="general_msg_content">';   
                                    li += '         <div class="row"><span class="user_name">'+this.userRegistrationOut+'</span> <span class="message_date">'+this.dateCreationOut +'</span></div>';
                                    li += '         <div class="row"><div class="message_text">'+this.messageOut+'</div></div>';
                                    li += '     </div>';
                                    li += '     <div class="clear"></div>';
                                    li += '     <div class="general_msg_footer">';
                                    li += '         <span class="general_time"></span>';
<?php if (UserHelper::fnGetAvailableReplyMessage()): ?>
                                        li += '         <span class="msg_response_btn" r_to_user="'+this.userCreateOut+'" r_to_message="'+this.idOut+'">';
                                        li += '         <i class="icon_response"></i>';
                                        li += '         <?php echo __("Responder") ?>';
                                        li += '         </span>';                                            
                                        if(this.responseOut>0){
                                            li += '         <span class="msg_response_view" r_to_message="'+this.responseOut+'">';
                                            li += '         <i class="icon_view_msg"></i>';
                                            li += '         <?php echo __("Mostrar") ?>';
                                            li += '         </span>';
                                        }
<?php endif; ?>
                                    li += '         <div class="clear"></div>';
                                    li += '     </div>';
                                    li += ' </div>';
                                    li +='</li>';
                                    $('div.more_messages').parent().before(li);
                                }
                            });
                                    
                            if(oData.hasOwnProperty('date')){
                                t_current_unix_date = oData.date.current_unix_date;
                                $.each($('ul#ulMessages li.notification'), function(){
                                    t_temp_unix_time = $(this).attr('unix_time');
                                    $(this).find('span.general_time').text(fnGetDateUpdate(t_temp_unix_time));
                                });
                            }
                                    
                            if(oData.previousCount>0){
                                $('div.more_messages').html("["+oData.previousCount+"]<?php echo __("Mensajes Anteriores") ?>");
                            }else{
                                $('div.more_messages').parent().fadeOut();
                            }
                        }
                    }
                });
            }
        });     
<?php if (UserHelper::fnGetAvailableReplyMessage()): ?>   
            f_instant_response = $('#f_instant_response').validate({
                rules: {
                    f_instant_msg_cont: {
                        required: true
                    }
                },
                messages: {
                    f_instant_msg_cont: {
                        required: '<?php echo __("Mensaje es requerido") ?>'
                    }
                },
                errorContainer:'#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {
                    $(element).addClass('ui-state-error');
                },
                unhighlight:function(element, errorClass, validClass){
                    $(element).removeClass('ui-state-error');
                },
                submitHandler: function(oForm){
                    //Ajax =D
                    if(bAvailableSaveMessage){
                        bAvailableSaveMessage = false;
                        showLoading = 1;
                        o_data_form = $(oForm).serializeObject();                        
                        $.extend(o_data_form, {t_type_notification: 2});
                        $.ajax({
                            url:"/private/messages/saveNotification"+jquery_params,
                            dataType:'jsonp',
                            data: o_data_form,
                            success:function(resp){
                                bAvailableSaveMessage = true;
                                if(resp.code==code_success){
                                    oData = resp.data;
                                    if(oData==1){
                                        $li_msg_response.removeClass('li_post_message');                                            
                                        $('#msg_response_div').hide();
                                    }
                                }
                            }
                        });                    
                    }
                }
            });
<?php endif; ?>
             
<?php if (UserHelper::fnGetAvailableReplyMessage()): ?>
            $('.msg_response_view').live('click', function(){    
                $('#msg_response_view_div').css('display', 'block');
                $('.msg_response_view_loading').css('display', 'block');
                t_i_response = $(this).attr('r_to_message');
                                                
                $('#msg_response_div').css('display', 'none');
                $li_msg_response = $(this).parents('li');
                                                
                $.each($('#ulMessages li'),function(){
                    $(this).removeClass('li_post_message'); 
                });
                $li_msg_response.addClass('li_post_message');
                                                
                $.ajax({
                    url:"/private/messages/getMessageById"+jquery_params,
                    dataType:'jsonp',
                    data: {
                        i_response: t_i_response
                    },
                    success:function(resp){                        
                        if(resp.code==code_success){
                            oData = resp.data;
                            $('.msg_response_view_loading').css('display', 'none');
                            $('.msg_response_view_inner div h4').text(oData.userRegistrationOut);
                            $('.msg_response_view_inner div p').text(oData.messageOut);
                            $('.r_view_date').text(oData.dateCreationOut);
                        }
                    }
                });
            });     
                                            
                                            
            $('.msg_response_btn').live('click', function(){      
                                            
                $('#msg_response_view_div').hide();
                                            
                $('#f_instant_message_response').val($(this).attr('r_to_message'));
                $('#f_instant_to_id').val($(this).attr('r_to_user'));
                                            
                $li_msg_response = $(this).parents('li');
                                                
                $.each($('#ulMessages li'),function(){
                    $(this).removeClass('li_post_message'); 
                });
                $li_msg_response.addClass('li_post_message');
                $('#msg_response_div').css('display', 'block');
                                                
                if($li_msg_response.offset().top - 110 < 0){
                    var li_total_fil = 0;
                    $.each($('#ulMessages li:lt('+($li_msg_response.index()-1)+')'), function(){
                        li_total_fil += $(this).height() +2;
                    });
                    $('#ulMessages').scrollTop(li_total_fil);
                }
                $('#msg_response_div').css('top', $li_msg_response.offset().top - 110);

            });
                                            
                                            
            $('#ulMessages').scroll(function(){
                if($('#msg_response_div').is(':visible') && $li_msg_response != null){                    
                    if($li_msg_response.offset().top - 110< 0 || $li_msg_response.offset().top - 110 > 400 - $li_msg_response.height() ){
                        $li_msg_response.removeClass('li_post_message');
                        $('#msg_response_div').hide();
                        f_instant_response.currentForm.reset();
                        f_instant_response.resetForm();
                        $('input, select, textarea', f_instant_response.currentForm).removeClass('ui-state-error');
                        $('#msg_response_div').css('display', 'none');                        
                    }else{
                        $('#msg_response_div').css('top', $li_msg_response.offset().top-110);
                    }
                }
            });
<?php endif; ?>
             
             
<?php if (UserHelper::fnGetAvailableWriteMessage()): ?>
            var s_txt_search_text_t = '<?php echo __("Para usuario(s)") ?>';
            var s_txt_message_text_t = '<?php echo __("Aqui tu Mensaje") ?>';

            $( ".class_effect_light" ).focus(function(){
                if($(this).attr('id') == 'dlg_form_value_search' ){
                    if($(this).val()==s_txt_search_text_t){
                        $(this).val('');
                    }
                    $(this).removeClass('lightSearch');
                    return;
                }
                if($(this).attr('id') == 'dlg_form_message_content' ){
                    if($(this).val()==s_txt_message_text_t){
                        $(this).val('');
                    }
                    $(this).removeClass('lightSearch');
                    return;
                }
            }).blur(function(){
                if($(this).attr('id') == 'dlg_form_value_search' ){
                    if($(this).val()==''){
                        $(this).addClass('lightSearch');
                        $(this).val(s_txt_search_text_t);
                    }
                    return;
                }
                if($(this).attr('id') == 'dlg_form_message_content'){
                    if($(this).val()==''){
                        $(this).addClass('lightSearch');
                        $(this).val(s_txt_message_text_t);
                    }
                    return;
                }
            });

            $( "#dlg_form_value_search" ).autocomplete({
                minLength: 1,
                source:function(req,response){
                    $.ajax({
                        url:"/private/messages/getUsersMessage"+jquery_params,
                        dataType:'jsonp',
                        data: {
                            term : req.term
                        },
                        success:function(resp){
                            if(resp.code==code_success){
                                response( $.map( resp.data, function( item ) {
                                    var text = "<span class='spanSearchLeft'>" + item.value_search + "</span>";                                
                                    text += "<span class='spanSearchRight'>" + item.prefix + "</span>";                                
                                    text += "<div class='clear'></div>";
                                    return {
                                        label:  text.replace(
                                        new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(req.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>" ),
                                        value:  '@'+item.value_search+':'+item.prefix,                                    
                                        to_city : item.to_city,
                                        to_office : item.to_office,
                                        to_user : item.to_user

                                    }
                                }));
                            } else {
                                msgBox(resp.msg,resp.code);
                            }
                        }
                    });
                },
                select: function( event, ui ) {                
                    $('#dlg_form_to_city').val(ui.item.to_city);
                    $('#dlg_form_to_office').val(ui.item.to_office);
                    $('#dlg_form_to_user').val(ui.item.to_user);
                },
                change: function( event, ui ) {
                    if ( !ui.item ) {
                        $( this ).val( s_txt_search_text_t );
                        $( this ).addClass('lightSearch');

                        $('#dlg_form_to_city').val('');
                        $('#dlg_form_to_office').val('');
                        $('#dlg_form_to_user').val('');

                        return false;
                    }
                    return true;
                }
            }).data( "autocomplete" )._renderItem = function( ul, item ) {               
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };

            $.validator.addMethod("v_ini_message_text_t", function(value, element, params) {            
                return value==s_txt_message_text_t?false:true; 
            }, "<?php echo __('Mensaje requerido') ?>");

            $.validator.addMethod("v_ini_to_text_t", function(value, element, params) {            
                return value==s_txt_search_text_t?false:true; 
            }, "<?php echo __('Usuario requerido') ?>");

            form_message_t = $('#form_message_t').validate({
                rules: {
                    dlg_form_message_content: {
                        required: true,
                        v_ini_message_text_t: true
                    },
                    dlg_form_value_search: {
                        required: true,
                        v_ini_to_text_t: true
                    }
                },
                messages: {
                    dlg_form_message_content: {
                        required: '<?php echo __("Mensaje es requerido") ?>'
                    }
                },
                errorContainer:'#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {
                    $(element).addClass('ui-state-error');
                },
                unhighlight:function(element, errorClass, validClass){
                    $(element).removeClass('ui-state-error');
                },
                submitHandler: function(oForm){
                    if(bAvailableSaveMessage){
                        bAvailableSaveMessage = false;
                        showLoading = 1;
                        o_data_form = $(oForm).serializeObject();                        
                        $.extend(o_data_form, {t_type_notification: 1});
                        $.ajax({
                            url:"/private/messages/saveNotification"+jquery_params,
                            dataType:'jsonp',
                            data: o_data_form,
                            success:function(resp){
                                bAvailableSaveMessage = true;
                                if(resp.code==code_success){
                                    oData = resp.data;
                                    if(oData==1)
                                        $('#dlg_message_t').dialog('close');
                                }
                            }
                        });                    
                    }
                }
            });
                                            
            $('#add_message_t').button({
                icons: {
                    primary: 'ui-icon-pencil'
                },
                text: false
            });

            $('#add_message_t').click(function(){
                $('#dlg_message_t').dialog('open');
            });

            $('#dlg_message_t').dialog({
                title: '<?php echo __("Registro Nuevo Mensaje") ?>',
                autoOpen: false,
                resizable: false,
                dragable: false,
                height:'auto',
                width:400,
                modal: true,
                hide: 'drop',
                show: 'drop',
                buttons: {                
                    "<?php echo __("Aceptar") ?>": function() {                    
                        $('#form_message_t').submit();                    
                    }
                }
            });
                                                                                                    
<?php endif; ?>
    });
        </script>
    </head>
   <?php
    //cache por 8 horas
    if (!Fragment::load(Session::instance('database')->get('sessionKeyId') . Session::instance('database')->get('user_name') . 'admin_header', 28800, true)) {
        ?>
    <body>
        <div id="dlg_message_t" class="none">
            <form id="form_message_t" method="POST" action="#">                
                <input name="dlg_form_to_city" id="dlg_form_to_city" type="hidden"/>
                <input name="dlg_form_to_office" id="dlg_form_to_office" type="hidden"/>
                <input name="dlg_form_to_user" id="dlg_form_to_user" type="hidden"/>
                <ul>
                    <li>
                        <div class="divLine">
                            <dl>                        
                                <dd>
                                    <textarea name="dlg_form_message_content" id="dlg_form_message_content" class="lightSearch class_effect_light"><?php echo __("Aqui tu Mensaje") ?></textarea>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>                
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="divLine">
                            <dl>                        
                                <dd>
                                    <input class="lightSearch class_effect_light" name="dlg_form_value_search" id="dlg_form_value_search" type="text" value="<?php echo __("Para usuario(s)") ?>"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                    </li>
                </ul>
            </form>
        </div>
        <div id="iMenuContainer">

            <div id="iMenu" class="ui-state-default <?php echo $header_full_css_class; ?>">
                <div>
                    <ul class="iMainMenu">
                        <?php foreach ($menu as $mitem) {
                            ?>
                            <li><p>
                                    <?php echo __($mitem->name); ?></p>
                                <?php
                                if (!empty($mitem->items)) {
                                    echo $mitem->printMenu($mitem);
                                }
                                ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div>
                    <ul class="menu_right none">
                         <?php if ((bool) $user_rootOption) { ?>
                        <button id="btncleaningSessionUsers" class="btn left" style="font-size: 13px;"><?php echo __("Limpiar Sesiones Usuarios"); ?></button>
                        <button id="btnRegenerateLanguages" class="btn left" style="font-size: 13px;"><?php echo __("Limpiar Lenguaje"); ?></button>
                        <button id="btnCleaningCache" class="btn left" style="font-size: 13px;"><?php echo __("Limpiar Cache"); ?></button>
                        <?php } ?>
                        <?php if (UserHelper::fnGetAvailableWriteMessage()): ?>

                            <li class="menu_right_li_top menu_right_li_top_no_padding">
                                <button id="add_message_t" class="btn left"  style="font-size: 13px;"><?php echo __("Agregar Mensaje") ?></button>
                            </li>
                        <?php endif; ?>                

                        <li class="menu_right_li_top commentStatus ui-state-default ui-widget ui-corner-all ui-helper-clearfix" style="padding: 6px;">
                            <a id="viewMessages" class="item">
                                <span>
                                    <em class="ui-icon ui-icon-comment"></em>
                                    <b><?php echo __("Mensajes Nuevos") ?> (0)</b>
                                    <div class="clear"></div>
                                </span>
                                <div class="clear"></div>
                            </a>
                            <audio id="sound_message" src="/media/sounds/ChatIncomingInitial.wav"></audio>
                            <div id="menu_cont_msg">
                                <div id="msg_response_view_div">
                                    <div class="msg_response_view_shadow ui-corner-all"></div>
                                    <div class="msg_response_view_cont padding10">                                
                                        <div class="msg_response_view_inner ui-corner-all">
                                            <div class="padding10">
                                                <div class="msg_response_view_loading ui-corner-all"></div>
                                                <h4></h4>
                                                <p>

                                                </p>
                                                <span class="r_view_date"></span>
                                            </div>
                                        </div>                                                             
                                    </div>
                                </div>
                                <div id="msg_response_div">
                                    <div class="msg_response_shadow ui-corner-all"></div>
                                    <div class="msg_response_form_div padding10">
                                        <form id="f_instant_response" method="post" action="#">
                                            <textarea id="f_instant_msg_cont" name="f_instant_msg_cont" alt="<?php echo __("Mensaje") ?>"></textarea>                                
                                            <input id="f_instant_to_id" name="f_instant_to_id" type="hidden"/>
                                            <input id="f_instant_message_response" name="f_instant_message_response" type="hidden"/>
                                            <div class="f_instant_actions">
                                                <button id="f_instant_close" class="f_instant_btn ui-corner-all" type="button">
                                                    <span class="f_instant_icon ui-icon ui-icon-close"></span>                                        
                                                    <span class="f_instant_text"><?php echo __("Cerrar") ?></span>
                                                </button>
                                                <button id="f_instant_submit" class="f_instant_btn ui-corner-all" type="submit">
                                                    <span class="f_instant_icon ui-icon ui-icon-check"></span>
                                                    <span class="f_instant_text"><?php echo __("Aceptar") ?></span>
                                                </button>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="clear"></div>
                                        </form>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <ul id="ulMessages" class="none">                        
                                    <?php foreach (UserHelper::fnGetLastNotifications() as $iCount => $aNotification): ?>                        
                                        <?php if (is_numeric($iCount)): ?>
                                            <li id="message_id_<?php echo $aNotification['idOut'] ?>" unix_time="<?php echo $aNotification['updateNotification'] ?>" class="old_notification notification">
                                                <div class="div_img_action_msg<?php echo ($aNotification['responseOut'] > 0 ? ' div_img_action_response' : '') ?>"></div>
                                                <div class="div_message">
                                                    <div class="general_msg_image ui-corner-all ui-widget-header" style="background: url(<?php echo $aNotification['user_image'] ?>) center center no-repeat;backgroud-size: 100%;">

                                                    </div>
                                                    <div class="general_msg_content">
                                                        <div class="row">
                                                            <span class="user_name">
                                                                <?php echo $aNotification['userRegistrationOut'] ?>
                                                            </span>
                                                            <span class="message_date">
                                                                <?php echo $aNotification['dateCreationOut'] ?>
                                                            </span>
                                                        </div>
                                                        <div class="row">
                                                            <div class="message_text">
                                                                <?php echo $aNotification['messageOut'] ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="general_msg_footer">
                                                        <span class="general_time"></span>
                                                        <?php if (UserHelper::fnGetAvailableReplyMessage()): ?>
                                                            <span class="msg_response_btn" r_to_user="<?php echo $aNotification['userCreateOut'] ?>" r_to_message="<?php echo $aNotification['idOut'] ?>">
                                                                <i class="icon_response"></i>
                                                                <?php echo __("Responder") ?>
                                                            </span>
                                                            <?php if ($aNotification['responseOut'] > 0): ?>
                                                                <span class="msg_response_view" r_to_message="<?php echo $aNotification['responseOut'] ?>">
                                                                    <i class="icon_view_msg"></i>
                                                                    <?php echo __("Mostrar") ?>
                                                                </span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>                            
                                    <li <?php
                                    if (UserHelper::fnGetCountPreviousNotification(0, $i_last_notification) == 0) {
                                        echo "class='none'";
                                    }
                                    ?>>
                                        <div class="more_messages">                                    
                                            [<?php echo UserHelper::fnGetCountPreviousNotification(0, $i_last_notification) ?>] <?php echo __("Mensajes Anteriores") ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li style="float: left;">
                            <div class="ibus-profile-btn">
                                <img src="/media/ico/ico_profile.png" style="float: left; margin-right: 5px;"></img>
                                <span><?php echo $username; ?>&nbsp;<label style="text-transform: uppercase; cursor: pointer;">(<?php echo __("es");?>)</label></span>
                            </div> 
                            <div class="ibus-user-profile-options">
                                <div>
                                    <a id="accountInfo"><?php echo __("Mi Perfil"); ?></a>
                                    <a href="/private/authentication/logout"><?php echo __("Cerrar Sesión"); ?></a>
                                    <hr style="margin-top: 2px; margin-bottom: 2px;"/>
                                    <a id="idioma_en" style="cursor: pointer;" >
                                        <?php echo __("Inglés"); ?>
                                        <img src="/media/ico/en.png" alt="[<?php echo __("Inglés"); ?>]" title="[<?php echo __("Inglés"); ?>]" width="16" style="margin-top: 5px;vertical-align:-10% "/>
                                    </a>
                                    <a id="idioma_es" style="cursor: pointer;">
                                        <?php echo __("Español"); ?>
                                        <img src="/media/ico/es.png" alt="[<?php echo __("Español"); ?>]" title="[<?php echo __("Español"); ?>]" width="16" style="margin-top: 5px;vertical-align: -10% "/>
                                    </a>
                                </div>
                            </div> 
                        </li>



                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>
          <div id="content">
                <?php
                Fragment::save();
            }
            ?>
            <div id="subcontent">

                <div id="dynamic_content">
                         <?php
                    $cacheSessionDynamic = Session::instance('database')->get('sessionKeyId') . Request::initial()->controller() . Session::instance('database')->get('user_name') . Request::initial()->action();
                    if (Kohana::$environment == Kohana::DEVELOPMENT) {
                        echo $content;
                    } elseif (Kohana::$environment == Kohana::PRODUCTION) {
                        if ((!Fragment::load($cacheSessionDynamic, 28800, true))) {
                            echo $content;
                            Fragment::save();
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
            <?php
            //cache por 8 horas
            if (!Fragment::load(Session::instance('database')->get('sessionKeyId') . Session::instance('database')->get('user_name') . 'admin_footer', 28800, true)) {
                ?>
        <div id="user_info" class="none">
            <fieldset class="ui-corner-all">
                <legend class="ui-widget-header ui-corner-all"><?php echo __("Usuario") ?></legend>
                <div class="label_output_ui left">

                    <div>
                        <label><?php echo __("Nombre(s)"); ?>:</label>
                        <div id="user_fName"></div>
                    </div>
                    <div>
                        <label><?php echo __("Apellidos"); ?>:</label>
                        <div id="user_lName"></div>
                    </div>
                    <div>
                        <label><?php echo __("Grupo"); ?>:</label>
                        <div id="user_group_name"></div>
                    </div>
                    <div>
                        <label><?php echo __("Usuario"); ?>:</label>
                        <div id="user_name_info"></div>
                    </div>
                    <div>
                        <label><?php echo __("Correo-e"); ?>:</label>
                        <div id="user_email"></div>
                    </div>
                    <div>
                        <label><?php echo __("Oficina"); ?>:</label>
                        <?php echo Helper::selectAll('office', 'idOffice', 'name', 'user_office') ?>
                    </div>
                </div>
                <div class="left">
                    <input id="imgProfile_file" type="hidden"/>
                    <div id="imgProfile_ui" class="ui-corner-all ui-widget-header" style="width: 128px; height: 128px; border-width: 2px; background-repeat: no-repeat;"></div>
                    <br/>
                    <center>
                        <div id="btnImgUpload_ui">
                            <noscript>			
                            <p>Please enable JavaScript to use file uploader.</p>
                            <!-- or put a simple form for upload here -->
                            </noscript>  
                        </div>
                    </center>
                    <br/>
                </div>
            </fieldset>
            <br/>
            <fieldset class="ui-corner-all">
                <legend class="ui-widget-header ui-corner-all"><?php echo __("Terminal") ?></legend>
                <div class="label_output_ui">
                    <div>
                        <label><?php echo __("Sistema Operativo"); ?>:</label>
                        <div id="user_terminal_os"></div>
                    </div>
                    <div>
                        <label><?php echo __("Impresora"); ?>:</label>
                        <div id="user_printer"></div>
                    </div>
                    <div>
                        <label><?php echo __("Navegador"); ?>:</label>
                        <div id="user_browser"></div>
                    </div>
                </div>
            </fieldset>
            <?php if ($ROOT_MIMIC_OPTION): ?>
                <br/>
                <script type="text/javascript">
                    $(document).ready(function(){
                        function changeLogin(id,name){
                            loadingcarga = 1;
                            var params = new Object();
                            if(id!=null && name!=null){
                                params.uid = id;
                                params.ufname = name;
                            } else{
                                params.reset = 'r';
                            }
                            $.ajax({
                                url:'getMimicUser'+jquery_params,
                                data:params,
                                asybn:false,
                                success: function(r) {
                                    $('#user_info').dialog('close');
                                    if(evalResponse(r)){
                                        msgBox(r.msg,r.code,function(){
                                            location.href='/private/authentication/logout';
                                        });
                                    }
                                }
                            });
                        }
                        $('#recoveryLogin').click(function(){
                            changeLogin(null,null);
                        });
                        function createAutocompleteUser(url,input_id){
                            if(typeof $('#'+input_id).data('autocomplete') != 'undefined')
                                $('#'+input_id).autocomplete('destroy');
                            $('#'+input_id).autocomplete({
                                autoFocus: true
                                ,dataType:'jsonp'
                                ,select:function(event,ui){
                                    changeLogin(ui.item.id,ui.item.value);
                                }
                                ,source:function(request,response){
                                    $.ajax({
                                        url:url,
                                        data:{term:request.term},
                                        success: function(r) {
                                            if(r.code==code_success){
                                                //response( $.map( r.data, function( item ) {return { label: item.value,value: item.id}}));
                                                response( $.map( r.data, function( item ) {
                                                    var text = "<span class='spanSearchLeft'>" + item.value+' @ '+item.office+ "</span>";                                
                                                    text += "<div class='clear'></div>";
                                                    return {
                                                        label:  text.replace(
                                                        new RegExp(
                                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                                            $.ui.autocomplete.escapeRegex(request.term) +
                                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                    ), "<strong>$1</strong>" ),
                                                        value:  item.value,
                                                        id : item.id                                   
                                                    }
                                                }));
                                            } else{
                                                msgBox(r.msg,r.code);
                                            }
                                        }
                                    });
                                }
                            }).data( "autocomplete" )._renderItem = function( ul, item ) {            
                                return $( "<li></li>" )
                                .data( "item.autocomplete", item )
                                .append( "<a>" + item.label + "</a>" )
                                .appendTo( ul ).css({width:'250px'});
                            };
                        }
                        createAutocompleteUser('getUsers'+jquery_params,'user_list');
                    });
                </script>
                <fieldset>
                    <legend class="ui-widget-header ui-corner-all"><?php echo __("Iniciar Sesión Como...") ?></legend>
                    <div class="label_output_ui">
                        <div>
                            <label><?php echo __("El Usuario"); ?>:</label>
                            <input id="user_list" name="user_list" type="text"/>
                            <button class="btn" id="recoveryLogin" style="margin-left: 5px;"><?php echo __("Recuperar Mi Inicio de Sesión"); ?></button>
                        </div>
                    </div>
                    <div style="margin: 15px; text-align: justify;">
                        <?php echo __("Para iniciar sesión como otro usuario, busque al usuario, selecciónelo e inicie sesión nuevamente. Para recuperar el inicio de sesión normal, busque su usuario y repita el procedimiento. O simplemente haga click en el botón") . " " . __("Recuperar Mi Inicio de Sesión"); ?>
                    </div>
                </fieldset>
            <?php endif ?>
                
            <?php echo $view_user_info_extra;?>
        </div>
        <div id="user_chpass" class="none">
            <form id="frmChPass" method="post" autocomplete="off">
                <fieldset class="ui-corner-all">
                    <legend class="ui-widget-header ui-corner-all"><?php echo __("Cambio de Contraseña") ?></legend>

                    <div class="label_output">
                        <div>
                            <label><?php echo __("Contraseña actual"); ?>:</label>
                            <input id="chpass_current" name="chpass_current" type="password"/>
                        </div>
                        <div>
                            <label><?php echo __("Contraseña nueva"); ?>:</label>
                            <input id="chpass_new" name="chpass_new" type="password"/>
                        </div>
                        <div>
                            <label><?php echo __("Confirme contraseña"); ?>:</label>
                            <input id="chpass_confirm" name="chpass_confirm" type="password"/>
                        </div>
                    </div>

                    <div id="cp_error_msg" class="ui-helper-hidden ui-state-error ui-corner-all user_msg">
                        <p><span class="ui-icon ui-icon-alert"></span><strong><?php echo __("Alerta"); ?>:</strong><span id="cp-text-error"></span></p>
                    </div>
                    <div id="cp_info_msg" class="ui-helper-hidden ui-state-highlight ui-corner-all user_msg"> 
                        <p><span class="icon_fix"><span class="ui-icon ui-icon-info"></span></span><strong><?php echo __("Exitoso"); ?>:</strong><span id="cp-text-info"></span></p>
                    </div>
                </fieldset>
                <input type="submit" class="ui-helper-hidden" />
            </form>
        </div>

        <div id="errorMessages">
            <div class="header"></div>
            <div class="content">
                <div class="img">
                    <img src="/media/images/msg_error.png" alt="[ERROR]"/>
                </div>
                <div class="text">
                    <div class="title">
                        <?php echo __("Error de Validación"); ?>
                    </div>
                    <div id="messageField" class="message">

                    </div>
                </div>
            </div>
            <div class="footer"></div>
        </div>
         <div id="subfooter" class="ui-widget-header <?php echo $header_full_css_class; ?>">            
        <label style="float: left;"> <?php echo $user_cityName ?> // <?php echo $user_officeName ?> // <?php echo $user_fullName ?> </label>
        <label style="float: right;"><?php echo __("©MCeTS 2009 - 2013 MCeTS. All rights reserved", array('2013' => date('Y'))); ?>&nbsp;&nbsp;</label>
                </div>
        <div id="sessionTimeoutWarning" class="ui-helper-hidden"></div>
          <?php
                Fragment::save();
            }
            ?>                   
    </body>
</html>
