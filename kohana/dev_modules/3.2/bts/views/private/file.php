<script type="text/javascript">
    var popupTitle = '<?php echo __("Archivo"); ?>';
    var newSubTitle = '<?php echo __("Nuevo Archivo"); ?>';
    var editSubTitle = '<?php echo __("Editar Archivo"); ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Archivo"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este archivo?"); ?>';
    var fileNameRequired = '<?php echo __("El nombre del archivo es necesario"); ?>'
    var urlSave = '/private/file/saveFile'+jquery_params;
    var url_files = '/private/file/listFiles'+jquery_params;
    var url_find = '/private/file/getFileById'+jquery_params;
    var url_trash = '/private/file/removeFile'+jquery_params;
    var titleConfirmBox = '<?php echo __("Eliminar Archivo"); ?>';
    var msgConfirmBox = '<?php echo __("¿Está seguro de eliminar este archivo?"); ?>';
    $(document).ready(function(){
        $("#frmFile").validate({
            rules: {
                fileName:{required: true}
            },
            messages:{
                fileName: {required: fileNameRequired}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element) {$(element).addClass('ui-state-error');},
            unhighlight:function(element){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                $.ajax({
                    url:urlSave,
                    data:$(form).serialize(),
                    success:function(response){
                        if(evalResponse(response)){
                            $('#dlgNewFile').dialog('close');
                            $('#file_jqGrid').trigger('reloadGrid');
                        } 
                    }
                });
            }
        });
        
        $('#dlgNewFile').dialog({
            title:popupTitle,
            width:400,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmFile").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        $('#btnNewFile').click(function(){
            $('#fileName').val('');
            $('#fileId').val('');
            $('#dlgNewFile').dialog('open');
        })
        
        $('#file_jqGrid').jqGrid({
            url:url_files,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:280,
            rowList: [20,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Tipo") ?>',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'ft',index:'ft',hidden:true,width:140},
                {name:'fileName',index:'fileName',sortable:true,width:200,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,align:'center',search:false}
            ],
            pager: "#file_jqGrid_pager",
            sortname: 'fileName',
            sortable: true,
            grouping: true,
            viewrecords:true,
            groupingView : {
                groupField : ['ft'],
                groupColumnShow : [false],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,
                groupOrder: ['asc'],
                groupDataSorted : true
                //, plusicon : 'ui-icon-plus'
                //, minusicon : 'ui-icon-minus'
            },
            gridComplete:function(){
                var ids = $('#file_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = "<a class=\"edit\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#file_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $('#cmbFileType').val(response.data.fileType);
                                $('#fileName').val(response.data.fileName);
                                $('#fileId').val(response.data.idFile);
                                $('#dlgNewFile').dialog('open');
                            } 
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(msgConfirmBox,titleConfirmBox,function(response){
                        if(response == true)
                        {
                            $.ajax({
                                url:url_trash,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#file_jqGrid').trigger('reloadGrid');
                                    } 
                                }
                            });
                    
                        }
                    });
                    return false;
                });
            }
        });
        $("#file_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        $('.sectionLink').button({
            icons:{primary: 'ui-icon-extlink'},
            text: true
        }).click(function(){
            window.location.href = "/private/section";
        });
    });
</script>
<style type="text/css">
    .label_output div label{
        min-width: 70px;
    }
    #cmbFileType{
        min-width: 70px;
    }
</style>
<button id="btnNewFile" class="btn"><?php echo __("Nuevo Archivo"); ?></button>
<button  class="sectionLink" ><?php echo __("Ir a Secciones"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Archivos"); ?></div></center>
<center>
    <table id="file_jqGrid"></table>
    <div id="file_jqGrid_pager"></div>
</center>

<div id="dlgNewFile" class="dialog-modal">
    <form id="frmFile" method="post">
        <div class="label_output">
            <div>
                <label><?php echo __("Tipo"); ?></label>
                <?php echo Form::select("cmbFileType", $file_type, NULL, array('id' => 'cmbFileType')); ?>
            </div>
            <div>
                <label><?php echo __("Nombre"); ?></label>
                <input id="fileName" name="fileName" size="30" type="text"/>
            </div>
            <input id="fileId" name="fileId" type="hidden"/>
        </div>
    </form>
</div>