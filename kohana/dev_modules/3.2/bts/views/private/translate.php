<script type="text/javascript">
var validator_translate;
    $(document).ready(function(){    
        //INICIO VALIDADOR
         validator_translate = $("form#frmtranslate").validate({
            rules: {
                t_text:{
                    minlength:1,
                    required: true
                },
                t_textTranslate: {
                    minlength:1,
                    required: true
                }
              
            },
            messages:{
                t_text:{
                    required: '<?php echo __("El Texto es obligatorio.");?>',
                    minlength:jQuery.format('<?php echo __("Se requiere al menos"); ?>{0} <?php echo __("caracteres para el texto."); ?>')
                },
                t_textTranslate: {
                    required: '<?php echo __("La traducción es obligatoria.");?>',
                    minlength:jQuery.format('<?php echo __("Se requiere al menos"); ?>{0} <?php echo __("caracteres para la traducción."); ?>')
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            }
            ,submitHandler: function(form){
        
                loadingcarga=1;
                $.ajax({
                    type:'POST',
                    url:'/private/translate/saveTranslate'+jquery_params,
                    data: $(form).serialize(),
                    dataType:'json',
                    success: function(r){
                        if(r.code==code_success){
                            $('div#frmtranslateDiv').dialog('close');
                            translate_jqGrid.trigger('reloadGrid');
                        
                        } else {
                            msgBox(r.msg,r.code);
                        }
                    }
                });
            }
        });
        
        // mostrar el jqgrid
        var url_options='/private/translate/listTranslate'+jquery_params;
        var translate_jqGrid = jQuery('#translate_jqGrid').jqGrid({
            url:url_options,
            postData:{
                ajax:'ajax'
            },
            datatype: 'json',
            mtype: 'POST',
            rowNum: 20,
            width:900,
            height:'auto',
            rowList: [20,50,75,100],
            colNames:[
                '<?php echo __("ID") ?>',
                '<?php echo __("Texto") ?>', 
                '<?php echo __("Traducción") ?>',             
                '<?php echo __("Acciones") ?>',
              
            ],
            colModel:[
                {name:'ID',index:'ID',key:true,hidden:true},
                {name:'texto_e',index:'translate.text',width:200,align:'left',search:true,ignoreCase:true,searchoptions :['cn']},
                {name:'texto_t',index:'translate.textTranslate',width:200,align:'left'},               
                {name:'translate_actions',index:'translate_actions',hidedlg:true,search:false,width:50}
            ],
            pager:"#translate_jqGrid_pager",
            sortname: 'idTranslate',
            sortable: true,            
            sortorder: "asc",
            rownumbers: true,       
            rownumWidth: 25,                      
            caption: '<?php echo __("Lista de Traducciones"); ?>',
            gridComplete:function(){
                var ids = $('#translate_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                   
                    
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#translate_jqGrid").jqGrid('setRowData',ids[i],{translate_actions:edit + trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    $('.frmSubtitle').text('<?php echo __("Editar Traducción"); ?>'); 
                    var it=$(this).attr("rel");
                    var url_etranslate='/private/translate/getTranslate'+jquery_params;
                    $.ajax({
                        url:url_etranslate,
                        data:{idt:it},
                        type:'POST',
                        success:function(response){
                       
                            if(response.code==code_success){
                                $('#idTranslate').val(response.data.idTranslate)
                                $('#t_text').val(response.data.text);
                                $('#t_textTranslate').val(response.data.textTranslate);
                                                                                          
                                $('div#frmtranslateDiv').dialog('open');
                            }
                            else {
                                msgBox(response.msg,response.code);
                            }
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                msgConfirmBox = $("span.msgConfirmBox").html();
                titleConfirmBox = $("span.titleConfirmBox").html();
                $(".trash").live("click",function(){
                    var it=$(this).attr("rel");
                    var url_dtranslate='/private/translate/removeTranslate'+jquery_params;
                    confirmBox(msgConfirmBox,titleConfirmBox,function(response){
                        if(response == true)
                        {
                            $.ajax(
                            {
                                type: "POST",
                                url: url_dtranslate,
                                data:{idt:it},
                                dataType:'json',
                                success: function(r)
                                {
                                    if(r.code==code_success){                            
                                        translate_jqGrid.trigger('reloadGrid');
                                    } else {
                                        msgBox(response.msg,response.code);
                                    }
                                }
                            });
                        }
                    });
                    return false;
                });
            }
        });
      $("#translate_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        // BOTON NUEVA Traducción
        $('input#btnNewTranslate').click( function (){           
            $('.frmSubtitle').text('<?php echo __("Nueva Traducción"); ?>');   
            $('#idTranslate').val("");
            $('div#frmtranslateDiv').dialog('open')
        });
 
        $('div#frmtranslateDiv').dialog({
            autoOpen:false,
            autoSize:true,
            modal:true,
            width:550,
            height:'auto',
            resizable:false,
            closeOnEscape:true,
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){                    
                    $('form#frmtranslate').submit();                    
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            },
            beforeClose: function(event, ui) {
                validator_translate.currentForm.reset();
                validator_translate.resetForm();
                $('input, select, textarea', validator_translate.currentForm).removeClass('ui-state-error');
            }
        });
        
    });
</script>

<style type="text/css">

</style>
<center><div class="titleTables"><?php echo __("Administración de Traducciones"); ?></div></center>
<input id="btnNewTranslate" type="button" class="btn" value="<?php echo __("Nueva Traducción"); ?>"/>

<center>
    <table id="translate_jqGrid"></table>
    <div id="translate_jqGrid_pager"></div>
</center>

<span class="titleForm"><?php echo __("Traducción"); ?></span>
<span class="titleConfirmBox"><?php echo __("Eliminar Tipo de Opción"); ?></span>
<span class="msgConfirmBox"><?php echo __("¿Está seguro de eliminar este Tipo de Opción?"); ?></span>


<div id="frmtranslateDiv" class="none">
    <form id="frmtranslate" method="post" action="" class="frm" >
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">

                <div>                       
                    <label><?php echo __("Texto"); ?>:</label>                  
                    <textarea id="t_text" name="t_text" ></textarea>
                </div>
                <div>
                    <label><?php echo __("Traducción"); ?>:</label>
                    <textarea id="t_textTranslate" name="t_textTranslate" ></textarea>   
                </div>
                <input type="hidden" name="idTranslate" id="idTranslate" value=""/>
            </div>
        </fieldset>        

    </form>
</div>



