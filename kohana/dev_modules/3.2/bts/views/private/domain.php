
<div class="ui-widget-content ui-corner-all" style="padding: 4px; width: 800px; margin:0 auto;">
    <div class="ui-state-active ui-corner-all" style="padding: 4px;">
        <?php echo __("Información del Dominio"); ?>
    </div>
    <br/>
    <div class="label_output_ui">
        <div class="left">
            <label><?php echo __("Dominio"); ?></label>
            <input type="text" value="<?php echo $domainName;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Tema"); ?></label>
            <input type="text" value="<?php echo $skin;?>" disabled />
        </div>
        <div class="clear"></div>
        <div>
            <label><?php echo __("Web site"); ?></label>
            <input type="text" value="<?php echo $siteName;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Nombre o IP del Servidor"); ?></label>
            <input type="text" value="<?php echo $serverNameIP;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Puerto MySQL"); ?></label>
            <input type="text" value="<?php echo $dbPort;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Nombre base de datos"); ?></label>
            <input type="text" value="<?php echo $dbName;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Usuario base de datos"); ?></label>
            <input type="text" value="<?php echo $dbUser;?>" disabled />
        </div>
        <div>
            <label><?php echo __("Contraseña base de datos"); ?></label>
            <input type="text" value="<?php echo $dbPassword;?>" disabled />
        </div>
    </div>
</div>