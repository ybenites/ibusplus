<script type="text/javascript">
    var url_updateDataRegister="/private/parameters/updateDataRegister";
    var notification='<?php echo __('Confirmacion')?>';
    $(document).ready(function(){        
        var frmDataRegister=$("#frmDataRegister").validate({
            rules: {
            },
            messages:{
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){   
                alert('hola');
            }
        });
        
        $('form#frmDataRegister input').keyup(function(event){            
            if(event.keyCode==13){
                if(!$("#frmDataRegister").valid())
                    return false;
                var valueRegister=$(this).val();
                var datai=$(this).attr('datai');
                console.log(datai);
                $.ajax({
                    url:url_updateDataRegister+jquery_params,                     
                    type:'POST',
                    dataType:'json',
                    data:{
                        valueRegister:valueRegister,
                        datai:datai
                    },
                    beforeSend:function(){
                        $("#loading").dialog("open");
                    },
                    complete:function(){
                        $("#loading").dialog("close");
                    },
                    success:function(response){ 
                        if(response.code==code_success){
                            var data=response.data;
                            msgBox(data,notification);
                        }
                    }
                });
            }
        });
    });
</script>
<style type="text/css">
    form#frmDataRegister{
        width: 500px;
        margin-left: auto;
        margin-right: auto;
     
    }
    form#frmDataRegister fieldset
    {
           padding: 5px;
    }
</style>

<div>
    <div class="titleTables" style="margin-left: auto;margin-right: auto;"><?php echo __('ADMINISTRACIÓN DE PARAMETROS'); ?></div>
</div>

<div>
    <form id="frmDataRegister" method="POST">
        <fieldset>
            <legend><?php echo __('Datos de Registros');?></legend><br/>
            <i>**<?php echo __('Presionar ENTER en cada campo para guardar los cambios.');?></i><br/><br/>
            <div class="label_output">
            <?php foreach ($dataRegisterTotal as $value) {
                ?>
            <div>
                <label><?php echo trim($value['display']);?> :</label>
                <input type="text" value="<?php echo trim($value['value']);?>" name="<?php echo trim($value['name']);?>" id="<?php echo trim($value['name']);?>" datai="<?php echo trim($value['name']);?>" size="50" class="required" />
            </div>
                <?php
            }?>
            </div>
        </fieldset>
    </form>
</div>  