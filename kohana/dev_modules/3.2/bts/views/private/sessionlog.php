<script type="text/javascript">
    $(document).ready(function(){
        var jquery_date_format = '<?php echo $jquery_date_format; ?>';
        var sql_date_format = '<?php echo $sql_date_format; ?>';
        var url_sessionlog='/private/sessionlog/listSessionLog'+jquery_params;
        $('#sessionlog_jqGrid').jqGrid({
            url:url_sessionlog,
            datatype: 'json',
            mtype:'POST',
            rowNum:50,
            height:300,
            rowList: [50,70,100],
            colNames:[
                'id',
                'eDate',
                '<?php echo __("Evento") ?>',
                '<?php echo __("Usuario") ?>',
                '<?php echo __("Fecha Hora Evento") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'eventDate',index:'eventDate',hidden:true,hidden:true,hidedlg:true},
                {name:'event',index:'event',width:160,search:false,align:'center'},
                {name:'user',index:'u.fullName',sortable:true,width:250,ignoreCase:true,searchoptions:{sopt:['cn']}},
                {name:'eDate',index:"eventDate",width:160,search:false,searchoptions:{dataInit:function(el){$(el).datepicker({dateFormat:jquery_date_format,
                                onSelect:function(){
                                    $(this).focus();
                                }});} }}
            ],
            pager: "#sessionlog_jqGrid_pager",
            sortname: 'eventDate',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(rowdata.event=='LOGIN'){
                    $('tr#'+rowid+' td:nth-child(3)').addClass('eLogin');
                }
                if(rowdata.event=='LOGOUT'){
                    $('tr#'+rowid+' td:nth-child(3)').addClass('eLogout');
                }
            }
        });
        $("#sessionlog_jqGrid").jqGrid('navGrid','#sessionlog_jqGrid_pager',{del:false,add:false,edit:false,search:false});
        $("#sessionlog_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
        $('#ed').datepicker({
            dateFormat:jquery_date_format,
            onSelect:function(){
                var val = $(this).val();
                $("#sessionlog_jqGrid").setGridParam({
                    postData:{
                        ed:function(){
                            return val;
                        }
                    }}).trigger("reloadGrid");
            }
        });
    });
</script>
<style type="text/css">
    .eLogin{
        color: green;
    }
    .eLogout{
        color:red;
    }
</style>

<center><br/><div class="titleTables"><?php echo __("Historial de Sesiones"); ?></div></center>
<br/>
<div class="ui-corner-all ui-widget-content" style="width:400px; margin: 0 auto;">
    <div class="label_output">
        <div>
            <label><?php echo __("Fecha"); ?></label>
            <input id="ed" type="text" value="<?php echo $today; ?>" readonly=""/>
        </div>
    </div>
</div>
<br/>
<center>
    <table id="sessionlog_jqGrid"></table>
    <div id="sessionlog_jqGrid_pager"></div>
</center>
