<style type="text/css">
    body{background-color: white;}
    div#subcontent{margin-top: 0px;}
    .div-border{
        border: red solid thin;
        background-color: white;
    }
    .g-margin{margin: 5px;}
    #cmp-container{
        border-bottom: 1px solid #EEEEEE;
    }
    .component{
        border-radius: 4px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        float: left;
        margin: 5px;
        font-size: 1.5em;
    }
    .active-link{
        background-color: #57A957;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        background-image: -moz-linear-gradient(center top , #62C462, #57A957);
        background-image: linear-gradient(center top , #62C462, #57A957);
        background-image: -webkit-linear-gradient(top, #62C462, #57A957);
        background-image: -o-linear-gradient(top, #62C462, #57A957);
        background-image: linear-gradient(top, #62C462, #57A957);
        background-repeat: repeat-x;
        color: white;
    }
    .group-link{
        background-color: #404040;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        background-image: -moz-linear-gradient(center top , #C0C0C0, #404040);
        background-image: linear-gradient(center top , #C0C0C0, #404040);
        background-image: -webkit-linear-gradient(top, #C0C0C0, #404040);
        background-image: -o-linear-gradient(top, #C0C0C0, #404040);
        background-image: linear-gradient(top, #C0C0C0, #404040);
        background-repeat: repeat-x;
        color: white;
    }
    .deactive-link{
        background-color: #C43C35;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        background-image: -moz-linear-gradient(center top , #EE5F5B, #C43C35);
        background-image: -webkit-linear-gradient(top, #EE5F5B, #C43C35);
        background-image: -o-linear-gradient(top, #EE5F5B, #C43C35);
        background-image: linear-gradient(top, #EE5F5B, #C43C35);
        background-repeat: repeat-x;
        color: white;
    }
    .open-link{
        background-color: #165696;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        background-image: -moz-linear-gradient(center top , #2692FF, #165696);
        background-image: -webkit-linear-gradient(top, #2692FF, #165696);
        background-image: -o-linear-gradient(top, #2692FF, #165696);
        background-image: linear-gradient(top, #2692FF, #165696);
        background-repeat: repeat-x;
        color: white;
    }
    .simple-link{
        background-color: #C6C6C6;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        background-image: -moz-linear-gradient(center top , #F2F2F2, #C6C6C6);
        background-image: -webkit-linear-gradient(top, #F2F2F2, #C6C6C6);
        background-image: -o-linear-gradient(top, #F2F2F2, #C6C6C6);
        background-image: linear-gradient(top, #F2F2F2, #C6C6C6);
        background-repeat: repeat-x;
        color: black;
    }
    .component > div{ margin: 5px;}
    .component:hover,.active-link:hover,.deactive-link:hover,.open-link:hover,.group-link:hover{
        background-position: 0 -15px;
        cursor: pointer;
    }
    .open-link:hover,.active-link:hover,.deactive-link:hover{
        cursor: default;
    }
    div#opt-items{margin-left: 15px;}
    div#opt-title{
        margin-top:15px;
        margin-bottom:15px;
        width: 40%;
        text-align: center;
        font-size: 1.7em;
        line-height: 2em;
        position: relative;
    }
    .opt-item{
        width: 450px;
        height: 45px;
        margin: 5px;
        font-size: 1.2em;
        line-height: 2.5em;
        border: 1px solid #EEEEEE;
        float: left;
        position: relative;
    }
    .opt-item > div > div:first-child{
        text-align: center;
    }

    .opt-item-value{
        width: 100px;
        float: left;
    }
    .opt-item-sDesc{
        width: 300px;
        float: left;
    }
    .opt-item-sDesc > div:hover{
        text-decoration: underline;
        cursor: pointer;
    }
    .opt-item-sDesc div, .opt-item-lDesc{
        padding: 4px;
        line-height: 1em;
        color:gray;
        text-align: justify;
    }
    .opt-item-lDesc{
        margin-left: 20px;
        margin-right: 20px;
        overflow: auto;
    }
    .opt-item-ico{
        width: 16px;
        height: 16px;
        background-repeat: no-repeat;
        position: absolute;
        cursor: pointer;

    }
    .opt-item-edit{
        background-image: url('/media/ico/edit_shadow.png');
        top: 5px;right: 5px;
    }
    .opt-item-edit:hover{
        background-image: url('/media/ico/edit.png');
    }
    .opt-item-expand{
        background-image: url('/media/ico/ico_expand_shadow.png');
        bottom: 5px;right: 5px;
    }
    .opt-item-expand:hover{
        background-image: url('/media/ico/ico_expand.png');
    }
    .opt-item-number{
        width: 32px;
        height: 32px;
        background-repeat: no-repeat;
        position: absolute;
        background-image: url('/media/ico/ico_circle_black.png');
        top: -12px; left: -12px;
        text-align: center;
        line-height: 32px;
        color: white;
        font-weight: bold;
    }
    .opt-item-edit-cancel{
        background-image: url('/media/ico/ico_cancel.png');
        top: 5px;right: 5px;
    }
    .opt-item-edit-save{
        background-image: url('/media/ico/ico_save_16x16.png');
        top: 24px;right: 5px;
    }
    .opt-title-ico-back{
        width: 32px;
        height: 32px;
        background-repeat: no-repeat;
        position: absolute;
        background-image: url('/media/ico/ico_return.png');
        top: 5px; right: 5px;
        background-position: center;
        cursor: pointer;
    }
    .search-msg{
        position: absolute; 
        right: 5px; 
        min-width: 100px;
        max-width: 250px;
        line-height: 1.5em;
        font-size: 1.2em;
        display: none;
    }
    .search-msg-success{border: solid #77c244 2px;}
    .search-msg-none{border: solid #FFD324 2px;}
    .m-fix{margin: 10px; float: left;}
    .element-edit{position: absolute; right: 26px; top: 5px}
    .boolean-edit{width: 50px; text-align: center;}
    .boolean-edit div{display: inline-block; width: 45px; line-height: 18px;}
    .boolean-edit div span{margin: 4px;}
    .boolean-edit .active-link, .boolean-edit .deactive-link{cursor: pointer;}
    .select-edit{min-width: 50px;}
    #opt-lDesc {position: relative;}
    #opt-lDesc .closeInfo{width: 16px; height: 16px; position: absolute; left: 10px; top: 5px;}
    #opt-lDesc > div.g-margin{border-width: 2px;}
    #opt-lDesc .txtInfo{margin-left: 25px;}
</style>
<script type="text/javascript">
    var txt_on = '<?php echo __("Activado"); ?>';
    var txt_off = '<?php echo __("Desactivado"); ?>';
    var edit_txt = '<?php echo __("Editar"); ?>';
    var cancel_txt = '<?php echo __("Cancelar"); ?>';
    var save_txt = '<?php echo __("Guardar"); ?>';
    var expand_txt = '<?php echo __("Más opciones"); ?>';
    var refresh_txt = '<?php echo __("Regenerar CSS"); ?>';
    var group_no_name_txt = '<?php echo __("Grupo sin Nombre"); ?>';
    var no_results = '<?php echo __("No se encontraron coincidencias para [SEARCH_TXT]"); ?>';
    var yes_results = '<?php echo __("Se encontró [N] coincidencia(s) para [SEARCH_TXT]"); ?>';
    var empty_desc_txt = '<?php echo __("No existe descripción disponible"); ?>';
    var group_text = '<?php echo __("Grupo"); ?>';
    $(document).ready(function(){
        $.mask.masks.opt_int = {mask: '9', type:'repeat'};
        $.mask.masks.opt_dec = {mask: '99.99999999999999999999', type:'reverse'};
        $.mask.masks.opt_per = {mask: '99.99'};
        $('.closeInfo').button({icons:{primary:'ui-icon-close'},text:false}).click(function(){
            $(this).parent().addClass('ui-helper-hidden');
        });
        function loadComponents(){
            $.ajax({
                url:'/private/options/listComponents'+jquery_params,
                success:function(response){
                    if(evalResponse(response)){
                        $.each(response.data,function(idx,obj){
                            var component = $(document.createElement('div'));
                            $.data(component.get(0),'cmp',obj.component);
                            component.append($(document.createElement('div')).text(obj.aliasComponent));
                            component.addClass('component simple-link');
                            $('div#cmp-container').append(component);
                        });
                        $('div#cmp-container').append($(document.createElement('div')).addClass('clear'));
                    }
                }
            });
        }
        loadComponents();
        $('#cmp-container').on('click','.component',function(){
            if(!$(this).hasClass('open-link')){
                $('#cmp-container > div').removeClass('open-link').addClass('simple-link');
                $(this).addClass('open-link').removeClass('simple-link');
                loadOptions($(this).data('cmp'));
            }    
        });
        function loadOptions(cmp,optKey,fn){
            var params = new Object();
            if(typeof cmp !=null) params.cmp = cmp;
            if(typeof optKey !='undefined') params.key = optKey;
            $.ajax({
                url:'/private/options/getOptions'+jquery_params,
                data:params,
                beforeSend:function(){
                    $('#opt-container').hide('drop');
                },
                complete:function(){
                    $('#opt-container').show('drop');
                },
                success:function(response){
                    $('#txtFilter').val('');
                    $('#search_info').hide().addClass('ui-helper-hidden').find('div').html('');
                    if(evalResponse(response)){
                        $('div#opt-items > div.opt-item').remove();
                        $.each(response.data,function(idx,obj){
                            var option = $(document.createElement('div')).addClass('opt-item ui-corner-all');
                            var option_edit = $(document.createElement('div')).addClass('opt-item-ico opt-item-edit ui-helper-hidden').attr('title',edit_txt).click(editOption);
                            var cancel_edit = $(document.createElement('div')).addClass('opt-item-ico opt-item-edit-cancel ui-helper-hidden').attr('title',cancel_txt).click(cancelEditOption);
                            var save_edit = $(document.createElement('div')).addClass('opt-item-ico opt-item-edit-save ui-helper-hidden').attr('title',save_txt).click(saveEditOption);
                            $.data(option_edit.get(0),'key',obj.key);
                            $.data(save_edit.get(0),'key',obj.key);
                            $.data(option_edit.get(0),'dt',obj.dataType);
                            $.data(option_edit.get(0),'aMode',obj.hasArrayValues);
                            $.data(option_edit.get(0),'value',obj.value);
                            var option_number = $(document.createElement('div')).addClass('opt-item-number').text(eval(idx+1));
                            var optionMargin = $(document.createElement('div')).addClass('g-margin');
                            var value = createValueOptionItem(obj);
                            var sDesc = $(document.createElement('div')).addClass('opt-item-sDesc').append($(document.createElement('div')).text(obj.shortDescription).click(showMoreInfo));
                            $.data(sDesc.get(0),'ldesc',obj.longDscription);
                            optionMargin.append(value).append(sDesc).append($(document.createElement('div')).addClass('clear'));
                            option.append(optionMargin).append(option_number);
                            if(obj.subOptions == 0){
                                option.append(option_edit).append(cancel_edit).append(save_edit);
                            } else {
                                $.data(value.get(0),'key',obj.key);
                                $.data(value.get(0),'cmp',null);
                                $.data(value.get(0),'gn',obj.groupName);
                                value.click(expandOption);
                                
                                var option_expand = $(document.createElement('div')).addClass('opt-item-ico opt-item-expand ui-helper-hidden').attr('title',expand_txt);
                                $.data(option_expand.get(0),'key',obj.key);
                                $.data(option_expand.get(0),'cmp',null);
                                $.data(option_expand.get(0),'gn',obj.groupName);
                                option_expand.click(expandOption);
                                option.append(option_expand);
                            }
                            option.mouseenter(hoverFunction).mouseleave(blurFunction);
<?php if ($user_rootOption) { ?>
                                option_number.attr('title',obj.key);
                                option_number.tipTip({maxWidth: "auto", edgeOffset: 0, defaultPosition:'right',activation:'click',fadeIn:0,fadeOut:0});
<?php } ?>
                            //option.append($(document.createElement('div')).html(obj.longDscription).addClass('opt-item-lDesc ui-helper-hidden'));
                            $('div#opt-items').append(option);
                        });
                        if(typeof fn ==='function') fn();
                        if(typeof optKey ==='undefined') {
                            if($.trim($('#opt-title').text())!='') {$('#cmp-container').show('blind');}
                            $('#opt-title').text('').html('');
                        } else{$('#cmp-container').hide('blind');}
                        if(params.cmp=='COLORES') $('#cmp_actions').find('.g-margin').append($(document.createElement('button')).text(refresh_txt).button({icons:{secondary:'ui-icon-refresh'}}).click(createFileColors));
                        else $('#cmp_actions').find('.g-margin').html('');
                        $('#opt-lDesc div.txtInfo').html('');
                        if(!$('#opt-lDesc').hasClass('ui-helper-hidden'))$('#opt-lDesc').addClass('ui-helper-hidden');
                    }
                }
            });
        }
        function createFileColors(){
            $.ajax({
                url:'/private/options/getGenerateFileColors'+jquery_params,
                success:function(response){
                    if(evalResponse(response)){
                        msgBox(response.msg,response.code);
                    }
                }
            });
        }
        function createValueOptionItem(obj){
            var value =  $(document.createElement('div')).addClass('opt-item-value ui-corner-all');
            var objValue = null;
            if(obj.suffix!=null && $.trim(obj.suffix)!='') objValue = obj.value+' '+obj.suffix;
            else objValue = obj.value;
            if(parseInt(eval(obj.subOptions)) > 0){
                value.addClass('group-link').text(group_text);
            } else {
                switch(obj.dataType){
                    case 'boolean':
                        if(Boolean(eval(obj.value)))
                            value.addClass('active-link').text(txt_on);
                        else
                            value.addClass('deactive-link').text(txt_off);
                        break;
                    case 'percentage':
                        value.addClass('open-link').text(objValue);
                        break;
                    case 'decimal':
                        value.addClass('open-link').text(objValue);
                        break;
                    case 'string':
                        value.addClass('open-link').text(objValue);
                        break;
                    case 'integer':
                        value.addClass('open-link').text(objValue);
                        break;
                    case 'color':
                        value.addClass('open-link')
                        .attr('style','background-image:none; background-color:'+obj.value+';')
                        .append($(document.createElement('span')).addClass('ui-corner-all').text(obj.value).css('background-color','black').css('padding','2px'));
                        break;
                    case 'array':
                        value.addClass('open-link').text(objValue);
                        break;
                }
            }
            return value;
        }
        function hoverFunction(){$(this).find('.opt-item-edit,.opt-item-expand').not('.editable').removeClass('ui-helper-hidden');}
        function blurFunction(){ $(this).find('.opt-item-edit,.opt-item-expand').not('editable').addClass('ui-helper-hidden');}
        
        function expandOption(event){
            loadOptions($('#cmp-container').find('div.open-link').data('cmp'),$(this).data('key'),setTitle(this));   
        }
        function setTitle(element){
            if($(element).data('gn')!=null && $.trim($(element).data('gn'))!='') $('#opt-title').text($(element).data('gn'));
            else $('#opt-title').text(group_no_name_txt);
            var returnLink = $(document.createElement('div')).addClass('opt-title-ico-back');
            $.data(returnLink.get(0),'cmp',$('#cmp-container').find('div.open-link').data('cmp'));
            $.data(returnLink.get(0),'key',$(element).data('key'));
            returnLink.click(function(){
                loadOptions($('#cmp-container').find('div.open-link').data('cmp'), $(element).data('key'), setTitle(returnLink.get(0)));
            });
            $('#opt-title').append(returnLink);
        }
        
        function filterOption(term){
            if($('div#search_info').hasClass('ui-helper-hidden'))
                $('div#search_info').hide('clip');
            $('div#search_info').removeClass('search-msg-none').removeClass('search-msg-success').find('div').html('');
            var cResult=0;
            var optFilter = $('div.opt-item div.opt-item-sDesc').filter(function(index){
                var response =($(this).text().toLowerCase().indexOf(term) == -1);
                if(response) cResult++;
                return response;
            });
            if(optFilter.length==0 || $.trim(term)=='') $('div.opt-item').show();
            else {
                var html;
                var txt;
                var css;
                if($('div.opt-item').length > cResult){
                    optFilter.parents('div.opt-item').hide();
                    txt = yes_results;
                    css = 'search-msg-success';
                } else {
                    txt = no_results;
                    css = 'search-msg-none';
                }
                html = txt.toString().replace('[N]','<b>'+eval($('div.opt-item').length - cResult)+'</b>','g').replace('[SEARCH_TXT]','<b>"'+term+'"</b>','g');
                $('div#search_info > div').html(html);
                $('div#search_info').addClass(css).show('clip').removeClass('ui-helper-hidden');
            };
        }
        
        $('#frmFilter').validate({
            submitHandler:function(form){
                var term = $(form).serializeObject();
                filterOption(term.txtFilter);
                $("#txtFilter").trigger('click');
                return false;
            }
        });
        $("#txtFilter").click(function() {
            $(this).select();
        });
        $("#txtFilter").keyup(function() {
            if($.trim($(this).val())==='') filterOption('');
        });
         
        function editOption(){
            var parent = $(this).parent();
            $(this).addClass('ui-helper-hidden');
            parent.find('.opt-item-edit,.opt-item-expand').addClass('editable');
            parent.find('.opt-item-value').hide('drop');
            parent.find('.opt-item-edit-cancel').removeClass('ui-helper-hidden');
            parent.prepend($(document.createElement('div')).addClass('m-fix'));
            var editElement = null;
            if(Boolean(eval($(this).data('aMode')))){
                parent.find('.opt-item-edit-save').removeClass('ui-helper-hidden');
                editElement = createArraySelectValues($(this).data('key'),$(this).data('value'),this);
            } else{
                switch($(this).data('dt')){
                    case 'boolean':
                        editElement = createBooleanEdit($(this).data('value'));
                        break;
                    case 'booleantf':
                        editElement = createBooleanEdit($(this).data('value'));
                        break;
                    default:
                        parent.find('.opt-item-edit-save').removeClass('ui-helper-hidden');
                        editElement = createInputEdit($(this).data('dt'),$(this).data('value'));
                        break;
                }
            }
            parent.append($(document.createElement('div')).addClass('element-edit').append(editElement));
        }
        
        function cancelEditOption(){
            var parent = $(this).parent();
            parent.find('.m-fix,.element-edit').remove();
            $(this).addClass('ui-helper-hidden');
            parent.find('.opt-item-edit-save').addClass('ui-helper-hidden');
            parent.find('.opt-item-value').show('drop');
            parent.find('.opt-item-edit,.opt-item-expand').removeClass('editable');
            parent.trigger('mouseenter');            
        }
        
        function saveEditOption(){
            var parent = $(this).parent();
            var value = null;
            if(parent.find('.select-edit').length > 0) value = parent.find('.select-edit').val();
            else if(parent.find('.boolean-edit').length > 0){
                value = parent.find('.boolean-edit').find('.selected').data('value');
            } else if(parent.find('.input-edit').length > 0){
                value = parent.find('.input-edit').val();
            }
            updateOptionValue($(this).data('key'),value,this);
            parent.find('.opt-item-edit-cancel').trigger('click');
        }
        
        function createBooleanEdit(value){
            var on_value = $(document.createElement('div')).addClass('active-link ui-corner-top on-value').append($(document.createElement('span')).text('ON')).click(clickBooleanEdit);
            $.data(on_value.get(0),'value',1);
            var off_value = $(document.createElement('div')).addClass('deactive-link ui-corner-bottom off-value').append($(document.createElement('span')).text('OFF')).click(clickBooleanEdit);
            $.data(off_value.get(0),'value',0);
            if(Boolean(eval(value))) on_value.addClass('selected');
            else off_value.addClass('selected');
            var element_booleanEdit = $(document.createElement('div')).addClass('boolean-edit').append(on_value).append(off_value);
            return element_booleanEdit;
        }
        
        function clickBooleanEdit(){
            var parent = $(this).parent();
            if(!$(this).hasClass('selected')){
                parent.find('div.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            var parent = $(this).parents('div.opt-item');
            var save_child = parent.find('.opt-item-edit-save');
            save_child.trigger('click');
        }
        
        function createArraySelectValues(key,value,element){
            var element_select = $(document.createElement('select')).addClass('select-edit ui-widget ui-widget-content ui-corner-left');
            var url = '/private/options/getArrayData'+jquery_params
            $.ajax({
                url:url,
                data:{key:key},
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        $.each(response.data,function(idx,obj){
                            var option = $(document.createElement('option')).attr('value',obj.id).text(obj.value);
                            element_select.append(option);
                        });
                        element_select.val(value);
                    }
                },
                error:function(jqXHR,textStatus,errorThrown){
                    msgBox(errorThrown,textStatus);
                    var parent = $(element).parent();
                    parent.find('.opt-item-edit-cancel').trigger('click');
                }
            });
            element_select.hover(function(){$(this).addClass('ui-state-highlight');}, function(){$(this).removeClass('ui-state-highlight');});
            return element_select;
        }
    
        function createInputEdit(dataType,value){
            var input = $(document.createElement('input')).attr('type', 'text').addClass('input-edit ui-widget ui-widget-content ui-corner-left');
            input.val(value);
            switch(dataType){
                case 'integer':
                    input.attr('alt','opt_int');
                    input.setMask({autoTab: false});
                    break;
                case 'seconds':
                    input.attr('alt','opt_int');
                    input.setMask({autoTab: false});
                    break;
                case 'decimal':
                    input.attr('alt','opt_dec');
                    input.setMask({autoTab: false});
                    break;
                case 'percentage':
                    input.attr('alt','opt_per');
                    input.setMask({autoTab: false});
                    break;
                case 'color':
                    input.attr('readonly','true');
                    input.ColorPicker({
                        onSubmit: function(hsb, hex, rgb, el) {
                            $(el).val('#'+hex);
                            $(el).ColorPickerHide();
                        },
                        onBeforeShow: function () {
                            $(this).ColorPickerSetColor(this.value.toString().replace('#','','g'));
                        }
                    }).bind('keyup', function(){
                        $(this).ColorPickerSetColor(this.value);
                    });
                    break;
            }
            return input;
        }
        function updateOptionValue(key,value,element){
            $.ajax({
                async:false,
                url:'/private/options/updateOptionValue'+jquery_params,
                data:{key:key,value:value},
                success:function(response){
                    if(evalResponse(response)){
                        var valueElement = createValueOptionItem(response.data);
                        var parent = $(element).parent();
                        parent.find('.opt-item-value').remove();
                        parent.find('.g-margin').prepend(valueElement);
                        parent.find('.opt-item-edit').data('value',response.data.value);
                    }
                }
            });
        }
        
        function showMoreInfo(){
            var desc = $(this).parent().data('ldesc');
            if(desc==null) desc = empty_desc_txt;
            $('#opt-lDesc div.txtInfo').html(desc);
            if($('#opt-lDesc').hasClass('ui-helper-hidden')) $('#opt-lDesc').removeClass('ui-helper-hidden');
        }
    });
</script>
<div id="opt-view-container">
    <div style="color: white; font-weight: bold; font-size: 2em; background-color: black; line-height: 2.1em; text-align: center; position: relative;">
        <div style="position: absolute; left:5px;">
            <form id="frmFilter" method="post">
                <div style="font-size: 12px;"> <?php echo __("Buscar"); ?><input style="margin-left: 10px;" type="text" id="txtFilter" name="txtFilter"/></div>

            </form>
        </div>
        <?php echo __("Opciones del Sistema"); ?>
    </div>
    <div id="cmp-container">
    </div>
    <div id="cmp_actions">
        <div class="g-margin"></div>
    </div>
    <div id="opt-lDesc" class="ui-helper-hidden">
        <div class="g-margin ui-state-highlight ui-corner-all">
            <div class="g-margin txtInfo"></div>
        </div>
        <button class="closeInfo"></button>
        <!--div class="ui-state-default ui-corner-all closeInfo"><span class="ui-icon ui-icon-close"></span></div-->
    </div>
    <div id="opt-container">
        <div id="opt-title" class="ui-corner-right open-link">
        </div>
        <div id="opt-items">
        </div>
    </div>

</div>
<div id="search_info" class="ui-corner-all search-msg ui-helper-hidden">
    <div class="g-margin">
    </div>
</div>