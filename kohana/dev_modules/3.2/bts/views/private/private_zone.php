<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title; ?></title>
        <meta name="keywords" content="<?php echo $meta_keywords; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta name="copyright" content="<?php echo $meta_copyright; ?>" />
        <link rel="shortcut icon" href="/media/images/<?php echo $skin; ?>/favicon.png"/>
        <link type="text/css" href="/media/css/ibusplus.custom.font.face.css" rel="stylesheet" />
        <link type="text/css" href="/media/css/<?php echo $skin.'/'.${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}; ?>/jquery-ui-<?php echo ${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}?>.custom.min.css" rel="stylesheet" />
        
        <?php echo HTML::script('media/js/jquery-'.${Kohana_Uconstants::VAR_JQUERY_VERSION}.'.min.js', array('language' => 'javascript')) ?>
        <?php echo HTML::script('media/js/jquery-ui-'.${Kohana_Uconstants::VAR_JQUERY_UI_VERSION}.'.custom.min.js', array('language' => 'javascript')) ?>
        <script type="text/javascript" src="/media/js/ibusplus.repo.code.js"></script>
        
        
        <script type="text/javascript">
            var jquery_params='?jsoncallback=?';
            var code_success = '<?php echo $CODE_SUCCESS; ?>';  
            var code_no_authorized = '<?php echo $CODE_NO_AUTHORIZED; ?>';  
            var code_error= '<?php echo $CODE_ERROR; ?>';
        </script>
        <script type="text/javascript" src="/media/js/jquery.ibusplus.changeLanguage.js"></script>
    </head>
    <?php echo $content; ?>
</html>
