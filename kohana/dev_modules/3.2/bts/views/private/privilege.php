<script type="text/javascript">
    $(document).ready(function(){
        function loadPrivileges(idMenu,parentRow){
            var params = new Object();
            if(typeof idMenu !='undefined')
                params.idm = idMenu;
            $.ajax({
                url:'/private/privilege/listPrivilegies'+jquery_params,
                data:params,
                async:false,
                success:function(response){
                    if(response.code==code_success){
                        //console.log(response.data);
                        $.each(response.data,function(idx,obj){
                            if(typeof idMenu !='undefined')
                                addRow(obj,parentRow,$(parentRow).data('level')+1);
                            else
                                addRow(obj);
                        });
                    } else {
                        msgBox(response.msg,response.code);   
                    }
                }
            });
        }
        loadPrivileges();
        
        function unloadPrivileges(parentRow){
            var lvl = $(parentRow).data('level');
            var rows = $(parentRow).nextAll('tr');
            var childRows = $.map(rows,function(row){
                if($(row).data('level')>lvl)
                    return row;
            });
            $.each(childRows, function(idx,obj){
                $(obj).remove();
            });
        }
        
        function addRow(objDataRow,parentRow,level){
            var hasParent = false
            var lvl = 0;
            if( typeof parentRow != 'undefined' ) hasParent = true;
            if( typeof level != 'undefined' ) lvl = level;
            var row = $(document.createElement('tr'));
            $.data(row.get(0),'level',lvl);
            var fCell =$(document.createElement('td')).text(objDataRow.name);
            var node = $(document.createElement('div'));
            if(objDataRow.type=='M') node.addClass('open_node');
            else node.addClass('php_action_node');
            var ml = eval(lvl*25);
            node.css('margin-left',ml+'px');
            fCell.append(node);
            $.data(fCell.get(0),'id',objDataRow.idMenu);
            row.append(fCell);
            var fGroup = objDataRow.fGroup.split(',');
            var cGroup =  new Array();
            if(objDataRow.cGroup!=null)
                cGroup = objDataRow.cGroup.split(',');
            $.each(fGroup,function(idx,value){
                var cel = $(document.createElement('td'));
                var chk = $(document.createElement('div')).attr('rel',objDataRow.idMenu+':'+value);
                $.data(chk.get(0),'idm',objDataRow.idMenu);
                $.data(chk.get(0),'idg',value);
                if($.inArray(value, cGroup)!=-1) {
                    $.data(chk.get(0),'st',1);
                    cel.append(chk.addClass('check_true'));
                } else {
                    $.data(chk.get(0),'st',0);
                    cel.append(chk.addClass('check_false'));
                }
                chk.click(chkClick);
                cel.append(chk);
                row.append(cel);
            });
            if(!hasParent) $('table#tblPrivilege tbody').append(row);
            else $(parentRow).after(row);
        }
        
        function chkClick(){
            var params = new Object();
            var chk = $(this);
            params.act = chk.data('st');
            params.gid = chk.data('idg');
            params.mid = chk.data('idm');
            $.ajax({
                url: '/private/privilege/createOrDeleteAccess'+jquery_params,
                data:params,
                success:function(response){                                       
                    if(response.code==code_success){
                        if(Boolean(eval(params.act))) chk.data('st',0);
                        else chk.data('st',1);
                        //console.log(response.data);
                        $.each(response.data,function(idx,val){
                            var id = val.toString()+":"+params.gid.toString();
                            if(!Boolean(eval(params.act))){
                                $('div[rel="'+id+'"]').removeClass('check_false').addClass('check_true').data('st',1);
                            } else{
                                $('div[rel="'+id+'"]').removeClass('check_true').addClass('check_false').data('st',0);
                            }
                        });
                    } else {
                        msgBox(response.msg,response.code);
                    }
                       
                }
            });
            
        }
        
        $('table').on('click','.open_node',function(){
            //console.log($(this).parent('td').data('id'));
            loadPrivileges($(this).parent('td').data('id'),$(this).parent('td').parent('tr'));
            $(this).removeClass('open_node').addClass('close_node');
        });
        $('table').on('click','.close_node',function(){
            //console.log($(this).parent('td').data('id'));
            unloadPrivileges($(this).parent('td').parent('tr'));
            $(this).removeClass('close_node').addClass('open_node');
        });
    });
</script>
<style type="text/css">
    #format { margin-top: 2em; }
    #tblPrivilege{
        width: 99%;
        margin: 0 auto;
        border: 2px solid;
    }
    #tblPrivilege thead th {
        padding: 3px 0px 3px 5px;
        text-align: center;        
        font-weight: bold;
        font-size: 14px;

    }
    #tblPrivilege td {
        padding: 2px 5px;
        font-size: 10px;
        border: 2px;
        text-align: center;
        line-height: 20px;
    }
    #tblPrivilege tr td:first-child {
        text-align: left;
    }
    #tblPrivilege tbody tr:hover{
        background-color: greenyellow;
    }

</style>

<center><div class="titleTables"><?php echo __("Administración de Privilegios de Grupo"); ?></div></center>
<table id="tblPrivilege" class="ui-corner-all ui-state-focus" cellpading="0" cellspacing="0">
    <thead class="ui-corner-top ui-widget-header" style="background-image: none;">
        <tr>
            <th style="width: 300px;">
                <?php echo __("Menú"); ?>&nbsp;
                <?php echo __("o"); ?>&nbsp;
                <?php echo __("Acciones"); ?>
            </th>
            <?php
            if (!empty($groups)) {
                foreach ($groups as $g) {
                    ?>
                    <th><?php echo $g->name; ?></th>
                    <?php
                }
            }
            ?>
        </tr>
    </thead>
    <tbody class="ui-widget-content">

    </tbody>
</table>
<!--<table id="table_privileges" border="0" cellpadding="0" cellspacing="0"></table>-->