<script type="text/javascript">
    var office_form_title = '<?php echo __("Oficina"); ?>';
    var office_name_required = '<?php echo __('El nombre de la oficina es obligatorio'); ?>';
    var office_city_required = '<?php echo __('Debe seleccionar una ciudad'); ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Oficina"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar esta oficina?"); ?>';
    var root_option = Boolean(eval('<?php echo $user_rootOption ?>'));
    var url_office = '/private/office/listOffices'+jquery_params;
    var url_trash = '/private/office/removeOffice'+jquery_params;
    var url_find = '/private/office/getOfficeById'+jquery_params;
    var ico_on = '<img src="/media/ico/ico_power_on.png" />';
    var ico_off = '<img src="/media/ico/ico_power_off.png" />';
    $(document).ready(function(){
        $('#office_phone').setMask();
        function listCities(){
            $.ajax({
                url:'/private/office/listCities'+jquery_params,
                data:{t:'A'},
                async:false,
                success:function(response){
                    $('option', '#office_city').remove();
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#office_city').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#office_city').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                            });
                        } else {
                            $('#office_city').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        listCities();
        $("#rootOption").buttonset();
        $("#frmOffice").validate({
            rules: {
                office_name: {
                    required: true
                },
                office_city: {
                    required: true
                }
            },
            messages:{
                office_name: {
                    required: office_name_required
                },
                office_city: {
                    required: office_city_required
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                $.ajax({
                    url: '/private/office/createOrUpdateOffice'+jquery_params,
                    data:$(form).serialize(),
                    success: function(response){
                        if(evalResponse(response)){
                            $('#office_jqGrid').trigger('reloadGrid');
                            $('#frmPopup').dialog('close');
                        } 
                    }
                });
            }
        });
        $('#frmPopup').dialog({
            title:office_form_title,
            width:500,
            buttons:[
                {
                    id:"btn-accept",
                    text: okButtonText,
                    click: function() {
                        $("#frmOffice").submit();
                    }
                },
                {
                    id:"btn-cancel",
                    text: cancelButtonText,
                    click: function() {
                        $(this).dialog("close");
                    }
                }]
        });
        
        $('#btnNewOffice').click(function(){
            $("#office_name").val('');
            $('#office_city').val('');
            $('#office_address').val('');
            $('#office_phone').val('');
            $('#office_id').val('');
            $('#frmPopup').dialog('open');
        })
        
        $('#office_jqGrid').jqGrid({
            url:url_office,
            datatype: 'json',
            mtype:'POST',
            rowNum:30,
            height:300,
            rowList: [30,40,60],
            colNames:[
                'id',
                'cDate',
                'lcDate',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Dirección") ?>',
                '<?php echo __("Teléfono") ?>',
                '<?php echo __("Ciudad") ?>',
                '<?php echo __("Opción ROOT") ?>',
                '<?php echo __("Creado por") ?>',
                '<?php echo __("Creado la fecha") ?>',
                '<?php echo __("Editado por") ?>',
                '<?php echo __("Editado la fecha") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true,hidedlg:true},
                {name:'cDate',index:'cDate',hidden:true,hidden:true,hidedlg:true},
                {name:'lcDate',index:'lcDate',hidden:true,hidden:true,hidedlg:true},
                {name:'name',index:'o.name',sortable:true,width:120},
                {name:'address',index:'address',sortable:true,width:160,search:false},
                {name:'phone',index:'phone',sortable:true,width:70,search:false},
                {name:'city',index:'city',sortable:true,width:100,search:false},
                {name:'rootOption',index:'rootOption',sortable:false,width:70,hidden:(!root_option),search:false,align:'center'},
                {name:'uc',index:'uc',sortable:true,width:140,search:false},
                {name:'regDate',index:'cDate',sortable:true,width:140,search:false},
                {name:'ulc',index:'ulc',sortable:true,width:140,search:false},
                {name:'lastDate',index:'lcDate',sortable:true,width:140,search:false},
                {name:'actions',index:'actions',sortable:false,width:70,search:false}
            ],
            pager: "#office_jqGrid_pager",
            sortname: 'name',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(Boolean(eval(rowdata.rootOption))){
                    $("#office_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_on});
                } else {
                    $("#office_jqGrid").jqGrid('setRowData',rowid,{rootOption:ico_off});
                }
            },
            gridComplete:function(){
                var ids = $('#office_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = edit_template.replace('[ID]',ids[i],'g');
                    trash="";
                    rowData = $('#office_jqGrid').jqGrid('getRowData',ids[i]);
                    if(!Boolean(eval(rowData.systemDefault))){
                        trash = trash_template.replace('[ID]',ids[i],'g');
                    }
                    $("#office_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.edit').click(function(){
                    var id = $(this).data('oid');
                    $.ajax({
                        url:url_find,
                        data:{id:id},
                        success:function(response){
                            if(evalResponse(response)){
                                $("#office_name").val(response.data.name);
                                $("#office_address").val(response.data.address);
                                $("#office_phone").val(response.data.phone);
                                $("#office_city").val(response.data.idCity);
                                $('#office_id').val(response.data.idOffice);
<?php if ((bool) $user_rootOption) { ?>
                                if(Boolean(eval(response.data.rootOption))){
                                    setRadio('rootOptionYES');
                                } else {
                                    setRadio('rootOptionNO');
                                }
<?php } ?>
                                $('#frmPopup').dialog('open');
                            } 
                        }
                    });
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.trash').click(function(){
                    var id = $(this).data('oid');
                    confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                        if(response == true){
                            $.ajax({
                                url:url_trash,
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                        $('#office_jqGrid').trigger('reloadGrid');
                                    }
                                }
                            });
                        }
                    });
                });
                return false;
            }
        });

        $("#office_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
    });
</script>
<style type="text/css">
    #jqgh_office_jqGrid_rootOption{
        height: 30px;
        white-space: pre-wrap;
    }
</style>
<button id="btnNewOffice" class="btn" ><?php echo __("Nueva Oficina"); ?></button>
<center><div class="titleTables"><?php echo __("Administración de Oficinas"); ?></div></center>
<center>
    <table id="office_jqGrid"></table>
    <div id="office_jqGrid_pager"></div>
</center>


<div id="frmPopup" class="dialog-modal">
    <form id="frmOffice" method="post">
        <div class="label_output">
            <div>
                <label><?php echo __("Nombre"); ?>:</label>
                <input id="office_name" name="office_name" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Ciudad"); ?>:</label>
                <select id="office_city" name="office_city" style="width: 200px;"></select>
            </div>
            <div>
                <label><?php echo __("Dirección"); ?>:</label>
                <input id="office_address"  name="office_address" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Teléfono"); ?>:</label>
                <input id="office_phone" name="office_phone" value="" type="text" maxlength="20" size="30" alt="number" />
            </div>
            <?php if ((bool) $user_rootOption) { ?>
                <div>
                    <label style="margin-top: 7px;"><?php echo __("Opción ROOT"); ?>:</label>
                    <div id="rootOption">
                        <input type="radio" id="rootOptionYES" name="rootOption" value="1"/><label for="rootOptionYES" style="min-width: 20px; padding: 0px;"><?php echo __("Sí"); ?></label>
                        <input type="radio" id="rootOptionNO" name="rootOption" checked="checked" value="0"/><label for="rootOptionNO" style="min-width: 20px; padding: 0px;"><?php echo __("No"); ?></label>
                    </div>
                </div>
            <?php } ?>
        </div>
        <input type="hidden" name="office_id" id="office_id" value="" />
    </form>
</div>