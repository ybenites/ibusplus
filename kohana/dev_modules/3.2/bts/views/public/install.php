<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
        <title><?php echo __("Instalación"); ?></title>
        <link rel="shortcut icon" href="/media/images/bts/favicon.png"/>
        <link type="text/css" href="/media/css/bts/jquery.ui.custom.css" rel="stylesheet" />
        <link type="text/css" href="/media/css/ibusplus.custom.theme.forms.css" rel="stylesheet" />
        <link type="text/css" href="/media/css/jquery.alerts.css" rel="stylesheet" />
        <link type="text/css" href="/media/css/jquery.tipTip.css" rel="stylesheet" />
        <script type="text/javascript" src="/media/js/jquery.min.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.ui.custom.min.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.ibusplus.ui.theme.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.validate.min.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.ibusplus.install.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.ibusplus.functions.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.meio.mask.min.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.ui.helptext.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.alerts.js" language="javascript"></script>
        <script type="text/javascript" src="/media/js/jquery.tipTip.js" language="javascript"></script>
        <script type="text/javascript">
            var code_success = '<?php echo $CODE_SUCCESS; ?>';  
            var code_error= '<?php echo $CODE_ERROR; ?>';
            var title='<?php echo __("Instalador BTS"); ?>';
            var btnInstall='<?php echo __("Instalar"); ?>';
            var titleRoot='<?php echo __("Contraseña de Administrador"); ?>';
            var siteName_req='<?php echo __("No ha ingresado el nombre del sitio web"); ?>';
            var website_req='<?php echo __("No ha ingresado la URL del sitio web"); ?>';
            var serverDBName_req='<?php echo __("No ha ingresado la IP o nombre del servidor"); ?>';
            var dbName_req='<?php echo __("No ha ingresado el nombre de la base de datos"); ?>';
            var dbUser_req='<?php echo __("No ha ingresado el nombre de usuario para la base de datos"); ?>';
            var dbPassword_req='<?php echo __("No ha ingresado el password de usuario para la base de datos"); ?>';
            var step_template = '<li><div class="ico_event waiting" title="<?php echo __("Esperando..."); ?>"></div>[MSG]</li>';
            var dbCreateMsg = '<?php echo __("Creación de la base de datos"); ?>';
            var tblCreateMsg = '<?php echo __("Creación de usuario y tablas del sistema BTS"); ?>';
            var stepOne = '<?php echo __("Información de instalación"); ?>';
            var installSuccess = '<?php echo __("La instalación fue exitosa. Redirigiendo al inicio de sesión"); ?>';
            $(document).ready(function(){
                $("#frmInstaller input").tipTip({maxWidth: "auto", edgeOffset: 10, defaultPosition:'right'});
                
                $('#timeZone').helptext({value: 'America/Lima'});
                $('#sessionLifeTime').helptext({value: '1800'});
                $('#skin').helptext({value: 'bts'});
                $('#serverDBName').helptext({value: 'localhost'});
                $('#serverPort').helptext({value: '3306'});
                
                $.mask.rules.r = /[0-9A-Za-z]/;
                $.mask.masks.opt_key = {mask: 'r', type:'repeat'};
                $.mask.masks.opt_int = {mask: '9', type:'repeat'};
                $('#serverPort,#sessionLifeTime,#skin,#dbName').setMask();
                
                $('#frmInstall').dialog({
                    title:title,
                    autoOpen:true,
                    width:900,
                    draggable:false,
                    show:'puff',
                    buttons:[{
                            id:"btn-accept",
                            text: btnInstall,
                            click: function() {
                                if($('#frmInstaller').valid()){
                                    $("#frmRootDialog").dialog('open');
                                }
                            }
                        }]
                });
                $('#frmRootDialog').dialog({
                    title:titleRoot,
                    buttons:[{
                            id:"btn-accept",
                            text: btnInstall,
                            click: function() {
                                resetSteps();
                                successStep();
                                createStep(dbCreateMsg);
                                $("#frmInstaller").submit();
                            }
                        }]
                });
                function resetSteps(){
                    $('li', '#installSteps').remove();
                    $('#installSteps').append(step_template.replace('[MSG]',stepOne, 'g'));
                }
                function createStep(msg){
                    $('#installSteps').append(step_template.replace('[MSG]',msg, 'g'));
                }
                function successStep(){
                    $('#installSteps li:last-child div').removeClass('waiting').addClass('success');
                }
                function failStep(){
                    $('#installSteps li:last-child div').removeClass('waiting').addClass('fail');
                    $('#frmInstaller input').removeAttr('readOnly');
                }
                $('#frmInstaller').validate({
                    rules: {
                        siteName: {required: true},
                        website: {required: true},
                        serverDBName: {required: true},
                        dbName: {required: true},
                        dbUser: {required: true},
                        dbPassword: {required: true}
                    },
                    messages:{
                        siteName: {required: siteName_req},
                        website: {required: website_req},
                        serverDBName: {required: serverDBName_req},
                        dbName: {required: dbName_req},
                        dbUser: {required: dbUser_req},
                        dbPassword: {required: dbPassword_req}
                    },
                    errorContainer:'#errorMessages',
                    errorLabelContainer: "#errorMessages #messageField",
                    wrapper: "li",
                    highlight: function(element, errorClass) {
                        $(element).addClass('ui-state-error');
                    },
                    unhighlight:function(element, errorClass, validClass){
                        $(element).removeClass('ui-state-error');
                    },
                    submitHandler:function(form){
                        var data = $(form).serialize()+"&"+$('#frmRoot').serialize();
                        $('#frmInstaller input').attr('readOnly',true);
                        $('#frmRootDialog').dialog('close');
                        
                        $.ajax({
                            type: "POST",
                            url: '/public/install/installBTS',
                            data:data,
                            dataType:'json',
                            success: function(response){
                                if(response.code==code_success){
                                    successStep();
                                    createStep(tblCreateMsg);
                                    $.ajax({
                                        type: "POST",
                                        url: '/public/install/installDataBase',
                                        data:data,
                                        dataType:'json',
                                        success:function(){
                                            if(response.code==code_success){
                                                successStep();
                                                jAlert(installSuccess,response.code);
                                                location.reload();
                                            }else{
                                                failStep();
                                                jAlert(response.msg,response.code);
                                            }
                                        }
                                    });
                                } else{
                                    failStep();
                                    jAlert(response.msg,response.code);
                                }
                            }
                        });
                    }
                });
            });
        </script>
        <style type="text/css">
            body{
                font-size: 14px;
            }
            .clear{clear: both;}
            ul li{
                list-style-image: url(/media/ico/ico_alert.png);
            }
            .label_output > div > label{
                min-width: 250px;
                max-width: 250px;
                line-height: 40px;
            }
            input{
                height: 40px;
            }
            ol{
                font-weight: bolder;
            }
            ol li{
                min-height: 32px;
            }
            .ico_event{
                background-repeat: no-repeat;
                width: 16px;
                height: 16px;
                float: left;
                margin-right: 4px;
            }

            .waiting{
                background-image: url(/media/images/bts/waiting.gif);
            }
            .success{
                background-image: url(/media/images/bts/ico_success.png);
            }
            .fail{
                background-image: url(/media/images/bts/ico_fail.png);
            }
            .info{
                display: block;
                padding: 3px;
                font-size: 0.7em;
                margin-bottom: 8px;
            }
            .helptext{
                color: gray;
            }
            #tiptip_holder{
                 font-family: DejaVuSans,Lucida Grande,Lucida Sans,Arial,sans-serif;
                 font-size: 12px;
            }
        </style>
    </head>
    <body>
        <div id="frmInstall" class="dialog-modal">
            <form id="frmInstaller" method="post">
                <div>
                    <div id="installInfo" class="label_output" style="float: left;">
                        <div>
                            <label><?php echo __("Nombre del sitio web"); ?></label>
                            <input id="siteName" name="siteName" type="text" size="35" title="<?php echo __("Por ejemplo: Google el buscador más usado");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("URL del Website"); ?></label>
                            <input id="website" name="website" type="text" size="35" title="<?php echo __("Por ejemplo: www.google.com");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Tema"); ?></label>
                            <input id="skin" name="skin" type="text" size="35" alt="opt_key" title="<?php echo __("Es el nombre de la carpeta donde se encuentran los estilos personalizados");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Zona Horaria"); ?></label>
                            <input id="timeZone" name="timeZone" type="text" size="35" title="<?php echo __("Define la hora para los usuarios de este sistema con respecto a la hora del servidor");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Duración de Sesión"); ?></label>
                            <input id="sessionLifeTime" name="sessionLifeTime" type="text" size="35" alt="opt_int" title="<?php echo __("Es el tiempo en segundos que dura la sesión de un usuario");?>"/>
                        </div>

                        <div>
                            <label><?php echo __("Nombre del servidor o IP"); ?></label>
                            <input id="serverDBName" name="serverDBName" type="text" size="35" title="<?php echo __("Por ejemplo 127.0.0.1 ó localhost");?>" />
                        </div>
                        <div>
                            <label><?php echo __("Puerto del Servidor"); ?></label>
                            <input id="serverPort" name="serverPort" type="text" size="35" alt="opt_int" title="<?php echo __("Es el número de puerto utilizado por mysql");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Nombre de la Base de Datos"); ?></label>
                            <input id="dbName" name="dbName" type="text" size="35" alt="opt_key" title="<?php echo __("Es el nombre de la nueva base de datos");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Usuario de la Base de Datos"); ?></label>
                            <input id="dbUser" name="dbUser" type="text" size="35" title="<?php echo __("Es el usuario que se creará para accesar a la nueva base de datos");?>"/>
                        </div>
                        <div>
                            <label><?php echo __("Password Usuario"); ?></label>
                            <input id="dbPassword" name="dbPassword" type="password" size="35" title="<?php echo __("Es la contraseña del nuevo usuario");?>"/>
                        </div>
                    </div>
                    <div id="installEvents" style="float: left; width: 315px;">
                        <center>
                            <img src="/media/images/bts/mini_logo.png"/>
                        </center>
                        <ol id="installSteps">
                            <li><div class="ico_event waiting" title="<?php echo __("Esperando..."); ?>"></div><p><?php echo __('Información de instalación'); ?></p></li>
                        </ol>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="errorMessages" class="ui-state-error ui-corner-all ui-helper-hidden">
                    <ul id="messageField"></ul>
                </div>
            </form>
        </div>
        <div id="frmRootDialog" class="dialog-modal">
            <form id="frmRoot" method="post">
                <div class="label_output">
                    <div>
                        <label style="text-align: left;"><?php echo __("Contraseña root"); ?></label>
                        <span class="info ui-corner-all ui-state-default"><?php echo __("Debe ingresar la contraseña del usuario root de Mysql para instalar el sistema BTS"); ?></span>
                        <input id="rp" name="rp" type="password" size="32"/>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
