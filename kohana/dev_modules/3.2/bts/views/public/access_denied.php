<script type="text/javascript">
    function timedRefresh(timeoutPeriod) {
        setTimeout("history.back();",timeoutPeriod);
    }
    $(document).ready(function(){
        var title='<?php echo __("Acceso Denegado"); ?>';
        $('#dialog').dialog({
            autoOpen:true,
            closeOnEscape:false,
            modal:true,
            title:title,
            resizable:false,
            close:function(){
                return false;
            },
            buttons:[
                {
                    id:'btnBack',
                    text:'<? echo __('Atrás'); ?>',
                    click:function(){
                        back();
                    }
                }
            ]
        });
    });
</script>
<style type="text/css">
    body{
        font-size: 12px;
    }
    #dialog{
        text-align: justify;
    }
    #dialog div label{
        width: 150px;
        text-align: right;
        display: inline-block;
        float: left;
        margin-right: 4px;
        font-weight: bold;
    }
    #dialog div{
        margin-bottom: 5px;

    }
</style>
<body onload="JavaScript:timedRefresh(5000);"  class="ui-state-highlight" style="border: none;">
    <div id="dialog">
        <?php echo __('Ud. no tiene privilegios para acceder a esta opción. Será redirigido a la pagina anterior en 5 segundos.') ?>    
    </div>
</body>
