<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8" />
        <title><?php echo $title; ?></title>
        <meta name="keywords" content="<?php echo $meta_keywords; ?>" />
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta name="copyright" content="<?php echo $meta_copyright; ?>" />
        <script type="text/javascript" src="/media/js/ibusplus.repo.code.js"></script>
        <link rel="shortcut icon" href="/media/images/<?php echo $skin; ?>/favicon.png"/>
        <?php
        foreach ($styles as $file) {
            echo HTML::style($file['mediaFile_fileName'], NULL, TRUE), "\n" . "\t";
        } ?>
        <?php
        foreach ($scripts as $file) {
            echo HTML::script($file['mediaFile_fileName'], array('language' => 'javascript')), "\n" . "\t";
        } ?>

    </head>
    <body>
        <?php echo $content;?>
    </body>
</html>



