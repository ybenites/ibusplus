<script type="text/javascript">
    var home = '/';
    var error_msg = '<?php echo __("Ha Ocurrido un error al intentar acceder al sistema"); ?>';
    var complete_msg = '<?php echo __("Redireccionando..."); ?>';
    var submit_msg = '<?php echo __("Debe ingresar el nombre de usuario y la contraseña"); ?>';
    var recovery_title = '<?php echo __("Recuperar Contraseña"); ?>';
    var recovery_msg = '<?php echo __("El nombre de usuario es requerido"); ?>';
    var url = '/private/authentication/login'+jquery_params;
    function showLogin(){
        $( "#login_container" ).show('fold', null, 500, null );
    }
    function hideLogin(){
        $( "#login_container" ).hide('fold', null, 500, null );
    }
    function showLoading(){
        $( ".overlay" ).show('slide', null, 500, null );
    }
    function hideLoading(){
        $( ".overlay" ).hide('slide', null, 500, null );
    }
    function centerLogin(){
        $('#login_container').css({
            position:'absolute',
            left: ($(window).width() - $('#login_container').outerWidth())/2,
            top: ($(window).height() - $('#login_container').outerHeight())/2
        });
					
        $('#login_loading').css({
            position:'absolute',
            left: ($(window).width() - $('#login_loading').outerWidth())/2,
            top: ($(window).height() - $('#login_loading').outerHeight())/2
        });
    }
			
    $(document).ready(function(){
        centerLogin();
        $(window).resize(function(){
            centerLogin();
        });
				
        $('input[type=text],input[type=password],textarea,select').addClass('ui-widget ui-widget-content ui-corner-all');
        $('input[type=text],input[type=password],textarea,select').focus(function(){
            $(this).addClass('ui-state-highlight');
        }).blur(function(){
            $(this).removeClass('ui-state-highlight');
        });
        $("#btnLogin").button({
            icons: {
                primary: "ui-icon-locked"
            }
        }).click(function(event){
            event.preventDefault();
            $('#error_msg').hide();
            var uname = $("#username").val();
            var upass = $("#password").val();
            var valid =1;
            if($.trim(uname)==''){
                $("#username").focus();
                valid++;
            }
            if($.trim(upass)==''){
                if(valid==1){
                    $("#password").focus();
                }
                valid++;
            }
            if(valid>1){
                $('#error_msg').find('#text-error').text(submit_msg);
                $('#error_msg').show();
                return false
            }
            $.ajax({
                type: "POST",
                url: url,
                data:{uname:uname,upass:upass},
                dataType:'json',
                beforeSend : function(){
                    hideLogin();
                    showLoading();
                },
                error: function(jqXHR, textStatus, errorThrown){            
                    $('#error_msg').find('#text-error').text(error_msg);
                    hideLoading();
                    showLogin();
                    $('#error_msg').show();
                },
                success: function(response){
                    if(response.code==code_success){
                        $('#login_loading').find('p').text(complete_msg);
                        window.location=response.data;
                    } else{
                        $('#error_msg').find('#text-error').text(response.msg);
                        hideLoading();
                        showLogin();
                        $('#error_msg').show();
                    }
                }
            });
        });
				
        $('#home_link,#language_link').hover(function(){
            $(this).addClass('ui-widget-header');
            $(this).removeClass('ui-widget-shadow');
            $(this).find('div').addClass('ui-state-content');
        },function(){
            $(this).addClass('ui-widget-shadow');
            $(this).removeClass('ui-widget-header');
            $(this).find('div').removeClass('ui-state-content');
        });
        $('#home_link').click(function(){
            window.location = home;
        });

        $('#language_link').toggle(function() {
            $("#util-regions").remove().prependTo("#placeholder-regions").slideToggle('swing');
        }, function() {
            $("#util-regions").slideToggle('swing');
        });
        $('#dialogPassRecovery').dialog({autoOpen:false,width:350,height:'auto',hide:'scale',show:'scale',modal:true,resizable:false,title:recovery_title});
        
        $('#login_remember').click(function(){
            $('#uname').val('');
            $('#pr_error_msg').hide();
            $('#pr_info_msg').hide();
            $('#dialogPassRecovery').dialog('open');
            $('#uname').focus();
        });
        $('#frmPassRecovery').submit(function(event){
            event.preventDefault();
            $('#pr_error_msg').hide();
            $('#pr_info_msg').hide();
            if($.trim($('#uname').val())==''){
                $('#pr-text-error').text(recovery_msg);
                $('#pr_error_msg').show();
            }
            $.ajax({
                type:'POST',
                dataType:'json',
                url:'/public/login/recoveryPass'+jquery_params,
                data:{uname:$('#uname').val()},
                beforeSend:function(content){
                    $('#uname').hide();
                    $('#mini_loader').show();
                },
                complete:function(){
                    $('#mini_loader').hide();
                    $('#uname').show();
                },
                success:function(response){
                    if(response.code==code_success){
                        $('#pr-text-info').text(response.msg);
                        $('#pr_info_msg').show();
                        $('#uname').val('');
                    } else {
                        $('#pr-text-error').text(response.msg);
                        $('#pr_error_msg').show();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown){            
                    $('#pr_info_msg').hide();
                    $('#pr_error_msg').find('#text-error').text(error_msg);
                    $('#pr_error_msg').show();
                }
            });
        });
        $('#username').focus();
    });
</script>
<style type="text/css">
    .ui-widget,.ui-accordion .ui-accordion-header a,.ui-datepicker .ui-datepicker-title select,.ui-widget .ui-widget{
        font-size: 1em;
    }
    #username,#password{
        width: 490px;
    }
    #frmLogin{
        width:490px;
        margin: 0 auto;
        margin-bottom:10px;
    }
    #frmLogin label{
        display:block;
        padding:8px;
        background:none;
        border:none;
    }
    #login_container{
        border-radius:7px;
        padding:15px;
        width:509px;
    }
    #logo_company{
        padding-top:20px;
        width:350px;
        height:80px;
        margin:0 auto;
        tex-align:center;
    }
    input[type=text],input[type=password],textarea,select{
        padding:5px;
    }
    .right{
        text-align:right;
    }
    .ui-state-highlight{
        border:none;
    }
    #login_remember{
        padding-right:50px;
        font-size:0.7em;
        cursor:pointer;
        background:none;
        border:none;
    }
    #login_remember_icon{
        display:inline-block;
    }
    .boder_padding_none{
        margin:0px;
        padding:0px;
    }
    #home_link,#language_link{
        cursor:pointer;
    }
    #home_link:hover{
        border-left:none;
        padding-left:0px;
        padding-top:4px;
        padding-right:4px;
        padding-bottom:4px;
    }
    #language_link:hover{
        border-right:none;
        padding-righ:0px;
        padding-top:4px;
        padding-left:4px;
        padding-bottom:4px;
    }
    #language_link{
        float: right;
        font-weight: bold;
        border-top-right-radius:0px;
        border-bottom-right-radius:0px;
    }
    .language_link_change{
        cursor: pointer;
    }
    #text-error,#pr-text-error,#pr-text-info{
        padding-left: 5px;
    }
    .hide{
        display:none;
    }
    .private_zone_bg{
        position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: -1;
    }

    #util-int {
        z-index: 1000;
    }
    #util-regions {
        display: none; /* hidden by default, will be shown by jQuery */
        text-align: center;
        width: 440px;
        margin: 0 auto;
        height: 90px;
        position: relative;
    }
    ul li{
        float: left;
        margin-right: 50px;
        list-style: none;
    }
    .user_msg{
        font-size:0.6em; padding-left:5px;padding-right:5px; margin: 5px;
    }
    .user_msg p{
        text-align: justify;
    }
    .user_msg p .ui-icon{
        float: left; margin-right: .3em;
    }
    .icon_fix{
        display: block;float: left;min-height: 40px;
    }

</style>	
<body class="boder_padding_none ui-state-highlight" onload="showLogin();">
    <!--LANGUAGE-->
    <div id="placeholder-regions" class="ui-widget-header ui-corner-bottom"></div>
    <!--LANGUAGE-->
    <!--LOGIN-->
    <div>
        <div id="home_link" class="ui-widget-shadow ui-corner-right" style="margin-top:40px;display:inline-block;" title="[<?php echo $siteName; ?>]">
            <div class="ui-widget ui-widget-content ui-corner-right" style="display:inline-block; padding:10px;">
                    <img src="/media/images/<?php echo $skin; ?>/mini_logo.png" alt="[<?php echo $siteName; ?>]"/>
            </div>
        </div>
        <div style="float: left;"></div>
        <div id="language_link" class="ui-widget-shadow ui-corner-left" style="margin-top:40px;display:inline-block; padding-right: 0px;" title="[<?php echo $siteName; ?>]">
            <div class="ui-widget ui-widget-content ui-corner-left" style="display:inline-block; padding:10px;">
                <img src="/media/images/internationalization.png" alt="[<?php echo __("Seleccione su idioma"); ?>]" style="float:left;"/>
                <p style="display: inline-block; padding-left: 8px;"><?php echo __("Seleccione su idioma"); ?></p>
            </div>
            <div id="util-regions">
                <ul>
                    <li>
                        <div id="idioma_en" class="language_link_change ui-widget ui-widget-content ui-corner-all" style="padding:8px; padding-left: 15px; padding-right: 15px;">
                            <img src="/media/ico/en.png" alt="[<?php echo __("Inglés"); ?>]" title="[<?php echo __("Inglés"); ?>]" style="float: left; padding-top: 12px;"/>
                            <p style="display: inline-block; padding-left: 8px;"><?php echo __("Inglés"); ?></p>
                        </div>
                    </li>
                    <li>
                        <div id="idioma_es" class="language_link_change ui-widget ui-widget-content ui-corner-all" style="padding:8px; padding-left: 15px; padding-right: 15px;">
                            <img src="/media/ico/es.png" alt="[<?php echo __("Español"); ?>]" title="[<?php echo __("Español"); ?>]" style="float: left; padding-top: 12px;"/>
                            <p style="display: inline-block; padding-left: 8px;"><?php echo __("Español"); ?></p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="login_container" class="ui-widget ui-widget-header ui-corner-all">
        <div style="width:500px; min-height:350px; border-width:2px;" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
            <div id="logo_company">
                <center>
                    <img src="/media/images/<?php echo $skin; ?>/logo.png" alt="[<?php echo $siteName; ?>]"/>
                </center>
            </div>
            <form id="frmLogin" method="post" autocomplete="off">
                <label for="username" class="ui-state-default"><?php echo __("Usuario"); ?></label>
                <input id="username" name="username" value="" title="<?php echo __("Usuario"); ?>"  tabindex="4" type="text"/>
                <label for="password" class="ui-state-default"><?php echo __("Contraseña"); ?></label>
                <input id="password" name="password" value="" title="Contraseña" tabindex="5" type="password"/>
                <label></label>
                <div class="right">
                    <a id="login_remember" class="ui-state-default"><?php echo __("¿Ha olvidado su contraseña?"); ?><span id="login_remember_icon" class="ui-button-icon-primary ui-icon ui-icon-info"></span></a>
                    <button id="btnLogin"><?php echo __("Iniciar Sesión"); ?></button>
                </div>
            </form>
            <div id="error_msg" class="hide ui-state-error ui-corner-all user_msg">
                <p><span class="ui-icon ui-icon-alert"></span><strong><?php echo __("Alerta"); ?>:</strong><span id="text-error"></span></p>
            </div>
        </div>
    </div>
    <div class="overlay hide ui-widget-overlay">
    </div>
    <div id="login_loading" class="overlay hide ui-widget ui-widget-content ui-corner-all" style="padding: 5px; margin-top: 10px; position:absolute; width:460px;">
        <div class="ui-corner-all ui-widget-header" style="padding: 5px; font-size:0.8em;">
            <?php echo __("Procesando su petición"); ?>
        </div>
        <img src="/media/images/<?php echo $skin; ?>/loading.gif" alt="[<?php echo __("Cargando"); ?>]" style="display:block; float:left; padding-left:10px; padding-right:10px;"/>
        <p style="margin-top:30px; text-align: center;"><?php echo __("Cargando, por favor espere un momento..."); ?></p>
    </div>

    <!--LOGIN-->
    <!--RECOVERY PASS-->
    <div id="dialogPassRecovery">
        <form id="frmPassRecovery" method="post" class="frm" autocomplete="off">
            <fieldset class="ui-corner-all ui-widget-content">
                <legend class="ui-corner-all ui-widget-header" style="font-size: 0.8em; padding: 4px;"><?php echo __("Usuario"); ?></legend>
                <input id="uname" name="uname"  title="<?php echo __("Usuario"); ?>"  tabindex="4" type="text" style="width: 100%;"/>
                <div id="mini_loader" class="hide" style="text-align: center;">
                    <div><?php echo __("Procesando su petición"); ?></div>
                    <img src="/media/images/<?php echo $skin; ?>/mini_loading.gif" alt="[<?php echo __("Cargando"); ?>]" style="clear: both;"/>
                </div>
                <p style="font-size: 0.6em; color:gray; text-align: justify;"><?php echo __("Ingrese su nombre de usuario y luego presione ENTER, un correo será enviado a la dirección de correo electrónico con la que se registro su usuario"); ?>.</p>
            </fieldset>
            <div id="pr_error_msg" class="hide ui-state-error ui-corner-all user_msg">
                <p><span class="icon_fix"><span class="ui-icon ui-icon-alert"></span></span><strong><?php echo __("Alerta"); ?>:</strong><span id="pr-text-error"></span></p>
            </div>
            <div id="pr_info_msg" class="hide ui-state-highlight ui-corner-all user_msg"> 
                <p><span class="icon_fix"><span class="ui-icon ui-icon-info"></span></span><strong><?php echo __("Exitoso"); ?>:</strong><span id="pr-text-info"></span></p>
            </div>
        </form>
    </div>
    <!--RECOVERY PASS-->
</body>