
$(document).ready(function(){
    
    /**
     * Configuraciones jQuery
     **/
    
    $.ajaxSetup({
        type:'POST',
        dataType:'json',
        beforeSend : function(){
            if (Boolean(eval(showLoading))) showLoader();
        },
        complete: function(){                
            if (Boolean(eval(showLoading))) hideLoader();
            showLoading=0;
        },
        error: function(jqXHR, textStatus, errorThrown){
            msgBox(errorMessage);
        }
    });  
     jQuery.validator.setDefaults({      
        errorElement: "li"
    });
    jQuery.extend(jQuery.validator.messages, {
        required: fieldRequired,
        remote: 'fieldRequired'
    });
    
    /**
     * Serializa un formulario a un objeto javascript
     * cada elemento debe tener definido el atributo name
     */
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    
    /*
    * Elimina un elemento de un arreglo por su valor
    **/
    $.removeFromArray = function(value, arr) {
        return $.grep(arr, function(elem, index) {
            return elem !== value;
        });
    };
    
    /**
     * Centra una elemento html en la pantalla
     */
    $.fn.center = function () {
        this.css("position","absolute");
        this.css("top", (($(window).height() - this.outerHeight()) / 2) + 
            $(window).scrollTop() + "px");
        this.css("left", (($(window).width() - this.outerWidth()) / 2) + 
            $(window).scrollLeft() + "px");
        return this;
    }
    /*
    $.fn.removeFromArray = function(value_idx, arr,type) {
        return $.grep(arr, function(elem, index) {
            if(typeof type =='undefined')
                return elem !== value_idx;
            else
                return index == value_idx;
        });
    };
    */
    function showLoader(){
        var loader = $(document.createElement('div')).attr("id","loading").attr("style","z-index:9999;")
        .addClass('ui-corner-all ui-widget-content')
        .append($(document.createElement('p')).attr("style","line-height: 60px;").attr("align","center").text(txtLoading).append($(document.createElement('img')).attr("src",imgLoading).attr("style","float: right;")));
        loader.appendTo($("body"));
        loader.center();
        $(document.createElement('div')).attr("id","loading_overlay").addClass("ui-widget-overlay").appendTo($("body")).attr("style","z-index:9998;");
    }
    function hideLoader(){
        $("#loading,#loading_overlay").remove();
    }
    
});
