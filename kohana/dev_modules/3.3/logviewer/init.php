<?php defined('SYSPATH') or die('No direct script access.');

Route::set('logdelete', 'logs/delete/<year>/<month>/<logfile>', array('logfile' => '.*'))
	->defaults(array(
		'controller' => 'Logs',
		'action'     => 'delete',
	));
Route::set('logviewer', 'logs(/<year>(/<month>(/<day>(/<level>))))')
	->defaults(array(
		'controller' => 'Logs',
		'action'     => 'index'
                ,'year' => date('Y')
                ,'month' => date('m')
                ,'day' => date('d')
	));