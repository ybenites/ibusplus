<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Log del Sistema</title>
        <meta name="description" content="A Kohana module for exploring log files">
        <meta name="Author" content="WNeeds - http://wneeds.com" />        
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION; ?>.min.css">    
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION; ?>.min.css">              
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_VERSION ?>.min.js"></script>
        <style>
            body{margin-bottom: 42px;}
            .span12 table{
                width: auto !important;
                margin-right: 20px;
                margin-bottom: 10px;
            }
            .pagination{
                margin: 10px 0;
            }

            .topbar .btn {
                border: 0;
            }
            .topbar h1{
                color: #1e90ff;
                padding-top: 10px;
            }

            .nav a {font-weight: bold; color: #708090;}
            .nav a:hover{color: #0d3349;}
            .nav .active a{
                color: white;
                text-shadow: 0 0 4px #1E90FF;
            }

            .utility { color: #708090;}
            .clear {clear: both;}


            td span.action {
                position: absolute;
                left: 325px;
                width: 54px;
                padding: 2px;
                background-color: #DDD;
                margin-top: -28px;
                display: block;
            }

            #category-list {
                height: 405px;
                overflow: auto;
            }

            .navbar_shortcuts{top: 41px;z-index: 100;}
            .nav_menu{margin-bottom: 0;}

            /* Sidebar day list */
            .pills a {
                background-color: #eeeeee;
                padding: 0 50px;
                margin-left: 20px;
                text-align: center;
                border-radius: 5px;
            }
            .pills .active a,
            .pills a:hover{
                background-color: #666;
                border-top-right-radius: 20px;
                border-top-left-radius: 10px;
                border-bottom-right-radius: 20px;
                border-bottom-left-radius: 10px;
                padding-right: 60px;
            }

            .padding_top_body {
                margin-top: 90px;
            }

            #footer {
                height: 40px;
                bottom: 0;
                position: fixed;
                width: 100%;
                border-bottom: none;
                -moz-border-radius: 0 4px 0 0; 
                border-radius: 0 4px 0 0;
                line-height: 2em;
                background-color: #f5f5f5;
            }
            .container em.credit {
                margin:7px 0;
            }
            .container strong.credit {
                margin:7px 0;
            }
            .label {
                background-color: #BFBFBF;
                border-radius: 3px 3px 3px 3px;
                color: #FFFFFF;
                font-weight: bold;
                padding: 4px;
                text-transform: uppercase;
                white-space: nowrap;
            }
            .label.important {
                background-color: #C43C35;
            }
            .label.warning {
                background-color: #F89406;
            }
            .label.success {
                background-color: #46A546;
            }
            .label.notice {
                background-color: #62CFFC;
            }
            ol{
                color: grey;    
            }
        </style>    
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top nav_menu">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#"><?php echo $SYSTEM_TITLE; ?></a>
                    <div class="nav-collapse">        
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-fixed-top navbar_shortcuts">
            <div class="navbar-inner">
                <div class="container">
                    <?php include(Kohana::find_file('views/logs', 'monthlist')); ?>
                </div>
            </div>
        </div>
        <div class="padding_top_body" style=""></div>
        <div class="container">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2 well">
                        <?php include(Kohana::find_file('views/logs', 'daylist')); ?>
                    </div>
                    <div class="span10">
                        <?php if (isset($content)) echo $content; ?>
                    </div>
                </div>
            </div>          
        </div> <!-- /container -->
        <div id="footer">
            <div class="container">
                <strong class="text-right muted credit" style="float: right;"><?php echo __('IBUSPLUS &reg; Todos los derechos Reservados. &copy; 2013'); ?></strong>
            </div>
        </div>
    </body>
</html>