<style type="text/css">
    .format div label{
        width: 205px;
        text-align: right;
        display: inline-block;
        float: left;
        margin-top: 4px;
        margin-right: 4px;
        font-weight: bold;
    }
    .format div p{
        padding-top: 4px;
    }
    .format div{
        margin-bottom: 5px;
    }
    .msg{
        font-size: 0.9em;
        text-align: justify;
        padding: 5px;
    }
</style>
<p class="msg">
    <?php echo __("Alguien esta intentando ingresar muchas veces al sistema de modo inapropiada.");?>
</p>
<div class="format">
    <div>
        <b><label><?php echo __("Usuario"); ?>:</label></b>
        <p>:userName</p>
    </div>
    <div>
        <b><label><?php echo __("Contraseña"); ?>:</label></b>
        <p>:password</p>
    </div>
    <div>
        <b><label><?php echo __("Ip"); ?>:</label></b>
        <p>:ip</p>
    </div>
    <div>
        <b><label><?php echo __("Sistema"); ?>:</label></b>
        <p>:system</p>
    </div>
</div>
