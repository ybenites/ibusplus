<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            *{
                margin: 0px;
                padding: 0px;
                font-family: 'Arial';
            }
        </style>
    </head>
    <body>
        <table style="
               width: 500px; 
               margin: 0 auto; 
               border-radius: 4px; 
               border: khaki solid 2px;
               background: #fff0a0; /* Old browsers */
               background: -moz-linear-gradient(top,  #fff0a0 0%, #fefcea 100%); /* FF3.6+ */
               background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fff0a0), color-stop(100%,#fefcea)); /* Chrome,Safari4+ */
               background: -webkit-linear-gradient(top,  #fff0a0 0%,#fefcea 100%); /* Chrome10+,Safari5.1+ */
               background: -o-linear-gradient(top,  #fff0a0 0%,#fefcea 100%); /* Opera 11.10+ */
               background: -ms-linear-gradient(top,  #fff0a0 0%,#fefcea 100%); /* IE10+ */
               background: linear-gradient(to bottom,  #fff0a0 0%,#fefcea 100%); /* W3C */
               filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff0a0', endColorstr='#fefcea',GradientType=0 ); /* IE6-9 */

               ">
            <tr>
                <td colspan="2" style="text-align: center; padding: 4px;">
                    <h2>Support Email from :CLIENT_DOMAIN</h2>
                    <hr style=" color:  red; border-style: solid;"/>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; text-align: right;">
                    Code Error
                </td>
                <td style="padding-left: 5px;">
                    <h2>:CODE_ERROR</h2>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; text-align: right;">
                    Date
                </td>
                <td style="padding-left: 5px;">
                    <h2>:DATE_ERROR</h2>
                </td>
            </tr>
            <tr>
                <td style="width: 150px; text-align: right;">
                    User
                </td>
                <td style="padding-left: 5px;">
                    <h2>:USER_ERROR</h2>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="width: 150px; text-align: right;">
                    <a href="http://:CLIENT_DOMAIN/logs">Go to logs</a>
                </td>
            </tr>
        </table>
    </body>
</html>
