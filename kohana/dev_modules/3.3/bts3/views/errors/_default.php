<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta />
        <title><?php echo __("Ops =)")?></title>
        <style>
            ::-moz-selection {
                background: #b3d4fc;
                text-shadow: none;
            }

            ::selection {
                background: #b3d4fc;
                text-shadow: none;
            }

            html {
                padding: 30px 10px;
                font-size: 20px;
                line-height: 1.4;
                color: #737373;
                background: #f0f0f0;
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
            }

            html,
            input {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            }

            body {
                max-width: 550px;
                _width: 500px;
                padding: 30px 20px 40px 50px;
                border: 1px solid #b3b3b3;
                border-radius: 4px;
                margin: 0 auto;
                box-shadow: 0 1px 10px #a7a7a7, inset 0 1px 0 #fff;
                background: #fcfcfc;
            }

            h1 {
                margin: 0 10px;
                font-size: 50px;
                text-align: center;
            }

            h1 span {
                color: #bbb;
            }

            h3 {
                margin: 1.5em 0 0.5em;
            }

            p {
                margin: 1em 0;
            }

            ul {
                padding: 0 0 0 40px;
                margin: 1em 0;
            }

            .container {
                _width: 380px;
                margin: 0 auto;
            }

            input::-moz-focus-inner {
                padding: 0;
                border: 0;
            }
        </style>
    </head>
    <body onload="" class="ui-state-highlight" style="border: none;">
        <div class="container">           
            <h2><?php echo __("Ops, Se Produjo un Error Inesperado") ?></h2>
            <i>
             <?php
            switch ((int) $code) {
                case 404:
                    echo __("Esta dirección no se encuentra en el servidor.");
                    echo "</br>  " . __('Por favor, vuelva a ingresar la dirección.');
                    break;
                case 441:
                    echo __($message);
                    echo "</br> " . __('Por favor, vuelva a generar la petición.');
                    break;
                case 0:                    
                    echo __("El servidor ha recibido información incorrecta.");
                    echo "</br>  " . __('Comuniquese con el área de Soporte para poder solucionar el inconveniente.');
                    break;
                default:
                    echo __("El servidor ha recibido información incorrecta.");
                    echo "</br>  " . __('En breve será redirigido a la pagina anterior.');
                    break;
            }            
            ?>
                </i>
        </div>
    </body>
</html>