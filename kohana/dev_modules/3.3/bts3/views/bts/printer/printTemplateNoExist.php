
<div id="modal_message" class="modal hide" data-backdrop="static">
    <div class="modal-body">
        <p style="text-align: justify;">
            <?php echo $ERROR_MESSAGE; ?>
        </p>
        <div>
            <label><?php echo __("Sistema Operativo"); ?>:</label>
            <div id="user_terminal_os"><?php echo $os; ?></div>
        </div>
        <div>
            <label><?php echo __("Navegador Web"); ?>:</label>
            <div id="user_browser"><?php echo $browser; ?></div>
        </div>
        <div>
            <label><?php echo __("Impresora"); ?>:</label>
            <div id="user_printer"><?php echo $printer; ?></div>
        </div>
        <div>
            <label><?php echo __("Documento"); ?>:</label>
            <div id="user_printer"><?php echo $document; ?></div>
        </div>
        
    </div>
</div>
