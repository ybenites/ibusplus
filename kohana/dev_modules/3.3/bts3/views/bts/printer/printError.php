<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title_dialog; ?></title>
        <link rel="shortcut icon" href="/media/images/<?php echo $SYSTEM_SKIN; ?>/favicon.png"/>
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/ui/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION;?>.min.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION ?>.min.css">    
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION?>.min.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/main.css">
        <link rel="stylesheet" href="/css/main.<?php echo $SYSTEM_SKIN ?>.css">
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_VERSION ?>.min.js"></script>
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION ?>.min.js"></script>
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION ?>.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#modal_message').modal('show');
            });
        </script>
        <style type="text/css">
            #dialog div label{
                width: 150px;
                text-align: right;
                display: inline-block;
                float: left;
                margin-right: 4px;
                font-weight: bold;
            }
            #dialog div{
                margin-bottom: 5px;

            }
        </style>
    </head>
    <body>
        <div id="dialog">
            <?php echo $content_dialog; ?>    
        </div>
    </body>
</html>