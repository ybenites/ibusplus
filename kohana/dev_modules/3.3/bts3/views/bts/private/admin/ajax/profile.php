<script type="text/javascript" src="/kernel/js/bts/core/select2.min.js"></script>
<script type="text/javascript" src="/kernel/js/bts/core/select2.i18n/select2_locale_es.js"></script>
<script type="text/javascript" src="/kernel/js/bts/core/jquery.fileuploader.min.js"></script>
<script type="text/javascript" src="/kernel/js/bts/core/bootstrap-typeahead.js"></script>
<script type="text/javascript" src="/kernel/js/bts/admin/profile.js"></script>
<link rel="stylesheet" href="/kernel/css/bts/select2/select2.css">
<!--        dialogo de INFOMACION DE USUARIO-->
<div id="modal_user_info" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Informacion del Usuario") ?></h3>
    </div>
    <div class="modal-body">
        <form name="editUser" id="editUser" class="form-horizontal">
            <div class="control-group tumbail_shorcut hide">   
                <ul class="thumbnails">
                    <li class="span2 controls">
                        <a href="#" class="thumbnail">
                            <input type="hidden" id="fmenu_image" name="fmenu_image" /> 
                            <img id ="imag" style="height: 128px;" >
                        </a>
                    </li>                    
                </ul>
            </div>
            <div class="control-group"> 
                <label class="control-label"><b><?php echo __("Nombre") ?>:</b></label>
                <div class="controls">
                    <input class="input-large" type="text" disabled value="<?php echo $BTS_USER_FULL_NAME; ?>"/>
                    <input name="id" id="id" type="hidden" value="<?php echo $BTS_SESSION_USER_ID; ?>"/>
                </div>
            </div>
            <div class="control-group"> 
                <label class="control-label"><b><?php echo __("Usuario") ?>:</b></label>
                <div class="controls">
                    <input name="fuser_username" id="fuser_username" class="input-small" type="text" disabled value="<?php echo $BTS_USER_NAME; ?>"/>
                </div>
            </div>
            <div class="control-group"> 
                <label class="control-label"><b><?php echo __("Grupo"); ?>:</b></label>
                <div class="controls">
                    <input class="input-large" type="text" disabled id="fuser_group" name="fuser_group"/>
                </div>
            </div>
            <div class="control-group"> 
                <label class="control-label"><b><?php echo __("Correo") ?>:</b></label>
                <div class="controls">
                    <input class="input-large" type="text" disabled value="<?php echo $BTS_SESSION_USER_EMAIL; ?>"/>
                </div>
            </div>                    
            <div class="control-group"> 
                <label class="control-label"><b><?php echo __("Oficina") ?>:</b></label>
                <div class="controls">
                    <select id="fuser_office" name="fuser_office">
                        <option value=""><?php echo __('Seleccionar') ?></option>
                    </select>    
                </div>
            </div>
            <div class="control-group div_add_img">
                <div id="btn_img">

                </div>
            </div>


            <?php if ($ROOT_MIMIC_OPTION OR $USER_MIMIC_OPTION) { ?>                        
                <script type="text/javascript">
                    $(document).ready(function() {
                        function changeLogin(id, name) {
                            var params = new Object();
                            if (id != null && name != null) {
                                params.uid = id;
                                params.ufname = name;
                            } else {
                                params.reset = 'r';
                            }
                            showloadingDialog = 1;
                            $.ajax({
                                url: '/private/user/getMimicUser' + jq_jsonp_callback,
                                data: params,
                                asybn: false,
                                success: function(j_response) {
                                    if (j_response.code == jq_json_code_success) {
                                        bootbox.alert(j_response.msg, {
                                            close: function() {
                                                location.href = '/public/login/logout';
                                            }
                                        });
                                    } else {
                                        bootbox.alert(j_response.msg);
                                    }
                                }
                            });
                        }
                        $('#recoveryLogin').click(function() {
                            changeLogin(null, null);
                        });
                        function createAutocompleteUser(url, input_id) {
                            $('#' + input_id).typeahead({
                                minLength: 3,
                                source: function(typeahead, query) {
                                    var term = typeahead.query;
                                    if (requestUsers == 0)
                                    {
                                        cacheUsers = {};
                                    }
                                    if (term in cacheUsers) {
                                        typeahead.process(cacheUsers[ term ]);
                                        return;
                                    }
                                    lastXhrUsers = $.ajax({
                                        url: url,
                                        data: {query: query},
                                        async: true,
                                        success: function(j_response, status, xhr) {
                                            if (j_response.code == jq_json_code_success) {
                                                data = j_response.data;
                                                if (data.length == 0) {
                                                    data.push({
                                                        id: "0",
                                                        value: s_not_result_found,
                                                        label: s_not_result_found
                                                    });
                                                }
                                                cacheUsers[ term ] = data;
                                                if (xhr === lastXhrUsers) {
                                                    typeahead.process(data);
                                                }
                                                requestUsers = requestUsers + 1;
                                            }
                                        }
                                    });
                                },
                                onselect: function(obj) {
                                    changeLogin(obj.id, obj.value);
                                }
                            });

                        }
                        createAutocompleteUser('/private/user/getUsers' + jq_jsonp_callback, 'user_list');
                    });
                </script>
                <fieldset>
                    <legend><?php echo __("Iniciar Sesión Como...") ?></legend>
                    <div class="control-group">                            
                        <label class="control-label"><b><?php echo __("El Usuario"); ?>:</b></label>
                        <div class="controls">
                            <input id="user_list" name="user_list" type="text"/>
                            <button class="btn" id="recoveryLogin" style="margin-left: 5px;"><?php echo __("Recuperar Mi Inicio de Sesión"); ?></button>
                        </div>

                        <div style="margin: 15px; text-align: justify;">
                            <small>
                                <?php echo __("Para iniciar sesión como otro usuario, busque al usuario, selecciónelo e inicie sesión nuevamente. Para recuperar el inicio de sesión normal, busque su usuario y repita el procedimiento. O simplemente haga click en el botón") . " " . __("Recuperar Mi Inicio de Sesión"); ?>
                            </small>
                        </div>
                    </div>
                </fieldset>
            <?php } ?>

        </form>   
    </div>
    <div class="modal-footer">
        <button id="btn_show_change_pass" type="submit" class="btn btn-primary"><?php echo __("Cambiar Password") ?></button>
        <button id="modal_btn_save" type="submit" class="btn btn-primary"><?php echo __("Guardar") ?></button>
    </div>
</div>            
<!--           CHANGE PASSWORD-->
<div id="modal-change-pass" class="modal hide" data-backdrop="static"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Cambiar Password") ?></h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" id="frmChangePass">
            <div class="control-group">
                <label class="control-label"><b><?php echo __("Contraseña Actual:") ?></b></label>
                <div class="controls">  
                    <input id="pass_actual" name="pass_actual" type="text" class="input-large"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><b><?php echo __("Contraseña Nueva:") ?></b></label>
                <div class="controls">  
                    <input id="new_pass" name="new_pass" type="text" class="input-large"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><b><?php echo __("Confirmar contraseña:") ?></b></label>
                <div class="controls">  
                    <input type="text" name="new_pass_confir" id="new_pass_confir" class="input-large"/>
                </div>
            </div>            
        </form>
    </div>
    <div class="modal-footer">
        <button id="bt_change_pass" type="submit" class="btn btn-primary"><?php echo __("Guardar") ?></button>
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>  
    </div>
</div>   