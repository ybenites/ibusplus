<?php //cache por 8 horas
if ( ! Fragment::load('admin_supportIndex', 28800, true))
{  
?>
<style type="text/css">
    div#support div[lang]>p{
        overflow:hidden;
        margin-top:20px;
        padding-left:3%;
    }
    div#support div[lang]>p img{
        float:left;
        width:98px;
        height:98px;
        border: none;
    }
    div#support div[lang]>p .texto{
        float:left;
        width:70%;
        padding-left:14px;
        vertical-align:middle;
        padding-top:25px;
        text-align:justify;
    }
    .supportUser{
        font-weight:bold;
    }
</style>
<br/>
<div id="support" class="ui-widget-content ui-corner-all" style="float: left; padding: 5px; width: 600px;">   
    <div lang="es">
        <div class="ui-widget-header ui-corner-all" style="padding: 5px;">
            <h2><?php echo __("Requerimientos del Sistema"); ?></h2>
        </div>         <br/>
        
         <i>**<?php echo __("Click en cada imagen para descargar"); ?>.</i>

        <p>
            <a href="https://www.google.com/intl/es/chrome/browser/?hl=es" target="__blank" ><img src="http://static.o.bts.mcets-inc.com/kernel/img/support/google-chrome.jpg" /></a>
            <span class="texto">
                <?php echo __("Google Chrome, navegador web por defecto para el uso del sistema."); ?>
            </span>
        </p>
        <p >
            <a href="http://www.mozilla.org/es-ES/firefox/new/" target="__blank" ><img src="http://static.o.bts.mcets-inc.com/kernel/img/support/firefox7.jpg" /></a>
            <span class="texto">
                <?php echo __("Firefox, navegador web alternativo para el uso del sistema."); ?>
            </span>
        </p>
        <p >
            <a href="http://www.foxitsoftware.com/downloads/thanks.php?product=Foxit-Reader&platform=Windows" target="__blank" ><img src="http://static.o.bts.mcets-inc.com/kernel/img/support/foxitreader.png" /></a>
            <span class="texto">
                <?php echo __("Foxit Reader es una programa de distribucion gratuita, que nos permite la correcta visualizacion de documentos en linea. Este componente es requerido para la visualizacion de reportes."); ?>
            </span>
        </p>

        <p>
            <a href="http://www.teamviewer.com/download/TeamViewer_Setup_es.exe" target="__blank"><img src="http://static.o.bts.mcets-inc.com/kernel/img/support/teamviewer.png" /></a>
            <span class="texto">
                <?php echo __("Teamviewer es un programa de distribucion gratuita, que nos permite la asistencia remota. Mediante este programa el equipo de soporte tiene acceso y el control de su PC para ayudarle resolver los problemas. Cuando Ud. requiera asistencia del equipo de soporte, debera propocionar el Id y la Contraseña de acceso que genera el programa"); ?>.
            </span>	
        </p>
    </div>
    </div>

    <div class="ui-widget-content ui-corner-all"  style="float: right; padding: 5px; width: 500px;">
         <div class="ui-widget-header ui-corner-all" style="padding: 5px;">
            <h2><?php echo __("Información de Soporte"); ?></h2>
        </div>         <br/>
        <p style="font-size: 14px;">
            <?php echo __("Nuestros clientes son lo más importante para nosotros, operamos un servicio de asistencia para ayudarle con todas sus necesidades en relación con nuestro sistema."); ?>
        </p>
        <p><br/>
              <b style="font-size: 15px;"> <?php echo __("Nuevo Sistema de Soporte:"); ?> </br>
                  <a href="http://mcets.zendesk.com" target="_blank">mcets.zendesk.com</a> </br> </br>
            <b><i><?php echo __("Envia un email a"); ?>: </i> </br></b>support@mcets.zendesk.com<br/>
            </b>
        </p>
        <p style="font-size: 14px;"> <br/>
            <b style="font-size: 14px;"><?php echo __("Contáctenos"); ?>:</b><br/>
            <b>USA:</b>          <br/>  
            <b><i>(<?php echo __("Linea") ?> 1):&nbsp;</i></b>214-771-8049<br/>
            <b><i>(<?php echo __("Linea") ?> 2):&nbsp;</i></b>214-393-9065<br/>
            <b><i>(<?php echo __("Linea") ?> 3):&nbsp;</i></b>214-856-7004<br/>         
            <br/>
            <b><?php echo __("PERÚ") ?>:</b><br/>
            <b><i>(<?php echo __("PER Fijo") ?>):&nbsp;</i></b> +5144-222293.<br/>
            <b><i>Call Center (PER):&nbsp;</i></b>Movistar: 956420050 // Movistar (RPM): #956420050<br/>
            <b><i>Call Center (PER):&nbsp;</i></b>Movistar: 999033741 // Movistar (RPM) #533590<br/>
        </p>
        <p style="font-size: 14px;">           
            <b><i><?php echo __("Correo Electrónico"); ?>:</i></b> consultas@ibusplus.com<br/>
            <b><i><?php echo __("Soporte"); ?>:</i></b> soporte@ibusplus.com<br/>
            <br/>
         </p>
    </div>
   <?php
  Fragment::save();
}
?>