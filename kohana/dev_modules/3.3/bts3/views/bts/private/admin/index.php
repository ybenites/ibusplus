<script type='text/javascript'>
    var v_user_group_id =<?php echo $user_group_id;  ?>;
    var v_user_group_root =<?php echo $user_group_root; ?>;
</script>
<div class="page-header">
    <h3><?php echo __('Bienvenido Sr(a).') ?> <em class="muted"><?php echo $s_user_full_name; ?></em></h3>
</div>
<?php
if ($s_browser_name == 'Firefox' && $s_browser_version != '22.0') {
    ?>
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Warning!</h4>
        <?php echo __('La version de su Navegador Firefox no esta Actualizada') . ' : '; ?> <br/>
        <?php echo __('La Ultima version es') . ': '; ?>
        <?php echo $s_browser_name; ?>
        <?php echo $s_browser_version; ?>
        <br/>
        <?php echo __('Comuniquese con el area de soporte para poder realizar la actualización') . ' '; ?>   <br/>   <br/>
        <p><small><a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Español') . ' '; ?></a></small></p>
         <p><small><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Ingles') . ' '; ?></a></small></p>
        <p><small> <a href="http://www.google.com/Chrome" target="_blank"><?php echo __('Descargar Google Chrome') . ' '; ?></a></small></p>
    </div>
    <?php
} elseif ($s_browser_name != 'Chrome' && $s_browser_name != 'Firefox') {
    ?>
    <div class="alert alert-block">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <h4>Warning!</h4>
            <h5><?php echo __('El Navegador que esta usando no es compatible con el Sistema. Debe Cambiar a Firefox o Google Chrome. Su Navegador es') . ': '; ?>
                <?php echo $s_browser_name; ?>
                <?php echo $s_browser_version; ?>        
            </h5>
        <br/>
        <?php echo __('Comuniquese con el area de soporte para poder realizar la actualización') . ' '; ?>   <br/>   <br/>
        <p><small><a href="http://www.mozilla.org/es-ES/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Español') . ' '; ?></a></small></p>
         <p><small><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank"><?php echo __('Descargar Firefox Ingles') . ' '; ?></a></small></p>
        <p><small> <a href="http://www.google.com/Chrome" target="_blank"><?php echo __('Descargar Google Chrome') . ' '; ?></a></small></p>        
    </div>
    <?php
}
?>
<?php if ($user_group_id == $user_group_root) { ?>   
<script src="http://maps.google.com/maps/api/js?sensor=false&language=en-US" type="text/javascript"></script>
    <div id="container"></div>
    <div class="onlineWidget">
        <div class="panel"><img class="preloader" src="http://static.o.bts.mcets-inc.com/kernel/img/bts/preloader.gif" alt="Loading.." width="22" height="22" /></div>
        <div class="count"></div>
        <div class="label"><?php echo __("En Linea"); ?></div>                
        <div class="arrow"></div>
    </div>
    <div id="infoUserOnline" class="modal hide" style="width: 750px;margin-left: -350px;" data-backdrop="static">
        <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel"><?php echo __("Informacion General"); ?></h3>
                </div>
                <div class="modal-body">
                     <form class="form-horizontal">
            <fieldset>
                <legend><?php echo __("Informacion de Usuario"); ?></legend>
                <div class="control-group"><label class="control-label" for="input01"><?php echo __("Nombre"); ?>:</label><div class="controls"><span id="nombreCompleto" class="input-large uneditable-input"></span></div></div>
                <div class="control-group"><label class="control-label" for="input01"><?php echo __("Usuario"); ?>:</label><div class="controls"><span id="usuario" class="input-large uneditable-input"></span></div></div>
                <div class="control-group"><label class="control-label" for="input01"><?php echo __("Id usuario"); ?>:</label><div class="controls"><span id="idUser" class="input-large uneditable-input"></span></div></div>
                <div class="control-group"><label class="control-label" for="input01"><?php echo __("Oficina"); ?>:</label><div class="controls"><span id="oficina" class="input-large uneditable-input"></span></div></div>
                <div class="control-group"><label class="control-label" for="input01"><?php echo __("Grupo"); ?>:</label><div class="controls"><span id="grupo" class="input-large uneditable-input"></span></div></div>
            </fieldset>
            <fieldset>
                <legend><?php echo __("Informacion de Sesion"); ?></legend>
                    <div class="control-group"><label class="control-label" for="input01"><?php echo __("Sesion Id"); ?>:</label><div class="controls"><span id="sessionId" class="input-large uneditable-input"></span></div></div>
                    <div class="control-group"><label class="control-label" for="input01"><?php echo __("Navegador"); ?>:</label><div class="controls"><span id="navegador" class="input-large uneditable-input"></span></div></div>
                    <div class="control-group"><label class="control-label" for="input01"><?php echo __("Fecha"); ?>:</label> <div class="controls"><span id="fecha" class="input-large uneditable-input"></span></div></div>
                    <div class="control-group"><label class="control-label" for="input01"><?php echo __("Actualizada"); ?>:</label><div class="controls"><span id="lastActive" class="input-large uneditable-input"></span></div></div>
                    <div class="control-group"><label class="control-label" for="input01"><?php echo __("IP"); ?>:</label><div class="controls"><span id="ip" class="input-large uneditable-input"></span></div></div>
            </fieldset>
            <fieldset>             
                <legend class="frmSubtitle"><?php echo __("Informacion de Ubicacion"); ?></legend>
                <div id="canvas_map" class="" style="width: 700px;;color: black; height: 300px;"></div>
            </fieldset>
        </form>
         </div>
    </div>
<?php } ?>