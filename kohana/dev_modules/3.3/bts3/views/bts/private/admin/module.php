<style type="text/css">
    .module_change_status{cursor: pointer;}
</style>

<script type="text/javascript">
    
    /* URL */
    var s_url_module_list = '/private/module/list.json'; 
    var s_url_module_create = '/private/module/create.json';
    var s_url_module_update = '/private/module/update.json';
    var s_url_module_get = '/private/module/get.json';
    var s_url_module_change_status = '/private/module/changeStatus.json';
    var s_url_module_share = '/private/module/share.json';
    
    /* MENSAJES */ 
    var s_validate_required_fmodule_id = '<?php echo __("Codigo requerido")?>';
    var s_validate_required_fmodule_name = '<?php echo __("Nombre requerido")?>';
    var s_module_saved = '<?php echo __("Modulo guardado")?>'; 
    var s_module_change = '<?php echo __("Estado de modulo cambiado")?>'; 
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
    var s_module_shared = '<?php echo __("Modulos Compartidos")?>';
    
    
    var a_jqgrid_module_col_names = [
        '<?php echo __("Codigo") ?>'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Estado") ?>'
        , '<?php echo __("Order") ?>'
        , '<?php echo __("Creado por") ?>'
        , '<?php echo __("Creado la fecha") ?>'
        , '<?php echo __("Editado por") ?>'
        , '<?php echo __("Editado la fecha") ?>'
        , '<?php echo __("Acciones") ?>'        
    ];
  
    var a_jqgrid_module_col_model = [
        {name: 'module_id', index: 'module_id', key: true},
        {name: 'module_name', index: 'module_name', width: 120 },
        {name: 'module_status', index: 'module_status', width: 50},
        {name: 'module_order', index: 'module_order', hidden: true},
        {name: 'module_userCreate', index: 'module_userCreate' , width: 100},
        {name: 'module_dateCreate', index: 'module_dateCreate', width: 120},        
        {name: 'module_userLastChange', index: 'module_userLastChange',  width: 100},        
        {name: 'module_dateLastChange', index: 'module_dateLastChange', width: 120 },        
        {name: 'module_actions', index: 'module_actions',  width: 100, align:'center'}
    ];

</script>

<a href="#modal_module" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_modal_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small disabled" data-toggle="modal" disabled="disabled">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>

<table id="jqgrid_module"></table>
<div id="jqgrid_module_pager"></div>



<div id="modal_module" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Modulo") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_module" class="form-horizontal">
            <input id="fmodule_id" name="fmodule_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fmodule_id"><?php echo __("Codigo")?></label>
                <div class="controls">
                    <input type="text" id="fmodule_code" name="fmodule_code" placeholder="<?php echo __("Codigo")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fmodule_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fmodule_name" name="fmodule_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
           
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_module_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>