<script type="text/javascript">
    var a_jqgrid_option_col_names = [
        'KEY'
        , '<?php echo __("Componente") ?>'
        , 'hidden_data_type'
        , '<?php echo __("Tipo de Dato") ?>'
        , '<?php echo __("Prefijo") ?>'
        , '<?php echo __("Valor") ?>'
        , '<?php echo __("Sufijo") ?>'
        , '<?php echo __("Opcion Root") ?>'
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_option_col_model = [
        {name: 'keya', index: 'keya', hidden: false, key: true, width: 220, searchoptions: {sopt: ['cn']}},
        {name: 'component', index: 'component', width: 100},
        {name: 'hidden_data_type', index: 'hidden_data_type', width: 200, hidden: true},
        {name: 'data_type', index: 'data_type', width: 100, align: 'center', search: false},
        {name: 'prefix', index: 'prefix', width: 70, search: false},
        {name: 'value', index: 'value', width: 80, search: false},
        {name: 'suffix', index: 'suffix', width: 70, search: false},
        {name: 'is_root', index: 'is_root', width: 80, align: 'center', search: false},
        {name: 'actions', index: 'actions', width: 120, sortable: false, align: 'center', search: false}
    ];

    var s_url_option_list = '/private/option/list.json';
    var s_url_option_create = '/private/option/create.json';
    var s_url_option_update = '/private/option/update.json';
    var s_url_option_save_order = '/private/option/saveOrder.json';
    var s_url_option_get = '/private/option/get.json';
    var s_url_option_delete = '/private/option/delete.json';
    var s_url_option_set_root = '/private/option/setroot.json';
    var s_url_option_list_order = '/private/option/listOrder.json';
    var s_url_option_enum_component = '/private/option/listValuesComponent.json';
    var s_url_option_update_enum_component = '/private/option/updateEnumValuesComponent.json';

    var s_url_option_share = '/private/option/share.json';
    
    var s_place_holder_component = "<?php echo __('Seleccionar Componente') ?>";
    var s_place_holder_type = "<?php echo __('Seleccionar Tipo') ?>";

    var s_option_saved = '<?php echo __("Opcion Guardada") ?>';
    var s_option_deleted = '<?php echo __("Opcion Eliminada") ?>';
    var s_option_order_saved = '<?php echo __("Orden de Opciones Guardada") ?>';
    var s_option_shared = '<?php echo __("Opcion Compartida") ?>';

    var s_validate_required_foption_component = '<?php echo __("Componente requerido") ?>';
    var s_validate_required_foption_type = '<?php echo __("Tipo requerido") ?>';
    var s_validate_required_foption_type_boolean = '<?php echo __("Valor requerido") ?>';
    var s_validate_required_foption_type_string = '<?php echo __("Valor requerido") ?>';
    var s_validate_required_foption_type_color = '<?php echo __("Color requerido") ?>';
    var s_validate_required_foption_type_integer = '<?php echo __("Valor requerido") ?>';
    var s_validate_required_foption_key = '<?php echo __("Key requerido") ?>';
    var s_validate_required_foption_key_regex = '<?php echo __("Key invalida") ?>';
    var s_validate_required_foption_description = '<?php echo __("Descripcion requerido") ?>';
    var s_validate_required_foption_isroot = '<?php echo __("Es Root requerido") ?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido") ?>';

    var s_option_type_boolean = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_BOOLEAN ?>';
    var s_option_type_string = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_STRING ?>';
    var s_option_type_color = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_COLOR ?>';
    var s_option_type_integer = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_INTEGER ?>';

    var s_option_component_with_option = '<?php echo __("El componente tienes opciones asignadas, por lo tanto no se puede eliminar") ?>';
    var s_option_select_text_all = '<?php echo __("Todos") ?>';
</script>
<style type="text/css">
    textarea{resize: none;}
    .option_change_is_root, .option_sort, #cmpList li i{cursor: pointer;}
    .icon-remove{margin-right: 4px;}
</style>
<a href="#" role="button" id="btn_new_component" class="btn btn-small" data-toggle="modal">
    <i class="icon-cog"></i><?php echo __("Componente") ?>
</a>
<a href="#modal_option" role="button" id="btn_add_option" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_modal_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small disabled" data-toggle="modal" disabled="disabled">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_option"></table>
<div id="jqgrid_option_pager"></div>

<div id="modal_option_sort" class="modal hide" data-keyboard="false" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Ordenar") ?></h3>
    </div>
    <div class="modal-body">
        <ul id="option_sort_list" class="unstyled">

        </ul>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_option_sort_btn_ok" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_component" class="modal hide" data-keyboard="false" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Componentes") ?></h3>
    </div>
    <div class="modal-body">
        <div class="control-group">
            <form id="form_option_component" class="form-horizontal">
                <button type="submit" style="height: 0;width: 0;display: block;margin: 0;padding: 0;line-height: 0;opacity: 0;"></button>
                <input type="hidden" id="fcomponent_hidden_key" name="fcomponent_hidden_key"/>
                <div class="control-group">
                    <label class="control-label" for="foption_key"><?php echo __("Nombre") ?></label>
                    <div class="controls">
                        <input type="text" id="fname_component" name="fname_component" placeholder="<?php echo __("Nombre") ?>"/>
                        <a id="btn_add_component" role="button" class="btn btn-small" data-toggle="modal">
                            <i class="icon-thumbs-up"></i>
                        </a>                        
                    </div>
                    <div style="margin-top: 10px;">
                        <ul id="cmpList" class="unstyled connect_list_js"></ul>
                    </div>
                </div>
            </form>
            <form id="frmComponent" method="post">
                <ul id="cmpList" class="list">

                </ul>
            </form>            
        </div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_option_component_btn_ok" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_option" class="modal hide" data-keyboard="false" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Opcion") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_option" class="form-horizontal">
            <button type="submit" style="height: 0;width: 0;display: block;margin: 0;padding: 0;line-height: 0;opacity: 0;"></button>
            <input type="hidden" id="foption_hidden_key" name="foption_hidden_key"/>
            <div class="control-group">
                <label class="control-label" for="foption_component"><?php echo __("Componente") ?></label>
                <div class="controls">
                    <select id="foption_component" name="foption_component" id="foption_component">
                        <option></option>
                        <?php foreach ($BTS_A_OPTION_COMPONENT as $a_component): ?>
                            <option value="<?php echo $a_component ?>"><?php echo __($a_component) ?></option>                        
                        <?php endforeach ?>
                    </select>                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foption_key"><?php echo __("Key") ?></label>
                <div class="controls">
                    <input type="text" id="foption_key" name="foption_key" placeholder="<?php echo __("Key") ?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foption_type"><?php echo __("Tipo") ?></label>
                <div class="controls">
                    <select id="foption_type" name="foption_type">
                        <option></option>
                        <?php foreach ($BTS_A_OPTION_TYPE as $a_type): ?>
                            <option value="<?php echo $a_type['value'] ?>"><?php echo __($a_type['display']) ?></option>                        
                        <?php endforeach ?>
                    </select>                    
                </div>
            </div>
            <div class="control-group foption_type foption_type_boolean">
                <label class="control-label"><?php echo __("Valor") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="foption_type_boolean" id="foption_type_boolean_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="foption_type_boolean" id="foption_type_boolean_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group foption_type foption_type_string hide">
                <label class="control-label" for="foption_prefix"><?php echo __("Prefijo") ?></label>
                <div class="controls">
                    <input type="text" id="foption_prefix" name="foption_prefix" placeholder="<?php echo __("Prefijo") ?>"/>
                </div>
            </div>
            <div class="control-group foption_type foption_type_string hide">
                <label class="control-label" for="foption_type_string"><?php echo __("Valor") ?></label>
                <div class="controls">
                    <input type="text" id="foption_type_string" name="foption_type_string" placeholder="<?php echo __("Valor") ?>" required/>
                </div>
            </div>
            <div class="control-group foption_type foption_type_string hide">
                <label class="control-label" for="foption_suffix"><?php echo __("Sufijo") ?></label>
                <div class="controls">
                    <input type="text" id="foption_suffix" name="foption_suffix" placeholder="<?php echo __("Sufijo") ?>"/>
                </div>
            </div>
            <div class="control-group foption_type foption_type_integer hide">
                <label class="control-label" for="foption_type_integer"><?php echo __("Valor") ?></label>
                <div class="controls">
                    <input type="text" id="foption_type_integer" name="foption_type_integer" placeholder="<?php echo __("Valor") ?>" required/>
                </div>
            </div>
            <div class="control-group foption_type foption_type_color hide">
                <label class="control-label" for="foption_type_color"><?php echo __("Color") ?></label>
                <div class="controls">
                    <input type="text" id="foption_type_color" name="foption_type_color" placeholder="<?php echo __("Color") ?>"/>
                </div>
            </div>            
            <div class="control-group">
                <label class="control-label" for="foption_description"><?php echo __("Descripcion Corta") ?></label>
                <div class="controls">
                    <textarea id="foption_description" name="foption_description" rows="2" placeholder="<?php echo __("Descripcion Corta") ?>"></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foption_long_description"><?php echo __("Descripcion Larga") ?></label>
                <div class="controls">
                    <textarea id="foption_long_description" name="foption_long_description" rows="2" placeholder="<?php echo __("Descripcion Larga") ?>"></textarea>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Root") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="foption_isroot" id="foption_isroot_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="foption_isroot" id="foption_isroot_2" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_option_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>
<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach ($BTS_A_DB_CONFIGS as $a_config): ?>
                        <label class="checkbox">
                            <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE] ?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE] ?>" />
                            <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME] ?>
                        </label>
                    <?php endforeach ?>
                </div>
            </div>
        </form>        
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>