<script type="text/javascript">
    var s_country_search = '<?php echo $BTS_OPTION_DEFAULT_COUNTRY?>';
    
    var s_url_city_save = '/private/city/save.json';
</script>
<style type="text/css">
    #ul_container_cities_list li i,
    #ul_container_cities_saved li i{cursor: pointer;}
    
    #ul_container_cities_list li i.icon-remove{display: none;}
    #ul_container_cities_saved li i.icon-plus{display: none;}

    #ul_container_cities_list, #ul_container_cities_saved{
        min-height: 50px;
    }
</style>
<div class="row">
    <div class="span6"> 
        <form id="form_city_list" class="form-inline">
            <div class="control-group">
                <label class="control-label" for="fcity_list_search"><?php echo __("Filtrar")?></label>
                <input type="text" name="fcity_list_search" id="fcity_list_search"/>                
            </div>
        </form>
        <ul id="ul_container_cities_list" class="unstyled connect_list_cities"></ul>
    </div>
    <div class="span6">         
        <form id="form_city_saved" class="form-inline">
            <div class="control-group">
                <label class="control-label" for="fcity_saved_search"><?php echo __("Filtrar")?></label>
                <input type="text" name="fcity_saved_search" id="fcity_saved_search"/>
                <button id="form_city_saved_btn_save" type="submit" class="btn btn-primary"><?php echo __("Guardar")?></button>                
            </div>
        </form>
        
        <ul id="ul_container_cities_saved" class="unstyled connect_list_cities">
            <?php foreach($BTS_A_CITIES_SAVED as $o_city):?>
            <li id="key_<?php echo $o_city->idCity?>" data-name="<?php echo $o_city->name?>" data-country="<?php echo $o_city->country?>" class="alert alert-success">
                <i class="icon-remove"></i>
                <i class="icon-plus"></i>
                <?php echo $o_city->country?>, 
                <?php echo $o_city->name?>
            </li>
            <?php endforeach?>                                        
        </ul>
    </div>
</div>