<script type="text/javascript">
    var s_url_section_get_list_files = '/private/section/getListFile.json';
    var s_url_section_save_files = '/private/section/saveFiles.json';
    var s_url_section_share = '/private/section/share.json';
    
    var s_section_shared = '<?php echo __("Seccion Compartida")?>';
    
    var s_validate_required_fsection_page = '<?php echo __("Pagina requerida")?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
</script>
<style type="text/css">
    #ul_container_js li i,
    #ul_container_js_drop li i,
    #ul_container_css li i,
    #ul_container_css_drop li i{cursor: pointer;}
    
    #ul_container_js li i.icon-remove{display: none;}
    #ul_container_js_drop li i.icon-plus{display: none;}
    
    #ul_container_css li i.icon-remove{display: none;}
    #ul_container_css_drop li i.icon-plus{display: none;}

    #ul_container_js, #ul_container_js_drop,
    #ul_container_css, #ul_container_css_drop{
        min-height: 50px;
    }
</style>
<div class="row">
    <div class="span6">        
        <form id="form_section_search" class="form-inline">
            <div class="control-group">
                <label class="control-label"><?php echo __("Buscar")?></label>
                <input type="text" id="fsection_search" name="fsection_search"/>
            </div>
        </form>
        <div class="tabbable"> <!-- Only required for left/right tabs -->
          <ul class="nav nav-tabs">
            <li class="active">
                <a href="#div_js" data-toggle="tab">JS</a>
            </li>
            <li>
                <a href="#div_css" data-toggle="tab">CSS</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="div_js">
                <ul id="ul_container_js" class="unstyled connect_list_js">
                    <?php foreach($BTS_A_FILE_JS_OBJ as $o_file):?>
                    <li id="js_core_sort_key_<?php echo $o_file->idFile?>" class="item_core_js alert">
                        <i class="icon-remove"></i>
                        <i class="icon-plus"></i>
                        <?php echo $o_file->idFile?>
                    </li>
                    <?php endforeach?>                                        
                </ul>
            </div>
            <div class="tab-pane" id="div_css">
                <ul id="ul_container_css" class="unstyled connect_list_css">
                    <?php foreach($BTS_A_FILE_CSS_OBJ as $o_file):?>
                    <li id="css_core_sort_key_<?php echo $o_file->idFile?>" class="item_core_css alert">
                        <i class="icon-remove"></i>
                        <i class="icon-plus"></i>
                        <?php echo $o_file->idFile?>                        
                    </li>
                    <?php endforeach?>                    
                </ul>
            </div>
          </div>
        </div>
    </div>
    <div class="span6">  
        <div class="row">
            <div class="span6">
                <form id="form_section" class="form-inline">
                    <div class="control-group">
                        <label class="control-label"><?php echo __("Pagina")?></label>
                        <select id="fsection_page" name="fsection_page">
                            <option></option>
                            <?php foreach($BTS_A_PAGES_OBJ as $o_page):?>
                            <option value="<?php echo $o_page->idPage?>"><?php echo $o_page->idPage?></option>
                            <?php endforeach;?>
                        </select>
                        <button id="form_section_btn_save" type="submit" class="btn btn-primary"><?php echo __("Guardar")?></button>
                        <button id="form_section_btn_share" type="button" class="btn"><?php echo __("Compartir")?></button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="span3">
                <p class="text-center">
                    <span class="label label-success">JS</span>
                </p>
                <ul id="ul_container_js_drop" class="unstyled connect_list_js">
                                       
                </ul>
            </div>
            <div class="span3">
                <p class="text-center">
                    <span class="label label-info">CSS</span>
                </p>
                <ul id="ul_container_css_drop" class="unstyled connect_list_css">
                    
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>