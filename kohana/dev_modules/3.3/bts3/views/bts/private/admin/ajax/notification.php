<link rel="stylesheet" href="/kernel/css/bts/select2/select2.css">
<script type="text/javascript" src="/kernel/js/bts/core/select2.js"></script>
<script type="text/javascript" src="/kernel/js/bts/core/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/kernel/js/bts/core/ckeditor/config.js?t=CAPD"></script>
<script type="text/javascript" src="/kernel/js/bts/core/ckeditor/lang/en.js?t=CAPD"></script>
<link rel="stylesheet" href="/kernel/js/bts/core/ckeditor/skins/moono/editor.css?t=CAPD"/>
<script type="text/javascript" src="/kernel/js/bts/core/ckeditor/plugins/youtube/plugin.js?t=CAPD"></script>
<script type="text/javascript" src="/kernel/js/bts/core/ckeditor/styles.js?t=CAPD"></script>
<script type="text/javascript">
     function fnNotificationFormatIcon(state) {
            if (!state.id)
                return '';
            var $o_option = $(state.element);
            var s_icon = 'icon-globe';
            if ($o_option.data('to_city') > 0) {
                s_icon = 'icon-screenshot';
            } else if ($o_option.data('to_office') > 0) {
                s_icon = 'icon-home';
            } else if ($o_option.data('to_user') > 0) {
                s_icon = 'icon-user';
            } else if ($o_option.data('to_group') > 0) {
                s_icon = 'icon-list';
            } else if ($o_option.data('to_group_name') > 0) {
                s_icon = 'icon-asterisk';
            }


            return '<b class="' + s_icon + '"></b> ' + $(state.element).text();
        }
$(document).ready(function() {
  $("#dlg_form_value_search").select2({
            formatResult: fnNotificationFormatIcon,
            formatSelection: fnNotificationFormatIcon,
            escapeMarkup: function(m) {
                return m;
            },
            width: 'resolve',
            allowClear: true
        }).change(function() {
            var $this = $(this);

            $('#form_message_t input[type=hidden]').val('');

            if ($this.val() != '') {
                var o_option = $this.find('option:selected');
                $('#dlg_form_to_city').val(o_option.data('to_city'));
                $('#dlg_form_to_office').val(o_option.data('to_office'));
                $('#dlg_form_to_user').val(o_option.data('to_user'));
                $('#dlg_form_to_group').val(o_option.data('to_group'));
                $('#dlg_form_to_group_name').val(o_option.data('to_group_name'));
            }
        });    
if (b_fnGetAllowNotifications){
    
                    CKEDITOR.replace('dlg_form_message_content', {
                        width: 355,
                        height: 250
                    });

                    var ck_editor = CKEDITOR.instances.dlg_form_message_content;
                    var form_message_t = $('#form_message_t').validate({
                        rules: {
                            dlg_form_value_search: {
                                required: true
                            }
                        },
                        messages: {
                            dlg_form_value_search: {
                                required: '<?php echo __("Seleccione un usuario") ?>'
                            }
                        },                        
                        errorClass: 'help-inline',
                        highlight: function(element, errorClass) {
                            $(element).parents('.control-group').addClass('error');
                        },
                        unhighlight: function(element, errorClass, validClass) {
                            $(element).parents('.control-group').removeClass('error');
                        },
                        submitHandler: function(oForm) {
                            if (b_ajax) {                                
                                o_data_form = $(oForm).serializeObject();
                                $.extend(o_data_form, {
                                    dlg_form_message_content: fnGetContentCKEditorMessage(),
                                    t_type_notification: 1
                                });
                                $.ajax({
                                    url: "/private/notification/save.json" + jq_jsonp_callback,                                    
                                    data: o_data_form
                                    , beforeSend: function() {
                                        $('#modal_notification_btn_save').button('loading');
                                    }
                                    , complete: function() {
                                        $('#modal_notification_btn_save').button('reset');
                                    },
                                    success: function(resp) {
                                        if (evalResponse(resp)) {
                                            o_data = resp.data;                                            
                                            if (o_data == 1)
                                            {
                                                $('#modal_notification').modal('hide');
                                                CKEDITOR.instances.dlg_form_message_content.setData('');
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });               

                    $('#modal_notification_btn_save').click(function(){
                        $('#form_message_t').submit();
                    });

                    CKEDITOR.on('dialogDefinition', function(ev) {
                        var dialogDefinition = ev.data.definition;
                        dialogDefinition.afterOk = function(e) {
                            fnUpdateCKEditorMessage();
                        };
                    });


                    ck_editor.on("afterCommandExec", function() {
                        fnUpdateCKEditorMessage();
                    });

                    ck_editor.on("instanceReady", function() {
                        this.document.on("keyup", function() {
                            fnUpdateCKEditorMessage();
                        });
                    });

                    function fnGetContentCKEditorMessage() {
                        return  ck_editor.getData();
                    }
                    function fnUpdateCKEditorMessage() {
                        var tmp_ele = $('<div/>').html(fnGetContentCKEditorMessage());
                        if ($.trim(tmp_ele.text()) == '') {
                            $('li#li_preview').remove();
                        } else {
                            if ($('li#li_preview').length == 0) {
                                var li_preview = $('<li>');
                                li_preview.attr('id', 'li_preview');
                                li_preview.addClass('old_notification notification item_message');
                                var div_notification = $('<div>').addClass('div_notification');

                                var div_general_notification_image = $('<div>').addClass('general_notification_image ui-corner-all ui-widget-header');
                                div_general_notification_image.css({
                                    'background-size': '100%'
                                    , 'background-image': 'url("http://static.o.bts.mcets-inc.com/kernel/img/profile/64/default.png")'
                                    , 'background-position': 'center center'
                                    , 'background-repeat': 'no-repeat'
                                });

                                div_notification.append(div_general_notification_image);

                                var div_general_notification_content = $('<div>').addClass('general_notification_content');

                                var div_row = $('<div>').addClass('row_notification');

                                var span_user_name = $('<span>').addClass('user_name');
                                span_user_name.text('<?php echo $BTS_USER_FULL_NAME ?>');

                                var span_message_date = $('<span>').addClass('message_date');
                                span_message_date.text(s_now);

                                div_row.append(span_user_name);
                                div_row.append(span_message_date);

                                div_general_notification_content.append(div_row);

                                var div_row = $('<div>').addClass('row_notification');

                                var div_notification_text = $('<div>').addClass('notification_text');

                                div_notification_text.html(fnGetContentCKEditorMessage());

                                div_row.append(div_notification_text);

                                div_general_notification_content.append(div_row);

                                div_notification.append(div_general_notification_content);

                                var div_clear = $('<div>').addClass('clearfix');
                                div_notification.append(div_clear);

                                var div_general_notification_footer = $('<div>').addClass('general_notification_footer');
                                var span_general_time = $('<span>').addClass('general_time');

                                span_general_time.text(s_now);

                                div_general_notification_footer.append(span_general_time);
                                div_notification.append(div_general_notification_footer);

                                var div_clear = $('<div>').addClass('clearfix');
                                div_notification.append(div_clear);

                                li_preview.append(div_notification);

                                $('#ul_preview_message').append(li_preview);
                            } else {
                                $('li#li_preview div.notification_text').html(fnGetContentCKEditorMessage());
                            }
                        }
                    }
}
});
   </script>    

<?php if(HelperNotification::fnGetAllowNotificationsShow() AND HelperNotification::fnGetAllowNotifications()):?>
        <div id="modal_notification" class="modal hide" style="width: 800px;margin-left: -400px;" data-backdrop="static">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3><?php echo __("Notificación") ?></h3>
            </div>
            <div class="modal-body">
                <textarea name="dlg_form_message_content" id="dlg_form_message_content"></textarea>
                <ul id="ul_preview_message">
                    <li id="message_id_n" class="old_notification item_message blur_effect">
                        <div class="div_img_action_msg"></div>
                        <div class="div_notification">
                            <div class="general_notification_image" style="background-size: 100%; background-image: url('http://static.o.bts.mcets-inc.com/kernel/img/profile/64/default.png');background-position:center center; background-repeat:no-repeat;">
                            </div>
                            <div class="general_notification_content">
                                <div class="row_notification">
                                    <span class="user_name">
                                        MCETS
                                    </span> 
                                    <span class="message_date">
                                        01/01/2013 07:00 AM                                                        
                                    </span>
                                </div>
                                <div class="row_notification">
                                    <div class="notification_text">
                                        Hello, Welcome to the new system.  Please let us know if you have any questions.
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="general_notification_footer">           
                                <span class="general_time">10 Dic</span>
                            </div>
                            <div class="clearfix"></div>                            
                        </div>
                    </li>                    
                </ul>

                
            </div>
            <div class="modal-footer">
                <form id="form_message_t" method="POST" action="#" class="form-horizontal">
                    
                    <input name="dlg_form_to_group" id="dlg_form_to_group" type="hidden"/>
                    <input name="dlg_form_to_city" id="dlg_form_to_city" type="hidden"/>
                    <input name="dlg_form_to_office" id="dlg_form_to_office" type="hidden"/>
                    <input name="dlg_form_to_user" id="dlg_form_to_user" type="hidden"/>                    
                    <input name="dlg_form_to_group_name" id="dlg_form_to_group_name" type="hidden"/>
               
                    <div class="control-group">
                        <div class="controls">
                            <select name="dlg_form_value_search" id="dlg_form_value_search">
                            </select>
                        </div>
                    </div>
                    
                </form>
                <button id="modal_notification_btn_save" type="button" class="btn btn-primary"><?php echo __("Guardar") ?></button>
            </div>
        </div>
        <?php endif?>