<script type="text/javascript">
    var a_jqgrid_group_col_names = [
        'ID'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Root") ?>'
        , '<?php echo __("Mensajes") ?>'        
        , '<?php echo __("Notificaciones") ?>'        
        , '<?php echo __("Notificar") ?>'        
        , '<?php echo __("Activo") ?>'        
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_group_col_model = [
        {name: 'group_id', index: 'group_id', hidden: true, key: true},
        {name: 'group_name', index: 'group_name', width: 200},
        {name: 'group_root_option', index: 'group_root_option', width: 80, align: 'center'},
        {name: 'group_allow_chat', index: 'group_allow_chat', width: 80, align: 'center'},
        {name: 'group_allow_notifications_show', index: 'group_allow_notifications_show', width: 80, align: 'center'},
        {name: 'group_allow_notifications', index: 'group_allow_notifications', width: 80, align: 'center'},
        {name: 'group_status', index: 'group_status', width: 80, align: 'center'},
        {name: 'group_actions', index: 'group_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    var a_jqgrid_group_active_col_names = [
        'ID'
        , '<?php echo __("Nombre") ?>'      
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_group_active_col_model = [
        {name: 'group_id', index: 'group_id', hidden: true, key: true},
        {name: 'group_name', index: 'group_name', width:300},     
        {name: 'group_actions', index: 'group_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    
    var s_url_group_list = '/private/group/list.json';
    var s_url_group_active_list = '/private/group/listActive.json';
    var s_url_group_create = '/private/group/create.json';
    var s_url_group_update = '/private/group/update.json';    
    var s_url_group_get = '/private/group/get.json';
    var s_url_group_change_root_option = '/private/group/changeRootOption.json';
    var s_url_group_change_allow_chat = '/private/group/changeAllowChat.json';
    var s_url_group_change_allow_notifications = '/private/group/changeAllowNotifications.json';
    var s_url_group_change_allow_notifications_show = '/private/group/changeAllowNotificationsShow.json';
    var s_url_group_change_status = '/private/group/changeStatus.json';
    var s_url_group_remove_final='/private/group/removeFinal.json';
    var s_url_group_active='/private/group/active.json';
    var s_validate_required_fgroup_name = '<?php echo __("Nombre requerido")?>';
    var s_validate_required_fgroup_default_page = '<?php echo __("Página requerida")?>';
    var s_dialog2_button_label_confirmation_active_text='<?php echo __("Desea activar Grupo?")?>'; 
    
    var s_group_saved = '<?php echo __("Grupo Guardado") ?>';
    var s_group_active = '<?php echo __("Grupo Activado") ?>';
    var s_group_deleted = '<?php echo __("Grupo Eliminado") ?>';
    
    var s_group_select_text_all = '<?php echo __("Todos")?>';    
</script>
<style type="text/css">
    .group_change_root_option,
    .group_change_allow_chat,
    .group_change_allow_notifications,
    .group_change_status{cursor: pointer;}
</style>
<div>
    <a href="#modal_group" role="button" class="btn btn-small" data-toggle="modal">
        <i class="icon-plus"></i><?php echo __("Agregar") ?>
    </a>
    <a href="#modal_group_active" role="button" class="btn btn-small" data-toggle="modal">
        <i class="icon-filter"></i><?php echo __("Activar/Eliminar") ?>
    </a>
</div>
<table id="jqgrid_group"></table>
<div id="jqgrid_group_pager"></div>

<div id="modal_group_active" class="modal hide" data-backdrop="static" style="width:500px" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Activar/Eliminar Grupo") ?></h3>
    </div>
    <div class="modal-body">
        <table id="jqgrid_group_active"></table>
        <div id="jqgrid_group_active_pager"></div>        
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>        
    </div>
</div>

<div id="modal_group" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Grupo") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_group" class="form-horizontal">
            <input id="fgroup_id" name="fgroup_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fgroup_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fgroup_name" name="fgroup_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fgroup_default_page"><?php echo __("Página por defecto")?></label>
                <div class="controls">
                    <input type="text" id="fgroup_default_page" name="fgroup_default_page" placeholder="<?php echo __("Página por defecto")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Root Option") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fgroup_root_option" id="fgroup_root_option_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fgroup_root_option" id="fgroup_root_option_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Chat") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_chat" id="fgroup_allow_chat_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_chat" id="fgroup_allow_chat_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Mostrar Notificaciones") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_notifications_show" id="fgroup_allow_notifications_show_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_notifications_show" id="fgroup_allow_notifications_show_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Notificaciones") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_notifications" id="fgroup_allow_notifications_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fgroup_allow_notifications" id="fgroup_allow_notifications_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_group_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>