<script type="text/javascript">
    var a_jqgrid_file_col_names = [
        'KEY'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Tipo") ?>'
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_file_col_model = [
        {name: 'file_key', index: 'file_key', hidden: false, key: true, width: 220, searchfiles: {sopt: ['cn']}},
        {name: 'file_name', index: 'file_name', width: 200},
        {name: 'file_type', index: 'file_type', width: 80, align: 'center'},
        {name: 'file_actions', index: 'file_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    
    var s_place_holder_type = '<?php echo __("Tipo")?>';
    
    var s_validate_required_ffile_key_regex = '<?php echo __("Key Invalida")?>';
    var s_validate_required_ffile_key = '<?php echo __("Key requerida")?>';
    var s_validate_required_ffile_name = '<?php echo __("Nombre requerido")?>';
    var s_validate_required_ffile_type = '<?php echo __("Tipo requerido")?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
    
    var s_file_shared = '<?php echo __("Archivo compartido") ?>';
    var s_file_saved = '<?php echo __("Archivo Guardada") ?>';
    var s_file_deleted = '<?php echo __("Archivo Eliminada") ?>';
    var s_file_order_saved = '<?php echo __("Orden de Archivos Guardado") ?>';
    
    var s_url_file_list = '/private/file/list.json';
    var s_url_file_create = '/private/file/create.json';
    var s_url_file_update = '/private/file/update.json';
    var s_url_file_save_order = '/private/file/saveOrder.json';
    var s_url_file_get = '/private/file/get.json';
    var s_url_file_delete = '/private/file/delete.json';
    var s_url_file_share = '/private/file/share.json';
    
    
    var s_file_select_text_all = '<?php echo __("Todos")?>';
</script>
<a href="#modal_file" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_modal_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small disabled" data-toggle="modal" disabled="disabled">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_file"></table>
<div id="jqgrid_file_pager"></div>

<div id="modal_file" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Archivo") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_file" class="form-horizontal">
            <input id="ffile_id" name="ffile_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="ffile_key"><?php echo __("Key")?></label>
                <div class="controls">
                    <input type="text" id="ffile_key" name="ffile_key" placeholder="<?php echo __("Key")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="ffile_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="ffile_name" name="ffile_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="ffile_type"><?php echo __("Tipo")?></label>
                <div class="controls">
                    <select id="ffile_type" name="ffile_type">
                        <option></option>
                        <?php foreach($BTS_A_FILES as $s_file):?>
                        <option value="<?php echo $s_file?>"><?php echo $s_file?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="ffile_isCoreFile"><?php echo __("Parte del Kernel")?></label>
                <div class="controls">
                    <input type="checkbox" id="ffile_isCoreFile" name="ffile_isCoreFile" title="<?php echo __("Parte del Kernel")?>"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_file_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>
<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>