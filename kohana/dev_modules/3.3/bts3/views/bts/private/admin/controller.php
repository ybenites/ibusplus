<script type="text/javascript">
    var a_jqgrid_controller_col_names = [
        'ID'
        , '<?php echo __("Directorio") ?>'
        , '<?php echo __("Controlador") ?>'
        , '<?php echo __("Accion") ?>'        
        , '<?php echo __("Tipo") ?>'        
        , '<?php echo __("Acciones") ?>'
    ];
    
    var a_jqgrid_controller_col_model = [
        {name: 'idController', index: 'idController', width: 1, hidden: true, key: true},
        {name: 'directory', index: 'directory', width: 250},
        {name: 'controller', index: 'controller', width: 250},
        {name: 'action', index: 'action', width: 100},
        {name: 'type', index: 'type', width: 80, align: 'center'},        
        {name: 'actions', index: 'actions', width: 160, sortable: false, align: 'center'}
    ];
    
    var s_place_holder_controller = '<?php echo __("Seleccione un controlador")?>';
    var s_place_holder_action = '<?php echo __("Seleccione una Accion")?>';
    
    var s_validate_required_fcontroller_controller = '<?php echo __("Controlador requerido")?>';
    var s_validate_required_fcontroller_action = '<?php echo __("Accion requerido")?>';
    var s_validate_required_fcontroller_type = '<?php echo __("Tipo requerido")?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
    
    var s_url_controller_share = '/private/controller/share.json';
    var s_url_controller_create = '/private/controller/create.json';
    var s_url_controller_update = '/private/controller/update.json';
    var s_url_controller_get = '/private/controller/get.json';
    var s_url_controller_getActions = '/private/controller/getActions.json';
    var s_url_controller_list = '/private/controller/list.json';
    var s_url_controller_delete = '/private/controller/delete.json';
    
    var s_controller_shared = '<?php echo __("Controlador Compartido")?>';
    var s_controller_saved = '<?php echo __("Controlador Guardado")?>';
    var s_controller_deleted = '<?php echo __("Controlador Eliminado")?>';
    
    var s_controller_select_text_all = '<?php echo __("Todos")?>';
</script>
<a href="#modal_controller" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_modal_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small disabled" data-toggle="modal" disabled="disabled">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_controller"></table>
<div id="jqgrid_controller_pager"></div>
<div id="modal_controller" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Controller") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_controller" class="form-horizontal">
            <input id="fcontroller_id" name="fcontroller_id" type="hidden"/>
            <input id="fcontroller_directory" name="fcontroller_directory" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fcontroller_controller"><?php echo __("Controlador")?></label>
                <div class="controls">
                    <select id="fcontroller_controller" name="fcontroller_controller">
                        <option></option>
                        <?php foreach($BTS_A_CONTROLLERS as $a_icon):?>
                        <option value="<?php echo $a_icon['file']?>" data-directory="<?php echo $a_icon['directory']?>"><?php echo $a_icon['file']?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fcontroller_action"><?php echo __("Accion")?></label>
                <div class="controls">
                    <select id="fcontroller_action" name="fcontroller_action">
                        <option></option>                        
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Tipo") ?></label>
                <div class="controls">
                    <?php foreach ($BTS_A_CONTROLLER_TYPES as $a_item):?>                        
                    <label class="radio">
                        <input type="radio" name="fcontroller_type" id="fcontroller_type_<?php echo $a_item['value']?>" value="<?php echo $a_item['value']?>" <?php echo $a_item['default']?'checked':''?>/>
                        <?php echo __($a_item['display']) ?>
                    </label>
                    <?php endforeach?>                    
                </div>
            </div>
        </form>
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_controller_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>
<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>