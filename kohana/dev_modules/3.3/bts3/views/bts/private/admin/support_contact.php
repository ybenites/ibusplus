<script type="text/javascript">
    var s_url_load_contact_emails = '/private/supportcontact/getSupportEmails';
    var s_url_save_contact_emails = '/private/supportcontact/saveSupportEmails';
    var s_input_name = '<? echo __('Ingrese el nombre');?>';
    var s_input_email = '<? echo __('Ingrese correo electrónico');?>';
    var s_input_empty_validate = '<? echo __('Ningún campo debe estar en blanco');?>';
    var s_input_empty_list_validate = '<? echo __('Al menos debe ingresar un nombre y correo electrónico');?>';
    var s_btn_remove= '<? echo __('Eliminar');?>';
</script>
<style type="text/css">
    #form_support_email ul {
        list-style-type: none;
    }
    #form_support_email i {
        margin-right: 2px;
    }
    a.remove_support_team{
        margin-left: 4px;
        margin-bottom: 7px;
    }
</style>
<p>
<h4><?php echo __('Por favor ingrese los nombres y correos electrónicos del personal de soporte del sistema'); ?>.</h4>
</p>

<p>
    <a id="add_contact_support" class="btn btn-small">
        <i class="icon-plus-sign"></i><?php echo __("Agregar") ?>
    </a>
    <a id="save_contact_support" class="btn btn-small">
        <i class="icon-ok-sign"></i><?php echo __("Guardar") ?>
    </a>
</p>
<br/>
<br/>

<div>
    <form id="form_support_email">
        <ul>
        </ul>
    </form>
</div>