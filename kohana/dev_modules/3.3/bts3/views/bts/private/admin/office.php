<script type="text/javascript">
    var a_jqgrid_office_col_names = [
        'ID'
                , '<?php echo __("Ciudad") ?>'
                , '<?php echo __("Nombre") ?>'
                , '<?php echo __("Direccion") ?>'
                , '<?php echo __("Telefono") ?>'
                , '<?php echo __("Acciones") ?>'
    ];

    var a_jqgrid_office_col_model = [
        {name: 'office_id', index: 'office_id', hidden: true, key: true},
        {name: 'office_city', index: 'office_city', width: 120},
        {name: 'office_name', index: 'office_name', width: 120},
        {name: 'office_address', index: 'office_address', width: 200},
        {name: 'office_phone', index: 'office_phone', width: 100},
        {name: 'office_actions', index: 'office_actions', width: 120, sortable: false, align: 'center', search: false}
    ];

    var s_place_holder_city = '<?php echo __("Seleccione una ciudad") ?>';

    var s_url_office_list = '/private/office/list.json';
    var s_url_office_create = '/private/office/create.json';
    var s_url_office_update = '/private/office/update.json';
    var s_url_office_delete = '/private/office/delete.json';
    var s_url_office_get = '/private/office/get.json';
    var s_url_office_confirm_delete = '/private/office/confirmDelete.json';

    var s_office_saved = '<?php echo __("Oficina guardada") ?>';
    var s_office_deleted = '<?php echo __("Oficina eliminada") ?>';
    var s_office_activated = '<?php echo __("Oficina activada") ?>';

    var s_office_select_text_all = '<?php echo __("Todos") ?>';

    var s_validate_required_foffice_city = '<?php echo __("Ciudad requerida") ?>';
    var s_validate_required_foffice_name = '<?php echo __("Nombre requerido") ?>';

</script>
<a href="#modal_office" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<a href="#modal_activate_delete_office" id="btn_act_del_office" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-retweet" style="margin-right: 3px;"></i><?php echo __("Activar / Eliminar") ?>
</a>
<table id="jqgrid_office"></table>
<div id="jqgrid_office_pager"></div>

<div id="modal_office" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Activar / Eliminar") ?> <?php echo __("Oficinas") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_office" class="form-horizontal">
            <input id="foffice_id" name="foffice_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="foffice_city"><?php echo __("Ciudad") ?></label>
                <div class="controls">
                    <select id="foffice_city" name="foffice_city">                        
                        <?php foreach ($BTS_A_CITIES_LIST as $o_city): ?>
                            <option value="<?php echo $o_city->idCity ?>"><?php echo __($o_city->name) ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foffice_name"><?php echo __("Nombre") ?></label>
                <div class="controls">
                    <input type="text" id="foffice_name" name="foffice_name" placeholder="<?php echo __("Nombre") ?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foffice_address"><?php echo __("Dirección") ?></label>
                <div class="controls">
                    <input type="text" id="foffice_address" name="foffice_address" placeholder="<?php echo __("Dirección") ?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="foffice_phone"><?php echo __("Teléfono") ?></label>
                <div class="controls">
                    <input type="text" id="foffice_phone" name="foffice_phone" placeholder="<?php echo __("Teléfono") ?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Opción Root") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="foffice_root_option" id="foffice_root_option_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="foffice_root_option" id="foffice_root_option_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_office_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_activate_delete_office" class="modal hide" data-backdrop="static" style="width: auto; margin-left: -385px;">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Oficina") ?></h3>
    </div>
    <div class="modal-body">
        <table id="jqgrid_act_del_office"></table>
        <div id="jqgrid_act_del_office_pager"></div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_office_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>