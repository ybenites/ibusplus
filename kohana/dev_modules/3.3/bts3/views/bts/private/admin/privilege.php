<script type="text/javascript">    
    var bts_a_groups = [<?php echo $BTS_S_A_GROUPS?>];
    
    var a_jqgrid_privilege_col_names = [
        'ID'
        , '<?php echo __('Directorio')?>'
        , '<?php echo __('Controller')?>'
        , '<?php echo __('Tipo')?>'
        , '<?php echo __('Tipo')?>'
    ];   
    var a_jqgrid_privilege_col_model = [
        {name: 'privilege_key', index: 'privilege_key', hidden: true, key: true, width: 220},
        {name: 'privilege_directory', index: 'privilege_directory', width: 150},
        {name: 'privilege_controller', index: 'privilege_controller', width: 150},
        {name: 'privilege_type_hidden', index: 'privilege_type_hidden', hidden: true},
        {name: 'privilege_type', index: 'privilege_type', width: 80, align: 'center'}
    ];
    
    //Add dinamic Groups
    for(var i= 0;i<bts_a_groups.length;i++){
        a_jqgrid_privilege_col_names.push(bts_a_groups[i].group_name);
        a_jqgrid_privilege_col_model.push({name: ('group_'+bts_a_groups[i].group_id), index: ('group_'+bts_a_groups[i].group_id), width: 90, search: false, align: 'center'});
    }

    var s_url_privilege_list = '/private/privilege/list.json';
    var s_url_privilege_change = '/private/privilege/change.json';
    
    var s_privilege_saved = '<?php echo __('Privilegio guardado')?>';
    
</script>
<style type="text/css">
    .privilege_change{cursor: pointer;}
</style>
<table id="jqgrid_privilege"></table>
<div id="jqgrid_privilege_pager"></div>