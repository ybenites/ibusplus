<script type="text/javascript">
    var a_jqgrid_user_col_names = [
        'ID'
        , '<?php echo __("Grupo") ?>'
        , '<?php echo __("Oficina") ?>'
        , '<?php echo __("Usuario") ?>'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Email") ?>'
        , '<?php echo __("Root") ?>'
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_user_col_model = [
        {name: 'user_id', index: 'user_id', hidden: true, key: true},
        {name: 'user_group', index: 'user_group', width: 120},
        {name: 'user_office', index: 'user_office', width: 120},
        {name: 'user_username', index: 'user_username', width: 90},
        {name: 'user_full_name', index: 'user_full_name', width: 150},
        {name: 'user_email', index: 'user_email', width: 170},
        {name: 'user_root_option', index: 'user_root_option', width: 70, align: 'center'},
        {name: 'user_actions', index: 'user_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    var a_jqgrid_user_active_col_names = [
        'ID'
        , '<?php echo __("Usuario") ?>'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Grupo") ?>'
        , '<?php echo __("Oficina") ?>'   
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_user_active_col_model = [
        {name: 'user_id', index: 'user_id', hidden: true, key: true},
        {name: 'user_username', index: 'user_username', width: 90},
        {name: 'user_full_name', index: 'user_full_name', width: 150},
        {name: 'user_group', index: 'user_group', width: 120},
        {name: 'user_office', index: 'user_office', width: 120},        
        {name: 'user_actions', index: 'user_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    
    var s_place_holder_group = '<?php echo __("Seleccione un grupo")?>';
    var s_place_holder_office = '<?php echo __("Seleccione una office")?>';
    
    var s_url_user_list = '/private/user/list.json';   
    var s_url_user_active_list = '/private/user/listActive.json';   
    var s_url_user_create = '/private/user/create.json';
    var s_url_user_update = '/private/user/update.json';
    var s_url_user_delete = '/private/user/delete.json';
    var s_url_user_get = '/private/user/get.json';
    var s_url_user_upload_img = '/private/user/photoUpload.json';
    var s_url_user_active='/private/user/active.json';
    var  s_url_user_remove_final='/private/user/removeFinal.json';
    
    var s_user_saved = '<?php echo __("Usuario guardado")?>'; 
    var s_user_deleted = '<?php echo __("Usuario eliminado")?>'; 
    var s_user_active = '<?php echo __("Usuario activado")?>'; 
    var s_dialog2_button_label_confirmation_active_text='<?php echo __("¿Desea activar usuario?")?>'; 
    var s_user_select_text_all = '<?php echo __("Todos")?>'; 
    
    var s_validate_required_fuser_group = '<?php echo __("Grupo requerido")?>';
    var s_validate_required_fuser_office = '<?php echo __("Oficina requerida")?>';
    var s_validate_required_fuser_city = '<?php echo __("Ciudad requerida")?>';
    var s_validate_required_fuser_name = '<?php echo __("Nombre requerido")?>';
    var s_validate_required_fuser_lastname = '<?php echo __("Apellido requerido")?>';
    var s_validate_required_fuser_email = '<?php echo __("Email requerido")?>';
    
    
    var s_validate_required_fuser_username = '<?php echo __("Nombre de usuario requerido")?>';
    var s_validate_required_fuser_username_minlength = '<?php echo __("Nombre de usuario como mínimo 6 caracteres")?>';
    var s_validate_required_fuser_username_maxlength = '<?php echo __("Nombre de usuario como máximo 12 caracteres")?>';
    var s_validate_required_fuser_username_regex = '<?php echo __("Usuario inválido")?>';
    
    var s_validate_required_fuser_password_strength = '<?php echo __("Contrasena muy débil, por favor inserte una contraseña con 6 caracteres, un caracter en mayúscula , números o caracteres especiales")?>';
    var s_validate_required_fuser_password = '<?php echo __("Contraseña requerida")?>';
    var s_validate_required_fuser_password_verify = '<?php echo __("Repita la contraseña")?>';
    var s_validate_required_fuser_password_verify_equal = '<?php echo __("Contraseña no coincide")?>';
        
</script>
<style type="text/css">
    #fuser_password_strength{margin-bottom: 0;}
    #btn_change_pass{cursor: pointer;}
</style>
<div>
    <a href="#modal_user" role="button" class="btn btn-small" data-toggle="modal">
        <i class="icon-plus"></i><?php echo __("Agregar") ?>
    </a>
    <a href="#modal_user_active" role="button" class="btn btn-small" data-toggle="modal">
        <i class="icon-retweet" style="margin-right: 3px;"></i><?php echo __("Activar / Eliminar") ?>
    </a>
</div>
<table id="jqgrid_user"></table>
<div id="jqgrid_user_pager"></div>

<div id="modal_user_active" class="modal hide" data-backdrop="static" style="width:700px" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Activar / Eliminar") ?> <?php echo __("Usuarios") ?></h3>
    </div>
    <div class="modal-body">
        <table id="jqgrid_user_active"></table>
        <div id="jqgrid_user_active_pager"></div>        
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>        
    </div>
</div>
<div id="modal_user" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Usuario") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_user" class="form-horizontal">
            <input id="fuser_id" name="fuser_id" type="hidden"/>
            <div class="row-fluid">    
                <p class="text-center">
                    <img id="user_photo" class="thumbnail" src="http://static.o.bts.mcets-inc.com/kernel/img/profile/128/default.png" style="display: inline-block; height: 128px;"/>
                    <input id="fuser_photo_data" name="fuser_photo_data" type="hidden" />
                </p>
            </div>
            <div id="btnImgUpload">
                <noscript>			
                <p>Please enable JavaScript to use file uploader.</p>
                <!-- or put a simple form for upload here -->
                </noscript>  
            </div>            
            <div class="control-group">
                <label class="control-label" for="fuser_group"><?php echo __("Grupo")?></label>
                <div class="controls">
                    <select id="fuser_group" name="fuser_group">                        
                        <?php foreach($BTS_A_GROUP_LIST as $o_group):?>
                        <option value="<?php echo $o_group->idGroup?>"><?php echo __($o_group->name)?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_office"><?php echo __("Oficina")?></label>
                <div class="controls">
                    <select id="fuser_office" name="fuser_office">                        
                        <?php foreach($BTS_A_OFFICE_LIST as $o_office):?>
                        <option value="<?php echo $o_office->idOffice?>"><?php echo __($o_office->name)?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_username"><?php echo __("Usuario")?></label>
                <div class="controls">
                    <input type="text" id="fuser_username" name="fuser_username" placeholder="<?php echo __("Usuario")?>"/>
                    <a id="btn_change_pass" style="display:none;"><i class="icon-certificate"></i></a>
                    <input type="hidden" id="fuser_change_password" name="fuser_change_password"/>
                </div>
            </div>
            <div class="control-group div_user_password">
                <label class="control-label" for="fuser_password"><?php echo __("Contraseña")?></label>
                <div class="controls">
                    <input type="password" id="fuser_password" name="fuser_password" placeholder="<?php echo __("Contraseña")?>"/>
                </div>
            </div>
            <div class="control-group div_user_password">
                <div class="controls">
                    <div id="fuser_password_strength" class="progress" style="width: 220px;height: 5px;">
                        <div class="bar bar-danger" style="width: 34%;display: none;"></div>
                        <div class="bar bar-warning" style="width: 33%;display: none;"></div>
                        <div class="bar bar-success" style="width: 33%;display: none;"></div>
                    </div>                    
                </div>
            </div>
            <div class="control-group div_user_password">
                <label class="control-label" for="fuser_password_verify"><?php echo __("Verificar contraseña")?></label>
                <div class="controls">
                    <input type="password" id="fuser_password_verify" name="fuser_password_verify" placeholder="<?php echo __("Verificar contraseña")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Opción Root") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fuser_root_option" id="fuser_root_option_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fuser_root_option" id="fuser_root_option_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Apropiarse de Sesión") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fuser_own_session" id="fuser_own_session_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fuser_own_session" id="fuserx_own_session_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <hr/>
            <div class="control-group">
                <label class="control-label" for="fuser_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fuser_name" name="fuser_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_lastname"><?php echo __("Apellido")?></label>
                <div class="controls">
                    <input type="text" id="fuser_lastname" name="fuser_lastname" placeholder="<?php echo __("Apellido")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_address"><?php echo __("Dirección")?></label>
                <div class="controls">
                    <input type="text" id="fuser_address" name="fuser_address" placeholder="<?php echo __("Dirección")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_phone"><?php echo __("Teléfono")?></label>
                <div class="controls">
                    <input type="text" id="fuser_phone" name="fuser_phone" placeholder="<?php echo __("Teléfono")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fuser_email"><?php echo __("Email")?></label>
                <div class="controls">
                    <input type="text" id="fuser_email" name="fuser_email" placeholder="<?php echo __("Email")?>"/>
                </div>
            </div>
            
            
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_user_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>