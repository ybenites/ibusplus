<script type="text/javascript">
    var a_jqgrid_page_col_names = [
        'KEY'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Acciones") ?>'        
    ];
    var a_jqgrid_page_col_model = [
        {name: 'page_key', index: 'page_key', hidden: false, key: true, width: 220, searchoptions: {sopt: ['cn']}},
        {name: 'page_alias', index: 'page_alias', width: 100},        
        {name: 'page_actions', index: 'page_actions', width: 120, sortable: false, align: 'center', search: false}
    ];

    var s_url_page_list = '/private/page/list.json';
    var s_url_page_create = '/private/page/create.json';
    var s_url_page_update = '/private/page/update.json';    
    var s_url_page_get = '/private/page/get.json';
    var s_url_page_delete = '/private/page/delete.json';
    var s_url_page_share = '/private/page/share.json';

    var s_page_shared = '<?php echo __("Controlador Compartido")?>';
    var s_page_saved = '<?php echo __("Pagina Guardada") ?>';
    var s_page_deleted = '<?php echo __("Pagina Eliminada") ?>';
    
    var s_validate_required_fpage_key = '<?php echo __("Key requerido") ?>';
    var s_validate_required_fpage_key_regex = '<?php echo __("Key invalida") ?>';
    var s_validate_required_fpage_alias = '<?php echo __("Alias requerido") ?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';

    var s_page_select_text_all = '<?php echo __("Todos")?>';
</script>
<button href="#modal_page" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</button>
<button id="btn_modal_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small disabled" data-toggle="modal" disabled="disabled">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_page"></table>
<div id="jqgrid_page_pager"></div>
<div id="modal_page" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Pagina") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_page" class="form-horizontal">
            <input id="fpage_id" name="fpage_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fpage_key"><?php echo __("Key")?></label>
                <div class="controls">
                    <input type="text" id="fpage_key" name="fpage_key" placeholder="<?php echo __("Key")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fpage_alias"><?php echo __("Alias")?></label>
                <div class="controls">
                    <input type="text" id="fpage_alias" name="fpage_alias" placeholder="<?php echo __("Alias")?>"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_page_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>
<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>