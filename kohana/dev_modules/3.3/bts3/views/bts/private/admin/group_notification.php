<script type="text/javascript">
    var a_jqgrid_group_notification_col_names = [
        'ID'
        , '<?php echo __("Nombre") ?>'        
        , '<?php echo __("Acciones") ?>'        
    ];
    
    
    var a_jqgrid_group_notification_col_model = [
        {name: 'group_notification_id', index: 'group_notification_id', hidden: true, key: true, width: 220},
        {name: 'group_notification_name', index: 'group_notification_name', width: 350},        
        {name: 'group_notification_actions', index: 'group_notification_actions', width: 250, sortable: false, align: 'center', search: false}
    ];
    
    var a_jqgrid_group_notification_user_list_col_names = [
        'ID'
        , '<?php echo __("Usuario") ?>'
        , '<?php echo __("Acciones") ?>'        
    ];
    
    var a_jqgrid_group_notification_user_list_col_model = [
        {name: 'group_notification_user_list_id', index: 'group_notification_user_list_id', hidden: true, key: true, width: 220},
        {name: 'group_notification_user_list_name', index: 'group_notification_user_list_name', width: 220},        
        {name: 'group_notification_user_list_actions', index: 'group_notification_user_list_actions', width: 120, sortable: false, align: 'center', search: false}
    ];
    
    var s_validate_required_fgroup_notification_name = '<?php echo __("Nombre requerido")?>';
    var s_validate_required_fgroup_notification_user_list_user_id = '<?php echo __("Usuario requerido")?>';

    
    var s_group_notification_saved = '<?php echo __("Grupo Guardada") ?>';
    var s_group_notification_deleted = '<?php echo __("Grupo Eliminada") ?>';

    var s_group_notification_user_list_saved = '<?php echo __("Usuario asignado") ?>';
    var s_group_notification_user_list_deleted = '<?php echo __("Usuario desasignado") ?>';
    
    var s_url_group_notification_list = '/private/groupnotification/list.json';
    var s_url_group_notification_create = '/private/groupnotification/create.json';
    var s_url_group_notification_update = '/private/groupnotification/update.json';
    var s_url_group_notification_get = '/private/groupnotification/get.json';
    var s_url_group_notification_delete = '/private/groupnotification/delete.json';
    
    var s_url_group_notification_user_list_list = '/private/groupnotification/userlist.json';
    var s_url_group_notification_user_list_assign = '/private/groupnotification/assign.json';
    var s_url_group_notification_user_list_unassign = '/private/groupnotification/unassign.json';
   
    var s_group_notification_select_text_all = '<?php echo __("Todos")?>';
</script>

<a href="#modal_group_notification" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<table id="jqgrid_group_notification"></table>
<div id="jqgrid_group_notification_pager"></div>

<div id="modal_group_notification" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Grupo") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_group_notification" class="form-horizontal">
            <input id="fgroup_notification_id" name="fgroup_notification_id" type="hidden"/>            
            <div class="control-group">
                <label class="control-label" for="fgroup_notification_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fgroup_notification_name" name="fgroup_notification_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_group_notification_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_group_notification_user_list" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Usuarios") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_group_notification_user_list" class="form-horizontal">            
            <input type="hidden" name="fgroup_notification_user_list_group_id" id="fgroup_notification_user_list_group_id"/>
            <div class="control-group">
                <label class="control-label" for="fgroup_notification_user_id"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <select id="fgroup_notification_user_list_user_id" name="fgroup_notification_user_list_user_id">                        
                        <?php foreach($A_BTS_USER_LIST as $o_user):?>
                        <option value="<?php echo $o_user->idUser?>"><?php echo $o_user->userName?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <a id="modal_group_notification_user_list_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
                </div>
            </div>
        </form>
        
        <table id="jqgrid_group_notification_user_list"></table>
        <div id="jqgrid_group_notification_user_list_pager"></div>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
    </div>
</div>