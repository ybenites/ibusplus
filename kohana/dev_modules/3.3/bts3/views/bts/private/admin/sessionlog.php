
<script type="text/javascript">
    // URL 
    var s_url_sessionlog_list = '/private/sessionlog/list.json';
    // VARIABLES PHP
    var jquery_date_format = '<?php echo $jquery_date_format; ?>';
    var sql_date_format = '<?php echo $sql_date_format; ?>';
    var input_picker='';
    var a_jqgrid_sessionlog_col_names = [
        'Id',
        '<?php echo __("Evento") ?>',
        '<?php echo __("Usuario") ?>',
        '<?php echo __("Ip") ?>',
        '<?php echo __("Agente") ?>',
        '<?php echo __("Fecha Hora Evento") ?>'
    ];
    var a_jqgrid_sessionlog_col_model = [
        {name: 'sessionlog_id', index: 'sessionlog_id', key: true, hidden: true},
        {name: 'sessionlog_event', index: 'sessionlog_event', width: 100, search: false},
        {name: 'sessionlog_user', index: 'sessionlog_user', width: 120, searchoptions: {sopt: ['cn']}},
        {name: 'sessionlog_ip', index: 'sessionlog_ip', width: 100},
        {name: 'sessionlog_agent', index: 'sessionlog_agent', width: 300, search: false},
        {name: 'sessionlog_eventDate', index: 'sessionlog_eventDate', width: 130,search:true,stype:'text', searchoptions:{dataInit:function(elem){console.log(elem);input_picker=elem;}}}
    ];

    var s_event_log_in = '<?php echo Kohana_BtsConstants::BTS_EVENT_LOG_IN?>';
    var s_event_log_out = '<?php echo Kohana_BtsConstants::BTS_EVENT_LOG_OUT?>';
    
</script>
<style type="text/css">
    .status_off{background-color: rgba(255,0,0,.35)}
    .status_on{background-color: rgba(0,255,0,.35)}
</style>



<div class="form-inline">
    <label class="control-label"><?php echo __("Fecha"); ?>:</label>
    <div class="input-append date form_datetime" id="fdate_sessionlog_search" data-date="<?php echo $today; ?>" data-date-format="<?php echo $jquery_date_format ?>" >
      <input size="13" type="text" value="<?php echo $today; ?>">
      <span class="add-on"><i class="icon-th"></i></span>
    </div>
</div>
<table id="jqgrid_sessionlog"></table>
<div id="jqgrid_sessionlog_pager"></div>
