<script type="text/javascript">
    var a_jqgrid_menu_col_names = [
        'ID'
                , '<?php echo __("Nombre") ?>'
                , '<?php echo __("Ruta") ?>'
                , '<?php echo __("Icono") ?>'                
                , 'rtype'
                , '<?php echo __("Tipo") ?>'
                , '<?php echo __("Acciones") ?>'
    ];
    
    var a_jqgrid_menu_col_model = [
        {name: 'idMenu', index: 'idMenu', width: 1, hidden: true, key: true},
        {name: 'name', index: 'name', width: 250},
        {name: 'url', index: 'url', width: 250},
        {name: 'ico', index: 'ico', width: 50, align: 'center'},
        {name: 'rtype', index: 'rtype', width: 50, hidden: true},
        {name: 'type', index: 'type', width: 50, align: 'center'},
        {name: 'actions', index: 'actions', width: 160, sortable: false, align: 'center'}
    ];
    
    var s_url_menu = '/private/menu/list.xml?nodeid=0&n_level=0';
    var s_url_menu_create = '/private/menu/create.json';
    var s_url_menu_update = '/private/menu/update.json';
    var s_url_menu_get = '/private/menu/get.json';
    var s_url_menu_delete = '/private/menu/delete.json';
    var s_url_menu_list_order = '/private/menu/listOrder.json';
    var s_url_menu_save_order = '/private/menu/saveOrder.json';
    var s_url_menu_shorcut = '/private/menu/getUploadImageMenu.json';
    var s_url_menu_order_level = '/private/menu/updateOrderLevel.json';
    var s_url_menu_share = '/private/menu/share.json';

    var s_place_holder_module = "<?php echo __('Seleccionar Modulo')?>";
    var s_place_holder_option = "<?php echo __('Seleccionar Opcion')?>";
    var s_place_holder_menu_type = "<?php echo __('Seleccionar Tipo')?>";
    var s_place_holder_icon = "<?php echo __('Seleccionar Icono')?>";
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
    var s_menu_shared = '<?php echo __("Menú Compartido")?>';
    
 
    var s_menu_saved = '<?php echo __("Menu Guardada") ?>';
    var s_menu_deleted = '<?php echo __("Menu Eliminada") ?>';
    var s_menu_order_saved = '<?php echo __("Orden de Menu Guardada") ?>';
    var s_menu_order_level_confirm = '<?php echo __("¿Desea mover el menú [S] al menú [D]?"); ?>';
    var s_menu_order_level_deneg = '<?php echo __("No puede mover a este Menu"); ?>';
    var s_menu_select_destionation = '<?php echo __("Seleccione el menú de destino"); ?>';
    var s_menu_select_first_level = '<?php echo __(" al primer nivel"); ?>';
    var s_menu_cancel_first_level = '<?php echo __("Cerrar") ?>';
    var s_menu_change_level = '<?php echo __("Cambio de Nivel"); ?>';
    
    var s_validate_required_fmenu_type = '<?php echo __("Tipo requerido") ?>';
    var s_validate_required_fmenu_name = '<?php echo __("Nombre requerido") ?>';
    var s_validate_required_fmenu_url = '<?php echo __("URL requerido") ?>';
    var s_validate_required_fmenu_option_type_string = '<?php echo __("Valor requerido") ?>';
    var s_validate_required_fmenu_option_type_integer = '<?php echo __("Valor requerido") ?>';
    var s_validate_required_fmenu_option_type_color = '<?php echo __("Color requerido") ?>';
    
    var s_option_type_boolean = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_BOOLEAN ?>';
    var s_option_type_string = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_STRING ?>';
    var s_option_type_color = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_COLOR ?>';
    var s_option_type_integer = '<?php echo Kohana_BtsConstants::BTS_OPTION_TYPE_INTEGER ?>';
    
    var s_menu_type_item = '<?php echo Kohana_BtsConstants::BTS_MENU_TYPE_ITEM ?>';
    var s_menu_type_separator = '<?php echo Kohana_BtsConstants::BTS_MENU_TYPE_SEPARATOR ?>';
    var s_menu_type_header = '<?php echo Kohana_BtsConstants::BTS_MENU_TYPE_HEADER ?>';
    var s_menu_img_shorcut = '<?php echo __("Subir Imagen") ?>';
    var i_change_level = 0;
    var BTS_SKIN='<?php echo $BTS_SKIN; ?>';
</script>
<style type="text/css">
    #popover_menu{
        display: none;
        position: absolute;
        top: 24%;
        left: 76%;
    }
    #popover_menu_btn_save_order_level{margin-right: 5px;}
</style>

<a href="#modal_menu" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_translate_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_menu"></table>
<div id="jqgrid_menu_pager"></div>

<div id="modal_menu_sort" class="modal hide" data-keyboard="false" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Ordenar") ?></h3>
    </div>
    <div class="modal-body">
        <ul id="menu_sort_list" class="unstyled">
            
        </ul>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_menu_sort_btn_ok" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>

<div id="modal_menu" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Menu") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_menu" class="form-horizontal">
            <input type="hidden" name="fmenu_id" id="fmenu_id"/>
            <input type="hidden" name="fmenu_super_menu" id="fmenu_super_menu"/>
            <div class="control-group">
                <label class="control-label" for="fmenu_type"><?php echo __("Tipo")?></label>
                <div class="controls">
                    <select id="fmenu_type" name="fmenu_type">                        
                        <?php foreach($BTS_A_MENU_TYPES as $a_menu_type):?>
                        <option value="<?php echo $a_menu_type['value']?>"><?php echo __($a_menu_type['display'])?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group fmenu_type fmenu_type_item fmenu_type_header">
                <label class="control-label" for="fmenu_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fmenu_name" name="fmenu_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
            <div class="control-group fmenu_type fmenu_type_item">
                <label class="control-label" for="fmenu_url"><?php echo __("URL")?></label>
                <div class="controls">
                    <input type="text" id="fmenu_url" name="fmenu_url" placeholder="<?php echo __("URL")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fmenu_module"><?php echo __("Modulo")?></label>
                <div class="controls">
                    <select id="fmenu_module" name="fmenu_module">
                        <option></option>
                        <?php foreach($BTS_A_MODULES as $o_module):?>
                        <option value="<?php echo $o_module->idModule?>"><?php echo __($o_module->name)?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>            
            <div class="control-group">
                <label class="control-label" for="fmenu_option"><?php echo __("Opcion")?></label>
                <div class="controls">
                    <select id="fmenu_option" name="fmenu_option">
                        <option></option>
                        <?php foreach($BTS_A_OPTIONS_LIST as $o_option):?>
                        <option data-data_type="<?php echo $o_option->dataType?>" value="<?php echo $o_option->key?>"><?php echo $o_option->key?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group fmenu_option_type fmenu_option_type_boolean hide">
                <label class="control-label"><?php echo __("Boolean") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fmenu_option_type_boolean" id="fmenu_option_type_boolean_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fmenu_option_type_boolean" id="fmenu_option_type_boolean_0" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group fmenu_option_type fmenu_option_type_string hide">
                <label class="control-label" for="fmenu_option_type_string"><?php echo __("String") ?></label>
                <div class="controls">
                    <input type="text" id="fmenu_option_type_string" name="fmenu_option_type_string" placeholder="<?php echo __("String") ?>" required/>
                </div>
            </div>            
            <div class="control-group fmenu_option_type fmenu_option_type_integer hide">
                <label class="control-label" for="fmenu_option_type_integer"><?php echo __("Integer") ?></label>
                <div class="controls">
                    <input type="text" id="fmenu_option_type_integer" name="fmenu_option_type_integer" placeholder="<?php echo __("Integer") ?>" required/>
                </div>
            </div>
            <div class="control-group fmenu_option_type fmenu_option_type_color hide">
                <label class="control-label" for="fmenu_option_type_color"><?php echo __("Color") ?></label>
                <div class="controls">
                    <input type="text" id="fmenu_option_type_color" name="fmenu_option_type_color" placeholder="<?php echo __("Color") ?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fmenu_icon"><?php echo __("Icono")?></label>
                <div class="controls">
                    <select id="fmenu_icon" name="fmenu_icon">
                        <option></option>
                        <?php foreach($BTS_A_ICONS_BOOTSTRAP as $a_icon):?>
                        <option value="<?php echo $a_icon['value']?>"><?php echo $a_icon['display']?></option>
                        <?php endforeach?>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><?php echo __("Root") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fmenu_isroot" id="fmenu_isroot_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fmenu_isroot" id="fmenu_isroot_2" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="shourtcut">
                <label class="control-label"><?php echo __("Atajo") ?></label>
                <div class="controls">
                    <label class="radio">
                        <input type="radio" name="fmenu_shortcut" id="fmenu_shortcut_1" value="1" />
                        <?php echo __("SI") ?>
                    </label>
                    <label class="radio">
                        <input type="radio" name="fmenu_shortcut" id="fmenu_shortcut_2" value="0" checked/>
                        <?php echo __("NO") ?>
                    </label>
                </div>
            </div>
            <div class="control-group div_add_img hide">
                <div id="btn_add_img">                    
                </div>
            </div>
            <div class="control-group tumbail_shorcut hide">   
                <ul class="thumbnails">
                    <li class="span2 controls">
                        <a href="#" class="thumbnail">
                            <input type="hidden" id="fmenu_hide_image" name="fmenu_hide_image" > 
                            <img data-src="holder.js/160x120" alt="" style="width: 160px; height: 120px;" src="/tmp/1365542254.png" >
                        </a>
                    </li>                    
                </ul>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_menu_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>


<div id="popover_menu">
<input type="hidden" name="hmenu_level_source" id="hmenu_level_source"/>
<input type="hidden" name="hmenu_level_destination" id="hmenu_level_destination"/>
    <a href="#" id="popover_menu_order_level" rel="popover" ></a>
</div>

<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>