<script type="text/javascript">
var a_jqgrid_translate_col_names = [
        '<?php echo __("ID") ?>',
        '<?php echo __("Texto") ?>',
        '<?php echo __("Texto a Traducir") ?>',
        '<?php echo __("Estado") ?>'
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_translate_col_model = [
        {name: 'idTranslate', index: 'idTranslate', hidden:true,key: true, search:true, width:100},
        {name: 'text', index: 'text',search:true, width: 200},
        {name: 'textTranslate', index: 'textTranslate',search:false, width: 300},
        {name: 'status', index: 'status', search:false,width: 100},
        {name: 'actions', index: 'actions', width: 160, search:false, sortable: false, align: 'center'}
    ];


    var s_url_translate_list = '/private/translate/list.json'
    var s_url_translate_create = '/private/translate/createTranslate.json'
    var s_url_translate_change_status = '/private/translate/changeStatus.json'
    var s_url_translate_update = '/private/translate/updateTranslate.json'
    var s_url_translate_get ='/private/translate/getTranslate.json';
    var s_url_translate_share = '/private/translate/share.json';
    
    var s_controller_select_text_all = '<?php echo __("Todos")?>';
    var s_validate_required_textT = '<?php echo __("El texto es requerido")?>';
    var s_validate_required_textTranslate = '<?php echo __("El texto a traducir es requerido")?>';
    var s_translate_saved='<?php echo __("Listo")?>';
    var s_validate_required_fshare_hosts = '<?php echo __("Sitio Requerido")?>';
    var s_translate_shared = '<?php echo __("Traducciones Compartidas")?>';
</script>

<a href="#modal_save_translate" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-plus"></i><?php echo __("Agregar") ?>
</a>
<button id="btn_translate_share_hosts_share" href="#modal_share_hosts" role="button" class="btn btn-small" data-toggle="modal">
    <i class="icon-share"></i><?php echo __("Compartir") ?>
</button>
<table id="jqgrid_translate"></table>
<div id="jqgrid_translate_pager"></div>

<div id="modal_save_translate" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4><?php echo __("Nueva Traduccion") ?></h4>
    </div>        
        <div class="modal-body">
            <form id="form_translate" class="form-horizontal">
             <input type="hidden" name="idTranslate" id="idTranslate">   
            <div class="control-group">
                <label class="control-label"><b><?php echo __("Texto:") ?></b></label>
                    <div class="controls">
                        <textarea name="textT" id="textT" rows="5"> </textarea>
                    </div>
            </div>
            <div class="control-group">
                <b><label class="control-label"><b><?php echo __("Texto a Traducir:") ?></b></label></b>
                    <div class="controls">
                        <textarea name="textTranslate" id="textTranslate" rows="5"> </textarea>
                    </div>
            </div>    
            </form> 
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
            <button id="modal_bt_translate" type="submit" class="btn btn-primary"><?php echo __("Aceptar") ?></button>
        </div>
      
    
    
</div>

<div id="modal_share_hosts" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Compartir") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_share_hosts" class="form-horizontal">
            <div class="control-group">
                <label class="control-label"><?php echo __("Sitios") ?></label>
                <div class="controls">
                    <?php foreach($BTS_A_DB_CONFIGS as $a_config):?>
                    <label class="checkbox">
                        <input checked type="checkbox" name="fshare_hosts[]" id="fshare_hotst_<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" value="<?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE]?>" />
                        <?php echo $a_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SITE_NAME]?>
                    </label>
                    <?php endforeach?>
                </div>
            </div>
        </form>        
    </div>
     <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_share_hosts_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>