<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $SYSTEM_TITLE ?></title>
        <meta name="description" content="<?php echo $SYSTEM_DESCRIPTION ?>">
        <meta name="viewport" content="width=device-width">        
       <?php        
        $siteUnique= SITE_DOMAIN . Session::instance('database')->get('USER_NAME');
        $dsproject = $SYSTEM_CONFIG->get(Kohana_BtsConstants::BTS_SYS_CONFIG_STATIC_WEBSITE);
        $siteUniqueUrl = Request::initial()->controller().Request::initial()->action();
        if (!Fragment::load($siteUnique.$siteUniqueUrl . 'admin_header_css_vars', 28800, true)) { ?>
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/ui/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION.$SYSTEM_DEV_EXT_FILE; ?>.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION.$SYSTEM_DEV_EXT_FILE; ?>.css">    
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION .$SYSTEM_DEV_EXT_FILE; ?>.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/main<?php echo $SYSTEM_DEV_EXT_FILE; ?>.css">        
        <?php foreach ($SYSTEM_A_STYLES as $a_style): ?>
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com<?php echo $a_style['file_name'] ?>">
        <?php endforeach ?>
        <?php foreach ($SYSTEM_A_PROJ_STYLES as $a_proj_style): ?>
        <link rel="stylesheet" href="<?php echo $dsproject.$a_proj_style['file_name'] ?>">
        <?php endforeach ?>
            <?php if ($b_BTS_CHAT){ ?>
        <link type="text/css" rel="stylesheet" media="all" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/chat.css" />
        <?php } ?>
        <?php  Fragment::save(); } ?>
    </head>
    <body>
        
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-inverse navbar-fixed-top nav_menu" style="<?php echo ($USER_FULLSCREEN ? 'display:none;position:inherit;' : '') ?>">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </a>
                    <a class="brand" href="<?php echo $BTS_SESSION_DEFAULT_HOME;  ?>"><?php echo $SYSTEM_TITLE ?></a>
                    <div class="nav-collapse collapse">
                        <ul class="nav">
                            <?php
                            if (!Fragment::load($siteUnique . 'admin_header_menu', 28800, true)) {
                                echo HelperMenu::fnGetMenu($SYSTEM_A_MENU, array(), $BTS_A_ACCESS, $BTS_A_SYSTEM_OPTIONS);
                                Fragment::save();
                            }
                            ?>
                        </ul>
                        <ul class="nav pull-right menu_right">
                            <li class="open">                           
                                <a id="user_info" role="button">
                                    <b class="icon-user"></b>
                                    <?php echo $BTS_USER_NAME ?>
                                </a>
                            </li>
                            <?php if (HelperNotification::fnGetAllowNotificationsShow()): ?>
                                <?php if (HelperNotification::fnGetAllowNotifications()): ?>
                                    <li class="open">
                                        <a id="bts_write_notification" href="#write_notification">
                                            <b class="icon-edit"></b>                                    
                                        </a>
                                    </li>
                                <?php endif ?>
                                <li>
                                    <a id="bts_view_notification" href="#notification">
                                        <b class="icon-bullhorn"></b>
                                        <span class="notification label label-info hidden">0</span>
                                    </a>
                                    <audio id="sound_notification" src="http://static.js.bts.mcets-inc.com/kernel/sounds/ChatIncomingInitial.wav"></audio>
                                    <div id="menu_cont_notification">
                                        <div id="notification_response_view_div">
                                            <div class="notification_response_view_shadow ui-corner-all"></div>
                                            <div class="notification_response_view_cont padding10">                                
                                                <div class="notification_response_view_inner ui-corner-all">
                                                    <div class="padding10">
                                                        <div class="notification_response_view_loading ui-corner-all"></div>
                                                        <h4></h4>
                                                        <p></p>
                                                        <span class="r_view_date"></span>
                                                    </div>
                                                </div>                                                             
                                            </div>
                                        </div>

                                        <?php $a_user_notifications = HelperNotification::fnGetLastNotifications(); ?>
                                        <?php $i_last_notification = 0; ?>
                                        <?php if (array_key_exists(0, $a_user_notifications)): ?>
                                            <?php $i_last_notification = $a_user_notifications[0]['updateNotification']; ?>
                                        <?php endif ?>                                    
                                        <ul id="ulNotification" class="none">                        
                                            <?php foreach (HelperNotification::fnGetLastNotifications() as $i_count => $a_notification): ?>                        
                                                <?php if (is_numeric($i_count)): ?>
                                                    <li id="message_id_<?php echo $a_notification['idOut'] ?>" unix_time="<?php echo $a_notification['updateNotification'] ?>" class="old_notification notification li_message">
                                                        <div class="div_img_action_msg<?php echo ($a_notification['responseOut'] > 0 ? ' div_img_action_response' : '') ?>"></div>
                                                        <div class="div_notification">
                                                            <div class="general_notification_image ui-corner-all ui-widget-header" style="background-size: 100%; background-image: url('<?php echo $a_notification['user_image'] ?>');background-position:center center; background-repeat:no-repeat;">

                                                            </div>
                                                            <div class="general_notification_content">
                                                                <div class="row_notification">
                                                                    <span class="user_name">
                                                                        <?php echo $a_notification['userRegistrationOut'] ?>
                                                                    </span> 
                                                                    <span class="message_date">
                                                                        <?php echo $a_notification['dateCreationOut'] ?>
                                                                    </span>
                                                                </div>
                                                                <div class="row_notification">
                                                                    <div class="notification_text">
                                                                        <?php echo $a_notification['messageOut'] ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="general_notification_footer">           
                                                                <span class="general_time"></span>
                                                                <?php echo __("Responder") ?>
                                                                    </span>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endif; ?>
                                            <?php endforeach; ?>                            
                                            <li <?php echo (HelperNotification::fnGetCountPreviousNotification(0, $i_last_notification) == 0) ? "class='hidden'" : '' ?>>
                                                <div class="more_notification">                                    
                                                    [<?php echo HelperNotification::fnGetCountPreviousNotification(0, $i_last_notification) ?>] <?php echo __("Mensajes Anteriores") ?>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            <?php endif ?>
                            <li class="open">
                                <a class="btn_logout" href="/public/login/logout.html" onclick="disconnectUser()">
                                    <b class="icon-off"></b>
                                </a>
                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="navbar navbar-fixed-top navbar_shortcuts" style="<?php echo ($USER_FULLSCREEN ? 'position:inherit;' : '') ?>">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav shortcuts" style="padding-top: 4px;">
                        <?php
                        if (!Fragment::load($siteUnique. 'admin_header_menu_shortcuts', 28800, true)) {
                            echo HelperMenu::fnGetShorCuts();
                            Fragment::save();
                        }
                        ?>
                        <a href="https://mcets.zendesk.com/home" target="__blank"><img src="/kernel/img/bts/shortcuts/callcenter.png" width="32" height="32"  title="<?php echo __("Servicio al Cliente"); ?>" /></a>
                    </ul>
                    <ul id="shortcutsRoot">
                        <a href="#" id="btnactivateDeactivatedSystem" class="icon-lock" title="<?php echo __("Activar/Desactivar Sistema"); ?>" ></a>
                        <a href="#" id="btncleaningSessionUsers" class="icon-user" title="<?php echo __("Limpiar Sesion Usuarios"); ?>" ></a>
                        <a href="#" id="btnRegenerateLanguages" class="icon-flag" title="<?php echo __("Limpiar Lenguajes"); ?>" ></a>
                        <a href="#" id="btnCleaningCache" class="icon-refresh" title="<?php echo __("Limpiar Cache de Empresa"); ?>" ></a>
                        <?php if ($BTS_OPTION_LANGUAGE == Kohana_BtsConstants::BTS_STATUS_ACTIVE){  ?><a href="#" id="btnChangeLanguage" data-placement="bottom" data-toggle="popover" class="icon-globe" title="<?php echo __("Cambiar Idioma"); ?>" ></a> <?php }?>
                    </ul>
                </div>
            </div>
        </div>
          <div class="padding_top_body" style="<?php echo ($USER_FULLSCREEN ? 'margin-top: 5px;' : '') ?>"></div>
           <div class="container">
            <?php echo $SYSTEM_BREADCRUMBS ?>            
            <?php
            $cacheSessionDynamic = $siteUnique.$siteUniqueUrl;
            if (Kohana::$environment == Kohana::DEVELOPMENT) {
                echo $SYSTEM_BODY;
            } elseif (Kohana::$environment == Kohana::PRODUCTION) {
                if ((!Fragment::load($cacheSessionDynamic, 28800, true))) {
                    echo $SYSTEM_BODY;
                    Fragment::save();
                }
            }
            ?>
            <div id="loading" class="modal hide" title="<?php echo __("Cargando"); ?>">
                <div class="modal-header">                
                    <h5><?php echo __("Cargando...") ?></h5>
                </div>
                <div class="modal-body">
                    <p align="center">
                        <?php echo __("Por favor, espere un momento..."); ?><br/><br/>
                        <img alt="Cargando..." src="http://static.o.bts.mcets-inc.com/kernel/img/ajax-loader.gif" title="<?php echo __("Cargando"); ?>"/>
                    </p>
                </div>
            </div>
        </div> <!-- /container -->    
        <script type="text/javascript">
            var rParams = {};
            rParams.sid = '<? echo $BTS_SESSION_ID;?>';
        </script>
        <?php if (!Fragment::load($siteUnique . 'admin_header_js_vars', 28800, true)) { ?>
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_VERSION ?>.min.js"></script>        
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION ?>.min.js"></script>
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION ?>.min.js"></script>
        <!--Loading Scripts--> 
        <script type="text/javascript">
            var ajax_response_format_default = '<?php echo $SYSTEM_AJAX_DATA_TYPE ?>';
            <?php include_once DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'jquery_global.js' //Falta cambiar por DEV MOD PATH     ?>
            var jq_jsonp_callback = '<?php echo ($SYSTEM_AJAX_DATA_TYPE == 'jsonp' ? '?' . $SYSTEM_JSONP_CALL_BACK . '=?' : '') ?>';
            var jq_json_code_success = '<?php echo Kohana_BtsConstants::BTS_CODE_SUCCESS ?>';
            var jq_json_code_error = '<?php echo Kohana_BtsConstants::BTS_CODE_ERROR ?>';
            var showloadingDialog = 0;
            // Lenguaje
            var l_spanish = "<?php echo __("Español") ?>";
            var l_english = "<?php echo __("Ingles") ?>";
            var cacheUsers = {}, lastXhrUsers = null;
            var requestUsers = 0;
            var s_not_result_found = '<?php echo __("Sin Resultados"); ?>';
            var b_available_more_notification_ajax = true;
            var n_page = 1;
            var i_last_notification = <?php echo $i_last_notification; ?>;
            var s_preview_message = '<?php echo __("Mensajes Anteriores") ?>';
            var s_activateDeactivatedSystem = '<?php echo __("Activar/Desactivar Sistema") ?>';
            var s_deactivated = '<?php echo __("Desactivar") ?>';
            var s_activated = '<?php echo __("Activar") ?>';
            var s_now = '<?php echo __('ahora') ?>';
            var s_seconds = '<?php echo __("segundos") ?>';
            var s_ago= '<?php echo __('hace') ?>';
            var s_hours= '<?php echo __('horas') ?>';
            var s_hour= '<?php echo __('hora') ?>';
            var s_minutes= '<?php echo __('minutos') ?>';
            var s_minute= '<?php echo __('minuto') ?>';
            var b_fnGetAllowNotifications = Boolean(eval('<?php echo HelperNotification::fnGetAllowNotifications(); ?>'));
            var b_fnGetAllowNotificationsShow = Boolean(eval('<?php echo HelperNotification::fnGetAllowNotificationsShow(); ?>'));
            var d_SYSTEM_TIME_REFRESH_MESSAGES = <?php echo $SYSTEM_TIME_REFRESH_MESSAGES; ?>;
            var lastNotification = '<?php echo HelperNotification::fnGetLastDateNotification() ?>';
            var s_mes_en = "<?php echo __("Ene") ?>";
            var s_mes_feb =  "<?php echo __("Feb") ?>";
            var s_mes_mar =  "<?php echo __("Mar") ?>";
            var s_mes_abr = "<?php echo __("Abr") ?>";
            var s_mes_may = "<?php echo __("May") ?>";
            var s_mes_jun = "<?php echo __("Jun") ?>";
            var s_mes_jul = "<?php echo __("Jul") ?>";
            var s_mes_ago = "<?php echo __("Ago") ?>";
            var s_mes_set = "<?php echo __("Set") ?>";
            var s_mes_oct = "<?php echo __("Oct") ?>";
            var s_mes_nov = "<?php echo __("Nov") ?>";
            var s_mes_dic = "<?php echo __("Dic") ?>";
            <?php $d_current_date = HelperNotification::fnGetCurrentDate(); ?>
            var t_current_unix_date = <?php echo $d_current_date['current_unix_date'] ?>;
            var s_dialog2_button_label_prompt_title = '<?php echo __("Aviso") ?>';
            var s_dialog2_button_label_alert_title = '<?php echo __("Alerta") ?>';
            var s_dialog2_button_label_confirmation_title = '<?php echo __("Confirmacion") ?>';
            var s_dialog2_button_label_ok = '<?php echo __("Aceptar") ?>';
            var s_dialog2_button_label_cancel = '<?php echo __("Cancelar") ?>';
            var s_dialog2_button_label_yes = '<?php echo __("Si") ?>';
            var s_dialog2_button_label_no = '<?php echo __("No") ?>';
            var s_proccesOk = '<?php echo __("Proceso Satisfactorio") ?>';
            var s_dialog2_button_label_confirmation_delete_text = '<?php echo __("Desea eliminar el registro seleccionado?") ?>';
            var s_dialog2_button_label_confirmation_activate_text = '<?php echo __("¿Desea activar el registro seleccionado?") ?>';
            var s_support_send_email_click = '<?php echo __("Por favor contacte con el grupo de soporte haciendo clic en la imagen siguiente") ?>';
            var s_support_send_email_alt = '<?php echo __("Enviar email al grupo de soporte") ?>';
            var s_support_send_email_confirm = '<?php echo __("El email se ha enviado al grupo de soporte del sistema") ?>';        
            var s_url_profile_view = '/private/profile/loadData';
            var s_url_profile_update = '/private/profile/updateUser';
            var s_change_pass = '<?php echo __("Contraseña modificada") ?>';
            var s_edit_Office = '<?php echo __("Guardado") ?>';
            var s_mesage_equalTo = '<?php echo __("Por favor ingrese contraseña otra vez") ?>';
            var s_mesage_req_new = '<?php echo __("La confirmacion de contraseña es requerida") ?>';
            var s_mesage_req_pass = '<?php echo __("La contraseña nueva es requerida") ?>';
            var s_mesage_req_now = '<?php echo __("La contraseña actual es requerida") ?>';
            var s_menu_img_shorcut = '<?php echo __("Subir Imagen") ?>';
            var s_url_profile_shorcut = '/private/profile/getUploadImageMenu.json';
            var idleTime = eval('<?php echo $SYSTEM_CONFIG->get(Kohana_BtsConstants::BTS_SYS_CONFIG_SESSION_USER_LIFE_TIME); ?>') * 1000;
            var s_domain = "<? echo SITE_DOMAIN;?>";
            var s_jsonp_callback = '?jsoncallback=?';
            rParams.qlang = '<? echo $BTS_SESSION_LANG;?>';
            rParams.qdf = '<? echo $BTS_SESSION_DATE_FORMAT;?>';
            var websocket='';
            var socket_chat='';
            var b_BTS_CHAT='<?php echo $b_BTS_CHAT ?>';  
        </script>
        <?php Fragment::save(); } 
        if (!Fragment::load($siteUnique.$siteUniqueUrl . 'admin_header_js_vars_end', 28800, true)) {            
        foreach ($SYSTEM_A_SCRIPTS as $a_script): ?>
        <script type="text/javascript" src="http://static.js.bts.mcets-inc.com<?php echo $a_script['file_name'];?>"></script>
        <?php endforeach ?>
        <?php         
        foreach ($SYSTEM_A_PROJ_SCRIPTS as $a_proj_script):?>
        <script type="text/javascript" src="<?php echo $dsproject.$a_proj_script['file_name'];?>"></script>
        <?php endforeach ?>
            <?php if ($b_BTS_CHAT){ ?>
        <script src="http://192.168.0.100:6969/socket.io/socket.io.js"></script>
        <script type="text/javascript">
            websocket = io.connect("http://192.168.0.100:6969/"+s_domain);
            socket_chat = io.connect("http://192.168.0.100:6969/"+'<?php echo $BTS_USER_NAME ?>'+s_domain);
            websocket.emit('userConect',{u:'<?php echo $BTS_USER_NAME ?>',dom:s_domain});
        </script>            
        <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/chat.js"></script>
        <?php } ?>
        <script type="text/javascript">
        <?php include_once DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js_after_scripts.js' ?>
        </script>        
        <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/jquery.ibusplus.sessionTimeout.js"></script>
        <div id="footer">
            <div class="container">
                <em class="text-left muted credit" style="float: left;"><?php echo $BTS_SESSION_CITY_NAME ?> // <?php echo $BTS_SESSION_OFFICE_NAME; ?> // <?php echo $BTS_GROUP_NAME; ?> // <?php echo $BTS_USER_FULL_NAME; ?></em>
                <strong class="text-right muted credit" style="float: right;"><?php echo __('IBUSPLUS &reg; Todos los derechos Reservados. &copy; 2013'); ?></strong>
            </div>
        </div>
        <div id="modal_session_timeout" class="modal hide" data-backdrop="static">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3><?php echo __("Su sesión va a expirar...") ?></h3>
            </div>
            <div class="modal-body">
                <p>
                    <?php echo __('Su sesión está apunto de terminar automáticamente en'); ?>&nbsp;<b id="sto_counter">60</b>&nbsp;<?php echo __('segundos'); ?>.<br/>
                    <?php echo __('Para mantener activa su sesión, presione el botón CONTINUAR o presione EXPIRAR para terminar su sesión'); ?>.
                </p>
            </div>
            <div class="modal-footer">
                <button id="btn_sto_expires" class="btn"><?php echo __('Expirar') ?></button>
                <button id="btn_sto_continue" class="btn btn-success"><?php echo __('Continuar') ?></button>
            </div>
        </div>
        <a accesskey="m" onclick="changeUserWindow()" class="none"></a>
        <?php Fragment::save(); } ?>
        <div id="div_profile_after"></div>
        <div id="div_notifications_after"></div>
    </body>
</html>