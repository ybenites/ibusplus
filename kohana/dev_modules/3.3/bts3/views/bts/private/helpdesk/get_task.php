<script type="text/javascript">
    var task_create_date="<?php echo $a_task['task_create_date'] ?>";    
    var s_validate_required_ftask_coment="<?php echo __('Ingresar Comentario') ?>";  
    var s_url_create_task_comment='/private/Gettask/create.json';
    var s_ticket_time_horas=" <?php echo __('horas') ?> ";
    var s_ticket_time_minutos=" <?php echo __('minutos') ?> ";
    var s_ticket_time_segundos=" <?php echo __('segundos') ?> ";
    var s_ticket_time="<?php echo __('hace') ?> ";
    var month_names = new Array( );
                month_names[month_names.length] = "<?php echo __("Ene") ?>";
                month_names[month_names.length] = "<?php echo __("Feb") ?>";
                month_names[month_names.length] = "<?php echo __("Mar") ?>";
                month_names[month_names.length] = "<?php echo __("Abr") ?>";
                month_names[month_names.length] = "<?php echo __("May") ?>";
                month_names[month_names.length] = "<?php echo __("Jun") ?>";
                month_names[month_names.length] = "<?php echo __("Jul") ?>";
                month_names[month_names.length] = "<?php echo __("Ago") ?>";
                month_names[month_names.length] = "<?php echo __("Set") ?>";
                month_names[month_names.length] = "<?php echo __("Oct") ?>";
                month_names[month_names.length] = "<?php echo __("Nov") ?>";
                month_names[month_names.length] = "<?php echo __("Dic") ?>";
</script>
<style type="text/css">    
    .class_answer{font-size:200%;display: block;text-align:center}    
    .task_center{float: none;margin-left: auto;margin-right: auto}
</style>
<div class="row">
    <div class="task_center span10 control-group controls-row row label label-info padding5" style="margin-top: 10px">
        <span class="span"><b><?php echo __("Ticket")?> # <?php echo $a_task['task_code'] ?></b></span>
    </div>
    <div id="div_task_coment" class="task_center span10 control-group controls-row row" style="margin-top: 10px">
        <div class="span2">
            <div class="label label-important  padding5">
                <span class="class_answer">
                    <?php echo $a_task['task_total_comment']; ?>
                </span>
                <?php echo __("respuestas")?>
            </div>            
        </div>
        <div class="span3">
            <div class="control-group">                     
                <span class="text-info"><b><?php echo $a_task['task_title'] ?></b></span>
            </div>                        
            <div class="control-group">
                <span><?php echo $a_task['task_description'] ?></span>
            </div>
            <div>
                <?php if(count($a_task['task_tags'])>0): ?>
                    <?php foreach ($a_task['task_tags'] as $v) : ?>
                        <span class="label label-info" data-id="<?php echo $v['tag_id'] ?>"><?php echo $v['tag_name'] ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>            
        </div>
        <div class="span4">
            <div class="task_creation_date">                
            </div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="task_center span10 control-group controls-row row" style="margin-top:30px"> 
        <div class="tabbable"> 
            <ul class="nav nav-tabs">
                <li><a href="#tab1" data-toggle="tab"><i class="icon-plus" style="padding-bottom: 8px;"></i> <b><?php echo __('Agregar') ?></b></a></li>
                <li class="active"><a href="#tab2" data-toggle="tab"><label class="label"><?php echo count($a_task['task_comments']) ?></label> <b><?php echo __('Comentarios') ?></b></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane" id="tab1">
                    <form id="frm_new_task_comment" class="form-horizontal">
                        <div class="control-group">
                            <input type="hidden" name="ftask_idTask" id="ftask_idTask" value="<?php echo $a_task['task_id'] ?>" /> 
                            <textarea name="ftask_coment" rows="10" class="input-block-level">
                            </textarea>
                        </div>
                        <div>                        
                            <button id="f_save_commnet" class="btn"><?php echo __("Guardar") ?></button>
                        </div>
                    </form>
                </div>
                <div class="tab-pane active" id="tab2">
                    <?php if(count($a_task['task_comments'])>0) : ?>
                        <?php foreach ($a_task['task_comments'] as $v): ?>
                        <div class="well controls-row control-group">
                            <div class="span1">                                
                            </div>
                            <div class="span4">
                                <?php echo $v['task_comment'] ?>
                            </div>
                            <div class="span4">                        
                                <div class="control-group"><p class="muted"><?php echo $v['task_comment_date'] ?></div>
                                <div class="control-group"><p class="text-info"><b><?php echo $v['user_comment']['user_name'] ?></b></p></div>                                
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>                    
                </div>
            </div>
        </div>
    </div>    
</div>