<script type="text/javascript"> 
    
    window.open("https://mcets.zendesk.com/home",'_blank');
    window.location="/private/admin/index.html";
    
    var s_url_image_ticket="/private/tickets/getUploadFileHelpDesk";
    var s_url_deleteImage_ticket='deleteImageTicket.json';
    var s_url_listTags='listTagTicket.json';
    var s_url_create_ticket='create';
    var s_url_list_ticket='list.json';
    var s_place_holder_category="<? echo __('Seleccione Categoria')?>";
    var msgUploadFileExcel="<?php echo __('Seleccione Archivo') ?>";
    var s_validate_required_fticket_title="<?php echo __('Titulo requerido') ?>";
    var s_validate_required_fticket_category="<?php echo __('Categoria requerido') ?>";
    var s_validate_required_fticket_description="<?php echo __('Descripción requerido') ?>";
    var s_ticket_saved='<?php echo __("Ticket Enviado") ?>';
    var s_pagination_last='<?php echo __("Es la ultima Pagina") ?>';
    var s_pagination_first='<?php echo __("Es la primera Pagina") ?>';
    var s_ticket_answer='<?php echo __("respuestas")?>';
    var s_ticket_new='<?php echo __("Nuevo Ticket") ?>';
    var s_ticket_cancel='<?php echo __("Anular") ?>';
    var s_ticket_time="<?php echo __('hace') ?> ";
    var s_ticket_time_horas=" <?php echo __('horas') ?> ";
    var s_ticket_time_minutos=" <?php echo __('minutos') ?> ";
    var s_ticket_time_segundos=" <?php echo __('segundos') ?> ";
    
    var month_names = new Array( );
                month_names[month_names.length] = "<?php echo __("Ene") ?>";
                month_names[month_names.length] = "<?php echo __("Feb") ?>";
                month_names[month_names.length] = "<?php echo __("Mar") ?>";
                month_names[month_names.length] = "<?php echo __("Abr") ?>";
                month_names[month_names.length] = "<?php echo __("May") ?>";
                month_names[month_names.length] = "<?php echo __("Jun") ?>";
                month_names[month_names.length] = "<?php echo __("Jul") ?>";
                month_names[month_names.length] = "<?php echo __("Ago") ?>";
                month_names[month_names.length] = "<?php echo __("Set") ?>";
                month_names[month_names.length] = "<?php echo __("Oct") ?>";
                month_names[month_names.length] = "<?php echo __("Nov") ?>";
                month_names[month_names.length] = "<?php echo __("Dic") ?>";                              
</script>
<style type="text/css">    
    .clear{clear: both} 
    .none{display: none}
    .qq-upload-list{margin-top: 0px}
    #div_pagination{margin:0px 0px}
    #div_pagination ul li{cursor:pointer}
    .class_answer{font-size:200%;display: block;text-align:center}
    .task_center{float: none;margin-left: auto;margin-right: auto}
</style>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong><?php echo __("Bienvenido al modulo de Soporte!") ?></strong>
  <?php echo __("Mediante esta sección usted podrá enviar un ticket al área de soporte para solucionar/ayudar en cualquier tema referente al sistema."); ?>
</div>
<div class="row">
    <div class="span10 task_center"> 
        <div class="control-group controls-row row">            
            <div class="control-group span3">                
                <button class="btn " data-toggle="button" id="btn_new_ticket" type="button"><i class="icon-plus"></i> <?php echo __("Nuevo Ticket") ?></button>                
            </div>
            <div class="control-group span7">
                <div class="pagination pagination-large pagination-right" id="div_pagination">
                    <ul>
                         <li><a>&laquo;</a></li>
                         <li class="active"><a>1</a></li>                                               
                         <li><a>&raquo;</a></li>
                    </ul>
                </div>
            </div>            
        </div>        
        <div class="controls control-group none" id="block_new_ticket">            
            <form id="form_new_ticket" class="form-horizontal">
                <div class="control-group controls-row row">                    
                    <div class="span6">
                        <label class="pull-left" style="margin-top:5px;margin-right:5px" for="new_ticket_title"><? echo __("Titulo")?></label>
                        <div class="row">
                            <input class="span5" type="text" id="new_ticket_title" name="new_ticket_title" autocomplete="off" >
                        </div>
                    </div>                    
                    <div class="span4 controls-row row">
                        <label class="span1" style="margin-top:5px" for="new_ticket_category"><?php echo __("Categoria")?></label>
                        <div class="span2">
                            <select id="new_ticket_category" name="new_ticket_category">
                                <option value=""><?php echo __("Seleccione una opcion") ?></option>
                                <?php foreach($a_categories as $o_category):?>
                                <option value="<?php echo $o_category['customercategory_id'] ?>"><?php echo __($o_category['category_name']) ?></option>                                
                                <?php endforeach?>
                            </select>
                        </div>
                    </div>                    
                </div>
                <div class="control-group">
                    <label><?php echo __("Descripción")?></label>
                    <textarea id="new_ticket_description" name="new_ticket_description" class="input-block-level" rows="10">
                    </textarea>
                </div>
                <div  class="control-group">
                    <label for="new_ticket_tags"><?php echo __("Etiquetas")?></label>
                    <div>                        
                        <input class="input-xxlarge" id="new_ticket_tags" name="new_ticket_tags[]" type="text" autocomplete="off" />
                    </div>
                </div>
                <div  class="control-group">
                    <div id="btn_add_input">                        
                    </div>                    
                </div>
                <div  class="control-group">
                    <button class="btn_save_ticket btn" type="button"><i class="icon-hdd"></i> <?php echo __("Guardar") ?></button>                    
                </div>
            </form>
        </div>
    </div>
    <div id="div_tickets" class="span10 task_center">        
    </div>
</div>