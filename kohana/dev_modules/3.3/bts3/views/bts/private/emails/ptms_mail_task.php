<style type="text/css">
    *{
        margin: 0px;
        padding: 0px;
        font-family: 'Calibri','Tahoma','Times New Roman';
    }
    .c-border-radius{
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
    }
    .c-text-justify{
        text-align: justify;
    }
    .c-text-right{
        text-align: right;
    }
    .c-text-left{
        text-align: left;
    }
    .c-text-center{
        text-align: center;
    }
    .c-fLeft{
        float: left;
    }
    .c-clear{
        clear: both;
    }
    .websale-oBorder{
        border: #CCCCCC dashed 2px;
    }

    .websale-header-info label{
        font-weight: bold;
    }
    #table-data{width:40em}
    #table-data td:nth-child(1){width:18%}
</style>

<div class="websale-oBorder c-border-radius" style="margin: 5px; padding: 4px;">
    <div style="margin: 5px;padding: 4px;font-size: 1.2em;text-align: left;font-weight: bold;">
        <span style="margin: 5px; padding: 4px;border-style: none none dashed;border-width: thin;">
            [#:code]: :title
        </span>
    </div>
    <div style="margin: 5px; padding: 4px;">
        <div style="margin: 5px;">
            <table id="table-data">                
                <tr>
                    <td><?php echo __("Cliente"); ?>:</td>
                    <td>:client</td>
                </tr>
                <tr>
                    <td><?php echo __("Proyecto"); ?></td>
                    <td>:project</td>
                </tr>   
                <tr>
                    <td><?php echo __("Tipo Tarea"); ?></td>
                    <td>:typetask</td>
                </tr>                
                <tr>
                    <td><?php echo __("Severidad"); ?></td>
                    <td>:severity</td>
                </tr>                
                <tr>
                    <td><?php echo __("Prioridad"); ?></td>
                    <td>:priority</td>
                <tr>
                    <td><?php echo __("Asignado a "); ?></td>
                    <td>:assinedBy</td>
                </tr>                
                <tr>
                    <td><?php echo __("Creado por"); ?></td>
                    <td>:createBy</td>
                </tr>                
            </table>
        </div>        
    </div>
    <div style="margin: 5px; padding: 4px;">
        <div style="margin:5px;">
            <div style="font-weight: bolder;"><?php echo __("Descripcion:"); ?></div>
            <div>:description</div>
        </div>        
    </div>
    <div style="font-size: small; color: gray; padding: 4px;">
        <p style="text-align: right;">
            <?php echo __("Para contactar con nuestro servicio de atención al cliente, visite nuestro Centro de Asistencia o llame al 877-815-1531 (dentro de los EE.UU.)"); ?>
            <br/><?php echo __("Atentamente"); ?>
            <br/>:contactCompany
            <br/>:contactAddress
            <br/>:contactPhone
            <br/>:website
        </p>
    </div>
</div>