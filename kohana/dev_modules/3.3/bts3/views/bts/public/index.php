<!DOCTYPE html>
<html lang="es"> 
    <head>    
       <title><?php echo $SYSTEM_TITLE; ?></title>
        <meta name="description" content="<?php echo $SYSTEM_DESCRIPTION; ?>">
        <meta name="viewport" content="width=device-width">    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="author" content="IBUSPLUS">
        <?php
        $siteUnique = SITE_DOMAIN . Session::instance('database')->get('USER_NAME');
        $dsproject = $SYSTEM_CONFIG->get(Kohana_BtsConstants::BTS_SYS_CONFIG_STATIC_WEBSITE);
        $siteUniqueUrl = Request::initial()->controller() . Request::initial()->action();
        if (!Fragment::load($siteUnique . $siteUniqueUrl . 'public_header_css_vars', 28800, true)) {
            ?>
            <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/ui/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION; ?>.min.css">
            <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION; ?>.min.css">    
            <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION; ?>.min.css">
            <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/main.public<?php echo $SYSTEM_DEV_EXT_FILE; ?>.css">        
            <?php foreach ($SYSTEM_A_STYLES as $a_style): ?>
                <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com<?php echo $a_style['file_name'] ?>">
            <?php endforeach ?>
            <?php foreach ($SYSTEM_A_PROJ_STYLES as $a_proj_style): ?>
                <link rel="stylesheet" href="<?php echo $dsproject . $a_proj_style['file_name'] ?>">
            <?php endforeach ?>
            <?php Fragment::save();
        }
        ?>
    </head>
    <body>
  <div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
      <div class="container">
     <img src="/img/travelynx/head.png"/>
      <div class="nav-collapse">        
      </div>
    </div>
  </div>
</div>
        <div class="padding_top_body"></div>
        <div class="container">
            <?php
            $cacheSessionDynamic = $siteUnique . $siteUniqueUrl;
            if (Kohana::$environment == Kohana::DEVELOPMENT) {
                echo $SYSTEM_BODY;
            } elseif (Kohana::$environment == Kohana::PRODUCTION) {
                if ((!Fragment::load($cacheSessionDynamic, 28800, true))) {
                    echo $SYSTEM_BODY;
                    Fragment::save();
                }
            }
            ?>
        </div> <!-- /container -->
        <div class="navbar navbar-fixed-bottom">
            <div class="navbar-inner">
                <div class="container">
                      <img src="/img/travelynx/footer.png"/>
                </div>
            </div>    
        </div>
        <?php if (!Fragment::load($siteUnique . 'public_header_js_vars', 28800, true)) { ?>
            <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_VERSION ?>.min.js"></script>        
            <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION ?>.min.js"></script>
            <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION ?>.min.js"></script>
            <!--Loading Scripts-->
            <script type="text/javascript">
                var ajax_response_format_default = '<?php echo $SYSTEM_AJAX_DATA_TYPE ?>';
    <?php include_once DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'jquery_global.js' //Falta cambiar por DEV MOD PATH       ?>
                var jq_jsonp_callback = '<?php echo ($SYSTEM_AJAX_DATA_TYPE == 'jsonp' ? '?' . $SYSTEM_JSONP_CALL_BACK . '=?' : '') ?>';
                var jq_json_code_success = '<?php echo Kohana_BtsConstants::BTS_CODE_SUCCESS ?>';
                var jq_json_code_error = '<?php echo Kohana_BtsConstants::BTS_CODE_ERROR ?>';
                var showloadingDialog = 0;
            </script>
            <?php
            Fragment::save();
        }
        if (!Fragment::load($siteUnique . $siteUniqueUrl . 'public_header_js_vars_end', 28800, true)) {
            foreach ($SYSTEM_A_SCRIPTS as $a_script):
                ?>
                <script type="text/javascript" src="http://static.js.bts.mcets-inc.com<?php echo $a_script['file_name']; ?>"></script>
            <?php endforeach ?>
            <?php foreach ($SYSTEM_A_PROJ_SCRIPTS as $a_proj_script): ?>
                <script type="text/javascript" src="<?php echo $dsproject . $a_proj_script['file_name']; ?>"></script>
            <?php endforeach ?>           
            <script type="text/javascript">
    <?php include_once DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'js_after_scripts.js' ?>
            </script>        
            <?php Fragment::save();
        }
        ?>     
    </body>
</html>