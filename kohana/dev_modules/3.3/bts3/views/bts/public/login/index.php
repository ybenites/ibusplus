<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo $SYSTEM_TITLE ?></title>
        <meta name="description" content="<?php echo $SYSTEM_DESCRIPTION ?>">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/ui/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION.$SYSTEM_DEV_EXT_FILE; ?>.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION.$SYSTEM_DEV_EXT_FILE; ?>.css">    
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION.$SYSTEM_DEV_EXT_FILE; ?>.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/main<?php echo $SYSTEM_DEV_EXT_FILE; ?>.css">
        <link rel="stylesheet" href="http://static.css.bts.mcets-inc.com/kernel/css/bts/ubrowser/browser-detection<?php echo $SYSTEM_DEV_EXT_FILE; ?>.css" />                
    </head>
    <body class="body_login">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->        
        <div class="container">
            <form method="POST" id="form_login" class="form-signin">
                <div class="change_language">
                    <div id='language_link' class="padding5" style="text-align: center;">
                        <a id="lang_es">
                            <span class="badge badge-info">es</span>
                        </a>
                        <div style="height: 4px;"></div>
                        <a id="lang_en">
                            <span class="badge badge-info">en</span>                            
                        </a>
                    </div>
                </div>
                <div>
                    <img src="/img/<?php echo $SYSTEM_SKIN ?>/logo_login.png" class="login_logo" />
                </div>
                <h2 class="form-signin-heading"><?php echo __("Iniciar Sesion") ?></h2>
                <div class="control-group">
                    <input id="user_name" name="user_name" type="text" class="input-xlarge" placeholder="<?php echo __('Usuario') ?>"/>
                </div>
                <div class="control-group">
                    <input id="user_password" name="user_password" type="password" class="input-xlarge" placeholder="<?php echo __('Contraseña') ?>"/>
                </div>
                <div id="message_login"></div>
                <input type="hidden" name="check_form_direct_login_key" id="check_form_direct_login_key" value="<?php echo $check_form_direct_login_key ?>"/>
                <div>                        
                    <div class="pull-left">                            
                        <button id="btn_process_login" class="btn btn-primary" type="submit" data-loading-text="<?php echo __("Ingresando") ?>...">
                            <?php echo __("Ingresar") ?>
                        </button>
                    </div>
                    <div class="pull-right">
                        <a id="recovery_pass" href="#m_recovery_pass" data-toggle="modal">
                            <i class="icon-asterisk"></i>
                            <?php echo __("Olvidé mi contraseña") ?>
                        </a>
                        <br/>
                        <a id="recovery_user" href="#m_recovery_user" data-toggle="modal">
                            <i class="icon-user"></i>
                            <?php echo __("Olvidé mi nombre de usuario") ?>
                        </a>                        
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div> <!-- /container -->

        <!--Recuperar contraseña -->
        <div id="m_recovery_pass" class="modal hide" data-backdrop="static">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4><?php echo __("Recuperar Contraseña") ?></h4>
            </div>
            <div class="modal-body">
                <form id="recoverpass" class="form-horizontal"> 
                    <div class="control-group"> 
                        <label class="control-label"><b><?php echo __("Usuario") ?>: </b></label>
                        <div class="controls">
                            <input id="user_recovery" placeholder="" class="input-large" type="text"/>
                        </div>
                    </div> 
                    <div class="control-group">
                        <blockquote>
                            <small><?php echo __("Ingrese su nombre de usuario y luego presione ENTER, un correo será enviado a la dirección de correo electrónico con la que se registro su usuario") ?>.
                            </small>
                        </blockquote>
                    </div>
            </div>
        </form>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>  
        </div>
    </div>
    <!--Alerta de Recuperar contraseña -->
    <div id="modal_alert_pass" class="modal hide" data-backdrop="static">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4><?php echo __("Cambiar Password") ?></h4>
        </div>
        <div class="modal-body">
            <div class="control-group">
                <label class="control-label"><?php echo __("Desea cambiar el password?") ?></label>
            </div>
        </div>
        <div class="modal-footer">
            <button id="modal_btn_save" type="submit" class="btn btn-primary"><?php echo __("Aceptar") ?></button>
            <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>  
        </div>
    </div>
    <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/modernizr.matchMedia.respond<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <?php if ($SYSTEM_LANGUAGE == 'es'): ?>
        <script src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/ubrowser/browser-detection.lang-es<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <?php endif ?>
    <script src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/ubrowser/browser-detection<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_VERSION.$SYSTEM_DEV_EXT_FILE;?>.js"></script>
    <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-ui-<?php echo Kohana_Controller_Private_Kernel::$JQUERY_UI_VERSION.$SYSTEM_DEV_EXT_FILE;?>.js"></script>
    <script src="http://static.js.bts.mcets-inc.com/kernel/js/vendor/bootstrap.<?php echo Kohana_Controller_Private_Kernel::$BOOTSTRAP_VERSION.$SYSTEM_DEV_EXT_FILE;?>.js"></script>
    <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/jquery.validate<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/jquery.ibusplus.changeLanguage<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/core/bootbox<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <script type="text/javascript" src="http://static.js.bts.mcets-inc.com/kernel/js/bts/public/login<?php echo $SYSTEM_DEV_EXT_FILE; ?>.js"></script>
    <!--Loading Scripts-->

    <script type="text/javascript">
        var ajax_response_format_default = '<?php echo $SYSTEM_AJAX_DATA_TYPE ?>';
        var jq_jsonp_callback = '<?php echo ($SYSTEM_AJAX_DATA_TYPE == 'jsonp' ? '?' . $SYSTEM_JSONP_CALL_BACK . '=?' : '') ?>';
        var jq_json_code_success = '<?php echo Kohana_BtsConstants::BTS_CODE_SUCCESS ?>';
        var jq_json_code_error = '<?php echo Kohana_BtsConstants::BTS_CODE_ERROR ?>';
        var l_spanish = "<?php echo __("Español") ?>";
        var l_english = "<?php echo __("Ingles") ?>";
        var l_sys = "<?php echo $SYSTEM_LANGUAGE ?>";
        var s_user_required = "<?php echo __("Usuario requerido") ?>";
        var s_pass_required = "<?php echo __("Contraseña requerido") ?>";
        var form_login;
        var url_recovery_pass = '/public/login/recoveryPass';
        var s_recovery_pass = 'Listo!';
        var s_recovery_unforget = 'Ingresa nombre de usuario!';
        <?php include_once DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3' . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'jquery_global.js' //Falta cambiar por DEV MOD PATH   ?>      
    </script>
</body>
</html>