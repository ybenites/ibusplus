bootbox.animate(0);
bootbox.backdrop('static');
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
$.fn.serializeDisabled = function() {
    var obj = {};
    $(':disabled', this).each(function() {
        if ($(this).is(':radio')) {
            $.each($(this), function() {
                if ($(this).is(':checked'))
                    obj[this.name] = $(this).val();
            });
        } else {
            obj[this.name] = $(this).val();
        }
    });
    return obj;
};


//eval response

function evalResponse(response) {
    var evalResponse = false;
    if (response.code == jq_json_code_success) {
        evalResponse = true;
    } else {
        evalResponse = false;
        var error = response.msg;
        var errorText = $(document.createElement('div'));
        if (typeof error === 'string') {
            errorText = error;
        } else if (error.eEnviroment == 10) {
            errorText.append($(document.createElement('p'))
                    .text(error.eText)
                    .attr("style", "text-align:left;"));
            errorText.append($(document.createElement('div'))
                    .attr("style", "line-height:30px; text-align:center;")
                    .append($(document.createElement('b'))
                    .text(s_support_send_email_click))
                    .append($(document.createElement('br'))));
            var imgLink =
                    $(document.createElement('img'))
                    .attr("src", "/kernel/img/ico/ico_send_email.png")
                    .attr("title", s_support_send_email_alt)
                    .attr("style", "cursor:pointer;");
            $.data(imgLink.get(0), error);
            errorText.find("div").append(imgLink);
            imgLink.click(function() {
                var image = $(this);
                image.hide();
                var loading = $(document.createElement('img'))
                        .attr('src', '/kernel/img/wait_loading.gif');
                image.after(loading);
                showLoading = 1;
                $.ajax({
                    url: '/private/profile/sendSupportEmail',
                    data: $(this).data(),
                    success: function(response) {
                        if (response.code == jq_json_code_success) {
                            bootbox.alert(s_support_send_email_confirm);
                        } else {
                            bootbox.alert(response.msg.eText);
                        }
                    }, complete: function() {
                        image.show();
                        loading.remove();
                    }
                });
            });
        } else {
            var tbl = $(document.createElement('table'));

            tbl.append($(document.createElement('tr'))
                    .append($(document.createElement('td')).attr('style', 'text-align:left; width:25%;').append($(document.createElement('b')).text('ERROR CODE:')))
                    .append($(document.createElement('td')).attr('style', 'text-align:left; width:25%;').append(error.eCode))
                    .append($(document.createElement('td')).attr('style', 'text-align:left; width:25%;').append($(document.createElement('b')).text('LINE:')))
                    .append($(document.createElement('td')).attr('style', 'text-align:left; width:25%;').append(error.eLine)));

            tbl.append($(document.createElement('tr')).append($(document.createElement('td')).attr('colspan', 4).append($(document.createElement('b')).text('FILE:'))));
            tbl.append($(document.createElement('tr')).append($(document.createElement('td')).attr('colspan', 4).append(error.eFile)));
            tbl.append($(document.createElement('tr')).append($(document.createElement('td')).attr('colspan', 4).append($(document.createElement('b')).text('MSG:'))));
            tbl.append($(document.createElement('tr')).append($(document.createElement('td')).attr('colspan', 4).append(error.eText)));

            errorText.append($(document.createElement('pre')).append(tbl));
            errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('STACK TRACE:')).append($(document.createElement('br'))).append(error.eTrace).append($(document.createElement('br'))));
            errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('PARAMS:')).append($(document.createElement('br'))).append(error.rParams).append($(document.createElement('br'))));
            if (error.eParams !== null)
                errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('EXTRA PARAMS:')).append($(document.createElement('br'))).append(error.eParams).append($(document.createElement('br'))));
        }
        bootbox.alert(errorText);
    }
    return evalResponse;
}

function fnCreateUploadFileInstance(DOM_button, URL_upload_action, fn_on_submit, fn_success, fn_unsuccess, fn_after_create, a_allowed_ext) {
    var o_instance = null;
    if (typeof qq === 'undefined') {
        console.warn('jquery.fileuploader.min.js is not included');
    } else {
        if (typeof a_allowed_ext === 'undefined') {
            a_allowed_ext = ['jpg', 'jpeg', 'png'];
        }
        o_instance = new qq.FileUploader({
            element: DOM_button,
            action: URL_upload_action,
            allowedExtensions: a_allowed_ext,
            onSubmit: function(id, fileName) {
                if (typeof fn_on_submit !== 'function') {
                    console.warn('fn_on_submit is not a function');
                } else {
                    fn_on_submit(id, fileName);
                }
            },
            onComplete: function(id, fileName, responseJSON) {
                if (typeof fn_success !== 'function') {
                    console.warn('fn_success is not a function');
                } else if (typeof fn_success !== 'function') {
                    console.warn('fn_unsuccess is not a function');
                } else {
                    if (responseJSON.code == jq_json_code_success && responseJSON.success == 'true') {
                        fn_success(id, fileName, responseJSON);
                    } else {
                        fn_unsuccess(id, fileName, responseJSON);
                    }
                }
            }
        }, s_menu_img_shorcut);
        if (typeof fn_after_create !== 'function') {
            console.warn('fn_after_create is not a function');
        } else {
            fn_after_create();
        }
    }
    return o_instance;
}

var CookieTools = {
    createCookie: function(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else
            var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },
    readCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    eraseCookie: function(name) {
        CookieTools.createCookie(name, "", -1);
    }
};
function changeUserWindow() {
    var upreferences = $.parseJSON(CookieTools.readCookie('upreferences'));
    if (upreferences == null) {
        CookieTools.createCookie("upreferences", JSON.stringify({fs: true}));
        upreferences = $.parseJSON(CookieTools.readCookie('upreferences'));
    } else {
        upreferences.fs = !upreferences.fs;
        CookieTools.createCookie("upreferences", JSON.stringify(upreferences));
    }
    switchScreenUser(upreferences.fs);
}
function switchScreenUser(full) {
    $('div.nav_menu').attr('style', full ? 'display:none;position:inherit' : '');
    $('div.navbar_shortcuts').attr('style', full ? 'position:inherit' : '');
    $('div.padding_top_body').attr('style', full ? 'margin-top: 5px;' : '');
}
function loadUserPreferences() {
    var upreferences = $.parseJSON(CookieTools.readCookie('upreferences'));
    switchScreenUser(upreferences.fs);
}