var b_ajax = true;
$(document).ajaxStart(function() {
    b_ajax = false;
}).ajaxComplete(function() {
    b_ajax = true;
});

$.ajaxSetup({
    type: 'POST',
    dataType: ajax_response_format_default,
    global: true,
    cache: true,
    beforeSend: function() {
        if (showloadingDialog == 1)
            $("#loading").modal('show');
    },
    complete: function(event, r) {
        if (showloadingDialog == 1)
            $("#loading").modal('hide');
        showloadingDialog = 0;
    },
    error: function(event, r) {
    }
});