<?php

defined('SYSPATH') or die('No direct script access.');
return array(
    Kohana::DEVELOPMENT => array(
        Kohana_BtsConstants::BTS_BOOTSTRAP_VERSION => '2.3.2'
        , Kohana_BtsConstants::BTS_JQUERY_VERSION => '2.0.3'
        , Kohana_BtsConstants::BTS_JQUERY_UI_VERSION => '1.10.3'
    ),
    Kohana::PRODUCTION => array(
        Kohana_BtsConstants::BTS_BOOTSTRAP_VERSION => '2.3.2'
        , Kohana_BtsConstants::BTS_JQUERY_VERSION => '2.0.3'
        , Kohana_BtsConstants::BTS_JQUERY_UI_VERSION => '1.10.3'
    )
);