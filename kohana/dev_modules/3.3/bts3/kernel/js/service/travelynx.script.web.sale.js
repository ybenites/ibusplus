$(function(){    
    $.ajax({
        data: {
            companyId: IBUS_SALE_WEB.config.companyId , 
            code: IBUS_SALE_WEB.config.code , 
            lang: IBUS_SALE_WEB.config.lang ,             
            height: IBUS_SALE_WEB.config.height , 
            width: IBUS_SALE_WEB.config.width ,
            date_format : IBUS_SALE_WEB.config.date_format
        }, 
        type: 'POST', 
        url: IBUS_SALE_WEB.config.host + IBUS_SALE_WEB.config.url+'?jsonp_callback=?', 
        dataType: 'jsonp' , 
        success: function(j_response){
            try{
                $('#qb_form_web_sale').replaceWith(j_response.data);                
            }catch(e){
                 console.log(e);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
        console.log(ajaxOptions);
        }
    });
});