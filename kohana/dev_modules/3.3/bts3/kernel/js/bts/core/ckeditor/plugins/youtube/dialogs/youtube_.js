CKEDITOR.dialog.add('youtube', function(editor) {

    return {
        title: 'YouTube',
        minWidth: 450,
        minHeight: 300,
        contents: [
            {
                id: 'plugin_text',
                label: '',
                title: '',
                expand: true,
                padding: 0,
                elements:
                        [
                            {
                                type: 'html',
                                html: '<img src="/js/bts/core/ckeditor/plugins/youtube/images/youTube.png" alt="YouTube Video Dialog"><br /><br /><p>' + 'Enter the address of your YouTube video\'s webpage below' + '</p>'
                            },
                            {
                                type: 'text',
                                id: 'url',
                                label: 'URL',
                                validate: CKEDITOR.dialog.validate.notEmpty('Please enter a valid YouTube video webpage address.'),
                                required: true,
                                commit: function(data) {
                                    data.url = this.getValue();
                                }
                            },
//	                {
//	                    type: 'select',
//	                    id: 'size',
//	                    label: 'Size',
//	                    items:
//		                [
//			                ['640 x 505', 'c'],
//			                ['480 x 385', 'b'],
//                            ['425 x 344', 'a']
//		                ],
//	                    commit: function (data) {
//	                        data.size = this.getValue();
//	                    }
//	                }
                        ]
            }
        ],
        onOk: function() {
            // Create a link element and an object that will store the data entered in the dialog window.
            // http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.dom.document.html#createElement
            var dialog = this,
                    data = {},
                    iFrameElement = editor.document.createElement('iframe');

            // Populate the data object with data entered in the dialog window.
            // http://docs.cksource.com/ckeditor_api/symbols/CKEDITOR.dialog.html#commitContent
            this.commitContent(data);

            var width;
            var heigth;

            switch (data.size) {
                case 'a':
                    width = 425;
                    heigth = 344;
                    break;
                case 'b':
                    width = 480;
                    heigth = 385;
                    break;
                case 'c':
                    width = 640;
                    heigth = 505;
                    break;
            }

            //http://www.youtube.com/watch?v=rLN8M9ZLeg0
            //http://www.youtube.com/watch?v=qSqLTTs1tNE&feature=topvideos_people
            //change to 
            //http://www.youtube.com/embed/rLN8M9ZLeg0
            var src = data.url.replace("watch?v=", "embed/").split("&", 1)
            //.replace("&feature=related", "");
            //<iframe title='YouTube video player' type='text/html' width='{0}' height='{1}' src='{2}' frameborder='0' ></iframe>"           

            iFrameElement.setAttribute('title', 'YouTube video player');
            iFrameElement.setAttribute('type', 'text/html');
//            iFrameElement.setAttribute('width', width);
//            iFrameElement.setAttribute('height', heigth);
            iFrameElement.setAttribute('src', src);
            iFrameElement.setAttribute('frameborder', '0');
            editor.insertElement(iFrameElement);           
            if(typeof this.definition.afterOk =='function')this.definition.afterOk(this);

        }
    };
});
