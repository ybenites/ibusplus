var noticeLang = 'custom';
var noticeLangCustom = new Object();
noticeLangCustom.title = 'Navegador Obsoleto Detectado';
noticeLangCustom.notice = 'Parece que está utilizando un navegador obsoleto que impide el acceso a algunas de las funciones o características de nuestro sitio web. Se requiere actualizar o instalar un nuevo navegador.';
noticeLangCustom.selectBrowser = 'Visite los sitios oficiales de los navegadores más populares a continuación:';
noticeLangCustom.remindMeLater = 'Ahora no, pero más tarde quizás.';
noticeLangCustom.neverRemindAgain = 'No, no recordármelo otra vez.';