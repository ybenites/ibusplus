$(document).ready(function() {
    form_login = $('#form_login').validate({
        rules: {
            user_name: 'required'
                    , user_password: 'required'
        },
        messages: {
            user_name: {
                required: s_user_required
            }
            , user_password: {
                required: s_pass_required
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parent().addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parent().removeClass('error');
        }
        , submitHandler: function(o_form) {
            $('#message_login').html('');
            if (b_ajax) {
                $.ajax({
                    url: '/public/login/auth.json' + jq_jsonp_callback
                            , data: $(o_form).serialize()
                            , beforeSend: function() {
                        $('#btn_process_login').button('loading');
                    }
                    , complete: function() {
                        $('#btn_process_login').button('reset');
                    }
                    , success: function(j_response) {
                        if (j_response.code == jq_json_code_success) {
                            window.location = j_response.data.url;
                        } else {
                            $('#message_login').append(
                                    $('<div/>').addClass('alert alert-error')
                                    .html(j_response.msg)
                                    .prepend(
                                    $('<button/>').attr('type', 'button')
                                    .attr('data-dismiss', 'alert')
                                    .addClass('close')
                                    .html('&times;')
                                    )
                                    );
                        }
                    }
                });
            }
        }
    });

    $('#recovery_pass').click(function() {
        $('#user_recovery').val('');
    });

    // Para recuperar de pass
    $('#recoverpass').submit(function(event) {
        event.preventDefault();
        if ($.trim($('#user_recovery').val()) == '') {
            bootbox.alert(s_recovery_unforget);
        } else {
            $('#modal_alert_pass').modal('show');
        }


        $('#modal_btn_save').click(function() {
            $('#recoverpass').submit();
        });

        //contrasenia recuperada
        $recover = $('#recoverpass').validate({
            rules: {user_recovery: 'required'},
            message: {user_recovery: {required: s_user_required}},
            errorClass: 'help-inline',
            highlight: function(element, errorClass) {
                $(element).parents('.control-group').addClass('error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.control-group').removeClass('error');
            },
            submitHandler: function(form) {
                if (b_ajax) {
                    if ($('#user_recovery').val() != '') {
                        var user = $('#user_recovery').val();
                    }
                    $.ajax({
                        url: url_recovery_pass + jq_jsonp_callback
                                , data: $.extend($(form).serialize(), {user: user})
                                , beforeSend: function() {
                            $('#modal_btn_save').button('loading');

                        }
                        , complete: function() {
                            $('#modal_btn_save').button('reset');
                        }
                        , success: function(j_response) {
                            if (j_response.code == jq_json_code_success) {
                                bootbox.alert(s_recovery_pass);
                                $('#modal_alert_pass').modal('hide');
                                $('#modal_user_info').modal('hide');
                                $('#m_recovery_pass').modal('hide');
                            } else {
                                bootbox.alert(j_response.msg);
                            }
                        }
                    });
                }
            }
        });
    });

    // Para cambiar de idioma 
    if (l_sys === 'es') {
        $('#lang_es span.badge').removeClass('badge-info');
        $('#lang_es span.badge').toggleClass('badge-important');
    } else {
        $('#lang_en span.badge').removeClass('badge-info');
        $('#lang_en span.badge').toggleClass('badge-important');
    }
});