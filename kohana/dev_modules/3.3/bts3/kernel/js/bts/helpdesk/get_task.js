$(document).ready(function(){    
    $('.task_creation_date').html(fnGetDateUpdateDiff(task_create_date));            
    var frm_new_task_comment=$("#frm_new_task_comment").validate({
        rules:{
            ftask_coment: {required:true}
        },
        messages:{ 
            ftask_coment: {required: s_validate_required_ftask_coment}
        }
        ,errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            var frm_datastring=$(o_form).serialize();             
            $.ajax({
                url: s_url_create_task_comment + jq_jsonp_callback
                , data:frm_datastring
                , beforeSend: function() {
                    $('#f_save_commnet').button('loading');
                }
                , complete: function() {
                    $('#f_save_commnet').button('reset');
                }
                , success: function(j_response) {
                    if (j_response.code == jq_json_code_success) {
                        
                        setTimeout(function(){
                            window.location.reload();   
                            },1500);
                    } else {
                        bootbox.alert(j_response.msg);
                    }
                }
            });
        }
    });
    $("#ftask_save_commnet").click(function(){
        $("#frm_new_task_comment").submit();
    });
});
function fnGetDateUpdateDiff(t_time){                       
        t_time = new Date(t_time*1000);                
        var t_current_unix_date_f = new Date();         
        var d_diff_date = t_current_unix_date_f.getTime()-t_time.getTime();        
        var d_sec = 1000;
        var d_min = 60*d_sec;
        var d_hour = d_min * 60;
        var d_day = 24 * d_hour;                
        if(d_diff_date/d_day >= 1){                    
            var d_date_update = t_time;
            return d_date_update.getDate() + ' '+month_names[d_date_update.getMonth()] ;
        }
        if(d_diff_date/d_hour >= 1){                    
            var d_hour_update = d_diff_date / d_hour;                    
            d_hour_update = parseInt(d_hour_update);                    
            if(d_hour_update > 1){
                return s_ticket_time+ d_hour_update +s_ticket_time_horas;
            }else{
                return s_ticket_time+ d_hour_update + s_ticket_time_horas;
            }                    
        }
                
        if(d_diff_date/d_min >= 1){                    
            var d_min_update = d_diff_date / d_min;
                    
            d_min_update = parseInt(d_min_update);
                    
            if(d_min_update > 1){
                return s_ticket_time+ d_min_update + s_ticket_time_minutos;
            }else{                        
                return s_ticket_time+ d_min_update + s_ticket_time_minutos;
            }                    
        }
                
        if(d_diff_date/d_sec >= 1){                    
            var d_sec_update = d_diff_date / d_sec;
                    
            d_sec_update = parseInt(d_sec_update);
                    
            if(d_sec_update > 1){
                return s_ticket_time+ d_sec_update +s_ticket_time_segundos;
            }else{                        
                return 'ahora';
            }                    
        }          
    }