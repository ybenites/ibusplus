$(document).ready(function(){
    $('#new_ticket_description').empty();
    $("#new_ticket_category").select2({
        placeholder: s_place_holder_category,
        width: 'resolve',
        allowClear: true
    });
    addImage();    
    
    var form_new_ticket=$("#form_new_ticket").validate({
        rules:{
            new_ticket_title: {required:true}
            ,new_ticket_category: {required:true}
            ,new_ticket_description: {required:true}
        },
        messages:{ 
            new_ticket_title: {required: s_validate_required_fticket_title}
            ,new_ticket_category: {required: s_validate_required_fticket_category}
            ,new_ticket_description: {required: s_validate_required_fticket_description}
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            var frm_datastring=$(o_form).serialize();             
            var string_array='';
            var string_array_o='';            
            if($('.qq-upload-success').length>0){                    
                $.each($('.qq-upload-success'),function(i,d){
                    string_array=string_array+'&array_file_upload['+i+']='+$(d).data('file_upload');
                    string_array_o=string_array_o+'&name_orig_file_upload['+i+']='+$(d).data('name_orig_file_upload');
                });
            }               
            var new_ticket_tags=$("#new_ticket_tags").select2('data');
            string_array_tags='';
            if(new_ticket_tags.length>0){                
                $.each(new_ticket_tags,function(i,d){
                    string_array_tags=string_array_tags+'&new_ticket_tags_id['+d.id+']='+d.value;                    
                });
            }
            frm_datastring=frm_datastring+string_array+string_array_o+string_array_tags;
            $.ajax({
                url: s_url_create_ticket + jq_jsonp_callback
                , data:frm_datastring
                , beforeSend: function() {
                    $('#btn_save_ticket').button('loading');
                }
                , complete: function() {
                    $('#btn_save_ticket').button('reset');
                }
                , success: function(j_response) {
                    if(evalResponse(j_response)){    
                        $('#btn_new_ticket').click();                         
                        bootbox.alert(s_ticket_saved);                        
                    }
                }
            });
        }
    });
    $(".btn_save_ticket").on('click',function(){        
        $("#form_new_ticket").submit();
    });
    
    $("#btn_new_ticket").click(function(){        
        if($("#block_new_ticket").css('display')=='block'){            
            $("#btn_new_ticket").html('<i class="icon-plus"></i>  ');
            $("#btn_new_ticket").append(s_ticket_new);
            deleteImageTicket('.qq-upload-success',true);
            $("#id_ticket_task").val(0);                                               
       }else if($("#block_new_ticket").css('display')=='none'){
           $('#form_new_ticket').get(0).reset();
           $("#new_ticket_category").select2("val",'');
           $("#new_ticket_tags").select2('val','');
            $("#btn_new_ticket").html('<i class=" icon-minus"></i>  ');
            $("#btn_new_ticket").append(s_ticket_cancel);            
            $("#id_ticket_task").val(0);
       }
       $("#block_new_ticket").toggle();
    });
       
    var coun_l=0;
    $("#new_ticket_tags").select2({
        minimumInputLength:3
        ,maximumInputLength:10
        ,tags: true
        ,multiple: true 
        ,quietMillis: 100
        ,ajax:{
            url:s_url_listTags,
            dataType: 'json',
            data: function (term, page) {
                return {
                    q: term
                };
            }
            ,results:function(data, page){                
                return {results: data.data};              
            }
        }  
        ,formatResult:function(data){
            return '<div>' + data.value + '</div>';
        }
        ,formatSelection:function(data){
            coun_l=coun_l+1;
            return data.value;
        }
        ,createSearchChoice:function(term, data) {            
            if ($(data).filter(function() { return this.value.localeCompare(term)===0; }).length===0){                
                return {id:'id_'+coun_l, value:term};
            } 
        }
        ,dropdownCssClass: "bigdrop"                
        ,escapeMarkup: function (m) { return m; }          
    });
    fnUpdateTableTickets(0,0,0,0);    
});
function addImage(){
    var uploader = new qq.FileUploader({
        element:document.getElementById('btn_add_input'),
        action:s_url_image_ticket,
        allowedExtensions: ['jpg','jpeg','png','xlsx','xls','txt','doc','docx'],
        multiple: false,
        onSubmit: function(id, fileName){
        },  
        onComplete: function(id, fileName, responseJSON){
            if(responseJSON.code==jq_json_code_success && responseJSON.success=='true'){                    
                $('.qq-upload-file').addClass('span2');
                $('.qq-upload-size').addClass('span0');
                $('.qq-upload-success:last').data('file_upload',responseJSON.data.tmpfn);
                $('.qq-upload-success:last').data('name_orig_file_upload',fileName);                    
                $('.qq-upload-success:last').append("<span class='span0'><img src='/kernel/img/ico/ico_check.png' alt='' /><img class='cancel_upload' src='/kernel/img/ico/ico_trash.png' alt='' /></span>");                        
                $('li.qq-upload-success span').on('click','.cancel_upload',function(){                        
                    var line_g=$(this).parent().parent('.qq-upload-success');                            
                    deleteImageTicket(line_g,false);                            
                });
            } else{
                $(".qq-upload-failed-text").hide();
                $('.qq-upload-fail:last').append("<span style='margin-left:2em;'><img src='/kernel/img/ico/ico_close.png' alt='' /><img class='cancel_upload' src='/kernel/media/ico/ico_cancel.png' alt='' /></span>");
                bootbox.alert(responseJSON.msg);
            }
        } 
    },msgUploadFileExcel);
    $('.qq-uploader').addClass('controls-row row');
    $(".qq-upload-list").css('margin-left','100px');
    $(".qq-upload-list").addClass("span5");
    $('#btn_add_input .qq-upload-button').addClass('btn span5');                
    $('#btn_add_input .qq-upload-button').hover(function(){                
        if($(this).hasClass('btn-info')){            
            $(this).removeClass('btn-info');
        }else {            
            $(this).addClass('btn-info');
        }
    });    
}
function deleteImageTicket(img_delete,deleteAll){
    var array_file_delete=[];
    if(deleteAll===true){
        if($(img_delete).length>0){
            $.each($(img_delete),function(i,d){
                array_file_delete[i]=$(d).data('file_upload');                    
            });
        }
    }else{
        array_file_delete[0]=$(img_delete).data('file_upload');
    }
    if(array_file_delete.length>0){
        $.ajax({
            url: s_url_deleteImage_ticket+jq_jsonp_callback
            , data:{
                'img_delete[]':array_file_delete
            }
            , type: 'post'
            , beforeSend:function(){                        
            }
            , success: function(j_response){                         
                if(j_response.code==jq_json_code_success && j_response.success==true){
                    if(deleteAll===true){
                        $(img_delete).remove();
                    }else{
                        $(img_delete).hide(500,function(){$(img_delete).remove();});
                    }
                }else{
                    bootbox.alert(j_response.msg);                        
                }                        
            }
            ,complete: function(){                        
            }
        });
    }
}
function fnUpdateDrawPagination(page,total,record,first,center,last,flag_click){
    var ul_prev_active='<ul><li><a>&laquo;</a></li>';
    var ul_prev_disabled='<ul><li class="disabled"><a>&laquo;</a></li>';
    var ul_next_active='<li><a>&raquo;</a></li></ul>';
    var ul_next_disabled='<li class="disabled"><a>&raquo;</a></li></ul>';
    var div_pagination='';
    var class_active='';
    var ul_prev='';
    var ul_next='';
    var pos_li_active=$("#div_pagination ul li.active").index();
    var text_li_active=$("#div_pagination ul li.active").find('a').text();
    var li_prev_text=$("#div_pagination ul li.active").prev().text();
    var li_next_text=$("#div_pagination ul li.active").next().text();
    if(total<=5){        
        for(var i=1;i<=total;i++){
            if(i==page){
                class_active='class="active"';
            }else{class_active='';}
            div_pagination+='<li '+class_active+'><a>'+i+'</a></li>';
        }
        ul_prev=ul_prev_active;
        ul_next=ul_next_active;
        $("#div_pagination").html(ul_prev+div_pagination+ul_next);
        fnClickPagination();
    }else{        
        if(first==0 && last==0){
            div_pagination+='<li class="active disabled"><a>'+1+'</a></li>';
            div_pagination+='<li><a>'+2+'</a></li>';
            div_pagination+='<li><a>'+3+'</a></li>';
            div_pagination+='<li><a>'+4+'</a></li>';
            div_pagination+='<li class="disabled"><a>...</a></li>';   
            ul_prev=ul_prev_disabled;
            ul_next=ul_next_active;
            $("#div_pagination").html(ul_prev+div_pagination+ul_next);
            fnClickPagination();
        }                
        if(flag_click==1){
            center=parseInt(center);
            if(last=='...' && first!='...'){
                bootbox.alert(s_pagination_first);
                return false;
            }else if (last!='...' && first=='...'){                                
                if(last-4<=0){
                    div_pagination+='<li><a>...</a></li>';
                    div_pagination+='<li><a>'+(center-1)+'</a></li>';
                    div_pagination+='<li><a>'+center+'</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li><a>'+(center+2)+'</a></li>';                    
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_disabled;                    
                }else{                    
                    div_pagination+='<li class="disabled"><a>...</a></li>';
                    div_pagination+='<li><a>'+(center)+'</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li><a>'+(center+2)+'</a></li>';
                    div_pagination+='<li class="disabled"><a>...</a></li>'; 
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_active;
                }
                $("#div_pagination").html(ul_prev+div_pagination+ul_next);
            }else if(last=='...' && first=='...'){
                center=parseInt(center);
                if(center-3<0){
                    div_pagination+='<li><a>'+(center-1)+'</a></li>';                    
                    div_pagination+='<li><a>'+center+'</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li><a>'+(center+2)+'</a></li>';
                    div_pagination+='<li class="disabled"><a>...</a></li>';                    
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_disabled;                    
                }else{                    
                    div_pagination+='<li class="disabled"><a>...</a></li>';
                    div_pagination+='<li><a>'+(center-1)+'</a></li>';
                    div_pagination+='<li><a>'+(center)+'</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li class="disabled"><a>...</a></li>'; 
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_active;
                }
                $("#div_pagination").html(ul_prev+div_pagination+ul_next);
            }
            if(li_next_text=='»'){
                get_prev_pos=$("#div_pagination ul li").get(pos_li_active-1);
                $(get_prev_pos).addClass('active');
            }else if(li_next_text=='...' || (parseInt(text_li_active)-3>0 && parseInt(text_li_active)+3<=total)){
                get_prev_pos=$("#div_pagination ul li").get(pos_li_active);
                $(get_prev_pos).addClass('active');
            }else{
                get_prev_pos=$("#div_pagination ul li").get(pos_li_active+1);
                $(get_prev_pos).addClass('active');
            }
            fnClickPagination();
        }else if (flag_click==2){
        }else if (flag_click==3){
            if(last=='...' && first!='...'){
                first=parseInt(first);
                if(first+4>=total){
                    div_pagination+='<li class="disabled"><a>...</a></li>';
                    div_pagination+='<li><a>'+(first+1)+'</a></li>';
                    div_pagination+='<li><a>'+(first+2)+'</a></li>';
                    div_pagination+='<li><a>'+(first+3)+'</a></li>';
                    div_pagination+='<li><a>'+(first+3)+'</a></li>'; 
                }else{                    
                    div_pagination+='<li class="disabled"><a>...</a></li>';
                    div_pagination+='<li><a>'+(first+1)+'</a></li>';
                    div_pagination+='<li><a>'+(first+2)+'</a></li>';
                    div_pagination+='<li><a>'+(first+3)+'</a></li>';
                    div_pagination+='<li class="disabled"><a>...</a></li>'; 
                }
                ul_prev=ul_prev_active;
                ul_next=ul_next_active;
                $("#div_pagination").html(ul_prev+div_pagination+ul_next);
            }else if (last!='...' && first=='...'){
                bootbox.alert(s_pagination_last);
                return false;
            }else if(last=='...' && first=='...'){
                center=parseInt(center);
                if(center+3>=total){
                    div_pagination+='<li><a>...</a></li>';
                    div_pagination+='<li><a>'+center+'</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li><a>'+(center+2)+'</a></li>';
                    div_pagination+='<li><a>'+(center+3)+'</a></li>'; 
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_disabled;                    
                }else{                    
                    div_pagination+='<li><a>...</a></li>';
                    div_pagination+='<li><a>'+(center+1)+'</a></li>';
                    div_pagination+='<li><a>'+(center+2)+'</a></li>';
                    div_pagination+='<li><a>'+(center+3)+'</a></li>';
                    div_pagination+='<li><a>...</a></li>'; 
                    ul_prev=ul_prev_active;
                    ul_next=ul_next_active;
                }
                $("#div_pagination").html(ul_prev+div_pagination+ul_next);
            }
            fnClickPagination();
            if(li_prev_text=='«'){
                get_prev_pos=$("#div_pagination ul li").get(pos_li_active+1);
                $(get_prev_pos).addClass('active');
            }else{
                get_prev_pos=$("#div_pagination ul li").get(pos_li_active);
                $(get_prev_pos).addClass('active');
            }
        }        
    }    
}
function fnClickPagination(){
    $("#div_pagination ul li:not(:first,:last)").on('click',function(){         
        $("#div_pagination ul li.active").removeClass('active');
        $(this).addClass('active'); 
        first=$("#div_pagination ul li:first").next().text();
        center=$("#div_pagination ul li:first").next().next().text();
        last=$("#div_pagination ul li:last").prev().text();
        flag_click=2;
        fnUpdateTableTickets(first,center,last,flag_click);
    }); 
    $("#div_pagination ul li:first").on('click',function(){
        first=$("#div_pagination ul li:first").next().text();
        center=$("#div_pagination ul li:first").next().next().text();
        last=$("#div_pagination ul li:last").prev().text();
        flag_click=1;        
        fnUpdateTableTickets(first,center,last,flag_click);        
    });
    $("#div_pagination ul li:last").on('click',function(){
        first=$("#div_pagination ul li:first").next().text();
        center=$("#div_pagination ul li:first").next().next().text();
        last=$("#div_pagination ul li:last").prev().text();
        flag_click=3;
        fnUpdateTableTickets(first,center,last,flag_click);
    });
}
function fnUpdateTableTickets(first,center,last,flag_click){     
        $.ajax({
            url:s_url_list_ticket+jq_jsonp_callback
            , data:{
                '_search' : false

                ,'rows': function(){
                    return 5; //Numero de registros por pagina
                }
                ,'page': function(){
                    return $("#div_pagination ul li.active a").text();
                }                      
            }    
            ,dataType:'json'
            , type: 'post'
            , success: function(j_response){                  
                $('#div_tickets').html('');
                if(evalResponse(j_response)){    
                    var page=j_response.page;
                    var total=j_response.total;
                    var record=j_response.record;                       
                    fnUpdateDrawPagination(page,total,record,first,center,last,flag_click);
                    var data=j_response.data;                    
                    div_tickets='';
                    $.each(data,function(i,d){
                        div_tickets+='<div class="well controls-row">';
                        div_tickets+='  <div class="span2">';
                        div_tickets+='      <div class="label label-important  padding5">';
                        div_tickets+='          <span class="class_answer">'+d.total_coments+'</span> '+s_ticket_answer;
                        div_tickets+='      </div>';
                        div_tickets+='  </div>';
                        div_tickets+='  <div class="span5">';
                        div_tickets+='      <div class="control-group">';                        
                        div_tickets+='          <a href="/private/GetTask/index/'+d.task_id+'/'+d.url+'"> '+d.task_title+'</a>';
                        div_tickets+='      </div>';                        
                        div_tickets+='      <div class="control-group">';
                        div_tickets+='          <span>'+d.task_description+'</span>';
                        div_tickets+='      </div>'; 
                        if(d.tag_name!=null){
                            var a_tag_label = (d.tag_name).split(",");                            
                            var a_tag_id = (d.tag_id).split(",");        
                            div_tickets += '            <div>';
                            $.each(a_tag_label, function(i_key, s_label){                                
                                div_tickets += '                <span class="label label-info" data-id="'+a_tag_id[i_key]+'">'+s_label+'</span>';                                
                            });
                            div_tickets += '            </div>';
                        }                      
                        div_tickets+='  </div>';                        
                        div_tickets+='  <div class="span2">';                        
                        div_tickets+='      <div><p class="muted">'+fnGetDateUpdateDiff(d.task_date)+'</p></div>';                        
                        div_tickets+='      <div><img src="/img'+d.user_image+'" /></div>';                        
                        div_tickets+='      <div><p class="muted">'+d.user_name+'</p></div>';                        
                        div_tickets+='  </div>';                        
                        div_tickets+='</div>';                        
                    });                                       
                    $("#div_tickets").html(div_tickets);                    
                    return false;
                }
            }
        }); 
    }
    function fnGetDateUpdateDiff(t_time){                       
        t_time = new Date(t_time*1000);                
        var t_current_unix_date_f = new Date();         
        var d_diff_date = t_current_unix_date_f.getTime()-t_time.getTime();        
        var d_sec = 1000;
        var d_min = 60*d_sec;
        var d_hour = d_min * 60;
        var d_day = 24 * d_hour;                
        if(d_diff_date/d_day >= 1){                    
            var d_date_update = t_time;
            return d_date_update.getDate() + ' '+month_names[d_date_update.getMonth()] ;
        }
        if(d_diff_date/d_hour >= 1){                    
            var d_hour_update = d_diff_date / d_hour;                    
            d_hour_update = parseInt(d_hour_update);                    
            if(d_hour_update > 1){
                return s_ticket_time+ d_hour_update +s_ticket_time_horas;
            }else{
                return s_ticket_time+ d_hour_update + s_ticket_time_horas;
            }                    
        }
                
        if(d_diff_date/d_min >= 1){                    
            var d_min_update = d_diff_date / d_min;
                    
            d_min_update = parseInt(d_min_update);
                    
            if(d_min_update > 1){
                return s_ticket_time+ d_min_update + s_ticket_time_minutos;
            }else{                        
                return s_ticket_time+ d_min_update + s_ticket_time_minutos;
            }                    
        }
                
        if(d_diff_date/d_sec >= 1){                    
            var d_sec_update = d_diff_date / d_sec;
                    
            d_sec_update = parseInt(d_sec_update);
                    
            if(d_sec_update > 1){
                return s_ticket_time+ d_sec_update +s_ticket_time_segundos;
            }else{                        
                return 'ahora';
            }                    
        }          
    }