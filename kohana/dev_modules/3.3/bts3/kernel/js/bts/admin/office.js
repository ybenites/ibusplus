$(document).ready(function(){
    $("#modal_office").modal({
        "backdrop": "static",
        "keyboard": true,
        "show": false
    });
    $("#foffice_city").select2({
        placeholder: s_place_holder_city,
        width: 'resolve',
        allowClear: true
    });
    
    fnGetListAllOffices('#jqgrid_office', 1);    
    
    //SUBMIT on CLICK
    $('#modal_office_btn_save').click(function() {
        $('#form_office').submit();
    });
    
    $('#btn_act_del_office').click(function() {
        $('table#jqgrid_act_del_office').trigger('reloadGrid');
        fnGetListAllOffices('#jqgrid_act_del_office', 0);
    });
    
    //VALIDATE FORM OPTION
    form_office = $('#form_office').validate({
        rules: {
            foffice_city: {
                required: true
            }
            , 
            foffice_name: {
                required: true
            }
        },
        messages: {
            foffice_city: {
                required: s_validate_required_foffice_city
            }
            , 
            foffice_name: {
                required: s_validate_required_foffice_name
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , 
        submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_office_create;
                if($('#foffice_id').val() != ''){
                    s_url = s_url_office_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , 
                    data: $(o_form).serialize()
                    , 
                    beforeSend: function() {
                        $('#modal_office_btn_save').button('loading');
                    }
                    , 
                    complete: function() {
                        $('#modal_office_btn_save').button('reset');
                    }
                    , 
                    success: function(j_response) {
                        if(evalResponse(j_response)){                            
                            $('#modal_office').modal('hide');
                            bootbox.alert(s_office_saved);
                            $('table#jqgrid_office').trigger('reloadGrid');
                            fnGetListAllOffices('#jqgrid_office', 0);
                        }
                    }
                });
            }
        }
    });
    
    $('table#jqgrid_office').on('click', '.office_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_office_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , 
                success: function(j_response) {                    
                    if(evalResponse(j_response)){    
                        var o_office = j_response.data;
                        
                        $('#foffice_id').val(o_office.office_id);
                        $('#foffice_city').select2("val", [o_office.office_city]).trigger('change');
                        $('#foffice_name').val(o_office.office_name);
                        $('#foffice_address').val(o_office.office_address);
                        $('#foffice_phone').val(o_office.office_phone);
                        $('input[name=foffice_root_option]').val([o_office.office_root_option]);
                        
                        $('#modal_office').modal('show');
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_office').on('click', '.office_delete', function() {
        fnUpdateStatusOffice(this, 0, s_dialog2_button_label_confirmation_delete_text, s_office_deleted);
    });    
    
    $('table#jqgrid_act_del_office').on('click', '.office_revert_opt', function() {
        fnUpdateStatusOffice(this, 1, s_dialog2_button_label_confirmation_activate_text, s_office_activated);        
    });
    
    $('table#jqgrid_act_del_office').on('click', '.office_delete_opt', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_office_confirm_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , 
                            success: function(j_response) {
                                if(evalResponse(j_response)){    
                                    bootbox.alert(s_office_deleted);
                                    $('table#jqgrid_act_del_office').trigger('reloadGrid');
                                    fnGetListAllOffices('#jqgrid_act_del_office', 0);
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
    $('#modal_office').on('hidden', function() {
        $('#foffice_city').select2("val", "").trigger('change');
        form_office.currentForm.reset();
        form_office.resetForm();
        $('input[type=hidden]', form_office.currentForm).val('');
        $('input, select, textarea', form_office.currentForm).parents('.control-group').removeClass('error');
    });    

    function fnGetListAllOffices(selector, opt){
        var o_param = new Object();
        o_param.opt = opt;
        jqgrid_office = $(selector).jqGrid({
            url: s_url_office_list,
            datatype: 'json',
            postData: o_param,
            mtype: 'POST',
            colNames: a_jqgrid_office_col_names,
            colModel: a_jqgrid_office_col_model,
            pager: selector+"_pager",        
            height: 'auto',              
            viewrecords: true,   
            rownumbers: true,
            rowNum: 50,
            rowList: [50,100,-1],
            sortname: 'of.idOffice',
            afterInsertRow: function(row_id, row_data){
                var tt = '';                
                if(opt == 0){
                    var s_html_office_delete_opt = '<a data-id=' + row_id + ' class="btn btn-mini office_delete_opt"><i class="icon-remove-sign"></i></a>';
                    var s_html_office_revert_opt = '<a data-id=' + row_id + ' class="btn btn-mini office_revert_opt"><i class="icon-circle-arrow-left"></i></a>';
                    tt = s_html_office_revert_opt+s_html_office_delete_opt;                    
                }else{
                    var s_html_office_edit = '<a data-id=' + row_id + ' class="btn btn-mini office_edit"><i class="icon-pencil"></i></a>';
                    var s_html_office_delete = '<a data-id=' + row_id + ' class="btn btn-mini office_delete"><i class="icon-trash"></i></a>';                    
                    tt = s_html_office_edit +s_html_office_delete;
                }
                jqgrid_office.setRowData(row_id, {
                    office_actions: tt
                });
            },
            gridComplete: function() {            
                $(jqgrid_office.getGridParam('pager')).find("option[value=-1]").text(s_office_select_text_all);           
            }
        });
    }
    
    function fnUpdateStatusOffice(thes, sts, msg, confirm){
        var o_param = new Object();
        var $this = $(thes);
        o_param.sts = sts;
        o_param.id = $this.data('id');
        if (b_ajax) {            
            bootbox.confirm(msg, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_office_delete + jq_jsonp_callback,
                            data: o_param, 
                            success: function(j_response) {
                                if(evalResponse(j_response)){
                                    bootbox.alert(confirm);
                                    $('table#jqgrid_office').trigger('reloadGrid');                                    
                                    fnGetListAllOffices('#jqgrid_office', 1);
                                    if(sts == 1){
                                        $('#modal_activate_delete_office').modal('hide');
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }        
    }    
    
});