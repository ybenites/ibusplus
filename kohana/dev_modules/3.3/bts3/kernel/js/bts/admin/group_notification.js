var jqgrid_group_notification;
$(document).ready(function(){
    
    $('#fgroup_notification_user_list_user_id').select2({        
        width: 'resolve'
    });
    
    jqgrid_group_notification = $('#jqgrid_group_notification').jqGrid({
        url: s_url_group_notification_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_group_notification_col_names,
        colModel: a_jqgrid_group_notification_col_model,
        pager: "#jqgrid_group_notification_pager",        
        height: 'auto',              
        viewrecords: true,        
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'gn.idGroupNotification',
        
        afterInsertRow: function(row_id, row_data){            
            //Actions
            var s_html_group_notification_edit = '<a data-id=' + row_id + ' class="btn btn-mini group_notification_edit"><i class="icon-pencil"></i></a>',
            s_html_group_notification_delete = '<a data-id=' + row_id + ' class="btn btn-mini group_notification_delete"><i class="icon-trash"></i></a>',
            s_html_group_notification_assign = '<a href="#modal_group_notification_user_list" role="button" data-toggle="modal" data-id=' + row_id + ' class="btn btn-mini group_notification_assign"><i class="icon-user"></i></a>';

            jqgrid_group_notification.setRowData(row_id, {
                group_notification_actions: s_html_group_notification_edit + s_html_group_notification_delete + s_html_group_notification_assign
            });
        },
        gridComplete: function() {            
            $(jqgrid_group_notification.getGridParam('pager')).find("option[value=-1]").text(s_group_notification_select_text_all);           
        }
    });
    
    jqgrid_group_notification_user_list = $('#jqgrid_group_notification_user_list').jqGrid({
        url: s_url_group_notification_user_list_list,
        datatype: 'json',
        postData:{
            group_id : function(){
                return $('#fgroup_notification_user_list_group_id').val();
            }
        },
        mtype: 'POST',
        colNames: a_jqgrid_group_notification_user_list_col_names,
        colModel: a_jqgrid_group_notification_user_list_col_model,
        pager: "#jqgrid_group_notification_user_list_pager",        
        height: 250,              
        viewrecords: true,        
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'gn.idGroupNotification',
        
        afterInsertRow: function(row_id, row_data){            
            //Actions
            var s_html_group_notification_user_list_delete = '<a data-id=' + row_id + ' class="btn btn-mini group_notification_user_list_unassign"><i class="icon-trash"></i></a>';
            
            jqgrid_group_notification_user_list.setRowData(row_id, {
                group_notification_user_list_actions: s_html_group_notification_user_list_delete
            });
        },
        gridComplete: function() {            
            $(jqgrid_group_notification_user_list.getGridParam('pager')).find("option[value=-1]").text(s_group_notification_select_text_all);           
        }
    });
    
    //SUBMIT on CLICK
    $('#modal_group_notification_btn_save').click(function() {
        $('#form_group_notification').submit();
    });
    
    //VALIDATE FORM OPTION
    form_group_notification = $('#form_group_notification').validate({
        rules: {
            fgroup_notification_name: 'required'            
        },
        messages: {
            fgroup_notification_name: {
                required: s_validate_required_fgroup_notification_name
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_group_notification_create;
                if($('#fgroup_notification_id').val() != ''){
                    s_url = s_url_group_notification_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_group_notification_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_group_notification_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                         if (evalResponse(j_response)) {                          
                            $('#modal_group_notification').modal('hide');
                            bootbox.alert(s_group_notification_saved);
                            jqgrid_group_notification.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    
    $('#modal_group_notification').on('hidden', function() {        
        form_group_notification.currentForm.reset();
        form_group_notification.resetForm();
        $('input[type=hidden]').val('');
        $('input, select, textarea', form_group_notification.currentForm).parents('.control-group').removeClass('error');
    });
    
    $('table#jqgrid_group_notification').on('click', '.group_notification_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_notification_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                     if (evalResponse(j_response)) { 
                        var o_group_notification = j_response.data;
                        $('#fgroup_notification_id').val(o_group_notification.group_notification_id);                        
                        $('#fgroup_notification_name').val(o_group_notification.group_notification_name);                        
                        $('#modal_group_notification').modal('show');
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_group_notification').on('click', '.group_notification_delete', function() {
        if (b_ajax) {
            var $this = $(this);
              bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_group_notification_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {                                    
                                    bootbox.alert(s_group_notification_deleted);
                                    jqgrid_group_notification_user_list.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_group_notification').on('click', '.group_notification_assign', function() {
        var $this = $(this);
        
        $('#fgroup_notification_user_list_group_id').val($this.data('id'));
        
        jqgrid_group_notification_user_list.trigger('reloadGrid');
    });
    
    //SUBMIT on CLICK
    $('#modal_group_notification_user_list_btn_save').click(function() {
        $('#form_group_notification_user_list').submit();
    });
    
    //VALIDATE FORM OPTION
    form_group_notification_user_list = $('#form_group_notification_user_list').validate({
        rules: {
            fgroup_notification_user_list_user_id: 'required'            
        },
        messages: {
            fgroup_notification_user_list_user_id: {
                required: s_validate_required_fgroup_notification_user_list_user_id
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {                
                $.ajax({
                    url: s_url_group_notification_user_list_assign + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_group_notification_user_list_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_group_notification_user_list_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (j_response.code == jq_json_code_success) {
                            bootbox.alert(s_group_notification_user_list_saved);
                            jqgrid_group_notification_user_list.trigger('reloadGrid');
                        } else {
                            bootbox.alert(j_response.msg);
                        }
                    }
                });
            }
        }
    });
    
    $('table#jqgrid_group_notification_user_list').on('click', '.group_notification_user_list_unassign', function() {
        if (b_ajax) {
            var $this = $(this);
             bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_group_notification_user_list_unassign + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {                                    
                                    bootbox.alert(s_group_notification_user_list_deleted);
                                    jqgrid_group_notification_user_list.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
});