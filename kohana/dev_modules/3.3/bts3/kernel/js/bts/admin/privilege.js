var jqgrid_privilege;
$(document).ready(function() {
    // JQGRID OPTION
    jqgrid_privilege = $('#jqgrid_privilege').jqGrid({
        url: s_url_privilege_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_privilege_col_names,
        colModel: a_jqgrid_privilege_col_model,
        pager: "#jqgrid_privilege_pager",
        height: 'auto',
        viewrecords: false,
        rowNum: -1,
        sortname: 'ctr.controller',
        grouping: true,
        groupingView: {
            groupField: ['privilege_directory']
            , groupColumnShow: [false]
            , groupText: ['<b>{0} - ({1})</b>']
            , groupCollapse: false
            , groupOrder: ['asc']
            , groupSummary: [false]
            , showSummaryOnHide: true
            , groupDataSorted: true
            , plusicon: 'icon-plus'
            , minusicon: 'icon-minus'
        },
        gridComplete: function() {
            var a_ids = jqgrid_privilege.getDataIDs();

            $.each(a_ids, function(i, i_id) {
                var o_data = jqgrid_privilege.getRowData(i_id);

                $.each(bts_a_groups, function(j, o_group){
                    var s_html_privilege_update = "<a data-privilege_group_id='"+o_group.group_id+"' data-privilege_directory='"+o_data.privilege_directory+"' data-privilege_controller='"+o_data.privilege_controller+"' data-privilege_type='"+o_data.privilege_type_hidden+"' class='privilege_change icon-remove'></a>";
                    if(o_data['group_'+o_group.group_id]==1){                        
                        s_html_privilege_update = "<a data-privilege_group_id='"+o_group.group_id+"' data-privilege_directory='"+o_data.privilege_directory+"' data-privilege_controller='"+o_data.privilege_controller+"' data-privilege_type='"+o_data.privilege_type_hidden+"' class='privilege_change icon-ok'></a>";                        
                    }
                    
                    var o_insert = {};
                    eval("o_insert."+'group_'+o_group.group_id+" = \""+ s_html_privilege_update + "\";");
                    jqgrid_privilege.setRowData(i_id, o_insert);
                });

            });
        }
    });
    
    $('table#jqgrid_privilege').on('click', '.privilege_change', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_privilege_change + jq_jsonp_callback,
                data: $this.data()
                , success: function(j_response) {
                     if(evalResponse(j_response)){
                        jqgrid_privilege.trigger('reloadGrid');
                         }
                    }
            });
        }
    });
});