var jqgrid_group;
$(document).ready(function() {
    jqgrid_group = $('#jqgrid_group').jqGrid({
        url: s_url_group_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_group_col_names,
        colModel: a_jqgrid_group_col_model,
        pager: "#jqgrid_group_pager",
        height: 'auto',
        rownumbers: true,
        viewrecords: true,
        rowNum: 50,
        rowList: [50, 100, -1],
        sortname: 'gr.idGroup',
        afterInsertRow: function(row_id, row_data) {
            var s_root_option_icon = 'icon-minus',
                    s_allow_chat_icon = 'icon-minus',
                    s_allow_notifications_show_icon = 'icon-minus',
                    s_allow_notifications_icon = 'icon-minus',
                    s_status_icon = 'icon-ban-circle';
            if (row_data.group_root_option == 1) {
                s_root_option_icon = 'icon-star';
            }
            if (row_data.group_allow_chat == 1) {
                s_allow_chat_icon = 'icon-comment';
            }
            if (row_data.group_allow_notifications_show == 1) {
                s_allow_notifications_show_icon = 'icon-bullhorn';
            }
            if (row_data.group_allow_notifications == 1) {
                s_allow_notifications_icon = 'icon-edit';
            }
            if (row_data.group_status == 1) {
                s_status_icon = 'icon-ok-circle';
            }

            var s_html_group_edit = '<a data-id=' + row_id + ' class="btn btn-mini group_edit"><i class="icon-pencil"></i></a>';

            jqgrid_group.setRowData(row_id, {
                group_root_option: '<a data-id=' + row_id + ' class="group_change_root_option"><i class="' + s_root_option_icon + '"></i></a>',
                group_allow_chat: '<a data-id=' + row_id + ' class="group_change_allow_chat"><i class="' + s_allow_chat_icon + '"></i></a>',
                group_allow_notifications_show: '<a data-id=' + row_id + ' class="group_change_allow_notifications_show"><i class="' + s_allow_notifications_show_icon + '"></i></a>',
                group_allow_notifications: '<a data-id=' + row_id + ' class="group_change_allow_notifications"><i class="' + s_allow_notifications_icon + '"></i></a>',
                group_status: '<a data-id=' + row_id + ' class="group_change_status"><i class="' + s_status_icon + '"></i></a>',
                group_actions: s_html_group_edit
            });

        },
        gridComplete: function() {
            $(jqgrid_group.getGridParam('pager')).find("option[value=-1]").text(s_group_select_text_all);
            $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
        }
    });

    //SUBMIT on CLICK
    $('#modal_group_btn_save').click(function() {
        $('#form_group').submit();
    });

    //VALIDATE FORM OPTION
    form_group = $('#form_group').validate({
        rules: {
            fgroup_name: {
                required: true
            }
            , fgroup_default_page: {
                required: true
            }
        },
        messages: {
            fgroup_name: {
                required: s_validate_required_fgroup_name
            }
            , fgroup_default_page: {
                required: s_validate_required_fgroup_default_page
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_group_create;
                if ($('#fgroup_id').val() != '') {
                    s_url = s_url_group_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                            , data: $(o_form).serialize()
                            , beforeSend: function() {
                        $('#btn_process_form_group').button('loading');
                    }
                    , complete: function() {
                        $('#btn_process_form_group').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_group').modal('hide');
                            bootbox.alert(s_group_saved);
                            jqgrid_group.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });

    $('#modal_group').on('hidden', function() {
        form_group.currentForm.reset();
        form_group.resetForm();
        $('input[type=hidden]', form_group.currentForm).val('');
        $('input, select, textarea', form_group.currentForm).parents('.control-group').removeClass('error');
    });

    $('table#jqgrid_group').on('click', '.group_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            showloadingDialog = 1;
            $.ajax({
                url: s_url_group_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var o_group = j_response.data;

                        $('#fgroup_id').val(o_group.group_id);
                        $('#fgroup_name').val(o_group.group_name);
                        $('#fgroup_default_page').val(o_group.group_default_page);

                        $('input[name=fgroup_root_option]').val([o_group.group_root_option]);
                        $('input[name=fgroup_allow_chat]').val([o_group.group_allow_chat]);
                        $('input[name=fgroup_allow_notifications]').val([o_group.group_allow_notifications]);
                        $('input[name=fgroup_allow_notifications_show]').val([o_group.group_allow_notifications_show]);

                        $('#modal_group').modal('show');
                    }
                }
            });
        }
    });

    $('table#jqgrid_group').on('click', '.group_change_root_option', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_change_root_option + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_group_saved);
                        jqgrid_group.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('table#jqgrid_group').on('click', '.group_change_allow_chat', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_change_allow_chat + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_group_saved);
                        jqgrid_group.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('table#jqgrid_group').on('click', '.group_change_allow_notifications', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_change_allow_notifications + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_group_saved);
                        jqgrid_group.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('table#jqgrid_group').on('click', '.group_change_allow_notifications_show', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_change_allow_notifications_show + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_group_saved);
                        jqgrid_group.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('table#jqgrid_group').on('click', '.group_change_status', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_group_change_status + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_group_saved);
                        jqgrid_group.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    jqgrid_group_active();
    $('table#jqgrid_group_active').on('click', '.group_remove_final', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_group_remove_final + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_group_deleted);
                                    jqgrid_group_active.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
        $('table#jqgrid_group_active').on('click', '.group_active', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_active_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_group_active+ jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_group_active);
                                    jqgrid_group.trigger('reloadGrid');
                                    jqgrid_group_active.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    $("#modal_group_active").on('show',function(){
        jqgrid_group_active.trigger('reloadGrid');
    });
});

function jqgrid_group_active(){
    jqgrid_group_active = $('#jqgrid_group_active').jqGrid({
        url: s_url_group_active_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_group_active_col_names,
        colModel: a_jqgrid_group_active_col_model,
        pager: "#jqgrid_group_active_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        rowList: [50, 100, -1],
        sortname: 'gr.idGroup',
        afterInsertRow: function(row_id, row_data) {          
            var s_html_group_active= '<a data-id=' + row_id + ' class="btn btn-mini group_active"><i class="icon-circle-arrow-left"></i></a>',
                    s_html_group_delete = '<a data-id=' + row_id + ' class="btn btn-mini group_remove_final"><i class="icon-remove-sign"></i></a>';

            jqgrid_group_active.setRowData(row_id, {                
                group_actions: s_html_group_active + s_html_group_delete
            });
        },
        gridComplete: function() {
            $(jqgrid_group_active.getGridParam('pager')).find("option[value=-1]").text(s_group_select_text_all);
        }
    });
}