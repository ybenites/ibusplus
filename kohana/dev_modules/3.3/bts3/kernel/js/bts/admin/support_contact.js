$(document).ready(function() {

    function loadSupportEmails() {
        $.ajax({
            url: s_url_load_contact_emails
                    , success: function(response) {
                if (evalResponse(response)) {
                    var ul_list = $('#form_support_email ul');
                    $.each(response.data.support_emails, function(idx, obj) {
                        addSupportContact(ul_list, obj, response.data.support_name[idx])
                    });
                }
            }
        });
    }
    loadSupportEmails();

    function addSupportContact(ul_list, name, email) {
        var input_email = $(document.createElement('input'))
                .attr('type', 'text')
                .attr('placeholder', s_input_email)
                .attr('name', 'support_mails[]').blur(validateEmail);
        if (typeof name !== 'undefined')
            input_email.val(name);
        var input_name = $(document.createElement('input'))
                .attr('type', 'text')
                .attr('placeholder', s_input_name)
                .attr('name', 'support_team[]');
        if (typeof name !== 'undefined')
            input_name.val(email);

        var li = $(document.createElement('li'))
                .append($(document.createElement('i')).addClass('icon-info-sign'))
                .append(input_name)
                .append('&nbsp;')
                .append(input_email);

        var remove = $(document.createElement('a'))
                .addClass('remove_support_team btn btn-small')
                .append($(document.createElement('i')).addClass('icon-minus-sign'))
                .append(s_btn_remove);
        input_email.after(remove);
        ul_list.append(li);
    }
    
    $('form#form_support_email').on('click','a.remove_support_team',function(){
        if($('#form_support_email li').size() >= 2)
            $(this).parent().remove();
        else
            bootbox.alert(s_input_empty_list_validate);
    });
    
    $('a#add_contact_support').click(function() {
        addSupportContact($('#form_support_email ul'));
    });

    form_support_contact = $('#form_support_email').validate({
        submitHandler: function(o_form) {
            if (validateFields()) {
                if (b_ajax) {
                    $.ajax({
                        url: s_url_save_contact_emails
                                , data: $(o_form).serializeObject()
                                , success: function(j_response) {
                            if (evalResponse(j_response)) {
                                bootbox.alert(j_response.msg);
                            }
                        }
                    });
                }
            } else {
                bootbox.alert(s_input_empty_validate);
            }
        }
    });

    $('#save_contact_support').click(function() {
        $('#form_support_email').submit();
    });

    function validateEmail() {
        var regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if (!regex.test($(this).val())) {
            $(this).val('');
        }
    }
    function validateFields() {
        return $('input[name^="support_mails"],input[name^="support_team"]').filter(function() {
            return $.trim($(this).val()) == '';
        }).size() === 0;
    }
});

