//VARs to USE on WINDOWS
$(document).ready(function() {
    $(".item_core_js").hover(
            function() {
                $("#" + $(this).children('div').children('.change-option').attr('id')).show();
            },
            function() {
                $("#" + $(this).children('div').children('.change-option').attr('id')).hide();
            }
    );
    $(".desc-option").hover(
            function() {
                $(this).addClass('tdecortunder');
            },
            function() {
                $(this).removeClass('tdecortunder');
            }
    ).click(function() {
        var text = $(this).data('ldesc');
        if (text == "") {
            $(".text-show-desc").addClass('color-alternate');
            text = s_option_no_long_description;
        } else {
            $(".text-show-desc").removeClass('color-alternate');
        }
        $(".text-show-desc").text(text);
        $('.show-description').removeClass('hide');
    });
    $('.txt_color').colorpicker();
    $(".icon-ban-circle").click(function() {
        $('.show-description').addClass('hide');
    });
    $(".change-option").click(function() {
        var id = $(this).attr('id');
        $("#icon_ok_" + id + ", #icon_remove_" + id).removeClass('hide');
        switch ($(this).data("type")) {
            case "BOOLEAN":
                $("#input_boolean_" + id).removeClass('hide');
                break;
            case "COLOR":
                $("#input_color_" + id).removeClass('hide');
                break;
            case "INTEGER":
                $("#input_integer_" + id).removeClass('hide');
                break;
            case "STRING":
                $("#input_string_" + id).removeClass('hide');
                break;
            default:
        }
        $("#" + id).text("");
    });
    $(".icon-remove").click(function() {
        var id = $(this).attr('id').split('_').slice(-3).slice(-2).join('_');
        var dt = $(this).data("type");
        var nt = $("#button_" + id).children('span').text();
        fnHideOptForUpdate(id, dt);
        if (dt == 'BOOLEAN') {
            var $radios = $('input:radio[name=radio_' + id + ']');
            if (nt === 'Activado') {
                $radios.filter('[value=on]').prop('checked', true);
            } else {
                $radios.filter('[value=off]').prop('checked', true);
            }
        } else {
            $(this).parent().children('.input_option').children('input').val(nt);
        }
        $("#" + id).text("Cambiar");
    });
    $(".icon-ok").click(function() {
        var id = $(this).attr('id').split('_').slice(-3).slice(-2).join('_');
        var dt = $(this).data("type");
        var o_param = new Object();
        switch (dt) {
            case "BOOLEAN":
                var ibool = $("#input_boolean_" + id).children('.i-right').children('input');
                o_param.key = ibool.attr('id');
                o_param.val = $("input[name='radio_" + id + "']:checked").val();
                break;
            case "COLOR":
                var icolor = $("#input_color_" + id).children('input');
                o_param.key = icolor.attr('id');
                o_param.val = icolor.val();
                break;
            case "INTEGER":
                var iinteger = $("#input_integer_" + id).children('input');
                o_param.key = iinteger.attr('id');
                o_param.val = iinteger.val();
                break;
            case "STRING":
                var istring = $("#input_string_" + id).children('input');
                o_param.key = istring.attr('id');
                o_param.val = istring.val();
                break;
            default:
                break;
        }
        if (o_param.val == "") {
            bootbox.alert(s_option_no_value_in_text);
            return false;
        }
        if (b_ajax) {
            $.ajax({
                url: s_url_option_update_value + jq_jsonp_callback,
                data: o_param,
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        fnHideOptForUpdate(id, dt);
                        var val = o_param.val;
                        if (dt == 'COLOR') {
                            $("#button_" + id).css('background-color', o_param.val);
                        }
                        if (dt == 'BOOLEAN') {
                            var tx = s_option_boolean_deactivated;
                            if (o_param.val == 1) {
                                tx = s_option_boolean_activated;
                            }
                            $("#button_" + id).text(tx);
                        } else {
                            $("#button_" + id).text(val.length > 11 ? val.substring(0, 10) + '...' : val).attr('title', val).css('font-size', '11px');
                        }
                        bootbox.alert(s_option_again_login, function() {});
                    }
                }
            });
        }
    });

    function fnHideOptForUpdate(id, dt) {
        $("#icon_ok_" + id + ", #icon_remove_" + id).addClass('hide');
        switch (dt) {
            case "BOOLEAN":
                $("#input_boolean_" + id).addClass('hide');
                break;
            case "COLOR":
                $("#input_color_" + id).addClass('hide');
                break;
            case "INTEGER":
                $("#input_integer_" + id).addClass('hide');
                break;
            case "STRING":
                $("#input_string_" + id).addClass('hide');
                break;
            default:
        }
        $("#" + id).text(s_option_change);
    }

});
