var a_bts_users_notification = [];
var s_url_notification_get_user_list = '/private/notification/getUsers.json';
$(document).ready(function() {
    $('#btnCleaningCache').on('click', function(e) {
        e.preventDefault();
        showloadingDialog = 1;
        $.ajax({
            url: '/private/admin/cleaningCache' + jq_jsonp_callback,
            data: {},
            success: function(j_response) {
                if (j_response.code == jq_json_code_success) {
                    bootbox.alert(s_proccesOk, function() {
                    location.reload();
                 });
                } else {                    
                    bootbox.alert(j_response.msg);
                }
            }
        });
    });
    $('#btnRegenerateLanguages').on('click', function(e) {
        e.preventDefault();
        showloadingDialog = 1;
        $.ajax({
            url: '/private/admin/cleaningLanguages' + jq_jsonp_callback,
            data: {},
            success: function(j_response) {
                if (j_response.code == jq_json_code_success) {
                    bootbox.alert(s_proccesOk, function() {
                    location.reload();
                 });
                } else {                    
                    bootbox.alert(j_response.msg);
                }
            }
        });
    });
    $('#btncleaningSessionUsers').on('click', function(e) {
        showloadingDialog = 1;
        e.preventDefault();
        $.ajax({
            url: '/private/admin/cleaningSessionsUsers' + jq_jsonp_callback,
            data: {},
            success: function(j_response) {
                 if (j_response.code == jq_json_code_success) {
                    bootbox.alert(s_proccesOk, function() {
                    location.reload();
                 });
                } else {                    
                    bootbox.alert(j_response.msg);
                }
            }
        });
    });

    $('#btnactivateDeactivatedSystem').on('click', function(e) {
        showloadingDialog = 1;
        e.preventDefault();
        bootbox.dialog(s_activateDeactivatedSystem, [{
                "label": s_deactivated,
                "class": "btn-success",
                "callback": function() {
                           $.ajax({
                            url: '/private/admin/activateDeactivatedSystem' + jq_jsonp_callback,
                            data: {activateDeactivated:0},
                            success: function(j_response) {
                                 if (j_response.code == jq_json_code_success) {
                                    bootbox.alert(s_proccesOk, function() {
                                    location.reload();
                                 });
                                } else {                    
                                    bootbox.alert(j_response.msg);
                                }
                            }
                        });
                }
            }, {
                "label": s_activated,
                "class": "btn-danger",
                "callback": function() {
                            $.ajax({
                            url: '/private/admin/activateDeactivatedSystem' + jq_jsonp_callback,
                            data: {activateDeactivated:1},
                            success: function(j_response) {
                                 if (j_response.code == jq_json_code_success) {
                                    bootbox.alert(s_proccesOk, function() {
                                    location.reload();
                                 });
                                } else {                    
                                    bootbox.alert(j_response.msg);
                                }
                            }
                        });
                }            
            }, {
                "label": "Cancelar",
                "class": "btn-danger",
                "callback": function() {

                }
            }]
    );

    });
    $('#user_info').click(function() {
        userInfo();
        return false;
    });
    if (b_fnGetAllowNotificationsShow) {
        var msgData = {};
        var notification_ids = [];
        var i_message_count = 0;
        var month_names = new Array( );
        month_names[month_names.length] = s_mes_en;
        month_names[month_names.length] = s_mes_feb;
        month_names[month_names.length] = s_mes_mar;
        month_names[month_names.length] = s_mes_abr;
        month_names[month_names.length] = s_mes_may;
        month_names[month_names.length] = s_mes_jun;
        month_names[month_names.length] = s_mes_jul;
        month_names[month_names.length] = s_mes_ago;
        month_names[month_names.length] = s_mes_set;
        month_names[month_names.length] = s_mes_oct;
        month_names[month_names.length] = s_mes_nov;
        month_names[month_names.length] = s_mes_dic;
        function fnGetDateUpdate(t_time) {
            t_time = new Date(t_time * 1000);
            t_current_unix_date_f = new Date(t_current_unix_date * 1000);
            d_diff_date = t_current_unix_date_f.getTime() - t_time.getTime();
            d_sec = 1000;
            d_min = 60 * d_sec;
            d_hour = d_min * 60;
            d_day = 24 * d_hour;
            if (d_diff_date / d_day >= 1) {
                var d_date_update = t_time;
                return d_date_update.getDate() + ' ' + month_names[d_date_update.getMonth()];
            }
            if (d_diff_date / d_hour >= 1) {
                var d_hour_update = d_diff_date / d_hour;
                d_hour_update = parseInt(d_hour_update);
                if (d_hour_update > 1) {
                    return s_ago + d_hour_update + s_hours;
                } else {
                    return s_ago + d_hour_update + s_hour;
                }
            }
            if (d_diff_date / d_min >= 1) {
                var d_min_update = d_diff_date / d_min;

                d_min_update = parseInt(d_min_update);

                if (d_min_update > 1) {
                    return s_ago + d_min_update + s_minutes;
                } else {
                    return s_ago + d_min_update + s_minute;
                }
            }
            if (d_diff_date / d_sec >= 1) {
                var d_sec_update = d_diff_date / d_sec;

                d_sec_update = parseInt(d_sec_update);

                if (d_sec_update > 1) {
                    return s_ago + d_sec_update + s_seconds;
                } else {
                    return s_now;
                }
            }
        }
        var bAvailableSaveMessage = true;
        $li_notification_response = null;
        var timerAlert = null;
        var notification = {
            request_url: "/private/notification/get.json" + jq_jsonp_callback,
            do_request: function() {
                $.ajax({
                    url: notification.request_url,
                    data: {lastNotification: lastNotification},
                    error: function() {
                        //setTimeout( function(){notification.do_request();}, 5000);
                    },
                    success: function(j_response) {
                        if (evalResponse(j_response)) {
                            o_data = j_response.data;

                            $.each(o_data, function(i, value) {
                                if (value.hasOwnProperty('idOut')) {
                                    //console.log(value);
                                    i_message_count++;
                                    $('#sound_notification').get(0).play();
                                    li = '<li id="message_id_' + value.idOut + '" unix_time="' + value.updateNotification + '" class="message_new notification li_message">';
                                    li += ' <div class="div_img_action_msg' + (value.responseOut > 0 ? ' div_img_action_response' : '') + '"></div>';
                                    li += ' <div class="div_notification">';
                                    li += '     <div class="general_notification_image ui-corner-all ui-widget-header" style="background-image: url(' + value.user_image + ');background-position: center center;background-repeat:no-repeat;background-size: 100%;">';
                                    li += '     </div>';
                                    li += '     <div class="general_notification_content">';
                                    li += '         <div class="row_notification"><span class="user_name">' + value.userRegistrationOut + '</span> <span class="message_date">' + value.dateCreationOut + '</span></div>';
                                    li += '         <div class="row_notification"><div class="notification_text">' + value.messageOut + '</div></div>';
                                    li += '     </div>';
                                    li += '     <div class="clearfix"></div>';
                                    li += '     <div class="general_notification_footer">';
                                    li += '         <span class="general_time"></span>';
                                    li += '         <div class="clearfix"></div>';
                                    li += '     </div>';
                                    li += ' </div>';
                                    li += '</li>';
                                    $('ul#ulNotification').prepend(li);
                                    lastNotification = value.updateNotification;
                                    notification_ids.push(value.idOut);
                                }
                            });

                            if (o_data.hasOwnProperty('date')) {
                                t_current_unix_date = o_data.date.current_unix_date;

                                $.each($('ul#ulNotification li.notification'), function() {
                                    t_temp_unix_time = $(this).attr('unix_time');
                                    $(this).find('span.general_time').text(fnGetDateUpdate(t_temp_unix_time));
                                });
                            }
                            if (i_message_count > 0) {
                                $('#bts_view_notification')
                                        .parents('li')
                                        .addClass('open');
                                $('#bts_view_notification span.notification')
                                        .removeClass('hidden')
                                        .html(i_message_count)
                                        .effect("pulsate", {times: 5}, 1000);
                                $('#sound_notification').get(0).play();
                            } else {
                                $('#bts_view_notification')
                                        .parents('li')
                                        .removeClass('open');
                                $('#bts_view_notification span.notification')
                                        .addClass('hidden')
                                        .html(0);
                                $("#sound_notification").get(0).pause();
                            }
                        }
                        setTimeout(function() {
                            notification.do_request();
                        }, d_SYSTEM_TIME_REFRESH_MESSAGES);
                    }
                });
            }
        };
        notification.do_request();
        var b_available_view_notification_ajax = true;
        click_ulNotification = false;
        $('#ulNotification, #bts_view_notification, #notification_response_div, #notification_response_view_div').hover(function() {
            click_ulNotification = true;
        }, function() {
            click_ulNotification = false;
        });
    }
    $('#bts_write_notification').click(function() {
        notifications();
    });
    $('#bts_view_notification').click(function() {
        var $this = $(this);

        if ($this.data('clicked')) {
            $this.data('clicked', false);
            $('#menu_cont_notification').hide();//.css('z-index', 0);
            // $('div#content').css('z-index', 30);
            $('#ulNotification').hide();
            $('#notification_response_view_div').hide();
            if ($li_notification_response != null)
                $li_notification_response.removeClass('li_post_notification');
            $('#notification_response_div').hide();
        } else {
            $this.data('clicked', true);
            $('#menu_cont_notification').show();
            if (i_message_count > 0) {
                if (b_ajax) {
                    $.ajax({
                        url: "/private/notification/update.json" + jq_jsonp_callback,
                        data: {lastNotification: lastNotification}
                    });
                }
            }
            i_message_count = 0;

            $('#bts_view_notification')
                    .parents('li')
                    .removeClass('open');
            $('#bts_view_notification span.notification')
                    .addClass('hidden')
                    .html(0);
            $('#ulNotification').show();
            $.each(notification_ids, function() {
                $("#message_id_" + this).show('highlight');
            });
            notification_ids = [];
            clearTimeout(timerAlert);
            $("#sound_notification").get(0).pause();
        }

    });
    $("html").click(function() {
        if (!click_ulNotification) {
            $('#bts_view_notification').data('clicked', false);
            $('div#content').css('z-index', 30);
            $('#ulNotification').hide();
            $('#menu_cont_notification').hide();
            $('#notification_response_view_div').hide();
            if ($li_notification_response != null)
                $li_notification_response.removeClass('li_post_notification');
            $('#notification_response_div').hide();
        }
    });

    $('#f_instant_close').click(function() {
        $li_notification_response.removeClass('li_post_notification');
        $('#notification_response_div').hide();
        f_instant_response.currentForm.reset();
        f_instant_response.resetForm();
        $('input, select, textarea', f_instant_response.currentForm).removeClass('ui-state-error');
    });
    $('div.more_notification').click(function() {
        if (b_ajax) {
            $.ajax({
                url: "/private/notification/previous.json" + jq_jsonp_callback,
                data: {n_page: n_page, lastNotification: i_last_notification},
                beforeSend: function() {
                    $('div.more_notification').append($('<img/>').attr('src', '/img/ajax-loading.gif').css('vertical-align', 'middle'));
                },
                error: function() {
                    $('div.more_notification img').remove();
                },
                success: function(j_response) {
                    $('div.more_notification img').remove();
                    if (evalResponse(j_response)) {
                        o_data = j_response.data;
                        n_page++;
                        o_data = j_response.data;
                        $.each(o_data, function() {
                            if (this.hasOwnProperty('idOut')) {
                                li = '<li id="message_id_' + this.idOut + '" unix_time="' + this.updateNotification + '" class="notification li_message">';
                                li += ' <div class="div_img_action_msg' + (this.responseOut > 0 ? ' div_img_action_response' : '') + '"></div>';
                                li += ' <div class="div_notification">';
                                li += '     <div class="general_notification_image ui-corner-all ui-widget-header" style="background-image: url(' + this.user_image + ');background-position: center center;background-repeat: no-repeat;background-size: 100%;">';
                                li += '     </div>';
                                li += '     <div class="general_notification_content">';
                                li += '         <div class="row_notification"><span class="user_name">' + this.userRegistrationOut + '</span> <span class="message_date">' + this.dateCreationOut + '</span></div>';
                                li += '         <div class="row_notification"><div class="notification_text">' + this.messageOut + '</div></div>';
                                li += '     </div>';
                                li += '     <div class="clearfix"></div>';
                                li += '     <div class="general_notification_footer">';
                                li += '         <span class="general_time"></span>';
                                li += '         <div class="clearfix"></div>';
                                li += '     </div>';
                                li += ' </div>';
                                li += '</li>';
                                $('div.more_notification').parent().before(li);
                            }
                        });

                        if (o_data.hasOwnProperty('date')) {
                            t_current_unix_date = o_data.date.current_unix_date;
                            $.each($('ul#ulNotification li.notification'), function() {
                                t_temp_unix_time = $(this).attr('unix_time');
                                $(this).find('span.general_time').text(fnGetDateUpdate(t_temp_unix_time));
                            });
                        }
                        if (o_data.previousCount > 0) {
                            $('div.more_notification').html("[" + o_data.previousCount + "]" + s_preview_message);
                        } else {
                            $('div.more_notification').parent().fadeOut();
                        }
                    }
                }
            });
        }
    });

});
var cacheProfile = false;
function userInfo() {    
    if (!cacheProfile) {
    $('#div_profile_after').load('/private/user/getInfoUser.json', function() {
        cacheProfile = true;
    });
    }else{
         showloadingDialog = 1;
        $.ajax({
            url: s_url_profile_view + jq_jsonp_callback,
            data: {
                id: $('#id').val()
            },
            dataType: "json",
            success: function(j_response) {
                if (evalResponse(j_response)) {
                    var o_user = j_response.data;
                    var o_allOffice = j_response.listOf;
                    $('#fuser_group').val(o_user.namegr);
                    $('#fuser_office').empty();
                    $('.tumbail_shorcut').removeClass('hide');
                    $('#imag').attr('src', o_user.imgProfile);
                    $.each(o_allOffice, function(key, valor) {
                        $('#fuser_office').append($('<option selected value="' + valor.idOffice + '">' + valor.nameof + '</option>'));
                    });
                    $('#fuser_office').val(o_user.idOffice);
                    $('#modal_user_info').modal('show');
                }
            }
        });
    }
}
var cacheNotifications = false;
function notifications() {
    if (cacheNotifications) {
        if (a_bts_users_notification.length === 0) {
            showloadingDialog = 1;
            $.ajax({
                url: s_url_notification_get_user_list + jq_jsonp_callback,
                success: function(j_response) {
                    if (j_response.code == jq_json_code_success) {
                        $('#dlg_form_value_search').empty();
                        $('#dlg_form_value_search').append('<option></option>');
                        a_bts_users_notification = j_response.data;
                        $.each(a_bts_users_notification, function(i, o_item) {
                            $('#dlg_form_value_search').append(
                                    "<option value='" + o_item.value_search + "' data-to_group='" + o_item.to_group + "' data-to_city='" + o_item.to_city + "' data-to_office='" + o_item.to_office + "' data-to_user='" + o_item.to_user + "' data-to_group_name='" + o_item.to_groupName + "'>" + o_item.value_search + "</option>"
                                    );
                        });
                        $('#modal_notification').modal('show');
                    }
                }
            });
        } else {
            $('#modal_notification').modal('show');            
        }
    } else {
        $('#div_notifications_after').load('/private/notification/notificationsLayout.json', function() {
            cacheNotifications = true;
            if (a_bts_users_notification.length === 0) {
                showloadingDialog = 1;
                $.ajax({
                    url: s_url_notification_get_user_list + jq_jsonp_callback,
                    success: function(j_response) {
                        if (j_response.code == jq_json_code_success) {
                            $('#dlg_form_value_search').empty();
                            $('#dlg_form_value_search').append('<option></option>');
                            a_bts_users_notification = j_response.data;
                            $.each(a_bts_users_notification, function(i, o_item) {
                                $('#dlg_form_value_search').append(
                                        "<option value='" + o_item.value_search + "' data-to_group='" + o_item.to_group + "' data-to_city='" + o_item.to_city + "' data-to_office='" + o_item.to_office + "' data-to_user='" + o_item.to_user + "' data-to_group_name='" + o_item.to_groupName + "'>" + o_item.value_search + "</option>"
                                        );
                            });
                            $('#modal_notification').modal('show');
                        }
                    }
                });
            } else {
                $('#modal_notification').modal('show');
            }
        });
    }
}