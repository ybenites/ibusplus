$(document).ready(function() {

    //SELECT2
    $("#fmenu_module").select2({
        placeholder: s_place_holder_module,
        width: 'resolve',
        allowClear: true
    });
    $("#fmenu_option").select2({
        placeholder: s_place_holder_option,
        width: 'resolve',
        allowClear: true
    });
    $("#fmenu_type").select2({
        placeholder: s_place_holder_menu_type,
        width: 'resolve'
    });
    $("#fmenu_icon").select2({
        placeholder: s_place_holder_icon,
        formatResult: fnFormatIcon,
        formatSelection: fnFormatIcon,
        escapeMarkup: function(m) {
            return m;
        },
        width: 'resolve',
        allowClear: true
    });

    //COLOR PICKER
    $('#fmenu_option_type_color').colorpicker();

    //SHOW / HIDE => VALUE TYPES
    $('#fmenu_type').change(function() {
        $('.fmenu_type').addClass('hide');
        switch ($(this).val()) {
            case s_menu_type_item:
                $('.fmenu_type_item').removeClass('hide');
                break;
            case s_menu_type_header:
                $('.fmenu_type_header').removeClass('hide');
                break;
            default:
                break;
        }
    });
    $('#fmenu_option').change(function() {
        var s_data_type = $(this).find('option:selected').data('data_type');
        $('.fmenu_option_type').addClass('hide');
        switch (s_data_type) {
            case s_option_type_boolean:
                $('.fmenu_option_type_boolean').removeClass('hide');
                break;
            case s_option_type_string:
                $('.fmenu_option_type_string').removeClass('hide');
                break;
            case s_option_type_color:
                $('.fmenu_option_type_color').removeClass('hide');
                break;
            case s_option_type_integer:
                $('.fmenu_option_type_integer').removeClass('hide');
                break;
            default:
                break;
        }
    });
    $('input[name=fmenu_shortcut]').change(function() {
        if ($(this).val() == 1)
        {
            $('.div_add_img').show();
        } else {
            $('.div_add_img').hide();
        }
    });

    $('#modal_menu_btn_save').click(function() {
        $('#form_menu').submit();
    });
    form_menu = $('#form_menu').validate({
        rules: {
            fmenu_type: {
                required: true
            }
            , fmenu_name: {
                required: function() {
                    return ($('#fmenu_type').val() == s_menu_type_item || $('#fmenu_type').val() == s_menu_type_header ? true : false);
                }
            }
            , fmenu_url: {
                required: function() {
                    return ($('#fmenu_type').val() == s_menu_type_item ? true : false);
                }
            }
            , fmenu_option_type_string: {
                required: function() {
                    return ($('#fmenu_option option:selected').data('data_type') == s_option_type_string ? true : false);
                }
            }
            , fmenu_option_type_integer: {
                required: function() {
                    return ($('#fmenu_option option:selected').data('data_type') == s_option_type_integer ? true : false);
                }
            }
            , fmenu_option_type_color: {
                required: function() {
                    return ($('#fmenu_option option:selected').data('data_type') == s_option_type_color ? true : false);
                }
            }
        },
        messages: {
            fmenu_type: {
                required: s_validate_required_fmenu_type
            }
            , fmenu_name: {
                required: s_validate_required_fmenu_name
            }
            , fmenu_url: {
                required: s_validate_required_fmenu_url
            }
            , fmenu_option_type_string: {
                required: s_validate_required_fmenu_option_type_string
            }
            , fmenu_option_type_integer: {
                required: s_validate_required_fmenu_option_type_integer
            }
            , fmenu_option_type_color: {
                required: s_validate_required_fmenu_option_type_color
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_menu_create;
                if ($('#fmenu_id').val() != '') {
                    s_url = s_url_menu_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                            , data: $(o_form).serialize() + '&foption_type=' + $('#fmenu_option option:selected').data('data_type')
                            , beforeSend: function() {
                        $('#modal_menu_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_menu_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_menu').modal('hide');
                            bootbox.alert(s_menu_saved);
                            jqgrid_menu.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    jqgrid_menu = $('#jqgrid_menu').jqGrid({
        treeGrid: true,
        treeGridModel: 'adjacency',
        ExpandColumn: 'name',
        url: s_url_menu,
        datatype: 'xml',
        height: 'auto',
        mtype: 'POST',
        colNames: a_jqgrid_menu_col_names,
        colModel: a_jqgrid_menu_col_model,
        pager: "#jqgrid_menu_pager",
        ondblClickRow: function(rowid) {
            var rowDataDestination = $('#jqgrid_menu').jqGrid('getRowData', rowid);
            if (i_change_level == 1) {
                if (rowDataDestination.rtype == 'ITEM') {
                    var id_source = $('#hmenu_level_source').val();
                    if (id_source != rowid) {
                        $('#hmenu_level_destination').val(rowid);
                        var rowDataSource = $('#jqgrid_menu').jqGrid('getRowData', id_source);
                        var msg = s_menu_order_level_confirm.replace('[S]', '<b>' + rowDataSource.name + '</b>', 'g').replace('[D]', '<b>' + rowDataDestination.name + '</b>', 'g');
                        bootbox.confirm(msg, function(result) {
                            if (result) {
                                fnBTSUpdateOrderLevel(id_source, rowid);
                            }
                        });
                    } else {
                        bootbox.alert(s_menu_order_level_deneg);
                    }
                } else {
                    bootbox.alert(s_menu_order_level_deneg);
                }
            }
        }
    });

    $('table#jqgrid_menu').on('click', '.menu_add_sub_menu', function() {
        $('#fmenu_super_menu').val($(this).data('id'));
        $('#modal_menu').modal('show');
    });
    $('table#jqgrid_menu').on('click', '.menu_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_menu_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var o_menu = j_response.data;

                        $('#fmenu_type').select2("val", [o_menu.menu_type]).trigger('change');

                        switch (o_menu.menu_type) {
                            case s_option_type_string:
                                $('#fmenu_option_type_string').val(o_menu.option_value);
                                break;
                            case s_option_type_boolean:
                                $('input[name=fmenu_option_type_boolean]').val([o_menu.option_value]);
                                break;
                            case s_option_type_color:
                                $('#fmenu_option_type_color').val(o_menu.option_value);
                                break;
                            case s_option_type_integer:
                                $('#fmenu_option_type_integer').val(o_menu.option_value);
                                break;
                            default:
                                break;
                        }

                        $('#fmenu_id').val(o_menu.menu_id);
                        $('#fmenu_super_menu').val(o_menu.menu_super_menu);
                        $('#fmenu_name').val(o_menu.menu_name);
                        $('#fmenu_url').val(o_menu.menu_url);
                        $('#fmenu_icon').select2("val", [o_menu.menu_icon]).trigger('change');
                        $('#fmenu_module').select2("val", [o_menu.menu_module]);
                        $('#fmenu_option').val(o_menu.menu_option_key).trigger('change');

                        switch ($('#fmenu_option option:selected').data('data_type')) {
                            case s_option_type_boolean:
                                $('input[name=fmenu_option_type_boolean]').val([o_menu.menu_option_key_value]);
                                break;
                            case s_option_type_string:
                                $('#fmenu_option_type_string').val(o_menu.menu_option_key_value);
                                break;
                            case s_option_type_color:
                                $('#fmenu_option_type_color').val(o_menu.menu_option_key_value);
                                break;
                            case s_option_type_integer:
                                $('#fmenu_option_type_integer').val(o_menu.menu_option_key_value);
                                break;
                            default:
                                break;
                        }
                        $('input[name=fmenu_isroot]').val([o_menu.menu_is_root]);
                        $('input[name=fmenu_shortcut]').val([o_menu.menu_is_withshortcut]);
                        console.log(o_menu.menu_is_withshortcut);
                        if(o_menu.menu_is_withshortcut==='1'){
                            $("#fmenu_hide_image").val(o_menu.menu_iconshortcut);
                            $(".tumbail_shorcut img").attr('src','/img/'+BTS_SKIN+'/shortcuts/'+o_menu.menu_iconshortcut)
                            $(".tumbail_shorcut,.div_add_img").show();
                        }else{
                            $(".tumbail_shorcut,.div_add_img").hide();
                        }                        
                        $('#modal_menu').modal('show');
                    }
                }
            });
        }
    });
    $('table#jqgrid_menu').on('click', '.menu_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_menu_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_menu_deleted);
                                    jqgrid_menu.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    $('table#jqgrid_menu').on('click', '.menu_sort', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_menu_list_order + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var a_data = j_response.data;
                        $('#menu_sort_list').html('');
                        $.each(a_data, function(i, o_value) {
                            $('#menu_sort_list').append($('<li id="menu_sort_key_' + o_value.menu_id + '" class="alert alert-info"><span class="icon-resize-vertical"></span>' + o_value.menu_name + '</li>'));
                        });
                        $("#menu_sort_list").sortable();
                        $('#modal_menu_sort').modal('show');
                    }
                }
            });
        }
    });
    $('table#jqgrid_menu').on('click', '.menu_order_level', function() {
        var $this = $(this);
        $('table#jqgrid_menu').removeClass();
        $('table#jqgrid_menu tr#' + $this.data('id')).addClass("text-warning");
        i_change_level = 1;
        $('#hmenu_level_source').val($this.data('id'));

        var html2 = "<div style='width: 202px;'><label class='text-info'>" + s_menu_select_destionation + ":</label>\n\
                    <strong class='text-info' style='margin-left: 2px;'>Doble Click</strong>" +
                "<a href='#' id='popover_menu_btn_save_order_level' role='button' class='btn btn-success'><i class='icon-arrow-up'></i>" + s_menu_select_first_level + "</a>" +
                "<input type='button'id='popover_menu_btn_cancel_order_level' name='popover_menu_btn_cancel_order_level' class='btn btn-success' value='" + s_menu_cancel_first_level + "'/>\n\
                    </div>";
        $('#popover_menu').css('display', 'block');
        $("#popover_menu_order_level").popover({
            title: s_menu_change_level,
            content: html2,
            html: true
        }).popover('show');
        jqgrid_menu.hideCol("actions");

        // Subir al primer level
        $('#popover_menu_btn_save_order_level').on('click', function() {
            fnBTSUpdateOrderLevel($('#hmenu_level_source').val(), null);
        });
        // Cancelar el movimiento level 
        $('input#popover_menu_btn_cancel_order_level').on('click', function() {
            $('#popover_menu_order_level').popover('hide');
            i_change_level = 0;
            jqgrid_menu.showCol("actions");
            $('table#jqgrid_menu tr#' + $this.data('id')).removeClass();
        });
    });
    $('#modal_menu_sort_btn_ok').click(function() {
        if (b_ajax) {
            var a_data = $("#menu_sort_list").sortable("serialize");
            $.ajax({
                url: s_url_menu_save_order + jq_jsonp_callback,
                data: a_data
                        , beforeSend: function() {
                    $('#modal_menu_sort_btn_ok').button('loading');
                }
                , complete: function() {
                    $('#modal_menu_sort_btn_ok').button('reset');
                }, success: function(j_response) {
                    if (evalResponse(j_response)) {
                        $('#modal_menu_sort').modal('hide');
                        bootbox.alert(s_menu_order_saved);
                        jqgrid_menu.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('#modal_menu').on('hidden', function() {
        $('#fmenu_module').select2("val", "").trigger('change');
        $('#fmenu_option').select2("val", "").trigger('change');
        $('#fmenu_icon').select2("val", "").trigger('change');
        form_menu.currentForm.reset();
        form_menu.resetForm();
        $('input[type=hidden]').val('');
        $('input, select, textarea', form_menu.currentForm).parents('.control-group').removeClass('error');
    });
    fnAddImage();



});
function fnFormatIcon(state) {
    if (!state.id)
        return '';
    return '<b class="' + $(state.element).val() + '"></b>  ' + $(state.element).text();
}
function fnBTSUpdateOrderLevel(idSource, idDestionation) {
    var hmenu_level_destination = '';
    if (idDestionation != null)
    {
        hmenu_level_destination = $('#hmenu_level_destination').val();
    }
    if (b_ajax) {
        var $this = $(this);
        $.ajax({
            url: s_url_menu_order_level + jq_jsonp_callback,
            data: {
                hmenu_level_source: idSource,
                hmenu_level_destination: hmenu_level_destination
            }
            , success: function(j_response) {
                if (evalResponse(j_response)) {
                    $('#popover_menu_order_level').popover('hide');
                    jqgrid_menu.trigger('reloadGrid');
                    i_change_level = 0;
                    jqgrid_menu.showCol("actions");
                }
            }
        });
    }
}
function fnAddImage() {
    var uploader = new qq.FileUploader({
        element: document.getElementById('btn_add_img'),
        action: s_url_menu_shorcut,
        allowedExtensions: ['jpg', 'jpeg', 'png'],
        multiple: false,
        onSubmit: function(id, fileName) {
        },
        onComplete: function(id, fileName, responseJSON) {
            if (responseJSON.code == jq_json_code_success && responseJSON.success == 'true') {
                $(".tumbail_shorcut").removeClass('hide');
                $("#fmenu_hide_image").val(responseJSON.data.tmpfn);
                $(".tumbail_shorcut img").attr('src', '/tmp/' + responseJSON.data.tmpfn);
            } else {
                $(".qq-upload-failed-text").hide();
                $('.qq-upload-fail:last').append("<span style='float: right;margin-left:2em;'><img src='/media/ico/ico_close.png' alt='' /><img class='cancel_upload' src='/media/ico/ico_cancel.png' alt='' /></span>");
                msgBox(responseJSON.msg, responseJSON.code);
            }
        }
    }, s_menu_img_shorcut);
    $('.qq-uploader').append('<div class="clear"></div>');
    //$(".qq-upload-list").addClass("qq_up_list");
    $(".qq-upload-list").addClass("hide");
    $('.qq-upload-button').addClass('btn controls');
    $('.qq-upload-button').hover(function() {
        if ($(this).hasClass('btn-info')) {
            $(this).removeClass('btn-info');
        } else {
            $(this).addClass('btn-info');
        }
    });

    $('#modal_share_hosts_btn_save').click(function() {
        $('#form_share_hosts').submit();
    });

    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var o_data = $(o_form).serializeObject();
                $.ajax({
                    url: s_url_menu_share + jq_jsonp_callback
                            , data: o_data
                            , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_share_hosts').modal('hide');
                            bootbox.alert(s_menu_shared);
                        }
                    }
                });
            }
        }
    });
}
