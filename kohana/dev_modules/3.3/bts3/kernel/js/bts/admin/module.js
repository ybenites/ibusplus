$(document).ready(function() {

    jqgrid_module = $('#jqgrid_module').jqGrid({
        url: s_url_module_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_module_col_names,
        colModel: a_jqgrid_module_col_model,
        pager: "#jqgrid_module_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        multiselect: true,
        rowNum: 50,
        width: 'auto',
        sortname: 'mo.idModule',
        afterInsertRow: function(row_id, row_data) {
            var s_html_module_edit = '<a data-id=' + row_id + ' class="btn btn-mini module_edit"><i class="icon-pencil"></i></a>';
            var s_status_icon = 'icon-ban-circle';
            if (row_data.module_status == 1) {
                s_status_icon = 'icon-ok-circle';
            }
            jqgrid_module.setRowData(row_id, {
                module_status: '<a data-id=' + row_id + ' class="module_change_status"><i class="' + s_status_icon + '"></i></a>',
                module_actions: s_html_module_edit
            });
        },
        onSelectAll: function() {
            if (jqgrid_module.getGridParam('selarrrow').length > 0) {
                $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
            } else {
                $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
            }
        },
        onSelectRow: function(row_id) {
            if (jqgrid_module.getGridParam('selarrrow').length > 0) {
                $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
            } else {
                $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
            }
        }
    });
    // Editar Modulo
    $('table#jqgrid_module').on('click', '.module_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_module_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var o_module = j_response.data;
                        $('#fmodule_id').val(o_module.module_code);
                        $('#fmodule_code').val(o_module.module_code);
                        $('#fmodule_name').val(o_module.module_name);
                        $('#modal_module').modal('show');
                    }
                }
            });
        }
    });
    // Cambiar Estado de Modulo 
    $('table#jqgrid_module').on('click', '.module_change_status', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_module_change_status + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_module_change);
                        jqgrid_module.trigger('reloadGrid');
                    }
                }
            });
        }
    });



    $('#modal_module').on('hidden', function() {
        form_module.currentForm.reset();
        form_module.resetForm();
        $('input[type=hidden]', form_module.currentForm).val('');
        $('input, select, textarea', form_module.currentForm).parents('.control-group').removeClass('error');
    });
    $('#modal_module_btn_save').click(function() {
        $('#form_module').submit();
    });



    form_module = $('#form_module').validate({
        rules: {
            fmodule_code: {
                required: true
            }
            , fmodule_name: {
                required: true
            }
        },
        messages: {
            fmodule_code: {
                required: s_validate_required_fmodule_id
            }
            , fmodule_name: {
                required: s_validate_required_fmodule_name
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_module_create;
                if ($('#fmodule_id').val() != '') {
                    s_url = s_url_module_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                            , data: $(o_form).serialize()
                            , beforeSend: function() {
                        $('#modal_module_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_module_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_module').modal('hide');
                            bootbox.alert(s_module_saved);
                            jqgrid_module.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });

    $('#modal_share_hosts_btn_save').click(function() {
        $('#form_share_hosts').submit();
    });

    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var o_share = {'fshare_modules[]': jqgrid_module.getGridParam('selarrrow')};
                var o_data = $.extend($(o_form).serializeObject(), o_share);
                $.ajax({
                    url: s_url_module_share + jq_jsonp_callback
                    , data: o_data
                    , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                         if(evalResponse(j_response)){
                            $('#modal_share_hosts').modal('hide');        
                            bootbox.alert(s_module_shared);
                        }
                    }
                });
            }
        }
    });
});
