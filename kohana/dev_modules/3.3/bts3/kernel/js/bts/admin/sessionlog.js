$(document).ready(function() {

    jqgrid_sessionlog = $('#jqgrid_sessionlog').jqGrid({
        url: s_url_sessionlog_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_sessionlog_col_names,
        colModel: a_jqgrid_sessionlog_col_model,
        pager: "#jqgrid_sessionlog_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        width: 'auto',
        sortname: 'slog.idSessionLog',
        afterInsertRow: function(row_id, row_data) {
            var s_status_icon = "icon-user";
            var s_status_class = "status_on";
            
            if(row_data.sessionlog_event == s_event_log_out) {
                s_status_icon = 'icon-fire';
                s_status_class = 'status_off';
            }
            
            jqgrid_sessionlog.setRowData(row_id, {
                sessionlog_event: '<i class="'+s_status_class+' ' + s_status_icon + '">       ' + row_data.sessionlog_event + '</i>'
            });
        }
    });
    jqgrid_sessionlog.filterToolbar({stringResult: true, searchOnEnter:false});

    var fdate_sessionlog_search = $('#fdate_sessionlog_search')
            .datetimepicker({
        format:jquery_date_format
        ,todayBtn:true
        ,viewSelect:'month'
        ,minView:'month' 
        ,pickerPosition: "bottom-left"        
    })
            .on('changeDate', function(ev) {
        $('#fdate_sessionlog_search').datetimepicker('hide');
        var o_date = new Date(ev.date);
        var s_sql_date = o_date.getFullYear().toString() + '/' + ((o_date.getMonth() + 1 < 10 ? '0' : '') + (o_date.getMonth() + 1)) + '/' + (o_date.getDate()+1 < 10 ? '0' : '') + (o_date.getDate()+1).toString();
        $("#jqgrid_sessionlog").setGridParam({
            postData: {
                fdate_sessionlog_search: function() {
                    return s_sql_date;
                }
            }}).trigger("reloadGrid");

    });        

    $(input_picker).datetimepicker({
        format:jquery_date_format          
        ,todayBtn:true
        ,viewSelect:'month'
        ,minView:'month'
        ,pickerPosition: "bottom-left"       
    }).on('changeDate',function(ev){        
        $('#jqgrid_sessionlog')[0].triggerToolbar();
        $(this).datetimepicker('hide');        
    });
});