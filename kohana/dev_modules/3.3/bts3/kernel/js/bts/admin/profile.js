function loadData() {        
        showloadingDialog = 1;
        $.ajax({
            url: s_url_profile_view + jq_jsonp_callback,
            data: {
                id: $('#id').val()
            },
            dataType: "json",
            success: function(j_response) {
                if (evalResponse(j_response)) {
                    var o_user = j_response.data;
                    var o_allOffice = j_response.listOf;
                    $('#fuser_group').val(o_user.namegr);
                    $('#fuser_office').empty();
                    $('.tumbail_shorcut').removeClass('hide');
                    $('#imag').attr('src', o_user.imgProfile);
                    $.each(o_allOffice, function(key, valor) {
                        $('#fuser_office').append($('<option selected value="' + valor.idOffice + '">' + valor.nameof + '</option>'));
                    });
                    $('#fuser_office').val(o_user.idOffice);
                    $('#modal_user_info').modal('show');
                }
            }
        });
    }
$(document).ready(function(){
    loadData();
      $('#fuser_office').select2({
            placeholder: 'fuser_office',
            width: 'resolve',
            allowClear: true
        });    
    $('#modal_btn_save').click(function() {
        $('#editUser').submit();
    });

    frmUser = $('#editUser').validate({
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        },
        submitHandler: function(form) {
            if (b_ajax) {
                $.ajax({
                    url: s_url_profile_update + jq_jsonp_callback
                            , data: $(form).serialize()
                            , beforeSend: function() {
                        $('#modal_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_user_info').modal('hide');
                            bootbox.alert(s_edit_Office);
                        }
                    }
                });
            }
        }
    });


    //para que se muestre el espacio para la iamgen
    $('input[name=fmodal_user_upload]').change(function() {
        if ($(this).val() === 1) {
            $('.div_add_img').show();
        } else {
            $('.div_add_img').hide();
        }
    });

    var o_profile_img_upload = fnCreateUploadFileInstance(
            document.getElementById('btn_img'),
            s_url_profile_shorcut,
            function() {
                $('.tumbail_shorcut img').attr('src', '/kernel/img/wait_loading.gif');
            },
            function(id, fileName, responseJSON) {
                //success                
                $(".tumbail_shorcut").removeClass('hide');
                $("#fmenu_image").val(responseJSON.data.tmpfn);
                $(".tumbail_shorcut img").attr('src', '/tmp/' + responseJSON.data.tmpfn);
            },
            function(id, fileName, responseJSON) {
                $("btn_img .qq-upload-failed-text").hide();
                $('btn_img .qq-upload-fail:last').append("<span style='float: right;margin-left:2em;'><img src='/media/ico/ico_close.png' alt='' /><img class='cancel_upload' src='/media/ico/ico_cancel.png' alt='' /></span>");

                bootbox.alert(responseJSON.msg);
            },
            function() {
                //after_create
                $('#btn_img .qq-uploader').append('<div class="clear"></div>');
                $(".qq-upload-list").addClass("hide");
                $('#btn_img .qq-upload-button').addClass('btn controls');
                $('#btn_img .qq-upload-button').hover(function() {
                    if ($(this).hasClass('btn-info')) {
                        $(this).removeClass('btn-info');
                    } else {
                        $(this).addClass('btn-info');
                    }
                });
            }
    );

});
