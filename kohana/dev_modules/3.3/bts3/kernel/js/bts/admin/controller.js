var form_controller;
var jqgrid_controller;
$(document).ready(function() {
    //SELECT2
    $("#fcontroller_controller").select2({
        placeholder: s_place_holder_controller,
        width: 'resolve'
    }).change(function(e, fnCallback){        
        var $this = $(this);
        $('#fcontroller_directory').val($this.find('option:selected').data('directory'));
        $('#fcontroller_action').select2("val", "").select2('close');
        $('#fcontroller_action').empty();
        $('#fcontroller_action').append('<option></option>');
        if($this.val()!=''){            
            if(b_ajax){
                $.ajax({
                    url: s_url_controller_getActions + jq_jsonp_callback,
                    data: {
                        directory: $this.find('option:selected').data('directory')
                        , controller: $this.val()
                    }
                    , success: function(j_response) {
                       if(evalResponse(j_response)){    
                            $.each(j_response.data, function(i, o_value){
                                var i_controller_id = o_value.id == null?'':o_value.id;
                                $('#fcontroller_action').append('<option data-id="'+i_controller_id+'" value="'+o_value.action+'">'+o_value.action+'</option>');
                            });
                            if(typeof fnCallback == 'function'){
                                fnCallback();
                            }
                        }
                    }
                });
            }
        }
    });
    
    //RENDER
    function fnFormatIcon(state) {   
        if(!state.id) return '';
        var s_icon = '<b class="icon-plus"></b> ';
        if($(state.element).data('id')>0)
            s_icon = '<b class="icon-refresh"></b> ';
        
        return s_icon + $(state.element).text();       
    }
    //SELECTED
    function fnFormatIconSelect(state) {   
        if(!state.id) return '';
        
        var s_icon = '<b class="icon-plus"></b> ';
        if($(state.element).data('id')>0){            
            s_icon = '<b class="icon-refresh"></b> ';
            $('#fcontroller_id').val($(state.element).data('id'));
        }
        
        return s_icon + $(state.element).text();
    }
    
    $("#fcontroller_action").select2({
        placeholder: s_place_holder_action,
        formatResult: fnFormatIcon,
        formatSelection: fnFormatIconSelect,
        escapeMarkup: function(m) { return m; },
        width: 'resolve'
    });
    
    $('#modal_controller_btn_save').click(function() {
        $('#form_controller').submit();
    });
    
    form_controller = $('#form_controller').validate({
        rules: {
            fcontroller_controller: {
                required: true
            }           
            , fcontroller_action: {
                required: true
            }           
            , fcontroller_type: {
                required: true
            }
        },
        messages: {
            fcontroller_controller: {
                required: s_validate_required_fcontroller_controller
            }
            , fcontroller_action: {
                required: s_validate_required_fcontroller_action
            }
            , fcontroller_type: {
                required: s_validate_required_fcontroller_type
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_controller_create;
                if($('#fcontroller_id').val() != ''){
                    s_url = s_url_controller_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_controller_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_controller_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){                          
                            $('#modal_controller').modal('hide');
                            bootbox.alert(s_controller_saved);
                            jqgrid_controller.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    
    $('#modal_share_hosts_btn_save').click(function(){
        $('#form_share_hosts').submit();
    });
    
    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {                
                var o_share = {'fshare_instances[]': jqgrid_controller.getGridParam('selarrrow')};
                var o_data = $.extend($(o_form).serializeObject(), o_share);
                $.ajax({
                    url: s_url_controller_share + jq_jsonp_callback
                    , data: o_data
                    , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                         if(evalResponse(j_response)){                         
                            $('#modal_share_hosts').modal('hide');        
                            bootbox.alert(s_controller_shared);
                        }
                    }
                });
            }
        }
    });
    
    // JQGRID OPTION
    jqgrid_controller = $('#jqgrid_controller').jqGrid({
        url: s_url_controller_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_controller_col_names,
        colModel: a_jqgrid_controller_col_model,
        pager: "#jqgrid_controller_pager",        
        height: 'auto',        
        viewrecords: true,
        multiselect: true,
        onSelectAll: function(){
            if( jqgrid_controller.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        onSelectRow: function(row_id){
           if( jqgrid_controller.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'ctr.idController',
        afterInsertRow: function(row_id, row_data){            
            //Actions
            var s_html_option_edit = '<a data-id=' + row_id + ' class="btn btn-mini controller_edit"><i class="icon-pencil"></i></a>',
            s_html_option_delete = '<a data-id=' + row_id + ' class="btn btn-mini controller_delete"><i class="icon-trash"></i></a>';

            jqgrid_controller.setRowData(row_id, {
                actions: s_html_option_edit + s_html_option_delete
            });
        },
        gridComplete: function(){    
            $(jqgrid_controller.getGridParam('pager')).find("option[value=-1]").text(s_controller_select_text_all);
            $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
        }
    });
    
    $('table#jqgrid_controller').on('click', '.controller_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_controller_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {                    
                     if(evalResponse(j_response)){    
                        var o_controller = j_response.data;
                        
                        $('input[name=fcontroller_type]').val([o_controller.controller_type]);
                        
                        setTimeout(function(){
                            $('#fcontroller_controller')
                            .select2("val", [o_controller.controller_controller])
                            .trigger('change', function(){
                                $('#fcontroller_action')
                                .select2("val", [o_controller.controller_action])
                                .trigger('change');
                            });                            
                        }, 10);

                        $('#modal_controller').modal('show');
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_controller').on('click', '.controller_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_controller_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {                                    
                                    bootbox.alert(s_controller_deleted);
                                    jqgrid_controller.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
    $('#modal_controller').on('hidden', function() {  
        $('#fcontroller_controller').select2("val", "");
        $('#fcontroller_action').select2("val", "");
        form_controller.currentForm.reset();
        form_controller.resetForm();
        $('input[type=hidden]', form_controller.currentForm).val('');
        $('input, select, textarea', form_controller.currentForm).parents('.control-group').removeClass('error');
    });    
});