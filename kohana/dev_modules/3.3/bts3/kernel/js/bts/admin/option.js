//VARs to USE on WINDOWS
var form_option;
var jqgrid_option;
$(document).ready(function() {
    var iList = '<li id="menu_[ID]" class="item_core_js alert" data-value="[VALUE]"><i class="icon-remove"></i></i>[VALUE]</li>';
    var iComp = '<option value="[VALUE]">[VALUE]</li>';

    //SELECT2
    $("#foption_component").select2({
        placeholder: s_place_holder_component,
        width: 'resolve',
        allowClear: true
    });
    $("#foption_type").select2({
        placeholder: s_place_holder_type,
        width: 'resolve',
        allowClear: true
    });

    //COLOR PICKER
    $('#foption_type_color').colorpicker();

    //SHOW / HIDE => VALUE TYPES
    $('#foption_type').change(function() {
        $('.foption_type').addClass('hide');
        switch ($(this).val()) {
            case s_option_type_boolean:
                $('.foption_type_boolean').removeClass('hide');
                break;
            case s_option_type_string:
                $('.foption_type_string').removeClass('hide');
                break;
            case s_option_type_color:
                $('.foption_type_color').removeClass('hide');
                break;
            case s_option_type_integer:
                $('.foption_type_integer').removeClass('hide');
                break;
            default:
                break;
        }
    });

    //SUBMIT on CLICK
    $('#modal_option_btn_save').click(function() {
        $('#form_option').submit();
    });
    
    $('#btn_new_component').click(function(event) {
        event.preventDefault();
        if (b_ajax) {
            $.ajax({
                url: s_url_option_enum_component + jq_jsonp_callback,
                success: function(j_response) {
                    if(evalResponse(j_response)){
                        var str="";
                        $('#cmpList').html('');
                        if(j_response.data.length>0){
                            $.each(j_response.data,function(){
                                str += iList.replace('[ID]', this, 'g').replace('[VALUE]', this, 'g');
                                $('#cmpList').html(str);
                                $("#modal_component").modal('show');
                            });
                        } else{
                            $('#cmpList').html('<center><?php echo __("No existen Componentes"); ?></center>');
                        }
                        $('#cmpName').val('');
                    }
                }
            });
        }
    });
    
    $('#btn_add_option').click(function() {
        if (b_ajax) {
            $.ajax({
                url: s_url_option_enum_component + jq_jsonp_callback,
                success: function(j_response) {
                    if(evalResponse(j_response)){
                        var str="";
                        $('#foption_component').html('');
                        if(j_response.data.length>0){
                            $.each(j_response.data,function(){
                                str += iComp.replace('[ID]', this, 'g').replace('[VALUE]', this, 'g');
                                $('#foption_component').html(str);
                            });
                        } else{
                            $('#foption_component').html('<center><?php echo __("No existen Componentes"); ?></center>');
                        }
                    }
                }
            });
        } 
    });
    
    $('#btn_add_component').click(function() {
        var name = $('#fname_component').val().toUpperCase().replace(' ','_','g');
        name = $.trim(name);
        if(name!=''){
            $('#cmpList').append(iList.replace('[ID]',name, 'g').replace('[VALUE]',name, 'g'));            
            $('#fname_component').val('');
        }
    });        

    $('#cmpList').on('click', 'i.icon-remove', function() {
        var thes = this;
        var comp = $(thes).parent().data('value').toString();
        $.ajax({
            url: s_url_option_list_order + jq_jsonp_callback,
            data: {
                component: comp
            }
            , 
            success: function(j_response) {
                if(evalResponse(j_response)){
                    var a_data = j_response.data;
                    if(a_data.length>0){
                        bootbox.alert(s_option_component_with_option);
                    }else{
                        switch(comp){
                            case 'GENERAL':
                                break;
                            case 'boolean':
                                break;
                            case 'color':
                                break;
                            case 'decimal':
                                break;
                            case 'integer':
                                break;
                            case 'string':
                                break;
                            case 'percentage':
                                break;
                            default:
                                $(thes).parent().fadeOut().remove();
                                break;
                        }                        
                    }
                }
            }
        });
    });   

    //REGEX Key
    $.validator.addMethod("rule_foption_key", function(value, element) {
        return /^[A-Z][A-Z_]+$/.test(value);
    }, s_validate_required_foption_key_regex);

    //VALIDATE FORM OPTION
    form_option = $('#form_option').validate({
        rules: {
            foption_component: 'required'
            , 
            foption_type: 'required'
            , 
            foption_type_boolean: {
                required: function() {
                    return $('#foption_type').val() == s_option_type_boolean ? true : false;
                }
            }
            , 
            foption_type_string: {
                required: function() {
                    return $('#foption_type').val() == s_option_type_string ? true : false;
                }
            }
            , 
            foption_type_color: {
                required: function() {
                    return $('#foption_type').val() == s_option_type_color ? true : false;
                }
            }
            , 
            foption_key: {
                required: true
                , 
                rule_foption_key: true
            }
            , 
            foption_description: 'required'
            , 
            foption_isroot: 'required'
        },
        messages: {
            foption_component: {
                required: s_validate_required_foption_component
            }
            , 
            foption_type: {
                required: s_validate_required_foption_type
            }
            , 
            foption_type_boolean: {
                required: s_validate_required_foption_type_boolean
            }
            , 
            foption_type_string: {
                required: s_validate_required_foption_type_string
            }
            , 
            foption_type_color: {
                required: s_validate_required_foption_type_color
            }
            , 
            foption_key: {
                required: s_validate_required_foption_key
            }
            , 
            foption_description: {
                required: s_validate_required_foption_description
            }
            , 
            foption_isroot: {
                required: s_validate_required_foption_isroot
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , 
        submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_option_create;
                if($('#foption_hidden_key').val() != ''){
                    s_url = s_url_option_update;
                }
                //                console.log($(o_form).serialize());
                //                return false;
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , 
                    data: $(o_form).serialize()
                    , 
                    beforeSend: function() {
                        $('#btn_process_form_option').button('loading');
                    }
                    , 
                    complete: function() {
                        $('#btn_process_form_option').button('reset');
                    }
                    , 
                    success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_option').modal('hide');
                            bootbox.alert(s_option_saved);
                            jqgrid_option.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });

    // JQGRID OPTION
    jqgrid_option = $('#jqgrid_option').jqGrid({
        url: s_url_option_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_option_col_names,
        colModel: a_jqgrid_option_col_model,
        pager: "#jqgrid_option_pager",        
        height: 'auto',
        grouping:true,
        multiselect: true,
        onSelectAll: function(){
            if( jqgrid_option.getGridParam('selarrrow').length > 0){               
                $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
            }else{
                $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
            }
        },
        onSelectRow: function(row_id){
            if( jqgrid_option.getGridParam('selarrrow').length > 0){               
                $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
            }else{
                $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
            }
        },
        groupingView : {
            groupField : ['component']
            , 
            groupColumnShow : [false]
            , 
            groupText : ['<b>{0} - ({1})</b> <a data-component="{0}" class="option_sort icon-list"></a>']
            , 
            groupCollapse : false
            , 
            groupOrder: ['asc']
            , 
            groupSummary : [false] 
            , 
            showSummaryOnHide: true
            , 
            groupDataSorted : true
            , 
            plusicon: 'icon-plus'
            , 
            minusicon: 'icon-minus'
        },        
        viewrecords: true,
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'op.order',
        gridComplete: function() {

            $(jqgrid_option.getGridParam('pager')).find("option[value=-1]").text(s_option_select_text_all);          

            var a_ids = jqgrid_option.getDataIDs();
            
            $.each(a_ids, function(i, i_id){  
                
                var o_data = jqgrid_option.getRowData(i_id);
                
                //Option ROOT
                var s_is_root_icon = 'icon-minus';
                if (o_data.is_root == 1) {
                    s_is_root_icon = 'icon-star';
                }

                //Actions
                var s_html_option_edit = '<a data-id=' + i_id + ' class="btn btn-mini option_edit"><i class="icon-pencil"></i></a>',
                s_html_option_delete = '<a data-id=' + i_id + ' class="btn btn-mini option_delete"><i class="icon-trash"></i></a>';

                //Display Data Type
                var s_html_data_type_display = '';
                switch (o_data.hidden_data_type) {
                    case s_option_type_integer:
                        s_html_data_type_display = o_data.value;
                        break;
                    case s_option_type_string:
                        s_html_data_type_display = o_data.value;
                        break;
                    case s_option_type_boolean:
                        s_html_data_type_display = s_dialog2_button_label_no;
                        if (o_data.value == 1)
                            s_html_data_type_display = s_dialog2_button_label_yes;
                        break;
                    case s_option_type_color:
                        s_html_data_type_display = '<div style="width:16px;height:16px;background-color:' + o_data.value + '"></div>';
                        break;
                    default:
                        break;
                }

                jqgrid_option.setRowData(i_id, {
                    is_root: '<a data-id=' + i_id + ' class="option_change_is_root"><i class="' + s_is_root_icon + '"></i></a>'
                    , 
                    value: s_html_data_type_display
                    , 
                    actions: s_html_option_edit + s_html_option_delete
                });
            });
        }
    });

    // jqgrid_option.gridResize({minHeight: 250});
    
    jqgrid_option.filterToolbar({
        stringResult: true,
        searchOnEnter : false
    });

    $('table#jqgrid_option').on('click', '.option_change_is_root', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_option_set_root + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , 
                success: function(j_response) {
                    if(evalResponse(j_response)){    
                        bootbox.alert(s_option_saved);
                        jqgrid_option.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    $('table#jqgrid_option').on('click', '.option_sort', function() {
        if (b_ajax) {
            var $this = $(this);            
            $.ajax({
                url: s_url_option_list_order + jq_jsonp_callback,
                data: {
                    component: $this.data('component')
                }
                , 
                success: function(j_response) {
                    if(evalResponse(j_response)){    
                        var a_data = j_response.data;
                        $('#option_sort_list').html('');
                        $.each(a_data, function(i, o_value){
                            $('#option_sort_list').append($('<li id="option_sort_key_'+o_value.option_key+'" class="alert alert-info"><span class="icon-resize-vertical"></span>'+o_value.option_key+'</li>'));
                        });
                        $( "#option_sort_list" ).sortable();
                        $('#modal_option_sort').modal('show');
                    }
                }
            });
        }
    });

    $('table#jqgrid_option').on('click', '.option_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_option_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , 
                            success: function(j_response) {
                                if(evalResponse(j_response)){    
                                    bootbox.alert(s_option_deleted);
                                    jqgrid_option.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    $('table#jqgrid_option').on('click', '.option_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_option_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , 
                success: function(j_response) {
                    if (j_response.code == jq_json_code_success) {
                        var o_option = j_response.data;

                        $('#foption_component').select2("val", [o_option.option_component]);
                        $('#foption_type').select2("val", [o_option.option_data_type]).trigger('change');

                        switch (o_option.option_data_type) {
                            case s_option_type_string:
                                $('#foption_type_string').val(o_option.option_value);
                                break;
                            case s_option_type_boolean:
                                $('input[name=foption_type_boolean]').val([o_option.option_value]);
                                break;
                            case s_option_type_color:
                                $('#foption_type_color').val(o_option.option_value);
                                break;
                            case s_option_type_integer:
                                $('#foption_type_integer').val(o_option.option_value);
                                break;
                            default:
                                break;
                        }

                        $('#foption_hidden_key').val(o_option.option_key);
                        $('#foption_key').val(o_option.option_key);
                        $('#foption_prefix').val(o_option.option_prefix);
                        $('#foption_suffix').val(o_option.option_suffix);
                        $('#foption_description').val(o_option.option_description);
                        $('#foption_long_description').val(o_option.option_long_description);
                        $('input[name=foption_isroot]').val([o_option.option_is_root]);

                        $('#modal_option').modal('show');
                    } else {
                        bootbox.alert(j_response.msg);
                    }
                }
            });
        }
    });

    $('#modal_option').on('hidden', function() {
        $('#foption_component').select2("val", "").trigger('change');
        $('#foption_type').select2("val", "").trigger('change');
        form_option.currentForm.reset();
        form_option.resetForm();
        $('input[type=hidden]').val('');
        $('input, select, textarea', form_option.currentForm).parents('.control-group').removeClass('error');
    });
    
    $('#modal_option_component_btn_ok').click(function(){
        if (b_ajax) {
            var data = new Array();
            $.each($('#cmpList li'),function(){
                data.push($(this).data('value'));
            });
            $.ajax({
                url:s_url_option_update_enum_component + jq_jsonp_callback,
                data:{
                    values:data
                },
                success:function(j_response){
                    if(evalResponse(j_response)){    
                        $('#modal_component').modal('hide');
                    }
                }
            });
        }
    });
    
    $('#modal_option_sort_btn_ok').click(function(){
        if (b_ajax) {
            var a_data = $.map( $( "#option_sort_list" ).sortable( "toArray"), function(val, i) {
                return val.substring(16);
            });            
            $.ajax({
                url: s_url_option_save_order + jq_jsonp_callback,
                data: {
                    'sort[]': a_data
                }
                , 
                beforeSend: function() {
                    $('#modal_option_sort_btn_ok').button('loading');
                }
                , 
                complete: function() {
                    $('#modal_option_sort_btn_ok').button('reset');
                }, 
                success: function(j_response) {
                    if(evalResponse(j_response)){    
                        $('#modal_option_sort').modal('hide');
                        bootbox.alert(s_option_order_saved);
                        jqgrid_option.trigger('reloadGrid');
                    }
                }
            });
        }
    });
    
    $('#modal_share_hosts_btn_save').click(function(){
        $('#form_share_hosts').submit();
    });
    
    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , 
        submitHandler: function(o_form) {
            if (b_ajax) {                
                var o_share = {
                    'fshare_instances[]': jqgrid_option.getGridParam('selarrrow')
                };
                var o_data = $.extend($(o_form).serializeObject(), o_share);
                $.ajax({
                    url: s_url_option_share + jq_jsonp_callback
                    , 
                    data: o_data
                    , 
                    beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , 
                    complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , 
                    success: function(j_response) {
                        if(evalResponse(j_response)){                             
                            $('#modal_share_hosts').modal('hide');
                            bootbox.alert(s_option_shared);                            
                        }
                    }
                });
            }
        }
    });
    
    function fn_list_component(){
        if (b_ajax) {
            $.ajax({
                url: s_url_option_enum_component + jq_jsonp_callback,
                success: function(r) {
                    if(evalResponse(j_response)){    
                        var str="";
                        $('#cmpList').html('');
                        if(j_response.data.length>0){
                            $.each(j_response.data,function(){
                                str += iList.replace('[ID]', this, 'g').replace('[VALUE]', this, 'g');
                                $('#cmpList').html(str);
                            });
                        } else{
                            $('#cmpList').html('<center><?php echo __("No existen Componentes"); ?></center>');
                        }
                        $('#cmpName').val('');
                    }
                }
            });
        }        
    }
    
});
