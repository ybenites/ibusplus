$(document).ready(function(){    
   $('div.more_messagesuser').on('click',function(){       
      if (b_ajax) {                
                $.ajax({
                    url: s_url_messageListPrev + jq_jsonp_callback
                    , data:{
                        n_page: num_page, lastNotification:m_last_notification
                    }               
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            oData = j_response.data;
                            numbAvailableMoreMessageAjax = true;
                            num_page ++;
                            $.each(oData,function(){                                    
                                if(this.hasOwnProperty('idNotification')){
                                    dates='';
                                    if(this.idUserDestination>0){
                                        dates='dates';
                                    }
                                    s_html = '<div id="messageUser_id_'+this.idNotification+'" unix_time="'+this.lastNotification+'" class="messageUser">';
                                    s_html +='<div class="control-group well well-small controls-row">';
                                    s_html +='<div class="span2">';
                                    s_html +='<img class="imgUser" alt="Imagen" src="/profile/'+SKIN+'/64/'+this.img+'"/>';
                                    s_html +='</div>';
                                    s_html +='<div class="span5">';
                                    s_html +='<div class="span5 control-group row-fluid">';
                                    s_html +='<span class="nameBold span3" id="'+this.idUserSender+'">'+this.sender+' </span>';
                                    s_html +='<span class="span1"><i class="icon-circle-arrow-right"></i></span>';
                                    s_html +='<span class="span3 nameBold dates '+ dates +'" id="'+this.idUserDestination+'"> '+this.destination+'</span>';
                                    s_html +='</div>' ;  
                                    s_html +='<div class="span5 control-group">';
                                    s_html +='<span class="text-info">'+this.message+'</span>';
                                    s_html +='</div>';
                                    s_html +='<div class="span5 control-group">';
                                    s_html += '<span class="muted text-center">'+this.creation_dates+'</span>';
                                    s_html +='</div>';
                                    s_html +='</div>';                                    
                                    s_html +='</div>';
                                    s_html +='</div> ';                                    
                                    $('div.more_messagesuser').before(s_html);
                                }
                            });
                            if(oData.previousCount<=0){
                                  $('div.more_messagesuser').fadeOut();
                            }
                        }
                    }
                });
            }
   });
   $('button.btn_clear_message').on('click',function(){
       bootbox.confirm(s_dialog2_button_label_confirmation_delete_message, function(result) {
                if (result) {
               if (b_ajax) {                
                    $.ajax({
                        url: s_url_message_delete + jq_jsonp_callback
                        , data:{                        
                        }               
                        , success: function(j_response) {
                            if (evalResponse(j_response)) {
                             bootbox.alert(s_message_delete);
                            location.reload();   
                            }                            
                        }
                    });
               }
           }
       });       
   });
   
    $("#select_user_message").on('change',function(){
        $('#displaysCantMessages').html('');
        $('.more_cantmessagesuser').css('display','none');
       var rowid=$(this).val();
       numPagUser = 0;
       numbAvailableMoreMessageUserAjax = true;
       m_user_last_notification = 0;
       if(rowid !=''){
           $.ajax({
                url:s_url_LastNotificationUser,
                // async: false,
                data:{
                    idUser: rowid
                },
                success:function(j_response){
                    if (evalResponse(j_response)) {
                         m_user_last_notification = j_response.data;                     
                        fnGetMsgByUser(rowid, numPagUser ,  m_user_last_notification, numbAvailableMoreMessageUserAjax );
                    }                
                }
            });
       }              
   });
   
   fill_select_message();
   
   $("#select_user_message").select2({
       placeholder: s_place_holder_message
       ,width: 'resolve'
       ,allowClear: true
   }); 
   $('div.more_cantmessagesuser').on('click',function(){
       fnGetMsgByUser($('.more_cantmessagesuser').data('user_id')
            , $('.more_cantmessagesuser').data('numPagUser')
            ,  m_user_last_notification, true);            
   });
});
function fnGetMsgByUser(idUser, numPagUser, m_user_last_notification, numbAvailableMoreMessageUserAjax){
    if(numbAvailableMoreMessageUserAjax){        
        $.ajax({
            url:s_url_cantmessagesuser_list,
            data:{
                n_page:numPagUser,
                idUser: idUser,
                lastNotification:m_user_last_notification
            },
            success:function(j_response){
                if (evalResponse(j_response)) {
                    console.log(j_response);
                    var oResponse=j_response.data;
                    m_user_last_notification_new = oResponse.lastNotification;
                    numPagUser ++;
                            
                    $('#displaysCantMessages').fadeIn();
                    $('.more_cantmessagesuser').data('numPagUser', numPagUser);
                    $('.more_cantmessagesuser').data('user_id', idUser);                    
                    $('.more_cantmessagesuser').data('m_user_last_notification_new', m_user_last_notification_new);
                    
                    numbAvailableMoreMessageUserAjax = true;
//                            ob_ajax_user.ajax(); 
                    $.each(oResponse, function(i, valor){  
                        if(this.hasOwnProperty('idNotification')){ 
                            c_html = '<div class="cantMessageUser well well-small" id="messageUser_id_'+valor.idNotification+'" unix_time="'+valor.lastNotification+'">';                            
                            c_html += '<div class="cont_cantmessageuser control-group">';              
                            c_html += '<span><i class="icon-circle-arrow-right"></i></span>';
                            c_html += '<span class="nameBold">'+valor.destination+'</span>';
                            c_html += '</div>';
                            c_html += '<div class="control-group">';
                            c_html += '<span class="text-info">'+valor.message+'</span>';                            
                            c_html += '</div>';                            
                            c_html += '<div class="control-group text-center muted">';
                            c_html += '<span>'+valor.creation_dates+'</span>';
                            c_html += '</div>';
                            c_html += '</div>';                            

                            $('#displaysCantMessages').append(c_html);
                        }
                    });

                    if(oResponse.previousCount>0){
                        $('div.more_cantmessagesuser').fadeIn();
                        $('div.more_cantmessagesuser').html(oResponse.previousCount +' '+ s_message_previus);                        
                    }else{
                        $('div.more_cantmessagesuser').fadeOut();
                    } 
                }   
            }
                });        
            }
        }
function fill_select_message(){
    $.ajax({
        url: s_url_message_getListCantMessages + jq_jsonp_callback
        , data:{                        
        }               
        , success: function(j_response) {
            if (evalResponse(j_response)) {
                var data=j_response.data;                
               var option='<option value=""></option>';               
               $.each(data,function(i,d){
                   if(parseInt(d.numberMessages)>0){
                       option+='<option value="'+d.ID+'">'+d.user+'==>'+d.numberMessages+'</option>';
                   }                       
               });
               $("#select_user_message").html(option);   
            }            
        }
    });
}