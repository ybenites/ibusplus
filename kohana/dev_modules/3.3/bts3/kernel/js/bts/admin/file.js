var jqgrid_file;
$(document).ready(function(){
    
    $("#ffile_type").select2({
        placeholder: s_place_holder_type,
        width: 'resolve',
        allowClear: true
    });
    
    jqgrid_file = $('#jqgrid_file').jqGrid({
        url: s_url_file_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_file_col_names,
        colModel: a_jqgrid_file_col_model,
        pager: "#jqgrid_file_pager",        
        height: 'auto',              
        viewrecords: true,
        multiselect: true,
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'fi.idFile',
        onSelectAll: function(){
            if( jqgrid_file.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        onSelectRow: function(row_id){
           if( jqgrid_file.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        afterInsertRow: function(row_id, row_data){            
            //Actions
            var s_html_file_edit = '<a data-id=' + row_id + ' class="btn btn-mini file_edit"><i class="icon-pencil"></i></a>',
            s_html_file_delete = '<a data-id=' + row_id + ' class="btn btn-mini file_delete"><i class="icon-trash"></i></a>';

            jqgrid_file.setRowData(row_id, {
                file_actions: s_html_file_edit + s_html_file_delete
            });
        },
        gridComplete: function() {            
            $(jqgrid_file.getGridParam('pager')).find("option[value=-1]").text(s_file_select_text_all);
            $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
        }
    });
    
    //SUBMIT on CLICK
    $('#modal_file_btn_save').click(function() {
        $('#form_file').submit();
    });

    //REGEX Key
    $.validator.addMethod("rule_ffile_key", function(value, element) {
        return /^[A-Z][A-Z_]+$/.test(value);
    }, s_validate_required_ffile_key_regex);

    //VALIDATE FORM OPTION
    form_file= $('#form_file').validate({
        rules: {
            ffile_key: {
                required: true
                , rule_ffile_key: true
            }
            , ffile_name: 'required'
            , ffile_type: 'required'
        },
        messages: {
            ffile_key: {
                required: s_validate_required_ffile_key
            }
            , ffile_name: {
                required: s_validate_required_ffile_name
            }
            , ffile_type: {
                required: s_validate_required_ffile_type
            }            
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_file_create;
                if($('#ffile_id').val() != ''){
                    s_url = s_url_file_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_file_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_file_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                         if(evalResponse(j_response)){                                
                            $('#modal_file').modal('hide');                            
                            bootbox.alert(s_file_saved);
                            jqgrid_file.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    
    $('#modal_file').on('hidden', function() {
        $('#ffile_type').select2("val", "").trigger('change');
        form_file.currentForm.reset();
        form_file.resetForm();
        $('input[type=hidden]').val('');
        $('input, select, textarea', form_file.currentForm).parents('.control-group').removeClass('error');
    });
    
    $('table#jqgrid_file').on('click', '.file_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_file_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if(evalResponse(j_response)){    
                        var o_file = j_response.data;
                        $('#ffile_type').select2("val", [o_file.file_type]);                        
                        $('#ffile_id').val(o_file.file_key);
                        $('#ffile_key').val(o_file.file_key);
                        $('#ffile_name').val(o_file.file_name);
                        if(Boolean(eval(o_file.file_isCoreFile)))
                            $('#ffile_isCoreFile').prop('checked',true);
                        else
                            $('#ffile_isCoreFile').prop('checked',false);
                        $('#modal_file').modal('show');
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_file').on('click', '.file_delete', function() {
        if (b_ajax) {
            var $this = $(this);
             bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_file_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {                                    
                                    bootbox.alert(s_file_deleted);
                                    jqgrid_file.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
    $('#modal_share_hosts_btn_save').click(function(){
        $('#form_share_hosts').submit();
    });
    
    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {                
                var o_share = {'fshare_instances[]': jqgrid_file.getGridParam('selarrrow')};
                var o_data = $.extend($(o_form).serializeObject(), o_share);
                $.ajax({
                    url: s_url_file_share + jq_jsonp_callback
                    , data: o_data
                    , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                         if(evalResponse(j_response)){                          
                            $('#modal_share_hosts').modal('hide');                         
                            bootbox.alert(s_file_shared);
                        }
                    }
                });
            }
        }
    });
});