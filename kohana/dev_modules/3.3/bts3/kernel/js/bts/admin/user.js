var jqgrid_user;

function fnBTSCheckUsernameStrength(password) {

    //initial strength
    var strength = 0

//    //if the password length is less than 6, return message.
//    if (password.length < 6) {
//        return strength;
//    }

    //length is ok, lets continue.

    //if length is 8 characters or more, increase strength value
    if (password.length >= 6)
        strength += 1;
    //if password contains both lower and uppercase characters, increase strength value
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
        strength += 1;
    //if it has numbers and characters, increase strength value
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
        strength += 1;
    //if it has one special character, increase strength value
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
        strength += 1;
    //if it has two special characters, increase strength value
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/))
        strength += 1;
    //now we have calculated strength value, we can return messages

    return strength;
}

$(document).ready(function() {


    jqgrid_user = $('#jqgrid_user').jqGrid({
        url: s_url_user_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_user_col_names,
        colModel: a_jqgrid_user_col_model,
        pager: "#jqgrid_user_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        rowList: [50, 100, -1],
        sortname: 'us.idUser',
        afterInsertRow: function(row_id, row_data) {

            var s_root_option_icon = 'icon-minus';

            var s_html_user_edit = '<a data-id=' + row_id + ' class="btn btn-mini user_edit"><i class="icon-pencil"></i></a>',
                    s_html_user_delete = '<a data-id=' + row_id + ' class="btn btn-mini user_delete"><i class="icon-trash"></i></a>';

            if (row_data.user_root_option == 1)
                s_root_option_icon = 'icon-star';


            jqgrid_user.setRowData(row_id, {
                user_root_option: '<a data-id=' + row_id + ' class="user_change_root_option"><i class="' + s_root_option_icon + '"></i></a>',
                user_actions: s_html_user_edit + s_html_user_delete
            });
        },
        gridComplete: function() {
            $(jqgrid_user.getGridParam('pager')).find("option[value=-1]").text(s_user_select_text_all);
        }
    });

    $('#fuser_password').keyup(function() {
        var i_strength = fnBTSCheckUsernameStrength($(this).val());

        $('#fuser_password_strength div').css({display: 'none'});

        if ($(this).val().length == 0) {

        } else if (i_strength < 2) {
            $('#fuser_password_strength div:eq(0)').css({display: 'block'});
        } else if (i_strength >= 2 && i_strength <= 3) {
            $('#fuser_password_strength div:eq(0), #fuser_password_strength div:eq(1)').css({display: 'block'});
        } else if (i_strength >= 4) {
            $('#fuser_password_strength div').css({display: 'block'});
        }
    });
    //REGEX Username
    $.validator.addMethod("rule_fuser_username", function(value, element) {
        return /^[a-zA-Z][a-zA-Z0-9]+$/i.test(value);
    }, s_validate_required_fuser_username_regex);

    $.validator.addMethod("rule_fuser_password_strength", function(value, element, params) {
        if (fnBTSCheckUsernameStrength(value) > 1)
            return true;
        else
            return false;
    }, s_validate_required_fuser_password_strength);

    //SUBMIT on CLICK
    $('#modal_user_btn_save').click(function() {
        $('#form_user').submit();
    });

    //VALIDATE FORM OPTION
    form_user = $('#form_user').validate({
        rules: {
            fuser_username: {
                required: true
                        , minlength: 6
                        , maxlength: 12
                        , rule_fuser_username: true
            }
            , fuser_password: {
                required: function() {
                    return $('#fuser_change_password').val() == 1;
                }
                , rule_fuser_password_strength: function() {
                    return $('#fuser_change_password').val() == 1;
                }
            }
            , fuser_password_verify: {
                required: function() {
                    return $('#fuser_change_password').val() == 1;
                }
                , equalTo: "#fuser_password"
            }
        },
        messages: {
            fuser_username: {
                required: s_validate_required_fuser_username
                        , minlength: s_validate_required_fuser_username_minlength
                        , maxlength: s_validate_required_fuser_username_maxlength
            }
            , fuser_password: {
                required: s_validate_required_fuser_password
            }
            , fuser_password_verify: {
                required: s_validate_required_fuser_password_verify
                        , equalTo: s_validate_required_fuser_password_verify_equal
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_user_create;
                if ($('#fuser_id').val() != '') {
                    s_url = s_url_user_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                            , data: $(o_form).serialize()
                            , beforeSend: function() {
                        $('#modal_user_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_user_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_user').modal('hide');
                            bootbox.alert(s_user_saved);
                            jqgrid_user.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });

    $('#modal_user').on('hidden', function() {
        $('#btn_change_pass').css({display: 'none'});
        $('#fuser_password_strength div').css({display: 'none'});
        form_user.currentForm.reset();
        form_user.resetForm();
        $('input[type=hidden]', form_user.currentForm).val('');
        $('input, select, textarea', form_user.currentForm).parents('.control-group').removeClass('error');
        $('#user_photo').attr('src', 'http://static.o.bts.mcets-inc.com/kernel/img/profile/128/default.png');
    });

    $('table#jqgrid_user').on('click', '.user_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            showloadingDialog = 1;
            $.ajax({
                url: s_url_user_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var o_user = j_response.data;
                        $('#fuser_id').val(o_user.user_id);
                        $('#fuser_group').val(o_user.user_group);
                        $('#fuser_office').val(o_user.user_office);
                        $('#fuser_username').val(o_user.user_username);
                        $('input[name=fuser_root_option]').val([o_user.user_root_option]);
                        $('input[name=fuser_own_session]').val([o_user.user_own_session]);
                        $('#fuser_name').val(o_user.user_name);
                        $('#fuser_lastname').val(o_user.user_lastname);
                        $('#fuser_address').val(o_user.user_address);
                        $('#fuser_phone').val(o_user.user_phone);
                        $('#fuser_email').val(o_user.user_email);
                        $('#fuser_photo_data').val('');
                        $('#user_photo').attr('src', o_user.user_photo);
                        $('#fuser_change_password').val(1);
                        $('#btn_change_pass').css({display: 'inline-block'}).trigger('click');
                        $('#modal_user').modal('show');
                    }
                }
            });
        }
    });

    $('table#jqgrid_user').on('click', '.user_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_user_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_user_deleted);
                                    jqgrid_user.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });

    $('#btn_change_pass').click(function() {
        if ($('#fuser_change_password').val() == 1) {
            $('.div_user_password').css({display: 'none'});
            $('#fuser_change_password').val('');
        } else {
            $('.div_user_password').css({display: 'block'});
            $('#fuser_change_password').val(1);
        }
    });   

    var o_user_img_upload = fnCreateUploadFileInstance(
            document.getElementById('btnImgUpload'),
            s_url_user_upload_img,
            function() {
                $('#user_photo').attr('src', '/kernel/img/wait_loading.gif');
            },
            function(id, fileName, responseJSON) {
                //success                
                $('#user_photo').attr('src', responseJSON.data.path);
                $('#fuser_photo_data').val(responseJSON.data.tmpfn);
            },
            function(id, fileName, responseJSON) {
                $('#user_photo').attr('src', 'http://static.o.bts.mcets-inc.com/kernel/img/profile/128/default.png');
                bootbox.alert(responseJSON.msg);
            },
            function() {
                //after_create
                $('div#btnImgUpload ul.qq-upload-list').addClass('hide');
                $('div#btnImgUpload div.qq-upload-button').addClass('btn controls');
                $('div#btnImgUpload').attr('style', 'margin-bottom:10px;');
            }
    );
        jqgrid_user_active();
        $('table#jqgrid_user_active').on('click', '.user_remove_final', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_user_remove_final + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_user_deleted);
                                    jqgrid_user_active.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
        $('table#jqgrid_user_active').on('click', '.user_active', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_active_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_user_active+ jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if (evalResponse(j_response)) {
                                    bootbox.alert(s_user_active);
                                    jqgrid_user.trigger('reloadGrid');
                                    jqgrid_user_active.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    $("#modal_user_active").on('show',function(){
        jqgrid_user_active.trigger('reloadGrid');
    });
});
function jqgrid_user_active(){
    jqgrid_user_active = $('#jqgrid_user_active').jqGrid({
        url: s_url_user_active_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_user_active_col_names,
        colModel: a_jqgrid_user_active_col_model,
        pager: "#jqgrid_user_active_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        rowList: [50, 100, -1],
        sortname: 'us.idUser',
        afterInsertRow: function(row_id, row_data) {          
            var s_html_user_active= '<a data-id=' + row_id + ' class="btn btn-mini user_active"><i class="icon-circle-arrow-left"></i></a>',
                    s_html_user_delete = '<a data-id=' + row_id + ' class="btn btn-mini user_remove_final"><i class="icon-remove-sign"></i></a>';

            jqgrid_user_active.setRowData(row_id, {                
                user_actions: s_html_user_active + s_html_user_delete
            });
        },
        gridComplete: function() {
            $(jqgrid_user_active.getGridParam('pager')).find("option[value=-1]").text(s_user_select_text_all);
        }
    });
}