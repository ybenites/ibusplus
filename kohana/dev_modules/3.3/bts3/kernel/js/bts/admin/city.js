$(document).ready(function() {
    var a_countries_search = s_country_search.split(',');
    
    var a_url_countries_search = $.map(a_countries_search, function(value, i){
        return 'defaultCountry[]='+value;
    });
    
    var s_url_city = 'http://globalm.mcets-inc.com/private/city/listCitiesBus?'+a_url_countries_search.join('&')+'&jsoncallback=?';    
    
    $.getJSON(s_url_city, function(data) {        
        $.each(data, function(){
            $('#ul_container_cities_list').append('<li data-country="'+this.country+'" data-name="'+this.name+'" id="key_'+this.idCity+'" class="alert"><i class="icon-remove"></i><i class="icon-plus"></i>'+this.country+', '+this.name+'</li>');
        });
    });
    
    $("#ul_container_cities_list, #ul_container_cities_saved").sortable({
        connectWith: ".connect_list_cities"
    });
    
    $('#ul_container_cities_list').on('click', 'i.icon-plus', function() {
        $('#ul_container_cities_saved').append($(this).parents('li'));
        $("#ul_container_cities_list, #ul_container_cities_saved").trigger("sortupdate");
    });
    
    $('#ul_container_cities_saved').on('click', 'i.icon-remove', function() {
        $('#ul_container_cities_list').append($(this).parents('li'));
        $("#ul_container_cities_list, #ul_container_cities_saved").trigger("sortupdate");
    });
    
    form_city_saved = $('#form_city_saved').validate({        
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        },
        submitHandler: function(o_form) {
            var a_cities_data = $.map($("#ul_container_cities_saved").sortable("toArray"), function(val, i) {
                return {
                    city_id:val.substring(val.indexOf("key_") + 4)
                    , city_name: $('#'+val).data('name')
                    , city_country: $('#'+val).data('country')
                };
            });  
            
            if(b_ajax){
                $.ajax({
                    url: s_url_city_save+ jq_jsonp_callback
                    , data: {
                        'fcities_saved' : a_cities_data
                    } 
                    , beforeSend: function() {
                        $('#form_city_saved_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#form_city_saved_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        b_ajax = true;
                        if(evalResponse(j_response)){    
                            location.reload();
                        }
                    }
                });
            }
        }
    });
    
    $('#fcity_list_search').keyup(function(){
        var $this = $(this);
        $('#ul_container_cities_list li').addClass('hidden');
        if($this.val()!=''){
            var a_search_result_list = $('#ul_container_cities_list li').filter(function(i){
               
                return !$(this).hasClass('hidden_search') && ($(this).text().toUpperCase()).indexOf($this.val().toUpperCase()) > -1;
            });
            $(a_search_result_list).removeClass('hidden');
            
        }else{
            $('#ul_container_cities_list li:not(.hidden_search)').removeClass('hidden');
        }
    });
    
    $('#fcity_saved_search').keyup(function(){
        var $this = $(this);
        $('#ul_container_cities_saved li').addClass('hidden');
        if($this.val()!=''){
            var a_search_result_list = $('#ul_container_cities_saved li').filter(function(i){
               
                return !$(this).hasClass('hidden_search') && ($(this).text().toUpperCase()).indexOf($this.val().toUpperCase()) > -1;
            });
            $(a_search_result_list).removeClass('hidden');
            
        }else{
            $('#ul_container_cities_saved li:not(.hidden_search)').removeClass('hidden');
        }
    });
    
});