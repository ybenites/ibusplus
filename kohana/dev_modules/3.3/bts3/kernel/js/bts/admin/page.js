//VARs to USE on WINDOWS
var form_option;
var jqgrid_option;
$(document).ready(function() {
    //REGEX Key
    $.validator.addMethod("rule_fpage_key", function(value, element) {
        return /^[A-Z][A-Z_]+$/.test(value);
    }, s_validate_required_fpage_key_regex);

    //SUBMIT on CLICK
    $('#modal_page_btn_save').click(function() {
        $('#form_page').submit();
    });
    //VALIDATE FORM OPTION
    form_page = $('#form_page').validate({
        rules: {
            fpage_key: {
                required: true
                , rule_fpage_key: true
            }
            , fpage_alias: 'required'            
        },
        messages: {            
            fpage_key: {
                required: s_validate_required_fpage_key
            }
            , fpage_alias: {
                required: s_validate_required_fpage_alias
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_page_create;
                if($('#fpage_id').val() != ''){
                    s_url = s_url_page_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#btn_process_form_page').button('loading');
                    }
                    , complete: function() {
                        $('#btn_process_form_page').button('reset');
                    }
                    , success: function(j_response) {
                       if(evalResponse(j_response)){                      
                            $('#modal_page').modal('hide');
                            bootbox.alert(s_page_saved);
                            jqgrid_page.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    
    $('#modal_share_hosts_btn_save').click(function(){
        $('#form_share_hosts').submit();
    });
    
    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {                
                var o_share = {'fshare_instances[]': jqgrid_page.getGridParam('selarrrow')};
                var o_data = $.extend($(o_form).serializeObject(), o_share);
                $.ajax({
                    url: s_url_page_share + jq_jsonp_callback
                    , data: o_data
                    , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                       if(evalResponse(j_response)){                          
                            $('#modal_share_hosts').modal('hide');
                            bootbox.alert(s_page_shared);                            
                        }
                    }
                });
            }
        }
    });
    
    jqgrid_page = $('#jqgrid_page').jqGrid({
        url: s_url_page_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_page_col_names,
        colModel: a_jqgrid_page_col_model,
        pager: "#jqgrid_page_pager",        
        height: 'auto',              
        viewrecords: true,
        rowNum: 50,
        rowList: [50,100,-1],
        sortname: 'pa.idPage',
        multiselect: true,
        onSelectAll: function(){
            if( jqgrid_page.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        onSelectRow: function(row_id){
           if( jqgrid_page.getGridParam('selarrrow').length > 0){               
               $('#btn_modal_share_hosts_share').removeClass('disabled').prop('disabled', false);
           }else{
               $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
           }
        },
        afterInsertRow: function(row_id, row_data){            
            //Actions
            var s_html_page_edit = '<a data-id=' + row_id + ' class="btn btn-mini page_edit"><i class="icon-pencil"></i></a>',
            s_html_page_delete = '<a data-id=' + row_id + ' class="btn btn-mini page_delete"><i class="icon-trash"></i></a>';

            jqgrid_page.setRowData(row_id, {
                page_actions: s_html_page_edit + s_html_page_delete
            });
        },
        gridComplete: function() {
            $(jqgrid_page.getGridParam('pager')).find("option[value=-1]").text(s_page_select_text_all);
            $('#btn_modal_share_hosts_share').addClass('disabled').prop('disabled', true);
        }
    });    
    
    $('#modal_page').on('hidden', function() {       
        form_page.currentForm.reset();
        form_page.resetForm();
        $('input[type=hidden]', form_page.currentForm).val('');
        $('input, select, textarea', form_page.currentForm).parents('.control-group').removeClass('error');
    });
    
    $('table#jqgrid_page').on('click', '.page_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_page_get + jq_jsonp_callback,
                data: {
                    id: $this.data('id')
                }
                , success: function(j_response) {                    
                    if(evalResponse(j_response)){    
                        var o_page = j_response.data;                        
                        $('#fpage_id').val(o_page.page_key);
                        $('#fpage_key').val(o_page.page_key);
                        $('#fpage_alias').val(o_page.page_alias);
                        $('#modal_page').modal('show');
                    }
                }
            });
        }
    });
    
    $('table#jqgrid_page').on('click', '.page_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                    if (b_ajax) {
                        $.ajax({
                            url: s_url_page_delete + jq_jsonp_callback,
                            data: {
                                id: $this.data('id')
                            }
                            , success: function(j_response) {
                                if(evalResponse(j_response)){    
                                    bootbox.alert(s_page_deleted);
                                    jqgrid_page.trigger('reloadGrid');
                                }
                            }
                        });
                    }
                }
            });
        }
    });
    
});