var jqgrid_translate;
var form_translate;

$(document).ready(function() {
    $("#textT").val('');
    $("#textTranslate").val('');
    // JQGRID OPTION
    jqgrid_translate = $('#jqgrid_translate').jqGrid({
        url: s_url_translate_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_translate_col_names,
        colModel: a_jqgrid_translate_col_model,
        pager: "#jqgrid_translate_pager",
        height: 'auto',
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        rowList: [50, 100, -1],
        sortname: 'tra.idTranslate',
        afterInsertRow: function(row_id, row_data) {
            //Actions
            var s_html_translate_edit = '<a data-id=' + row_id + ' class="btn btn-mini translate_edit"><i class="icon-pencil"></i></a>',
                    s_html_translate_delete = '<a data-id=' + row_id + ' class="btn btn-mini translate_delete"><i class="icon-trash"></i></a>';

            jqgrid_translate.setRowData(row_id, {actions: s_html_translate_edit + s_html_translate_delete, status: 'Activo'});
        }
    });

    /*Busqueda*/
    jqgrid_translate.filterToolbar({stringResult: true, searchOnEnter: false});


    // Editar traduccion
    $('table#jqgrid_translate').on('click', '.translate_edit', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_translate_get + jq_jsonp_callback,
                data: {
                    idTranslate: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var o_translate = j_response.data;
                        console.log(o_translate);
                        $('#idTranslate').val(o_translate.idTranslate);
                        $('#textT').val(o_translate.textT);
                        $('#textTranslate').val(o_translate.textTranslate);
                        $('#modal_save_translate').modal('show');
                    }
                }
            });
        }
    });


    $('#modal_save_translate').on('hidden', function() {
        form_translate.currentForm.reset();
        form_translate.resetForm();
        $('input[type=hidden]', form_translate.currentForm).val('');
        $('input, select, textarea', form_translate.currentForm).parents('.control-group').removeClass('error');
    });


    //change estado
    $('table#jqgrid_translate').on('click', '.translate_delete', function() {
        if (b_ajax) {
            var $this = $(this);
            $.ajax({
                url: s_url_translate_change_status + jq_jsonp_callback,
                data: {
                    idTranslate: $this.data('id')
                }
                , success: function(j_response) {
                    if (evalResponse(j_response)) {
                        bootbox.alert(s_translate_saved);
                        jqgrid_translate.trigger('reloadGrid');
                    }
                }
            });
        }
    });


    $('#modal_bt_translate').click(function() {
        $('#form_translate').submit();
    });


    //validar-guardado translate

    form_translate = $('#form_translate').validate({
        rules:
                {textT: {required: true}, textTranslate: {required: true}},
        messages: {
            textT: {
                required: s_validate_required_textT
            }
            , textTranslate: {
                required: s_validate_required_textTranslate
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }, submitHandler: function(form) {
            if (b_ajax) {
                var s_url = s_url_translate_create;
                if ($('#idTranslate').val() != '') {
                    s_url = s_url_translate_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                            , data: $(form).serialize()
                            , beforeSend: function() {
                        $('#modal_bt_translate').button('loading');
                    }
                    , complete: function() {
                        $('#modal_bt_translate').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_save_translate').modal('hide');
                            bootbox.alert(s_translate_saved);
                            jqgrid_translate.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });

    $('#modal_share_hosts_btn_save').click(function() {
        $('#form_share_hosts').submit();
    });

    form_share_hosts = $('#form_share_hosts').validate({
        rules: {
            'fshare_hosts[]': {
                required: true
            }
        },
        messages: {
            'fshare_hosts[]': {
                required: s_validate_required_fshare_hosts
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var o_data = $(o_form).serializeObject();
                $.ajax({
                    url: s_url_translate_share + jq_jsonp_callback
                            , data: o_data
                            , beforeSend: function() {
                        $('#modal_share_hosts_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_share_hosts_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if (evalResponse(j_response)) {
                            $('#modal_share_hosts').modal('hide');
                            bootbox.alert(s_translate_shared);
                        }
                    }
                });
            }
        }
    });
});
