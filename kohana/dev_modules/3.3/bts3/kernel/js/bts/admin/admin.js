if (v_user_group_id== v_user_group_root){
    var directionDisplay;
        var directionsService = new google.maps.DirectionsService();
        var map;
        function geolocate(ip,timezone, cityPrecision) {
            var key = '4bf434140059fdcc099eb60b4302a7166d7dfe9bf758fe16604463c5d33a57e4';
            var api = (cityPrecision) ? "ip-city" : "ip-country";
            var domain = 'api.ipinfodb.com';
            var version = 'v3';
            var url = "http://" + domain + "/" + version + "/" + api + "/?key=" + key +"&ip="+ip+"&format=json" + "&callback=?";
            var geodata;
            var JSON = JSON || {};
            JSON.stringify = JSON.stringify || function (obj) {
                var t = typeof (obj);
                if (t != "object" || obj === null) {
                    if (t == "string") obj = '"'+obj+'"';
                    return String(obj);
                } else {
                    var n, v, json = [], arr = (obj && obj.constructor == Array);
                    for (n in obj) {
                        v = obj[n]; t = typeof(v);
                        if (t == "string") v = '"'+v+'"';
                        else if (t == "object" && v !== null) v = JSON.stringify(v);
                        json.push((arr ? "" : '"' + n + '":') + String(v));
                    }
                    return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
                }
            };

            JSON.parse = JSON.parse || function (str) {
                if (str === "") str = '""';
                eval("var p=" + str + ";");
                return p;
            };                    
            this.checkcookie = function(callback) { 
                                            
                getGeolocation(callback);
                                            
            }
            this.getField = function(field) {
                try {
                    return geodata[field];
                } catch(err) {}
            }
                                        
            function getGeolocation(callback) {
                try {
                    $.getJSON(url,
                    function(data){
                        if (data['statusCode'] == 'OK') {
                            geodata = data;
                            JSONString = JSON.stringify(geodata);
                            callback();
                        }
                    });
                } catch(err) {}
            }
                                        
        }
        function initialize(lat,longt) {
            var latlng = new google.maps.LatLng(lat,longt);
            var myOptions = {
                zoom: 10,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
                                
            var map = new google.maps.Map(document.getElementById('canvas_map'), myOptions); 
            var infowindow = new google.maps.InfoWindow({
                map: map,
                position: latlng,
                content: 'Informacion de Ubicacion Actual.'
            });
                                
            var marker = new google.maps.Marker({
                position: latlng, 
                map: map, 
                zoom: 10,
                title:"IBUSPLUS SAC"
            });   
            marker.setMap(map);
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
                                    
            });
        }
}
$(document).ready(function(){    
	// This function is executed once the document is loaded
	
	// Caching the jQuery selectors:
	var count = $('.onlineWidget .count');
	var panel = $('.onlineWidget .panel');
	var timeout;
	
	// Loading the number of users online into the count div:
//	count.load('/private/user/getCountUsersOnline');
        if(b_BTS_CHAT){
            websocket.on("getCountUsersOnline",function(data){            
                count.html(data);
            });
        }        
	$('.onlineWidget').hover(
		function(){
			// Setting a custom 'open' event on the sliding panel:
			
			clearTimeout(timeout);
			timeout = setTimeout(function(){panel.trigger('open');},500);
		},
		function(){
			// Custom 'close' event:
			
			clearTimeout(timeout);
			timeout = setTimeout(function(){panel.trigger('close');},500);
		}
	);
	
	var loaded=false;	// A flag which prevents multiple ajax calls to geodata.php;
	
	// Binding functions to custom events:	
	panel.bind('open',function(){
		panel.slideDown(function(){
			if(!loaded)
			{
				// Loading the countries and the flags once the sliding panel is shown:
//				panel.load('/private/user/getUsersOnline');                                
                                
                                websocket.emit('getUsersOnline','');
                                websocket.on("getUsersOnline",function(data){
                                    panel.html(data);
                                    loaded=true;
                                });
			}
		});
	}).bind('close',function(){
		panel.slideUp();
                loaded=false;
	});
        
         $('div.onlineWidget').on('click','img.userInfo',function()
            {
                idSessionlog = $(this).attr('rel');
                idUser = $(this).attr('id');                
                $.ajax({
                    url:'/private/user/getInfoUserOnline'+jq_jsonp_callback,
                    data: {'idUser':idUser,
                        'idSessionlog':idSessionlog
                    },
                    success: function(j_response){
                       if(evalResponse(j_response)){    
                            var sessionInfo = '';
                            var userInfo = '';
                            sessionInfo = j_response.data.sessionInfo[0];
                            userInfo = j_response.data.userInfo[0];                       
                            $('span#nombreCompleto').text(userInfo.fullName);
                            $('span#usuario').text(userInfo.userName);
                            $('span#idUser').text(userInfo.idUser);
                            $('span#oficina').text(userInfo.officeName);
                            $('span#grupo').text(userInfo.groupName);
                            $('span#navegador').text(sessionInfo.agent);
                            $('span#sessionId').text(sessionInfo.sessionId);
                            $('span#lastActive').text(sessionInfo.lastActive);
                            $('span#fecha').text(sessionInfo.eventDate);
                            $('span#ip').text(sessionInfo.ip);                           
                            $('div#infoUserOnline').modal('show');
                            var visitorGeolocation = new geolocate(sessionInfo.ip,false, true);
                            var callback = function(){
                                initialize(visitorGeolocation.getField('latitude'),visitorGeolocation.getField('longitude') );
                            };
                            visitorGeolocation.checkcookie(callback);
                                                
                        }
                    }
                });                                        
            });
});