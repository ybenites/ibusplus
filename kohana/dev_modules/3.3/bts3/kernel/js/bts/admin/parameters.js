$(document).ready(function(){    
    var frm_parameter=$("#frm_parameter").validate({
        rules:{},
        messages:{ },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        },
        submitHandler: function(o_form) {
    
        }
    });
    $(".saveParamameter").on('click',function(e){        
        e.preventDefault();
        if(!$("#frm_parameter").valid())
            return false;
        var value=$(this).prev().val();
        var param=$(this).prev().data('param'); 
        if(b_ajax){
            bootbox.confirm(s_dialog2_button_label_confirmation_update_parameter, function(result) {
                if (result) {
                    if(b_ajax){                                                
                        $.ajax({
                        url: url_parameter_update + jq_jsonp_callback
                        , data:{
                            value:value,
                            param:param
                        }
                        , success: function(j_response) {
                              if(evalResponse(j_response)){                        
                                bootbox.alert(s_parameter_updated);                        
                            }
                        }
                    });
                    }
                }
            });
        }                                         
    });
    $("form#frm_parameter input").on('keyup',function(e){
        e.preventDefault();
        if(e.keyCode==13){
            $(this).next().trigger('click');
        }        
    });
});