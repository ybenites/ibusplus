<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsPerson
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsPerson extends Kohana_BtsOrm implements Kohana_BtsConstants{
    protected $_table_names_plural = false;
    protected $_table_name = 'bts_person';
    protected $_primary_key = 'idPerson';
    
    protected $_db_group =self::BTS_HELP_TASK_INSTANCE_CONNECTION;
}