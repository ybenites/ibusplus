<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_Groupnotification extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_group_notification';
    protected $_primary_key = 'idGroupNotification';

    public function saveArray($a_params) {
        $this->name = trim($a_params['name']);
        parent::save();
    }
}