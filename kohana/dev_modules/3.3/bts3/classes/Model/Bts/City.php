<?php

class Model_Bts_City extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_city';
    protected $_primary_key = 'idCity';

    public static function fnGetAll() {
        $a_select =  DB::select(
                                array('ci.idCity', 'city_id')
                        )
                        ->from(array('bts_city', 'ci'))                        
                        ->where('ci.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('ci.name')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_City($a_item['city_id']);
        }, $a_select);
    }
    
    public function clearAll(){
        $o_model_city = new Model_Bts_City;
        $a_model_cities = $o_model_city                    
                    ->and_where('status', '=', self::BTS_STATUS_ACTIVE)
                    ->find_all();
        foreach($a_model_cities as $o_city){
            $o_city->status = self::BTS_STATUS_DEACTIVE;
            $o_city->save();
        }
    }

    public function saveList($a_list){        
        foreach ($a_list as $i_key => $a_city){
            $o_model_city = new Model_Bts_City($a_city['city_id']);
            
            if(!$o_model_city->loaded()){                
                $o_model_city->idCity = $a_city['city_id'];
                $o_model_city->name = trim($a_city['city_name']);
                $o_model_city->country = $a_city['city_country'];
            }
            
            $o_model_city->status = self::BTS_STATUS_ACTIVE;
            
            $o_model_city->save();
        }
    }
}