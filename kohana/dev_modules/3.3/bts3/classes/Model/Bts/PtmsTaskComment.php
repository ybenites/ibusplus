<?php 
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsTaskComment extends Kohana_BtsOrm implements Kohana_BtsConstants{
     protected $_table_names_plural = false;
    protected $_table_name = 'pt_taskcomment';
    protected $_primary_key = 'idTaskComment';
    protected $_db_group =self::BTS_HELP_TASK_INSTANCE_CONNECTION;
    
    
    public function fnGetCommentByTaskArray($i_task) { 
        
        $a_result = DB::select(
                        array('tc.idTaskComment', 'task_comment_id')
                        ,array('tc.idTask', 'task_id')
                        ,array('tc.idUser', 'task_user_id')
                        ,array('tc.userType', 'task_user_type')
                        ,array('tc.comment', 'task_comment')
                        ,array('tc.creationDate', 'task_comment_date')
                )
                ->from(array('pt_taskcomment', 'tc'))                
                ->where('tc.idTask', '=', $i_task)
                ->order_by('tc.creationDate', 'DESC')
                ->execute($this->_db_group)
                ->as_array();
        
        foreach($a_result as $i_key => $a_item){            
            $a_user_create = array();            
            if ($a_item['task_user_type'] == self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER) {
                $o_model_user = new Model_Bts_User($a_item['task_user_id']);
                $a_user_create['user_name'] = $o_model_user->person->fullName;
                $a_user_create['user_image'] = $o_model_user->imgProfile;
            } elseif ($a_item['task_user_type'] == self::BTS_HELP_TASK_CREATE_TYPE_SYSTEM) {
                $o_model_user = new Model_Bts_PtmsUser($a_item['task_user_id']);
                $a_user_create['user_name'] = $o_model_user->person->fullName;
                $a_user_create['user_image'] = 'http://ptms.ibusplus.com' . $o_model_user->imgProfile;
            } elseif ($a_item['task_user_type'] == self::BTS_HELP_TASK_CREATE_TYPE_EMAIL) {
                $a_user_create['user_name'] = 'Email';
                $a_user_create['user_image'] = 'none';
            }            
            $a_result[$i_key]['task_comment_date'] = HelperDate::fnGetDateUpdate($a_item['task_comment_date']);
            $a_result[$i_key]['user_comment'] = $a_user_create;
            
        }               
        
        return $a_result;
    }
    
    public function saveArray($a_params) {
        $this->idTask = $a_params['idTask'];
        $this->idUser = $a_params['idUser'];     
        $this->comment = $a_params['comment'];     
        $this->userType = $a_params['userType'];     
        parent::save();
    }
}

