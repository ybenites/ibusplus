<?php

class Model_Bts_Page extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_page';
    protected $_primary_key = 'idPage';

    public static function fnValidKey($s_key, $s_key_page = null) {
        //Verificar si la KEY existe
        $o_select = DB::select(array(DB::expr('COUNT(*)'), 'total'))
                ->from('bts_page')
                ->where('idPage', '=', $s_key);

        if ($s_key_page) {
            //Al momento de editar
            $o_select->and_where('idPage', '<>', $s_key_page);
        }
        return !$o_select->execute()
                        ->get('total');
    }

    public function saveArray($a_params) {
        $this->idPage = $a_params['idPage'];
        $this->alias = $a_params['alias'];
        parent::save();
    }

    public function saveInstance($s_instance, $o_page) {
        $a_page_db = DB::select(
                        array(DB::expr('COUNT(*)'), 'total')
                )
                ->from('bts_page')
                ->where('idPage', '=', $o_page->idPage)
                ->execute($s_instance)
                ->get('total');

        if (!$a_page_db) {
            $a_page_insert = array(
                "idPage" => $o_page->idPage
                , "alias" => $o_page->alias
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );
            DB::insert('bts_page')
                    ->values(array_values($a_page_insert))
                    ->columns(array_keys($a_page_insert))
                    ->execute($s_instance);
        }
    }

    public static function fnGetAll() {
        $a_select =  DB::select(
                                array('pa.idPage', 'page_id')
                        )
                        ->from(array('bts_page', 'pa'))
                        ->where('pa.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('pa.idPage')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_Page($a_item['page_id']);
        }, $a_select);
    }
}