<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_Person extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_person';
    protected $_primary_key = 'idPerson';

    public function saveArray($a_params) {
        $this->document       = !empty($a_params['document'])?$a_params['document']:NULL;
        $this->idDocumentType = !empty($a_params['idDocumentType'])?$a_params['idDocumentType']:NULL;
        $this->fName          = trim($a_params['fName']);
        $this->lName          = trim($a_params['lName']);
        $this->fullName       = trim($a_params['fullName']);
        $this->sex            = !empty($a_params['sex'])?$a_params['sex']:NULL;
        $this->phone1         = !empty($a_params['phone1'])?$a_params['phone1']:NULL;
        $this->phone2         = !empty($a_params['phone2'])?$a_params['phone2']:NULL;
        $this->cellPhone      = !empty($a_params['cellPhone'])?$a_params['cellPhone']:NULL;
        $this->email          = !empty($a_params['email'])?$a_params['email']:NULL;
        $this->address1       = !empty($a_params['address1'])?$a_params['address1']:NULL;
        $this->address2       = !empty($a_params['address2'])?$a_params['address2']:NULL;
        $this->type           = !empty($a_params['type'])?$a_params['type']:NULL;
        $this->dob            = !empty($a_params['dob'])?$a_params['dob']:NULL;
        parent::save();
    }
    
}