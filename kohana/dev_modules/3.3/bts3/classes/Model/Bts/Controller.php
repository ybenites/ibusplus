<?php

class Model_Bts_Controller extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_controller';
    protected $_primary_key = 'idController';
    
    public function saveArray($a_params) {

        $this->directory = $a_params['directory'];
        $this->controller = $a_params['controller'];
        $this->action = $a_params['action'];
        $this->type = $a_params['type'];        

        parent::save();
    }
    
    public function saveInstance($s_instance, $o_controller){
        
        $a_controller_db = DB::select(
                            array(DB::expr('COUNT(*)'), 'total')
                        )
                        ->from('bts_controller')
                        ->where('directory', '=', $o_controller->directory)
                        ->and_where('controller', '=', $o_controller->controller)
                        ->and_where('action', '=', $o_controller->action)
                        ->execute($s_instance)
                        ->get('total');
        
        if(!$a_controller_db){
            
            $a_controller_insert = array(
                "directory" => $o_controller->directory
                , "controller" => $o_controller->controller
                , "action" => $o_controller->action
                , "type" => $o_controller->type
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );
            
            DB::insert('bts_controller')
                    ->values(array_values($a_controller_insert))
                    ->columns(array_keys($a_controller_insert))
                    ->execute($s_instance);           
            
        }
        
    }
}