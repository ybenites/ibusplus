<?php

class Model_Bts_Option extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_option';
    protected $_primary_key = 'key';

    public static function fnValidKey($s_key, $s_key_option = null) {
        //Verificar si la KEY existe
        $o_select = DB::select(array(DB::expr('COUNT(*)'), 'total'))
                ->from('bts_option')
                ->where('key', '=', $s_key);

        if ($s_key_option) {
            //Al momento de editar
            $o_select->and_where('key', '<>', $s_key_option);
        }
        return !$o_select->execute()
                        ->get('total');
    }

    public function saveArray($a_params) {

        $this->key = trim($a_params['key']);
        $this->component = $a_params['component'];
        $this->description = trim($a_params['description']);
        $this->longDescription = trim($a_params['longDescription']);
        $this->dataType = $a_params['dataType'];
        $this->value = $a_params['value'];
        $this->rootOption = $a_params['rootOption'];
        $this->prefix = $a_params['prefix'];
        $this->suffix = $a_params['suffix'];

        parent::save();
    }

    public static function fnGetKeysByComponent($s_component) {
        return DB::select(
                                array('op.key', 'option_key')
                        )
                        ->from(array('bts_option', 'op'))
                        ->where('op.component', '=', $s_component)
                        ->and_where('op.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('op.order')
                        ->execute()
                        //->as_array('key');
                        ->as_array();
    }

    public static function fnGetAll() {
        $a_select = DB::select(
                        array('op.key', 'option_key')
                )
                ->from(array('bts_option', 'op'))
                ->where('op.status', '=', self::BTS_STATUS_ACTIVE)
                ->order_by('op.key')
                ->execute()
                ->as_array();

        return array_map(function($a_item) {
                            return new Model_Bts_Option($a_item['option_key']);
                        }, $a_select);
    }

    public function saveInstance($s_instance, $o_option) {

        $a_option_db = DB::select(
                        array(DB::expr('COUNT(*)'), 'total')
                )
                ->from('bts_option')
                ->where('key', '=', $o_option->key)
                ->execute($s_instance)
                ->get('total');

        if (!$a_option_db) {

            $a_option_insert = array(
                "key" => $o_option->key
                , "component" => $o_option->component
                , "dataType" => $o_option->dataType
                , "value" => $o_option->value
                , "rootOption" => $o_option->rootOption
                , "prefix" => $o_option->prefix == "" ? NULL : $o_option->prefix
                , "suffix" => $o_option->suffix == "" ? NULL : $o_option->suffix
                , "description" => $o_option->description
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );

            DB::insert('bts_option')
                    ->values(array_values($a_option_insert))
                    ->columns(array_keys($a_option_insert))
                    ->execute($s_instance);
        }
    }

    public function updateOption($val) {
        $this->value = $val;
        parent::save();
    }

}