<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsTagTask
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsTagTask extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'pt_tag_task';
    protected $_primary_key = 'idTagTask';
    
    protected $_db_group = self::BTS_HELP_TASK_INSTANCE_CONNECTION;
       
    public function saveArray($a_params){
        $this->idTag = $a_params['idTag'];
        $this->idTask = $a_params['idTask'];
        $this->status =self::BTS_STATUS_ACTIVE;
        parent::save();
    }
}