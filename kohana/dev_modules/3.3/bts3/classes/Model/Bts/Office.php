<?php

class Model_Bts_Office extends Kohana_BtsOrm implements Kohana_BtsConstants{
    protected $_table_names_plural = false;
    protected $_table_name = 'bts_office';
    protected $_primary_key ='idOffice';
    protected $_belongs_to = array(
        'city' => array(
            'model' => 'Bts_City',
            'foreign_key' => 'idCity'
        )
    );
    public function saveArray($a_params) {

        $this->idCity = $a_params['idCity'];
        $this->name = trim($a_params['name']);
        $this->address = $a_params['address'];
        $this->phone = $a_params['phone'];
        $this->rootOption = $a_params['rootOption'];
        parent::save();
    }
    
    public static function fnGetAll() {
        $a_select =  DB::select(
                                array('of.idOffice', 'office_id')
                        )
                        ->from(array('bts_office', 'of'))
                        ->where('of.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('of.name')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_Office($a_item['office_id']);
        }, $a_select);
    }
}