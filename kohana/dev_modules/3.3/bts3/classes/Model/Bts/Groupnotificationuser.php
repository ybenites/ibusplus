<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_Groupnotificationuser extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_group_notification_user';
    protected $_primary_key = 'idGroupNotificationUser';

    public function saveArray($a_params) {
        
        $this->idGroupNotification = $a_params['idGroupNotification'];
        $this->idUser = $a_params['idUser'];
        
        parent::save();
    }
    
    
    public static function getAllByUser($i_user_id){
        $a_result =  DB::select(
                    array('gnu.idGroupNotification', 'group_notification_id')
                )
                ->distinct(true)
                ->from(array('bts_group_notification_user', 'gnu'))
                ->where('gnu.idUser', '=', $i_user_id)
                ->and_where('gnu.status', '=', self::BTS_STATUS_ACTIVE)
                ->execute()
                ->as_array();
        
        if($a_result)
            return array_map(function($a_item){
                return $a_item['group_notification_id'];
            }, $a_result);
        
        return array();
    }
    
}