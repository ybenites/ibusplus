<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsCustomerCategory
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsCustomerCategory extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'pt_customercategory';
    protected $_primary_key = 'idCustomercategory';
    protected $_db_group = self::BTS_HELP_TASK_INSTANCE_CONNECTION;

    public function fnGetCategories($s_keyCustomer) {
        $a_result = DB::select(
                        array('ck.idCustomerCategory', 'customercategory_id')
                        , array('ka.name', 'category_name')
                )
                ->from(array('pt_category', 'ka'))
                ->join(array('pt_customercategory', 'ck'))
                ->on('ka.idCategory', '=', 'ck.idCategory')
                ->join(array('pt_customer', 'cu'))
                ->on('ck.idCustomer', '=', 'cu.idCustomer')
                ->join(array('pt_customerproject', 'cp'))
                ->on('cu.idCustomer', '=', 'cp.idCustomer')
                ->where('ka.status', '=', 1)
                ->and_where('cu.status', '=', 1)
                ->and_where('cp.keyCustomer', '=', $s_keyCustomer)
                ->execute($this->_db_group)
                ->as_array();
        return $a_result;
    }

}
