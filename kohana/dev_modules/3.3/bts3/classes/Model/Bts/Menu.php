<?php

class Model_Bts_Menu extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_menu';
    protected $_primary_key = 'idMenu';

    public function saveArray($a_params) {

        $this->name = trim($a_params['name']);
        $this->url = $a_params['url'];
        $this->type = $a_params['type'];
        $this->icon = $a_params['icon'];
        $this->rootOption = $a_params['rootOption'];
        $this->withshortcut = $a_params['withshortcut'];
        $this->optionKey = $a_params['optionKey'];
        $this->optionKeyValue = $a_params['optionKeyValue'];
        $this->idModule = $a_params['idModule'];
        $this->idSuperMenu = $a_params['idSuperMenu'];
        $this->iconShortcut = $a_params['iconShortcut'];

        parent::save();
    }

    public static function fnGetAllSameLevel($i_id) {

        $o_model_menu = new Model_Bts_Menu($i_id);

        if (!$o_model_menu->loaded())
            throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

        return DB::select(
                                array('me.idMenu', 'menu_id')
                                , array(
                            DB::expr(
                                    "CASE 
                                        WHEN me.type = '" . self::BTS_MENU_TYPE_ITEM . "'
                                            THEN me.name
                                        WHEN me.type = '" . self::BTS_MENU_TYPE_HEADER . "'
                                            THEN me.name
                                        WHEN me.type = '" . self::BTS_MENU_TYPE_SEPARATOR . "'
                                            THEN '========='
                                        ELSE
                                        me.name
                                    END"
                            )
                            , 'menu_name'
                                )
                        )
                        ->from(array('bts_menu', 'me'))
                        ->where('me.idSuperMenu', '=', $o_model_menu->idSuperMenu)
                        ->and_where('me.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('me.order')
                        ->execute()
                        ->as_array();
    }

    public static function fnGetMenuBySuper($i_super_menu = null, $b_all = false) {
        $o_cache_instance = Cache::instance('apc');
        $s_request_url = 'fnGetMenuBySuper' . $i_super_menu . ($b_all ? 'true' : 'false');
        $s_cache_key = SITE_DOMAIN . $s_request_url;
        $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        if ($a_in_cache_object === null) {
            $o_select = DB::select(
                            array('me.idMenu', 'menu_id')
                            , array('me.idSuperMenu', 'menu_super_id')
                            , array('me.name', 'menu_name')
                            , array('me.type', 'menu_type')
                            , array('me.icon', 'menu_icon')
                            , array('me.url', 'menu_url')
                            , array('me.optionKey', 'menu_option_key')
                            , array('me.optionKeyValue', 'menu_option_value')
                            , array(DB::expr('IF(mo.idModule IS NOT NULL, IF(mo.status = 1,1,0),1)'), 'menu_module_ok')
                    )
                    ->from(array('bts_menu', 'me'))
                    ->join(array('bts_module', 'mo'), 'LEFT')
                    ->on('me.idModule', '=', 'mo.idModule');

            if (!$b_all)
                $o_select->and_where('me.idSuperMenu', '=', $i_super_menu);

            $a_menu = $o_select->and_where('me.status', '=', self::BTS_STATUS_ACTIVE)
                    ->order_by('me.order')
                    ->execute()
                    ->as_array('menu_id');

            $o_cache_instance->set($s_cache_key, $a_menu);
            $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        }
        return $a_in_cache_object;
    }

    public static function fnGetMenuById($i_menu_id = null) {
        $o_cache_instance = Cache::instance('apc');
        $s_request_url = 'fnGetMenuById' . $i_menu_id;
        $s_cache_key = SITE_DOMAIN . $s_request_url;
        $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        if ($a_in_cache_object === null) {
            $a_menu = DB::select(
                            array('me.idMenu', 'menu_id')
                            , array('me.idSuperMenu', 'menu_super_id')
                            , array('me.name', 'menu_name')
                            , array('me.type', 'menu_type')
                            , array('me.icon', 'menu_icon')
                            , array('me.url', 'menu_url')
                            , array('me.optionKey', 'menu_option_key')
                            , array('me.optionKeyValue', 'menu_option_value')
                            , array(DB::expr('IF(mo.idModule IS NOT NULL, IF(mo.status = 1,1,0),1)'), 'menu_module_ok')
                    )
                    ->from(array('bts_menu', 'me'))
                    ->join(array('bts_module', 'mo'), 'LEFT')
                    ->on('me.idModule', '=', 'mo.idModule')
                    ->and_where('me.idMenu', '=', $i_menu_id)
                    ->and_where('me.status', '=', self::BTS_STATUS_ACTIVE)
                    ->order_by('me.order')
                    ->execute()
                    ->current();

            $o_cache_instance->set($s_cache_key, $a_menu);
            $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        }
        return $a_in_cache_object;
    }

    public function getAllMenuIDs() {

        $menu_second_level = DB::select('bts_menu.idMenu')
                ->from('bts_menu')
                ->where('bts_menu.idSuperMenu', 'IS NOT', NULL);

        $menu_first_level = DB::select('bts_menu.idMenu')
                ->from('bts_menu')
                ->union($menu_second_level,TRUE)
                ->where('bts_menu.idSuperMenu', 'IS', NULL);
        return $menu_first_level->execute()->as_array();
    }

    public function saveInstance($s_instance, $o_module) {

        
        $a_translate_db = DB::select(
                        array(DB::expr('COUNT(*)'), 'total')
                )
                ->from('bts_menu')
                ->where(DB::expr('LOWER(name)'), 'LIKE', strtolower($o_module->name))
                ->and_where('url', 'LIKE', $o_module->url)
                ->and_where('type', 'LIKE', $o_module->type)
                ->and_where('status', '=', Kohana_BtsConstants::BTS_STATUS_ACTIVE)
                ->execute($s_instance)
                ->get('total');

        if (!$a_translate_db) {

            $a_controller_insert = array(
                "name" => $o_module->name
                , "url" => $o_module->url
                , "type" => $o_module->type
                , "icon" => $o_module->icon
                , "rootOption" => $o_module->rootOption
                , "order" => $o_module->order
                , "optionKey" => $o_module->optionKey
                , "optionKeyValue" => $o_module->optionKeyValue
                , "idModule" => $o_module->idModule
                , "idSuperMenu" => $o_module->idSuperMenu
                , "withshortcut" => $o_module->withshortcut
                , "iconShortcut" => $o_module->iconShortcut
                , "status" => $o_module->status
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );
            DB::query(Database::UPDATE, "SET FOREIGN_KEY_CHECKS=0")->execute($s_instance);
            DB::insert('bts_menu')
                    ->values(array_values($a_controller_insert))
                    ->columns(array_keys($a_controller_insert))
                    ->execute($s_instance);
            DB::query(Database::UPDATE, "SET FOREIGN_KEY_CHECKS=1")->execute($s_instance);
        }
    }

}