<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_SessionLog extends Kohana_BtsOrm {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_sessionlog';
    protected $_primary_key = 'idSessionLog';

}