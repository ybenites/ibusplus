<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsFileTask
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsFileTask extends Kohana_BtsOrm implements Kohana_BtsConstants{
    protected $_table_names_plural = false;
    protected $_table_name = 'pt_filetask';
    protected $_primary_key = 'idFileTask';

    protected $_db_group = self::BTS_HELP_TASK_INSTANCE_CONNECTION;
    
    public function saveArray($a_params){
        $this->idTask = $a_params['idTask'];
        $this->pathFileTask = $a_params['pathFileTask'];
        $this->nameOriginFile = $a_params['nameOriginFile'];
        $this->status =self::BTS_STATUS_ACTIVE;
        parent::save();        
    }
}