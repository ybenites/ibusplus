<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_Privilege extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_privilege';
    protected $_primary_key = 'idPrivilege';
    
    public static function fnGetAccess($a_group_id){
        return DB::select(
                array(DB::expr("LOWER(ctr.directory)"), "access_directory")
                , array(DB::expr("LOWER(ctr.controller)"), "access_controller")
                , array("ctr.action", "access_action")
        )
        ->from(array('bts_privilege', 'pr'))
        ->join(array('bts_controller', 'ctr'))
        ->on(DB::expr("pr.directory = ctr.directory AND pr.controller = ctr.controller "),"AND",DB::expr(" pr.type = ctr.type"))
        ->where('pr.idGroup', 'IN', DB::expr("(".  implode(',', $a_group_id).")"))        
        ->and_where('ctr.status', '=', self::BTS_STATUS_ACTIVE)        
        ->and_where('pr.access', '=', self::BTS_STATUS_ACTIVE)        
        ->execute()
        ->as_array();
    }
    
}