<?php

class Model_Bts_Section extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_section';
    protected $_primary_key = 'idSection';

    public function saveArray($a_params) {
        $this->idPage = $a_params['idPage'];
        $this->idFile = $a_params['idFile'];
        $this->fileOrder = $a_params['fileOrder'];
        parent::save();
    }
    
    public function clearFile($s_page){
        $o_model_section = new Model_Bts_Section;
        $a_model_sections = $o_model_section                    
                    ->where('idPage', '=', $s_page)
                    ->and_where('status', '=', self::BTS_STATUS_ACTIVE)
                    ->find_all();
        foreach($a_model_sections as $o_section){
            $o_section->status = self::BTS_STATUS_DEACTIVE;
            $o_section->save();
        }
    }

    public function saveFiles($a_files, $s_page){        
        foreach ($a_files as $i_key => $s_file){
            $o_model_section = new Model_Bts_Section;
            $o_model_section->where('idFile', '=', $s_file)
                    ->and_where('idPage', '=', $s_page)
                    ->find();
            
            if(!$o_model_section->loaded()){
                $o_model_section->idFile = $s_file;
                $o_model_section->idPage = $s_page;
            }else{
            }
            
            $o_model_section->fileOrder = $i_key;
            $o_model_section->status = self::BTS_STATUS_ACTIVE;
            
            $o_model_section->save();
        }
    }
    
    public function saveInstance($s_instance, $s_page, $a_file) {
        
        foreach($a_file as $i_key => $s_file){
            $a_section_db = DB::select(
                            array(DB::expr('COUNT(*)'), 'total')
                    )
                    ->from('bts_section')
                    ->where('idPage', '=', $s_page)
                    ->and_where('idFile', '=', $s_file)                    
                    ->execute($s_instance)
                    ->get('total');

            if (!$a_section_db) {
                $a_section_insert = array(
                    "idPage" => $s_page
                    , "idFile" => $s_file 
                    , "fileOrder" => ($i_key+1)
                    //, Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
                );
                DB::insert('bts_section')
                        ->values(array_values($a_section_insert))
                        ->columns(array_keys($a_section_insert))
                        ->execute($s_instance);
            }            
        }
    }
}