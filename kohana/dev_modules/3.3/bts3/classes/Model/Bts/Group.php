<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_Group extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_group';
    protected $_primary_key = 'idGroup';

    public function saveArray($a_params) {

        $this->name = trim($a_params['name']);
        $this->defaultHomePage = $a_params['defaultHomePage'];
        $this->rootOption = $a_params['rootOption'];     
        $this->allowChat = $a_params['allowChat'];     
        $this->allowNotifications = $a_params['allowNotifications'];     
        $this->allowNotificationsShow = $a_params['allowNotificationsShow'];
        parent::save();
    }
    
    public static function fnGetAll() {
        $a_select =  DB::select(
                                array('gr.idGroup', 'group_id')
                        )
                        ->from(array('bts_group', 'gr'))
                        ->where('gr.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('gr.name')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_Group($a_item['group_id']);
        }, $a_select);
    }
}
