<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsUser
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsUser extends Kohana_BtsOrm implements Kohana_BtsConstants{
    protected $_table_names_plural = false;
    protected $_table_name = 'bts_user';
    protected $_primary_key = 'idUser';
    
    protected $_db_group =self::BTS_HELP_TASK_INSTANCE_CONNECTION;
       
    protected $_has_one = array('person' => array('model'=> 'Bts_PtmsPerson','foreign_key' => 'idPerson'));
}
