<?php
class Model_Bts_Translate extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_translate';
    protected $_primary_key = 'idTranslate';
    
    
    public function saveTranslate($a_form_post_data){
      $this->text = $a_form_post_data['text'];
      $this->textTranslate = $a_form_post_data['textTranslate'];
      $this->text_hash = sha1($a_form_post_data['text']); 
      parent::save();
  }
  
  public function getAllTranslateIDs(){
      return DB::select('bts_translate.idTranslate')->from('bts_translate')->execute()->as_array();
  }
  
  public function saveInstance($s_instance, $o_module){
        
        $a_translate_db = DB::select(
                            array(DB::expr('COUNT(*)'), 'total')
                        )
                        ->from('bts_translate')
                        ->where('text', 'LIKE', $o_module->text)
                        ->and_where('textTranslate', 'LIKE', $o_module->textTranslate)
                        ->and_where('language_base', 'LIKE', $o_module->language_base)
                        ->and_where('language_target', 'LIKE', $o_module->language_target)
                        ->and_where('status', '=', Kohana_BtsConstants::BTS_STATUS_ACTIVE)
                        ->execute($s_instance)
                        ->get('total');
        
        if(!$a_translate_db){
            
            $a_controller_insert = array(
                "text" => $o_module->text
                , "textTranslate" => $o_module->textTranslate
                , "language_base" => $o_module->language_base
                , "language_target" => $o_module->language_target
                , "status" => $o_module->status
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );
            
            DB::insert('bts_translate')
                    ->values(array_values($a_controller_insert))
                    ->columns(array_keys($a_controller_insert))
                    ->execute($s_instance);           
            
        }
        
    }
    
    
}  