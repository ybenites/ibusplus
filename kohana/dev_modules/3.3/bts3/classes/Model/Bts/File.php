<?php

class Model_Bts_File extends Kohana_BtsOrm implements Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_file';
    protected $_primary_key = 'idFile';

    public static function fnValidKey($s_key, $s_key_file = null) {
        //Verificar si la KEY existe
        $o_select = DB::select(array(DB::expr('COUNT(*)'), 'total'))
                ->from('bts_file')
                ->where('idFile', '=', $s_key);

        if ($s_key_file) {
            //Al momento de editar
            $o_select->and_where('idFile', '<>', $s_key_file);
        }
        return !$o_select->execute()
                        ->get('total');
    }

    public function saveArray($a_params) {

        $this->idFile = $a_params['idFile'];
        $this->fileName = $a_params['fileName'];
        $this->fileType = $a_params['fileType'];
        $this->isCoreFile = $a_params['isCoreFile'];

        parent::save();
    }

    public static function fnGetByType($s_type) {
        $a_select = DB::select(
                        array('fi.idFile', 'file_id')
                )
                ->from(array('bts_file', 'fi'))
                ->where('fi.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('fi.fileType', '=', $s_type)
                ->order_by('fi.idFile')
                ->execute()
                ->as_array();

        return array_map(function($a_item) {
                    return new Model_Bts_File($a_item['file_id']);
                }, $a_select);
    }

    public function saveInstance($s_instance, $o_file) {

        $a_file_db = DB::select(
                        array(DB::expr('COUNT(*)'), 'total')
                )
                ->from('bts_file')
                ->where('idFile', '=', $o_file->idFile)
                ->execute($s_instance)
                ->get('total');

        if (!$a_file_db) {

            $a_file_insert = array(
                "idFile" => $o_file->idFile
                , "fileType" => $o_file->fileType
                , "fileName" => $o_file->fileName
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );

            DB::insert('bts_file')
                    ->values(array_values($a_file_insert))
                    ->columns(array_keys($a_file_insert))
                    ->execute($s_instance);
        }
    }

}