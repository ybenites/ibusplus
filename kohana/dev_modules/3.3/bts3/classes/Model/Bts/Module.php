<?php
class Model_Bts_Module extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_module';
    protected $_primary_key = 'idModule';
    
    
    public static function fnValidCodeName($s_code, $s_name, $s_key_option = null) {
        //Verificar si el nombre del codigo existe      
        $o_select = DB::select(array(DB::expr('COUNT(*)'), 'total'))
                ->from('bts_module');
        if(isset($s_code))
                $o_select = $o_select->where('idModule', '=', $s_code);
        elseif(isset($s_name)){
                $o_select = $o_select->where('name', '=', $s_name);
        }    

        if ($s_key_option) {
            //Al momento de editar
            $o_select->and_where('idModule', '<>', $s_key_option);
        }
        return !$o_select->execute()
                        ->get('total');
    }
    public function saveArray($a_params) {

        $this->idModule = $a_params['idModule'];
        $this->name = trim($a_params['name']);
        $this->status = $a_params['status'];
        parent::save();
    }
    
    public static function fnGetAll(){
        $a_select =  DB::select(
                                array('mo.idModule', 'module_id')
                        )
                        ->from(array('bts_module', 'mo'))                        
                        ->where('mo.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('mo.name')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_Module($a_item['module_id']);
        }, $a_select);
    }
    
    public function saveInstance($s_instance, $o_module){
        
        $a_module_db = DB::select(
                            array(DB::expr('COUNT(*)'), 'total')
                        )
                        ->from('bts_module')
                        ->where('idModule', '=', $o_module->idModule)
                        ->execute($s_instance)
                        ->get('total');
        
        if(!$a_module_db){
            
            $a_controller_insert = array(
                "idModule" => $o_module->idModule
                , "name" => $o_module->name
                , "status" => $o_module->status
                , "order" => $o_module->order
                , Kohana_BtsConstants::BTS_TABLE_CREATION_DATE => date('Y-m-d H:i:s')
            );
            
            DB::insert('bts_module')
                    ->values(array_values($a_controller_insert))
                    ->columns(array_keys($a_controller_insert))
                    ->execute($s_instance);           
            
        }
        
    }
    
}
