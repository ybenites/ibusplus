<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_User extends Kohana_BtsOrm implements Kohana_BtsConstants{

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_user';
    protected $_primary_key = 'idUser';
    protected $_belongs_to = array(
        'group' => array(
            'model' => 'Bts_Group',
            'foreign_key' => 'idGroup'
        ),
        'office' => array(
            'model' => 'Bts_Office',
            'foreign_key' => 'idOffice'
        )
    );
    protected $_has_one = array(
        'person' => array(
            'model' => 'Bts_Person',
            'foreign_key' => 'idPerson'
        )
    );

    public static function fnValidUniqueUsername($s_username, $i_wc_id = null) {
        // Check if the username already exists in the database
        $o_select = DB::select(array(DB::expr('COUNT(userName)'), 'total'))
                        ->from('bts_user')
                        ->where('userName', '=', $s_username);
        if($i_wc_id){
            $o_select->and_where('idUser', '<>', $i_wc_id);
        }
        return !$o_select->execute()
                        ->get('total');
    }
    
    
    public static function fnValidUniqueEmail($s_email, $i_wc_id = null) {
        // Check if the username already exists in the database
                
        $o_select = DB::select(
                                array(DB::expr('COUNT(us.idUser)'), 'total')
                        )
                        ->from(array('bts_user', 'us'))
                        ->join(array('bts_person', 'pe'))
                        ->on('us.idUser', '=', 'pe.idPerson')
                        ->where('pe.email', '=', $s_email);                     
        if($i_wc_id){
            $o_select->and_where('us.idUser', '<>', $i_wc_id);
        }        
        return !$o_select->execute()
                        ->get('total');
        
    }
    
    public static function fnGetAll() {
        $a_select =  DB::select(
                                array('us.idUser', 'user_id')
                                ,array('us.idGroup', 'group_id')
                        )
                        ->from(array('bts_user', 'us'))
                        ->where('us.status', '=', self::BTS_STATUS_ACTIVE)
                        ->order_by('us.idGroup')
                        ->execute()
                        ->as_array();
        
        return array_map(function($a_item){
            return new Model_Bts_User($a_item['user_id']);
        }, $a_select);
    }
    
    public function saveArray($a_params) {
        
        
        if(!empty($a_params['idUser']))
        $this->idUser = $a_params['idUser'];
        
        if(!empty($a_params['idGroup']))
        $this->idGroup = $a_params['idGroup'];
        
        if(!empty($a_params['idOffice']))
        $this->idOffice = $a_params['idOffice'];
        
        if(!empty($a_params['userName']))
        $this->userName = $a_params['userName'];
        
        if(!empty($a_params['ownSession']))
        $this->rootOption = $a_params['ownSession'];
        
        if(!empty($a_params['password']))
            $this->password = $a_params['password'];
                
        if(!empty($a_params['imgProfile']))
            $this->imgProfile = $a_params['imgProfile'];
                
        parent::save();
    }
    
    public function updateOffice($o_check_post,$idUser,$idGroup,$fullname){
       $idOffice = $o_check_post['selectOffice'];
       
         $o_model_user = new Model_Bts_User($idUser);
            if (!$o_model_user->loaded())
                throw new Exception(__("El registro no existe."), self::BTS_CODE_SUCCESS);
            
            $o_model_user->idUser = $idUser;
            $o_model_user->idGroup = $idGroup;
            $o_model_user->idOffice =$idOffice;
            $o_model_user->userName =trim($fullname);
            $o_model_user->status =self::BTS_STATUS_ACTIVE;
            $o_model_user->save();
            
              $a_task = array(
            'idUser'=>$o_model_user->idUser,
            'idGroup'=> $o_model_user->idGroup,
            'idOffice'=>$o_model_user->idOffice 
            );
        return $a_task;
        }
        
        public function changePass($pass,$idUser,$idGroup,$idOffice){
            
            $o_model_user = new Model_Bts_User($idUser);
            if (!$o_model_user->loaded())
                throw new Exception(__("El registro no existe."), self::BTS_CODE_SUCCESS);
            
            $o_model_user->idUser = $idUser;
            $o_model_user->idGroup = $idGroup;
            $o_model_user->idOffice =$idOffice;
            $o_model_user->password =md5($pass);
            $o_model_user->status =self::BTS_STATUS_ACTIVE;
            $o_model_user->save();
            
               
        }
        
        
    

}