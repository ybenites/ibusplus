<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PtmsTask
 *
 * @author BENITES
 */

defined('SYSPATH') or die('No direct script access.');

class Model_Bts_PtmsTask extends Kohana_BtsOrm implements Kohana_BtsConstants{
    protected $_table_names_plural = false;
    protected $_table_name = 'pt_task';
    protected $_primary_key = 'idTask';
    
    protected $_db_group = self::BTS_HELP_TASK_INSTANCE_CONNECTION;
    
    public function saveArray($a_params) {
        $this->idCustomerCategory = $a_params['idCustomerCategory'];
        $this->title = $a_params['title'];
        $this->description = $a_params['description'];
        $this->idUserRequest = $a_params['idUserRequest'];
        $this->emailFrom = $a_params['emailFrom'];
        $this->nameUserRequest = $a_params['nameUserRequest'];
        $this->codeTask = $a_params['codeTask'];
        $this->createType = $a_params['createType'];
        $this->updateType = $a_params['updateType'];
        $this->status = $a_params['status'];
        parent::save();
    }
    
    public function fnGetTagsArray(){
         $a_result = DB::select(
                        array('tg.idTag', 'tag_id')
                        ,array('tg.name', 'tag_name')
                )
                ->from(array('pt_task', 'ta'))
                ->join(array('pt_tag_task', 'tt'))
                ->on('ta.idTask', '=', 'tt.idTask')                
                ->join(array('pt_tag', 'tg'))
                ->on('tt.idTag', '=', 'tg.idTag')                
                ->where('ta.status', '=', 1)
                ->and_where('tt.status', '=', 1)
                ->and_where('tg.status', '=', 1)
                ->and_where('ta.idTask', '=', $this->idTask)                
                ->execute($this->_db_group)
                ->as_array();
        
        return $a_result;
    }
    
    public function fnGetFilesArray(){
        $a_result=DB::select(
                array('ft.idFileTask','id_file')
                ,array('ft.pathFileTask','path_file')
                ,array('ft.nameOriginFile','name_file')
                )->from(array('pt_task','ta'))
                ->join(array('pt_filetask','ft'))->on('ta.idTask','=','ft.idTask')
                ->where('ta.status', '=',1)
                ->and_where('ft.status', '=',1)
                ->and_where('ta.idTask', '=',$this->idTask)
                ->execute($this->_db_group)->as_array();
        return $a_result;
    }
}
