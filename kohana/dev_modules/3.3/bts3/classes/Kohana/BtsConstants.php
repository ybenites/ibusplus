<?php

interface Kohana_BtsConstants {
    /*
     * ORM ATTR
     */

    CONST BTS_TABLE_USER_CREATION = 'userCreate';
    CONST BTS_TABLE_CREATION_DATE = 'creationDate';
    CONST BTS_TABLE_USER_LAST_CHANGE = 'userLastChange';
    CONST BTS_TABLE_LAST_CHANGE_DATE = 'lastChangeDate';

    /*
     * SESSION
     */
    CONST BTS_SESSION_ID = 'SESSION_ID';
    CONST BTS_SESSION_ACTIONS_ACCESS = 'ACTIONS_ACCESS';
    CONST BTS_SESSION_USER_ID = 'USER_ID';
    CONST BTS_SESSION_USER_TYPE = 'USER_TYPE';
    CONST BTS_SESSION_GROUP_NAME = 'GROUP_NAME';
    CONST BTS_SESSION_GROUP = 'GROUP';
    CONST BTS_SESSION_USER_NAME = 'USER_NAME';
    CONST BTS_SESSION_USER_FULL_NAME = 'USER_FULL_NAME';
    CONST BTS_SESSION_USER_EMAIL = 'USER_EMAIL';
    CONST BTS_SESSION_OFFICE_ID = 'OFFICE_ID';
    CONST BTS_SESSION_CITY_ID = 'CITY_ID';
    CONST BTS_SESSION_OFFICE_NAME = 'OFFICE_NAME';
    CONST BTS_SESSION_CITY_NAME = 'CITY_NAME';
    CONST BTS_SESSION_GROUP_ID = 'GROUP_ID';
    CONST BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID = 'GROUP_NOTIFICATION_ARRAY_ID';
    CONST BTS_SESSION_CUSTOM_PRIVILEGE = 'CUSTOM_PRIVILEGE';
    CONST BTS_SESSION_ROOT_OPTION = 'ROOT_OPTION';
    CONST BTS_SESSION_USER_MIMIC_OPTION = 'USER_MIMIC_OPTION';
    CONST BTS_SESSION_ROOT_OPTION_MIMIC = 'ROOT_OPTION_MIMIC';
    CONST BTS_SESSION_ROOT_OPTION_MIMIC_REAL_ID = 'ROOT_OPTION_MIMIC_REAL_ID';
    CONST BTS_SESSION_DEFAULT_HOME = 'DEFAULT_HOME';
    CONST BTS_SESSION_SYSTEM_OPTIONS = 'SYSTEM_OPTIONS';

     /*
     * COOKIE USER DATA
     */
    
    CONST BTS_COOKIE_USER_PREFERENCES = 'upreferences';
    
    /*
     * MESSAGES
     */
    const BTS_MSG_COUNT_DISPLAY_TOTAL =20;
    const BTS_MSG_COUNT_DISPLAY =5;
    const BTS_ZERO = 0;
    const BTS_MAX_ROW_LIMIT = '18446744073709551615';

    /*
     * OPTION
     */
    CONST BTS_OPTION_DEFAULT_COUNTRY = 'BTS_DEFAULT_COUNTRY';
    CONST BTS_LANGUAGE = 'BTS_LANGUAGE';
    const BTS_SO_DATE_FORMAT='DATE_FORMAT';
    const BTS_SO_BTS_CHAT='BTS_CHAT';

    /*
     * MYSQL ERROR CODE
     */
    CONST MYSQL_CODE_ER_DUP_ENTRY = 1062;
    CONST MYSQL_CODE_ER_ROW_IS_REFERENCED_2 = 1451;
    CONST MYSQL_CODE_NONE = 99999;
    CONST MYSQL_CODE_ER_DATA_TOO_LONG = 1406;
    CONST MYSQL_CODE_ER_FOREIGN_KEY_CONSTRAINT = 1452;

    /*
     * CODE SYSTEM ACCESS
     */
    CONST BTS_CODE_SUCCESS = 441;
    CONST BTS_CODE_NO_AUTHORIZED = 442;
    CONST BTS_CODE_ERROR = 443;

    /*
     * FILES TYPE
     */
    CONST BTS_FILE_TYPE_CSS = 'css';
    CONST BTS_FILE_TYPE_JS = 'js';

    /*
     * JS GLOBALES
     */
    CONST BTS_BOOTSTRAP_VERSION = 'BOOTSTRAP_VERSION';
    CONST BTS_JQUERY_VERSION = 'JQUERY_VERSION';
    CONST BTS_JQUERY_UI_VERSION = 'JQUERY_UI_VERSION';

    /*
     * RESPONSE ACTIONS
     */
    CONST BTS_RESPONSE_TYPE_HTML = 'HTML';
    CONST BTS_RESPONSE_TYPE_JSON = 'JSON';
    CONST BTS_RESPONSE_TYPE_JQGRID = 'JQGRID';
    CONST BTS_RESPONSE_TYPE_JSONP = 'JSONP';
    CONST BTS_RESPONSE_TYPE_XML = 'XML';

    /*
     * ATTR'S JSON RESPONSE
     */
    CONST BTS_RESPONSE_TYPE_JSON_CODE = 'code';
    CONST BTS_RESPONSE_TYPE_JSON_MSG = 'msg';

    /*
     * KOHANA SESSION VARS
     */
    CONST KOHANA_SESSION_NAME_DATABASE = 'database';
    CONST KOHANA_SESSION_NAME_NATIVE = 'native';

    /*
     * EVENTs
     */
    const BTS_EVENT_LOG_IN = "LOGIN";
    const BTS_EVENT_LOG_OUT = "LOGOUT";

    /*
     * GENERAL STATUS 
     */
    const BTS_STATUS_ACTIVE = 1;
    const BTS_STATUS_DEACTIVE = 0;

    /*
     * GENERAL CONFIG
     */
    const BTS_SYS_CONFIG_INSTANCE = 'instance';
    const BTS_SYS_CONFIG_SITE_NAME = 'site_name';
    const BTS_SYS_CONFIG_SERVER = 'server';
    const BTS_SYS_CONFIG_PORT = 'port';
    const BTS_SYS_CONFIG_DB_NAME = 'db_name';
    const BTS_SYS_CONFIG_DB_USER = 'db_user';
    const BTS_SYS_CONFIG_DB_PASSWORD = 'db_password';
    const BTS_SYS_CONFIG_SKIN = 'skin';
    const BTS_SYS_CONFIG_WEBSITE = 'website';
    const BTS_SYS_CONFIG_STATIC_WEBSITE = 'static_website';
    const BTS_SYS_CONFIG_TIME_ZONE = 'time_zone';
    const BTS_SYS_CONFIG_DEFAULT_INDEX = 'default_index';
    const BTS_SYS_CONFIG_EMAIL = 'email';
    const BTS_SYS_CONFIG_CONTACT_EMAIL = 'contact_email';
    const BTS_SYS_CONFIG_CONTACT_EMAIL_FROM = 'contact_email_from';
    const BTS_SYS_CONFIG_CONTACT_EMAIL_FROM_NAME = 'contact_email_from_name';
    const BTS_SYS_CONFIG_CONTACT_COMPANY = 'contact_company';
    const BTS_SYS_CONFIG_CONTACT_PHONE = 'contact_phone';
    const BTS_SYS_CONFIG_CONTACT_ADDRESS = 'contact_address';
    const BTS_SYS_CONFIG_SESSION_USER_LIFE_TIME = 'session_user_life_time';
    const BTS_SYS_CONFIG_SESSION_NATIVE_LIFE_TIME = 'session_native_life_time';
    const BTS_SYS_CONFIG_SESSION_DB_LIFE_TIME = 'session_db_life_time';
    const BTS_SYS_CONFIG_PAYPAL_USER = 'paypal_user';
    const BTS_SYS_CONFIG_PAYPAL_PASSWORD = 'paypal_password';
    const BTS_SYS_CONFIG_PAYPAL_SIGNATURE = 'paypal_signature';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_EMAIL = 'paypal_merchant_email';
    const BTS_SYS_CONFIG_PAYPAL_ENVIRONMENT = 'paypal_environment';
    const BTS_SYS_CONFIG_PAYPAL_GATEWAY = 'paypal_gateway';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_PARTNER = 'paypal_merchant_partner';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_VENDOR = 'paypal_merchant_vendor';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_USER = 'paypal_merchant_user';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_PASSWORD = 'paypal_merchant_password';
    const BTS_SYS_CONFIG_PAYPAL_METHOD = 'paypal_method';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_USER = 'paypal_merchant_web_user';
    const BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_PASSWORD = 'paypal_merchant_web_password';
    const BTS_SYS_CONFIG_ENVIRONMENT = 'environment';
    const BTS_SYS_CONFIG_LANGUAGE = 'language';
    const BTS_SUPORT_SYS_EMAILS = 'supportTeam';
    const BTS_COUNTRY_US = 'US';
    const BTS_COUNTRY_MX = 'MX';
    const BTS_COUNTRY_PE = 'PE';
    const BTS_COUNTRY_AR = 'AR';
    const BTS_FORMAT_DATE_VALUE = 'DATE_VALUE';
    const BTS_FORMAT_DATE_JQUERY = 'DATE_JQUERY';
    const BTS_FORMAT_DATE_SQL = 'DATE_SQL';
    const BTS_FORMAT_TIME_SQL = 'TIME_SQL';
    const BTS_FORMAT_DATE_PHP = 'DATE_PHP';
    const BTS_FORMAT_TIME_SQL_RAW = 'TIME_SQL_RAW';
    const BTS_FORMAT_TIME_SQL_LIMIT = 'TIME_SQL_LIMIT';
    const BTS_FORMAT_DATE_TIME_SQL = 'DATE_TIME_SQL';
    const BTS_FORMAT_TIME_JQUERY_ENTRY_24_HOURS = 'TIME_JQUERY_ENTRY_24_HOURS';
    const BTS_FORMAT_TIME_PHP_INPUT = 'TIME_PHP_INPUT';
    const BTS_FORMAT_TIME_SQL_OUTPUT = 'TIME_SQL_OUTPUT';

    /*
     * Option Type
     */
    const BTS_OPTION_TYPE_BOOLEAN = 'BOOLEAN';
    const BTS_OPTION_TYPE_STRING = 'STRING';
    const BTS_OPTION_TYPE_COLOR = 'COLOR';
    const BTS_OPTION_TYPE_INTEGER = 'INTEGER';
    const BTS_OPTION_TYPE_ARRAY = 'ARRAY';

    /*
     * Option Component
     */
    const BTS_OPTION_COMPONENT_GENERAL = 'GENERAL';

    /*
     * Modules
     */
    const BTS_MODULE_ADMINISTRATION = 'ADMINISTRATION';
    const BTS_MODULE_SUPPORT = 'SUPPORT';

    /*
     * Controller Type
     */
    const BTS_CONTROLLER_TYPE_READ = 'READ';
    const BTS_CONTROLLER_TYPE_CREATE = 'CREATE';
    const BTS_CONTROLLER_TYPE_UPDATE = 'UPDATE';
    const BTS_CONTROLLER_TYPE_DELETE = 'DELETE';


    /*
     * Menu Types
     */
    const BTS_MENU_TYPE_ITEM = 'ITEM';
    const BTS_MENU_TYPE_SEPARATOR = 'SEPARATOR';
    const BTS_MENU_TYPE_HEADER = 'HEADER';
    const BTS_GROUP_ROOT = 0;
    const BTS_GROUP_ADMIN = 1;
    const BTS_USER_SYSTEM_ROOT_ID = 0;
    /*
     * Icons
     */
    const BTS_ICON_GLASS = 'icon-glass';
    const BTS_ICON_MUSIC = 'icon-music';
    const BTS_ICON_SEARCH = 'icon-search';
    const BTS_ICON_ENVELOPE = 'icon-envelope';
    const BTS_ICON_HEART = 'icon-heart';
    const BTS_ICON_STAR = 'icon-star';
    const BTS_ICON_STAR_EMPTY = 'icon-star-empty';
    const BTS_ICON_USER = 'icon-user';
    const BTS_ICON_FILM = 'icon-film';
    const BTS_ICON_TH_LARGE = 'icon-th-large';
    const BTS_ICON_TH = 'icon-th';
    const BTS_ICON_TH_LIST = 'icon-th-list';
    const BTS_ICON_OK = 'icon-ok';
    const BTS_ICON_REMOVE = 'icon-remove';
    const BTS_ICON_ZOOM_IN = 'icon-zoom-in';
    const BTS_ICON_ZOOM_OUT = 'icon-zoom-out';
    const BTS_ICON_OFF = 'icon-off';
    const BTS_ICON_SIGNAL = 'icon-signal';
    const BTS_ICON_COG = 'icon-cog';
    const BTS_ICON_TRASH = 'icon-trash';
    const BTS_ICON_HOME = 'icon-home';
    const BTS_ICON_FILE = 'icon-file';
    const BTS_ICON_TIME = 'icon-time';
    const BTS_ICON_ROAD = 'icon-road';
    const BTS_ICON_DOWNLOAD_ALT = 'icon-download-alt';
    const BTS_ICON_DOWNLOAD = 'icon-download';
    const BTS_ICON_UPLOAD = 'icon-upload';
    const BTS_ICON_INBOX = 'icon-inbox';
    const BTS_ICON_PLAY_CIRCLE = 'icon-play-circle';
    const BTS_ICON_REPEAT = 'icon-repeat';
    const BTS_ICON_REFRESH = 'icon-refresh';
    const BTS_ICON_LIST_ALT = 'icon-list-alt';
    const BTS_ICON_LOCK = 'icon-lock';
    const BTS_ICON_FLAG = 'icon-flag';
    const BTS_ICON_HEADPHONES = 'icon-headphones';
    const BTS_ICON_VOLUME_OFF = 'icon-volume-off';
    const BTS_ICON_VOLUME_DOWN = 'icon-volume-down';
    const BTS_ICON_VOLUME_UP = 'icon-volume-up';
    const BTS_ICON_QRCODE = 'icon-qrcode';
    const BTS_ICON_BARCODE = 'icon-barcode';
    const BTS_ICON_TAG = 'icon-tag';
    const BTS_ICON_TAGS = 'icon-tags';
    const BTS_ICON_BOOK = 'icon-book';
    const BTS_ICON_BOOKMARK = 'icon-bookmark';
    const BTS_ICON_PRINT = 'icon-print';
    const BTS_ICON_CAMERA = 'icon-camera';
    const BTS_ICON_FONT = 'icon-font';
    const BTS_ICON_BOLD = 'icon-bold';
    const BTS_ICON_ITALIC = 'icon-italic';
    const BTS_ICON_TEXT_HEIGHT = 'icon-text-height';
    const BTS_ICON_TEXT_WIDTH = 'icon-text-width';
    const BTS_ICON_ALIGN_LEFT = 'icon-align-left';
    const BTS_ICON_ALIGN_CENTER = 'icon-align-center';
    const BTS_ICON_ALIGN_RIGHT = 'icon-align-right';
    const BTS_ICON_ALIGN_JUSTIFY = 'icon-align-justify';
    const BTS_ICON_LIST = 'icon-list';
    const BTS_ICON_INDENT_LEFT = 'icon-indent-left';
    const BTS_ICON_INDENT_RIGHT = 'icon-indent-right';
    const BTS_ICON_FACETIME_VIDEO = 'icon-facetime-video';
    const BTS_ICON_PICTURE = 'icon-picture';
    const BTS_ICON_PENCIL = 'icon-pencil';
    const BTS_ICON_MAP_MARKER = 'icon-map-marker';
    const BTS_ICON_ADJUST = 'icon-adjust';
    const BTS_ICON_TINT = 'icon-tint';
    const BTS_ICON_EDIT = 'icon-edit';
    const BTS_ICON_SHARE = 'icon-share';
    const BTS_ICON_CHECK = 'icon-check';
    const BTS_ICON_MOVE = 'icon-move';
    const BTS_ICON_STEP_BACKWARD = 'icon-step-backward';
    const BTS_ICON_FAST_BACKWARD = 'icon-fast-backward';
    const BTS_ICON_BACKWARD = 'icon-backward';
    const BTS_ICON_PLAY = 'icon-play';
    const BTS_ICON_PAUSE = 'icon-pause';
    const BTS_ICON_STOP = 'icon-stop';
    const BTS_ICON_FORWARD = 'icon-forward';
    const BTS_ICON_FAST_FORWARD = 'icon-fast-forward';
    const BTS_ICON_STEP_FORWARD = 'icon-step-forward';
    const BTS_ICON_EJECT = 'icon-eject';
    const BTS_ICON_CHEVRON_LEFT = 'icon-chevron-left';
    const BTS_ICON_CHEVRON_RIGHT = 'icon-chevron-right';
    const BTS_ICON_PLUS_SIGN = 'icon-plus-sign';
    const BTS_ICON_MINUS_SIGN = 'icon-minus-sign';
    const BTS_ICON_REMOVE_SIGN = 'icon-remove-sign';
    const BTS_ICON_OK_SIGN = 'icon-ok-sign';
    const BTS_ICON_QUESTION_SIGN = 'icon-question-sign';
    const BTS_ICON_INFO_SIGN = 'icon-info-sign';
    const BTS_ICON_SCREENSHOT = 'icon-screenshot';
    const BTS_ICON_REMOVE_CIRCLE = 'icon-remove-circle';
    const BTS_ICON_OK_CIRCLE = 'icon-ok-circle';
    const BTS_ICON_BAN_CIRCLE = 'icon-ban-circle';
    const BTS_ICON_ARROW_LEFT = 'icon-arrow-left';
    const BTS_ICON_ARROW_RIGHT = 'icon-arrow-right';
    const BTS_ICON_ARROW_UP = 'icon-arrow-up';
    const BTS_ICON_ARROW_DOWN = 'icon-arrow-down';
    const BTS_ICON_SHARE_ALT = 'icon-share-alt';
    const BTS_ICON_RESIZE_FULL = 'icon-resize-full';
    const BTS_ICON_RESIZE_SMALL = 'icon-resize-small';
    const BTS_ICON_PLUS = 'icon-plus';
    const BTS_ICON_MINUS = 'icon-minus';
    const BTS_ICON_ASTERISK = 'icon-asterisk';
    const BTS_ICON_EXCLAMATION_SIGN = 'icon-exclamation-sign';
    const BTS_ICON_GIFT = 'icon-gift';
    const BTS_ICON_LEAF = 'icon-leaf';
    const BTS_ICON_FIRE = 'icon-fire';
    const BTS_ICON_EYE_OPEN = 'icon-eye-open';
    const BTS_ICON_EYE_CLOSE = 'icon-eye-close';
    const BTS_ICON_WARNING_SIGN = 'icon-warning-sign';
    const BTS_ICON_PLANE = 'icon-plane';
    const BTS_ICON_CALENDAR = 'icon-calendar';
    const BTS_ICON_RANDOM = 'icon-random';
    const BTS_ICON_COMMENT = 'icon-comment';
    const BTS_ICON_MAGNET = 'icon-magnet';
    const BTS_ICON_CHEVRON_UP = 'icon-chevron-up';
    const BTS_ICON_CHEVRON_DOWN = 'icon-chevron-down';
    const BTS_ICON_RETWEET = 'icon-retweet';
    const BTS_ICON_SHOPPING_CART = 'icon-shopping-cart';
    const BTS_ICON_FOLDER_CLOSE = 'icon-folder-close';
    const BTS_ICON_FOLDER_OPEN = 'icon-folder-open';
    const BTS_ICON_RESIZE_VERTICAL = 'icon-resize-vertical';
    const BTS_ICON_RESIZE_HORIZONTAL = 'icon-resize-horizontal';
    const BTS_ICON_HDD = 'icon-hdd';
    const BTS_ICON_BULLHORN = 'icon-bullhorn';
    const BTS_ICON_BELL = 'icon-bell';
    const BTS_ICON_CERTIFICATE = 'icon-certificate';
    const BTS_ICON_THUMBS_UP = 'icon-thumbs-up';
    const BTS_ICON_THUMBS_DOWN = 'icon-thumbs-down';
    const BTS_ICON_HAND_RIGHT = 'icon-hand-right';
    const BTS_ICON_HAND_LEFT = 'icon-hand-left';
    const BTS_ICON_HAND_UP = 'icon-hand-up';
    const BTS_ICON_HAND_DOWN = 'icon-hand-down';
    const BTS_ICON_CIRCLE_ARROW_RIGHT = 'icon-circle-arrow-right';
    const BTS_ICON_CIRCLE_ARROW_LEFT = 'icon-circle-arrow-left';
    const BTS_ICON_CIRCLE_ARROW_UP = 'icon-circle-arrow-up';
    const BTS_ICON_CIRCLE_ARROW_DOWN = 'icon-circle-arrow-down';
    const BTS_ICON_GLOBE = 'icon-globe';
    const BTS_ICON_WRENCH = 'icon-wrench';
    const BTS_ICON_TASKS = 'icon-tasks';
    const BTS_ICON_FILTER = 'icon-filter';
    const BTS_ICON_BRIEFCASE = 'icon-briefcase';
    const BTS_ICON_FULLSCREEN = 'icon-fullscreen';

    # helpdesk constant
    const BTS_HELP_TASK_INSTANCE_CONNECTION = 'ptms_connect';
    const BTS_SYS_CONFIG_KEY_SYSTEM_CUSTOMER = 'key_system_customer';
    const BTS_HELP_TASK_CREATE_TYPE_CUSTOMER = 'CUSTOMER';
    const BTS_HELP_TASK_CREATE_TYPE_EMAIL = 'EMAIL';
    const BTS_HELP_TASK_CREATE_TYPE_SYSTEM = 'SYSTEM';
    const BTS_HELP_TASK_STATUS_NEW = 'new';
    const BTS_HELP_TASK_STATUS_ASSIGNED = 'assigned';
    const BTS_HELP_TASK_STATUS_PENDING = 'pending';
    const BTS_HELP_TASK_STATUS_PROGRESS = 'progress';
    const BTS_HELP_TASK_STATUS_SOLVED = 'solved';
    const BTS_HD_PRIORY_URG = 1;
    const BTS_HD_PRIORY_ALT = 2;
    const BTS_HD_PRIORY_MED = 3;
    const BTS_HD_PRIORY_BAJ = 4;
    const BTS_HD_STATUST_ESP = 1;
    const BTS_HD_STATUST_ENP = 2;
    const BTS_HD_STATUST_COM = 3;
    const BTS_HD_STATUST_CAN = 4;
    const BTS_HD_NIVEL_1 = 'NIVEL1';
    const BTS_HD_NIVEL_2 = 'NIVEL2';
    const BTS_HD_NIVEL_3 = 'NIVEL3';
    const BTS_HD_NIVEL_4 = 'NIVEL4';

    /*
     * CARPETAS PARA SUBIR ARCHIVOS
     */
    const TMP_UPLOAD = "tmp";
    const UPLOAD_IMAGE_HELPDESK = "image_helpdesk";
    const TMP_FILEXCEL = "excel";
    const PHOTO_PROFILE_DIR = "profile";
    const PHOTO_UPLOAD_128_DIR = "128";
    const PHOTO_UPLOAD_64_DIR = "64";
    const SHORCUTS = 'shortcuts';
    
    /*
     * CONSTANTES PARA REPORTES
     */
    
    const BTS_REPORT_LANG = "qlang";
    const BTS_REPORT_DATE_FORMAT = "qdf";
}
