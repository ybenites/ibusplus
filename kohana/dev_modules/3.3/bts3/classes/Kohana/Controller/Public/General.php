<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Public_General extends Kohana_Controller_Public_Common {

    public function action_cleaningCacheBasic() {
        try {
            $v_general = View::factory('bts/public/general/index');
            Cache::instance('apc')->delete_all();
            HelperCacheFile::fndeleteAllCache();
            apc_clear_cache();
            $s_message = __('Todo el Cache APC, y Archivos se borraron satisfactoriamente');
            $v_general->s_message =$s_message;
            $this->template->SYSTEM_BODY =$v_general;            
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_cleaningCacheAdvanced() {        
        try {
            $v_general = View::factory('bts/public/general/index');
            Cache::instance('apc')->delete_all();
            $s_message = __('Todo el Cache APC,Sesiones Nativas, de Base de datos, y Archivos se borraron satisfactoriamente');
            $v_general->s_message =$s_message;
            $this->template->SYSTEM_BODY =$v_general;  
            apc_clear_cache();
            HelperCacheFile::fndeleteAllCache();
            $this->fnSYSDestroySession();
            $this->fnSYSDestroyNativeSession();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_regenerateLanguages() {
        
    }

}