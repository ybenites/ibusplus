<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Public_Internationalization extends Kohana_Controller_Private_Kernel {

    public function action_change2Es() {
        $this->fnChangeClientLanguage('es');
    }

    public function action_change2En() {
        $this->fnChangeClientLanguage('en');
    }

    public function fnChangeClientLanguage($lang) {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        Cookie::set(self::BTS_LANGUAGE, $lang . '-' . $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN));
        $this->fnSYSResponseFormat($a_response);
        
    }

}
