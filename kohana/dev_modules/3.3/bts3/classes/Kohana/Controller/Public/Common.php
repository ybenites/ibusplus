<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common
 *
 * @author Hugo Casanova
 */
defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Public_Common extends Kohana_Controller_Private_Kernel {
     public $template = 'bts/public/index';
      public function before() {
        parent::before();
          if (!$this->request->is_ajax()) {
            $this->template->SYSTEM_A_SCRIPTS = $this->a_bts_scripts;
            $this->template->SYSTEM_A_STYLES = $this->a_bts_styles;
            $this->template->SYSTEM_A_PROJ_SCRIPTS = $this->a_proj_scripts;
            $this->template->SYSTEM_A_PROJ_STYLES = $this->a_proj_styles;
            $o_cache_instance = Cache::instance('apc');
            $s_request_url = 'CORE_PUBLIC';
            $s_cache_key_css = SITE_DOMAIN . $s_request_url . 'css';
            $s_cache_key_js = SITE_DOMAIN . $s_request_url . 'js';
            $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
            $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            if ($a_in_cache_object_css === null) {
                $o_cache_instance->set($s_cache_key_css, self::fnSYSGetFilePage('CORE_PUBLIC', self::BTS_FILE_TYPE_CSS));
                $o_cache_instance->set($s_cache_key_js, self::fnSYSGetFilePage('CORE_PUBLIC', self::BTS_FILE_TYPE_JS));
                $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
                $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            }
            $this->fnSYSAddScripts($a_in_cache_object_js);
            $this->fnSYSAddStyles($a_in_cache_object_css);
            $this->template->SYSTEM_A_MENU = self::fnSYSGenMenuArray();
            $this->template->SYSTEM_A_MENU_ACTIVE = $this->fnSYSGetArrayMenuActive();
            $this->template->SYSTEM_BREADCRUMBS = $this->fnSYSGetBreadcrumbs();
            $this->template->BTS_A_ACCESS = self::fnSYSGetSessionParameter(self::BTS_SESSION_ACTIONS_ACCESS);
            $this->template->BTS_A_SYSTEM_OPTIONS = self::fnSYSGetSessionParameter(self::BTS_SESSION_SYSTEM_OPTIONS);
            $this->template->BTS_USER_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
            $this->template->BTS_USER_FULL_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_FULL_NAME);
            $this->template->BTS_SESSION_USER_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
            $this->template->BTS_SESSION_OFFICE_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID);
            $this->template->BTS_SESSION_OFFICE_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_NAME);
            $this->template->BTS_SESSION_CITY_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_ID);
            $this->template->BTS_SESSION_CITY_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_NAME);
            $this->template->BTS_GROUP_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_NAME);
            $this->template->BTS_SESSION_GROUP_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID);
            $this->template->BTS_SESSION_USER_EMAIL = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_EMAIL);
            $this->template->BTS_SESSION_DEFAULT_HOME = self::fnSYSGetSessionParameter(self::BTS_SESSION_DEFAULT_HOME);
            $this->template->BTS_OPTION_LANGUAGE = $this->fnSYSGetOptionValue(self::BTS_LANGUAGE);
            $this->template->ROOT_MIMIC_OPTION = (self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION) OR self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_MIMIC_OPTION));
            $this->template->USER_MIMIC_OPTION = self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC);
        }
      }
}