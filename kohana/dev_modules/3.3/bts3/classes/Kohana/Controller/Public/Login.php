<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Public_Login extends Kohana_Controller_Private_Kernel {

    /**
     *
     * @var string Route template
     */
    public $template = 'bts/public/login/index';

    /**
     * Action INDEX
     */
    
    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        $this->fnSYSLoadClientLanguage();
        $a_interface = Kohana::$config->load('uInterface');
        foreach ($a_interface as $s_className) {
            $this->imports(get_class($s_className));
        }
    }        
    
    public function action_index() {        
        $this->template->check_form_direct_login_key = '123';
        if ($this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID) == NULL) {
            $this->fnSYSDestroySession();
        } else {
            $this->redirect(self::fnSYSGetSessionParameter(self::BTS_SESSION_DEFAULT_HOME));
        }
    }

    /**
     * Action AUTH
     */
    public function action_auth() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $s_user_name = $this->request->post('user_name');
        $s_user_pass = $this->request->post('user_password');
        if ($s_user_name == NULL OR $s_user_pass == NULL) {
            $this->action_logout();
        } else {
            try {
                $o_user = new Model_Bts_User();
                $o_user->where('userName', '=', $s_user_name)
                        ->where('password', '=', md5($s_user_pass))
                        ->and_where('status', '=', self::BTS_STATUS_ACTIVE)                        
                        ->find();

                if ($o_user->loaded()) {

                    if(! $o_user->systemActive){
                         throw new Exception(__("El servicio del Sistema ha sido deshabilitado por falta de pago. Comunicarse con su Supervisor, para poder regualizar el pago del servicio, gracias por su atencion."), self::BTS_CODE_SUCCESS);
                    }
                    Session::instance(Kohana_BtsConstants::KOHANA_SESSION_NAME_DATABASE);
                    
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_GROUP_ID, $o_user->group->idGroup);
                    $a_permited_actions_array = array();
                    //LOS PRIVILEGIOS DE USUARIO ESTÁN ACTIVADOS
//                    if (self::fnSYSGetSessionParameter(self::USER_CUSTOM_PRIVILEGE) == 1) {
//                        $actions = $this->createCustomUserPrivileges();
//                        foreach ($actions as $a) {
//                            array_push($permited_actions_array, $a['url']);
//                        }
//                    } else {
//                        //LOS PRIVILEGIOS DE USUARIO NO ESTAN ACTIVADOS, SE TOMA LOS PRIVILEGIOS DE GRUPO
//                        $actions = $this->createRestrictedMenuArray(NULL, TRUE);
//                        foreach ($actions as $a) {
//                            array_push($permited_actions_array, $a->url);
//                        }
//                    }

                    $a_permited_actions_array = self::fnSYSGetPrivilegesAccess(array($o_user->group->idGroup));                   
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_ACTIONS_ACCESS, $a_permited_actions_array);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_SYSTEM_OPTIONS, self::fnSYSGetOptionKeys());
                    
                    $a_actions = self::fnSYSGetSessionParameter(self::BTS_SESSION_ACTIONS_ACCESS);
                    #MUESTRA LAS URL DE LOS PERMISOS DEL USUARIO QUE INICIA SESIÓN
                    
                    if (empty($a_actions)) {
                        throw new Exception(__("Su cuenta no tiene privilegios en el sistema"), self::BTS_CODE_SUCCESS);
                        self::fnSYSDestroySession();
                    }
                    
                    $i_root_mimic = 0;
                    $i_root_real_user_iD = $o_user->idUser;
                    if ((bool) $o_user->rootOption AND $o_user->idUserMimic != NULL) {
                        $o_user_mimic = DB::select(DB::expr('um.idUser as idUser'))
                                        ->from(array('bts_user', 'u'))
                                        ->join(array('bts_user', 'um'))->on('u.idUserMimic', '=', 'um.idUser')
                                        ->where('u.idUser', '=', $o_user->idUser)
                                        ->and_where('um.status', '=', self::BTS_STATUS_ACTIVE)
                                        ->as_object()->execute()->current();
                        if ($o_user_mimic != NULL) {
                            $i_root_mimic = $o_user->rootOption;
                            $o_user = ORM::factory('Bts_User', $o_user_mimic->idUser);
                        }
                    }
                    if ((bool) $o_user->mimicOption AND $o_user->idUserMimic != NULL) {
                        $o_user_mimic = DB::select(DB::expr('um.idUser as idUser'))
                                        ->from(array('bts_user', 'u'))
                                        ->join(array('bts_user', 'um'))->on('u.idUserMimic', '=', 'um.idUser')
                                        ->where('u.idUser', '=', $o_user->idUser)
                                        ->and_where('um.status', '=', self::BTS_STATUS_ACTIVE)
                                        ->as_object()->execute()->current();
                        if ($o_user_mimic != NULL) {
                            $i_root_mimic = $o_user->mimicOption;
                            $o_user = ORM::factory('Bts_User', $o_user_mimic->idUser);
                        }
                    }
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_ID, md5(Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->id()));                    
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_NAME, $o_user->userName);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_FULL_NAME, $o_user->person->fullName);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_EMAIL, $o_user->person->email);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_ID, $o_user->idUser);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_OFFICE_ID, $o_user->office->idOffice);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_OFFICE_NAME, $o_user->office->name);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_CITY_ID, $o_user->office->idCity);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_CITY_NAME, $o_user->office->city->name);                    
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_TYPE, $o_user->person->type);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_ROOT_OPTION, $o_user->rootOption);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_USER_MIMIC_OPTION, $o_user->mimicOption);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC, $i_root_mimic);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC_REAL_ID, $i_root_real_user_iD);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_DEFAULT_HOME, $o_user->group->defaultHomePage);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_GROUP_NAME, $o_user->group->name);
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_GROUP_ID, $o_user->group->idGroup);                    
                    self::fnSYSSetSessionParameter(self::BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID,Model_Bts_Groupnotificationuser::getAllByUser($o_user->idUser));
                    //Event Log
                    self::fnSYSCreateSessionEvent(self::BTS_EVENT_LOG_IN);

                    //Extra Variables en session
                    if (array_key_exists('fnSYSLoadExtraDataSession', $this->a_imported_functions)) {
                        $data = $this->fnSYSLoadExtraDataSession();
                        foreach ($data as $key => $value) {
                            self::fnSYSSetSessionParameter($key, $value);
                        }
                    }
                    $a_response['data'] = array(
                        'url' => $o_user->group->defaultHomePage
                    );
            } else {
                    $ip = Request::$client_ip;                    
                    $o_userLoginF  = new Model_Bts_UserLoginLog();
                    $o_userLoginF->where('ip', '=', $ip)->find();
                    if ($o_userLoginF->loaded()) {
                    $o_userLoginF->count=$o_userLoginF->count+1;
                    $o_userLoginF->update();
                    }
                    
                    $o_userLogin  = new Model_Bts_UserLoginLog();
                    $o_userLogin->where('ip', '=', $ip)
                    ->and_where('count', '>', 4)  
                            ->find();

                    if ($o_userLogin->loaded()) {
                    $this->fnSYSAddNewModule('swift', 'swift');    
                    $system = $_SERVER['HTTP_HOST'];
                    Email::instance('Ingreso no autorizado')
                                ->to(array('hugo.casanova@ibusplus.com' =>'Hugo Casanova M'))
                                ->message(Email::template(View::factory('emails/password_retry'), array(":userName" => $s_user_name, ":password" => $s_user_pass, ":ip" => $ip, ":system" => $system)))
                                ->from('no_reply@ibusplus.com')->send(FALSE);
                    throw new Exception(__("Haz superado los numeros de intento de ingreso al sistema, seras un robot?."), self::BTS_CODE_SUCCESS);
                    }else{
                    $browser = get_browser(null, true);
                    $string = preg_replace('/[^(\x20-\x7F)]*/','', $browser);
                    $browser = json_encode($string);                  
                    $loginLog = new Model_Bts_UserLoginLog();
                    $loginLog->username=$s_user_name;
                    $loginLog->password=$s_user_pass;
                    $loginLog->ip=$ip;
                    $loginLog->dateAccess=  HelperDate::fnGetMysqlCurrentDateTime();
                    $loginLog->agent=Request::$user_agent;
                    $loginLog->moreInfo=$browser;
                    //$loginLog->host=$_SERVER['REMOTE_HOST'];
                    $loginLog->count=1;
                    $loginLog->create();
                    throw new Exception(__("Usuario o contraseña no válidos"), self::BTS_CODE_SUCCESS);
                    }
                }
            } catch (Exception $exc) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($exc);
            }
        }
        $this->fnSYSResponseFormat($a_response, self::BTS_RESPONSE_TYPE_JSONP);
    }

    public function action_logout() {
        if (self::fnSYSCreateSessionEvent(self::BTS_EVENT_LOG_OUT)) {
            Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->destroy();
            $this->redirect('/public/login/index.html');
        }
    }
    public function action_continueSession() {
        $this->auto_render = FALSE;
        $this->fnSYSResponseFormat($this->a_bts_json_response);
    }
    
    public function action_recoveryPass(){
        $a_response = $this->a_bts_json_response;
        $o_check_recovery = Validation::factory($this->request->post());         
        try {
              $o_check_recovery                
                ->rule('user', 'not_empty');
        if (!$o_check_recovery->check()) {
            throw new Exception(__("Información insuficiente para procesar su petición."), self::BTS_CODE_SUCCESS);
        }else{
              $nameUser = $o_check_recovery['user'];
              if ($nameUser == NULL) {
                $aResponse['code'] = self::CODE_ERROR;
                $aResponse['msg'] = __('El nombre de usuario es necesario para recuperar la contraseña');
            } else {
                    $result = DB::select(
                                 array('us.idUser','idUser')
                                 ,array('us.userName','userName')
                                 ,array('us.password','password')
                                 ,array('per.email','email')
                                 ,array('per.fullName','fullName')
                                 )
                            ->from(array('bts_user','us'))
                            ->join(array('bts_person','per'))->on('us.idUser','=','per.idPerson')
                            ->where('us.userName', 'LIKE', $nameUser)
                            ->and_where('us.status', '=', self::BTS_STATUS_ACTIVE)
                            ->execute()->current();
            
                    if (!empty($result)) {
                    if ($result['email'] == NULL) {
                        $aResponse['code'] = self::CODE_ERROR;
                        $aResponse['msg'] = __("Su cuenta de usuario no tiene asociado un correo electrónico por favor contéctese con el administrador del sistema para recuperar su contraseña");
                    } else {
                        $this->fnSYSAddNewModule('swift', 'swift');
                        $new_pass = strtoupper(base_convert((date('s') . $result['idUser'] . date('s') . $result['idUser'] . date('s') . date('His')), 10, 36));                        
                        $o_user = new Model_Bts_User($result['idUser']);
                        $o_user->password = md5($new_pass);
                        $o_user->update();
                        $o_userLog = new Model_Bts_UserLog();
                        $o_userLog->idUser =$result['idUser'];
                        $o_userLog->ip = Request::$client_ip;
                        $o_userLog->password =$new_pass;
                        $o_userLog->changeDate = HelperDate::fnGetMysqlCurrentDateTime();
                        $o_userLog->save();
                        Email::instance(__("Recuperación de Contraseña"))
                                ->to(array($result['email'] => $result['fullName']))
                                ->message(Email::template(View::factory('emails/password_recovery'), array(":userName" => $result['userName'], ":password" => $new_pass)))
                                ->from('no_reply@ibusplus.com')->send(FALSE);
                        $emailSendArray = explode('@', $result['email']);
                        $emailReplace = substr_replace($emailSendArray[0], '***', 4);
                        $emailSend = $emailReplace . '@' . $emailSendArray[1];
                        $aResponse['msg'] = __('La contraseña se ha enviado a la dirección de correo electrónico') . " " . $emailSend;
                    }
                }else {
                    $aResponse['code'] = self::BTS_CODE_NO_AUTHORIZED;
                    $aResponse['msg'] = __("El nombre de usuario no se encuentra registrado en el sistema o ha sido dado de baja");
            }
            
         }
            }
        }catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public static function fnSYSCreateSessionEvent($s_event) {
        try {
            $o_session_log = ORM::factory('Bts_SessionLog');
            $o_session_log->idUser = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
            $o_session_log->sessionId = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->id();
            $o_session_log->event = $s_event;
            $o_session_log->ip = Request::$client_ip;
            $o_session_log->agent = Request::$user_agent;
            $o_session_log->save();
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

}