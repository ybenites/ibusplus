<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Tickets extends Kohana_Controller_Private_Admin {

    function action_index() {
        $this->redirect('https://mcets.zendesk.com/home');die();
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('HELPDESK_TICKETS', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('HELPDESK_TICKETS', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );

        $v_helpdesk_tickets = View::factory('bts/private/helpdesk/tickets');
        $v_helpdesk_tickets->aPrioridades = $this->HD_PRIORIDADES;
        $v_helpdesk_tickets->aNiveles = $this->HD_NIVELES;
        $v_helpdesk_tickets->aStatus = $this->HD_STATUST;

        $o_model_ptms_customer_category = new Model_Bts_PtmsCustomerCategory();
        $v_helpdesk_tickets->a_categories = $o_model_ptms_customer_category->fnGetCategories($this->fnSYSGetConfig(self::BTS_SYS_CONFIG_KEY_SYSTEM_CUSTOMER));
        $this->template->SYSTEM_BODY = $v_helpdesk_tickets;
    }

    public function action_list() {
        try {
            $a_response = $this->a_bts_json_response;

            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');

            $i_idx = 'task_order DESC, ta.lastChangeDate ';
            $s_ord = 'DESC';

            $a_array_cols = array(
                "task_id" => "ta.idTask"
                , "task_title" => "ta.title"
                , 'task_description' => "
                    CASE 
                        WHEN LENGTH(ta.description) > 200
                            THEN CONCAT(SUBSTRING(ta.description, 1, 200), '...')
                        ELSE ta.description 
                    END"
                , "task_status" => "ta.status"
                , "tag_name" => "GROUP_CONCAT(tg.name)"
                , "tag_id" => "GROUP_CONCAT(tg.idTag)"
                , "task_date" => " UNIX_TIMESTAMP(ta.lastChangeDate)"
                , "task_order" => "
                    CASE
                        WHEN ta.status = '" . self::BTS_HELP_TASK_STATUS_NEW . "'
                            THEN 0
                        ELSE
                            1
                    END
                    "
                , 'task_create_type' => "ta.createType"
                , 'task_create_user' => "ta.userCreate"
            );
            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);

            $s_tables_join = ' pt_task ta
                        INNER JOIN pt_customercategory ck
                                ON ta.idCustomerCategory = ck.idCustomerCategory
                        INNER JOIN pt_customer cu
                                ON ck.idCustomer = cu.idCustomer
                        INNER JOIN pt_customerproject cp
                                ON cu.idCustomer = cp.idCustomer
                        LEFT JOIN pt_tag_task tt
                                ON ta.idTask = tt.idTask AND tt.status = 1
                        LEFT JOIN pt_tag tg
                                ON tt.idTag = tg.idTag
                    ';
            $key_system_customer = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_KEY_SYSTEM_CUSTOMER);
            $s_where_conditions = " cp.keyCustomer = '" . $key_system_customer . "' ";

            if ($this->fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID) == self::BTS_GROUP_ROOT || $this->fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID) == self::BTS_GROUP_ADMIN) {
                $s_where_conditions .= " AND ta.idUserRequest IS NOT NULL";
            } else {
                $s_where_conditions .= " AND ta.idUserRequest = " . $this->getSessionParameter(self::BTS_SESSION_USER_ID) . " ";
            }
            $s_group = 'GROUP BY ta.idTask';

            $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, NULL, '', 'DISTINCT ta.idTask', NULL, self::BTS_HELP_TASK_INSTANCE_CONNECTION);
            $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, NULL, $i_idx, $s_ord, $i_limit, $i_start, $s_group, '', self::BTS_HELP_TASK_INSTANCE_CONNECTION);

            $a_result = $a_result->as_array();
            foreach ($a_result as $key => $a_item) {
                $a_user_create = array();

                if ($a_item['task_create_type'] == self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER) {
                    $o_model_user = new Model_Bts_PtmsUser($a_item['task_create_user']);
                    $a_user_create['user_name'] = $o_model_user->person->fullName;
                    $a_user_create['user_image'] = $o_model_user->imgProfile;
                } elseif ($a_item['task_create_type'] == self::BTS_HELP_TASK_CREATE_TYPE_SYSTEM) {
                    $o_model_user = new Model_Bts_PtmsUser($a_item['task_create_user']);
                    $a_user_create['user_name'] = $o_model_user->person->fullName;
                    $a_user_create['user_image'] = 'http://ptms.ibusplus.com' . $o_model_user->imgProfile;
                } elseif ($a_item['task_create_type'] == self::BTS_HELP_TASK_CREATE_TYPE_EMAIL) {
                    $a_user_create['user_name'] = 'Email';
                    $a_user_create['user_image'] = 'none';
                }

                $a_select_total_commemt = DB::select(
                                array(DB::expr('COUNT(*)'), 'total')
                        )
                        ->from(array('pt_taskcomment', 'pc'))
                        ->where('pc.idTask', '=', $a_item['task_id'])
                        ->and_where('pc.userType', '=', self::BTS_HELP_TASK_CREATE_TYPE_SYSTEM)
                        ->execute(self::BTS_HELP_TASK_INSTANCE_CONNECTION)
                        ->current();

                $a_item['total_coments'] = $a_select_total_commemt['total'];
                $a_item['user_name'] = $a_user_create['user_name'];
                $a_item['user_image'] = $a_user_create['user_image'];
                $a_item['url'] = HelperFile::fnToAscii($a_item['task_title'], array("'"));
                $a_result[$key] = $a_item;
            }

            $a_response['page'] = $i_page;
            $a_response['total'] = ceil($i_count / $i_limit);
            $a_response['record'] = $i_count;
            $a_response['data'] = $a_result;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getUploadFileHelpDesk() {
        $this->fnSYSUploadFileTMP(self::TMP_UPLOAD, NULL, NULL, TRUE, TRUE);
    }

    public function action_deleteImageTicket() {
        try {
            $a_response = $this->a_bts_json_response;
            $img_delete = $this->request->post('img_delete');
            if (count($img_delete) > 0) {
                foreach ($img_delete as $v) {
                    HelperFile::deleteFileOrFolderRecursive(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $v);
                }
            }
            $a_response['success'] = true;
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_listTagTicket() {
        try {
            $a_response = $this->a_bts_json_response;
            $s_term = $this->request->query('q');
            $a_tag = DB::select(
                            array(DB::expr('DISTINCT `ta`.`idTag`'), 'id')
                            , array('ta.name', 'value')
                    )
                    ->from(array('pt_tag', 'ta'))
                    ->where('ta.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('ta.name', 'LIKE', '%' . $s_term . '%')
                    ->execute(self::BTS_HELP_TASK_INSTANCE_CONNECTION)
                    ->as_array();
            $a_response['data'] = $a_tag;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSTicketSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSTicketSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('new_ticket_title', 'not_empty')
                ->rule('new_ticket_category', 'not_empty')
                ->rule('new_ticket_description', 'not_empty');

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'new_ticket_title':
                        $s_label_name = __("Titulo");
                        break;
                    case 'new_ticket_category':
                        $s_label_name = __("Categoria");
                        break;
                    case 'new_ticket_description':
                        $s_label_name = __("Descripción");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }
        Database::instance()->begin();
        $a_form_post_data = $o_check_post->data();
        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_ptms_task = new Model_Bts_PtmsTask();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_ptms_task = new Model_Bts_PtmsTask();
            if (!$o_model_ptms_task->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
        $o_model_person = new Model_Bts_Person($this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID));
        $a_task_save = array(
            'idCustomerCategory' => $a_form_post_data['new_ticket_category']
            , 'title' => $a_form_post_data['new_ticket_title']
            , 'description' => $a_form_post_data['new_ticket_description']
            , 'idUserRequest' => $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID)
            , 'emailFrom' => $o_model_person->email
            , 'nameUserRequest' => $o_model_person->fullName
            , 'codeTask' => 'IN' . strtoupper(base_convert(rand() . (date('dHis')) . rand(), 7, 36))
            , 'createType' => self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER
            , 'updateType' => self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER
            , 'status' => self::BTS_HELP_TASK_STATUS_NEW
        );
        $o_model_ptms_task->saveArray($a_task_save);

        if (array_key_exists('new_ticket_tags_id', $a_form_post_data) && count($a_form_post_data['new_ticket_tags_id']) > 0) {
            foreach ($a_form_post_data['new_ticket_tags_id'] as $key => $tag) {
                if (strpos($key, 'id_') !== FALSE) {
                    $o_model_tag = new Model_Bts_PtmsTag();
                    $a_tag_save = array(
                        'name' => strtoupper($tag)
                    );
                    $o_model_tag->saveArray($a_tag_save);
                } else {
                    $o_model_tag = new Model_Bts_PtmsTag($key);
                    if (!$o_model_tag->loaded())
                        throw new Exception(__("Tag no encontrado"), self::BTS_CODE_SUCCESS);
                }
                $o_model_tag_task = new Model_Bts_PtmsTagTask();
                $a_tag_task_save = array(
                    'idTag' => $o_model_tag->idTag
                    , 'idTask' => $o_model_ptms_task->idTask
                );
                $o_model_tag_task->saveArray($a_tag_task_save);
            }
        }

        if (array_key_exists('array_file_upload', $a_form_post_data) && array_key_exists('name_orig_file_upload', $a_form_post_data)) {
            $array_file_upload = $a_form_post_data['array_file_upload'];
            $name_orig_file_upload = $a_form_post_data['name_orig_file_upload'];
            $dir = 'img';
            $skin = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
            $sub_dir = self::UPLOAD_IMAGE_HELPDESK;
            if (count($array_file_upload) > 0 && count($name_orig_file_upload) > 0) {
                if (!file_exists($dir . DIRECTORY_SEPARATOR . $skin)) {
                    mkdir($dir . DIRECTORY_SEPARATOR . $skin);
                }
                if (!file_exists($dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir)) {
                    mkdir($dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir);
                }
                foreach ($array_file_upload as $key => $v) {
                    copy(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $v, $dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir . DIRECTORY_SEPARATOR . $v);
                    HelperFile::deleteFileOrFolderRecursive(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $v);
                    $o_model_filetask = new Model_Bts_PtmsFileTask();
                    $a_file_task_save = array(
                        'idTask' => $o_model_ptms_task->idTask
                        , 'pathFileTask' => DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir . DIRECTORY_SEPARATOR . $v
                        , 'nameOriginFile' => $name_orig_file_upload[$key]
                    );
                    $o_model_filetask->saveArray($a_file_task_save);
                }
            }
        }


        $emailSend = "soporte@ibusplus.com";
        $config = $this->fnSYSGetConfig(NULL, TRUE);
        $personSend = $config->get(self::BTS_SYS_CONFIG_SITE_NAME);
        $website = $_SERVER['HTTP_HOST'];
        $view = "bts/private/emails/ptms_mail_task";
        $company = $config->get(self::BTS_SYS_CONFIG_SITE_NAME);
        $contactCompany = $config->get(self::BTS_SYS_CONFIG_CONTACT_COMPANY);

        $asunto = $o_model_ptms_task->status . ': #[' . $o_model_ptms_task->codeTask . '] ' . $o_model_ptms_task->title . ' [' . $company . ']';

        $contactAddress = $config->get(self::BTS_SYS_CONFIG_CONTACT_ADDRESS);
        $contactPhone = $config->get(self::BTS_SYS_CONFIG_CONTACT_PHONE);

        $msj = array(
            ":contactCompany" => $contactCompany
            , ":contactAddress" => $contactAddress
            , ":contactPhone" => $contactPhone
            , ":website" => $website
            , ":code" => $o_model_ptms_task->codeTask
            , ':title' => $o_model_ptms_task->title
            , ":client" => $company
            , ":project" => ""
            , ":typetask" => $o_model_ptms_task->type
            , ":severity" => $o_model_ptms_task->importance
            , ":priority" => $o_model_ptms_task->idPriority
            , ":assinedBy" => ""
            , ":createBy" => $o_model_person->fullName
            , ":description" => $o_model_ptms_task->description
        );
        
//        $this->fnSYSSendEmail($asunto, $o_model_person->email, $o_model_person->fullName, $view,$obj=NULL, $msj, $emailSend, $personSend);

        Database::instance()->commit();
    }

}