<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Module extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_MODULE', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_MODULE', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                    , array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );
        $v_admin_module = View::factory('bts/private/admin/module');
        $v_admin_module->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();
        $this->template->SYSTEM_BODY = $v_admin_module;
    }

    public function action_list() {
        try {
            $o_cacheInstance = Cache::instance('apc');
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' mo.idModule ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "module_id" => "mo.idModule"
                , "module_name" => "mo.name"
                , "module_status" => "mo.status"
                , "module_order" => "mo.order"
                , "module_userCreate" => "pe.fullName"
                , "module_dateCreate" => "mo.creationDate"
                , "module_userLastChange" => "per.fullName"
                , "module_dateLastChange" => "mo.lastChangeDate"
            );
            $a_columns_search = array(
                "module_id" => "mo.idModule"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_module mo INNER JOIN bts_person pe ON mo.userCreate = pe.idPerson
            INNER JOIN bts_person per ON mo.userLastChange = per.idPerson
            ';

            //    $s_where_conditions = " mo.status  = ". self::BTS_STATUS_ACTIVE." ";
            $s_where_conditions = "1=1";
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action() . $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSModuleSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSModuleSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_module = new Model_Bts_Module($a_form_post_data['id']);

            if (!$o_model_module->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'module_code' => $o_model_module->idModule
                , 'module_name' => $o_model_module->name
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSModuleSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('fmodule_code', 'not_empty')
                ->rule('fmodule_name', 'not_empty');
        $o_check_post->rule('fmodule_code', 'Model_Bts_Module::fnValidCodeName', array(':value', null, $this->request->post('fmodule_id')));
        $o_check_post->rule('fmodule_name', 'Model_Bts_Module::fnValidCodeName', array(null, ':value', $this->request->post('fmodule_id')));

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'fmodule_id':
                        $s_label_name = __("Id");
                        break;
                    case 'fmodule_code':
                        $s_label_name = __("Codigo");
                        break;

                    case 'fmodule_name':
                        $s_label_name = __("Nombre");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    case 'Model_Bts_Module::fnValidCodeName':
                        $a_error_labels[] = $s_label_name . ' ' . __("duplicado");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_ERROR);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();

        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_module = new Model_Bts_Module();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_module = new Model_Bts_Module($a_form_post_data['fmodule_id']);
            if (!$o_model_module->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }

        $a_module_save = array(
            'idModule' => $a_form_post_data['fmodule_code']
            , 'name' => $a_form_post_data['fmodule_name']
            , 'status' => self::BTS_STATUS_ACTIVE
        );
        $o_model_module->saveArray($a_module_save);
    }

    public function action_changeStatus() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_module = new Model_Bts_Module($a_form_post_data['id']);

            if (!$o_model_module->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_module->status = ($o_model_module->status == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);
            $o_model_module->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_share(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            foreach ($o_check_post['fshare_modules'] as $i_local_id) {
                $o_model_controller = new Model_Bts_Module($i_local_id);

                if (!$o_model_controller->loaded())
                    $a_error_labels[] = __("Registro no encontrado") . ' ID:' . $i_local_id;

                foreach ($o_check_post['fshare_hosts'] as $s_instance) {
                    $o_model_instance_controller = new Model_Bts_Module();
                    $o_model_instance_controller->saveInstance($s_instance, $o_model_controller);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}
