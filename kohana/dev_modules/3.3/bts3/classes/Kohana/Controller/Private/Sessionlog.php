<?php
defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Sessionlog extends Kohana_Controller_Private_Admin {
     public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {            
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('SUPPORT_SESSION_LOG', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('SUPPORT_SESSION_LOG', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    ) 
                    , array(
                        'file_name' => '/kernel/js/bts/core/datepicker/bootstrap-datetimepicker.' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );
        $format_date_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL);
        $format_date_jquery=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_JQUERY);
        $format_date_php=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_PHP);        
        $v_admin_sessionlog = View::factory('bts/private/admin/sessionlog');        
        $v_admin_sessionlog->jquery_date_format =$format_date_jquery;
        $v_admin_sessionlog->sql_date_format =$format_date_sql;
        $v_admin_sessionlog->today = date($format_date_php);        
        $this->template->SYSTEM_BODY = $v_admin_sessionlog;
    }
    
    public function action_list(){
        try {
            
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');
            
            if (!$s_idx)
                $s_idx = ' slog.idSessionLog ';

            $s_where_search = "";
            $b_date_format =$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL);
            $b_time_format =$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL);
            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(                
                'sessionlog_id' => "slog.idSessionLog",
                'sessionlog_event' => "slog.event",
                'sessionlog_user' => "u.fullName",
                'sessionlog_ip' => "slog.ip",
                'sessionlog_agent' => "slog.agent",
                'sessionlog_eventDate' => "DATE_FORMAT(slog.eventDate,'$b_date_format $b_time_format')"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);
            $a_columns_search = array(
                "sessionlog_user" => 'u.fullName',
                "sessionlog_ip" => 'slog.ip',
                "sessionlog_eventDate" =>"DATE_FORMAT(slog.eventDate,'$b_date_format')",
            );
            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));                    
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);                    
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            print_r($s_value);die();
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_sessionlog slog  
                    INNER JOIN bts_person u ON u.idPerson = slog.idUser 
            ';
            $s_where_conditions = "slog.sessionId IS NOT NULL";

            if (isset($_REQUEST['fdate_sessionlog_search'])) {
                $s_where_conditions .= " AND CAST(slog.eventDate AS DATE) = '" . $_REQUEST['fdate_sessionlog_search'] . "' ";
            }
            $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_total_pages = 0;

            $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);

            $this->fnSYSResponseFormat(HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
?>
