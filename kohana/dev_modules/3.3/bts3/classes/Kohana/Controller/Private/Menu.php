<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Menu extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_MENU', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_MENU', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                    , array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );

        $v_admin_menu = View::factory('bts/private/admin/menu');
        $v_admin_menu->BTS_A_MODULES = Model_Bts_Module::fnGetAll();
        $v_admin_menu->BTS_A_OPTIONS_LIST = Model_Bts_Option::fnGetAll();
        $v_admin_menu->BTS_A_MENU_TYPES = $this->a_bts_menu_type;
        $v_admin_menu->BTS_A_ICONS_BOOTSTRAP = $this->a_bts_icons_bootstrap;
        $v_admin_menu->BTS_SKIN =$this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
        $v_admin_menu->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();

        $this->template->SYSTEM_BODY = $v_admin_menu;
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_menu = new Model_Bts_Menu($a_form_post_data['id']);

            if (!$o_model_menu->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'menu_id' => $o_model_menu->idMenu
                , 'menu_name' => $o_model_menu->name
                , 'menu_url' => $o_model_menu->url
                , 'menu_type' => $o_model_menu->type
                , 'menu_icon' => $o_model_menu->icon
                , 'menu_is_root' => $o_model_menu->rootOption
                , 'menu_is_withshortcut' => $o_model_menu->withshortcut
                , 'menu_iconshortcut' => $o_model_menu->iconShortcut
                , 'menu_option_key' => $o_model_menu->optionKey
                , 'menu_option_key_value' => $o_model_menu->optionKeyValue
                , 'menu_module' => $o_model_menu->idModule
                , 'menu_super_menu' => $o_model_menu->idSuperMenu
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSMenuSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('fmenu_type', 'not_empty');

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'fmenu_type':
                        $s_label_name = __("Tipo");
                        break;
                    default:
                        break;
                }

                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();

        switch ($a_form_post_data['fmenu_type']) {
            case self::BTS_MENU_TYPE_ITEM:
                $o_check_post->rule('fmenu_name', 'not_empty');
                $o_check_post->rule('fmenu_url', 'not_empty');
                break;
            case self::BTS_MENU_TYPE_HEADER:
                $o_check_post->rule('fmenu_name', 'not_empty');
                break;
            default:
                break;
        }

        $s_value = null;
        switch ($a_form_post_data['foption_type']) {
            case self::BTS_OPTION_TYPE_BOOLEAN:
                $s_value = $a_form_post_data['fmenu_option_type_boolean'];
                $o_check_post->rule('fmenu_option_type_boolean', 'not_empty');
                break;
            case self::BTS_OPTION_TYPE_STRING:
                $s_value = $a_form_post_data['fmenu_option_type_string'];
                $o_check_post->rule('fmenu_option_type_string', 'not_empty');
                break;
            case self::BTS_OPTION_TYPE_COLOR:
                $s_value = $a_form_post_data['fmenu_option_type_color'];
                $o_check_post->rule('fmenu_option_type_color', 'not_empty');
                break;
            case self::BTS_OPTION_TYPE_INTEGER:
                $s_value = $a_form_post_data['fmenu_option_type_integer'];
                $o_check_post->rule('fmenu_option_type_integer', 'not_empty');
                $o_check_post->rule('fmenu_option_type_integer', 'numeric');
                break;
            default:
                break;
        }

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {

                switch ($s_key) {
                    case 'fmenu_name':
                        $s_label_name = __("Nombre");
                        break;
                    case 'fmenu_url':
                        $s_label_name = __("URL");
                        break;
                    case 'fmenu_option_type_boolean':
                        $s_label_name = __("Valor");
                        break;
                    case 'fmenu_option_type_string':
                        $s_label_name = __("Valor");
                        break;
                    case 'fmenu_option_type_color':
                        $s_label_name = __("Valor");
                        break;
                    case 'fmenu_option_type_integer':
                        $s_label_name = __("Valor");
                        break;
                    default:
                        break;
                }

                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_menu = new Model_Bts_Menu();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_menu = new Model_Bts_Menu($a_form_post_data['fmenu_id']);
            if (!$o_model_menu->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
        $a_menu_save = array(
            'name' => $a_form_post_data['fmenu_name']
            , 'url' => $a_form_post_data['fmenu_url']
            , 'type' => $a_form_post_data['fmenu_type']
            , 'icon' => $a_form_post_data['fmenu_icon']
            , 'rootOption' => $a_form_post_data['fmenu_isroot']
            , 'withshortcut' => $a_form_post_data['fmenu_shortcut']
            , 'optionKey' => $a_form_post_data['fmenu_option'] != '' ? $a_form_post_data['fmenu_option'] : null
            , 'optionKeyValue' => $s_value
            , 'idModule' => $a_form_post_data['fmenu_module'] != '' ? $a_form_post_data['fmenu_module'] : null
            , 'idSuperMenu' => $a_form_post_data['fmenu_super_menu'] != '' ? $a_form_post_data['fmenu_super_menu'] : null
            , 'iconShortcut' => ($a_form_post_data['fmenu_shortcut'] == 1) ? $a_form_post_data['fmenu_hide_image'] : null
        );
        if ($a_form_post_data['fmenu_shortcut'] == 1 && $a_form_post_data['fmenu_hide_image'] != NULL) {
            $dir = 'img';
            $skin = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
            $sub_dir = self::SHORCUTS;
            if (!file_exists($dir . DIRECTORY_SEPARATOR . $skin)) {
                mkdir($dir . DIRECTORY_SEPARATOR . $skin);
            }
            if (!file_exists($dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir)) {
                mkdir($dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir);
            }
            if(file_exists(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $a_form_post_data['fmenu_hide_image'])){
                copy(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $a_form_post_data['fmenu_hide_image'], $dir . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . $sub_dir . DIRECTORY_SEPARATOR . $a_form_post_data['fmenu_hide_image']);
                HelperFile::deleteFileOrFolderRecursive(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $a_form_post_data['fmenu_hide_image']);
            }
        }
        $o_model_menu->saveArray($a_menu_save);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSMenuSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSMenuSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_list() {
        $this->auto_render = FALSE;
        try {
            $i_node = (integer) $_REQUEST['nodeid'];
            $i_lvl = (integer) $_REQUEST['n_level'];
            $s_sql_leaf_nodes =
                    ' SELECT l.idSuperMenu '
                    . ' FROM bts_menu AS m '
                    . ' LEFT JOIN bts_menu AS l '
                    . ' ON m.idMenu = l.idSuperMenu '
                    . ' WHERE l.idMenu IS NOT NULL AND l.status=' . self::BTS_STATUS_ACTIVE
                    . ' ORDER BY l.order';
            $a_result_leaf_nodes = DB::query(
                            Database::SELECT
                            , $s_sql_leaf_nodes
                    )
                    ->execute()
                    ->as_array();
            $a_leaf_nodes = array();
            foreach ($a_result_leaf_nodes as $a_item) {
                $a_leaf_nodes[$a_item['idSuperMenu']] = $a_item['idSuperMenu'];
            }

            header("Content-type: text/xml;charset=utf-8");
            $et = ">";
            echo "<?xml version='1.0' encoding='utf-8'?$et\n";
            echo "<rows>";
            echo "<page>1</page>";
            echo "<total>1</total>";
            echo "<records>1</records>";
            if ($i_node > 0) {
                $s_add_where = 'menu.idSuperMenu=' . $i_node . ' AND menu.status=' . self::BTS_STATUS_ACTIVE;
                $i_lvl++;
            } else {
                $s_add_where = 'ISNULL(menu.idSuperMenu) AND menu.status=' . self::BTS_STATUS_ACTIVE;
            }
            $s_sql_next_node =
                    "SELECT menu.idMenu, "
                    . "CASE 
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_ITEM . "'
                                THEN menu.name
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_HEADER . "'
                                THEN menu.name
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_SEPARATOR . "'
                                THEN '========='
                            ELSE
                            menu.name
                        END name, "
                    . "CASE 
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_ITEM . "'
                                THEN menu.url
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_HEADER . "'
                                THEN ''
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_SEPARATOR . "'
                                THEN ''
                            ELSE
                            menu.url
                        END url, "
                    . " menu.idSuperMenu, "
                    . "CASE 
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_ITEM . "'
                                THEN menu.icon
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_HEADER . "'
                                THEN menu.icon
                            WHEN menu.type = '" . self::BTS_MENU_TYPE_SEPARATOR . "'
                                THEN ''
                            ELSE
                            menu.icon
                        END icon, "
                    . " menu.type, "
                    . " menu.order "
                    . " FROM bts_menu menu "
                    . " WHERE " . $s_add_where
                    . " ORDER BY menu.order";
            $a_next_node = DB::query(Database::SELECT, $s_sql_next_node)->execute()->as_array();
            foreach ($a_next_node as $a_item) {
                #VALIDACIONES
                if (!$a_item['idSuperMenu'])
                    $valp = 'NULL';
                else
                    $valp = $a_item['idSuperMenu'];
                if (array_search($a_item['idMenu'], $a_leaf_nodes) > 0)
                    $leaf = 'false';
                else
                    $leaf = 'true';

                $ico_cell = htmlspecialchars('<i class="' . $a_item['icon'] . '"></i>');

                $edit_option = htmlspecialchars('<a title="' . __("Editar") . '" class="btn btn-mini menu_edit" style="cursor: pointer;" data-id="' . $a_item['idMenu'] . '"><i class="icon-pencil"></i></a>');
                $delete_option = htmlspecialchars('<a title="' . __("Eliminar") . '" class="btn btn-mini menu_delete"  data-id="' . $a_item['idMenu'] . '" ><i class="icon-trash"></i></a>');

                $submenu_option = htmlspecialchars('<a title="' . __("Agregar Submenu") . '" class="btn btn-mini menu_add_sub_menu"  data-id="' . $a_item['idMenu'] . '"><i class="icon-plus"></i></a>');
                //$php_action_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-php-action" style="cursor: pointer;" rel="' . $a_item['idMenu'] . '" title="' . __("Nueva Acción PHP") . '" ></a>');

                $order_option = htmlspecialchars('<a title="' . __("Ordenar") . '" class="btn btn-mini menu_sort" data-id="' . $a_item['idMenu'] . '"><i class="icon-resize-vertical"></i></a>');
                $level_option = htmlspecialchars('<a title="' . __("Mover Menu") . '" class="btn btn-mini menu_order_level" data-id="' . $a_item['idMenu'] . '" id="popover_menu_order_level_' . $a_item['idMenu'] . '"><i class="icon-retweet"></i></a>');

                switch ($a_item['type']) {
                    case self::BTS_MENU_TYPE_ITEM:
                        $options = $edit_option . $delete_option . $order_option . $submenu_option . $level_option;
                        break;
                    case self::BTS_MENU_TYPE_SEPARATOR:
                        $options = $edit_option . $delete_option . $order_option . $level_option;
                    case self::BTS_MENU_TYPE_HEADER:
                        $options = $edit_option . $delete_option . $order_option . $level_option;
                        break;
                    default :
                        break;
                }

                $s_menu_type_icon = '';
                switch ($a_item['type']) {
                    case self::BTS_MENU_TYPE_ITEM :
                        $s_menu_type_icon = 'icon-list';
                        break;
                    case self::BTS_MENU_TYPE_SEPARATOR :
                        $s_menu_type_icon = 'icon-minus';
                        break;
                    case self::BTS_MENU_TYPE_HEADER :
                        $s_menu_type_icon = 'icon-font';
                        break;
                    default:
                        break;
                }

                $type_cell = htmlspecialchars('<i class="' . $s_menu_type_icon . '"></i>');


                echo "<row>";
                echo "<cell>" . $a_item['idMenu'] . "</cell>";
                echo "<cell>" . __($a_item['name']) . "</cell>";
                echo "<cell>" . $a_item['url'] . "</cell>";
                echo "<cell>" . $ico_cell . "</cell>";
                echo "<cell>" . $a_item['type'] . "</cell>";
                echo "<cell>" . $type_cell . "</cell>";
                echo "<cell>" . $options . "</cell>";
                echo "<cell>" . $i_lvl . "</cell>";
                echo "<cell><![CDATA[" . $valp . "]]></cell>";
                echo "<cell>" . $leaf . "</cell>";
                echo "<cell>false</cell>";
                echo "</row>";
            } echo "</rows>";
        } catch (Exception $exc) {
            echo $this->fnSYSErrorHandling($exc);
        }
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_menu = new Model_Bts_Menu($a_form_post_data['id']);

            if (!$o_model_menu->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_menu->status = self::BTS_STATUS_DEACTIVE;

            $o_model_menu->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_listOrder() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $a_response['data'] = Model_Bts_Menu::fnGetAllSameLevel($a_form_post_data['id']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_saveOrder() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('menu_sort_key', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'menu_sort_key':
                            $s_label_name = __("Menus");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();


            foreach ($a_form_post_data['menu_sort_key'] as $i => $i_id) {
                $o_model_menu = new Model_Bts_Menu($i_id);

                if (!$o_model_menu->loaded())
                    throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

                $o_model_menu->order = $i + 1;

                $o_model_menu->save();
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getUploadImageMenu() {
        $this->fnSYSUploadFileTMP(self::TMP_UPLOAD, NULL, 32);
    }

    public function action_updateOrderLevel() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('hmenu_level_source', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'hmenu_level_source':
                            $s_label_name = __("Menu inicio");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_model_menu = new Model_Bts_Menu($a_form_post_data['hmenu_level_source']);
            if ($a_form_post_data['hmenu_level_destination'] != '')
                $o_model_menu->idSuperMenu = $a_form_post_data['hmenu_level_destination'];
            else
                $o_model_menu->idSuperMenu = NULL;

            $o_model_menu->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_share() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $obj = new Model_Bts_Menu();
            $a_translate_ids = $obj->getAllMenuIDs();            
            foreach ($a_translate_ids as $i_local_id) {
                $o_model_menu = new Model_Bts_Menu($i_local_id);

                if (!$o_model_menu->loaded())
                    $a_error_labels[] = __("Registro no encontrado") . ' ID:' . $i_local_id;

                foreach ($o_check_post['fshare_hosts'] as $s_instance) {
                    $o_model_instance_controller = new Model_Bts_Menu();
                    $o_model_instance_controller->saveInstance($s_instance, $o_model_menu);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

}