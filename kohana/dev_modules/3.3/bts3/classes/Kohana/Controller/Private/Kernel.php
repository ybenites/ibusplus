<?php

defined('SYSPATH') or die('No direct script access.');
define('LOG_BTS_EXCEPTION', 441);

abstract class Kohana_Controller_Private_Kernel extends Controller_Template implements Kohana_BtsConstants {

    public static $JSONP_CALLBACK = 'jsonp_callback';
    public static $AJAX_DATA_TYPE = 'jsonp';
    public $template = 'bts/private/layout';
    private $a_imported_class = array();
    protected $a_imported_functions = array();
    public static $BOOTSTRAP_VERSION = '2.3.2';
    public static $JQUERY_VERSION = '2.0.1';
    public static $JQUERY_UI_VERSION = '1.10.3';

    /**
     * Global Variables 
     */
    public $a_bts_styles = array();
    public $a_bts_scripts = array();
    public $a_proj_styles = array();
    public $a_proj_scripts = array();
    public $a_bts_json_response = array(
        self::BTS_RESPONSE_TYPE_JSON_CODE => self::BTS_CODE_SUCCESS,
        self::BTS_RESPONSE_TYPE_JSON_MSG => self::BTS_CODE_SUCCESS
    );
    public $a_bts_file_type = array(
        self::BTS_FILE_TYPE_CSS => self::BTS_FILE_TYPE_CSS,
        self::BTS_FILE_TYPE_JS => self::BTS_FILE_TYPE_JS
    );
    public $a_bts_modules = array(
        self::BTS_MODULE_ADMINISTRATION => array(
            'value' => self::BTS_MODULE_ADMINISTRATION
            , 'display' => 'Administracion'
            , 'short' => 'ADM'
        )
        , self::BTS_MODULE_SUPPORT => array(
            'value' => self::BTS_MODULE_SUPPORT
            , 'display' => 'Soporte'
            , 'short' => 'SUP'
        )
    );
    public $a_bts_controller_types = array(
        self::BTS_CONTROLLER_TYPE_READ => array(
            'value' => self::BTS_CONTROLLER_TYPE_READ
            , 'display' => 'Leer'
            , 'short' => 'REA'
            , 'default' => true
        )
        , self::BTS_CONTROLLER_TYPE_CREATE => array(
            'value' => self::BTS_CONTROLLER_TYPE_CREATE
            , 'display' => 'Crear'
            , 'short' => 'CRE'
            , 'default' => false
        )
        , self::BTS_CONTROLLER_TYPE_UPDATE => array(
            'value' => self::BTS_CONTROLLER_TYPE_UPDATE
            , 'display' => 'Actualizar'
            , 'short' => 'UPD'
            , 'default' => false
        )
        , self::BTS_CONTROLLER_TYPE_DELETE => array(
            'value' => self::BTS_CONTROLLER_TYPE_DELETE
            , 'display' => 'Eliminar'
            , 'short' => 'DEL'
            , 'default' => false
        )
    );
    public $a_bts_menu_type = array(
        self::BTS_MENU_TYPE_ITEM => array(
            'value' => self::BTS_MENU_TYPE_ITEM
            , 'display' => 'Item'
            , 'short' => 'ITE'
        )
        , self::BTS_MENU_TYPE_SEPARATOR => array(
            'value' => self::BTS_MENU_TYPE_SEPARATOR
            , 'display' => 'Separador'
            , 'short' => 'SEP'
        )
        , self::BTS_MENU_TYPE_HEADER => array(
            'value' => self::BTS_MENU_TYPE_HEADER
            , 'display' => 'Titulo'
            , 'short' => 'HEA'
        )
    );
    public $a_bts_icons_bootstrap = array(
        self::BTS_ICON_GLASS => array(
            'value' => self::BTS_ICON_GLASS
            , 'display' => 'GLASS'
        )
        , self::BTS_ICON_MUSIC => array(
            'value' => self::BTS_ICON_MUSIC
            , 'display' => 'MUSIC'
        )
        , self::BTS_ICON_SEARCH => array(
            'value' => self::BTS_ICON_SEARCH
            , 'display' => 'SEARCH'
        )
        , self::BTS_ICON_ENVELOPE => array(
            'value' => self::BTS_ICON_ENVELOPE
            , 'display' => 'ENVELOPE'
        )
        , self::BTS_ICON_HEART => array(
            'value' => self::BTS_ICON_HEART
            , 'display' => 'HEART'
        )
        , self::BTS_ICON_STAR => array(
            'value' => self::BTS_ICON_STAR
            , 'display' => 'STAR'
        )
        , self::BTS_ICON_STAR_EMPTY => array(
            'value' => self::BTS_ICON_STAR_EMPTY
            , 'display' => 'STAR EMPTY'
        )
        , self::BTS_ICON_USER => array(
            'value' => self::BTS_ICON_USER
            , 'display' => 'USER'
        )
        , self::BTS_ICON_FILM => array(
            'value' => self::BTS_ICON_FILM
            , 'display' => 'FILM'
        )
        , self::BTS_ICON_TH_LARGE => array(
            'value' => self::BTS_ICON_TH_LARGE
            , 'display' => 'TH LARGE'
        )
        , self::BTS_ICON_TH => array(
            'value' => self::BTS_ICON_TH
            , 'display' => 'TH'
        )
        , self::BTS_ICON_TH_LIST => array(
            'value' => self::BTS_ICON_TH_LIST
            , 'display' => 'TH LIST'
        )
        , self::BTS_ICON_OK => array(
            'value' => self::BTS_ICON_OK
            , 'display' => 'OK'
        )
        , self::BTS_ICON_REMOVE => array(
            'value' => self::BTS_ICON_REMOVE
            , 'display' => 'REMOVE'
        )
        , self::BTS_ICON_ZOOM_IN => array(
            'value' => self::BTS_ICON_ZOOM_IN
            , 'display' => 'ZOOM IN'
        )
        , self::BTS_ICON_ZOOM_OUT => array(
            'value' => self::BTS_ICON_ZOOM_OUT
            , 'display' => 'ZOOM OUT'
        )
        , self::BTS_ICON_OFF => array(
            'value' => self::BTS_ICON_OFF
            , 'display' => 'OFF'
        )
        , self::BTS_ICON_SIGNAL => array(
            'value' => self::BTS_ICON_SIGNAL
            , 'display' => 'SIGNAL'
        )
        , self::BTS_ICON_COG => array(
            'value' => self::BTS_ICON_COG
            , 'display' => 'COG'
        )
        , self::BTS_ICON_TRASH => array(
            'value' => self::BTS_ICON_TRASH
            , 'display' => 'TRASH'
        )
        , self::BTS_ICON_HOME => array(
            'value' => self::BTS_ICON_HOME
            , 'display' => 'HOME'
        )
        , self::BTS_ICON_FILE => array(
            'value' => self::BTS_ICON_FILE
            , 'display' => 'FILE'
        )
        , self::BTS_ICON_TIME => array(
            'value' => self::BTS_ICON_TIME
            , 'display' => 'TIME'
        )
        , self::BTS_ICON_ROAD => array(
            'value' => self::BTS_ICON_ROAD
            , 'display' => 'ROAD'
        )
        , self::BTS_ICON_DOWNLOAD_ALT => array(
            'value' => self::BTS_ICON_DOWNLOAD_ALT
            , 'display' => 'DOWNLOAD ALT'
        )
        , self::BTS_ICON_DOWNLOAD => array(
            'value' => self::BTS_ICON_DOWNLOAD
            , 'display' => 'DOWNLOAD'
        )
        , self::BTS_ICON_UPLOAD => array(
            'value' => self::BTS_ICON_UPLOAD
            , 'display' => 'UPLOAD'
        )
        , self::BTS_ICON_INBOX => array(
            'value' => self::BTS_ICON_INBOX
            , 'display' => 'INBOX'
        )
        , self::BTS_ICON_PLAY_CIRCLE => array(
            'value' => self::BTS_ICON_PLAY_CIRCLE
            , 'display' => 'PLAY CIRCLE'
        )
        , self::BTS_ICON_REPEAT => array(
            'value' => self::BTS_ICON_REPEAT
            , 'display' => 'REPEAT'
        )
        , self::BTS_ICON_REFRESH => array(
            'value' => self::BTS_ICON_REFRESH
            , 'display' => 'REFRESH'
        )
        , self::BTS_ICON_LIST_ALT => array(
            'value' => self::BTS_ICON_LIST_ALT
            , 'display' => 'LIST ALT'
        )
        , self::BTS_ICON_LOCK => array(
            'value' => self::BTS_ICON_LOCK
            , 'display' => 'LOCK'
        )
        , self::BTS_ICON_FLAG => array(
            'value' => self::BTS_ICON_FLAG
            , 'display' => 'FLAG'
        )
        , self::BTS_ICON_HEADPHONES => array(
            'value' => self::BTS_ICON_HEADPHONES
            , 'display' => 'HEADPHONES'
        )
        , self::BTS_ICON_VOLUME_OFF => array(
            'value' => self::BTS_ICON_VOLUME_OFF
            , 'display' => 'VOLUME OFF'
        )
        , self::BTS_ICON_VOLUME_DOWN => array(
            'value' => self::BTS_ICON_VOLUME_DOWN
            , 'display' => 'VOLUME DOWN'
        )
        , self::BTS_ICON_VOLUME_UP => array(
            'value' => self::BTS_ICON_VOLUME_UP
            , 'display' => 'VOLUME UP'
        )
        , self::BTS_ICON_QRCODE => array(
            'value' => self::BTS_ICON_QRCODE
            , 'display' => 'QRCODE'
        )
        , self::BTS_ICON_BARCODE => array(
            'value' => self::BTS_ICON_BARCODE
            , 'display' => 'BARCODE'
        )
        , self::BTS_ICON_TAG => array(
            'value' => self::BTS_ICON_TAG
            , 'display' => 'TAG'
        )
        , self::BTS_ICON_TAGS => array(
            'value' => self::BTS_ICON_TAGS
            , 'display' => 'TAGS'
        )
        , self::BTS_ICON_BOOK => array(
            'value' => self::BTS_ICON_BOOK
            , 'display' => 'BOOK'
        )
        , self::BTS_ICON_BOOKMARK => array(
            'value' => self::BTS_ICON_BOOKMARK
            , 'display' => 'BOOKMARK'
        )
        , self::BTS_ICON_PRINT => array(
            'value' => self::BTS_ICON_PRINT
            , 'display' => 'PRINT'
        )
        , self::BTS_ICON_CAMERA => array(
            'value' => self::BTS_ICON_CAMERA
            , 'display' => 'CAMERA'
        )
        , self::BTS_ICON_FONT => array(
            'value' => self::BTS_ICON_FONT
            , 'display' => 'FONT'
        )
        , self::BTS_ICON_BOLD => array(
            'value' => self::BTS_ICON_BOLD
            , 'display' => 'BOLD'
        )
        , self::BTS_ICON_ITALIC => array(
            'value' => self::BTS_ICON_ITALIC
            , 'display' => 'ITALIC'
        )
        , self::BTS_ICON_TEXT_HEIGHT => array(
            'value' => self::BTS_ICON_TEXT_HEIGHT
            , 'display' => 'TEXT HEIGHT'
        )
        , self::BTS_ICON_TEXT_WIDTH => array(
            'value' => self::BTS_ICON_TEXT_WIDTH
            , 'display' => 'TEXT WIDTH'
        )
        , self::BTS_ICON_ALIGN_LEFT => array(
            'value' => self::BTS_ICON_ALIGN_LEFT
            , 'display' => 'ALIGN LEFT'
        )
        , self::BTS_ICON_ALIGN_CENTER => array(
            'value' => self::BTS_ICON_ALIGN_CENTER
            , 'display' => 'ALIGN CENTER'
        )
        , self::BTS_ICON_ALIGN_RIGHT => array(
            'value' => self::BTS_ICON_ALIGN_RIGHT
            , 'display' => 'ALIGN RIGHT'
        )
        , self::BTS_ICON_ALIGN_JUSTIFY => array(
            'value' => self::BTS_ICON_ALIGN_JUSTIFY
            , 'display' => 'ALIGN JUSTIFY'
        )
        , self::BTS_ICON_LIST => array(
            'value' => self::BTS_ICON_LIST
            , 'display' => 'LIST'
        )
        , self::BTS_ICON_INDENT_LEFT => array(
            'value' => self::BTS_ICON_INDENT_LEFT
            , 'display' => 'INDENT LEFT'
        )
        , self::BTS_ICON_INDENT_RIGHT => array(
            'value' => self::BTS_ICON_INDENT_RIGHT
            , 'display' => 'INDENT RIGHT'
        )
        , self::BTS_ICON_FACETIME_VIDEO => array(
            'value' => self::BTS_ICON_FACETIME_VIDEO
            , 'display' => 'FACETIME VIDEO'
        )
        , self::BTS_ICON_PICTURE => array(
            'value' => self::BTS_ICON_PICTURE
            , 'display' => 'PICTURE'
        )
        , self::BTS_ICON_PENCIL => array(
            'value' => self::BTS_ICON_PENCIL
            , 'display' => 'PENCIL'
        )
        , self::BTS_ICON_MAP_MARKER => array(
            'value' => self::BTS_ICON_MAP_MARKER
            , 'display' => 'MAP MARKER'
        )
        , self::BTS_ICON_ADJUST => array(
            'value' => self::BTS_ICON_ADJUST
            , 'display' => 'ADJUST'
        )
        , self::BTS_ICON_TINT => array(
            'value' => self::BTS_ICON_TINT
            , 'display' => 'TINT'
        )
        , self::BTS_ICON_EDIT => array(
            'value' => self::BTS_ICON_EDIT
            , 'display' => 'EDIT'
        )
        , self::BTS_ICON_SHARE => array(
            'value' => self::BTS_ICON_SHARE
            , 'display' => 'SHARE'
        )
        , self::BTS_ICON_CHECK => array(
            'value' => self::BTS_ICON_CHECK
            , 'display' => 'CHECK'
        )
        , self::BTS_ICON_MOVE => array(
            'value' => self::BTS_ICON_MOVE
            , 'display' => 'MOVE'
        )
        , self::BTS_ICON_STEP_BACKWARD => array(
            'value' => self::BTS_ICON_STEP_BACKWARD
            , 'display' => 'STEP BACKWARD'
        )
        , self::BTS_ICON_FAST_BACKWARD => array(
            'value' => self::BTS_ICON_FAST_BACKWARD
            , 'display' => 'FAST BACKWARD'
        )
        , self::BTS_ICON_BACKWARD => array(
            'value' => self::BTS_ICON_BACKWARD
            , 'display' => 'BACKWARD'
        )
        , self::BTS_ICON_PLAY => array(
            'value' => self::BTS_ICON_PLAY
            , 'display' => 'PLAY'
        )
        , self::BTS_ICON_PAUSE => array(
            'value' => self::BTS_ICON_PAUSE
            , 'display' => 'PAUSE'
        )
        , self::BTS_ICON_STOP => array(
            'value' => self::BTS_ICON_STOP
            , 'display' => 'STOP'
        )
        , self::BTS_ICON_FORWARD => array(
            'value' => self::BTS_ICON_FORWARD
            , 'display' => 'FORWARD'
        )
        , self::BTS_ICON_FAST_FORWARD => array(
            'value' => self::BTS_ICON_FAST_FORWARD
            , 'display' => 'FAST FORWARD'
        )
        , self::BTS_ICON_STEP_FORWARD => array(
            'value' => self::BTS_ICON_STEP_FORWARD
            , 'display' => 'STEP FORWARD'
        )
        , self::BTS_ICON_EJECT => array(
            'value' => self::BTS_ICON_EJECT
            , 'display' => 'EJECT'
        )
        , self::BTS_ICON_CHEVRON_LEFT => array(
            'value' => self::BTS_ICON_CHEVRON_LEFT
            , 'display' => 'CHEVRON LEFT'
        )
        , self::BTS_ICON_CHEVRON_RIGHT => array(
            'value' => self::BTS_ICON_CHEVRON_RIGHT
            , 'display' => 'CHEVRON RIGHT'
        )
        , self::BTS_ICON_PLUS_SIGN => array(
            'value' => self::BTS_ICON_PLUS_SIGN
            , 'display' => 'PLUS SIGN'
        )
        , self::BTS_ICON_MINUS_SIGN => array(
            'value' => self::BTS_ICON_MINUS_SIGN
            , 'display' => 'MINUS SIGN'
        )
        , self::BTS_ICON_REMOVE_SIGN => array(
            'value' => self::BTS_ICON_REMOVE_SIGN
            , 'display' => 'REMOVE SIGN'
        )
        , self::BTS_ICON_OK_SIGN => array(
            'value' => self::BTS_ICON_OK_SIGN
            , 'display' => 'OK SIGN'
        )
        , self::BTS_ICON_QUESTION_SIGN => array(
            'value' => self::BTS_ICON_QUESTION_SIGN
            , 'display' => 'QUESTION SIGN'
        )
        , self::BTS_ICON_INFO_SIGN => array(
            'value' => self::BTS_ICON_INFO_SIGN
            , 'display' => 'INFO SIGN'
        )
        , self::BTS_ICON_SCREENSHOT => array(
            'value' => self::BTS_ICON_SCREENSHOT
            , 'display' => 'SCREENSHOT'
        )
        , self::BTS_ICON_REMOVE_CIRCLE => array(
            'value' => self::BTS_ICON_REMOVE_CIRCLE
            , 'display' => 'REMOVE CIRCLE'
        )
        , self::BTS_ICON_OK_CIRCLE => array(
            'value' => self::BTS_ICON_OK_CIRCLE
            , 'display' => 'OK CIRCLE'
        )
        , self::BTS_ICON_BAN_CIRCLE => array(
            'value' => self::BTS_ICON_BAN_CIRCLE
            , 'display' => 'BAN CIRCLE'
        )
        , self::BTS_ICON_ARROW_LEFT => array(
            'value' => self::BTS_ICON_ARROW_LEFT
            , 'display' => 'ARROW LEFT'
        )
        , self::BTS_ICON_ARROW_RIGHT => array(
            'value' => self::BTS_ICON_ARROW_RIGHT
            , 'display' => 'ARROW RIGHT'
        )
        , self::BTS_ICON_ARROW_UP => array(
            'value' => self::BTS_ICON_ARROW_UP
            , 'display' => 'ARROW UP'
        )
        , self::BTS_ICON_ARROW_DOWN => array(
            'value' => self::BTS_ICON_ARROW_DOWN
            , 'display' => 'ARROW DOWN'
        )
        , self::BTS_ICON_SHARE_ALT => array(
            'value' => self::BTS_ICON_SHARE_ALT
            , 'display' => 'SHARE ALT'
        )
        , self::BTS_ICON_RESIZE_FULL => array(
            'value' => self::BTS_ICON_RESIZE_FULL
            , 'display' => 'RESIZE FULL'
        )
        , self::BTS_ICON_RESIZE_SMALL => array(
            'value' => self::BTS_ICON_RESIZE_SMALL
            , 'display' => 'RESIZE SMALL'
        )
        , self::BTS_ICON_PLUS => array(
            'value' => self::BTS_ICON_PLUS
            , 'display' => 'PLUS'
        )
        , self::BTS_ICON_MINUS => array(
            'value' => self::BTS_ICON_MINUS
            , 'display' => 'MINUS'
        )
        , self::BTS_ICON_ASTERISK => array(
            'value' => self::BTS_ICON_ASTERISK
            , 'display' => 'ASTERISK'
        )
        , self::BTS_ICON_EXCLAMATION_SIGN => array(
            'value' => self::BTS_ICON_EXCLAMATION_SIGN
            , 'display' => 'EXCLAMATION SIGN'
        )
        , self::BTS_ICON_GIFT => array(
            'value' => self::BTS_ICON_GIFT
            , 'display' => 'GIFT'
        )
        , self::BTS_ICON_LEAF => array(
            'value' => self::BTS_ICON_LEAF
            , 'display' => 'LEAF'
        )
        , self::BTS_ICON_FIRE => array(
            'value' => self::BTS_ICON_FIRE
            , 'display' => 'FIRE'
        )
        , self::BTS_ICON_EYE_OPEN => array(
            'value' => self::BTS_ICON_EYE_OPEN
            , 'display' => 'EYE OPEN'
        )
        , self::BTS_ICON_EYE_CLOSE => array(
            'value' => self::BTS_ICON_EYE_CLOSE
            , 'display' => 'EYE CLOSE'
        )
        , self::BTS_ICON_WARNING_SIGN => array(
            'value' => self::BTS_ICON_WARNING_SIGN
            , 'display' => 'WARNING SIGN'
        )
        , self::BTS_ICON_PLANE => array(
            'value' => self::BTS_ICON_PLANE
            , 'display' => 'PLANE'
        )
        , self::BTS_ICON_CALENDAR => array(
            'value' => self::BTS_ICON_CALENDAR
            , 'display' => 'CALENDAR'
        )
        , self::BTS_ICON_RANDOM => array(
            'value' => self::BTS_ICON_RANDOM
            , 'display' => 'RANDOM'
        )
        , self::BTS_ICON_COMMENT => array(
            'value' => self::BTS_ICON_COMMENT
            , 'display' => 'COMMENT'
        )
        , self::BTS_ICON_MAGNET => array(
            'value' => self::BTS_ICON_MAGNET
            , 'display' => 'MAGNET'
        )
        , self::BTS_ICON_CHEVRON_UP => array(
            'value' => self::BTS_ICON_CHEVRON_UP
            , 'display' => 'CHEVRON UP'
        )
        , self::BTS_ICON_CHEVRON_DOWN => array(
            'value' => self::BTS_ICON_CHEVRON_DOWN
            , 'display' => 'CHEVRON DOWN'
        )
        , self::BTS_ICON_RETWEET => array(
            'value' => self::BTS_ICON_RETWEET
            , 'display' => 'RETWEET'
        )
        , self::BTS_ICON_SHOPPING_CART => array(
            'value' => self::BTS_ICON_SHOPPING_CART
            , 'display' => 'SHOPPING CART'
        )
        , self::BTS_ICON_FOLDER_CLOSE => array(
            'value' => self::BTS_ICON_FOLDER_CLOSE
            , 'display' => 'FOLDER CLOSE'
        )
        , self::BTS_ICON_FOLDER_OPEN => array(
            'value' => self::BTS_ICON_FOLDER_OPEN
            , 'display' => 'FOLDER OPEN'
        )
        , self::BTS_ICON_RESIZE_VERTICAL => array(
            'value' => self::BTS_ICON_RESIZE_VERTICAL
            , 'display' => 'RESIZE VERTICAL'
        )
        , self::BTS_ICON_RESIZE_HORIZONTAL => array(
            'value' => self::BTS_ICON_RESIZE_HORIZONTAL
            , 'display' => 'RESIZE HORIZONTAL'
        )
        , self::BTS_ICON_HDD => array(
            'value' => self::BTS_ICON_HDD
            , 'display' => 'HDD'
        )
        , self::BTS_ICON_BULLHORN => array(
            'value' => self::BTS_ICON_BULLHORN
            , 'display' => 'BULLHORN'
        )
        , self::BTS_ICON_BELL => array(
            'value' => self::BTS_ICON_BELL
            , 'display' => 'BELL'
        )
        , self::BTS_ICON_CERTIFICATE => array(
            'value' => self::BTS_ICON_CERTIFICATE
            , 'display' => 'CERTIFICATE'
        )
        , self::BTS_ICON_THUMBS_UP => array(
            'value' => self::BTS_ICON_THUMBS_UP
            , 'display' => 'THUMBS UP'
        )
        , self::BTS_ICON_THUMBS_DOWN => array(
            'value' => self::BTS_ICON_THUMBS_DOWN
            , 'display' => 'THUMBS DOWN'
        )
        , self::BTS_ICON_HAND_RIGHT => array(
            'value' => self::BTS_ICON_HAND_RIGHT
            , 'display' => 'HAND RIGHT'
        )
        , self::BTS_ICON_HAND_LEFT => array(
            'value' => self::BTS_ICON_HAND_LEFT
            , 'display' => 'HAND LEFT'
        )
        , self::BTS_ICON_HAND_UP => array(
            'value' => self::BTS_ICON_HAND_UP
            , 'display' => 'HAND UP'
        )
        , self::BTS_ICON_HAND_DOWN => array(
            'value' => self::BTS_ICON_HAND_DOWN
            , 'display' => 'HAND DOWN'
        )
        , self::BTS_ICON_CIRCLE_ARROW_RIGHT => array(
            'value' => self::BTS_ICON_CIRCLE_ARROW_RIGHT
            , 'display' => 'CIRCLE ARROW RIGHT'
        )
        , self::BTS_ICON_CIRCLE_ARROW_LEFT => array(
            'value' => self::BTS_ICON_CIRCLE_ARROW_LEFT
            , 'display' => 'CIRCLE ARROW LEFT'
        )
        , self::BTS_ICON_CIRCLE_ARROW_UP => array(
            'value' => self::BTS_ICON_CIRCLE_ARROW_UP
            , 'display' => 'CIRCLE ARROW UP'
        )
        , self::BTS_ICON_CIRCLE_ARROW_DOWN => array(
            'value' => self::BTS_ICON_CIRCLE_ARROW_DOWN
            , 'display' => 'CIRCLE ARROW DOWN'
        )
        , self::BTS_ICON_GLOBE => array(
            'value' => self::BTS_ICON_GLOBE
            , 'display' => 'GLOBE'
        )
        , self::BTS_ICON_WRENCH => array(
            'value' => self::BTS_ICON_WRENCH
            , 'display' => 'WRENCH'
        )
        , self::BTS_ICON_TASKS => array(
            'value' => self::BTS_ICON_TASKS
            , 'display' => 'TASKS'
        )
        , self::BTS_ICON_FILTER => array(
            'value' => self::BTS_ICON_FILTER
            , 'display' => 'FILTER'
        )
        , self::BTS_ICON_BRIEFCASE => array(
            'value' => self::BTS_ICON_BRIEFCASE
            , 'display' => 'BRIEFCASE'
        )
        , self::BTS_ICON_FULLSCREEN => array(
            'value' => self::BTS_ICON_FULLSCREEN
            , 'display' => 'FULLSCREEN'
        )
    );
    public $a_bts_date_format = array(
        self::BTS_COUNTRY_US => array(
            self::BTS_FORMAT_DATE_VALUE => self::BTS_COUNTRY_US,
            self::BTS_FORMAT_DATE_JQUERY => 'mm-dd-yyyy',
            self::BTS_FORMAT_DATE_SQL => '%m-%d-%Y',
            self::BTS_FORMAT_TIME_SQL => '%H:%i:%s',
            self::BTS_FORMAT_DATE_PHP => 'm-d-Y',
            self::BTS_FORMAT_TIME_SQL_RAW => '%H:%i:%s',
            self::BTS_FORMAT_TIME_SQL_LIMIT => '23:59:59',
            self::BTS_FORMAT_DATE_TIME_SQL => '%m-%d-%Y %H:%i',
            self::BTS_FORMAT_TIME_JQUERY_ENTRY_24_HOURS => self::BTS_STATUS_ACTIVE,
            self::BTS_FORMAT_TIME_PHP_INPUT => 'H:i',
            self::BTS_FORMAT_TIME_SQL_OUTPUT => '%H:%i'
        ),
        self::BTS_COUNTRY_MX => array(
            self::BTS_FORMAT_DATE_VALUE => self::BTS_COUNTRY_MX,
            self::BTS_FORMAT_DATE_JQUERY => 'dd/mm/yyyy',
            self::BTS_FORMAT_DATE_SQL => '%d/%m/%Y',
            self::BTS_FORMAT_TIME_SQL => '%h:%i %p',
            self::BTS_FORMAT_DATE_PHP => 'd/m/Y',
            self::BTS_FORMAT_TIME_SQL_RAW => '%H:%i:%s',
            self::BTS_FORMAT_TIME_SQL_LIMIT => '23:59:59',
            self::BTS_FORMAT_DATE_TIME_SQL => '%d/%m/%Y %h:%i %p',
            self::BTS_FORMAT_TIME_JQUERY_ENTRY_24_HOURS => self::BTS_STATUS_ACTIVE,
            self::BTS_FORMAT_TIME_PHP_INPUT => 'h:i A',
            self::BTS_FORMAT_TIME_SQL_OUTPUT => '%h:%i %p'
        ),
        self::BTS_COUNTRY_PE => array(
            self::BTS_FORMAT_DATE_VALUE => self::BTS_COUNTRY_PE,
            self::BTS_FORMAT_DATE_JQUERY => 'dd/mm/yyyy',
            self::BTS_FORMAT_DATE_SQL => '%d/%m/%Y',
            self::BTS_FORMAT_TIME_SQL => '%h:%i %p',
            self::BTS_FORMAT_DATE_PHP => 'd/m/Y',
            self::BTS_FORMAT_TIME_SQL_RAW => '%H:%i:%s',
            self::BTS_FORMAT_TIME_SQL_LIMIT => '23:59:59',
            self::BTS_FORMAT_DATE_TIME_SQL => '%d/%m/%Y %h:%i %p',
            self::BTS_FORMAT_TIME_JQUERY_ENTRY_24_HOURS => self::BTS_STATUS_DEACTIVE,
            self::BTS_FORMAT_TIME_PHP_INPUT => 'h:i A',
            self::BTS_FORMAT_TIME_SQL_OUTPUT => '%h:%i %p'
        ),
    );
    public $a_bts_option_component = array(
        self::BTS_OPTION_COMPONENT_GENERAL => array(
            'display' => 'General'
            , 'value' => self::BTS_OPTION_COMPONENT_GENERAL
            , 'short' => 'GEN'
        ),
    );
    public $a_bts_option_type = array(
        self::BTS_OPTION_TYPE_BOOLEAN => array(
            'display' => 'Boolean'
            , 'value' => self::BTS_OPTION_TYPE_BOOLEAN
            , 'short' => 'BOO'
        ),
        self::BTS_OPTION_TYPE_STRING => array(
            'display' => 'String'
            , 'value' => self::BTS_OPTION_TYPE_STRING
            , 'short' => 'STR'
        ),
        self::BTS_OPTION_TYPE_COLOR => array(
            'display' => 'Color'
            , 'value' => self::BTS_OPTION_TYPE_COLOR
            , 'short' => 'COL'
        ),
        self::BTS_OPTION_TYPE_INTEGER => array(
            'display' => 'Entero'
            , 'value' => self::BTS_OPTION_TYPE_INTEGER
            , 'short' => 'INT'
        ),
        self::BTS_OPTION_TYPE_ARRAY => array(
            'display' => 'Array'
            , 'value' => self::BTS_OPTION_TYPE_ARRAY
            , 'short' => 'ARR'
        ),
    );
    public $a_option_type = array();
    public $a_option_component = array();
    public $a_modules = array();
    public $BTS_CONFIGURATION_PARAMETERS = array(
        self::BTS_SYS_CONFIG_INSTANCE => array(
            'display' => 'instancia',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_INSTANCE
        )
        , self::BTS_SYS_CONFIG_SITE_NAME => array(
            'display' => 'Nombre Sitio',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SITE_NAME
        )
        , self::BTS_SYS_CONFIG_SERVER => array(
            'display' => 'server',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SERVER
        )
        , self::BTS_SYS_CONFIG_PORT => array(
            'display' => 'port',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PORT
        )
        , self::BTS_SYS_CONFIG_DB_NAME => array(
            'display' => 'db_name',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_DB_NAME
        )
        , self::BTS_SYS_CONFIG_DB_USER => array(
            'display' => 'db_user',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_DB_USER
        )
        , self::BTS_SYS_CONFIG_DB_PASSWORD => array(
            'display' => 'db_password',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_DB_PASSWORD
        )
        , self::BTS_SYS_CONFIG_SKIN => array(
            'display' => 'skin',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SKIN
        )
        , self::BTS_SYS_CONFIG_WEBSITE => array(
            'display' => 'website',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_WEBSITE
        )
        , self::BTS_SYS_CONFIG_TIME_ZONE => array(
            'display' => 'time_zone',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_TIME_ZONE
        )
        , self::BTS_SYS_CONFIG_DEFAULT_INDEX => array(
            'display' => 'default_index',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_DEFAULT_INDEX
        )
        , self::BTS_SYS_CONFIG_EMAIL => array(
            'display' => 'email',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_EMAIL
        )
        , self::BTS_SYS_CONFIG_CONTACT_EMAIL => array(
            'display' => 'contact_email',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_EMAIL
        )
        , self::BTS_SYS_CONFIG_CONTACT_COMPANY => array(
            'display' => 'contact_company',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_COMPANY
        )
        , self::BTS_SYS_CONFIG_CONTACT_PHONE => array(
            'display' => 'contact_phone',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_PHONE
        )
        , self::BTS_SYS_CONFIG_CONTACT_ADDRESS => array(
            'display' => 'contact_address',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_ADDRESS
        )
        , self::BTS_SYS_CONFIG_SESSION_NATIVE_LIFE_TIME => array(
            'display' => 'session_native_life_time',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SESSION_NATIVE_LIFE_TIME
        )
        , self::BTS_SYS_CONFIG_SESSION_DB_LIFE_TIME => array(
            'display' => 'session_db_life_time',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SESSION_DB_LIFE_TIME
        )
        , self::BTS_SYS_CONFIG_PAYPAL_USER => array(
            'display' => 'paypal_user',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_USER
        )
        , self::BTS_SYS_CONFIG_PAYPAL_PASSWORD => array(
            'display' => 'paypal_password',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_PASSWORD
        )
        , self::BTS_SYS_CONFIG_PAYPAL_SIGNATURE => array(
            'display' => 'paypal_signature',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_SIGNATURE
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_EMAIL => array(
            'display' => 'paypal_merchant_email',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_EMAIL
        )
        , self::BTS_SYS_CONFIG_PAYPAL_ENVIRONMENT => array(
            'display' => 'paypal_environment',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_ENVIRONMENT
        )
        , self::BTS_SYS_CONFIG_ENVIRONMENT => array(
            'display' => 'environment',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_ENVIRONMENT
        )
        , self::BTS_SYS_CONFIG_KEY_SYSTEM_CUSTOMER => array(
            'display' => 'key_system_customer',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_KEY_SYSTEM_CUSTOMER
        )
        , self::BTS_SYS_CONFIG_LANGUAGE => array(
            'display' => 'language',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_LANGUAGE
        )
        , self::BTS_SYS_CONFIG_SESSION_USER_LIFE_TIME => array(
            'display' => 'session_user_life_time',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_SESSION_USER_LIFE_TIME
        )
        , self::BTS_SYS_CONFIG_PAYPAL_GATEWAY => array(
            'display' => 'paypal_gateway',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_GATEWAY
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PARTNER => array(
            'display' => 'paypal_merchant_partner',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PARTNER
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_VENDOR => array(
            'display' => 'paypal_merchant_vendor',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_VENDOR
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_USER => array(
            'display' => 'paypal_merchant_user',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_USER
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PASSWORD => array(
            'display' => 'paypal_merchant_password',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PASSWORD
        )
        , self::BTS_SYS_CONFIG_PAYPAL_METHOD => array(
            'display' => 'paypal_method',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_METHOD
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_USER => array(
            'display' => 'paypal_merchant_web_user',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_USER
        )
        , self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_PASSWORD => array(
            'display' => 'paypal_merchant_web_password',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_PASSWORD
        )
        , self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM => array(
            'display' => 'contact_email_from',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM
        )
        , self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM_NAME => array(
            'display' => 'contact_email_from_name',
            'unique_root_option' => self::BTS_STATUS_ACTIVE,
            'name' => self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM_NAME
        )
    );
    public $HD_PRIORIDADES =
            array(
        'Urgente' => array(
            'display' => 'Urgente',
            'value' => self::BTS_HD_PRIORY_URG,
            'timeResponse' => 1,
            'timeResolution' => 1)
        ,
        'Alto' => array(
            'display' => 'Alto',
            'value' => self::BTS_HD_PRIORY_ALT,
            'timeResponse' => 2,
            'timeResolution' => 2
        ),
        'Medio' => array(
            'display' => 'Medio',
            'value' => self::BTS_HD_PRIORY_MED,
            'timeResponse' => 3,
            'timeResolution' => 3
        ),
        'Bajo' => array(
            'display' => 'Bajo',
            'value' => self::BTS_HD_PRIORY_BAJ,
            'timeResponse' => 4,
            'timeResolution' => 4
        )
    );
    public $HD_STATUST =
            array(
        'En espera' => array(
            'display' => 'En espera',
            'value' => self::BTS_HD_STATUST_ESP,
            'timeResponse' => 1,
            'timeResolution' => 1)
        ,
        'En proceso' => array(
            'display' => 'En proceso',
            'value' => self::BTS_HD_PRIORY_ALT,
            'timeResponse' => 2,
            'timeResolution' => 2
        ),
        'Completado' => array(
            'display' => 'Completado',
            'value' => self::BTS_HD_STATUST_COM,
            'timeResponse' => 3,
            'timeResolution' => 3
        ),
        'Cancelado' => array(
            'display' => 'Cancelado',
            'value' => self::BTS_HD_STATUST_CAN,
            'timeResponse' => 4,
            'timeResolution' => 4
        )
    );
    public $HD_NIVELES =
            array(
        'Nivel 1' => array(
            'display' => 'Nivel 1',
            'value' => self::BTS_HD_NIVEL_1
        ),
        'Nivel 2' => array(
            'display' => 'Nivel 2',
            'value' => self::BTS_HD_NIVEL_2
        ),
        'Nivel 3' => array(
            'display' => 'Nivel 3',
            'value' => self::BTS_HD_NIVEL_3
        ),
        'Nivel 4' => array(
            'display' => 'Nivel 4',
            'value' => self::BTS_HD_NIVEL_4
        )
    );
    public $HD_TYPEREQUEST =
            array(
        'Bugs' => array(
            'display' => 'Bugs',
            'value' => 'bugs'
        ),
        'Desarrollo' => array(
            'display' => 'Desarrollo',
            'value' => 'desarrollo'
        ),
        'SoporteTecnico' => array(
            'display' => 'Soporte Tecnico',
            'value' => 'soportetecnico'
        ),
        'WebSite' => array(
            'display' => 'Web Site',
            'value' => 'website'
        ),
        'Urgente' => array(
            'display' => 'Urgente',
            'value' => 'urgente'
        ),
        'Mercadeo' => array(
            'display' => 'Mercado',
            'value' => 'mercado'
        ),
        'Reportes' => array(
            'display' => 'Reportes',
            'value' => 'reportes'
        ),
        'Consultas' => array(
            'display' => 'Consultas',
            'value' => 'consultas'
        ),
        'Mejoras' => array(
            'display' => 'Mejoras',
            'value' => 'mejoras'
        ),
        'Contabilidad' => array(
            'display' => 'Alto',
            'value' => 'urgente'
        ),
        'CallCenter' => array(
            'display' => 'Call Center',
            'value' => 'callCenter'
        ),
        'Migracion' => array(
            'display' => 'Migracion',
            'value' => 'migracion'
        )
    );

    protected function imports($o_object) {
        // the new object to import
        $o_new_import = new $o_object();
        // the name of the new object (class name)
        $s_import_name = get_class($o_new_import);
        // the new functions to import
        $a_import_functions = get_class_methods($o_new_import);

        // add the object to the registry
        array_push($this->a_imported_class, array($s_import_name, $o_new_import));

        // add teh methods to the registry
        foreach ($a_import_functions as $s_key => $s_function_name) {
            $this->a_imported_functions[$s_function_name] = &$o_new_import;
        }
    }

    public function __call($s_method, $a_args = array()) {
        if (array_key_exists($s_method, $this->a_imported_functions)) {
            return call_user_func_array(array($this->a_imported_functions[$s_method], $s_method), $a_args);
        }

        throw new Exception('Call to undefined method/class function: ' . $s_method);
    }

    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        if (!$this->request->is_ajax()) {
            $this->fnSYSLoadClientLanguage();
            $a_interface = Kohana::$config->load('uInterface');
            foreach ($a_interface as $s_className) {
                $this->imports(get_class($s_className));
            }
            $a_js_project_versions = Kohana::$config->load('js_file_version');
            if (isset($a_js_project_versions[Kohana::$environment][self::BTS_BOOTSTRAP_VERSION]))
                self::$BOOTSTRAP_VERSION = $a_js_project_versions[Kohana::$environment][self::BTS_BOOTSTRAP_VERSION];
            if (isset($a_js_project_versions[Kohana::$environment][self::BTS_JQUERY_VERSION]))
                self::$JQUERY_VERSION = $a_js_project_versions[Kohana::$environment][self::BTS_JQUERY_VERSION];
            if (isset($a_js_project_versions[Kohana::$environment][self::BTS_JQUERY_UI_VERSION]))
                self::$JQUERY_UI_VERSION = $a_js_project_versions[Kohana::$environment][self::BTS_JQUERY_UI_VERSION];

            $a_jsonp_callback = Kohana::$config->load('jsonp_callback');
            if (isset($a_jsonp_callback['jsonp_callback']))
                self::$JSONP_CALLBACK = $a_jsonp_callback['jsonp_callback'];
            if (isset($a_jsonp_callback['ajax_data_type']))
                self::$AJAX_DATA_TYPE = $a_jsonp_callback['ajax_data_type'];
        }
    }

    public function before() {
        parent::before();
        if (!$this->request->is_ajax()) {
            $this->template->SYSTEM_BODY = '';
            $this->template->SYSTEM_JSONP_CALL_BACK = self::$JSONP_CALLBACK;
            $this->template->SYSTEM_AJAX_DATA_TYPE = self::$AJAX_DATA_TYPE;
            $config = $this->fnSYSGetConfig(NULL, TRUE);
            $this->template->SYSTEM_CONFIG = $config;
            $this->template->SYSTEM_TITLE = $config->get(self::BTS_SYS_CONFIG_SITE_NAME);
            $this->template->SYSTEM_DESCRIPTION = 'Description';
            $this->template->SYSTEM_SKIN = $config->get(self::BTS_SYS_CONFIG_SKIN);
            $this->template->SYSTEM_TIME_REFRESH_MESSAGES = 60000;
            $a_sys_lang = explode('-', $this->fnSYSCurrentClientLanguage());
            $this->template->SYSTEM_LANGUAGE = $a_sys_lang[0];
            if (array_key_exists(self::BTS_COOKIE_USER_PREFERENCES, $_COOKIE)) {
                $u_data = json_decode($_COOKIE[self::BTS_COOKIE_USER_PREFERENCES]);
                $this->template->USER_FULLSCREEN = $u_data->fs;
            } else {
                $this->template->USER_FULLSCREEN = FALSE;
            }
            $this->template->SYSTEM_A_SCRIPTS = $this->a_bts_scripts;
            $this->template->SYSTEM_A_STYLES = $this->a_bts_styles;
            $this->template->SYSTEM_A_PROJ_SCRIPTS = $this->a_proj_scripts;
            $this->template->SYSTEM_A_PROJ_STYLES = $this->a_proj_styles;
            $this->template->BTS_USER_NAME = '';
            $this->template->BTS_USER_FULL_NAME = '';
            $this->template->BTS_SESSION_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_ID);
            $this->template->BTS_SESSION_DATE_FORMAT = $this->fnSYSGetOptionValue(self::BTS_SO_DATE_FORMAT);
            $this->template->BTS_SESSION_LANG = substr(I18n::lang(), 0, 2);
            $this->template->BTS_SESSION_USER_ID = '';
            $this->template->BTS_SESSION_OFFICE_ID = '';
            $this->template->BTS_SESSION_OFFICE_NAME = '';
            $this->template->BTS_SESSION_CITY_ID = '';
            $this->template->BTS_SESSION_CITY_NAME = '';
            $this->template->BTS_GROUP_NAME = '';
            $this->template->BTS_SESSION_GROUP_ID = '';
            $this->template->BTS_SESSION_USER_EMAIL = '';
            $this->template->BTS_SESSION_DEFAULT_HOME = '';
            $this->template->SYSTEM_A_MENU = array();
            $this->template->SYSTEM_A_MENU_ACTIVE = array();
            $this->template->SYSTEM_BREADCRUMBS = '';
            $this->template->BTS_A_ACCESS = array();
            $this->template->BTS_A_SYSTEM_OPTIONS = array();
            $this->template->BTS_OPTION_LANGUAGE = '';
            $this->template->SYSTEM_DEV_EXT_FILE = (Kohana::$environment == Kohana::DEVELOPMENT ? '' : '.min');
        }
        //End
    }

    public static function fnSYSDestroySession() {
        Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->destroy();
    }

    public static function fnSYSDestroyNativeSession() {
        Session::instance(self::KOHANA_SESSION_NAME_NATIVE)->destroy();
    }

    public static function fnSYSGetSessionParameter($i_parameter_id = NULL) {
        return Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get($i_parameter_id);
    }

    public static function fnSYSSetSessionParameter($i_parameter_id = NULL, $s_parameter_value = NULL) {
        Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->set($i_parameter_id, $s_parameter_value);
    }

    public static function fnSYSGetNativeSessionParameter($i_parameter_id = NULL) {
        return Session::instance(self::KOHANA_SESSION_NAME_NATIVE)->get($i_parameter_id);
    }

    public static function fnSYSSetNativeSessionParameter($i_parameter_id = NULL, $s_parameter_value = NULL) {
        Session::instance(self::KOHANA_SESSION_NAME_NATIVE)->set($i_parameter_id, $s_parameter_value);
    }

    public static function fnSYSGetSessionId() {
        return md5(Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->id());
    }

    public static function fnSYSDeleteVariableSession($s_key) {
        return Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->delete($s_key);
    }

    public static function fnSYSDeleteVariableNativeSession($s_key) {
        return Session::instance(self::KOHANA_SESSION_NAME_NATIVE)->delete($s_key);
    }

    public function fnSYSAddStyles($a_styles) {
        $this->template->SYSTEM_A_STYLES = array_merge($this->template->SYSTEM_A_STYLES, $a_styles);
    }

    public function fnSYSAddScripts($a_scripts) {
        $this->template->SYSTEM_A_SCRIPTS = array_merge($this->template->SYSTEM_A_SCRIPTS, $a_scripts);
    }

    public function fnSYSProjAddStyles($a_styles, $a_more_styles = null) {
        if (!empty($a_more_styles)) {
            $this->template->SYSTEM_A_PROJ_STYLES = array_merge($a_styles, $a_more_styles);
        } else {
            $this->template->SYSTEM_A_PROJ_STYLES = $a_styles;
        }
    }

    public function fnSYSProjAddScripts($a_scripts, $a_more_scripts = null) {
        if (!empty($a_more_scripts)) {
            $this->template->SYSTEM_A_PROJ_SCRIPTS = array_merge($a_scripts, $a_more_scripts);
        } else {
            $this->template->SYSTEM_A_PROJ_SCRIPTS = $a_scripts;
        }
    }

    public static function fnSYSGetFilePage($s_page_id, $s_type_file) {
        try {
            $enviroment = Kohana::$environment;
            $status_enviroment = Kohana::DEVELOPMENT;
            $a_media_files = DB::select(
                            DB::expr("CASE 
WHEN bts_file.isCoreFile = 1 THEN CONCAT ('/kernel/',bts_file.fileType,'/',bts_file.fileName, IF($enviroment=$status_enviroment,  '' , '.min'),'.',bts_file.fileType)
ELSE CONCAT ('/',bts_file.fileType,'/',bts_file.fileName,IF($enviroment=$status_enviroment,  '' , '.min'),'.',bts_file.fileType)
 END AS file_name")
                    )
                    ->from('bts_file')
                    ->join('bts_section')
                    ->on('bts_file.idFile', '=', 'bts_section.idFile')
                    ->join('bts_page')
                    ->on('bts_page.idPage', '=', 'bts_section.idPage')
                    ->and_where('bts_page.idPage', '=', $s_page_id)
                    ->and_where('bts_file.fileType', '=', $s_type_file)
                    ->and_where('bts_page.status', '=', 1)
                    ->and_where('bts_file.status', '=', 1)
                    ->and_where('bts_section.status', '=', 1)
                    ->order_by('bts_file.fileType')
                    ->order_by('bts_section.fileOrder')
                    ->execute()
                    ->as_array();
            return $a_media_files;
        } catch (Database_Exception $e) {
            echo $e->getTraceAsString();
        }
    }

    public function fnSYSResponseFormat(
    $a_return
    , $s_format = self::BTS_RESPONSE_TYPE_JSON
    , $a_header_xml = array()
    ) {
        switch (strtoupper($s_format)) {
            case self::BTS_RESPONSE_TYPE_JSON:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'application/json');
                echo json_encode($a_return, true);
                break;
            case self::BTS_RESPONSE_TYPE_JSONP:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'application/json');
                echo HelperJQuery::fnJSONEncode($a_return, true);
                break;
            case self::BTS_RESPONSE_TYPE_JQGRID:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'application/json');
                echo HelperJQuery::fnJSONEncode($a_return, false);
                break;
            case self::BTS_RESPONSE_TYPE_XML:
                $this->auto_render = FALSE;
                $this->response->headers('Content-Type', 'text/xml');
                $o_dom = HelperXML::toXML($a_return, $a_header_xml);
                echo $o_dom->saveXML();
                break;
            case self::BTS_RESPONSE_TYPE_HTML:
                $this->auto_render = FALSE;
                echo $a_return;
                break;
            default:
                break;
        }
    }

    public function fnSYSErrorHandling($o_exc, $a_extra_params = NULL) {
        $this->fnSYSLoadClientLanguage();
        $a_remove = array("\n", "\r\n", "\n\r", "\r");
        $a_params = str_replace($a_remove, '', Debug::dump($_REQUEST));
        if ($a_extra_params != null) {
            $a_remove = array("\n", "\r\n", "\n\r", "\r");
            $a_extra_params = str_replace($a_remove, '', Debug::dump($a_extra_params));
        }
        $a_error = '';
        if ($o_exc->getCode() != self::BTS_CODE_SUCCESS) {

            if (Kohana::$environment == Kohana::DEVELOPMENT) {
                if ($o_exc->getCode() == self::MYSQL_CODE_ER_DUP_ENTRY) {
                    $a_error['eText'] = ("Se ha violado una restricción de base de datos UNIQUE.");
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_ER_ROW_IS_REFERENCED_2) {
                    $a_error['eText'] = __("No es posible eliminar esta información. Ya está siendo utilizada en el sistema");
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_NONE) {
                    $a_error['eText'] = $o_exc->getMessage();
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_ER_DATA_TOO_LONG) {
                    $a_error = __("El valor que intenta ingresar es demasiado largo. Intente un valor mas corto.");  
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_ER_FOREIGN_KEY_CONSTRAINT) {
                    $a_error = __("No se puede Actualizar/Eliminar la solicitud requerida.Vuelva a procesar la solicitud.");    
                } elseif ($o_exc->getCode() == self::BTS_CODE_NO_AUTHORIZED) {
                    $a_error['eText'] = __("Usted no tiene acceso a esta opción");
                } else {
                    $a_error['eCode'] = $o_exc->getCode();
                    $a_error['eEnviroment'] = Kohana::$environment;
                    $a_error['eLine'] = $o_exc->getLine();
                    $a_error['eFile'] = $o_exc->getFile();
                    $a_error['eText'] = $o_exc->getMessage();
                    $a_error['eTrace'] = $o_exc->getTraceAsString();
                    $a_error['rParams'] = $a_params;
                    $a_error['eParams'] = $a_extra_params;
                }
            } else {
                $s_error_log_level = Log::ERROR;
                $s_code_human_error = $o_exc->getCode();
                if (isset(Kohana_Exception::$php_errors[$s_code_human_error])) {
                    $s_code_human_error = Kohana_Exception::$php_errors[$s_code_human_error];
                    if ($s_code_human_error == 'Notice') {
                        $s_error_log_level = 5;
                    } elseif ($s_code_human_error == 'Fatal Error') {
                        $s_error_log_level = 3;
                    } elseif ($s_code_human_error == 'User Error') {
                        $s_error_log_level = 3;
                    } elseif ($s_code_human_error == 'Parse Error') {
                        $s_error_log_level = 3;
                    } elseif ($s_code_human_error == 'Warning') {
                        $s_error_log_level = 4;
                    } elseif ($s_code_human_error == 'User Warning') {
                        $s_error_log_level = 4;
                    } elseif ($s_code_human_error == 'Strict') {
                        $s_error_log_level = 3;
                    } elseif ($s_code_human_error == 'Alert') {
                        $s_error_log_level = 1;
                    } elseif ($s_code_human_error == 'Recoverable Error') {
                        $s_error_log_level = 3;
                    } else {
                        $s_error_log_level = Log::ERROR;
                    }
                } else {
                    $s_error_log_level = Log::ERROR;
                }
                if ($o_exc->getCode() == self::MYSQL_CODE_ER_DUP_ENTRY) {
                    $a_error = __("Se ha violado una restricción de base de datos UNIQUE.");
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_ER_ROW_IS_REFERENCED_2) {
                    $a_error = __("Existen Valores ingresados que no pueden ser borrados.");
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_ER_DATA_TOO_LONG) {
                    $a_error = __("El valor que intenta ingresar es demasiado largo. Intente un valor mas corto.");     
                } elseif ($o_exc->getCode() == self::MYSQL_CODE_NONE) {
                    $a_error = $o_exc->getMessage();
                } else {
                    $s_code = strtoupper(base_convert(self::fnSYSGetSessionParameter(self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID) . self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID)) . rand() . (date('mdHis') . rand()), 10, 36));
                    $s_code = 'EH_' . $s_code;
                    $s_more_message = '<b>Nro Codigo:  ' . $s_code . '</b><br/><br/>';
                    $s_more_message .='<b>Error en: </b> ' . SITE_DOMAIN . '<br/><br/>';
                    if (self::fnSYSGetSessionParameter(self::BTS_SESSION_ID) != NULL) {
                        $s_more_message .= '<b>Id Usuario:</b> ' . Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_ID) . '<br/>';
                        $s_more_message .= '<b>Usuario:</b > ' . Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_NAME) . '<br/>';
                        $s_more_message .= '<b>Nombre Completo: </b > ' . Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_FULL_NAME) . '<br/>';
                        $s_more_message .= '<b>Id Oficina:</b> ' . Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_OFFICE_ID) . '<br/>';
                        $s_more_message .= '<b>oficina:</b> ' . Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_OFFICE_NAME) . '<br/>';
                        $s_more_message .= '<b>URI:</b> ' . $_SERVER['REQUEST_URI'] . '<br/>';
                        $s_more_message .= '<b>PHP SELF:</b> ' . $_SERVER['PHP_SELF'] . '<br/>';
                    }
                    if (isset($_SERVER['REMOTE_ADDR'])) {
                        $s_more_message .= '<b>REMOTE_ADDR:</b> ' . $_SERVER['REMOTE_ADDR'] . '<br/>';
                    }
                    if (isset($_SERVER['HTTP_USER_AGENT'])) {
                        $s_more_message .= '<b>HTTP USER AGENT:</b> ' . $_SERVER['HTTP_USER_AGENT'] . '<br/>';
                    }
                    if (isset($_SERVER['HTTP_REFERER'])) {
                        $s_more_message .= '<b>HTTP REFERER:</b> ' . $_SERVER['HTTP_REFERER'] . '<br/>';
                    }
                    if (isset($_SERVER['REDIRECT_URL'])) {
                        $s_more_message .= '<b>REDIRECT URL:</b> ' . $_SERVER['REDIRECT_URL'] . '<br/>';
                    }
                    if (isset($_SERVER['PATH_INFO'])) {
                        $s_more_message .= '<b>PATH INFO:</b> ' . $_SERVER['PATH_INFO'] . '<br/>';
                    }
                    if (isset($_SERVER['PATH_TRANSLATED'])) {
                        $s_more_message .= '<b>PATH TRANSLATED:</b> ' . $_SERVER['PATH_TRANSLATED'] . '<br/>';
                    }
                    if (isset($_SERVER['SCRIPT_FILENAME'])) {
                        $s_more_message .= '<b>SCRIPT FILENAMEL:</b> ' . $_SERVER['SCRIPT_FILENAME'] . '<br/>';
                    }
                    $s_more_message .= '<b>PARAMETROS: </b><br/><pre>' . $a_params . '</pre><br/>';
                    $s_more_message .= '<b>Extra Params:</b><pre>' . $a_extra_params . '</pre><br/>';
                    $a_remove = array("\n", "\r\n", "\n\r", "\r");
                    $s_get_message = str_replace($a_remove, '', strip_tags($o_exc->getMessage()));
                    $s_info = sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($o_exc), $o_exc->getCode(), '<div style="word-wrap:break-word;">' . $s_more_message . $s_get_message . '</div>', Debug::path($o_exc->getFile()), $o_exc->getLine());

                    Kohana::$log->add($s_error_log_level
                            , ':info'
                            , array(
                        ':info' => $s_info
                    ));
                    $s_more_message_debug = '<b>Nro Codigo:  ' . $s_code . '</b><br/><br/>';
                    $s_infoDebug = sprintf('%s [ %s ]: %s ~ %s [ %d ]', get_class($o_exc), $o_exc->getCode(), '<div style="word-wrap:break-word;">' . $s_more_message_debug . strip_tags($o_exc->getMessage()) . '</div>', Debug::path($o_exc->getFile()), $o_exc->getLine());
                    $s_trace = $s_infoDebug . "\n--\n" . $o_exc->getTraceAsString();
                    Kohana::$log->add(Log::DEBUG, $s_trace);
                    $s_error_html = '<b>Error:</b><br/>';
                    $s_error_html .= '<b>Codigo:</b>' . $s_code . '<br/><br/>';
                    $s_error_html .= '<b>Hora:</b>' . HelperDate::fnGetMysqlCurrentDateTime() . '<br/><br/>';
                    $s_error_html .= '<b>Sistema:</b>' . $_SERVER['HTTP_HOST'] . '<br/><br/>';
                    $s_error_html .= '<b>Id Usuario:</b>' . self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID) . '<br/><br/>';
                    $s_error_html .= '<b>Usuario:</b>' . self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME) . '<br/><br/>';
                    $a_error['eCode'] = $s_code;
                    $a_error['eEnviroment'] = Kohana::$environment;
                    $a_error['eTime'] = HelperDate::fnGetMysqlCurrentDateTime();
                    $a_error['eDomain'] = SITE_DOMAIN;
                    $a_error['eUserName'] = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
                    $a_error['eText'] = __("Se produjo un error inesperado en el sistema. Por favor contactarse con el area de soporte para solucionar el problema. Codigo de Error:") . ' ' . $s_code;
                }
            }
        } else {
            $a_error = $o_exc->getMessage();
        }
        return $a_error;
    }

    public static function fnSYSGenMenuArray($i_super_menu = null) {
        $o_cache_instance = Cache::instance('apc');
        $s_request_url = 'fnSYSGenMenuArray' . $i_super_menu;
        $s_cache_key = SITE_DOMAIN . $s_request_url;
        $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        if ($a_in_cache_object === null) {
            $a_menu = array();
            $a_select_menu = Model_Bts_Menu::fnGetMenuBySuper($i_super_menu);

            foreach ($a_select_menu as $a_item) {
                $a_temp = $a_item;
                $a_temp['sub_menu'] = self::fnSYSGenMenuArray($a_item['menu_id']);
                array_push($a_menu, $a_temp);
            }

            $o_cache_instance->set($s_cache_key, $a_menu);
            $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        }
        return $a_in_cache_object;
    }

    public static function fnSYSGetPrivilegesAccess($a_group_id) {
        return Model_Bts_Privilege::fnGetAccess($a_group_id);
    }

    public function fnSYSHaveLoginAccess() {
        if (self::fnSYSGetSessionParameter(self::BTS_SESSION_ID))
            return true;
        else
            return false;
    }

    public function fnSYSHavePrivilegeAccess() {
        $a_permited_actions = self::fnSYSGetSessionParameter(self::BTS_SESSION_ACTIONS_ACCESS);

        $a_select_menu = Model_Bts_Menu::fnGetMenuBySuper(null, true);

        $a_menu = array_map(function($a_item) {
                    return $a_item['menu_url'];
                }, $a_select_menu);

        if (!is_array($a_permited_actions))
            return false;

        $i_menu_id = array_search($this->fnSYSGetRouteString() . '.' . Request::initial()->param('format'), $a_menu);

        if ($i_menu_id) {
            return in_array($this->fnSYSGetRouteString(), array_map(function($a_item) {
                                        return '/' . $a_item['access_directory'] . '/' . $a_item['access_controller'] . '/' . $a_item['access_action'];
                                    }, $a_permited_actions)
                    ) AND $a_select_menu[$i_menu_id]['menu_module_ok'] == 1;
        }

        return in_array($this->fnSYSGetRouteString(), array_map(function($a_item) {
                            return '/' . $a_item['access_directory'] . '/' . $a_item['access_controller'] . '/' . $a_item['access_action'];
                        }, $a_permited_actions)
        );
    }

    public function fnSYSHaveReportActiveSession() {
        $response = TRUE;
        $pSID = $this->request->query('sid');
        if ($pSID == NULL) {
            $response = FALSE;
        } else {
            $response = (bool) DB::select(DB::expr('IF( FROM_UNIXTIME(last_active) <= DATE_SUB(NOW(), INTERVAL 10 MINUTE) , 0, 1) AS IS_ACTIVE'))
                            ->from('bts_sessions')
                            ->where(DB::expr('MD5(bts_sessions.session_id)'), 'LIKE', $pSID)->as_object()->execute()->get("IS_ACTIVE");
        }
        return $response;
    }

    public function fnSYSGetArrayMenuActive() {
        $a_select_menu = Model_Bts_Menu::fnGetMenuBySuper(null, true);

        $a_menu = array_map(function($a_item) {
                    return $a_item['menu_url'];
                }, $a_select_menu);

        $i_menu_id = array_search($this->fnSYSGetRouteString() . '.' . Request::initial()->param('format'), $a_menu);

        if ($i_menu_id) {
            $a_menu_active = array();
            self::fnSYSGetMenuActive($i_menu_id, $a_menu_active);
            return $a_menu_active;
        }

        return array();
    }

    public function fnSYSGetBreadcrumbs() {
        $a_select_menu = Model_Bts_Menu::fnGetMenuBySuper(null, true);
        $a_menu = array_map(function($a_item) {
                    return $a_item['menu_url'];
                }, $a_select_menu);

        $i_menu_id = array_search($this->fnSYSGetRouteString() . '.' . Request::initial()->param('format'), $a_menu);
        if ($i_menu_id) {
            $s_breadcrumbs_list = '';
            self::fnSYSGetBreadcrumbsMenu($i_menu_id, true, $s_breadcrumbs_list);
            return '<ul class="breadcrumb">'
                    . $s_breadcrumbs_list
                    . '</ul>';
        }
        return '';
    }

    public static function fnSYSGetMenuActive($i_menu_id, &$a_menu_active) {
        $a_menu = Model_Bts_Menu::fnGetMenuById($i_menu_id);
        $a_menu_active[] = $a_menu['menu_id'];
        if ($a_menu['menu_super_id'] > 0) {
            self::fnSYSGetMenuActive($a_menu['menu_super_id'], $a_menu_active);
        }
    }

    public static function fnSYSGetBreadcrumbsMenu($i_menu_id, $b_first, &$s_breadcrumbs_list) {

        $a_menu = Model_Bts_Menu::fnGetMenuById($i_menu_id);

        if ($a_menu['menu_super_id'] > 0) {
            if ($b_first)
                $s_breadcrumbs_end = '<li class="active">' . __($a_menu['menu_name']) . '</li>';
            else
                $s_breadcrumbs_end = '<li><a href="' . $a_menu['menu_url'] . '">' . __($a_menu['menu_name']) . '</a><span class="divider">/</span></li>';

            $s_breadcrumbs_list .= self::fnSYSGetBreadcrumbsMenu($a_menu['menu_super_id'], false, $s_breadcrumbs_list) . $s_breadcrumbs_end;
        }else {

            $s_breadcrumbs_list .= '<li><a href="' . $a_menu['menu_url'] . '">' . __($a_menu['menu_name']) . '</a><span class="divider">/</span></li>' . $s_breadcrumbs_list;
        }
    }

    public function fnSYSGetRouteString() {
        return '/' . strtolower(Request::initial()->directory())
                . '/' . strtolower(Request::initial()->controller())
                . '/' . Request::initial()->action();
    }

    public static function fnSYSGetArrayFileConfigs() {
        $a_configs = array();

        if ($s_config_directories = opendir(realpath('..') . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'config')) {
            while (false !== ($s_config_name = readdir($s_config_directories))) {
                if ($s_config_name != "." && $s_config_name != "..") {
                    if (preg_match("/^config_[a-zA-Z]+/i", $s_config_name)) {
                        $s_config_name = substr($s_config_name, 0, strlen($s_config_name) - 4);
                        $a_configs[] = array(
                            'config_file' => $s_config_name
                        );
                    }
                }
            }
            closedir($s_config_directories);
        }
        return $a_configs;
    }

    public static function fnSYSGetArrayDataBaseConfigs() {
        $a_config = self::fnSYSGetArrayFileConfigs();
        $a_data_base_config = array();
        foreach ($a_config as $a_item) {
            $a_db_config = Kohana::$config->load($a_item['config_file']);

            $a_data_base_config[] = array(
                $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_INSTANCE] => array
                    (
                    'type' => 'Mysqli',
                    'connection' => array(
                        'hostname' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SERVER],
                        'database' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_NAME],
                        'username' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_USER],
                        'password' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_PASSWORD],
                        'persistent' => FALSE,
                    ),
                    'table_prefix' => '',
                    'charset' => 'utf8',
                    'caching' => FALSE,
                    'profiling' => TRUE,
                )
            );
        }

        return $a_data_base_config;
    }

    public static function fnSYSGetArrayConfigs() {
        $a_config = self::fnSYSGetArrayFileConfigs();
        $a_data_base_config = array();
        foreach ($a_config as $a_item) {
            $a_db_config = Kohana::$config->load($a_item['config_file']);

            $a_data_base_config[] = $a_db_config;
        }

        return $a_data_base_config;
    }

    public static function fnSYSGetOptionKeys() {
        $a_options = DB::select('op.key', 'value')
                ->from(
                        array('bts_option', 'op')
                )
                ->where('op.status', '=', self::BTS_STATUS_ACTIVE)
                ->execute()
                ->as_array('key');
        return $a_options;
    }

    public static function fnSYSGetOptionValue($s_key) {
        $a_options = self::fnSYSGetSessionParameter(self::BTS_SESSION_SYSTEM_OPTIONS);
        if ($a_options)
            return array_key_exists($s_key, $a_options) ? $a_options[$s_key]['value'] : null;

        return null;
    }

    public static function fnSYSAddNewModule($s_module_name, $s_folder_name) {
        $a_modules = array("$s_module_name" => DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . $s_folder_name);
        Kohana::modules(Kohana::modules() + $a_modules);
    }

    public function fnSYSIsKohanaModuleActive($moduleName) {
        $modules = Kohana::modules();
        return array_key_exists($moduleName, $modules);
    }

    public function fnSYSUploadFileTMP($DIR_TMP, $FILE_NAME = NULL, $RESIZE_VAL = 128, $b_response = true, $anyFile = false, $s_resize = Image::HEIGHT) {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $this->fnSYSAddNewModule('qqFileploader', 'qqFileploader');
        if (Fileupload::exists()) {
            $a_pathinfo = pathinfo(Fileupload::name());
            //$filename = $pathinfo['filename'];
            $FILE_NAME = time();
            $EXT = $a_pathinfo['extension'];
            $FILE_NAME .= '.' . $EXT;
            $FILE_PATH = $DIR_TMP . DIRECTORY_SEPARATOR . $FILE_NAME;
            if (Fileupload::save($FILE_PATH)) {
                if ($anyFile == false) {
                    $FILE_IMG_RESIZE = Kohana_Image::factory($FILE_PATH);
                    $FILE_IMG_RESIZE->resize($RESIZE_VAL, $RESIZE_VAL, $s_resize);
                    $FILE_IMG_RESIZE->save($FILE_PATH);
                }
                $a_response['success'] = 'true';
                $a_response['data'] = array(
                    'path' => str_replace(DIRECTORY_SEPARATOR, '/', DIRECTORY_SEPARATOR . $FILE_PATH),
                    'tmpfn' => $FILE_NAME);
            } else {
                throw new Exception(__('Error al crear el archivo'), self::CODE_SUCCESS);
            }
        } else {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        if ($b_response) {
            $this->fnSYSResponseFormat($a_response);
        } else {
            return $a_response;
        }
    }

    public function fnSYSSaveUploadProfileImage($s_img_profile) {
        $skin = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
        $tmp_source_img = self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $s_img_profile;
        $up_profile_dir = self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;
        $up_profile_dir_small = self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . self::PHOTO_UPLOAD_64_DIR . DIRECTORY_SEPARATOR;
        $destination_img = $up_profile_dir . $s_img_profile;
        $destination_img_small = $up_profile_dir_small . $s_img_profile;
        if (!file_exists($up_profile_dir)) {
            mkdir($up_profile_dir, 0777, TRUE);
            mkdir($up_profile_dir_small, 0777, TRUE);
        }
        copy($tmp_source_img, $destination_img);
        copy($tmp_source_img, $destination_img_small);
        $img_small = Kohana_Image::factory($destination_img_small);
        $img_small->resize(64, 64, Image::HEIGHT);
        $img_small->save($destination_img_small);
        HelperFile::deleteFileOrFolderRecursive(self::TMP_UPLOAD . DIRECTORY_SEPARATOR . $s_img_profile);
    }

    public function fnSYSCleaningAllCache() {
        try {
            Cache::instance('apc')->delete_all();
            $website = SITE_DOMAIN;
            $cacheDir = "/$website/";
            HelperCacheFile::fndeleteAllCache($cacheDir);
            apc_clear_cache();
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function fnSYSCleaningLanguages() {
        try {
            global $a_db_config;
            $a_lang = explode('-', $a_db_config[self::BTS_SYS_CONFIG_LANGUAGE]);
            HelperLanguage::loadData($a_lang[1], 'es', 'es', true);
            HelperLanguage::loadData($a_lang[1], 'es', 'en', true);
            Cache::instance('apc')->delete_all();
            HelperCacheFile::fndeleteAllCache();
            apc_clear_cache();
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function fnSYSCleaningSessionsUsers() {
        try {
            DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessions`")->execute();
            DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessionlog`")->execute();
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public function fnSYSActivateDeactivatedSystem($activateDeactivated) {
        try {
            $activateDeactivated = (bool) $activateDeactivated;
            if ($activateDeactivated) {
                DB::query(Database::UPDATE, "UPDATE bts_user SET  systemActive= :opcion where userName NOT IN ('root','adminibus')")
                        ->parameters(array(":opcion" => 1))->execute();
            } else {
                DB::query(Database::UPDATE, "UPDATE bts_user SET  systemActive= :opcion where userName NOT IN ('root','adminibus')")
                        ->parameters(array(":opcion" => 0))->execute();
                DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessions`")->execute();
                DB::query(Database::DELETE, "TRUNCATE TABLE `bts_sessionlog`")->execute();
            }
            return true;
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    public static function fnSYSDeleteFileOrFolderRecursive($file, $deleteThisFileFolder = TRUE) {
        if (file_exists($file)) {
            if (is_dir($file)) {
                $handle = opendir($file);
                while ($filename = readdir($handle)) {
                    if ($filename != "." AND $filename != ".." AND strpos($filename, 'svn') === FALSE) {
                        FileHelper::deleteFileOrFolderRecursive($file . "/" . $filename);
                    }
                }
                closedir($handle);
                if ($deleteThisFileFolder)
                    rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

    public function fnSYSLoadClientLanguage($lang = NULL) {
        $lang = is_null($lang) ? Cookie::get(self::BTS_LANGUAGE) : $lang;
        if (!is_null($lang))
            I18n::lang($lang);
        else
            I18n::lang($this->fnSYSGetConfig(self::BTS_LANGUAGE));
    }

    public function fnSYSCurrentClientLanguage() {
        return I18n::lang();
    }

    public function fnSYSSendEmail($subject, $mailRecipient, $nameRecipient, $emailView, $emailArrayMsgDataObj, $emailArrayMsgData, $emailFrom, $nameFrom, $lang = NULL) {
        $this->fnSYSLoadClientLanguage($lang);
        $this->fnSYSAddNewModule('swift', 'swift');
        $subject = __($subject);
        $to = (is_array($mailRecipient) AND count($mailRecipient) > 0) ? $mailRecipient : array($mailRecipient => $nameRecipient);
        Email::instance($subject)
                ->to($to)
                ->message(Email::template(View::factory($emailView, $emailArrayMsgDataObj)->render(), $emailArrayMsgData, 'Content-type: text/html; charset=iso-8859-1'))
                ->from(array($emailFrom => $nameFrom))->send(FALSE);
    }

    public function fnSYSGetConfig($key_config, $b_only_config = FALSE) {
        global $a_db_config;
        return $b_only_config ? $a_db_config : $a_db_config->get($key_config);
    }

    /**
     * Pagar via Paypal 
     * Method: Instant Payment AND payPayFlow Payment
     * function fnSYSPaypalInstantPayment
     * @param $cardNumber, $cardExpYear, $cardExpMonth, $amount, $cardCvv, $userId, $itemId, $billingFname, $billingLname, $billingState, $billingCity, $billingAddress, $billingCountry, $billingZip, $billingCompany, $billingEmail, $customHtml, $items, $paymentType, $info, $infotwo, $creditCardType = null
     */
    public function fnSYSPaypalInstantPayment($cardNumber, $cardExpYear, $cardExpMonth, $amount, $cardCvv, $userId, $itemId, $billingFname, $billingLname, $billingState, $billingCity, $billingAddress, $billingCountry, $billingZip, $billingCompany, $billingEmail, $customHtml, $items, $paymentType, $info, $infotwo, $creditCardType = null) {
        $a_db_config = $this->fnSYSGetConfig(NULL, TRUE);
        $payment_gateway = $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_GATEWAY];
        $gateway = 'Paypal';
        try {
            $this->fnSYSAddNewModule('payment', 'payment');
            if ($billingState == "") {
                $billingState = 'DL';
            }
            if ($billingCountry == "") {
                $billingCountry = 'US';
            }
            if ($billingZip == "") {
                $billingZip = '10';
            }
            if ($billingAddress == "") {
                $billingAddress = 'Not Address';
            }
            if ($billingCity == "") {
                $billingCity = 'Not City';
            }
            $payment = Payment_Instant::factory();
            $payment->card_number = $cardNumber;
            $payment->card_exp_year = $cardExpYear;
            $payment->card_exp_month = $cardExpMonth;
            $payment->amount = $amount;
            $payment->card_cvv = $cardCvv;
            $payment->item_id = $items;
            $payment->billing_fname = $billingFname;
            $payment->billing_lname = $billingLname;
            $payment->billing_state = $billingState;
            $payment->billing_city = $billingCity;
            $payment->billing_address = $billingAddress;
            $payment->billing_country = $billingCountry;
            $payment->billing_zip = $billingZip;
            $payment->billing_company = $billingCompany;
            $payment->billing_email = $billingEmail;
            $payment->custom = $customHtml;
            $payment->paymentType = $paymentType;
            $payment->creditCardType = $creditCardType;
            if ($payment_gateway == 'paypal_instant') {
                Payment_Instance::Requester($gateway, 'Instant')->pay($payment);
                return $payment->as_array();
            } else {
                $payment->commentone = $info;
                $payment->commenttwo = $infotwo;
                $response = Payment_Instance::Requester($gateway, 'Instant')->payPayFlow($payment);
                return $response;
            }
        } catch (Payment_Exception $payment_exception) {
            throw new Exception($payment_exception->getMessage(), self::BTS_CODE_SUCCESS);
        }
    }

    /** Function: fnSYSPaypalInstantCancel
     * Action: Anular una venta mediante Paypal
     * @param $transactionID, $type = null, $amount = null  
     */
    public function fnSYSPaypalInstantCancel($transactionID, $type = null, $amount = null) {
        $gateway = 'paypal';
        global $a_db_config;
        $payment_gateway = $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_GATEWAY];
        try {
            $this->fnSYSAddNewModule('payment', 'payment');
            $aryData['transactionID'] = $transactionID;
            if ($type == null) {
                $aryData['refundType'] = "Full";
                $aryData['memo'] = __('Se Reembolso el total del dinero.');
            } else {
                $aryData['refundType'] = $type;
                $aryData['memo'] = __('Se Reembolso modo Parcial del dinero.');
            }
            if ($amount != null) {
                $aryData['amount'] = $amount;
            }
            $aryData['currencyCode'] = "USD";

            if ($payment_gateway == 'paypal_instant') {
                $response = Payment_Instance::Requester($gateway, 'instant')->refundAmount($aryData);
                if ($response['ACK'] == "Success") {
                    Database::instance()->commit();
                    return $response;
                } else {
                    return false;
                }
            } else {
                $response = Payment_Instance::Requester($gateway, 'instant')->payPayFlow_cancel($aryData);
                if ($response['RESULT'] == 0 AND $response['RESPMSG'] == 'Approved') {
                    Database::instance()->commit();
                    return $response;
                } else {
                    return false;
                }
            }
        } catch (Payment_Exception $payment_exception) {
            throw new Exception($payment_exception->getMessage(), self::BTS_CODE_SUCCESS);
        }
    }

    public function fnSYSPrintDocument($s_document_type, $a_data_template = array(), $b_universal_print = FALSE, $b_only_return_view = FALSE) {
        $empty_template = new View('/bts/printer/emptyTemplate');

        $SKIN = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
        $BROWSER_FOLDER = 'ALL_BROWSERS';
        $OS_FOLDER = 'ALL_OS';
        $PRINTER_FOLDER = 'ALL_PRINTERS';
        $FOLDER_TEMPLATE = 'printer' . DIRECTORY_SEPARATOR . $BROWSER_FOLDER . DIRECTORY_SEPARATOR . $OS_FOLDER . DIRECTORY_SEPARATOR . $PRINTER_FOLDER . DIRECTORY_SEPARATOR . strtoupper($SKIN);
        $HTML_TEMPLATE = $FOLDER_TEMPLATE . DIRECTORY_SEPARATOR . $s_document_type;

        if (count(Kohana::find_file('views', $HTML_TEMPLATE, NULL, TRUE)) == 0) {
            $print_error_view = new View('/bts/printer/printError');
            $print_template_no_exist_view = new View('/bts/printer/printTemplateNoExist');
            $print_template_no_exist_view->ERROR_MESSAGE = __("No existe un formato para la configuración de su terminal y el documento que desea imprimir. Su Configuración es la siguiente:");
            $print_template_no_exist_view->os = $OS_FOLDER;
            $print_template_no_exist_view->printer = $PRINTER_FOLDER;
            $print_template_no_exist_view->browser = $BROWSER_FOLDER;
            $print_template_no_exist_view->document = $s_document_type;
            $print_error_view->SYSTEM_SKIN = $SKIN;
            $print_error_view->title_dialog = __('No Existe Formato');
            $print_error_view->content_dialog = $print_template_no_exist_view;
            $empty_template->printable_document = $print_error_view;
            $this->template = $empty_template;
        } else {
            $documentPrint_view = new View($HTML_TEMPLATE, $a_data_template);
            $empty_template->printable_document = $documentPrint_view;
        }
        $this->template = $empty_template;
    }

    public function fnSYSGetDATE_FORMAT_FOR($DF, $BTS_SO_DATE_FORMAT = NULL) {
        if ($BTS_SO_DATE_FORMAT == NULL) {
            $BTS_SO_DATE_FORMAT = $this->fnSYSGetOptionValue(self::BTS_SO_DATE_FORMAT);
        }
        return $this->a_bts_date_format[$BTS_SO_DATE_FORMAT][$DF];
    }

}
