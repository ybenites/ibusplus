<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Message
 *
 * @author BENITES
 */
abstract class Kohana_Controller_Private_Message extends Kohana_Controller_Private_Admin {
    public function action_index(){
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_MESSAGE', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_MESSAGE', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }   
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $format_date_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL);
        $format_time_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL);            
        $v_admin_message = View::factory('bts/private/admin/message');  
        $v_admin_message->format_date_sql=$format_date_sql;
        $v_admin_message->format_time_sql=$format_time_sql;
        $v_admin_message->SKIN=$this->template->SYSTEM_SKIN;
        $this->template->SYSTEM_BODY = $v_admin_message;
    }
    public function action_messageListPrev(){        
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('n_page', 'not_empty')
                    ->rule('lastNotification', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'n_page':
                            $s_label_name = __("Pagina");
                            break;
                        case 'lastNotification':
                            $s_label_name = __("Ultima Notificacion");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
     
            $iPage =$a_form_post_data['n_page'];
            $i_last_notification =$a_form_post_data['lastNotification'];
            $format_date_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL);
            $format_time_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL);
            $a_response['data'] = HelperNotification::fngetInfoMessages($iPage, $i_last_notification, null,$format_date_sql,$format_time_sql);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] =self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_delete(){
         $a_response = $this->a_bts_json_response;
        try {                     
            $a_response['data'] = DB::query(Database::DELETE, "TRUNCATE TABLE `bts_notification`")->execute(); 
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] =self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_getListCantMessages(){
        $a_response = $this->a_bts_json_response;
        try {             
            $a_query=DB::select(
                    array('u.idUser','ID'),array('p.fullName','user')
                    ,array(DB::expr('COUNT(not.userCreate)'),'numberMessages')
                    )->from(array('bts_notification','not'))
                    ->join(array('bts_user','u'))->on('not.userCreate','=','u.idUser')
                    ->join(array('bts_person','p'))->on('u.idUser','=','p.idPerson')
                    ->where('state','=',self::BTS_STATUS_ACTIVE)
                    ->execute()->as_array();            
            $a_response['data']=$a_query;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] =self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_lastNotificationUser(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idUser', 'not_empty');                    
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'idUser':
                            $s_label_name = __("Usuario");
                            break;                        
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();              
            $a_response['data']=HelperNotification::fnGetLastDateNotificationMessage($a_form_post_data['idUser']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] =self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_cantmessagesuser_list(){
         $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('lastNotification', 'not_empty')
                    ->rule('n_page', 'not_empty')
                    ->rule('idUser', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'lastNotification':
                            $s_label_name = __("Ultima Notificación");
                            break;                        
                        case 'n_page':
                            $s_label_name = __("Nro Pagina");
                            break;                        
                        case 'idUser':
                            $s_label_name = __("Usuario");
                            break;                        
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();   
            $format_date_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL);
            $format_time_sql=$this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL);
            $a_response['data']=HelperNotification::fngetInfoMessages($a_form_post_data['n_page'],$a_form_post_data['lastNotification'],$a_form_post_data['idUser'],$format_date_sql,$format_time_sql);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] =self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}
