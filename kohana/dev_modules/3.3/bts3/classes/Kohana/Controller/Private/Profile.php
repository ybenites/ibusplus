<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Profile extends Kohana_Controller_Private_Admin {

    public function action_loadData() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            if (!$o_check_post->check()) {
                throw new Exception(__("Error en la validación de la Información."), self::BTS_CODE_ERROR);
            } else {
                $iduser = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
                $o_cacheInstance = Cache::instance('apc');
                $s_cacheProfileKey = SITE_DOMAIN.get_class().__METHOD__.'profile'.$iduser;
                $s_cacheofficesKey = SITE_DOMAIN.get_class().__METHOD__.'allOffices'.$iduser;
                $a_incacheProfileObject = $o_cacheInstance->get($s_cacheProfileKey);
                $a_incacheOfficesObject = $o_cacheInstance->get($s_cacheofficesKey);
                if ($a_incacheProfileObject === null) {
                $DIRECOTRY =
                        'http://static.o.bts.mcets-inc.com'.DIRECTORY_SEPARATOR.'img/'. self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR .
                        $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN) . DIRECTORY_SEPARATOR
                        . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;
                $DIRECOTRY_BTS =
                        'http://static.o.bts.mcets-inc.com'.DIRECTORY_SEPARATOR . 'kernel' . DIRECTORY_SEPARATOR.'img/'
                        . self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR
                        . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;
                $loadProfile = DB::select(
                                        array('us.idUser', 'idUser')
                                        , array('us.userName', 'userName')
                                        , array('gro.idGroup', 'idGroup')
                                        , array('gro.name', 'namegr')
                                        , DB::expr("IF(us.imgProfile IS NULL,CONCAT(:DIRECTORY_BTS,'default.png'),CONCAT(:DIRECTORY,us.imgProfile)) AS imgProfile")
                                        , array('of.idOffice', 'idOffice')
                                )
                                ->from(array('bts_user', 'us'))
                                ->join(array('bts_group', 'gro'))->on('gro.idGroup', '=', 'us.idGroup')
                                ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'us.idOffice')
                                ->where('us.idUser', '=', $iduser)
                                ->param(':DIRECTORY', $DIRECOTRY)
                                ->param(':DIRECTORY_BTS', $DIRECOTRY_BTS)
                                ->execute()->current();

                $allOffice = DB::select(
                                        array('of.idOffice', 'idOffice')
                                        , array('of.name', 'nameof')
                                )
                                ->from(array('bts_office', 'of'))
                                ->where('of.status', '=', self::BTS_STATUS_ACTIVE)
                                ->execute()->as_array();
                $o_cacheInstance->set($s_cacheProfileKey, $loadProfile);
                $a_incacheProfileObject = $o_cacheInstance->get($s_cacheProfileKey);
                $o_cacheInstance->set($s_cacheofficesKey, $allOffice);
                $a_incacheOfficesObject = $o_cacheInstance->get($s_cacheofficesKey);
                }
                $a_response['data'] = $a_incacheProfileObject;
                $a_response['listOf'] = $a_incacheOfficesObject;
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSProfileSave($s_controller_type, $o_check_post) {

        if (!$o_check_post->check()) {
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        $a_form_post_data = $o_check_post->data();
        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_user = new Model_Bts_User();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_user = new Model_Bts_User($a_form_post_data['id']);
            if (!$o_model_user->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        } else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }


        $a_user_save = array(
            'idOffice' => $a_form_post_data['fuser_office']
            , 'imgProfile' => $a_form_post_data['fmenu_image']
        );

        if ($a_form_post_data['fmenu_image'] != NULL) {
            $this->fnSYSSaveUploadProfileImage($a_form_post_data['fmenu_image']);
        }

        $o_model_user->saveArray($a_user_save);
    }

    public function action_updateUser() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSProfileSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getMessage();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changePass() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            if (!$o_check_post->check()) {
                throw new Exception(__("Error en la validación de la Información."), self::BTS_CODE_ERROR);
            } else {

                $pass_actual = md5($o_check_post['pass_actual']);
                $userName = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
                $iduser = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
                $idGroup = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
                $idOffice = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID);

                $password = DB::select(
                                        array('us.password', 'password')
                                        , array('us.userName', 'userName')
                                )
                                ->from(array('bts_user', 'us'))
                                ->join(array('bts_person', 'per'))->on('per.idPerson', '=', 'us.idUser')
                                ->where('us.userName', '=', $userName)
                                ->execute()->current();

                $pass_consulta = $password['password'];

                //comparo claves actuales
                if ($pass_actual == $pass_consulta) {
                    $new_pass = $o_check_post['new_pass'];
                    $new_pass_confir = $o_check_post['new_pass_confir'];
                    //comparo claves nuevas
                    if ($new_pass == $new_pass_confir) {
                        $o_model_user = new Model_Bts_User();
                        $o_model_user->changePass($new_pass, $iduser, $idGroup, $idOffice);
                    } else {
                        throw new Exception(__("Las contraseña no coinciden"), self::BTS_CODE_ERROR);
                    }
                } else {
                    throw new Exception(__("Su contraseña actual no coincide"), self::BTS_CODE_ERROR);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getUploadImageMenu() {
        $this->fnSYSUploadFileTMP(self::TMP_UPLOAD);
    }

}

