<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Privilege extends Kohana_Controller_Private_Admin {

    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        $this->fnSYSLoadClientLanguage();
        $a_interface = Kohana::$config->load('uInterface');
        foreach ($a_interface as $s_className) {
            $this->imports(get_class($s_className));
        }
    }    
    
    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_PRIVILEGE', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_PRIVILEGE', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );
        $v_admin_privilege = View::factory('bts/private/admin/privilege');

        $o_model_groups = new Model_Bts_Group();
        $a_groups = $o_model_groups->order_by('idGroup')->find_all()->as_array();

        $v_admin_privilege->BTS_S_A_GROUPS =
                implode(',', array_map(function($o_group) {
                            return "{'group_id': " . $o_group->idGroup . ", 'group_name': '" . $o_group->name . "'}";
                        }, $a_groups)
        );


        $this->template->SYSTEM_BODY = $v_admin_privilege;
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' pr.idPrivilege ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "privilege_key" => "CONCAT(ctr.directory, ctr.controller, ctr.type)"
                , "privilege_directory" => "ctr.directory"
                , "privilege_controller" => "ctr.controller"
                , "privilege_type_hidden" => "ctr.type"
                , "privilege_type" => "
                    CASE 
                        WHEN  ctr.type = '" . self::BTS_CONTROLLER_TYPE_READ . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_READ]['display']) . "' 
                        WHEN  ctr.type = '" . self::BTS_CONTROLLER_TYPE_CREATE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_CREATE]['display']) . "' 
                        WHEN  ctr.type = '" . self::BTS_CONTROLLER_TYPE_UPDATE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_UPDATE]['display']) . "' 
                        WHEN  ctr.type = '" . self::BTS_CONTROLLER_TYPE_DELETE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_DELETE]['display']) . "'                         
                    ELSE pr.type END"
            );

            $o_model_groups = new Model_Bts_Group();
            $a_groups = $o_model_groups->order_by('idGroup')->find_all()->as_array();

            $s_extra_groups = "";
            foreach ($a_groups as $o_group) {
                $a_array_cols['group_' . $o_group->idGroup] = " IF( FIND_IN_SET( " . $o_group->idGroup . ", GROUP_CONCAT(group_.idGroup) ) > 0 , 1, 0) ";
            }
            $s_extra_groups.=" LEFT JOIN bts_group group_
                                ON group_.idGroup = pr.idGroup AND pr.access = 1 ";

            $a_columns_search = array(
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_privilege pr
                RIGHT JOIN bts_controller ctr
                    ON ctr.directory = pr.directory AND ctr.controller = pr.controller AND ctr.type = pr.type
            ';
            $s_tables_join .= $s_extra_groups;

            $s_where_conditions = " TRUE ";

            $s_group = " GROUP BY ctr.directory , ctr.controller , ctr.type ";
            $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_group);

            $i_total_pages = 0;

            $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start, $s_group);
            $this->fnSYSResponseFormat(HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_change() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('privilege_group_id', 'not_empty')
                    ->rule('privilege_directory', 'not_empty')
                    ->rule('privilege_controller', 'not_empty')
                    ->rule('privilege_type', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {

                    switch ($s_key) {
                        case 'privilege_group_id':
                            $s_label_name = __("Grupo");
                            break;
                        case 'privilege_directory':
                            $s_label_name = __("Directorio");
                            break;
                        case 'privilege_controller':
                            $s_label_name = __("Controlador");
                            break;
                        case 'privilege_type':
                            $s_label_name = __("Tipo");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_privilege = new Model_Bts_Privilege();
            $o_model_privilege
                    ->where('directory', '=', $a_form_post_data['privilege_directory'])
                    ->and_where('controller', '=', $a_form_post_data['privilege_controller'])
                    ->and_where('type', '=', $a_form_post_data['privilege_type'])
                    ->and_where('idGroup', '=', $a_form_post_data['privilege_group_id'])
                    ->find();

            if (!$o_model_privilege->loaded()) {
                $o_model_privilege = new Model_Bts_Privilege();
                $o_model_privilege->access = 0;
                $o_model_privilege->directory = $a_form_post_data['privilege_directory'];
                $o_model_privilege->controller = $a_form_post_data['privilege_controller'];
                $o_model_privilege->type = $a_form_post_data['privilege_type'];
                $o_model_privilege->idGroup = $a_form_post_data['privilege_group_id'];
            }

            $o_model_privilege->access = ($o_model_privilege->access == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_privilege->save();
            $a_response['data']['privilege_directory'] = $o_model_privilege->directory;
            $a_response['data']['privilege_controller'] =$o_model_privilege->controller;
            $a_response['data']['privilege_type'] = $o_model_privilege->type;
            $a_response['data']['privilege_group_id'] = $o_model_privilege->idGroup;
            $a_response['data']['access'] = $o_model_privilege->access;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

}