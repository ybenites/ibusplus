<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Parameters extends Kohana_Controller_Private_Admin {
    public function action_index(){  
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {            
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_PARAMETERS', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_PARAMETERS', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);            

        $parametersShow=$this->BTS_CONFIGURATION_PARAMETERS;
        
        $this->fnSYSCleaningAllCache();
        
        $fileName='config_'.str_replace(".", "", SITE_DOMAIN).'.php ';                
        $urlFile=APPPATH.'config';
        $file=$urlFile.DIRECTORY_SEPARATOR.$fileName;       
        $fichero_url = fopen (trim($file), "r+");
        
        $isRoot=$this->fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION); 
                
        if(!$isRoot){
            foreach ($parametersShow as $key => $v) {
                if($v['unique_root_option']){                    
                    unset ($parametersShow[$key]);
                }
            }
        }
                       
        while (!feof($fichero_url))
        {    
            $pos=ftell($fichero_url);            
            $linea = fgets($fichero_url) ;
            $position_inline=strpos($linea,'=>');                        
            if($position_inline!=false){                
                $clean_val=array(",","=>","'");
                $linea_right=substr($linea,$position_inline+2);                
                $linea_left=substr($linea,0,$position_inline);                
                $newLinea_left=str_replace($clean_val,'',$linea_left);
                $newLinea_right=str_replace($clean_val,'',$linea_right);
                $parametersShow[constant(trim($newLinea_left))]['value']=$newLinea_right;
                $parametersShow[constant(trim($newLinea_left))]['position']=$pos;
            }        
        }                   
        fclose($fichero_url);                                    
        $v_admin_parameters= View::factory('bts/private/admin/parameters');                                        
        $v_admin_parameters->parametersShow=$parametersShow;
        $this->template->SYSTEM_BODY = $v_admin_parameters;
    }
    public function action_update(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSParameterSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post,$a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function fnSYSParameterSave($s_controller_type, $o_check_post, &$a_error_labels){
         $o_check_post
                ->rule('param', 'not_empty')
                ->rule('value', 'not_empty');                
        
        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {                    
                switch ($s_key) {
                    case 'param':
                        $s_label_name = __("Parametro");
                        break;
                    case 'value':
                        $s_label_name = __("Valor");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }        
        $a_post_data = $o_check_post->data();
        if($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE){
            $fileName='config_'.str_replace(".", "", SITE_DOMAIN).'.php';           
            $urlFile=APPPATH.'config';
            $file=$urlFile.DIRECTORY_SEPARATOR.$fileName;                                            
            $a_data_file = file(trim($file));
            foreach ($a_data_file as $key => $linea) {
                $pos_line_a=  strpos($linea,'=>');
                if($pos_line_a!=false){
                    $clean_val=array(",","=>","'");                        
                    $linea_left=substr($linea,0,$pos_line_a); 
                    $newLinea_left=str_replace($clean_val,'',$linea_left);                    
                    if(constant(trim($newLinea_left))==$a_post_data['param']){                            
                        $a_data_file[$key]=$linea_left.'=> '."'".$a_post_data['value']."' \n";
                        file_put_contents(trim($file),$a_data_file);
                    }                    
                }
            }            
        }else{
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
    }
}