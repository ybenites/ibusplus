<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Welcome extends Kohana_Controller_Private_Admin {

    public function action_index() {
        if (self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID) == self::BTS_GROUP_ROOT)
        {          
            $this->redirect(self::fnSYSGetSessionParameter(self::BTS_SESSION_DEFAULT_HOME));
        }
    }
}