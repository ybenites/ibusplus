<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Supportcontact extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_SUPPORT_CONTACT', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_SUPPORT_CONTACT', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $v_services = View::factory('bts/private/admin/support_contact');
        $this->template->SYSTEM_BODY = $v_services;
    }
    public function action_info() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_SUPPORT_INFO', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_SUPPORT_INFO', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $v_info = View::factory('bts/private/admin/support_info');
        $this->template->SYSTEM_BODY = $v_info;
    }

    public function action_getSupportEmails() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        try {
            try {
            $a_data = parse_ini_file(APPPATH . '/config/support.ini');    
            } catch (Exception $exc) {
                throw new Exception(__('EL archivo de configuración support.ini no existe o está dañado'), self::BTS_CODE_SUCCESS);
            }
            if ($a_data) {
                $a_response['data'] = $a_data;
            } else {
                throw new Exception(__('EL archivo de configuración support.ini no existe o está dañado'), self::BTS_CODE_SUCCESS);
            }
        } catch (Exception $exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_saveSupportEmails() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        try {
            $a_emails = $this->request->post('support_mails');
            $a_team = $this->request->post('support_team');
            
            if(count($a_emails) == 0){
                throw new Exception(__('Al menos debe ingresar un nombre y correo electrónico'), self::BTS_CODE_SUCCESS);
            }
            
            $ini_file = APPPATH . 'config' . DIRECTORY_SEPARATOR . 'support.ini';
            HelperFile::deleteFileOrFolderRecursive($ini_file);
            $controllerFile = fopen($ini_file, 'a') or die(__('EL archivo de configuración support.ini no existe o está dañado'));
            fputs($controllerFile, "[support_mails]\n");
            foreach ($a_emails as $email) {
                fputs($controllerFile, "support_emails[] = $email\n");
            }
            fputs($controllerFile, "\n[support_team]\n");
            foreach ($a_team as $team_name) {
                fputs($controllerFile, "support_name[] = $team_name\n");
            }
            fclose($controllerFile);
            $a_response['msg'] = __('Se ha guardado la información en el arhivo support.ini');
        } catch (Exception $exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

}