<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_User extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $o_cache_instance = Cache::instance('apc');
        $s_request_url = $this->request->controller() . $this->request->action();
        $s_cache_key_css = SITE_DOMAIN . $s_request_url . 'css';
        $s_cache_key_js = SITE_DOMAIN . $s_request_url . 'js';
        $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
        $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
        if ($a_in_cache_object_css === null) {
            $o_cache_instance->set($s_cache_key_css, self::fnSYSGetFilePage('ADMIN_USER', self::BTS_FILE_TYPE_CSS));
            $o_cache_instance->set($s_cache_key_js, self::fnSYSGetFilePage('ADMIN_USER', self::BTS_FILE_TYPE_JS));
            $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
            $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($a_in_cache_object_js);
        $this->fnSYSAddStyles($a_in_cache_object_css);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );

        $v_admin_user = View::factory('bts/private/admin/user');
        $v_admin_user->BTS_A_GROUP_LIST = Model_Bts_Group::fnGetAll();
        $v_admin_user->BTS_A_OFFICE_LIST = Model_Bts_Office::fnGetAll();
        $this->template->SYSTEM_BODY = $v_admin_user;
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;
            $o_cacheInstance = Cache::instance('apc');
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' us.idUser ';

            $s_where_search = "";
            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));
            $a_array_cols = array(
                "user_id" => "us.idUser"
                , "user_group" => "gr.name"
                , "user_office" => "of.name"
                , "user_username" => "us.userName"
                , "user_full_name" => "pe.fullName"
                , "user_email" => "pe.email"
                , "user_root_option" => "us.rootOption"
            );

            $a_columns_search = array(
                "user_id" => "us.idUser"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_user us 
                INNER JOIN bts_group gr
                    ON us.idGroup = gr.idGroup
                INNER JOIN bts_office of
                    ON us.idOffice = of.idOffice
                INNER JOIN bts_person pe
                    ON us.idUser = pe.idPerson
            ';

            $s_where_conditions = " us.status = " . self::BTS_STATUS_ACTIVE . " ";
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            //x filtro
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action(). $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function fnSYSUserSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('fuser_group', 'not_empty')
                ->rule('fuser_office', 'not_empty')
                ->rule('fuser_username', 'not_empty')
                ->rule('fuser_name', 'not_empty')
//                ->rule('fuser_lastname', 'not_empty')
//                ->rule('fuser_address', 'not_empty')
//                ->rule('fuser_phone', 'not_empty')
//                ->rule('fuser_email', 'not_empty')
//                ->rule('fuser_email', 'email')
                ->rule('fuser_username', 'regex', array(':value', '/^[a-zA-Z][a-zA-Z0-9]+$/i'))
                ->rule('fuser_username', 'min_length', array(':value', '6'))
                ->rule('fuser_username', 'max_length', array(':value', '12'));

        $b_set_password = false;

        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_check_post->rule('fuser_password', 'not_empty');
            $o_check_post->rule('fuser_password_verify', 'not_empty');
            $o_check_post->rule('fuser_password', 'min_length', array(':value', '6'));
            $o_check_post->rule('fuser_password_verify', 'matches', array(':validation', ':field', 'fuser_password'));
            $b_set_password = true;

            $o_check_post->rule('fuser_username', 'Model_Bts_User::fnValidUniqueUsername');
            //$o_check_post->rule('fuser_email', 'Model_Bts_User::fnValidUniqueEmail');
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            //IF va editar el password?
            if ($o_check_post['fuser_change_password'] == 1) {
                $o_check_post->rule('fuser_password', 'not_empty');
                $o_check_post->rule('fuser_password_verify', 'not_empty');
                $o_check_post->rule('fuser_password', 'min_length', array(':value', '6'));
                $o_check_post->rule('fuser_password_verify', 'matches', array(':validation', ':field', 'fuser_password'));
                $b_set_password = true;
            }
            //Mas parametros u.u
            $o_check_post->rule('fuser_username', 'Model_Bts_User::fnValidUniqueUsername', array(':value', $o_check_post['fuser_id']));
            //$o_check_post->rule('fuser_email', 'Model_Bts_User::fnValidUniqueEmail', array(':value', $o_check_post['fuser_id']));
        }

        //Usuario in DB

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'fuser_group':
                        $s_label_name = __("Grupo");
                        break;
                    case 'fuser_office':
                        $s_label_name = __("Oficina");
                        break;
                    case 'fuser_username':
                        $s_label_name = __("Usuario");
                        break;
                    case 'fuser_password':
                        $s_label_name = __("Contraseña");
                        break;
                    case 'fuser_password_verify':
                        $s_label_name = __("Verificación de Contraseña");
                        break;
                    case 'fuser_name':
                        $s_label_name = __("Nombre");
                        break;
//                    case 'fuser_lastname':
//                        $s_label_name = __("Apellido");
//                        break;
//                    case 'fuser_address':
//                        $s_label_name = __("Dirección");
//                        break;
//                    case 'fuser_phone':
//                        $s_label_name = __("Teléfono");
//                        break;
//                    case 'fuser_email':
//                        $s_label_name = __("Email");
//                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    case 'regex':
                        $a_error_labels[] = $s_label_name . ' ' . __("invalido");
                        break;
                    case 'Model_Bts_User::fnValidUniqueUsername':
                        $a_error_labels[] = $s_label_name . ' ' . __("duplicado");
                        break;
                    case 'Model_Bts_User::fnValidUniqueEmail':
                        $a_error_labels[] = $s_label_name . ' ' . __("duplicado");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();

        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_user = new Model_Bts_User();
            $o_model_person = new Model_Bts_Person();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_user = new Model_Bts_User($a_form_post_data['fuser_id']);
            $o_model_person = new Model_Bts_Person($a_form_post_data['fuser_id']);
            if (!$o_model_user->loaded() || !$o_model_person->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        } else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }


        $a_person_save = array(
            //'document' => $a_form_post_data['']
            //, 'idDocumentType' => $a_form_post_data['']
            'fName' => $a_form_post_data['fuser_name']
            , 'lName' => $a_form_post_data['fuser_lastname']
            , 'fullName' => $a_form_post_data['fuser_name'] . ' ' . $a_form_post_data['fuser_lastname']
            //, 'sex' => $a_form_post_data['']
            , 'phone1' => $a_form_post_data['fuser_phone']
            //, 'phone2' => $a_form_post_data['']
            //, 'cellPhone' => $a_form_post_data['']
            , 'email' => $a_form_post_data['fuser_email']
            , 'address1' => $a_form_post_data['fuser_address']
                //, 'address2' => $a_form_post_data['']
                //, 'type' => $a_form_post_data['']
                //, 'dob' => $a_form_post_data['']
        );

        $o_model_person->saveArray($a_person_save);

        $a_user_save = array(
            'idUser' => $o_model_person->idPerson
            , 'idGroup' => $a_form_post_data['fuser_group']
            , 'idOffice' => $a_form_post_data['fuser_office']
            , 'ownSession' => $a_form_post_data['fuser_own_session']
            , 'userName' => $a_form_post_data['fuser_username']
            //Image...
            , 'imgProfile' => array_key_exists('fuser_photo_data', $a_form_post_data) ? $a_form_post_data['fuser_photo_data'] : NULL
            , 'rootOption' => $a_form_post_data['fuser_root_option']
        );

        if ($b_set_password)
            $a_user_save['password'] = md5($a_form_post_data['fuser_password']);

        $o_model_user->saveArray($a_user_save);
        if ($a_user_save['imgProfile'] != NULL AND $a_user_save['imgProfile'] != '') {
            $this->fnSYSSaveUploadProfileImage($a_user_save['imgProfile']);
        }
    }

    public function action_create() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {

            Database::instance()->begin();
            $this->fnSYSUserSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
            Database::instance()->commit();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            Database::instance()->begin();
            $this->fnSYSUserSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
            Database::instance()->commit();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_model_user = new Model_Bts_User($a_form_post_data['id']);
            $o_model_person = new Model_Bts_Person($a_form_post_data['id']);

            if (!$o_model_user->loaded() || !$o_model_person->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
            $DIRECOTRY =
                    DIRECTORY_SEPARATOR . self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR .
                    $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN) . DIRECTORY_SEPARATOR
                    . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;
            $a_response['data'] = array(
                'user_id' => $o_model_user->idUser
                , 'user_group' => $o_model_user->idGroup
                , 'user_group_name' => $o_model_user->group->name
                , 'user_office' => $o_model_user->idOffice
                , 'user_office_name' => $o_model_user->office->name
                , 'user_username' => $o_model_user->userName
                , 'user_root_option' => $o_model_user->rootOption
                , 'user_own_session' => $o_model_user->ownSession
                , 'user_name' => $o_model_person->fName
                , 'user_lastname' => $o_model_person->lName
                , 'user_address' => $o_model_person->address1
                , 'user_phone' => $o_model_person->phone1
                , 'user_email' => $o_model_person->email
                , 'user_photo' => $DIRECOTRY . $o_model_user->imgProfile
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_user = new Model_Bts_User($a_form_post_data['id']);

            if (!$o_model_user->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_user->status = self::BTS_STATUS_DEACTIVE;
            $o_model_user->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $s_cacheKey_action = SITE_DOMAIN . $this->request->controller().'listActive';
            $o_cacheInstance->delete($s_cacheKey_action);
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getUsers() {
        $this->auto_render = false;
        $a_response = $this->a_bts_json_response;
        try {
            $term = $this->request->post('query');
            $type = $this->request->post('t');
            $cols = array(
                array('pe.idPerson', 'id'),
                array('pe.fullname', 'value'),
                array('o.name', 'office')
            );
            if ($type != NULL) {
                if ($type == 'f') {
                    $cols = array('pe.*', 'u.*');
                }
            }
            $o_users = DB::select_array($cols)
                    ->from(array('bts_user', 'u'))
                    ->join(array('bts_person', 'pe'))->on('u.idUser', '=', 'pe.idPerson')
                    ->join(array('bts_office', 'o'))->on('o.idOffice', '=', 'u.idOffice')
                    ->where('u.status', '=', self::BTS_STATUS_ACTIVE);
            if ($term != NULL OR $type == 'c') {
                $o_users =
                        $o_users
                        ->and_where('pe.fullname', 'LIKE', '%' . $term . '%');
            }
            $a_response['data'] = $o_users->execute()->as_array();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getMimicUser() {
        $this->auto_render = false;
        $a_response = $this->a_bts_json_response;
        try {
            if ((self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION) OR self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC))) {
                if ($this->request->post('reset') != NULL) {
                    $realUserId = self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC_REAL_ID);
                    if ($realUserId != NULL) {
                        $realUser = new Model_Bts_User($realUserId);
                        $realUser->idUserMimic = NULL;
                        $realUser->update();
                        $a_response['msg'] = __("Ahora puede iniciar sesión con su propio usuario");
                    }
                } else {
                    $targetUserId = $this->request->post('uid');
                    $targetUserName = $this->request->post('ufname');
                    $realUserId = self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC_REAL_ID);
                    $realUser = new Model_Bts_User($realUserId);
                    if ($realUserId == $targetUserId) {
                        $realUser->idUserMimic = NULL;
                        $a_response['msg'] = __("Ahora puede iniciar sesión con su propio usuario");
                    } else {
                        $realUser->idUserMimic = $targetUserId;
                        $a_response['msg'] = __("Debe cerrar e iniciar sesión nuevamente para iniciar sesión como el usuario <b>" . $targetUserName . "</b>");
                    }
                    $realUser->update();
                }
            } else {
                throw new Exception(__("No tiene los privilegios necesarios para realizar esta acción"), self::BTS_CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getCountUsersOnline() {

        try {
            $this->auto_render = FALSE;
            $sql = "SELECT  last_active,  sl.ip, p.fullName,u.userName, u.idUser, u.imgProfile, s.session_id,  MAX( FROM_UNIXTIME(s.last_active) ) lastActive
                    FROM bts_sessions s 
                    INNER JOIN bts_sessionlog sl 
                    ON s.session_id = sl.sessionId
                    INNER JOIN bts_user u
                    ON u.idUser = sl.idUser
                    INNER JOIN bts_person p
                    ON p.idPerson = u.idUser                    
      WHERE UNIX_TIMESTAMP(NOW()) - CAST(s.last_active AS SIGNED) < 900 AND sl.event ='LOGIN' GROUP BY sl.idUser ORDER BY lastActive DESC";
            $online = DB::query(Database::SELECT, $sql)->execute()->count();
        } catch (Exception $e) {
            $online = $e->getMessage();
            Kohana::$log->add(Kohana_Log::ERROR, 'Error al obtener usuarios en línea: ":msg"', array(':msg' => $e->getMessage()));
        }
        echo $online;
    }

    public function action_getUsersOnline() {
        $this->auto_render = FALSE;
        try {
            $sql = "SELECT  MAX( FROM_UNIXTIME(s.last_active) ) lastActive , sl.idSessionLog, s.session_id, sl.ip, p.fullName,u.userName, u.idUser, u.imgProfile, 
            DATE_FORMAT(sl.eventDate, '%m-%d-%Y') AS lastActive,
            sl.agent, sl.info  FROM bts_sessions s 
                    INNER JOIN bts_sessionlog sl 
                    ON s.session_id = sl.sessionId
                    INNER JOIN bts_user u
                    ON u.idUser = sl.idUser
                    INNER JOIN bts_person p
                    ON p.idPerson = u.idUser  WHERE UNIX_TIMESTAMP(NOW()) - CAST(s.last_active AS SIGNED) < 900  AND sl.event ='LOGIN' GROUP BY sl.idUser ORDER BY lastActive DESC";
            $online = DB::query(Database::SELECT, $sql)->execute();
            foreach ($online as $value) {
                $userName = $value['userName'];
                echo '
                <div class="geoRow">
		<div class="flag"><img src="' . strtolower($value['imgProfile']) . '" width="16" height="11" /></div>
		<div class="country"><img src="http://static.css.bts.mcets-inc.com/kernel/img/bts/user-info.png" class="userInfo" rel="' . $value['idSessionLog'] . '" id="' . $value['idUser'] . '" >&nbsp;&nbsp;<a href="javascript:void(0)" onclick="javascript:chatWith(\'' . $userName . '\')">' . $value['userName'] . ' :: ' . $value['ip'] . ' :: ' . $value['lastActive'] . '</a>                
                </div>
                </div>';
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        die();
    }

    public function action_getInfoUserOnline() {
        $idUser = $this->request->post('idUser');
        $idSessionlog = $this->request->post('idSessionlog');
        $this->auto_render = false;
        $a_response = $this->a_bts_json_response;
        try {
            $sql = "SELECT u.`idUser`, u.`idGroup`, u.`idOffice`, u.`userName`, u.`imgProfile`,p.`sex`,p.`fullName`,o.`name` AS officeName, g.`name` AS groupName FROM `bts_user` u
                    INNER JOIN bts_person p ON u.`idUser` = p.`idPerson`
                    INNER JOIN bts_office o ON u.`idOffice` = o.`idOffice`
                    INNER JOIN `bts_group` g ON u.`idGroup` = g.`idGroup`
                    WHERE u.iduser=$idUser";
            $sqlSession = "SELECT sl.agent,sl.ip,DATE_FORMAT(sl.eventDate, '%m-%d-%Y') as eventDate, sl.eventDate,sl.sessionId,FROM_UNIXTIME(s.last_active) AS lastActive FROM bts_sessionlog sl
                            INNER JOIN bts_sessions s ON sl.sessionId = s.session_id
                            WHERE sl.idSessionLog =$idSessionlog";
            $userInfo = DB::query(Database::SELECT, $sql)->execute()->as_array();
            $sessionInfo = DB::query(Database::SELECT, $sqlSession)->execute()->as_array();
            $a_response['data'] = array('userInfo' => $userInfo, 'sessionInfo' => $sessionInfo);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_photoUpload() {
        $this->fnSYSUploadFileTMP(self::TMP_UPLOAD);
    }
    public function action_getInfoUser() {
        $this->fnSYSLoadClientLanguage();
        $this->auto_render = false;
        $ajax_view = new View('bts/private/admin/ajax/profile');
        $ajax_view->BTS_USER_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
        $ajax_view->BTS_USER_FULL_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_FULL_NAME);
        $ajax_view->BTS_SESSION_USER_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
        $ajax_view->BTS_SESSION_OFFICE_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID);
        $ajax_view->BTS_SESSION_OFFICE_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_NAME);
        $ajax_view->BTS_SESSION_CITY_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_ID);
        $ajax_view->BTS_SESSION_CITY_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_NAME);
        $ajax_view->BTS_GROUP_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_NAME);
        $ajax_view->BTS_SESSION_GROUP_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID);
        $ajax_view->BTS_SESSION_USER_EMAIL = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_EMAIL);
        $ajax_view->BTS_SESSION_DEFAULT_HOME = self::fnSYSGetSessionParameter(self::BTS_SESSION_DEFAULT_HOME);
        $ajax_view->BTS_OPTION_LANGUAGE = $this->fnSYSGetOptionValue(self::BTS_LANGUAGE);
        $ajax_view->ROOT_MIMIC_OPTION = (self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION) OR self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_MIMIC_OPTION));
        $ajax_view->USER_MIMIC_OPTION = self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC);
        echo $ajax_view;
    }
     /*
     * Funciones para Chat
     */

    public function action_getchatHeartbeat() {
        $this->auto_render = FALSE;
        $chatHistory = self::fnSYSGetSessionParameter('chatHistory');
        if (!isset($chatHistory)) {
            self::fnSYSSetSessionParameter('chatHistory', array());
        }
        $openChatBoxes = self::fnSYSGetSessionParameter('openChatBoxes');
        if (!isset($openChatBoxes)) {
            self::fnSYSSetSessionParameter('openChatBoxes', array());
        }
        $sql = "select * from bts_chat where (bts_chat.to = '" . self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME) . "' AND recd = 0) order by id ASC";
        $result = DB::query(Database::SELECT, $sql)->execute()->as_array();
        $items = '';
        $chatBoxes = array();
        $openChatBoxes = self::fnSYSGetSessionParameter('openChatBoxes');
        $chatHistory = self::fnSYSGetSessionParameter('chatHistory');
        foreach ($result as $chat) {
            if (!isset($openChatBoxes[$chat['from']]) && isset($chatHistory[$chat['from']])) {
                $items = $chatHistory[$chat['from']];
            }

            $chat['message'] = $this->sanitize($chat['message']);

            $items .= <<<EOD
					   {
			"s": "0",
			"f": "{$chat['from']}",
			"m": "{$chat['message']}"
	   },
EOD;

            if (!isset($chatHistory[$chat['from']])) {
                $chatHistory[$chat['from']] = '';
                $openChatBoxes[$chat['from']] = '';
            }

            $chatHistory[$chat['from']] .= <<<EOD
						   {
			"s": "0",
			"f": "{$chat['from']}",
			"m": "{$chat['message']}"
	   },
EOD;
            $tsChatBoxes = self::fnSYSGetSessionParameter('tsChatBoxes');
            unset($tsChatBoxes[$chat['from']]);
            $openChatBoxes[$chat['from']] = $chat['sent'];
        }

        if (!empty($openChatBoxes)) {
            foreach ($openChatBoxes as $chatbox => $time) {
                if (!isset($tsChatBoxes[$chatbox])) {
                    $now = time() - strtotime($time);
                    $time = date('g:iA M dS', strtotime($time));
                    $message = "Sent at $time";
                    if ($now > 180) {
                        $items .= <<<EOD
{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
                        if (!isset($chatHistory[$chatbox])) {
                            $chatHistory[$chatbox] = '';
                        }

                        $chatHistory[$chatbox] .= <<<EOD
		{
"s": "2",
"f": "$chatbox",
"m": "{$message}"
},
EOD;
                        $tsChatBoxes[$chatbox] = 1;
                    }
                }
            }
        }

        $sql = "update bts_chat set recd = 1 where bts_chat.to = '" . self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME) . "' and recd = 0";
        $query = DB::query(Database::UPDATE, $sql)->execute();
        if ($items != '') {
            $items = substr($items, 0, -1);
        }
        header('Content-type: application/json');
        ?>
        {
        "items": [
        <?php echo $items; ?>
        ]
        }

        <?php
        die();
    }

    public function listchatBoxSession($chatbox) {

        $chatHistory = self::fnSYSGetSessionParameter('chatHistory');
        $items = '';

        if (isset($chatHistory[$chatbox])) {
            $items = $chatHistory[$chatbox];
        }

        return $items;
    }

    public function action_getStartChatSession() {
        $this->auto_render = FALSE;
        $openChatBoxes = self::fnSYSGetSessionParameter('openChatBoxes');
        $items = '';
        if (!empty($openChatBoxes)) {
            foreach ($openChatBoxes as $chatbox => $void) {
                $items .= $this->listchatBoxSession($chatbox);
            }
        }
        if ($items != '') {
            $items = substr($items, 0, -1);
        }
        header('Content-type: application/json');
        ?>
        {
        "username": "<?php echo self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME); ?>",
        "items": [
        <?php echo $items; ?>
        ]
        }

        <?php
        die();
    }

    public function action_createsendChat() {
        $this->auto_render = FALSE;
        $from = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
        $chatHistory = self::fnSYSGetSessionParameter('chatHistory');
        $openChatBoxes = self::fnSYSGetSessionParameter('openChatBoxes');
        $tsChatBoxes = self::fnSYSGetSessionParameter('tsChatBoxes');
        $to = $_POST['to'];
        $message = $_POST['message'];

        $openChatBoxes[$_POST['to']] = date('Y-m-d H:i:s', time());

        $messagesan = $this->sanitize($message);

        if (!isset($chatHistory[$_POST['to']])) {
            $chatHistory[$_POST['to']] = '';
        }

        $chatHistory[$_POST['to']] .= <<<EOD
					   {
			"s": "1",
			"f": "{$to}",
			"m": "{$messagesan}"
	   },
EOD;


        unset($tsChatBoxes[$_POST['to']]);

        $sql = "insert into bts_chat  (bts_chat.from,bts_chat.to,message,sent) values ('" . $from . "', '" . $to . "','" . $message . "',NOW())";
        DB::query(Database::INSERT, $sql)->execute();
        echo "1";
        die();
    }

    public function action_createcloseChat() {
        $this->auto_render = FALSE;
        $openChatBoxes = self::fnSYSGetSessionParameter('openChatBoxes');
        unset($openChatBoxes[$_POST['chatbox']]);
        echo "1";
        die();
    }

    public function sanitize($text) {
        $text = htmlspecialchars($text, ENT_QUOTES);
        $text = str_replace("\n\r", "\n", $text);
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\n", "<br>", $text);
        return $text;
    }
    public function action_listActive(){
        try {
            $this->auto_render = FALSE;
            $o_cacheInstance = Cache::instance('apc');
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' us.idUser ';

            $s_where_search = "";
            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));
            $a_array_cols = array(
                "user_id" => "us.idUser"
                , "user_username" => "us.userName"
                , "user_full_name" => "pe.fullName"
                , "user_group" => "gr.name"
                , "user_office" => "of.name"                
            );

            $a_columns_search = array(
                "user_id" => "us.idUser"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_user us 
                INNER JOIN bts_group gr
                    ON us.idGroup = gr.idGroup
                INNER JOIN bts_office of
                    ON us.idOffice = of.idOffice
                INNER JOIN bts_person pe
                    ON us.idUser = pe.idPerson
            ';

            $s_where_conditions = " us.status = ".self::BTS_STATUS_DEACTIVE;              
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            $s_cacheKeyGlobalControllerAction = SITE_DOMAIN . $this->request->controller().$this->request->action();
            //x filtro
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action() . $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            $a_incacheObjectControllerAction = $o_cacheInstance->get($s_cacheKeyGlobalControllerAction);

            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL OR $a_incacheObjectControllerAction === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $o_cacheInstance->set($s_cacheKeyGlobalControllerAction, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }
    public function action_active(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_user = new Model_Bts_User($a_form_post_data['id']);

            if (!$o_model_user->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_user->status = self::BTS_STATUS_ACTIVE;
            $o_model_user->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();            
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_removeFinal(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_user = new Model_Bts_User($a_form_post_data['id']);

            if (!$o_model_user->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
           
            $o_model_user->delete();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}
