<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Option extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_OPTION', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_OPTION', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                    , array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );

        $v_admin_option = View::factory('bts/private/admin/option');
        $v_admin_option->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();
        $v_admin_option->BTS_A_OPTION_TYPE = array_merge($this->a_bts_option_type, $this->a_option_type);
//        $v_admin_option->BTS_A_OPTION_COMPONENT = array_merge($this->a_bts_option_component, $this->fnGetClassVariable('a_option_component'));
        $v_admin_option->BTS_A_OPTION_COMPONENT = $this->getEnumValuesFromTable('bts_option', 'component');

        $this->template->SYSTEM_BODY = $v_admin_option;
    }

    public function fnSYSOptionSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('foption_component', 'not_empty')
                ->rule('foption_type', 'not_empty')
                ->rule('foption_key', 'not_empty')
                ->rule('foption_description', 'not_empty')
                ->rule('foption_isroot', 'not_empty');

        //Check is ROOT
        $o_check_post->rule('foption_isroot', 'in_array', array(':value', array(self::BTS_STATUS_ACTIVE, self::BTS_STATUS_DEACTIVE)));

        //KEY regex
        $o_check_post->rule('foption_key', 'regex', array(':value', '/^[A-Z][A-Z_]+$/'));

        //Key DB
        $o_check_post->rule('foption_key', 'Model_Bts_Option::fnValidKey', array(':value', $this->request->post('foption_hidden_key')));

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {

                switch ($s_key) {
                    case 'foption_component':
                        $s_label_name = __("Componente");
                        break;
                    case 'foption_type':
                        $s_label_name = __("Tipo");
                        break;
                    case 'foption_key':
                        $s_label_name = __("Key");
                        break;
                    case 'foption_description':
                        $s_label_name = __("Descripcion");
                        break;
                    case 'foption_isroot':
                        $s_label_name = __("es Root");
                        break;
                    default:
                        break;
                }

                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    case 'in_array':
                        $a_error_labels[] = $s_label_name . ' ' . __("no acepta el valor enviado");
                        break;
                    case 'regex':
                        $a_error_labels[] = $s_label_name . ' ' . __("invalido");
                        break;
                    case 'Model_Bts_Option::fnValidKey':
                        $a_error_labels[] = $s_label_name . ' ' . __("duplicado");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();


        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_option = new Model_Bts_Option();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_option = new Model_Bts_Option($a_form_post_data['foption_hidden_key']);
            if (!$o_model_option->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }

        $s_value = null;
        switch ($a_form_post_data['foption_type']) {
            case self::BTS_OPTION_TYPE_BOOLEAN:
                $s_value = $a_form_post_data['foption_type_boolean'];
                break;
            case self::BTS_OPTION_TYPE_STRING:
                $s_value = $a_form_post_data['foption_type_string'];
                break;
            case self::BTS_OPTION_TYPE_COLOR:
                $s_value = $a_form_post_data['foption_type_color'];
                break;
            case self::BTS_OPTION_TYPE_INTEGER:
                $s_value = $a_form_post_data['foption_type_integer'];
                break;
            default:
                break;
        }

        $a_option_save = array(
            'key' => $a_form_post_data['foption_key']
            , 'component' => $a_form_post_data['foption_component']
            , 'description' => $a_form_post_data['foption_description']
            , 'longDescription' => $a_form_post_data['foption_long_description']
            , 'dataType' => $a_form_post_data['foption_type']
            , 'value' => $s_value
            , 'rootOption' => $a_form_post_data['foption_isroot']
            , 'prefix' => $a_form_post_data['foption_prefix']
            , 'suffix' => $a_form_post_data['foption_suffix']
        );

        $o_model_option->saveArray($a_option_save);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSOptionSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSOptionSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_saveOrder() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('sort', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'sort':
                            $s_label_name = __("Keys");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();


            foreach ($a_form_post_data['sort'] as $i => $s_key) {
                $o_model_option = new Model_Bts_Option($s_key);

                if (!$o_model_option->loaded())
                    throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

                $o_model_option->order = $i + 1;

                $o_model_option->save();
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_option = new Model_Bts_Option($a_form_post_data['id']);

            if (!$o_model_option->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_option->status = self::BTS_STATUS_DEACTIVE;

            $o_model_option->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_setroot() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_option = new Model_Bts_Option($a_form_post_data['id']);

            if (!$o_model_option->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_option->rootOption = ($o_model_option->rootOption == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_option->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' op.key ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "keya" => 'op.key'
                , "component" => "op.component"
                , "hidden_data_type" => "op.dataType"
                , "data_type" => "
                    CASE 
                        WHEN op.dataType = '" . self::BTS_OPTION_TYPE_BOOLEAN . "' THEN '" . __($this->a_bts_option_type[self::BTS_OPTION_TYPE_BOOLEAN]['display']) . "' 
                        WHEN op.dataType = '" . self::BTS_OPTION_TYPE_STRING . "' THEN '" . __($this->a_bts_option_type[self::BTS_OPTION_TYPE_STRING]['display']) . "' 
                        WHEN op.dataType = '" . self::BTS_OPTION_TYPE_COLOR . "' THEN '" . __($this->a_bts_option_type[self::BTS_OPTION_TYPE_COLOR]['display']) . "'
                        WHEN op.dataType = '" . self::BTS_OPTION_TYPE_INTEGER . "' THEN '" . __($this->a_bts_option_type[self::BTS_OPTION_TYPE_INTEGER]['display']) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END"
                , "prefix" => "IF(op.dataType = '" . self::BTS_OPTION_TYPE_STRING . "',op.prefix, '')"
                , "value" => "op.value"
                , "suffix" => "IF(op.dataType = '" . self::BTS_OPTION_TYPE_STRING . "',op.suffix, '')"
                , "is_root" => "op.rootOption"
            );

            $a_columns_search = array(
                "keya" => 'op.key'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_option op ';


            $s_where_conditions = " op.status = " . self::BTS_STATUS_ACTIVE . " ";

            $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_total_pages = 0;

            $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);

            $this->fnSYSResponseFormat(HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_option = new Model_Bts_Option($a_form_post_data['id']);

            if (!$o_model_option->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'option_key' => $o_model_option->key
                , 'option_component' => $o_model_option->component
                , 'option_data_type' => $o_model_option->dataType
                , 'option_value' => $o_model_option->value
                , 'option_is_root' => $o_model_option->rootOption
                , 'option_prefix' => $o_model_option->prefix
                , 'option_suffix' => $o_model_option->suffix
                , 'option_description' => $o_model_option->description
                , 'option_long_description' => $o_model_option->longDescription
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_listOrder() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('component', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    if ($s_key == 'component') {
                        $s_label_name = __("GRUPO");
                    }

                    if ($a_item[0] == 'not_empty') {
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $a_response['data'] = Model_Bts_Option::fnGetKeysByComponent($a_form_post_data['component']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_listValuesComponent() {
        $this->listEnumValues('bts_option', 'component');
    }

    public function action_updateEnumValuesComponent() {
        $this->alterEnumValues('bts_option', 'component');
    }

    public function action_share() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {

            $a_form_post_data = $o_check_post->data();

            $a_components = $this->getEnumValuesFromTable('bts_option', 'component');


            foreach ($a_form_post_data['fshare_instances'] as $i_local_id) {
                $o_model_file = new Model_Bts_Option($i_local_id);

                if (!$o_model_file->loaded())
                    $a_error_labels[] = __("Registro no encontrado") . ' ID:' . $i_local_id;

                foreach ($a_form_post_data['fshare_hosts'] as $s_instance) {
                    $this->alterEnumValuesForTable('bts_option', 'component', $a_components, $s_instance);
                }
                foreach ($a_form_post_data['fshare_hosts'] as $s_instance) {
                    $o_model_instance_option = new Model_Bts_Option();
                    $o_model_instance_option->saveInstance($s_instance, $o_model_file);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function listEnumValues($tableName, $columName) {
        $a_response = $this->a_bts_json_response;
        try {
            $this->fnSYSCleaningAllCache();
            $a_response['data'] = $this->getEnumValuesFromTable($tableName, $columName);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function alterEnumValues($tableName, $columName) {
        $a_response = $this->a_bts_json_response;
        try {
            $values = $this->request->post('values');
            $this->alterEnumValuesForTable($tableName, $columName, $values);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

}