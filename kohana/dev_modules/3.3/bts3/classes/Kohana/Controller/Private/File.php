<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_File extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_FILE', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_FILE', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                    , array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );
        $v_admin_file = View::factory('bts/private/admin/file');
        $v_admin_file->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();
        $v_admin_file->BTS_A_FILES = $this->a_bts_file_type;
        $this->template->SYSTEM_BODY = $v_admin_file;
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' file.idFile ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "file_key" => "fi.idFile"
                , "file_name" => "fi.fileName"
                , "file_type" => "fi.fileType"
            );

            $a_columns_search = array(
                "file_key" => "fi.key"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_file fi ';

            $s_where_conditions = " fi.status = " . self::BTS_STATUS_ACTIVE . " ";

            $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_total_pages = 0;

            $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);

            $this->fnSYSResponseFormat(HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function fnSYSFileSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('ffile_key', 'not_empty')
                ->rule('ffile_name', 'not_empty')
                ->rule('ffile_type', 'not_empty');

        //KEY regex
        $o_check_post->rule('ffile_key', 'regex', array(':value', '/^[A-Z][A-Z_]+$/'));

        //Key DB
        $o_check_post->rule('ffile_key', 'Model_Bts_File::fnValidKey', array(':value', $this->request->post('ffile_id')));

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'ffile_key':
                        $s_label_name = __("Key");
                        break;
                    case 'ffile_name':
                        $s_label_name = __("Nombre");
                        break;
                    case 'ffile_type':
                        $s_label_name = __("Tipo");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    case 'regex':
                        $a_error_labels[] = $s_label_name . ' ' . __("invalido");
                        break;
                    case 'Model_Bts_File::fnValidKey':
                        $a_error_labels[] = $s_label_name . ' ' . __("duplicado");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();


        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_file = new Model_Bts_File();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_file = new Model_Bts_File($a_form_post_data['ffile_id']);
            if (!$o_model_file->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }

        $a_file_save = array(
            'idFile' => $a_form_post_data['ffile_key']
            , 'fileName' => $a_form_post_data['ffile_name']
            , 'fileType' => $a_form_post_data['ffile_type']
            , 'isCoreFile' => key_exists('ffile_isCoreFile', $a_form_post_data) AND $a_form_post_data['ffile_isCoreFile'] == 'on' ? self::BTS_STATUS_ACTIVE : self::BTS_STATUS_DEACTIVE
        );

        $o_model_file->saveArray($a_file_save);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSFileSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSFileSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_file = new Model_Bts_File($a_form_post_data['id']);

            if (!$o_model_file->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'file_key' => $o_model_file->idFile
                , 'file_name' => $o_model_file->fileName
                , 'file_type' => $o_model_file->fileType
                , 'file_isCoreFile' => $o_model_file->isCoreFile
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_file = new Model_Bts_File($a_form_post_data['id']);

            if (!$o_model_file->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_file->status = self::BTS_STATUS_DEACTIVE;

            $o_model_file->save();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_share() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {

            $a_form_post_data = $o_check_post->data();

            foreach ($a_form_post_data['fshare_instances'] as $i_local_id) {
                $o_model_file = new Model_Bts_File($i_local_id);

                if (!$o_model_file->loaded())
                    $a_error_labels[] = __("Registro no encontrado") . ' ID:' . $i_local_id;

                foreach ($a_form_post_data['fshare_hosts'] as $s_instance) {
                    $o_model_instance_file = new Model_Bts_File();
                    $o_model_instance_file->saveInstance($s_instance, $o_model_file);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

}