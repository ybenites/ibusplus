<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GetTask
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Gettask extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $i_task_id = $this->request->param('id');
        $s_title = $this->request->param('params');

        $o_model_task = new Model_Bts_PtmsTask($i_task_id);

        if (!$o_model_task->loaded())
            throw new Exception('ERROR - ' . __('Tarea no encontrada'), self::BTS_CODE_ERROR);
        if (helperFile::fnToAscii($o_model_task->title, array("'")) != $s_title)
            throw new Exception('ERROR - ' . __('Tarea no encontrada'), self::BTS_CODE_ERROR);

        $this->fnSYSAddScripts(self::fnSYSGetFilePage('HELPDESK_GETTASK', self::BTS_FILE_TYPE_CSS));
        $this->fnSYSAddScripts(self::fnSYSGetFilePage('HELPDESK_GETTASK', self::BTS_FILE_TYPE_JS));

        $a_user_create = array();

        if ($o_model_task->createType == self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER) {
            $o_model_user = new Model_Bts_User($o_model_task->userCreate);
            $a_user_create['user_name'] = $o_model_user->person->fullName;
            $a_user_create['user_image'] = $o_model_user->imgProfile;
        } elseif ($o_model_task->createType == self::BTS_HELP_TASK_CREATE_TYPE_SYSTEM) {
            $o_model_user = new Model_BTS_PtmsUser($o_model_task->userCreate);
            $a_user_create['user_name'] = $o_model_user->person->fullName;
            $a_user_create['user_image'] = $o_model_user->imgProfile;
        } elseif ($o_model_task->createType == self::BTS_HELP_TASK_CREATE_TYPE_EMAIL) {
            $a_user_create['user_name'] = 'Email';
            $a_user_create['user_image'] = 'none';
        }
        $a_select_total_commemt = DB::select(
                        array(DB::expr('COUNT(*)'), 'total')
                )
                ->from(array('pt_taskcomment', 'pc'))
                ->where('pc.idTask', '=', $o_model_task->idTask)
                ->and_where('pc.userType', '=', self::BTS_HELP_TASK_CREATE_TYPE_SYSTEM)
                ->execute(self::BTS_HELP_TASK_INSTANCE_CONNECTION)
                ->current();

        $o_model_task_comment = new Model_Bts_PtmsTaskComment();

        $a_comments = $o_model_task_comment->fnGetCommentByTaskArray($o_model_task->idTask);

//            echo '<pre>';
//            
//            print_r($o_model_task->fnGetFilesArray());die();
        $a_task = array(
            'task_id' => $o_model_task->idTask
            , 'task_code' => $o_model_task->codeTask
            , 'task_title' => $o_model_task->title
            , 'task_title_ascii' => HelperFile::fnToAscii($o_model_task->title, array("'"))
            , 'task_description' => $o_model_task->description
            , 'task_user' => $a_user_create
            , 'task_total_comment' => $a_select_total_commemt['total']
            , 'task_status' => $o_model_task->status
            , 'task_create_date' => strtotime($o_model_task->creationDate)
            , 'task_comments' => $a_comments
            , 'task_tags' => $o_model_task->fnGetTagsArray()
            , 'task_files' => $o_model_task->fnGetFilesArray()
        );
//            echo '<pre>';
//            print_r($a_task);die();
        $v_get_task = View::factory('bts/private/helpdesk/get_task');
        $v_get_task->a_task = $a_task;
        $this->template->SYSTEM_BODY = $v_get_task;
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSTaskComentSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSTaskComentSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('ftask_coment', 'not_empty')
                ->rule('ftask_idTask', 'not_empty');
        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'ftask_coment':
                        $s_label_name = __("Comentario");
                        break;
                    case 'ftask_idTask':
                        $s_label_name = __("Tarea");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        $a_form_post_data = $o_check_post->data();
        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_comment = new Model_Bts_PtmsTaskComment();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_comment = new Model_Bts_PtmsTaskComment();
            if (!$o_model_comment->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
        $o_model_task = new Model_Bts_PtmsTask($a_form_post_data['ftask_idTask']);
        if (!$o_model_task->loaded())
            throw new Exception('ERROR - ' . __('Ticket no encontrado'), self::CODE_ERROR);
        $a_task_coment_save = array(
            'idTask' => $o_model_task->idTask
            , 'idUser' => $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID)
            , 'comment' => trim($a_form_post_data['ftask_coment'])
            , 'userType' => self::BTS_HELP_TASK_CREATE_TYPE_CUSTOMER
        );
        $o_model_comment->saveArray($a_task_coment_save);

        $emailSend = "soporte@ibusplus.com";
        $config = $this->fnSYSGetConfig(NULL, FALSE);
        $personSend = $config->get(self::BTS_SYS_CONFIG_SITE_NAME);
        $website = $_SERVER['HTTP_HOST'];

        $view = "bts/private/emails/ptms_mail_task";
        $company = $config->get(self::BTS_SYS_CONFIG_SITE_NAME);
        $contactCompany = $config->get(self::BTS_SYS_CONFIG_CONTACT_COMPANY);
        $asunto = $o_model_task->status . ': #[' . $o_model_task->codeTask . '] ' . $o_model_task->title . ' [' . $company . ']';

        $contactAddress = $config->get(self::BTS_SYS_CONFIG_CONTACT_ADDRESS);
        $contactPhone = $config->get(self::BTS_SYS_CONFIG_CONTACT_PHONE);
        $o_model_person = new Model_Bts_Person($this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID));

        $msj = array(
            ":contactCompany" => $contactCompany
            , ":contactAddress" => $contactAddress
            , ":contactPhone" => $contactPhone
            , ":website" => $website
            , ":code" => $o_model_task->codeTask
            , ':title' => $o_model_task->title
            , ":client" => $company
            , ":project" => ""
            , ":typetask" => $o_model_task->type
            , ":severity" => $o_model_task->importance
            , ":priority" => $o_model_task->idPriority
            , ":assinedBy" => ""
            , ":createBy" => $o_model_person->fullName
            , ":description" => trim($s_comment_description)
        );
        $this->sendEmail($asunto, $o_model_person->email, $o_model_person->fullName, $view, $msj, $emailSend, $personSend);
    }

}

