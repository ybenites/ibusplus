<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Group extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_GROUP', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_GROUP', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );

        $v_admin_group = View::factory('bts/private/admin/group');

        $this->template->SYSTEM_BODY = $v_admin_group;
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;            
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');
            
            $o_cacheInstance = Cache::instance('apc');

            if (!$s_idx)
                $s_idx = 'gr.idGroup ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "group_id" => "gr.idGroup"
                , "group_name" => "gr.name"
                , "group_root_option" => "gr.rootOption"
                , "group_allow_chat" => "gr.allowChat"
                , "group_allow_notifications_show" => "gr.allowNotificationsShow"
                , "group_allow_notifications" => "gr.allowNotifications"
                , "group_status" => "gr.status"
            );

            $a_columns_search = array(
                "group_id" => "gr.idGroup"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_group gr ';

            $s_where_conditions = " TRUE AND gr.status=".self::BTS_STATUS_ACTIVE;
            
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            //x filtro
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action(). $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);            
        } catch (Exception $e_exc) {
            echo $this->fnSYSErrorHandling($e_exc);
        }
    }

    public function fnSYSGroupSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('fgroup_name', 'not_empty')
                ->rule('fgroup_default_page', 'not_empty');

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'fgroup_name':
                        $s_label_name = __("Nombre");
                        break;
                    case 'fgroup_default_page':
                        $s_label_name = __("Página");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();


        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_group = new Model_Bts_Group();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_group = new Model_Bts_Group($a_form_post_data['fgroup_id']);
            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }

        $a_file_save = array(
            'name' => $a_form_post_data['fgroup_name']
            , 'defaultHomePage' => $a_form_post_data['fgroup_default_page']
            , 'rootOption' => $a_form_post_data['fgroup_root_option']
            , 'allowChat' => $a_form_post_data['fgroup_allow_chat']
            , 'allowNotifications' => $a_form_post_data['fgroup_allow_notifications']
            , 'allowNotificationsShow' => $a_form_post_data['fgroup_allow_notifications_show']
        );

        $o_model_group->saveArray($a_file_save);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSGroupSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
            Database::instance()->commit();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSGroupSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'group_id' => $o_model_group->idGroup
                , 'group_name' => $o_model_group->name
                , 'group_default_page' => $o_model_group->defaultHomePage
                , 'group_root_option' => $o_model_group->rootOption
                , 'group_allow_chat' => $o_model_group->allowChat
                , 'group_allow_notifications' => $o_model_group->allowNotifications
                , 'group_allow_notifications_show' => $o_model_group->allowNotificationsShow
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changeRootOption() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->rootOption = ($o_model_group->rootOption == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changeAllowChat() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->allowChat = ($o_model_group->allowChat == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changeAllowNotifications() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->allowNotifications = ($o_model_group->allowNotifications == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changeAllowNotificationsShow() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->allowNotificationsShow = ($o_model_group->allowNotificationsShow == 1 ? self::BTS_STATUS_DEACTIVE : self::BTS_STATUS_ACTIVE);

            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_changeStatus() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->status =self::BTS_STATUS_DEACTIVE;

            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $s_cacheKey_action = SITE_DOMAIN . $this->request->controller().'listActive';
            $o_cacheInstance->delete($s_cacheKey_action);
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    
    public function action_listActive(){
        try {
            $this->auto_render = FALSE;
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');
            $o_cacheInstance = Cache::instance('apc');

            if (!$s_idx)
                $s_idx = 'gr.idGroup ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "group_id" => "gr.idGroup"
                , "group_name" => "gr.name"    
            );

            $a_columns_search = array(
                "group_id" => "gr.idGroup"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_group gr ';

            $s_where_conditions = " TRUE AND gr.status=".self::BTS_STATUS_DEACTIVE;
            
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            $s_cacheKeyGlobalControllerAction = SITE_DOMAIN . $this->request->controller().$this->request->action();
            //x filtro
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action() . $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            $a_incacheObjectControllerAction = $o_cacheInstance->get($s_cacheKeyGlobalControllerAction);

            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL OR $a_incacheObjectControllerAction === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $o_cacheInstance->set($s_cacheKeyGlobalControllerAction, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $this->fnSYSErrorHandling($e_exc);
        }
    }
    public function action_active(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_group->status =self::BTS_STATUS_ACTIVE;
     
            $o_model_group->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();            
            $o_cacheInstance->delete($s_cacheKey);
            $s_cacheKey_action = SITE_DOMAIN . $this->request->controller().'listActive';
            $o_cacheInstance->delete($s_cacheKey_action);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_removeFinal(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            
            $o_model_group = new Model_Bts_Group($a_form_post_data['id']);           

            if (!$o_model_group->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
           
            $o_model_group->delete();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}