<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Panel extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_PANEL', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_PANEL', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $v_admin_panel_option = View::factory('bts/private/admin/panel_option');
        $myar = DB::select()->from("bts_option")->order_by("component")->execute()->as_array();
        $v_admin_panel_option->ACTIVE = 1;
        $v_admin_panel_option->DEACTIVE = 0;
        $v_admin_panel_option->BTS_A_OPTION_COMPONENT = $this->getEnumValuesFromTable('bts_option', 'component');
        $v_admin_panel_option->BTS_OPTION_COMPONENT_GENERAL = $myar;

        $this->template->SYSTEM_BODY = $v_admin_panel_option;
    }

    public function action_updateValueOption() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('key', 'not_empty')
                    ->rule('val', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'key':
                            $s_label_name = __("Key");
                            break;
                        case 'val':
                            $s_label_name = __("Value");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_model_option = new Model_Bts_Option($a_form_post_data['key']);
            $o_model_option->updateOption($a_form_post_data['val']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

}