<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Controller extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';

        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_CONTROLLER', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_CONTROLLER', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $this->fnSYSAddScripts(
                array(
                    array(
                        'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                        'file_type' => 'js'
                    )
                    , array(
                        'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                        'file_type' => 'js'
                    )
                )
        );
        $v_admin_controller = View::factory('bts/private/admin/controller');

        $a_controllers = array();

        if ($dir_directories = opendir(realpath('..') . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Controller')) {
            while (false !== ($s_directory_name = readdir($dir_directories))) {
                if ($s_directory_name != "." && $s_directory_name != "..") {
                    //Files Names
                    if ($dir_files = opendir(realpath('..') . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . $s_directory_name)) {
                        while (false !== ($s_file_name = readdir($dir_files))) {
                            if ($s_file_name != "." && $s_file_name != "..") {
                                $s_controller_name = substr($s_file_name, 0, strlen($s_file_name) - 4);
                                //if ($s_controller_name != 'Kernel')
                                $a_controllers[] = array(
                                    'file' => $s_controller_name
                                    , 'directory' => $s_directory_name
                                );
                            }
                        }
                        closedir($dir_files);
                    }
                }
            }
            closedir($dir_directories);
        }

        $v_admin_controller->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();

        $v_admin_controller->BTS_A_CONTROLLERS = $a_controllers;

        $v_admin_controller->BTS_A_CONTROLLER_TYPES = $this->a_bts_controller_types;

        $this->template->SYSTEM_BODY = $v_admin_controller;
    }

    public function action_getActions() {

        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('directory', 'not_empty')
                    ->rule('controller', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'directory':
                            $s_label_name = __("Directorio");
                            break;
                        case 'controller':
                            $s_label_name = __("Controlador");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $a_methods = get_class_methods('Controller_' . $a_form_post_data['directory'] . '_' . $a_form_post_data['controller']);

            $a_actions = array();
            foreach ($a_methods as $s_action) {
                if (preg_match("/^action_[a-zA-Z]+/i", $s_action))
                    array_push($a_actions, substr($s_action, 7));
            }

            $a_action_response = array();

            foreach ($a_actions as $s_action) {
                $a_select_controller = DB::select(
                                        array('ctr.idController', 'controller_id')
                                )
                                ->from(array('bts_controller', 'ctr'))
                                ->where('ctr.directory', '=', $a_form_post_data['directory'])
                                ->and_where('ctr.controller', '=', $a_form_post_data['controller'])
                                ->and_where('ctr.action', '=', $s_action)
                                ->execute()->current();

                $a_action_response[] = array(
                    'action' => $s_action
                    , 'id' => $a_select_controller['controller_id']
                );
            }

            $a_response['data'] = $a_action_response;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSControllerSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('fcontroller_controller', 'not_empty')
                ->rule('fcontroller_directory', 'not_empty')
                ->rule('fcontroller_action', 'not_empty')
                ->rule('fcontroller_type', 'not_empty');

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'fcontroller_controller':
                        $s_label_name = __("Controlador");
                        break;
                    case 'fcontroller_directory':
                        $s_label_name = __("Directorio");
                        break;
                    case 'fcontroller_action':
                        $s_label_name = __("Accion");
                        break;
                    case 'fcontroller_type':
                        $s_label_name = __("Tipo");
                        break;
                    default :
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }

        //PARTE VALIDA
        $a_form_post_data = $o_check_post->data();

        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_controller = new Model_Bts_Controller();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_controller = new Model_Bts_Controller($a_form_post_data['fcontroller_id']);
            if (!$o_model_controller->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }

        $a_controller_save = array(
            'directory' => $a_form_post_data['fcontroller_directory']
            , 'controller' => $a_form_post_data['fcontroller_controller']
            , 'action' => $a_form_post_data['fcontroller_action']
            , 'type' => $a_form_post_data['fcontroller_type']
        );

        $o_model_controller->saveArray($a_controller_save);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSControllerSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_share() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            foreach ($o_check_post['fshare_instances'] as $i_local_id) {
                $o_model_controller = new Model_Bts_Controller($i_local_id);

                if (!$o_model_controller->loaded())
                    $a_error_labels[] = __("Registro no encontrado") . ' ID:' . $i_local_id;

                foreach ($o_check_post['fshare_hosts'] as $s_instance) {
                    $o_model_instance_controller = new Model_Bts_Controller();
                    $o_model_instance_controller->saveInstance($s_instance, $o_model_controller);
                }
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSControllerSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_list() {
        try {
            $this->auto_render = FALSE;
            $o_cacheInstance = Cache::instance('apc');
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' op.key ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "idController" => 'ctr.idController'
                , "directory" => "ctr.directory"
                , "controller" => "ctr.controller"
                , "action" => "ctr.action"
                , "type" => "
                    CASE 
                        WHEN ctr.type = '" . self::BTS_CONTROLLER_TYPE_READ . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_READ]['display']) . "' 
                        WHEN ctr.type = '" . self::BTS_CONTROLLER_TYPE_CREATE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_CREATE]['display']) . "' 
                        WHEN ctr.type = '" . self::BTS_CONTROLLER_TYPE_UPDATE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_UPDATE]['display']) . "'
                        WHEN ctr.type = '" . self::BTS_CONTROLLER_TYPE_DELETE . "' THEN '" . __($this->a_bts_controller_types[self::BTS_CONTROLLER_TYPE_DELETE]['display']) . "'
                    ELSE ctr.type END"
            );

            $a_columns_search = array(
                    // "keya" => 'ctr.'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str, $a_columns_search);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' bts_controller ctr ';
            $s_where_conditions = " ctr.status =  " . self::BTS_STATUS_ACTIVE . " ";
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            $s_cacheKey = SITE_DOMAIN . $this->request->controller().$this->request->action() . $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
            $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_controller = new Model_Bts_Controller($a_form_post_data['id']);

            if (!$o_model_controller->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $a_response['data'] = array(
                'controller_id' => $o_model_controller->idController
                , 'controller_directory' => $o_model_controller->directory
                , 'controller_controller' => $o_model_controller->controller
                , 'controller_action' => $o_model_controller->action
                , 'controller_type' => $o_model_controller->type
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_controller = new Model_Bts_Controller($a_form_post_data['id']);

            if (!$o_model_controller->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_controller->status = self::BTS_STATUS_DEACTIVE;
            $o_model_controller->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_report(){
        $this->auto_render = FALSE;
        echo "JAJAJA";
    }

}