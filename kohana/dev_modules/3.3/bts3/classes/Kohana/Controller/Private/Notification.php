<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Notification extends Kohana_Controller_Private_Admin {

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());

        try {
            $o_check_post->rule('lastNotification', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'lastNotification':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            $i_group_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID);
            $i_city_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_CITY_ID);
            $i_office_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID);
            $i_user_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);

            $a_user_group_message_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID);

            $s_group_message = '';

            if (count($a_user_group_message_id) > 0) {
                $s_group_message = ' OR nt.toIdGroupMessage IN (' . implode(',', $a_user_group_message_id) . ')';
            }

            $i_last_notification = $o_check_post['lastNotification'];

            $DIRECOTRY =
                    DIRECTORY_SEPARATOR . self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR .
                    $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN) . DIRECTORY_SEPARATOR
                    . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;
            $DIRECOTRY_BTS =
                    DIRECTORY_SEPARATOR . 'kernel' . DIRECTORY_SEPARATOR
                    . self::PHOTO_PROFILE_DIR . DIRECTORY_SEPARATOR
                    . self::PHOTO_UPLOAD_128_DIR . DIRECTORY_SEPARATOR;

            $a_select_messages = DB::select(
                                    array(
                                'nt.idNotification'
                                , 'idOut'
                                    )
                                    , array(
                                'nt.message'
                                , 'messageOut'
                                    )
                                    , array(
                                'ur.fullName'
                                , 'userRegistrationOut'
                                    )
                                    , array(
                                DB::expr("IF(us.imgProfile IS NULL,CONCAT(:DIRECTORY_BTS,'default.png'),CONCAT(:DIRECTORY,us.imgProfile))", array(':DIRECTORY_BTS' => $DIRECOTRY_BTS, ':DIRECTORY' => $DIRECOTRY))
                                , 'user_image'
                                    )
                                    , array(
                                DB::expr('UNIX_TIMESTAMP(nt.creationDate)')
                                , 'updateNotification'
                                    )
                                    , array(
                                DB::expr("DATE_FORMAT(nt.creationDate, '%d/%m/%Y %h:%i %p')")
                                , 'dateCreationOut'
                                    )
                                    , array(
                                "nt.toIdCity"
                                , 'cityOut'
                                    )
                                    , array(
                                "nt.toIdOffice"
                                , 'officeOut'
                                    )
                                    , array(
                                "nt.toIdUser"
                                , 'userOut'
                                    )
                                    , array(
                                "nt.toIdGroupMessage"
                                , 'groupMessageOut'
                                    )
                                    , array(
                                "nt.userCreate"
                                , 'userCreateOut'
                                    )
                                    , array(
                                'nt.idResponse'
                                , 'responseOut'
                                    )
                            )
                            ->from(array('bts_notification', 'nt'))
                            ->order_by('nt.creationDate', 'ASC')
                            ->join(array('bts_user', 'us'))
                            ->on('nt.userCreate', '=', 'us.idUser')
                            ->join(array('bts_person', 'ur'))
                            ->on('nt.userCreate', '=', 'ur.idPerson')
                            ->join(array('bts_person', 'u'), 'LEFT')
                            ->on('nt.toIdUser', '=', 'u.idPerson')
                            ->join(array('bts_office', 'o'), 'LEFT')
                            ->on('nt.toIdOffice', '=', 'o.idOffice')
                            ->join(array('bts_city', 'c'), 'LEFT')
                            ->on('nt.toIdCity', '=', 'c.idCity')
                            ->join(array('bts_group_notification', 'gn'), 'LEFT')
                            ->on('nt.toIdGroupMessage', '=', 'gn.idGroupNotification')
                            ->where(
                                    DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '>', (int) $i_last_notification
                            )
                            ->and_where(
                                    'nt.userCreate', '<>', intval($i_user_id)
                            )
                            ->and_where(
                                    DB::expr("
                                        CASE
                                            WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0 AND nt.toIdGroupMessage=0  THEN TRUE
                                        ")
                                    , 'ELSE', DB::expr("
                                            nt.toIdGroup = :ID_GROUP
                                            OR nt.toIdCity = :ID_CITY 
                                            OR nt.toIdOffice = :ID_OFFICE 
                                            OR nt.toIdUser = :ID_USER 
                                            $s_group_message
                                        END"
                                            , array(
                                        ':ID_GROUP' => intval($i_group_id),
                                        ':ID_CITY' => intval($i_city_id),
                                        ':ID_OFFICE' => intval($i_office_id),
                                        ':ID_USER' => intval($i_user_id)
                                            )
                                    )
                            )
                            ->and_where('nt.state', '=', 1)
                            ->execute()->as_array();
            $a_response['data'] = array_merge(
                    $a_select_messages
                    , array(
                'date' => HelperNotification::fnGetCurrentDate()
                    )
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());

        try {
            $o_check_post->rule('lastNotification', 'not_empty');
            if (!$o_check_post->check())
                throw new Exception(__('Data inválida'), self::BTS_CODE_SUCCESS);

            $i_user_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);

            $a_post = $o_check_post->data();
            $i_last_notification = $a_post['lastNotification'];


            $o_update_notification = DB::update('bts_user')
                            ->value('lastDateMessage', DB::expr("FROM_UNIXTIME(:LAST_NOTIFICATION)", array(':LAST_NOTIFICATION' => $i_last_notification)))
                            ->where('idUser', '=', intval($i_user_id))->execute();
            $a_response['data'] = $o_update_notification;
            //$this->fncleaningFragmentCache(true);
        } catch (Exception $exc) {
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_previous() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());

        try {
            $o_check_post
                    ->rule('lastNotification', 'not_empty')
                    ->rule('n_page', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'lastNotification':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            $i_page = $this->request->post('n_page');
            $i_last_notification = $this->request->post('lastNotification');
            $a_response['data'] = HelperNotification::fnGetLastNotifications($i_page, $i_last_notification);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getUsers() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());

        try {
            $i_user_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
            $o_select_user_type = DB::select(
                            array(
                        'g.idGroup'
                        , 'to_group'
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_city')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_office')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_user')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_groupName')
                            )
                            , array(
                        'g.name',
                        DB::expr('value_search')
                            )
                            , array(
                        DB::expr("'" . __("Grupo") . "'"),
                        DB::expr('prefix')
                            )
                    )
                    ->from(
                            array('bts_group', 'g')
                    )
                    ->where('g.status', '=', 1)
                    ->and_where('g.idGroup', '>', 0)
                    ->order_by('value_search');

            $o_select_group_message = DB::select(
                            array(
                        DB::expr(0),
                        DB::expr('to_group')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_city'))
                            , array(
                        DB::expr(0),
                        DB::expr('to_office')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_user')
                            ), array(
                        'gn.idGroupNotification'
                        , 'to_groupName'
                            )
                            , array(
                        'gn.name',
                        DB::expr('value_search')
                            )
                            , array(
                        DB::expr("'" . __("Grupo Mensaje") . "'"),
                        DB::expr('prefix')
                            )
                    )
                    ->from(
                            array('bts_group_notification', 'gn')
                    )
                    ->and_where('gn.status', '=', 1);
            $o_select_city_messages = DB::select(
                            array(
                        DB::expr(0),
                        DB::expr('to_group')
                            ), array(
                        'c.idCity'
                        , 'to_city'
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_office')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_user')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_groupName')
                            )
                            , array(
                        'c.name',
                        DB::expr('value_search')
                            )
                            , array(
                        DB::expr("'" . __("Ciudad") . "'"),
                        DB::expr('prefix')
                            )
                    )
                    ->from(
                            array('bts_city', 'c')
                    )
                    ->where('c.status', '=', 1);

            $o_select_offices_messages = DB::select(
                            array(
                        DB::expr(0),
                        DB::expr('to_group')
                            ), array(
                        DB::expr(0)
                        , 'to_city'
                            )
                            , array(
                        'o.idOffice',
                        DB::expr('to_office')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_user')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_groupName')
                            )
                            , array(
                        'o.name',
                        DB::expr('value_search')
                            ), array(
                        DB::expr("'" . __("Oficina") . "'"),
                        DB::expr('prefix')
                            )
                    )
                    ->from(
                            array('bts_city', 'c')
                    )
                    ->join(
                            array('bts_office', 'o')
                    )
                    ->on('c.idCity', '=', 'o.idCity')
                    ->where('c.status', '=', 1)
                    ->and_where('o.status', '=', 1);

            $o_select_users_messages = DB::select(
                            array(
                        DB::expr(0),
                        DB::expr('to_group')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_city')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_office')
                            )
                            , array(
                        'u.idUser',
                        DB::expr('to_user')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_groupName')
                            )
                            , array(
                        'p.fullName',
                        DB::expr('value_search')
                            ), array(
                        DB::expr("'" . __("Usuario") . "'"),
                        DB::expr('prefix')
                            )
                    )
                    ->from(
                            array('bts_city', 'c')
                    )
                    ->join(
                            array('bts_office', 'o')
                    )
                    ->on('c.idCity', '=', 'o.idCity')
                    ->join(
                            array('bts_user', 'u')
                    )
                    ->on('o.idOffice', '=', 'u.idOffice')
                    ->join(
                            array('bts_person', 'p')
                    )
                    ->on('u.idUser', '=', 'p.idPerson')
                    ->where('c.status', '=', 1)
                    ->and_where('o.status', '=', 1)
                    ->and_where('u.status', '=', 1)
                    ->and_where('u.idUser', '<>', intval($i_user_id));
            $o_select_all_messages = DB::select(
                            array(
                        DB::expr(0),
                        DB::expr('to_group')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_city')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_office')
                            )
                            , array(
                        DB::expr(0),
                        DB::expr('to_user')
                            ), array(
                        DB::expr(0),
                        DB::expr('to_groupName')
                            )
                            , array(
                        DB::expr("'Todos'"),
                        DB::expr('value_search')
                            ), array(
                        DB::expr("'" . __("Todos") . "'"),
                        DB::expr('prefix')
                            )
                    )->from(DB::expr('DUAL'))
                    //->where(DB::expr("'".__("Todos")."'"), 'LIKE', DB::expr(":SEARCH", array(':SEARCH' => $sSearch)))
                    ->union($o_select_users_messages, true)
                    ->union($o_select_offices_messages, true)
                    ->union($o_select_city_messages, true)
                    ->union($o_select_group_message, true)
                    ->union($o_select_user_type, true);

            $a_result = $o_select_all_messages->execute()->as_array();
            $a_response['data'] = $a_result;

        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_save() {

        $a_response = $this->a_bts_json_response;
//        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());

        try {
            $o_check_post->rule('t_type_notification', 'not_empty');

            if (!$o_check_post->check())
                throw new Exception(__('Data inválida'), self::BTS_CODE_SUCCESS);

            $a_post = $o_check_post->data();

            $i_type_notification = $a_post['t_type_notification'];

            if ($i_type_notification == 1) {

                $o_check_post->rule('dlg_form_to_group', 'not_empty')
                        ->rule('dlg_form_to_city', 'not_empty')
                        ->rule('dlg_form_to_office', 'not_empty')
                        ->rule('dlg_form_to_user', 'not_empty')
                        ->rule('dlg_form_to_group_name', 'not_empty')
                        ->rule('dlg_form_message_content', 'not_empty');
                if (!$o_check_post->check())
                    throw new Exception(__('Data inválida'), self::BTS_CODE_SUCCESS);

                $i_user_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);

                $i_to_group = $a_post['dlg_form_to_group'];
                $i_to_city = $a_post['dlg_form_to_city'];
                $i_to_office = $a_post['dlg_form_to_office'];
                $i_to_user = $a_post['dlg_form_to_user'];
                $i_to_group_message = $a_post['dlg_form_to_group_name'];
                $s_message = $a_post['dlg_form_message_content'];

                $o_model_notification = new Model_Bts_Notification;
                $o_model_notification->toIdGroup = $i_to_group;
                $o_model_notification->toIdCity = $i_to_city;
                $o_model_notification->toIdOffice = $i_to_office;
                $o_model_notification->toIdUser = $i_to_user;
                $o_model_notification->toIdGroupMessage = $i_to_group_message;
                $o_model_notification->message = $s_message;
                $o_model_notification->state = 1;

                if ($o_model_notification->save()) {
                    $a_response['data'] = 1;
                } else {
                    $a_response['data'] = 0;
                }
            } elseif ($i_type_notification == 2) {
                $o_check_post
                        ->rule('f_instant_message_response', 'not_empty')
                        ->rule('f_instant_to_id', 'not_empty')
                        ->rule('f_instant_msg_cont', 'not_empty');
                if (!$o_check_post->check())
                    throw new Exception(__('Data inválida'), self::BTS_CODE_SUCCESS);

                $i_user_id = $this->fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);

                $i_to_group = 0;
                $i_to_city = 0;
                $i_to_office = 0;
                $i_to_groupMessage = 0;
                $i_to_user = $a_post['f_instant_to_id'];
                $s_message = $a_post['f_instant_msg_cont'];
                $i_response_id = $a_post['f_instant_message_response'];

                $o_model_notification = new Model_Bts_Notification;
                $o_model_notification->toIdGroup = $i_to_group;
                $o_model_notification->toIdCity = $i_to_city;
                $o_model_notification->toIdOffice = $i_to_office;
                $o_model_notification->toIdUser = $i_to_user;
                $o_model_notification->toIdGroupMessage = $i_to_groupMessage;
                $o_model_notification->message = $s_message;
                $o_model_notification->idResponse = $i_response_id;
                $o_model_notification->state = 1;
                //$this->fncleaningFragmentCache(true);
                if ($o_model_notification->save()) {
                    $a_response['data'] = 1;
                } else {
                    $a_response['data'] = 0;
                }
            }
        } catch (Exception $exc) {
            $a_response['code'] = self::BTS_CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnSYSResponseFormat($a_response);
    }
       public function action_notificationsLayout() {
        $this->auto_render = false;
        $ajax_view = new View('bts/private/admin/ajax/notification');
        $ajax_view->BTS_USER_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
        $ajax_view->BTS_USER_FULL_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_FULL_NAME);
        $ajax_view->BTS_SESSION_USER_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
        echo $ajax_view;
    }

}