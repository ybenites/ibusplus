<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Kohana_Controller_Private_Section extends Kohana_Controller_Private_Admin {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {            
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN_SECTION', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN_SECTION', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $this->fnSYSAddScripts($incacheObjectJs);
        $this->fnSYSAddStyles($incacheObjectCss);
        $v_admin_section = View::factory('bts/private/admin/section');

        $v_admin_section->BTS_A_DB_CONFIGS = self::fnSYSGetArrayConfigs();
        $v_admin_section->BTS_A_PAGES_OBJ = Model_Bts_Page::fnGetAll();
        $v_admin_section->BTS_A_FILE_JS_OBJ = Model_Bts_File::fnGetByType(self::BTS_FILE_TYPE_JS);
        $v_admin_section->BTS_A_FILE_CSS_OBJ = Model_Bts_File::fnGetByType(self::BTS_FILE_TYPE_CSS);

        $this->template->SYSTEM_BODY = $v_admin_section;
    }

    public function action_getListFile() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }                    
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }
            
            $a_form_post_data = $o_check_post->data();

            $a_response['data'] =
                    array(
                        self::BTS_FILE_TYPE_JS => DB::select(
                                        array('se.idSection', 'section_id')
                                        , array('se.idPage', 'page_id')
                                        , array('se.idFile', 'file_id')
                                )
                                ->from(array('bts_section', 'se'))
                                ->join(array('bts_file', 'fi'))
                                ->on('se.idFile', '=', 'fi.idFile')
                                ->where('se.idPage', 'LIKE', $a_form_post_data['id'])
                                ->and_where('fi.fileType', '=', self::BTS_FILE_TYPE_JS)
                                ->and_where('se.status', '=', self::BTS_STATUS_ACTIVE)
                                ->order_by('se.fileOrder')
                                ->execute()
                                ->as_array()
                        , self::BTS_FILE_TYPE_CSS => DB::select(
                                        array('se.idSection', 'section_id')
                                        , array('se.idPage', 'page_id')
                                        , array('se.idFile', 'file_id')
                                )
                                ->from(array('bts_section', 'se'))
                                ->join(array('bts_file', 'fi'))
                                ->on('se.idFile', '=', 'fi.idFile')
                                ->where('se.idPage', 'LIKE', $a_form_post_data['id'])
                                ->and_where('fi.fileType', '=', self::BTS_FILE_TYPE_CSS)
                                ->and_where('se.status', '=', self::BTS_STATUS_ACTIVE)
                                ->order_by('se.fileOrder')
                                ->execute()
                                ->as_array()
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    
    public function action_saveFiles(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('fsection_page', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'fsection_page':
                            $s_label_name = __("Pagina");
                            break;
                        default:
                            break;
                    }                    
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerida");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_ERROR);
            }
                        
            $a_form_post_data = $o_check_post->data();
            
            Database::instance()->begin();
            $a_model_section = new Model_Bts_Section;
            
            $a_model_section->clearFile($a_form_post_data['fsection_page']);
            if(!empty($a_form_post_data['fsection_js']))
                $a_model_section->saveFiles($a_form_post_data['fsection_js'], $a_form_post_data['fsection_page']);            
            if(!empty($a_form_post_data['fsection_css']))
            $a_model_section->saveFiles($a_form_post_data['fsection_css'], $a_form_post_data['fsection_page']);
            
            Database::instance()->commit();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_share() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {            
            $o_check_post
                    ->rule('fsection_page', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'fsection_page':
                            $s_label_name = __("Pagina");
                            break;
                        default:
                            break;
                    }                    
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerida");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_ERROR);
            }
            $a_form_post_data = $o_check_post->data();

            foreach ($a_form_post_data['fshare_hosts'] as $s_instance) {
                $o_model_instance_section = new Model_Bts_Section();
                $o_model_instance_section->saveInstance($s_instance, $a_form_post_data['fsection_page'], $a_form_post_data['a_check_js']);
                $o_model_instance_section->saveInstance($s_instance, $a_form_post_data['fsection_page'], $a_form_post_data['a_check_css']);
            }            
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}