<?php
defined('SYSPATH') or die('No direct script access.');
class Kohana_BtsOrm extends ORM {

    public function create(Validation $o_validation = NULL) {
        $a_columns = $this->_table_columns;

        if (isset($a_columns[Kohana_BtsConstants::BTS_TABLE_USER_CREATION])) {
            if ($this->{Kohana_BtsConstants::BTS_TABLE_USER_CREATION} === NULL)
                $this->{Kohana_BtsConstants::BTS_TABLE_USER_CREATION} = Session::instance(Kohana_BtsConstants::KOHANA_SESSION_NAME_DATABASE)
                ->get(Kohana_BtsConstants::BTS_SESSION_USER_ID);
        }
        if (isset($a_columns[Kohana_BtsConstants::BTS_TABLE_CREATION_DATE])) {
            $this->{Kohana_BtsConstants::BTS_TABLE_CREATION_DATE} = date("Y-m-d H:i:s");
        }
        if (isset($a_columns[Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE])) {
            if ($this->{Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE} === NULL)
                $this->{Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE} = Session::instance(Kohana_BtsConstants::KOHANA_SESSION_NAME_DATABASE)
                ->get(Kohana_BtsConstants::BTS_SESSION_USER_ID);
        }
        if (isset($a_columns[Kohana_BtsConstants::BTS_TABLE_LAST_CHANGE_DATE])) {
            $this->{Kohana_BtsConstants::BTS_TABLE_LAST_CHANGE_DATE} = date("Y-m-d H:i:s");
        }
        return parent::create($o_validation);
    }

    public function update(Validation $o_validation = NULL) {
        $a_columns = $this->_table_columns;
        if (isset($a_columns[Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE])) {
            if ($this->{Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE} === NULL)
                $this->{Kohana_BtsConstants::BTS_TABLE_USER_LAST_CHANGE} = Session::instance(Kohana_BtsConstants::KOHANA_SESSION_NAME_DATABASE)
                ->get(Kohana_BtsConstants::BTS_SESSION_USER_ID);
        }
        parent::update($o_validation);
    }

}