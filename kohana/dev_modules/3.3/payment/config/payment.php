<?php
defined('SYSPATH') or die('No direct script access.');
if (Kohana::$environment == Kohana::DEVELOPMENT) {
    $environment = 'sandbox';
}else
{
    $environment = 'live';
}
global $a_db_config;
return array(
    //linkpoint gateway
    'linkpoint' => array(
        // Linkpoint storenumber
        'storenumber' => '1909059978',
        //Keyfile path,should has 'r' permisson
        'keyfile' => 'c:/htdocs/linkpt.test/cert.pem',
        // Linkpoint environment: live, sandbox
        'environment' => 'sandbox',
    ),
    //paypal gateway
    'paypal' => array(
        // PayPal API and username
        'username' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_USER],
        'password' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_PASSWORD],
        // PayPal API signature
        'signature' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_SIGNATURE],
        //merchant id and email
        'merchant_email' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_EMAIL],
        'merchant_id' => 'PES2WUN4VA5UW',
        // PayPal environment: live, sandbox, beta-sandbox
        'environment' => $environment
        )
);
