<?php defined('SYSPATH') or die('No direct script access.');
/** 
 * Managing paypal instant payments
 * 
 * @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoDirectPayment
 * 
 * @author Alexey Geno <alexeygeno@gmail.com>
 * @package payment
 */
class Payment_Requester_Instant_Paypal extends Payment_Requester_Paypal implements Payment_Requester_Instant_Interface {
               
	/**
	* Does Paying order
	* @link https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_Requester_r_DoDirectPayment
	* @param Payment_Instant
	* @return Payment_Instant
	*/
	public function pay(Payment_Instant &$payment)
	{
		$empty_fields = $payment->empty_fields(array('billing_fname','billing_lname','amount','card_number','card_exp_month','card_exp_year'));
		if($empty_fields!==array())
		{
			throw new Payment_Exception(':fields_list fields are required for Paypal_Instant::Pay', array(':fields_list'=>implode(',',$empty_fields)));
		}

		//required fields
		$request = array(
			'IPADDRESS'=>  Request::$client_ip,
			'FIRSTNAME'=> $payment->billing_fname,
			'LASTNAME'=> $payment->billing_lname,
			'AMT' => $payment->amount,
			'ACCT' => $payment->card_number,
			'EXPDATE' => $payment->card_exp_month.$payment->card_exp_year,

		);
                  $request['PAYMENTTYPE'] = $payment->paymentType;
                  $request['CREDITCARDTYPE'] = $payment->creditCardType;
		if($payment->card_cvv)
		{
			$request['CVV2'] = $payment->card_cvv;
		}
		if($payment->billing_email)
		{
			$request['EMAIL'] = $payment->billing_email;
		}
		if($payment->billing_address)
		{
			$request['STREET'] = $payment->billing_address;
		}
		if($payment->billing_city)
		{
			$request['CITY'] = $payment->billing_city;
		}
		if($payment->billing_state)
		{
			$request['STATE'] = $payment->billing_state;
		}
		if($payment->billing_country)
		{
			$request['COUNTRYCODE'] = $payment->billing_country;
		}
		if($payment->billing_zip)
		{
			$request['ZIP'] = $payment->billing_zip;
		}
		if($payment->custom)
		{
			$request['CUSTOM'] = $payment->custom;
		}
		if(! empty($payment->item_id))
		{  
                        $request['ITEMAMT'] = $payment->amount;
			foreach ($payment->item_id as $key => $value)
                        {        
                            $request['L_NUMBER'.$key] = $key;
                            $request['L_NAME'.$key] = $value['L_NAMEn'];
                            $request['L_DESC'.$key] = $value['L_DESCn'];
                            $request['L_AMT'.$key] = $value['L_AMTn'];                            
                            $request['L_OPTIONSNAME'.$key] = $value['L_NAMEn'];                            
                            $request['L_OPTIONSVALUE'.$key] = $value['L_NAMEn'];                            
                            $request['L_QTY'.$key] = 1;
                        }                        
		}
                $request['DESC'] ='Compra de Tickets';
		$response = $this->_post('DoDirectPayment', $request,'instant','pay');                
		$payment->date = strtotime($response['TIMESTAMP']);
		$payment->txn_id = $response['TRANSACTIONID'];
		$payment->currency_code = $response['CURRENCYCODE'];
                $payment->log_id =0;
		return $payment;
	}
	/**
	* Does refund order
	* @param Payment_Instant
	* @return Payment_Instant
	*/
	public function  refund(Payment_Instant &$payment)
	{
		//Not implemented
		return $payment;
	}
         /** 
         * Anular una transaccion en Paypal 
         * @param string - $requestString 
         * @return array - returns the response 
         */ 
        public function sendRefundRequest($requestString) 
        {  
            $this->version = "51.0"; 
            $ch = curl_init();             
            curl_setopt($ch, CURLOPT_URL, $this->_api_url()); 
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($ch, CURLOPT_POST, 1); 
            // Set the API operation, version, and API signature in the request. 
            $reqStr = "METHOD=RefundTransaction&VERSION={$this->version}&PWD={$this->_password}&USER={$this->_username}&SIGNATURE={$this->_signature}$requestString"; 

            // Set the request as a POST FIELD for curl. 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $reqStr); 
            // Get response from the server. 
            $curlResponse = curl_exec($ch); 
            if(!$curlResponse) 
                return array("ERROR_MESSAGE"=>"RefundTransaction failed".curl_error($ch)."(".curl_errno($ch).")"); 

            // Extract the response details. 
            $httpResponseAr = explode("&", $curlResponse); 

            $aryResponse = array(); 
            foreach ($httpResponseAr as $i => $value) 
            { 
                $tmpAr = explode("=", $value); 
                if(sizeof($tmpAr) > 1) 
                { 
                    $aryResponse[$tmpAr[0]] = urldecode($tmpAr[1]); 
                } 
            } 

            if((0 == sizeof($aryResponse)) || !array_key_exists('ACK', $aryResponse)) 
                return array("ERROR_MESSAGE"=>"Invalid HTTP Response for POST request ($reqStr) to {$this->API_Endpoint}"); 

            return $aryResponse; 
        } 
        
         /** 
         * @param array $aryData 
         * @return array 
         */ 
        public function refundAmount($aryData) 
        { 
            if(trim(@$aryData['currencyCode'])=="") 
                return array("ERROR_MESSAGE"=>"Currency Code is Missing"); 
            if(trim(@$aryData['refundType'])=="") 
                return array("ERROR_MESSAGE"=>"Refund Type is Missing"); 
            if(trim(@$aryData['transactionID'])=="") 
                return array("ERROR_MESSAGE"=>"Transaction ID is Missing"); 

            $requestString = "&TRANSACTIONID={$aryData['transactionID']}&REFUNDTYPE={$aryData['refundType']}&CURRENCYCODE={$aryData['currencyCode']}";

            if(trim(@$aryData['invoiceID'])!="") 
                $requestString = "&INVOICEID={$aryData['invoiceID']}"; 

            if(isset($aryData['memo'])) 
                $requestString .= "&NOTE={$aryData['memo']}"; 

            if(strcasecmp($aryData['refundType'], 'Partial') == 0) 
            { 
                if(!isset($aryData['amount'])) 
                { 
                    return array("ERROR_MESSAGE"=>"For Partial Refund - It is essential to mention Amount"); 
                } 
                else 
                { 
                    $requestString = $requestString."&AMT={$aryData['amount']}"; 
                } 

                if(!isset($aryData['memo'])) 
                { 
                    return array("ERROR_MESSAGE"=>"For Partial Refund - It is essential to enter text for Memo"); 
                } 
            }                        
        $resCurl = $this->sendRefundRequest($requestString);
       if ($resCurl['ACK'] == "Success")
       {
        $data['recieved_data'] = serialize($resCurl);
        $data['txn_id'] = $resCurl['REFUNDTRANSACTIONID'];
        $data['amount'] = $resCurl['GROSSREFUNDAMT'];
        $data['type'] = 'requester';
        $data['gateway'] = 'paypal';
        $data['interface'] = 'instant';
        $data['action'] = 'cancel';
        $data['created_date'] = DB::expr('NOW()');
        $data['userCreate'] =  Session::instance('database')->get("user_id");
        $data['creationDate'] = date("Y-m-d H:i:s");
        $data['userLastChange'] =  Session::instance('database')->get("user_id");
        $data['lastChangeDate'] = date("Y-m-d H:i:s");
        $data['ip'] = Request::$client_ip;        
        $data['success'] = (bool) 1;
        $data['canceled'] = (bool) 1;
        DB::insert('bts_payment_logs', array_keys($data))->values(array_values($data))->execute();
       }   else{           
               throw new Kohana_Exception('PayPal API request for :method failed :error0 <b>(:code0)</b>',
                    array(
                        ':method' => $resCurl['L_SEVERITYCODE0'],
                        ':error0' => $resCurl['L_LONGMESSAGE0'],
                        ':code0' => $resCurl['L_ERRORCODE0']),'99999');
       }       
            return $resCurl; 
        } 
        
        
        function parse_payflow_string($str) {
        $workstr = $str;
        $out = array();

        while (strlen($workstr) > 0) {
            $loc = strpos($workstr, '=');
            if ($loc === FALSE) {
                // Truncate the rest of the string, it's not valid
                $workstr = "";
                continue;
            }

            $substr = substr($workstr, 0, $loc);
            $workstr = substr($workstr, $loc + 1); // "+1" because we need to get rid of the "="

            if (preg_match('/^(\w+)\[(\d+)]$/', $substr, $matches)) {
                // This one has a length tag with it.  Read the number of characters
                // specified by $matches[2].
                $count = intval($matches[2]);

                $out[$matches[1]] = substr($workstr, 0, $count);
                $workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
            } else {
                // Read up to the next "&"
                $count = strpos($workstr, '&');
                if ($count === FALSE) { // No more "&"'s, read up to the end of the string
                    $out[$substr] = $workstr;
                    $workstr = "";
                } else {
                    $out[$substr] = substr($workstr, 0, $count);
                    $workstr = substr($workstr, $count + 1); // "+1" because we need to get rid of the "&"
                }
            }
        }

        return $out;
    }

// run_payflow_call: Runs a Payflow API call.  $params is an associative array of
// Payflow API parameters.  Returns FALSE on failure, or an associative array of response
// parameters on success.
    function run_payflow_call($params) {
        global $environment;

        $paramList = array();
        foreach ($params as $index => $value) {
            $paramList[] = $index . "[" . strlen($value) . "]=" . $value;
        }

        $apiStr = implode("&", $paramList);
        global $a_db_config;
        
        $environment =$a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_GATEWAY];
        $paypal_environment = $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_ENVIRONMENT];
        
        // Which endpoint will we be using?
        if ($environment == "paypal_payflow")
        {
            if ($paypal_environment == "sandbox"){
                $endpoint = "https://pilot-payflowpro.paypal.com/";
            }else{
            $endpoint = "https://payflowpro.paypal.com";
            }
        }
        else
        {
            echo 'error';
            die();
        }
        // Initialize our cURL handle.
             $curl = curl_init($endpoint);
            curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($curl, CURLOPT_POST, 1);             
            curl_setopt($curl, CURLOPT_POSTFIELDS, $apiStr);
            $result = curl_exec($curl);
        if ($result === FALSE) {
            echo curl_error($curl);
            return FALSE;
        }
        else
            return $this->parse_payflow_string($result);
    }
        
	public function payPayFlow(Payment_Instant &$payment){
 $empty_fields = $payment->empty_fields(array('billing_fname','billing_lname','amount','card_number','card_exp_month','card_exp_year'));
		if($empty_fields!==array())
		{
			throw new Payment_Exception(':fields_list fields are required for Paypal_Instant::Pay', array(':fields_list'=>implode(',',$empty_fields)));
		}
// TRXTYPE A single character indicating the type of transaction to perform. Website Payments Pro Payflow Edition supports the following values:
//S = Sale transaction
//A = Authorization
//C = Credit
//D = Delayed Capture
//V = Void
//Tender The tender type
// (method of payment). 
// Values are: @ C = Credit card for Direct Payment transactions
//  @ P = PayPal for Express Checkout transactions
                //required fields
                global $a_db_config;
		$request = array(
                        "PARTNER" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PARTNER],
                        "VENDOR" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_VENDOR],
                        "USER" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_USER],
                        "PWD" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_PASSWORD],
                        "TENDER" => "C",
                        "TRXTYPE" => "S",
			'IPADDRESS'=>  Request::$client_ip,
			'FIRSTNAME'=> $payment->billing_fname,
			'LASTNAME'=> $payment->billing_lname,
			'AMT' => $payment->amount,
                        "CURRENCY" => "USD",                        
			'ACCT' => $payment->card_number,
			'EXPDATE' => $payment->card_exp_month.  substr($payment->card_exp_year,2),
                        "VERBOSITY"=>"Medium"                        
		);
                $request['PAYMENTTYPE'] = $payment->paymentType;
                $request['CREDITCARDTYPE'] = $payment->creditCardType;
		if($payment->card_cvv)
		{
			$request['CVV2'] = $payment->card_cvv;
		}
		if($payment->billing_email)
		{
			$request['EMAIL'] = $payment->billing_email;
		}
		if($payment->billing_address)
		{
			$request['BILLTOSTREET'] = $payment->billing_address;                         
		}
		if($payment->billing_city)
		{
			$request['BILLTOCITY'] = $payment->billing_city;
		}
		if($payment->billing_state)
		{			
			$request['BILLTOSTATE'] = $payment->billing_state;                                               
		}
		if($payment->billing_country)
		{			
			$request['BILLTOCOUNTRY'] = $payment->billing_country;
		}
		if($payment->billing_zip)
		{
			$request['BILLTOZIP'] = $payment->billing_zip;                                               
		}
		if($payment->commentone)
		{
			$request['COMMENT1'] = $payment->commentone;
		}
		if($payment->commenttwo)
		{
			$request['COMMENT2'] = $payment->commenttwo;
		}
		if($payment->custom)
		{
			$request['COMMENT2'] = $payment->commenttwo.' '.$payment->custom;
		}
		if(is_array($payment->item_id))
		{     
                        $request['ITEMAMT'] = $payment->amount;
			foreach ($payment->item_id as $key => $value)
                        {
                            $request['L_NUMBER'.$key] = $key;
                            $request['L_NAME'.$key] = $value['L_NAMEn'];
                            $request['L_DESC'.$key] = $value['L_DESCn'];
                            $request['L_AMT'.$key] = $value['L_AMTn'];                            
                            $request['L_OPTIONSNAME'.$key] = $value['L_NAMEn'];                            
                            $request['L_OPTIONSVALUE'.$key] = $value['L_NAMEn'];                            
                            $request['L_QTY'.$key] = 1;
                        }
		}
            $request['DESC'] ='Compra de Tickets';    
            $response = $this->run_payflow_call($request);         
          if(isset($response['PREFPSMSG']))
          {
              $PREFPSMSG = $response['PREFPSMSG'];
          }else{
              $PREFPSMSG='';
          }
          if(isset($response['PNREF']))
          {
              $PNREF = $response['PNREF'];
          }else{
              $PNREF='';
          }
         if ($response['RESULT'] != 0) {
        $data['recieved_data'] = serialize($response);
        $data['txn_id'] = '999999';
        $data['amount'] = $payment->amount;
        $data['type'] = 'requester';
        $data['gateway'] = 'paypal';
        $data['interface'] = 'payflow';
        $data['action'] = 'pay';
        $data['sent_data'] = serialize($request);
        $data['date'] = HelperDate::fnGetMysqlCurrentDateTime();
        $data['custom'] = $PREFPSMSG.' '.$PNREF.' '.$response['RESPMSG'].' '.$payment->custom;
        $data['created_date'] = DB::expr('NOW()');
        $idCurrency = Session::instance()->get('DEFAULT_CURRENCY');
        if ($idCurrency == NULL) {
            $idCurrency = Session::instance('database')->get('DEFAULT_CURRENCY');
        }
        $data['normalAmount'] = $payment->amount;
        $data['firstName'] =  $payment->billing_fname;
        $data['lastName'] = $payment->billing_lname;
        $data['address'] =$payment->billing_address;
        $data['city'] = $payment->billing_city;
        $data['country'] = $payment->billing_country;
        $data['zip'] = $payment->billing_zip;
        $data['creditCardNumber'] = $payment->card_number;
        $data['datecard'] = $payment->card_exp_month.$payment->card_exp_year;
        $data['tc'] = 0;  
        $data['paymentType'] = $payment->paymentType;       
        $data['creditCardType'] = $payment->creditCardType;
        $data['userCreate'] = Session::instance('database')->get("USER_ID");
        $data['creationDate'] = date("Y-m-d H:i:s");
        $data['userLastChange'] = Session::instance('database')->get("USER_ID");
        $data['lastChangeDate'] = date("Y-m-d H:i:s");
        $data['ip'] = Request::$client_ip;
        $data['success'] = 0;
        $resultInsert =DB::insert('bts_payment_logs', array_keys($data))->values(array_values($data))->execute();
        Database::instance()->commit();
            $keys = array_keys($response);
            if (in_array('L_LONGMESSAGE1', $keys) AND in_array('L_ERRORCODE1', $keys))
                throw new Kohana_Exception ('PayPal API request for :method failed: :error1 <b>(:code1)</b>. :error0 <b>(:code0)</b>',
                array (
                ':method' => $PNREF,
                ':error0' => $response['RESPMSG'],
                ':code0' => $response['HOSTCODE'],
                ':error1' => $PREFPSMSG),'99999'
            );
            else
            throw new Kohana_Exception('PayPal API request for Transaction :method failed, Status Transaction :error0 <b>(:code0)</b>',
                    array(
                        ':method' =>$PNREF,
                        ':error0' => $response['RESPMSG'],
                        ':code0' => $PREFPSMSG),'99999');
        }else{
            $response['amount']= $payment->amount;
            $response['currency_code']= 'USD';
        
        $data['recieved_data'] = serialize($response);
        $data['txn_id'] = $response['PNREF'];
        $data['amount'] = $response['amount'];
        $data['type'] = 'requester';
        $data['gateway'] = 'paypal';
        $data['interface'] = 'payflow';
        $data['action'] = 'pay';
        $data['sent_data'] = serialize($request);
        $data['date'] = HelperDate::fnGetMysqlCurrentDateTime();
        $data['custom'] = $payment->custom;
        $data['created_date'] = DB::expr('NOW()');
        $idCurrency = Session::instance()->get('DEFAULT_CURRENCY');
        if ($idCurrency == NULL) {
            $idCurrency = Session::instance('database')->get('DEFAULT_CURRENCY');
        }
        
        $data['normalAmount'] = $payment->amount;
        $data['firstName'] =  $payment->billing_fname;
        $data['lastName'] = $payment->billing_lname;
        $data['address'] =$payment->billing_address;
        $data['city'] = $payment->billing_city;
        $data['country'] = $payment->billing_country;
        $data['zip'] = $payment->billing_zip;
        $data['creditCardNumber'] = $payment->card_number;
        $data['datecard'] = $payment->card_exp_month.$payment->card_exp_year;
        $data['tc'] = 0;  
        $data['paymentType'] = $payment->paymentType;
        $data['creditCardType'] = $payment->creditCardType;
        $data['userCreate'] = Session::instance('database')->get("USER_ID");
        $data['creationDate'] = date("Y-m-d H:i:s");
        $data['userLastChange'] = Session::instance('database')->get("USER_ID");
        $data['lastChangeDate'] = date("Y-m-d H:i:s");
        $data['ip'] = Request::$client_ip;
        $data['success'] = 1;
        $resultInsert =DB::insert('bts_payment_logs', array_keys($data))->values(array_values($data))->execute();
        Session::instance('database')->set('log_id', $resultInsert[0]);// Venta en oficina
        Session::instance()->set('paypalLogID', $resultInsert[0]);// Venta Web
        Database::instance()->commit();
        }
           return $response;
        }
	public function payPayFlow_cancel($aryData){
                // TRXTYPE A single character indicating the type of transaction to perform. Website Payments Pro Payflow Edition supports the following values:
                //S = Sale transaction
                //A = Authorization
                //C = Credit
                //D = Delayed Capture
                //V = Void
                //Tender The tender type
                // (method of payment). 
                // Values are: @ C = Credit card for Direct Payment transactions
                //  @ P = PayPal for Express Checkout transactions
                //required fields
             global $a_db_config;
		$request = array(
                        "PARTNER" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_PARTNER],
                        "VENDOR" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_VENDOR],
                        "USER" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_USER],
                        "PWD" => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_PAYPAL_MERCHANT_WEB_PASSWORD],
                        "TENDER" => "C",
                        "TRXTYPE" => "C",
			'IPADDRESS'=>  Request::$client_ip,                        
                        "ORIGID" => "TEST",
                        "VERBOSITY"=>"Medium",
                        "ORIGID"=>$aryData['transactionID'],                        
                        "CURRENCY" => "USD",
		);
            $request['DESC'] ='Anulacion de Tickets';     
            if (isset ($aryData['amount'])){
            $request['AMT'] =$aryData['amount'];}
            $response = $this->run_payflow_call($request);            
          if(isset($response['RESPMSG']))
          {
              $PREFPSMSG = $response['RESPMSG'];
          }else{
              $PREFPSMSG='';
          }
          if(isset($response['PNREF']))
          {
              $PNREF = $response['PNREF'];
          }else{
              $PNREF='';
          }
         if ($response['RESULT'] != 0) {
            $keys = array_keys($response);
            if (in_array('L_LONGMESSAGE1', $keys) AND in_array('L_ERRORCODE1', $keys))
                throw new Kohana_Exception ('PayPal API request for :method failed: :error1 <b>(:code1)</b>. :error0 <b>(:code0)</b>',
                array (
                ':method' => $PNREF,
                ':error0' => $response['RESPMSG'],
                ':code0' => $response['HOSTCODE'],
                ':error1' => $PREFPSMSG),'99999'
            );
            else                
            throw new Kohana_Exception('PayPal API request for Transaction :method failed, Status Transaction :error0 <b>(:code0)</b>',
                    array(
                        ':method' =>$PNREF,
                        ':error0' => $response['RESPMSG'],
                        ':code0' => $PREFPSMSG),'99999');
        }else{
        $response['currency_code']= 'USD';        
        $data['recieved_data'] = serialize($response);        
        $data['sent_data'] = serialize($request);
        $data['txn_id'] = $response['PNREF'];
        $data['date'] = HelperDate::fnGetMysqlCurrentDateTime();
        $data['type'] = 'requester';
        $data['gateway'] = 'paypal';
        $data['interface'] = 'payflow';
        $data['action'] = 'cancel';
        $data['created_date'] = DB::expr('NOW()');
        $data['userCreate'] =  Session::instance('database')->get("USER_ID");
        $data['creationDate'] = date("Y-m-d H:i:s");
        $data['userLastChange'] =  Session::instance('database')->get("USER_ID");
        $data['lastChangeDate'] = date("Y-m-d H:i:s");
        $data['ip'] = Request::$client_ip;        
        $data['success'] = (bool) 1;
        $data['canceled'] = (bool) 1;        
        $resultInsert =DB::insert('bts_payment_logs', array_keys($data))->values(array_values($data))->execute();
        Session::instance('database')->set('log_id', $resultInsert[0]);// Venta en oficina
        Session::instance()->set('paypalLogID', $resultInsert[0]);// Venta Web
       
        }
            return $response;
        }
}