<?php

defined('SYSPATH') or die('No direct script access.');

class HelperMenu implements Kohana_BtsConstants {

    public static function  fnGetMenu($a_menu, $a_menu_active, $a_access, $a_options) {
            $s_return = '';
            $a_access_format = array_map(function($a_menu_item) {
                        return '/' . $a_menu_item['access_directory'] . '/' . $a_menu_item['access_controller'] . '/' . $a_menu_item['access_action'];
                    }, $a_access);

            foreach ($a_menu as $a_item) {
                $b_access = false;

                $a_url_parts = explode('/', $a_item['menu_url']);

                if (count($a_url_parts) > 3) {
                    $a_action_part = explode('.', $a_url_parts[3]);
                    array_pop($a_action_part);
                    $s_menu_url = '/' . $a_url_parts[1] . '/' . $a_url_parts[2] . '/' . implode('.', $a_action_part);

                    if (in_array($s_menu_url, $a_access_format))
                        $b_access = true;
                } else {
                    $b_access = true;
                }
                $b_option = true;

            if(!empty($a_item['menu_option_key'])){
                $b_option = ($a_options[$a_item['menu_option_key']]['value']==$a_item['menu_option_value']?true:false);                
                }

                if ($b_access AND $b_option AND $a_item['menu_module_ok'] == 1) {

                    $b_have_items = count($a_item['sub_menu']) > 0 ? true : false;

                    switch ($a_item['menu_type']) {
                        case self::BTS_MENU_TYPE_ITEM:
                            if ($b_have_items) {
                                if ($a_item['menu_super_id'] != '')
                                $s_return .= '<li class="dropdown-submenu'.(in_array($a_item['menu_id'], $a_menu_active)?' active':'').'">';
                                else
                                $s_return .= '<li class="dropdown'.(in_array($a_item['menu_id'], $a_menu_active)?' active':'').'">';
                            }else {
                            $s_return .= '<li'.(in_array($a_item['menu_id'], $a_menu_active)?' class="active"':'').'>';
                            }
                            $s_return .= '<a href="' . $a_item['menu_url'] . '" ' . ($b_have_items ? 'class="dropdown-toggle" data-toggle="dropdown"' : '') . '>';
                            $s_return .= ($a_item['menu_icon'] != '' ? '<b class="' . $a_item['menu_icon'] . '"></b> ' : '');
                            $s_return .= __($a_item['menu_name']);
                            $s_return .= ($b_have_items && $a_item['menu_super_id'] == '' ? ' <b class="caret"></b>' : '');
                            $s_return .= '</a>';
                            break;
                        case self::BTS_MENU_TYPE_HEADER:
                            $s_return .= '<li class="nav-header">' . __($a_item['menu_name']);
                            break;
                        case self::BTS_MENU_TYPE_SEPARATOR:
                            $s_return .= '<li class="divider">';
                            break;

                        default:
                            break;
                    }

                    if ($b_have_items) {
                        $s_return .= '<ul class="dropdown-menu">';
                    $s_return .= self::fnGetMenu($a_item['sub_menu'],$a_menu_active, $a_access, $a_options);
                        $s_return .= '</ul>';
                    }
                    $s_return .= '</li>';
                }
            }
        return $s_return;
        }
    public static function fnGetShorCuts() {
        $o_cache_instance = Cache::instance('apc');
        $s_cache_key = SITE_DOMAIN . 'shortCuts';
        $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        if ($a_in_cache_object === null) {
            $consulta = DB::select('bts_menu.iconShortcut', 'bts_menu.url', 'bts_menu.name')
                            ->from('bts_menu')
                            ->join('bts_module')->on('bts_menu.idModule', '=', 'bts_module.idModule')
                            ->where('bts_menu.withshortcut', '=', 1)
                            ->and_where('bts_module.status', '=', 1)
                            ->execute()->as_array();
            $return = '';
            foreach ($consulta as $icons) {
                $icon = $icons['iconShortcut'];
                $url = $icons['url'];
                $name = $icons['name'];
                global $a_db_config;
                $skin = $a_db_config[self::BTS_SYS_CONFIG_SKIN];
                $return.= '<a href="' . $url . '"><img src="/img/' . $skin . '/shortcuts/' . $icon . '" width="32" height="32"  title="'.__($name).'" /></a>';
            }
            $o_cache_instance->set($s_cache_key, $return);
            $a_in_cache_object = $o_cache_instance->get($s_cache_key);
        }
        return $a_in_cache_object;
    }

}