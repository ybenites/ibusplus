<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of function
 *
 * @author Hugo Casanova
 */
class HelperText {
    public static function randomKeys($in, $to_num = false, $pad_up = false, $passKey = null) {
        $index = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($passKey !== null) {
            // Although this function's purpose is to just make the
            // ID short - and not so much secure,
            // with this patch by Simon Franz (http://blog.snaky.org/)
            // you can optionally supply a password to make it harder
            // to calculate the corresponding numeric ID

            for ($n = 0; $n < strlen($index); $n++) {
                $i[] = substr($index, $n, 1);
            }

            $passhash = hash('sha256', $passKey);
            $passhash = (strlen($passhash) < strlen($index)) ? hash('sha512', $passKey) : $passhash;

            for ($n = 0; $n < strlen($index); $n++) {
                $p[] = substr($passhash, $n, 1);
            }

            array_multisort($p, SORT_DESC, $i);
            $index = implode($i);
        }

        $base = strlen($index);

        if ($to_num) {
            // Digital number  <<--  alphabet letter code
            $in = strrev($in);
            $out = 0;
            $len = strlen($in) - 1;
            for ($t = 0; $t <= $len; $t++) {
                $bcpow = bcpow($base, $len - $t);
                $out = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
            }

            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $out -= pow($base, $pad_up);
                }
            }
            $out = sprintf('%F', $out);
            $out = substr($out, 0, strpos($out, '.'));
        } else {
            // Digital number  -->>  alphabet letter code
            if (is_numeric($pad_up)) {
                $pad_up--;
                if ($pad_up > 0) {
                    $in += pow($base, $pad_up);
                }
            }

            $out = "";
            for ($t = floor(log($in, $base)); $t >= 0; $t--) {
                $bcp = bcpow($base, $t);
                $a = floor($in / $bcp) % $base;
                $out = $out . substr($index, $a, 1);
                $in = $in - ($a * $bcp);
            }
            $out = strrev($out); // reverse
        }

        return $out;
    }
    public static function makeRound($number, $decimal) {

        $factor = pow(10, $decimal);
        return (round($number * $factor) / $factor);
    }

    static function num2letras($num, $idCurrency, $fem = true, $dec = true) {
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande");                      
        $ifent = '00/100';
        if ($idCurrency != null) {
            $obj_currency = new Model_Currency($idCurrency);
            if ($obj_currency->loaded()) {
                if (($obj_currency->idCurrency == 1)) {
                    $gen = ' Dolares Americanos';
                }
                if (($obj_currency->idCurrency == 2)) {
                    $gen = ' Nuevos Soles';
                }
            } else {
                echo 'no existe moneda';
            }
        }
        $num = trim((string) @$num);
        if ($num[0] == '-') {
            $neg = 'menos ';
            $num = substr($num, 1);
        }
        else
            $neg = '';
        while ($num[0] == '0')
            $num = substr($num, 1);
        if ($num[0] < '1' or $num[0] > 9)
            $num = '0' . $num;
        $zeros = true;
        $punt = false;
        $ent = '';
        $fra = '';
        for ($c = 0; $c < strlen($num); $c++) {
            $n = $num[$c];
            if (!(strpos(".,'''", $n) === false)) {
                if ($punt)
                    break;
                else {
                    $punt = true;
                    continue;
                }
            } elseif (!(strpos('0123456789', $n) === false)) {
                if ($punt) {
                    if ($n != '0')
                        $zeros = false;
                    $fra .= $n;
                }
                else
                    $ent .= $n;
            }
            else
                break;
        }

        if ($dec and $fra and !$zeros) {
//            $fin = ' coma';
//            for ($n = 0; $n < strlen($fra); $n++) {
//                if (($s = $fra[$n]) == '0')
//                    $fin .= ' cero';
//                elseif ($s == '1')
//                    $fin .= $fem ? ' una' : ' un';
//                else
//                    $fin .= ' ' . $matuni[$s];
//            }
            $fin = $fra . '/100';
            $ifent = '';
        }
        else
            $fin = '';
        if ((int) $ent === 0)
            return 'Cero ' . $fin;

        $tex = self::calcLetrasNumeros($num, $ent, $fem);
        $tex = $neg . substr($tex, 1) . ' con ' . $ifent . $fin . $gen;
        return ucfirst($tex);
    }

    public static function calcLetrasNumeros($num, $ent, $fem) {
        $matuni[2] = "dos";
        $matuni[3] = "tres";
        $matuni[4] = "cuatro";
        $matuni[5] = "cinco";
        $matuni[6] = "seis";
        $matuni[7] = "siete";
        $matuni[8] = "ocho";
        $matuni[9] = "nueve";
        $matuni[10] = "diez";
        $matuni[11] = "once";
        $matuni[12] = "doce";
        $matuni[13] = "trece";
        $matuni[14] = "catorce";
        $matuni[15] = "quince";
        $matuni[16] = "dieciseis";
        $matuni[17] = "diecisiete";
        $matuni[18] = "dieciocho";
        $matuni[19] = "diecinueve";
        $matuni[20] = "veinte";
        $matunisub[2] = "dos";
        $matunisub[3] = "tres";
        $matunisub[4] = "cuatro";
        $matunisub[5] = "quin";
        $matunisub[6] = "seis";
        $matunisub[7] = "sete";
        $matunisub[8] = "ocho";
        $matunisub[9] = "nove";

        $matdec[2] = "veint";
        $matdec[3] = "treinta";
        $matdec[4] = "cuarenta";
        $matdec[5] = "cincuenta";
        $matdec[6] = "sesenta";
        $matdec[7] = "setenta";
        $matdec[8] = "ochenta";
        $matdec[9] = "noventa";
        $matsub[3] = 'mill';
        $matsub[5] = 'bill';
        $matsub[7] = 'mill';
        $matsub[9] = 'trill';
        $matsub[11] = 'mill';
        $matsub[13] = 'bill';
        $matsub[15] = 'mill';
        $matmil[4] = 'millones';
        $matmil[6] = 'billones';
        $matmil[7] = 'de billones';
        $matmil[8] = 'millones de billones';
        $matmil[10] = 'trillones';
        $matmil[11] = 'de trillones';
        $matmil[12] = 'millones de trillones';
        $matmil[13] = 'de trillones';
        $matmil[14] = 'billones de trillones';
        $matmil[15] = 'de billones de trillones';
        $matmil[16] = 'millones de billones de trillones';


        $sub = 0;
        $tex = '';
        $mils = 0;
        $neutro = false;
        $ent = '     ' . $ent;
        while (($num = substr($ent, -3)) != '   ') {
            $ent = substr($ent, 0, -3);
            if (++$sub < 3 and $fem) {
                $matuni[1] = 'una';
                $subcent = 'as';
            } else {
                $matuni[1] = $neutro ? 'un' : 'uno';
                $subcent = 'os';
            }
            $t = '';
            $n2 = substr($num, 1);
            if ($n2 == '00') {
                
            } elseif ($n2 < 21)
                $t = ' ' . $matuni[(int) $n2];
            elseif ($n2 < 30) {
                $n3 = $num[2];
                if ($n3 != 0)
                    $t = 'i' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }else {
                $n3 = $num[2];
                if ($n3 != 0)
                    $t = ' y ' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }
            $n = $num[0];
            if ($n == 1 AND $n2 == 0) {
                $t = ' cien' . $t;
            } elseif ($n == 1) {
                $t = ' ciento' . $t;
            } elseif ($n == 5) {
                $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
            } elseif ($n != 0) {
                $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
            }
            if ($sub == 1) {
                
            } elseif (!isset($matsub[$sub])) {
                if ($num == 1) {
                    $t = ' mil';
                } elseif ($num > 1) {
                    $t .= ' mil';
                }
            } elseif ($num == 1) {
                $t .= ' ' . $matsub[$sub] . '?n';
            } elseif ($num > 1) {
                $t .= ' ' . $matsub[$sub] . 'ones';
            }

            if ($num == '000')
                $mils++;
            elseif ($mils != 0) {
                if (isset($matmil[$sub]))
                    $t .= ' ' . $matmil[$sub];
                $mils = 0;
            }
            $neutro = true;
            $tex = $t . $tex;
            return $tex;
        }
    }

    public static function toAscii($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

    /* @var $a_months Array */

    protected static $a_months = array(
        1 => array(
            'short_name' => 'Ene'
            , 'full_name' => 'Enero'
        )
        , 2 => array(
            'short_name' => 'Fen'
            , 'full_name' => 'Febrero'
        )
        , 3 => array(
            'short_name' => 'Mar'
            , 'full_name' => 'Marzo'
        )
        , 4 => array(
            'short_name' => 'Abr'
            , 'full_name' => 'Abril'
        )
        , 5 => array(
            'short_name' => 'May'
            , 'full_name' => 'Mayo'
        )
        , 6 => array(
            'short_name' => 'Jun'
            , 'full_name' => 'Junio'
        )
        , 7 => array(
            'short_name' => 'Jul'
            , 'full_name' => 'Julio'
        )
        , 8 => array(
            'short_name' => 'Ago'
            , 'full_name' => 'Agosto'
        )
        , 9 => array(
            'short_name' => 'Set'
            , 'full_name' => 'Setiembre'
        )
        , 10 => array(
            'short_name' => 'Oct'
            , 'full_name' => 'Octubre'
        )
        , 11 => array(
            'short_name' => 'Nov'
            , 'full_name' => 'Noviembre'
        )
        , 12 => array(
            'short_name' => 'Dic'
            , 'full_name' => 'Diciembre'
        )
    );

    public static function fnGetDateUpdate($s_time) {
        $t_time = DateTime::createFromFormat('Y-m-d H:i:s', $s_time);
        $t_current_unix_date_f = new DateTime();
        $d_diff_date = $t_current_unix_date_f->getTimestamp() - $t_time->getTimestamp();

        $d_sec = 1;
        $d_min = 60 * $d_sec;
        $d_hour = $d_min * 60;
        $d_day = 24 * $d_hour;

        if ($d_diff_date / $d_day >= 1) {
            $d_date_update = $t_time;
            return $d_date_update->format('d') . ' ' . self::$a_months[$d_date_update->format('n')]['short_name'];
        }

        if ($d_diff_date / $d_hour >= 1) {
            $d_hour_update = $d_diff_date / $d_hour;
            $d_hour_update = intval($d_hour_update);
            if ($d_hour_update > 1) {
                return __('hace') . ' ' . $d_hour_update . ' ' . __('horas');
            } else {
                return __('hace') . ' ' . $d_hour_update . ' ' . __('hora');
            }
        }

        if ($d_diff_date / $d_min >= 1) {
            $d_min_update = $d_diff_date / $d_min;

            $d_min_update = intval($d_min_update);

            if ($d_min_update > 1) {
                return __('hace') . ' ' . $d_min_update . ' ' . __('minutos');
            } else {
                return __('hace') . ' ' . $d_min_update . ' ' . __('minuto');
            }
        }

        if ($d_diff_date / $d_sec >= 1) {
            $d_sec_update = $d_diff_date / $d_sec;

            $d_sec_update = intval($d_sec_update);

            if ($d_sec_update > 1) {
                return __('hace') . ' ' . $d_sec_update . ' ' . __('segundos');
            } else {
                return 'ahora';
            }
        }
    }
    public static function fnDateObjectFromStringFormat($s_date, $s_format) {
        return DateTime::createFromFormat($s_format, $s_date);
    }

    static function validateRuc($ruc) {
        $result = 0;
        if (!is_numeric($ruc)) {
            $result = false;
        }
        if (strlen(trim($ruc)) <> 11) {
            $result = false;
        }
        if ($result === 0) {
            $N1 = substr($ruc, 0, 1) * 5;
            $N2 = substr($ruc, 1, 1) * 4;
            $N3 = substr($ruc, 2, 1) * 3;
            $N4 = substr($ruc, 3, 1) * 2;
            $N5 = substr($ruc, 4, 1) * 7;
            $N6 = substr($ruc, 5, 1) * 6;
            $N7 = substr($ruc, 6, 1) * 5;
            $N8 = substr($ruc, 7, 1) * 4;
            $N9 = substr($ruc, 8, 1) * 3;
            $N10 = substr($ruc, 9, 1) * 2;
            $N11 = substr($ruc, 10, 1);
            $suma = $N1 + $N2 + $N3 + $N4 + $N5 + $N6 + $N7 + $N8 + $N9 + $N10;
            $residuo = $suma - (int) ($suma / 11) * 11;
            $resta = 11 - $residuo;
            if ($resta == 10) {
                $digChk = 0;
            } else if ($resta == 11) {
                $digChk = 1;
            } else {
                $digChk = (int) $resta;
            }
            if ($N11 == $digChk) {
                $result = true;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }
        return $result;
    }

    static function checkEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    static function getProtocol() {
        $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
        $p = explode('/', $_SERVER["SERVER_PROTOCOL"]);
        $protocol = strtolower($p[0]) . $s;
        return $protocol;
    }

    static function generateAlphanumCode($prevValue='') {
                $code = strtoupper(base_convert(rand(3, 20) . (date('dHis')) . rand(), 10, 36));
                $code = $prevValue. $code;
                return substr($code, 0, 16);
                break;        
    }

}