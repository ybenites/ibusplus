<?php

defined('SYSPATH') or die('No direct script access.');

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Clase loadLanguage
 *  ------------ Crea archivos de traduccion a partir de la Base de datos, tabla traduccion.
 * Funcion loadData($controllersArray,$language,$modoTraduccion)
 * $controllersArray: Es el arreglo de controladores a generar
 * $language : el idioma que quieres que genere es,fr,br,en
 * $modoTraduccion : 0 si es se desea el idioma base (es) ó 1 se desea el idioma base traducido (en)
 * $deleteFiles : si es 1 entonces elimina los archivos, antes de crearlos.
  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

class HelperLanguage {

    public static function loadData($language_file_name, $language_base, $language_target, $deleteFiles = false) {

        $language_file_name = $language_target . '/' . $language_file_name;
        if ($deleteFiles){ 
            HelperFile::deleteFileOrFolderRecursive('../application/i18n/' . $language_file_name . '.php');
        }
        if (!file_exists('../application/i18n/' . $language_file_name . '.php')) {
            $modoTraduccion = 1;       
            $o_translate = DB::select(
                    array('t.text', 'text')
                    , array('t.textTranslate', 'textTranslate')
                )
                ->from(array('bts_translate', 't'))
                
                ->where('t.language_base', '=', $language_base);
            if ($language_base != $language_target) {
                $modoTraduccion = 0;
                $o_translate
                ->and_where('t.language_target', '=', $language_target);
            }
            $a_translate = $o_translate->execute()->as_array();
            if (count($a_translate) > 0) {
                $controllerFile = fopen('../application/i18n/' . $language_file_name . '.php', 'a') or die("problemas al crear archivo");
                fputs($controllerFile, "<?php \n /*" . $language_file_name . ".php -- archivo de idiomas Kohana " . strtoupper($language_base) . " \n");
                fputs($controllerFile, " Author: Emmanuel Walter Cumpa Niño & Diana Miguel Saldaña \n");
                fputs($controllerFile, " Kohana Framework 3.3 Todos los derecho reservados */ \n\n");
                fputs($controllerFile, "defined('SYSPATH') or die('No direct script access.'); \n\n");
                fputs($controllerFile, "return array\n");
                fputs($controllerFile, "(\n");

                foreach($a_translate as $a_item) {                    
                    if ($modoTraduccion == 0) {
                        $text = $a_item["text"];
                        $textTranslate = $a_item["textTranslate"];
                    } else {
                        $text = $a_item["textTranslate"];
                        $textTranslate = $a_item["text"];
                    }
                    fputs($controllerFile, "\t'" . str_ireplace("'", "\'", $text) . "' => '" . str_ireplace("'", "\'", $textTranslate) . "', \n");
                }
                
                fputs($controllerFile, " \n); \n");
                fputs($controllerFile, " \n ");
                fclose($controllerFile);
                
            }
        }
    }

}

