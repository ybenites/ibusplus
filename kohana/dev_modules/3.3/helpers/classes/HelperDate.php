<?php

defined('SYSPATH') or die('No direct script access.');

class HelperDate {

    public static function fnGetMysqlCurrentDate() {
        return date('Y-m-d');
    }

    public static function fnGetMysqlCurrentDateTime() {
        return date('Y-m-d H:i:s');
    }
    
    public static function fnGetDateUpdate($s_time) {
        $t_time = DateTime::createFromFormat('Y-m-d H:i:s', $s_time);
        $t_current_unix_date_f = new DateTime();
        $d_diff_date = $t_current_unix_date_f->getTimestamp() - $t_time->getTimestamp();

        $d_sec = 1;
        $d_min = 60 * $d_sec;
        $d_hour = $d_min * 60;
        $d_day = 24 * $d_hour;

        if ($d_diff_date / $d_day >= 1) {
            $d_date_update = $t_time;
            return $d_date_update->format('d') . ' ' . self::$a_months[$d_date_update->format('n')]['short_name'];
        }

        if ($d_diff_date / $d_hour >= 1) {
            $d_hour_update = $d_diff_date / $d_hour;
            $d_hour_update = intval($d_hour_update);
            if ($d_hour_update > 1) {
                return __('hace') . ' ' . $d_hour_update . ' ' . __('horas');
            } else {
                return __('hace') . ' ' . $d_hour_update . ' ' . __('hora');
            }
        }

        if ($d_diff_date / $d_min >= 1) {
            $d_min_update = $d_diff_date / $d_min;

            $d_min_update = intval($d_min_update);

            if ($d_min_update > 1) {
                return __('hace') . ' ' . $d_min_update . ' ' . __('minutos');
            } else {
                return __('hace') . ' ' . $d_min_update . ' ' . __('minuto');
            }
        }

        if ($d_diff_date / $d_sec >= 1) {
            $d_sec_update = $d_diff_date / $d_sec;

            $d_sec_update = intval($d_sec_update);

            if ($d_sec_update > 1) {
                return __('hace') . ' ' . $d_sec_update . ' ' . __('segundos');
            } else {
                return 'ahora';
            }
        }
    }
}
