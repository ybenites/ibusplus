<?php
defined('SYSPATH') or die('No direct script access.');
/*
 * Funciones especiales

 */

/**
 * Funciones especiales para uso domestico
 *
 * @author Hardlick
 */
class HelperBrowser {

    /**
 * Obtiene el nombre del navegador y la version
 * return:     array con :  
 * $infoBrowser['browser_name']
 * $infoBrowser['browser_version'];
 * @author Hardlick
 */
 static function getBrowser()
    {
       $useragent = $_SERVER['HTTP_USER_AGENT'];
       $infoBrowser = array();
       if (preg_match('|MSIE ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'IE';
        } elseif (preg_match('|Opera ([0-9].[0-9]{1,2})|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Opera';
        } elseif (preg_match('|Firefox/([0-9\.]+)|', $useragent, $matched)) {
            $browser_version = $matched[1];
            $browser = 'Firefox';
        }
        elseif(preg_match('|Chrome/([0-9\.]+)|', $useragent, $matched))
        {
            $browser_version = $matched[1];
            $browser = 'Chrome';    
        } else {
            $browser_version = 0;
            $browser = 'other';
            
        }
        $infoBrowser['browser_name']=$browser;
        $infoBrowser['browser_version']=$browser_version;
        return $infoBrowser;

 }
}