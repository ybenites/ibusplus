<?php

defined('SYSPATH') or die('No direct script access.');
/*
 * Funciones especiales

 */

/**
 * Funciones especiales para uso domestico
 *
 * @author Hardlick
 */
class HelperCacheFile {

    /**
     * Funcion deleteAllCacheSesion
     * @author Hardlick
     */
    static function fndeleteAllCacheSesion($files) {
      
        try {
            foreach ($files as $value) {
                unlink($value);
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    /**
     * Funcion deleteAllCache
     * @author Hardlick
     */
    static function fndeleteAllCache($cacheDir=null) {        
        if ($cacheDir==null)
        {
        HelperFile::deleteFileOrFolderRecursive(APPPATH.'cache',FALSE);    
        }
        else
        {
        HelperFile::deleteFileOrFolderRecursive(APPPATH.'cache'.$cacheDir.'/',FALSE);    
        }        
    }

}