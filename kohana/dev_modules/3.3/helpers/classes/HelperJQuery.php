<?php

defined('SYSPATH') or die('No direct script access.');

class HelperJQuery {

    public static function fnJSONEncode($a_array_json, $b_is_array=true) {       
        
        $a_json_callback = Kohana::$config->load('jsonp_callback');
        
        if ($b_is_array)
            return (isset($_REQUEST[$a_json_callback['jsonp_callback']])?$_REQUEST[$a_json_callback['jsonp_callback']].'(':'' ) . json_encode($a_array_json) . (isset($_REQUEST[$a_json_callback['jsonp_callback']])?')':'');
        else
            return (isset($_REQUEST[$a_json_callback['jsonp_callback']])?$_REQUEST[$a_json_callback['jsonp_callback']].'(':'' ) . ($a_array_json) . (isset($_REQUEST[$a_json_callback['jsonp_callback']])?')':'');
    }

}
