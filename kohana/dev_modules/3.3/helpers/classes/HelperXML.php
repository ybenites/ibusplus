<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of xmlhelper
 *
 * @author Emmanuel
 */
class HelperXML {

    private static function array2XML(&$dom, &$xreport, $array_data) {
        foreach ($array_data as $k => $item) {
            //echo $k; die();
            $nameRow = (is_integer($k)) ? 'row' : $k;
            $row = $dom->createElement($nameRow);
            if (is_array($item)) {
                self::array2XML($dom, $row, $item);
            } else {
                $text = $dom->createTextNode($item);
                $row->appendChild($text);
            }
            $xreport->appendChild($row);
        }
    }

    public static function toXML($array_rows, $response_array,$rootElement='report' ) {
        $response_array['response'] = $array_rows;
        $dom = new DOMDocument('1.0');
        $xreport = $dom->createElement($rootElement);
        self::array2XML($dom, $xreport, $response_array);
        $dom->appendChild($xreport);
        return $dom;
    }

}