<?php

defined('SYSPATH') or die('No direct script access.');

class HelperNotification implements Kohana_BtsConstants {
    protected static $fnGetPrivilegesMessages = array();
    protected static $fnGetCountAfterNotification = array();

    public static function fnGetAllowNotifications() {
        //Write Notification
        $a_response = self::fnGetPrivilegesMessages();

        if ($a_response['allow_notifications'] == 1) {
            return true;
        }
        return false;
    }
    
    public static function fnGetAllowNotificationsShow() {
        //SHOW
        $a_response = self::fnGetPrivilegesMessages();

        if ($a_response['allow_notifications_show'] == 1) {
            return true;
        }
        return false;
    }

    public static function fnGetAllowChat() {

        $a_response = self::fnGetPrivilegesMessages();
        if ($a_response['allow_chat'] == 1) {
            return true;
        }
        return false;
    }

    public static function fnGetLastDateNotification() {
        try {
            $i_user_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_ID);

            $o_select_user = DB::select(
                                    array(
                                        DB::expr("UNIX_TIMESTAMP(COALESCE(u.lastDateMessage, '0000-00-00 00:00:00'))"), 'lastNotification'
                                    )
                            )
                            ->from(array('bts_user', 'u'))
                            ->where('u.idUser', '=', $i_user_id)
                            ->as_object()
                            ->execute()->current();
//            echo Database::instance()->last_query;
//            die();
            if ($o_select_user) {
                return $o_select_user->lastNotification;
            } else {
                return '';
            }
        } catch (Database_Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    /**
     * Total de mensajes que faltan mostrarse apartir de (Pagina)
     * 
     * @param int $iStart
     * @return object Totales 
     */
    public static function fnGetCountPreviousNotification($i_page = 0, $i_last_notification = 0) {
        try {
            $i_group_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_ID);
            $i_city_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_CITY_ID);
            $i_office_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_OFFICE_ID);
            $i_user_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_ID);

            $a_user_group_message_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID);
//echo '</div>';
//echo '</div>';
//echo '</div>';
//echo '</div>';
//print_r($a_user_group_message_id);die();
            $s_group_message = '';

            if (count($a_user_group_message_id)>0) {
                $s_group_message = ' OR nt.toIdGroupMessage IN ('.implode(',', $a_user_group_message_id).')';
            }
            
            $o_select_count_previous_messages = DB::select(
                            array(DB::expr('count(DISTINCT nt.idNotification)')
                                , 'totalPrevious')
                    )
                    ->from(array('bts_notification', 'nt'))
                    ->limit(self::BTS_MAX_ROW_LIMIT)
                    ->where(
                            DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '<=', intval(self::fnGetLastDateNotification())
                    )
                    ->and_where(
                            'nt.userCreate', '<>', intval($i_user_id)
                    )
                    ->and_where(
                            DB::expr("
                            CASE
                                WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0 THEN TRUE
                            ")
                                , 'ELSE', DB::expr(" 
                                        nt.toIdGroup = :ID_GROUP
                                        OR nt.toIdCity = :ID_CITY 
                                        OR nt.toIdOffice = :ID_OFFICE 
                                        OR nt.toIdUser = :ID_USER 
                                        $s_group_message
                            END", array(
                                    ':ID_GROUP' => intval($i_group_id),
                                    ':ID_CITY' => intval($i_city_id),
                                    ':ID_OFFICE' => intval($i_office_id),
                                    ':ID_USER' => intval($i_user_id)
                                        )
                                )
                           )
                    ->and_where('nt.state', '=', 1)
                    ->as_object()
                    ->execute()
                    ->current();
            //echo Database::instance()->last_query;die();
            if ($o_select_count_previous_messages) {
                $i_count_return = (($o_select_count_previous_messages->totalPrevious)
                        - (($i_page + 1) * self::BTS_MSG_COUNT_DISPLAY) - (self::fnGetCountAfterNotification($i_last_notification)));
                return $i_count_return < 0 ? 0 : $i_count_return;
            } else {
                return 0;
            }
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

    public static function fnGetCountAfterNotification($i_last_notification) {
        try {

            if (array_key_exists($i_last_notification, self::$fnGetCountAfterNotification)) {
                $a_tmp = self::$fnGetCountAfterNotification;
                $o_select_count_previous_messages = $a_tmp[$i_last_notification];
            } else {
                $i_group_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_ID);
                $i_city_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_CITY_ID);
                $i_office_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_OFFICE_ID);
                $i_user_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_ID);

                $a_user_group_message_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID);


                $s_group_message = '';

                if (count($a_user_group_message_id)>0) {
                    //$s_group_message = ' OR nt.toIdGroupMessage IN ('.implode(',', $a_user_group_message_id).')';
                }
                
                $o_select_count_previous_messages = DB::select(
                                array(DB::expr('count(DISTINCT nt.idNotification)')
                                    , 'totalAfter')
                        )
                        ->from(array('bts_notification', 'nt'))
                        ->limit(self::BTS_MAX_ROW_LIMIT)
                        ->where(
                                DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '>', $i_last_notification
                        )
                        ->and_where(
                                'nt.userCreate', '<>', intval($i_user_id)
                        )
                        ->and_where(
                                DB::expr("
                            CASE
                                WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0 
                                THEN TRUE
                            ")
                                , 'ELSE', DB::expr(" 
                                        me.toIdGroup = :ID_GROUP
                                        OR me.toIdCity = :ID_CITY 
                                        OR me.toIdOffice = :ID_OFFICE 
                                        OR me.toIdUser = :ID_USER 
                                        $s_group_message
                            END", array(
                                    ':ID_GROUP' => intval($i_group_id),
                                    ':ID_CITY' => intval($i_city_id),
                                    ':ID_OFFICE' => intval($i_office_id),
                                    ':ID_USER' => intval($i_user_id)
                                        )
                                )
                        )
                        ->and_where('nt.state', '=', 1)
                        ->as_object()
                        ->execute()
                        ->current();

                $a_tmp = self::$fnGetCountAfterNotification;

                $a_tmp[$i_last_notification] = $o_select_count_previous_messages;

                self::$fnGetCountAfterNotification = $a_tmp;
            }

            //echo Database::instance()->last_query;die();
            if ($o_select_count_previous_messages) {
                return $o_select_count_previous_messages->totalAfter < 0 ? 0 : $o_select_count_previous_messages->totalAfter;
            } else {
                return 0;
            }
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

    public static function fnGetLastNotifications($i_page_offset = 0, $i_last_notification = 0) {
        try {
            $i_group_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_ID);
            $i_city_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_CITY_ID);
            $i_office_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_OFFICE_ID);
            $i_user_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_USER_ID);
            
            $a_user_group_message_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_NOTIFICATION_ARRAY_ID);

            $s_group_message = '';

            if (count($a_user_group_message_id)>0) {
                              
               // $s_group_message = ' OR nt.toIdGroupMessage IN ('.implode(',', $a_user_group_message_id).')';
            }
            
            $i_count_page_offset = $i_page_offset * self::BTS_MSG_COUNT_DISPLAY;

            if ($i_last_notification == 0)
                $i_last_notification = self::fnGetLastDateNotification();

            $i_count_page_offset += ( intval(self::fnGetCountAfterNotification($i_last_notification)));

            $a_select_messages = DB::select(
                                    array(
                                'nt.idNotification'
                                , 'idOut'
                                    )
                                    , array(
                                'nt.message'
                                , 'messageOut'
                                    )
                                    , array(
                                'ur.fullName'
                                , 'userRegistrationOut'
                                    )
                                    , array(
                                DB::expr("REPLACE(us.imgProfile, '/128/', '/64/')")
                                , 'user_image'
                                    )
                                    , array(
                                DB::expr('UNIX_TIMESTAMP(nt.creationDate)')
                                , 'updateNotification'
                                    )
                                    , array(
                                DB::expr("DATE_FORMAT(nt.creationDate, '%d/%m/%Y %h:%i %p')")
                                , 'dateCreationOut'
                                    )
                                    , array(
                                "nt.toIdCity"
                                , 'cityOut'
                                    )
                                    , array(
                                "nt.toIdOffice"
                                , 'officeOut'
                                    )
                                    , array(
                                "nt.toIdUser"
                                , 'userOut'
                                    )
                                    , array(
                                "nt.userCreate"
                                , 'userCreateOut'
                                    ), array(
                                'nt.idResponse'
                                , 'responseOut'
                                    )
                            )
                            ->from(array('bts_notification', 'nt'))
                            ->limit(self::BTS_MSG_COUNT_DISPLAY)
                            ->offset($i_count_page_offset)
                            ->order_by('nt.creationDate', 'DESC')
                            ->join(array('bts_user', 'us'))
                            ->on('nt.userCreate', '=', 'us.idUser')
                            ->join(array('bts_person', 'ur'))
                            ->on('nt.userCreate', '=', 'ur.idPerson')
                            ->join(array('bts_person', 'u'), 'LEFT')
                            ->on('nt.toIdUser', '=', 'u.idPerson')
                            ->join(array('bts_office', 'o'), 'LEFT')
                            ->on('nt.toIdOffice', '=', 'o.idOffice')
                            ->join(array('bts_city', 'c'), 'LEFT')
                            ->on('nt.toIdCity', '=', 'c.idCity')
                            ->where(
                                    DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '<=', intval(self::fnGetLastDateNotification())
                            )          
                            ->and_where(
                                    DB::expr("
                                    CASE
                                        WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0 
                                        THEN TRUE
                                    ")
                                    , 'ELSE', DB::expr("
                                        nt.toIdGroup = :ID_GROUP
                                        OR nt.toIdCity = :ID_CITY 
                                        OR nt.toIdOffice = :ID_OFFICE 
                                        OR nt.toIdUser = :ID_USER 
                                        $s_group_message
                                    END"
                                            , array(
                                        ':ID_GROUP' => intval($i_group_id),
                                        ':ID_CITY' => intval($i_city_id),
                                        ':ID_OFFICE' => intval($i_office_id),
                                        ':ID_USER' => intval($i_user_id)
                                            )
                                    )
                            )
                            ->and_where('nt.state', '=', 1)
                            ->execute()->as_array();
            
//            echo '<pre>';
//            print_r($a_select_messages);
//            echo Database::instance()->last_query;
//            die();
//            
            return array_merge(
                            $a_select_messages
                            , array(
                        'previousCount' => self::fnGetCountPreviousNotification($i_page_offset, $i_last_notification)
                        , 'date' => self::fnGetCurrentDate()
                            )
            );
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

    public static function fnGetCurrentDate() {
        return array(
            'current_date' => date('Y-m-d H:i:s'),
            'current_unix_date' => time()
        );
    }

    public static function fnGetPrivilegesMessages() {
        try {
            if (count(self::$fnGetPrivilegesMessages) == 0) {
                $i_group_id = Session::instance(self::KOHANA_SESSION_NAME_DATABASE)->get(self::BTS_SESSION_GROUP_ID);
            
                $a_select = DB::select(
                                array('gr.allowChat', 'allow_chat')
                                , array('gr.allowNotifications', 'allow_notifications')
                                , array('gr.allowNotificationsShow', 'allow_notifications_show')
                        )
                        ->from(array('bts_group', 'gr'))
                        ->where('gr.idGroup', '=', $i_group_id)
                        ->execute()
                        ->current();
                self::$fnGetPrivilegesMessages = $a_select;
            }
            return self::$fnGetPrivilegesMessages;
        } catch (Exception $e_exc) {
            return array();
        }
    }

    //mensajes
    public static function fnGetLastDateNotificationMessage($i_user_id = null) {
        try {
            $o_select_user = DB::select(array(DB::expr("UNIX_TIMESTAMP(COALESCE(nt.creationDate, '0000-00-00 00:00:00'))"), 'lastNotification'))
                    ->from(array('bts_notification', 'nt'));

            if ($i_user_id != null) {
                $o_select_user->where('nt.userCreate', '=', $i_user_id);
            }
                $o_result = $o_select_user->order_by('nt.creationDate', 'DESC')
                            ->as_object()
                            ->execute()->current();

            if ($o_result) {
                return $o_result->lastNotification;
            } else {
                return '';
            }
        } catch (Database_Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public static function fngetInfoMessages($i_page_offset = 0, $i_last_notification = 0, $i_user_id = null, $s_date_format = '', $s_time_format = '') {

        try {

            $i_count_page_offset = $i_page_offset * self::BTS_MSG_COUNT_DISPLAY_TOTAL;

            if ($i_user_id != null) {
                if ($i_last_notification == 0)
                    $i_last_notification = self::fnGetLastDateNotificationMessage($i_user_id);
            }else {
                if ($i_last_notification == 0)
                    $i_last_notification = self::fnGetLastDateNotificationMessage();
            }


            $o_select_messages = DB::select(
                            array('nt.idNotification', 'idNotification')
                            , array('pr.fullName', 'sender')
                            , array('ur.idUser', 'idUserSender')
                            , DB::expr("IF(nt.toIdUser > 0,ud.idUser,0) AS idUserDestination")
                            , array('ur.imgProfile', 'img')
                            , DB::expr("DATE_FORMAT(nt.creationDate,'$s_date_format $s_time_format') as creation_dates")
                            , array(DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), 'lastNotification')
                            , array('nt.creationDate', 'creation_date')
                            , array(DB::expr(" 
                    CASE
                     WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0
                        THEN '" . __("todos") . "'
                     WHEN nt.toIdGroup > 0 AND nt.toIdCity = 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0
                        THEN CONCAT('" . __("grupo") . ": ', g.name )
                     WHEN nt.toIdGroup = 0 AND nt.toIdCity > 0 AND nt.toIdOffice = 0 AND nt.toIdUser = 0
                        THEN CONCAT('" . __("ciudad") . ": ', c.name)
                     WHEN nt.toIdGroup = 0 AND nt.toIdCity = 0 AND nt.toIdOffice > 0 AND nt.toIdUser = 0
                        THEN CONCAT('" . __("oficina") . ": ', o.name)
                     ELSE 
                     pd.fullName END"), 'destination')
                            , array('nt.message', 'message')
                    )
                    ->from(array('bts_notification', 'nt'))
                    ->limit(self::BTS_MSG_COUNT_DISPLAY_TOTAL)
                    ->offset($i_count_page_offset)
                    ->order_by('nt.creationDate', 'Desc')
                    ->join(array('bts_user', 'ur'))->on('nt.userCreate', '=', 'ur.idUser')
                    ->join(array('bts_person', 'pr'))->on('ur.idUser', '=', 'pr.idPerson')
                    ->join(array('bts_user', 'ud'), 'LEFT')->on('nt.toIdUser', '=', 'ud.idUser')
                    ->join(array('bts_person', 'pd'), 'LEFT')->on('ud.idUser', '=', 'pd.idPerson')
                    ->join(array('bts_city', 'c'), 'LEFT')->on('nt.toIdCity', '=', 'c.idCity')
                    ->join(array('bts_group', 'g'), 'LEFT')->on('nt.toIdGroup', '=', 'g.idGroup')
                    ->join(array('bts_office', 'o'), 'LEFT')->on('nt.toIdOffice', '=', 'o.idOffice')
                    ->where(DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '<=', intval($i_last_notification));
            if ($i_user_id != null) {
                $o_select_messages->where('nt.userCreate', '=', $i_user_id);
            }
            $a_result = $o_select_messages->execute()->as_array();
            return array_merge($a_result, array('previousCount' => self::fnGetCountMessages($i_page_offset, $i_last_notification, $i_user_id)), array('lastNotification' => self::fnGetLastDateNotificationMessage($i_user_id)));
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }

    public static function fnGetCountMessages($i_page = 0, $i_last_notification = 0, $i_user_id = null) {
        try {

            $i_page_num = ($i_page + 1) * self::BTS_MSG_COUNT_DISPLAY_TOTAL;

            $o_selectCount_previous_messages = DB::select(
                            array(DB::expr('count(DISTINCT nt.idNotification)'), 'totalPrevious')
                    )
                    ->from(array('bts_notification', 'nt'))
                    ->where(
                    DB::expr('UNIX_TIMESTAMP(nt.creationDate)'), '<=', intval($i_last_notification)
            );
            if ($i_user_id != null) {
                $o_selectCount_previous_messages->and_where('nt.userCreate', '=', $i_user_id);
            }

            $a_result = $o_selectCount_previous_messages->as_object()
                    ->execute()
                    ->current();

            if ($a_result) {
                return $a_result->totalPrevious - $i_page_num;
            } else {
                return 0;
            }
        } catch (Database_Exception $e_exc) {
            return $e_exc->getMessage();
        }
    }
}