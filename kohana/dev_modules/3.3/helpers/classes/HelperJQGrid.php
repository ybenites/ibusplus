<?php

defined('SYSPATH') or die('No direct script access.');

class HelperJQGrid {

    /**
     *
     * @param array(array(),...) $a_columns Arreglo en formato array(array('COLUMNA_1','ALIAS_1'),array('COLUMNA_2','ALIAS_2'))
     * @return String COLUMNAS EN FORMATO SQL =  COLUMNA_1 AS ALIAS_1, COLUMNA_2 AS ALIAS_2, ..., COLUMNA_N AS ALIAS_N
     */
    public static function fnArrayColsToSQL($a_columns) {
        $s_sql_cols = "";
        foreach ($a_columns as $s_key => $s_value) {
            $s_sql_cols .= $s_value . " AS " . $s_key;
            $s_sql_cols .=', ';
        }
        return substr($s_sql_cols, 0, strlen($s_sql_cols) - 2);
    }

    /**
     *
     * @param String $s_cols Columnas y sus alias en formato SQL
     * @param String $s_tables_join Relaciones entre las tablas en formato SQL
     * @param String $s_where_conditions Condiciones en formato SQL
     * @param String $s_where_search Condiciones adicionales desde el jqGrid
     * @param String $s_idx Columna de ordenamiento
     * @param String $s_ord Dirección del ordenamiento ASC o DESC
     * @param Srting $i_limit Valor para el LIMIT
     * @param String $i_start Valor para el OFFSET
     * @return array Resultado de la consulta
     */
    public static function fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start, $s_grouping='', $s_having='', $s_connection = 'default') {
//        print_r('SELECT ' . $s_cols . ' FROM ' . $s_tables_join . ' WHERE ' . $s_where_conditions . $s_where_search  . $s_grouping .  ' ORDER BY ' . $s_idx . ' ' . $s_ord . ' LIMIT ' . $i_limit . ' OFFSET ' . $i_start);
//        die();
        
        $s_limit = ' LIMIT '.$i_limit.' OFFSET '.$i_start;
        if($i_limit < 0){
            $s_limit = '';
        }
        return DB::query(
                Database::SELECT
                , 'SELECT ' . $s_cols 
                . ' FROM ' . $s_tables_join 
                . ' WHERE ' . $s_where_conditions . $s_where_search . ' ' . $s_grouping . ' ' . $s_having . ' ' 
                . ' ORDER BY ' . $s_idx . ' ' . $s_ord . $s_limit)->execute($s_connection);
    }

    /**
     *
     * @param String $s_cols Columnas y sus alias en formato SQL
     * @param String $s_tables_join Relaciones entre las tablas en formato SQL
     * @param String $s_where_conditions Condiciones en formato SQL
     * @param String $s_where_search Condiciones adicionales desde el jqGrid
     * @param String $s_grouping Sentencia GROUP BY personalizada
     * @param String $s_count_detail Sentencia dentro del COUNT($count_detail) personalizada
     * @return int Número de resultados de la consulta con las condiciones dadas.
     */
    public static function fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search='', $s_grouping='', $s_count_detail='*', $s_having=NULL, $s_connection = 'default') {
//        print_r('SELECT COUNT('.$s_count_detail.') AS total FROM ' . $s_tables_join . ' WHERE ' . $s_where_conditions . ' ' . $s_where_search . ' ' . $s_grouping);die();
        $s_sql_sub_query_pre = '';
        $s_sql_select = 'SELECT COUNT(' . $s_count_detail . ') AS total';
        $s_sql_sub_query_post = '';
        if ($s_having != NULL) {
            $s_sql_sub_query_pre = 'SELECT COUNT(*) AS total FROM ( ';
            $s_sql_select = ' SELECT ' . $s_cols . ' ';
            $s_sql_sub_query_post = ' ) tblTesting ';
        }
        $a_count = DB::query(
                Database::SELECT
                , $s_sql_sub_query_pre 
                . $s_sql_select 
                . ' FROM ' . $s_tables_join 
                . ' WHERE ' . $s_where_conditions . ' ' . $s_where_search . ' ' . $s_grouping . ' ' . $s_having . ' ' . $s_sql_sub_query_post)->execute($s_connection)->as_array();
        //print_r(Database::instance()->last_query);
        if (count($a_count) != 0)
            $a_count = $a_count[0]['total'];
        else
            $a_count = 0;
        return $a_count;
    }

    /**
     *
     * @param int $i_page Número de página del paginador jqGrid
     * @param int $i_count Número de resultados de la consulta con las condiciones dadas.
     * @param int $i_limit Número de resultados por página para el jqGrid
     * @param int $i_total_pages Número de páginas para el paginador, este párametro se pasa por referencia
     * @return int $start El siguiente número de fila que inicia en la siguiente página del paginador jqGrid
     */
    public static function fnCalculateStart($i_page, $i_count, $i_limit, &$i_total_pages) {
        if ($i_count > 0 && $i_limit > 0) {
            $i_total_pages = ceil($i_count / $i_limit);
        } elseif($i_count > 0){
            $i_total_pages = 1;
        } else{
            $i_total_pages = 0;            
        }
        if ($i_page > $i_total_pages)
            $i_page = $i_total_pages;
        $i_start = $i_limit * $i_page - $i_limit;

        if ($i_start < 0)
            $i_start = 0;
        return $i_start;
    }

    /**
     *
     * @param String $s_search_string
     * @return string Condiciones para la búsqueda en nu jqGrid
     */
    public static function fnConstructWhere($s_search_string, $a_column_array =null) {
        $s_qwery = "";
        //['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
        $a_qopers = array(
            'eq' => " = ",
            'ne' => " <> ",
            'lt' => " < ",
            'le' => " <= ",
            'gt' => " > ",
            'ge' => " >= ",
            'bw' => " LIKE ",
            'bn' => " NOT LIKE ",
            'in' => " IN ",
            'ni' => " NOT IN ",
            'ew' => " LIKE ",
            'en' => " NOT LIKE ",
            'cn' => " LIKE ",
            'nc' => " NOT LIKE ");
        if ($s_search_string) {
            $a_json_filter = json_decode($s_search_string, true);
            if (is_array($a_json_filter)) {
                $a_gopr = $a_json_filter['groupOp'];
                $a_rules = $a_json_filter['rules'];
                $i = 0;
                foreach ($a_rules as $s_key => $s_val) {
                    if ($a_column_array != NULL) {
                        $s_field = $a_column_array[$s_val['field']];
                    } else {
                        $s_field = $s_val['field'];
                    }
                    $s_op = $s_val['op'];
                    $s_v = $s_val['data'];
                    if ($s_v && $s_op) {
                        $i++;
                        // ToSql in this case is absolutley needed
                        $s_v = self::fnToSql($s_field, $s_op, $s_v);
                        if ($i == 1)
                            $s_qwery = " AND ";
                        else
                            $s_qwery .= " " . $a_gopr . " ";
                        switch ($s_op) {
                            // in need other thing
                            case 'in' :
                            case 'ni' :
                                $s_qwery .= $s_field . $a_qopers[$s_op] . " (" . $s_v . ")";
                                break;
                            default:
                                $s_qwery .= $s_field . $a_qopers[$s_op] . $s_v;
                        }
                    }
                }
            }
        }
        return $s_qwery;
    }

    /**
     *
     * @param String $s_field columna de la consulta
     * @param String $s_oper operador comparativo
     * @param any_type $m_val
     * @return String retorna en formato SQL las condiciones para un campo y operador recibido
     */
    private static function fnToSql($s_field, $s_oper, $m_val) {
        // we need here more advanced checking using the type of the field - i.e. integer, string, float
        switch ($s_field) {            
            default :
                //mysql_real_escape_string is better
                if ($s_oper == 'bw' || $s_oper == 'bn')
                    return "'" . addslashes($m_val) . "%'";
                else if ($s_oper == 'ew' || $s_oper == 'en')
                    return "'%" . addslashes($m_val) . "'";
                else if ($s_oper == 'cn' || $s_oper == 'nc')
                    return "'%" . addslashes($m_val) . "%'";
                else
                    return "'" . addslashes($m_val) . "'";
        }
    }

    public static function fnStrip($s_value) {
        if (get_magic_quotes_gpc() != 0) {
            if (is_array($s_value))
                if (self::fnArrayIsAssociative($s_value)) {
                    foreach ($s_value as $s_k => $s_v)
                        $a_tmp_val[$s_k] = stripslashes($s_v);
                    $s_value = $a_tmp_val;
                }
                else
                    for ($j = 0; $j < sizeof($s_value); $j++)
                        $s_value[$j] = stripslashes($s_value[$j]);
            else
                $s_value = stripslashes($s_value);
        }
        return $s_value;
    }

    private static function fnArrayIsAssociative($a_array) {
        if (is_array($a_array) && !empty($a_array)) {
            for ($i = count($a_array) - 1; $i; $i--) {
                if (!array_key_exists($i, $a_array)) {
                    return true;
                }
            }
            return!array_key_exists(0, $a_array);
        }
        return false;
    }

    public static function fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result_array, $a_array_cols, $b_include_id=false,$a_user_data=NULL) {
        $s_json = "";
        $s_json .= "{";
        $s_json .= "\"page\":\"$i_page\",";
        $s_json .= "\"total\":\"$i_total_pages\",";
        $s_json .= "\"records\":\"$i_count\",";
        $s_json .= "\"rows\": [";
        $s_rc = false;

        $col_alias_name = array_values($a_array_cols);
        $i_lenght = count($col_alias_name);

        $a_key_pk = array_keys($a_array_cols);
        $s_key_pk = $a_key_pk[0];

        if (!$b_include_id) {
            $a_array_cols = array_slice($a_array_cols, 1);
        }

        foreach ($a_result_array as $a_result_row) {
            if ($s_rc)
                $s_json .= ",";
            $s_json .= "{";
            $s_json .= "\"id\":\"" . $a_result_row[$s_key_pk] . "\",";
            $s_json .= "\"cell\":[";

            foreach ($a_array_cols as $s_key => $s_value) {
                $s_json .=self::fnGenerateJSONdata4jqGrid($a_result_row, $s_key);
            }

            $s_json = substr($s_json, 0, strlen($s_json) - 1);
            $s_json .= "]}";
            $s_rc = true;
        }
        $s_json .= "]";
        if (is_array($a_user_data))
            $s_json .= ",\"userdata\": " . json_encode($a_user_data);

        $s_json .= "}";

        return $s_json;
    }

    private static function fnGenerateJSONdata4jqGrid($result_row, $alias_name) {
        return "\"" . htmlspecialchars(self::fnNl2br2($result_row[$alias_name])) . "\",";
    }

    public static function fnNl2br2($string) {
        $string = str_replace(array("\r\n", "\r", "\n"), "", $string);
        return $string;
    }

}
