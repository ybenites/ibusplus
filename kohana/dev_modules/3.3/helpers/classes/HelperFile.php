<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HelperFile
 *
 * @author Emmanuel
 */
class HelperFile {

    /**
     *
     * @param String $file URL del arhivo o carpeta que se eliminará recursivamente
     */
    public static function deleteFileOrFolderRecursive($file, $deleteThisFileFolder = TRUE) {
        if (file_exists($file)) {
            if (is_dir($file)) {
                $handle = opendir($file);
                while ($filename = readdir($handle)) {
                    if ($filename != "." AND $filename != ".." AND strpos($filename, 'svn') === FALSE) {
                        HelperFile::deleteFileOrFolderRecursive($file . "/" . $filename);
                    }
                }
                closedir($handle);
                if ($deleteThisFileFolder)
                    rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

    public static function DIR_PATH_SEPARATOR() {
        if (strpos(PHP_OS, "WIN") !== false) {
            return $separator = '\\';
        } else {
            //LINUX
            return $separator = '/';
        }
    }
    
    public static function fnToAscii($str, $replace = array(), $delimiter = '-') {
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }

}