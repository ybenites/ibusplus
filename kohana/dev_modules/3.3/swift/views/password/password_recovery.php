<style type="text/css">
    .format div label{
        width: 205px;
        text-align: right;
        display: inline-block;
        float: left;
        margin-top: 4px;
        margin-right: 4px;
        font-weight: bold;
    }
    .format div p{
        padding-top: 4px;
    }
    .format div{
        margin-bottom: 5px;
    }
    .msg{
        font-size: 0.9em;
        text-align: justify;
        padding: 5px;
    }
</style>
<p class="msg">
    <?php echo __("El Sistema ha generado una contraseña nueva. Ud. mismo puede cambiar su contraseña una vez dentro del sistema.");?>
</p>
<div class="format">
    <div>
        <label><?php echo __("Usuario"); ?>:</label>
        <p>:user_name</p>
    </div>
    <div>
        <label><?php echo __("Contraseña"); ?>:</label>
        <p>:user_password</p>
    </div>
</div>
