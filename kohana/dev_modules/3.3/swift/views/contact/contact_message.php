<style type="text/css">
    .format div label{
        width: 205px;
        text-align: right;
        display: inline-block;
        float: left;
        margin-top: 4px;
        margin-right: 4px;
        font-weight: bold;
    }
    .format div p{
        padding-top: 4px;
    }
    .format div{
        margin-bottom: 5px;
    }
    .msg{
        font-size: 0.9em;
        text-align: justify;
        padding: 5px;
    }
</style>
<p class="msg">
    <?php echo __("Un nuevo mensaje ha sido enviado.");?>
</p>
<div class="format">
    <div>
                <label><?php echo __("Nombre"); ?>:</label>
                <p>:contact_name</p>
            </div>
            <div>
                <label><?php echo __("Email"); ?>:</label>
                <p>:contact_email</p>
            </div>
            <div>
                <label><?php echo __("Asunto"); ?>:</label>
                <p>:contact_subject</p>
            </div>
            <div>
                <label><?php echo __("Mensaje"); ?>:</label>
                <p>:contact_message</p>
            </div>             
</div>