<style type="text/css">
    .format div p{
        padding-top: 4px;
    }
    .format div{
        margin-bottom: 5px;
    }
</style>
<div class="format">
    <div>
        <p><?php echo __("Se produjo un error en el sistema. "); ?> :CLIENT</p>
        <p><?php echo __("A continuacion se detalla el problema:"); ?></p>
        <p>:CODE</p>
        <p>:CLIENT</p>
        <p>:USER</p>
        <p>:USER_ID</p>
        <p>:TIME</p>
        <p>:MSG</p>
        <p>:TRACE</p>
        <p>:PARAMS</p>
        <p>:CODE</p>
        <p>:LINE</p>
    </div>

</div>
