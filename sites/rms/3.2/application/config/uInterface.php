<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uInterface
 *
 * @author Emmanuel
 */
class rmsInterface implements Kohana_Uconstants, Rms_Constants {

    public function a_TYPE_CURRENCY() {
        return array(
            self::CURRENCY_SOLES => array(
                'display' => 'Soles'
                , 'value' => self::CURRENCY_SOLES
                , 'short' => 'NS'
                , 'default' => true
            )
            , self::CURRENCY_DOLARES => array(
                'display' => 'Dolares'
                , 'value' => self::CURRENCY_DOLARES
                , 'short' => 'DA'
            )
        );
    }

    public function a_TYPE_MOVE_STOCK() {
        return array(
            self::TYPE_MOVE_INGRESO => array(
                'display' => 'Ingreso'
                , 'value' => self::TYPE_MOVE_INGRESO
                , 'short' => 'Ingreso'
                , 'default' => true
            )
            , self::TYPE_MOVE_SALIDA => array(
                'display' => 'Salida'
                , 'value' => self::TYPE_MOVE_SALIDA
                , 'short' => 'Salida'
            )
        );
    }
    
    public function a_ALL_TYPE_MOVE() {
        return array(
            self::TYPE_MOVE_INGRESO => array(
                'display' => 'Ingreso'
                , 'value' => self::TYPE_MOVE_INGRESO
                , 'short' => 'Ingreso'
                , 'default' => true
            )
            , self::TYPE_MOVE_SALIDA => array(
                'display' => 'Salida'
                , 'value' => self::TYPE_MOVE_SALIDA
                , 'short' => 'Salida'
            )
            , self::TYPE_MOVE_TRAS_SALIDA => array(
                'display' => 'Trasferencia de Salida'
                , 'value' => self::TYPE_MOVE_TRAS_SALIDA
                , 'short' => 'Trasferencia de Salida'
            )
            , self::TYPE_MOVE_TRAS_INGRESO => array(
                'display' => 'Trasferencia de Ingreso'
                , 'value' => self::TYPE_MOVE_TRAS_INGRESO
                , 'short' => 'Trasferencia de Ingreso'
            )
            , self::TYPE_MOVE_VENTA_DIRECTA => array(
                'display' => 'Venta Directa'
                , 'value' => self::TYPE_MOVE_VENTA_DIRECTA
                , 'short' => 'Venta Directa'
            )
            , self::TYPE_MOVE_TRAS_SALIDA_CANCEL => array(
                'display' => 'Salida por Cancelacion'
                , 'value' => self::TYPE_MOVE_TRAS_SALIDA_CANCEL
                , 'short' => 'Salida por Cancelacion'
            )
            , self::TYPE_MOVE_VENTA_RESERVA => array(
                'display' => 'Movimiento por Reserva'
                , 'value' => self::TYPE_MOVE_VENTA_RESERVA
                , 'short' => 'Movimiento por Reserva'
            )
        );
    }

    public function a_UNIT_MEASURES() {
        $array = array(
            self::UNIT_MEASURES_KILOGRAMOS => array(
                'display' => 'Kilogramos'
                , 'value' => self::UNIT_MEASURES_KILOGRAMOS
                , 'short' => 'KG'
                , 'default' => true
            ),
            self::UNIT_MEASURES_LIBRAS => array(
                'display' => 'Libras'
                , 'value' => self::UNIT_MEASURES_LIBRAS
                , 'short' => 'LB'
                , 'default' => true
            ),
            self::UNIT_MEASURES_TONELADAS_LARGAS => array(
                'display' => 'Toneladas Largas'
                , 'value' => self::UNIT_MEASURES_TONELADAS_LARGAS
                , 'short' => 'TNL'
                , 'default' => true
            ),
            self::UNIT_MEASURES_TONELADAS_METRICAS => array(
                'display' => 'Toneladas Metricas'
                , 'value' => self::UNIT_MEASURES_TONELADAS_METRICAS
                , 'short' => 'TNM'
                , 'default' => true
            ),
            self::UNIT_MEASURES_TONELADAS_CORTAS => array(
                'display' => 'Toneladas Cortas'
                , 'value' => self::UNIT_MEASURES_TONELADAS_CORTAS
                , 'short' => 'TNC'
                , 'default' => true
            ),
            self::UNIT_MEASURES_GRAMOS => array(
                'display' => 'Gramos'
                , 'value' => self::UNIT_MEASURES_GRAMOS
                , 'short' => 'GR'
                , 'default' => true
            ),
            self::UNIT_MEASURES_UNIDADES => array(
                'display' => 'Unidades'
                , 'value' => self::UNIT_MEASURES_UNIDADES
                , 'short' => 'UND'
                , 'default' => true
            ),
            self::UNIT_MEASURES_LITROS => array(
                'display' => 'Litros'
                , 'value' => self::UNIT_MEASURES_LITROS
                , 'short' => 'L'
                , 'default' => true
            ),
            self::UNIT_MEASURES_GALONES => array(
                'display' => 'Galones'
                , 'value' => self::UNIT_MEASURES_GALONES
                , 'short' => 'GL'
                , 'default' => true
            ),
            self::UNIT_MEASURES_BARRILES => array(
                'display' => 'Barriles'
                , 'value' => self::UNIT_MEASURES_BARRILES
                , 'short' => 'BR'
                , 'default' => true
            ),
            self::UNIT_MEASURES_LATAS => array(
                'display' => 'Latas'
                , 'value' => self::UNIT_MEASURES_LATAS
                , 'short' => 'LT'
                , 'default' => true
            ),
            self::UNIT_MEASURES_CAJAS => array(
                'display' => 'Cajas'
                , 'value' => self::UNIT_MEASURES_CAJAS
                , 'short' => 'C'
                , 'default' => true
            ),
            self::UNIT_MEASURES_MILLARES => array(
                'display' => 'Millares'
                , 'value' => self::UNIT_MEASURES_MILLARES
                , 'short' => 'ML'
                , 'default' => true
            ),
            self::UNIT_MEASURES_METROS_CUBICOS => array(
                'display' => 'Metros Cúbicos'
                , 'value' => self::UNIT_MEASURES_METROS_CUBICOS
                , 'short' => 'MC'
                , 'default' => true
            ),
            self::UNIT_MEASURES_METROS => array(
                'display' => 'Metros'
                , 'value' => self::UNIT_MEASURES_METROS
                , 'short' => 'M'
                , 'default' => true
                ));
        foreach ($array as $key => $value) {
            if ($value['default'] == false) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    public function a_PERSON_DOCUMENT_TYPE() {
        return array(
            self::PERSON_DOCUMENT_TYPE_DNI => array(
                'display' => 'Dni'
                , 'value' => self::PERSON_DOCUMENT_TYPE_DNI
                , 'short' => 'DNI'
                , 'default' => true
            )
            , self::PERSON_DOCUMENT_TYPE_PASAPORTE => array(
                'display' => 'Pasaporte'
                , 'value' => self::PERSON_DOCUMENT_TYPE_PASAPORTE
                , 'short' => 'PAS'
            )
            , self::PERSON_DOCUMENT_TYPE_CARNET => array(
                'display' => 'Carnet'
                , 'value' => self::PERSON_DOCUMENT_TYPE_CARNET
                , 'short' => 'CAR'
            )
        );
    }

    //tipo de cliente

    public function a_clients_type() {
        return array(
            self::CLIENT_TYPE_PERSON => array(
                'display' => 'Persona'
                , 'value' => self::CLIENT_TYPE_PERSON
                , 'short' => 'PER'
                , 'default' => true
            )
            , self::CLIENT_TYPE_COMPANY => array(
                'display' => 'Empresa'
                , 'value' => self::CLIENT_TYPE_COMPANY
                , 'short' => 'COM'
                , 'default' => false
            )
        );
    }

    // TIPO DE DOCUMENTO 

    public $a_documents_type =
            array(
                self::DT_TICKET => array(
            'display' => 'Ticket'
            , 'contable' => self::TIPO_DOC_CONTABLE
            , 'value' => self::DT_TICKET
            , 'short' => 'TICKET'
            , 'default' => true
            , 'sales' => true
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_BOLETA => array(
            'display' => 'Boleta'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_BOLETA
            , 'short' => 'BOLETA'
            , 'default' => false
            , 'sales' => true
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_FACTURA => array(
            'display' => 'Factura'
            , 'contable' => self::TIPO_DOC_CONTABLE
            , 'value' => self::DT_FACTURA
            , 'short' => 'FACTURA'
            , 'default' => false
            , 'sales' => true
            , 'tax' => true
            , 'have_serie' => true
        ),       
        self::DT_RECIBO => array(
            'display' => 'Recibo'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_RECIBO
            , 'short' => 'RECIBO'
            , 'default' => false
            , 'sales' => true
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_NOTA_PEDIDO => array(
            'display' => 'Nota de Pedido'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_NOTA_PEDIDO
            , 'short' => self::DT_NOTA_PEDIDO
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_NOTA_INGRESO => array(
            'display' => 'Nota Ingreso'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_NOTA_INGRESO
            , 'short' => self::DT_NOTA_INGRESO
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_NOTA_SALIDA => array(
            'display' => 'Nota Salida'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_NOTA_SALIDA
            , 'short' => self::DT_NOTA_SALIDA
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        )
        ,
        self::DT_GUIA_REMISION => array(
            'display' => 'Guia Remisión'
            , 'contable' => self::TIPO_DOC_CONTABLE
            , 'value' => self::DT_GUIA_REMISION
            , 'short' => self::DT_GUIA_REMISION
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        )
        ,
        self::DT_ORDENCOMPRA => array(
            'display' => 'Orden Compra'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_ORDENCOMPRA
            , 'short' => self::DT_ORDENCOMPRA
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_RECIBO_INGRESO => array(
            'display' => 'Recibo de Ingreso'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_RECIBO_INGRESO
            , 'short' => self::DT_RECIBO_INGRESO
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        ),
        self::DT_RECIBO_EGRESO => array(
            'display' => 'Recibo de Egreso'
            , 'contable' => self::TIPO_DOC_NO_CONTABLE
            , 'value' => self::DT_RECIBO_EGRESO
            , 'short' => self::DT_RECIBO_EGRESO
            , 'default' => false
            , 'sales' => false
            , 'tax' => false
            , 'have_serie' => true
        )
    );

    public function a_year_report() {

        return array(
            '2013' => array(
                'display' => '2013'
                , 'value' => '2013'
            ),
            '2014' => array(
                'display' => '2014'
                , 'value' => '2014'
            )
        );
    }

    public $a_month_report =
        array(
            'Enero' => array(
                'display' => 'Enero'
                , 'value' => '1'
            ),
            'Febrero' => array(
                'display' => 'Febrero'
                , 'value' => '2'
            ),
            'Marzo' => array(
                'display' => 'Marzo'
                , 'value' => '3'
            ),
            'Abril' => array(
                'display' => 'Abril'
                , 'value' => '4'
            ),
            'Mayo' => array(
                'display' => 'Mayo'
                , 'value' => '5'
            ),
            'Junio' => array(
                'display' => 'Junio'
                , 'value' => '6'
            ),
            'Julio' => array(
                'display' => 'Julio'
                , 'value' => '7'
            ),
            'Agosto' => array(
                'display' => 'Agosto'
                , 'value' => '8'
            ),
            'Septiembre' => array(
                'display' => 'Septiembre'
                , 'value' => '9'
            ),
            'Octubre' => array(
                'display' => 'Octubre'
                , 'value' => '10'
            ),
            'Noviembre' => array(
                'display' => 'Noviembre'
                , 'value' => '11'
            ),
            'Diciembre' => array(
                'display' => 'Diciembre'
                , 'value' => '12'
            )
        );
    

    public function getMonthName($index_value) {
        foreach ($this->a_month_report as $key => $value) {
            if ($value['value'] == $index_value) {
                return $key;
            }
        }
    }
    
    public function getDocumentsByFilter($sFilter = null, $sValue = null) {
        $aDocuments = array();
        foreach ($this->a_documents_type as $sKey => $aValue) {
            if ($sFilter == null) {
                $aDocuments[$sKey] = $aValue;
            } else {
                if ($aValue[$sFilter] == $sValue)
                    $aDocuments[$sKey] = $aValue;
            }
        }
        return $aDocuments;
    }

    public $a_purchaseOrder_status =
            array(
        self::PURCHASE_ORDER_STATUS_APROBADO => array(
            'display' => 'Aprobado'
            , 'value' => self::PURCHASE_ORDER_STATUS_APROBADO
            , 'short' => 'POS_A'
        ),
        self::PURCHASE_ORDER_STATUS_CANCELADO => array(
            'display' => 'Cancelado'
            , 'value' => self::PURCHASE_ORDER_STATUS_CANCELADO
            , 'short' => 'POS_C'
        ),
        self::PURCHASE_ORDER_STATUS_EMITIDO => array(
            'display' => 'Emitido'
            , 'value' => self::PURCHASE_ORDER_STATUS_EMITIDO
            , 'short' => 'POS_E'
        ),
        self::PURCHASE_ORDER_STATUS_FACTURADO => array(
            'display' => 'Facturado'
            , 'value' => self::PURCHASE_ORDER_STATUS_FACTURADO
            , 'short' => 'POS_F'
        ),
        self::PURCHASE_ORDER_STATUS_ANULADO => array(
            'display' => 'Anulado'
            , 'value' => self::PURCHASE_ORDER_STATUS_ANULADO
            , 'short' => 'POS_An'
        ),
        self::PURCHASE_ORDER_STATUS_DESAPROBADO => array(
            'display' => 'Desaprobado'
            , 'value' => self::PURCHASE_ORDER_STATUS_DESAPROBADO
            , 'short' => 'POS_D'
        ),
        self::PURCHASE_ORDER_STATUS_RECEPCIONADO => array(
            'display' => 'Recepcionado'
            , 'value' => self::PURCHASE_ORDER_STATUS_RECEPCIONADO
            , 'short' => 'POS_R'
        )
    );
    public $a_purchaseorder_payment =
            array(
        self::PURCHASE_ORDER_CASH => array(
            'display' => 'Efectivo',
            'value' => self::PURCHASE_ORDER_CASH,
            'short' => 'POP_C',
            'default' => true
        ),
        self::PURCHASE_ORDER_CREDIT => array(
            'display' => 'Crédito',
            'value' => self::PURCHASE_ORDER_CREDIT,
            'short' => 'POP_T'
        )
    );
    public $a_sales_payment =
            array(
        self::SALES_PAYMENT_CASH => array(
            'display' => 'Efectivo',
            'value' => self::SALES_PAYMENT_CASH,
            'short' => 'SALES_T',
            'default' => true
        ),
        self::SALES_PAYMENT_CREDITCARD => array(
            'display' => 'Crédito',
            'value' => self::SALES_PAYMENT_CREDITCARD,
            'short' => 'SALES_C'
        ),
       self::SALES_PAYMENT_MIXTO => array(
            'display' => 'Mixto',
            'value' => self::SALES_PAYMENT_MIXTO,
            'short' => 'SALES_M'
       )
    );
    public $a_creditcard_payment =
            array(
        self::SALES_CARD_VISA => array(
            'display' => 'Visa',
            'value' => self::SALES_CARD_VISA
        ),
        self::SALES_CARD_MASTERCARD => array(
            'display' => 'MasterCard',
            'value' => self::SALES_CARD_MASTERCARD
        ),
        self::SALES_CARD_AMERICAN => array(
            'display' => 'American Express',
            'value' => self::SALES_CARD_AMERICAN
        ),
        self::SALES_CARD_CMR => array(
            'display' => 'CMR Falabella',
            'value' => self::SALES_CARD_CMR
        ),
        self::SALES_CARD_RYPLEY => array(
            'display' => 'Ripley',
            'value' => self::SALES_CARD_RYPLEY
        ),
        self::SALES_CARD_DINNERS => array(
            'display' => 'Dinners Club',
            'value' => self::SALES_CARD_DINNERS
        )
    );
    public $a_purchase_documents =
            array(
        self::OC_FACTURA => array(
            'display' => '01 Factura',
            'value' => self::OC_FACTURA
        ),
        self::OC_RECIBOPORHONORARIOS => array(
            'display' => '02 Recibo por Honorarios',
            'value' => self::OC_RECIBOPORHONORARIOS
        ),
        self::OC_BOLETA => array(
            'display' => '03 Boleta de Venta',
            'value' => self::OC_BOLETA
        ),
        self::OC_NOTACREDITO => array(
            'display' => '07 Nota de Crédito',
            'value' => self::OC_NOTACREDITO
        ),
        self::OC_NOTADEBITO => array(
            'display' => '08 Nota de Débito',
            'value' => self::OC_NOTADEBITO
        ),
        self::OC_GUIAREMISION => array(
            'display' => '09 Guía de Remisión',
            'value' => self::OC_GUIAREMISION
        ),
        self::OC_TICKETFACTURA => array(
            'display' => '12 Ticket Factura',
            'value' => self::OC_TICKETFACTURA
        ),
        self::OC_TICKETBOLETA => array(
            'display' => '12 Ticket Boleta',
            'value' => self::OC_TICKETBOLETA
        )
    );
    public $a_ordersales_payment =
            array(
        self::SALES_ORDER_PAYMENT_CASH => array(
            'display' => 'Efectivo',
            'value' => self::SALES_ORDER_PAYMENT_CASH,
            'short' => 'ORDERSALES_CASH',
            'default' => true
        ),
        self::SALES_ORDER_PAYMENT_CREDIT => array(
            'display' => 'Crédito',
            'value' => self::SALES_ORDER_PAYMENT_CREDIT,
            'short' => 'ORDERSALES_CREDIT'
        )
    );
    public $a_ordersales_status =
            array(
        self::SALES_ORDER_STATUS_CANCELED => array(
            'display' => 'Cancelado',
            'value' => self::SALES_ORDER_STATUS_CANCELED
        ),
        self::SALES_ORDER_STATUS_EXPIRED => array(
            'display' => 'Expirado',
            'value' => self::SALES_ORDER_STATUS_EXPIRED
        ),
        self::SALES_ORDER_STATUS_RESERVED => array(
            'display' => 'Reservado',
            'value' => self::SALES_ORDER_STATUS_RESERVED
        ),
        self::SALES_ORDER_STATUS_SOLD => array(
            'display' => 'Vendido',
            'value' => self::SALES_ORDER_STATUS_SOLD
        )
    );

    public function fnGetClassVariable($s_variable_name) {
        return $this->{$s_variable_name};
    }

}

return array(
    'uInterface' => new rmsInterface());
