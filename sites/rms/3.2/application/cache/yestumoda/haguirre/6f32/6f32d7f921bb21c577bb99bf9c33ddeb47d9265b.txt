s:11290:"        <div id="user_info" class="none">
            <fieldset class="ui-corner-all">
                <legend class="ui-widget-header ui-corner-all">Usuario</legend>
                <div class="label_output_ui left">

                    <div>
                        <label>Nombre(s):</label>
                        <div id="user_fName"></div>
                    </div>
                    <div>
                        <label>Apellidos:</label>
                        <div id="user_lName"></div>
                    </div>
                    <div>
                        <label>Grupo:</label>
                        <div id="user_group_name"></div>
                    </div>
                    <div>
                        <label>Usuario:</label>
                        <div id="user_name_info"></div>
                    </div>
                    <div>
                        <label>Correo-e:</label>
                        <div id="user_email"></div>
                    </div>
                    <div>
                        <label>Oficina:</label>
                        <select style="width : 185px" class="" id="user_office"  name="office"><option value="">Seleccione una Opción....</option><option value="4">Trujillo - Central</option><option value="2">Trujillo - El Virrey</option><option value="3">Trujillo - Junin</option><option value="1">Trujillo - Zela</option></select>                    </div>
                </div>
                <div class="left">
                    <input id="imgProfile_file" type="hidden"/>
                    <div id="imgProfile_ui" class="ui-corner-all ui-widget-header" style="width: 128px; height: 128px; border-width: 2px; background-repeat: no-repeat;"></div>
                    <br/>
                    <center>
                        <div id="btnImgUpload_ui">
                            <noscript>			
                            <p>Please enable JavaScript to use file uploader.</p>
                            <!-- or put a simple form for upload here -->
                            </noscript>  
                        </div>
                    </center>
                    <br/>
                </div>
            </fieldset>
            <br/>
            <fieldset class="ui-corner-all">
                <legend class="ui-widget-header ui-corner-all">Terminal</legend>
                <div class="label_output_ui">
                    <div>
                        <label>Sistema Operativo:</label>
                        <div id="user_terminal_os"></div>
                    </div>
                    <div>
                        <label>Impresora:</label>
                        <div id="user_printer"></div>
                    </div>
                    <div>
                        <label>Navegador:</label>
                        <div id="user_browser"></div>
                    </div>
                </div>
            </fieldset>
                            <br/>
                <script type="text/javascript">
                    $(document).ready(function(){
                        function changeLogin(id,name){
                            loadingcarga = 1;
                            var params = new Object();
                            if(id!=null && name!=null){
                                params.uid = id;
                                params.ufname = name;
                            } else{
                                params.reset = 'r';
                            }
                            $.ajax({
                                url:'getMimicUser'+jquery_params,
                                data:params,
                                asybn:false,
                                success: function(r) {
                                    $('#user_info').dialog('close');
                                    if(evalResponse(r)){
                                        msgBox(r.msg,r.code,function(){
                                            location.href='/private/authentication/logout';
                                        });
                                    }
                                }
                            });
                        }
                        $('#recoveryLogin').click(function(){
                            changeLogin(null,null);
                        });
                        function createAutocompleteUser(url,input_id){
                            if(typeof $('#'+input_id).data('autocomplete') != 'undefined')
                                $('#'+input_id).autocomplete('destroy');
                            $('#'+input_id).autocomplete({
                                autoFocus: true
                                ,dataType:'jsonp'
                                ,select:function(event,ui){
                                    changeLogin(ui.item.id,ui.item.value);
                                }
                                ,source:function(request,response){
                                    $.ajax({
                                        url:url,
                                        data:{term:request.term},
                                        success: function(r) {
                                            if(r.code==code_success){
                                                //response( $.map( r.data, function( item ) {return { label: item.value,value: item.id}}));
                                                response( $.map( r.data, function( item ) {
                                                    var text = "<span class='spanSearchLeft'>" + item.value+' @ '+item.office+ "</span>";                                
                                                    text += "<div class='clear'></div>";
                                                    return {
                                                        label:  text.replace(
                                                        new RegExp(
                                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                                            $.ui.autocomplete.escapeRegex(request.term) +
                                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                                    ), "<strong>$1</strong>" ),
                                                        value:  item.value,
                                                        id : item.id                                   
                                                    }
                                                }));
                                            } else{
                                                msgBox(r.msg,r.code);
                                            }
                                        }
                                    });
                                }
                            }).data( "autocomplete" )._renderItem = function( ul, item ) {            
                                return $( "<li></li>" )
                                .data( "item.autocomplete", item )
                                .append( "<a>" + item.label + "</a>" )
                                .appendTo( ul ).css({width:'250px'});
                            };
                        }
                        createAutocompleteUser('getUsers'+jquery_params,'user_list');
                    });
                </script>
                <fieldset>
                    <legend class="ui-widget-header ui-corner-all">Iniciar Sesión Como...</legend>
                    <div class="label_output_ui">
                        <div>
                            <label>El Usuario:</label>
                            <input id="user_list" name="user_list" type="text"/>
                            <button class="btn" id="recoveryLogin" style="margin-left: 5px;">Recuperar Mi Inicio de Sesión</button>
                        </div>
                    </div>
                    <div style="margin: 15px; text-align: justify;">
                        Para iniciar sesión como otro usuario, busque al usuario, selecciónelo e inicie sesión nuevamente. Para recuperar el inicio de sesión normal, busque su usuario y repita el procedimiento. O simplemente haga click en el botón Recuperar Mi Inicio de Sesión                    </div>
                </fieldset>
                            
                    </div>
        <div id="user_chpass" class="none">
            <form id="frmChPass" method="post" autocomplete="off">
                <fieldset class="ui-corner-all">
                    <legend class="ui-widget-header ui-corner-all">Cambio de Contraseña</legend>

                    <div class="label_output">
                        <div>
                            <label>Contraseña actual:</label>
                            <input id="chpass_current" name="chpass_current" type="password"/>
                        </div>
                        <div>
                            <label>Contraseña nueva:</label>
                            <input id="chpass_new" name="chpass_new" type="password"/>
                        </div>
                        <div>
                            <label>Confirme contraseña:</label>
                            <input id="chpass_confirm" name="chpass_confirm" type="password"/>
                        </div>
                    </div>

                    <div id="cp_error_msg" class="ui-helper-hidden ui-state-error ui-corner-all user_msg">
                        <p><span class="ui-icon ui-icon-alert"></span><strong>Alerta:</strong><span id="cp-text-error"></span></p>
                    </div>
                    <div id="cp_info_msg" class="ui-helper-hidden ui-state-highlight ui-corner-all user_msg"> 
                        <p><span class="icon_fix"><span class="ui-icon ui-icon-info"></span></span><strong>Exitoso:</strong><span id="cp-text-info"></span></p>
                    </div>
                </fieldset>
                <input type="submit" class="ui-helper-hidden" />
            </form>
        </div>

        <div id="errorMessages">
            <div class="header"></div>
            <div class="content">
                <div class="img">
                    <img src="/media/images/msg_error.png" alt="[ERROR]"/>
                </div>
                <div class="text">
                    <div class="title">
                        Error de Validación                    </div>
                    <div id="messageField" class="message">

                    </div>
                </div>
            </div>
            <div class="footer"></div>
        </div>
         <div id="subfooter" class="ui-widget-header ">            
        <label style="float: left;"> Trujillo // Trujillo - Zela // Harby Edgar Huamanchumo Aguirre </label>
        <label style="float: right;">©MCeTS 2009 - 2017 MCeTS. All rights reserved&nbsp;&nbsp;</label>
                </div>
        <div id="sessionTimeoutWarning" class="ui-helper-hidden"></div>
          ";