<style type="text/css">
    .ui-menu .ui-menu-item {font-size: 12px;}
    /*registro de cliente*/
    #formRegisterClient ul{float: left;}
    #formRegisterClient .div_buttons{padding: 10px 0;}
    #formRegisterClient input{width: 166px;}
    #formRegisterClient select{width: 178px;}
    #div_register_client{position: absolute;top:0;left: 0;z-index: 0;box-shadow: rgba(0, 0, 0, 1) 0 0 5px;-webkit-box-shadow: rgba(0, 0, 0, 1) 0px 0px 5px;}
    #div_register_client .divLine,.divLine.autoPayment,.divLine.divMeasures{width: auto;}
    .divLine.divLineShort{width:310px;}
    .ui_icon_close_register_client{display: block;float: right;cursor: pointer;}
    .ui_icon_close_register_client span{float: left;}
    .register_client_title{padding: 5px;margin-bottom: 10px;line-height: 16px;}
    #div_register_client .arrow_left{left: -25px;top: 22px;}
    .arrow_left{position: absolute;width: 18px;height: 28px;background-position: 0 -125px;background-image: url(/media/images/arrows.png);}
    li.containerCompany{display: none;}
    /*fin registro cliente*/
    .divDocInfo{float:right;}
    input.helptext{font-style: italic;color: #CCCCCC;}
    div.divDocInfo{display:none;font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    .relative{position: relative;}
    .search_client_wrapper span{margin: 3px 4px;}
    .ui-widget-content .search_client_wrapper{background-image: none;margin-right: 5px;}
    .search_client_wrapper,.search_client_wrapper span,
    .search_client_wrapper input{float: left;}
    .ui-widget-content .search_client_wrapper input{ width: 245px;border-top: transparent;border-bottom: transparent;border-right: transparent;-moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;-moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;}
    /***mini lots***/
   
    .search_client_wrapper_lot span{margin: 3px 4px;}
    .ui-widget-content .search_client_wrapper_lot{background-image: none;margin-right: 5px;}
    .search_client_wrapper_lot,.search_client_wrapper_lot span,
    .search_client_wrapper_lot input{float: left;}
    .ui-widget-content .search_client_wrapper_lot input{ width: 150px;border-top: transparent;border-bottom: transparent;border-right: transparent;-moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;-moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;}
    .icon_lots{
        margin-top: 4px;
    }

    input.inputData{width: 270px;}
    input[type="text"], input[type="password"], textarea, select {padding: 5px;}
    hr.hr_separate{border-top-width: 3px;border-top-style: dashed;border-bottom: none;border-left: none;border-right: none;}
    .divLine.divLineShort{width:310px;}
    div.divLeft{float: left;}
    input.inputQuantity{width: 50px;}
    .divSearch{width: 165px;}
    .divSearch label{font-weight: bold;}
    .labelprice {display: block;float: left;font-weight: bold;height: 16px;padding-top: 5px;width: 100px;}
    label.frmlblshort{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    label.frmlblcv{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    input.subPrice{width: 200px; height: 19px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    input.subPriceLong{width: 200px; height: 19px; text-align: left;float: left;margin-top: 5px;margin-right: 5px;}
    .ui-widget input.subPrice {font-size: 18px;background-image: none; width: 90px;}
    .subTotal{float: left;}
    #addPackageDetail{margin: 5px 0;}
    .buttonadd{margin-top: 13px;}
    .priceTax{display:none;}
    .ui-widget input.lightSearch{font-style: italic;color: #CCCCCC;}
    #quantity{width: 50px;text-align: right;margin-right: 10px;}
    input.inputCustomPrice{width: 60px;text-align: right;}
    .ui-widget-content input[type="text"].ui-autocomplete-loading ,
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-image: url('/media/images/ajax-loading.gif');background-position: right center;background-repeat: no-repeat; }
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-position: right top;}
    .myOverlay, .myShadowMsg, .myContentMsg,.my_payment_shadow,.my_payment_content{display: none;}
    .myOverlay{z-index: 999;position: fixed;}
    .myShadowMsg{height: 122px;left: 50%;margin-left: -205px;margin-top: -100px;position: fixed;top: 50%;width: 417px;z-index: 999;}
    .myContentMsg{ font-family: UbuntuTitle;font-size: 65px;height: 80px;left: 50%;line-height: 80px;margin-left: -195px;margin-top: -90px;padding: 10px;position: fixed;text-align: center;top: 50%;width: 375px;z-index: 999;}
    .myOverlay{display: none;}
    #grid_uno{
        float: left;
        padding-right:20px;
    }
    #grid_dos{
        float: left;
    }
    input.serie{width: 80px;text-align: right;}
    input.correlative{width: 135px;text-align: right;}
    .lot_qua{
        float: left;
        padding: 10px;
        font-size: 15px;
    }
    .lot_qua>div{        
        padding-bottom:7px;
    }
    .Combo{
        width: 150px;
    }
    .lot_qua>div label{        
        float: left;
        font-weight: bolder;
        text-align: left;
        width: 85px;
    }
    .ui-jqgrid tr.jqgrow td{
        white-space: normal;
    }
    /*** Impresion de la Venta'***/
    .lPayment, .lSymbol {
        float: left;
        font-size: 18px;
        line-height: 35px;
        width: 105px;
    }
    .lSymbol {
        text-align: center;
        width: 50px;
    }
    #post_confirm_payment {
        border: medium none;
        font-family: UbuntuTitle;
        font-size: 40px;
        height: 41px;
        text-align: right;
        width: 242px;
    }
    .rowLine {
        padding: 4px 3px;
    }
    .ui-widget div.post_display_prices { 
        border: 1px solid #AAAAAA;
        border-radius: 16px;
        cursor: pointer;
        float: left;
        font-size: 12px;
        height: 32px;
        left: 50%;

        margin: 0 10px 0 auto;
        position: relative;
        text-align: center;
        transition: color 0.5s linear 0.5s;
        width: 32px;
    }

    .ui-widget .post_confirm_payment_total_amount,.ui-widget .post_confirm_payment_return_cash{
        float: left;
        font-family: UbuntuTitle;
        font-size: 18px;
        height: 45px;
        line-height: 36px;
        margin-left: 18px;
        text-align: right;
        width: 85px;
    }
    #post_confirm_payment{
        font-family: UbuntuTitle;font-size: 18px;
        height: 33px;text-align: left;
        width: 74px;border: none; 
        background-color:#ffef8f;
        float:left;
        margin-left: 38px;
        text-align: center;
    }

    #frmCreditCash{
        display:block;
    }
    #frmCreditCard {
        display:none;
    }
    /** CSS HARBY **/
    orderSales{width: 90px; text-align: right;font-size-adjust: 20px;}
    .note{padding-left: 15px; text-align: right;}
    .amountOrderSales{margin-top: 10px;}
    .credNote {    padding-left: 15px;
                   text-align: right; }

    span.spanSearchLeft{float: left;max-width: 184px;}
    .ui-menu span.ui-icon.spanSearchLeft,
    .ui-menu span.ui-icon.spanIcon
    {float: left;display: inline-block;position: relative;top: 0;left: 0;}
    
    span.spanPref{float: left;margin-right: 2px;}
    span.spanIcon{float: left;margin-right: 2px;}
    span.spanSearchRight{float: right;}
    
    .payment{
        margin: 10px 7px;
    }
    .payment input{
        margin-bottom: 2px;
        margin-top: 6px;
        width: 113px;
    }
    .payment label{
        display: table-row;
        float: left;
        font-weight: bold;
        margin-left: -28px;
        max-width: 150px;
        min-width: 150px;
        padding: 3px;
        text-align: right;
        white-space: pre-line;
        margin-top: 8px;
    }
    #amount_paymentCashMixto{
        background-color:#ffef8f;
     }
</style>

<script type='text/javascript'>
    
    /* URL */
    var url_GetClients = '/private/sales/getClients'+jquery_params;
    var url_GetProduct = '/private/sales/getProduct'+jquery_params;
    var url_getDetailByProductId = '/private/sales/getDetailByProductId'+jquery_params; 
    var url_getLotByProductId = '/private/sales/getProductLotById'+jquery_params; 
    var url_RegisterSales = '/private/sales/createSales'+jquery_params;
    var url_getSerieCorrelativeByTerm='/private/sales/getSerieCorrelativeCanceledByTerm'+jquery_params;  
    var url_GetProductByCodigo = '/private/sales/getProductByCode'+jquery_params;
    var url_getCodeOrderSales='/private/sales/getCodeOrderSales'+jquery_params; 
    var url_detailSalesOrderSales = '/private/sales/getDetailSalesOrder'+jquery_params;
    var url_detailSalesCreditNote = '/private/salesmanagement/getCreditNoteByIdSale'+jquery_params;
    var url_GetProductByDescription = '/private/sales/getProductByDescription'+jquery_params;
    
    /* VARIABLES CONTROLADOR */
    var bAvailableDocumentType = '<?php echo $bAvailableDocumentType ?>';
    var documentManual = '<?php echo $documentManual ?>';
    var bBarcodeReader = '<?php echo $bBarcodeReader ?>';
    var bTaxEnabled = '<?php echo $bTaxEnabled; ?>';
    var dt_boleta = '<?php echo $BOLETA ?>'; 
    var typeCash ='<?php echo $cash; ?>';
    var typeCard = '<?php echo $creditCard; ?>';
    var typeMixto = '<?php echo $mixto; ?>';
    
    /* VARIABLES */
    var totalSubTotales = 0.0; // El total de subtotales
    var tblSalesDetail;
    var bctblSalesDetail;
    var array_detail_id_p=[];
    var array_detail_id_pl=[];
    var arrayDetails = new Object();
    var countarray = 0;
    var keycountarray= 0;
    var quantity = 0;
    var flags = {};
    var iCountLot = 0;
    flags.isRange = 0;
    flags.haveLot = 0;
    var arrayDetails = new Object();
    var arrayDetails_lots = new Object();  
    var save_arrayDetails_lots = new Object();
    var cbitemDetail = new Object();
    
    /* Mensajes  PHP */
    var msg_warn_delete='<?php echo __("Esta Seguro de Eliminar ?") ?>';
    var title_delete='<?php echo __("Eliminación") ?>';
    var sTxtSearchClient = '<?php echo __('Búsqueda de Cliente') ?>';
    var msgSelectEmpty='<?php echo __("No se encontró información"); ?>';
    var msgSelectDefaultOption='<?php echo __("Seleccione una Opción...."); ?>';
    var detail_form_Title = '<?php echo __("Detalle de Otros Productos"); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Producto") ?>';
    var msgAlerta = "<?php echo __("Alerta"); ?>";
    var stockInsuficiente = '<?php echo __("Stock de Productos Insufientes.") ?>';
    var creditcard_form_Title = "<?php echo __("Tarjeta de Crédito"); ?>";
    var creditCardTitle ="<?php echo __("Información de Tarjeta de Crédito"); ?>";
    var negative_amount = '<?php echo __("La cantidad debe ser mayor a 0"); ?>';
    var txt_selectsearch = '<?php echo __("Selecionar Usuario"); ?>';
    var txt_requestsclient = '<?php echo __("El Cliente es Requerido") ?>';
    var confirmationSales = '<?php echo __("Desea Confirmar la Venta?"); ?>';
    
$(document).ready(function(){
    /* ButtonSet */
    $('.buttonsContainer').buttonset();
    /* Hide */
    $('.note, .credNote,  #frmMixtoCard').hide();
    /* Buttons */
    $('.addSender').button({
        icons: {
            primary: 'ui-icon-plus'
        },
        text:false
    }); 
    $('#addPackageDetail').button({
        icons:{
            primary: 'ui-icon-plus'
        }
    }).click(function(){
        $('#formSalesDetail').submit();    
    });
    $('#saveSales').button({
      icons:{
          primary: 'ui-icon-disk'
      }
    });
    $('#cancel').button({
      icons:{
          primary: 'ui-icon-cancel'
      }
    });        
    $('#search').button({
        icons:{
            primary:'ui-icon-search'
        }
    }).click(function(){
        salesOrderSales();
    });
    $('#searchCreditNote').button({
        icons:{
            primary:'ui-icon-search'
        }
    }).click(function(){
        salesCreditNote();
    });
  
    /* Helptext */
    $('#chars_Customers').focus();
    $('#chars_Customers').helptext({value: '', customClass: 'lightSearch'});
    
    /* PriceFormat */
    $('#price,#subTotalpriceUnit,#priceTotal, #descuento,#post_confirm_payment,#custom_price,#amountPaid, #amount_paymentCashMixto, #amount_paymentCreditMixto').priceFormat({
        prefix: '',
        centsSeparator: '.',
        thousandsSeparator: ''
    });
    /* Datepicker */
    $("#input_date_sale").datepicker({
        dateFormat:'<?php echo $jquery_date_format ?>'      
    });
    
    /* KeyUp */
    $('#quantity').keyup(function(key){
        var precio = 0;            
        returnarray($('#idproduct').val(),$('#idstore').val());            
        if ((countarray == 0 && keycountarray==0) || edit_product == 1){
            stock = $('#amount').val() - $('#quantity').val();
        }else{
            stock = $('#amount').val() - (parseInt(quantity) + parseInt($('#quantity').val())) ;
        }
        countarray = 0;
        keycountarray = 0;
        if($('#idproduct').val()==""){
            msgBox("Seleccione producto");
            $(this).val('');
        }else 
        if(stock<0){
            msgBox("Cantidad es mayor al stock");
            $(this).val('');
        }else if($(this).val()==0){
        }else{
            calculationPriceDetail();
            $('#priceTotal').val(precio.toFixed(2));
        }            
        stock = 0;
        $('#priceTotal').val((priceUnit * quantity).toFixed(2));
    });   
    $('#cb_quantity').keyup(function(key){
        var precio = 0;            
        returnarray($('#idproduct').val(),$('#idstore').val());            
        if ((countarray == 0 && keycountarray==0) || edit_product == 1){
            stock = $('#amount').val() - $('#quantity').val();
        }else{
            stock = $('#amount').val() - (parseInt(quantity) + parseInt($('#quantity').val())) ;
        }
        countarray = 0;
        keycountarray = 0;
//        if($('#idproduct').val()==""){
//            msgBox("Seleccione producto");
//            $(this).val('');
//        }else 
        if(stock<0){
            msgBox("Cantidad es mayor al stock");
            $(this).val('');
        }else if($(this).val()==0){
        }else{
            calculationPriceDetail();
            $('#priceTotal').val(precio.toFixed(2));
        }            
        stock = 0;
        $('#priceTotal').val((priceUnit * quantity).toFixed(2));
    });
    $('#custom_price').keyup(function(key){
        calculationPriceDetail();
    });
    $('#post_confirm_payment').keyup(function(key){
       fnUpdateConfirm_post();
    });
    $('#amount_paymentCashMixto, #amount_paymentCreditMixto').keyup(function(key){
       fnUpdateConfirm_post();
    });
    $("input#codigo").keypress(function(event) {
        if(($('#cb_quantity').val())>=1){
            if ( event.which == 13 ) {
                event.preventDefault();
                code = $.trim($(this).val());
                getProductByCode(code.substring(0,7));               
            }}
        else{
            msgBox(negative_amount);   
        }
    });
    
    $('#cd_description').keypress(function(event){
        if ( event.which == 13 ) {
            event.preventDefault();
            code = $.trim($(this).val());
            $('#quantity').val('1');
            getProductByDescription(code.substring(0,16));               
        }
    });
       
    /* SetMask */
    $('#quantity,#custom_price,#cardNumber,#operationNumber').setMask();
    /* Combobox */
    // Listar Vendedores de la venta
    $('#cmb_listUserCode').combobox({
        selected:function(){
          // Focus en producto
            setTimeout(function(){
              $("#codigo").focus();                
           }, 10);

        }
    });      
    $("#typeCard , #typeCardMixto").combobox();
    $("#quantity").select(function(){
       $("#quantity").val(''); 
    });
    
    /* Reset Form */
    jQuery.fn.reset = function () {
    $(this).each (function() {
        this.reset();       
    });
    };
    /* Documentos Asignados */
    if (bAvailableDocumentType == 1 && documentManual!= 1){
        successDocument = false;   
        fnUpdateCorrelativePackage = function(sType){                
            try{                
                oDocumentType = getCurrentCorrelative(sType);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                if(oDocumentType.serie != null)
                    displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.serie + '-' +oDocumentType.correlative;
                else
                    displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.correlative;

                $('div.divDocInfo')
                .removeClass('ui-state-highlight')
                .removeClass('ui-state-error')
                .addClass('ui-state-highlight')
                .html(displayDocument);
                successDocument = true;


            }catch(e){
                successDocument = false;
                $('div.divDocInfo')
                .removeClass('ui-state-highlight')
                .removeClass('ui-state-error')
                .addClass('ui-state-error')
                .html('<?php echo __("Documento no Asignado") ?>');
            }
        };                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
        fnUpdateCorrelativePackage($("input[name=document_type]:checked").val());
        $('div.divDocInfo').css('display', 'block');
        $('input[name=document_type]').change(function(){            
            fnUpdateCorrelativePackage($(this).val());
        });                                                
    }
    
    /* Tipo de Venta */
    $('input[name=for_sales]').change(function(){
          $('#totalAmount').val('');
          $('#amountPaid').val('');
          $('#amountToPaid').val('');
          totalSubTotales = 0.0;
          iCountItem = 0;
          if($(this).val()=='direct'){
              $("#bc_salesDetail_jqGrid").clearGridData();
              $('#formSalesDetail')[0].reset();
              $('#idCustomers').val('');
              $('#select_purchaseOrders').val('');
              $("#chars_Customers").removeAttr("disabled");
              $("#chars_Customers").helptext('refresh');
              $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
              $('#cmb_listUserCode-combobox').prop("disabled",false);
              $('#cmb_listUserCode-button-combobox').button("enable");
              $('.order').show();
              $('.note').hide();
              $('.credNote').hide();
          //    $('.numOrder').hide();
              deleteRowsAll( bctblSalesDetail);
              arrayDetails = {};
          }
          else if($(this).val()=='salesorder'){
              $("#bc_salesDetail_jqGrid").clearGridData();
              $('#formSalesDetail')[0].reset();
              $('#frm_customers')[0].reset();
              $("#chars_Customers").attr("disabled",true);
              $('#idCustomers').val('');
              $('.order').hide();
              $('.credNote').hide();
            //  $('.numOrder').show();
              $('.note').show();
              $('#cmb_listUserCode-combobox').prop('disabled',true);
              $('#cmb_listUserCode-button-combobox').button('disable');
              deleteRowsAll(bctblSalesDetail);
              arrayDetails = {};
          }else if($(this).val()=='creditnote'){
              $("#bc_salesDetail_jqGrid").clearGridData();
              $('.order').show();
              $('.note').hide();
           //   $('.numOrder').hide();
              $('.credNote').show();
              $('#cmb_listUserCode-combobox').prop('disabled',false);
              $('#cmb_listUserCode-button-combobox').button('enable');
          }
    });

    
    
    
    /* Autocomplete de Cliente */
    $("#chars_Customers" ).autocomplete({
        minLength: 0,
        autoFocus: true,
        position: {offset: "-25px 2px"},
        appendTo: '#auto_div_person_sender',
        source:function(req,response){
            $.ajax({
                url:url_GetClients,
                dataType:'jsonp',
                data: {
                    term : req.term
                },
                success:function(resp){
                    if(evalResponse(resp)){
                        response( $.map( resp.data, function( item ) {
                            item_icon = 'ui-icon-help';
                            if(item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                                item_icon = 'ui-icon-person';
                            }
                            if(item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                                item_icon = 'ui-icon-home';
                            }
                            var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                            text += "<span class='spanSearchLeft'>" + item.customers + "</span>";
                            text += "<div class='clear'></div>";
                            return {
                                label:  text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>" ),
                                value:  item.customers,
                                id : item.idCustomers,
                                type : item.type
                            }
                        }));
                    }
                }
            });
        },
        focus: function(event, ui) {
            if(ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
            }
            if(ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
            }
        },
        select: function( event, ui ) {
            $('#idCustomers').val(ui.item.id);

            $(this).data('autocomplete').text_value = ui.item.value;

            if(ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                $('.icon_sender_type').removeClass('ui-icon-home ui-icon-help').addClass('ui-icon-person');

            }
            if(ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-help').addClass('ui-icon-home');
            }
            // Para que vaya al producto 
            setTimeout(function(){
                $("#description").focus();                    
            }, 10);
        },
        change: function( event, ui ) {   
            if ( !ui.item ) {              
                $('#idCustomers').val('');
                $(this).helptext('refresh');       
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                return false;
            }
            //$(this).helptext('refresh');
            return true;   
        }

    }).bind('blur', function(){       
        if($($(this).data('autocomplete').element).val() != $(this).data('autocomplete').text_value){
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                $( this ).helptext('refresh');
        }
    }).data( "autocomplete" )._renderItem = function( ul, item ) {            
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
    };
    $( "#chars_Customers" ).data( "autocomplete" )._resizeMenu = function () {
        var ul = this.menu.element;
        ul.css({width: 290, 'margin-bottom': 5});            
    };
    /* Buscar Productos */
    if(bBarcodeReader == 0 ){
        $("#description").autocomplete({
                minLength: 1,
                autoFocus: true,
                source:function(req,response){
                    $.ajax({
                        url:url_GetProduct,
                        dataType:'jsonp',
                        data: {
                            term : req.term
                        },
                        success:function(resp){
                            if(evalResponse(resp)){
                                response( $.map( resp.data, function( item ) {

                                    var text = "<span class='spanSearchLeft'>" + item.product + "</span>";
                                    text += "<div class='clear'></div>";
                                    return {
                                        label:  text.replace(
                                        new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                            $.ui.autocomplete.escapeRegex(req.term) +
                                            ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                    ), "<strong>$1</strong>" ),
                                        value:  item.product,
                                        id : item.idProduct,
                                        price : item.productPriceSell,
                                        idstore : item.idStore,
                                        amount:item.Amount,
                                        haveDetail:item.haveDetail
                                    }
                                }));
                            }else{
                                msgBox(resp.msg,resp.code);
                            }
                        }
                    });
                },
                focus: function(event, ui) {
                    $('#custom_price').val(ui.item.price);
                },
                select: function( event, ui ) {
                    $("#haveDetail").val(ui.item.haveDetail);
                    $('#idproduct').val(ui.item.id);
                    //                $('#custom_price').val(ui.item.price);
                    $('#idstore').val(ui.item.idstore);
                    $('#amount').val(ui.item.amount);
                } 
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
    }


    /***************** Activacion de codigo de barras ******************/
    bctblSalesDetail = $('#bc_salesDetail_jqGrid').jqGrid({            
        height: '100%',                        
        width:'830',
        datatype: 'local',
        sortable: false,
        colNames:[                                
            '<?php echo __("ID") ?>',
            '<?php echo __("ID Prod.") ?>',
            '<?php echo __("Descripción") ?>',
            '<?php echo __("Cantidad") ?>',
            '<?php echo __("Precio Unitario") ?>',                
            '<?php echo __("Impuesto") ?>',
            '<?php echo __("Descuento") ?>',
            '<?php echo __("SubTotal") ?>',
            '<?php echo __("Tiene Detalle") ?>',
            '<?php echo __("Opción") ?>'
        ],
        colModel:[                                
            {name:'idOut',index:'idOut',key:true,width:240,hidden:true},
            {name:'id',index:'id',width:240,hidden:true},
            {name:'descriptionOut',index:'descriptionOut',width:240},
            {name:'quantityOut',index:'quantityOut',width:100, align: 'center'},
            {name:'unitPriceOut',index:'unitPriceOut', width:100, align: 'right'},                
            {name:'taxUnitOut',index:'taxUnitOut',width:100, align: 'right'},
            {name:'discountOut',index:'discountOut',width:100, align: 'right'},
            {name:'subTotalOut',index:'subTotalOut',width:50,align:'right',sorttype:'number',formatter:'number', summaryType:mysubTotalOustrSum,summaryTpl : '{0}'},                
            {name:'haveDetail',index:'haveDetail', width:100, align: 'right',hidden:true},
            {name:'optionsOut',index:'optionsOut', width:60, align: 'center'}
        ],
        viewrecords: true,
        sortname: 'idOut',            
        gridview : true,
        rownumbers: true,      
        rownumWidth: 30,            
        footerrow:true,
        grouping: true,
        groupingView : {                
            groupColumnShow : [true],
            groupText : ['<b>{0} - ({1})</b>'],
            groupCollapse : false,
            groupOrder: ['asc'],
            groupSummary : [true], 
            showSummaryOnHide:true,
            groupDataSorted : true
        }
        ,afterInsertRow:function(rowid,rowdata,rowElement){
            var deleteOption = "<a class=\"cb_deleteItem\" style=\"cursor: pointer;\" data-total=\""+rowdata.subTotalOut+"\" data-id=\""+rowid+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
            switch($('input[name=for_sales]:checked').val()){
                case 'direct':
                   $(this).jqGrid('setRowData',rowid,{optionsOut: deleteOption});
                   totalSubTotales = parseFloat(totalSubTotales)+parseFloat(rowdata.subTotalOut);
                   break;
                case 'salesorder':
                    totalSubTotales = parseFloat($('#amountToPaid').val());
                    break;
                case 'creditnote':
                    $(this).jqGrid('setRowData',rowid,{optionsOut: deleteOption});
                    var totalSub = 0.0;
                    var total = 0.0;
                    totalSub = parseFloat(totalSub)+parseFloat(rowdata.subTotalOut);
                    var rows = jQuery("#bc_salesDetail_jqGrid").jqGrid('getRowData');
                    
                        for(var i=0; i<rows.length; i++){
                            var row = rows[i];
                            total += parseFloat(row['subTotalOut']);  
                        }
                        
                    totalSubTotales = parseFloat(total) - parseFloat($('#amount_creditNote').val()) ;
                    break;
                default: 
                   break;
            }
            bctblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});
            
        }
        ,gridComplete:function(){  
            try{
                $('.cb_deleteItem').button({icons: {primary: 'ui-icon-trash'},text: false});
                $('.cb_deleteItem').click(function(){
                    var id_delete = $(this).data('id');
                    var subtotal = $(this).data('total');
                    confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                        if(response){
                            delete save_arrayDetails_lots[id_delete];
                            bctblSalesDetail.delRowData(id_delete);
                            // Descontar al total y actualiza
                            totalSubTotales = parseFloat(totalSubTotales) - parseFloat(subtotal);
                            bctblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});                              
                            $('input#codigo').val('');
                            $('input#codigo').focus();
                        }
                    });
                });  
            }catch(e){ 
                // GG *_* 
            }
        }
    }); 
    //Buscar codigo 
    function getProductByCode(code){
        showLoading=1;
        $.ajax({
            url:url_GetProductByCodigo,                        
            type:'POST',
            dataType:'jsonp',
            data:{
                code:function(){
                    return code;
                }
            }, 
            async:false,
            success:function(response){
                if(evalResponse(response)){
                    var exit;
                    var r=response.data;
                    var quantity = parseInt($('input#cb_quantity').val());
                    if(r.length>0){                            
                        $.each(r,function(index,data){ 
                            /* Si la cantidad es mayor al stock del producto */
                            if(parseInt($.trim(data.Amount)) >= quantity){
                                flags.isRange = 1;                                               
                                /***** Verificar Duplicidad de ID Product *****/
                                var ids=$("#bc_salesDetail_jqGrid").jqGrid('getDataIDs');                    
                                var count=0;
                                if(ids.length>0){
                                    $.each(ids,function(i,d){
                                        renDatos = $("#bc_salesDetail_jqGrid").getRowData(ids[i]);
                                        if(renDatos.id==data.idProduct){                               
                                            count=count+1;
                                        } 
                                    });
                                    if(count>0){ 
                                        exit = 0;
                                        msgBox('<?php echo __("Este Producto Ya esta Agregado") ?>','<?php echo __("Alerta") ?>');
                                        return false;
                                    }
                                }
                                if (exit == 0)
                                    return false;
                                /***** Fin Verificar Duplicidad de ID Product *****/
                                /*** Proceso de Verificar Lotes, Almacenes y Stock ***/ 
                                var id=data.idProduct;                             
                                $.ajax({
                                    url:url_getLotByProductId,                        
                                    type:'POST',
                                    dataType:'json',
                                    data:{idpg:id}, 
                                    async:false,
                                    success:function(response){
                                        if(evalResponse(response)){
                                            var r=response.data; 
                                            if(r=='not_store'){ 
                                                msgBox('<?php echo __("Su usuario de Venta no tiene asignado un almacén. <br/>Debe hacerlo para poder vender.") ?>','<?php echo __("Aviso") ?>');
                                                return false;                                       
                                                flags.haveLot = 0;
                                            }else{
                                                if(r.length>0){
                                                    flags.haveLot = 1; // Tiene lote
                                                    var j = 0;
                                                    $.each(r,function(i,d){ 
                                                        var itemDetail = new Object();
                                                        itemDetail.id = d.idLot;
                                                        itemDetail.productName = d.productName;
                                                        itemDetail.amount = d.amount;
                                                        itemDetail.creationDate = d.creationDate;                                                    
                                                        // Ingreso todos los lotes en el arreglo
                                                        arrayDetails_lots[j++] = itemDetail;
                                                    });                            

                                                }else{
                                                    msgBox('<?php echo __("No hay Productos en Lotes") ?>','<?php echo __("Alerta") ?>');
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                });                            
                                /*** Agregar al jqgrid ***/                                    
                                if(flags.haveLot){   
                                    cbitemDetail.id = data.idProduct;
                                    cbitemDetail.descriptionOut = data.product;
                                    cbitemDetail.idProductOut = data.haveDetail;
                                    cbitemDetail.quantityOut = $('#cb_quantity').val();
                                    cbitemDetail.unitPriceOut = data.productPriceSell;
                                    cbitemDetail.discountOut = $('#descuento').val();
                                    cbitemDetail.subTotalOut = ($.trim($('#cb_quantity').val())*$.trim(data.productPriceSell)) - $.trim($('#descuento').val()) ;
                                    cbitemDetail.amount = data.Amount;
                                    cbitemDetail.haveDetail=data.haveDetail;
                                    $("#bc_salesDetail_jqGrid").jqGrid('addRowData',iCountLot,cbitemDetail);                              
                                    //$('#custom_price').val(data.productPriceSell);                                    
                                }else{
                                    return false;
                                } 

                            }else{
                                flags.isRange = 0;
                                msgBox(stockInsuficiente,msgAlerta);
                                return false;
                            }
                        });
                        if (exit == 0)
                            return false;
                        // Si hay productos 
                        if(flags.isRange){
                            var temp_q =  0;
                            var c=0;
                            temp_q = parseInt(quantity); 
                            // Recorro los lotes para disminuir la cantidad segun su stock
                            $.each(arrayDetails_lots,function(i,d){
                                var s_itemDetail = new Object(); 
                                if(quantity <= parseInt(d.amount) && i==0){    
                                    s_itemDetail.id  = d.id;
                                    s_itemDetail.amount  = quantity; 
                                    save_arrayDetails_lots[iCountLot++] = s_itemDetail;                    
                                    return false;
                                }else{
                                    if(temp_q>0){                                         
                                        if(temp_q >= parseInt(d.amount)){   
                                            s_itemDetail.id  = d.id;
                                            s_itemDetail.amount  = parseInt(d.amount);
                                            temp_q= temp_q-parseInt(d.amount);  
                                            save_arrayDetails_lots[iCountLot++] = s_itemDetail;//                                          
                                        }else{            
                                            s_itemDetail.id  = d.id;
                                            s_itemDetail.amount  = temp_q;
                                            temp_q= temp_q-d.amount;   
                                            save_arrayDetails_lots[iCountLot++] = s_itemDetail;//  

                                        }                                           
                                    }                                 
                                }                                
                            });                                                    
                        }else{
                            return false;
                        }      
                    }else{
                        msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>');
                        return false;
                    }
                }
                /* Regresa el focus */
                $('input#codigo').val('');
                $('input#codigo').focus();
            }
        });
    }
    

    /***************** Desactivacion de codigo de barras *****************/
    tblSalesDetail = $('#salesDetail_jqGrid').jqGrid({            
        height: '100%',                        
        width:'820',
        datatype: 'local',
        sortable: false,
        colNames:[                                
            '<?php echo __("ID") ?>',
            '<?php echo __("ID Prod.") ?>',
            '<?php echo __("Descripción") ?>',
            '<?php echo __("Cantidad") ?>',
            '<?php echo __("Precio Unitario") ?>',                
            '<?php echo __("Impuesto") ?>',
            '<?php echo __("Descuento") ?>',
            '<?php echo __("SubTotal") ?>',
            '<?php echo __("Tiene Detalle") ?>',
            '<?php echo __("Opción") ?>'
        ],
        colModel:[                                
            {name:'idOut',index:'idOut',key:true,width:10,hidden:true},
            {name:'id',index:'id',width:10,hidden:true},
            {name:'descriptionOut',index:'descriptionOut',width:200},
            {name:'quantityOut',index:'quantityOut',width:70, align: 'center'},
            {name:'unitPriceOut',index:'unitPriceOut', width:70, align: 'right'},                
            {name:'taxUnitOut',index:'taxUnitOut',width:70, align: 'right'},
            {name:'discountOut',index:'discountOut',width:70, align: 'right'},
            {name:'subTotalOut',index:'subTotalOut',width:100,align:'right',sorttype:'number',formatter:'number', summaryType:mysubTotalOustrSum,summaryTpl : '{0}'},                
            {name:'haveDetail',index:'haveDetail', width:10, align: 'right',hidden:true},
            {name:'optionsOut',index:'optionsOut', width:90, align: 'center'}
        ],
        viewrecords: true,
        sortname: 'idOut',            
        gridview : true,
        rownumbers: true,      
        rownumWidth: 30,            
        footerrow:true,
        grouping: true,
        groupingView : {                
            groupColumnShow : [true],
            groupText : ['<b>{0} - ({1})</b>'],
            groupCollapse : false,
            groupOrder: ['asc'],
            groupSummary : [true], 
            showSummaryOnHide:true,
            groupDataSorted : true
        }
        ,afterInsertRow:function(rowid,rowdata,rowElement){
            var deleteOption = "<a class=\"cb_deleteItem\" style=\"cursor: pointer;\"  data-detail=\""+rowdata.haveDetail+"\" data-total=\""+rowdata.subTotalOut+"\" data-id=\""+rowid+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
            $(this).jqGrid('setRowData',rowid,{optionsOut: deleteOption});
            // Calculamos el Total 
            totalSubTotales = parseFloat(totalSubTotales)+parseFloat(rowdata.subTotalOut);
            tblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});
        }
        ,gridComplete:function(){  
            try{
                $('.cb_deleteItem').button({icons: {primary: 'ui-icon-trash'},text: false});
                $('.cb_deleteItem').click(function(){
                    var id_delete = $(this).data('id');
                    var subtotal = $(this).data('total');
                    var detail = $(this).data('detail');
                    confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                        if(response){
                            if(detail == 0 )
                                array_detail_id_pl.pop(id_delete);
                            else
                                array_detail_id_p.pop(id_delete);
                            tblSalesDetail.delRowData(id_delete);
                            // Descontar al total y actualiza
                            totalSubTotales = parseFloat(totalSubTotales) - parseFloat(subtotal);
                            tblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});                                     
                        }
                    });
                });  
            }catch(e){ 
                // GG *_* 
            }
        }
    });

    // Buscar codigo con codigo de barras 
    function getProductByDescription(code){
        showLoading=1;
        $.ajax({
            url:url_GetProductByDescription,                        
            type:'POST',
            dataType:'jsonp',
            data:{
                code:function(){
                    return code;
                }
            }, 
            async:false,
            success:function(response){
                var r=response.data;
                if(evalResponse(response) && r != false){
                    // Verificar duplicados
                    var exit = 0;
                    var ids=$("#salesDetail_jqGrid").jqGrid('getDataIDs'); 
                    $.each(ids,function(index,data){   
                        if(data == r.idProductDetail){
                           msgBox('<?php echo __("Ya se agregó este producto") ?>','<?php echo __("Alerta") ?>');
                           exit = 1;
                        }
                    });
                    if(exit == 1)
                        return false;
                    else{     
                        var pd= new Object();        
                        pd.idOut = r.idProductDetail;
                        pd.id = r.idProduct;
                        pd.descriptionOut= r.productName;
                        pd.quantityOut='1';
                        pd.taxUnitOut = '0.0';
                        pd.discountOut= '0.0';
                        if($('#custom_price').val() == ''){
                            pd.unitPriceOut=r.productPriceSell;
                            pd.subTotalOut= r.productPriceSell;  
                        }else{
                            pd.subTotalOut = $('#custom_price').val();
                            pd.unitPriceOut = $('#custom_price').val();
                        }
                        pd.haveDetail =r.haveDetail;
                        $("#salesDetail_jqGrid").jqGrid('addRowData',r.idProductDetail,pd); 
                    }       
                }else{
                     msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>');
                }
                /* Regresa el focus */
                $('input#cd_description').val('');
                $('input#cd_description').focus();
            }
        });
    }    
    /* Validacion de Agregar Producto */
    $.validator.addMethod("validQuantity", function(value, element, params) {            
        return (value <= 0 || value=='')?false:true; 
    }, "<?php echo __('La Cantidad Es Menor que 0') ?>");
    $.validator.addMethod("validPrice", function(value, element, params) {            
        return (value <= 0 || value=='')?false:true; 
    }, "<?php echo __('El Precio Es Menor que 0') ?>");
    formSalesDetail = $('#formSalesDetail').validate({
        rules: {                
            quantity :{
                required: true,
                validQuantity:true
            }
            ,description :{
                required: true
            },
            custom_price:{
                required:true,
                validPrice:true
            }

        },
        messages:{                
            custom_price :{required: '<?php echo __("Ingrese el precio.") ?>'},
            quantity :{required: '<?php echo __("La cantidad es requerida.") ?>'},
            description :{required: '<?php echo __("La Descripción es obligatoria.") ?>'}
        },
        errorContainer:'#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {
            $(element).addClass('ui-state-error');
        },
        unhighlight:function(element, errorClass, validClass){
            $(element).removeClass('ui-state-error');
        },
        submitHandler: function(form){
            if($("#select_store").attr('data')==$("#idstore").val()){
                var ids=$("#salesDetail_jqGrid").jqGrid('getDataIDs');                    
                var count=0;
                var exit = 0;
                if(ids.length>0){                        
                    $.each(ids,function(i,d){
                        if(d==$("#idproduct").val()){                               
                            count=count+1;
                        } 
                    });
                    if(count>0){                            
                        msgBox('<?php echo __("Este Producto Ya esta Agregado") ?>','<?php echo __("Alerta") ?>');
                        exit= 1 ;
                        return false;
                    }
                } 
                if(exit == 1){
                    return false;
                }
                var itemDetail = new Object();
                if($('#discountUnit').val()==''){
                    itemDetail.discountOut =0.0;
                }else{
                    itemDetail.discountOut = $('#discountUnit').val(); 
                }                    

                itemDetail.descriptionOut = $('#description').val();
                itemDetail.idProductOut = $('#idproduct').val();
                itemDetail.quantityOut = $('#quantity').val();
                itemDetail.unitPriceOut = $('#custom_price').val();
                itemDetail.subTotalOut = $('#priceTotal').val();
                itemDetail.amount = $('#amount').val();
                itemDetail.haveDetail=$("#haveDetail").val();
                if(itemDetail.haveDetail==true){                   
                    getAllDetailProduct($('#idproduct').val());
                    return false;
                }else{
                    getAllDetailOutProduct($('#idproduct').val());
                    return false;
                }
                var id=$('#idproduct').val().split(' ').join('');
                itemDetail.product_id = $('#idproduct').val().split(' ').join('');
                itemDetail.store_id = $('#idstore').val().split(' ').join('');
                itemDetail.id = id;

                // Impuesto
                if(bAvailableDocumentType && bTaxEnabled){
                    if($('input[name=document_type]:checked').data('force_tax') == 1){
                        itemDetail.haveTax = true;
                        itemDetail.taxUnitOut = $('#priceTax').val();
                    }else{
                        itemDetail.haveTax = false;
                        itemDetail.taxUnitOut = 0.0;
                    }
                }
                else if (bTaxEnabled){
                    if($('#active_tax').is(':checked')){
                        itemDetail.haveTax = true;
                        itemDetail.taxUnitOut = $('#priceTax').val();
                    }else{
                        itemDetail.haveTax = false;
                        itemDetail.taxUnitOut = 0.0;
                    }        
               }
               // Sin impuesto 
               itemDetail.haveTax = false;
               itemDetail.taxUnitOut = 0.0;

                total = returnarray(itemDetail.product_id,itemDetail.store_id);                    

                $("#salesDetail_jqGrid").jqGrid('addRowData',id,itemDetail);                                                  
                bctblSalesDetail.trigger("reloadGrid");                   
                formSalesDetail.resetForm();
                $(formSalesDetail.currentForm).find('input[type=text],input[type=hidden]').val('');
                $('#description').focus();                                       
            } else{
                msgBox('<?php echo __("Producto En Otro Almacen") ?>','<?php echo __("Alerta") ?>');
            }                
        }
    });
    

    /* Validacion de Cajero y Vendedor */
    var frm_customers=$("#frm_customers").validate({
        rules:{
            idCustomers:{
                required:true
            }
            ,cmb_listUserCode:{
                required:function(){
                    return $("#postformConfirmPayment input[name=for_sales][value='salesorder']:checked").is(':checked');
                }                   
            }
        },
        messages:{
            idCustomers:{
                required:txt_requestsclient                    
            }
            ,cmb_listUserCode:{required: txt_selectsearch }
        },
        errorContainer:'#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
        unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');}
    });     
    
    //Proceso Antiguo de Producto Detalle 
    /* Adquirir Producto Detalle 
    function getAllDetailProduct(idPG){
          showLoading=1;
          $.ajax({
              url:url_getDetailByProductId,                        
              type:'POST',
              dataType:'jsonp',
              data:{idpg:idPG}, 
              async:false,
              success:function(response){
                  if(evalResponse(response)){
                      var r=response.data;
                      if(r.length>0){                            
                          $.each(r,function(index,data){                                
                              var pd={};  
                              nameProduct = data.productName;
                              pd.productName=data.productName;
                              pd.idGeneral=data.idProductDetail;
                              pd.description=data.description;
                              pd.serie_electronica=data.serie_electronic;                            
                              $("#grid_income_detail").jqGrid('addRowData',data.idProductDetail,pd); 
                          });    
                          $("#input_detail").dialog('open');
                      }else{
                          msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>');
                      }
                  }
              }
          });
      }  
    // Dialog de Producto Detalle 
    $("#input_detail").dialog({
       title:'<?php echo "Ingresar Detalle Productos"; ?>',
       autoOpen: false,
       width: 855,
       height:'auto',
       show: "slide",
       hide: "slide",
       modal:true, 
       resizable: false, 
       buttons:{
           '<?php echo "Aceptar" ?>':function(){                     
               var data=$("#grid_income_detail2").jqGrid('getGridParam','data');
               if(data.length!=0 && data.length==$("#quantity").val()){ 
                   $.each(data,function(i,d){
                       array_detail_id_p.push(d.id);
                   });                        
                   var id=$("#idproduct").val();  

                   var itemDetail = new Object();
                   if($('#discountUnit').val()==''){
                       itemDetail.discountOut =0;
                   }else{
                       itemDetail.discountOut = $('#discountUnit').val(); 
                   }
                   itemDetail.id = id;
                   itemDetail.descriptionOut = $('#description').val();
                   itemDetail.quantityOut = $('#quantity').val();
                   itemDetail.unitPriceOut = $('#custom_price').val();
                   itemDetail.priceTotal = $('#priceTotal').val();
                   itemDetail.amount = $('#amount').val();
                   itemDetail.haveDetail=$("#haveDetail").val();                                                 
                   itemDetail.idProductOut = $('#idproduct').val();
                   itemDetail.subTotalOut = $('#priceTotal').val(); 
                   if(bAvailableDocumentType && bTaxEnabled){                                
                       if($('input[name=document_type]:checked').data('force_tax') == 1){
                           itemDetail.haveTax = true;
                           itemDetail.taxUnitOut = $('#priceTax').val();
                       }else{
                           itemDetail.haveTax = false;
                           itemDetail.taxUnitOut = 0;
                       }
                   }else if(bTaxEnabled){                                            
                       if($('#active_tax').is(':checked')){
                           itemDetail.haveTax = true;
                           itemDetail.taxUnitOut = $('#priceTax').val();
                       }else{
                           itemDetail.haveTax = false;
                           itemDetail.taxUnitOut = 0;
                       }
                   }
                   //Ninguno
                   itemDetail.haveTax = false;
                   itemDetail.taxUnitOut = 0;                                                    
                   total = returnarray(itemDetail.product_id,itemDetail.store_id);                    
                   $("#salesDetail_jqGrid").jqGrid('addRowData',id,itemDetail);                                                                      
                   $("#input_detail").dialog('close');                                    
                   $("#grid_income_detail").jqGrid("clearGridData");
                   $("#grid_income_detail2").jqGrid("clearGridData");

                   bctblSalesDetail.trigger("reloadGrid");                   
                   formSalesDetail.resetForm();
                   $(formSalesDetail.currentForm).find('input[type=text],input[type=hidden]').val('');
                   $('#description').focus();                  
                   $('#description').val('');                  
               }else{                        
                   alert('Aun falta ingresar datos');
               }
           },
           '<?php echo "Cancelar" ?>':function(){
               $(this).dialog('close');
               $("#grid_income_detail").jqGrid("clearGridData");                    
               $("#grid_income_detail2").jqGrid("clearGridData");                                         
               $("#input_hidden_id_product").val('');                    
           }
       },
       close:function(){                
           $("#grid_income_detail").jqGrid("clearGridData");                    
           $("#grid_income_detail2").jqGrid("clearGridData");                                         
           $("#input_hidden_id_product").val('');
       }
    });
    //Numero Uno
    var grid_income_detail=$("#grid_income_detail").jqGrid({
       datatype: "local",
       mtype: 'POST',            
       colNames:[
           '<?php echo __("Codigo General") ?>',
           '<?php echo __("Producto") ?>',
           '<?php echo __("Serie Electrónica") ?>',                                                        
           '<?php echo __("Descripción") ?>',
       ],
       colModel:[
           {name:'idGeneral',index:'idGeneral',hidden:true, key:true},
           {name:'productName',index:'productName',search: false },
           {name:'serie_electronica',index:'serie_electronica',align:'center'},       
           {name:'description',index:'description',align:'center', search:false},
       ],
       pager:'#grid_income_detail_pager',              
       rowNum:10, 
       sortorder: "asc",            
       width:400,
       height:250,                        
       viewrecords: true, 
       sortable: true,
       rownumbers:true,
       multiselect: true,
       caption:'<?php echo __("Lista de Productos") ?>',
       ondblClickRow:function(rowid,iRow,iCol,e){                       
           var array_grid_income_detail2=$("#grid_income_detail2").jqGrid('getGridParam','data');
           if(array_grid_income_detail2.length< $("#quantity").val()){                    
               var a=$("#grid_income_detail").getRowData(rowid);                    
               $("#grid_income_detail2").jqGrid('addRowData',rowid,a);                     
               $("#grid_income_detail").jqGrid('delRowData',rowid);                    
               grid_income_detail[0].clearToolbar();
               grid_income_detail.trigger("reloadGrid");                                                                           
               grid_income_detail2.trigger("reloadGrid"); 
           }else{
               alert('Ya Completo la salida');
           }
       }
   });  
    $("#grid_income_detail").jqGrid('navGrid','#grid_income_detail_pager',{del:false,add:false,edit:false,search:false}); 
    $("#grid_income_detail").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
    $("#grid_income_detail").jqGrid('navButtonAdd','#grid_income_detail_pager',{            
       caption:"", 
       buttonicon:"ui-icon-plusthick", 
       onClickButton: function(){
           var recs1 = $("#grid_income_detail").getGridParam("records");
           var recs2 = $("#grid_income_detail2").getGridParam("records");
           if(recs1>0){
               var val_grid_sel1=grid_income_detail.getGridParam('selarrrow');
               var val_grid_sel_tot1=val_grid_sel1.length+recs2;
               if(val_grid_sel1.length>0 && val_grid_sel_tot1<=$("#quantity").val()){
                   var len = val_grid_sel1.length;
                   for(var i=len-1;i>=0;i--){
                       var a=$("#grid_income_detail").getRowData(val_grid_sel1[i]);
                       $("#grid_income_detail2").jqGrid('addRowData',val_grid_sel1[i],a); 
                       $("#grid_income_detail").delRowData(val_grid_sel1[i]);
                   }                        
                   grid_income_detail[0].clearToolbar();
                   grid_income_detail.trigger("reloadGrid");                                                                           
                   grid_income_detail2.trigger("reloadGrid");   
               }else{
                   alert('No hay Productos Seleccionados');
               }
           }else{
               alert('No hay mas Productos en Almacen');
           }                 
       },
       title:'Quitar en Grupo',
       position:"last"
   });
    var grid_income_detail2= $("#grid_income_detail2").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Producto") ?>',
                '<?php echo __("Serie Electrónica") ?>',                                                        
                '<?php echo __("Descripción") ?>',
            ],
            colModel:[
                {name:'idGeneral',index:'idGeneral',hidden:true,key:true},
                {name:'productName',index:'productName',search:false, hidden: true},
                {name:'serie_electronica',index:'serie_electronica',align:'center', search:false},       
                {name:'description',index:'description',align:'center', search:false},
            ],
            pager:'#grid_income_detail_pager2',              
            rowNum:10, 
            sortorder: "asc",            
            width:400,
            height:250,                        
            viewrecords: true, 
            rownumbers:true,
            sortable: true,  
            multiselect: true,
            caption:'<?php echo __("Productos Seleccionados") ?>',
            ondblClickRow:function(rowid,iRow,iCol,e){ 
                var a=$("#grid_income_detail2").getRowData(rowid);                    
                $("#grid_income_detail").jqGrid('addRowData',rowid,a); 
                $("#grid_income_detail2").jqGrid('delRowData',rowid);
                grid_income_detail2[0].clearToolbar();
                grid_income_detail.trigger("reloadGrid");                                                                           
                grid_income_detail2.trigger("reloadGrid");                
            }
        });  
    $("#grid_income_detail2").jqGrid('navGrid','#grid_income_detail_pager2',{del:false,add:false,edit:false,search:false}); 
    $("#grid_income_detail2").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
    $("#grid_income_detail2").jqGrid('navButtonAdd','#grid_income_detail_pager2',{            
        caption:"", 
        buttonicon:"ui-icon-minusthick", 
        onClickButton: function(){
            var recs1 = $("#grid_income_detail").getGridParam("records");
            var recs2 = $("#grid_income_detail2").getGridParam("records");
            if(recs1>0){
                var val_grid_sel2=grid_income_detail2.getGridParam('selarrrow');

                if(val_grid_sel2.length>0){
                    var len = val_grid_sel2.length;
                    for(var i=len-1;i>=0;i--){
                        var a=$("#grid_income_detail2").getRowData(val_grid_sel2[i]);
                        $("#grid_income_detail").jqGrid('addRowData',val_grid_sel2[i],a); 
                        $("#grid_income_detail2").delRowData(val_grid_sel2[i]);
                    }                        
                    grid_income_detail[0].clearToolbar();
                    grid_income_detail.trigger("reloadGrid");                                                                           
                    grid_income_detail2.trigger("reloadGrid");   
                }else{
                    alert('No hay Productos Seleccionados');
                }
            }else{
                alert('No hay mas Productos en Almacen');
            }
        },
        title:'Quitar en Grupo',
        position:"last"
    });
    
    */

    /* Adquirir Producto Lote */
    function getAllDetailOutProduct(idPG){
        showLoading=1;
        $.ajax({
            url:url_getLotByProductId,                        
            type:'POST',
            dataType:'json',
            data:{idpg:idPG}, 
            async:false,
            success:function(response){
                if(evalResponse(response)){
                    var r=response.data;                        
                    var template='';                                                        
                    if(r.length>0){
                        $.each(r,function(i,d){ 

                            var lot="<div><label><?php echo __('Lote') ?>: </label><span>"+d.lotName+"</span></div>";
                            var product="<div><label><?php echo __('Producto') ?>: </label><span>"+d.productName+"</span></div>";
                            var amount_total="<div style='float:left'><label><?php echo __('Cantidad') ?>: </label></div>";                                
                            var amount_discount="<div class='search_client_wrapper_lot ui-corner-all ui-state-default'><span class='icon_lots'>"+d.amount+"</span><input type='text' data-id='"+d.idLot+"' data-amt='"+d.amount+"' size='5' class='txt_amount_ds ui-corner-all widget-content' /><div class='clear'></div></div>"

                            template=template+"<div class='lot_qua'>"+lot+product+"<div>"+amount_total+amount_discount+"</div></div><div style='clear:both'></div><hr class='hr_separate ui-widget-content'/>";                                                                   
                        });                            
                        template=template+"<div style='clear:both'></div>";
                        $("#show_data").html(template);
                        $(".txt_amount_ds").setMask();
                        $("#input_DetailOther").dialog('open');
                    }else{
                        msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>')
                    }
                }
            }
        });
    }  
    /* Dialog de Lotes */
    $('#input_DetailOther').dialog({
       title: detail_form_Title,
       autoOpen: false,
       width:"auto",
       height:'auto',
       show: "slide",
       hide: "slide",
       modal:true, 
       resizable: false, 
       buttons: {
           "<?php echo __("Guardar"); ?>":function(){
               var txt=$(".txt_amount_ds");
               var total=$("#quantity").val();
               var t=0;
               var inc=0;                    
               var array_temp=[];
               $.each(txt,function(i,d){
                   var prl={};
                   var val_lim=$(d).data('amt');
                   var val=($(d).val()=='')?0:$(d).val();
                   t=t+parseInt(val);                         
                   if(val_lim<parseInt(val)){                          
                       inc=inc+1;
                   }else{
                       prl.id=$(d).data('id');
                       prl.amount=val;
                       if(parseInt(val)>0)
                           array_temp.push(prl); 
                   }                   
               });  
               if(total!=t || isNaN(t) || inc>0){
                   $(".txt_amount_ds").addClass('ui-state-error');
                   return false;
               }else{
                   $(".txt_amount_ds").removeClass('ui-state-error');
                   var data=array_temp;                        

                   $.each(data,function(i,d){ 
                       array_detail_id_pl.push(d);
                      });                        
                   var id=$("#idproduct").val(); 
                   var itemDetail = new Object();
                   if($('#discountUnit').val()==''){
                       itemDetail.discountOut ='0.0';
                   }else{
                       itemDetail.discountOut = $('#discountUnit').val(); 
                   }
//                   itemDetail.idOut = ;
                   itemDetail.id = $('#idproduct').val();
                   itemDetail.descriptionOut = $('#description').val();
                   itemDetail.quantityOut = $('#quantity').val();
                   itemDetail.unitPriceOut = $('#custom_price').val();
                   itemDetail.taxUnitOut = '0.0';
                   itemDetail.haveDetail=$("#haveDetail").val();                                                 
                   itemDetail.subTotalOut =$('#quantity').val() * $('#custom_price').val();                  
                   $("#salesDetail_jqGrid").jqGrid('addRowData',id,itemDetail);                                                                                 
 
                   $("#input_DetailOther").dialog('close');                                     
                   bctblSalesDetail.trigger("reloadGrid");                   
                   formSalesDetail.resetForm();
                   $(formSalesDetail.currentForm).find('input[type=text],input[type=hidden]').val('');
                   $('#description').focus();
               }
           },
           "<?php echo __("Cancelar"); ?>": function() {
               $(this).dialog('close');   
           }
       }
   });
    
    
    /* Funciones */
    function returnarray(idproduc,idstore){
      $.each(arrayDetails, function(key, value) {
          if($('#idproduct').val() == value.product_id && $('#idstore').val()==value.store_id){
              countarray = 1;
              keycountarray = key;
              quantity = value.quantity;
          }
      });
    }
    // Calcular precios 
    calculationPriceDetail = function(){
        priceUnit = $('#custom_price').val().split(' ').join('');
        priceUnit = priceUnit==''?0:parseFloat(priceUnit);
        fPreTotal = priceUnit;
        if(bBarcodeReader == 1){
            quantity = $('#cb_quantity').val().split(' ').join('');
            discount = $('#descuento').val().split(' ').join('');
        }else{
            quantity = $('#quantity').val().split(' ').join('');
            discount = $('#discountUnit').val().split(' ').join('');
        }
        quantity = quantity==''?0:parseFloat(quantity);
        priceTotal = priceUnit * quantity;
        $('#priceUnit').val(priceTotal.toFixed(2));
        $('#priceTotal').val(priceTotal.toFixed(2));           
        discount = discount==''?0:parseFloat(discount);

        if(discount > priceTotal){
            discount.val(0);
            discount = 0;
        }else{
            //var total_discount = priceTotal * discount;
            priceTotal = priceTotal - discount;
            $('#priceTotal').val(priceTotal.toFixed(2));
        }
        if (bTaxEnabled){    
            if($('input[name=document_type]:checked').data('force_tax') == 1){                                    
                fTax = (priceTotal*<?php echo $fTax ?>)/(100+<?php echo $fTax ?>);                
                fTax = roundFloat(fTax, 2);
                fPreTotal = priceTotal;
                priceUnit = priceTotal - fTax;
                $('#priceUnit').val(priceUnit.toFixed(2));                    
                $('#priceTax').val(fTax.toFixed(2));
                $('.priceTax').css('display','block');

            }else{
                var zero = 0;
                $('#priceTax').val(zero.toFixed(2));
                $('.priceTax').css('display','none');
            }    
        } else if (bTaxEnabled){                
            if($('#active_tax').is(':checked')){                
                fTax = (priceUnit*<?php echo $fTax ?>)/(100+<?php echo $fTax ?>);
                fTax = roundFloat(fTax, 2);
                fPreTotal = priceUnit;
                priceUnit = priceUnit - fTax;
                $('#priceUnit').val(priceUnit.toFixed(2));                    
                $('#priceTax').val(fTax.toFixed(2));
                $('.priceTax').css('display','block');

            }else{
                $('.priceTax').css('display','none');
            }
        }
    };
    // Calcular monto a pagar 
    fnUpdateConfirm_post = function(){
           if($("#postformConfirmPayment input[name=payment_type][value='<?php echo $cash; ?>']:checked").is(':checked') == true){
               fConfirmPayment_post = $('#post_confirm_payment').val().split(' ').join('');
               fConfirmPayment_post = fConfirmPayment_post==''?0:fConfirmPayment_post;
               fReturnCash_post = parseFloat(fConfirmPayment_post) - parseFloat($('.post_confirm_payment_total_amount').text());
               $('.post_confirm_payment_return_cash').text(fReturnCash_post.toFixed(2));
           }
           else if($("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto; ?>']:checked").is(':checked') == true){
               amount_paymentCreditMixto = $('#amount_paymentCreditMixto').val();
               amount_paymentCashMixto = $('#amount_paymentCashMixto').val();
               totalMixto = parseFloat(amount_paymentCreditMixto) + parseFloat(amount_paymentCashMixto);
               fReturnCash_post = parseFloat(totalMixto) - totalSubTotales;
               $('#amount_paymentreturn').val(fReturnCash_post.toFixed(2));
           }

    };
    // Eliminar filas del JQgrid
    function deleteRowsAll(tblJQgrid){
        var rowIds = tblJQgrid.jqGrid('getDataIDs');
        for(var i=0,len=rowIds.length;i<len;i++){
            var currRow = rowIds[i];
            tblJQgrid.jqGrid('delRowData', currRow);
        }
    }
    // Calcular Subtotal
    function mysubTotalOustrSum(val, name, record){  
        var total = parseFloat(val||0)+parseFloat(record.subTotalOut);
        return total;
    }
    
    /*****************     Guardar Venta  *****************************/
    $(document).keydown(function(tecla){
        if (tecla.keyCode == 112) { 
            SaveSales();
        }
    });
    $("#saveSales").on('click',function(e){ 
        SaveSales();
    });
    // Cancelar Venta
    $("#cancelSales").on('click',function(e){
          window.location.reload();
    });
    // Guardar Venta
    function SaveSales(){        
        if(!$("#frm_customers").valid())
            return false;  
        // Anulacion 
        if( $('input[name=for_sales]:checked').val() == 'creditnote')
        {
            if(totalSubTotales < 0){
                 msgBox('<?php echo __("El total es menor al credito") ?>');
                 return false;
            }
        }
   
        // Valido si tengo productos en mi JQgrid
        if(bBarcodeReader == 1){
            var bc_arrayDataGrid=$("#bc_salesDetail_jqGrid").jqGrid("getRowData"); 
            if(bc_arrayDataGrid.length<=0){
                msgBox('Usted no tiene productos para su Venta','Alerta de Venta');
                return false;
            }else{
                $('input#post_confirm_payment_hidden_total_amount').val(totalSubTotales.toFixed(2));
                $('.post_confirm_payment_total_amount').text($('#post_confirm_payment_hidden_total_amount').val());
                $("#frmTypePay").dialog('open'); 
            }
        }
        else{
            var arrayDataGrid=$("#salesDetail_jqGrid").jqGrid("getRowData"); 
            if(arrayDataGrid.length<=0){ 
                msgBox('Usted no tiene productos para su Venta','Alerta de Venta');
                return false;
            }else{
                $('input#post_confirm_payment_hidden_total_amount').val(totalSubTotales.toFixed(2));
                $('.post_confirm_payment_total_amount').text($('#post_confirm_payment_hidden_total_amount').val());
                $("#frmTypePay").dialog('open');
            }
     }
    }
    // Pago de la Venta 
    $("#frmTypePay").dialog({
        title:'<?php echo "Seleccionar el Tipo de Pago"; ?>',
        autoOpen: false,
        width: 'auto',
        height:'auto',
        show: "slide",
        hide: "slide",
        modal:true, 
        resizable: false, 
        position:["center", 70],
        buttons:{
            '<?php echo "Aceptar" ?>':function(){    
                $("#postformConfirmPayment").submit();             
            },
            '<?php echo "Cancelar" ?>':function(){
                $(this).dialog('close');
                $("#postformConfirmPayment").reset(); 
            }
        },
        close:function(){                                        
            $("#postformConfirmPayment").reset();
            
        }
    });
    // Confirmacion de Pago
    // Para el pago mixto 
    $.validator.addMethod("validMixto", function(value, element, params) { 
            $amountCash = $('input#amount_paymentCashMixto').val();
            $amountCredit = $('input#amount_paymentCreditMixto').val();
            $totalReal = totalSubTotales.toFixed(2);
            // Verificar el monto de credito y efectivo      
            $calculo = parseFloat($amountCash) + parseFloat($amountCredit);
            if($("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked')== true){
                return parseFloat($calculo) >= parseFloat($totalReal)?true:false; 
            }else 
                return true;
            
    }, "");
    $.validator.addMethod("validPayment", function(value, element, params) { 
            if($("#postformConfirmPayment input[name=payment_type][value='<?php echo $cash ?>']:checked").is(':checked')== true){
                return parseFloat(value) >= parseFloat($('input#post_confirm_payment_hidden_total_amount').val())?true:false; 
            }else
                return true;
        
    }, "");
    var postformConfirmPayment = $('#postformConfirmPayment').validate({
              rules:{
                  post_confirm_payment: {
                      required: function(){
                          return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $cash ?>']:checked").is(':checked');
                      },
                      validPayment:true

                  },
                  // Credit 
                  typeCard:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                     }
                  },
                  cardNumber:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                     }
                  },
                  operationNumber:{
                      required:function(){
                          return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                      }
                  },
                  amount_paymentCredit:{
                       required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                       } 
                  },
                  // Mixto
                  typeCardMixto:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked');
                     }
                  },
                  cardNumberMixto:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked');
                     }
                  },
                  operationNumberMixto:{
                      required:function(){
                          return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked');
                      }
                  },
                  amount_paymentCreditMixto:{
                       required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked');
                       },
                       validMixto:true
                  },
                  amount_paymentCashMixto:{
                       required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $mixto ?>']:checked").is(':checked');
                       },
                       validMixto:true
                  }
                      
                 

              },
              messages:{
                  post_confirm_payment: {
                      required: '<?php echo __('El pago es requerido') ?>',
                      validPayment: '<?php echo __('El pago debe ser mayor o igual al monto total') ?>'

                  },
                  typeCard:{required: '<?php echo __('El tipo de tarjeta es requerido') ?>'},
                  cardNumber:{required: '<?php echo __('El numero de tarjeta es requerido') ?>'},
                  operationNumber:{required: '<?php echo __('El numero de operacion  es requerido') ?>'},
                  amount_paymentCredit:{required: '<?php echo __('El pago es requerido') ?>'},
                  typeCardMixto:{required: '<?php echo __('El tipo de tarjeta es requerido') ?>'},
                  cardNumberMixto:{required: '<?php echo __('El numero de tarjeta es requerido') ?>'},
                  operationNumberMixto:{required: '<?php echo __('El numero de operacion  es requerido') ?>'},
                  amount_paymentCreditMixto:{required: '<?php echo __('El pago credito es requerido') ?>',
                                             validMixto:'<?php echo __('El monto pagado es menor al total') ?>'  },
                  amount_paymentCashMixto:{required: '<?php echo __('El pago efectivo es requerido') ?>'}
              },
              errorContainer:'#errorMessages',
              errorLabelContainer: "#errorMessages .content .text #messageField",
              wrapper: "p",
              highlight: function(element, errorClass) {
                  $(element).addClass('ui-state-error');
              },
              unhighlight:function(element, errorClass, validClass){
                  $(element).removeClass('ui-state-error');
              },
              submitHandler: function(postformConfirmPayment){                        
                 var arrayDataGrid=$("#salesDetail_jqGrid").jqGrid("getRowData");
                 var bc_arrayDataGrid=$("#bc_salesDetail_jqGrid").jqGrid("getRowData"); 
                 var oData = new Object();
                 //Si tiene productos el jqGrid
                 if(bBarcodeReader == 1){ // Si esta activo
                      if(bc_arrayDataGrid.length<=0){
                          msgBox('Usted no tiene productos para su Venta','Alerta de Venta');
                          return false;
                      }else{
                          oData.detail =bc_arrayDataGrid;
                          oData.detail_product_l=save_arrayDetails_lots;
                      }
                  }
                 else{
                      if(arrayDataGrid.length<=0){
                          msgBox('Usted no tiene productos para su Venta','Alerta de Venta'); 
                          return false;
                      }
                      else{
                          oData.detail =arrayDataGrid;
                          oData.detail_product_l=array_detail_id_pl;
                      }
                  }
                 confirmBox(confirmationSales,'Venta',function(event){
                     if(event){                        
                         oData.header = $("#frm_customers").serializeObject();
                         oData.payment = $("#postformConfirmPayment").serializeObject();    
                         oData.idStore = $("#select_store").attr('data'); 
                         oData.userCode = $("#cmb_listUserCode").val();
                         oData.detail_product = array_detail_id_p;
                         oData.type = $('input[name=for_sales]:checked').val();
                         oData.methodPayment = $('input[name=payment_type]:checked').val();                    
                         if($('input[name=for_sales]:checked').val()=='creditnote'){
                             oData.header.amountCancel = $('input#amount_creditNote').val();
                             oData.header.idCreditNote = $('input#hidden_idSale').val();
                         }
                         showLoading=1;
                         $.ajax({                
                             url:url_RegisterSales,                        
                             type:'POST',
                             dataType:'json',
                             data:oData, 
                             async:false,
                             success:function(response){                    
                                 if(evalResponse(response)){
                                     var data = response.data;
                                     var id = response.id;
                                     if(data.length>0){
                                         msm = '';
                                         $.each(data, function(key, value) { 
                                             msm = msm + ' Stock actual en el almacén <strong>' + value.almn + '</strong> del producto <strong>' + value.product+'</strong> = <strong>'+value.stock + '</strong><br/>';
                                         });
                                         msgBox(msm);
                                     }else{
                                         confirmBox  ('<?php echo __("Desea Imprimir Documentos?"); ?>','Confirmar Impresión',function(e){
                                             if(e){
                                                 var my_pop_up = window.open('/private/sales/getPrintSales?sales_id='+id.idsales+'&document='+id.document, '<?php echo __("Impresión de Documento de Envío") ?>');

                                             }else{                                                
                                             }
                                             window.location.reload();
                                         });                                                                                                                    
                                     }
                                 }
                             }
                         });                      
                   }
                 });                                    
              }
     });       

    // Tipos de Pago
    $('input[name=payment_type]').click(function(){
            $('.post_confirm_payment_return_cash').text('0.0');
            var b_paymentType = $(this).val();
            // Evaluo el tipo de pago 
            if(b_paymentType == typeCash ){
                    $("#frmCreditCash").show();
                    $("#frmCreditCard").hide(); 
                    $('#frmMixtoCard').hide();
                    $('input#post_confirm_payment_hidden_total_amount').val(totalSubTotales.toFixed(2));
                    $('.post_confirm_payment_total_amount').text($('#post_confirm_payment_hidden_total_amount').val());
                    $('#post_confirm_payment').val('0.0');
                    $('.post_confirm_payment_return_cash').text('0.0');                
            }else if(b_paymentType ==typeCard){
                    $("#frmCreditCard").show(); 
                    $("#frmCreditCash").hide();
                    $('#frmMixtoCard').hide();
                    $("#cardNumber").val('');
                    $("#operationNumber").val('');
                    $('input#amount_paymentCredit').val(totalSubTotales.toFixed(2));  
                    $('input#amount_paymentCredit').attr('readonly',true);
                    $('.post_confirm_payment_total_amount').text('0.0');
            }else if(b_paymentType ==typeMixto){
                    $('#frmMixtoCard').show();
                    $("#frmCreditCash").hide();
                    $("#frmCreditCard").hide(); 
                    $("#cardNumberMixto").val('');
                    $("#operationNumberMixto").val('');
                    $('input#amount_paymentCashMixto').val('0.0');  
                    $('input#amount_paymentCreditMixto').val('0.0');
                    $('#amount_paymentreturn').val('0.0');
            }
     }); 
         
         
         
    /******************************** Reservas  **************************************/ 
    function salesOrderSales(){           
         $.ajax({
             url:url_detailSalesOrderSales,
             data:{ idOrder: $('#select_purchaseOrders').val() },
             async:false,
             success:function(response){
                    $("#bc_salesDetail_jqGrid").clearGridData();
                 arrayDetails = {};
                 deleteRowsAll( bctblSalesDetail);
                 if(evalResponse(response)){
                     iCountItem = 0;
                     if(response.data.length>0){
                         $.each(response.data, function(index, array) {
                             $('#chars_Customers').val(array.customers);
                             $('#idCustomers').val(array.idCustomers);
                             $('#idOrderSales').val(array.idSalesOrder);
                             $('#amountPaid').val(array.amountPaid);
                             $('#amountToPaid').val(array.amountToPaid);
                             $('#totalAmount').val(array.totalAmount);
                             $('#cmb_listUserCode').val(array.idUserCode);
                             $('#cmb_listUserCode-combobox').val($('#cmb_listUserCode option:selected').text());
                             if(array.typeCustomers == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                                 $('.icon_sender_type').removeClass('ui-icon-home ui-icon-help').addClass('ui-icon-person');
                             }
                             if(array.typeCustomers == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                                 $('.icon_sender_type').removeClass('ui-icon-person ui-icon-help').addClass('ui-icon-home');
                             } 
                             var itemDetail = new Object();
                             itemDetail.description = array.description;
                             itemDetail.quantity = array.quantity;
                             itemDetail.priceUnit = array.unitPrice;
                             itemDetail.priceTotal = array.totalPrice;
                             itemDetail.amount = array.amount;
                             itemDetail.taxUnitOut = 0;                      
                             itemDetail.discountOut = array.discount;

                             itemDetail.product_id = array.idProduct;
                             itemDetail.store_id = array.idStore;

                             arrayDetails[iCountItem] = itemDetail;
                             bctblSalesDetail.addRowData(iCountItem, {
                                 descriptionOut:array.description,
                                 quantityOut:array.quantity,
                                 unitPriceOut:array.unitPrice,                    
                                 discountOut: array.discount,                    
                                 taxUnitOut: parseFloat(0).toFixed(2),
                                 subTotalOut:array.totalPrice,
                                 idProductOut:array.idProduct,
                                 idStoreOut:array.idStore
                             });

                             iCountItem++;
                         });
                     }else{
                         msgBox('<?php echo __("Nota de Pedido no Existe") ?>');
                     }
                 }else {
                     msgBox(response.msg,response.code);
                 }
             }
         });
     }
    $( "#select_purchaseOrders" ).autocomplete({
        minLength: 1,
        autoFocus: true,
        source:function(req,response){
            $.ajax({
                url:url_getCodeOrderSales,
                dataType:'jsonp',
                data: {
                    term : req.term
                },
                success:function(resp){
                    if(evalResponse(resp)){
                        response( $.map( resp.data, function( item ) {

                            var text = "<span class='spanSearchLeft'>" + item.name + "</span>";
                            text += "<div class='clear'></div>";
                            return {
                                label:  text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>" ),
                                value:  item.name,
                                id : item.id
                            }
                        }));
                    }else{
                        msgBox(resp.msg,resp.code);
                    }
                }
            });
        },
        focus: function(event, ui) {
            //                $('#custom_price').val(ui.item.price);
        },
        select: function( event, ui ) {
            $('#hidden_idSale').val(ui.item.id);
        } })
    .data( "autocomplete" )._renderItem = function( ul, item ) {            
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
        };
        
        
        
    /******************************** Anulacion *************************************/
    function salesCreditNote(){
        $.ajax({
            url:url_detailSalesCreditNote,
            data:{idSale: $('#hidden_idSale').val() },
            async:false,
            beforeSend:function(){
                $("#loading").dialog("open");},
            complete:function(){
                $("#loading").dialog("close");                                               
            },
            success:function(response){                    
                //deleteRowsAll(tblSalesDetail);
                if(evalResponse(response)){

                    if(response.data.length>0){
                        $.each(response.data, function(index, array) {
                            $('#hidden_idSale').val(array.idCreditNote);
                            $('#amount_creditNote').val(array.amountSale);
                            $('#date_saleCanceled').val(array.date_canceled);                                
                        });
                    }else{
                        //GG
                        msgBox('<?php echo __("Nota de Credito ya ha sido Usada.") ?>');
                    }

                }else {
                    msgBox(response.msg,response.code);
                }


            }
        });

    }
    $( "#select_creditNote" ).autocomplete({
        minLength: 1,
        autoFocus: true,
        source:function(req,response){
            $.ajax({
                url:url_getSerieCorrelativeByTerm,
                dataType:'jsonp',
                data: {
                    term : req.term
                },
                success:function(resp){
                    if(evalResponse(resp)){
                        response( $.map( resp.data, function( item ) {

                            var text = "<span class='spanSearchLeft'>" + item.name + "</span>";
                            text += "<div class='clear'></div>";
                            return {
                                label:  text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>" ),
                                value:  item.name,
                                id : item.id
                            }
                        }));
                    }else{
                        msgBox(resp.msg,resp.code);
                    }
                }
            });
        },
        focus: function(event, ui) {
            //                $('#custom_price').val(ui.item.price);
        },
        select: function( event, ui ) {
            $('#hidden_idSale').val(ui.item.id);
        } 
        })
    .data( "autocomplete" )._renderItem = function( ul, item ) {            
        return $( "<li></li>" )
        .data( "item.autocomplete", item )
        .append( "<a>" + item.label + "</a>" )
        .appendTo( ul );
        };           
    
    
    /* CLIENTES 
     * 
     *
    var url_SaveCustomers = '/private/customers/createCustomers'+jquery_params; 
    var url_Ubigeo= '/private/customers/ubigeoCustomers'+jquery_params;
     *   $("#passanger_dst").combobox();
        $("#passanger_prov").combobox({
            selected:function(){
                fillCombo("passanger_dst", $("#passanger_prov").val(), $("#passanger_dpto").val());
            }
        });
        $("#passanger_dpto").combobox({
            selected:function(){
                fillCombo("passanger_prov", $("#passanger_dpto").val(),'provincia');
            }
        });

        /***********************     Nuevo Cliente   ******************
        /* Tipo de Cliente
        $('input:radio[name=typeClient]').change(function(){
            $(this).each(function(){
                if($(this).val()=='<?php echo $CLIENT_TYPE_PERSON ?>'){
                    $('li.containerCompany').hide();
                    $('li.containerPerson').show();
                }else{
                    $('li.containerPerson').hide();
                    $('li.containerCompany').show();
                }
            });
        });
        function fillCombo(updateId,value,des){
            $.ajax({                  
                type:"POST",
                dataType:"json",
                url:url_Ubigeo,
                data:{
                    id:value,
                    des:des   
                },
                success:function(data){
                    $("#"+updateId).empty();
                    $.each(data, function(i, item) { 
                        $("#"+updateId).append("<option value='" 
                            +item.valor+"'>" +item.descripcion+ "</option>");
                    });
                }
            });
        }  
        // Formulario de Registro Cliente
        $('.addSender').click(function(){          
            o_btn_add_client_position = $(this).offset();            
            formRegisterClient.currentForm.reset();
            formRegisterClient.resetForm();
            $('input, select, textarea', formRegisterClient.currentForm).removeClass('ui-state-error');           
            $('#div_register_client').css({
                left: (o_btn_add_client_position.left+60)+'px'
                , top: (o_btn_add_client_position.top-60)+'px'
            });
            
                !$('#div_register_client').is(':visible')?$('#div_register_client').show('slide'):null;            
            
        });     
        $('.ui_icon_close_register_client').click(function(){
            formRegisterClient.currentForm.reset();
            formRegisterClient.resetForm();
            $('input, select, textarea', formRegisterClient.currentForm).removeClass('ui-state-error');
            $('#div_register_client').hide('slide');
        });
        // Validación de Registro de Cliente
        formRegisterClient = $('#formRegisterClient').validate({
            rules: {                
                company_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_COMPANY ?>']:checked").is(':checked');
                    }
                },
                company_code :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_COMPANY ?>']:checked").is(':checked');
                    },
                    maxlength:11,
                    minlength:11
                },
                passanger_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON ?>']:checked").is(':checked');
                    }
                },
                passanger_lastname :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON ?>']:checked").is(':checked');
                    }
                },
                passanger_mail :{
                    required: true,
                    email:true
                },
                passanger_documento:{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON ?>']:checked").is(':checked');
                    },
                    maxlength:8,
                    minlength:8
                }
                
            },
            messages:{
                company_name :{required: '<?php echo __("La Razón Social es obligatorio.") ?>'},
                company_code :{required: '<?php echo __("El RUC es obligatorio.") ?>',
                    maxlength: '<?php echo __("El RUC no es correcto"); ?>',
                    minlength: '<?php echo __("El RUC no es correcto"); ?>'
                },
                
                passanger_name :{required: '<?php echo __("El Nombre es obligatorio.") ?>'},
                passanger_lastname :{required: '<?php echo __("El Apellido es obligatorio.") ?>'},
                passanger_mail :{
                    required: 'El E-mail es requerido.',
                    email:'El E-mail es incorrecto.'
                },
                passanger_documento:{
                    required:'<?php echo __("El DNI es obligatorio.") ?>',
                    maxlength: '<?php echo __("El DNI no es correcto"); ?>',
                    minlength: '<?php echo __("El DNI no es correcto"); ?>'
                    
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){                
                $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_SaveCustomers,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res)){                            
                            var data = res.data;                            
                            if(data.typeCustomers == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
                            }else if(data.typeCustomers == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
                            }                
                            $('#idCustomers').val(data.idCustomers);
                            $('#chars_Customers').val(data.customers);
                            msgBox('<?php echo __("Registrado"); ?>');
                            $('#div_register_client').hide('slide');
                            formRegisterClient.currentForm.reset();                                                        
                        }
                    }
                });
            }
        });
     
     
     
     
     
     * 
     * 
     * */

    fnLoadCheck();
});
</script>

<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;"<?php endif ?>>
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4><?php echo __("Registro de Ventas") ?></h4>
        </div>
    </div>
    <?php if ($bAvailableDocumentType && !$documentManual): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix"></div>
    <?php endif ?>
    <div class="clear"></div>
</div>
<div class="divContent">
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="divContentPadding">
            <ul>
                <li>
                    <div class="divLine">
                        <!--Tipo de Venta-->
                        <dl>
                            <dd>
                                <label for="chars_Customers"><?php echo __("Tipo de Venta"); ?>:</label>
                            </dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div> 
                                <input type="radio" id="direct" name="for_sales" value="direct" checked="checked"/><label for="direct">Directa</label>
                                <input type="radio" id="salesorder" name="for_sales" value="salesorder" /><label for="salesorder">Reservas</label>
                                <input type="radio" id="creditnote" name="for_sales" value="creditnote" /><label for="creditnote">Anulacion</label>
                                <div class="clear"></div>
                            </dd>
                        </dl>
                        <br/>
                        <!--Reservas-->
                        <div>
                            <div class="note" style="margin-bottom: 10px;">
                                <dl  style="width: 290px;">
                                    <dd class="buttonsContainer" style="margin-top: 4px; width: 360px">
                                        <label><?php echo __("Código de Reserva"); ?>:</label>
                                        <input type="text" id="select_purchaseOrders" name="select_purchaseOrders">
                                        <button type="button" id="search" class="ui-state-highlight"><?php echo __("Buscar") ?></button>
                                    </dd>
                                    <dd class="buttonsContainer" style="margin-top: 7px;">
                                        <label><?php echo __("Total de Venta"); ?>:</label>
                                        <input class="orderSales ui-state-highlight" type="text" id="totalAmount" name="totalAmount" readonly>
                                    </dd>
                                    <dd class="buttonsContainer" style="margin-top: 7px;">
                                        <label><?php echo __("Monto a Cuenta"); ?>:</label>
                                        <input class="orderSales ui-state-highlight" type="text" id="amountPaid" name="amountPaid" readonly>
                                    </dd>
                                    <dd class="buttonsContainer" style="margin-top: 7px;">
                                        <label><?php echo __("Saldo"); ?>:</label>
                                        <input class="orderSales ui-state-error" type="text" id="amountToPaid" name="amountToPaid" readonly>
                                    </dd>
                                    <div class="clear"></div>
                                </dl>
<!--                                  <div>-->
                                    <dl>
                                        
                                    </dl>
<!--                         </div>-->
                            </div>
                        </div>
                        <div class="clear"></div>
                        <!--Anulacion-->
                        <div>                        
                            <div class="credNote">
                                <dl  style="width: 290px;">
                                    <dd class="buttonsContainer" style="margin-top: 4px; width: 360px">
                                        <label><?php echo __("Código de Venta"); ?>:</label>
                                        <input type="text" id="select_creditNote" name="select_creditNote">
                                        <input type="hidden" id="hidden_idSale" name="hidden_idSale">
                                        <button type="button" id="searchCreditNote" class="ui-state-highlight"><?php echo __("Buscar") ?></button>
                                    </dd>
                                    <dd class="buttonsContainer" style="margin-top: 7px;">
                                        <label><?php echo __("Monto Cancelado"); ?>:</label>
                                        <input class="orderSales ui-state-highlight"  type="text" id="amount_creditNote" name="amount_creditNote" readonly>
                                    </dd>
                                    <dd class="buttonsContainer" style="margin-top: 7px;">
                                        <label><?php echo __("Fecha Cancelación"); ?>:</label>
                                        <input  class="orderSales ui-state-error" type="text" id="date_saleCanceled" name="date_saleCanceled" readonly>
                                    </dd>
                                    <div class="clear"></div>
                                </dl>
                            </div>

                        </div>
                    </div>
                    <!--Almacen Asignado-->
                    <div class="divLeft" style="padding-left:20px;float: right;font-weight: bolder">
                        <dl>
                            <dd>
                                <label><?php echo __("Almacén Asignado"); ?></label>                                
                            </dd>
                            <dd>                                
                                <input id="select_store" name="select_store" value="<?php echo (count($store) != 0) ? $store[0]['display'] : 'ASIGNESE UN ALMACEN'; ?>" data="<?php echo (count($store) != 0) ? $store[0]['id'] : '0'; ?>" class="<?php echo (count($store) == 0) ? 'ui-state-error' : 'ui-state-highlight'; ?> ui-corner-all" readonly="" style="padding: 7px;font-weight: bolder;" />
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div> 
                </li>
                <li>
                    <hr class="hr_separate ui-widget-content"/>
                </li>
            </ul>
            <!--Cliente-->
            <form id="frm_customers">
                <ul>
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Cliente"); ?>:</label>
                                </dd>
                                <dd class="relative">
                                    <div class="search_client_wrapper ui-corner-all ui-state-default">
                                        <span class="icon_sender_type ui-icon ui-icon-help"></span>
                                        <input class="autocompleteInput inputData" type="text" id="chars_Customers" name="chars_Customers" value="<?php echo 'Búsqueda de Cliente' ?>"/>
                                        <input type="hidden" id="idCustomers" name="idCustomers"/>
                                        <input type="hidden" id="idOrderSales" name="idOrderSales"/>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="order">
                                        <!--<button type="button" class="addSender"><?php echo __("Agregar Cliente") ?></button>-->
                                    </div>
                                    <div id="auto_div_person_sender"></div>
                                    <div class="clear"></div>
                                </dd>                            
                            </dl>
                            <?php if($aSeller) :?>
                            <dl>
                                <dd>
                                    <!--CODIGO USUARIO VENDEDOR-->
                                    <label  for="cmb_listUserCode"><?php echo __("Vendedor") ?>:</label>
                                </dd>
                                  <dd >
                                    <select  style="width: 150px"  size="1" id="cmb_listUserCode" name="cmb_listUserCode" class="Combo" >
                                        <option value=""><?php echo __("Seleccionar Usuario") ?></option>
                                        <?php foreach ($listUserCode as $usercode) { ?>
                                            <option value="<?php echo $usercode['id'] ?>"><?php echo $usercode['nameUser'] ?></option>
                                        <?php } ?>                            
                                    </select>
                                
                                </dd>
                            </dl>
                             <?php endif ?>
                            <div class="clear"></div>
                        </div>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Tipo de Documento"); ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div> 

                                    <?php foreach ($a_documents_type as $sKey => $aDocumentType): ?>                                            
                                        <?php echo Helper::fnGetDrawJqueryButton($aDocumentType, 'document_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'force_tax', Rms_Constants::ARRAY_KEY_VALUE => $aDocumentType['tax']))) ?>                                            
                                    <?php endforeach ?>
                                </dd>
                            </dl>
                        </div>
                        <!--Si es manual el documento--> 
                        <?php if ($documentManual): ?>
                            <div class="divLine">
                                <dl>
                                    <dd>
                                        <label><?php echo __("Serie-Correlativo") ?>:</label>
                                    </dd>
                                    <dd class="buttonsContainer">
                                        <input class="serie ui-state-highlight"  id="serie" name="serie" value="" type="text" alt="number"/>
                                        <label><?php echo __("-") ?></label>
                                        <input class="correlative ui-state-highlight"  id="correlative" name="correlative" value="" type="text" alt="number"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div> 
                            </div> 
                        <?php endif ?>
                        <div class="divLeft divSearch">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Fecha de Venta"); ?>: </label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <input type="text" id="input_date_sale" name="input_date_sale" value="<?php echo date($date_format_php) ?>" />
                                </dd>
                            </dl>
                        </div>         
                        <div class="clear"></div> 

                    </li>
                </ul>
            </form>

            <ul>
                <li style="margin-top: 10px; ">
                    <hr class="hr_separate ui-widget-content"/>
                </li>
            </ul>
            
            <!--Ingreso de Productos-->
            <form class="orderForm" id="formSalesDetail" method="POST" action="#">
                <ul>
                    <li class="order">
                        <div class="divLine left">
                            <?php if ($bBarcodeReader): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label for="codigo" class="frmlblshort priceUnit"><?php echo __("Código") ?>:</label>
                                            <input id="codigo" name="codigo" class="subPriceLong priceUnit" type="text" />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>  
                                    <dl>
                                        <dd  style=" margin-top: 5px; ">
                                            <label for="descuento" class="frmlblshort priceUnit" ><?php echo __("Descuento") ?>:</label>
                                            <input id="descuento" class ="inputCustomPrice"name="descuento" type="text" value="0.0" />

                                        </dd>
                                    </dl>
                                </div>
                                <div class="divLine left">

                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" id="description" name="description" type="hidden"/>
                                        <input type="hidden" name="haveDetail" id="haveDetail"/>
                                        <input type="hidden" name="idproduct" id="idproduct"/>
                                        <input type="hidden" name="idstore" id="idstore"/>
                                        <input type="hidden" name="amount" id="amount"/>
                                    </dd>                               
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="order">
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <label for="cb_quantity" class="frmlblshort priceUnit"><?php echo __("Cantidad") ?>:</label>

                                        <input id="cb_quantity" name="cb_quantity" class="subPrice priceUnit" type="text" value="1"/>
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputCustomPrice ui-state-highlight" type="hidden" id="custom_price" name="custom_price" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <?php if ($bTaxEnabled): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>
                                            <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="text" disabled />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>

                            <div class="clear"></div>
                        </li>
                    
                        <?php else: ?>
                        <li class="order">
                            <div class="divLine left">
                                <div class="divLine divLineShort">
                                    <dl>
                                        <dd>
                                            <label for="cd_description" class="frmlbl"><?php echo __("Código") ?>:</label>                    
                                        </dd>
                                        <dd>                
                                            <div class="clear"></div>
                                            <input class="inputData" id="cd_description" name="cd_description" type="text"/>
                                        </dd>                               
                                    </dl>
                                    <dl>
                                        <dd>
                                            <label for="description" class="frmlbl"><?php echo __("Descripción") ?>:</label>                    
                                        </dd>
                                        <dd>                
                                            <div class="clear"></div>
                                            <input class="inputData" id="description" name="description" type="text"/>
                                            <input type="hidden" name="haveDetail" id="haveDetail"/>
                                            <input type="hidden" name="idproduct" id="idproduct"/>
                                            <input type="hidden" name="idstore" id="idstore"/>
                                            <input type="hidden" name="amount" id="amount"/>
                                        </dd>                               
                                    </dl>
                                    <div class="clear"></div>

                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft">
                                <dl>
                                    <dd>
                                        <label for="quantity" class="labelprice"><?php echo __("Cantidad") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputQuantity" type="text" id="quantity" name="quantity" alt="number" autocomplete="off" value ="1"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft">
                                <dl>
                                    <dd>
                                        <label for="custom_price" class="labelprice"><?php echo __("Precio") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputCustomPrice ui-state-highlight" type="text" id="custom_price" name="custom_price" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft addSales buttonadd">
                                <dl>
                                    <dd>
                                        <button type="button" id="addPackageDetail" class="ui-state-highlight"><?php echo __("Agregar") ?></button>
                                    </dd>
                                </dl>
                            </div>

                            <div class="clear"></div>
                        </li>
                       <input id="priceUnit" name="priceUnit" class="subPrice priceUnit" type="hidden" disabled/>
                       <input id="discountUnit" name="discountUnit" class="subPrice discountUnit ui-state-active" type="hidden" />
                       <input id="priceTotal" name="priceTotal" class="subPrice priceTotal ui-state-hover" type="hidden" disabled />
                       <?php if ($bTaxEnabled): ?>
                        <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="hidden" disabled />
                       <?php endif ?>
                       <li class="order">
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                       <!--<label class="frmlblshort priceUnit"><?php echo __("Precio Total") ?>:</label>-->

                                        <input id="priceUnit" name="priceUnit" class="subPrice priceUnit" type="hidden" disabled/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <?php if ($bTaxEnabled): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>
                                            <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="hidden" disabled />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <!--<label class="frmlblshort discountUnit"><?php echo __("Desc.") ?>:</label>-->

                                        <input id="discountUnit" name="discountUnit" class="subPrice discountUnit ui-state-active" type="hidden" />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <!--<label class="frmlblshort priceTotal"><?php echo __("SubTotal") ?>:</label>-->

                                        <input id="priceTotal" name="priceTotal" class="subPrice priceTotal ui-state-hover" type="hidden" disabled />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>                           

                            <div class="clear"></div>
                        </li>
                    <?php endif; ?>
                    <li>
                        <div class="clear"></div>
                        <?php if ($bBarcodeReader): ?>
                            <table id="bc_salesDetail_jqGrid"></table>
                        <?php else: ?>
                            <table id="salesDetail_jqGrid"></table>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="clear"></div>
                        </br>
                        <button id="saveSales" name="saveSales" type="button" class="ui-state-error"><?php echo __("Guardar") ?></button>
                        <button id="cancel" name="cancel" type="button" class="ui-state-error"><?php echo __("Cancelar") ?></button>
                        <div class="clear"></div>
                    </li>

                </ul>
            </form> 
        </div>
    </div>
</div>

<!--Dialog de Producto Detalle-->
<div id="input_detail" class="dialog">    
    <div id="grid_uno">
        <table id="grid_income_detail"> </table>
        <div id="grid_income_detail_pager"></div>
    </div>
    <div id="grid_dos">
        <table id="grid_income_detail2"> </table>
        <div id="grid_income_detail_pager2"></div>
    </div>
    <div class="clear"></div>
</div>
<!--Dialog de Lote-->
<div id="input_DetailOther" class="dialog">
    <form id="frm_sale_data">
        <div id="show_data">
        </div>
    </form>
</div>
<!-- Confirmacion de Pago -->
<div id="frmTypePay" class="dialog">
    <form id="postformConfirmPayment" method="post">
        <fieldset class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle">Tipo de Pago</legend>
            <div class="label_output">
                <label><?php echo __("Elija un Tipo de Pago: "); ?></label>
            </div>
            <?php if ($methodPayment) : ?>
                <div class="divLine" style="text-align: center;">
                    <dl>
                        <dd>
                            <label for="chars_Customers"><?php echo __("Tipos de Pago"); ?></label>
                        </dd>
                        <dd class="buttonsContainer">
                            <div class="clear"></div> 
                            <?php foreach ($salesPayment as $sKey => $aSalesPayment): ?>                                            
                                <?php echo Helper::fnGetDrawJqueryButton($aSalesPayment, 'payment_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'cardType', Rms_Constants::ARRAY_KEY_VALUE => __($aSalesPayment['display'])))) ?>                                            
                            <?php endforeach ?>
                        </dd>
                    </dl>
                </div>
            <?php endif ?>
            <div class="clear"></div>

            <!-- Pogo con Tarjeta -->
            <div id="frmCreditCard" class="ui-corner-all cFieldSet ui-widget-content payment">
                <!--<form id="frmPayment" method="post">-->
                <div>
                    <div>
                        <label><?php echo __("Tarjeta"); ?>:</label>
                        <select name="typeCard" id="typeCard">
                            <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                            <?php foreach ($creditCardPayment as $creditcard) {
                                ?>
                                <option value="<?php echo $creditcard['value']; ?>"><?php echo $creditcard['display']; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                    <div>
                        <label><?php echo __("Nro de Tarjeta"); ?>:</label>
                        <input id="cardNumber" name="cardNumber" value="" type="text" maxlength="150" size="30" alt="number"/>
                    </div>
                    <div>
                        <label><?php echo __("Nro de Operación"); ?>:</label>
                        <input id="operationNumber" name="operationNumber" value="" type="text" maxlength="150" size="30" alt="number"/>
                    </div>
                    <div>
                        <label><?php echo __("Monto"); ?>:</label>
                        <input id="amount_paymentCredit" name="amount_paymentCredit" value="" type="text" maxlength="150" size="30"/>
                    </div>
                </div>
            </div>
            <!-- Pago efectivo -->
            <div id="frmCreditCash" class="ui-corner-all cFieldSet ui-widget-content payment">
                <!--Monto Calculado-->
                <input id="post_confirm_payment_hidden_total_amount" name="post_confirm_payment_hidden_total_amount" type="hidden" />
                <div class="rowLine">
                    <div class="lPayment">
                        <?php echo __("Monto Total") ?>
                    </div>
                    <div>
                        <div class="lSymbol">
                            <div class="ui-widget post_display_prices ui-state-active">                
                                <?php echo "S/." ?>
                            </div>
                        </div>
                        <div class="post_confirm_payment_total_amount">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="rowLine">
                        <div class="lPayment">
                            <?php echo __("Pago") ?>
                        </div>
                        <div>
                            <div class="lSymbol">
                                <div class="ui-widget post_display_prices ui-state-active" >                
                                    <?php echo "S/." ?>
                                </div>
                            </div>
                            <input name="post_confirm_payment" id="post_confirm_payment" class="ui-corner-tr ui-corner-br" maxlength="10"/>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="rowLine">
                        <div class="lPayment">
                            <?php echo __("Vuelto") ?>
                        </div>
                        <div class="lSymbol">
                            <div class="ui-widget post_display_prices ui-state-active" >                
                                <?php echo "S/." ?>
                            </div>
                        </div>
                        <div class="post_confirm_payment_return_cash">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>   
                <div class="clear"></div>
                <div class="legend_prices">
                    <div class="post_container_prices">
                        <!--
                                        <div class="ui-widget post_display_prices ui-state-active" >                
                        <?php echo "S/." ?>
                                        </div>
                                        <div class="clear"></div>-->
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                <input name="post_idLuggage" id="post_idLuggage" type="hidden" value="0" />

            </div>   
            <!--Pago Mixto-->
            <div id="frmMixtoCard" class="ui-corner-all cFieldSet ui-widget-content payment">
                <div style="height: 224px">
                  <div>
                      <label><?php echo __("Tarjeta"); ?>:</label>
                      <select name="typeCardMixto" id="typeCardMixto">
                          <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                          <?php foreach ($creditCardPayment as $creditcard) {
                              ?>
                              <option value="<?php echo $creditcard['value']; ?>"><?php echo $creditcard['display']; ?></option>
                          <?php } ?>

                      </select>
                  </div>
                  <div>
                      <label><?php echo __("Nro de Tarjeta"); ?>:</label>
                      <input id="cardNumberMixto" name="cardNumberMixto" value="" type="text" maxlength="150" size="30" alt="number"/>
                  </div>
                  <div>
                      <label><?php echo __("Nro de Operación"); ?>:</label>
                      <input id="operationNumberMixto" name="operationNumberMixto" value="" type="text" maxlength="150" size="30" alt="number"/>
                  </div>
                  <div>
                      <label><?php echo __("Monto Credito"); ?>:</label>
                      <input id="amount_paymentCreditMixto" name="amount_paymentCreditMixto" value="" type="text" maxlength="150" size="30"/>
                  </div>
                  <div>
                      <label><?php echo __("Monto Efectivo"); ?>:</label>
                      <input id="amount_paymentCashMixto" name="amount_paymentCashMixto" value="" type="text" maxlength="150" size="30" alt="number"/>
                  </div>
                  <div>
                      <label><?php echo __("Vuelto"); ?>:</label>
                      <input id="amount_paymentreturn" name="amount_paymentreturn" style=" font-size: 18px; font-weight: bold;" readonly="true"/>
                  </div>
              </div>
          </div>          

        </fieldset>
    </form>
</div>


<!-- Registrar Nuevo Cliente !-->
<!--<div id="div_register_client" class="ui-widget ui-widget-content ui-corner-all none">
    <div class="arrow_left"></div>
    <div class="padding10">
        <form id="formRegisterClient" action="" method="post" class="frm">
            <h4 class="register_client_title ui-widget-header ui-corner-all">
                <?php echo __("Cliente") ?>
                <b class="ui_icon_close_register_client ui-state-highlight ui-corner-all" role="button">
                    <span class="ui-corner-all ui-icon ui-icon-close register_client_close"></span>
                    <span class="register_client_text"><?php echo __("Cerrar") ?></span>
                </b>
            </h4>
            <ul>            
                <li>
                    <div class="divLine">
                        <dl>
                            <dd><label><?php echo __("Tipo Cliente") ?>:</label></dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div>
                                <input id="id_customers" name="id_customers" type="hidden"/>
                                <?php foreach ($a_clients_type as $a_client): ?>
                                    <?php echo Helper::fnGetDrawJqueryButton($a_client, 'typeClient') ?>                                
                                <?php endforeach ?>                               
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_name" class="frmlbl"><?php echo __("Razón Social") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_name" name="company_name" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_code" class="frmlbl"><?php echo __("RUC") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_code" name="company_code" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_contact" class="frmlbl"><?php echo __("Contacto") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_contact" name="company_contact" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Nombre") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="passanger_name" name="passanger_name" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Apellido") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog required" id="passanger_lastname" name="passanger_lastname" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Sexo") ?>:</label>                    
                            </dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div>
                                <input type="radio" id="sexM" name="passanger_sexo" value="M"  checked  />
                                <label for="sexM"><?php echo __("M") ?></label>
                                <input type="radio" id="sexF" name="passanger_sexo" value="F" />
                                <label for="sexF"><?php echo __("F") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>

                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Tipo Documento") ?>:</label>                        
                            </dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div>
                                <input type='radio' 
                                       id='passanger_tdocumento' name='passanger_tdocumento' value="DNI" checked="true" >
                                <label for='passanger_tdocumento'>DNI</label>

                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Documento") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="passanger_documento" name="passanger_documento" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine div_buttons">
                        <button id="btn_save_client" type="button"><?php echo __('Listo') ?></button>                        
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            <ul class="div_passanger_dir">
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Dirección") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_dir1" name="passanger_dir1" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("E-mail") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_mail" name="passanger_mail" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                 Para determinar P.Geografica 
                <li class="containerPerson">
                    <div class="divLine"> 
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Departamento") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_dpto" id="passanger_dpto" >
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>
                                    <?php foreach ($a_dpto as $a_co): ?>
                                        <option value="<?php echo $a_co->iddpto; ?>"> <?php echo $a_co->descripcion; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li  class="containerPerson"> 
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Provincia") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_prov" id="passanger_prov">
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson" >
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Distrito") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_dst" id="passanger_dst">
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Teléfono") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_phone" name="passanger_phone" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Celular") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_cellphone" name="passanger_cellphone" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            <div class="clear"></div>
        </form>
    </div>
</div>-->