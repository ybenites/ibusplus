<style type="text/css"> 
    #Confirm{
        margin-left: 78px;
    }
    #cashbox_container {
        margin: 0 auto;
        width: 800px;
        height:400px;
        margin-top: 10px;    
    }
    #btnOption_cashBox{
        margin-left: 167px;
    }

    #titleCashbox{
        margin-top: 18px;   
    }

    .titleTables{    
        font-size: 19px;
        font-weight: bold;
        margin-bottom: 10px;
        margin-top: 25px;
        padding: 3px;
        text-align: center;
        width: 95%;
    }

    hr.hr_separate {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        background: none repeat scroll 0 0 transparent;
        border-color: #0da3cb -moz-use-text-color -moz-use-text-color;
        border-image: none;
        border-style: solid none none;
        border-width: 2px medium medium;
        margin-right: 119px;
        /*    margin: 3px;*/
        opacity: 0.55;
    }

    .left{float:left;}
    .right{float:right;}
    .marginTitle{
        margin:20px;
    }

    .userBody > div div.label_output {min-width: 228px;}
    div.label_output {width: 235px; margin: -10px auto;}
    .label_output div label{max-width: 150px;
                            min-width: 99px; width: 80px; white-space: nowrap;}
    div.data_separator{margin-top: 8px; margin-left: 8px;}
    .userBody{line-height: 1.5em;  }
    #infoAperBox{margin:15px;margin-top:112px;}
    h3, #bodyAperBox{width:420px;}
    #userInformation{width:500px;}
    .label_cbx {
        font-size: 15px;
        line-height:36px;}
    div.cMargin {margin: 8px;}
    .txt_center {text-align: center;}
    .txt_right {text-align: right;}
    #cashbox_status, #cashbox_code, #final_amount {
        font-size: 15px;
        line-height: 20px;
        margin-left: 8px;
        width: 180px;
    }
    .txt_right{text-align: right;}
    #openOtherCash>div> input[type="text"], input[type="password"], textarea, select{
        padding: 6px;
    }

</style>

<script type="text/javascript"> 

    var url_getCashBoxUser='/private/cashbox/getCashBoxUser'+jquery_params;
    var url_fnOpenCashBox ='/private/cashbox/fnOpenCashBox'+jquery_params;
    var url_fnOpenLastCashBox ='/private/cashbox/fnOpenLastCashBox'+jquery_params;
    var url_findpasswordConfirm ='/private/cashbox/findpasswordConfirm';

    var url_registerCashBox ='/private/cashbox/registerCashbox'+jquery_params;
    var cashboxTitleText = 'Abrir/Cerrar Caja';
    var cashboxOkText = '<?php echo __("Se realizo de manera satisfactoria el proceso de Caja.");?>';    
    var titleConfirm = '<?php echo __("Abrir/Cerrar Caja"); ?>';
    var txtConfirm = '<?php echo __("¿Desea realizar esta operacion?"); ?>';

    $(document).ready(function(){ 
      
        $('#btnSaveCashbox').button({
            icons:
                {secondary:'ui-icon-disk'}
        }).click(function(){
            OpenOrCloseCashbox();
        });
        
        $("#btnOptionCashbox").button({
            icons:{secondary:'ui-icon-newwin'}
        }).on('click',function(e){
            e.preventDefault();
            $("#dlgPasswordConfirm").dialog('open');                   
        });
        
        $('#btnAddAmount').button({
            icons:
                {secondary:'ui-icon-circle-plus'}
        });
    
        $('#btnAddAmount').click(function(){
            var amoun = $('#amount_initial').val();
            $('#cbx_status1').text(amoun);
        });
    
        $('#amount_initial').priceFormat({
        prefix: '',
        centsSeparator: '.',
        thousandsSeparator: ''
        });
        
        $("#dlgPasswordConfirm").dialog({
            title:'<?php echo __('Confirmacion Password'); ?>',           
            minWidth:400,
            minHeight:80,
            buttons:{
                '<?php echo __("Aceptar") ?>':function(){
                    $("#frmPasswordConfirm").submit();
                },
                '<?php echo __("Cancelar") ?>':function(){
                    $(this).dialog('close');
                }
            },
            close:function( event, ui ){                
                frmPasswordConfirm.currentForm.reset();                
            }
        });
        
        $("#dateCashBox").datepicker({
            dateFormat:'<?php echo $jquery_date_format; ?>',
            maxDate:'now',
            onSelect: function(dateText, inst) {
                $.ajax({
                    url: url_getCashBoxUser,
                    dataType:'jsonp',
                    data:{ 
                        dateText:dateText
                    },                    
                    success:function(r){
                        if(evalResponse(r)){                           
                            var template='';
                            $.each(r.data,function(i,v){
                                template=template+'<option value="'+v.idCashBox+'">'+v.cashBox+'</option>'                               
                            }); 
                            $("#selectCashBoxOpen").html(template);
                        }else{
                            msgBox(r.msg,r.code); 
                        }
                    }
                });                
            }
        }); 
        
        $("#selectCashBoxOpen").combobox({
            selected:function(event,ui){                
                confirmBox('<?php echo __("¿Desea Abrir Esta Caja?"); ?>','<?php echo __("Abrir Caja"); ?>',function(e){
                    if(e==true){                        
                        $.ajax({
                            url:url_fnOpenCashBox,
                            dataType:'jsonp',
                            data:{ 
                                idCashBox:ui.item.value
                            },                            
                            success:function(r){ 
                                if(evalResponse(r)){
                                    $("#openOtherCash").dialog('close');
                                    location.href='/private/sales/index';
                                }
                                else{
                                    msgBox(r.msg,r.code);
                                }
                            }  
                        });
                    }else{                        
                        $("#selectCashBoxOpen-combobox").val('');
                        return true;
                    }
                });
            }
        });
        
        $("#openOtherCash").dialog({
            title:'<?php echo __('Operaciones Caja Chica'); ?>',
            resizable: true,
            minWidth: 350,
            minHeight:80
        });
        
        $("#openLastCashBox").button({
            text: false,
            icons:{
                primary:'ui-icon-arrowreturnthick-1-w'
            }
        }).click(function(){            
            $.ajax({
                url: url_fnOpenLastCashBox,
                dataType:'jsonp',
                data:{
                },                            
                success:function(r){ 
                    if(evalResponse(r)){
                        $("#openOtherCash").dialog('close');
                        location.href='/private/sales/index';
                    }
                    else{
                        msgBox(r.msg,r.code);
                    }
                }  
            });
        });
        
        var clickSave = true;
        function OpenOrCloseCashbox(){
            var cashbox = new Object();
            if(($.trim($('#cbxid').val())!='')){
                cashbox.cbixd = $('#cbxid').val();
                cashbox.mount = $('#amount_initial').val();
            }else{
                cashbox.mount = $('#amount_initial').val();
            }                       
            var params = cashbox;
            confirmBox(txtConfirm,titleConfirm,function(r){                
                if(r){
                    if(clickSave){
                        clickSave = false;                        
                        loadingcarga=1;
                        $.ajax({
                            url:url_registerCashBox,
                            data:params,      
                            success:function(response){                                
                                if(evalResponse(response)){
                                    //console.log(response.data);return false;
                                    if(response.data != 'noturl'){                                        
                                        window.location="/"+response.data;
                                    }
                                    else{
                                        msgBox(cashboxOkText,cashboxTitleText, function(){
                                            location.reload();
                                        });
                                        return false;
                                    }
                                } else{
                                    msgBox(response.msg,response.code,function(){
                                    });         
                                }
                                clickSave = true;
                            },
                            error:function(){
                                clickSave = true;
                                msgBox(errorMessage);
                            }
                        });
                    }
                }
            }); 
        }
        
       var frmPasswordConfirm = $("#frmPasswordConfirm").validate({
            rules:{
                input_password_confirm:{
                    required:true                    
                }
            },
            messages:{
                input_password_confirm:{
                    required:"<?php echo __('Ingrese un Password') ?>"
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){                                      
                var obj_confirm_password={};
                obj_confirm_password.password_confirm=$("#input_password_confirm").val(); 
                
                $.ajax({
                    url:url_findpasswordConfirm,
                    dataType:'json',
                    data:obj_confirm_password,                            
                    success:function(r){ 
                        if(r.code==code_success){
                            if(r.data==true){
                                $("#dateCashBox").val('');
                                $("#openOtherCash").dialog('open');
                                $("#dlgPasswordConfirm").dialog('close');                            
                            }else{
                                msgBox("<?php echo __("Password Incorrecto") ?>","<?php echo __("Alerta Password") ?>",function(){$("#input_password_confirm").val('');})                                
                            }                            
                        }
                        else{
                            msgBox(r.msg,r.code);
                        }
                    }  
                });                                    
            }
        });
        
    
    });
    
    
    


</script>
<div id="cashbox_container" class="ui-corner-all ui-widget-content">
    <center>
        <div id="titleCashbox" class="titleTables">
            <?php
            if ($status['status'] == 'closed') {
                echo __("Abrir Caja");
            } else {
                $f = new DateTime($status['openDate']);
                echo $f->format("$date_format $time_format");
            }
            ?>
        </div>
    </center>    
    <div id="userInformation" class="left">
        <div  class="marginTitle" style="margin-top: 0px;">
            <h3><?php echo __("Informacion de Usuario"); ?> </h3>
            <hr class="hr_separate ">
        </div>
        <div class="userBody"> 
            <div>
                <div class="label_output left">
                    <div>
                        <label><?php echo __("Nombres"); ?>:</label>
                        <div id="cbx_fname">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['fName']) ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div>
                        <label><?php echo __("Apellidos"); ?>:</label>
                        <div id="cbx_lname">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['lName']) ?>
                            <?php endforeach ?>    
                        </div>
                    </div>
                    <div>
                        <label><?php echo __("Email"); ?>:</label>
                        <div id="cbx_email">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['email']) ?>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
                <div class="label_output left">
                    <div>
                        <label><?php echo __("Usuario"); ?>:</label>
                        <div id="cbx_uname">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['userName']) ?>
                            <?php endforeach ?>    
                        </div>
                    </div>
                    <div>
                        <label><?php echo __("Grupo"); ?>:</label>
                        <div id="cbx_ugroup">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['namegro']) ?>
                            <?php endforeach ?>    
                        </div>
                    </div>
                    <div>
                        <label><?php echo __("Oficina"); ?>:</label>
                        <div id="cbx_uoffice">
                            <?php foreach ($user as $la) : ?>
                                <?php echo __($la['nameof']) ?>
                            <?php endforeach ?>    
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="data_separator">
            <div id="infoAperBox">
                <h3><?php echo __("Informacion sobre la Apertura de Caja"); ?></h3>
                <hr class="hr_separate ">
            </div>
            <div id="bodyAperBox">    
                <p style="width:530px; font-size:1.5em; margin:20px; "><?php
                            if ($status['status'] == 'closed') {
                                echo __("Usted está cerrando Caja en la oficina de");
                            } else {
                                echo __("Usted está abriendo Caja en la oficina de");
                            }
                            ?>
                    <?php foreach ($user as $la) : ?>
                        <b><?php echo __($la['nameof']) ?></b>
                    <?php endforeach ?>    
                </p>
                <p style=" width:530px; margin:20px; font-size: 12px"><b><?php echo __('Ingreso en efectivo'); ?></b></p>
                <div style="margin-left:200px; ">
                    <label ><b><?php
                    if ($status['status'] == 'closed') {
                        echo __("Monto Inicial");
                    } else {
                        echo __('Monto Final');
                    }
                    ?></b></label>
                    <input style ="text-align:right; height: 15px;" type="text" name="amount_initial" id="amount_initial" autocomplete="off" value="0.00">
                </div>
                <div class="right" style="margin:4px;">
                    <button id="btnAddAmount">
<?php echo __("Agregar Monto"); ?></button> 
                </div>
            </div> 
        </div>
    </div>
    <!--    botones al costado-->
    <div id="buttonsCash" class="left" style="width: 280px; height: 300px;">
        <div class="left" style="width: 50px; margin-left:10px;">
            <label class="label_cbx"><b><?php echo __('Caja') ?></b></label>  
        </div>
        <div style="width: 180px;" id="cashbox_status" class="left ui-corner-all <?php if ($status['status'] == 'closed') echo 'ui-state-active'; else echo 'ui-state-highlight'; ?>">
            <div class="cMargin txt_center" id="cbx_status">
                <?php
                if ($status['status'] == 'closed') {
                    echo __('Cerrada');
                } else {
                    echo __('Abierta');
                }
                ?>
            </div>
        </div>
        <div style ="margin-left:10px; margin-top: 15px;" class="left label_cbx"><b><?php echo __("Código"); ?></b>
        </div>
        <div id="cashbox_code" class=" ui-corner-all ui-state-default" style="background-image: none; margin-top: 50px; margin-left: 70px;">
            <div class="cMargin txt_center" id="cbx_status">
                       <?php if ($status['status'] == 'opened') {
                           echo __($status['idCashBox']);
                       } else {
                           echo __('--');
                       }
                       ?>  
                <input id="cbxid" name="cbxid" type="hidden" value="<?php if ($status['status'] == 'opened') echo __($status['idCashBox']); ?>" />
            </div>
        </div>
        <div style="margin-left:10px; margin-top:50px; ">
            <label class="label_cbx" ><b><?php
                    if ($status['status'] == 'closed') {
                        echo __("Monto Inicial");
                    } else {
                        echo __('Monto Final');
                    }
                       ?></b></label>
        </div>
        <div id ="final_amount" class="ui-corner-all ui-state-active " style="margin-left: 70px;">
            <div class="cMargin txt_right" id="cbx_status1" >
<?php echo '0.00'; ?>
            </div>
        </div>
        <div>
            <div class="txt_right" style="margin-top:15px; margin-right: 28px; ">
                <button id="btnSaveCashbox"><?php echo __("Guardar"); ?></button>
            </div>
            
<?php if ($status['status'] == 'closed' && $typeUser == 1) { ?>
            <div id="btnOption_cashBox">
                    <button id="btnOptionCashbox"><?php echo __("Opciones"); ?></button> 
                </div>                    
<?php } ?> 
        </div>

    </div>           
</div>
<div id="dlgPasswordConfirm" class="none dialog">
    <form id="frmPasswordConfirm" >
        <div id="Confirm">
            <label><?php echo __("Contraseña"); ?>:</label><input type="password" id="input_password_confirm" name="input_password_confirm" />
        </div>
    </form>    
</div>

<div id="openOtherCash" class="none dialog" style="padding:5px;">
    <div style="padding:5px;">
        <label style="font-weight: bolder;width:100px;text-align:right;float: left;margin-right: 10px;"><?php echo __('Fecha de Caja Abrir'); ?> </label>
        <input type="text" id="dateCashBox" name="dateCashBox" size="25" readonly="" />
    </div>
    <div style="padding:5px;">
        <label style="font-weight: bolder;width:100px;text-align:right;float: left;margin-right: 10px;"><?php echo __('Seleccione Caja Abrir'); ?> </label>
        <select id="selectCashBoxOpen" name="selectCashBoxOpen">  
            <option value=""><?php echo __('Selecciona Una Opción'); ?></option>
        </select>
    </div>
    <div style="padding:5px;">
        <label style="font-weight: bolder;width:100px;text-align: right;float: left;margin-right: 10px;"><?php echo __('Abrir Ultima Caja'); ?> </label>
        <button id="openLastCashBox"><?php echo __("Abrir Ultima Caja Cerrada"); ?></button> 
    </div>
    <div class="clear"></div>
</div>
