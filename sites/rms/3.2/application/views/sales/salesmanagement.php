<style type="text/css">
    .sales_not td{
        background-color: #cd0a0a;
    }
    .div_new_product{
        margin-left: auto;
        margin-right: auto;
        width: 1000px;
    }
    .div_grid_product{        
        padding-top: 10px;
    }
    .ui-jqgrid tr.jqgrow td {
        white-space: normal;
    }

    .ui-jqgrid .ui-jqgrid-htable th div{
        white-space: normal;
        height: 30px;
        vertical-align: middle;
        text-align: center;
    }

    /*input.inputQuantity, input.inputCustomPrice{width: 50px;}*/
    /***** Seraching ***/
    .zone_rate_wrapper{width: 500px;margin: 0 auto;}
    .zone_wrapper{border-width: 3px;background-image: none;margin-bottom: 10px;position: relative;}
    .ui-widget .zone_alias{
        background-image: none;background-color: #A6C9E2;
        width: 50px;height: 50px;line-height: 50px;margin-left: 10px;text-align: center;color: white;
        text-shadow: rgba(0, 0, 0, 1) 0px 0px 5px;font-size: 18px;float: left;
    }
    .zone_name{float: left;margin-left: 10px;line-height: 50px;}
    /*    .column_left{width: 221px;float: left;}*/
    .column_right{width: 100%;float: left;background-color: #A6C9E2;background-image: none;border-bottom: none;border-top: none;border-right: none;}
    .content_items{text-align: center;}
    .ui-widget .save_zone_rate{background-image: none;}
    .content_sub_zones{margin: 0 auto;}

    .content_sub_zones li div.sub_item_zone{background-color: #FDFDFD;}
    .content_sub_zones li dl dd{margin: 5px;padding-left: 20px; width: 438px; float: left;}
    .content_sub_zones li dl dd label{float: left;font-weight: bolder;text-align: right;width: 130px;margin-right: 5px;}
    .content_sub_zones li dl dd input{width: 130px;text-align: left;}
    .content_sub_zones li:not(:last-child) div.sub_item_zone{border-bottom: 3px dashed #A6C9E2;}
    .content_sub_zones li:first-child div.sub_item_zone{
        -moz-border-radius-topleft: 16px; -webkit-border-top-left-radius: 16px; -khtml-border-top-left-radius: 16px; border-top-left-radius: 16px;
        -moz-border-radius-topright: 16px; -webkit-border-top-right-radius: 16px; -khtml-border-top-right-radius: 16px; border-top-right-radius: 16px;
        /*-moz-border-radius-bottomleft: 16px; -webkit-border-bottom-left-radius: 16px; -khtml-border-bottom-left-radius: 16px; border-bottom-left-radius: 16px;*/
    }
    .content_sub_zones li:last-child div.sub_item_zone{
        -moz-border-radius-bottomleft: 16px; -webkit-border-bottom-left-radius: 16px; -khtml-border-bottom-left-radius: 16px; border-bottom-left-radius: 16px;
        -moz-border-radius-bottomright: 16px; -webkit-border-bottom-right-radius: 16px; -khtml-border-bottom-right-radius: 16px; border-bottom-right-radius: 16px;     
    }
    span.alias_circle{
        float: left;
        display: block;
        width: 32px;height: 32px;
        line-height: 32px;
        text-align: center;
        background-image: none;
        -moz-border-radius: 16px; -webkit-border-radius: 16px; -khtml-border-radius: 16px; border-radius: 16px;        
    }
    .sub_item_top .alias_name{line-height: 32px;height: 32px;float: left;margin-left: 10px;font-size: 18px;}
    .width_field{
        padding: 10px;
        text-align: center;
        width: 210px;
        /*margin: 0 auto;*/
        margin-left: 125px;
    }
    .content_sub_zones li dl dd fieldset label {
        margin-top: 4px;
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        text-align: left;
        width: 140px;
    }
    .ui-jqgrid .ui-pg-selbox{
        height: 23px;
    }
    /*****/
    #rdoTypeSearch label {
        text-align: right;
        width: auto;
        padding: 2px;
    }
    .ui-autocomplete-input{
        height: 20px;
        width: 160px;
    }
    .content_sub_zones li dl dd input {
        text-align: left;
        width: 160px;
    }
    .orderForm {
        width: 200px; text-align: right; font-size-adjust: 20px;
        list-style: none;
    }
    .divLine.divLineShort{width:310px;}
    .divLeft{float: right;}
    
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        text-align: right;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 160px;
    }

    label.frmlblshort{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    #addPackageDetail{margin: 5px 0;}
    input.inputData{width: 270px;}
    input.subPrice{width: 200px; height: 19px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    input.subPriceLong{width: 200px; height: 19px; text-align: left;float: left;margin-top: 5px;margin-right: 5px;}
    .ui-widget input.subPrice {    float: left;
    font-size: 13px;
    margin-top: 5px;
    text-align: right;
    width: 35px;}
    input.inputCustomPrice{    float: left;
    height: 17px;
    margin-top: 8px;
    text-align: right;
    width: 35px;}
    
    
    /*Frm Editar Sales...*/
    .labelEditSales{
       margin-top: 4px;
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        text-align: left;
        width: 140px;
    }
    
    /* Boton Buscar */
    #idSearhSale {
        float: right; 
        padding: 5px;
        position:relative; 
        margin-top: -15px;
    }
    .label_output{
        margin-left: -57px;
    }
    /* Editar Detalle */
    .formEdit{
        float: left;
        height: 150px;
        width: 300px;
    }
    .formEdit >dd{
        margin-top: 2px;
    }
    .formEdit input{       
        margin-top: -18px;
        margin-left: 73px;      
    }
    .frmlbl{
        text-align: right;
        width: 66px;
    }
    #div_detail{
        display: none;
    }
    
</style>
<script type="text/javascript">
    /* URL */
    var url_listSales = '/private/salesmanagement/getListSalesOrder'+jquery_params;
    var url_subsalesOrder = '/private/salesmanagement/getDetalleSalesOrder'+jquery_params;
    var url_UpdateSalesOrderById='/private/salesmanagement/createCancel'+jquery_params; 
    var url_getSerieCorrelativeByTerm='/private/salesmanagement/getSerieCorrelativeByTerm'+jquery_params; 
    var url_createCreditNote='/private/salesmanagement/createCreditNote'+jquery_params; 
    
    var url_editSales='/private/salesmanagement/editSales'+jquery_params; 
    var url_GetProductByCodigo = '/private/sales/getProductByCode'+jquery_params;

    

    var url_getSalesDetailByID = '/private/salesmanagement/getSalesDetailByID'+jquery_params;
    var url_GetProduct = '/private/salesmanagement/getProduct'+jquery_params;
    var url_getDetailByProductId = '/private/salesmanagement/getDetailByProductId'+jquery_params;
    var url_getLotByProductId = '/private/salesmanagement/getProductLotById'+jquery_params;  
    var url_editSalesDetail = '/private/salesmanagement/editSalesDetail'+jquery_params;
    var url_deleteSalesDetail = '/private/salesmanagement/deleteSalesDetail'+jquery_params;
    
    /* Adquirir Venta */
    var url_getSalesById = '/private/salesmanagement/getSalesById'+jquery_params;
    
    var conftrash = '<?php echo __("¿Está seguro que desea anular esta Venta?"); ?>';
    var titletrash = '<?php echo __("Anular"); ?>';
    var creditNote_newTitle ='<?php echo __("Nueva Nota de Crédito") ?>';
    var idSales = 0;
    var url_deleteSalesDetail = '/private/salesmanagement/deleteSalesDetail'+jquery_params; 
    var deleteSubTitle = '<?php echo __("Eliminar Detalle de Venta") ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>';
    var txt_editSales = '<?php echo __("Editar Venta") ?>';
    var activeSeller = '<?php echo $activeSeller ?>';
    var url_GetClients = '/private/sales/getClients'+jquery_params;
    var flags = {};
    var iCountLot = 0;
    flags.isRange = 0;
    flags.haveLot = 0;
    var countarray = 0;
    var keycountarray= 0; 
    var quantity = 0;
    var edit_product = 0;
    var arrayDetails = new Object();
    var grid_list_ordersales;
    
    /* VARIABLES PHP */
    var bBarcodeReader = '<?php echo $bBarcodeReader ?>';
    /* VARIABLES */
    var dataLot;
    var quantity_one;
    
    $(document).ready(function(){
        
        /* ButtonSet */
        $( "#rdoTypeSearch" ).buttonset();
        /* PriceFormat */
        $('#priceTotal, #descuento, #igv, #totalPrice, #amountpayment, #amountreturn').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        $('#custom_price, #total_price, #discount').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* Combobox */
        $("#cmb_listUserCode").combobox();
        
        /* KeyUp */
        $('#discount, #custom_price').keyup(function(key){
            calculationPriceDetail();
        });
        
        $('#quantity').keyup(function(key){
            var precio = 0;
            if($('#haveDetail').val() == 0){
                var stock = (parseInt($('#amount').val()) + (parseInt(quantity_one))) - parseInt($('#quantity').val());
                if(stock<0){
                    msgBox("Cantidad es mayor al stock");
                    $(this).val('');
                }else
                    calculationPriceDetail();      
            }else
                calculationPriceDetail();
          
            var dif = parseInt($(this).val()) - parseInt(quantity_one);
            $("#quantity_lot").val(dif);
        });
        
        /* Funciones */
        calculationPriceDetail = function(){
            priceUnit = $('#custom_price').val().split(' ').join('');
            priceUnit = priceUnit==''?0:parseFloat(priceUnit);
           
            quantity = $('#quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            discount = $('#discount').val().split(' ').join('');
            discount = discount==''?0:parseFloat(discount);
            
            priceTotal = (priceUnit * quantity) - discount;
            
            $('#total_price').val(priceTotal.toFixed(2));
   
        };
        /* JQgrid de Ventas  */
       grid_list_ordersales= $("#grid_list_ordersales").jqGrid({
            url:url_listSales,            
            datatype: 'json',
            mtype: 'POST',
            colNames:[
                '<?php echo __("ID") ?>',
                '<?php echo __("Serie") ?>',
                '<?php echo __("Correlativo") ?>',                         
                '<?php echo __("Tipo de Pago") ?>',                         
                '<?php echo __("Cliente") ?>',
                '<?php echo __("Almacén") ?>',
                '<?php echo __("Documento") ?>',
                '<?php echo __("Estado") ?>',
                '<?php echo __("IGV") ?>',                
                '<?php echo __("Precio Total") ?>',
                '<?php echo __("Monto de Pago") ?>',
                '<?php echo __("Monto devuelto") ?>',
                '<?php echo __("Fecha") ?>',
                '<?php echo __("Cajero") ?>',
                'idUsercode',
                 '<?php echo __("Vendedor") ?>',
                '<?php echo __("Opciones") ?>'                
            ],
            colModel:[
                {name:'idSales',index:'idSales',hidden:true,key:true},
                {name:'serie',index:'serie',width:100,align:"center"},
                {name:'correlative',index:'correlative',width:140,align:"center"},
                {name:'methodPayment',index:'methodPayment',width:130,align:"center"},
                {name:'customers',index:'customers',width:300, align:"center"},
                {name:'storeName',index:'storeName',width:250, align:"center"},
                {name:'documentP', index:'documentP'},
                {name:'status',index:'status',width:150, align:"center"},
                {name:'tax',index:'tax',width:120, align:"right", hidden:true},
                {name:'totalPrice',index:'totalPrice',width:120, align:"right"},
                {name:'paymentAmount',index:'paymentAmount',width:120, align:"right"},
                {name:'returnCash',index:'returnCash',width:120, align:"right"},
                {name:'salesDate',index:'salesDate',width:150, align:"center"},
                {name:'seller',index:'seller',width:180, align:"center"},
                {name:'idUserCode',index:'idUserCode',hidden:true},
                {name:'UserCode',index:'UserCode',width:100, align:"center",hidden:<?php echo $activeSeller==0?'true':'false' ?>},
                {name:'actions',index:'actions',width:180, align:"center"}                
            ],
            rowList: [10,20,50,100,200,400],
            pager:'grid_list_ordersales_pager', 
            sortname: 'rs.salesDate',
            gridview: true,             
            rowNum:15,
            sortorder: "desc",
            width:1200,
            height:300,
            shrinkToFit:true,
            rownumbers: true, 
            rownumWidth: 30,
            viewrecords: true, 
            subGrid:true,
            subGridOptions: { 
                plusicon : "ui-icon-plus", 
                minusicon : "ui-icon-minus", 
                openicon : "ui-icon-carat-1-sw"
            }
            ,gridComplete:function(){
                $.each($(this).jqGrid('getDataIDs'), function(i,d){
                    var renDatos = $("#grid_list_ordersales").getRowData(this);
                    var options='';
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    print = "<a class=\"print\" style=\"cursor: pointer;\" doc=\""+renDatos.documentP+"\" rel=\""+this+"\" title=\"<?php echo __('Reimprimir'); ?>\" ><?php echo __('Reimprimir'); ?></a>";
                    cancel = "<a class=\"cancel\" style=\"cursor: pointer;\" rel=\""+this+"\" title=\"<?php echo __('Cancelar'); ?>\" ><?php echo __('Anular'); ?></a>";
                    switch(renDatos.status){
                        case 'Anulado':
                              $('#grid_list_ordersales tr#'+this).addClass('ui-state-error');   
                              break;              
                        default:
                            options = edit+print+cancel;
                            break; 
                    }
                    $("#grid_list_ordersales").jqGrid('setRowData',this,{actions:options});
                }); 
                /* Reimprimir Venta */
                $('.print').button({
                    icons:{primary: 'ui-icon-print'},
                    text: false
                })
                .click(function(){
                    var idl=$(this).attr('rel');
                    var doc = $(this).attr('doc');
                     confirmBox  ('<?php echo __("Desea reimprimir documentos?"); ?>',
                     'Confirmar Impresión',function(e){
                         if(e){
                             var my_pop_up = window.open('/private/sales/getPrintSales?sales_id='+idl+'&document='+doc,
                             '<?php echo __("Impresión de Documento de Envío") ?>');
                         }else{                                                
                         }
                        window.location.reload();
                    });                              
                });

                /* Editar */
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                .click(function(){  
                    $(".frmEditSales").text(txt_editSales);
                    var id=$(this).attr('rel');
                    $.ajax({
                        url:url_getSalesById,
                        type:'POST',
                        dataType:'jsonp',
                        data:{id:id},
                        beforeSend:function(){
                            $("#loading").dialog("open");
                        },
                        complete:function(){
                            $("#loading").dialog("close");
                        },
                        success:function(response){
                            if(evalResponse(response)){
                               var r=response.data;                 
                                $("#idSales").val(r.idSales);
                                $("#idCustomers").val(r.idCustomers);
                                $("#chars_Customers").val(r.nameCustomers);
                                $("#dateSales").val(r.date);
                                $("#cmb_listUserCode").val(r.idUsercode);
                                $("#cmb_listUserCode-combobox").val($("#cmb_listUserCode option:selected").text());
                                $('#editSales').dialog('open');     
                            }
                        }
                    });
                });
                /* Eliminar */
                $('.cancel').button({
                    icons:{primary: 'ui-icon-cancel'},
                    text: false
                })
                .click(function(){
                    var idl=$(this).attr('rel');
                    confirmBox(conftrash,titletrash,function(event){
                        if(event){                 
                            $.ajax({
                               url:url_UpdateSalesOrderById,                        
                               type:'POST',
                               dataType:'json',
                               data:{idl:idl, idCancel:'0'},
                               beforeSend:function(){
                                   $("#loading").dialog("open");
                               },
                               complete:function(){
                                   $("#loading").dialog("close");
                               },
                               success:function(response){
                                   if(evalResponse(response)){
                                       grid_list_ordersales.trigger("reloadGrid"); 
                                       idSale = response.idSales;
                                       dataSale = response.sales;
                                       $('#creditnote_idSale').val(idSale);
                                       $('#creditnote_amountSale').val(dataSale.totalPrice);
                                       $('#creditnote_documento').val(dataSale.document);
                                       $('#creditnote_serie').val(dataSale.serie);
                                       $('#creditnote_correlative').val(dataSale.correlative);
                                       $('#confirm_payment').dialog('open');
                                   }else{
                                       msgBox(response.msg,response.code);
                                   }
                               }                       
                        });
                        }
                    });
                });
            }
            ,subGridRowExpanded: function(subgrid_id, row_id) {                  
                var rd = $("#grid_list_ordersales").getRowData(row_id);                  
                var subgrid_ordersales, subgrid_ordersales_pager;
                subgrid_ordersales = subgrid_id+"_t";                
                subgrid_ordersales_pager = "p_"+subgrid_ordersales; 
                $("#"+subgrid_id).html("<table id='"+subgrid_ordersales+"' class='scroll'></table><div id='"+subgrid_ordersales_pager+"' class='scroll'></div>");                                
                $("#"+subgrid_ordersales).jqGrid({ 
                    url:url_subsalesOrder, 
                    datatype: "jsonp",
                    mtype: 'POST',
                    postData:{
                        idSal:function(){
                            return row_id;
                        }
                    },                 
                    colNames: [
                        'idSalesOrderDetail',         
                        '<?php echo __("Cod. Producto") ?>',
                        '<?php echo __("Nombre Producto") ?>',
                        '<?php echo __("Lote") ?>',
                        '<?php echo __("Almacen") ?>',
                        '<?php echo __("Cantidad") ?>',
                        '<?php echo __("Precio Unit.") ?>',
                        '<?php echo __("Descuento") ?>',
                        '<?php echo __("Precio Total") ?>',
                        'status',
                        '<?php echo __("Acciones") ?>'
                    ],                       
                    colModel: [ 
                        {name:"idSalesOrderDetail",index:"idSalesOrderDetail",hidden:true,width:80},
                        {name:"aliasProduct",index:"aliasProduct",width:80,align:"center"},
                        {name:"productName",index:"productName",width:150,align:"center"},
                        {name:"lote",index:"lote",width:120,align:"center"},
                        {name:"storeName",index:"storeName",width:120,align:"center"},
                        {name:"quantity",index:"quantity",width:80,align:"center"},
                        {name:"unitPrice",index:"unitPrice",width:80,align:"right"},
                        {name:"discount",index:"discount",width:80,align:"right"},
                        {name:"totalPrice",index:"totalPrice",width:80,align:"right"},                       
                        {name:"status",index:"status", hidden:true},
                        {name:"actions",index:"actions",width:100,align:"right"}
                    ],
                    rowNum:20, 
                    pager: subgrid_ordersales_pager, 
                    sortname: 'rs.idSales',
                    sortorder: "desc",
                    height: '100%',
                    rownumbers: true,
                    afterInsertRow: function(rowid, aData){
                       var optionSub = '';             
                       var editSub = "<a class=\"editSub\" style=\"cursor: pointer;\" rel=\""+rowid+"\" title=\"<?php echo __('Editar'); ?>\"><?php echo __('Editar'); ?></a>";                        
                       var trashSub = "<a class=\"trashSub\" style=\"cursor: pointer;\" rel=\""+rowid+"\" title=\"<?php echo __('Eliminar'); ?>\"><?php echo __('Eliminar'); ?></a>";                        
                       if(aData.status== '<?php echo Rms_Constants::SALES_STATUS_SOLD ?>'){     
                            optionSub = editSub+trashSub;
                       }
                       $(this).jqGrid('setRowData',rowid,{actions: optionSub});
                    }
                    ,gridComplete:function(){
                        /* Editar Detalle de Venta */
                        $('.editSub').button({
                            icons:{primary: 'ui-icon-pencil'},
                            text:false
                        })
                        .click(function(){
                            var id= $(this).attr("rel");
                            $.ajax({
                                url:url_getSalesDetailByID,
                                type:'POST',
                                dataType:'jsonp',
                                data:{id:id},
                                success:function(response){
                                    if(evalResponse(response)){
                                       var r=response.data;
                                       $("#idDetail").val(r.idDetail);
                                       $("#idStoreDetail").val(r.idStore);
                                       $("#idProduct").val(r.idProduct);
                                       $("#idSalesDetail").val(r.idSalesDetail);
                                        $("#description").val(r.product);
                                        $('#haveDetail').val(r.haveDetail);
                                        $('#idstore').val(r.idStore);
                                        $('#idproduct').val(r.idProduct);
                                        $('#amount').val(r.amount);
                                       if(r.haveDetail == 1)
                                          $("#quantity").attr('disabled','true');
                                       else
                                          quantity_one = r.quantity;
                                      
                                       $("#discount").val(r.discount);
                                       $("#quantity").val(r.quantity);
                                       $("#custom_price").val(r.unitPrice);
                                       $("#total_price").val(r.totalPrice);
                                       $('#dlg_editDetailSales').dialog('open');

                                    }
                                }
                            });
                        });
                        /* Eliminar Detalle de Venta */
                        $('.trashSub').button({
                            icons:{primary: 'ui-icon-trash'},
                            text:false
                        })
                        .click(function(){
                            var idsd=$(this).attr('rel');
                            confirmBox(conftrash,titletrash,function(event){
                                if(event){                 
                                    $.ajax({
                                       url:url_deleteSalesDetail,                        
                                       type:'POST',
                                       dataType:'json',
                                       data:{idsd:idsd},
                                       beforeSend:function(){
                                           $("#loading").dialog("open");
                                       },
                                       complete:function(){
                                           $("#loading").dialog("close");
                                       },
                                       success:function(response){
                                           if(evalResponse(response)){
                                               grid_list_ordersales.trigger("reloadGrid"); 
                                           }else{
                                               msgBox(response.msg,response.code);
                                           }
                                       }                       
                                  });
                                 }   
                            });
                      });
                    }
                });
                $("#"+subgrid_ordersales).jqGrid('navGrid',"#"+subgrid_ordersales_pager,{edit:false,add:false,del:false,search:false,refresh:false});
                //                $("#"+subgrid_ordersales).jqGrid('navButtonAdd','#'+subgrid_ordersales_pager,
                //                { caption: '<?php echo __("Seleccione Columnas"); ?>', title: '<?php echo __("Seleccione las columnas que desea ver"); ?>', onClickButton : function (){ jQuery("#"+subgrid_package).jqGrid('columnChooser'); } });
            }               
        });   
       $("#grid_list_ordersales").jqGrid('navGrid','#grid_list_ordersales_pager',{add:false,edit:false,del:false,search:false,refresh:false});
       
       /* Dialog Editar Venta */
        $("#editSales").dialog({
         title:'<?php echo "Editar Venta"; ?>',
         autoOpen: false,
         width:450,
         height:'auto',
         show: "slide",
         hide: "slide",
         modal:true, 
         resizable: false, 
         position:["center", 70],
         buttons:{
             '<?php echo "Aceptar" ?>':function(){             
                 if(!$("#frmEditSales").valid())
                     return false;
                 $("#frmEditSales").submit();
             },
             '<?php echo "Cancelar" ?>':function(){
                         $(this).dialog('close');
             }
         }
       });                 
       /* Validate Editar Venta */
       var frmEditSales=$("#frmEditSales").validate({
            rules:{
                s_num_document:{
                    required:true
                },         
                dateSales :{
                    required:true
                },
                cmb_listUserCode:{
                    required:true
                }
                
            },
            messages:{
                chars_Customers:{
                    required:'<?php echo __("Ingrese un Cliente") ?>'
                },                
                dateSales :{
                    required:'<?php echo __("Ingrese una Fecha") ?>'
                },
                cmb_listUserCode:{
                    required:'<?php echo __("Seleccione un cliente") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                $.ajax({
                    data:$("#frmEditSales").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:url_editSales,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Guardado"); ?>');
                            grid_list_ordersales.clearGridData();
                            grid_list_ordersales.trigger("reloadGrid");
                            $("div#editSales").dialog('close');
                            frmEditSales.currentForm.reset();
                        }
                    }
                });
                
            }
        });   
       /* Dialog Confirmacion de Nota de Credito */
       $('#confirm_payment').dialog({
            title: '<?php echo __("DEVOLUCION") ?>',
            autoOpen: false,
            resizable: false,
            dragable: false,
            height:'auto',
            width:350,
            modal: true,
            hide: 'blind',
            show: 'blind',
            buttons: {                
                "<?php echo __("Sí") ?>": function() {
                    $('#confirm_payment').dialog('close');
                    $(".frmSubtitle").text(creditNote_newTitle);
                    $("div#frmCreditNoteDiv").dialog('open');
                },
                "<?php echo __("No") ?>": function() {
                    $(this).dialog('close');
                }
            }
        }); 
       /* Dialog de Nota de Credito */
       $("div#frmCreditNoteDiv").dialog({
            width:350,
            title:'<?php echo __("Nota de Crédito"); ?>',
            autoOpen: false,
            resizable: false,
            modal: true,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frmCreditNote").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $("#idProduct").val(0);
                    frm_credit_note.currentForm.reset();
                    $(this).dialog('close');                      
                }
            }
        });
       /* Validacion y Creacion de Nota de Credito */
       var frm_credit_note=$("#frmCreditNote").validate({
            rules:{
                creditnote_date:{
                    required:true
                },               
                creditnote_description:{
                    required:true
                }                               
        
            },
            messages:{
                creditnote_date:{
                    required:'<?php echo __("Ingreso de Fecha Obligatoria.") ?>'
                },               
                creditnote_description:{
                    required:'<?php echo __("Se requiere una descripción obligatoria.") ?>'
                }              
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){  
                $.ajax({
                    data:$("#frmCreditNote").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:url_createCreditNote,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Guardada"); ?>');
                            grid_list_ordersales.trigger("reloadGrid"); 
                            $("div#frmCreditNoteDiv").dialog('close');
                            frm_credit_note.currentForm.reset();
                        }
                    }
                });          
            }
        });
      
       /* Editar Detalle de Venta */   
        $("#dlg_editDetailSales").dialog({
         title:'<?php echo "Editar Detalle de Venta"; ?>',
         autoOpen: false,
         width:410,
         height:'auto',
         show: "slide",
         hide: "slide",
         modal:true, 
         buttons:{
             '<?php echo "Aceptar" ?>':function(){    
                 $("#formEditSalesDetail").submit();
             },
             '<?php echo "Cancelar" ?>':function(){
                 $(this).dialog('close');
             }
         } 

        });
        /* Validacion de Editar Detalle de Venta */
        formSalesDetail = $("#formEditSalesDetail").validate({
            rules: {                
                quantity:{
                    required: true     
                }
                ,discount:{
                    required: true
                }
                ,custom_price:{
                    required: true
                }
            },
            messages:{                
                discount:{ required: '<?php echo __("El descuento es requerido.") ?>'},
                quantity:{ required: '<?php echo __("La cantidad es requerida.") ?>'},
                custom_price:{ required: '<?php echo __("El precio es requerido.") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){  
                 $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_editSalesDetail,
                    data:   $.extend($(form).serializeObject(),
                            $(form).serializeDisabled()),  
                    success:function(res){
                        if(evalResponse(res)){                            
                            formSalesDetail.currentForm.reset();
                            $("#dlg_editDetailSales").dialog('close');
                            grid_list_ordersales.trigger("reloadGrid");
                            
                        }
                    }
                });
            }
        });
        
        if(bBarcodeReader == 1){
         /********* Activacion de Codigo de Barras ******/ 
        }
        else{
         /********* Desactivacion de Codigo de Barras ***********/ 
         /* Buscar Producto */   
            $("#description").autocomplete({
            minLength: 1,
            autoFocus: true,
            source:function(req,response){
                $.ajax({
                    url:url_GetProduct,
                    dataType:'jsonp',
                    data: {
                        term: req.term,
                        idStore: $("#idStoreDetail").val()
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {

                                var text = "<span class='spanSearchLeft'>" + item.product + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.product,
                                    id : item.idProduct,
                                    price : item.productPriceSell,
                                    idstore : item.idStore,
                                    amount:item.Amount,
                                    haveDetail:item.haveDetail
                                };
                            }));
                        }else{
                            msgBox(resp.msg,resp.code);
                        }
                    }
                });
            },
            focus: function(event, ui) {
                $('#custom_price').val(ui.item.price);
            },
            select: function( event, ui ) {
                $("#haveDetail").val(ui.item.haveDetail);
                $('#idproduct').val(ui.item.id);
                $('#idstore').val(ui.item.idstore);
                $('#amount').val(ui.item.amount);
                $("#grid_income_detail").clearGridData();
                if(ui.item.haveDetail==1){                   
                    getAllDetailProduct($('#idproduct').val(), $('#idstore').val());
                    $('#div_detail').show();
                    $('#quantity').val('1');
                }else{
                    getAllLotProduct($('#idproduct').val(), $('#idstore').val());
                    $('#div_lot').show();
                }
            } 
            })
            .data( "autocomplete" )._renderItem = function( ul, item ) {            
                return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + item.label + "</a>" )
                .appendTo( ul );
            };
         /* Adquirir Producto Detalle */
            function getAllDetailProduct(idPG, idStore){
                showLoading=1;
                $.ajax({
                    url:url_getDetailByProductId,                        
                    type:'POST',
                    dataType:'jsonp',
                    data:{id:idPG,
                          idStore:idStore}, 
                    async:false,
                    success:function(response){
                        if(evalResponse(response)){
                            var r=response.data;
                            if(r.length>0){                            
                                $.each(r,function(index,data){                                
                                    var pd={};  
                                    nameProduct = data.productName;
                                    pd.productName=data.productName;
                                    pd.idGeneral=data.idProductDetail;
                                    pd.description=data.description;
                                    pd.serie_electronica=data.serie_electronic;                            
                                    $("#grid_income_detail").jqGrid('addRowData',data.idProductDetail,pd); 
                                });    
                            }else{
                                $('#div_detail').html('No hay detalles para este producto');
                            }
                        }
                    }
                });
            }  
         /* JQGRID de Detalle de Producto */
            var grid_income_detail=$("#grid_income_detail").jqGrid({
               datatype: "local",
               mtype: 'POST',            
               colNames:[
                   '<?php echo __("Codigo General") ?>',
                   '<?php echo __("Producto") ?>',
                   '<?php echo __("Serie Electrónica") ?>',                                                        
                   '<?php echo __("Descripción") ?>'
               ],
               colModel:[
                   {name:'idGeneral',index:'idGeneral',hidden:true, key:true},
                   {name:'productName',index:'productName',search: false },
                   {name:'serie_electronica',index:'serie_electronica',align:'center'},       
                   {name:'description',index:'description',align:'center', search:false},
               ],
               pager:'#grid_income_detail_pager',              
               rowNum:10, 
               sortorder: "asc",            
               width:400,
               height:'auto',                        
               rownumbers:true,
               caption:'<?php echo __("Lista de Productos") ?>',
               ondblClickRow:function(rowid,iRow,iCol,e){                       
                  // Informacion del Producto Seleccionado
                  var datos = $("#grid_income_detail").getRowData(rowid);
                  $('#idProductDetail').val(rowid);
                  $('#info_detail').html('Producto:'+ datos.productName + 'Serie Electronica:'+ datos.serie_electronica
                                    +' Color: ' + datos.description);
               }
              
           });
            $("#grid_income_detail").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        /* Adquirir Producto Lote */
            function getAllLotProduct(idPG, idStore){
                showLoading=1;
                $.ajax({
                    url:url_getLotByProductId,                        
                    type:'POST',
                    dataType:'json',
                    data:{id:idPG,
                          idStore:idStore
                    },
                    async:false,
                    success:function(response){
                        if(evalResponse(response)){
                            var r=response.data;                        
                            var template='';                                                        
                            if(r.length>0){
                                $.each(r,function(i,d){ 
                                    var lot="Lote: "+d.lotName+" Stock: "+d.amount+" Cantidad :\n\
                                    <input type='text' data-id='"+d.idLot+"' data-amt='"+d.amount+"' size='5' class='txt_amount_ds' />";
                                    template=template + lot;                                                                   
                                });                            
                                template=template+"<div style='clear:both'></div>";
                                $("#div_lot").html(template);
                                $(".txt_amount_ds").setMask();
                            }else{
                                $("#div_lot").html('No hay lotes para este producto');
                            }
                        }
                    }
                });
            }     
        
        
        }


       //Click de Codigo de Barras
        $("input#codigo").keypress(function(event) {
            if ( event.which == 13 ) {
                event.preventDefault();
                code = $.trim($(this).val());
                getProductByCode(code.substring(0,7));               
            } 
        });
        function returnarray(idproduc,idstore){
            console.log(idproduc+"  - "+idstore);
            $.each(arrayDetails, function(key, value) {

                if($('#idproduct').val() == value.product_id && $('#idstore').val()==value.store_id){
                    countarray = 1;
                    keycountarray = key;
                    quantity = value.quantity;
                }
            });
        }
        //calculo de precio detalle 
        $('#descuento').keyup(function(key){
            calculationPriceDetail();
        });
        $('#cb_quantity').keyup(function(key){
            var precio = 0;            
            returnarray($('#idproduct').val(),$('#idstore').val());            
            if ((countarray == 0 && keycountarray==0) || edit_product == 1){
                stock = $('#amount').val() - $('#quantity').val();
            }else{
                stock = $('#amount').val() - (parseInt(quantity) + parseInt($('#quantity').val())) ;
            }
            countarray = 0;
            keycountarray = 0;
//            if($('#idproduct').val()==""){
//                msgBox("Seleccione producto");
//                $(this).val('');
//            }else 
                
            if(stock<0){
                
                msgBox("Cantidad es mayor al stock");
                $(this).val('');
            }else if($(this).val()==0){
            }else{
                calculationPriceDetail();
                $('#cb_precioTotal').val(precio.toFixed(2));
            }            
            stock = 0;
        });
          

        /****************** 
         * Function
         *getProductByCode *
         *
         ********************/
        function getProductByCode(code){
            
            showLoading=1;
            $.ajax({
                url:url_GetProductByCodigo,                        
                type:'POST',
                dataType:'jsonp',
                data:{
                    code:function(){
                        return code
                    }
                }, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        var r=response.data;
                        if(r.length>0){                            
                            $.each(r,function(index,data){                                                                                            
                                    /*** Proceso de Verifcar Lotes, Almacenes y Stock ***/                
                                    var id=data.idProduct; 
                                    $.ajax({
                                        url:url_getLotByProductId,                        
                                        type:'POST',
                                        dataType:'json',
                                        data:{idpg:id}, 
                                        async:false,
                                        success:function(response){
                                            if(evalResponse(response)){
                                                var r=response.data;  
                                                if(r=='not_store'){ 
                                                    msgBox('<?php echo __("Su usuario de Venta no tiene asignado un almacén. <br/>Debe hacerlo para poder vender.") ?>','<?php echo __("Aviso") ?>');
                                                    return false;
                                                    flags.haveLot = 0;                                                 
                                                }else{
                                                    if(r.length>0){
                                                        flags.haveLot = 1;
                                                        var j = 0;
                                                        $.each(r,function(i,d){ 
                                                            $("#codigo").val(d.productName);
                                                            $("#descuento").val('0.0');
                                                            $("#cb_quantity").val('1');
                                                            $("#cb_precio").val(d.unitPrice);
                                                            $("#cb_preciototal").val(d.unitPrice);
                                                            
                                                            
                                                            // EL producto nuevo 
                                                            $('#haveDetail').val(d.haveDetail);
                                                            $('#idstore').val(d.idStore);
                                                            $('#idproduct').val(d.idProduct);
                                                            $('#amount').val(d.amount);

//                                                            var itemDetail = new Object();
//                                                            itemDetail.id = d.idLot;
//                                                            itemDetail.productName = d.productName;
//                                                            itemDetail.amount = d.amount;
//                                                            itemDetail.creationDate = d.creationDate;                                                    
//                                                    
//                                                            arrayDetails_lots[j++] = itemDetail;                                                  
                                                        });                            
                                            
                                                    }else{
                                                        msgBox('<?php echo __("No hay Productos en Lotes") ?>','<?php echo __("Alerta") ?>');
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    /** Fin **/
                                    
                                    
                       });
                     }    
                    }
                    $('input#codigo').focus();
                    }
             });
       }
    
        
        /************************ 
         *Fin getProductByCode 
         *
         ************************/

                                 
       
        
        $('#idSearhSale').button({
            icons:{
                primary:'ui-icon-search'
            }
        }).click(function(){
            nrodocument = $('#s_num_document').val();
            store = $('#idStore').val();
            //tb = $('#listSearchType').val();
            tb = $('input[name=rdoSearch]:checked').val();
            d = $("#accountsreceivable_date").val();
            sd = $("#accountsreceivable_bdaterango").val();
            ed = $("#accountsreceivable_fdaterango").val();
            a = $("#anio").val();
            m = $("#accountsreceivable_listMonth").val();  

            if(tb=="0"){
                msgBox('<?php echo __("Seleccione Tipo de Búsqueda"); ?>');     
            }else{
                
                delete grid_list_ordersales.getGridParam('postData')['d'];          
                delete grid_list_ordersales.getGridParam('postData')['m'];          
                delete grid_list_ordersales.getGridParam('postData')['a'];          
                delete grid_list_ordersales.getGridParam('postData')['sd'];
                delete grid_list_ordersales.getGridParam('postData')['ed'];
               
                if(tb=="tbd"){
                    grid_list_ordersales.setGridParam({
                        postData: {
                            store:function(){
                                return store;
                            },
                            d:function(){
                                return d;
                            },                           
                            nrodocument:function(){
                                return nrodocument;
                            }
                        }
                    }).trigger("reloadGrid");
                }
             
                if(tb=="tbr"){
                    grid_list_ordersales.setGridParam({
                        postData: {
                            store:function(){
                                return store;
                            },
                            sd:function(){
                                return sd;
                            },
                            ed: function(){
                                return ed;
                            },                            
                            nrodocument:function(){
                                return nrodocument;
                            }                        
                        }
                    }).trigger("reloadGrid");
                }
                if(tb=="tbm"){
                    if ($("#accountsreceivable_listMonth").val()  == 0 || $('#anio').val() == 0){
                        alert ('<?php echo __("Seleccione Mes y Año"); ?>');
                    }else{
                        grid_list_ordersales.setGridParam({
                            postData: {
                                store:function(){
                                    return store;
                                },
                                m:function(){
                                    return m;
                                },
                                a: function(){
                                    return a;
                                },                                
                                nrodocument:function(){
                                    return nrodocument;
                                }  
                            }
                        }).trigger("reloadGrid");
                    }
                }
                
                $("#grid_list_ordersales").trigger('reloadGrid');
            }
        });
        
        $("#idStore").combobox({
            selected:function(ui,event){
                //$("#idStore").trigger('change');
            }
        });
        
        /*--datepicker--*/
        $( "#accountsreceivable_date,#accountsreceivable_bdaterango, #accountsreceivable_fdaterango" ).datepicker(
        {
            dateFormat: '<?php echo $jquery_date_format; ?>'       
        });
        $('#accountsreceivable_listMonth,#anio').combobox();
        fnLoadCheck();
        //fecha
            
        $('#rdoTypeSearch').change(function(){
            tipob=$('input[name=rdoSearch]:checked').val();
            if(tipob=='tbd'){
                $('.tbd_group').show();
                $('.tbm_group').hide();
                $('.tbr_group').hide();
            }
            if(tipob=='tbm'){
                $('.tbm_group').show();
                $('.tbd_group').hide();
                $('.tbr_group').hide();
            }
            if(tipob=='tbr'){
                $('.tbr_group').show();
                $('.tbd_group').hide();
                $('.tbm_group').hide();
            }
            if(tipob=='0'){   
                $('.tbr_group').hide();
                $('.tbd_group').hide();
                $('.tbm_group').hide();
            }
        });
        //fin fecha
        
     $('input[name=rdoSearch]:checked').val($('input[name=rdoSearch]:checked').val()).trigger('change');
        
       
        /*AUTOCOMPLETE-CUSTOMERS*/
        $( "#chars_Customers" ).autocomplete({
            minLength: 0,
            source:function(req,response){
                $.ajax({
                    url:url_GetClients,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                if(item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                                    item_icon = 'ui-icon-person';
                                }
                                if(item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                                    item_icon = 'ui-icon-home';
                                }
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.customers + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.customers,
                                    id : item.idCustomers,
                                    type : item.type
                                }
                            }));
                        }
                    }
                });
            },
            focus: function(event, ui) {
                if(ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                    $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
                }
                if(ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                    $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
                }
            },
            select: function( event, ui ) {
                
                $('#idCustomers').val(ui.item.id);
                
                $(this).data('autocomplete').text_value = ui.item.value;

                if(ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>'){
                    $('.icon_sender_type').removeClass('ui-icon-home ui-icon-help').addClass('ui-icon-person');

                }
                if(ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>'){
                    $('.icon_sender_type').removeClass('ui-icon-person ui-icon-help').addClass('ui-icon-home');
                }   
            },
            change: function( event, ui ) {   
                if ( !ui.item ) {
                   
                    $('#idCustomers').val('');
//                    $(this).helptext('refresh');
                    
                    $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                    return false;
                }
                //$(this).helptext('refresh');
                return true;   

            }
            
        }).bind('blur', function(){       
            if(
                $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').text_value            
            ){
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
//                $( this ).helptext('reini');

            }else{

            }
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        /*END AUTOCOMPLETE-CUSTOMERS*/
        
        $( "#s_num_document" ).autocomplete({
            minLength: 1,
            //autoFocus: true,
            source:function(req,response){
                $.ajax({
                    url:url_getSerieCorrelativeByTerm,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                                                                                                                                                                                        
                                var text = "<span class='spanSearchLeft'>" + item.name + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.name,
                                    id : item.id
                                }
                            }));
                        }else{
                            msgBox(resp.msg,resp.code);
                        }
                    }
                });
            },
            focus: function(event, ui) {
                //                $('#custom_price').val(ui.item.price);
            },
            select: function( event, ui ) {
            
            } 
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $("#creditnote_date, #dateSales").datepicker({
            dateFormat:'<?php echo $jquery_date_format ?>'
        });
        
        fnLoadCheck();
        $('#s_num_document').focus();

    });
</script>

<div>
    <center><div class="titleTables"><?php echo __('Administración de Ventas'); ?></div></center>
</div>

<!--Busquedad-->
<div class="zone_rate_wrapper">
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="padding10">
            <form id="form_zone_rate_3">
                <input type="hidden" value="3" name="zone_id">
                <ul class="content_sub_zones">
                    <li>
                        <div class="sub_item_zone">
                            <div class="padding10">
                                <div class="sub_item_top">
                                    <span class="alias_circle ui-state-highlight"><img src="/media/ico/ico_folder_search.png"/></span>
                                    <span class="alias_name">Busqueda</span>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __('Almacén'); ?>:</label>
                                        <select id="idStore">
                                            <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                                            <?php foreach ($array_store as $va): ?>                                   
                                                <option value="<?php echo $va->idStore ?>"><?php echo $va->storeShortName ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </dd>
                                    <dd>
                                        <label><?php echo __('Nro. Comprobante'); ?>:</label>
                                        <input type="text"  id="s_num_document" name="s_num_document" />
                                    </dd>
                                    <dd class="buttonsContainer">
                                        <label><?php echo __('Fecha por'); ?>:</label>                                      
                                        <div id="rdoTypeSearch">
                                            <input type="radio" id="radio1" name="rdoSearch" value="tbd" checked="checked"/><label for="radio1"><?php echo __("Día") ?></label>
                                            <input type="radio" id="radio2" name="rdoSearch"  value="tbr" /><label for="radio2"><?php echo __("Rango de Fechas") ?></label>
                                            <input type="radio" id="radio3" name="rdoSearch" value="tbm"/><label for="radio3"><?php echo __("Mes") ?></label>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dd>
                                        <label class="tbd_group label_text"><?php echo __("Día:") ?></label>
                                        <div class="item_content">
                                             <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                                        </div>
                                    </dd>
                                    <dd> 
                                        <label class="tbr_group label_text"><?php echo __("Fecha Inicial:") ?></label>
                                        <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                                        <div style="margin-top:15px;">
                                               <label class="tbr_group label_text"><?php echo __("Fecha Final:") ?></label>
                                               <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_fdaterango" value="" type="text" maxlength="25" size="20" />  
                                        </div>
                                    </dd>
                                    <dd style="margin-top:-21px;">
                                        <label class="tbm_group label_text"><?php echo __("Mes:") ?></label>
                                        <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                                            <option value="0"><?php echo __("Seleccione Mes") ?></option>
                                            <?php foreach ($a_month_report as $a_dt): ?>
                                                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                            <?php endforeach; ?>
                                        </select>
                                        <div style="margin-top:15px;">
                                            <label class="tbm_group label_text"><?php echo __("Año:") ?></label>
                                            <select class="tbm_group" id="anio" name="anio">
                                                <option value="0"><?php echo __("Seleccione Año") ?></option>
                                                <?php foreach ($a_year_report as $a_dt): ?>
                                                    <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </dd>                                        
                               </dl>                                
                               <button type="button"  id="idSearhSale" name="idSearhSale">Buscar</button>                                    
                            </div>
                        </div>
                        <div class="clear"></div>
                    </li>
                </ul>
            </form>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
<!--JQGRID DE VENTAS-->
<div>
    <div class="div_grid_product">
        <center>
            <div>
                <table id="grid_list_ordersales"> </table>
                <div id="grid_list_ordersales_pager"></div>
            </div>
        </center>
    </div>
</div>
<div class="clear"></div>
<br/>

<!--Nota de Credito-->
<div id="confirm_payment" class="none">
    <h3><?php echo __("¿Desea generar Nota de Crédito?") ?></h3>
</div>
<div id="frmCreditNoteDiv" class="dialog-modal"> 
    <form id="frmCreditNote" method="post">       
        <fieldset  class="ui-corner-all cFieldSet"> 
            <legend class="frmSubtitle"></legend>
            <div class="label_output"> 
                <div>                   
                    <label><?php echo __('Monto de Venta ') ?>:</label>
                    <input type="text" id="creditnote_amountSale" name="creditnote_amountSale" alt="number" class="input_label" readonly/>
                    <input type="hidden" id="creditnote_idSale" name="creditnote_idSale" value="0" readonly/>
                    <div class="clear"></div>
                </div>
                <div>
                    <label><?php echo __('Documento') ?>:</label>
                    <input type="text" id="creditnote_documento" name="creditnote_documento" alt="number" class="input_label" readonly/>
                </div>
                <div style="text-align: center;"> 
                    <label><?php echo __('Nro') ?>:</label>
                    <input type="text" id="creditnote_serie" name="creditnote_serie" alt="number" style="width: 30px;" class="ui-state-active ui-corner-all" readonly/><span><?php echo " - "; ?></span>
                    <input type="text" id="creditnote_correlative" name="creditnote_correlative" alt="number" style="width: 65px;" class="ui-state-active ui-corner-all" readonly/>
                </div>
                <div>
                    <label><?php echo __('Fecha') ?>:</label>
                    <input type="text" id="creditnote_date" name="creditnote_date" class="input_label"  value="<?php echo date($date_format_php) ?>"/>
                </div>   
                <div>
                    <label><?php echo __('Descripción') ?>:</label>
                    <textarea  id="creditnote_description" name="creditnote_description" cols="28" rows="7"></textarea>
                </div> 

        </fieldset>
    </form>
</div>            
<!--Editar Venta --> 
<div id="editSales">
    <form id="frmEditSales" method="POST">
        <fieldset class="ui-corner-all cFieldSet">
            <legend class="frmEditSales"></legend>
            <input type="hidden" id="idSales" name="idSales"/>
            <div>
                    <dl>
                        <dd>
                            <label for="chars_Customers" class="labelEditSales"><?php echo __("Cliente") ?>:</label>                    
                        </dd>
                        <dd>
                            <input type="hidden" id="idCustomers" name="idCustomers"/>
                            <input class="inputs_num_document" type="text" id="chars_Customers" name="chars_Customers" alt="number" autocomplete="off"/>
                        </dd>
                    </dl>
            </div>
            <div>
                    <dl>
                        <dd>
                            <label for="dateSales" class="labelEditSales"><?php echo __("Fecha de Venta") ?>:</label>                    
                        </dd>
                        <dd>     
                            <input id="dateSales" name="dateSales" value="" type="text" maxlength="25" size="20" />
                        </dd>
                    </dl>
            </div>
             <?php if ($activeSeller): ?>
            <div>
                <dl>
                        <dd>
                            <!--CODIGO USUARIO VENDEDOR-->
                            <input type="hidden" id="idUserCode" name="idUserCode"/>
                            <label  for="cmb_listUserCode" class="labelEditSales"><?php echo __("Vendedor") ?>:</label>
                        </dd>
                        <dd >
                            <select  style="width: 150px"  size="1" id="cmb_listUserCode" name="cmb_listUserCode" class="Combo" >
                                <option value=""><?php echo __("Seleccionar Usuario") ?></option>
                                <?php foreach ($listUserCode as $usercode) { ?>
                                    <option value="<?php echo $usercode['id'] ?>"><?php echo $usercode['nameUser'] ?></option>
                                <?php } ?>                            
                            </select>
                        <!--END CODIGO U-V
                        class="search_client_wrapper ui-corner-all ui-state-default"
                        -->
                        </dd>
                </dl>
            </div>   
            <?php endif ?>
        </fieldset>
    </form>
</div>

<!--Editar Detalle de la Venta -->          
<div id="dlg_editDetailSales" class="dialog-modal">   
        <fieldset  class="ui-corner-all cFieldSet"> 
            <legend> Editar Detalle de Venta</legend>
                <form id="formEditSalesDetail" method="POST" action="#">
                        <input id="idDetail" name="idDetail" type="hidden" />
                        <input id="idStoreDetail" name="idStoreDetail" type="hidden" />
                        <input id="idProduct" name="idProduct" type="hidden" />
                        <input id="idSalesDetail" name="idSalesDetail" type="hidden" />                                                                           
                        <input id="quantity_lot" name="quantity_lot" type="hidden" />                                                                           
                        <div class="formEdit">                            
                            <dl>
                                <dd>
                                    <label for="description" class="frmlbl"><?php echo __("Producto") ?>:</label>                    
                                </dd>
                                <dd>                
                                    <div class="clear"></div>
                                    <input class="inputData" id="description" name="description" type="text" readonly="true" style="width: 268px"/>
                                    <input type="hidden" name="haveDetail" id="haveDetail"/>
                                    <input type="hidden" name="idproduct" id="idproduct"/>
                                    <input type="hidden" name="idstore" id="idstore"/>
                                    <input type="hidden" name="amount" id="amount"/>
                                </dd>
                             </dl>
                            <div class="clear"></div>

                            <dl>
                                <dd>
                                    <label for="quantity" class="frmlbl"><?php echo __("Cantidad") ?>:</label>                    
                                </dd>
                                <dd>                
                                    <div class="clear"></div>
                                    <input type="text" id="quantity" name="quantity" alt="number" style="width:40px" />
                                </dd>
                            </dl>
                            <div class="clear"></div>
                            <dl>
                                <dd>
                                    <label for="custom_price" class="frmlbl"><?php echo __("Precio") ?>:</label>                    
                                </dd>
                                <dd>                
                                    <div class="clear"></div>
                                    <input type="text" id="custom_price" name="custom_price" autocomplete="off" style="width:60px" />
                                </dd>
                            </dl>
                            <dl>
                                <dd>
                                    <label for="discount" class="frmlbl"><?php echo __("Descuento") ?>:</label>                    
                                </dd>
                                <dd>                
                                    <div class="clear"></div>
                                    <input type="text" id="discount" name="discount" autocomplete="off" style="width:60px"  />
                                </dd>
                            </dl>
                            <dl>
                                <dd>
                                    <label for="total_price" class="frmlbl"><?php echo __("Total") ?>:</label>                    
                                </dd>
                                <dd>                
                                    <div class="clear"></div>
                                    <input type="text" id="total_price" name="total_price" autocomplete="off" style="width:60px"  />
                                </dd>
                            </dl>
                        </div>
                </form>
        </fieldset>
</div>