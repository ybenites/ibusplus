<style type="text/css">
    .divDocInfo{float:right;}
    li.containerCompany{display: none;}
    div.divDocInfo{display:none;font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    #div_register_client{position: absolute;top:0;left: 0;z-index: 0;
                         box-shadow: rgba(0, 0, 0, 1) 0 0 5px;
                         -webkit-box-shadow: rgba(0, 0, 0, 1) 0px 0px 5px;}
    #div_register_client .divLine,.divLine.autoPayment,.divLine.divMeasures{width: auto;}

    .divLine.divLineShort{width:310px;}
    #div_register_client .arrow_left{
        left: -25px;
        top: 22px;
    }
    .arrow_left{
        position: absolute;
        width: 18px;
        height: 28px;
        background-position: 0 -125px;
        background-image: url(/media/images/arrows.png);        
    }
    .ui_icon_close_register_client{display: block;float: right;cursor: pointer;}
    .ui_icon_close_register_client span{float: left;}
    .register_client_title{padding: 5px;margin-bottom: 10px;line-height: 16px;}
    #formRegisterClient ul{float: left;}
    #formRegisterClient .div_buttons{padding: 10px 0;}
    #formRegisterClient input{width: 166px;}
    #formRegisterClient select{width: 178px;}
    /**/
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    .relative{position: relative;}
    .search_client_wrapper span{margin: 3px 4px;}
    .ui-widget-content .search_client_wrapper{background-image: none;margin-right: 5px;}
    .search_client_wrapper,.search_client_wrapper span,
    .search_client_wrapper input{float: left;}
    .ui-widget-content .search_client_wrapper input{ 
        width: 245px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    input.inputData{width: 270px;}
    input[type="text"], input[type="password"], textarea, select {
        padding: 5px;
    }
    hr.hr_separate{border-top-width: 3px;border-top-style: dashed;border-bottom: none;border-left: none;border-right: none;}
    label.frmlblcv{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    .divLine.divLineShort{width:310px;}
    div.divLeft{float: left;}
    input.inputQuantity{width: 50px;}
    .labelprice {
        display: block;
        float: left;
        font-weight: bold;
        height: 16px;
        padding-top: 5px;
        width: 100px;
    }
     label.frmlblshort{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    label.frmlblcv{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    input.subPrice{width: 200px; height: 19px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    .ui-widget input.subPrice {font-size: 18px;background-image: none; width: 90px;}
    .subTotal{
        float: left;
    }
    input.subPriceLong{width: 200px; height: 19px; text-align: left;float: left;margin-top: 5px;margin-right: 5px;}
    input.inputData{width: 160px;}
    input.amountPaid{width: 100px;text-align: right;}
    #addPackageDetail{margin: 5px 0;}
    .buttonadd{margin-top: 15px;}
    .priceTax{display:none;}
    .ui-widget input.lightSearch{font-style: italic;color: #CCCCCC;}
    #quantity{width: 50px;text-align: right;margin-right: 10px;}
    input.inputCustomPrice{width: 60px;text-align: right;}
    .ui-widget-content input[type="text"].ui-autocomplete-loading ,
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-image: url('/media/images/ajax-loading.gif');background-position: right center;background-repeat: no-repeat; }
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-position: right top;}
    .myOverlay, .myShadowMsg, .myContentMsg,.my_payment_shadow,.my_payment_content{display: none;}
    .myOverlay{z-index: 999;position: fixed;}
    .myShadowMsg{height: 122px;
                 left: 50%;
                 margin-left: -205px;
                 margin-top: -100px;
                 position: fixed;
                 top: 50%;
                 width: 417px;
                 z-index: 999;}
    .myContentMsg{ 
        font-family: UbuntuTitle;
        font-size: 65px;
        height: 80px;
        left: 50%;
        line-height: 80px;
        margin-left: -195px;
        margin-top: -90px;
        padding: 10px;
        position: fixed;
        text-align: center;
        top: 50%;
        width: 375px;
        z-index: 999;
    }
    #frmCreditCash{
        display:block;
        margin: 5px;
    }
    #frmCreditCard {
        display:none;
        margin: 5px;
    }

    .lPayment, .lSymbol {
        float: left;
        font-size: 18px;
        line-height: 35px;
        width: 105px;
    }
    .lPayment{text-align: right;}
    .lSymbol {
        text-align: center;
        width: 50px;
    }
    .rowLine {
        padding: 0px20px;
    }
    .post_display_prices{
        border: 1px solid #AAAAAA;
        border-radius: 16px 16px 16px 16px;
        cursor: pointer;
        float: left;
        font-size: 12px;
        height: 32px;
        left: 50%;
        margin: 0 10px 0 auto;
        position: relative;
        text-align: center;
        transition: color 0.5s linear 0.5s;
        width: 32px;
    }
    .ui-widget .post_confirm_payment_return_cash, .ui-widget .post_confirm_payment_total_on_account{
        float: left;
        font-family: UbuntuTitle;
        font-size: 18px;
        /*height: 45px;*/
        line-height: 36px;
        text-align: right;
        margin-left: 38px;
    }
    .Combo{
        width: 175px;
    }

    #post_confirm_payment {
        background-color: #FFEF8F;
        border: medium none;
        float: left;
        font-family: UbuntuTitle;
        font-size: 18px;
        height: 33px;
        margin-left: 38px;
        text-align: center;
        width: 53px;
    }
    .myOverlay{display: none;}
    input.helptext{font-style: italic;color: #CCCCCC;}
    /* Cliente */
    span.spanSearchLeft{float: left;max-width: 184px;}
    .ui-menu span.ui-icon.spanSearchLeft,
    .ui-menu span.ui-icon.spanIcon
    {float: left;display: inline-block;position: relative;top: 0;left: 0;}
    span.spanPref{float: left;margin-right: 2px;}
    span.spanIcon{float: left;margin-right: 2px;}
    span.spanSearchRight{float: right;}
    
    .infoReserva {
        margin-bottom: 10px;
        margin-top: 10px;
        width: 168px;
        margin-left: 86px;
        margin-right: 86px;
        float: left;
        border-style: dashed;
        text-align: left;
    }
    .lblInfoReserva{
        float: left;
        margin-left: 12px;
        font-weight: bolder;
        margin-right: 11px;
    }
    
    #frmCreditCard .label_output label{min-width: 120px;}
</style>
<script type="text/javascript">
    /* URL */
    var url_SaveCustomers = '/private/customers/createCustomers'+jquery_params;
    var url_GetClients = '/private/sales/getClients'+jquery_params;
    var url_GetProduct = '/private/orderform/getProduct'+jquery_params;
    var url_RegisterOrder = '/private/orderform/createSalesOrder'+jquery_params;
    var url_Ubigeo= '/private/customers/ubigeoCustomers'+jquery_params;
    var url_GetProductByCodigo = '/private/sales/getProductByCode'+jquery_params;
    var url_GetDetailByProductId = '/private/sales/getDetailByProductId'+jquery_params; 
    var url_getLotByProductId = '/private/sales/getProductLotById'+jquery_params;  
    var url_RegisterOrderSales = '/private/orderform/createSalesOrder' +jquery_params;
    var url_createSalesOrder = '/private/orderform/createSalesOrder'+jquery_params;
    
    /* MENSAJES PHP */
    var sTxtSearchClient = '<?php echo __('Búsqueda de Cliente') ?>';
    var order_title = '<?php echo __('Nota de Venta') ?>';
    var order_msg_confirm = '<?php echo __('¿Desea confirmar el registro de la Nota de Pedido?') ?>';
    var order_customer = '<?php echo __("El Cliente es obligatorio."); ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>';
    var deleteSubTitle = "<?php echo __("Alerta"); ?>";
    var orderSalesMsgConfirm = "<?php echo __("¿Desea confirmar la reserva de sus Productos.?"); ?>";
    var orderSalesTitle = "<?php echo __("Confirmar Reserva"); ?>";
    var amount_count = '<?php echo __("Debe ingresar un monto a cuenta"); ?>';
    var amount_exceed_total = '<?php echo __("El monto no debe exceder ni ser igual al total "); ?>';
    var reason_required = '<?php echo __("La Razón Social es obligatorio.") ?>';
    var mandatory_ruc = '<?php echo __("El RUC es obligatorio.") ?>';
    var ruc_not_correct = '<?php echo __("El RUC no es correcto"); ?>';
    var name_required = '<?php echo __("El Nombre es obligatorio.") ?>';
    var lastname_required = '<?php echo __("El Apellido es obligatorio.") ?>';
    var dni_required = '<?php echo __("El DNI es obligatorio.") ?>';
    var dni_not_correct = '<?php echo __("El DNI no es correcto"); ?>';    
    var registered = '<?php echo __("Registrado"); ?>';
    
    //ColName reserveDetail_jqGrid
    var id_colName = '<?php echo __("ID") ?>';
    var description_jqGrid =  '<?php echo __("Descripción") ?>';
    var amount_jqGrid = '<?php echo __("Cantidad") ?>';
    var unit_price = '<?php echo __("Precio Unitario") ?>';
    var tax = '<?php echo __("Impuesto") ?>';
    var discount = '<?php echo __("Descuento") ?>';
    var sub_total = '<?php echo __("SubTotal") ?>';
    var has_Detail = '<?php echo __("Tiene Detalle") ?>';
    var option = '<?php echo __("Opción") ?>';
    var bc_id_product = '<?php echo __("ID Prod.") ?>';
    var amounttotal = '<?php echo __("Total") ?>';
    var select_type_payment = '<?php echo "Seleccionar el Tipo de Pago"; ?>';
    var Aceptar = '<?php echo "Aceptar" ?>';
    var Cancelar = '<?php echo "Cancelar" ?>';
    var product_added = '<?php echo __("Este Producto Ya esta Agregado") ?>';
    var no_products_batches = '<?php echo __("No hay Productos en Lotes") ?>';
    var insufientes_stock_product = '<?php echo __("Stock de Productos Insufientes.") ?>';
    var no_products = '<?php echo __("No hay Productos") ?>';
    var unassigned_document = '<?php echo __("Documento No Asignado") ?>';
    var payment_required = '<?php echo __('El pago es requerido') ?>';
    var payment_greater_equal_total_amount = '<?php echo __('El pago debe ser mayor o igual al monto total') ?>';
    var paymentmix_greater_equal_total_amount = '<?php echo __('El pago mixto debe ser mayor o igual al monto total') ?>';
    var alert_amount_text = '<?php echo __('El monto a Cuenta debe ser menor al Monto Total del costo de(los) prodcto(s)') ?>';
    var minamount = '<?php echo __("El pago debe se Mayor o Igual al monto a Cuenta") ?>';
    var documents_print = '<?php echo __("Desea Imprimir Documentos?"); ?>';
    var shipping_document_printing = '<?php echo __("Impresión de Documento de Envío") ?>';
    var negative_amount = '<?php echo __("La cantidad debe ser mayor a 0"); ?>';
    var txt_selectsearch = '<?php echo __("Selecionar Usuario"); ?>';
    
    /* VARIABLES PHP */
    var dt_boleta = '<?php echo $document; ?>';
    var bBarcodeReader = '<?php echo $bBarcodeReader; ?>';
    var jquery_date_format = '<?php echo $jquery_date_format; ?>';
    var CLIENT_TYPE_COMPANY = '<?php echo $CLIENT_TYPE_COMPANY ?>';
    var CLIENT_TYPE_PERSON = '<?php echo $CLIENT_TYPE_PERSON ?>';
    var typeCash ='<?php echo $cash; ?>';
    var typeCard = '<?php echo $creditCard; ?>';
    var bAvailableDocumentType = '<?php echo $bAvailableDocumentType ?>';
    
    /* VARIABLES */
    var formRegisterClient;     
    var frm_customers;
    var tblSalesDetail;
    var bctblSalesDetail;
    var array_detail_id_p=[];
    var array_detail_id_pl=[];
    var arrayDetails_lots = new Object();  
    var save_arrayDetails_lots = new Object();  
    var flags = {};
    var iCountLot = 0;
    var amountToPaid = 0.00;
    var totalSubTotales = 0.00;
    

    var countarray = 0;
    var keycountarray= 0;
    var quantity = 0;
    flags.isRange = 0;
    flags.haveLot = 0;
    var arrayDetails = new Object();
    var cbitemDetail = new Object();
    
    
    
   
    flags.isRange = 0;
    flags.haveLot = 0;
    
    $(document).ready(function(){
        
        /* Buttons */
        $('.addCustomer').button({
            icons: {
                primary: 'ui-icon-plus'
            },
            text:false
        });
        $('#saveReserve').button({
            icons:{
                primary: 'ui-icon-disk'
            }
        });
        $('#cancelSales').button({
            icons:{
                primary: 'ui-icon-cancel'
            }
        });
        $('#addPackageDetail').button({
            icons:{
                primary: 'ui-icon-plus'
            }
        });
        $('#btn_save_client').button({
            icons:{
                primary: 'ui-icon-check'
            }
        });
        /* PriceFormat */
        $('#amountPaid, #descuento, #post_confirm_payment').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* Datepicker */
        $('#expirationDate' ).datepicker({
            dateFormat: jquery_date_format,
            beforeShow: function(){
                return {minDate:$("#orderSalesDate").datepicker("getDate")};           
            }
        });  
        $('#expirationDate' ).datepicker("setDate", "+7d" );
        $('#orderSalesDate').datepicker({
            dateFormat: jquery_date_format
        });
        
        /* Combobox */
        $('#passanger_country').combobox();
        $('#cmb_listUserCode').combobox({       
            selected:function(){
                // Focus en producto
                setTimeout(function(){
                  $("#codigo").focus();                
                }, 10);
            }
        });
        $("#passanger_dst").combobox();
        $("#passanger_prov").combobox({
            selected:function(){
                fillCombo("passanger_dst", $("#passanger_prov").val(), $("#passanger_dpto").val());
            }
        });
        $("#passanger_dpto").combobox({
            selected:function(){
                fillCombo("passanger_prov", $("#passanger_dpto").val(),'provincia');
            }
        });
        /* ButtonSet */
        $('.buttonsContainer').buttonset();
        $('#chars_Customers').helptext({value: sTxtSearchClient, customClass: 'lightSearch'});
        /* KeyUp */
        $('#post_confirm_payment').keyup(function(key){
             fnUpdateConfirm_post();
        });
        
        
        /* Tipo de Documento */
        if (bAvailableDocumentType == 1){
            successDocument = false;                
            fnUpdateCorrelativeOrderSales = function(sType){
                try{                
                    oDocumentType = getCurrentCorrelative(sType);   
                    if(oDocumentType.serie != null)
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.serie + '-' +oDocumentType.correlative;
                    else
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.correlative;                                                                                
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-highlight')
                    .html(displayDocument);
                    successDocument = true;                                                              
                }catch(e){
                    successDocument = false;
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-error')
                    .html(unassigned_document);
                }
            };
            fnUpdateCorrelativeOrderSales(dt_boleta);
            $('div.divDocInfo').css('display', 'block');
            $('input[name=document_type]').change(function(){
                fnUpdatePurchaseOrders($(this).val());
            });                                  
        }
    
  
        /************************** Nuevo Cliente *************************************/     
        //Validación del Registro de Nuevo Cliente
        formRegisterClient = $('#formRegisterClient').validate({
            rules: {                
                company_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value=CLIENT_TYPE_COMPANY]:checked").is(':checked');
                    }
                },
                company_code :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value=CLIENT_TYPE_COMPANY]:checked").is(':checked');
                    },
                    maxlength:11,
                    minlength:11
                },
                passanger_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value=CLIENT_TYPE_PERSON]:checked").is(':checked');
                    }
                },
                passanger_lastname :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value=CLIENT_TYPE_PERSON]:checked").is(':checked');
                    }
                },
                passanger_mail :{
                    required: true,
                    email:true
                },
                passanger_documento:{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value=CLIENT_TYPE_PERSON]:checked").is(':checked');
                    },
                    maxlength:8,
                    minlength:8
                }
            },
            messages:{
                company_name :{required: reason_required},
                company_code :{required: mandatory_ruc,
                    maxlength: ruc_not_correct,
                    minlength: ruc_not_correct
                },
                
                passanger_name :{required: name_required},
                passanger_lastname :{required: lastname_required},
                passanger_mail :{
                    required: 'El E-mail es requerido.',
                    email:'El E-mail es incorrecto.'
                },
                passanger_documento:{
                    required: dni_required,
                    maxlength: dni_not_correct,
                    minlength: dni_not_correct
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){   
              
                $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_SaveCustomers,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res)){                            
                            var data = res.data;                            
                            if(data.typeCustomers == CLIENT_TYPE_PERSON){
                                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
                            }else if(data.typeCustomers == CLIENT_TYPE_COMPANY){
                                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
                            }                
                            $('#idCustomers').val(data.idCustomers);
                            $('#chars_Customers').val(data.customers);
                            msgBox(registered);
                            $('#div_register_client').hide('slide');
                            formRegisterClient.currentForm.reset();                                                        
                        }
                    }
                });
            }
        });
        // Tipo de Cliente
        $('input:radio[name=typeClient]').change(function(){
            $(this).each(function(){
                if($(this).val()==CLIENT_TYPE_PERSON){
                    $('li.containerCompany').hide();
                    $('li.containerPerson').show();
                }else{
                    $('li.containerPerson').hide();
                    $('li.containerCompany').show();
                }
            });
        });         
        // Crear Nuevo Cliente
        $('.addCustomer').click(function(){
            o_btn_add_client_position = $(this).offset();            
            formRegisterClient.currentForm.reset();
            formRegisterClient.resetForm();  
            $('input, select, textarea', formRegisterClient.currentForm).removeClass('ui-state-error');
            $('#div_register_client').css({left: (o_btn_add_client_position.left+60)+'px', top: (o_btn_add_client_position.top-60)+'px'
            });
                !$('#div_register_client').is(':visible')?$('#div_register_client').show('slide'):null;            
        });
        $('.ui_icon_close_register_client').click(function(){
            formRegisterClient.currentForm.reset();
            formRegisterClient.resetForm();
            $('input, select, textarea', formRegisterClient.currentForm).removeClass('ui-state-error');
            $('#div_register_client').hide('slide');
        });
        

        /************************* Reserva   ******************************************/
        // Autocomplete de CLiente 
        $( "#chars_Customers" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_person_sender',
            source:function(req,response){
                $.ajax({
                    url:url_GetClients,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                if(item.type == CLIENT_TYPE_PERSON){
                                    item_icon = 'ui-icon-person';
                                }
                                if(item.type == CLIENT_TYPE_COMPANY){
                                    item_icon = 'ui-icon-home';
                                }
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.customers + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.customers,
                                    id : item.idCustomers,
                                    type : item.type
                                }
                            }));
                        }else{
                            msgBox(resp.msg,resp.code);
                        }
                    }
                });
            },
            focus: function(event, ui) {
                if(ui.item.type == CLIENT_TYPE_PERSON){
                    $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
                }
                if(ui.item.type == CLIENT_TYPE_COMPANY){
                    $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
                }
            },
            select: function( event, ui ) {
                $('#idCustomers').val(ui.item.id);
                
                $(this).data('autocomplete').text_value = ui.item.value;

                if(ui.item.type == CLIENT_TYPE_PERSON){
                    $('.icon_sender_type').removeClass('ui-icon-home ui-icon-help').addClass('ui-icon-person');

                }
                if(ui.item.type == CLIENT_TYPE_COMPANY){
                    $('.icon_sender_type').removeClass('ui-icon-person ui-icon-help').addClass('ui-icon-home');
                }   
            },
            change: function( event, ui ) {   
                if ( !ui.item ) {       
                    $('#idCustomers').val('');
                    $(this).helptext('refresh');
                    $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                    return false;
                }
                return true;   
            }
            
        })
        .bind('blur', function(){       
            if(
            $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').text_value            
        ){
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                $( this ).helptext('reini');

            }else{

            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };        
        $( "#chars_Customers" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 320, 'margin-bottom': 5});            
        };
        function fillCombo(updateId,value,des){
            $.ajax({                  
                type:"POST",
                dataType:"json",
                url:url_Ubigeo,
                data:{
                    id:value,
                    des:des   
                },
                success:function(data){
                    $("#"+updateId).empty();
                    $.each(data, function(i, item) { 
                        $("#"+updateId).append("<option value='" 
                            +item.valor+"'>" +item.descripcion+ "</option>");
                    });
                }
            });
        }
        
    
    
        /********* Activación de codigo de barras **********/
        //Click de Codigo de Barras
        $("input#codigo").keypress(function(event) {
        if(($("#cb_quantity").val()>=1)){
            if ( event.which == 13 ) {
                event.preventDefault();
                code = $.trim($(this).val());
                getProductByCode(code.substring(0,7));
            }}
        else {
            msgBox(negative_amount);
        }
        });
        /* Buscar Producto */ 
        function getProductByCode(code){
            showLoading=1;
            $.ajax({
                url:url_GetProductByCodigo,                        
                type:'POST',
                dataType:'jsonp',
                data:{
                    code:function(){
                        return code;
                    }
                }, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        var exit;
                        var r=response.data;
                        var quantity = parseInt($('input#cb_quantity').val());
                        if(r.length>0){                            
                            $.each(r,function(index,data){ 
                                /* Si la cantidad es mayor al stock del producto */
                                if(parseInt($.trim(data.Amount)) >= quantity){
                                    flags.isRange = 1;                                               
                                    /***** Verificar Duplicidad de ID Product *****/
                                    var ids=$("#bc_salesDetail_jqGrid").jqGrid('getDataIDs');                    
                                    var count=0;
                                    if(ids.length>0){
                                        $.each(ids,function(i,d){
                                            renDatos = $("#bc_salesDetail_jqGrid").getRowData(ids[i]);
                                            if(renDatos.id==data.idProduct){                               
                                                count=count+1;
                                            } 
                                        });
                                        if(count>0){ 
                                            exit = 0;
                                            msgBox('<?php echo __("Este Producto Ya esta Agregado") ?>','<?php echo __("Alerta") ?>');
                                            return false;
                                        }
                                    }
                                    if (exit == 0)
                                        return false;
                                    /***** Fin Verificar Duplicidad de ID Product *****/
                                    /*** Proceso de Verificar Lotes, Almacenes y Stock ***/ 
                                    var id=data.idProduct;                             
                                    $.ajax({
                                        url:url_getLotByProductId,                        
                                        type:'POST',
                                        dataType:'json',
                                        data:{idpg:id}, 
                                        async:false,
                                        success:function(response){
                                            if(evalResponse(response)){
                                                var r=response.data; 
                                                if(r=='not_store'){ 
                                                    msgBox('<?php echo __("Su usuario de Venta no tiene asignado un almacén. <br/>Debe hacerlo para poder vender.") ?>','<?php echo __("Aviso") ?>');
                                                    return false;                                       
                                                    flags.haveLot = 0;
                                                }else{
                                                    if(r.length>0){
                                                        flags.haveLot = 1; // Tiene lote
                                                        var j = 0;
                                                        $.each(r,function(i,d){ 
                                                            var itemDetail = new Object();
                                                            itemDetail.id = d.idLot;
                                                            itemDetail.productName = d.productName;
                                                            itemDetail.amount = d.amount;
                                                            itemDetail.creationDate = d.creationDate;                                                    
                                                            // Ingreso todos los lotes en el arreglo
                                                            arrayDetails_lots[j++] = itemDetail;
                                                        });                            

                                                    }else{
                                                        msgBox('<?php echo __("No hay Productos en Lotes") ?>','<?php echo __("Alerta") ?>');
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    });                            
                                    /*** Agregar al jqgrid ***/                                    
                                    if(flags.haveLot){   
                                        cbitemDetail.id = data.idProduct;
                                        cbitemDetail.descriptionOut = data.product;
                                        cbitemDetail.idProductOut = data.haveDetail;
                                        cbitemDetail.quantityOut = $('#cb_quantity').val();
                                        cbitemDetail.unitPriceOut = data.productPriceSell;
                                        cbitemDetail.discountOut = $('#descuento').val();
                                        cbitemDetail.subTotalOut = ($.trim($('#cb_quantity').val())*$.trim(data.productPriceSell)) - $.trim($('#descuento').val()) ;
                                        cbitemDetail.amount = data.Amount;
                                        cbitemDetail.haveDetail=data.haveDetail;
                                        $("#bc_salesDetail_jqGrid").jqGrid('addRowData',iCountLot,cbitemDetail);                              
                                        //$('#custom_price').val(data.productPriceSell);                                    
                                    }else{
                                        return false;
                                    } 

                                }else{
                                    flags.isRange = 0;
                                    msgBox(stockInsuficiente,msgAlerta);
                                    return false;
                                }
                            });
                            if (exit == 0)
                                return false;
                            // Si hay productos 
                            if(flags.isRange){
                                var temp_q =  0;
                                var c=0;
                                temp_q = parseInt(quantity); 
                                // Recorro los lotes para disminuir la cantidad segun su stock
                                $.each(arrayDetails_lots,function(i,d){
                                    var s_itemDetail = new Object(); 
                                    if(quantity <= parseInt(d.amount) && i==0){    
                                        s_itemDetail.id  = d.id;
                                        s_itemDetail.amount  = quantity; 
                                        save_arrayDetails_lots[iCountLot++] = s_itemDetail;                    
                                        return false;
                                    }else{
                                        if(temp_q>0){                                         
                                            if(temp_q >= parseInt(d.amount)){   
                                                s_itemDetail.id  = d.id;
                                                s_itemDetail.amount  = parseInt(d.amount);
                                                temp_q= temp_q-parseInt(d.amount);  
                                                save_arrayDetails_lots[iCountLot++] = s_itemDetail;//                                          
                                            }else{            
                                                s_itemDetail.id  = d.id;
                                                s_itemDetail.amount  = temp_q;
                                                temp_q= temp_q-d.amount;   
                                                save_arrayDetails_lots[iCountLot++] = s_itemDetail;//  

                                            }                                           
                                        }                                 
                                    }                                
                                });                                                    
                            }else{
                                return false;
                            }      
                        }else{
                            msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>');
                            return false;
                        }
                    }
                    /* Regresa el focus */
                    $('input#codigo').val('');
                    $('input#codigo').focus();
                }
            });
        }
        /* JQGrid */
        bctblSalesDetail = $('#bc_salesDetail_jqGrid').jqGrid({            
            height: '100%',                        
            width:'840',
            datatype: 'local',
            sortable: false,
            colNames:[                                
                id_colName,
                bc_id_product,
                description_jqGrid,
                amount_jqGrid,
                unit_price,
                tax,
                discount,
                sub_total,
                has_Detail,
                option
            ],
            colModel:[                                
                {name:'idOut',index:'idOut',key:true,width:240,hidden:true},
                {name:'id',index:'id',width:240,hidden:true},
                {name:'descriptionOut',index:'descriptionOut',width:240},
                {name:'quantityOut',index:'quantityOut',width:100, align: 'center'},
                {name:'unitPriceOut',index:'unitPriceOut', width:100, align: 'right'},                
                {name:'taxUnitOut',index:'taxUnitOut',width:100, align: 'right'},
                {name:'discountOut',index:'discountOut',width:100, align: 'right'},
                {name:'subTotalOut',index:'subTotalOut',width:50,align:'right',sorttype:'number',formatter:'number', summaryType:mysubTotalOustrSum,summaryTpl : '{0}'},                
                {name:'haveDetail',index:'haveDetail', width:100, align: 'right',hidden:true},
                {name:'optionsOut',index:'optionsOut', width:60, align: 'center'}
            ],
            viewrecords: true,
            sortname: 'idOut',            
            //gridview : true,
            rownumbers: true,      
            rownumWidth: 30,            
            footerrow:true,
            grouping: true,
            groupingView : {                
                groupColumnShow : [true],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,
                groupOrder: ['asc'],
                groupSummary : [true], 
                showSummaryOnHide:true,
                groupDataSorted : true
            },
            afterInsertRow: function(rowid, rowdata){
                var deleteOption = "<a class=\"cb_deleteItem\" style=\"cursor: pointer;\" data-total=\""+rowdata.subTotalOut+"\" data-id=\""+rowid+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                bctblSalesDetail.setRowData(rowid,{optionsOut: deleteOption});
                totalSubTotales = parseFloat(totalSubTotales)+parseFloat(rowdata.subTotalOut);
                bctblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});
            },
            gridComplete:function(){  
                try{
                    $('.cb_deleteItem').button({
                        icons: {primary: 'ui-icon-trash'},text: false
                    })
                    .click(function(){
                        var id_delete = $(this).data('id');
                        var subtotal = $(this).data('total');
                        confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                            if(response){                              
                                delete save_arrayDetails_lots[id_delete];
                                bctblSalesDetail.delRowData(id_delete);
                                // Descontar al total y actualiza
                                totalSubTotales = parseFloat(totalSubTotales) - parseFloat(subtotal);
                                bctblSalesDetail.footerData('set',{discountOut:'<?php echo __("Total") ?>',subTotalOut:totalSubTotales});                              
                                $('input#codigo').val('');
                                $('input#codigo').focus();                   
                            }
                        });
                    });
             
                }catch(e){
                    //GG *_*
                }
            }
        });
 
 
        /******** Desactivacion de codigo de barras *******/
        /* JQGrid -- Chekear para mundomovil */
        tblSalesDetail = $('#reserveDetail_jqGrid').jqGrid({            
            height: '100%',                        
            width:'840',
            datatype: 'local',
            sortable: false,
            colNames:[                                
                id_colName,
                description_jqGrid,
                amount_jqGrid,
                unit_price,
                tax,
                discount,
                sub_total,
                has_Detail,
                option
            ],
            colModel:[                                
                {name:'id',index:'id',key:true,width:240,hidden:true},
                {name:'descriptionOut',index:'descriptionOut',width:240},
                {name:'quantityOut',index:'quantityOut',width:100, align: 'center'},
                {name:'unitPriceOut',index:'unitPriceOut', width:100, align: 'right'},                
                {name:'taxUnitOut',index:'taxUnitOut',width:100, align: 'right'},
                {name:'discountOut',index:'discountOut',width:100, align: 'right'},
                {name:'subTotalOut',index:'subTotalOut',width:50,align:'right',sorttype:'number',formatter:'number', summaryType:mysubTotalOustrSum,summaryTpl : '{0}'},                
                {name:'haveDetail',index:'haveDetail', width:100, align: 'right',hidden:true},
                {name:'optionsOut',index:'optionsOut', width:60, align: 'center'}
            ],
            viewrecords: true,
            sortname: 'idOut',            
            //gridview : true,
            rownumbers: true,      
            rownumWidth: 30,            
            footerrow:true,
            grouping: true,
            groupingView : {                
                groupColumnShow : [true],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,
                groupOrder: ['asc'],
                groupSummary : [true], 
                showSummaryOnHide:true,
                groupDataSorted : true
            },
            
            gridComplete:function(){  
                //                try{
                //                    var aIDs = tblSalesDetail.getDataIDs();
                //                    $.each(aIDs,function(){
                //                         deleteOption = "<button class='deleteItem' type='button' data-id='"+this+"'>Eliminar</button>"
                //                         tblSalesDetail.setRowData(this,{
                //                             optionsOut: deleteOption
                //                         });
                //                    });
                //                    $('.deleteItem').button({icons: {primary: 'ui-icon-minus'},text: false});
                //                    $('.deleteItem').click(function(){
                //                        var id_delete = $(this).data('id');
                //                        confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                //                            if(response){
                //                                delete arrayDetails[id_delete];
                //                                tblSalesDetail.delRowData(id_delete);
                //                            }
                //                        });
                //                    });
                //                }catch(e){
                //                    
                //                }
            }
        });
 
        /* Funciones */
        // Calcular el SubTotal
        function mysubTotalOustrSum(val, name, record){            
            return parseFloat(val||0)+parseFloat(record.subTotalOut);
        }
        // Calcular vuelto 
        fnUpdateConfirm_post = function(){
           fConfirmPayment_post = $('#post_confirm_payment').val().split(' ').join('');
           fConfirmPayment_post = fConfirmPayment_post ==''?0:fConfirmPayment_post;
           if($("#postformConfirmPayment input[name=payment_type][value='<?php echo Rms_Constants::SALES_PAYMENT_CASH; ?>']:checked").is(':checked') == true){
                fReturnCash_post = parseFloat(fConfirmPayment_post) - parseFloat($('.post_confirm_payment_total_on_account').text());
           }
           $('.post_confirm_payment_return_cash').text(fReturnCash_post.toFixed(2));
        };

        /******************************** Guardar Reserva *************************************/
        /* Validación de Cliente */
        frm_customers=$("#frm_customers").validate({
            rules:{
                idCustomers:{
                    required:true
                }
            },
            messages:{
                idCustomers:{
                    required:order_customer
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        /* Validacion de Reserva */
        formSalesDetail = $("#formSalesDetail").validate({
            rules:{
                amountPaid:{
                    required:true,
                    max: function(){
                        return totalSubTotales-1;
                    }
                }
            },
            messages:{
                amountPaid:{
                    required:  amount_count,
                    max: amount_exceed_total
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');}
        });
        /* Guardar Reserva */
        $("#saveReserve").click(function(){                
            if(!$("#frm_customers").valid())
                return false;
            if(!$("#formSalesDetail").valid())
                return false;
            if($("#cmb_listUserCode").val()==''){
                msgBox(txt_selectsearch);
                return false; 
            }
            /* Calculamos Saldo */
            $(".post_confirm_payment_total_on_account").text($("#amountPaid").val()); 
            $(".post_confirm_payment_total").text($("#amountPaid").val()); 
            $(".post_confirm_payment_total_amount").text(totalSubTotales.toFixed(2));
            $('#amount_paymentCredit').val($("#amountPaid").val());
            var total_amount = $(".post_confirm_payment_total_amount").text();
            var total_on_account = $(".post_confirm_payment_total_on_account").text();
            var balance_reserve = parseFloat(total_amount) - parseFloat(total_on_account);        
            $(".post_confirm_payment_return_balance").text(balance_reserve.toFixed(2)); 
            $("#frmTypePay").dialog('open');
            $('#post_confirm_payment').focus();
        });
        /* Cancelar Reserva */
        $("#cancelSales").on('click',function(e){
            window.location.reload();
        });
        /* Dialog de Confirmacion de Pago */
        $("#frmTypePay").dialog({
            title: select_type_payment,
            autoOpen: false,
            width: 'auto',
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: false, 
            position:["center", 70],
            buttons:{
                Aceptar :function(){    
                    $("#postformConfirmPayment").submit();
                },
                Cancelar :function(){
                    $(this).dialog('close');
                    postformConfirmPayment.currentForm.reset();
                    $(".post_confirm_payment_return_cash").text('');
                    
                }
            },
            close:function(){                                        
                postformConfirmPayment.currentForm.reset();
                $(".post_confirm_payment_return_cash").val('');
            },
            open: function(){
                $("input[name=payment_type]").val(["CASH"]);
                $("input[name=payment_type]:checked").trigger("change");                
            }
        });
        // Tipo de Pago 
        $('input[name=payment_type]').change(function(){
            var b_paymentType = $(this).val();
            $('#post_confirm_payment').val('0.0');
            $('.post_confirm_payment_return_cash').text('0.0');   
            // Evaluo el tipo de pago 
            if(b_paymentType == typeCash){
                    $("#frmCreditCash").show();
                    $("#frmCreditCard").hide();           
            }else if(b_paymentType == typeCard ){
                    $("#frmCreditCash").hide();
                    $("#frmCreditCard").show();
                    $("#cardNumber").val('');
                    $("#operationNumber").val('');
                    $("#typeCard").val('');
            }
        }); 
        /* Confirmacion de Pago */
        $.validator.addMethod("validPayment", function(value, element, params) { 
            if($("#postformConfirmPayment input[name=payment_type][value='<?php echo $cash ?>']:checked").is(':checked')== true){
                return parseFloat(value) >= parseFloat($('.post_confirm_payment_total_on_account').text())?true:false; 
            }else
                return true;
        
        }, "");
        var postformConfirmPayment = $('#postformConfirmPayment').validate({
              rules:{
                  post_confirm_payment: {
                      required: function(){
                          return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $cash ?>']:checked").is(':checked');
                      },
                      validPayment: true

                  },
                  // Credit 
                  typeCard:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                     }
                  },
                  cardNumber:{
                     required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                     }
                  },
                  operationNumber:{
                      required:function(){
                          return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                      }
                  },
                  amount_paymentCredit:{
                       required:function(){
                           return $("#postformConfirmPayment input[name=payment_type][value='<?php echo $creditCard ?>']:checked").is(':checked');
                       } 
                  }
                 

              },
              messages:{
                  post_confirm_payment: {
                      required: '<?php echo __('El pago es requerido') ?>',
                      validPayment: '<?php echo __('El pago debe ser mayor o igual al monto total') ?>'

                  },
                  typeCard:{required: '<?php echo __('El tipo de tarjeta es requerido') ?>'},
                  cardNumber:{required: '<?php echo __('El numero de tarjeta es requerido') ?>'},
                  operationNumber:{required: '<?php echo __('El numero de operacion  es requerido') ?>'},
                  amount_paymentCredit:{required: '<?php echo __('El pago es requerido') ?>'}
              },
              errorContainer:'#errorMessages',
              errorLabelContainer: "#errorMessages .content .text #messageField",
              wrapper: "p",
              highlight: function(element, errorClass) {
                  $(element).addClass('ui-state-error');
              },
              unhighlight:function(element, errorClass, validClass){
                  $(element).removeClass('ui-state-error');
              },
              submitHandler: function(postformConfirmPayment){                          
                confirmBox  (orderSalesMsgConfirm ,'Reserva',function(event){
                    if(event){    
                        var bc_arrayDataGrid=jQuery("#bc_salesDetail_jqGrid").jqGrid("getRowData"); 
                        var oData = new Object();
                        oData.amountPaid = $(".post_confirm_payment_total_on_account").text();
                        oData.header = $("#frm_customers").serializeObject();
                        oData.userCode = $("#cmb_listUserCode").val();
                                       
                        oData.paymentCard = $("#frmPayment").serializeObject();
                        oData.idStore = $("#select_store").attr('data');  
                        // Tipo de dato
                        //                                var paymentType = $('input[name=payment_type]:checked').val();
                        oData.payment = $("#postformConfirmPayment").serializeObject();    
                        oData.idStore = $("#select_store").attr('data');  
                        if(bBarcodeReader){
                           oData.detail =bc_arrayDataGrid;
                           oData.detail_product_l=save_arrayDetails_lots;
                        }else{
                            oData.detail =arrayDataGrid;
                            oData.detail_product_l=array_detail_id_pl;
                        }
        
                        oData.detail_product =array_detail_id_p;
                        
                        oData.methodPayment = $('input[name=payment_type]:checked').val();
                        oData.methodPayment = $('input[name=payment_type]:checked').val();
                        showLoading=1;

                        $.ajax({
                            url:url_createSalesOrder,                        
                            type:'POST',
                            dataType:'json',
                            data:oData,
                            async:false,
                            success:function(response){                    
                                if(evalResponse(response)){
                                    var data = response.data;
                                    var id = response.id;

                                    if(data.length>0){
                                        msm = '';
                                        $.each(data, function(key, value) { 
                                            msm = msm + ' Stock actual en el almacén <strong>' + value.almn + '</strong> del producto <strong>' + value.product+'</strong> = <strong>'+value.stock + '</strong><br/>';
                                        });
                                        msgBox(msm);
                                    }else{
                                    window.location.reload();
                                    }
//                                    else{
//                                        confirmBox  (orderSalesMsgConfirm,'Confirmar Impresión',function(e){
//                                            if(e){
//                                                var my_pop_up = window.open('/private/sales/getPrintSales?sales_id='+id.idsales+'&document='+id.document, shipping_document_printing);
//
//                                            }else{                                                
//                                            }
//                                            window.location.reload();
//                                        })                                                                                                                    
//                                    }
                                }
                            }
                        });                      
                    } 
                });
            }
        });
        
        fnLoadCheck();
    });
</script>

<div class="divTitle">

    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;"<?php endif ?>>
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4><?php echo __("REGISTRO DE RESERVA DE PRODUCTOS") ?></h4>
        </div>
    </div> 
    <?php if ($bAvailableDocumentType): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix">

        </div>
    <?php endif ?>
    <div class="clear"></div>
</div>
<!--Formulario General-->
<div class="divContent">
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="divContentPadding">
            <ul>
                <form id="frm_customers">
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Cliente"); ?></label>
                                </dd>
                                <dd class="relative">
                                    <div class="search_client_wrapper ui-corner-all ui-state-default">
                                        <span class="icon_sender_type ui-icon ui-icon-help"></span>
                                        <input class="autocompleteInput inputData" type="text" id="chars_Customers" name="chars_Customers" value="<?php echo 'Búsqueda de Cliente' ?>"/>
                                        <input type="hidden" id="idCustomers" name="idCustomers"/>
                                        <div class="clear"></div>
                                    </div>
                                    <button type="button" class="addCustomer"><?php echo __("Agregar Cliente") ?></button>
                                    <div id="auto_div_person_sender"></div>
                                    <div class="clear"></div>
                                </dd>
                            </dl>
                              <?php if($aSeller) :?>
                            <!--CODIGO USUARIO VENDEDOR-->
                                    <dl >
                                        <dd style="margin-top: 3px;">
                                            <label for="cmb_listUserCode"><?php echo __("Vendedor") ?>:</label>
                                        </dd>
                                        <dd>
                                            <select class="inputData" style="width: 150px"  size="1" id="cmb_listUserCode" name="cmb_listUserCode" rel="0" class="Combo" >
                                                <option value=""><?php echo __("Seleccionar Usuario") ?></option>
                                                <?php foreach ($listUserCode as $usercode) { ?>
                                                    <option value="<?php echo $usercode['id'] ?>"><?php echo $usercode['nameUser'] ?></option>
                                                <?php } ?> 
                                                                           
                                            </select>
                                        </dd>
                                    </dl>

                                <!--END CODIGO U-V-->
                          <?php endif ?>
                            <div class="clear"></div>
                        </div>                         
                        <div class="divLeft">
                            <div class="divLine divLineShort">
                                <dl>
                                    <dd>
                                        <label><?php echo __("Fecha de Reserva") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" name="orderSalesDate" id="orderSalesDate" type="text" value="<?php echo date($date_format_php) ?>"/>
                                    </dd>                               
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __("Fecha de Vencimiento") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" name="expirationDate" id="expirationDate" type="text" value="<?php echo date($date_format_php) ?>"/>
                                    </dd>                               
                                </dl>
                                <div class="clear"></div> 
                            </div>
                        </div>
                        <div class="divLeft" style="padding-left:20px;float: right;font-weight: bolder">
                            <dl>
                                <dd>
                                    <label><?php echo __("Almacén Asignado"); ?></label>                                
                                </dd>
                                <dd>                                
                                    <input id="select_store" name="select_store" value="<?php echo (count($store) != 0) ? $store[0]['display'] : 'ASIGNESE UN ALMACEN'; ?>" data="<?php echo (count($store) != 0) ? $store[0]['id'] : '0'; ?>" class="<?php echo (count($store) == 0) ? 'ui-state-error' : 'ui-state-highlight'; ?> ui-corner-all" readonly="" style="padding: 7px;font-weight: bolder;" />
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>

                    </li>
                </form>
                <br/>
                <li>
                    <hr class="hr_separate ui-widget-content"/>
                </li>
                <br/>
                <form class="orderForm" id="formSalesDetail" method="POST" action="#">
                    <li class="order">
                        <div class="divLine left">
                            <?php if ($bBarcodeReader): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label for="codigo" class="frmlblshort priceUnit"><?php echo __("Código") ?>:</label>

                                            <input id="codigo" name="codigo" class="subPriceLong priceUnit" type="text" />
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dd  style=" margin-top: 43px; ">
                                            <label for="descuento" class="frmlblshort priceUnit" ><?php echo __("Descuento") ?>:</label>

                                            <input id="descuento" class ="inputCustomPrice"name="descuento" type="text" value="0.0" />

                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                                <div class="divLine left">

                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" id="description" name="description" type="hidden"/>
                                        <input type="hidden" name="haveDetail" id="haveDetail"/>
                                        <input type="hidden" name="idproduct" id="idproduct"/>
                                        <input type="hidden" name="idstore" id="idstore"/>
                                        <input type="hidden" name="amount" id="amount"/>
                                    </dd>                               
                                    </dl>
                                    <div class="clear"></div>

                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="order">
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <label for="cb_quantity" class="frmlblshort priceUnit"><?php echo __("Cantidad") ?>:</label>

                                        <input id="cb_quantity" name="cb_quantity" class="subPrice priceUnit" type="text" value="1"/>
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputCustomPrice ui-state-highlight" type="hidden" id="custom_price" name="custom_price" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <?php if ($bTaxEnabled): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>

                                            <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="text" disabled />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>

                            <div class="clear"></div>
                        </li>
                    <?php else: ?>
                        <li class="order">
                            <div class="divLine left">
                                <div class="divLine divLineShort">
                                    <dl>
                                        <dd>
                                            <label for="description" class="frmlbl"><?php echo __("Descripción") ?>:</label>                    
                                        </dd>
                                        <dd>                
                                            <div class="clear"></div>
                                            <input class="inputData" id="description" name="description" type="text"/>
                                            <input type="hidden" name="haveDetail" id="haveDetail"/>
                                            <input type="hidden" name="idproduct" id="idproduct"/>
                                            <input type="hidden" name="idstore" id="idstore"/>
                                            <input type="hidden" name="amount" id="amount"/>
                                        </dd>                               
                                    </dl>
                                    <div class="clear"></div>

                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft">
                                <dl>
                                    <dd>
                                        <label for="quantity" class="labelprice"><?php echo __("Cantidad") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputQuantity" type="text" id="quantity" name="quantity" alt="number" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft">
                                <dl>
                                    <dd>
                                        <label for="quantity" class="labelprice"><?php echo __("Precio") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputCustomPrice ui-state-highlight" type="text" id="custom_price" name="custom_price" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                            <div class="divLeft addSales buttonadd">
                                <dl>
                                    <dd>
                                        <button type="button" id="addPackageDetail" class="ui-state-highlight"><?php echo __("Agregar") ?></button>
                                    </dd>
                                </dl>
                            </div>

                            <div class="clear"></div>
                        </li>
                        <li class="order">
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <label class="frmlblshort priceUnit"><?php echo __("Precio Total") ?>:</label>

                                        <input id="priceUnit" name="priceUnit" class="subPrice priceUnit" type="text" disabled/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <?php if ($bTaxEnabled): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>

                                            <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="text" disabled />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>
                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <label class="frmlblshort discountUnit"><?php echo __("Desc.") ?>:</label>

                                        <input id="discountUnit" name="discountUnit" class="subPrice discountUnit ui-state-active" type="text" />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <div class="subTotal">
                                <dl>
                                    <dd>
                                        <label class="frmlblshort priceTotal"><?php echo __("SubTotal") ?>:</label>

                                        <input id="priceTotal" name="priceTotal" class="subPrice priceTotal ui-state-hover" type="text" disabled />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>                           

                            <div class="clear"></div>
                        </li>
                    <?php endif; ?>
                    <li>
                        <div class="clear"></div>
                        <?php if ($bBarcodeReader): ?>
                            <table id="bc_salesDetail_jqGrid"></table>
                        <?php else: ?>
                            <table id="reserveDetail_jqGrid"></table>
                        <?php endif; ?>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="divLine divLineShort"  style="margin-top: 9px; text-align: right; width: 841px;">
                            <label><?php echo __("Monto a Cuenta"); ?>:</label>
                            <input class="amountPaid ui-state-highlight"  id="amountPaid" name="amountPaid" value="" type="text" alt="number"/>
                        </div>
                    </li>
                    <li>
                        <div class="clear"></div>
                        </br>
                        <button id="saveReserve" name="saveReserve" type="button" class="ui-state-error"><?php echo __("Guardar") ?></button>


                        <button id="cancelSales" name ="cancelSales"  type="button" class="ui-state-error"><?php echo __("Cancelar") ?></button>
                        <div class="clear"></div>
                    </li>
                </form>
            </ul>
        </div>
    </div>
</div>
<!--Formulario de Clientes-->
<div id="div_register_client" class="ui-widget ui-widget-content ui-corner-all none">
    <div class="arrow_left"></div>
    <div class="padding10">
        <form id="formRegisterClient" action="#" method="POST" class="frm">
            <input type="submit" class="none"/>
            <h4 class="register_client_title ui-widget-header ui-corner-all">
                <?php echo __("Cliente") ?>
                <b class="ui_icon_close_register_client ui-state-highlight ui-corner-all" role="button">
                    <span class="ui-corner-all ui-icon ui-icon-close register_client_close"></span>
                    <span class="register_client_text"><?php echo __("Cerrar") ?></span>
                </b>
            </h4>
            <ul>            
                <li>
                    <div class="divLine">
                        <dl>
                            <dd><label><?php echo __("Tipo Cliente") ?>:</label></dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div>
                                <input id="id_customers" name="id_customers" type="hidden"/>
                                <?php foreach ($a_clients_type as $a_client): ?>
                                    <?php echo Helper::fnGetDrawJqueryButton($a_client, 'typeClient') ?>                                
                                <?php endforeach ?>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_name" class="frmlbl"><?php echo __("Razón Social") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_name" name="company_name" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_code" class="frmlbl"><?php echo __("RUC") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_code" name="company_code" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerCompany">
                    <div class="divLine">
                        <dl>
                            <dd><label for="company_contact" class="frmlbl"><?php echo __("Contacto") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="company_contact" name="company_contact" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Nombre") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="passanger_name" name="passanger_name" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Apellido") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog required" id="passanger_lastname" name="passanger_lastname" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Sexo") ?>:</label>                    
                            </dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div>
                                <input type="radio" id="sexM" name="passanger_sexo" value="M"  checked  />
                                <label for="sexM"><?php echo __("M") ?></label>
                                <input type="radio" id="sexF" name="passanger_sexo" value="F" />
                                <label for="sexF"><?php echo __("F") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Tipo Documento") ?>:</label>                        
                            </dd>
                            <dd class="buttonsContainer">
                                <input type='radio' 
                                       id='passanger_tdocumento' name='passanger_tdocumento' value="DNI" checked="true" >
                                <label for='passanger_tdocumento'>DNI</label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson">
                    <div class="divLine">
                        <dl>
                            <dd><label class="frmlbl"><?php echo __("Documento") ?>:</label></dd>
                            <dd>
                                <input class="input_dialog" id="passanger_documento" name="passanger_documento" value="" type="text"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine div_buttons">
                        <button id="btn_save_client" type="submit"><?php echo __('Listo') ?></button>                        
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            <ul class="div_passanger_dir">
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("E-mail") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_mail" name="passanger_mail" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Dirección ") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_dir1" name="passanger_dir1" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <!-- Para determinar P.Geografica -->
                <li class="containerPerson">
                    <div class="divLine"> 
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Departamento") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_dpto" id="passanger_dpto" >
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>
                                    <?php foreach ($a_dpto as $a_co): ?>
                                        <option value="<?php echo $a_co->iddpto; ?>"> <?php echo $a_co->descripcion; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li  class="containerPerson"> 
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Provincia") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_prov" id="passanger_prov">
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li class="containerPerson" >
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Distrito") ?> :</label>
                            </dd>
                            <dd>
                                <select name="passanger_dst" id="passanger_dst">
                                    <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                </select>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Teléfono") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_phone" name="passanger_phone" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label class="frmlbl"><?php echo __("Celular") ?> :</label>
                            </dd>
                            <dd>
                                <input class="input_dialog" id="passanger_cellphone" name="passanger_cellphone" type="text"/>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div>
                </li>
            </ul>
            <div class="clear"></div>


        </form>
    </div> 
</div>

<div id="frmTypePay" class="dialog">
    <form id="postformConfirmPayment" method="post">
        <fieldset class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle">Pago de la Reserva</legend>
            <div>
                <div class=" ui-corner-all cFieldSet ui-widget-content padding5 infoReserva">
                    <label class="lblInfoReserva"><?php echo __("PAGO DE LA RESERVA: "); ?></label>
                    <div class="clear"></div>
                    
                    <label class="lblInfoReserva">Monto Total:</label>
                    <div class="post_confirm_payment_total_amount"></div> 
                    <label class="lblInfoReserva">A cuenta:</label>
                    <div class="post_confirm_payment_total"></div>
                    <label class="lblInfoReserva">Saldo:</label>
                    <div class="post_confirm_payment_return_balance"></div>
                </div>
            </div>
            <?php if ($methodPayment) : ?>
                <div class="divLine" style="text-align: center;">
                    <dl>
                        <dd>
                            <label for="chars_Customers"><?php echo __("Tipos de Pago"); ?></label>
                        </dd>
                        <dd class="buttonsContainer">
                            <div class="clear"></div> 
                            <?php foreach ($salesPayment as $sKey => $aSalesPayment): ?>                                            
                                <?php echo Helper::fnGetDrawJqueryButton($aSalesPayment, 'payment_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'cardType', Rms_Constants::ARRAY_KEY_VALUE => __($aSalesPayment['display'])))) ?>                                            
                            <?php endforeach ?>
                        </dd>
                    </dl>
                </div>
            <?php endif ?>
            <div class="clear"></div>
            <!-- Pogo con Tarjeta -->
            <div id="frmCreditCard">
                <form id="frmPayment" method="post">
                <div class="label_output">
                    <div>
                        <label><?php echo __("Tarjeta"); ?>:</label>
                        <select name="typeCard" id="typeCard">
                            <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                            <?php foreach ($creditCardPayment as $creditcard) {
                                ?>
                                <option value="<?php echo $creditcard['value']; ?>"><?php echo $creditcard['display']; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                    <div>
                        <label><?php echo __("Nro de Tarjeta"); ?>:</label>
                        <input id="cardNumber" name="cardNumber" value="" type="text" maxlength="150" size="30" alt="number"/>
                    </div>
                    <div>
                        <label><?php echo __("Monto"); ?>:</label>
                        <input id="amount_paymentCredit" name="amount_paymentCredit" value="" type="text" maxlength="150" size="30" readonly="true"/>
                    </div>
                    <div>
                        <label><?php echo __("Nro de Operación"); ?>:</label>
                        <input id="operationNumber" name="operationNumber" value="" type="text" maxlength="150" size="30" alt="number"/>
                    </div>
                </div>
            </div>

            <!-- Pago efectivo -->
            <div id="frmCreditCash">
                <!--Monto Calculado-->
                <div class="rowLine">
                    <div class="rowLine">
                        <div class="lPayment">
                            <?php echo __("Total") ?>
                        </div>
                        <div class="lSymbol">
                            <div class="ui-widget post_display_prices ui-state-active" >                
                                <?php echo "S/." ?>
                            </div>
                        </div>
                        <div class="post_confirm_payment_total_on_account">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="rowLine">
                        <div class="lPayment">
                            <?php echo __("Pago") ?>
                        </div>
                        <div>
                            <div class="lSymbol">
                                <div class="ui-widget post_display_prices ui-state-active" >                
                                    <?php echo "S/." ?>
                                </div>
                            </div>
                            <input name="post_confirm_payment" id="post_confirm_payment" class="ui-corner-tr ui-corner-br" maxlength="10"/>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div class="rowLine">
                        <div class="lPayment">
                            <?php echo __("Vuelto") ?>
                        </div>
                        <div class="lSymbol">
                            <div class="ui-widget post_display_prices ui-state-active" >                
                                <?php echo "S/." ?>
                            </div>
                        </div>
                        <div class="post_confirm_payment_return_cash">
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>   
                <div class="clear"></div>
            </div>   

            <!--Pago Mixto-->

        </fieldset>
    </form>

</div>
