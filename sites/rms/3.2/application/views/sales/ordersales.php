<style type="text/css">
    .clasfont {
        font-size: 10px;
    }
    .div_new_product{
        margin-left: auto;
        margin-right: auto;
        width: 1000px;
    }
    .div_grid_product{        
        padding-top: 10px;
    }
    div.ui-dialog[aria-labelledby=ui-dialog-title-dlg_salesorder_detail] .ui-dialog-titlebar{display: none;}
    #dlg_salesorder_detail ul{position: relative;}
    #dlg_salesorder_detail ul li{padding: 0 0 5px 0;position: relative;}
    #dlg_salesorder_detail label, #dlg_salesorder label{font-weight: bold;}

    #dlg_salesorder_detail dd, #dlg_salesorder dd{padding-left: 10px;}
    #dlg_salesorder_detail dd,#dlg_salesorder_detail dt, #dlg_salesorder dd, #dlg_salesorder dt{padding-top: 2px;padding-bottom: 2px;line-height: 16px;     width: auto;}
    .divLeft{float: left; width: 100px;}
    .divRigth{float: right; width: 80px;}
    .right{float: right;}
    #quantitydet, #custom_pricedet,#discountUnitdet,#priceTotaldet,
    #quantity, #custom_price, #amountPaid {width: 50px;text-align: right;margin-right: 10px;}
    input[type="text"], input[type="password"], textarea, select {
        padding: 5px;
    }
    .textStock, .textStockRes, .textStockAct{font-size:16px; text-transform: uppercase;line-height: 30px; text-align: center; font-weight: bold;}
    input.inputData{width: 270px;}
    .subTotal{float: left;}
    label.frmlblshort{float: left;font-weight: bold;width: 97px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    #addPackageDetail{margin: 5px 0;}
    .buttonadd{margin-top: 15px;}
    input.subPrice{width: 200px; height: 19px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    .ui-widget input.subPrice {font-size: 18px;background-image: none; width: 90px;}
    .labelprice {
        display: block;
        float: left;
        font-weight: bold;
        height: 18px;
        line-height: 20px;
        width: 100px;
        padding-right: 10px;
        text-align: right;
    }
    .ui-jqgrid tr.jqgrow td {white-space: normal; padding: 3px;}
    .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column{
        white-space: normal;
    }
    .ui-jqgrid .ui-jqgrid-htable th div { height: 35px;}
    /*input.inputQuantity, input.inputCustomPrice{width: 50px;}*/
    label.frmLabel {
        float: left;
        font-weight: bold;
        line-height: 26px;
        width: 150px;
        text-align: right;
        margin-right: 16px;
    }
    .frmLabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 16px;
        padding-top: 5px;
        width: 200px;
    }
    #search_type>div{
        padding-top:5px;
    }
    .search_client_wrapper span{margin: 3px 4px;}
    .ui-widget-content .search_client_wrapper{background-image: none;margin-right: 5px;}
    .search_client_wrapper,.search_client_wrapper span,
    .search_client_wrapper input{float: left;}
    .ui-widget-content .search_client_wrapper input{ 
        width: 245px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    input.inputData{width: 270px;}
    hr.hr_separate{
        border-top-width: 3px;
        border-top-style: dashed;
        border-bottom: none;
        border-left: none;
        border-right: none;
        margin-top: 16px;
    }

    /* CSS History */
    .recovered, .share, .sold, .expired, .canceled, .reserved {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 13px;
        border: 1px solid;
        margin: 10px 0px;
        padding: 5px 10px 16px 14px;
        background-repeat: no-repeat;
        background-position: 10px center;
    }
    .reserved {
        color: rgb(172, 130, 24);
        background-color: rgb(255, 217, 121);
        background-image: url('/media/images/reservado.jpg');
        background-size: 60px 55px;
        background-position: 195px 4px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        border-top-width: 8px;
        font-weight: bolder;    
    }
    .recovered {
        color: rgb(50, 148, 20);
        background-color: rgb(179, 224, 138);
        background-image: url('/media/images/recovered.jpg');
        background-size: 60px 55px;
        background-position: 195px 6px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        border-top-width: 8px;
        font-weight: bolder;    
    }
    .share {
        color: rgb(46, 31, 153);
        background-color: rgb(121, 185, 255);
        background-image: url('/media/images/shares.jpg');
        background-size: 60px 55px;
        background-position: 195px 6px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        border-top-width: 8px;
        font-weight: bolder;    
    }
    .sold {
        color: rgb(8, 90, 38);
        background-color: rgb(102, 202, 106);
        background-image: url('/media/images/people.png');
        background-position: 212px 6px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        background-size: 60px 55px;
        font-weight: bolder;  
    }
    .expired {
        color: rgb(139, 29, 131);
        background-color: rgb(211, 174, 231);
        background-image: url('/media/images/expired.jpg');
        background-position: 196px 3px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        border-top-width: 8px;
        background-size: 60px 55px;
        font-weight: bolder;  
    }
    .canceled {
        color: rgb(223, 44, 44);
        background-color: rgb(230, 176, 171);
        background-image: url('/media/images/cancel.jpg');
        background-position: 195px 5px;
        border-bottom-right-radius: 35px;
        font-size: 12px;
        border-bottom-style: dashed;
        border-top-width: 8px;
        background-size: 60px 55px;
        font-weight: bolder;  
    }
    #btn_search{
        float: right;
    }
    #totalAmount, #amountToPaid {background: none;border: none; line-height: 21px;}
    .tbr_group, .tbm_group{
        margin-bottom: 10px;
    }
</style>
<script type="text/javascript">
    var url_salesOrder = '/private/ordersales/getListSalesOrder' + jquery_params;
    var url_subsalesOrder = '/private/ordersales/getDetalleSalesOrder' + jquery_params;
    var url_updateStatusSalesOrder = '/private/ordersales/updateStatusSalesOrder' + jquery_params;
    var url_UpdateSalesOrderDetailById = '/private/ordersales/createUpdateSalesOrderDetail' + jquery_params;
    var url_editSalesDetail = '/private/ordersales/getSalesOrderForDetail' + jquery_params;
    var url_updateSalesDetail = '/private/ordersales/createUpdateDetail' + jquery_params;
    var url_GetProduct = '/private/orderform/getProduct' + jquery_params;
    var url_detailSalesOrder = '/private/ordersales/getSalesOrder' + jquery_params;
    var url_EditOrder = '/private/ordersales/createUpdateOrderSales' + jquery_params;
    var url_GetClients = '/private/sales/getClients' + jquery_params;
    var url_listHistorySalesOrder = '/private/ordersales/listHistorySalesOrder' + jquery_params;
    var url_listHistoryShares = '/private/ordersales/listHistoryShares' + jquery_params;
    var url_createShares = '/private/ordersales/createShares' + jquery_params;
    var url_deleteShares = '/private/ordersales/deleteShares' + jquery_params;
    /* Mensajes */
    var sTxtSearchClient = '<?php echo __('Búsqueda de Cliente') ?>';
    var conftrash = '<?php echo __("¿Está seguro que desea eliminar la Reserva?"); ?>';
    var confrecovered = '<?php echo __("¿Está seguro que desea recuperar la Reserva?"); ?>';
    var titletrash = '<?php echo __("Eliminar Reserva"); ?>';
    var titlerecovered = '<?php echo __("Recuperar Reserva"); ?>';

    var arrayDetails = new Object();
    var edit_product = 0;
    var countarray = 0;
    var keycountarray = 0;
    var iCountItem = 1;
    var aIDs;
    var arrayShareSalesOrder = new Array();

    $(document).on("ready", function() {

        /* Set Mask */
        $('#quantitydet, #quantity').setMask();
        /* Price Format */
        $('#custom_pricedet, #amountPaid, #priceTotaldet, #discountUnitdet, #custom_price,#priceTotal, #discountUnit').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* Buttons */
        $('#addPackageDetail').button({
            icons: {
                primary: 'ui-icon-plus'
            }
        });
        /* KeyUp */
        $('#amountPaid').keyup(function(key) {
            calculationAmount();
        });

        /* Funciones */
        calculationAmount = function() {
            amountTotal = $('#totalAmount').val();
            amount = $('#amountPaid').val().split(' ').join('');
            retorna = parseFloat(amountTotal) - parseFloat(amount);
            $('#amountToPaid').val(retorna.toFixed(2));
        };
    /* JQgrid de Reservas */
    var grid_list_ordersales = $("#grid_list_ordersales").jqGrid({
        url: url_salesOrder,
        datatype: 'json',
        mtype: 'POST',
        colNames: [
            '<?php echo __("ID") ?>',
            '<?php echo __("Documento") ?>',
            '<?php echo __("Cliente") ?>',
            '<?php echo __("Estado") ?>',
            '<?php echo __("Fecha Reserva") ?>',
            '<?php echo __("Fecha Vencimiento") ?>',
            '<?php echo __("Importe Venta") ?>',
            '<?php echo __("Monto a Cuenta") ?>',
            '<?php echo __("Saldo") ?>',
            '<?php echo __("Acciones") ?>'
        ],
        colModel: [
            {name: 'idordersales', index: 'idordersales', hidden: true, key: true},
            {name: 'document', index: 'document', width: 80, align: "center"},
            {name: 'customers', index: 'customers', width: 200},
            {name: 'status', index: 'status', width: 80, align: "center"},
            {name: 'orderSalesDate', index: 'orderSalesDate', width: 120, align: "center"},
            {name: 'expirationDate', index: 'expirationDate', width: 100, align: "center"},
            {name: 'totalAmount', index: 'totalAmount', width: 50, align: "center"},
            {name: 'amountPaid', index: 'amountPaid', width: 50, align: "center"},
            {name: 'amountToPaid', index: 'amountToPaid', width: 50, align: "center"},
            {name: 'actions', index: 'actions', width: 120, align: "center"}
        ],
    pager: '#grid_list_ordersales_pager',
    sortname: 'so.orderSalesDate',
    rowNum: 12,
    sortorder: "desc",
    width: 1020,
    height: 'auto',
    rownumbers: true,
    rownumWidth: 20,
    viewrecords: true,
    caption: '<?php echo __("Lista de Reservas"); ?>',
    subGrid: true,
    subGridOptions: {
        plusicon: "ui-icon-plus",
        minusicon: "ui-icon-minus",
        openicon: "ui-icon-carat-1-sw"
    },
    subGridRowExpanded: function(subgrid_id, row_id) {
        var rd = $("#grid_list_ordersales").getRowData(row_id);
        var subgrid_ordersales, subgrid_ordersales_pager;
        subgrid_ordersales = subgrid_id + "_t";
        subgrid_ordersales_pager = "p_" + subgrid_ordersales;
        $("#" + subgrid_id).html("<table id='" + subgrid_ordersales + "' class='scroll'></table><div id='" + subgrid_ordersales_pager + "' class='scroll'></div>");
        $("#" + subgrid_ordersales).jqGrid({
            url: url_subsalesOrder,
            datatype: "jsonp",
            mtype: 'POST',
            postData: {
                idSal: function() {
                    return row_id;
                }
            },
            colNames: [
                'idSalesOrderDetail',
                'idSalesOrder',
                '<?php echo __("Producto") ?>',
                '<?php echo __("Almacén") ?>',
                '<?php echo __("Cantidad") ?>',
                '<?php echo __("Precio Unit.") ?>',
                '<?php echo __("Descuento") ?>',
                '<?php echo __("Precio Total") ?>'
            ],
            colModel: [
                {name: "idSalesOrderDetail", index: "idSalesOrderDetail", hidden: true},
                {name: "idSalesOrder", index: "idSalesOrder", hidden: true},
                {name: "product", index: "product", width: 300},
                {name: "storeName", index: "storeName", width: 150},
                {name: "quantity", index: "quantity", align: "center", width: 100},
                {name: "unitPrice", index: "unitPrice", align: "right", width: 150},
                {name: "discount", index: "discount", width: 75, align: "right", width: 100},
                {name: "totalPrice", index: "totalPrice", width: 75, align: "right", width:100}
            ],
            rowNum: 20,
            pager: subgrid_ordersales_pager,
            sortname: 'product',
            sortorder: "asc",
            width: 900,
            rownumbers: true

        });
        $("#" + subgrid_ordersales).jqGrid('navGrid', "#" + subgrid_ordersales_pager, {edit: false, add: false, del: false, search: false, refresh: false});
    }
    , afterInsertRow: function(rowid, rowdata, rowelem) {
        cancel = "<a class=\"cancel\" style=\"cursor: pointer;\" rel=\"" + rowid + "\" title=\"<?php echo __('Anular'); ?>\" ><?php echo __('Anular'); ?></a>";
        undo = "<a class=\"undo\" style=\"cursor: pointer;\" rel=\"" + rowid + "\"  status =\"" + rowdata.status + "\" title=\"<?php echo __('Recuperar'); ?>\" ><?php echo __('Recuperar'); ?></a>";
        shares = "<a class=\"shares\" style=\"cursor: pointer;\" rel=\"" + rowid + "\" total=\"" + rowdata.amountToPaid + "\" title=\"<?php echo __('Cuotas'); ?>\" ><?php echo __('Cuotas'); ?></a>";
        var history = "<a class=\"history\" style=\"cursor: pointer;\" rel=\"" + rowid + "\" title=\"<?php echo __('Historial'); ?>\" ><?php echo __('Historial'); ?></a>";
        var hShares = "<a class=\"hShares\" style=\"cursor: pointer;\" rel=\"" + rowid + "\" title=\"<?php echo __('Historial Cuotas'); ?>\" ><?php echo __('Historial Cuotas'); ?></a>";
        options = '';
        switch (rowdata.status) {
            case 'Reservado':
                options = cancel + shares + history + hShares;
                break;
            case 'Expirado':
                $('#grid_list_ordersales tr#' + rowid).addClass('ui-state-active');
                options = undo + history + hShares;
                break;
            case 'Cancelado':
                $('#grid_list_ordersales tr#' + rowid).addClass('ui-state-error');
                options = history + hShares;
                break;
            default:
                break;

        }
        $("#grid_list_ordersales").jqGrid('setRowData', rowid, {actions: options});
    }
    , gridComplete: function() {
        /* Recuperar */
        $('.undo').button({
            icons: {primary: 'ui-icon ui-icon-arrowreturnthick-1-s'},
            text: false
        })
        .click(function() {
            var idl = $(this).attr('rel');
            var actual =$(this).attr('status');
            var status = '<?php echo Rms_Constants::SALES_ORDER_STATUS_RESERVED ?>';
            confirmBox(confrecovered, titlerecovered, function(event) {
                if (event) {
                    $('#idReservaRecovered').val(idl);
                    $('#statusRecovered').val(status);
                    if(actual == 'Expirado'){             
                        $('#dlg_RecoveredOrderSales').dialog('open');
                    }
                }
            });
        });
        /* Anular Reserva */
        $('.cancel').button({
            icons: {primary: 'ui-icon-cancel'},
            text: false
        }).click(function() {
            var idl = $(this).attr('rel');
            var status = '<?php echo Rms_Constants::SALES_ORDER_STATUS_CANCELED ?>';
            confirmBox(conftrash, titletrash, function(event) {
                if (event) {
                    $.ajax({
                        url: url_updateStatusSalesOrder,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            id: idl,
                            status: status
                        },
                        beforeSend: function() {
                            $("#loading").dialog("open");
                        },
                        complete: function() {
                            $("#loading").dialog("close");
                        },
                        success: function(response) {
                            if (evalResponse(response)) {
                                grid_list_ordersales.trigger("reloadGrid");
                                msgBox('Reserva Anulada');
                            } else {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
            });
        });
        /*Historial de Cuotas*/
        $('.hShares').button({
            icons: {primary: 'ui-icon-folder-open'},
            text: false    
        })
        .click(function() {
            $('#hShares_content').empty();
            var idl = $(this).attr('rel');
            $.ajax({
                url: url_listHistoryShares,
                type: 'POST',
                dataType: 'json',
                data: {
                    id: idl
                },
                success: function(response) {
                    if (evalResponse(response)) {
                        var a_history = response.data;
                        if (a_history.length > 0) {
                            $.each(a_history, function(index, data) {
                                $('#hShares_content').append("<div class='share'>\n\
                                                <dl><dd><input class='cheShare' type='checkbox' name='check_qwerty[]' value=" +data.id+" /></dd>\n\
                                                <dd><label class='label_text'>Estado:</label>Cuota</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.name + "</dd>\n\
                                                <dd><label class='label_text'>Cuota:</label>" + data.cuota + "</dd>\n\
                                                </dl></div><div class='clear'></div>");
    
                                //                                                var dl = $(document.createElement("tr")).addClass("share");
                                //                                                dl.append($(document.createElement("td")).text(data.idTask));
                                //                                                dl.append($(document.createElement("td")).text(data.title));
                                //                                                dl.append($(document.createElement("td")).text(data.codetask));
                                //                                                dl.append($(document.createElement("td")).text(data.status));

                            });
                            $('#dlg_History_Shares').dialog('open');
                        }
                        else {
                            //GG - No tiene historial 
                            $('#history_content').append("<label>No se guardo historial</label>");
                            $('#dlg_History').dialog('open');
                        }
                    } else {
                        msgBox(response.msg, response.code);
                    }
                }
            });
        });
           
        /* Historial Reserva */
        $('.history').button({
            icons: {primary: 'ui-icon-note'},
            text: false
        })
        .click(function() {
            var idl = $(this).attr('rel');
            $.ajax({
                url: url_listHistorySalesOrder,
                type: 'POST',
                dataType: 'json',
                data: {
                    id: idl
                },
                success: function(response) {
                    if (evalResponse(response)) {
                        var a_history = response.data;
                        if (a_history.length > 0) {
                            $.each(a_history, function(index, data) {
                                switch (data.status) {
                                    case 'RESERVED':
                                        $('#history_content').append("<div class='reserved'><dl><dd><label class='label_text'>Estado:</label>Reservado</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.fullName + "</dd></dl></div><div class='clear'></div>");
                                        break;
                                    case 'RECOVERED':
                                        $('#history_content').append("<div class='recovered'><dl><dd><label class='label_text'>Estado:</label>Recuperado</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.fullName + "</dd></dl></div><div class='clear'></div>");
                                        break;
                                    case 'CANCELED':
                                        $('#history_content').append("<div class='canceled'><dl><dd><label class='label_text'>Estado:</label>Cancelado</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.fullName + "</dd></dl></div><div class='clear'></div>");
                                        break;
                                    case 'SOLD':
                                        $('#history_content').append("<div class='sold'><dl><dd><label class='label_text'>Estado:</label>Vendido</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.fullName + "</dd></dl></div><div class='clear'></div>");
                                        break;
                                    case 'EXPIRED':
                                        $('#history_content').append("<div class='expired'><dl><dd><label class='label_text'>Estado:</label>Expirado</dd>\n\
                                                <dd><label class='label_text'>Fecha:</label>" + data.date + "</dd>\n\
                                                <dd><label class='label_text'>Usuario:</label>" + data.fullName + "</dd></dl></div><div class='clear'></div>");
                                        break;
                                    default:
                                        break;

                                }
                            });
                            $('#dlg_History').dialog('open');
                        }
                        else {
                            //GG - No tiene historial 
                            $('#history_content').append("<label>No se guardo historial</label>");
                            $('#dlg_History').dialog('open');
                        }
                    } else {
                        msgBox(resp.msg, resp.code);
                    }
                }
            });
        });
        /* Agregar cuota */
        $('.shares').button({
            icons: {primary: 'ui-icon-document-b'},
            text: false
        }).click(function() {
            var idSalesOrder = $(this).attr('rel');
            var total = $(this).attr('total');
            $("#totalAmount").val(total);
            $("#idSalesOrder").val(idSalesOrder);
            $("#dlg_AddShare").dialog('open');
        });
    }
    });
    /* Dialog del Recuperacion de Reserva */
    $('#dlg_RecoveredOrderSales').dialog({
        title: '<?php echo __("Recuperar Reserva") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 400,
        modal: true,
        buttons: {
            "<?php echo __("Guardar") ?>": function() {               
                $('#frmRecoveredOrderSales').submit();
            },
            "<?php echo __("Cancelar") ?>": function() {
                $(this).dialog("close");
                $('#txtFechaVencimiento').val('');
            }
        }
    });
    
    $('#frmRecoveredOrderSales').validate({
        rules: {
            txtFechaVencimiento: {
                required: true
            }
        },
        messages: {
            txtFechaVencimiento: {
                required: '<?php echo __("Ingresa Fecha de Vencimiento") ?>'
            }
        },
        errorContainer: '#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {
            $(element).addClass('ui-state-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('ui-state-error');
        },
        submitHandler: function(form) {
            $.ajax({
                 url: url_updateStatusSalesOrder,
                 type: 'POST',
                 dataType: 'json',
                 data: {
                     id: $('#idReservaRecovered').val(),
                     status: $('#statusRecovered').val(),
                     txtFechaVencimiento: $('#txtFechaVencimiento').val()
                 },
                 success: function(response) {
                     if (evalResponse(response)) {
                         grid_list_ordersales.trigger("reloadGrid");
                         msgBox('Reserva Recuperada');
                         $('#dlg_RecoveredOrderSales').dialog('close');
                     } else {
                         msgBox(response.msg, response.code);
                     }
                 }
           });
        }
    });
    
    /* Dialog del Historial */
    $('#dlg_History').dialog({
        title: '<?php echo __("Historial de la Reserva") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 300,
        modal: true,
        buttons: {
            "<?php echo __("Salir") ?>": function() {
                $(this).dialog("close");
                // $('#history_content').remove();
                $('#history_content').empty();
            }
        }
    });
    //Evento click Checked_shares
    $("#hShares_content").on('click','.cheShare',function(){
        //  console.log($(this).val());
        //            arrayShareSalesOrder
        if($(this).is(':checked')) {                   
            arrayShareSalesOrder.push($(this).attr("value"));   
        } else {                
            var pos = arrayShareSalesOrder.indexOf($(this).attr("value"));
            pos > -1 && arrayShareSalesOrder.splice(pos,1);
        }
    }); 
    /* Dialog del Historial de Cuotas */
    $('#dlg_History_Shares').dialog({
        title: '<?php echo __("Historial de Cuotas") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 300,
        modal: true,
        buttons: {
            "<?php echo __("Eliminar") ?>": function() {
                if(arrayShareSalesOrder.length > 0){
                    confirmBox('Seguro(a) de eliminar la(s) cuota(s)','Confirmar Eliminacion',function(event){
                         if(event){
                             console.log(arrayShareSalesOrder.length);
                            if(arrayShareSalesOrder.length > 0){

                                       $.ajax({
                                               url: url_deleteShares,
                                               type: 'POST',
                                               dataType: 'json',
                                               data: {arrayShares: arrayShareSalesOrder},
                                                beforeSend: function() {
                                                    $("#loading").dialog("open");
                                                },
                                                complete: function() {
                                                    $("#loading").dialog("close");
                                                },
                                                success: function(response) {
                                                    if (evalResponse(response)) {                                                    
                                                        msgBox('Cuota(s) Eliminada(s)');
                                                        $('#dlg_History_Shares').dialog("close");
                                                        grid_list_ordersales.trigger("reloadGrid");
                                                    } else {
                                                        msgBox(response.msg, response.code);
                                                    }
                                                }
                                           });                                   
                               }               
                         }
                    });  
                }              
            },
            "<?php echo __("Salir") ?>": function() {
                $(this).dialog("close");
                // $('#history_content').remove();
                $('#hShares_content').empty();
                    arrayShareSalesOrder = new Array();
            }
        }
    });
    /* Dialog de la Cuota */
    $('#dlg_AddShare').dialog({
        title: '<?php echo __("Agregar Nueva Cuota") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 300,
        modal: true,
        buttons: {
            "<?php echo __("Guardar") ?>": function() {
                $("#frmAddShare").submit();
            },
            "<?php echo __("Cancelar") ?>": function() {
                $(this).dialog("close");
                frmAddShare.currentForm.reset();
            }
        }
    });
    $.validator.addMethod("validPayment", function(value, element, params) {
        return parseFloat(value) < parseFloat($('#totalAmount').val()) ? true : false;
    }, "");
    frmAddShare = $('#frmAddShare').validate({
        rules: {
            amountPaid: {
                required: true,
                validPayment: true
            }
        },
        messages: {
            amountPaid: {
                required: '<?php echo __("Cantidad es Requerida") ?>',
                validPayment: '<?php echo __("Debe ser menor al monto total") ?>'
            }
        },
        errorContainer: '#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {
            $(element).addClass('ui-state-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('ui-state-error');
        },
        submitHandler: function(form) {

            $.ajax({
                url: url_createShares,
                type: 'POST',
                dataType: 'json',
                data: {
                    obj_shares: $('#frmAddShare').serializeObject()
                },
                success: function(response) {
                    if (evalResponse(response)) {
                        frmAddShare.currentForm.reset();
                        $('#dlg_AddShare').dialog('close');
                        grid_list_ordersales.trigger("reloadGrid");
                    } else {
                        msgBox(resp.msg, resp.code);
                        //GG :(
                    }
                }
            });
        }
    }); 
           

    function returnarray(idproduc, idstore) {

        $.each(arrayDetails, function(key, value) {

            if ($('#idproduct').val() == value.product_id && $('#idstore').val() == value.store_id) {
                countarray = 1;
                keycountarray = key;
                quantity = value.quantity;
            }
        });
    }

    $("#addPackageDetail").click(function() {
        if (!$("#formSalesDetail").valid())
            return false;
        //Agrego detalle
        var itemDetail = new Object();
        itemDetail.description = $('#description').val();
        itemDetail.quantity = $('#quantity').val();
        itemDetail.priceUnit = $('#custom_price').val();
        itemDetail.priceTotal = $('#priceTotal').val();
        itemDetail.amount = $('#amount').val();
        if ($('#discountUnit').val() == '') {
            itemDetail.discount = 0;
        } else {
            itemDetail.discount = $('#discountUnit').val();
        }
        itemDetail.product_id = $('#idproduct').val().split(' ').join('');
        itemDetail.store_id = $('#idstore').val().split(' ').join('');
        itemDetail.product_id = $('#idproduct').val().split(' ').join('');

        total = returnarray(itemDetail.product_id, itemDetail.store_id);

        //                if(edit_product==1){
        //                    itemDetail.exist = arrayDetails[keycountarray].exist;
        //                    itemDetail.remove =arrayDetails[keycountarray].remove;
        //                    itemDetail.idSalesOrderDetail =arrayDetails[keycountarray].idSalesOrderDetail;
        //                    arrayDetails[keycountarray] = itemDetail;
        //    
        //                    tblSalesDetail.setRowData(keycountarray, {
        //                        descriptionOut:itemDetail.description,
        //                        quantityOut:itemDetail.quantity,
        //                        unitPriceOut:itemDetail.priceUnit,                    
        //                        discountOut:parseFloat(itemDetail.discount).toFixed(2),                    
        //                        subTotalOut:itemDetail.priceTotal,
        //                        idProductOut:itemDetail.product_id,
        //                        idStoreOut:itemDetail.store_id
        //                        
        //                    });
        //  
        //                    edit_product = 0;
        //                    keycountarray = 0;
        //                }else 
        if (countarray == 0 && keycountarray == 0) {
            itemDetail.exist = 0;
            itemDetail.remove = 0;
            itemDetail.idSalesOrderDetail = 0;
            arrayDetails[iCountItem] = itemDetail;
            tblSalesDetail.addRowData(iCountItem, {
                descriptionOut: itemDetail.description,
                quantityOut: itemDetail.quantity,
                unitPriceOut: itemDetail.priceUnit,
                discountOut: parseFloat(itemDetail.discount).toFixed(2),
                subTotalOut: itemDetail.priceTotal,
                idProductOut: itemDetail.product_id,
                idStoreOut: itemDetail.store_id
            });

            iCountItem++;
        } else {
            arrayDetails[keycountarray].quantity = parseInt(arrayDetails[keycountarray].quantity) + parseInt(itemDetail.quantity);
            arrayDetails[keycountarray].priceTotal = (parseFloat(arrayDetails[keycountarray].priceTotal) + parseFloat(itemDetail.priceTotal)).toFixed(2);
            arrayDetails[keycountarray].discount = (parseFloat(arrayDetails[keycountarray].discount) + parseFloat(itemDetail.discount)).toFixed(2);

            tblSalesDetail.setRowData(keycountarray, {
                descriptionOut: itemDetail.description,
                quantityOut: arrayDetails[keycountarray].quantity,
                unitPriceOut: parseFloat(itemDetail.priceUnit).toFixed(2),
                discountOut: parseFloat(arrayDetails[keycountarray].discount).toFixed(2),
                subTotalOut: arrayDetails[keycountarray].priceTotal,
                idProductOut: itemDetail.product_id,
                idStoreOut: itemDetail.store_id
            });

            countarray = 0;
            keycountarray = 0;
        }
        console.log(arrayDetails);
        tblSalesDetail.trigger("reloadGrid");
        $('#description').val('');
        $('#quantity').val('1');
        $('#discountUnit').val('0.00');
        $('#custom_price').val('0.00');
        $('#description').focus();


    });

    // Agregar productooo
    formSalesDetail = $('#formSalesDetail').validate({
        rules: {
            description: {
                required: true
            }
            ,
            quantity: {
                required: true
            }
        },
        messages: {
            description: {required: '<?php echo __("El Producto es requerido.") ?>'}
            ,
            quantity: {required: '<?php echo __("La Cantidad es requerido.") ?>'}

        },
        errorContainer: '#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {
            $(element).addClass('ui-state-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('ui-state-error');
        }
    });
    //agregar datos
    tblSalesDetail = $('#salesDetail_jqGrid').jqGrid({
        height: '100%',
        width: '800',
        datatype: 'local',
        colNames: [
            'IdProduct',
            'IdStore',
            '<?php echo __("Descripción") ?>',
            '<?php echo __("Cantidad") ?>',
            '<?php echo __("Precio Unitario") ?>',
            '<?php echo __("Descuento") ?>',
            '<?php echo __("SubTotal") ?>',
            '<?php echo __("Opción") ?>'
        ],
        colModel: [
            {name: 'idProductOut', index: 'idProductOut', hidden: true},
            {name: 'idStoreOut', index: 'idStoreOut', hidden: true},
            {name: 'descriptionOut', index: 'descriptionOut', width: 240},
            {name: 'quantityOut', index: 'quantityOut', width: 70, align: 'center'},
            {name: 'unitPriceOut', index: 'unitPriceOut', width: 110, align: 'right'},
            {name: 'discountOut', index: 'discountOut', width: 90, align: 'right'},
            {name: 'subTotalOut', index: 'subTotalOut', width: 80, align: 'right'},
            {name: 'optionsOut', index: 'optionsOut', width: 60, align: 'center'}
        ],
        viewrecords: true,
        sortname: 'idOut',
        gridview: true,
        rownumbers: true,
        sortable: true,
        sortorder: "asc",
        rownumWidth: 30,
        footerrow: true,
        gridComplete: function() {
            try {
                aIDs = tblSalesDetail.getDataIDs();
                $.each(aIDs, function() {
                    deleteOption = "<button class='editItem' type='button' data-id='" + this + "'><?php echo __('Editar'); ?>\
                                            </button><button class='delItem' type='button' data-id='" + this + "'><?php echo __('Eliminar'); ?></button>";

                    tblSalesDetail.setRowData(this, {
                        optionsOut: deleteOption
                    });
                });

                $('.delItem').button({
                    icons: {
                        primary: 'ui-icon-minus'
                    }
                    , text: false
                });


                $('.editItem').button({
                    icons: {
                        primary: 'ui-icon-pencil'
                    }
                    , text: false
                });

                $('.delItem').click(function() {

                    var id_delete = $(this).data('id');
                    confirmBox('<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>', '<?php echo __("Eliminar Producto") ?>', function(b_response) {
                        if (b_response) {
                            if (arrayDetails[id_delete].exist == 1) {
                                arrayDetails[id_delete].remove = 1;
                            } else {
                                delete arrayDetails[id_delete];
                            }
                            tblSalesDetail.delRowData(id_delete);
                        }
                    });
                });

                $('.editItem').click(function() {
                    //var p=$("#salesDetail_jqGrid").jqGrid('getRowData',$(this).data('id')); 
                    var id_delete = $(this).data('id');
                    $('#description').val(arrayDetails[id_delete].description);
                    $('#idproduct').val(arrayDetails[id_delete].product_id);
                    $('#idstore').val(arrayDetails[id_delete].store_id);
                    $('#amount').val(arrayDetails[id_delete].amount);
                    $('#quantity').val(arrayDetails[id_delete].quantity);
                    $('#custom_price').val(arrayDetails[id_delete].priceUnit);
                    $('#discountUnit').val(arrayDetails[id_delete].discount);
                    edit_product = 1;
                    calculationPrice();
                });

                fPreTotal = 0;
                $.each(arrayDetails, function() {
                    fPreTotal += parseFloat(this.preSubTotal);
                });

                var aGridData = tblSalesDetail.getRowData();
                fTotalAmountSum = 0;
                $.each(aGridData, function(i, oRowData) {
                    //console.log(i,oRowData);
                    fTotalAmountSum += parseFloat(oRowData.subTotalOut);
                });

                tblSalesDetail.footerData('set', {discountOut: '<?php echo __("TOTAL") ?>', subTotalOut: fTotalAmountSum.toFixed(2)});

            } catch (e) {
                //GG
            }
        }
    });
    //


    $('#quantity').keyup(function(key) {
        var precio = 0;

        returnarray($('#idproduct').val(), $('#idstore').val());

        if ((countarray == 0 && keycountarray == 0) || edit_product == 1) {
            stock = $('#amount').val() - $('#quantity').val();
        } else {
            stock = $('#amount').val() - (parseInt(quantity) + parseInt($('#quantity').val()));
        }
        countarray = 0;
        keycountarray = 0;
        if ($('#idproduct').val() == "") {
            msgBox("Seleccione un Producto");
            $(this).val('');
        } else if (stock < 0) {
            msgBox("La Cantidad es mayor al Stock", "Alerta");
            $(this).val('');
        } else if ($(this).val() == 0) {
            msgBox("La Cantidad no puede ser menos al valor 0.", "Alerta");
            $(this).val('');
        } else {
            calculationPrice();
            $('#priceTotal').val(precio.toFixed(2));
        }

        stock = 0;
    });
    $('#discountUnit').keyup(function(key) {

        calculationPrice();

    });
    calculationPrice = function() {
        priceUnit = $('#custom_price').val().split(' ').join('');
        priceUnit = priceUnit == '' ? 0 : parseFloat(priceUnit);

        fPreTotal = priceUnit;

        quantity = $('#quantity').val().split(' ').join('');
        quantity = quantity == '' ? 0 : parseFloat(quantity);


        priceTotal = priceUnit * quantity;

        $('#priceUnit').val(priceTotal.toFixed(2));
        $('#priceTotal').val(priceTotal.toFixed(2));

        var discount = $('#discountUnit').val().split(' ').join('');
        discount = discount == '' ? 0 : parseFloat(discount);

        if (discount > priceTotal) {
            $('#discountUnit').val(0);
            discount = 0;
        } else {
            //var total_discount = priceTotal * discount;
            priceTotal = priceTotal - discount;
            $('#priceTotal').val(priceTotal.toFixed(2));
        }

    }

    $("#description").autocomplete({
        minLength: 1,
        autoFocus: true,
        source: function(req, response) {
            $.ajax({
                url: url_GetProduct,
                dataType: 'jsonp',
                data: {
                    term: req.term
                },
                success: function(resp) {
                    if (evalResponse(resp)) {
                        response($.map(resp.data, function(item) {

                            var text = "<span class='spanSearchLeft'>" + item.product + "</span>";
                            text += "<div class='clear'></div>";
                            return {
                                label: text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>"),
                                value: item.product,
                                id: item.idProduct,
                                price: item.productPriceSell,
                                idstore: item.idStore,
                                amount: item.Amount
                            }
                        }));
                    } else {
                        msgBox(resp.msg, resp.code);
                    }
                }
            });
        },
        focus: function(event, ui) {
            $('#custom_price').val(ui.item.price);
        },
        select: function(event, ui) {
            $('#idproduct').val(ui.item.id);
            $('#custom_price').val(ui.item.price);
            $('#idstore').val(ui.item.idstore);
            $('#amount').val(ui.item.amount);
        }
    }).data("autocomplete")._renderItem = function(ul, item) {
        return $("<li></li>")
        .data("item.autocomplete", item)
        .append("<a>" + item.label + "</a>")
        .appendTo(ul);
    };

    //agregar productos a orden de pedido

    function salesOrder(idsalesOrder) {
        $.ajax({
            url: url_detailSalesOrder,
            data: {id: idsalesOrder},
            async: false,
            success: function(response) {
                if (evalResponse(response)) {
                    var data = response.so;
                    if (data.serie == null) {
                        $('.orderDocument').text(data.documentType + "  " + '0' + "-" + data.correlative);
                    } else {
                        $('.orderDocument').text(data.documentType + "  " + data.serie + "-" + data.correlative);
                    }
                    $('#chars_Customers').val(data.fullName);
                    $('#idCustomers').val(data.idCustomers);

                    $('#idOrderSales').val(data.idSalesOrder);
                    $('#amountPaid').val(data.amountPaid);
                    $("#dlg_salesorder").dialog("open");
                } else {
                    msgBox(response.msg, response.code);
                }
            }
        });

    }

    function deleteRowsAll(tblJQgrid) {
        var rowIds = tblJQgrid.jqGrid('getDataIDs');
        for (var i = 0, len = rowIds.length; i < len; i++) {
            var currRow = rowIds[i];
            tblJQgrid.jqGrid('delRowData', currRow);
        }
    }
    //

    $('#dlg_salesorder_detail').dialog({
        title: '<?php echo __("Editar Detalle") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 550,
        modal: true,
        //            hide: 'drop',
        //            show: 'drop',
        hide: 'blind',
        show: 'blind',
        buttons: {
            "<?php echo __("Aceptar") ?>": function() {
                $("#updateDetail").submit();
            },
            "<?php echo __("Cancelar") ?>": function() {
                $(this).dialog("close");
            }

        }
    });

    $('#dlg_salesorder').dialog({
        title: '<?php echo __("Agregar Producto") ?>',
        autoOpen: false,
        resizable: false,
        dragable: false,
        height: 'auto',
        width: 820,
        modal: true,
        hide: 'blind',
        show: 'blind',
        buttons: {
            "<?php echo __("Aceptar") ?>": function() {
                var oData = new Object();
                oData.orderSales = $("#idOrderSales").val();
                oData.detail = arrayDetails;
                oData.amountPaid = $("#amountPaid").val();
                oData.idCustomers = $("#idCustomers").val();
                if (oData.amountPaid != null || oData.idCustomers != null)
                {
                    $.ajax({
                        url: url_EditOrder,
                        type: 'POST',
                        dataType: 'json',
                        data: oData,
                        async: false,
                        success: function(response) {
                            if (evalResponse(response)) {
                                $('#dlg_salesorder').dialog("close");
                                formSalesDetail.currentForm.reset();
                                $('#salesDetail_jqGrid').clearGridData();
                                $("#grid_list_ordersales").trigger("reloadGrid");
                            }
                        }
                    });


                } else
                {
                    $('#chars_Customers').focus();
                    msgBox('Falta ingresar datos', 'Error');
                }
            },
            "<?php echo __("Cancelar") ?>": function() {
                formSalesDetail.currentForm.reset();
                $('#salesDetail_jqGrid').clearGridData();
                $(this).dialog("close");
            }
        }
    });


    $('#quantitydet').keyup(function(key) {

        if ($(this).val() == 0) {
            msgBox("La cantidad no puede ser 0");
            $(this).val('');
        } else {
            calculationPriceDetail();
            //$('#priceTotal').val(precio.toFixed(2));
        }

        calculationPriceDetail();

    });

    $('#discountUnitdet').keyup(function(key) {

        calculationPriceDetail();

    });


    calculationPriceDetail = function() {
        priceUnit = $('#custom_pricedet').val().split(' ').join('');
        priceUnit = priceUnit == '' ? 0 : parseFloat(priceUnit);

        fPreTotal = priceUnit;

        quantity = $('#quantitydet').val().split(' ').join('');
        quantity = quantity == '' ? 0 : parseFloat(quantity);

        priceTotal = priceUnit * quantity;

        $('#priceTotaldet').val(priceTotal.toFixed(2));

        var discount = $('#discountUnitdet').val().split(' ').join('');
        discount = discount == '' ? 0 : parseFloat(discount);

        if (discount > priceTotal) {
            $('#discountUnitdet').val(0);
            discount = 0;
        } else {
            //var total_discount = priceTotal * discount;
            priceTotal = priceTotal - discount;
            $('#priceTotaldet').val(priceTotal.toFixed(2));
        }

    }
    $("#accountsreceivable_date,#accountsreceivable_bdaterango,#accountsreceivable_fdaterango, #txtFechaVencimiento").datepicker(
    {
        dateFormat: '<?php echo $jquery_date_format; ?>'
    });
    $("#accountsreceivable_listMonth,#anio").combobox();
    $('.tbd_group').hide();
    $('.tbm_group').hide();
    $('.tbr_group').hide();
    // Busquedad de Fechas 
    $("#listSearchType").combobox({
        selected: function() {
            tipob = $(this).val();
            if (tipob == 'tbd') {
                $('.tbd_group').show();
                $('.tbm_group').hide();
                $('.tbr_group').hide();
                $('#accountsreceivable_date').focus();
            }
            if (tipob == 'tbm') {
                $('.tbm_group').show();
                $('.tbd_group').hide();
                $('.tbr_group').hide();
                $('#accountsreceivable_listMonth').focus();
            }
            if (tipob == 'tbr') {
                $('.tbr_group').show();
                $('.tbd_group').hide();
                $('.tbm_group').hide();
                $('#accountsreceivable_bdaterango').focus();
            }
            if (tipob == '0') {
                $('.tbr_group').hide();
                $('.tbd_group').hide();
                $('.tbm_group').hide();
            }
        }
    });
    $("#btn_search").button({
        icons: {
            primary: "ui-icon-search"
        },
        text: true
    });
    $('#btn_search').click(function() {

        tb = $('#listSearchType').val();
        d = $("#accountsreceivable_date").val();
        sd = $("#accountsreceivable_bdaterango").val();
        ed = $("#accountsreceivable_fdaterango").val();
        a = $("#anio").val();
        m = $("#accountsreceivable_listMonth").val();
        if (tb == "0") {
            msgBox('<?php echo __("Seleccione Tipo de Búsqueda"); ?>');
        } else {

            delete grid_list_ordersales.getGridParam('postData')['d'];
            delete grid_list_ordersales.getGridParam('postData')['m'];
            delete grid_list_ordersales.getGridParam('postData')['a'];
            delete grid_list_ordersales.getGridParam('postData')['sd'];
            delete grid_list_ordersales.getGridParam('postData')['ed'];

            if (tb == "tbd") {
                grid_list_ordersales.setGridParam({
                    postData: {
                        d: function() {
                            return d;
                        }
                    }
                }).trigger("reloadGrid");
            }
            if (tb == "tbr") {
                grid_list_ordersales.setGridParam({
                    postData: {
                        sd: function() {
                            return sd;
                        },
                        ed: function() {
                            return ed;
                        }
                    }
                }).trigger("reloadGrid");
            }
            if (tb == "tbm") {
                if ($("#accountsreceivable_listMonth").val() == 0 || $('#anio').val() == 0) {
                    alert('<?php echo __("Seleccione Mes y Año"); ?>');
                } else {
                    grid_list_ordersales.setGridParam({
                        postData: {
                            m: function() {
                                return m;
                            },
                            a: function() {
                                return a;
                            }
                        }
                    }).trigger("reloadGrid");
                }
            }
            grid_list_ordersales.trigger('reloadGrid');
        }
    });
    // Busquedad de los clientes
    //   $('#chars_Customers').helptext({value: sTxtSearchClient, customClass: 'lightSearch'});

    $("#chars_Customers").autocomplete({
        minLength: 0,
        autoFocus: true,
        position: {offset: "-25px 2px"},
        appendTo: '#auto_div_person_sender',
        source: function(req, response) {
            $.ajax({
                url: url_GetClients,
                dataType: 'jsonp',
                data: {
                    term: req.term
                },
                success: function(resp) {
                    if (evalResponse(resp)) {
                        response($.map(resp.data, function(item) {
                            item_icon = 'ui-icon-help';
                            if (item.type == '<?php echo $CLIENT_TYPE_PERSON ?>') {
                                item_icon = 'ui-icon-person';
                            }
                            if (item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>') {
                                item_icon = 'ui-icon-home';
                            }
                            var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                            text += "<span class='spanSearchLeft'>" + item.customers + "</span>";
                            text += "<div class='clear'></div>";
                            return {
                                label: text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>"),
                                value: item.customers,
                                id: item.idCustomers,
                                type: item.type
                            }
                        }));
                    } else {
                        msgBox(resp.msg, resp.code);
                    }
                }
            });
        },
        focus: function(event, ui) {
            if (ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>') {
                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-home').addClass('ui-icon-person');
            }
            if (ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>') {
                $('.icon_sender_type').removeClass('ui-icon-help ui-icon-person').addClass('ui-icon-home');
            }
        },
        select: function(event, ui) {
            $('#idCustomers').val(ui.item.id);

            $(this).data('autocomplete').text_value = ui.item.value;

            if (ui.item.type == '<?php echo $CLIENT_TYPE_PERSON ?>') {
                $('.icon_sender_type').removeClass('ui-icon-home ui-icon-help').addClass('ui-icon-person');

            }
            if (ui.item.type == '<?php echo $CLIENT_TYPE_COMPANY ?>') {
                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-help').addClass('ui-icon-home');
            }
        },
        change: function(event, ui) {
            if (!ui.item) {

                $('#idCustomers').val('');
                $(this).helptext('refresh');

                $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
                return false;
            }
            return true;

        }

    }).bind('blur', function() {
        if (
        $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').text_value
    ) {
            $('.icon_sender_type').removeClass('ui-icon-person ui-icon-home').addClass('ui-icon-help');
            $(this).helptext('reini');

        } else {

        }
    }).data("autocomplete")._renderItem = function(ul, item) {
        return $("<li></li>")
        .data("item.autocomplete", item)
        .append("<a>" + item.label + "</a>")
        .appendTo(ul);
    };

    $("#chars_Customers").data("autocomplete")._resizeMenu = function() {
        var ul = this.menu.element;
        ul.css({width: 320, 'margin-bottom': 5});
    };

    $("#chars_Customers").data("autocomplete")._resizeMenu = function() {
        var ul = this.menu.element;
        ul.css({width: 320, 'margin-bottom': 5});
    };
    /* Validate Detalle de */
    formupdateDetail = $('#updateDetail').validate({
        rules: {
            quantitydet: {
                required: true
            },
            custom_pricedet: {
                required: true
            }

        },
        messages: {
            quantitydet: {required: '<?php echo __("Cantidad es Requerido") ?>'},
            custom_pricedet: {required: '<?php echo __("Precio es Requerido") ?>'}
        },
        errorContainer: '#errorMessages',
        errorLabelContainer: "#errorMessages .content .text #messageField",
        wrapper: "p",
        highlight: function(element, errorClass) {
            $(element).addClass('ui-state-error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('ui-state-error');
        },
        submitHandler: function(form) {
            $.ajax({
                data: $.extend($(form).serializeObject(), $(form).serializeDisabled()),
                type: "POST",
                dataType: "json",
                url: url_updateSalesDetail,
                beforeSend: function() {
                    $("#loading").dialog("open");
                },
                complete: function() {
                    $("#loading").dialog("close");
                },
                success: function(res) {
                    if (evalResponse(res))
                    {
                        if (res.code == code_success) {
                            msgBox('<?php echo __("Se registro correctamente"); ?>');
                            $("#dlg_salesorder_detail").dialog('close');
                            formupdateDetail.currentForm.reset();
                            $("#grid_list_ordersales").trigger("reloadGrid");

                            //                                $.each(data, function(key, value) { 
                            //                                    $(".textStock").text(value.amount);
                            //                                    $(".textStockRes").text(value.reserv);
                            //                                });
                            //                                $(".textStockAct").text(parseInt($(".textStock").text())-parseInt($(".textStockRes").text()));
                        } else {
                            msgBox('<?php echo __("Ocurrio un error"); ?>');
                            $("#dlg_salesorder_detail").dialog('close');
                            formupdateDetail.currentForm.reset();
                            $("#grid_list_ordersales").trigger("reloadGrid");
                        }
                    }
                }
            });
        }
    });
    });



</script>

<div>
    <center><div class="titleTables"><?php echo __('Administración de Reservas de Productos'); ?></div></center>
</div>


<div class="ui-corner-all ui-widget-content" style="margin: 4px; width: 420px; margin: 0 auto;">    
    <div class="padding10">
        <div class="clear"></div>  
        <div id="search_type">
            <label class="frmLabel"><?php echo __("Tipo de Búsqueda") ?>:</label>
            <div class="item_content">
                <select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival" >
                    <option value="0"><?php echo __("Seleccione una Opción") ?></option>  
                    <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                    <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                    <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                </select> 
            </div>
            <div class="clear"></div>
            <div>
                <dl>
                    <dd class="tbd_group">
                        <label class="frmLabel"><?php echo __("Día:") ?></label>
                        <div class="item_content">
                            <input id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                        </div>
                    </dd>
                    <dd class="tbm_group">
                        <label class="frmLabel"><?php echo __("Mes") ?>:</label>
                        <select id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                            <option value="0"><?php echo __("Seleccione Mes") ?></option>
                            <?php foreach ($a_month_report as $a_dt): ?>
                                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                            <?php endforeach; ?>
                        </select>
                    </dd>
                    <dd class="tbm_group">
                        <label class="frmLabel"><?php echo __("Año") ?>:</label>
                        <select id="anio" name="anio">
                            <option value="0"><?php echo __("Seleccione Año") ?></option>
                            <?php foreach ($a_year_report as $a_dt): ?>
                                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                            <?php endforeach; ?>
                        </select>
                    </dd>
                    <dd class="tbr_group">   
                        <label class="frmLabel"><?php echo __("Fecha Inicial") ?>:</label>
                        <input id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                    </dd>
                    <dd class="tbr_group">
                        <label class="frmLabel"><?php echo __("Fecha Final") ?>:</label>
                        <input id="accountsreceivable_fdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                    </dd>     
                </dl>
            </div>
            <div class="clear"></div>
        </div>
        <div>
            <button id="btn_search"><?php echo __("Buscar"); ?></button>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>  
</div>
<div>
    <div class="div_grid_product">
        <center>
            <div>
                <table id="grid_list_ordersales"> </table>
                <div id="grid_list_ordersales_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="dlg_salesorder_detail" class="none">
    <form id="updateDetail" action="#" method="POST"  style="width: 500px">
        <ul style="float: left;">
            <li>
                <div class="divLeft">
                    <dl>
                        <dt>
                        <label><?php echo __("Producto") ?>:</label>
                        </dt>
                        <dd class="detailProduct"></dd>
                    </dl>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <dl>
                    <dt><label><?php echo __("Almacén") ?>:</label></dt>
                    <dd class="detailStore"></dd>
                </dl>

            </li>
        </ul>
        <div class="clear"></div>

        <ul>
            <li>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="quantity" ><?php echo __("Cantidad") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input type="hidden" id="idDetail" name="idDetail">
                            <input type="hidden" id="idProductDetail" name="idProductDetail">
                            <input type="hidden" id="idStoreDetail" name="idStoreDetail">
                            <input type="hidden" id="idLotDetail" name="idLotDetail">
                            <input class="inputQuantity" type="text" id="quantitydet" name="quantitydet" alt="number"/>
                        </dd>
                    </dl>

                </div>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="quantity" ><?php echo __("Precio") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input class="inputCustomPrice ui-state-highlight" type="text" id="custom_pricedet" name="custom_pricedet" disable="true"/>
                        </dd>
                    </dl>

                </div>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="quantity" ><?php echo __("Descuento") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input id="discountUnitdet" name="discountUnitdet" class="discountUnit ui-state-active" type="text"/>
                        </dd>
                    </dl>

                </div>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="quantity" ><?php echo __("Total") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input id="priceTotaldet" name="priceTotaldet" class="priceTotal ui-state-hover" type="text" disabled />
                        </dd>
                    </dl>

                </div>
            </li>

        </ul>

        <div class="clear"></div>
    </form>
</div>

<div id="dlg_salesorder" class="none">
    <ul>
        <li>
            <div>
                <dl>
                    <dd>
                        <label><?php echo __("Documento") ?>:</label>
                        <div class="orderDocument"></div>
                    </dd>
                </dl>
            </div>
            <div class="clear"></div>
        </li>
        <form class="orderForm" id="formSalesDetail">
            <li>
                <dl>
                    <dd>
                        <label for="chars_Customers"><?php echo __("Cliente"); ?></label>
                    </dd>
                    <dd>
                        <div class="search_client_wrapper ui-corner-all ui-state-default">
                            <span class="icon_sender_type ui-icon ui-icon-help"></span>
                            <input class="autocompleteInput inputData" type="text" id="chars_Customers" name="chars_Customers" value="<?php echo 'Búsqueda de Cliente' ?>"/>
                            <input type="hidden" id="idCustomers" name="idCustomers"/>
                            <div class="clear"></div>
                        </div>
                        <!--<button type="button" class="addCustomer"><?php echo __("Agregar Cliente") ?></button>-->
                        <div id="auto_div_person_sender"></div>
                        <div class="clear"></div>
                    </dd>
                    <dd>
                        <label class="frmlbl"><?php echo __("Monto a Cuenta"); ?></label> 
                    </dd>
                    <dd>
                        <!--<input type="text" id="amountPaid" name="amountPaid" />-->
                    </dd>
                </dl>

            </li>

            <hr class="hr_separate ui-widget-content">
            <li>
            <dd style="display: none;"><input type="hidden" name="idOrderSales" id="idOrderSales"></dd>
            <div class="divLine left">
                <div class="divLine divLineShort">
                    <dl>
                        <dd>
                            <label for="description" class="frmlbl"><?php echo __("Descripción") ?>:</label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input class="inputData" id="description" name="description" type="text"/>
                            <input type="hidden" name="idproduct" id="idproduct"/>
                            <input type="hidden" name="idstore" id="idstore"/>
                            <input type="hidden" name="amount" id="amount"/>
                        </dd>          

                    </dl>
                    <div class="clear"></div>

                </div>
                <div class="clear"></div>
            </div>
            <div class="divLeft">
                <dl>
                    <dd>
                        <label for="quantity" class="labelprice"><?php echo __("Cantidad") ?>:</label>                    
                    </dd>
                    <dd>                
                        <div class="clear"></div>
                        <input class="inputQuantity" type="text" id="quantity" name="quantity" alt="number" value="1"/>
                    </dd>
                </dl>
                <div class="clear"></div>
            </div>
            <div class="divLeft">
                <dl>
                    <dd>
                        <label for="quantity" class="labelprice"><?php echo __("Precio") ?>:</label>                    
                    </dd>
                    <dd>                
                        <div class="clear"></div>
                        <input class="inputCustomPrice ui-state-highlight" type="text" id="custom_price" name="custom_price" disabled/>
                    </dd>
                </dl>
                <div class="clear"></div>
            </div>
            <div class="divLeft buttonadd">
                <dl>
                    <dd>
                        <button type="button" id="addPackageDetail" class="ui-state-highlight"><?php echo __("Agregar") ?></button>
                    </dd>
                </dl>
            </div>
            <div class="clear"></div>
            </li>

            <li>
                <div class="subTotal">
                    <dl>
                        <dd>
                            <!--<label class="frmlblshort priceUnit"><?php echo __("Precio") ?>:</label>-->

                            <input id="priceUnit" name="priceUnit" class="subPrice priceUnit" type="hidden" disabled/>
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </div>

                <div class="subTotal">
                    <dl>
                        <dd>
                            <label class="frmlblshort discountUnit"><?php echo __("Descuento") ?>:</label>

                            <input id="discountUnit" name="discountUnit" class="subPrice discountUnit ui-state-active" type="text" val="0.00"/>
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </div>

                <div class="subTotal">
                    <dl>
                        <dd>
                            <!--<label class="frmlblshort priceTotal"><?php echo __("SubTotal") ?>:</label>-->

                            <input id="priceTotal" name="priceTotal" class="subPrice priceTotal ui-state-hover" type="hidden" disabled />
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </div>                           

                <div class="clear"></div>
            </li>
            </br>
            <li>
                <div class="clear"></div>
                <table id="salesDetail_jqGrid"></table>
                <div class="clear"></div>
                <br/>
            </li>

        </form>
    </ul>
</div>

<div id='dlg_History'>
    <div id='history_content'>
    </div>
</div>
<div id='dlg_History_Shares'>
    <form id="form_History_Shares">        
        <div id='hShares_content'>
        </div>
    </form>
</div>
<div id='dlg_AddShare'>
    <form id="frmAddShare">
        <input id='idSalesOrder' type='hidden' name='idSalesOrder'/>
        <label class="labelprice">Total Reserva:</label><input id='totalAmount' name='totalAmount' readonly="true"/>
        <div class='clear'></div>
        <label class='labelprice'>A Cuenta:</label><input id='amountPaid' name='amountPaid' type='text'/></br>
        <div class='clear'></div>
        <label class="labelprice">Monto Restante:</label><input id='amountToPaid' name='amountToPaid' readonly="true"/>

    </form>

</div>
<div id="dlg_RecoveredOrderSales">
    <form id="frmRecoveredOrderSales" name="frmRecoveredOrderSales">
        <dl>
            <dd>
                <label class="labelprice">Fecha de Vencimiento</label>
                <input id="txtFechaVencimiento" name="txtFechaVencimiento" type="text"/>
                <input id="idReservaRecovered" name="idReservaRecovered" type="hidden"/>
                <input id="statusRecovered" name="statusRecovered" type="hidden"/>
                <div class="clear"></div>
            </dd>
        </dl>
   </form>
</div>