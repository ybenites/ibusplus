<style type="text/css">
    label.frmlbl{width: 150px;font-weight: bold;float: left;line-height: 15px;}

    label{line-height: 16px;}
    li.containerCompany{display: none;}
    #div_register_client .divLine,.divLine.autoPayment,.divLine.divMeasures{width: auto;}
    .div_passanger_dir{margin-left: 10px;}
    #formRegisterClient ul{float: left;}
    #formRegisterClient .div_buttons{padding: 10px 0;}
    #formRegisterClient input{width: 166px;}
    #formRegisterClient select{width: 178px;}
    .input_dialog{width:195px;}
    li.containerCompany{display: none;}
    input[type="text"], textarea, select {
        padding: 5px;
    }
    .div_grid_Customers{        
        padding-top: 10px;
    }
    .div_newCustomer{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
</style>
<script type="text/javascript">
    
    var url_SaveCustomers = '/private/customers/createCustomers'+jquery_params;
    var url_listCustomers = '/private/customers/listAllCustomers'+jquery_params;
    var url_deleteCustomers = '/private/customers/deleteCustomers'+jquery_params;
    var url_ListByIdCustomers='/private/customers/getCustomersById'+jquery_params;
    var url_Ubigeo= '/private/customers/ubigeoCustomers'+jquery_params;
    var msg_warn_delete="<?php echo __("¿Esta seguro que desea eliminar el Cliente?") ?>";
    var title_delete="<?php echo __("Eliminar Cliente") ?>";
    var customer_form_title = "<?php echo __("Cliente"); ?>";
    var customer_newTitle = "<?php echo __("Nuevo Cliente"); ?>";
    var customer_editTitle = "<?php echo __("Editar Cliente"); ?>"; 
    var formRegisterClient;

    $(document).ready(function(){
        //validacion
        formRegisterClient = $('#formRegisterClient').validate({
            rules: {                
                company_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_COMPANY ?>']:checked").is(':checked');
                    }
                },
                company_code :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_COMPANY ?>']:checked").is(':checked');
                    },
                    maxlength:11,
                    minlength:11
                },
                passanger_name :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON  ?>']:checked").is(':checked');
                    }
                },
                passanger_lastname :{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON ?>']:checked").is(':checked');
                    }
                },
                passanger_mail :{
                    required: true,
                    email:true
                },
                passanger_documento:{
                    required: function(){
                        return $("#formRegisterClient input[name=typeClient][value='<?php echo $CLIENT_TYPE_PERSON ?>']:checked").is(':checked');
                    },
                    maxlength:8,
                    minlength:8
                }
                
            },
            messages:{
                company_name :{required: '<?php echo __("La Razón Social es obligatorio.") ?>'},
                company_code :{required: '<?php echo __("El RUC es obligatorio.") ?>',
                    maxlength: '<?php echo __("El RUC no es correcto"); ?>',
                    minlength: '<?php echo __("El RUC no es correcto"); ?>'
                },
                
                passanger_name :{required: '<?php echo __("El Nombre es obligatorio.") ?>'},
                passanger_lastname :{required: '<?php echo __("El Apellido es obligatorio.") ?>'},
                passanger_mail :{
                    required: 'El E-mail es requerido.',
                    email:'El E-mail es incorrecto.'
                },
                passanger_documento:{
                    required:'<?php echo __("El DNI es obligatorio.") ?>',
                    maxlength: '<?php echo __("El DNI no es correcto"); ?>',
                    minlength: '<?php echo __("El DNI no es correcto"); ?>'
                    
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #menssageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){                
                $.ajax({
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()),                    
                    type:"POST",
                    dataType:"json",
                    url:url_SaveCustomers,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Registrado"); ?>');
                            $("#div_register_client").dialog('close');
                            formRegisterClient.currentForm.reset();
                            $("#grid_list_customers").trigger("reloadGrid");
                        }
                    }
                });
            }
        });



        $('#passanger_documento, #company_code').setMask();
        $('.buttonsContainer').buttonset();
//        $("#passanger_dst").combobox();
//        $("#passanger_prov").combobox({
//            selected:function(){
//                fillCombo("passanger_dst", $("#passanger_prov").val(), $("#passanger_dpto").val());
//            }
//        });
        $("#passanger_dpto").combobox({
            selected:function(){
                fillCombo("passanger_prov", $("#passanger_dpto").val(),'provincia');
            }
        
        });
        $('#type_customers').combobox({
            selected:function(event,ui){
                grid_list_customers.clearGridData();
                grid_list_customers.trigger("reloadGrid");
            }
        });

        function fillCombo(updateId,value,des){
            $.ajax({                  
                type:"POST",
                dataType:"json",
                url:url_Ubigeo,
                data:{
                    id:value,
                    des:des   
                },
                success:function(data){
                    $("#"+updateId).empty();
                    $.each(data, function(i, item) { 
                        $("#"+updateId).append("<option value='" 
                            +item.valor+"'>" +item.descripcion+ "</option>");
                    });
                }
            });
        }
        
        $("#btnNewCustomers").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        
        //grid
        grid_list_customers=$("#grid_list_customers").jqGrid({ 
            url:url_listCustomers,
            datatype: "jsonp",
            mtype: 'POST',  
            width:1200,
            height:'auto',
            postData:{
                type_client : function(){
                    return $('#type_customers').val()
                }
            },
            colNames:[ 
                'id',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Documento") ?>',
                '<?php echo __("Dirección") ?>',
                '<?php echo __("Teléfono") ?>',
                '<?php echo __("Celular") ?>',
                '<?php echo __("Email") ?>',
                '<?php echo __("Distrito") ?>',
                'type',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id',key:true,hidden:true},
                {name:'name',index:'name',align:'center',width:120,searchoptions:{sopt:['cn']} , search:true},
                {name:'document',index:'document',align:'center',width:70,searchoptions:{sopt:['cn']}, search:true},
                {name:'address', index: 'address',align:'center',width:150,search:false},
                {name:'phone', index: 'phone',align:'center',width:60,search:false},
                {name:'cellphone', index: 'cellphone',align:'center',width:70,search:false},
                {name:'mail', index: 'mail',align:'center',width:120,search:false},
                {name:'city', index: 'city',align:'center',width:80,search:false},
                {name:'type', index: 'type',hidden:true},
                {name:'actions', index: 'actions',align:'center',width:80,search:false}                
            ],
            pager:'grid_list_customers_pager', 
            sortname: 'name',         
            gridview: true,
            rowNum:10, 
            rownumbers: true,   
            sortorder: "asc",            
            width:1200,
            height:220,
            shrinkToFit:true,
            rownumWidth: 30,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Clientes") ?>',
            gridComplete:function(){
                $.each(grid_list_customers.jqGrid('getDataIDs'), function(){
                    var options='';                                      
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                    options=edit+' '+trash;                    
                    grid_list_customers.setRowData(this, {actions:options});                    
                }) 
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });                    
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });                
            }
        });
        $("#grid_list_customers").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});    
        $('table#grid_list_customers').on('click','.trash',function(){            
            var idb=$(this).data('id');
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:url_deleteCustomers,
                        data:{idb:idb},
                        success: function(response){                            
                            if(evalResponse(response)){
                                $("#grid_list_customers").trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code);
                            }
                        }
                    });          
                }
            });
            return false
        });
        $('table#grid_list_customers').on('click','.edit',function(){            
            $('.frmSubtitle').text(customer_editTitle);
            var idb=$(this).data('id'); 
            loadingcarga=1;
            $.ajax({
                data:{                    
                    id:idb
                },                    
                type:"POST",
                dataType:"json",
                url:url_ListByIdCustomers,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                       
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data = res.data;
                        $('input:radio[name=typeClient]').button('disable');
                        $("#id_customers").val(data.idCustomers);
                        if(data.idCustomersCompany==null){
                            var ubi = res.ubi;

                            var per=res.data.person;
                            $('input:radio[name=typeClient]').val(['PERSON']);
                            $('input:radio[name=typeClient]:checked').trigger('change');
                            $("#id_person").val(per.idPerson);
                            $("#passanger_name").val(per.fName);
                            $("#passanger_lastname").val(per.lName);
                            $('input:radio[name=passanger_sexo]').val([per.sex]);
                            $('input:radio[name=passanger_sexo]:checked').trigger('change');
                            $('input:radio[name=passanger_tdocumento]').val('DNI');
                            // $('input:radio[name=passanger_tdocumento]:checked').trigger('change');
                            $("#passanger_documento").val(per.document);
                            $("#passanger_dir1").val(per.address2);
                            //$("#passanger_dir2").val(per.address2);
                            $("#passanger_mail").val(per.email);
                            $("#passanger_phone").val(per.phone1);
                            $("#passanger_cellphone").val(per.cellPhone);
                        }else{
                            var rr=res.rr;
                            var cus = data.company;
                            $("#id_address").val(rr.idAddress);
                            $("#id_company").val(cus.idCustomersCompany);
                            $("#company_name").val(cus.name);
                            $("#company_code").val(cus.code);
                            $('input:radio[name=typeClient]').val(['COMPANY']);
                            $('input:radio[name=typeClient]:checked').trigger('change');
                            $("#passanger_dir1").val(rr.address2);
                            //$("#passanger_dir2").val(rr.address2);
                            $("#passanger_mail").val(rr.email);
                            //$("#passanger_city").val(rr.customCityName);
                            //$("#passanger_city-combobox").val($("#passanger_city option:selected").text());
                            //$("#passanger_state").val(rr.state);
                            $("#passanger_phone").val(rr.phone);
                            $("#passanger_cellphone").val(rr.cellphone);
                        }
                        
                          //  $("#passanger_dpto").val(ubi.iddpto);
                            $("#passanger_dpto-combobox").val($("#passanger_dpto option:selected").text());

                        $("div#div_register_client").dialog('open');                        
                    }                                        
                }
            });
        });
        
        $('#div_register_client').dialog({
            title:customer_form_title,
            autoOpen: false,
            autoSize:true,
            modal:true,
            width:550,
            resizable: false,
            resizable:false,
            closeOnEscape:true,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#formRegisterClient").submit();
                },
                
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close');                      
                }
            }
        });
        $('input:radio[name=typeClient]').change(function(){
            $(this).each(function(){
                if($(this).val()=='<?php echo $CLIENT_TYPE_PERSON ?>'){
                    $('li.containerCompany').hide();
                    $('li.containerPerson').show();
                }else{
                    $('li.containerPerson').hide();
                    $('li.containerCompany').show();
                }
            });
        });
        
        $("#btnNewCustomers").click(function(){
            $('input:radio[name=typeClient]').button('enable');
            formRegisterClient.currentForm.reset();
            $('.frmSubtitle').text(customer_newTitle);
            $('#div_register_client').dialog('open');
        });
        fnLoadCheck();
    });
</script>

<div>
    <center>
        <div class="titleTables">
            <span class="title"><?php echo __("Administración de Clientes"); ?></span>
        </div>
    </center>
</div>

<div>
    <div class="div_newCustomer">
        <button id="btnNewCustomers" ><?php echo __("Nuevo Cliente"); ?></button>    
        <select id="type_customers" name="type_customers">
            <?php foreach ($a_clients_type as $a_dt): ?>
                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
            <?php endforeach; ?>
        </select>
    </div>
    <div class="div_grid_Customers">
        <center>
            <div>
                <table id="grid_list_customers"> </table>
                <div id="grid_list_customers_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="div_register_client" class="dialog_modal">

    <form id="formRegisterClient" action="#" method="POST" class="frm">
        <fieldset>
            <legend class="frmSubtitle"></legend> 
            <div class="padding10">
                <ul>            
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd><label><?php echo __("Tipo de Cliente") ?>:</label></dd>

                                <dd class="buttonsContainer">
                                    <div class="clear"></div>

                                    <?php foreach ($a_clients_type as $a_client): ?>
                                        <?php
                                        echo "<input 
                                    type='radio' 
                                    id='typeClient" . $a_client['short'] . "' 
                                    name='typeClient'
                                    value='" . $a_client['value'] . "'"
                                        . (array_key_exists('default', $a_client) ? ($a_client['default'] ? 'checked' : '') : '') . "/>
                                    <label for='typeClient" . $a_client['short'] . "'>
                                    " . __($a_client['display']) . "
                                    </label> ";
                                    endforeach;
                                    ?>

                                </dd>

                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerCompany">
                        <div class="divLine">
                            <dl>
                                <dd><label for="company_name" class="frmlbl"><?php echo __("Razón Social") ?>:</label></dd>
                                <dd>
                                    <input class="input_dialog" id="company_name" name="company_name" value="" type="text"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerCompany">
                        <div class="divLine">
                            <dl>
                                <dd><label for="company_code" class="frmlbl"><?php echo __("RUC") ?>:</label></dd>
                                <dd>
                                    <input class="input_dialog" id="company_code" name="company_code" value="" maxlength="11" type="text" alt="number"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson">
                        <div class="divLine">
                            <dl>
                                <dd><label class="frmlbl"><?php echo __("Nombre") ?>:</label></dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_name" name="passanger_name" value="" type="text"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson">
                        <div class="divLine">
                            <dl>
                                <dd><label class="frmlbl"><?php echo __("Apellido") ?>:</label></dd>
                                <dd>
                                    <input class="input_dialog required" id="passanger_lastname" name="passanger_lastname" value="" type="text"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson">
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Sexo") ?>:</label>                    
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div>
                                    <input type="radio" id="sexM" name="passanger_sexo" value="M"  checked  />
                                    <label for="sexM"><?php echo __("M") ?></label>
                                    <input type="radio" id="sexF" name="passanger_sexo" value="F" />
                                    <label for="sexF"><?php echo __("F") ?></label>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson">
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Tipo de Documento") ?>:</label>                        
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div>
                                    <input type='radio' 
                                           id='passanger_tdocumento' name='passanger_tdocumento' value="DNI" checked="true" >
                                    <label for='passanger_tdocumento'>DNI</label>

                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson">
                        <div class="divLine">
                            <dl>
                                <dd><label class="frmlbl"><?php echo __("Nro Documento") ?>:</label></dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_documento" name="passanger_documento" value="" type="text" alt="number"/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                </ul>
                <ul class="div_passanger_dir">
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("E-mail") ?> :</label>
                                </dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_mail" name="passanger_mail" type="text"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Dirección ") ?> :</label>
                                </dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_dir1" name="passanger_dir1" type="text"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <!-- Para determinar P.Geografica -->
                    <li class="containerPerson">
                        <div class="divLine"> 
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Departamento") ?> :</label>
                                </dd>
                                <dd>
                                    <select name="passanger_dpto" id="passanger_dpto" >
                                        <option value="" ><?php echo __("Selecciona una Opción") ?></option>
                                        <?php foreach ($a_dpto as $a_co): ?>
                                            <option value="<?php echo $a_co->iddpto; ?>"> <?php echo $a_co->descripcion; ?></option>                
                                        <?php endforeach; ?>
                                    </select>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
<!--                    <li  class="containerPerson"> 
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Provincia") ?> :</label>
                                </dd>
                                <dd>
                                    <select name="passanger_prov" id="passanger_prov">
                                        <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                    </select>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li class="containerPerson" >
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Distrito") ?> :</label>
                                </dd>
                                <dd>
                                    <select name="passanger_dst" id="passanger_dst">
                                        <option value="" ><?php echo __("Selecciona una Opción") ?></option>

                                    </select>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>-->




                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Teléfono") ?> :</label>
                                </dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_phone" name="passanger_phone" type="text"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label class="frmlbl"><?php echo __("Celular") ?> :</label>
                                </dd>
                                <dd>
                                    <input class="input_dialog" id="passanger_cellphone" name="passanger_cellphone" type="text"/>
                                    <input id="id_customers" name="id_customers" type="hidden"/>
                                    <input id="id_person" name="id_person" type="hidden"/>
                                    <input id="id_company" name="id_company" type="hidden"/>
                                    <input id="id_address" name="id_address" type="hidden"/>
                                </dd>
                            </dl>
                        </div>
                        <div class="clear"></div>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </fieldset>
    </form>
</div>