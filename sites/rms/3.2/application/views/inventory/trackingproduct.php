<style type="text/css">
    divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}

    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    label {
       font-size: 12px;
       font-weight: bolder;
       margin-top: 3px;
    }
    #cmbbox{
        margin-left: 60px;
        margin-bottom: 20px;
        font-size: 13px;
    }    
</style>
<script type="text/javascript">
    var fnListLotGrid= "/private/trackingproduct/listAllLotes";
    var url_listAllMove = "/private/trackingproduct/listMove";
    var url_historyMove = "/private/trackingproduct/historyMove";
    var colNameProductDetail = '<?php echo __("Producto Detalle") ?>';

    $(document).ready(function(){
      $("#typeProduct").combobox({
            selected:function(){
                grid_list_product.clearGridData();
                if($(this).val() == '1'){
                     $("#grid_list_product").jqGrid('hideCol',["lot_name"]);
                     $("#grid_list_product").jqGrid('showCol',["serie_electronic"]);
                }else{
                     $("#grid_list_product").jqGrid('hideCol',["serie_electronic"]);
                     $("#grid_list_product").jqGrid('showCol',["lot_name"]);
                }
                grid_list_product.trigger('reloadGrid');
            }
        });
        grid_list_product= $("#grid_list_product").jqGrid({ 
            url:fnListLotGrid+jquery_params,
            datatype: "jsonp",
            mtype: 'POST',
            postData:{
                typeProduct:function(){
                    return $("#typeProduct").val();
                }
            },
            colNames:[ 
                colNameProductDetail,                 
                '<?php echo __("Producto") ?>',             
                '<?php echo __("Serie Electronica") ?>',                 
                '<?php echo __("Nombre Lote") ?>',                 
                '<?php echo __("Codigo de Producto") ?>',               
                '<?php echo __("Nombre de Producto") ?>' ,                
                '<?php echo __("Descripcion") ?>',                 
                '<?php echo __("Cantidad") ?>',                 
                '<?php echo __("Precio de Costo") ?>',                 
                '<?php echo __("Precio de Venta") ?>'                
             //   '<?php echo __("Acciones") ?>'                 
            ],
            colModel:[
                {name:'idProductDetail',index:'idProductDetail',key:true, hidden:true},
                {name:'idProduct',index:'idProduct',align:'center', hidden:true },                
                {name:'serie_electronic',index:'serie_electronic', hidden: true ,align:'center',width:120,search: true,ignoreCase:true,searchoptions:{sopt:['cn']}},                
                {name:'lot_name',index:'lot_name',align:'center',width:120,search: false,ignoreCase:true},                
                {name:'po.aliasProduct',index:'po.aliasProduct',align:'center',search:true,ignoreCase:true,searchoptions:{sopt:['cn']}},                
                {name:'po.productName',index:'po.productName',align:'center', search:false},                                         
                {name:'description',index:'description',align:'center',search:false, width:100},             
                {name:'amount',index:'amount',align:'center',search:false, width: 80},                
                {name:'unitPrice',index:'unitPrice',align:'center',search:false, width:80 },                               
                {name:'salePrice',index:'salePrice',align:'center',search:false, width:80 }
            ],
            pager:'#grid_list_product_pager', 
            sortname: 'po.idProduct',         
            gridview: true,
            rowNum:20,  
            rowList:[15,30,50],
            width:1200,
            height:400,
            shrinkToFit:true,
            rownumWidth: 20,
            rownumbers: true,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Productos") ?>',
            subGrid:true,
            subGridRowExpanded: function(subgrid_id, row_id) {
                var data=$("#grid_list_product").jqGrid("getRowData",row_id);
                var subgrid_table_id, pager_id;
                pager_id = "p_"+subgrid_table_id;
                subgrid_table_id = subgrid_id+"_t";
                jQuery("#"+subgrid_id).html("<table data-id_product='"+data.idProduct+"' id='"+subgrid_table_id+"' class='scroll'></table>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url:url_listAllMove+jquery_params,
                    datatype: "jsonp",
                    mtype: 'POST',
                    postData:{
                        idTypeProduct:function(){return data.idProductDetail},
                        typeProduct:function(){return $("#typeProduct").val()}
                    },
                    colNames: [
                        '<?php echo __("Detalle de Movimiento") ?>',
                        '<?php echo __("Tipo de Movimiento") ?>',
                        '<?php echo __("Descripcion de Movimiento") ?>',
                        '<?php echo __("Nombre Almacen") ?>',
                        '<?php echo __("Cantidad") ?>',
                        '<?php echo __("Nombre Usuario") ?>',
                        '<?php echo __("Fecha de Registro") ?>',
                        '<?php echo __("Actions") ?>'
                    ],
                    colModel: [
                        {name: 'idMoveDetail', index:'idMoveDetail', search:false,hidden: true, key: true},
                        {name: 'TypeMoveStore', index: 'TypeMoveStore', align: 'center', search: false, width:150},
                        {name: 'MoveReference', index:'MoveReference', align:'center', search: false, width:150},
                        {name: 'Store', index: 'Store', align: 'center', search:true,width: 150},
                        {name: 'amount', index: 'amount', align: 'center', search: false, width: 150},
                        {name: 'FullName', index: 'FullName', align: 'center', search: false, width: 150},
                        {name: 'CreationDate', index: 'CreationDate', align: 'center', search: false,width:150},
                        {name: 'Actions', index: 'Actions', align: 'center',search: false, width:100}
                    ],
                    //scroll:true,
                    rowNum:50,
                    pager: pager_id,
                    rownumWidth:50,
                    altRows:true,
                    rownumbers: true,
                    viewrecords: true,
                    // gridview: true,
                    shrinkToFit:true,
                    sortname: 'idMoveDetail',
                    //                    sortorder: "ASC",
                    afterInsertRow: function(rowId, rowData, rowElement){
                        record = "<a class=\"record\" style=\"cursor: pointer;\" data-type=\""+rowData.TypeMoveStore+"\" data-id=\""+rowId+"\" title=\"<?php echo __('Historial'); ?>\" ><?php echo __('Historial'); ?></a>";
                        $("#"+subgrid_table_id).jqGrid('setRowData',rowId,{Actions: record});
                        
                    },gridComplete: function(){
                        $('.record').button({
                            icons: {
                                primary: 'ui-icon-folder-collapsed'
                            } ,
                            text: false
                        });
                        $('.record').click(function(){
                            /* Datos de detalle */
                            var type = $(this).data('type');
                            var id = $(this).data('id');
                            $.ajax({
                                url: url_historyMove,
                                data:{ 
                                    id:id,
                                    type:type
                                },                    
                                type:"POST",
                                dataType:"json",
                                success:function(response){
                                   if(evalResponse(response)){ 
                                        var d = response.data;
                                        var tras = response.tras;
                                        var content;
                                        if(d.typeMoveStore == 'INGRESO' ){
                                            content = 
                                            "<label>Proveedor:   </label>"+d.name+"<br>"+
                                            "<label>Almacen:  </label>"+d.storeShortName+"<br>"+
                                            "<label>Movimiento Referencia:  </label>"+d.moveReference+"<br>"+
                                            "<label>Tipo de Movimiento:  </label>"+d.typeMoveName+"<br>"+
                                            "<label>Cantidad:  </label>"+d.amount+"<br>";
                                       }else if(d.typeMoveStore == 'VENTA_DIRECTA'){
                                           if(d.userName != null && d.doc != null)
                                           {
                                               var vend = d.userName;
                                               var doc = d.doc;
                                           }else{
                                               var vend = 'Ninguno';
                                               var doc = 'Ninguno';
                                           }
                                           content = 
                                            "<label>Vendedor:   </label>"+vend+"<br>"+
                                            "<label>Almacen:  </label>"+d.storeShortName+"<br>"+
                                            "<label>Documento Nro:  </label>"+doc+"<br>"+
                                            "<label>Cliente:  </label>"+d.fullName+"<br>"+
                                            "<label>Fecha de Registro:  </label>"+d.salesDate+"<br>"+
                                            "<label>Movimiento Referencia:  </label>"+d.moveReference+"<br>"+
                                            "<label>Precio de Unidad:  </label>"+d.unitPrice+"<br>"+
                                            "<label>Cantidad:  </label>"+d.amount+"<br>";   
                                       }else if(d.typeMoveStore == 'SALIDA'){
                                            content = 
                                            "<label>Movimiento Referencia:   </label>"+d.moveReference+"<br>"+
                                            "<label>Almacen:  </label>"+d.storeShortName+"<br>"+
                                            "<label>Tipo Movimientos:  </label>"+d.typeMoveName+"<br>"+
                                            "<label>Cliente:  </label>"+d.fullName+"<br>"+
                                            "<label>Fecha de Salida:  </label>"+d.dateOutcome+"<br>"+
                                            "<label>Cantidad:  </label>"+d.amount+"<br>"; 
                                       }else if(d.typeMoveStore == 'TRAS_INGRESO'){
                                            content = 
                                            "<label>Movimiento Referencia:   </label>"+d.moveReference+"<br>"+
                                            "<label>Almacen Salida:   </label>"+tras.storeShortName+"<br>"+
                                            "<label>Almacen Entrada:  </label>"+d.storeShortName+"<br>"+
                                            "<label>Fecha de Ingreso:  </label>"+d.dateIncome+"<br>"+
                                            "<label>Cantidad:  </label>"+d.amount+"<br>"; 
                                       }else if(d.typeMoveStore== 'TRAS_SALIDA'){
                                            content = 
                                            "<label>Movimiento Referencia:   </label>"+d.moveReference+"<br>"+
                                            "<label>Almacen Salida:   </label>"+d.storeShortName+"<br>"+
                                            "<label>Almacen Entrada:  </label>"+tras.storeShortName+"<br>"+
                                            "<label>Fecha de Salida:  </label>"+d.dateOutcome+"<br>"+
                                            "<label>Cantidad:  </label>"+d.amount+"<br>"; 
                                       }                                       
                                       $("#history_content").html(content);
                                       $("#record_dialog").dialog('open');
                                    }
                                }
                            });
                      });
                    }
            });
            }
        });
        $("#grid_list_product").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});

        $("#record_dialog").dialog({
            title: 'Producto',
            width: 500,
            height: 200,
            buttons:{
            'OK':function()
                {
                    $(this).dialog('close');

                }
            }
        });

    });
</script>
<div>
    <center>
        <div class="titleTables">
            <span class="title"><?php echo __("Seguimiento de Productos"); ?></span>
        </div>
    </center>
</div>
<div id="cmbbox">
    <label><?php echo __("Tipo de Productos ");?>:       </label>
    <select id="typeProduct" name="typeProduct">
        <option value="2"><?php echo "Lotes"?></option>
        <option value="1"><?php echo "Producto Detalle"?></option>
    </select>
</div>
<div>
    <div class="div_grid_product">
        <center>
            <div>
                <table id="grid_list_product"> </table>
                <div id="grid_list_product_pager"></div>
            </div>
        </center>
    </div>
</div>

<!-- Formulario del Historial de Movimiento -->

<div id="record_dialog" class="dialog" >
    <form id="record_frm" method="POST" >
        <fieldset class="ui-corner-all cFieldSet">   
            <legend><?php echo __("Historial de Producto") ?></legend>
            <div id="history_content">
                
            </div>
        </fieldset>
    </form>
</div>



