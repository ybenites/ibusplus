<style type="text/css"> 
    .clear{
        clear: both;    
    }
    #income_content{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
        padding: 20px;
        margin-top: 10px;
        margin-bottom: 30px;
    }
    .divDocInfo{float:right;}
    input.helptext{font-style: italic;color: #CCCCCC;}
    div.divDocInfo{font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    .div_grid_stokeincome{        
        padding-top: 15px;
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    .cFieldSet{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
        border-bottom-style: dashed;
        border-left: none;
        border-right: none;
        border-top: none;
        border-width: 3px;
        padding-bottom: 25px;
    }
    .cFieldSet>div>div{
        padding: 5px;
    }
    .cFieldSet .ui-buttonset .ui-button {
        width: auto;
        margin-top: 0;
    } 
    .cFieldSet>div>div>label{        
        float: left;
        font-weight: bolder;
        margin-top: 7px;
        padding-right: 10px;
        text-align: right;
        width: 140px;
    }

    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px;               
    }
    #for_div_1{
        float: left;
        padding-right: 15px;        
    }
    #for_div_2{
        float: left;
        padding-left: 15px;
        padding-right: 15px;  
        border-bottom: none;
        border-left-style: dashed;
        border-right-style: dashed;        
        border-top: none;
        border-width: 3px;
    }
    #for_div_2>div>input{
        width: 150px;
    }
    #for_div_3{
        padding-left: 15px;
        float: left;
    }
    #for_div_3>div>input{
        width: 180px;        
    }
    #btn_buttons{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
        padding-top: 15px;
    }
    #buttons_add_product{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;        
    }
    #div_add_product{
        float: left;
        margin-right: 15px;
        padding: 10px;
        width: 400px;   
        border-style: groove;        
        border-width: 5px;
    }
    #div_grid_income{
        float: left;
    }
    #check_type_search_product{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    #div_input_search_product input{
        height: 22px;
        float: left;
        margin-right:5px;
        width: 250px;
    }
    #div_input_search_product{
        margin-right: auto;
        margin-left: auto;
        width:300px;
    }
    #field_product{
        padding: 10px;
    }
    #field_product>div{
        padding:5px;
    }
    .two_colum{
        float: left;
    }

    #div_product_find{
        padding: 7px;
    }
    #div_product_find input{
        height:19px;
    }
    #div_product_find>div{
        padding: 5px;
    }
    #div_product_find>div label{
        font-weight: bolder;
        float: left;
        min-width: 70px;
        padding-top: 5px;
        text-align:right;
        padding-right: 3px;
    }
    input.helptext{font-style: italic;color: #CCCCCC;}

    #frm_new_product>fieldset>div{
        padding-bottom: 7px;
    }    
    #frm_new_product>fieldset>div label{        
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 160px;        
    }
    #input_detail>div{
        padding-top:10px;
    }
    #frm_detail_product>div{
        padding-top:10px;
    }    
    #frm_detail_product>div>label{
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 150px;
    }    
</style>
<script type="text/javascript">
    /* URL */
    var url_getAllPurchaseOrder="/private/stockincome/getAllPurchaseOrder";
    var url_getDataGeneralByPurchaseOrder="/private/stockincome/getDataGeneralByPurchaseOrder"; 
    var url_createIncomeStock="/private/stockincome/createIncomeStock"; 
    var url_getDataSearchAutocompleteProduct="/private/stockincome/getProductAutocomplete"; 
    var url_getProductById="/private/stockincome/getProductById";     
    
    /* VARIABLES */
    var grid_list_stokeincome = '';
    var product_detail = new ArrayMap();
    var product_lot = new ArrayMap();
    
    
    /* MENSAJES */
    var msg_warn_delete="<?php echo __('Desea Eliminar Este Producto de la Lista'); ?>";
    var title_delete="<?php echo __('Eliminar Producto') ?>";
    var asignarLot = '<?php echo "Asignar Lote" ?>';
    var countLot = 0;
    var flags = {};
    flags.typeIncome = 0;

    /* VARIABLES PHP */
    var bAvailableDocumentType = '<?php echo $bAvailableDocumentType; ?>';
    var type_document='<?php echo $type_document; ?>';
    var today = '<?php echo $today ?>';
    var arrayAllSerie = new Array();
    var serie_repeated=0;
    
    $(document).ready(function(){
        
        /* Hide */
        $('#div_add_product').hide();
        $(".doc_ref").hide();
        /* Buttons */
        $("#btn_register").button({
            icons: {
                primary: "ui-icon-check"                
            },
            text:true
        });        
        $("#btn_cancel").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        });
        $("#btn_add_detail").button({
            icons: {
                primary: "ui-icon-plusthick"                
            },
            text:true
        });
        $("#btn_add_product").button({
            icons: {
                primary: "ui-icon-plusthick"                
            },
            text:true
        });                 
        $("#btn_clear_product").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        }); 
        
        /* ButtonSet */ 
        $("#for_income, #for_currency, #check_type_search_product").buttonset();   
        /* Combobox */
        $("#select_typeMove, #select_store, #select_supplier").combobox();
        $("#select_purchaseOrders").combobox({
            selected:function(event,ui){ 
                $("#select_purchaseOrders").trigger('change');
            }
        });
        /* Datepicker */
        $("#input_date_income").datepicker({
            dateFormat:'<?php echo $jquery_date_format ?>'        
        });
        /* Keyup */
        $("#input_amount").keyup(function(e){                  
            var stock_static=$("#input_hidden_stock").val();
            var quantity=$(this).val();            
            setTimeout(function(){
                calcStock(stock_static,quantity);
                calcStockSale(stock_static,quantity);
            },50);
        });
        $("#input_price_product").keyup(function(e){
            var quantity=$("#input_amount").val();
            var price=$(this).val();
            setTimeout(function(){                
                calcStock($("#input_hidden_stock").val(),quantity);
            },50);
        });
        $("#input_saleprice_product").keyup(function(e){
            var quantity=$("#input_amount").val();
            var price=$(this).val();
            setTimeout(function(){                
                calcStockSale($("#input_hidden_stock").val(),quantity);
            },50);
        });      
        /* PriceFormat */
         $("#input_price_product,#input_saleprice_product ,#input_importe_product").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* SetMask */
        $("#input_amount").setMask();        
        

        /* Documentos Asignados */
        if (bAvailableDocumentType){
            successDocument = false;
             fnUpdateCorrelativePackage = function(sType){
                try{                
                    oDocumentType = getCurrentCorrelative(type_document);                                                                                                                                                                                                                                                                       
                    if(oDocumentType.serie != null)
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.serie + '-' +oDocumentType.correlative;
                    else
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.correlative;

                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-highlight')
                    .html(displayDocument);
                    successDocument = true;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                }catch(e){
                    successDocument = false;
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-error')
                    .html('<?php echo __("Documento no Asignado") ?>');
                }
            }; 
            fnUpdateCorrelativePackage(type_document);  
        }
        /* Tipo de ingreso */
        $("input[name=for_income]").change(function(){ 
            product_detail.clear();
            product_lot.clear();
            $('#sec_button_lotsAll').hide();
            $("#input_hidden_stock").val(0);               
            frm_product.currentForm.reset();
            frm_product.resetForm();  
            $(frm_content.currentForm).find('input[type=text],input[type=hidden],textarea,select').val('');
            $("#grid_list_stokeincome").jqGrid("clearGridData");
            if($(this).val()=='purchase'){
                $(".purchase").show();
                $(".doc_ref").hide();
                $("#input_date_income").val(today);
                $("#div_add_product").hide().fadeOut();
            }else if($(this).val()=='other'){                
                $(".purchase").hide();
                $(".doc_ref").show();
                $("#input_date_income").val(today);
                $("#div_add_product").show().fadeIn();
            }                            
        });

        /********************* Por Otros ***********************/
        /* Autocomplete de Productos */
        $('#input_search_product').helptext({value:'Búsqueda de Productos'});
        $("#input_search_product").autocomplete({
            minLength:1,            
            autoFocus: true,
            source:function(req,response){               
                $.ajax({
                    url:url_getDataSearchAutocompleteProduct+jquery_params,
                    dataType:'jsonp',
                    data: {
                        term : req.term,
                        typeSearch:$("input[name=check_type_search_product]:checked").val()
                    },
                    success:function(resp){                        
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {                                                                                                
                                var text = "<span class='spanSearchLeft'>" + item.dataMostrar+ "</span>";                         
                                text += "<span class='spanSearchRight'>" + item.otraData + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:item.dataMostrar,
                                    id:item.id,
                                    code:item.otraData
                                }
                            }));
                        }
                    }
                });
            },
            select: function( event, ui ) {                                                
                $('#input_search_product').val(ui.item.value);                 
                $('#input_search_product').helptext('refresh'); 
                edit_product_income=0;
                getProductById(ui.item.id); 
                return false;
            },
            change: function( event, ui ) {                                       
                if ( !ui.item ) {
                    $('#input_search_product').val('');  
                    $( this ).helptext('refresh');                                                        
                }  
                return true;                
            }
        })
        .bind('blur', function(){             
            if($(this).data('autocomplete').selectedItem == null && $(this).val() == ''
                || $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').term)
            {
                $('#input_search_product').val('');
                $( this ).helptext('refresh');                
            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {               
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        /* Adquirir Producto */
        function getProductById(id){
//            showLoading=1;
            var idStore=$("#select_store").val();            
            $.ajax({                
                url:url_getProductById+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{id:id,idStore:idStore}, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        var r=response.data;                        
                        var productStock=response.amount;                        
                        $("#input_product").val(r.productName);
                        $("#input_measure").val(r.productUnitMeasures);
                        $("#input_stock").val(parseInt(productStock)+parseInt($("#input_amount").val()));
                        $("#input_hidden_stock").val(productStock);
                        $("#input_saleprice_product").val(r.productPriceSell);
                        /** Actualiza Importes **/                       
                        var stock_static=$("#input_hidden_stock").val();
                        var quantity= $("#input_amount").val();            
                        setTimeout(function(){
                            calcStock(stock_static,quantity);
                            calcStockSale(stock_static,quantity);
                        },50);
                        /***/
                        var arrayDataGrid=$("#grid_list_stokeincome").jqGrid("getRowData");
                        var stk_hidden=0;
                        if(arrayDataGrid.length>0){
                            $.each(arrayDataGrid,function(index,data){                                  
                                if(data.codigo==r.aliasProduct){
                                    stk_hidden=parseInt(stk_hidden)+parseInt(data.amount);
                                    $("#input_hidden_stock").val(parseInt(productStock)+parseInt(stk_hidden));
                                    $("#input_stock").val(parseInt(productStock)+parseInt(stk_hidden)+parseInt($("#input_amount").val()));
                                }
                            });
                        }                                                
                        $("#input_hidden_id_product").val(r.idProduct);
                        $("#input_hidden_code_product").val(r.aliasProduct);
                        $("#input_hidden_have_detail").val(r.haveDetail);
                        $("#input_amount").focus();
                    }
                }
            });
        }   
        /* Funciones de Calculo de Importes */
        function calcStock(stock_static,quantity){
            quantity=(quantity=='')?0:quantity;                 
            $("#input_stock").val(eval(parseInt(stock_static,10)+parseInt(quantity,10)));
            $("#input_importe_product").val((quantity*$("#input_price_product").val()).toFixed(2));
        }
        function calcStockSale(stock_static,quantity){
            quantity=(quantity=='')?0:quantity;                 
            $("#input_stock").val(eval(parseInt(stock_static,10)+parseInt(quantity,10)));
            $("#input_importesale_product").val((quantity*$("#input_saleprice_product").val()).toFixed(2));
        }  
        /* Limpiar Producto */
        $("#btn_clear_product").on('click',function(e){
            e.preventDefault();
            $("#input_hidden_stock").val(0);
            $("#input_product").val('');
            $("#input_lote").val('');
            $("#input_measure").val('');
            $("#input_amount").val('');
            $("#input_stock").val('');
            $("#input_price_product").val('');
            $("#input_saleprice_product").val('');
            $("#input_importe_product").val('');
            $("#input_importesale_product").val('');
            $("#input_search_product").helptext('refresh');
            $("#input_hidden_stock").val(0);
            $("#input_search_product").helptext('refresh');
        });
        /* Agregar Producto al JQGrid */
        $("#btn_add_product").on('click',function(e){
            e.preventDefault();
            if(!$("#frm_product").valid())
                return false;
            var id=$("#input_hidden_id_product").val();  
            var trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                                        
            
            var p={};   
            p.id=id;
            p.haveDetail=$("#input_hidden_have_detail").val();
            p.codigo=$("#input_hidden_code_product").val();
            p.producto=$("#input_product").val();
            p.price=$("#input_price_product").val();
            p.saleprice=$("#input_saleprice_product").val();
            p.amount=parseInt($("#input_amount").val(),10);
            p.importe=$("#input_importe_product").val();
            p.importesale=$("#input_importesale_product").val();
            
            var arrayDataGrid=$("#grid_list_stokeincome").jqGrid("getRowData");            
            if(arrayDataGrid.length>0){ 
                var j=0;
                $.each(arrayDataGrid,function(index,data){                                                      
                    if(data.codigo==p.codigo){
                        j=j+1;                        
                    }
                });
                if(j>0){
                    msgBox('Ya existe el Producto agregado.');
                    return false;
                }                    
            }    
            if(p.haveDetail==true){
                var add_detail = "<a class=\"add_detail\" style=\"cursor: pointer;\" data-id=\""+p.id+"\" title=\"<?php echo __('Agregar Detalle'); ?>\" ><?php echo __('Agregar Detalle'); ?></a>";                                        
                var options=add_detail+trash;
                p.actions=options;     
                // Ingreso la serie electronicas ya ingresadas
                if(product_detail.get(id) != null){
                    var objectProducts = product_detail.get(id).allDetail.valSet();
                    $.each(objectProducts, function(index,data){
                        $("#grid_income_detail").jqGrid('addRowData',data.serie_electronica,data);
                    });
                }
                $("#input_detail").dialog('open');
            }else{ 
                var add_lot = "<a class=\"lot\" style=\"cursor: pointer;\" data-id=\""+p.id+"\" data-icount=\"\" title=\"<?php echo __('Agregar Lote'); ?>\" ><?php echo __('Agregar Lote'); ?></a>";                                        
                var options=add_lot+trash;
                p.actions=options; 
                $('input#add_id_prod').val(id);
                data = grid_list_stokeincome.getRowData(id);
                $('input#add_icont_prod').val(data.idmovdetail);
                $('input#add_lot_description').val(data.lote);
                $('#frmAddLotDiv').dialog('open');
            }
            p.actions= options;    
            $("#grid_list_stokeincome").jqGrid('addRowData',id,p); 
            grid_list_stokeincome.trigger("reloadGrid"); 
        });
        /* Eliminar Producto del JQGrid */
        $('table#grid_list_stokeincome').on('click','.trash',function(){                          
            var p=$("#grid_list_stokeincome").jqGrid('getRowData',$(this).data('id'));                
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){ 
                    $("#grid_list_stokeincome").jqGrid('delRowData',p.id);
                    grid_list_stokeincome.trigger("reloadGrid");          
                }
            });
            return false;
        });
        /* Validacion de Productos */
        $.validator.addMethod("validStockAvible", function(value, element, params) {            
            return  parseFloat(value)< 0?false:true; 
        }, "<?php echo __('El Stock Es Menor que 0') ?>");
        $.validator.addMethod("validPrice", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        $.validator.addMethod("validAmount", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        $.validator.addMethod('decimal', function(value, element) {
            return this.optional(element) || /^\d+(\.\d{0,2})?$/.test(value); 
        }, "<?php echo __('El Formato Correcto para Precio de Venta es: xxxxx.xx') ?>");
        var frm_product=$("#frm_product").validate({
            rules:{
                input_product:{
                    required:true
                },
                input_measure:{
                    required:true
                },
                input_amount:{
                    required:true,
                    validAmount:true
                },                                
                input_stock:{
                    required:true,
                    validStockAvible:true
                },                                
                input_price_product:{
                    required:true,
                    validPrice:true
                },                                
                input_importe_product:{
                    required:true
                },                     
                input_saleprice_product:{
                    decimal:true
                }                             
            },
            messages:{
                input_product:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_measure:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_amount:{
                    required:'<?php echo __("Ingrese La Cantidad") ?>',
                    validAmount:'<?php echo __("Ingrese La Cantidad Mayor que Cero") ?>'
                },                                
                input_stock:{
                    required:'<?php echo __("No puede ser vacio el stock") ?>',
                    validStockAvible:'<?php echo __("el stock no puede ser negativo") ?>'
                },                                
                input_price_product:{
                    required:true,
                    validPrice:'<?php echo __("El Precio no Puede Ser Cero") ?>'
                },                                
                input_saleprice_product:{
                    decimal:'<?php echo __("El Formato de Precio de Venta es Incorrecto") ?>'
                },                                
                input_importe_product:{
                    required:'<?php echo __("El Importe no Puede se vacio") ?>'
                }                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        
        
        /********************* Por Compras ********************/
        /* Listar Ordenes de Compras */
        getAllPurchaseOrder();
        function getAllPurchaseOrder(){
      //      showLoading=1;
            $.ajax({                
                url:url_getAllPurchaseOrder+jquery_params,                        
                type:'POST',
                dataType:'json',
                async:false,
                success:function(response){
                    var o_detail = new Object();
                    if(evalResponse(response)){
                        var r=response.data;                        
                        var template="<option value=''><?php echo __('Seleccione Una Opción...'); ?></option>";
                        $.each(r, function(index,data){
                            template=template+"<option value='"+data.id+"'>"+data.display+"</option>";
                        });
                        $("#select_purchaseOrders").html(template);                        
                    }
                }
            }); 
        }
        /* JQGRID de Ordenes de Compra */       
        grid_list_stokeincome=$("#grid_list_stokeincome").jqGrid({            
            datatype: "local",
            mtype: 'POST',            
            colNames:[ 
                '<?php echo __("id") ?>',
                '<?php echo __("Tiene Detalle") ?>',
                '<?php echo __("Código") ?>',
                '<?php echo __("Producto") ?>',                
                '<?php echo __("Cantidad") ?>',                               
                '<?php echo __("Precio Unit. Compra") ?>',                
                '<?php echo __("Precio Unit. Venta") ?>',                
                '<?php echo __("Importe Compra") ?>',                
                '<?php echo __("Importe Venta") ?>',                
                '<?php echo __("Lote") ?>',                
                '<?php echo __("ID Mov Detail") ?>',                
                '<?php echo __("Acciones") ?>'                
            ],
            colModel:[
                {name:'id',index:'id',hidden:true,width:50},
                {name:'haveDetail',index:'haveDetail',hidden:true},
                {name:'codigo',index:'codigo',align:'center'},
                {name:'producto',index:'producto',align:'center',search:true},          
                {name:'amount',index:'amount',align:'center',search:true}, 
                {name:'price',index:'price',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                               
                {name:'saleprice',index:'saleprice',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                               
                {name:'importe',index:'importe',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                                               
                {name:'importesale',index:'importesale',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                                               
                {name:'lote',index:'lote',align: 'center'},                                                               
                {name:'idmovdetail',index:'idmovdetail',align: 'center',hidden:true},                                                               
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_stokeincome_pager',         
            gridview: true,
            rowNum:100,   
            sortorder: "asc",            
            width:900,
            height:300,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            caption:'<?php echo __("Lista De Productos") ?>',
            grouping:true,
            groupingView : { 
                groupField : ['producto'],
                groupColumnShow: [false],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,                    
                groupSummary : [true], 
                showSummaryOnHide: true,
                groupDataSorted : true
            },
            gridComplete:function(){
                $('.add_detail').button({
                    icons:{primary: 'ui-icon-note'},
                    text: false
                });                    
                $('.lot').button({
                    icons:{primary: 'ui-icon-link'},
                    text: false
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                
            }
        });
        /* Seleccionar Orden de Compra */
        $("#select_purchaseOrders").on('change',function(e){
          //  $('#sec_button_lotsAll').show(); para asignar varios lotes
            $("#grid_list_stokeincome").jqGrid("clearGridData");
           // $("#input_hidden_stock").val(0);
//            data_detail_general=[];
//            edit_product_income=0;                                    
//            showLoading=1;
            var id_purchase_order=$(this).val();            
            $.ajax({                
                data:{
                    id_purchase_order:id_purchase_order
                },                    
                type:"POST",
                dataType:"json",
                url:url_getDataGeneralByPurchaseOrder+jquery_params,
                success:function(res){
                    if(evalResponse(res))               
                    {                        
                        var general=res.dataGeneral;                        
                        var detail=res.dataDetail; 
                        $("#select_supplier").val(general.idSuplier);
                        $("#select_supplier-combobox").val($("#select_supplier option:selected").text());
                        $("#select_currency").val(general.currency);
                        $("#select_currency-combobox").val($("#select_currency option:selected").text());
                        $.each($.parseJSON(detail),function(index,p){
                            if(p.haveDetail== true){                                
                                var add_detail = "<a class=\"add_detail\" style=\"cursor: pointer;\" data-id=\""+p.id+"\" title=\"<?php echo __('Agregar Detalle'); ?>\" ><?php echo __('Agregar Detalle'); ?></a>";                                        
                                var options=add_detail;
                                p.actions=options;
                            }else{
                                var add_lot = "<a class=\"lot\" style=\"cursor: pointer;\" data-id=\""+p.id+"\" data-icount=\"\" title=\"<?php echo __('Agregar Lote'); ?>\" ><?php echo __('Agregar Lote'); ?></a>";                                        
                                var options=add_lot;
                                p.actions=options; 
                            }
                            $("#grid_list_stokeincome").jqGrid('addRowData',p.id,p);                
                            grid_list_stokeincome.trigger("reloadGrid");                      
                        });                        
                    }
                }
            });
        });
        
        
        /*************** Agregar detalles ***********/
        
       /* Abrir el Detalle de Productos */
        $('table#grid_list_stokeincome').on('click','.add_detail',function(){         
            idProduct = $.trim($(this).data('id'));
            var p = grid_list_stokeincome.getRowData(idProduct);    
            $("#grid_income_detail").jqGrid("clearGridData");
            if(p.haveDetail==true){                
                $("#input_hidden_id_product").val(p.id);
                $("#input_amount").val(p.amount);             
                $("#input_detail").dialog('open');
                // Ingreso la serie electronicas ya ingresadas
                if(product_detail.get(idProduct) != null){
                    var objectProducts = product_detail.get(idProduct).allDetail.valSet();
                    $.each(objectProducts, function(index,data){
                        $("#grid_income_detail").jqGrid('addRowData',data.serie_electronica,data);
                    });
                }
            }else{
                alert("Usted no tiene esta opción");                
            }            
            return false;
        });
        /* Agregar Detalles */
        $("#btn_add_detail").on('click',function(e){
            e.preventDefault();
            var serie_elec = $('#serie_elect').val(); 
            var detail_serie = $('#description_detail').val();
            if(serie_elec!="" && detail_serie!=""){
                $.ajax({
                   url: '/private/stockincome/validateSerie'+jquery_params
                   , data: {
                       val_serie: serie_elec
                   }
                   , dataType: 'json'
                   , type: 'POST'
                   , success: function(j_response){                       
                       if(evalResponse(j_response)){                           
                           if(j_response.data==null){
                               arrayAllSerie.push($("#serie_elect").val());
                               serie_repeated=0;
                               bucle:
                               for(var i=0; i<arrayAllSerie.length; i++){
                                   aux = arrayAllSerie[i];
                                   for(var j=0; j<arrayAllSerie.length; j++){
                                       if(aux == arrayAllSerie[j]){
                                           serie_repeated++;
                                       }
                                       if(serie_repeated>1){
                                            arrayAllSerie.pop();
                                            msgBox(          
                                                '<?php echo __("Esta Serie Electronica ya se encuentra asignada ") ?>',
                                                '<?php echo __("ERROR") ?>'
                                            ); break bucle; 
                                        }        
                                   }
                                   serie_repeated=0;
                               }
                               if(serie_repeated<2)    
                                    $("#frm_detail_product").submit();   
                           }
                           else{
                              msgBox(
                            '<?php echo __("Esta Serie Electronica ya se encuentra asignada ") ?>',
                            '<?php echo __("ERROR") ?>'
                                );  
                           }
                       }else{
                           msgBox(j_response.msg);
                       }
                   }
               }); 
            }
        });
        /* Validacion de Detalles y agrega al jqGrid */
        var frm_detail_product=$("#frm_detail_product").validate({
            rules:{
                serie_elect:{
                    required:true
                },
                description_detail:{
                    required:true
                }
            },
            messages:{
                serie_elect:{
                    required:'<?php echo __("Ingrese la Serie Electrónica.") ?>'
                },
                description_detail:{
                    required:'<?php echo __("Ingrese la Descripción.") ?>'
                }  
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                  
                var data_total={}; 
                var id=$("#serie_elect").val();
                data_total.serie_electronica=$("#serie_elect").val();
                data_total.idGeneral=$("#input_hidden_id_product").val();
                var arrayDataGrid=$("#grid_income_detail").jqGrid("getRowData");
                if(arrayDataGrid.length>0){ 
                    var j=0;
                    $.each(arrayDataGrid,function(index,data){                                                      
                        if(data.serie_electronica == data_total.serie_electronica){
                            j=j+1;                        
                        }
                    });
                    if(j>0){
                          alert('El producto ya tiene la serie electronica que desea ingresar.');
                          frm_detail_product.currentForm.reset();
                          frm_detail_product.resetForm();
                          $("#serie_elect").focus();
                          return false;
                    }else if(arrayDataGrid.length>=$("#input_amount").val()){
                          alert('Ya completo el total de productos con detalle.');
                          frm_detail_product.currentForm.reset();
                          frm_detail_product.resetForm();
                          $("#serie_elect").focus();
                          return false;
                    }                   
                }else{
                    //GG - No valida serie_electronica 
                }
                var trash = "<a class=\"trash_detail\" style=\"cursor: pointer;\" data-id=\""+id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                var options=trash;                                 
                data_total.description=$("#description_detail").val();
                data_total.actions=options;                
                $("#grid_income_detail").jqGrid('addRowData',id,data_total); 
                grid_income_detail.trigger("reloadGrid"); 
                frm_detail_product.currentForm.reset();
                frm_detail_product.resetForm();
                $("#serie_elect").focus();
            }
        });
        /* Dialog Detalles de Productos */
        $("#input_detail").dialog({
            title:'<?php echo "Ingresar Detalle de Productos"; ?>',
            autoOpen: false,
            width: 800,
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: true, 
            buttons:{
                '<?php echo "Crear" ?>':function(){                      
                    var data=$("#grid_income_detail").jqGrid('getGridParam','data');                     
                    if(data.length != 0 && data.length == $("#input_amount").val()){
                        //Agrego todos los productos detalles
                        var idProduct= $("#input_hidden_id_product").val();
                        var oData = new Object();
                        oData.idGeneral = idProduct;                      
                        oData.allDetail = new  ArrayMap();
                        product_detail.put(idProduct,oData);
                        $.each(data, function(index,objeto){
                              var oDataGeneral = product_detail.get(idProduct);
                              var oSubData = new Object();
                              oSubData.serie_electronica = objeto.serie_electronica;
                              oSubData.description = objeto.description; 
                              var trash = "<a class=\"trash_detail\" style=\"cursor: pointer;\" data-id=\""+objeto.serie_electronica+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                              var options=trash;   
                              oSubData.actions = options;
                              oDataGeneral.allDetail.put(objeto.serie_electronica,oSubData);
                        });
                        $("#grid_income_detail").jqGrid("clearGridData");
                        $(this).dialog('close');
                        $("#grid_list_stokeincome").jqGrid('setCell', idProduct, 'lote', 'No');
                        //Aqui Sera Para Agregar el producto de manera general                                                
                        var id=$("#input_hidden_id_product").val();
                        if(!$("#for_purchase").is(":checked")){                            
//                            var p={};   
//                            p.id=id;                            
//                            p.haveDetail=$("#input_hidden_have_detail").val();
//                            p.codigo=$("#input_hidden_code_product").val();
//                            p.producto=$("#input_product").val();
//                            p.price=$("#input_price_product").val();
//                            p.saleprice=$("#input_saleprice_product").val();
//                            p.amount=parseInt($("#input_amount").val(),10);
//                            p.importe=$("#input_importe_product").val(); 
//                            p.importesale=$("#input_importesale_product").val(); 
//                            $("#grid_list_stokeincome").jqGrid('addRowData',id,p);
//                            $("#grid_list_stokeincome").trigger("reloadGrid");
                            $("#input_search_product").focus();
//                            
                        }    
                        frm_detail_product.currentForm.reset();
                        frm_product.currentForm.reset();
                        frm_product.resetForm();
                        $("#input_hidden_stock").val(0);
                        $("#input_search_product").helptext('refresh');
                    }else{     
                        // Nro que le falta ingresar
                        var nro = $("#input_amount").val()- data.length;
                        msgBox('Aun falta ingresar '+nro+' productos con detalle');
                    }
                },
                '<?php echo "Cancelar" ?>':function(){                    
                    $(this).dialog('close');
                    $("#grid_income_detail").jqGrid("clearGridData");
                    $("#input_hidden_id_product").val('');
                    frm_detail_product.currentForm.reset();
//                    frm_product.currentForm.reset();
//                    frm_product.resetForm();
                }
            } 
        });
        /* JQGRID de Productos Detalles */
        var grid_income_detail=$("#grid_income_detail").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Serie Electrónica") ?>',                           
                '<?php echo __("Descripción") ?>',                
                '<?php echo __('Acciones'); ?>'
            ],
            colModel:[
                {name:'idGeneral', index:'idGeneral',hidden:true,width:50},
                {name:'serie_electronica',index:'serie_electronica',align:'center'},                      
                {name:'description',index:'amount',align:'center',search:true},                                 
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_stokeincome_pager',  
            gridview: true,
            rowNum:10,   
            sortorder: "asc",            
            width:750,
            height:220,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            caption:'<?php echo __("Lista Detalles de Productos") ?>',
            gridComplete:function(){             
                $('.trash_detail').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
            }
        });
        /* Eliminar Detalles */
        $('table#grid_income_detail').on('click','.trash_detail',function(){  
            var id = $(this).data('id'); // serie electronica
            var p=$("#grid_income_detail").jqGrid('getRowData',id);
            var index_serie = arrayAllSerie.indexOf(p['serie_electronica']);
            console.log(arrayAllSerie);
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){ 
                    $("#grid_income_detail").jqGrid('delRowData',p.serie_electronica);  
                    grid_income_detail.trigger("reloadGrid");
                    arrayAllSerie.splice( index_serie, 1 );
                    console.log(arrayAllSerie);
                }
            });
            return false;
        });
        
        /************** Agregar lotes **************/
        
        /* Abrir el Lote de Productos */
        $('table#grid_list_stokeincome').on('click','.lot',function(){ 
            idProduct = $.trim($(this).data('id'));
            $('input#add_id_prod').val(idProduct);
            data = grid_list_stokeincome.getRowData(idProduct);
            $('input#add_icont_prod').val(data.idmovdetail);
            $('input#add_lot_description').val(data.lote);
            $('#frmAddLotDiv').dialog('open');
            
        }); 
        /* Dialog de Lotes */
        $("#frmAddLotDiv").dialog({
            title: asignarLot,
            autoOpen: false,
            width: "auto",
            show: "slide",
            hide: "slide",
            modal:true,            
            buttons:{
                '<?php echo "Aceptar" ?>':function(){
                     $("#frmAddLot").submit();
                },
                '<?php echo "Cancelar" ?>':function(){
                    $(this).dialog('close');
                    frm_add_Lots.currentForm.reset();
                }
            }
//            open: function() {
//                $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('bt_aceptar_addlot');
//                $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('bt_cancel_addlot');
//                $('.bt_aceptar_addlot').button({                    
//                    icons: {
//                        primary: 'ui-icon-circle-check'
//                    }
//                    , text: true
//                }); 
//                $('.bt_cancel_addlot').button({                    
//                    icons: {
//                        primary: 'ui-icon-circle-close'
//                    }
//                    , text: true
//                }); 
//            }
        });     
        /* Validacion de Lotes */
        var frm_add_Lots=$("#frmAddLot").validate({
            rules:{
                add_lot_description:{
                    required:true
                }                              
            },
            messages:{
                add_lot_description:{
                    required:'<?php echo __("Nombre de Lote Obligatorio.") ?>'
                }  
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                          
                var idProduct=$("#add_id_prod").val();
                var icount=$("#add_icont_prod").val();
                var lot=$("#add_lot_description").val();                  
                $("#grid_list_stokeincome").jqGrid('setCell', idProduct, 'lote', lot);
                $("#grid_list_stokeincome").jqGrid('setCell', idProduct, 'idmovdetail', countLot);
                var renDatos = $('#grid_list_stokeincome').getRowData(idProduct);
                // Guardar los lotes
                var oData = new Object();
                oData.idGeneral = idProduct;                      
                oData.nameLot = renDatos.lote;
                product_lot.put(idProduct, oData);         
                $("#frmAddLotDiv").dialog('close');
                $("#grid_list_stokeincome").trigger("reloadGrid"); 
                frm_add_Lots.currentForm.reset();
                frm_add_Lots.resetForm();
                frm_product.currentForm.reset();
                frm_product.resetForm();
                $("#input_search_product").focus();                      
            }
        });
        
        
        
        
        /****************    Guardar y Cancelar Orden de Compra    *******************/
        /* Validacion de Orden de Compra */
        var frm_content=$("#frm_content").validate({
            rules:{
                input_date_income:{
                    required:true
                },
                'select_purchaseOrders-combobox':{
                    required:function(){
                        return $("#select_purchaseOrders-combobox:visible").length>0;
                    }
                },
                select_store:{
                    required:true
                    //validAsignStore:true
                },
                select_purchaseOrders:{
                    required:function(){
                        return $("#select_purchaseOrders:visible").length>0;
                    }
                },        
                text_doc_ref:{
                    required:function(){
                        return $("#text_doc_ref:visible").length>0;
                    }                    
                },                                
                'select_typeMove-combobox':{
                    required:true                    
                },                                
                select_typeMove:{
                    required:true                    
                },                                                              
                'select_supplier-combobox':{
                    required:true
                } ,   
                'select_store-combobox':{
                    required:true
                } , 
                select_supplier:{
                    required:true
                },                               
                'select_currency-combobox':{
                    required:true
                },                        
                select_currency:{
                    required:true
                }                               
            },
            messages:{
                input_date_income:{
                    required:'<?php echo __("Ingrese Una Fecha") ?>'
                },
                'select_purchaseOrders-combobox':{
                    required:'<?php echo __("Seleccione Una Orden de Compra") ?>'
                },
                select_purchaseOrders:{
                    required:'<?php echo __("Seleccione Una Orden de Compra") ?>'
                },
                text_doc_ref:{
                    required:'<?php echo __("Ingrese Un Documento de Referencia") ?>'                  
                },                                
                'select_typeMove-combobox':{
                    required:'<?php echo __("Seleccione Un Tipo de Movimiento") ?>'
                },                                
                select_typeMove:{
                    required:'<?php echo __("Seleccione Un Tipo de Movimiento") ?>'
                },                                                              
                select_store:{
                    required:'<?php echo __("Asignar un Almacén") ?>'
                    //validAsignStore:'<?php echo __("Asignese un Almacén") ?>'
                },                              
                'select_supplier-combobox':{
                    required:'<?php echo __("Seleccione Un Proveedor") ?>'
                } ,                              
                'select_store-combobox':{
                    required:'<?php echo __("Seleccione Un Almacén") ?>'
                } ,                              
                select_supplier:{
                    required:'<?php echo __("Seleccione Un Proveedor") ?>'
                },                               
                'select_currency-combobox':{
                    required:'<?php echo __("Seleccione Una Moneda") ?>'
                },                        
                select_currency:{
                    required:'<?php echo __("Seleccione Una Moneda") ?>'
                } 
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
        });
        /* Guardar Orden de Compra */        
        $("#btn_register").on('click',function(e){
            e.preventDefault();
            if(!$('div.divDocInfo').hasClass('ui-state-error')){
                if(!$("#frm_content").valid())
                    return false;  
                // Productos
                var arrayDataGrid=$("#grid_list_stokeincome").jqGrid("getRowData");
                var ind_f = 0;
                var ind_l = 0;
                $.each(arrayDataGrid,function(index,value){
                    if(value.lote == ''){
                        ind_l++; 
                    }
                    ind_f++;
                });                               
                
//                if($("input[name=for_income]:checked").val()=='purchase'){
//                    flags.typeIncome = 1;
//                }else if($("input[name=for_income]:checked").val()=='other'){
//                    flags.typeIncome = 0;
//                }
                if(ind_l == 0){                    
                    if(arrayDataGrid.length>0){
                        confirmBox  ('<?php echo __("Desea Confirmar el Ingreso de Productos?"); ?>','Registrar Ingreso',function(event){
                            if(event){
                                var obj_frm_content=$("#frm_content").serializeObject();
                                obj_frm_content.select_store=$("#select_store").val();
//                                showLoading=1;
                                var array_detail_product = {};
                                $.each((product_detail.valSet()),function(index, data){
                                    var te_q_m = {};
                                    $.each((data.allDetail.valSet()),function(i,val){
                                          te_q_m[i]=val;  
                                    });
                                    array_detail_product[data.idGeneral]= te_q_m;
                                    
                                });
                                var array_lot_product = {};
                                $.each((product_lot.valSet()),function(index, data){
                                    array_lot_product[index] = data;                                    
                                });    
                                $.ajax({                
                                    url:url_createIncomeStock+jquery_params,                        
                                    type:'POST',
                                    dataType:'json',
                                    data:{
                                        array_detail_product:array_detail_product,
                                        array_product:arrayDataGrid,
                                        array_lot_product:array_lot_product,
                                        obj_frm:obj_frm_content                               
                                    }, 
                                    async:false,
                                    success:function(response){  
                                        if(response.code == code_success){
                                            $("#btn_cancel").trigger('click');
                                            getAllPurchaseOrder();
                                        }
                                    }
                                });                       
                            }
                        });                                 
                    }else{
                        msgBox('Usted No Tiene Productos Para Ingreso','Alerta de Ingreso');
                    }
                
                } else{
                    msgBox('<?php echo __("- Debe Ingresar todos los Lotes.<br/> - Debe Ingresar todos los Detalles ") ?>'+'</b>','<?php echo __("Alerta") ?>');
                } 
            }
            return false;            
        });
        
        /* Cancelar Orden de Compra */
        $("#btn_cancel").on('click',function(e){
            e.preventDefault();                         
         //   frm_product.currentForm.reset();
         //   frm_product.resetForm();
            frm_content.currentForm.reset();
            frm_content.resetForm();  
            if($("input[name=for_income]:checked").val()=='purchase'){                
                $(".purchase").show();
                $(".doc_ref").hide();
                $("#div_add_product").hide().fadeOut();
            }            
            product_detail.clear();
            product_lot.clear();
            $("#input_hidden_stock").val(0);
          //  edit_product_income=0;                                    
            $("#grid_list_stokeincome").jqGrid("clearGridData");
       //     $("#input_search_product").helptext('refresh');
        });
        fnLoadCheck();
    }); 
       
</script>
<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;" <?php endif ?> >
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4><?php echo __('Ingreso De Productos - Almacén'); ?></h4>
        </div>
    </div>
    <?php if ($bAvailableDocumentType): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix">
        </div>
    <?php endif ?>
    <div class="clear"></div>
</div>
<div id="income_content" class="ui-widget-content ui-corner-all">     
    <div>        
        <form id="frm_content">
            <div class="cFieldSet ui-widget-content">             
                <div id="for_div_1">
                    <div>
                        <label><?php echo __("Fecha de Ingreso"); ?>: </label>
                        <input type="text" id="input_date_income" name="input_date_income" value="<?php echo $today; ?>" />
                    </div>
                    <div id="for_income">
                        <label><?php echo __("Ingreso Por"); ?>: </label>
                        <input type="radio" id="for_purchase" name="for_income" value="purchase" checked="checked"/><label for="for_purchase">Compras</label>
                        <input type="radio" id="for_other" name="for_income" value="other" /><label for="for_other">Otros</label>
                        <div class="clear"></div>
                    </div>                
                </div>
                <div id="for_div_2" class="ui-widget ui-widget-content">
                    <div class="purchase">
                        <label><?php echo __("Orden de Compra"); ?>: </label>
                        <select id="select_purchaseOrders" name="select_purchaseOrders">                       
                        </select>
                    </div>
                    <div class="doc_ref">
                        <label><?php echo __("Doc. Referencia"); ?>: </label>
                        <input type="text" id="text_doc_ref" name="text_doc_ref" />
                    </div>
                    <div>
                        <label><?php echo __("Concepto Movimiento"); ?>: </label>
                        <select id="select_typeMove" name="select_typeMove">
                            <option value=""><?php echo __("Seleccionar Una Opción..."); ?></option>
                            <?php foreach ($typeMove as $v) {
                                ?>
                                <option value="<?php echo $v['id']; ?>"><?php echo $v['display']; ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                </div>            
                <div id="for_div_3">
                    <!--                    <div>
                                            <label><?php echo __("Almacen"); ?>: </label>                         
                                            <input id="select_store" name="select_store" value="<?php echo (count($store) != 0) ? $store[0]['display'] : 'ASIGNESE UN ALMACEN'; ?>" data="<?php echo (count($store) != 0) ? $store[0]['id'] : '0'; ?>" class="<?php echo (count($store) == 0) ? 'ui-state-error' : 'ui-state-highlight'; ?> ui-corner-all" readonly="" style="padding: 7px;font-weight: bolder;" />
                                        </div>-->

                    <div>
                        <label class="label_text"><?php echo __("Almacén") ?>: </label>
                        <select id="select_store" name="select_store" class="item_content">
                            <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                            <?php foreach ($array_store as $va): ?>                                   
                                <option value="<?php echo $va->idStore ?>"><?php echo $va->storeShortName ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div>
                        <label><?php echo __("Proveedor"); ?>: </label>
                        <select id="select_supplier" name="select_supplier" >
                            <option value=""><?php echo __("Seleccionar Una Opción..."); ?></option>
                            <?php foreach ($supplier as $v) {
                                ?>
                                <option value="<?php echo $v['id']; ?>"><?php echo $v['display']; ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                    <div id="for_currency">
                        <label><?php echo __("Tipo Moneda"); ?>: </label>
                        <dd>
                            <?php foreach ($currency as $sKey => $aCurrency): ?>    
                                <?php echo Helper::fnGetDrawJqueryButton($aCurrency, 'currency', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aCurrency['display'])))) ?>                                            
                            <?php endforeach ?>   
                        </dd>
                    </div>
                </div>
                <div class="clear"></div>            
            </div>
        </form>
        <div class="div_grid_stokeincome"> 
            <div id="div_add_product" class="ui-corner-all ui-widget-content">
                <form id="frm_product">
                    <div>                     
                        <div>
                            <fieldset id="field_product">
                                <legend><?php echo __("Búsqueda de Producto"); ?></legend>
                                <div id="check_type_search_product">
                                    <input type="radio" id="search_product_code" name="check_type_search_product" checked="checked" value="search_code" /><label for="search_product_code"><?php echo __("Código"); ?></label>
                                    <input type="radio" id="search_product_desc" name="check_type_search_product" value="search_description" /><label for="search_product_desc"><?php echo __("Modelo"); ?></label>                 
                                </div>
                                <div id="div_input_search_product">                                
                                    <input type="text" id="input_search_product" name="input_search_product" size="40" />
                                    <!--<button id="btn_new_product"><?php echo __("Nuevo Producto"); ?></button>-->                                
                                    <div class="clear"></div>
                                </div>                            
                            </fieldset>
                        </div>
                        <div id="div_product_find">
                            <div>
                                <label><?php echo __("Producto"); ?>: </label>
                                <input type="text" id="input_product" name="input_product" size="38" readonly="" />
                                <input type="hidden" id="input_hidden_id_product" name="input_hidden_id_product"  />
                                <input type="hidden" id="input_hidden_code_product" name="input_hidden_code_product"  />
                                <input type="hidden" id="input_hidden_have_detail" name="input_hidden_have_detail"  />
                            </div>
                            <div>
                                <div class="two_colum">                                
                                    <label><?php echo __("Medida"); ?>: </label>
                                    <input type="text" id="input_measure" name="input_measure" size="10" readonly=""  />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div>
                                <div class="two_colum" id="div_amount">
                                    <label><?php echo __("Cantidad"); ?>: </label>
                                    <input type="text" id="input_amount" name="input_amount" size="10" value="0" alt="number" autocomplete="off" />
                                </div>
                                <div class="two_colum" id="div_stock">
                                    <label><?php echo __("Stock"); ?>: </label>
                                    <input type="text" id="input_stock" name="input_stock" size="10" value="0" readonly="" />                                    
                                    <input type="hidden" id="input_hidden_stock" name="input_hidden_stock" value="0" size="10" readonly="" />                                    
                                </div>
                                <div class="clear"></div>
                            </div>  
                            <div class="ui-widget-content ui-corner-all" style="padding: 5px;">
                                <div style="padding: 5px 0px;">
                                    <div class="two_colum">
                                        <label><?php echo __("Precio Compra"); ?>: </label>
                                        <input type="text" id="input_price_product" name="input_price_product" value="0" size="10" autocomplete="off" /> 
                                    </div>                      
                                    <div class="two_colum">
                                        <label><?php echo __("Precio Venta"); ?>: </label>
                                        <input type="text" id="input_saleprice_product" name="input_saleprice_product" value="0" size="10" autocomplete="off" /> 
                                    </div>  
                                    <div class="clear"></div>
                                </div>
                                <div style="padding: 5px 0px;">
                                    <div class="two_colum">
                                        <label><?php echo __("Importe Comp."); ?>: </label>
                                        <input type="text" id="input_importe_product" name="input_importe_product" value="0" size="10" readonly="" /> 
                                    </div>  
                                    <div class="two_colum">
                                        <label><?php echo __("Importe Ven."); ?>: </label>
                                        <input type="text" id="input_importesale_product" name="input_importesale_product" value="0" size="10" readonly="" /> 
                                    </div>  
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                        <div id="buttons_add_product">
                            <button id="btn_add_product"><?php echo __("Agregar Producto"); ?></button>
                            <button id="btn_clear_product"><?php echo __("Limpiar"); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="div_grid_income">
<!--                <fieldset id="sec_button_lotsAll" style="width: 150px; height: 70px;padding-top:10px; float: right;" class="ui-corner-all ui-state-processing ui-widget-content">
                    <legend style="padding: 5px; margin-left: 10px;" class="ui-corner-all ui-widget-header"><?php //echo __("Asignar Lote"); ?></legend>  
                    <div>
                        <label><?php //echo __("Asignar Lote para todos los Productos"); ?>: </label>
                        <button id="addAllLots" name="addAllLots"><?php //echo __("Agregar Lote"); ?></button>
                    </div>
                </fieldset>-->
                <div class="clear"></div>
                <br/>
                <table id="grid_list_stokeincome"> </table>
                <div id="grid_list_stokeincome_pager"></div>
            </div> 
            <div class="clear"></div>
        </div>        
        <div class="clear"></div>
    </div>
    <div id="btn_buttons">
        <button id="btn_register"><?php echo __("Registrar"); ?></button>
        <button id="btn_cancel"><?php echo __("Cancelar"); ?></button>        
    </div>
</div>

<!--Dialog de Productos Detalle-->
<div id="input_detail" class="dialog">
    <div>
        <form id="frm_detail_product">
            <div>
                <label><?php echo __('Serie Electrónica') ?>: </label>
                <input type="text" id="serie_elect" name="serie_elect" class="ui-widget ui-widget-content" autocomplete="OFF" />
            </div>
            <div>
                <label><?php echo __('Descripción') ?>: </label>
                <input type="text" id="description_detail" name="description_detail" class="ui-widget ui-widget-content" autocomplete="OFF" />
            </div>
            <div style="margin-left:100px;">
                <button id="btn_add_detail"><?php echo __('Agregar Detalle') ?></button>
            </div>
        </form>
    </div>
    <div>
        <table id="grid_income_detail"> </table>
        <div id="grid_income_detail_pager"></div>
    </div>
</div>

<!--Dialog de Productos Lotes-->
<div id="frmAddLotDiv">
    <div class="ui-widget-content ui-corner-all">
        <form id="frmAddLot">  
            <div style="margin:20px;" class="none">
                <label style="font-weight: bolder;"><?php echo __('ID') ?>: </label>
                <input type="text" id="add_id_prod" name="add_id_prod" class="ui-widget ui-widget-content" autocomplete="OFF" />
            </div>
            <div style="margin:20px;" class="none">
                <label style="font-weight: bolder;"><?php echo __('ICount') ?>: </label>
                <input type="text" id="add_icont_prod" name="add_icont_prod" value="" class="ui-widget ui-widget-content" autocomplete="OFF" />
            </div>
            <div style="margin:20px;">
                <label style="font-weight: bolder;"><?php echo __('Nombre Lote') ?>: </label>
                <input type="text" id="add_lot_description" name="add_lot_description" class="ui-widget ui-widget-content" autocomplete="OFF" />
            </div>
        </form>    
    </div>

</div>