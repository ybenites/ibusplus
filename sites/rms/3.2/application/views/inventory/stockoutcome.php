<style type="text/css"> 
    .clear{
        clear: both;
    }
    #outcome_content{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
        padding: 20px;
        margin-top: 10px;
        margin-bottom: 30px;
    }
    #type_doc_move{
        padding: 10px;
        max-width: -moz-fit-content;
        margin-left: auto;
        margin-right: auto;
    }
    .div_grid_stokeoutcome{        
        padding-top: 15px;
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    .cFieldSet{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
        border-bottom-style: dashed;
        border-left: none;
        border-right: none;
        border-top: none;
        border-width: 3px;
        padding-bottom: 25px;
    }
    .cFieldSet>div>div{
        padding: 5px;
    }
    .cFieldSet .ui-buttonset .ui-button {
        width: auto;
        margin-top: 0;
    } 
    .cFieldSet>div>div>label{        
        float: left;
        font-weight: bolder;
        margin-top: 7px;
        padding-right: 10px;
        text-align: right;
        width: 140px;
    }

    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px;               
    }
    #for_div_1{
        float: left;
        padding-right: 15px;        
    }
    #for_div_2{
        float: left;
        padding-left: 15px;
        padding-right: 15px;  
        border-bottom: none;
        border-left-style: dashed;
        border-right-style: dashed;        
        border-top: none;
        border-width: 3px;
    }
    #for_div_2>div>input{
        width: 150px;
    }
    #for_div_3{        
        float: left;
    }
    #for_div_3>div>input{
        width: 230px;        
    }
    #btn_buttons{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
        padding-top: 15px;
    }
    #buttons_add_product{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;        
    }
    #div_less_product{
        float: left;
        margin-right: 15px;
        padding: 10px;
        width: 400px;   
        border-style: groove;        
        border-width: 5px;
    }
    #div_grid_outcome{
        float: left;
    }
    #check_type_search_product{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    #div_input_search_product input{
        height: 18px;
    }
    #div_input_search_product{
        margin-right: auto;
        margin-left: auto;
        width:250px;
    }
    #field_product{
        padding: 10px;
    }
    #field_product>div{
        padding:5px;
    }
    .two_colum{
        float: left;
    }

    #div_product_find{
        padding: 7px;
    }
    #div_product_find input{
        height:19px;
    }
    #div_product_find>div{
        padding: 5px;
    }
    #div_product_find>div label{
        font-weight: bolder;
        float: left;
        min-width: 70px;
        padding-top: 5px;
        text-align:right;
        padding-right: 3px;
    }    

    .divDocInfo{float:right;}
    input.helptext{font-style: italic;color: #CCCCCC;}
    div.divDocInfo{font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}

    #input_detail>div{
        padding-top:10px;
    }
    #grid_uno{
        float: left;
        padding-right:20px;
    }
    #grid_dos{
        float: left;
    }
    /*Ocultar Salida X */
    .ui-dialog-titlebar-close{
        display: none;
    }
    .lot_qua{float: left;margin: 7px;padding: 7px;box-shadow: 3px 3px;}
    .frm_class{display: inline-table;    font-weight: bolder;    padding-right: 5px;    text-align: right;    width: 60px;}
</style>
<script type="text/javascript">   
    var url_getAllSalesOrder="/private/stockoutcome/getAllSalesOrder"; 
    var url_getDataSearchAutocompleteProduct="/private/stockoutcome/getDataSearchAutocompleteProduct"; 
    var url_createOutcomeStock="/private/stockoutcome/createOutcomeStock"; 
    var url_getProductById="/private/stockoutcome/getProductById"; 
    var getProductLotById="/private/stocktransfer/getProductLotById"; 
    var url_getDataGeneralBySalesOrder="/private/stockoutcome/getDataGeneralBySalesOrder";       
    var url_compare_data_outcome="/private/stockoutcome/getDataByDetailProduct";       
    var url_getClientsAutocomplete="/private/stockoutcome/getClients";       
    var url_getDateilByProductId="/private/stockoutcome/getDateilByProductId";       
    var msg_warn_delete="<?php echo __('Desea eliminar este Producto de la Lista'); ?>";
    var title_delete="<?php echo __('Eliminar Producto de la Compra') ?>";
    var edit_product_outcome=0;  
    var type_document='<?php echo $type_document; ?>';
    var detail_form_Title = '<?php echo __("Detalle de Otros Productos"); ?>';
    var array_detail_id_p=[];
    var array_detail_id_pl=[];
    $(document).ready(function(){
        $("#btn_less_product").button({
            icons: {
                primary: "ui-icon-minusthick"                
            },
            text:true
        });        
        $("#btn_clear_product").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        });        
        $("#btn_register").button({
            icons: {
                primary: "ui-icon-check"                
            },
            text:true
        });        
        $("#btn_cancel").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        });         
        $("#for_outcome").buttonset();        
        $("#for_currency").buttonset();        
        $("#check_type_search_product").buttonset();                
        $("#select_typeMove").combobox();        
        $("#input_date_outcome").datepicker({
            dateFormat:'<?php echo $jquery_date_format ?>'
        });
        $(".doc_ref").hide();
        $("#div_less_product").hide();
        
        $("input[name=for_outcome]").change(function(){             
            $("#input_hidden_stock").val(0);
            edit_product_outcome=0;
            array_detail_id_p=[];
            frm_product.currentForm.reset();
            frm_product.resetForm();             
            $(frm_content.currentForm).find('input[type=text],input[type=hidden],textarea,select').val('');
            $("#grid_list_stokeoutcome").jqGrid("clearGridData");
            if($(this).val()=='sell'){
                $(".purchase").show();
                $(".doc_ref").hide();
                $("#div_less_product").hide().fadeOut();
                $('#select_customer').val('<?php echo __("Buscar Cliente")?>');
                $("#input_date_outcome").val('<?php echo $today ?>');
            }else if($(this).val()=='other'){                
                $(".purchase").hide();
                $(".doc_ref").show();
                $("#div_less_product").show().fadeIn();
                $('#select_customer').val('<?php echo __("Buscar Cliente")?>');
                $("#input_date_outcome").val('<?php echo $today ?>');
            }                            
        });
        $("#select_salesOrders").combobox({
            selected:function(event,ui){ 
                $("#select_salesOrders").trigger('change');
            }
        });
       
        $("#select_salesOrders").on('change',function(){
            $("#input_hidden_stock").val(0);
            edit_product_outcome=0; 
            array_detail_id_p=[];
            showLoading=1;
            var id_sales_order=$(this).val();            
            $.ajax({                
                data:{
                    id_sales_order:id_sales_order
                },                    
                type:"POST",
                dataType:"json",
                url:url_getDataGeneralBySalesOrder+jquery_params,
                success:function(res){                    
                    if(evalResponse(res))               
                    {                        
                        var general=res.dataGeneral;                         
                        var detail=res.dataDetail;                        
                        $("#select_customer").val(general.customersName);
                        $("#select_customer").attr('rel',general.idCustomer);                                                                      
                        $("#select_customer").removeClass('helptext');
                        
                        $("#grid_list_stokeoutcome").jqGrid("clearGridData");
                        if(detail.length>0){
                            $.each(detail,function(index,p){  
                                if(p.haveDetail==true){ 
                                    var add_detail = "<a class=\"add_detail\" style=\"cursor: pointer;\" data-id=\""+p.idProduct+"\" title=\"<?php echo __('Agregar Detalle'); ?>\" ><?php echo __('Agregar Detalle'); ?></a>";                                        
                                    var options=add_detail;
                                    p.actions=options;
                                }                                
                                $("#grid_list_stokeoutcome").jqGrid('addRowData',p.idProduct,p);                                                      
                            }); 
                        }else{
                            msgBox('<?php echo __("Posiblemente Tiene que ir a otro Almacén") ?>','<?php echo __("No hay Productos") ?>')
                        }                                                                
                        grid_list_stokeoutcome.trigger("reloadGrid");
                    }
                }
            });
        });
        getAllSalesOrder();
        function getAllSalesOrder(){
            showLoading=1;
            $.ajax({                
                url:url_getAllSalesOrder+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{
                }, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        var r=response.data;                        
                        var template="<option value=''><?php echo __('Seleccione Una Opción...'); ?></option>";
                        $.each(r, function(index,data){
                            template=template+"<option value='"+data.id+"'>"+data.display+"</option>"
                        });
                        $("#select_salesOrders").html(template);                        
                    }
                }
            });
        }
        var grid_list_stokeoutcome=$("#grid_list_stokeoutcome").jqGrid({            
            datatype: "local",
            mtype: 'POST',            
            colNames:[ 
                '<?php echo __("id") ?>',
                '<?php echo __("Tiene Detalle") ?>',
                '<?php echo __("Código") ?>',
                '<?php echo __("Producto") ?>',                
                '<?php echo __("Cantidad") ?>',                               
                '<?php echo __("Precio Unitario") ?>',                
                '<?php echo __("Importe") ?>',                
                '<?php echo __("Acciones") ?>'                
            ],
            colModel:[
                {name:'id',index:'id',hidden:true,width:50},
                {name:'haveDetail',index:'haveDetail',hidden:true,width:50},
                {name:'codigo',index:'codigo',align:'center'},
                {name:'producto',index:'producto',align:'center',search:true},          
                {name:'amount',index:'amount',align:'center',search:true}, 
                {name:'price',index:'price',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                               
                {name:'importe',index:'importe',align: 'right',sorttype:'number',formatter:'number', summaryType:'sum',summaryTpl : '{0}'},                                                               
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_stokeoutcome_pager', 
            //            sortname: 'st.idStore',         
            gridview: true,
            rowNum:10,   
            sortorder: "asc",            
            width:750,
            height:220,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            caption:'<?php echo __("Lista De Productos") ?>',
            grouping:true,
            groupingView : { 
                groupField : ['producto'],
                groupColumnShow: [false],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,                    
                groupSummary : [true], 
                showSummaryOnHide: true,
                groupDataSorted : true
            },
            gridComplete:function(){                      
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.add_detail').button({
                    icons:{primary: 'ui-icon-note'},
                    text: false
                });
            }
        });
        $.validator.addMethod("validStockAvible", function(value, element, params) {            
            return  parseFloat(value)< 0?false:true; 
        }, "<?php echo __('El Stock Es Menor que 0') ?>");
        $.validator.addMethod("validPrice", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        $.validator.addMethod("validAmount", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        var frm_product=$("#frm_product").validate({
            rules:{
                input_product:{
                    required:true
                },
                input_measure:{
                    required:true
                },
                input_amount:{
                    required:true,
                    validAmount:true
                },                                
                input_stock:{
                    required:true,
                    validStockAvible:true
                },                                
                input_price_product:{
                    required:true,
                    validPrice:true
                },                                
                input_importe_product:{
                    required:true
                }                               
            },
            messages:{
                input_product:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_measure:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_amount:{
                    required:'<?php echo __("Ingrese La Cantidad") ?>',
                    validAmount:'<?php echo __("Ingrese La Cantidad Mayor que Cero") ?>'
                },                                
                input_stock:{
                    required:'<?php echo __("No puede ser vacio el stock") ?>',
                    validStockAvible:'<?php echo __("El stock no puede ser negativo") ?>'
                },                                
                input_price_product:{
                    required:true,
                    validPrice:'<?php echo __("El Precio no Puede Ser Cero") ?>'
                },                                
                input_importe_product:{
                    required:'<?php echo __("El Importe no Puede se vacio") ?>'
                }  
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        $.validator.addMethod("validAsignStore", function(value, element, params) {
            return value=="ASIGNESE UN ALMACEN"?false:true; 
        }, "<?php echo __('Asignese un Almacén') ?>");
        var frm_content=$("#frm_content").validate({
            rules:{
                input_date_income:{
                    required:true
                },
                'select_salesOrders-combobox':{
                    required:function(){
                        return $("#select_salesOrders-combobox:visible").length>0;
                    }
                },
                select_salesOrders:{
                    required:function(){
                        return $("#select_salesOrders:visible").length>0;
                    }
                }, 
                select_store:{
                    required:true,
                    validAsignStore:true
                },
                text_doc_ref:{
                    required:function(){
                        return $("#text_doc_ref:visible").length>0;
                    }                    
                },                                
                'select_typeMove-combobox':{
                    required:true                    
                },                                
                select_typeMove:{
                    required:true                    
                },
                idCustomers:
                {
                    required:true
                },               
                input_date_outcome:
                    {
                    required:true
                    }
            },
            messages:{
                input_date_income:{
                    required:'<?php echo __("Ingrese Una Fecha") ?>'
                },
                'select_salesOrders-combobox':{
                    required:'<?php echo __("Seleccione Una Orden de Compra") ?>'
                },
                select_salesOrders:{
                    required:'<?php echo __("Seleccione Una Orden de Compra") ?>'
                },
                select_store:{
                    required:'<?php echo __("Asignar un Almacén") ?>',
                    validAsignStore:'<?php echo __("Asignese un Almacén") ?>'
                },
                text_doc_ref:{
                    required:'<?php echo __("Ingrese Un Documento de Referencia") ?>'                  
                },                                
                'select_typeMove-combobox':{
                    required:'<?php echo __("Seleccione Un Tipo de Movimiento") ?>'
                },                                
                select_typeMove:{
                    required:'<?php echo __("Seleccione Un Tipo de Movimiento") ?>'
                },
                idCustomers:{
                    required:'<?php echo __("Seleccione un Cliente") ?>'
                },
                input_date_outcome: {
                    required:'<?php echo __("Seleccione la Fecha de Ingreso") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        $("#input_amount").setMask();
        $("#input_amount").keyup(function(e){                  
            var stock_static=$("#input_hidden_stock").val();
            var quantity=$(this).val();            
            setTimeout(function(){
                calcStock(stock_static,quantity);
            },50);
        });
        $("#input_price_product,#input_importe_product").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        $("#input_price_product").keyup(function(e){
            var quantity=$("#input_amount").val();
            var price=$(this).val();
            setTimeout(function(){                
                calcStock($("#input_hidden_stock").val(),quantity);
            },50);
        });
        $("#btn_less_product").on('click',function(e){
            e.preventDefault();
            if(!$("#frm_product").valid())
                return false;
            var id=$("#input_hidden_id_product").val();                                 
            var trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
            var options = trash;                    
            var p={};   
            p.id=id;            
            p.codigo=$("#input_hidden_code_product").val();
            p.haveDetail=$("#input_hidden_have_detail").val();
            p.producto=$("#input_product").val();
            p.price=$("#input_price_product").val();
            p.amount=parseInt($("#input_amount").val(),10);
            p.importe=$("#input_importe_product").val();
            p.actions= options;
            
            var arrayDataGrid=$("#grid_list_stokeoutcome").jqGrid("getRowData");            
            if(arrayDataGrid.length>0){ 
                var j=0;
                $.each(arrayDataGrid,function(index,data){                                                      
                    if(data.codigo==p.codigo){
                        j=j+1;                        
                    }
                });
                if(j>0){
                    msgBox('Ya existe el Producto agregrado.','Alerta');
                    return false;
                }                    
            }
                        
            if(p.haveDetail==true){
                var idPG=$("#input_hidden_id_product").val();
                var idStore=$("#select_store").attr('data');
                fn_get_detail_product(idPG,idStore);
                return false;
            }else{
                var idPG=$("#input_hidden_id_product").val();
                var idStore=$("#select_store").attr('data');
                fn_get_lot_product(idPG,idStore);
                return false;
            }            
        });
        //FUNCIÓN DE PRODUCTO DETALLE
        function fn_get_detail_product(idPG,idStore){
            showLoading=1;
            $.ajax({
                url:url_getDateilByProductId+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{idpg:idPG,idstore:idStore}, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        r=response.data;                        
                        $.each(r,function(index,data){
                            var pd={};                            
                            pd.idGeneral=data.idProductDetail;
                            pd.description=data.description;
                            pd.serie_electronica=data.serie_electronic;
                            $("#grid_income_detail").jqGrid('addRowData',data.idProductDetail,pd); 
                        });
                        grid_list_stokeoutcome.trigger("reloadGrid"); 
                        $("#input_detail").dialog('open');
                    }
                }
            });
        }
        //FUNCIÓN DE LOTES 
        function fn_get_lot_product(idpg,idstore){
            showLoading=1;
            $.ajax({
                url:getProductLotById+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{idpg:idpg,idstore:idstore}, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){    
                        // Lotes  
                        var r=response.data;                        
                        var template='';                                                        
                        if(r.length>0){                                                        
                            var product="<div><label class='frm_class' style='margin-left: 7px;text-align: left;width: 110px;'><?php echo __('Lotes del Producto'); ?>:</label><span>"+r[0].productName+"</span></div>";    
                            var template='';
                            $.each(r,function(i,d){ 
                                var lot="<div><label class='frm_class'><?php echo __('Lote') ?>: </label><span>"+d.lotName+"</span></div>";                                
                                var amount_total="<div><label class='frm_class'><?php echo __('Stock') ?>:</label><span>"+d.amount+"</span></div>"
                                var amount_discount="<div><label class='frm_class'><?php echo __('Cantidad') ?>:</label><input type='text' data-lotname='"+d.lotName+"' data-id='"+d.idLot+"' data-amt='"+d.amount+"' size='5' class='txt_amount_ds ui-corner-all widget-content' /></div>"
                                template=template+"<div class='lot_qua ui-corner-all ui-widget-content'>"+lot+amount_total+amount_discount+"</div>";                                                                   
                            });
                            template=template+"<div class='clear'></div>";
                            template=product+'<div>'+template+"</div>";
                            $("#show_data").html(template);
                            $(".txt_amount_ds").setMask();
                             $("#input_DetailOther").dialog('open');
                        }else{
                        msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>')
                        }
                    }
                }
        });
        }
        
        //DIALOG DE PRODUCTO DETALLE
        $("#input_detail").dialog({
            title:'<?php echo "Ingresar Detalle Productos"; ?>',
            autoOpen: false,
            width: 855,
            closeOnEscape:false,
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: false,
            buttons:{
                '<?php echo "Aceptar" ?>':function(){                                          
                    var data=$("#grid_income_detail2").jqGrid('getGridParam','data');                    
                    if(data.length!=0 && data.length==$("#input_amount").val()){
                        $.each(data,function(i,d){
                            array_detail_id_p.push(d.id);
                        });                        
                        var id=$("#input_hidden_id_product").val();                                 
                        if(!$("#for_sell").is(":checked")){ 
                            var p={};   
                            p.id=id;
                            p.codigo=$("#input_hidden_code_product").val();
                            p.haveDetail=$("#input_hidden_have_detail").val();                            
                            p.producto=$("#input_product").val();
                            p.price=$("#input_price_product").val();
                            p.amount=parseInt($("#input_amount").val(),10);
                            p.importe=$("#input_importe_product").val();                        
                            $("#grid_list_stokeoutcome").jqGrid('addRowData',id,p);
                            grid_list_stokeoutcome.trigger("reloadGrid"); 
                        }else{
                            $(".add_detail[data-id="+id+"]").hide();
                        }                                                                        
                        
                        frm_product.currentForm.reset();
                        frm_product.resetForm();
                        $("#input_hidden_stock").val(0);
                        $("#input_search_product").helptext('refresh');

                        $("#input_detail").dialog('close');                                    
                        $("#grid_income_detail").jqGrid("clearGridData");
                        $("#grid_income_detail2").jqGrid("clearGridData");
                        
                    }else{                        
                        alert('Aun falta ingresar datos');
                    }
                },
                '<?php echo "Cancelar" ?>':function(){
                    $(this).dialog('close');
                    $("#grid_income_detail").jqGrid("clearGridData");                    
                    $("#grid_income_detail2").jqGrid("clearGridData");                                         
//                    $("#input_hidden_id_product").val('');                    
//                    frm_product.currentForm.reset();
//                    frm_product.resetForm();
                }
            } 
        });        
        // DIALOG DE PRODUCTO LOTES
           $('#input_DetailOther').dialog({
            title: detail_form_Title,
            autoOpen: false,
            closeOnEscape:false,
            width:600,
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: false, 
            buttons: {
                "<?php echo __("Guardar"); ?>":function(){
                    var txt=$(".txt_amount_ds");
                    var total=$("#input_amount").val();
                    var t=0;
                    var inc=0;                    
                    var array_temp=[];
                    $.each(txt,function(i,d){
                        var prl={};                        
                        var val_lim=$(d).data('amt');
                        var val=($(d).val()=='')?0:$(d).val();
                        t=t+parseInt(val);                         
                        if(val_lim<parseInt(val)){
                            inc=inc+1;
                        }                                             
                        prl.idLot=$(d).data('id');
                        prl.lotName=$(d).data('lotname');                        
                        prl.amount=val;
                        if(val!=0)
                            array_temp.push(prl);                    
                    });  
                    if(total!=t || isNaN(t) || inc>0){
                        $(".txt_amount_ds").addClass('ui-state-error');
                        //Mensaje de error
                        msgBox('<?php echo __("Cantidad incorrecta") ?>','<?php echo __("Alerta") ?>');
                        return false;
                    }else{
                        $(".txt_amount_ds").removeClass('ui-state-error');
                        var data=array_temp;                        
                        var id=$("#input_hidden_id_product").val();  
                       var p={}; 
                       var count = 0;
                        $.each(data,function(i,d){       
                            // Meto al arreglo todos los lotes que sacaré
                            array_detail_id_pl.push(d);
                            count = count + parseInt(d.amount,10);
                        
                        })
                        p.id=id;
                        p.codigo=$("#input_hidden_code_product").val();
                        p.producto=$("#input_product").val();
                        p.haveDetail=$("#input_hidden_have_detail").val();   
                        
                        p.price=$("#input_price_product").val();                        
                        p.importe=$("#input_importe_product").val();
                        
                        p.amount = count;
                        $("#grid_list_stokeoutcome").jqGrid('addRowData',id,p);
                        
                        frm_product.currentForm.reset();
                        frm_product.resetForm();
                        $("#input_hidden_stock").val(0);
                        $("#input_search_product").helptext('refresh');                               
                        $("#input_DetailOther").dialog('close');                          
                        grid_list_stokeoutcome.trigger("reloadGrid");                         
                    }
                },
                "<?php echo __("Cancelar"); ?>":function(){
                    $(this).dialog('close');                                      
                    $("#input_hidden_id_product").val('');                    
                    frm_product.currentForm.reset();
                    frm_product.resetForm();
                }
            }
        });
        
        $('#input_search_product').helptext({value:'Búsqueda de Productos'});
        $("#input_search_product").autocomplete({
            minLength:1,            
            autoFocus: true,
            source:function(req,response){               
                $.ajax({
                    url:url_getDataSearchAutocompleteProduct+jquery_params,
                    dataType:'jsonp',
                    data: {
                        term : req.term,
                        typeSearch:$("input[name=check_type_search_product]:checked").val()
                    },
                    success:function(resp){                        
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {                                                                                                
                                var text = "<span class='spanSearchLeft'>" + item.dataMostrar+ "</span>";                         
                                text += "<span class='spanSearchRight'>" + item.otraData + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:item.dataMostrar,
                                    id:item.id,
                                    code:item.otraData
                                }
                            }));
                        }
                    }
                });
            },
            select: function( event, ui ) {  
                var array=$("#grid_list_stokeoutcome").jqGrid('getRowData');
                $.each(array, function(index,data){                    
                    if(parseInt(data.id)==ui.item.id){               
                        return false;
                    }
                });
                $('#input_search_product').val(ui.item.value);                 
                $('#input_search_product').helptext('refresh'); 
                edit_product_outcome=0;
                getProductById(ui.item.id); 
                return false;
            },
            change: function( event, ui ) {                                       
                if ( !ui.item ) {
                    $('#input_search_product').val('');  
                    $( this ).helptext('refresh');                                                        
                }  
                return true;                
            }
        })
        .bind('blur', function(){             
            if($(this).data('autocomplete').selectedItem == null && $(this).val() == ''
                || $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').term)
            {
                $('#input_search_product').val('');
                $( this ).helptext('refresh');                
            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {               
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        fnLoadCheck();
        function getProductById(id){            
            showLoading=1;
            var idStore=$("#select_store").attr('data');
            $.ajax({                
                url:url_getProductById+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{id:id,idStore:idStore}, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        var r=response.data;
                        var productStock=response.amount;
                        $("#input_product").val(r.productName);
                        $("#input_measure").val(r.productUnitMeasures);
                        $("#input_price_product").val(r.productPriceSell);
                        $("#input_stock").val(parseInt(productStock)-parseInt($("#input_amount").val()));
                        $("#input_hidden_stock").val(productStock);
//                        var arrayDataGrid=$("#grid_list_stokeoutcome").jqGrid("getRowData");
//                        var stk_hidden=0;
//                        if(arrayDataGrid.length>0){
//                            $.each(arrayDataGrid,function(index,data){                                   
//                                if(data.codigo==r.aliasProduct){                                    
//                                    stk_hidden=parseInt(stk_hidden)+parseInt(data.amount);                                     
//                                    $("#input_hidden_stock").val(parseInt(productStock)-parseInt(stk_hidden));                                    
//                                    $("#input_stock").val(parseInt(productStock)+parseInt(stk_hidden)-parseInt($("#input_amount").val()));
//                                }
//                            });
//                        }                                                
                        $("#input_hidden_id_product").val(r.idProduct);
                        $("#input_hidden_code_product").val(r.aliasProduct);
                        $("#input_hidden_have_detail").val(r.haveDetail);                        
                        $("#input_amount").focus();
                    }
                }
            });
        }
        function calcStock(stock_static,quantity){ 
            quantity=(quantity=='')?0:quantity; 
            $("#input_stock").val(eval(parseInt(stock_static,10)-parseInt(quantity,10)));
            $("#input_importe_product").val((quantity*$("#input_price_product").val()).toFixed(2));
        }
        $('table#grid_list_stokeoutcome').on('click','.trash',function(){                          
            var p=$("#grid_list_stokeoutcome").jqGrid('getRowData',$(this).data('id'));                
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){ 
                    $("#grid_list_stokeoutcome").jqGrid('delRowData',p.id);
                    grid_list_stokeoutcome.trigger("reloadGrid");          
                }
            });
            return false
        });
        $('table#grid_list_stokeoutcome').on('click','.add_detail',function(){                          
            var p=$("#grid_list_stokeoutcome").jqGrid('getRowData',$(this).data('id'));   
            if(p.haveDetail==true){                                        
                $("#input_hidden_id_product").val(p.id);
                $("#input_amount").val(p.amount);                
                var idStore=$("#select_store").attr('data');
                fn_get_detail_product(p.id,idStore);                
            }else{
                alert("Usted no tiene esta Opción");                
            }            
            return false
        });
        $('table#grid_list_stokeoutcome').on('click','.edit',function(){
            var p=$("#grid_list_stokeoutcome").jqGrid('getRowData',$(this).data('id'));             
            $("#input_hidden_id_product").val(p.id);
            $("#input_hidden_code_product").val(p.codigo);
            getProductById($("#input_hidden_id_product").val());
            $("#input_amount").val(p.amount);
            $("#input_price_product").val(p.price);
            var stock_hidden=$("#input_hidden_stock").val()-$("#input_amount").val();
            $("#input_hidden_stock").val(stock_hidden);
            calcStock($("#input_hidden_stock").val(),$("#input_amount").val());
            edit_product_outcome=1;
        });
        $("#btn_clear_product").on('click',function(e){
            e.preventDefault();
            // frm_product.currentForm.reset();
            // frm_product.resetForm();
            $("#input_product").val(0);
            $("#input_measure").val(0);
            $("#input_amount").val(0);
            $("#input_price_product").val(0);
            $("#input_importe_product").val(0);
            $("#input_hidden_stock").val(0);
            edit_product_outcome=0;
            $("#input_search_product").helptext('refresh');
        });
        $("#btn_cancel").on('click',function(e){
            e.preventDefault();                         
            frm_product.currentForm.reset();
            frm_product.resetForm();
            frm_content.currentForm.reset();
            frm_content.resetForm();             
            if($("input[name=for_outcome]:checked").val()=='sell'){                
                $(".purchase").show();
                $(".doc_ref").hide();
                $("#div_less_product").hide().fadeOut();
            }            
            $("#input_hidden_stock").val(0);
            edit_product_outcome=0;                                    
            
            array_detail_id_p=[];
            array_detail_id_pl=[];
            $("#grid_list_stokeoutcome").jqGrid("clearGridData");
            $("#input_search_product").helptext('refresh');
        });
        $("#btn_register").on('click',function(e){
            e.preventDefault();
            if(!$("#frm_content").valid())
                return false;  
            var arrayDataGrid=$("#grid_list_stokeoutcome").jqGrid("getRowData");
            if(arrayDataGrid.length>0){
                if($("#for_sell").is(":checked")){
                    if($('.add_detail:visible').length==0){                          
                    }else{
                        alert("Aun le falta ingresar los detalles");
                        return false;
                    }
                }
                confirmBox  ('<?php echo __("Desea Confirmar la Salida de Productos?"); ?>','Confirmar Salida',function(event){
                    if(event){
                        var obj_frm_content=$("#frm_content").serializeObject();
                        obj_frm_content.idStore=$("#select_store").attr('data');
                        obj_frm_content.idClient=$("#select_customer").attr('rel');
                        showLoading=1;
                        $.ajax({
                            url:url_createOutcomeStock+jquery_params,                        
                            type:'POST',
                            dataType:'json',
                            data:{
                                obj_frm:obj_frm_content,
                                array_grid:arrayDataGrid,
                                array_grid_detail:array_detail_id_p,
                                array_detail_id_pl:array_detail_id_pl
                            }, 
                            async:false,
                            success:function(response){                    
                                if(evalResponse(response)){
                                    array_detail_id_p=[];
                                    array_detail_id_pl=[];
                                    $("#btn_cancel").trigger('click');
                                    getAllSalesOrder();
                                }
                            }
                        });                        
                    }
                });                                 
            }else{
                msgBox('Usted No Tiene Productos Para Salida','Alerta de Salida');
            }
            return false;
        });
         
<?php if ($bAvailableDocumentType): ?>
            successDocument = false;                
            fnUpdateCorrelativePackage = function(sType){
                try{                
                    oDocumentType = getCurrentCorrelative(type_document);                                                                                                                                                                                                                                                                       
                    if(oDocumentType.serie != null)
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.serie + '-' +oDocumentType.correlative;
                    else
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.correlative;
                                                                                                                                                                                                                                                                                
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-highlight')
                    .html(displayDocument);
                    successDocument = true;
                                                                                                                                                                                                                                                                                                                                
                                                                                                                                                                                                                                                                                                                                
                }catch(e){
                    successDocument = false;
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-error')
                    .html('<?php echo __("Documento no Asignado") ?>');
                }
            };            
            fnUpdateCorrelativePackage(type_document);  
<?php else: ?>
<?php endif ?> 

        $('#select_customer').helptext({value:"Buscar Cliente", customClass: 'lightSearch'});
        $( "#select_customer" ).autocomplete({            
            minLength: 0,
            autoFocus: true,
            source:function(req,response){
                $.ajax({
                    url:url_getClientsAutocomplete+jquery_params,
                    dataType:'json',
                    data: {
                        term : req.term     
                    },
                    success:function(resp){                        
                        response( $.map( resp.data, function( item ) {                            
                            var text = "<span>" + item.customers + "</span>";                            
                            return {
                                label:  text.replace(
                                new RegExp(
                                "(?![^&;]+;)(?!<[^<>]*)(" +
                                    $.ui.autocomplete.escapeRegex(req.term) +
                                    ")(?![^<>]*>)(?![^&;]+;)", "gi"
                            ), "<strong>$1</strong>" ),
                                    value:  item.customers,
                                    id : item.id
                          
                            }
                        }));

                    }
                });
            },              
            select: function( event, ui ) {                         
                $(this).data('autocomplete').text_value = ui.item.customers;
                $(this).attr('rel',ui.item.id);       
                $('#idCustomers').val(ui.item.id);
            },
            change: function( event, ui ) {                                    
                if ( !ui.item ) { 
                    $(this).val('');   
                    $('#idCustomers').val('');
                    $(this).attr('rel','');
                    $( this ).helptext('refresh');  
                }                 
                return true;                
            }
        })
        .bind('blur', function(){            
            if( 
            $(this).data('autocomplete').selectedItem == null 
                && $(this).val() == ''
                || $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').term
        ){                
                if($('#select_customer').val()!='' && $(this).attr('rel')!=''){
                    $(this).val($(this).val());
                    $(this).attr($(this).attr('rel'));
                }else{
                    $(this).val('');
                    $(this).attr('rel','');
                    $( this ).helptext('refresh');
                }                
            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {               
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        

        var grid_income_detail=$("#grid_income_detail").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Serie Electrónica") ?>',                                                        
                '<?php echo __("Descripción") ?>',
                '<?php echo __("Acciones") ?>',
            ],
            colModel:[
                {name:'idGeneral',index:'idGeneral',hidden:true,width:10},
                {name:'serie_electronica',index:'serie_electronica',align:'center'},       
                {name:'description',index:'description',align:'center'},
                {name:'actions',index:'actions',align:'center'},
            ],
            pager:'#grid_income_detail_pager',              
            rowNum:10, 
            rowList:[10,20,50,100],
            sortorder: "asc",            
            width:400,
            height:250,                        
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            multiselect: true,
            caption:'<?php echo __("Lista Detalles de Productos") ?>',
            ondblClickRow:function(rowid,iRow,iCol,e){                       
                var array_grid_income_detail2=$("#grid_income_detail2").jqGrid("getRowData");
                if(array_grid_income_detail2.length<$("#input_amount").val()){                    
                    var a=$("#grid_income_detail").getRowData(rowid);                    
                    $("#grid_income_detail2").jqGrid('addRowData',rowid,a);                     
                    $("#grid_income_detail").jqGrid('delRowData',rowid);                    
                    grid_income_detail[0].clearToolbar();
                    grid_income_detail.trigger("reloadGrid");                                                                           
                    grid_income_detail2.trigger("reloadGrid"); 
                }else{
                    alert('Ya completo la Salida');
                }
            },
            gridComplete:function(){ 
                $('.trash_detail').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
            }
        });  
        jQuery("#grid_income_detail").jqGrid('navGrid','#grid_income_detail_pager',{del:false,add:false,edit:false,search:false}); 
        jQuery("#grid_income_detail").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        jQuery("#grid_income_detail").jqGrid('navButtonAdd','#grid_income_detail_pager',{            
            caption:"", 
            buttonicon:"ui-icon-plusthick", 
            onClickButton: function(){
                var recs1 = $("#grid_income_detail").getGridParam("records");
                var recs2 = $("#grid_income_detail2").getGridParam("records");
                if(recs1>0){
                    var val_grid_sel1=grid_income_detail.getGridParam('selarrrow');
                    var val_grid_sel_tot1=val_grid_sel1.length+recs2;
                    if(val_grid_sel1.length>0 && val_grid_sel_tot1<=$("#input_amount").val()){
                        var len = val_grid_sel1.length;
                        for(var i=len-1;i>=0;i--){
                            var a=$("#grid_income_detail").getRowData(val_grid_sel1[i]);
                            $("#grid_income_detail2").jqGrid('addRowData',val_grid_sel1[i],a); 
                            $("#grid_income_detail").delRowData(val_grid_sel1[i]);
                        }
                        
                        grid_income_detail[0].clearToolbar();
                        grid_income_detail.trigger("reloadGrid");                                                                           
                        grid_income_detail2.trigger("reloadGrid");   
                    }else{
                        alert('No hay Productos Seleccionados o Ya Completo la Salida');
                    }
                }else{
                    alert('No hay mas Productos en Almacén');
                }                 
            },
            title:'Quitar en Grupo',
            position:"last"
        });
        
        $('table#grid_income_detail').on('click','.trash_detail',function(){                          
            var p=$("#grid_income_detail").jqGrid('getRowData',$(this).data('id'));                
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){ 
                    $("#grid_income_detail").jqGrid('delRowData',p.serie_electronica);
                    grid_income_detail.trigger("reloadGrid");          
                }
            });
            return false
        });
        
        var grid_income_detail2=$("#grid_income_detail2").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Serie Electrónica") ?>',                                                        
                '<?php echo __("Descripción") ?>',
            ],
            colModel:[
                {name:'idGeneral',index:'idGeneral',hidden:true,width:10},
                {name:'serie_electronica',index:'serie_electronica',align:'center'},       
                {name:'description',index:'description',align:'center'},
            ],
            pager:'#grid_income_detail_pager2',              
            rowNum:10, 
            rowList:[10,20,50,100],
            sortorder: "asc",            
            width:400,
            height:250,                        
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            multiselect: true,
            caption:'<?php echo __("Productos Seleccionados") ?>',
            ondblClickRow:function(rowid,iRow,iCol,e){ 
                var a=$("#grid_income_detail2").getRowData(rowid);                    
                $("#grid_income_detail").jqGrid('addRowData',rowid,a); 
                $("#grid_income_detail2").jqGrid('delRowData',rowid);
                grid_income_detail2[0].clearToolbar();
                grid_income_detail.trigger("reloadGrid");                                                                           
                grid_income_detail2.trigger("reloadGrid");                
            },
            gridComplete:function(){                     
            }
        });  
        jQuery("#grid_income_detail2").jqGrid('navGrid','#grid_income_detail_pager2',{del:false,add:false,edit:false,search:false}); 
        jQuery("#grid_income_detail2").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        jQuery("#grid_income_detail2").jqGrid('navButtonAdd','#grid_income_detail_pager2',{            
            caption:"", 
            buttonicon:"ui-icon-minusthick", 
            onClickButton: function(){
                var recs1 = $("#grid_income_detail").getGridParam("records");
                var recs2 = $("#grid_income_detail2").getGridParam("records");
                if(recs1>0){
                    var val_grid_sel2=grid_income_detail2.getGridParam('selarrrow');
                     
                    if(val_grid_sel2.length>0){
                        var len = val_grid_sel2.length;
                        for(var i=len-1;i>=0;i--){
                            var a=$("#grid_income_detail2").getRowData(val_grid_sel2[i]);
                            $("#grid_income_detail").jqGrid('addRowData',val_grid_sel2[i],a); 
                            $("#grid_income_detail2").delRowData(val_grid_sel2[i]);
                        }                        
                        grid_income_detail[0].clearToolbar();
                        grid_income_detail.trigger("reloadGrid");                                                                           
                        grid_income_detail2.trigger("reloadGrid");   
                    }else{
                        alert('No hay Productos Seleccionados.');
                    }
                }else{
                    alert('No hay mas Productos en Almacén.');
                }
            },
            title:'Quitar en Grupo',
            position:"last"
        });
    });
</script>

<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;" <?php endif ?> >
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4><?php echo __('Salida De Productos - Almacén'); ?></h4>
        </div>
    </div>
    <?php if ($bAvailableDocumentType): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix">
        </div>
    <?php endif ?>
    <div class="clear"></div>
</div>

<div id="outcome_content" class="ui-widget-content ui-corner-all">    
    <div>        
        <form id="frm_content">            
            <div class="cFieldSet ui-widget-content">             
                <div id="for_div_1">               
                    <div>
                        <label><?php echo __("Fecha de Salida"); ?>: </label>
                        <input type="text" id="input_date_outcome" name="input_date_outcome" value="<?php echo $today; ?>" />
                    </div>
                    <div id="for_outcome">
                        <label><?php echo __("Salida Por"); ?>: </label>
                        <input type="radio" id="for_sell" name="for_outcome" value="sell" checked="checked" /><label for="for_sell">Ventas</label>
                        <input type="radio" id="for_other" name="for_outcome" value="other" /><label for="for_other">Otros</label>
                        <div class="clear"></div>
                    </div>                
                </div>
                <div id="for_div_2" class="ui-widget ui-widget-content">
                    <div class="purchase">
                        <label><?php echo __("Pedido"); ?>: </label>
                        <select id="select_salesOrders" name="select_salesOrders">
                        </select>
                    </div>
                    <div class="doc_ref">
                        <label><?php echo __("Doc. Referencia"); ?>: </label>
                        <input type="text" id="text_doc_ref" name="text_doc_ref" />
                    </div>
                    <div>
                        <label><?php echo __("Concepto Movimiento"); ?>: </label>
                        <select id="select_typeMove" name="select_typeMove">
                            <option value=""><?php echo __("Seleccionar Una Opción..."); ?></option>
                            <?php foreach ($typeMove as $v) {
                                ?>
                                <option value="<?php echo $v['id']; ?>"><?php echo $v['display']; ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                </div>                                    
                <div id="for_div_3">
                    <div>
                        <label><?php echo __("Almacén"); ?>: </label>
                        <input id="select_store" name="select_store" value="<?php echo (count($store) != 0) ? $store[0]['display'] : 'ASIGNESE UN ALMACEN'; ?>" data="<?php echo (count($store) != 0) ? $store[0]['id'] : '0'; ?>" class="<?php echo (count($store) == 0) ? 'ui-state-error' : 'ui-state-highlight'; ?> ui-corner-all" readonly="" style="padding: 7px;font-weight: bolder;" />
                    </div>
                    <div>
                        <label><?php echo __("Cliente"); ?>: </label>
                        <input type="text" id="select_customer" name="select_customer" />
                        <input type="hidden" id="idCustomers" name="idCustomers"/>
                    </div>
                    <div id="for_currency">
                        <label><?php echo __("Tipo Moneda"); ?>: </label>
                        <dd>
                            <?php foreach ($currency as $sKey => $aCurrency): ?>    
                                <?php echo Helper::fnGetDrawJqueryButton($aCurrency, 'currency', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aCurrency['display'])))) ?>                                            
                            <?php endforeach ?>   
                        </dd>
                    </div>
                </div>
                <div class="clear"></div>            
            </div>
        </form>
        <div class="div_grid_stokeoutcome"> 
            <div id="div_less_product" class="ui-corner-all ui-widget-content">
                <form id="frm_product">
                    <div>                     
                        <div>
                            <fieldset id="field_product">
                                <legend><?php echo __("Busqueda de Producto"); ?></legend>
                                <div id="check_type_search_product">
                                    <input type="radio" id="search_product_code" name="check_type_search_product" checked="checked" value="search_code" /><label for="search_product_code"><?php echo __("Código"); ?></label>
                                    <input type="radio" id="search_product_desc" name="check_type_search_product" value="search_description" /><label for="search_product_desc"><?php echo __("Modelo"); ?></label>                                                 
                                </div>
                                <div id="div_input_search_product">                                
                                    <input type="text" id="input_search_product" name="input_search_product" size="40" />
                                </div>                            
                            </fieldset>
                        </div>
                        <div id="div_product_find">
                            <div>
                                <label><?php echo __("Producto"); ?>: </label>
                                <input type="text" id="input_product" name="input_product" size="38" readonly="" />
                                <input type="hidden" id="input_hidden_id_product" name="input_hidden_id_product"  />
                                <input type="hidden" id="input_hidden_code_product" name="input_hidden_code_product"  />
                                <input type="hidden" id="input_hidden_have_detail" name="input_hidden_have_detail"  />
                            </div>
                            <div>
                                <label><?php echo __("Medida"); ?>: </label>
                                <input type="text" id="input_measure" name="input_measure" size="23" readonly=""  />
                            </div>
                            <div>
                                <div class="two_colum" id="div_amount">
                                    <label><?php echo __("Cantidad"); ?>: </label>
                                    <input type="text" id="input_amount" name="input_amount" size="10" value="0" alt="number" autocomplete="off" />
                                </div>
                                <div class="two_colum" id="div_stock">
                                    <label><?php echo __("Stock"); ?>: </label>
                                    <input type="text" id="input_stock" name="input_stock" size="10" value="0" readonly="" />                                    
                                    <input type="hidden" id="input_hidden_stock" name="input_hidden_stock" value="0" size="10" readonly="" />                                    
                                </div>
                                <div class="clear"></div>
                            </div>                        
                            <div class="ui-widget-content ui-corner-all" style="padding: 5px;">
                                <div style="padding: 5px 0px;">
                                <div class="two_colum">
                                    <label><?php echo __("Precio Venta"); ?>:</label>
                                    <input type="text" id="input_price_product" name="input_price_product" value="0" size="10" autocomplete="off" /> 
                                </div>                              
                                <div class="two_colum">
                                    <label><?php echo __("Importe Venta"); ?>:</label>
                                    <input type="text" id="input_importe_product" name="input_importe_product" value="0" size="10" readonly="" /> 
                                </div>  
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div id="buttons_add_product">
                            <button id="btn_less_product"><?php echo __("Sacar Producto"); ?></button>
                            <button id="btn_clear_product"><?php echo __("Limpiar"); ?></button>
                        </div>
                    </div>
                </form>
            </div>
            <div id="div_grid_outcome">
                <table id="grid_list_stokeoutcome"> </table>
                <div id="grid_list_stokeoutcome_pager"></div>
            </div>            
            <div class="clear"></div>
        </div>        
        <div class="clear"></div>
    </div>
    <div id="btn_buttons">
        <button id="btn_register"><?php echo __("Registrar"); ?></button>
        <button id="btn_cancel"><?php echo __("Cancelar"); ?></button>        
    </div>
</div>

 <!--DIALOG PRODUCTO DETALLE-->
<div id="input_detail" class="dialog">    
    <div id="grid_uno">
        <table id="grid_income_detail"> </table>
        <div id="grid_income_detail_pager"></div>
    </div>
    <div id="grid_dos">
        <table id="grid_income_detail2"> </table>
        <div id="grid_income_detail_pager2"></div>
    </div>
    <div class="clear"></div>
</div>
 <!--DIALOG LOTE-->
<div id="input_DetailOther" class="dialog">
    <form id="frm_sale_data">
        <div id="show_data">
        </div>
    </form>
</div>
