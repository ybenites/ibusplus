<style type="text/css">
    .div_new_brand{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_grid_brand{        
        padding-top: 10px;
    }
    #frm_new_brand{
        padding: 10px;
    }
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        font-weight: bolder;
        text-align: right;
        width: 180px;
        margin-right: 5px;
        margin-top: 6px;
    }
    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px;       
    }
</style>
<script type="text/javascript">
    var fnListBrandGrid="/private/brands/listAllBrands";
    var fnCreateNewBrand="/private/brands/createNewBrand";
    var fnUpdateNewBrand="/private/brands/updateNewBrand";
    var fnListByIdBrand="/private/brands/getBrandById";
    var fnDeleteBrands="/private/brands/deleteBrands";
    var msg_warn_delete='<?php echo __("Esta Seguro de Eliminar Marca?")?>';
    title_delete='<?php echo __("Eliminar Marca")?>';
    $(document).ready(function(){        
        $("#btn_new_brand").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        $("#btn_new_brand").on('click',function(){
            $("#dlg_new_brand").dialog('open');
        });
        grid_list_brand=$("#grid_list_brand").jqGrid({ 
            url:fnListBrandGrid+jquery_params,
            datatype: "jsonp",
            mtype: 'POST',          
            colNames:[ 
                '<?php echo __("ID") ?>',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Nombre Comercial") ?>',
                '<?php echo __("Usuario Registro") ?>',
                '<?php echo __("status") ?>',
                '<?php echo __("Fecha Creacion") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'idBrand',index:'idBrand',key:true,hidden:true},
                {name:'brandName',index:'brandName',align:'center',search:true},
                {name:'brandNameCommercial',index:'brandNameCommercial',align:'center',search:true},
                {name:'userCreate', index: 'p.fullName',align:'center',search:true},
                {name:'status', index: 'status',align:'center',hidden:true,search:true},
                {name:'dateCreation', index: 'dateCreation',align:'center',search:false},
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'grid_list_brand_pager', 
            sortname: 'br.idBrand',         
            gridview: true,
            rowNum:15,   
            sortorder: "asc",            
            width:1200,
            height:410,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Marcas")?>',
            gridComplete:function(){
                $.each(grid_list_brand.jqGrid('getDataIDs'), function(){
                    var options='';                                      
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                    options=edit+' '+trash;                    
                    grid_list_brand.setRowData(this, {actions:options});                    
                }) 
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });                    
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });                
            }
        });
        $("#grid_list_brand").jqGrid('navGrid','#grid_list_brand_pager',{add:false,edit:false,del:false,search:false,refresh:false});                        
        jQuery("#grid_list_brand").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        $('table#grid_list_brand').on('click','.trash',function(){            
            var idb=$(this).data('id');
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:fnDeleteBrands+jquery_params,
                        data:{idb:idb},
                        success: function(response){                            
                            if (response.code == code_success)
                            {
                                $("#grid_list_brand").trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code);
                            }
                        }
                    });          
                }
            });
            return false
        });
        $('table#grid_list_brand').on('click','.edit',function(){            
            var idb=$(this).data('id');            
            $.ajax({
                data:{                    
                    id:idb
                },                    
                type:"POST",
                dataType:"json",
                url:fnListByIdBrand+jquery_params,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $("#idBrand").val(data.idBrand);
                        $("#nameBrande").val(data.brandName);
                        $("#nameBrandCommerciale").val(data.brandNameCommercial);
                        $("div#dlg_edit_brand").dialog('open');                        
                    }                                        
                }
            });
            return false
        });
        
        var frm_new_brand=$("#frm_new_brand").validate({
            rules:{
                nameBrand:{
                    required:true
                },
                nameBrandCommercial:{
                    required:true
                }
            },
            messages:{
                nameBrand:{
                    required:'<?php echo __("Ingrese Nombre Marca") ?>'
                },
                nameBrandCommercial:{
                    required:'<?php echo __("Ingrese Nombre Comercial") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:{
                        name:function(){
                            return $("#nameBrand").val();
                        },
                        nameCommercial:function(){                            
                            return $("#nameBrandCommercial").val();                            
                        }
                    },                    
                    type:"POST",
                    dataType:"json",
                    url:fnCreateNewBrand+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Registrado"); ?>');
                            grid_list_brand.clearGridData();
                            grid_list_brand.trigger("reloadGrid");
                            $("div#dlg_new_brand").dialog('close');
                            frm_new_brand.currentForm.reset();
                        }
                    }
                });
            }
        });
        var frm_edit_brand=$("#frm_edit_brand").validate({
            rules:{
                nameBrande:{
                    required:true
                },
                nameBrandCommerciale:{
                    required:true
                }
            },
            messages:{
                nameBrande:{
                    required:'<?php echo __("Ingrese Nombre Marca") ?>'
                },
                nameBrandCommerciale:{
                    required:'<?php echo __("Ingrese Nombre Comercial") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:{
                        id:$("#idBrand").val(),
                        name:function(){
                            return $("#nameBrande").val();
                        },
                        nameCommercial:function(){                            
                            return $("#nameBrandCommerciale").val();                            
                        }
                    },                    
                    type:"POST",
                    dataType:"json",
                    url:fnUpdateNewBrand+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                            
                            grid_list_brand.clearGridData();
                            grid_list_brand.trigger("reloadGrid");
                            $("div#dlg_edit_brand").dialog('close');
                            frm_edit_brand.currentForm.reset();
                        }
                    }
                });
            }
        });
        $("div#dlg_new_brand").dialog({
            width:450,
            title:'<?php echo __("Registrar Marca"); ?>',
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_new_brand").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close');                      
                }
            }
        });        
        $("div#dlg_edit_brand").dialog({
            width:450,
            title:'<?php echo __("Editar Marca"); ?>',
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_edit_brand").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close');                      
                }
            }
        });        
    });
</script>
<div>
    <center><div class="titleTables"><?php echo __('Administración de Marcas'); ?></div></center>
</div>

<div>
    <div class="div_new_brand">
        <button id="btn_new_brand"><?php echo __('Nueva Marca'); ?></button>    
    </div>
    <div class="div_grid_brand">
        <center>
            <div>
                <table id="grid_list_brand"> </table>
                <div id="grid_list_brand_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="dlg_new_brand">    
    <form id="frm_new_brand">
        <fieldset  class="ui-corner-all cFieldSet">            
            <div>
                <label><?php echo __('Nombre de Marca')?>:</label>
                <input type="text" id="nameBrand" name="nameBrand" />
            </div>
            <div>
                <label><?php echo __('Nombre Comercial')?>:</label>
                <input type="text" id="nameBrandCommercial" name="nameBrandCommercial" />                
            </div>                        
        </fieldset>
    </form>
</div>
<div id="dlg_edit_brand">    
    <form id="frm_edit_brand">
        <fieldset  class="ui-corner-all cFieldSet">   
            <input type="hidden" id="idBrand" name="idBrand" />
            <div>
                <label><?php echo __('Nombre de Marca')?>:</label>
                <input type="text" id="nameBrande" name="nameBrande" />
            </div>
            <div>
                <label><?php echo __('Nombre Comercial')?>:</label>
                <input type="text" id="nameBrandCommerciale" name="nameBrandCommerciale" />                
            </div>                        
        </fieldset>
    </form>
</div>