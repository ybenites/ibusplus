<style type="text/css">   
    .div_new_category{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_grid_category{        
        padding-top: 10px;
    }
    #frm_new_category{
        padding: 10px;
    }
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 130px;
    }
    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px;       
    }
</style>
<script type="text/javascript">    
    var fnListCategoryTree="/private/category/listCategoryXML";
    var fnCreateNewCategory="/private/category/createNewCategory";
    var fnListByIdCategory="/private/category/getCategoryById";
    var fnUpdateNewCategory="/private/category/updateNewCategory";
    var fnDeleteCategory="/private/category/deleteCategory";    
    var msg_warn_delete='<?php echo __("Esta Seguro de Eliminar Categoria?") ?>';
    var title_delete='<?php echo __("Eliminar Categoria") ?>';
    $(document).ready(function(){
        $("#btn_new_category").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        $("#btn_new_category").on('click',function(){           
            $("#dlg_new_category").dialog('open');
        });
                
        var tree_list_category = $('#tree_list_category').jqGrid({
            treeGrid:true,            
            treeGridModel : 'adjacency',
            ExpandColumn :'categoryName',
            url:fnListCategoryTree+'?nodeid=0&n_level=0',
            datatype:'xml',
            mtype:'POST',
            colNames:[
                '<?php echo __("ID") ?>',
                '<?php echo __("Nombre") ?>',                
                '<?php echo __("Usuario Registro") ?>',
                '<?php echo __("status") ?>',
                '<?php echo __("Fecha Creacion") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[               
                {name:'idBrand',index:'idCategory',key:true,hidden:true},
                {name:'categoryName',index:'rms_category.categoryName',align:'left'},                
                {name:'userCreate', index: 'p.fullName',align:'center'},
                {name:'status', index: 'status',align:'center',hidden:true},
                {name:'dateCreation', index: 'dateCreation',align:'center'},
                {name:'actions', index: 'actions',align:'center',width:80}
            ],
            width:1200,
            height:'auto',
            viewrecords: true,
            pager : "#tree_list_category_pager",
            caption: "Lista de Categorias",
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });                    
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });
                $('.add').button({
                    icons:{primary: 'ui-icon-plusthick'},
                    text: false
                });
            }
        });        
        $("#tree_list_category").jqGrid('navGrid','#tree_list_category_pager',{add:false,edit:false,del:false,search:false,refresh:true});                                
        
        $('table#tree_list_category').on('click','.edit',function(){            
            var idc=$(this).data('id');            
            $.ajax({
                data:{                    
                    id:idc
                },                    
                type:"POST",
                dataType:"json",
                url:fnListByIdCategory+jquery_params,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;                        
                        if(data.haveSuperCategory==true){  
                            $("#padre").show();
                            $("#superCategoryNamee").val(data.superCategoryName);
                            $("#categoryCodee").attr("readonly","");
                            //$("#categoryCodee").val(data.codigo);
                        }else{ 
                            $("#padre").hide();
                        }
                        $("#idCategory").val(data.idCategory);
                        $("#categoryCodee").val(data.codigo);                        
                        $("#categoryNamee").val(data.categoryName);                        
                        $("div#dlg_edit_category").dialog('open');                        
                    }                                        
                }
            });
            return false
        });
        
        $('table#tree_list_category').on('click','.add',function(){
            $("#idc").val($(this).data('id'));
            $("#dlg_new_category").dialog('open');
        });            
        $('table#tree_list_category').on('click','.trash',function(){            
            var idc=$(this).data('id');
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:fnDeleteCategory+jquery_params,
                        data:{idc:idc},
                        success: function(response){                            
                            if (response.code == code_success)
                            {
                                $("#tree_list_category").trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code);
                            }
                        }
                    });          
                }
            });
            return false
        });
        
        $("div#dlg_new_category").dialog({
            width:450,
            title:'<?php echo __("Registro Nueva Categoria"); ?>',
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_new_category").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){ 
                    $("#idc").val(0);
                    $('#errorMessages').hide();
                    frm_edit_category.currentForm.reset();
                    $(this).dialog('close');                      
                }
            }
        });        
        $("div#dlg_edit_category").dialog({
            width:450,
            title:'<?php echo __("Editar Categoria"); ?>',
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_edit_category").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    frm_edit_category.currentForm.reset();
                    $(this).dialog('close');                      
                }
            }
        });
        var frm_new_category=$("#frm_new_category").validate({
            rules:{
                nameCategory:{
                    required:true
                }
            },
            messages:{
                nameCategory:{
                    required:'<?php echo __("Ingrese Nombre Categoria") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:{
                        name:function(){
                            return $("#nameCategory").val();
                        },
                        codigo:function(){
                            return $("#categoryCode").val();
                        },
                        idc:function(){
                            return $("#idc").val();
                        }
                    },                    
                    type:"POST",
                    dataType:"json",
                    url:fnCreateNewCategory+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Registrado"); ?>');
                            tree_list_category.clearGridData();
                            tree_list_category.trigger("reloadGrid");
                            $("div#dlg_new_category").dialog('close');
                            $("#idc").val(0);
                            frm_new_category.currentForm.reset();
                        }
                    }
                });
            }
        });
        var frm_edit_category=$("#frm_edit_category").validate({
            rules:{
                categoryNamee:{
                    required:true
                }
            },
            messages:{
                categoryNamee:{
                    required:'<?php echo __("Ingrese Nombre Categoria") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:{
                        id:$("#idCategory").val(),
                        name:function(){
                            return $("#categoryNamee").val();
                        },
                        codigo:function(){
                            return $("#categoryCodee").val();
                        }
                    },                    
                    type:"POST",
                    dataType:"json",
                    url:fnUpdateNewCategory+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                            
                            tree_list_category.clearGridData();
                            tree_list_category.trigger("reloadGrid");
                            $("div#dlg_edit_category").dialog('close');
                            frm_edit_category.currentForm.reset();
                        }
                    }
                });
            }
        });
    });
    
    function resetForm($form) {
        $form.find('input:text, input:password, input:file, select, textarea').val('');
        $form.find('input:radio, input:checkbox')
        .removeAttr('checked').removeAttr('selected');
    }
</script>
<div>
    <center><div class="titleTables"><?php echo __('Administración de Categorías'); ?></div></center>
</div>

<div>
    <div class="div_new_category">
        <button id="btn_new_category"><?php echo __('Nueva Categoria'); ?></button>    
    </div>
    <div class="div_grid_category">
        <center>
            <div>
                <table id="tree_list_category"> </table>
                <div id="tree_list_category_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="dlg_new_category">    
    <form id="frm_new_category">
        <input type="hidden" id="idc" name="idc" value="0"/>
        <fieldset  class="ui-corner-all cFieldSet">  
            <?php if ($bTagGenerator): ?>
                <div>
                    <label><?php echo __('Código Asociado') ?>:</label>
                    <input type="text" id="categoryCode" name="categoryCode"  size="10"/>
                </div> 
            <?php endif; ?>
            <div>
                <label><?php echo __('Nombre') ?>:</label>
                <input type="text" id="nameCategory" name="nameCategory" />
            </div>  

        </fieldset>
    </form>
</div>
<div id="dlg_edit_category">    
    <form id="frm_edit_category">
        <fieldset  class="ui-corner-all cFieldSet">   
            <input type="hidden" id="idCategory" name="idCategory" />
            <div id="padre" class="none">
                <label><?php echo __('Nombre Padre') ?>:</label>                
                <input type="text" id="superCategoryNamee" name="superCategoryNamee" readonly="" />
            </div>    
            <?php if ($bTagGenerator): ?>
                <div>
                    <label><?php echo __('Código Asociado') ?>:</label>
                    <input type="text" id="categoryCodee" name="categoryCodee" size="10" />
                </div>   

            <?php endif; ?>
            <div>
                <label><?php echo __('Nombre') ?>:</label>
                <input type="text" id="categoryNamee" name="categoryNamee" />
            </div> 
        </fieldset>
    </form>
</div>