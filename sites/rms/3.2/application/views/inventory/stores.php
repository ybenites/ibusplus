<style type="text/css">
    .div_new_store{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_grid_store{        
        padding-top: 10px;
    }
    #frm_new_store{
        padding: 10px;
    }
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 130px;
    }
    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px;       
    }
</style>
<script type="text/javascript">
    /*--url--*/ 
    var fnListStoreGrid="/private/stores/listAllStores";
    var fnCreateNewStore="/private/stores/createNewStore";
    var fnUpdateNewStore="/private/stores/updateNewStore";
    var fnListByIdStore="/private/stores/getStoreById";
    var fnDeleteStore="/private/stores/deleteStore";
    var fnCompleteStoreUser="/private/stores/getCompleteStoreUser";
        /*--text--*/       
    var store_form_Title = "<?php echo __("Almacén"); ?>";
    var store_newTitle = "<?php echo __("Nuevo Almacén"); ?>";
    var store_editTitle = "<?php echo __("Editar Almacén"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Almacén"); ?>";
    var store_name_required = '<?php echo __("El Nombre de Almacén es obligatorio."); ?>';
    var store_shortname_required = '<?php echo __("El Alias de Almacén es obligatorio."); ?>';
    var store_office_required = '<?php echo __("Debe seleccionar una Oficina."); ?>';
    var store_user_required = '<?php echo __("Debe seleccionar una Usuario."); ?>';
    var msg_warn_delete='<?php echo __("Esta seguro de eliminar Este Almacén?")?>';
    
    $(document).ready(function(){
        $("#btn_new_store").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        $("#btn_new_store").on('click',function(){
             $('.frmSubtitle').text(store_newTitle);
            $("#dlg_new_store").dialog('open');
            return false;
        });
        $("#storeOffice").combobox({
            selected:function(ui,event){
                $("#storeOffice").trigger('change');
            }
        });
        $("#storeOfficee").combobox();
        $("div#dlg_new_store").dialog({
            width:450,
            title:store_form_Title,
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_new_store").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close');                      
                }
            }
        });
        $("div#dlg_edit_store").dialog({
            width:450,
            title:store_form_Title,
            autoOpen: false,
            resizable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_edit_store").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close');                      
                }
            }
        });
        var frm_new_store=$("#frm_new_store").validate({
            rules:{
                storeName:{
                    required:true
                },
                storeShortName:{
                    required:true
                },
                'storeOffice-combobox':{
                    required:true
                },
                'storeUser-combobox':{
                    required:true
                }
            },
            messages:{
                storeName:{
                    required:store_name_required
                },
                storeShortName:{
                    required:store_shortname_required
                },               
                'storeOffice-combobox':{
                    required:store_office_required
                },
                'storeUser-combobox':{
                    required:store_user_required
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:$("#frm_new_store").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:fnCreateNewStore+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Registrado"); ?>');
                            grid_list_store.clearGridData();
                            grid_list_store.trigger("reloadGrid");
                            $("div#dlg_new_store").dialog('close');
                            frm_new_store.currentForm.reset();
                        }
                    }
                });
            }
        });
        var frm_edit_store=$("#frm_edit_store").validate({
            rules:{
                storeNamee:{
                    required:true
                },
                storeShortNamee:{
                    required:true
                },              
                'storeOfficee-combobox':{
                    required:true
                }
            },
            messages:{
                storeNamee:{
                    required:store_name_required
                },
                storeShortNamee:{
                    required:store_shortname_required
                },
                'storeOfficee-combobox':{
                    required:store_office_required             

                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:$("#frm_edit_store").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:fnUpdateNewStore+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Editado"); ?>');
                            grid_list_store.clearGridData();
                            grid_list_store.trigger("reloadGrid");
                            $("div#dlg_edit_store").dialog('close');
                            frm_edit_store.currentForm.reset();
                        }
                    }
                });
            }
        });
        grid_list_store=$("#grid_list_store").jqGrid({ 
            url:fnListStoreGrid+jquery_params,
            datatype: "jsonp",
            mtype: 'POST',          
            colNames:[ 
                '<?php echo __("ID") ?>',
                '<?php echo __("Nombre") ?>',                
                '<?php echo __("Nombre Corto") ?>',                
                '<?php echo __("Nombre Oficina") ?>',                
                '<?php echo __("Dirección") ?>',                
                '<?php echo __("Telefono") ?>',                
                '<?php echo __("Encargado") ?>',                
                '<?php echo __("status") ?>',
                '<?php echo __("Fecha Creacion") ?>',
                '<?php echo __("Acciones") ?>'                
            ],
            colModel:[
                {name:'idStore',index:'idStore',key:true,hidden:true},
                {name:'storeName',index:'storeName',align:'center',search:true},                
                {name:'storeShortName',index:'storeShortName',align:'center',search:true},                
                {name:'officeName',index:'bo.name',align:'center',search:true},                
                {name:'address',index:'bo.address',align:'center',search:true},                
                {name:'phone',index:'bo.phone',align:'center',search:true},                
                {name:'encargado', index: 'bp.fullName',align:'center',search:true},                
                {name:'status', index: 'status',align:'center',hidden:true,search:true},
                {name:'dateCreation', index: 'dateCreation',align:'center',search:false},
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_store_pager', 
            sortname: 'st.idStore',         
            gridview: true,
            rowNum:10,   
            sortorder: "asc",            
            width:1200,
            height:220,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Almacenes")?>',
            gridComplete:function(){
                $.each(grid_list_store.jqGrid('getDataIDs'), function(){
                    var options='';                                      
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                    options=edit+' '+trash;                    
                    grid_list_store.setRowData(this, {actions:options});                    
                }) 
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });                    
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });                
            }
        });
        $("#grid_list_store").jqGrid('navGrid','#grid_list_store_pager',{add:false,edit:false,del:false,search:false,refresh:false});                        
        jQuery("#grid_list_store").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        $('table#grid_list_store').on('click','.edit',function(){   
        $('.frmSubtitle').text(store_editTitle);
            var ids=$(this).data('id');   
            showLoading=1;
            $.ajax({
                data:{                    
                    id:ids
                },                    
                type:"POST",
                dataType:"json",
                url:fnListByIdStore+jquery_params,
                async:false,
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $("#idStore").val(data.idStore);
                        $("#storeNamee").val(data.storeName);                        
                        $("#storeShortNamee").val(data.storeShortName);                        
                        $("#storeOfficee").val(data.idOffice);                           
                        $("#storeOfficee-combobox").val($("#storeOfficee option:selected").text()); 
                        var e=$("#storeUsere");
                        completeStoreUser(data.idOffice,e);                        
                        $("#storeUsere").val(data.idUser);                           
                        $("#storeUsere-combobox").val($("#storeUsere option:selected").text()); 
                        $("div#dlg_edit_store").dialog('open');                        
                    }                                        
                }
            });
            return false
        });
        $('table#grid_list_store').on('click','.trash',function(){            
            var ids=$(this).data('id');
            confirmBox(msg_warn_delete,deleteSubTitle,function(response){
                if(response){                    
                    $.ajax({
                        url:fnDeleteStore+jquery_params,
                        data:{ids:ids},
                        success: function(response){                              
                            if(evalResponse(response)){                                              
                                $("#grid_list_store").trigger("reloadGrid");
                            }                            
                        }
                    });          
                }
            });
            return false
        });
        $("#storeUser").combobox();
        $("#storeUsere").combobox();
        function completeStoreUser(idOffice,e){
            showLoading=1;
            $.ajax({
                url:fnCompleteStoreUser+jquery_params,
                data:{idOffice:idOffice},
                async:false,
                success: function(response){                            
                    if(evalResponse(response)){
                        var r=response.data;
                        var t='<option value=""><?php echo __("Seleccione una opción...") ?></option>';                        
                        $.each(r,function(index,data){                            
                            t=t+'<option value="'+data.id+'">'+data.display+'</option>'
                        });
                        e.html(t);
                    }
                }
            });
        }
        $("#storeOffice").on('change',function(){
            var idoffice=$("#storeOffice option:selected").val();
            var e=$("#storeUser");
            completeStoreUser(idoffice,e);
        });
        $("#storeOfficee").on('change',function(){
            var idoffice=$("#storeOfficee option:selected").val();
            var e=$("#storeUsere");
            completeStoreUser(idoffice,e);
        });
    });
</script>
<div>
    <center><div class="titleTables"><?php echo __('Administración de Almacenes'); ?></div></center>
</div>
<div>
    <div class="div_new_store">
        <button id="btn_new_store"><?php echo __('Nuevo Almacen'); ?></button>    
    </div>
    <div class="div_grid_store">
        <center>
            <div>
                <table id="grid_list_store"> </table>
                <div id="grid_list_store_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="dlg_new_store">    
    <form id="frm_new_store">
        <fieldset  class="ui-corner-all cFieldSet">  
             <legend class="frmSubtitle"></legend> 
            <div>
                <label><?php echo __('Nombre Almacén')?>:</label>
                <input type="text" id="storeName" name="storeName" />
            </div>                      
            <div>
                <label><?php echo __('Nombre Corto')?>:</label>
                <input type="text" id="storeShortName" name="storeShortName" />
            </div>
            <div>
                <label><?php echo __('Oficinas')?>:</label>
                <select id="storeOffice" name="storeOffice">
                    <option value=""><?php echo __("Seleccione una Opción"); ?></option>
                    <?php foreach ($all_office as $key => $value) {
                        ?>
                    <option value="<?php echo $value['id'] ?>"><?php echo $value['display'] ?></option>
                            <?php
                    } ?>
                </select>
            </div>
            <div>
                <label><?php echo __('Encargado')?>:</label>
                <select id="storeUser" name="storeUser">  
                    <option value=""><?php echo __('Seleccione una opción...')?></option>
                </select>
            </div>
        </fieldset>
    </form>
</div>

<div id="dlg_edit_store">    
    <form id="frm_edit_store">
        <fieldset  class="ui-corner-all cFieldSet">  
            <legend class="frmSubtitle"></legend> 
            <input type="hidden" id="idStore" name="idStore">
            <div>
                <label><?php echo __('Nombre')?>:</label>
                <input type="text" id="storeNamee" name="storeName" />
            </div>                      
            <div>
                <label><?php echo __('Nombre Corto')?>:</label>
                <input type="text" id="storeShortNamee" name="storeShortName" />
            </div>
            <div>
                <label><?php echo __('Oficinas')?>:</label>
                <select id="storeOfficee" name="storeOfficee">
                    <option value=""><?php echo __("Seleccione una Opción"); ?></option>
                    <?php foreach ($all_office as $key => $value) {
                        ?>
                    <option value="<?php echo $value['id'] ?>"><?php echo $value['display'] ?></option>
                            <?php
                    } ?>
                </select>
            </div> 
            <div>
                <label><?php echo __('Encargado')?>:</label>
                <select id="storeUsere" name="storeUsere">
                    <option value=""><?php echo __("Seleccione una Opción"); ?></option>
                </select>
            </div>
        </fieldset>
    </form>
</div>