<script type="text/javascript">
    var fnListProductGrid = "/private/products/listAllProducts";
    var fnCreateORUpdateProduct = "/private/products/createOrUpdateProduct";
    var fnDeleteProduct = "/private/products/deleteProduct";
    var fnListByIdProduct = "/private/products/listByIdProduct";
    var fnGetSubCategoryById = "/private/category/getSubCategoryById";
    var msg_warn_delete = '<?php echo __("Esta Seguro de Eliminar Este Producto?") ?>';
    var title_delete = '<?php echo __("Eliminar Producto") ?>';
    var product_newTitle = '<?php echo __("Nuevo Producto") ?>';
    var flags = {};
    flags.isEditar = false;
    flags.isClear =false;
    var option;
    var option2;
    var product_editTitle = '<?php echo __("Editar Producto") ?>';
    var aux_fir_aliasProduct;
    var new_project = false;
    var aux_correlative;

    $(document).ready(function() {

        $('#idSubCategoryDIV').hide();
        $("#idbrand").combobox();

        generate_subCategory = function(idCategory) {
            if(flags.isEditar==false)
            $('#th_aliasProduct').val('');
            var select_sub_cat = $('#idSubCategory');
            
            $.ajax({
                url: fnGetSubCategoryById + jquery_params,
                data: {
                    id: function() {
                        return idCategory;
                    }
                },
                type: "POST",
                dataType: "json",
                async: false,
                success: function(response) {
                    console.log($('#idCategory option:selected').data('codigo'));
                    console.log(aux_fir_aliasProduct);
                    if(parseInt($('#idCategory option:selected').data('codigo'))!= parseInt(aux_fir_aliasProduct) || new_project ==true)
                        $('#th_aliasProduct').val(response.correlative);
                    else
                         $('#th_aliasProduct').val(aux_correlative)
                    if (response.data != '') {
                        if(flags.isClear==true)
                        $('input#sec_aliasProduct').val("");  
                        $('#idSubCategoryDIV').show();
                        option = '<option data-s_codigo="[CODE]" value="[VALUE]">[LABEL]</option>';
                        $('option', select_sub_cat).remove();
                        $(select_sub_cat).append(option.replace('[VALUE]', "", 'g').replace('[LABEL]', 'Seleccina Sub Categoria', 'g'));
                        $.each(response.data, function(index, array) {
                            $(select_sub_cat).append(option.replace('[VALUE]', array['idCategory'], 'g').replace('[LABEL]', array['superCategoryName'], 'g').replace('[CODE]', array['codigo'], 'g'));
                        });
                    } else {                  
//                        if(flags.isEditar==false)
                        $('input#sec_aliasProduct').val("00");  
                        option2 = '<option data-s_codigo="[CODE]" value="[VALUE]">[LABEL]</option>';
                        $('option', select_sub_cat).remove();
                        $(select_sub_cat).append(option2.replace('[VALUE]', "", 'g').replace('[LABEL]', "", 'g'));
                        $('#idSubCategoryDIV').hide();
                        msgBox('<?php echo __("No se asignado una Sub Categoria"); ?>', 'Error');
                    }
                }
            });
        }

        $("#idCategory").combobox({
            selected: function(event, ui) {
                var idCat = $(this).val();
                var cod = $('#idCategory option:selected').data('codigo');
                $('input#fir_aliasProduct').val(cod);
                $('#idSubCategory').val("");
                $("#idSubCategory-combobox").val($("#idSubCategory option:selected").text());   
                flags.isClear=true;
                generate_subCategory(idCat);
            }
        });

        $("#idSubCategory").combobox({
            selected: function(event, ui) {
                var s_cod = '';
                s_cod = $('#idSubCategory option:selected').data('s_codigo');
                //$('input#aliasProduct').val(temp_code.substring(0,2)+'-'+s_cod);
                $('input#sec_aliasProduct').val(s_cod);
            }
        });

        $("#unitMeasures").combobox();

        $('#maximunStock,#minimunStock,#codePrint').setMask();

        $('#productPriceSell').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });

        $("#new_brand").button({
            icons: {
                primary: 'ui-icon-plusthick'
            },
            text: false
        });

        $("#btn_new_product").button({
            icons: {
                primary: 'ui-icon-plusthick'
            },
            text: true
        }).click(function() {
            new_project = true ;
            resetForm($('form#frm_new_product'));
            fnLoadCheck();
            $("#idProduct").val(0);
            /**** Desabilita Generación de Código ****/
            $("#idCategory-combobox").autocomplete("option", "disabled", false);
            $("#idCategory-combobox").attr("disabled", false);
            $("#idSubCategory-combobox").autocomplete("option", "disabled", false);
            $("#idSubCategory-combobox").attr("disabled", false);
            /**** FIN Desabilita Generación de Código ****/
            $(".frmSubtitle").text(product_newTitle);
            $("#dlg_new_product").dialog('open');
        });

        $("div#dlg_new_product").dialog({
            width: 490,
            title: '<?php echo __("Producto"); ?>',
            autoOpen: false,
            resizable: false,
            modal: true,
            buttons: {
                '<?php echo __('Aceptar') ?>': function() {
                    $("#frm_new_product").submit();

                },
                '<?php echo __('Cancelar') ?>':function() {
                    $('#errorMessages').hide();
                    $("#idProduct").val(0);
                    frm_new_product.currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });
        grid_list_product = $("#grid_list_product").jqGrid({
            url: fnListProductGrid + jquery_params,
            datatype: "jsonp",
            mtype: 'POST',
            colNames: [
                '<?php echo __("ID") ?>',
                '<?php echo __("Código") ?>',
                '<?php echo __("Producto") ?>',
                '<?php echo __("Marca") ?>',
                '<?php echo __("Categoría") ?>',
                '<?php echo __("Unidad Medida") ?>',
                '<?php echo __("Detalle") ?>',
                '<?php echo __("Stock") ?>',
                '<?php echo __("Almacén") ?>',
                '<?php echo __("Precio Venta") ?>',
                '<?php echo __("Usuario Registro") ?>',
                '<?php echo __("status") ?>',
                '<?php echo __("Fecha Creación") ?>',
                '<?php echo __("Acciones") ?>'
                    ],
            colModel: [
                {name: 'id', index: 'idProduct', key: true, hidden: true},
                {name: 'aliasProduct', index: 'aliasProduct', align: 'center', search: true},
                {name: 'productName', index: 'productName', width: 350, align: 'center', search: true},
                {name: 'brandNameCommercial', index: 'brandNameCommercial', align: 'center', search: true},
                {name: 'categoryName', index: 'categoryName', width: 250, align: 'center', search: true},
                {name: 'productUnitMeasures', index: 'productUnitMeasures', align: 'center', search: false},
                {name: 'haveDetail', index: 'haveDetail', align: 'center', search: false},
                {name: 'productStock', index: 'productStock', align: 'center', search: false},
                {name: 'storeName', index: 'storeName', align: 'center', search: false},
                {name: 'productPriceSell', index: 'productPriceSell', align: 'center', search: false},
                {name: 'userCreate', index: 'p.fullName', align: 'center', hidden: true},
                {name: 'status', index: 'status', align: 'center', hidden: true},
                {name: 'dateCreation', index: 'dateCreation', align: 'center', hidden: true},
                {name: 'actions', index: 'actions', align: 'center', width: 120, search: false}
            ],
            pager: '#grid_list_product_pager',
            sortname: 'p.idProduct',
            gridview: true,
            rowNum: 15,
            rowList: [15, 30, 50],
            width: 1200,
            height: 'auto',
            shrinkToFit: true,
            rownumWidth: 10,
            viewrecords: true,
            caption: '<?php echo __("Lista de Productos") ?>',
            gridComplete: function() {
                $.each(grid_list_product.jqGrid('getDataIDs'), function() {
                    var options = '';
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-id=\"" + this + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\"" + this + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    print = "<a class=\"print\" style=\"cursor: pointer;\" data-id=\"" + this + "\" title=\"<?php echo __('Imprimir'); ?>\" ><?php echo __('Imprimir'); ?></a>";
                    options = edit + trash + print;
                    grid_list_product.setRowData(this, {actions: options});
                })
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: 'ui-icon-trash'},
                    text: false
                });
                $('.print').button({
                    icons: {primary: 'ui-icon-print'},
                    text: false
                });
            }
        });
        $("#grid_list_product").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});
        $('table#grid_list_product').on('click', '.edit', function() {
            new_project = false;
            flags.isClear=false;
            flags.isEditar = true;
            fnLoadCheck();
            $(".frmSubtitle").text(product_editTitle);
            var idp = $(this).data('id');
            $.ajax({
                data: {
                    id: idp
                },
                type: "POST",
                dataType: "jsonp",
                url: fnListByIdProduct + jquery_params,
                beforeSend: function() {
                    $("#loading").dialog("open");
                },
                complete: function() {
                    $("#loading").dialog("close");
                },
                success: function(res) {
                    if (evalResponse(res)) {
                        var data = res.data;
                        var category = res.category;
                        $("#idProduct").val(data.idProduct);
<?php if ($bTagGenerator): ?>
                            var mystr = data.aliasProduct;
                            $("#fir_aliasProduct").val(mystr.substring(0, 2));
                            aux_fir_aliasProduct = $("#fir_aliasProduct").val();
                            $("#sec_aliasProduct").val(mystr.substring(2, 4));
                            $("#th_aliasProduct").val(mystr.substring(4, 7));
                            aux_correlative =  $("#th_aliasProduct").val();
<?php else: ?>
                            $("#aliasProduct").val(data.aliasProduct);
<?php endif; ?>
                        $("#productName").val(data.productName);
                        $("#idbrand").val(data.idBrand);
                        $("#haveDetail").val([data.haveDetail]);
                        $('#haveDetail').button('refresh');
                        $("#idbrand-combobox").val($("#idbrand option:selected").text());
                        $("#idCategory").val(category.idCategory);
                        $("#idCategory-combobox").val($("#idCategory option:selected").text());
<?php if ($bTagGenerator): ?>
                            /**** Desabilita Generación de Código ****/
                         //   $('#idSubCategoryDIV').hide();
                         if(category.idSuperCategory!=null){
                            generate_subCategory(category.idCategory);
                            $("#idSubCategory").val(category.idSuperCategory);
                            $("#idSubCategory-combobox").val($("#idSubCategory option:selected").text());
                            $("#idCategory-combobox").autocomplete("option", "disabled", false);
                            $("#idCategory-combobox").attr("disabled", false);
                            $("#idSubCategory-combobox").autocomplete("option", "disabled", false);
                            $("#idSubCategory-combobox").attr("disabled", false);
                         }
                         else
                            $('#idSubCategoryDIV').hide();
                            /**** FIN Desabilita Generación de Código ****/
<?php endif; ?>
                        $("#unitMeasures").val(data.productUnitMeasures);
                        $("#unitMeasures-combobox").val($("#unitMeasures option:selected").text());
                        $("#minimunStock").val(data.minimumstock);
                        $("#maximunStock").val(data.maximumstock);
                        $("#productPriceSell").val(data.productPriceSell);
                        $("div#dlg_new_product").dialog('open');
                    }
                }
            });
            return false;
        });
        $('table#grid_list_product').on('click', '.trash', function() {
            var idp = $(this).data('id');
            confirmBox(msg_warn_delete, title_delete, function(response) {
                if (response) {
                    $.ajax({
                        url: fnDeleteProduct + jquery_params,
                        data: {idp: idp},
                        success: function(response) {
                            if (evalResponse(response))
                            {
                                $("#grid_list_product").trigger("reloadGrid");
                            } else {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
            });
            return false;
        });
        var frm_new_product = $("#frm_new_product").validate({
            rules: {
                productName: {
                    required: true
                },
                'idbrand-combobox': {
                    required: true
                },
                idbrand: {
                    required: true
                },
                'idCategory-combobox': {
                    required: true
                },
                idCategory: {
                    required: true
                },
//                'idSubCategory-combobox': {
//                    required: function() {
//                        return ($('#idSubCategory').val()<0);
//                     }                
//                 },
//                idSubCategory: {
//                    required: function() {
//                        return ($('#idCategory').val()>0);
//                     }
//                },
                'unitMeasures-combobox': {
                    required: true
                },
                unitMeasures: {
                    required: true
                },
                productPriceSell: {
                    required: true
                },
                maximunStock: {
                    min: function() {
                        var max = $("#minimunStock").val();
                        return parseInt(max);
                    }
                }

            },
            messages: {
                productName: {
                    required: '<?php echo __("Escriba Nombre Producto") ?>'
                },
                'idbrand-combobox': {
                    required: '<?php echo __("Seleccione Marca") ?>'
                },
                idbrand: {
                    required: '<?php echo __("Seleccione Marca") ?>'
                },
                'idCategory-combobox': {
                    required: '<?php echo __("Seleccione Categoria") ?>'
                },
                idCategory: {
                    required: '<?php echo __("Seleccione Categoria") ?>'
                },
//                'idSubCategory-combobox': {
//                    required: '<?php echo __("Seleccione Subcategoria") ?>'
////                },
//                idSubCategory: {
//                    required: '<?php echo __("Seleccione Subcategoria") ?>'
//                },
                'unitMeasures-combobox': {
                    required: '<?php echo __("Seleccione Unidad de Medida") ?>'
                },
                unitMeasures: {
                    required: '<?php echo __("Seleccione Unidad de Medida") ?>'
                },
                productPriceSell: {
                    required: '<?php echo __("Ingrese Precio de Venta") ?>'
                },
                maximunStock: {
                    min: '<?php echo __("El stock maximo debe ser mayor al stock minimo") ?>'
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {
                $.ajax({
                    data: $("#frm_new_product").serializeObject(),
                    type: "POST",
                    dataType: "json",
                    url: fnCreateORUpdateProduct + jquery_params,
                    beforeSend: function() {
                        $("#loading").dialog("open");
                    },
                    complete: function() {
                        $("#loading").dialog("close");
                    },
                    success: function(res) {
                        if (evalResponse(res))
                        {
                            msgBox('<?php echo __("Guardado"); ?>');
                            grid_list_product.clearGridData();
                            grid_list_product.trigger("reloadGrid");
                            $("div#dlg_new_product").dialog('close');
                            frm_new_product.currentForm.reset();
                            flags.isEditar = false;
                        }
                    }
                });

            }
        });
        $("#haveDetail").button();

        //Impresión de Códigos de Barra
        $('#grid_list_product').on('click', '.print', function() {
            var idp = $(this).data('id');
            $('#print_idProduct').val(idp);
            $('#printCode').dialog('open');
        });
        $("#printCode").dialog({
            width: "auto",
            title: '<?php echo __("Impresión de Códigos"); ?>',
            autoOpen: false,
            resizable: false,
            modal: true,
            buttons: {
                '<?php echo __('Imprimir') ?>': function() {
                    var cantidad = $("#codePrint").val();
                    var id = $('#print_idProduct').val();
                    if (cantidad == 0 || cantidad == "")
                        msgBox('<?php echo __("Ingresar número mayor a 0"); ?>');
                    else {
                        window.open('/private/products/printDataProduct?cantidad=' + cantidad + '&id=' + id, '<?php echo __("Impresión de Códigos") ?>');
                        $(this).dialog('close');
                    }
                },
                '<?php echo __('Cancelar') ?>':function() {
                    //$("div#printCode").currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });

    });
    function resetForm($form) {
        $form.find('input:text, input:password, input:file, select, textarea').val('');
        $form.find('input:radio, input:checkbox')
                .removeAttr('checked').removeAttr('selected');
    }

</script>
<style type="text/css">
    .div_new_product{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_grid_product{        
        padding-top: 10px;
    }
    #frm_new_product{
        padding: 10px;
    }
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        text-align: right;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 160px;
    }
    .cFieldSet>div input[type="password"], textarea {
        padding: 3px; 
        width: 180px;
    }
    .input_label{
        height: 18px;
        width: 180px;
    }

    #haveDetail+label{
        width: auto;        
    }
    .input_codigo{
        background-color: #90C140;
        width: 30px;
        height: 25px;
        margin: 5px;
        text-align: center;

    }
    .ui-autocomplete-input{
        height: 20px;
        width: 180px;
    }   
    .label_output div span.ui-button-text {
        float: none;
        font-weight: normal;
        max-width: none;
    }
    .label_output div label.ui-button{

        font-weight: bold;
        max-width: none;
        min-width: none;
        padding: 0;
        white-space: pre-line;
    }
    .label_output div label.ui-state-default{

        font-weight: bold;
    }
    .label_output div label.ui-state-active{

        font-weight: bold;
    }

    .label_output_ui div label {
        text-align: left;
    }
</style>

<div>
    <center><div class="titleTables"><?php echo __('Administración de Productos'); ?></div></center>
</div>

<div>
    <div class="div_new_product">
        <button id="btn_new_product"><?php echo __('Nuevo Producto'); ?></button>    
    </div>
    <div class="div_grid_product">
        <center>
            <div>
                <table id="grid_list_product"> </table>
                <div id="grid_list_product_pager"></div>
            </div>
        </center>
    </div>
</div>
<div id="printCode" class="dialog-modal">
    <fieldset  class="ui-corner-all cFieldSet"> 
        <div class="label_output">
            <div>
                <label><?php echo __('Nro de Impresiones') ?>:</label>
                <input type="text" id="codePrint" name="codePrint" style="width: 30px;" alt="number"/>
                <input type="hidden" id="print_idProduct" name="print_idProduct" />
            </div>  
        </div>
</div>
<div id="dlg_new_product" class="dialog-modal"> 

    <form id="frm_new_product" method="post">
        <input type="hidden" id="idProduct" name="idProduct" value="0" />
        <fieldset  class="ui-corner-all cFieldSet"> 
            <legend class="frmSubtitle"></legend>

            <div class="label_output"> 
                <?php if ($bTagGenerator): ?>
                    <div>
                        <label><?php echo __('Código Generado') ?>:</label>
                        <input type="text" id="fir_aliasProduct" name="fir_aliasProduct" readonly="true" class="input_codigo" size="5"/>
                        <input type="text" id="sec_aliasProduct" name="sec_aliasProduct" readonly="true" class="input_codigo" size="5"/>
                        <input type="text" id="th_aliasProduct" name="th_aliasProduct" readonly="true" class="input_codigo" size="5"/>
                    </div>  
                <?php else: ?>

                    <div>
                        <label><?php echo __('Código') ?>:</label>
                        <input type="text" id="aliasProduct" name="aliasProduct" class="input_label" size="5"/>
                    </div>  
                <?php endif; ?>
                <div>
                    <label><?php echo __('Nombre Producto') ?>:</label>
                    <input type="text" id="productName" name="productName" class="input_label"/>
                </div>                  
                <div>
                    <label><?php echo __('Nombre Marca') ?>:</label>
                    <select id="idbrand" name="idbrand">
                        <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                        <?php
                        foreach ($brand as $v) {
                            ?>
                            <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>                 
                    <div class="clear"></div>
                </div>                  
                <div>
                    <label><?php echo __('Nombre Categoria') ?>:</label>
                    <select id="idCategory" name="idCategory">
                        <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                        <?php
                        foreach ($category as $v) {
                            ?>
                            <option data-codigo="<?php echo $v['codigo'] ?>" value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>  
                    <div class="clear"></div>
                </div>  
                <div id="idSubCategoryDIV" class="none">
                    <label><?php echo __('Sub Categorias') ?>:</label>
                    <select id="idSubCategory" name="idSubCategory">
                         <option value=""><?php echo __("Seleccina Sub Categoria"); ?></option>
                    </select>
                </div>
                <div>
                    <label><?php echo __('Unidad Medida') ?>:</label>
                    <select id="unitMeasures" name="unitMeasures">
                        <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                        <?php
                        foreach ($measures as $v) {
                            ?>
                            <option value="<?php echo $v['value']; ?>"><?php echo $v['display']; ?></option>
                            <?php
                        }
                        ?>
                    </select>  
                    <div class="clear"></div>
                </div>
                <div>
                    <label><?php echo __('Tiene Detalle') ?>:</label>
                    <input type="checkbox" id="haveDetail" name="haveDetail" value="1" class="input_label"/><label for="haveDetail">Detalle</label>
                    <div class="clear"></div>
                </div>
                <div>
                    <label><?php echo __('Stock Mínimo') ?>:</label>
                    <input type="text" id="minimunStock" name="minimunStock" alt="number" class="input_label"/>
                </div>
                <div>
                    <label><?php echo __('Stock Máximo') ?>:</label>
                    <input type="text" id="maximunStock" name="maximunStock" alt="number" class="input_label"/>
                </div>
                <div>
                    <label><?php echo __('Precio Venta') ?>:</label>
                    <input type="text" id="productPriceSell" name="productPriceSell" class="input_label"/>
                </div>                

        </fieldset>
    </form>
</div>


