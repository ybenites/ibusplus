<style type="text/css">
    .div_new_move{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_grid_move{        
        padding-top: 10px;
    }
    #frm_new_move{
        padding: 10px;
    }
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 130px;
    }
    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 3.5px;       
    }
</style>
<script type="text/javascript">
    /*--url--*/
    var fnListMoveGrid="/private/typemove/listAllMove";
    var fnCreateOrUpdateMove="/private/typemove/createOrUpdateMove";
    var fnListByIdMove="/private/typemove/listByIdMove";
    var fnDeleteMove="/private/typemove/deleteMove";
    
    /*--text--*/       
    var typemove_form_Title = "<?php echo __("Tipo Movimiento"); ?>";
    var typemove_newTitle = "<?php echo __("Nuevo Tipo Movimiento"); ?>";
    var typemove_editTitle = "<?php echo __("Editar Tipo Movimiento"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Tipo Movimiento"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este Tipo de Movimiento?"); ?>';
    var typemove_status_required = '<?php echo __("Seleccione un estado de movimiento."); ?>';
    var typemove_name_required = '<?php echo __("El Nombre del Movimiento es obligatorio."); ?>';
    
    var msg_warn_delete='<?php echo __("Esta Seguro de Eliminar Este Movimiento?") ?>';
    var title_delete='<?php echo __("Eliminar Movimiento") ?>';
    $(document).ready(function(){
        
        var grid_list_move=$("#grid_list_move").jqGrid({ 
            url:fnListMoveGrid+jquery_params,
            datatype: "jsonp",
            mtype: 'POST',          
            colNames:[ 
                '<?php echo __("ID") ?>',
                '<?php echo __("Nombre") ?>',                
                '<?php echo __("Estado Mov.") ?>',                                
                '<?php echo __("Usuario Registro") ?>',
                '<?php echo __("status") ?>',
                '<?php echo __("Fecha Creacion") ?>',
                '<?php echo __("Acciones") ?>'                                     
            ],
            colModel:[
                {name:'idTypeMove',index:'idTypeMove',key:true,hidden:true},
                {name:'moveName',index:'tm.typeMoveName',align:'center',search:true},                
                {name:'moveSate',index:'tm.typeMoveState',align:'center',search:true},                           
                {name:'userCreate', index: 'p.fullName',align:'center',search:true},
                {name:'status', index: 'status',align:'center',hidden:true,search:false},
                {name:'dateCreation', index: 'dateCreation',align:'center',search:false},
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_move_pager', 
            sortname: 'tm.idTypeMove',         
            gridview: true,
            rowNum:10,   
            sortorder: "asc",            
            width:1200,
            height:220,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Tipo de Movimientos") ?>',
            gridComplete:function(){
                $.each(grid_list_move.jqGrid('getDataIDs'), function(){
                    var options='';                                      
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+this+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
                    options=edit+' '+trash;                    
                    grid_list_move.setRowData(this, {actions:options});                    
                }) 
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                });                    
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                });                
            }
        });
        jQuery("#grid_list_move").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        var validator_typeMove=$("#frm_new_move").validate({
            rules:{
                'typeMoveState-combobox':{
                    required:true
                },
                typeMoveName:{
                    required:true
                }
            },
            messages:{                            
                'typeMoveState-combobox':{
                    required:typemove_status_required
                },
                typeMoveName:{
                    required:typemove_name_required
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                $.ajax({
                    data:$("#frm_new_move").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:fnCreateOrUpdateMove+jquery_params,
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Registrado"); ?>');
                            grid_list_move.clearGridData();
                            grid_list_move.trigger("reloadGrid");
                            $("div#dlg_new_move").dialog('close');
                        }
                    }
                });
            }
        });
        $('table#grid_list_move').on('click','.trash',function(){            
            var id=$(this).data('id');
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:fnDeleteMove+jquery_params,
                        data:{id:id},
                        success: function(response){    
                            if(evalResponse(response)){
                                $("#grid_list_move").trigger("reloadGrid");
                            }                            
                        }
                    });          
                }
            });
            return false
        });
        $('table#grid_list_move').on('click','.edit',function(){    
            $('.frmSubtitle').text(typemove_editTitle);
            var id=$(this).data('id');            
            $.ajax({
                data:{                    
                    id:id
                },                    
                type:"POST",
                dataType:"jsonp",
                url:fnListByIdMove+jquery_params,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $("#id").val(data.idTypeMove);
                        $("#typeMoveState").val(data.typeMoveState);                           
                        $("#typeMoveState-combobox").val($("#typeMoveState option:selected").text());                        
                        $("#typeMoveName").val(data.typeMoveName);                                                                      
                        $("#dlg_new_move").dialog('open');                        
                    }                                        
                }
            });
            return false
        });
        $("div#dlg_new_move").dialog({
            width:450,
            title:typemove_form_Title,
            autoOpen: false,
            resizable: false,
            modal:true,
            closeonEscape:true,
            dragable: false,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_new_move").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    $(this).dialog('close'); 
                }
            },
            beforeClose: function(event, ui) {
                validator_typeMove.currentForm.reset();
                validator_typeMove.resetForm();
                $('input, select, textarea', validator_typeMove.currentForm).removeClass('ui-state-error');
            }
        });
        $("#typeMoveState").combobox();
        $("#btn_new_move").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        $("#btn_new_move").on('click',function(){
            $('.frmSubtitle').text(typemove_newTitle);
            $("#dlg_new_move").dialog('open');
        });
        
    });
</script>
<div>
    <center><div class="titleTables"><?php echo __('Administración de Tipo de Movimientos'); ?></div></center>
</div>

<div>
    <div class="div_new_move">
        <button id="btn_new_move"><?php echo __('Nuevo Movimiento'); ?></button>    
    </div>
    <div class="div_grid_move">
        <center>
            <div>
                <table id="grid_list_move"> </table>
                <div id="grid_list_move_pager"></div>
            </div>
        </center>
    </div>
</div>
<div id="dlg_new_move" class="dialog-modal">    
    <form id="frm_new_move" method="post">
        <fieldset  class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle"></legend>
            <input type="hidden" id="id" name="id" />
            <div>
                <label><?php echo __('Estado Movimiento') ?>:</label>
                <select id="typeMoveState" name="typeMoveState">
                    <option value=""><?php echo __("Seleccione una Opción"); ?></option>
                    <?php foreach ($statusMove as $key => $value) {
                        ?>
                        <option value="<?php echo $value['value'] ?>"><?php echo $value['display'] ?></option>
                    <?php }
                    ?>
                </select>
            </div>
            <div>
                <label><?php echo __('Nombre') ?>:</label>
                <input type="text" id="typeMoveName" name="typeMoveName" />
            </div>                                             
        </fieldset>
    </form>
</div>