<style type="text/css">
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    #gbox_grid_list_stock{
        margin-left: auto;
        margin-right: auto;
    }
    .input_label{
        height: 18px;
        width: 180px;
    }
    .clear{
        /*clear: both;*/
    }
    #frm_search{
        width:1200px;margin-left:auto;margin-right:auto;margin-bottom:5px;margin-top: 5px;
    }    
    #frm_search>fieldset{
        padding:10px;
    }
    #frm_search>fieldset label{
        float: left;
        font-weight: bolder;
        text-align: right;
        width: 150px;
    }
    #frm_search>fieldset input{
        width:250px;
    }
    #frm_search>fieldset>div{
        float: left;
        width:500px;        
    }
    #frm_search>fieldset>div>div{
        padding:3px;
    }
    #frm_edit_productdetail{
        padding: 10px;
        height: auto;
        min-height: 59.4px;
        width: auto;
    }
    .cFieldSet>div>label{
        float: left;
        text-align: right;
        margin-right: 5px;
        margin-top: 5px;
        font-weight: bolder;
        width: 117px;
    }
    .cFieldSet>div{
        padding:10px;
    }
    .ui-jqgrid .ui-pg-selbox{
        height: 23px;
    }
</style>
<script type="text/javascript">
    var url_listAllProduct="/private/stockproduct/listAllProduct";
    var url_listAllDetailProduct="/private/stockproduct/listAllDetailProduct";
    var url_listByDetail= "/private/stockproduct/listByDetailProduct";
    var fnUpdateProduct = "/private/stockproduct/editDetailProduct";
    var fnDeleteProduct = "/private/stockproduct/deleteDetailProduct";
    var fnGetSubCategoryById="/private/category/getSubCategoryById"+jquery_params;
    var product_editTitle = '<?php echo __("Detalle de Producto") ?>';
    var msg_warn_delete='<?php echo __("Esta Seguro de Eliminar Este Detalle de Producto?") ?>';
    var title_delete='<?php echo __("Eliminar Detalle de Producto") ?>';
    
    $(document).ready(function(){
        
        $("#unitPrice").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });      
        
        $("#edit_productdetail").dialog({
            width:"auto",
            title:'<?php echo __("Impresión de Códigos"); ?>',
            autoOpen: false,
            resizable: false,
            modal: true,
            
            title:product_editTitle,
       
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $("#frm_edit_productdetail").submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();
                    // frm_edit_category.currentForm.reset();
                    $(this).dialog('close');                      
                }
            }
        });
        
        //valida formulario del subjqgrid 
        var frm_edit_productdetail = $("#frm_edit_productdetail").validate({
            //            rules:{
            //                serie_electronic:{
            //                    required:true
            //                }             
            //            },
            //            messages:{
            //                serie_electronic:{
            //                    required:'<?php echo __("Ingrese la serie electronica") ?>'
            //                }
            //            },
            //            errorContainer:'#errorMessages',
            //            errorLabelContainer: "#errorMessages .content .text #messageField",
            //            wrapper: "p",
            //            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            //            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                                    
                $.ajax({
                    data:$("#frm_edit_productdetail").serializeObject(),                    
                    type:"POST",
                    dataType:"json",
                    url:fnUpdateProduct+jquery_params,
                    success:function(res){
                        if(evalResponse(res))               
                        {                          
                            msgBox('<?php echo __("Guardado"); ?>');
                           // grid_list_stock.clearGridData(); 
                            $("#edit_productdetail").dialog('close');
                            subgrid_stock.trigger('reloadGrid');
                            frm_edit_productdetail.currentForm.reset();
                        }
                    }
                });
                
            }
        })
        grid_list_stock=$("#grid_list_stock").jqGrid({
            url:url_listAllProduct+jquery_params,
            datatype: "jsonp",
            mtype: 'POST',  
            postData:{
                idProduct:function(){return $("#idProduct").val()},
                idStore:function(){return $("#idStore").val()},
                idBrand:function(){return $("#idBrand").val()},
                idCategory:function(){return $("#idCategory").val()},
                idSubCategory: function(){return $("#idSubCategory").val()}
            },
            colNames:[ 
                '<?php echo __("ID") ?>',
                '<?php echo __("Id Producto") ?>',
                '<?php echo __("Código") ?>',                
                '<?php echo __("Producto") ?>',                
                '<?php echo __("Marca") ?>',                
                '<?php echo __("Categoria") ?>',            
                '<?php echo __("Id Almacén") ?>',            
                '<?php echo __("Almacén") ?>',            
                '<?php echo __("Precio") ?>',                                                                                               
                '<?php echo __("stock") ?>',                
                // '<?php echo __("Acciones") ?>'                 
            ],
            colModel:[
                {name:'idStock',index:'idStock',key:true,hidden:true},
                {name:'idProduct',index:'idProduct',hidden:true},
                {name:'aliasProduct',index:'aliasProduct',align:'center',search:true},                
                {name:'productName',index:'productName',align:'center',search:true},                
                {name:'brandNameCommercial',index:'brandNameCommercial',align:'center',search:true},                
                {name:'categoryName',index:'categoryName',align:'center',search:true},                                                                                       
                {name:'idStore',index:'idStore',align:'center',search:true,hidden:true},                                                                                       
                {name:'storeName',index:'storeName',align:'center',search:true},                                                                                       
                {name:'unitPrice', index: 'unitPrice',align:'center',hidden:true},                
                {name:'stock', index: 'stock',align:'center'},
                //{name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_stock_pager', 
            sortname: '',         
            gridview: true,            
            rowNum:20,
            rowList:[30,50,100],
            sortorder: "asc",            
            width:1200,
            height:350,
            shrinkToFit:true,
            rownumWidth:40,
            rownumbers: true,
            viewrecords: true,                                        
            caption:'<?php echo __("Lista de Productos") ?>',
            gridComplete:function(){              
            },
            subGrid:true,
            subGridRowExpanded: function(subgrid_id, row_id) {
                var data=$("#grid_list_stock").jqGrid("getRowData",row_id);
                var subgrid_table_id;
                var subgrid_table_id, pager_id;
		pager_id = "p_"+subgrid_table_id;
                subgrid_table_id = subgrid_id+"_t";
                jQuery("#"+subgrid_id).html("<table data-id_product='"+data.idProduct+"' id='"+subgrid_table_id+"' class='scroll'></table>");
                subgrid_stock= $("#"+subgrid_table_id).jqGrid({
                    url:url_listAllDetailProduct+jquery_params,
                    datatype: "jsonp",
                    mtype: 'POST',
                    postData:{
                        product_id:function(){return data.idProduct},
                        idStore:function(){return data.idStore}
                    },
                    colNames: ['iddp','hd','idp','Producto','Serie Electrónica','Precio Venta','Cantidad','Descripción','Fecha Registro','Acciones'],
                    colModel: [
                        {name:"idProductDetail",index:"idProductDetail",width:80,hidden: true, key:true},
                        {name:"haveDetail",index:"haveDetail",width:80,hidden: true},
                        {name:"idProduct",index:"idProduct",width:130,hidden: true},
                        {name:"productName",index:"productName",width:100,align:"right",hidden: true},
                        {name:"serie_electronic",index:"serie_electronic",width:140,search:true},
                        {name:"unitPrice",index:"unitPrice",width:210,align:"right",sortable:false,search:false},                        
                        {name:"amount",index:"amount",width:210,align:"right",sortable:false,search:false},                        
                        {name:'description',index:'description', width:130,search:false},
                        {name:'creationDate',index:'creationDate', width:130,search:false},
                        {name:'actions', index: 'actions',align:'center',width:80,search:false}
                    ],
                    //scroll:true,
                    rowNum:50,
                    pager: pager_id,
                    rownumWidth:50,
                    altRows:true,
                    rownumbers: true,
                    viewrecords: true,
                    gridview: true,
                    shrinkToFit:true,
                    sortname: 'idProduct',
                    sortorder: "ASC",
                    gridComplete:function(){
                        $.each($("#"+subgrid_table_id).jqGrid('getDataIDs'), function(i,d){
                            var options='';
                            edit = "<a class=\"edit\" style=\"cursor: pointer;\" data-idp=\""+data.idProduct+"\"  data-id=\""+this+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                           // trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-ids=\""+data.idStore+"\" data-idp=\""+data.idProduct+"\"   data-id=\""+this+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                            var renDatos = $("#"+subgrid_table_id).getRowData(this);
                            
                            if(renDatos.haveDetail == 1){
                               options=edit;
                                   //+trash;                                      
                            }else{  
                                options=edit;  
                            }
                            $("#"+subgrid_table_id).setRowData(this, {actions:options});                    
                        }) 
                        $('.edit').button({
                            icons:{primary: 'ui-icon-pencil'},
                            text: false
                        });                    
//                        $('.trash').button({
//                            icons:{primary: 'ui-icon-trash'},
//                            text: false
//                        });                
                    }
                });
                $("#"+subgrid_table_id).jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false}); 
                $("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{edit:false,add:false,del:false})
                $("#"+subgrid_table_id).on('click','.edit',function(){
                    $("#edit_productdetail").dialog('open');
                    var iddp=$(this).data('id');
                    var idp=$(this).data('idp');
                    console.log(idp+" "+iddp);
                    $.ajax({
                        data:{                    
                            iddp:iddp,
                            idp:idp
                        },                    
                        type:"POST",
                        dataType:"json",
                        url:url_listByDetail,
                        success:function(res){
                            if(evalResponse(res)){
                                var data = res.data[0];
                                var detail = res.detail;
                                $("#detail").val(detail);
                                if(detail == 1)
                                {
                                    $("#idProductDetail").val(data.idProductDetail);
                                    $("#serie_electronic").val(data.serie_electronic);
                        
                                }else {
                                    $("#idProductDetail").val(data.idLot);
                                    $("#serie_electronic").val(data.lotName)
                                }
                                $("#idProduct").val(data.idProduct);
                                $("#unitPrice").val(data.unitPrice);
                                $("#amount").val(data.amount);
                                $("#description").val(data.description);
                                $("#creationDate").val(data.creationDate);
                                
                                
                            }                                        
                        }
                    });
                    return false
                });
                $("#"+subgrid_table_id).on('click','.trash',function(){            
                    var iddp=$(this).data('id'); 
                    var idp=$(this).data('idp');
                    var ids = $(this).data('ids');
                    confirmBox(msg_warn_delete,title_delete,function(response){
                        if(response){                    
                            $.ajax({
                                url:fnDeleteProduct,
                                data:{iddp:iddp,
                                    idp:idp,
                                    ids:ids
                                },
                                success: function(response){                            
                                    if(evalResponse(response)){                                              
                                        $("#"+subgrid_table_id).trigger("reloadGrid");
                                    }     
                                }
                            });          
                        }
                    });
                    return false;
                });
                
            }
        });
       
        $("#idProduct").combobox({
            selected:function(ui,event){
                $("#idProduct").trigger('change');
            }
        });
        $("#idSubCategory").combobox({
               selected:function(ui,event){
                $("#idSubCategory").trigger('change');
            }
        });
        $("#idCategory").combobox({
            selected:function(ui,event){
                $("#idCategory").trigger('change');
                $('#idSubCategory').val('');
                $.ajax({
                url:fnGetSubCategoryById,
                data:{
                    id: $(this).val()
                },
                type:"POST",
                dataType:"json",
                async:false,
                success:function(response){
                    //if(flags.isEditar==false)
                    //$('#th_aliasProduct').val(response.correlative);
                    //$('#idSubCategory').empty();
                    
                    var select_sub_cat =$('#idSubCategory');
                    if(response.data != ''){
                        $('#idSubCategoryDIV').show();
                        var option = '<option data-s_codigo="[CODE]" value="[VALUE]">[LABEL]</option>';
                        $('option', select_sub_cat).remove();
                        $(select_sub_cat).append(option.replace('[VALUE]', "",'g').replace('[LABEL]','Seleccina Sub Categoria', 'g'));
                        $.each(response.data, function(index, array) {                        
                            $(select_sub_cat).append(option.replace('[VALUE]', array['idCategory'],'g').replace('[LABEL]',array['superCategoryName'], 'g').replace('[CODE]',array['codigo'], 'g'));
                        });
                    }else {
                        $('#idSubCategoryDIV').hide();
                        msgBox('<?php echo __("No se asignado una Sub Categoria"); ?>','Error');
                    }
                }
            });
            }            
        });
        $("#idBrand").combobox({
            selected:function(ui,event){
                $("#idBrand").trigger('change');
            }
        });
        $("#idStore").combobox({
            selected:function(ui,event){
                $("#idStore").trigger('change');
            }
        });
        $("#idProduct,#idBrand,#idStore,#idCategory,#idSubCategory").on('change',function(){
            grid_list_stock.trigger("reloadGrid")
        });
        $("#btn_search").button({
            text:true,
            icons:{
                primary:'ui-icon-search'
            }
        });
        $("#btn_search").on('click',function(e){
            e.preventDefault();
            grid_list_stock.trigger("reloadGrid")
        })
    });    
</script>

<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all" >
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">
            <h4><?php echo __('LISTA DE PRODUCTOS POR ALMACEN'); ?></h4>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div>
    <div>
        <div>            
            <form id="frm_search">                
                <fieldset>
                    <legend><?php echo __("Búsqueda") ?></legend>                    
                    <div>
                        <div> 
                            <label><?php echo __("Producto") ?>: </label>
                            <select id="idProduct">
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                                <?php foreach ($array_product as $va): ?>                                   
                                    <option value="<?php echo $va->idProduct ?>"><?php echo $va->productName . '------>' . $va->aliasProduct ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                            
                        <div> 
                            <label><?php echo __("Marca") ?>: </label>
                            <select id="idBrand">
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                                <?php foreach ($array_brand as $va): ?>                                   
                                    <option value="<?php echo $va->idBrand ?>"><?php echo $va->brandName ?></option>
                                <?php endforeach; ?>
                            </select>                            
                        </div>                            
                    </div>
                    <div>
                        <div> 
                            <label><?php echo __("Almacén") ?>: </label>
                            <select id="idStore">
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                                <?php foreach ($array_store as $va): ?>                                   
                                    <option value="<?php echo $va->idStore ?>"><?php echo $va->storeShortName ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>   
                        <div> 
                            <label><?php echo __("Categoría") ?>: </label>
                            <select id="idCategory">
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                        <?php
                        foreach ($category as $v) {
                            ?>
                            <option data-codigo="<?php echo $v['codigo'] ?>" value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                            <?php
                        }
                        ?>
                            </select>
                        </div>   
                        <div id="idSubCategoryDIV" class="none">
                            <label><?php echo __('Sub Categorias') ?>:</label>
                            <select id="idSubCategory" id="idSubCategory">
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                            </select>
                        </div>
                    </div>
                    <div><button id="btn_search"><?php echo __("Buscar"); ?></button></div>
                    <div class="clear"></div>                                        
                </fieldset>                
            </form>            
        </div>
        <div>
            <table id="grid_list_stock"> </table>
            <div id="grid_list_stock_pager"></div>
        </div>
    </div>
</div>

<div id="edit_productdetail" class="dialog-modal">
    <form id="frm_edit_productdetail" method="post">
        <fieldset  class="ui-corner-all cFieldSet" > 
            <legend class="frmSubtitle"><?php echo "Editar" ?></legend>
            <input type="text" id="idProduct" name="idProduct" hidden="true"/>
            <input type="text" id="idProductDetail" name="idProductDetail" hidden="true"/>
            <input type="text" id="detail" name="detail" hidden="true" />
            <div>
                <label><?php echo __('Serie Electronica') ?>:</label>
                <input type="text" id="serie_electronic" name="serie_electronic" class="input_label"/>
            </div>
            <div>
                <label><?php echo __('Precio Unitario') ?>:</label>
                <input type="text" id="unitPrice" name="unitPrice" class="input_label" />
            </div>
            <div>
                <label><?php echo __('Cantidad') ?>:</label>
                <input type="text" id="amount" name="amount" class="input_label" readonly="true"/>
            </div>
            <div>
                <label><?php echo __('Descripción') ?>:</label>
                <input type="text" id="description" name="description" class="input_label"/>
            </div>
            <div>
                <label><?php echo __('Fecha Registro') ?>:</label>
                <input type="text" id="creationDate" name="creationDate" class="input_label" readonly="true"/>
            </div>
        </fieldset>
    </form>
</div>
