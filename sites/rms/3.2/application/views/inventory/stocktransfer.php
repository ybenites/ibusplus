<style type="text/css"> 
    .clear{
        clear: both;
    }
    .divDocInfo{float:right;}
    input.helptext{font-style: italic;color: #CCCCCC;}
    div.divDocInfo{font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}
    #transfer_content{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
        padding: 20px;
        margin-top: 10px;
        margin-bottom: 30px;
 
        
    }
    #btn_buttons{
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
        padding-top: 15px;
    }
    #div_less_product{
        float: left;
        margin-right: 15px;
        padding: 10px;
        width: 400px;   
        border-style: groove;        
        border-width: 5px;
    }
    #check_type_search_product{        
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    #div_input_search_product input{
        height: 18px;
    }
    #div_input_search_product{
        margin-right: auto;
        margin-left: auto;
        width:250px;
    }
    #div_product_find{
        padding: 7px;
    }
    #div_product_find input{
        height:19px;
    }
    #div_product_find>div{
        padding: 5px;
    }
    #div_product_find>div label{
        font-weight: bolder;
        float: left;
        min-width: 70px;
        padding-top: 5px;
        text-align:right;
        padding-right: 3px;
    }
    #field_product{
        padding: 10px;
    }
    #field_product>div{
        padding:5px;
    }
    .two_colum{
        float: left;
    }
    #div_grid_stocktransfer{
        float: left;
    }
    .cFieldSet{        
        border-bottom-style: dashed;
        border-left: none;
        border-right: none;
        border-top: none;
        border-width: 3px;
        padding-bottom: 25px;
    }
    .cFieldSet>div>div{
        padding: 5px;
    }
 
    .cFieldSet>div>div>label{        
        float: left;
        font-weight: bolder;
        margin-top: 7px;
        padding-right: 10px;
        text-align: right;
        width: 140px;
    }
    
    .cFieldSet>div input[type="text"], input[type="password"], textarea, select {
        padding: 5px; 
        width: 180px;
    }
    #div_product_grid{        
        padding-top: 15px;
        margin-left: auto;
        margin-right: auto;
        max-width: -moz-fit-content;
    }
    .doc_ref{
        float: left;
        padding-left:50px;
    }
    #input_detail>div{
        padding-top:10px;
    }
    #grid_uno{
        float: left;
        padding-right:20px;
    }
    #grid_dos{
        float: left;
    }
    .ui-dialog-titlebar-close{
        display: none;
    }
    .lot_qua{float: left;margin: 7px;padding: 7px;box-shadow: 3px 3px; width: 13em}
    .frm_class{display: inline-table;    font-weight: bolder;    padding-right: 5px;    text-align: right;    width: 60px;}
    
</style>
<script type="text/javascript">
    var url_getDataSearchAutocompleteProduct="/private/stockoutcome/getDataSearchAutocompleteProduct"; 
    var url_getProductById="/private/stocktransfer/getProductById"; 
    var url_getAllStore="/private/stocktransfer/getAllStore"; 
    var url_getDateilByProductId="/private/stockoutcome/getDateilByProductId";  
    var getProductLotById="/private/stocktransfer/getProductLotById";  
    var url_createTransferStock="/private/stocktransfer/createTransferStock"; 
    var array_detail_id_p=[];
    var array_detail_id_pl=[];
    var message_register="<?php echo 'Usted Desea Realizar El Traslado?' ; ?>";
    var title_register= "<?php echo 'Registrar'; ?>";
    var type_document='<?php echo $type_document; ?>';
    var detail_form_Title = '<?php echo __("Detalle de Otros Productos"); ?>';
    var txtEdit = '<?php echo __('Editar'); ?>';
    var msg_warn_delete="<?php echo __('Desea Eliminar Este Producto de la Lista'); ?>";
    var title_delete="<?php echo __('Eliminar Producto') ?>";
    var grid_list_stock = '';
   
    $(document).ready(function(){
        
        $("#input_date_transfer").datepicker({
            dateFormat:'<?php echo $jquery_date_format ?>'
        });
        $("#btn_register").button({
            icons: {
                primary: "ui-icon-check"                
            },
            text:true
        });  
        $("#btn_register").on('click',function(e){
            e.preventDefault();
            if(!$("#frm_content").valid())
                return false;
            var arrayDataGrid=$("#grid_list_stocktransfer").jqGrid("getRowData");
            
            console.log("Tamanio de filas del jqgrid: "+(arrayDataGrid.length));
            
            if(arrayDataGrid.length>0){
                confirmBox(message_register,title_register,function(e){
                    if(e){                        
                        var obj_frm_content=$("#frm_content").serializeObject();
                        obj_frm_content.idStore=$("#select_store").val();
                        showLoading=1;
                        $.ajax({
                            url:url_createTransferStock+jquery_params,                        
                            type:'POST',
                            dataType:'json',
                            data:{
                                obj_frm:obj_frm_content,
                                array_grid:arrayDataGrid,
                                array_grid_detail:array_detail_id_p,
                                array_grid_detail_l:array_detail_id_pl
                            }, 
                            async:false,
                            success:function(response){                    
                                if(evalResponse(response)){
                                 //   array_detail_id_p=[];
                               //     $("#btn_cancel").trigger('click');       
                              Move = response.idMove;
                               //Mandar imprimir 
                                 confirmBox  ('<?php echo __("Desea Imprimir Documentos?"); ?>',
                                 'Confirmar Impresión',function(e){
                                        if(e){
                                            var my_pop_up = window.open('/private/stocktransfer/getPrintGuiaRemision?mov_ts='+Move.idmov_ts+'&mov_ti='+Move.idmov_ti, '<?php echo __("Impresión de Documento de Envío") ?>');
                                        }else{                                                
                                        }
                                        window.location.reload();
                            });
                                          //          window.location.reload();
                            }
                            }
                        });
                    }
                });
            }else{
                msgBox('Usted no Tiene Productos a tranferir','Alerta');
            }
            return false;
        });
        $("#btn_clear_product").on('click',function(e){
            e.preventDefault();
            frm_product.currentForm.reset();
            frm_product.resetForm();
            $("#input_hidden_stock").val(0);            
            $("#input_search_product").helptext('refresh');
        });
        $("#btn_cancel").on('click',function(e){
            e.preventDefault();                         
            frm_product.currentForm.reset();
            frm_product.resetForm();
            frm_content.currentForm.reset();
            frm_content.resetForm();                                   
            $("#input_hidden_stock").val(0);           
            array_detail_id_p=[];
            array_detail_id_pl=[];
            $("#grid_list_stocktransfer").jqGrid("clearGridData");
            $("#input_search_product").helptext('refresh');
        });
        $("#btn_less_product").on('click',function(e){
            e.preventDefault();
            if(!$("#frm_product").valid())
                return false;
            var id=$("#input_hidden_id_product").val();  
            console.log("Este es el id "+id);
            var trash = "<a class=\"trash\" style=\"cursor: pointer;\" data-id=\""+id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";                                        
            var options=trash;      
            //p es un arreglo donde se almacena , cuando doy click en eliminar ahi debe ser delete
            var p={};   
            p.id=id;            
            p.codigo=$("#input_hidden_code_product").val();
            p.haveDetail=$("#input_hidden_have_detail").val();
            p.producto=$("#input_product").val();            
            p.amount=parseInt($("#input_amount").val(),10);            
            p.actions=options;
            var arrayDataGrid=$("#grid_list_stocktransfer").jqGrid("getRowData");
            if(arrayDataGrid.length>0){ 
                var j=0;
                $.each(arrayDataGrid,function(index,data){                                                      
                    if(data.codigo==p.codigo){
                        j=j+1;                        
                    }
                });
                if(j>0){
                    alert('Ya existe el Producto Agregrado.');
                    return false;
                }                    
            }
            if(p.haveDetail==true){
                var idPG=$("#input_hidden_id_product").val();
                var idStore=$("#select_store").val();
                fn_get_detail_product(idPG,idStore);
                return false;
            }else{
                var idPG=$("#input_hidden_id_product").val();
                var idStore=$("#select_store").val();
                fn_get_lot_product(idPG,idStore);
                //retornamos falso para cortar el proceso ya q vamos a hacer por lotes
                return false;
//                $("#grid_list_stocktransfer").jqGrid('addRowData',id,p);                 
//                grid_list_stocktransfer.trigger("reloadGrid"); 
//                frm_product.currentForm.reset();
//                frm_product.resetForm();
//                $("#input_hidden_stock").val(0);
//                $("#input_search_product").helptext('refresh');                
            }
        });
        
        fnLoadCheck();
        
        //FUNCION LOTES
        function fn_get_lot_product(idpg,idstore){
            showLoading=1;
            $.ajax({
                url:getProductLotById+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{idpg:idpg,idstore:idstore}, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){    
                        // Lotes  
                        var r=response.data;                        
                        var template='';                                                        
                        if(r.length>0){                                                        
                            var product="<div><label class='frm_class' style='margin-left: 7px;text-align: left;width: 110px;'><?php echo __('Lotes del Producto'); ?>:</label><span>"+r[0].productName+"</span></div>";    
                            var template='';
                            $.each(r,function(i,d){ 
                                var lot="<div><label class='frm_class'><?php echo __('Lote') ?>: </label><span>"+d.lotName+"</span></div>";                                
                                var amount_total="<div><label class='frm_class'><?php echo __('Stock') ?>:</label><span>"+d.amount+"</span></div>"
                                var amount_discount="<div><label class='frm_class'><?php echo __('Cantidad') ?>:</label><input type='text' data-lotname='"+d.lotName+"' data-id='"+d.idLot+"' data-amt='"+d.amount+"' size='5' class='txt_amount_ds ui-corner-all widget-content' /></div>"
                                template=template+"<div class='lot_qua ui-corner-all ui-widget-content'>"+lot+amount_total+amount_discount+"</div>";                                                                   
                            });
                            template=template+"<div class='clear'></div>";
                            template=product+'<div>'+template+"</div>";
                            $("#show_data").html(template);
                            $(".txt_amount_ds").setMask();
                             $("#input_DetailOther").dialog('open');
                        }else{
                        msgBox('<?php echo __("No hay Productos") ?>','<?php echo __("Alerta") ?>')
                        }
                    }
                }
        });
        }
      
      // DIALOG DE LOTES
      // DIALOG DE PRODUCTO LOTES
           $('#input_DetailOther').dialog({
            title: detail_form_Title,
            autoOpen: false,
            closeOnEscape:false,
            width:600,
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: false, 
            buttons: {
                "<?php echo __("Guardar"); ?>":function(){
                    var txt=$(".txt_amount_ds");
                    var total=$("#input_amount").val();
                    var t=0;
                    var inc=0;                    
                    var array_temp=[];
                    $.each(txt,function(i,d){
                        var prl={};                        
                        var val_lim=$(d).data('amt');
                        var val=($(d).val()=='')?0:$(d).val();
                        t=t+parseInt(val);                         
                        if(val_lim<parseInt(val)){
                            inc=inc+1;
                        }                                             
                        prl.idLot=$(d).data('id');
                        prl.lotName=$(d).data('lotname');                        
                        prl.amount=val;
                        if(val!=0)
                            array_temp.push(prl);                    
                    });  
                    if(total!=t || isNaN(t) || inc>0){
                        $(".txt_amount_ds").addClass('ui-state-error');
                        //Mensaje de error
                        msgBox('<?php echo __("Cantidad incorrecta") ?>','<?php echo __("Alerta") ?>');
                        return false;
                    }else{
                        $(".txt_amount_ds").removeClass('ui-state-error');
                        var data=array_temp;                        
                        var id=$("#input_hidden_id_product").val();  
                       var p={}; 
                       var count = 0;
                        $.each(data,function(i,d){       
                            // Meto al arreglo todos los lotes que sacaré
                            array_detail_id_pl.push(d);
                            count = count + parseInt(d.amount,10);
                        
                        })
                        p.id=id;
                        p.codigo=$("#input_hidden_code_product").val();
                        p.producto=$("#input_product").val();
                        p.haveDetail=$("#input_hidden_have_detail").val();   
                        p.amount = count;
                        //agrega el producto al jqgrid
                        $("#grid_list_stocktransfer").jqGrid('addRowData',id,p);
                        
                        frm_product.currentForm.reset();
                        frm_product.resetForm();
                        $("#input_hidden_stock").val(0);
                        $("#input_search_product").helptext('refresh');                               
                        $("#input_DetailOther").dialog('close');                          
                        grid_list_stocktransfer.trigger("reloadGrid");                         
                    }
                },
                "<?php echo __("Cancelar"); ?>":function(){
                    $(this).dialog('close');                                      
                    $("#input_hidden_id_product").val('');                    
                    frm_product.currentForm.reset();
                    frm_product.resetForm();
                }
            }
        });
        function fn_get_detail_product(idPG,idStore){
            showLoading=1;
            $.ajax({
                url:url_getDateilByProductId+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{idpg:idPG,idstore:idStore}, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        r=response.data;                        
                        $.each(r,function(index,data){
                            var pd={};                            
                            pd.idGeneral=data.idProductDetail;
                            pd.description=data.description;
                            pd.serie_electronica=data.serie_electronic;                            
                            $("#grid_income_detail").jqGrid('addRowData',data.idProductDetail,pd); 
                        });
                        grid_list_stocktransfer.trigger("reloadGrid"); 
                        $("#input_detail").dialog('open');
                    }
                }
            });
        }
        $("#input_detail").dialog({
            title:'<?php echo "Ingresar Detalle Producto"; ?>',
            autoOpen: false,
            width: 855,
            height:'auto',
            show: "slide",
            hide: "slide",
            modal:true, 
            resizable: false, 
            buttons:{
                '<?php echo "Aceptar" ?>':function(){                                          
                    var data=$("#grid_income_detail2").jqGrid('getGridParam','data');
                    if(data.length!=0 && data.length==$("#input_amount").val()){
                        $.each(data,function(i,d){
                            array_detail_id_p.push(d.id);
                        });                        
                        var id=$("#input_hidden_id_product").val();                                 
                        if(!$("#for_sell").is(":checked")){ 
                            var p={};   
                            p.id=id;
                            p.codigo=$("#input_hidden_code_product").val();
                            p.haveDetail=$("#input_hidden_have_detail").val();                            
                            p.producto=$("#input_product").val();                            
                            p.amount=parseInt($("#input_amount").val(),10);                            
                            $("#grid_list_stocktransfer").jqGrid('addRowData',id,p);
                            grid_list_stocktransfer.trigger("reloadGrid"); 
                        }else{
                            $(".add_detail[data-id="+id+"]").hide();
                        }                                                                        
                        
                        frm_product.currentForm.reset();
                        frm_product.resetForm();
                        $("#input_hidden_stock").val(0);
                        $("#input_search_product").helptext('refresh');

                        $("#input_detail").dialog('close');                                    
                        $("#grid_income_detail").jqGrid("clearGridData");
                        $("#grid_income_detail2").jqGrid("clearGridData");
                    }else{                        
                        alert('Aun falta ingresar datos');
                    }
                },
                '<?php echo "Cancelar" ?>':function(){
                    $(this).dialog('close');
                    $("#grid_income_detail").jqGrid("clearGridData");                    
                    $("#grid_income_detail2").jqGrid("clearGridData");                                         
                    $("#input_hidden_id_product").val('');                    
                    frm_product.currentForm.reset();
                    frm_product.resetForm();
                }
            } 
        });
        var grid_income_detail=$("#grid_income_detail").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Serie Electrónica") ?>',                                                        
                '<?php echo __("Descripción") ?>',
            ],
            colModel:[
                {name:'idGeneral',index:'idGeneral',hidden:true,width:10},
                {name:'serie_electronica',index:'serie_electronica',align:'center'},       
                {name:'description',index:'description',align:'center'},
            ],
            pager:'#grid_income_detail_pager',              
            rowNum:10, 
            rowList:[10,20,50,100],
            sortorder: "asc",            
            width:400,
            height:250,                        
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            multiselect: true,
            caption:'<?php echo __("Lista Detalles de Productos")?>',
            ondblClickRow:function(rowid,iRow,iCol,e){                       
                var array_grid_income_detail2=$("#grid_income_detail2").jqGrid("getRowData");
                if(array_grid_income_detail2.length<$("#input_amount").val()){                    
                    var a=$("#grid_income_detail").getRowData(rowid);                    
                    $("#grid_income_detail2").jqGrid('addRowData',rowid,a);                     
                    $("#grid_income_detail").jqGrid('delRowData',rowid);                    
                    grid_income_detail[0].clearToolbar();
                    grid_income_detail.trigger("reloadGrid");                                                                           
                    grid_income_detail2.trigger("reloadGrid"); 
                }else{
                    alert('Ya Completo la salida');
                }
            },
            gridComplete:function(){                     
            }
        });
        jQuery("#grid_income_detail").jqGrid('navGrid','#grid_income_detail_pager',{del:false,add:false,edit:false,search:false}); 
        jQuery("#grid_income_detail").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        jQuery("#grid_income_detail").jqGrid('navButtonAdd','#grid_income_detail_pager',{            
            caption:"", 
            buttonicon:"ui-icon-plusthick", 
            onClickButton: function(){
                 var recs1 = $("#grid_income_detail").getGridParam("records");
                 var recs2 = $("#grid_income_detail2").getGridParam("records");
                 if(recs1>0){
                     var val_grid_sel1=grid_income_detail.getGridParam('selarrrow');
                     var val_grid_sel_tot1=val_grid_sel1.length+recs2;
                     if(val_grid_sel1.length>0 && val_grid_sel_tot1<=$("#input_amount").val()){
                        var len = val_grid_sel1.length;
                        for(var i=len-1;i>=0;i--){
                            var a=$("#grid_income_detail").getRowData(val_grid_sel1[i]);
                             $("#grid_income_detail2").jqGrid('addRowData',val_grid_sel1[i],a); 
                            $("#grid_income_detail").delRowData(val_grid_sel1[i]);
                        }
                        
                         grid_income_detail[0].clearToolbar();
                         grid_income_detail.trigger("reloadGrid");                                                                           
                         grid_income_detail2.trigger("reloadGrid");   
                     }else{
                         alert('No hay Productos Seleccionados o Ya Completo la Salida');
                     }
                 }else{
                     alert('No hay mas Productos en Almacén');
                 }                 
            },
            title:'Quitar en Grupo',
            position:"last"
        });
        var grid_income_detail2=$("#grid_income_detail2").jqGrid({
            datatype: "local",
            mtype: 'POST',            
            colNames:[
                '<?php echo __("Código General") ?>',
                '<?php echo __("Serie Electrónica") ?>',                                                        
                '<?php echo __("Descripción") ?>',
            ],
            colModel:[
                {name:'idGeneral',index:'idGeneral',hidden:true,width:10},
                {name:'serie_electronica',index:'serie_electronica',align:'center'},       
                {name:'description',index:'description',align:'center'},
            ],
            pager:'#grid_income_detail_pager2',              
            rowNum:10, 
            rowList:[10,20,50,100],
            sortorder: "asc",            
            width:400,
            height:250,                        
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            multiselect: true,
            caption:'<?php echo __("Productos Seleccionados")?>',
            ondblClickRow:function(rowid,iRow,iCol,e){ 
                var a=$("#grid_income_detail2").getRowData(rowid);                    
                $("#grid_income_detail").jqGrid('addRowData',rowid,a); 
                $("#grid_income_detail2").jqGrid('delRowData',rowid);
                grid_income_detail2[0].clearToolbar();
                grid_income_detail.trigger("reloadGrid");                                                                           
                grid_income_detail2.trigger("reloadGrid");                
            },
            gridComplete:function(){                     
            }
        });  
        jQuery("#grid_income_detail2").jqGrid('navGrid','#grid_income_detail_pager2',{del:false,add:false,edit:false,search:false}); 
        jQuery("#grid_income_detail2").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false});
        jQuery("#grid_income_detail2").jqGrid('navButtonAdd','#grid_income_detail_pager2',{            
            caption:"", 
            buttonicon:"ui-icon-minusthick", 
            onClickButton: function(){
                var recs1 = $("#grid_income_detail").getGridParam("records");
                 var recs2 = $("#grid_income_detail2").getGridParam("records");
                 if(recs1>0){
                     var val_grid_sel2=grid_income_detail2.getGridParam('selarrrow');
                     
                     if(val_grid_sel2.length>0){
                        var len = val_grid_sel2.length;
                        for(var i=len-1;i>=0;i--){
                            var a=$("#grid_income_detail2").getRowData(val_grid_sel2[i]);
                             $("#grid_income_detail").jqGrid('addRowData',val_grid_sel2[i],a); 
                            $("#grid_income_detail2").delRowData(val_grid_sel2[i]);
                        }                        
                         grid_income_detail[0].clearToolbar();
                         grid_income_detail.trigger("reloadGrid");                                                                           
                         grid_income_detail2.trigger("reloadGrid");   
                     }else{
                         alert('No hay Productos Seleccionados');
                     }
                 }else{
                     alert('No hay mas Productos en Almacen');
                 }
            },
            title:'Quitar en Grupo',
            position:"last"
        });
        $.validator.addMethod("validStockAvible", function(value, element, params) {              
            return  parseFloat(value)< 0?false:true; 
        }, "<?php echo __('El Stock Es Menor que 0') ?>");
        $.validator.addMethod("validPrice", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        $.validator.addMethod("validAmount", function(value, element, params) {            
            return value <= 0?false:true; 
        }, "<?php echo __('El Precio Es Menor que 0') ?>");
        var frm_product=$("#frm_product").validate({
             rules:{
                input_product:{
                    required:true
                },
                input_measure:{
                    required:true
                },
                input_amount:{
                    required:true,
                    validAmount:true,
                    max:function(){
                                var max = $("#input_stock").val();
                                return parseInt(max);                        
                    }
                },                                
                input_stock:{
                    required:true,
                    validStockAvible:true
                }                                                              
            },
            messages:{
                input_product:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_measure:{
                    required:'<?php echo __("Seleccione un Producto") ?>'
                },
                input_amount:{
                    required:'<?php echo __("Ingrese La Cantidad") ?>',
                    validAmount:'<?php echo __("Ingrese La Cantidad Mayor que Cero") ?>',
                    max:'<?php echo __("La cantidad no puede ser mayor al stock") ?>'
                },                                
                input_stock:{
                    required:'<?php echo __("No puede ser vacio el stock") ?>',
                    validStockAvible:'<?php echo __("El stock no puede ser negativo") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        $("#input_amount").setMask();                           
        
      
//        $.validator.addMethod("validAsignStore", function(value, element, params) {
//            return value=="ASIGNESE UN ALMACEN"?false:true; 
//        }, "<?php echo __('Asignese Un Almacen') ?>");


        var frm_content=$("#frm_content").validate({
            rules:{
                input_date_outcome:{
                    required:true                    
                },
                select_store:{
                    required:true
                //    validAsignStore:true
                },
                store_send:{
                    required:true
                },        
                'store_send-combobox':{
                    required:true                    
                },                                                                                
                text_doc_ref:{
                    required:true                    
                }                 
            },
            messages:{
                input_date_outcome:{
                    required:'<?php echo __("Ingrese Una Fecha") ?>'
                },
                select_store:{
                    required:'<?php echo __("Seleccione almacén origen") ?>'
               
                },
                store_send:{
                    required:'<?php echo __("Seleccione un almacén de destino") ?>'
                },
                'store_send-combobox':{
                    required:'<?php echo __("Seleccione un Almacen") ?>'
                },                                
                text_doc_ref:{
                    required:'<?php echo __("Ingrese Un Documento de Referencia") ?>'                  
                } 
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){               
            }
        });
        $("#btn_cancel").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        }); 
        $("#check_type_search_product").buttonset();
        $("#btn_less_product").button({
            icons: {
                primary: "ui-icon-minusthick"                
            },
            text:true
        });        
        $("#btn_clear_product").button({
            icons: {
                primary: "ui-icon-newwin"                
            },
            text:true
        });
        
        
            //GRID DE lista de PRODUCTOS
        var grid_list_stocktransfer=$("#grid_list_stocktransfer").jqGrid({            
            datatype: "local",
            mtype: 'POST',            
            colNames:[ 
                '<?php echo __("id") ?>',
                '<?php echo __("Tiene Detalle") ?>',
                '<?php echo __("Código") ?>',
                '<?php echo __("Producto") ?>',                
                '<?php echo __("Grupo") ?>',                               
                '<?php echo __("Cantidad") ?>',                                                
                '<?php echo __("Acciones") ?>'                
            ],
            colModel:[
                {name:'id',index:'id',hidden:true,width:50},
                {name:'haveDetail',index:'haveDetail',hidden:true,width:50},
                {name:'codigo',index:'codigo',align:'center'},
                {name:'producto',index:'producto',align:'center',search:true},          
                {name:'lotName',index:'lotName',align:'center',search:true}, 
                {name:'amount',index:'amount',align: 'right',sorttype:'number', summaryType:'sum',summaryTpl : '{0}'},                
                {name:'actions', index: 'actions',align:'center',width:80,search:false}
            ],
            pager:'#grid_list_stocktransfer_pager', 
//            sortname: 'st.idStore',         
            gridview: false,
            rowNum:10,   
            sortorder: "asc",            
            width:750,
            height:220,
            shrinkToFit:true,
            rownumWidth: 10,
            viewrecords: true, 
            sortable: true,
            rownumbers: true,   
            caption:'<?php echo __("Lista De Productos")?>',
            grouping:true,
            groupingView : { 
                groupField : ['producto'],
                groupColumnShow: [false],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,                    
                groupSummary : [true], 
                showSummaryOnHide: true,
                groupDataSorted : true
            }, //tngo q enviar datos del controlador 
            gridComplete:function(){
                var id =  $("#grid_list_stocktransfer").jqGrid('getDataIDs');
                $.each(id,function(idx,value){
//                    var edit = $(document.createElement('a'));
//                    edit
//                        .addClass("edit")
//                        .attr("title",txtEdit)
//                        .text(txtEdit);
//                    $.data(edit.get(0),'oid',value);
                    trash = "<a class=\"trash\" data-oid=\""+value+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    //console.log(edit.html());
                    //console.log(edit.get(0));
                    $("#grid_list_stocktransfer").jqGrid('setCell',value,'actions',trash);  
                });
                $('.trash').button({
                    icons:{primary: 'ui-icon-power'},
                    text: false
                })
                
            }
        });
        
        $("#grid_list_stocktransfer").jqGrid('navGrid',"#grid_list_stocktransfer_pager",{edit:false,add:false,del:false}); 
        $("#grid_list_stocktransfer").jqGrid('inlineNav',"#grid_list_stocktransfer_pager"); 
        
        $('table#grid_list_stocktransfer').on('click','.trash',function(){
        
        var p=$("#grid_list_stocktransfer").jqGrid('getRowData',$(this).data('oid'));                
            confirmBox(msg_warn_delete,title_delete,function(response){
                if(response){ 
                    $("#grid_list_stocktransfer").jqGrid('delRowData',p.id);
                    delete[p.id];
                    // prueba
                    array_detail_id_pl.pop(p.id);
                    console.log(array_detail_id_pl);
                    
                    $("#grid_list_stocktransfer").trigger("reloadGrid");
                    
                    
                    
                }
            });
            return false
        
            
        });
        
        $('#input_search_product').helptext({value:'Búsqueda de Productos'});
        $("#input_search_product").autocomplete({
            minLength:1,            
            autoFocus: true,
            source:function(req,response){               
                $.ajax({
                    url:url_getDataSearchAutocompleteProduct+jquery_params,
                    dataType:'jsonp',
                    data: {
                        term : req.term,
                        typeSearch:$("input[name=check_type_search_product]:checked").val()
                    },
                    success:function(resp){                        
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {                                                                                                
                                var text = "<span class='spanSearchLeft'>" + item.dataMostrar+ "</span>";                         
                                text += "<span class='spanSearchRight'>" + item.otraData + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:item.dataMostrar,
                                    id:item.id,
                                    code:item.otraData
                                }
                            }));
                        }
                    }
                });
            },
            select: function( event, ui ) {  
                var array=$("#grid_list_stocktransfer").jqGrid('getRowData');
                $.each(array, function(index,data){                    
                    if(parseInt(data.id)==ui.item.id){               
                        return false;
                    }
                });
                $('#input_search_product').val(ui.item.value);                 
                $('#input_search_product').helptext('refresh'); 
                edit_product_outcome=0;
                getProductById(ui.item.id); 
                return false;
            },
            change: function( event, ui ) {                                       
                if ( !ui.item ) {
                    $('#input_search_product').val('');  
                    $( this ).helptext('refresh');                                                        
                }  
                return true;                
            }
        })
        .bind('blur', function(){             
            if($(this).data('autocomplete').selectedItem == null && $(this).val() == ''
                || $($(this).data('autocomplete').element).val() != $(this).data('autocomplete').term)
            {
                $('#input_search_product').val('');
                $( this ).helptext('refresh');                
            }
        })
        .data( "autocomplete" )._renderItem = function( ul, item ) {               
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        function getProductById(id){            
            showLoading=1;
            var idStore=$("#select_store").val();
            $.ajax({                
                url:url_getProductById+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{id:id,idStore:idStore}, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        var r=response.data;
                        var productStock=response.amount;
                        $("#input_product").val(r.productName);
                        $("#input_measure").val(r.productUnitMeasures);
                        $("#input_stock").val(parseInt(productStock)-parseInt($("#input_amount").val()));
                        $("#input_hidden_stock").val(productStock);
//                        var arrayDataGrid=$("#grid_list_stocktransfer").jqGrid("getRowData");
//                        var stk_hidden=0;
//                        if(arrayDataGrid.length>0){
//                            $.each(arrayDataGrid,function(index,data){                                   
//                                if(data.codigo==r.aliasProduct){                                    
//                                    stk_hidden=parseInt(stk_hidden)+parseInt(data.amount);                                     
//                                    $("#input_hidden_stock").val(parseInt(productStock)-parseInt(stk_hidden));                                    
//                                    $("#input_stock").val(parseInt(productStock)+parseInt(stk_hidden)-parseInt($("#input_amount").val()));
//                                }
//                            });
//                        }                                                
                        $("#input_hidden_id_product").val(r.idProduct);
                        $("#input_hidden_code_product").val(r.aliasProduct);
                        $("#input_hidden_have_detail").val(r.haveDetail);                        
                        $("#input_amount").focus();
                    }
                }
            });
        }
        //Adquirimos todos los almacenes 
        
        //getAllStore();
        $("#select_store").combobox({
                selected:function(ui, event){
                $("#select_store").trigger('change');
                getAllStore();
                }
        });
        $("#store_send").combobox({
            selected:function(ui,event){
                $("#store_send").trigger('change');
              //  getAllStore();
                
            }
        });
        
        function getAllStore(){
            showLoading=1;
            var idStore=$("#select_store").val();
            $("#store_send").val("");
            $.ajax({
                url:url_getAllStore+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{idStore:idStore}, 
                async:false,
                success:function(response){ 
                    if(evalResponse(response)){
                        data=response.data;
                        var template="<option value=''><?php echo 'Seleccione una opcion...'; ?></option>"
                        $.each(data,function(i,d){
                            template=template+"<option value="+d.idStore+">"+d.storeShortName+"</option>"
                        });                        
                        $("#store_send").html(template);
                    }
                } 
            }); 
        }
        <?php  if ($bAvailableDocumentType): ?>
            successDocument = false;                
            fnUpdateCorrelativePackage = function(sType){
                try{                
                    oDocumentType = getCurrentCorrelative(type_document);                                                                                                                                                                                                                                                                       
                    if(oDocumentType.serie != null)
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.serie + '-' +oDocumentType.correlative;
                    else
                        displayDocument = oDocumentType.documentType.display + ' ' +oDocumentType.correlative;
                                                                                                                                                                                                                                                                    
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-highlight')
                    .html(displayDocument);
                    successDocument = true;
                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                    
                }catch(e){
                    successDocument = false;
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-error')
                    .html('<?php echo __("Documento no Asignado") ?>');
                }
            };            
            fnUpdateCorrelativePackage(type_document);  
        <?php else: ?>
        <?php endif ?> 
    });
</script>
<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;" <?php endif ?> >
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4><?php echo __('Transferencia De Productos De Almacén'); ?></h4>
        </div>
    </div>
    <?php if ($bAvailableDocumentType): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix">
        </div>
    <?php endif ?>
    <div class="clear"></div>
</div>
<div id="transfer_content" class="ui-widget-content ui-corner-all"> 
    <div>
        <form id="frm_content">
            <div class="cFieldSet ui-widget-content">
                <div class="doc_ref">
                    <div>
                        <label><?php echo __("Fecha"); ?>: </label>
                        <input type="text" id="input_date_transfer" name="input_date_transfer" value="<?php echo date($date_format_php) ?>" />
                    </div>
                    <div>
                        <label><?php echo __("Almacén Origen"); ?>: </label>
                        <!--<input id="select_store" name="select_store" value="" />-->
                            <select id="select_store" name="select_store" >
                                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                                <?php foreach ($store as $va): ?>                                   
                                    <option value="<?php echo $va['id'] ?>"><?php echo $va['display']  ?></option>
                                <?php endforeach; ?>  
                            </select>
                </div> 
                </div>            
                <div class="doc_ref">
                    <div>
                        <label><?php echo __("Almacén Destino"); ?>: </label>
                        <select id="store_send" name="store_send"></select>
                    </div>
                    <div>
                        <label><?php echo __("Doc. Referencia"); ?>: </label>
                        <input type="text" id="text_doc_ref" name="text_doc_ref" />
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </form>
    </div>
    <div id="div_product_grid">
        <div id="div_less_product" class="ui-corner-all ui-widget-content">
            <form id="frm_product">
                <div>                     
                    <div>
                        <fieldset id="field_product">
                            <legend><?php echo __("Búsqueda de Producto"); ?></legend>
                            <div id="check_type_search_product">
                                <input type="radio" id="search_product_code" name="check_type_search_product" checked="checked" value="search_code" /><label for="search_product_code"><?php echo __("Código"); ?></label>
                                <input type="radio" id="search_product_desc" name="check_type_search_product" value="search_description" /><label for="search_product_desc"><?php echo __("Modelo"); ?></label>                                                 
                            </div>
                            <div id="div_input_search_product">                                
                                <input type="text" id="input_search_product" name="input_search_product" size="40" />
                            </div>                            
                        </fieldset>
                    </div>
                    <div id="div_product_find">
                        <div>
                            <label><?php echo __("Producto"); ?>: </label>
                            <input type="text" id="input_product" name="input_product" size="38" readonly="" />
                            <input type="hidden" id="input_hidden_id_product" name="input_hidden_id_product"  />
                            <input type="hidden" id="input_hidden_code_product" name="input_hidden_code_product"  />
                            <input type="hidden" id="input_hidden_have_detail" name="input_hidden_have_detail"  />
                        </div>
                        <div>
                            <label><?php echo __("Medida"); ?>: </label>
                            <input type="text" id="input_measure" name="input_measure" size="23" readonly=""  />
                        </div>
                        <div>
                            <div class="two_colum" id="div_amount">
                                <label><?php echo __("Cantidad"); ?>: </label>
                                <input type="text" id="input_amount" name="input_amount" size="10" value="0" alt="number" autocomplete="off" />
                            </div>
                            <div class="two_colum" id="div_stock">
                                <label><?php echo __("Stock"); ?>: </label>
                                <input type="text" id="input_stock" name="input_stock" size="10" value="0" readonly="" />                                    
                                <input type="hidden" id="input_hidden_stock" name="input_hidden_stock" value="0" size="10" readonly="" />                                    
                            </div>
                            <div class="clear"></div>
                        </div>                        
                    </div>
                    <div id="buttons_add_product">
                        <button id="btn_less_product"><?php echo __("Asignar Producto"); ?></button>
                        <button id="btn_clear_product"><?php echo __("Limpiar"); ?></button>
                    </div>                    
                </div>
            </form>
        </div>        
        <div id="div_grid_stocktransfer">
            <table id="grid_list_stocktransfer"> </table>
            <div id="grid_list_stocktransfer_pager"></div>
        </div> 
        <div class="clear"></div>
    </div>
    <div id="btn_buttons">
        <button id="btn_register"><?php echo __("Registrar"); ?></button>
        <button id="btn_cancel"><?php echo __("Cancelar"); ?></button>        
    </div>
</div>
<div id="input_detail" class="dialog">    
    <div id="grid_uno">
        <table id="grid_income_detail"> </table>
        <div id="grid_income_detail_pager"></div>
    </div>
    <div id="grid_dos">
        <table id="grid_income_detail2"> </table>
        <div id="grid_income_detail_pager2"></div>
    </div>
    <div class="clear"></div>
</div>
<div id="input_DetailOther" class="dialog">
    <form id="frm_sale_data">
        <div id="show_data">
        </div>
    </form>
</div>

<div id="dialog-ListProduct">
    
</div>