<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<style type="text/css">
     .orderForm {
        width: 200px; text-align: right; font-size-adjust: 20px;
        list-style: none;
    }
    .divLine.divLineShort{width:310px;}
    .divLeft{float: left;}

    label.frmlblshort{float: left;font-weight: bold;width:120px;height: 42px;line-height: 42px;margin-top: 0px;font-size: 15px;text-align: right;padding-right: 5px;}
    #addPackageDetail{margin: 5px 0;}
    input.inputData{width: 270px;}
    input.subPrice{width: 200px; height: 19px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    input.subPriceLong{width: 200px; height: 19px; text-align: left;float: left;margin-top: 5px;margin-right: 5px;}
    .ui-widget input.subPrice {    float: left;
    font-size: 13px;
    margin-top: 5px;
    text-align: right;
    width: 35px;}
    input.inputCustomPrice{    float: left;
    height: 17px;
    margin-top: 8px;
    text-align: right;
    width: 35px;}
    
    #content_cc{
         
        margin: 10px auto 30px;
        padding: 20px;
        width: 1200px;
        text-align: center;    
    }
    #content_cc>dl>dd{
        margin-top: 10px;
        font-family: arial;
        font-size: 12px;
        font-weight: bolder;    
    }
    .division{
            border-style: none none dashed;
            margin-bottom: 24px;
            margin-top: 24px;
            border-width:  2px;
    }
      .ui-autocomplete {
        max-height: 120px;
        overflow-x: hidden;
        overflow-y: auto;
        padding-right: 20px;
        }
       input.helptext{font-style: italic;}
       .relative{position: relative;}
       input.serie
       { width: 50px;text-align: right;
           margin-left: 4px;
           margin-top: 2px;}
       input.purchaseDocuments, input.chars_supplier{
           margin-left: 4px;
           margin-top: 2px;
       }
       input.correlative{width: 85px;text-align: right;}
           .ui-widget-content .search_supplier_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    .ui-widget-content .search_supplier_wrapper{background-image: none;margin-right: 5px;}
    .ui-widget-content .search_product_wrapper{background-image: none;margin-right: 5px;}
    .search_supplier_wrapper, .search_supplier_wrapper span,
    .search_product_wrapper, .search_product_wrapper span,
    .search_supplier_wrapper input{float: left;}
    .search_product_wrapper input{float: left;}
    .ui-widget-content .search_product_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    
    .labelPurchase{
                    float: left;
                    font-family: arial;
                    font-size: 12px;
                    font-weight: bolder;
                    text-align: right;
                    width: 147px;
                   
    }
    .txtPurchase{
           margin-left: 4px;
    }
    #quantity , #totalAmount, #priceUnit , #amountTotal{
            width: 45px;
    }
    .cFieldSet>dl>dd{
        margin-top: 5px
    }
</style>
<script type="text/javascript">
    
    var url_listSales = '/private/supportsales/listSales'+jquery_params;
    var url_subsalesOrder = '/private/supportsales/getDetalleSalesOrder'+jquery_params;
    var url_editSalesDetail = '/private/supportsales/editSalesDetail'+jquery_params;  
    var url_deleteSalesDetail = '/private/salesmanagement/deleteSalesDetail'+jquery_params; 
    var url_getSalesDetailByID = '/private/salesmanagement/getSalesDetailByID'+jquery_params;
    var url_GetProductByCodigo = '/private/sales/getProductByCode'+jquery_params;
    var url_getLotByProductId = '/private/sales/getProductLotById'+jquery_params;  
    var url_UpdateSalesOrderById='/private/salesmanagement/createCancel'+jquery_params; 
    var deleteSubTitle = '<?php echo __("Eliminar Detalle de Venta") ?>';
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>';
    var conftrash = '<?php echo __("¿Está seguro que desea anular esta Venta?"); ?>';
    var titletrash = '<?php echo __("Anular"); ?>';
    
    // Variables
    var flags = {};
    var iCountLot = 0;
    flags.isRange = 0;
    flags.haveLot = 0;
    var countarray = 0;
    var keycountarray= 0;
    var quantity = 0;
    var edit_product = 0;
    var arrayDetails = new Object();
    
    $(document).ready(function(){
            var grid_list_ordersales=$("#grid_list_ordersales").jqGrid({
            url:url_listSales,            
            datatype: 'json',
            mtype: 'POST',
            postData:{
                    nroSales: function(){ return $("#comprobante_search").val()},
                    date: function(){return $("#accountsreceivable_date").val()},
                    dateini: function(){return $("#accountsreceivable_bdaterango").val()},
                    datefin: function(){return $("#accountsreceivable_fdaterango").val()},
                    month: function(){return $("#accountsreceivable_listMonth").val()},
                    year: function(){return $("#anio").val()}      
            },
            colNames:[
                '<?php echo __("ID") ?>',
                '<?php echo __("Serie") ?>',
                '<?php echo __("Correlativo") ?>',                         
                '<?php echo __("Cliente") ?>',
                '<?php echo __("Almacén") ?>',
                'DocumentP',
                '<?php echo __("Documento") ?>',
                '<?php echo __("IGV") ?>',                
                '<?php echo __("Precio Total") ?>',
                '<?php echo __("Monto de Pago") ?>',
                '<?php echo __("Monto devuelto") ?>',
                '<?php echo __("Fecha") ?>',
                '<?php echo __("Vendedor") ?>',
                '<?php echo __("Opciones") ?>'
                
            ],
            colModel:[
                {name:'idSales',index:'idSales',hidden:true,key:true},
                {name:'serie',index:'serie',width:100,align:"center"},
                {name:'correlative',index:'correlative',width:140,align:"center"},
                {name:'customers',index:'customers',width:280, align:"center"},
                {name:'storeName',index:'storeName',width:250, align:"center"},
                {name:'documentP', index:'documentP', hidden:true},
                {name:'document',index:'document',width:150, align:"center"},
                {name:'tax',index:'tax',width:120, align:"right"},
                {name:'totalPrice',index:'totalPrice',width:120, align:"right"},
                {name:'paymentAmount',index:'paymentAmount',width:120, align:"right"},
                {name:'returnCash',index:'returnCash',width:120, align:"right"},
                {name:'salesDate',index:'salesDate',width:250, align:"center"},
                {name:'seller',index:'seller',width:280, align:"center"},
                {name:'actions',index:'actions',width:180, align:"center"}                
            ],
            rowList: [10,20,50,100,200,400],
            pager:'grid_list_ordersales_pager', 
            sortname: 'correlative',
            gridview: true,             
            rowNum:10,
            sortorder: "desc",
            width:1200,
            height:300,
            shrinkToFit:true,
            rownumbers: true, 
            rownumWidth: 30,
            viewrecords: true, 
            //multiselect:true, 
            subGrid:true,
            // caption:"Support", 
            subGridOptions: { 
                plusicon : "ui-icon-plus", 
                minusicon : "ui-icon-minus", 
                openicon : "ui-icon-carat-1-sw"
            },
            subGridRowExpanded: function(subgrid_id, row_id) {                  
                var rd = $("#grid_list_ordersales").getRowData(row_id);                  
                var subgrid_ordersales, subgrid_ordersales_pager;
                subgrid_ordersales = subgrid_id+"_t";                
                subgrid_ordersales_pager = "p_"+subgrid_ordersales; 
                $("#"+subgrid_id).html("<table id='"+subgrid_ordersales+"' class='scroll'></table><div id='"+subgrid_ordersales_pager+"' class='scroll'></div>");                                
                jQuery("#"+subgrid_ordersales).jqGrid({ 
                    url:url_subsalesOrder, 
                    datatype: "jsonp",
                    mtype: 'POST',
                    postData:{
                        idSal:function(){
                            return row_id;
                        }
                    },                 
                    colNames: [
                        'idSalesOrderDetail',         
                        '<?php echo __("Cod. Producto") ?>',
                        '<?php echo __("Nombre Producto") ?>',
                        '<?php echo __("Lote") ?>',
                        '<?php echo __("Almacen") ?>',
                        '<?php echo __("Cantidad") ?>',
                        '<?php echo __("Precio Unit.") ?>',
                        '<?php echo __("Descuento") ?>',
                        '<?php echo __("Precio Total") ?>',
                        'Give',
                        '<?php echo __("Acciones") ?>'
                    ] ,                       
                    colModel: [ 
                        {name:"idSalesOrderDetail",index:"idSalesOrderDetail",hidden:true,width:80},
                        {name:"aliasProduct",index:"aliasProduct",width:80,align:"center"},
                        {name:"productName",index:"productName",width:150,align:"center"},
                        {name:"lote",index:"lote",width:120,align:"center"},
                        {name:"storeName",index:"storeName",width:120,align:"center"},
                        {name:"quantity",index:"quantity",width:80,align:"center"},
                        {name:"unitPrice",index:"unitPrice",width:80,align:"right"},
                        {name:"discount",index:"discount",width:80,align:"right"},
                        {name:"totalPrice",index:"totalPrice",width:80,align:"right"},                       
                        {name:"isGive",index:"isGive", hidden:true},
                        {name:"actions",index:"actions",width:100,align:"right"}
                    ],
                    rowNum:20, 
                    pager: subgrid_ordersales_pager, 
                    sortname: 'rs.idSales',
                    sortorder: "desc",
                    height: '100%',
//                    onSelectRow: function(rowid){ 
//                        if(rowid && rowid!= lastsel){
//                            $("#"+subgrid_ordersales).jqGrid('restoreRow',lastsel);
//                            $("#"+subgrid_ordersales).jqGrid('editRow',rowid,true); 
//                            lastsel=rowid;
//                        }
//                    },
     //               editurl:editSalesDetail,
                    rownumbers: true,
                    afterInsertRow: function(rowid, aData){
                        if(aData.isGive==0){
                            //                            $('#'+subgrid_ordersales +'tr#'+rowid).addClass('sales_not');
                            $(this).find('tr#'+rowid).addClass('sales_not');
                        }
                    },
                    gridComplete:function(){
                        var idSub = $("#"+subgrid_ordersales).jqGrid('getDataIDs');                       
                        for(var i=0;i<idSub.length;i++){
                            var optionSub = '';             
                            var rd = $("#"+subgrid_ordersales).getRowData(idSub[i]); 
                            editSub = "<a class=\"editSub\" style=\"cursor: pointer;\" rel=\""+rd.idSalesOrderDetail+"\" title=\"<?php echo __('Editar'); ?>\"><?php echo __('Editar'); ?></a>";                        
                            trashSub = "<a class=\"trashSub\" style=\"cursor: pointer;\" rel=\""+rd.idSalesOrderDetail+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                            optionSub =  editSub + trashSub;
                         
                            $("#"+subgrid_ordersales).jqGrid('setRowData',idSub[i],{actions: optionSub}); 
                        }
                        $('.editSub').button({
                            icons:{primary: 'ui-icon-pencil'},
                            text:false
                        });
                        $('.trashSub').button({
                            icons:{primary: 'ui-icon-trash'},
                            text:false
                        });
                        
                        
                        // eliminar producto
                           $('.trashSub').click(function(){
                            var idsd=$(this).attr("rel");
                            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                                if(response){
                                    $.ajax({
                                        url:url_deleteSalesDetail,
                                        data:{idsd:idsd},
                                        success: function(response){
                                            console.log(response.code);
                                            if(response.code == code_success){
                                                $('#grid_list_ordersales').trigger("reloadGrid");
                                            }else{
                                                msgBox(response.msg, response.code)
                                            }
                                        }
                                    });
                                }
                            });
                        });
                        
                         // editar producto 
                        $('.editSub').on('click',function(){
                         var id= $(this).attr("rel");
                         $.ajax({
                             url:url_getSalesDetailByID,
                             type:'POST',
                             dataType:'jsonp',
                             data:{idsd:id},
                             beforeSend:function(){
                                 $("#loading").dialog("open");
                             },
                             complete:function(){
                                 $("#loading").dialog("close");
                             },
                             success:function(response){
                                 if(evalResponse(response)){
                                    var r=response.data;
                                    $("#idLot").val(r.idLot);
                                    $("#idStoreDetail").val(r.idStore);
                                    $("#idProduct").val(r.idProduct);
                                    $("#idSalesDetail").val(r.idSalesDetail);
                                    
                                    $("#codigo").val(r.product);
                                    $("#descuento").val(r.discount);
                                    $("#cb_quantity").val(r.quantity);
                                    $("#cb_precio").val(r.unitPrice);
                                    $("#cb_preciototal").val(r.totalPrice);
                                    
                                    $('#sales_detail').dialog('open');
                                    
                                    
                                    // para otro producto 
                                    
                                    $('#haveDetail').val(r.haveDetail);
                                    $('#idstore').val(r.idStore);
                                    $('#idproduct').val(r.idProduct);
                                    $('#amount').val(r.amount);

                                 }
                             }
                         })
                        });                 
                    }
                    });              
                jQuery("#"+subgrid_ordersales).jqGrid('navGrid',"#"+subgrid_ordersales_pager,{edit:false,add:false,del:false,search:false,refresh:false});
                //                $("#"+subgrid_ordersales).jqGrid('navButtonAdd','#'+subgrid_ordersales_pager,
                //                { caption: '<?php echo __("Seleccione Columnas"); ?>', title: '<?php echo __("Seleccione las columnas que desea ver"); ?>', onClickButton : function (){ jQuery("#"+subgrid_package).jqGrid('columnChooser'); } });
            }            
            ,
            subGridRowColapsed: function(subgrid_id, row_id) {
            }
            ,
            gridComplete:function(){
                var ids = $('#grid_list_ordersales').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    var options='';
                    var rd = $("#grid_list_ordersales").getRowData(ids[i]); 
                    
//                    print = "<a class=\"print\" style=\"cursor: pointer;\" doc=\""+rd.documentP+"\" rel=\""+ids[i]+"\" title=\"<?php echo __('Reimprimir'); ?>\" ><?php echo __('Reimprimir'); ?></a>";
                    cancel = "<a class=\"cancel\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Cancelar'); ?>\" ><?php echo __('Anular'); ?></a>";
                    options=cancel;
                    $("#grid_list_ordersales").jqGrid('setRowData',ids[i],{actions:options});
                }
                
                
//                $('.print').button({
//                    icons:{primary: 'ui-icon-print'},
//                    text: false
//                }).click(function(){
//                    var idl=$(this).attr('rel');
//                    var doc = $(this).attr('doc');
//                     confirmBox  ('<?php echo __("Desea reimprimir documentos?"); ?>',
//                     'Confirmar Impresión',function(e){
//                         if(e){
//                             var my_pop_up = window.open('/private/sales/getPrintSales?sales_id='+idl+'&document='+doc,
//                             '<?php echo __("Impresión de Documento de Envío") ?>');
//                         }else{                                                
//                         }
//                        window.location.reload();
//                    })                              
//                });


                $('.cancel').button({
                    icons:{primary: 'ui-icon-cancel'},
                    text: false
                }).click(function(){
                    var idl=$(this).attr('rel');
                    idSales = 0;
                    confirmBox  (conftrash,titletrash,function(event){
                        if(event){
                           
                            idSales = idl;
                            dailogCancel(idSales,1);
                            
                        }
                    });
                });
            }
        });         
        $("#grid_list_ordersales").jqGrid('navGrid','#grid_list_ordersales_pager',{add:false,edit:false,del:false,search:false,refresh:false});
        //        $("#grid_list_ordersales").jqGrid('navButtonAdd','#grid_list_ordersales_pager',
        //        { caption: '<?php echo __("Seleccione Columnas"); ?>', title: '<?php echo __("Seleccione las columnas que desea ver"); ?>', onClickButton : function (){ jQuery("#grid_packaging").jqGrid('columnChooser'); } 
        
       $("#sales_detail").dialog({
         title:'<?php echo "Editar detalle de Producto"; ?>',
         autoOpen: false,
         width:450,
         height:'auto',
         show: "slide",
         hide: "slide",
         modal:true, 
         resizable: false, 
         position:["center", 70],
         buttons:{
             '<?php echo "Aceptar" ?>':function(){    
                 $("#formSalesDetail").submit();
        

                 },
             '<?php echo "Cancelar" ?>':function(){
                 $(this).dialog('close');
 //                formSalesDetail.currentForm.reset();
               //  $(".post_confirm_payment_return_cash").val('');
             }
         } 

        });
        function dailogCancel(idl,idCancel){
            $.ajax({
                url:url_UpdateSalesOrderById,                        
                type:'POST',
                dataType:'json',
                data:{idl:idl, idCancel:idCancel},
                beforeSend:function(){
                    $("#loading").dialog("open");
                },
                complete:function(){
                    $("#loading").dialog("close");
                },
                success:function(response){
                    console.log(response);
                    if(evalResponse(response)){
                        grid_list_ordersales.trigger("reloadGrid"); 
                        //$('#confirm_payment').dialog("close");
                        //msgBox('La Venta se ha Cancelado.');
                        idSale = response.idSales;
                        dataSale = response.sales;
                        $('#creditnote_idSale').val(idSale);
                        $('#creditnote_amountSale').val(dataSale.totalPrice);
                        $('#creditnote_documento').val(dataSale.document);
                        $('#creditnote_serie').val(dataSale.serie);
                        $('#creditnote_correlative').val(dataSale.correlative);
                        $('#confirm_payment').dialog( "open" );
                                
                    }else{
                        msgBox(response.msg,response.code);
                    }
                }
            });
                            
        }    
       formSalesDetail = $("#formSalesDetail").validate({
         rules: {                
                cb_quantity:{
                    required: true
                 
                }
                ,descuento:{
                    required: true
                },
                codigo:{
                    required: true
                }                
            },
            messages:{                
                descuento:{ required: '<?php echo __("El descuento es requerido.") ?>'},
                cb_quantity:{ required: '<?php echo __("La cantidad es requerida.") ?>'}
                ,codigo:{required: '<?php echo __("El producto es obligatoria.") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){
                    $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_editSalesDetail,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res)){                            
                            var data = res.data;  
                            formSalesDetail.currentForm.reset();
                            $("#sales_detail").dialog('close');
                            grid_list_ordersales.trigger("reloadGrid");
                            
                        }
                    }
                });
            }
            
           
       });
       
           /* PriceFormat  */
          
        $('#purchaseOrderDate,#deliveryDate, #dateCreate, #accountsreceivable_bdaterango, \n\
        #accountsreceivable_fdaterango, #accountsreceivable_date').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>'
        });
        
       /* Validaciones */
       $("#descuento").setMask();

       //Click de Codigo de Barras
        $("input#codigo").keypress(function(event) {
            if ( event.which == 13 ) {
                event.preventDefault();
                code = $.trim($(this).val());
                getProductByCode(code.substring(0,7));               
            } 
        });
        
        //calculo de precio detalle 
        $('#descuento').keyup(function(key){
            calculationPriceDetail();
        });
        $('#cb_quantity').keyup(function(key){
            var precio = 0;            
            returnarray($('#idproduct').val(),$('#idstore').val());            
            if ((countarray == 0 && keycountarray==0) || edit_product == 1){
                stock = $('#amount').val() - $('#cb_quantity').val();
            }else{
                stock = $('#amount').val() - (parseInt(quantity) + parseInt($('#cb_quantity').val())) ;
            }
            countarray = 0;
            keycountarray = 0;
//            if($('#idproduct').val()==""){
//                msgBox("Seleccione producto");
//                $(this).val('');
//            }else 
                
            if(stock<0){
                
                msgBox("Cantidad es mayor al stock");
                $(this).val('');
            }else if($(this).val()==0){
            }else{
                calculationPriceDetail();
                $('#cb_precioTotal').val(precio.toFixed(2));
            }            
            stock = 0;
        });
          
        calculationPriceDetail = function(){
            priceUnit = $('#cb_precio').val().split(' ').join('');
            priceUnit = priceUnit==''?0:parseFloat(priceUnit);
            
            fPreTotal = priceUnit;
            
            //quantity = $('#cb_quantity').val().split(' ').join('');
            quantity = $('#cb_quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            priceTotal = priceUnit * quantity;
            
            $('#cb_precio').val(priceUnit.toFixed(2));
            $('#cb_preciototal').val(priceTotal.toFixed(2));
            
            var discount = $('#descuento').val().split(' ').join('');
            discount = discount==''?0:parseFloat(discount);
            
            if(discount > priceTotal){
                $('#descuento').val(0);
                discount = 0;
            }else{
                priceTotal = priceTotal - discount;
               // priceTotal = priceTotal - discount;
                $('#cb_preciototal').val(priceTotal.toFixed(2));
            }   
        }
        
         /****************** 
         * Function
         *getProductByCode *
         *
         ********************/
        function getProductByCode(code){
            
            showLoading=1;
            $.ajax({
                url:url_GetProductByCodigo,                        
                type:'POST',
                dataType:'jsonp',
                data:{
                    code:function(){
                        return code
                    }
                }, 
                async:false,
                success:function(response){
                    if(evalResponse(response)){
                        var r=response.data;
                        if(r.length>0){                            
                            $.each(r,function(index,data){                                                                                            
                                    /*** Proceso de Verifcar Lotes, Almacenes y Stock ***/                
                                    var id=data.idProduct; 
                                    $.ajax({
                                        url:url_getLotByProductId,                        
                                        type:'POST',
                                        dataType:'json',
                                        data:{idpg:id}, 
                                        async:false,
                                        success:function(response){
                                            if(evalResponse(response)){
                                                var r=response.data;  
 
                                                if(r=='not_store'){ 
                                                    msgBox('<?php echo __("Su usuario de Venta no tiene asignado un almacén. <br/>Debe hacerlo para poder vender.") ?>','<?php echo __("Aviso") ?>');
                                                    return false;
                                                    flags.haveLot = 0;                                                 
                                                }else{
                                                    if(r.length>0){
                                                        flags.haveLot = 1;
                                                        var j = 0;
                                                        $.each(r,function( i ,d){

                                                            $("#codigo").val(d.productName);
                                                            $("#descuento").val('0.0');
                                                            $("#cb_quantity").val('1');
                                                            $("#cb_precio").val(d.unitPrice);
                                                            $("#cb_preciototal").val(d.unitPrice);
                                                            
                                                            
                                                            // EL producto nuevo 
                                                            $('#haveDetail').val(d.haveDetail);
                                                            $('#idstore').val(d.idStore);
                                                            $('#idproduct').val(d.idProduct);
                                                            $('#amount').val(d.amount);

//                                                            var itemDetail = new Object();
//                                                            itemDetail.id = d.idLot;
//                                                            itemDetail.productName = d.productName;
//                                                            itemDetail.amount = d.amount;
//                                                            itemDetail.creationDate = d.creationDate;                                                    
//                                                    
//                                                            arrayDetails_lots[j++] = itemDetail;                                                  
                                                        });                            
                                            
                                                    }else{
                                                        msgBox('<?php echo __("No hay Productos en Lotes") ?>','<?php echo __("Alerta") ?>');
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    /** Fin **/
                                    
                                    
                       });
                     }    
                    }
                    $('input#codigo').focus();
                    }
             });
       }
       
       /* Rango de Fechas */
       /* Buscar */
       $("#btn_search").button({
            text:true,
            icons:{
                primary:'ui-icon-search'
            }
        });
        $("#btn_search").on('click',function(e){
            e.preventDefault();
            grid_list_ordersales.trigger("reloadGrid")
        });
         /* Rango de Fechas */         
         
         $("#accountsreceivable_listMonth").combobox();
         $("#anio").combobox();
         $('.tbd_group').hide();
         $('.tbm_group').hide();
         $('.tbr_group').hide();
         $("#listSearchType").combobox({
                selected: function(){
                tipob=$(this).val();
                if(tipob=='tbd'){
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if(tipob=='tbm'){
                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if(tipob=='tbr'){
                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }
                if(tipob=='0'){   
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                }
             }
        });    
        
        // Verificar Stock 
        
           function returnarray(idproduc,idstore){
            $.each(arrayDetails, function(key, value) {

                if($('#idproduct').val() == value.product_id && $('#idstore').val()==value.store_id){
                    countarray = 1;
                    keycountarray = key;
                    quantity = value.quantity;
                }
            });
        }
        
    
});
    
    
</script>


<div>
    <center>    
        <div class="titleTables">
            <span class="title">Soporte de Ventas</span>
        </div>
    </center>
</div>

<div id="content_cc" class="ui-widget-content ui-corner-all">
    <dl>
            <dd>
                        <label for="comprobante_search"><?php echo __("Nro Comprobante"); ?>:</label>
                        <input id="comprobante_search" name="comprobante_search" type="text" />
            </dd>
    </dl>
       <dl>
        <!--Rango de Fechas-->
            <dd>
                    <label><?php echo __("Tipo de Búsqueda") ?>:</label>
                    <select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0"  >
                        <option value="0"><?php echo __("Seleccione una Opción") ?></option>  
                        <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                        <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                        <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                    </select>
            </dd>          
             <dd>
                        <label class="tbd_group frmLabel"><?php echo __("Día:") ?></label>
                        <div class="item_content">
                            <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                        </div>
             </dd>
            <dd>
                <label class="tbm_group frmLabel"><?php echo __("Mes") ?>:</label>
                <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                    <option value="0"><?php echo __("Seleccione Mes") ?></option>
                    <?php foreach ($a_month_report as $a_dt): ?>
                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                    <?php endforeach; ?>
                </select>
            </dd>
            <dd>
                <label class="tbm_group frmLabel"><?php echo __("Año") ?>:</label>
                <select class="tbm_group" id="anio" name="anio">
                    <option value="0"><?php echo __("Seleccione Año") ?></option>
                    <?php foreach ($a_year_report as $a_dt): ?>
                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                    <?php endforeach; ?>
                </select>
            </dd>
            <dd>   
                <label class="tbr_group frmLabel"><?php echo __("Fecha Inicial") ?>:</label>
                <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
            </dd>
            <br/>
            <dd>
                <label class="tbr_group frmLabel"><?php echo __("Fecha Final") ?>:</label>
                <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
            </dd>     
            <dd>
                <button id="btn_search"><?php echo __("Buscar"); ?></button>
            </dd>
    </dl>
    <div class="division ui-widget-content"></div>    
    <div class="div_grid_product">
        <center>
            <div>
                <table id="grid_list_ordersales"> </table>
                <div id="grid_list_ordersales_pager"></div>
            </div>
        </center>
    </div>
</div>



            
<div id="sales_detail" class="dialog-modal">   
        <fieldset  class="ui-corner-all cFieldSet"> 
            <legend> Detalle de Venta</legend>
                <form class="orderForm" id="formSalesDetail" method="POST" action="#">
                    <li class="order">
                        <input id="idLot" name="idLot" type="hidden" />
                        <input id="idStoreDetail" name="idStoreDetail" type="hidden" />
                        <input id="idProduct" name="idProduct" type="hidden" />
                        <input id="idSalesDetail" name="idSalesDetail" type="hidden" />
                        
                        <div class="divLine left">
                            
                            <?php if ($bBarcodeReader)
                                : ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label for="codigo" class="frmlblshort priceUnit"><?php echo __("Código") ?>:</label>
                                            <input id="codigo" name="codigo" class="subPriceLong priceUnit" type="text" />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>  
                                    <dl>
                                        <dd  style=" margin-top: 5px; ">
                                            <label for="descuento" class="frmlblshort priceUnit" ><?php echo __("Descuento") ?>:</label>
                                            <input id="descuento" class ="inputCustomPrice"name="descuento" type="text" value="0.0" alt="number" />

                                        </dd>
                                    </dl>
                                    <dl>
                                       <dd>
                                            <label for="cb_quantity" class="frmlblshort priceUnit"><?php echo __("Cantidad") ?>:</label>

                                            <input id="cb_quantity" name="cb_quantity" class="subPrice priceUnit" type="text" value=""/>
                                        </dd>
                                        <dd>
                                            <label for="cb_precio" class="frmlblshort priceTotal">Precio:</label>
                                            <input id="cb_precio" name="cb_precio" class="subPrice priceTotal" type="text"  disabled="true"/>

                                        </dd>
                                        <dd>
                                            <label for="cb_preciototal" class="frmlblshort priceTotal">Total:</label>
                                            <input id="cb_preciototal" class="subPrice priceTotal" type="text" disabled="true"/>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="divLine left">

                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" id="description" name="description" type="hidden"/>
                                        <input type="hidden" name="haveDetail" id="haveDetail"/>
                                        <input type="hidden" name="idproduct" id="idproduct"/>
                                        <input type="hidden" name="idstore" id="idstore"/>
                                        <input type="hidden" name="amount" id="amount"/>
                                    </dd>                               
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </li>
                        <li class="order">
                            <div class="subTotal">
                                <dl>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputCustomPrice ui-state-highlight" type="hidden" id="custom_price" name="custom_price" autocomplete="off"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>

                            <?php if ($bTaxEnabled): ?>
                                <div class="subTotal">
                                    <dl>
                                        <dd>
                                            <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>

                                            <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="text" disabled />
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>

                            <div class="clear"></div>
                        </li>
                    
                            <?php else: ?>
                                <li class="order">
                                    <div class="divLine left">
                                        <div class="divLine divLineShort">
                                            <dl>
                                                <dd>
                                                    <label for="description" class="frmlbl"><?php echo __("Descripción") ?>:</label>                    
                                                </dd>
                                                <dd>                
                                                    <div class="clear"></div>
                                                    <input class="inputData" id="description" name="description" type="text"/>
                                                    <input type="hidden" name="haveDetail" id="haveDetail"/>
                                                    <input type="hidden" name="idproduct" id="idproduct"/>
                                                    <input type="hidden" name="idstore" id="idstore"/>
                                                    <input type="hidden" name="amount" id="amount"/>
                                                </dd>                               
                                            </dl>
                                            <div class="clear"></div>

                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="divLeft">
                                        <dl>
                                            <dd>
                                                <label for="quantity" class="labelprice"><?php echo __("Cantidad") ?>:</label>                    
                                            </dd>
                                            <dd>                
                                                <div class="clear"></div>
                                                <input class="inputQuantity" type="text" id="quantity" name="quantity" alt="number" autocomplete="off"/>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="divLeft">
                                        <dl>
                                            <dd>
                                                <label for="custom_price" class="labelprice"><?php echo __("Precio") ?>:</label>                    
                                            </dd>
                                            <dd>                
                                                <div class="clear"></div>
                                                <input class="inputCustomPrice ui-state-highlight" type="text" id="custom_price" name="custom_price" autocomplete="off"/>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="divLeft addSales buttonadd">
                                        <dl>
                                            <dd>
                                                <button type="button" id="addPackageDetail" class="ui-state-highlight"><?php echo __("Agregar") ?></button>
                                            </dd>
                                        </dl>
                                    </div>

                                    <div class="clear"></div>
                                </li>
                                <li class="order">

                                    <?php if ($bTaxEnabled): ?>
                                        <div class="subTotal">
                                            <dl>
                                                <dd>
                                                    <label class="frmlblshort priceTax"><?php echo $sTaxName . ' (' . $fTax . ' %)' ?>:</label>

                                                    <input id="priceTax" name="priceTax" class="subPrice priceTax ui-state-highlight" type="text" disabled />
                                                </dd>
                                            </dl>
                                            <div class="clear"></div>
                                        </div>
                                    <?php endif ?>                         

                                    <div class="clear"></div>
                                </li>
                        <?php endif; ?>  

                </form>
            
            
        </fieldset>
</div>


