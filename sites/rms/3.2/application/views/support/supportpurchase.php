<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
     #content_cc{
         
        margin: 10px auto 30px;
        padding: 20px;
        width: 1200px;
        text-align: center;    
    }
    #content_cc>dl>dd{
        margin-top: 10px;
        font-family: arial;
        font-size: 12px;
        font-weight: bolder;    
    }
    .division{
            border-style: none none dashed;
            margin-bottom: 24px;
            margin-top: 24px;
            border-width:  2px;
    }
      .ui-autocomplete {
        max-height: 120px;
        overflow-x: hidden;
        overflow-y: auto;
        padding-right: 20px;
        }
       input.helptext{font-style: italic;}
       .relative{position: relative;}
       input.serie
       { width: 50px;text-align: right;
           margin-left: 4px;
           margin-top: 2px;}
       input.purchaseDocuments, input.chars_supplier{
           margin-left: 4px;
           margin-top: 2px;
       }
       input.correlative{width: 85px;text-align: right;}
           .ui-widget-content .search_supplier_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    .ui-widget-content .search_supplier_wrapper{background-image: none;margin-right: 5px;}
    .ui-widget-content .search_product_wrapper{background-image: none;margin-right: 5px;}
    .search_supplier_wrapper, .search_supplier_wrapper span,
    .search_product_wrapper, .search_product_wrapper span,
    .search_supplier_wrapper input{float: left;}
    .search_product_wrapper input{float: left;}
    .ui-widget-content .search_product_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    
    .labelPurchase{
                    float: left;
                    font-family: arial;
                    font-size: 12px;
                    font-weight: bolder;
                    text-align: right;
                    width: 147px;
                   
    }
    .txtPurchase{
           margin-left: 4px;
    }
    #quantity , #totalAmount, #priceUnit , #amountTotal{
            width: 45px;
    }
    .cFieldSet>dl>dd{
        margin-top: 5px
    }
</style>
<script type="text/javascript">
    
    var url_listPurchase = '/private/supportpurchase/listPurchase'+jquery_params;
    var url_listSubPurchaseorder = '/private/supportpurchase/listPurchaseDetail'+jquery_params;
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar este detalle de orden de compra?") ?>';
    var deleteSubTitle = '<?php echo __("Eliminar Detalle de Orden de Compra") ?>';
    var url_deleteOrderDetail = '/private/supportpurchase/deletePurchaseDetail'+jquery_params;
    var url_deleteOrderPurchase = '/private/supportpurchase/deletePurchaseOrder'+jquery_params;
    var grid_purchase;
    var url_getSupplier = '/private/purchaseorders/getSupplier'+jquery_params;  
    var url_getOrderPurchase = '/private/supportpurchase/getOrderPurchase'+jquery_params;  
    var url_editOrderPurchase = '/private/supportpurchase/editOrderPurchase'+jquery_params;  
    var url_getOrderPurchaseDetail = '/private/supportpurchase/getOrderPurchaseDetail'+jquery_params;  
    var url_editOrderPurchaseDetail= '/private/supportpurchase/editOrderPurchaseDetail'+jquery_params;  
    var url_getProduct = '/private/purchaseorders/getProduct'+jquery_params;
    var sTxtSearchSupplier = '<?php echo __('Búsqueda de Proveedor') ?>';
    var sTxtSearchProduct = '<?php echo __('Búsqueda de Producto') ?>';
    
    $(document).ready(function(){

       $("#purchaseDocuments").combobox();        

       grid_purchase = $("#purchase_jqGrid").jqGrid({
            url: url_listPurchase,
            datatype: 'jsonp',
            mtype: 'POST', 
            postData:{
                    idSupplier : function(){ return $("#supplier_search").val()},
                    date: function(){return $("#accountsreceivable_date").val()},
                    dateini: function(){return $("#accountsreceivable_bdaterango").val()},
                    datefin: function(){return $("#accountsreceivable_fdaterango").val()},
                    month: function(){return $("#accountsreceivable_listMonth").val()},
                    year: function(){return $("#anio").val()}      
            },
            colNames:[
                'ID',
                '<?php echo __("Nro Documento") ?>',
                '<?php echo __("Documento") ?>', 
                '<?php echo __("Nro Order de Compra") ?>',
                '<?php echo __("Proveedor") ?>',
                '<?php echo __("Fecha") ?>',
                '<?php echo __("Estado") ?>',
                '<?php echo __("Tipo de Pago") ?>',
                '<?php echo __("Impuesto") ?>',
                '<?php echo __("Total") ?>',
                '<?php echo __("Opción") ?>'
            ],
            colModel:[ 
                {name:'ID',index:'ID', key:true, hidden:true},
                {name:'nrodocument',index:'nrodocument',width:100,align:'center', sorttype:'int'},
                {name:'purchaseDocument',index:'purchaseDocument',width:100,align:'center', search:false},
                {name:'document',index:'document',width:80,align:'center', search:false},
                {name:'name', index:'name',width:200,align:'center'},
                {name:'datePurchaseOrder',index:'datePurchaseOrder',width:200,align:'center'},
                {name:'statusPurchase',index:'statusPurchase',width:200,align:'center', search:false},
                {name:'methodPayment',index:'methodPayment',width:200,align:'center', search:false },
                {name:'totalTax',index:'totalTax',width:100,align:'center', search:false},
                {name:'totalAmount',index:'totalAmount',width:80,align:'center', search:false },  
                {name:'actions',index:'actions',width:80,align:'center', search:false}
            ],
            pager:"#purchase_jqGrid_pager",
            sortname: 'ID',
            sortable: true,            
            sortorder: "asc",
            rownumbers: true,       
            rownumWidth: 30,                      
            caption: '<?php echo __("Lista de Ordenes de Compra"); ?>',
            rowList: [10,20,50,100,200,400],
            gridview: true,             
            rowNum:30,
            width:1200,
            height:400,
            shrinkToFit:true,
            viewrecords: true, 
            //multiselect:true, 
            subGrid:true,
            // caption:"Support", 
            gridComplete:function(){
                var ids = $('#purchase_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){ 
                    var option='';
                    var po = $("#purchase_jqGrid").getRowData(ids[i]);
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    if(po.statusPurchase == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_CANCELADO; ?>')
                    {
                       $('#purchase_jqGrid tr#'+ids[i]).addClass('ui-state-error');
                       option = ''; 
                    
                    }else if(po.statusPurchase == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_RECEPCIONADO; ?>' ||
                            po.statusPurchase == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_FACTURADO; ?>'){
                       option = ''; 
                    }else 
                    {
                       option = edit + trash;  
                    }
                    $("#purchase_jqGrid").jqGrid('setRowData',ids[i],{actions :option });
                }
                $('.edit').button({icons:{primary: 'ui-icon-pencil'},text: false});
                $('.trash').button({icons:{primary: 'ui-icon-trash'},text: false});
                //Boton eliminar orden de compra                 
                $('.trash').click(function(){
                var id=$(this).attr("rel");                          
                confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                  if(response){
                        $.ajax({
                            url:url_deleteOrderPurchase,
                            data:{
                                id:id
                            },
                            success: function(response){
                                if(response.code == code_success){
                                    $('#purchase_jqGrid').trigger("reloadGrid");
                                }else{
                                    msgBox(response.msg, response.code)
                                }
                            }
                        });
                        }
                    });
                });
                
                $('.edit').click(function(){                
                 var id=$(this).attr('rel');
                    $.ajax({
                        url:url_getOrderPurchase,                        
                        type:'POST',
                        dataType:'json',
                        data:{id:id},
                        beforeSend:function(){
                            $("#loading").dialog("open");
                        },
                        complete:function(){
                            $("#loading").dialog("close");
                        },
                        success:function(response){
                            if(evalResponse(response)){
                                var or=response.data;
                                $('#correlative').val(or.correlative);
                                $('#serie').val(or.serie);
                                $('#idOrderPurchase').val(or.idPurchaseOrder);
                                // combobox! 
                                $('#purchaseDocuments').val(or.purchaseDocument);
                                $('#purchaseDocuments-combobox').val($("#purchaseDocuments option:selected").text());   
                                $('#deliveryAddress').val(or.deliveryAddress);
                                $('#chars_supplier').val(or.name);
                                $('#idSupplier').val(or.idSupplier);
                                $('#purchaseOrderDate').val(or.datePurchaseOrder);
                                $('#deliveryDate').val(or.dateDeliveryOrder);
                                $('#dlg_editOrderPurchase').dialog('open'); 
                            }
                        }             
                    });
                });
            },
            subGridRowExpanded: function(subgrid_id, row_id) {                  
                var rd = $("#purchase_jqGrid").getRowData(row_id);                  
                var subgrid_purchaseorder, subgrid_purhcaseorder_pager;
                subgrid_purchaseorder = subgrid_id+"_t";                
                subgrid_purchaseorder_pager = "p_"+subgrid_purchaseorder; 
                $("#"+subgrid_id).html("<table id='"+subgrid_purchaseorder+"' class='scroll'></table><div id='"+subgrid_purchaseorder_pager+"' class='scroll'></div>");                                
                jQuery("#"+subgrid_purchaseorder).jqGrid({
                    url:url_listSubPurchaseorder, 
                    datatype: "jsonp",
                    mtype: 'POST',
                    postData:{
                        idOrderPurchase:function(){
                            return row_id;
                        }
                    },                 
                    colNames: [       
                        'ID',
                        'IDPO',
                        '<?php echo __("Producto") ?>',
                        '<?php echo __("Serie Producto") ?>',
                        '<?php echo __("Cantidad") ?>',
                        '<?php echo __("Precio Unidad") ?>',
                        '<?php echo __("Total") ?>',
                        'status',
                        '<?php echo __("Acciones") ?>'
                        
                    ],                       
                    colModel: [ 
                        {name:"ID", index: "ID", hidden:true},
                        {name:"IDPO", index: "IDPO", hidden:true},
                        {name:"product", index: "product"},
                        {name:"aliasProduct", index: "aliasProduct"},
                        {name:"quantity", index: "quantity"},
                        {name:"totalUnit", index: "totalUnit"},
                        {name:"totalAmount", index: "totalAmount"},
                        {name:"status", index: "status", hidden: true},
                        {name:"actions",index:"actions",width:100,align:"right"}
                    ],
                    rowNum:20, 
                    width:1000,
                    height:400,
                    pager: subgrid_purhcaseorder_pager, 
                    sortname: 'ID',
                    sortorder: "desc",
                    height: '100%',             
                    gridComplete:function(){
                        var idSub = $("#"+subgrid_purchaseorder).jqGrid('getDataIDs');                        
                        for(var i=0;i<idSub.length;i++){
                            var optionSub = '';             
                            var rd = $("#"+subgrid_purchaseorder).getRowData(idSub[i]); 
   
                            editSub = "<a class=\"editSub\" style=\"cursor: pointer;\" rel=\""+rd.ID+"\" title=\"<?php echo __('Editar'); ?>\"><?php echo __('Editar'); ?></a>";                        
                            trashSub = "<a class=\"trashSub\" style=\"cursor: pointer;\" data-id= \""+rd.IDPO+"\" rel=\""+rd.ID+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>"; 
                            if(rd.status == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_CANCELADO; ?>' ||
                            rd.status == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_RECEPCIONADO; ?>' ||
                            rd.status == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_FACTURADO; ?>'){
                               optionSub = ''; 
                            }else 
                            {
                               optionSub = editSub + trashSub;
                            }                          
                            $("#"+subgrid_purchaseorder).jqGrid('setRowData',idSub[i],{actions: optionSub}); 
                        }
                        $('.editSub').button({
                            icons:{primary: 'ui-icon-pencil'},
                            text:false
                        });
                        $('.trashSub').button({
                            icons:{primary: 'ui-icon-trash'},
                            text:false
                        });
                        // Boton de Eliminar
                        $('.trashSub').click(function(){
                            var idod=$(this).attr("rel");
                            var st=$(this).attr("data-id");                            
                            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                                if(response){
                                    $.ajax({
                                        url:url_deleteOrderDetail,
                                        data:{
                                            idod:idod,
                                            st:st
                                        },
                                        success: function(response){
                                            if(response.code == code_success){
                                                $('#purchase_jqGrid').trigger("reloadGrid");
                                            }else{
                                                msgBox(response.msg, response.code)
                                            }
                                        }
                                    });
                                }
                            });
                        });
                        // Boton de Editar
                         $('.editSub').click(function(){
                                var id= $(this).attr("rel");
                              $.ajax({
                                url:url_getOrderPurchaseDetail,                        
                                type:'POST',
                                dataType:'json',
                                data:{id:id},
                                beforeSend:function(){
                                    $("#loading").dialog("open");
                                },
                                complete:function(){
                                    $("#loading").dialog("close");
                                },
                                success:function(response){
                                    if(evalResponse(response)){
                                        var or=response.data;
                                        
                                        $("#chars_product").val(or.productName);                                        
                                        $("#idOrderPurchaseDetail").val(or.idPurchaseOrderDetail);
                                        $("#idProduct").val(or.idProduct);
                                        $("#productUnitMeasures").val(or.productUnitMeasures);
                                     //   $("#description_note").val(or.idProduct);
                                        $("#quantity").val(or.quantity);
                                        $("#priceUnit").val(or.totalUnit);
                                        $("#amountTotal").val(or.totalAmount);
                                       
                                         $('#dlg_editDetailPurcharse').dialog('open')
                                    }
                                }             
                            });
                        });
                    
                    }

              });
            }
 
        });
        $("#purchase_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : false})
        $("#accountsreceivable_listMonth").combobox();
        $("#anio").combobox();
        /* PriceFormat  */
          
        $("#priceUnit, #amountTotal").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });      
        
        /* Función para calcular precio */ 
        
        $("#quantity").setMask();
        $("#priceUnit").keyup(function(key){
            if($("#quantity").val() != ''){
                CalculationPrice();                
            }        
        });
        $("#quantity").keyup(function(key){
            if($("#priceUnit").val() != ''){
                CalculationPrice();                
            }        
        });

        function CalculationPrice (){
            priceUnit = $('#priceUnit').val().split(' ').join('');
            priceUnit = priceUnit==''?0:parseFloat(priceUnit);          
            fPreTotal =priceUnit;
            quantity = $('#quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            amountTotal = priceUnit * quantity;
            $('#amountTotal').val(amountTotal.toFixed(2));
        };
        
        $("#purchase_jqGrid").jqGrid('navGrid','#purchase_jqGrid_pager',{add:false,edit:false,del:false,search:true,refresh:true});                        
        
        // 
        
        $("#dlg_editDetailPurcharse").dialog({
         title:'<?php echo "Editar Detalle de Orden de Compra"; ?>',
         autoOpen: false,
         width:450,
         height:'auto',
         show: "slide",
         hide: "slide",
         modal:true, 
         resizable: false, 
         position:["center", 70],
         buttons:{
             '<?php echo "Aceptar" ?>':function(){    
                     $("#frmPurchaseOrderDetail ").submit();
              },
             '<?php echo "Cancelar" ?>':function(){
                    $(this).dialog('close');
                     frmPurchaseOrderDetail .currentForm.reset();
             }
         } 
        }); 
        
        $("#dlg_editOrderPurchase").dialog({
         title:'<?php echo "Editar Orden de Compra"; ?>',
         autoOpen: false,
         width:450,
         height:'auto',
         show: "slide",
         hide: "slide",
         modal:true, 
         resizable: false, 
         position:["center", 70],
         buttons:{
             '<?php echo "Aceptar" ?>':function(){    
                 $("#frmPurchaseOrder").submit();
              },
             '<?php echo "Cancelar" ?>':function(){
                 $(this).dialog('close');
                 frmPurchaseOrder.currentForm.reset();
             }
         } 
        });
        
        /*--datepicker--*/
        $('#purchaseOrderDate,#deliveryDate, #dateCreate, #accountsreceivable_bdaterango, \n\
        #accountsreceivable_fdaterango, #accountsreceivable_date').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>'
        });
        
        /*-- Validación de Editar de Order de Compra --*/
        
        frmPurchaseOrder = $('#frmPurchaseOrder').validate({
            rules: {                
                purchaseDocuments:{required: true },
                chars_supplier:{required:true },
                serie:{required:true},
                correlative:{required:true},
                purchaseOrderDate:{required:true},
                deliveryDate:{required:true}
            },
            messages:{                
                purchaseDocuments:{ required: '<?php echo __("El documento es requerido.") ?>'},
                chars_supplier:{ required: '<?php echo __("El proveedor es requerido.") ?>'},
                serie:{ required: '<?php echo __("La serie es requerida.") ?>'},
                correlative:{ required: '<?php echo __("EL correlativo es requerido.") ?>'},
                purchaseOrderDate:{ required: '<?php echo __("La Fecha de Emisión es requerido.") ?>'},
                deliveryDate:{ required: '<?php echo __("La Fecha de Vencimiento es requerido.") ?>'}
                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){
                    $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_editOrderPurchase,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res)){     
                            var data = res.data;  
                            frmPurchaseOrder.currentForm.reset();
                            $("#dlg_editOrderPurchase").dialog('close');
                            grid_purchase.trigger("reloadGrid");
                            
                        }
                    }
                });
            }
        });
        /*-- Validación de Editar Detalle de Order de Compra --*/
         frmPurchaseOrderDetail = $('#frmPurchaseOrderDetail').validate({
             rules: {                
                chars_product:{required: true },
                quantity:{required: true },
                priceUnit: {required: true }
            },
            messages:{                
                chars_product:{ required: '<?php echo __("El producto es requerido.") ?>'},
                quantity:{ required: '<?php echo __("La cantidad es requerida.") ?>'},
                priceUnit:{ required: '<?php echo __("El precio es requerido.") ?>'}
                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){
                    $.ajax({
                    type:"POST",
                    dataType:"jsonp",
                    url:url_editOrderPurchaseDetail,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");},
                    complete:function(){
                        $("#loading").dialog("close");                                               
                    },
                    success:function(res){
                        if(evalResponse(res)){     
                            var data = res.data;  
                            frmPurchaseOrder.currentForm.reset();
                            $("#dlg_editDetailPurcharse").dialog('close');
                            grid_purchase.trigger("reloadGrid");
                            
                        }
                    }
                });
            }
         });
         /* Buscar */
       $("#btn_search").button({
            text:true,
            icons:{
                primary:'ui-icon-search'
            }
        });
        $("#btn_search").on('click',function(e){
            e.preventDefault();
            grid_purchase.trigger("reloadGrid")
        });
         /* Rango de Fechas */         
         $('.tbd_group').hide();
         $('.tbm_group').hide();
         $('.tbr_group').hide();
         $("#listSearchType").combobox({
                selected: function(){
                tipob=$(this).val();
                if(tipob=='tbd'){
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if(tipob=='tbm'){
                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if(tipob=='tbr'){
                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }
                if(tipob=='0'){   
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                }
             }
        });

        /*--Búsqueda de Proveedor--*/
        $( "#chars_supplier" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_supplier',
            source:function(req,response){
                $.ajax({
                    url:url_getSupplier,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.supplier + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.supplier,
                                    id : item.idSupplier,
                                    dir : item.address
                                }
                            }));
                        }
                    }
                });
            },
            focus: function (event, ui){
                $('#deliveryAddress').val(ui.item.dir);
            },
            select: function( event, ui ) {
                $('#idSupplier').val(ui.item.id); 
                $('#deliveryAddress').val(ui.item.dir);
            }
            
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#chars_supplier" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 290, 'margin-bottom': 5});            
        };
    /* Seleccion del Proveedor */
    
       $("#supplier_search").combobox(
       {
            selected:function(ui,event){
                $("#supplier_search").trigger('change');
            }
       }
       );
    $("#supplier_search").change(function(){
            grid_purchase.trigger("reloadGrid")
    });
    /* Busqueda de Producto  */
     $( "#chars_product" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_product',
            source:function(req,response){
                $.ajax({
                    url:url_getProduct,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.product + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.product,
                                    id : item.idProduct,
                                    unit : item.productUnitMeasures
                                }
                            }));
                        }
                    }
                });
            },
            focus: function (event, ui){
                $('#productUnitMeasures').val(ui.item.unit);
            },
            select: function( event, ui ) {
                $('#idProduct').val(ui.item.id); 
                $('#productUnitMeasures').val(ui.item.unit);
            }
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };        
        $( "#chars_product" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 290, 'margin-bottom': 5});            
        };
       $('#chars_supplier').helptext({value: sTxtSearchSupplier, customClass: 'lightSearch'});
       $('#chars_product').helptext({value: sTxtSearchProduct, customClass: 'lightSearch'});


});
</script>
<div>
    <center>    
        <div class="titleTables">
            <span class="title">Soporte de Compras</span>
        </div>
    </center>
</div>

<div id="content_cc" class="ui-widget-content ui-corner-all">
    <dl>
            <dd>
                        <label for="supplier_search"><?php echo __("Proveedor"); ?>:</label>
                        <select id="supplier_search" name="supplier_search">
                        <option value="0"><?php echo __("Seleccione una Opción"); ?></option>
                        <?php
                        foreach ($supplier as $v) {
                            ?>
                            <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                            <?php
                        }
                        ?>
                    </select> 
            </dd>
    </dl>
       <dl>
        <!--Rango de Fechas-->
            <dd>
                    <label><?php echo __("Tipo de Búsqueda") ?>:</label>
                    <select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0"  >
                        <option value="0"><?php echo __("Seleccione una Opción") ?></option>  
                        <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                        <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                        <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                    </select>
            </dd>          
             <dd>
                        <label class="tbd_group frmLabel"><?php echo __("Día:") ?></label>
                        <div class="item_content">
                            <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                        </div>
             </dd>
            <dd>
                <label class="tbm_group frmLabel"><?php echo __("Mes") ?>:</label>
                <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                    <option value="0"><?php echo __("Seleccione Mes") ?></option>
                    <?php foreach ($a_month_report as $a_dt): ?>
                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                    <?php endforeach; ?>
                </select>
            </dd>
            <dd>
                <label class="tbm_group frmLabel"><?php echo __("Año") ?>:</label>
                <select class="tbm_group" id="anio" name="anio">
                    <option value="0"><?php echo __("Seleccione Año") ?></option>
                    <?php foreach ($a_year_report as $a_dt): ?>
                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                    <?php endforeach; ?>
                </select>
            </dd>
            <dd>   
                <label class="tbr_group frmLabel"><?php echo __("Fecha Inicial") ?>:</label>
                <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
            </dd>
            <br/>
            <dd>
                <label class="tbr_group frmLabel"><?php echo __("Fecha Final") ?>:</label>
                <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
            </dd>     
            <dd>
                <button id="btn_search"><?php echo __("Buscar"); ?></button>
            </dd>
    </dl>
    <div class="division ui-widget-content"></div>    
    <div class="div_gridPurchase">
        <center>
            <div>
            <table id="purchase_jqGrid"></table>
            <div id="purchase_jqGrid_paper"></div>
            </div>
        </center>
    </div>

</div>

<div id="dlg_editDetailPurcharse" class="dialog-modal">   
    <form id="frmPurchaseOrderDetail" method="post">
      <fieldset  class="ui-corner-all cFieldSet"> 
            <legend class="frmSubtitle">Detalle de Orden de Compra</legend>
                        <dl>
                                    <dd>
                                        <input type="hidden" id="idOrderPurchaseDetail" name="idOrderPurchaseDetail" />
                                        <label for="chars_product" class="labelPurchase"><?php echo __("Producto"); ?>:</label>
                                        <div class="search_product_wrapper ui-corner-all ui-state-default">
                                            <input class="autocompleteInput inputData" type="text" id="chars_product" name="chars_product" value="<?php echo 'Búsqueda de Producto' ?>"/>
                                            <input type="hidden" id="idProduct" name="idProduct"/>
                                            <div class="clear"></div>
                                        </div>
                                        <div id="auto_div_product"></div>
                                        <div class="clear"></div>
                                    </dd>
   
                     <!--        <dd>
                                       <label for="description_note" class="frmlbl"><?php echo __("Nota") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <textarea id="description_note" name="description_note" class="frmlbl"></textarea>
                                    </dd>                               -->

                                    <dd>
                                        <label for="productUnitMeasures" class="labelPurchase"><?php echo __("Medida") ?>:</label>                    
                                        <input class="txtPurchase" type="text" id="productUnitMeasures" name="productUnitMeasures" disabled/>
                                    </dd>     
                                    <dd>
                                        <label for="quantity" class="labelPurchase"><?php echo __("Cantidad") ?>:</label>                    
                                        <input class="txtPurchase" type="text" id="quantity" name="quantity" alt="number"/>
                                    </dd>
                                    <dd>
                                        <label for="priceUnit" class="labelPurchase" ><?php echo __("Precio Unitario") ?>:</label>
                                        <input  type="text" id="priceUnit" name="priceUnit" class="txtPurchase" />
                                    </dd>
                                    <dd>
                                        <label for="amountTotal"  class="labelPurchase"><?php echo __("SubTotal") ?>:</label>
                                        <input class="txtPurchase" type="text" id="amountTotal" name="amountTotal" disabled/>
                                    </dd>
                            </dl>
      </fieldset>
    </form>
    
</div>
<div id="dlg_editOrderPurchase" class="dialog-modal">
    <form id="frmPurchaseOrder" method="POST" action="#">
        <fieldset class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle">Orden de Compra</legend>
                <dl>
                    <dd>
                        <input type="hidden" id="idOrderPurchase" name="idOrderPurchase" />
                    </dd>
                    <dd>
                        <label for="chars_supplier" class="labelPurchase"><?php echo __("Proveedor") ?>:</label>
                        <div class="search_supplier_wrapper ui-corner-all ui-state-default">
                            <input class="autocompleteInput inputData" type="text" id="chars_supplier" name="chars_supplier" value="<?php echo 'Búsqueda de Proveedor' ?>"/>
                            <input type="hidden" id="idSupplier" name="idSupplier"/>
                        </div>
                        <div id="auto_div_supplier"></div>
                    </dd>
                    <dd>
                    </dd>
                    <dd>
                        <label for="deliveryAddress" class="labelPurchase"><?php echo __("Dirección") ?>:</label>                    
                        <input id="deliveryAddress" name="deliveryAddress" type="text" class="txtPurchase"/>
                    </dd>
                    <dd>
                          <label for="purchaseDocuments" class="labelPurchase"><?php echo __("Tipo de Documento"); ?>:</label>
                          <select class="purchaseDocuments" id="purchaseDocuments" name="purchaseDocuments" >
                              <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                              <?php
                              foreach ($a_purchase_documents as $a_purchase) {
                                  ?>
                                  <option value="<?php echo $a_purchase['value'] ?>"><?php echo $a_purchase['display'] ?></option>
                                  <?php
                              }
                              ?>
                          </select>
                    </dd>
                    <dd>
                        <label for="serie" class="labelPurchase"><?php echo __("Serie-Correlativo") ?>:</label>
                        <input class="serie"  id="serie" name="serie" value="" type="text" alt="number"/>
                        <label><?php echo __("-") ?></label>
                        <input class="correlative"  id="correlative" name="correlative" value="" type="text" alt="number"/>
                    </dd>

                    <dd>
                        <label class="labelPurchase"><?php echo __("Fecha de Emisión") ?>:</label>                    
                        <input class="txtPurchase" name="purchaseOrderDate" id="purchaseOrderDate" type="text" value="<?php echo date($date_format_php) ?>"/>    
                    </dd>     
                    <dd>
                        <label class="labelPurchase"><?php echo __("Fecha de Vencimiento") ?>:</label>                              
                        <input class="txtPurchase" name="deliveryDate" id="deliveryDate" type="text" value="<?php echo date($date_format_php) ?>"/>
                    </dd>   
                </dl>

        </fieldset>    
    </form>
</div>