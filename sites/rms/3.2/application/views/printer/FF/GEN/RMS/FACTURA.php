<style type="text/css">
    #boleta{
        width: 600px;
        /*                border: solid 1px #00e57d;*/
    }
    #cabecera{
        /*       // height: 50px;*/
        /*                border: solid #000 1px;*/
        padding-top: 100px;
    }
    .cab{
        float:left;
        /*                border: solid #00bbe5 1px;*/
    }
    #date{
        width: 350px;

    }
    #document{
        width: 244px;
    }
    #day{
        margin-left: 100px;
        padding-left: 10px;
    }
    #day, #serie, #customers, #address{
        margin-left: 100px;
        padding-left: 10px;
    }
    #month, #year, #correlative{
        padding-left: 10px;
    }
    #address{
        width: 380px;
    }
    #doc{
        width: 110px;
        /*        margin-left: 200px;*/
    }
    .tex{
        font-size: 12px;
        font-family: Tahoma;
        margin: 3px 0 0 0;
        padding: 0;
    }
    #description{
        padding-top: 25px;
    }
    #amount{
        width: 50px;
        text-align: center;
    }
    #product{
        width: 320px;
        padding-left: 10px;
    }
    #unitprice, #discount{
        width: 60px; 
        text-align: right;
        padding-right: 10px;
    }
    #salesvalue{
        width: 70px; 
        text-align: right;
        padding-right: 10px;
    }
    #totaltext{
        margin-left: 50px;
        width: 470px;
    }
    #total{
        width: 70px; 
        text-align: right; 
        padding-right: 10px
    }
    .total{
        /*        border: solid 1px #00bbe5;*/
        float: right; 
        width: 70px; 
        text-align: right; 
        padding-right: 10px
    }
    .clear{
        clear:both;
    }
</style>
<div id="boleta">
    <div id="cabecera">
        <div class="cab" id="date">
            <div class="cab" id="day">
                <?php echo $day ?>
            </div>
            <div class="cab" id="month">
                <?php echo $month ?>
            </div>
            <div class="cab" id="year">
                <?php echo $year ?>
                
            </div>
        </div>
        <div class="cab" id="document">
            <div class="cab" id="serie">
                <?php echo $Serie ?>
            </div>
            <div class="cab" id="correlative">
                <?php echo $Correlative ?>
            </div>
        </div>
        <div class="clear"></div>
        <div>
            <div id="customers" >
                <?php echo $Customers->customers ?>
            </div>
            <div id="address" class="cab">
                <?php echo $Customers->address ?>
            </div>
            <div id="doc" class="cab">
                <?php echo $Customers->document ?>
            </div>
            <div class="clear"></div>
        </div>    
    </div>
    <div id="description">
        <?php foreach ($detail as $a_dt): ?>
            <div class="cab" id="amount">
                <?php echo $a_dt['quantity'] ?>
            </div>
            <div class="cab" id="product">
                <?php echo $a_dt['product'] ?>
            </div>
            <div class="cab" id="unitprice">
                <?php echo $a_dt['unitPrice'] ?>
            </div>
        <div class="cab" id="discount">
                <?php echo $a_dt['discount'] ?>
            </div>
            <div class="cab" id="salesvalue">
                <?php echo $a_dt['totalPrice'] ?>
            </div>
            <div class="clear"></div>
        <?php endforeach; ?>
    </div>
    <div class="cab" id="totaltext"><?php echo $TotalPriceText ?></div>

    <div class="total" id="subtotal">
        <?php echo $TotalPrice-$tax; ?>
    </div>


    <div class="clear"></div>
    <div class="total" id="igv">
        <?php echo $tax ?>
    </div>
    <div class="clear"></div>
    <div class="total" id="total">
        <?php echo $TotalPrice ?>
    </div>
    <div class="clear"></div>

</div>