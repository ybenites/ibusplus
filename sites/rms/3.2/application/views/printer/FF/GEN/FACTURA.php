<style type="text/css">
    #boleta{
        width: 300px;
    }
    #cabecera{
        padding-top: 10px;
        margin-left: 10px;
    }
    .cab{
        float:left;
    }
    #date{
        width: 350px;

    }
    #document{
        width: 244px;
        margin-left: 10px;
    }
    #date{
        margin-left: 50px;
        padding-left: 10px;
    }
    #serie, #customers, #address{
        margin-left: 50px;
        padding-left: 10px;
    }
    #correlative{
        padding-left: 5px;
    }
    #address{
        width: 380px;
    }
    #doc{
        width: 110px;
    }
    *{
        font-size: 12px;
        font-family: Tahoma;
        margin: 3px 0 0 0;
        padding: 0;
    }
    #description{
        padding-top: 35px;
    }
    #amount{
        width: 25px;
        text-align: center;
    }
    #product{
        width: 170px;
        padding-left: 10px;
    }
    #unitprice, #discount{
        width: 50px; 
        text-align: right;
        padding-right: 10px;
    }
    #salesvalue{
        width: 50px; 
        text-align: right;
        padding-right: 10px;
    }
    #totaltext{
        margin-left: 50px;
        width: 470px;
    }
    #total{
        width: 180px; 
        text-align: right; 
        padding-right: 40px;
        float: right;
    }
    #footer{
        width: 240px; 
        text-align: right; 
        float: left;
    }
    .clear{
        clear: both;
    }
</style>
<div id="boleta">
    <div id="companyname" style="width: 150px;margin: 10 auto; font-weight: bold;">
        <?php echo ("YESTUMODA E.I.R.L."); ?>
    </div> 
    <div id="companycode" style="width: 150px;margin: 10 auto; font-weight: bold;">
        <?php echo ("R.U.C. 20539824091"); ?>
    </div>  
    <div id="cabecera" class="cabecera">
        <div id="companyaddress">
            <?php echo ("Jr. Francisco de Zela 341 - Trujillo"); ?>
        </div>
        <div id="companyweb">
            <?php echo ("Visitenos en: www.yestumoda.com"); ?>
        </div>
        <div id="companyphone">
            <?php echo ("Tel.: 44-207152"); ?>
        </div>
        <div id="companycellphone">
            <?php echo ("Cel.: 949780697 / RPM: #533752"); ?>
        </div>     
        <div>
                <?php echo "<b>FECHA:</b> ".$SalesDate; ?>
        </div>
        <div>
                <?php echo "<b>RAZON SOCIAL:</b> ".$Customers->customers; ?>
        </div>
        <div>
                <?php echo "<b>VENDEDOR(A):</b> ".$Seller['seller']; ?>
        </div>
    </div>
    <div id="header">
        <div class="cab" id="document">
            <div class="cab">
                <?php echo "<b>" . $DocumentType . "</b>" ?>
            </div>
            <div class="cab" id="serie">
                <?php echo "<b>Nro:</b> " . $Serie . " - "; ?>
            </div>
            <div class="cab" id="correlative">
                <?php echo $Correlative ?>
            </div>
        </div>
    </div>
    <br/>
    <div id="description">
        <?php foreach ($detail as $a_dt): ?>
            <div class="cab" id="amount">
                <?php echo $a_dt['quantity'] ?>
            </div> 
            <div class="cab" id="product">
                <?php echo $a_dt['productName'] ?>
            </div>
            <div class="cab" id="salesvalue">
                <?php echo $a_dt['totalPrice'] ?>
            </div>
            <div class="clear"></div>
        <?php endforeach; ?>
    </div>
    <br/>
    <div id="total">
    <div>
        <?php echo "TOTAL: S/." . $TotalPrice; ?>
    </div>
    <div>
        <?php echo "TOTAL DE EFECTIVO: S/." . $PaymentAmount; ?>
    </div>
    <div>
        <?php echo "VUELTO: S/." . $ReturnCash; ?>
    </div>
    </div>
    <br/>
    <div id="footer">
        <p style="text-align: center;">*** NO SE ACEPTAN DEVOLUCIONES ***</p>
        <p style="text-align: justify;">El cambio de mercadería se hara dentro de 7 días, previa presentación de su comprobante de pago.</p>
        <p style="text-align: center;">GRACIAS POR SU COMPRA</p>
    </div>
</div>