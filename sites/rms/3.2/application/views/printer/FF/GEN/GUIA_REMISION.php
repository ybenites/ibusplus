<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>GUIA DE REMISIÓN</title>
        <style type="text/css">
            *{
                padding: 0px;
                margin: 0px;
            }
            body{
                background: yellow;
            }
            #guia{
                /* Tamaño A4*/
                height:1022px;
                width: 788px;
                background: white;
                font-family: "Times New Roman";
                font-size: 12px;
                margin-left: 15px;
                margin-right: 15px;
            }
            .encabezados{
                font-size:15px;
                font-weight: bold;
                text-align: center;
            }
            .store{
                /*color: red;*/
                margin-right: 15px;
                margin-left: 15px;
            }

            
            table {
                width: 758px;
                text-align: left;
                border-collapse: collapse;
                margin: 0 0 1em 0;
                caption-side: top;
                margin-left: 15px;
                margin-right: 15px;
            }
            caption, td, th {
                padding: 0.3em;
            }
            th, td {
                border: 1px solid #000;
                border-right: 1px solid #000;
            }
            th.last, td.last {
                /*border-right: 0;*/
            }
            tfoot th, tfoot td {
                /*border-bottom: 0;*/
                text-align: center;
            }
            th {
                /*width: 25%;*/
            }
            .line{
                margin-top: 15px;
                margin-bottom: 15px;
                border: #000 dashed 1px;
                margin-left: 15px;
                margin-right: 15px;
            }
            .content{
                text-align: center;
                
            }

        </style>
    </head>
    <body>
        
        <div id ="guia">
            <div class="encabezados"><?php echo ("GUIA DE REMISION") . "    " . $seriecorrelative; ?></div>
            <div class="content"><?php echo ("Usuario : ") . "   " . $User->fullName; ?></div>
            <div class="content"><?php echo ("Fecha : ") . "   " . $date; ?></div>
            <br>
            <br>
            <div class="encabezados"><?php echo ("TRANSFERENCIA DE PRODUCTOS"); ?></div>
            <div class="content"><?php echo ("Motivo de: ") . "   " . $moveReference; ?></div>
            <br>
            <br>

            <div class="store"><?php echo ("Almacén de Origen: ") . "   " . $storeOrigen; ?></div>
            <div class="store"><?php echo ("Almacén de Destino: ") . "   " . $storeDestino; ?></div>

            <hr class="line"/>
            
            <div>
                <table>
                    <thead>
                        <tr style="height: 20px; ">
                            <th><?php echo ("Código"); ?></th>
                            <th><?php echo ("Nombre del Producto"); ?></th>
                            <th style="width: 141px; "><?php echo ("Categoria"); ?></th>
                            <th style="width: 50px; "><?php echo ("Lote"); ?></th>
                            <th style="width: 73px; "><?php echo ("Precio Unidad"); ?></th>
                            <th><?php echo ("Cantidad"); ?></th>
                        </tr>
                    </thead>
                    </tbody>
                    <?php $count = 0; ?>
                    <?php foreach ($detail as $a_dt): ?>
                        <tr style="height: 20px; ">
                            <td style="width:100px; " ><?php echo $a_dt['aliasProduct'] ?></td>
                            <td  style="width: 250px; "><?php echo $a_dt['productName'] ?></td>
                            <td style="width: 180px; "><?php echo $a_dt['categoryName'] ?></td>
                            <td style="width: 70px; "><?php echo $a_dt['lotName'] ?></td>
                            <td  style="width: 45px; text-align: right;" ><?php echo $a_dt['unitPrice'] ?></td>
                            <td  style=" text-align: right;" ><?php echo $a_dt['amount']; ?></td>
                            <?php  $count = $count + $a_dt['amount']?>
                        </tr>
                    <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="font-weight: bold; text-align: right; ">Total</td>
                            <td style="text-align:  right; "><?php echo $count;  ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </body>
</html>

