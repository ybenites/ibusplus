<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            *{margin: 0; padding: 0; font-family: 'Arial';}
            .clear{clear: both;}
            div.br-container{
                /*border: black solid 1px;*/
                display: inline-block;
                width: 112px;
                margin-right: 10px;
                float:left;
            }
            .br-margin{
                margin: 2px;
            }
            .br-title{
                text-align: center;
                font-size: 7px;
                font-weight: bold;
            }
            img.br-code{
                /*width: 120px;
                height: 60px;*/
            }
            .br-data{
                float: left;
                width: 50px;
                text-align: center;
            }
            .br-code-number{
                font-weight: bold;
                font-size: 11px;
            }
            .br-date{
                font-size: 7px;
                font-weight: bold;
            }
            .br-price{
                float: left;
                display: inline-block;
                width: 45px;
                text-align: right;
                font-size: 14px;
            }
            .br-container-a > * {
                display: none
            }
            .br-data-margin{
                margin-left: 5px;
            }
            .prueba{
                width: 366px;
            }
        </style>
        <script src="/media/js/jquery-1.8.0.min.js" type="text/javascript"></script>
        <script src="/media/js/jquery.barcode.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $.each($('.barCode'),function(idx,obj){
                    
                    var code = $(obj).data('code').toString();
                    $(obj).barcode(code,'code128',{barHeight:25,showHRI:false,output:'bmp',moduleSize:10});
                });             
            });
        </script>
    </head>
    <body>
        <div class="prueba">
        <?php 
        for($i=0;$i<$cantidad;$i++){
  
        ?>
        <div class="br-container">
            <div class="br-title">
                <div class="br-margin">
                    <?php echo $producto->productName ?>
                </div>
            </div>
            <div class="barCode" data-code="<?php echo $producto->aliasProduct; ?>">
            </div>
            <div class="br-clear"></div>
            <div class="br-data-margin">
                <div class="br-data">
                    <div class="br-code-number"><?php echo $producto->aliasProduct.$i;?></div>
                    <div class="br-date"><?php echo date('d/m/Y') ?></div>
                    <div class="br-clear"></div>
                </div>
                <div class="br-price"><?php echo $producto->productPriceSell; ?></div>
            </div>
        </div>       
        
        
       <?php     //$tmp=$tmp+3;     
          } ?>
            <div class="clear"></div>
        </div>
    </body>
</html>
