<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="/media/images/<?php echo $skin; ?>/favicon.png"/>
        <script type="text/javascript" src="/media/js/jquery.min.js"></script>
        <script type="text/javascript" src="/media/js/jquery.ui.custom.min.js"></script>
        <link type="text/css" href="/media/css/<?php echo $skin;?>/jquery.ui.custom.css" rel="stylesheet" />
        <script type="text/javascript">
            $(document).ready(function(){
                var title='<?php echo $title_dialog; ?>';
                $('#dialog').dialog({
                    autoOpen:true,
                    closeOnEscape:false,
                    modal:true,
                    title:title,
                    resizable:false,
                    close:function(){
                        return false;
                    }
                });
            });
        </script>
        <style type="text/css">
            #dialog div label{
                width: 150px;
                text-align: right;
                display: inline-block;
                float: left;
                margin-right: 4px;
                font-weight: bold;
            }
            #dialog div{
                margin-bottom: 5px;

            }
        </style>
    </head>
    <body>
        <div id="dialog">
            <?php echo $content_dialog; ?>    
        </div>
    </body>
</html>