<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body style="background-color: whitesmoke; font-size: 16px;">
        <div style="background-color: white; border-radius: 4px; font-family: Arial; max-width:600px; margin: 0 auto;">
            <table style="margin: 2px; width: 99%;">
                <tr>
                    <td style="text-align: center;">
                           <img src="http://:website/media/images/:skin/logo.png" style="display: block;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <h3 style="font-style: italic;"><?php echo __("Notificacion de Productos con Minimo Stock"); ?></h3>
                        <p style="text-align: justify; font-size: 0.8em;">
                        <?php echo __("Se le notifica que algunos productos han llegado al minimo de su stock. A continuación se 
                            detallalos productos "); ?>
                        </p>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <p><label><?php echo __("Productos"); ?>:</label> :list_product</p>
                    </td>
                </tr>
            </table>           
            <table style="margin: 2px; width: 99%; color: gray; font-size: 0.7em;">
                <tr>
                    <td style="text-align: justify;">
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <label>Ibusplus -  Diana Paola Miguel Saldaña</label>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>