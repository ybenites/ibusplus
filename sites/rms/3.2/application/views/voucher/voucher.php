<style type="text/css">
    input.helptext{font-style: italic;color: #CCCCCC;}
    div.divDocInfo{font-size: 14px;line-height: 20px;padding: 5px;width: 300px;text-align: center; margin-bottom: 5px;}
    .divTitle h4{font-size:16px; text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important; margin: 10px auto 0;}

    .div_new_voucher{        
        margin-left: auto;
        margin-right: auto;
        width: 1000px;
    }    
    .label_output div *{
        float: none;
    }
    .label_output div label{
        min-width: 90px;
    }
    #cn>div{
        padding: 5px;
    }
    #search_type>div{
        padding-top:5px;
    }
   input.date{line-height: 16px; width: 160px;}
   .label_output{clear: both;margin-bottom: 10px;float: left;}
   label.frmLabel {
        float: left;
        font-weight: bold;
        line-height: 16px;
        width: 180px;
    }
</style>
<script type="text/javascript">
    var url_getAllUserAuthorize="/private/voucher/getAllUserAuthorize";
    var url_getAllStores="/private/voucher/getAllStores";
    var option_template ='<option value="[VALUE]">[LABEL]</option>';
    var frmPopupNewVoucher="<?php echo 'Nuevo Vale' ?>";
    var frmPopupEditVoucher='<?php echo __("Editar Vale"); ?>';
    var url_listVouchers='/private/voucher/listVouchers'+jquery_params;
    var frmPopupVoucherTitle='<?php echo __("Vale"); ?>';
    var msg_warn_cancel = '<?php echo __('¿Está seguro de anular este vale?'); ?>';
    var title_cancel = '<?php echo __("Anular Vale"); ?>';
    var current_store = '<?php echo $currentStore; ?>';
    
    //    var dt_egreso = '<?php echo $RECIBO_EGRESO; ?>';
    //    var dt_ingreso = '<?php echo $RECIBO_INGRESO; ?>';
    $(document).ready(function(){
        $("#btn_new_voucher").button({
            icons:{
                primary: 'ui-icon-plusthick'
            },
            text: true
        });
        /* Datepicker */
        $("#voucherDate").datepicker({
            dateFormat:'<?php echo $jquery_date_format ?>'        
        });
        $("#btn_new_voucher").click(function(){
            $('.frmSubtitle').text(frmPopupNewVoucher);
            $('#typeVoucher').val('');
            $('#idVoucher').val('');
            $('#importVoucher').val('');
            $('#voucherDate').val('<?php echo $fechaActual; ?>');
            $('#authorizaVoucher').val('');
            $('#a_almacen').val('<?php echo $currentStore; ?>');
            $('#conceptVoucher').val('');
            $('input,textarea,select').removeClass('ui-state-error');
            $("#dlg_new_voucher").dialog('open');
           
        });

        $("#dlg_new_voucher").dialog({
            autoOpen: false,                         
            width:410,
            modal:true,
            resizable:false,
            title:frmPopupVoucherTitle,
            buttons:{
                '<?php echo __('Aceptar') ?>':function(){
                    $('#frm_new_voucher').submit();
                },
                '<?php echo __('Cancelar') ?>':function(){                    
                    $('#errorMessages').hide();      
                    $("#idVoucher").val('');
                    frm_new_voucher.currentForm.reset();                    
                    $(this).dialog('close');
                }
            }
        });
        getAllUserAuthorize();
        function getAllUserAuthorize(){
            showLoading=1;
            $.ajax({                
                url:url_getAllUserAuthorize+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{
                }, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#authorizaVoucher').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#authorizaVoucher').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['name'], 'g'));
                            });
                        } else {
                            $('#authorizaVoucher').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        
        getAllStores();
        function getAllStores(){
            showLoading=1;
            $.ajax({                
                url:url_getAllStores+jquery_params,                        
                type:'POST',
                dataType:'json',
                data:{
                }, 
                async:false,
                success:function(response){                    
                    if(evalResponse(response)){
                        if(response.data.length>0){
                            $('#a_almacen').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                if(array['id'] == current_store){
                                    
                                    $('#a_almacen').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['name'], 'g'));
                                    $('#a_almacen').val(array['id']);
                                }else{
                                    $('#a_almacen').append(option_template.replace('[VALUE]', array['id'],'g').replace('[LABEL]',array['name'], 'g'));   
                                }
                            });
                        } else {
                            $('#a_almacen').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                        }
                    }
                }
            });
        }
        
        $('#importVoucher').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        
        var frm_new_voucher=$("#frm_new_voucher").validate({
            rules:{
                typeVoucher:{
                    required:true
                } ,                             
                authorizaVoucher:{
                    required:true
                },                          
                conceptVoucher:{
                    required:true
                },
                importVoucher:{
                    required:true
                }
            },
            messages:{
                typeVoucher:{
                    required:'<?php echo __("Seleccione un tipo de Vale") ?>'
                } ,                             
                authorizaVoucher:{
                    required:'<?php echo __("Seleccione quien le autoriza") ?>'
                },                          
                conceptVoucher:{
                    required:'<?php echo __("Concepto de Vale") ?>'
                },
                importVoucher:{
                    required:'<?php echo __("Monto del vale") ?>'
                }  
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){                
                var data = $("#frm_new_voucher").serialize();
                showLoading = 1;
                $.ajax({
                    url:'/private/voucher/createOrEditVoucher'+jquery_params,
                    data:data,
                    success:function(r){
                        if(evalResponse(r)){
                            $('#vouchers_jqGrid').trigger('reloadGrid');
                            $("#idVoucher").val('');
                            frm_new_voucher.currentForm.reset();
                            $('#dlg_new_voucher').dialog('close');
                        }                     
                    }
                });                
            }
        });
        function myVoucherSum(val, name, record){            
            return parseFloat(val||0)+parseFloat(record.amount);
        }
        var vouchers_jqGrid=$("#vouchers_jqGrid").jqGrid({
            url:url_listVouchers,
            postData:{                
                sd:function(){
                    return $('#sd').val();
                }
            },
            datatype: 'json',
            mtype: 'POST',
            rowNum: 20,
            width:1300,
            height:280,
            rowList: [20,40,60,80,100],
            colNames:[                                
                'idVoucher',                
                '<?php echo __("Fecha Registro") ?>',
                '<?php echo __("Fecha Ingreso") ?>',
                '<?php echo __("Tipo") ?>',       
                '<?php echo __("Autoriza") ?>',                                
                '<?php echo __("Concepto") ?>',
                '<?php echo __("Importe") ?>',                
                '<?php echo __("Almacén") ?>',                
                '<?php echo __("Serie") ?>', 
                '<?php echo __("Correlative") ?>',        
                '<?php echo __("Usuario") ?>',   
                '<?php echo __("Acciones") ?>'                                                                                                                 
            ],
            colModel:[
                {name:'idVoucher',index:'idVoucher',key:true, hidden:true,hidedlg:true},                
                {name:'creationDate',index:'creationDate',width:140,align:'center'},                
                {name:'voucherDate',index:'voucherDate',width:140,align:'center'},                
                {name:'documentType',index:'documentType',width:140,align:'center'},                
                {name:'authorize',index:'authorize',sortable:false,width:100},
                {name:'concept',index:'concept',width:100},   
                {name:'amount',index:'amount',width:50,align:'right',sorttype:'number',formatter:'number', summaryType:myVoucherSum,summaryTpl : '{0}'},              
                {name:'store',index:'store',width:50,width:120,align:'center'},              
                {name:'serie',index:'serie',width:100},
                {name:'correlative',index:'correlative',width:100},
                {name:'usercreate',index:'usercreate',width:100},
                {name:'actions',index:'actions',hidedlg:true,width:70}
            ],
            pager: "#vouchers_jqGrid_pager",
            sortname: 'vo.voucherDate',
            sortable: true,
            grouping: true,
            rownumbers: true,       
            rownumWidth: 40,
            groupingView : {
                groupField : ['documentType'],
                groupColumnShow : [false],
                groupText : ['<b>{0} - ({1})</b>'],
                groupCollapse : false,
                groupOrder: ['asc'],
                groupSummary : [true], 
                showSummaryOnHide:false,
                groupDataSorted : true
            },
            caption: '<?php echo __("Vales"); ?>',
            gridComplete:function(){
                var ids = $('#vouchers_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    var options='';
                    var rd = $("#vouchers_jqGrid").getRowData(ids[i]);
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";      
                    options +=edit+trash;              
                    $("#vouchers_jqGrid").jqGrid('setRowData',ids[i],{actions:options});
                    
                    $('.edit').button({
                        icons:{primary: 'ui-icon-pencil'},
                        text: false
                    });                    
                    $('.trash').button({
                        icons:{primary: 'ui-icon-trash'},
                        text: false
                    });
                }
            }
        });
        $('table#vouchers_jqGrid').on('click','.edit',function(){
            $('.frmSubtitle').text(frmPopupEditVoucher);
            var idv=$(this).attr('rel');
            showLoading = 1;
            $.ajax({
                url:'/private/voucher/findVoucherById'+jquery_params,
                type:'POST',
                dataType:'json',
                data:{idv:idv},
                success:function(response){
                    if(evalResponse(response)){
                        var data=response.data;                        
                        $('.frmSubtitle').text(frmPopupEditVoucher);                      
                        $("#idVoucher").val(data.idVoucher);
                        $("#typeVoucher").val(data.documentType);
                        $("#authorizaVoucher").val(data.idAuthorizes);
                        $("#a_almacen").val(data.idStore);
                        $("#conceptVoucher").val(data.concept);
                        $("#voucherDate").val(data.voucherDate);
                        $("#importVoucher").val(data.amount);
                        $('input,textarea,select').removeClass('ui-state-error');                        
                        $('#dlg_new_voucher').dialog('open');
                    }         
                }
            })
        });
        $('table#vouchers_jqGrid').on('click','.trash',function(){
            var idv=$(this).attr('rel');
            confirmBox(msg_warn_cancel,title_cancel,
            function(response){
                if(response){
                    showLoading = 1;
                    $.ajax({                        
                        url: '/private/voucher/cancelVoucher'+jquery_params,
                        data:{idv:idv},
                        success: function(response)
                        {
                            if(evalResponse(response)){
                                $("#vouchers_jqGrid").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false;
        });
        
        /*--datepicker--*/
        $('#search_input_date,#search_input_range_ini,#search_input_range_end' ).datepicker({dateFormat: '<?php echo $jquery_date_format; ?>'});
        $('.buttonset').buttonset();
        $("#idStore").combobox();
        $('input[name=search_type]').change(function(){
            if($(this).val()=='day'){
                $('div.search_type_range').css('display','none');
                $('div.search_type_date').css('display','block');
            }else{
                $('div.search_type_date').css('display','none');
                $('div.search_type_range').css('display','block');
            }
        }); 
        $("#btn_search").button({
            icons: {
                primary: "ui-icon-search"                
            },
            text:true
        });
        $('#btn_search').click(function(){
            tbd = $('input[name=search_type]:checked').val();
            store = $("#idStore").val();
            sd = $("#search_input_range_ini").val();
            ed = $("#search_input_range_end").val();
            d = $("#search_input_date").val();

            if(store=="0"){
                msgBox('<?php echo __("Seleccione un Almacén"); ?>');     
            }else{
                delete vouchers_jqGrid.getGridParam('postData')['d'];          
                delete vouchers_jqGrid.getGridParam('postData')['sd'];
                delete vouchers_jqGrid.getGridParam('postData')['ed'];
                if(tbd=="day"){
                    vouchers_jqGrid.setGridParam({
                        postData: {
                            store:function(){
                                return store;
                            },
                            d:function(){
                                return d;
                            }
                        }
                    }).trigger("reloadGrid");
                }else{
                    if(tbd=="range"){
                        vouchers_jqGrid.setGridParam({
                            postData: {
                                store:function(){
                                    return store;
                                },
                                sd:function(){
                                    return sd;
                                },
                                ed:function(){
                                    return ed;
                                }
                            }
                        }).trigger("reloadGrid");
                    } 
                }
                $("#vouchers_jqGrid").trigger('reloadGrid');
            }
        });

        //        $("#typeVoucher").change(function () {
        //            $("#typeVoucher option:selected").each(function () {
        //                option = $(this).val();
        //                if(option=='RECIBO_EGRESO'){
        //<?php if ($bAvailableDocumentType): ?>    
        //                                            successDocument = false;
        //                                            fnUpdateVouchers = function(sType){
        //                                                try{
        //                                                    oDocumentType = getCurrentCorrelative(sType);
        //                                                    if(oDocumentType.serie != null)
        //                                                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.serie + '-' + oDocumentType.correlative;
        //                                                    else
        //                                                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.correlative;
        //                               
        //                                                    $('div.divDocInfo')
        //                                                    .removeClass('ui-state-highlight')
        //                                                    .removeClass('ui-state-error')
        //                                                    .addClass('ui-state-highlight')
        //                                                    .html(displayDocument);
        //                                                    successDocument = true;
        //                                                                                                                                                                                                                                                                                                    
        //                                                                                                                                                                                                                                                                                                    
        //                                                }catch(e){
        //                                                    successDocument = false;
        //                                                    $('div.divDocInfo')
        //                                                    .removeClass('ui-state-highlight')
        //                                                    .removeClass('ui-state-error')
        //                                                    .addClass('ui-state-error')
        //                                                    .html('<?php echo __("Documento No Asignado") ?>');
        //                                                }
        //                                            };
        //                                            fnUpdateVouchers(dt_egreso);
        //                                            $('div.divDocInfo').css('display', 'block');
        //                                            $('input[name=typeVoucher]').change(function(){
        //                                                fnUpdateVouchers($(this).val());
        //                                            });                                                
        //<?php else: ?>
        //<?php endif ?>  
        //                }else{
        //<?php if ($bAvailableDocumentType): ?>    
        //                                            successDocument = false;
        //                                            fnUpdateVouchers = function(sType){
        //                                                try{
        //                                                    oDocumentType = getCurrentCorrelative(sType);
        //                                                    if(oDocumentType.serie != null)
        //                                                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.serie + '-' + oDocumentType.correlative;
        //                                                    else
        //                                                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.correlative;
        //                               
        //                                                    $('div.divDocInfo')
        //                                                    .removeClass('ui-state-highlight')
        //                                                    .removeClass('ui-state-error')
        //                                                    .addClass('ui-state-highlight')
        //                                                    .html(displayDocument);
        //                                                    successDocument = true;
        //                                                                                                                                                                                                                                                                                                    
        //                                                                                                                                                                                                                                                                                                    
        //                                                }catch(e){
        //                                                    successDocument = false;
        //                                                    $('div.divDocInfo')
        //                                                    .removeClass('ui-state-highlight')
        //                                                    .removeClass('ui-state-error')
        //                                                    .addClass('ui-state-error')
        //                                                    .html('<?php echo __("Documento No Asignado") ?>');
        //                                                }
        //                                            };
        //                                            fnUpdateVouchers(dt_ingreso);
        //                                            $('div.divDocInfo').css('display', 'block');
        //                                            $('input[name=typeVoucher]').change(function(){
        //                                                fnUpdateVouchers($(this).val());
        //                                            });                                                
        //<?php else: ?>
        //<?php endif ?>    
        //                }
        //                   
        //            });
        //        });

    });
   
</script>

<div>
    <center>    
        <div class="titleTables">
            <span class="title"><?php echo __("Administración de Vales"); ?></span>
        </div>
    </center>
</div>
<div id="cn">
    <div class="div_new_voucher">
        <button id="btn_new_voucher"><?php echo __('Nuevo Vale'); ?></button>    
    </div>    
    <div class="ui-corner-all ui-widget-content" style="margin: 4px; width: 700px; margin: 0 auto;">    
    <div class="padding10">
        <div> 
            <label class="frmLabel"><?php echo __("Almacen") ?>: </label>
            <select id="idStore">
                <option value=""><?php echo __("Seleccione una Opción...") ?></option>                                
                <?php foreach ($array_store as $va): ?>                                   
                    <option value="<?php echo $va->idStore ?>"><?php echo $va->storeShortName ?></option>
                <?php endforeach; ?>
            </select>
        </div>  
        <div class="clear"></div>  
        <div id="search_type">
            <div class="buttonset">
                <label class="frmLabel"><?php echo __("Tipo de Búsqueda") ?>:</label>                    
                <input type="radio" id="search_type_date" name="search_type" value="day" checked />
                <label for="search_type_date"><?php echo __("Día") ?></label>
                <input type="radio" id="search_type_range" name="search_type" value="range"  />              
                <label for="search_type_range"><?php echo __("Entre Fechas") ?></label>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="search_type_date">
                <label class="frmLabel" for="search_input_date"><?php echo __("Fecha") ?>:</label>
                <input class="date" name="search_input_date" id="search_input_date" type="text" value="<?php echo date($date_format_php) ?>"/>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="search_type_range none">
                <label class="frmLabel" for="search_input_range_ini"><?php echo __("Fecha Inicial") ?>:</label>
                <input class="date" name="search_input_range_ini" id="search_input_range_ini" type="text" value="<?php echo date($date_format_php) ?>"/>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="search_type_range none">
                <label class="frmLabel" for="search_input_range_end"><?php echo __("Fecha Final") ?>:</label>
                <input class="date" name="search_input_range_end" id="search_input_range_end" type="text" value=""/>
                <div class="clear"></div>
            </div> 
        </div>
        <div>
            <button id="btn_search"><?php echo __("Buscar"); ?></button>
        </div>
    </div>
    <div class="clear"></div>  
</div>
    <div class="div_grid_voucher">
        <center>
            <div>
                <table id="vouchers_jqGrid"> </table>
                <div id="vouchers_jqGrid_pager"></div>
            </div>
        </center>
    </div>
</div>
<div id="dlg_new_voucher" class="dialog"> 
    <form id="frm_new_voucher">
        <input type="hidden" id="idVoucher" name="idVoucher"/>    
        <fieldset  class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle"></legend>
            <br/>
            <!--     <?php if ($bAvailableDocumentType): ?>
                             <center>
                                 <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix">
                <?php echo __('Seleccione Tipo de Vale'); ?>
                                                         </div>
                                                     </center>-->
            <?php endif; ?>
            <div class="label_output">
                <div>            
                    <label><?php echo __('Almacén') ?>:</label>
                    <select id="a_almacen" name="a_almacen" style="width: 190px;">                    
                    </select>                  
                </div>
                <div>            
                    <label><?php echo __('Tipo') ?>:</label>
                    <select id="typeVoucher" name="typeVoucher" style="width: 190px;">
                        <option value=""><?php echo __('Seleccione el tipo'); ?></option>
                        <option value="<?php echo $RECIBO_INGRESO; ?>"><?php echo __('Ingreso'); ?></option>
                        <option value="<?php echo $RECIBO_EGRESO; ?>"><?php echo __('Egreso'); ?></option>
                    </select>                
                </div>
                <div>            
                    <label><?php echo __('Authoriza') ?>:</label>
                    <select id="authorizaVoucher" name="authorizaVoucher" style="width: 190px;">                    
                    </select>                  
                </div>
                <div>
                    <label><?php echo __("Fecha"); ?>:</label>
                    <input id="voucherDate" name="voucherDate" value="<?php echo $fechaActual; ?>" type="text"/>
                </div>
                <div>            
                    <label><?php echo __('Concepto') ?>:</label>
                    <textarea id="conceptVoucher" name="conceptVoucher" rows="6" cols="22" style="resize:none;"></textarea>                
                </div>
                <div>            
                    <label><?php echo __('Importe') ?>:</label>
                    <input type="text" id="importVoucher" name="importVoucher" />
                </div>
            </div>
        </fieldset>
    </form>
</div>