<style type="text/css">
    .div_newSupplier{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_gridSupplier{        
        padding-top: 10px;
    }
    .label_output{clear: both;margin-bottom: 5px;float: left;}
</style>

<script type="text/javascript">
    /*--url--*/
    var url_createSupplier = '/private/supplier/createorUpdateSupplier'+jquery_params; 
    var url_editSupplier= '/private/supplier/getSupplierById'+jquery_params;
    var url_updateStatus= '/private/supplier/updateStatus'+jquery_params;
    var url_deleteSupplier = '/private/supplier/deleteSupplier'+jquery_params;
    var url_supplier_list= '/private/supplier/listSupplier'+jquery_params;
    var ico_on = '<img src="/media/ico/turn_on_cp.png" />';
    var ico_off = '<img src="/media/ico/turn_off_cp.png" />';
    /*--text--*/       
    var supplier_form_Title = "<?php echo __("Proveedor"); ?>";
    var supplier_newTitle = "<?php echo __("Nuevo Proveedor"); ?>";
    var supplier_editTitle = "<?php echo __("Editar Proveedor"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Proveedor"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este proveedor?"); ?>';
    var supplier_name_required = '<?php echo __("El nombre del Proveedor es obligatorio."); ?>';
    var supplier_contactname_required = '<?php echo __("El nombre del Contacto Legal es obligatorio."); ?>';
    var supplier_code_required = '<?php echo __("El RUC es obligatorio."); ?>';
//    var supplier_product_required = '<?php echo __("El Producto es obligatorio."); ?>';
//    var supplier_address_required = '<?php echo __("La Dirección es obligatorio."); ?>';
//    var supplier_phone_required = '<?php echo __("El Nro de Teléfono es obligatorio."); ?>';
//    var supplier_cellphone_required = '<?php echo __("El Nro de Celular  es obligatorio."); ?>';
//    var supplier_email_required = '<?php echo __("El Correo Electrónico es obligatorio."); ?>';
    
    /*--grid--*/   
    var grid_supplier;

    /*--global--*/
    var g_demo;

    $(document).on("ready", function(){
       
        /*--jqgrid--*/
        grid_supplier = $('#supplier_jqGrid').jqGrid({
            url: url_supplier_list,
            datatype: 'jsonp',
            mtype: 'POST', 
            rowNum: 20,
            width:1200,
            height:'auto',
            rowList: [20,40,60,100],
            colNames:[
                'ID',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Representante Legal") ?>',
                '<?php echo __("Servicio") ?>',
                '<?php echo __("Estado") ?>',
                '<?php echo __("Dirección") ?>',
                '<?php echo __("Teléfono") ?>',
                '<?php echo __("Email") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'idSupplier',index:'idSupplier',key:true,hidden:true,hidedlg:true},
                {name:'name',index:'rms_supplier.name',width:220,align:'left',search:true},
                {name:'contactName',index:'rms_supplier.contactName',width:180,align:'center',search:true},
                {name:'service',index:'service',search:false,width:200,align:'center'},
                {name:'status',index:'status',search:false,width:100,align:'center'},
                {name:'address',index:'address',search:false,width:160,align:'center'},
                {name:'phone',index:'phone',search:false,width:80,align:'center'},
                {name:'email',index:'email',search:false,width:160,align:'center'},
                {name:'actions',index:'actions',width:80,search:false,align:'center'}
            ],
            pager:"#supplier_jqGrid_pager",
            sortname: 'idSupplier',
            sortable: true,            
            sortorder: "asc",
            rownumbers: true,       
            rownumWidth: 30,                      
            caption: '<?php echo __("Lista de Proveedores"); ?>',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(rowdata.status == 1){
                    $("#supplier_jqGrid").jqGrid('setRowData',rowid,{status: '<div class="s_status" data-id="'+rowid+'">'+ico_on+'</div>'});
                } else {
                    $("#supplier_jqGrid").jqGrid('setRowData',rowid,{status:'<div class="s_status" data-id="'+rowid+'">'+ico_off+'</div>'});
                }
            },
            gridComplete:function(){
                var ids = $('#supplier_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                   
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    // trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#supplier_jqGrid").jqGrid('setRowData',ids[i],{actions:edit });
                }
                $('.edit').button({icons:{primary: 'ui-icon-pencil'},text: false});
                $('.trash').button({icons:{primary: 'ui-icon-trash'},text: false});
            }
        });
        
        $("table").on("click",".s_status",function(){
            var id = $(this).data('id'); 
            $.ajax({
                url:url_updateStatus,
                data:{ids:id},
                success: function(response){                            
                    if (response.code == code_success){
                        $("#supplier_jqGrid").trigger("reloadGrid");
                    }else{
                        msgBox(response.msg, response.code);
                    }
                }
            });   
        });
        $("#supplier_jqGrid").jqGrid('navGrid','#supplier_jqGrid_pager',{add:false,edit:false,del:false,search:true,refresh:true});                        
        jQuery("#supplier_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        /*--validate--*/
        $('#s_code,#s_phone,#s_fax,#s_cellphone').setMask();
        
        var validator_Supplier= $("#frmSupplier").validate({
            debug:true,
            rules: {
                s_name: {required: true},
                s_contactName: {required: true},
                s_code: {required: true}
//                s_address: {required: true},
//                s_phone: {required: true},                
//                s_email: {required: true},
//                s_service: {required: true},
//                s_cellphone: {required: true}
            },
            messages:{
                s_name: {required: supplier_name_required},
                s_contactName: {required: supplier_contactname_required},
                s_code: {required: supplier_code_required}
//                s_service: {required: supplier_product_required},
//                s_address: {required: supplier_address_required},
//                s_phone: {required: supplier_phone_required},
//                s_email: {required: supplier_email_required},
//                s_cellphone: {required: supplier_cellphone_required}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                loadingcarga = 1;
                $.ajax({
                    type:"POST",
                    url:url_createSupplier,
                    data:$(form).serialize(),
                    dataType:'jsonp',
                    success: function(response){
                        if(evalResponse(response)){
                            $('#supplier_jqGrid').trigger('reloadGrid');
                            $('#frmPopup').dialog('close');
                        }
                    }
                })
            }
        });

        /*--dialog--*/
        $('#frmPopup').dialog({
            title: supplier_form_Title,
            autoOpen:false,
            autoSize:true,
            modal:true,
            width:550,        
            resizable:false,
            closeOnEscape:true,
            buttons: {
                "<?php echo __("Guardar"); ?>":function(){
                    $('#frmSupplier').submit();
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
            ,beforeClose: function(event, ui) {
                validator_Supplier.currentForm.reset();
                validator_Supplier.resetForm();
                $('input, select, textarea', validator_Supplier.currentForm).removeClass('ui-state-error');
            }
        });
        /*--fn para eliminar un Proveedor--*/
                $('#supplier_jqGrid').on('click','.trash',function(){            
                    var id=$(this).attr("rel");
                    confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                        if(response){                    
                            $.ajax({
                                url:url_deleteSupplier,
                                data:{ids:id},
                                success: function(response){                            
                                    if (response.code == code_success){
                                        $("#supplier_jqGrid").trigger("reloadGrid");
                                    }else{
                                        msgBox(response.msg, response.code);
                                    }
                                }
                            });          
                        }
                    });
                    return false
                });
        $('#supplier_jqGrid').on('click','.edit',function(){     
            $('.frmSubtitle').text(supplier_editTitle);
            var id=$(this).attr("rel");   
            loadingcarga=1;
            $.ajax({
                data:{ids: id},                    
                type:"POST",
                dataType:"jsonp",
                url:url_editSupplier,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $("#supplier_id").val(data.idSupplier);
                        $("#s_name").val(data.name);
                        $("#s_contactName").val(data.contactName);
                        $("#s_code").val(data.code);
                        $("#s_address").val(data.address);
                        $("#s_service").val(data.service);
                        $("#s_phone").val(data.phone);
                        $("#s_fax").val(data.fax);
                        $("#s_cellphone").val(data.cellphone);
                        $("#s_email").val(data.email);
                        $("#frmPopup").dialog('open');                        
                    } else {
                        msgBox(res.msg,res.code);
                    }                                        
                }
            });
            return false
        });
        /*--event click--*/
        $("#btnNewSupplier").button({icons:{primary: 'ui-icon-plusthick'},text: true});
        $('#s_status').buttonset();
        $('#btnNewSupplier').on('click',function(){
            $('.frmSubtitle').text(supplier_newTitle);
            $('#frmPopup').dialog('open');
        });
    });
</script>

<div>
    <center>    
        <div class="titleTables">
            <span class="title"><?php echo __("Administración de Proveedores"); ?></span>
        </div>
    </center>
</div>
<div>
    <div class="div_newSupplier">
        <button id="btnNewSupplier" class="btn"><?php echo __("Nuevo Proveedor"); ?></button>
    </div>
    <div class="div_gridSupplier">
        <center>
            <table id="supplier_jqGrid"></table>
            <div id="supplier_jqGrid_pager"></div>
        </center>
    </div>
</div>
<div id="frmPopup" class="dialog-modal">
    <form id="frmSupplier" method="post">
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div>
                    <label><?php echo __("Nombre"); ?>:</label>
                    <input id="s_name"  name="s_name" value="" type="text" maxlength="150" size="30" />
                </div>
                <div>
                    <label><?php echo __("Contacto Legal"); ?>:</label>
                    <input id="s_contactName" name="s_contactName" value="" type="text" maxlength="150" size="30"/>
                </div> 
                <div>
                    <label><?php echo __("RUC"); ?>:</label>
                    <input id="s_code" name="s_code" value="" type="text" maxlength="150" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Servicio"); ?>:</label>
                    <textarea id="s_service" name="s_service" ></textarea>
                </div>
                <!--                <div>
                                    <label style="margin-top: 5px;"><?php echo __("Estado"); ?>:</label>
                                    <div id="s_status">
                                        <input type="radio" id="s_statusActive" name="s_status" checked="checked" value="1"/><label for="s_statusActive" style="min-width: 20px; padding: 0px;"><?php echo __("Activo"); ?></label>
                                        <input type="radio" id="s_statusInactive" name="s_status"  value="0"/><label for="s_statusInactive" style="min-width: 20px; padding: 0px;"><?php echo __("Inactivo"); ?></label>
                                    </div> 
                                </div>-->
                <div>
                    <label><?php echo __("Dirección"); ?>:</label>
                    <input id="s_address" name="s_address" value="" type="text" maxlength="150" size="30"/>
                </div>
                <div>
                    <label><?php echo __("Teléfono"); ?>:</label>
                    <input id="s_phone" name="s_phone" value="" type="text" maxlength="150" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Fax"); ?>:</label>
                    <input id="s_fax" name="s_fax" value="" type="text" maxlength="150" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Celular"); ?>:</label>
                    <input id="s_cellphone" name="s_cellphone" value="" type="text" maxlength="150" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Email"); ?>:</label>
                    <input id="s_email" name="s_email" value="" type="text" maxlength="150" size="30"/>
                </div>
            </div>
        </fieldset>
        <input type="hidden" name="supplier_id" id="supplier_id" value="0" />
    </form>
</div>

