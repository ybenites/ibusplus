<style type="text/css">
    .div_newAssignmentSeries{
        margin-left: auto;
        margin-right: auto;
        width: 1055px;
        padding-top: 15px;
    }
    .div_gridAssignmentSeries{        
        padding-top: 25px;
    }
    .label_output{clear: both;margin-bottom: 10px;float: left;}
</style>

<script type="text/javascript">

    $.validator.methods.compareLimit = function(value, element, param) {
        return parseInt($(element).val()) > parseInt($('#correlative').val());
    };
    /*--url--*/
    var url_createSeries = '/private/assignmentseries/createorUpdateAssignmentSeries'+jquery_params; 
    var url_editSeries= '/private/assignmentseries/getAssignmentSeriesById'+jquery_params;
    var url_deleteSeries= '/private/assignmentseries/deleteAssignmentSeries'+jquery_params;
    var url_listSeries= '/private/assignmentseries/listAssignmentSeries'+jquery_params;
    var url_listOfficesByCities ='/private/assignmentdocuments/getOfficeByCity'+jquery_params;
    var url_listUserByOffice ='/private/assignmentdocuments/getUserByOffice'+jquery_params;
        
    /*--text--*/
    var series_form_Title = "<?php echo __("Asignación de Serie"); ?>";
    var series_newTitle = "<?php echo __("Nuevo Asignación de Serie"); ?>";
    var series_editTitle = "<?php echo __("Editar Asignación de Serie"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Asignación de Serie"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar esta Asignación de Serie?"); ?>';
    var series_city_required = '<?php echo __("Debe seleccionar una Ciudad."); ?>';
    var series_office_required = '<?php echo __("Debe seleccionar una OFicina."); ?>';
    var series_document_required = '<?php echo __("Debe seleccionar un Tipo de Documento."); ?>';
    var series_serie_required = '<?php echo __("La Serie es obligatorio."); ?>';
    var series_correlativo_required = '<?php echo __("El Correlativo es obligatorio."); ?>';
    var series_lcorrelativo_required = '<?php echo __(" El límite del correlativo es obligatorio"); ?>';
    
    /*--grid--*/  
    var grid_assignmentSeries;
    /*--global--*/ 
    var validatorAssignmentSeries;
    
    $(document).on("ready", function(){

        grid_assignmentSeries = $("#assignmentSeries_jqGrid").jqGrid({
            url: url_listSeries,
            datatype: "jsonp",
            mtype: 'POST',
            height: 'auto',
            scroll: 1,
            rowNum: 20,
            rowList: [20,40,60100],
            width:1200,
            colNames:[
                'ID',
                '<?php echo __("Tipo Documento") ?>',
                '<?php echo __("Serie") ?>',
                '<?php echo __("Correlativo") ?>',
                '<?php echo __("Máximo") ?>',
                '<?php echo __("Oficina") ?>',
                '<?php echo __("Ciudad") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id', hidden:true},
                {name:'documentType',index:'documentType',width:150,align:'center'},
                {name:'serie',index:'serie', width:140,sortable:true},
                {name:'correaltive',index:'correaltive', width:140,sortable:false},
                {name:'maxLimit',index:'maxLimit', width:140,sortable:false},
                {name:'office',index:'office', width:140,align:'center'},
                {name:'city',index:'city',width:180,sortable:true, align:'center'},
                {name:'actions',index:'actions',width:120,sortable:false, align:'center'}
            ],
            pager: "#assignmentSeries_jqGrid_pager",
            viewrecords: true,
            sortname: 'serie',
            grouping:true,
            sortable: true,
            gridview : true,
            rownumbers: true,       
            rownumWidth: 40,
            groupingView : {
                groupField : ['office'],
                groupColumnShow : [false],
                groupText : [
                    '<div class="titleLeft"><b>{0} :: [{1}] '
                        +'<?php echo __("Registro") ?>(s)</b></div>'
                ],
                groupCollapse : false,
                groupOrder: ['asc'] ,
                plusicon: 'ui-icon-plus',
                minusicon: 'ui-icon-minus'
            }, 
            caption: '<?php echo __("Lista de Asignación de Series"); ?>',
            gridComplete:function(){
                var ids = $('#assignmentSeries_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    jQuery("#assignmentSeries_jqGrid").jqGrid('setRowData',ids[i],{actions:edit+trash});
                }
                $('.edit').button({icons:{primary: 'ui-icon-pencil'},text: false});
                $('.trash').button({icons:{primary: 'ui-icon-trash'},text: false});
                
            }
        });
        
        $('#selectOffice').change(function(){
            if($(this).val()!=''){
                $("#assignmentSeries_jqGrid").setGridParam({url:url_listSeries+'&selectOffice='+$(this).val()}).trigger("reloadGrid");
            } else {
                $("#assignmentSeries_jqGrid").setGridParam({url:url_listSeries}).trigger("reloadGrid");
            }
        });
        
        $('#serie,#correlative,#lcorrelative').setMask();
         
        validatorAssignmentSeries = $("#frmAssignmentSeries").validate({
            debug:true,
            rules: {
                idCity: {required: true},
                idOffice: {required: true},
                documentType:{required:true},
                serie: {required: true},
                correlative:{required: true},
                lcorrelative:{required: true}
            },
            messages:{
                idCity: {required: series_city_required},
                idOffice: {required:  series_office_required},
                documentType: {required:  series_document_required},
                serie: {required: series_serie_required },
                correlative:{required: series_correlativo_required},
                lcorrelative:{required: series_lcorrelativo_required}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form) {
                loadingcarga = 1;
                $.ajax({
                    type:"POST",
                    url: url_createSeries,
                    data: $(form).serialize(),
                    dataType:'jsonp',
                    success: function(response){
                        if(evalResponse(response)){
                            $('#assignmentSeries_jqGrid').trigger('reloadGrid');  
                            $('#selectOffice').trigger('change');
                            $('#frmPopup').dialog('close');
                        }
                    }
                })
            }
        });

        $('#idCity').change(function(){
            loadingcarga = 1;
            if($(this).val()!=''){
                $('#idOffice').empty();
                $.ajax({
                    url:url_listOfficesByCities,
                    data: {idCity:$(this).val()},
                    async:false,
                    success: function(response){
                        var msg = '<?php echo __("Sin Resultados") ?>';
                        if(response.code==code_success){
                            var data = response.data;
                            if(data.length>0){
                                msg = '<?php echo __("Seleccione una Opción....") ?>';
                                $('#idOffice').append('<option value="">'+msg+'</option>');
                                $.each(data, function(){
                                    $('#idOffice').append('<option value="'+this.id+'">'+this.value+'</option>');
                                });
                            } else {
                                $('#idOffice').append('<option value="">'+msg+'</option>');
                            }
                        }else{
                            $('#idOffice').append('<option value="">'+msg+'</option>');
                            msgBox(response.msg, '<?php echo __('Aviso') ?>');
                        }
                    }
                });
            } else{
                $('#idOffice option[value!=""]').remove();
            }
        });
        $('#frmPopup').dialog({
            title:series_form_Title,
            autoOpen:false,
            width:400,
            height:300,
            autoSize:true,
            modal:true,
            resizable:false,
            closeOnEscape:true,
            beforeClose: function(event, ui) {
                $('#idOffice').empty();
                $('#idOffice').append('<option value=""><?php echo __("Seleccione una Opción...") ?></option>');
                validatorAssignmentSeries.currentForm.reset();
                validatorAssignmentSeries.resetForm();
                $('input, select, textarea', validatorAssignmentSeries.currentForm).removeClass('ui-state-error');
            },
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){
                    $("#frmAssignmentSeries").submit();
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
        });    
        $('#assignmentSeries_jqGrid').on('click','.edit',function(){     
            $('.frmSubtitle').text(series_editTitle);
            var id=$(this).attr("rel");
            loadingcarga = 1;
            $.ajax({
                type:"POST",
                url:url_editSeries,
                data:{ids: id},
                dataType: 'jsonp',
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $('#frmPopup').dialog('open');
                        $("#idAssignmentSeries").val(data.idAssignmentSeries);
                        $("#idCity").val(data.idCity);
                        $("#idOffice").val(data.idOffice);
                        $("#serie").val(data.serie);
                        $("#correlative").val(data.correlative);
                        $("#documentType").val(data.documentType);
                        $("#lcorrelative").val(data.sequence_max_value);
                        previusNumSeriesValue = data.correlative;
                    } else {
                        msgBox(res.msg,res.code);
                    }
                }
            });

        });
                
        $('#assignmentSeries_jqGrid').on('click','.trash',function(){ 
            var id=$(this).attr("rel");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){
                    $.ajax(
                    {
                        type: "POST",
                        url: url_deleteSeries,
                        data:{ids: id},
                        success: function(response)
                        {
                            if (response.code == code_success)
                            {
                                $("#assignmentSeries_jqGrid").trigger("reloadGrid");
                                $("#assignmentSeries_jqGrid").setGridParam({url:url_listSeries+'&selectOffice='+$('#selectOffice').val()}).trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code);
                            }
                        }
                    });          
                }
            });
            return false;
        });
        /*--event click--*/
        $('#btnNewAssignment').button({icons:{primary: 'ui-icon-plusthick'},text:true});
        $('#btnNewAssignment').on('click',function(){
            $('.frmSubtitle').text(series_newTitle);
            $('#frmPopup').dialog('open');
        });
    });
</script>

<div>
    <center>
        <div class="titleTables"><?php echo __("Asignación de Series"); ?></div>
    </center>
</div>
<div class="clear"></div>  
<div>
    <div class="div_newAssignmentSeries">
        <button id="btnNewAssignment" type="button"><?php echo __("Asignación de Serie") ?></button>
    </div>
</div>
<br/>
<div class="ui-corner-all ui-widget-content" style="margin: 5px; width: 880px; margin: 0 auto;">
    <div class="label_output">
        <div>
            <label for="selectOffice"><?php echo __("Oficina"); ?>:</label>
            <?php echo Helper::comboSimple($aOffice, 'idOffice', 'name', 'selectOffice', null, 'Todas'); ?>
        </div>
    </div>
    <div class="clear"></div>  
</div>
<div>
    <div class="div_gridAssignmentSeries">
        <center>
            <table id="assignmentSeries_jqGrid"></table>
            <div id="assignmentSeries_jqGrid_pager"></div>
        </center>  
    </div>
</div>

<div id="frmPopup" class="dialog-modal">
    <form id="frmAssignmentSeries" method="post" action="/">
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div>
                    <label for="idCity"><?php echo __("Ciudad"); ?>:</label>
                    <?php echo Helper::comboSimple($aCity, 'idCity', 'name', 'idCity'); ?>
                </div>
                <div>
                    <label for="idOffice"><?php echo __("Oficina"); ?>:</label>
                    <select id="idOffice" name="idOffice">
                        <option value=""><?php echo __("Seleccione una Opción....") ?></option>
                    </select>
                </div>
                <div>
                    <label for="documentType"><?php echo __("Tipo Documento"); ?>:</label>
                    <select id="documentType" name="documentType">
                        <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                        <?php foreach ($adocuments as $v) { ?>
                            <option value="<?php echo $v['value'] ?>"><?php echo $v['display'] ?></option>
                        <?php } ?>
                    </select>  
                </div>
                <div>
                    <label for="serie"><?php echo __("Serie"); ?>:</label>
                    <input id="serie" name="serie" type="text" maxlength="10" size="15" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Correlativo"); ?>:</label>
                    <input id="correlative" name="correlative" value="" type="text" maxlength="9" size="15" alt="number" />
                </div>

                <div>
                    <label><?php echo __("Límite Correlativo"); ?>:</label>
                    <input id="lcorrelative" name="lcorrelative" value="" type="text" maxlength="20" size="25" alt="number" />
                </div>

                <input type="submit" class="none"/>
            </div>
        </fieldset>
        <input type="hidden" name="idAssignmentSeries" id="idAssignmentSeries" value="0" />
    </form>
</div>
