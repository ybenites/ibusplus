<style type="text/css">
    div.divTable{width: 350px;margin: 0 auto;}
    form label.labelForm{width: 70px;float: left;line-height: 22px;font-weight: bold;height: 22px;margin-right: 5px;}
    form label.labelForm2{width: 250px;float: left;line-height: 22px;font-weight: bold;height: 22px;margin-right: 5px;}
    div.listDocuments{float: left;}    
    div.listDocuments ul li{margin-bottom: 5px;}
    div.listDocuments ul li label.ui-button{width: 250px;}
    div.listDocuments ul li:last-child{margin-bottom: 0;}
    div.formRow{margin: 5px 0;}
    .div_newAssigmentDocuments{
        margin-left: auto;
        margin-right: auto;
        width: 1055px;
        padding-top: 15px;
    }
    .div_gridAssignmentDocuments{        
        padding-top: 25px;
    }
    .label_output{clear: both;margin-bottom: 10px;float: left;}
</style>

<script type="text/javascript">
    /*--url--*/
    var url_createDocuments = '/private/assignmentdocuments/createorUpdateDocuments'+jquery_params; 
    // var url_editDocuments= '/private/assignmentdocuments/getDocumentsByOffice'+jquery_params;
    var url_deleteDocuments= '/private/assignmentdocuments/deleteAssignmentDocument'+jquery_params;
    var url_listDocuments= '/private/assignmentdocuments/listAssignmentDocuments'+jquery_params;
    var url_listOfficesByCities ='/private/assignmentdocuments/getOfficeByCity'+jquery_params;
    var url_listUserByOffice ='/private/assignmentdocuments/getUserByOffice'+jquery_params;
    var url_listDocumentsByOffice = '/private/assignmentdocuments/getDocumentsByOffice'+jquery_params;
    /*--text--*/       
    var documents_form_Title = "<?php echo __("Asignación de Documentos"); ?>";
    var documents_newTitle = "<?php echo __("Nuevo Asignación de Documentos"); ?>";
    var documents_editTitle = "<?php echo __("Editar Asignación de Documentos"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Asignación de Documentos"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar esta Asignación de Documentos?"); ?>';
    var documents_city_required = '<?php echo __("La Ciudad es obligatoria"); ?>';
    var documents_office_required = '<?php echo __("La Oficina es obligatoria"); ?>';
    var documents_user_required = '<?php echo __("El Usuario es obligatoria"); ?>';
    
    var validatorAssignmentDocument;
    
    $(document).on("ready", function(){   
 
        grid_AssignmentDocument = $('#assignmentDocuments_jqGrid').jqGrid({
            url:url_listDocuments,            
            datatype: "jsonp",
            mtype: 'POST',
            height: 'auto',
            scroll: 1,
            rowNum: 20,
            rowList: [25,50,100],
            width:1200,
            colNames:[
                'ID',
                '<?php echo __("Ciudad") ?>',
                '<?php echo __("Oficina") ?>',
                '<?php echo __("Usuario") ?>',
                '<?php echo __("Tipo de Documento") ?>',
                '<?php echo __("Serie") ?>',
                '<?php echo __("Correlativo") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'id',index:'id', hidden:true},
                {name:'city',index:'city',width:150,sortable:true},
                {name:'office',index:'office', width:140},
                {name:'user',index:'user', width:140},
                {name:'documentType',index:'documentType',width:140},
                {name:'serie',index:'serie', width:140,sortable:true},
                {name:'correaltive',index:'correaltive', width:140,sortable:false},
                {name:'actions',index:'actions',width:150,sortable:false}
            ],
            pager: "#assignmentDocuments_jqGrid_pager",
            viewrecords: true,
            sortname: 'serie',
            grouping:true,
            sortable: true,
            gridview : true,
            rownumbers: true,       
            rownumWidth: 40,
            groupingView : {
            groupField : ['city'],
            groupColumnShow : [false],
            groupText : [
                '<div class="titleLeft"><b>{0} :: [{1}] '
                    +'<?php echo __("Registro") ?>(s)</b></div>'
            ],
            groupCollapse : false,
            groupOrder: ['asc'] ,
            plusicon: 'ui-icon-plus',
            minusicon: 'ui-icon-minus'
        }, 
            caption: '<?php echo __("Asignación de Documentos") ?>',
            gridComplete:function(){
                var ids = $('#assignmentDocuments_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                 
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    jQuery("#assignmentDocuments_jqGrid").jqGrid('setRowData',ids[i],{actions:trash});
                }    
                $('.trash').button({icons:{primary: 'ui-icon-trash'},text: false});
            }
        });
        $('#selectOffice').change(function(){
            if($(this).val()!=''){
                $("#assignmentDocuments_jqGrid").setGridParam({url:url_listDocuments+'&selectOffice='+$(this).val()}).trigger("reloadGrid");
            } else {
                $("#assignmentDocuments_jqGrid").setGridParam({url:url_listDocuments}).trigger("reloadGrid");
            }
        });
        validatorAssignmentDocument = $('#frmAssignmentDocument').validate({
            debug:true,
            rules: {
                idCity: {required: true},
                idOffice: {required: true},
                idUser:{required: true},
                'checkDocuments[]':{required: true,minlength: 1}
            },
            messages:{
                idCity: {required: documents_city_required},
                idOffice: {required: documents_office_required},
                idUser:{required: documents_user_required},
                'checkDocuments[]':{
                    required: '<?php echo __("Documento es obligatorio") ?>',
                    minlength: '<?php echo __("Seleccione al menos un Tipo Documento") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight:function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form){                
                sDataUrl = $(form).serialize();
                $.ajax({
                    url:url_createDocuments,
                    data: sDataUrl,
                    dataType: 'json',
                    success: function(response){     
                        if(response.msg == 'OK'){
                            if(response.repeated == true){
                                msgBox(
                            '<?php echo __("No puede asignar el mismo tipo de documento a un usuario") ?>',
                            '<?php echo __("Documento Repetido") ?>');
                            }
                            else{
                                $("#assignmentDocuments_jqGrid").trigger("reloadGrid");
                                $('#frmPopup').dialog('close');
                            }
                        }else{
                            msgBox(
                            '<?php echo __("No se pudo guardar la asignacion") ?>',
                            '<?php echo __("ERROR") ?>'
                        );
                        }
                    }
                });
            }
        });
        
        $('#frmAssignmentDocument').get(0).reset();
        $('#assignmentDocuments_jqGrid').on('click','.trash',function(){                
            var idAssignmentDocument=$(this).attr('rel');
            var dataString='idAssignmentDocument='+idAssignmentDocument;
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){
                    $.ajax({
                        url: url_deleteDocuments,
                        data:dataString,
                        success: function(response) {
                            if (response.code == code_success) {
                                $("#assignmentDocuments_jqGrid").trigger("reloadGrid");
                                $("#assignmentDocuments_jqGrid").setGridParam({url:url_assignment_documents+'&selectOffice='+$('#selectOffice').val()}).trigger("reloadGrid");
                            } else {
                                msgBox(response.msg, '<?php echo __('ERROR') ?>');
                            }
                        }
                    });          
                }
            });
            return false;
        });
        $('#idCity').change(function(){
            if($(this).val()!=''){
                $.ajax({
                    url:url_listOfficesByCities,
                    data: {idCity:$(this).val()},
                    dataType: 'json',
                    success: function(response){                    
                        if (response.code == code_success) {
                            var option = '<option value=""><?php echo __("Seleccione una Opción...") ?></option>';
                            $.each(response.data, function(){
                                option += '<option value="'+this.id+'">'+this.value+'</option>';
                            });
                            $('#idOffice').html(option);
                        } else {
                            msgBox(response.msg,'<?php echo __("Aviso") ?>');
                        }
                    }
                });
            } else {
                $('#idOffice option[value!=""]').remove();
                $('#idUser option[value!=""]').remove();
            }
        });
        $('#idOffice').change(function(){
            if($(this).val()!=''){
                $.ajax({
                    url:url_listUserByOffice,
                    data: {idOffice:$(this).val()},
                    dataType: 'json',
                    success: function(response){
                        if (response.code == code_success) {
                            var option = '<option value=""><?php echo __("Seleccione una Opción...") ?></option>';
                            $.each(response.data, function(){
                                option += '<option value="'+this.id+'">'+this.value+'</option>';
                            });
                            $('#idUser').html(option);
                        } else {
                            msgBox(response.msg,'<?php echo __("Aviso") ?>');
                        }
                    }
                });
            } else{
                $('#idUser option[value!=""]').remove();
            }
        });
        
        $('#idUser').change(function(){
            if($(this).val()!=''){
                var params = new Object();
                params.idOffice = $('#idOffice').val();
                params.idUser = $(this).val();
                $.ajax({
                    url:url_listDocumentsByOffice,
                    data: params,
                    dataType: 'json',
                    success: function(response){
                        $('.listDocuments').html('');
                        try{
                            if (response.code!=null && response.code == code_success) {
                                var filas ='' ;                       
                                $.each(response.data, function(i,value){
                                    var checked = value.checked==1?'checked':'';
                                    filas += '<li>';
                                    filas += '     <input type="checkbox" '+checked
                                        +' value='+value.id+' id="checkDocuments'+i
                                        +'" name="checkDocuments[]"/>';
                                    filas +=  '    <label for="checkDocuments'+i
                                        +'">'+value.value+'</label>';
                                    filas += '</li>';
                                });                        
                                $('.listDocuments').html('<ul>'+filas+'</ul>');
                                $('.listDocuments ul li input').button();
                            } else {
                                msgBox(response.msg,'<?php echo __("Aviso") ?>');
                            }
                        }catch(e){
                        
                        }
                    }
                });
            } else{
                $('.listDocuments').html('');
            }
        });

        $('#btnNewAssignment').button({icons:{primary: 'ui-icon-plusthick'}});
        $('#btnNewAssignment').on('click',function(){
            $('.frmSubtitle').text(documents_newTitle);
            $('#frmPopup').dialog('open');
        });
        $('#selectOffice').change(function(){            
            $("#assignmentDocuments_jqGrid").setGridParam({
                url:url_listDocuments+'&selectOffice='+$(this).val()
            }).trigger("reloadGrid");
        });
        
        $('#frmPopup').dialog({            
            title:documents_form_Title,
            autoOpen:false,
            width:'auto',
            height:'auto',
            autoSize:true,
            modal:true,
            resizable:false,
            closeOnEscape:true,
            open: function(event, ui) {
                $('#idCity').val(['']);
                $('#idOffice').empty();
                $('#idOffice').append('<option value=""><?php echo __("Seleccione una Opción...") ?></option>');
                $('#idUser').empty();
                $('#idUser').append('<option value=""><?php echo __("Seleccione una Opción...") ?></option>');
                $('.listDocuments').html('');
                validatorAssignmentDocument.currentForm.reset();
                validatorAssignmentDocument.resetForm();
            },
            beforeClose: function(event, ui) {
                $('#idCity').val(['']);
                $('#idOffice').empty();
                $('#idOffice').append('<option value=""><?php echo __("Seleccione una Opción...") ?></option>');
                $('#idUser').empty();
                $('#idUser').append('<option value=""><?php echo __("Seleccione una Opción...") ?></option>');
                $('.listDocuments').html('');
                validatorAssignmentDocument.currentForm.reset();
                validatorAssignmentDocument.resetForm();
                $('input, select, textarea', validatorAssignmentDocument.currentForm).removeClass('ui-state-error');
            },
            buttons: {
                "<?php echo __("Aceptar") ?>":function(){
                    if(!validatorAssignmentDocument.form()){
                        return false;
                    }
                    $("#frmAssignmentDocument").submit();
                }
                ,"<?php echo __("Cancelar") ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
        });
    });
</script>
<div>
    <center>
        <div class="titleTables"><?php echo __("Asignación de Documentos") ?></div>
    </center>
</div>
<div class="clear"></div>  
<div>
    <div class="div_newAssigmentDocuments">
        <button id="btnNewAssignment" type="button"><?php echo __("Asignación de Documentos") ?></button>
    </div>
</div>
<br/>
<div class="ui-corner-all ui-widget-content" style="margin: 5px; width: 880px; margin: 0 auto;">    
    <div class="label_output">
        <div>
            <label for="selectOffice"><?php echo __("Oficina"); ?>:</label>
            <?php echo Helper::comboSimple($aOffice, 'idOffice', 'name', 'selectOffice', null, 'Todas'); ?>
        </div>
    </div>
    <div class="clear"></div>  
</div>
<br/>
<div><div class="div_gridAssignmentSeries">
        <center>
            <table id="assignmentDocuments_jqGrid"></table>
            <div id="assignmentDocuments_jqGrid_pager"></div>
        </center>
    </div>
</div>
<div id="frmPopup" class="none">
    <form id="frmAssignmentDocument" method="post" action="" class="frm">
        <fieldset>
            <legend class="frmSubtitle"></legend>
            <div class="divTable">
                <div class="formRow">
                    <label for="idCity" class="labelForm"><?php echo __("Ciudad") ?>:</label>
                    <?php echo Helper::comboSimple($aCitys, 'idCity', 'name', 'idCity') ?>
                </div>
                <div class="formRow">
                    <label for="idOffice" class="labelForm"><?php echo __("Oficina") ?>:</label>
                    <select id="idOffice" name="idOffice">
                        <option value=""><?php echo __("Seleccione una Opción...") ?></option>
                    </select>
                </div>
                <div class="formRow">
                    <label for="idUser" class="labelForm"><?php echo __("Usuario") ?>:</label>
                    <select id="idUser" name="idUser">
                        <option value=""><?php echo __("Seleccione una Opción...") ?></option>
                    </select>
                </div>           
                <div class="formRow">
                    <label class="labelForm2"><?php echo __("Documentos Asignados") ?>:</label>
                    <div class="listDocuments">

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </fieldset>
        <input type="hidden" name="idAssignmentDocument" id="idAssignmentDocument" value="0" />
    </form>
</div>
