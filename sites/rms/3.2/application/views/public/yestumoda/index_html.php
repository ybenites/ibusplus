<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Y.E.S. - By Catherine Design</title>
    <link href='/homepage/<?php echo $skin; ?>/css/estilos.css' rel='stylesheet' type='text/css' />
    <script src="/homepage/<?php echo $skin; ?>/js/lib/jquery.min.js" type="text/javascript"></script>
    <script src="/homepage/<?php echo $skin; ?>/js/jquery.queryloader2.js" type="text/javascript"></script>
    <script src="/homepage/<?php echo $skin; ?>/js/script.js" type="text/javascript"></script>      
    <link rel="stylesheet" href="/homepage/<?php echo $skin; ?>/css/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/homepage/<?php echo $skin; ?>/css/nivo-slider.css" type="text/css" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Satisfy' rel='stylesheet' type='text/css'>
    <script type='text/javascript' src='/homepage/<?php echo $skin; ?>/js/jquery.queryloader2.js'></script>
	<script type="text/javascript" src="/homepage/<?php echo $skin; ?>/js/jquery.nivo.slider.js"></script>
	<script type="text/javascript">$(window).load(function() {$('#slider').nivoSlider();});</script>

</head>

<body>

<div class="d0">
  <div id="contenedor1">
    <div id="c1logo">
    	<img class="img_peque" src="/homepage/<?php echo $skin; ?>/images/logo.png" />
    </div>
    <div id="c1menu">
        <ul id="button">
            <li><a href="/homepage/<?php echo $skin; ?>/index.html" class="activo">Inicio</a></li>
            <li><a href="/homepage/<?php echo $skin; ?>/quienesomos.html" >Quienes Somos</a></li>
            <li><a href="/homepage/<?php echo $skin; ?>/temporadas.html" >Temporadas</a></li>
            <li><a href="/homepage/<?php echo $skin; ?>/productos.html" >Productos</a></li>
            <li><a href="/homepage/<?php echo $skin; ?>/contactenos.html">Cont&aacute;ctenos</a></li>
        </ul>
    </div>
  </div>
  
  <div id="contenedor2">
  		<div id="slider" class="nivoSlider">
        	        <img src="/homepage/<?php echo $skin; ?>/images/slide01.jpg" />   
                    <img src="/homepage/<?php echo $skin; ?>/images/slide02.jpg" />
                    <img src="/homepage/<?php echo $skin; ?>/images/slide03.jpg" />
                    <img src="/homepage/<?php echo $skin; ?>/images/slide04.jpg" />
        </div>
  </div>
  
  <div id="contenedor3">
    <div id="c2ima1">
    	<a href="productos.html"><img  class="img_peque" src="/homepage/<?php echo $skin; ?>/images/thumb01.jpg" /></a>     
    </div>
    <div id="c2ima2">
    	<a href="productos.html"><img  class="img_peque" src="/homepage/<?php echo $skin; ?>/images/thumb02.jpg" /></a>
    </div>
    <div id="c2ima3">
    	<a href="temporadas.html"><img  class="img_peque" src="/homepage/<?php echo $skin; ?>/images/thumb03.jpg" /></a>
    </div>  
  </div>
  
  <div id="contenedor4">
    <div id="c4logoPeq">
    	<img  class="img_peque" src="/homepage/<?php echo $skin; ?>/images/logo.png" />
    </div>
    <div id="c4copyrig">
    <p class="letracopy">Copyright &copy; 2012 YESPERU.COM Todos los Derechos Reservados<br />DISE&Ntilde;O WEB: <a href="http://www.pyrusstudio.com" target="new">www.pyrusstudio.com</a> / Sistema Web:<a href="http://www.pyrusstudio.com" target="new">www.ibusplus.com</a> </p>
    
    </div>
    <div id="c4fb">
        <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FYES%2F285063168262089&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:90px; height:21px;" allowTransparency="true"></iframe>
    </div>
        
    <div>
    </div>
    <div id="f4tw">
        <a href="https://twitter.com/share" class="twitter-share-button" data-url="https://twitter.com/yes_moda" data-text="Y.E.S. - By Catherine Design" data-via="carlosa34528384" data-lang="es">Twittear</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>
  </div>
</div>
</body>
</html>
</html>