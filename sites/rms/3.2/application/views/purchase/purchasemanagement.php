<style type="text/css">
    .div_gridPurchase{        
        padding-top: 10px;
    }
    .label_output{clear: both;margin-bottom: 5px;float: left;}
</style>

<script type="text/javascript">
    /*--url--*/
    var url_cancelPurchase = '/private/purchasemanagement/deletePurchase'+jquery_params;
    var url_purchase_list= '/private/purchasemanagement/getListPurchase'+jquery_params;

    /*--text--*/       
    var cancelSubTitle = "<?php echo __("Cancelar Compra"); ?>";
    var confirmCancelMessage = '<?php echo __("¿Está seguro de cancelar esta Compra?"); ?>';
    
    /*--grid--*/   
    var grid_purchase;

    $(document).on("ready", function(){
        /*--jqgrid--*/
        grid_purchase = $('#purchase_jqGrid').jqGrid({
            url: url_purchase_list,
            datatype: 'jsonp',
            mtype: 'POST', 
            rowNum: 20,
            scroll: 1,
            width:1200,
            height:'auto',
            rowList: [20,40,60,100],
            colNames:[
                'ID',
                'document',
                '<?php echo __("Nro Documento") ?>',
                '<?php echo __("Moneda") ?>',
                '<?php echo __("Proveedor") ?>',
                '<?php echo __("Tipo de Pago") ?>',             
                '<?php echo __("SubTotal") ?>',
                '<?php echo __("I.G.V. (18%)") ?>',
                '<?php echo __("Total") ?>',
                '<?php echo __("Estado") ?>',
                //'<?php echo __("Opción") ?>'
            ],
            colModel:[
                {name:'ID',index:'ID',key:true,hidden:true,hidedlg:true},
                {name:'document',index:'document',key:true,hidden:true,hidedlg:true},
                {name:'nrodocument',index:'nrodocument',width:100,align:'center'},
                {name:'paymentType',index:'paymentType',search:false,width:80,align:'center'},
                {name:'supplier',index:'supplier',search:false,width:200,align:'center'},
                {name:'currency',index:'currency',search:false,width:80,align:'center'},
                {name:'subtotal',index:'subtotal',search:false,width:100,align:'center'},
                {name:'tax',index:'tax',search:false,width:100,align:'center'},
                {name:'total',index:'total',search:false,width:100,align:'center'},
                {name:'status',index:'status',search:false,width:100,align:'center'},
                //{name:'actions',index:'actions',width:80,search:false,align:'center'}
            ],
            pager:"#purchase_jqGrid_pager",
            sortname: 'ID',
            sortable: true,            
            sortorder: "asc",
            rownumbers: true,       
            rownumWidth: 20, 
            viewrecords: true,
            grouping:true,
            sortable: true,
            gridview : true,
            rownumbers: true,       
            rownumWidth: 40,
            groupingView : {
            groupField : ['document'],
            groupColumnShow : [false],
            groupText : [
                '<div class="titleLeft"><b>{0} :: [{1}] '
                    +'<?php echo __("Registro") ?>(s)</b></div>'
            ],
            groupCollapse : false,
            groupOrder: ['asc'] ,
            plusicon: 'ui-icon-plus',
            minusicon: 'ui-icon-minus'
            },
            caption: '<?php echo __("Lista de Ordenes de Compra"); ?>',
            gridComplete:function(){
                var ids = $('#purchase_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                   
                    cancel = "<a class=\"cancel\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Cancelar'); ?>\" ><?php echo __('Cancelar'); ?></a>";
                    $("#purchase_jqGrid").jqGrid('setRowData',ids[i],{actions:cancel });
                }
                $('.cancel').button({icons:{primary: 'ui-icon-cancel'},text: false});
                 $('.cancel').click(function(){
            var id=$(this).attr("rel");
            confirmBox(confirmCancelMessage,cancelSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_cancelPurchase,
                        data:{idp:id},
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchase_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        });
            }
        });
        
  
        $("#purchase_jqGrid").jqGrid('navGrid','#purchase_jqGrid_pager',{add:false,edit:false,del:false,search:true,refresh:true});                        
        jQuery("#purchase_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});

    });
</script>

<div>
    <center>    
        <div class="titleTables">
            <span class="title"><?php echo __("Administración de Compras"); ?></span>
        </div>
    </center>
</div>
<div>
    <div class="div_gridPurchase">
        <center>
            <div>
            <table id="purchase_jqGrid"></table>
            <div id="purchase_jqGrid_pager"></div>
            </div>
        </center>
    </div>
</div>

