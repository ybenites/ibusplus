<style type="text/css">
    .divTitle h4{font-size:16px;text-transform: uppercase;line-height: 30px;alignment-adjust: central}
    .divTitle,.divContent{min-width: 870px;width: 870px!important;}
    .divTitle,.divContent{  margin-right: auto; margin-left: auto; }
    div.search_type{float: left;}
    .divSearch{width: 165px;}
    .divSearch label{font-weight: bold;}
    .buttonSearch{margin-top: 10px;}
    hr.hr_separate{border-top-width: 3px;border-top-style: dashed;border-bottom: none;border-left: none;border-right: none;}
    .ui-widget-content .search_supplier_wrapper{background-image: none;margin-right: 5px;}
    .search_supplier_wrapper, .search_supplier_wrapper span,
    .search_product_wrapper, .search_product_wrapper span,
    .search_supplier_wrapper input{float: left;}
    .search_product_wrapper input{float: left;}
    .divLine.divLineShort{width:310px;}
    .ui-widget-content .search_supplier_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    input[type="text"], input[type="password"], textarea, select {padding: 5px;}
    input.serie{width: 80px;text-align: right;}
    input.correlative{width: 135px;text-align: right;}
    input.helptext{font-style: italic;color: #CCCCCC;}
</style> 
<script type="text/javascript">
    
    /*--url--*/
    var url_searchPurchaseOrder = '/private/billingpurchase/getPurchaseOrder'+jquery_params;
    var url_createPurchase = '/private/billingpurchase/createPurchase'+jquery_params;
    /*--text--*/       
    var msgConfirmRegister = "<?php echo __("¿Desea confirmar el registro de Compras?"); ?>";
    var titleConfirmRegister = "<?php echo __("Registro de Compra"); ?>";
    
    /*--grid--*/   
    var tblPurchaseOrderDetail;
    
    /*--global--*/
    var arrayDetails = new Object();
    var iCountItem = 1;
 
    
    $(document).on("ready", function(){
        $('.buttonsContainer').buttonset();
        $('#search').button({icons:{primary: 'ui-icon-search'}});
        $('#serie,#correlative').setMask();
        
        //JqGrid del Detalla de la Orden de Compra
        
        tblPurchaseOrderDetail = $('#purchaseOrderDetail_jqGrid').jqGrid({
            height: '100%',                        
            width:'840',
            datatype: 'local',
            sortable: false,
            colNames:[                
                '<?php echo __("Descripción") ?>',
                '<?php echo __("Unidad") ?>',
                '<?php echo __("Precio Unitario") ?>',                
                '<?php echo __("Cantidad") ?>',
                '<?php echo __("SubTotal") ?>',
            ],
            colModel:[   
                {name:'description',index:'description',width:240},
                {name:'productUnitMeasures',index:'productUnitMeasures',width:100, align: 'center'},
                {name:'amountUnit',index:'amountUnit', width:100, align: 'right'},                
                {name:'quantity',index:'quantity',width:100, align: 'right'},
                {name:'totalAmount',index:'totalAmount', width:100, align: 'right'},
            ],
            viewrecords:true,
            sortname:'description',
            gridview : true,
            rownumbers: true,      
            rownumWidth: 40,            
            footerrow:true,
            gridComplete:function(){
                try{
                    fPreTotal= 0;
                    $.each(arrayDetails, function(){
                        fPreTotal += parseFloat(this.preSubTotal);
                    })
                    
                    var aGridData = tblPurchaseOrderDetail.getRowData(),
                    fTotalAmountSum=0;
                    $.each(aGridData,function(i, oRowData){                    
                        fTotalAmountSum += parseFloat(oRowData.totalAmount);                    
                    });
                    
                    tblPurchaseOrderDetail.footerData('set',{quantity:'<?php echo __("TOTAL") ?>',totalAmount:fTotalAmountSum.toFixed(2)});
                  
                }catch(e){
                   
                }
            }
        });
        $('#savePurchase').button({
            icons:{
                primary: 'ui-icon-disk'
            }
        });
        $('#search').click(function(){
            purchaseOrder();
        });
        $("#savePurchase").on('click',function(e){
            e.preventDefault();
            var arrayDataGrid = $("#purchaseOrderDetail_jqGrid").jqGrid("getRowData");
            if(arrayDataGrid.length>0){
                confirmBox(msgConfirmRegister,titleConfirmRegister,function(event){
                    if(event){
                        var dataPurchase = new Object();
                        dataPurchase.headerPurchase = $("#frm_purchase").serializeObject();
                        dataPurchase.detailPurchase = arrayDetails;
                        showLoading=1;
                        $.ajax({
                            url:url_createPurchase,
                            type:"POST",
                            dataType:'json',
                            data: dataPurchase,
                            async:false,
                            success: function(response){
                                if(evalResponse(response)) {
                                    window.location.reload(); 
                                }
                            }
                        });                        
                    }
                });                                 
            }else{
                msgBox(purchase_detail_confirm,purchase_detail_register);
            }
            return false;
        });
        
        fnLoadCheck();  
        
        function purchaseOrder(){
            $.ajax({
                url:url_searchPurchaseOrder,
                data:{idPurchaseOrder: $('#search_purchaseorder').val() ,typeSearch: $('input[name=for_purchase]:checked').val()},
                async:false,
                success:function(response){
                    arrayDetails = {};
                    DeleteRowsAll(tblPurchaseOrderDetail)
                    if(evalResponse(response)){
                        iCountItem = 0;
                        if(response.data.length>0){
                            $.each(response.data, function(index, array) {
                               
                                $('#chars_supplier').val(array.supplier);
                                $('#idPurchaseOrder').val(array.idPurchaseOrder)
                                
                                var itemDetail = new Object();
                                itemDetail.description = array.description;
                                itemDetail.productUnitMeasures = array.productUnitMeasures;
                                itemDetail.amountUnit = array.amountUnit;
                                itemDetail.quantity = array.quantity;
                                itemDetail.totalAmount = array.totalAmount;
                                
                                arrayDetails[iCountItem] = itemDetail;
                                
                                tblPurchaseOrderDetail.addRowData(iCountItem,{
                                    description:array.description,
                                    productUnitMeasures:array.productUnitMeasures,
                                    amountUnit:array.amountUnit,
                                    quantity:array.quantity,
                                    totalAmount:array.totalAmount
                                });
                                iCountItem++;
                            });
                        }else{
                            msgBox('<?php echo __("La Orden de Compra no se encuentra.") ?>')
                        }
                    }else {
                        msgBox(response.msg,response.code);
                    }
                }
            });
        
        }
        
        function DeleteRowsAll(tblJQgrid){
            var rowIds = tblJQgrid.jqGrid('getDataIDs');
            for(var i=0, len=rowIds.length;i<len;i++){
                var currRow =   rowIds[i];
                tblJQgrid.jqGrid('delRowData',currRow);
            }
        }
        
        /*--Función para calcular el Precio SubTotal--*/
        calculationPriceDetail = function(){
            amountUnit = $('#amountUnit').val().split(' ').join('');
            amountUnit = amountUnit==''?0:parseFloat(amountUnit);
            
            fPreTotal = amountUnit;
            
            quantity = $('#quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            amountTotal = amountUnit * quantity;
            
            $('#amountTotal').val(amountTotal.toFixed(2));
            
        }
    }); 
       
       
   
     
   
</script>

<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all">
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4> <span class="title"><?php echo __("Facturación de Compras"); ?></span>
            </h4>
        </div>
    </div>
    <div class="clear"></div>
</div>
<br/>
<div class="divContent">
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="divContentPadding">
            <ul>
                <li>
                    <div class="divLine">
                        <dl>
                            <dd>
                                <label for="search_type"><?php echo __("Tipo de Búsqueda"); ?>:</label>
                            </dd>
                            <dd class="buttonsContainer">
                                <div class="clear"></div> 
                                <input type="radio" id="for_purchaseorder" name="for_purchase" value="purchaseorder" checked="checked"/><label for="for_purchaseorder">Orden de Compra</label>
                                <input type="radio" id="for_codepurchase" name="for_purchase" value="codeorder" /><label for="for_codepurchase">Guía de Remisión</label>
                                <div class="clear"></div>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </div>
                    <div class="search_type divSearch numberOrder">
                        <div class="clear"></div>
                        <div class="for_purchaseorder">
                            <label class="frmLabel" for="search_purchaseorder"><?php echo __("Nro de Documento") ?>:</label>
                            <input name="search_purchaseorder" id="search_purchaseorder" type="text"/>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>

                    </div>
                    <div class="search_type numberOrder buttonSearch">
                        <dl>
                            <dd>
                                <button type="submit" id="search" class="ui-state-highlight"><?php echo __("Buscar") ?></button>
                            </dd>
                        </dl>
                    </div>
                    <div class="clear"></div> 
                </li>  
                </br>
                <li>
                    <hr class="hr_separate ui-widget-content"/>
                </li>
                </br>
                <form id="frm_purchase">
                    <li>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_supplier"><?php echo __("Proveedor"); ?>:</label>
                                </dd>
                                <dd class="relative">
                                    <div class="search_supplier_wrapper ui-corner-all ui-state-default">
                                        <input class="autocompleteInput inputData" type="text" id="chars_supplier" name="chars_supplier" />
                                        <input type="hidden" id="idPurchaseOrder" name="idPurchaseOrder"/>
                                        <div class="clear"></div>
                                    </div>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Moneda") ?>:</label>                    
                                </dd>
                                <dd class="buttonsContainer">                
                                    <div class="clear"></div>
                                    <?php foreach ($a_currency as $sKey => $aCurrency): ?>    
                                        <?php echo Helper::fnGetDrawJqueryButton($aCurrency, 'currency', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aCurrency['display'])))) ?>                                            
                                    <?php endforeach ?>                                    
                                </dd>
                            </dl>
                            <div class="clear"></div> 
                        </div>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Tipo de Documento"); ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div> 

                                    <?php foreach ($a_documents_type as $sKey => $aDocumentType): ?>                                            
                                        <?php echo Helper::fnGetDrawJqueryButton($aDocumentType, 'document_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'force_tax', Rms_Constants::ARRAY_KEY_VALUE => $aDocumentType['tax']))) ?>                                            
                                    <?php endforeach ?>
                                </dd>
                            </dl>
                            <div class="clear"></div> 
                        </div>  
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label for="chars_Customers"><?php echo __("Tipo de Pago"); ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div> 
                                    <?php foreach ($a_purchaseorder_payment as $sKey => $aPurchasePayment): ?>                                            
                                        <?php echo Helper::fnGetDrawJqueryButton($aPurchasePayment, 'payment_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aPurchasePayment['display'])))) ?>                                            
                                    <?php endforeach ?>
                                </dd>
                            </dl>
                            <div class="clear"></div> 
                        </div>
                        <div class="divLine">
                            <dl>
                                <dd>
                                    <label><?php echo __("Serie-Correlativo") ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                   <input class="serie ui-state-highlight"  id="serie" name="serie" value="" type="text" alt="number"/>
                                   <label><?php echo __("-") ?></label>
                                   <input class="correlative ui-state-highlight"  id="correlative" name="correlative" value="" type="text" alt="number"/>
                                </dd>
                            </dl>
                            <div class="clear"></div> 
                        </div> 
                        <div class="clear"></div> 
                    </li>
                </form>
                </br>
                <li>
                    <hr class="hr_separate ui-widget-content"/>
                </li>
                </br>
                <form class="purchaseDetailForm" id="formPurchaseOrdersDetail" method="POST" action ="#">
                    <li>
                        <div class="clear"></div>
                        <table id="purchaseOrderDetail_jqGrid"></table>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <div class="clear"></div>
                        </br>
                        <button id="savePurchase" name="savePurchase" class="ui-state-highlight"><?php echo __("Guardar") ?></button>
                    </li>
                </form>
            </ul>
        </div>
    </div>
</div>

