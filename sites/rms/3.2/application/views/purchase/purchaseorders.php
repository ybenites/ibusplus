<style type="text/css">
    .divTitle h4{font-size:16px;text-transform: uppercase;line-height: 30px;}
    .divTitle,.divContent{min-width: 870px;width: 870px!important;}
    .divTitle,.divContent{  margin-right: auto; margin-left: auto; }
    .ui-widget-content input[type="text"].ui-autocomplete-loading ,
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-image: url('/media/images/ajax-loading.gif');background-position: right center;background-repeat: no-repeat; }
    .ui-widget-content textarea.ui-widget-content.ui-autocomplete-loading { background-position: right top;}
    #formProduct, #formSupplier ul{float: left;}
    #formProduct, #formSupplier .div_buttons{padding: 10px 0;}
    #formProduct, #formSupplier input{width: 166px;}
    #formProduct, #formSupplier select{width: 178px;}
    input.serie{width: 50px;text-align: right;}
    input.correlative{width: 85px;text-align: right;}
    input.purchaseDocuments{width: 200px;text-align: center;}    
    ol li label{width: 80px;float: left;}
    .relative{position: relative;}
    .rotate_text{
        display: block;
        width: 193px;
        height: 30px;
        text-align: center;
        position: absolute;
        left: -80px;
        top: 80px;
        line-height: 30px;
        font-size: 15px;
        color: #A3A3A3;
        -webkit-transform: rotate(-90deg); 
        -moz-transform: rotate(-90deg);	
        transform: rotate(-90deg);
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=1);
        text-shadow: 0px 1px 0px #E5E5EE;
        -moz-text-shadow: 0px 1px 0px #E5E5EE;
    }
    .rotate_text i{color:red;margin-right:2px;}

    .label_amountTotal{float: left;width: 150px;margin-right: 15px;}
    .label_right{float: left;width: 150px;margin-right: 15px;}
    #quantity{width: 60px;text-align: right;margin-right: 10px;}
    #s_service{width: 200px;height: 50px;}
    textArea{
        resize: none;
    }
    .div_dir_content,.div_dir_error{margin-top: 10px;}
    .div_wrapper_dir_label span{float: left;}
    .div_wrapper_dir_label span.ui-icon{
        -moz-border-radius: 8px; -webkit-border-radius: 8px; -khtml-border-radius: 8px; border-radius: 8px;
    }
    .div_wrapper_dir_label span.dir_label_text, .div_wrapper_dir_label span.dir_label_error{line-height: 18px;width: 290px;margin-left: 5px;}
    .div_wrapper_dir_label span.dir_label_error{color: #990000;}
    .div_wrapper_dir_label span{cursor: pointer;}
    .div_dir_error span:hover{text-decoration: underline;}
    .input_dialog{width:195px;}
    #div_newProduct,#div_newSupplier{position: absolute;top:0;left: 0;z-index: 50;
                                     box-shadow: rgba(0, 0, 0, 1) 0 0 5px;
                                     -webkit-box-shadow: rgba(0, 0, 0, 1) 0px 0px 5px;}
    #div_newProduct,#div_newSupplier .divLine,.divLine.divMeasures{width: auto;}
    .divLine.divLineShort{width:370px;}
    #div_newProduct .arrow_left{
        left: -25px;
        top: 22px;
    }
    #div_newSupplier .arrow_left{
        left: -25px;
        top: 22px;
    }
    div.div_separate{margin-top: 2px;}
    hr.hr_separate{border-top-width: 3px;border-top-style: dashed;border-bottom: none;border-left: none;border-right: none;}
    .ui-widget-content .search_supplier_wrapper{background-image: none;margin-right: 5px;}
    .ui-widget-content .search_product_wrapper{background-image: none;margin-right: 5px;}
    .search_supplier_wrapper, .search_supplier_wrapper span,
    .search_product_wrapper, .search_product_wrapper span,
    .search_supplier_wrapper input{float: left;}
    .search_product_wrapper input{float: left;}
    .ui-widget-content .search_supplier_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    .ui-widget-content .search_product_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    .ui-widget-content input[type=text], .ui-widget-content textarea,
    .ui-state-highlight  input[type=text], .ui-widget-content textarea.ui-widget-content
    {background-image: none;}
    .search_product_wrapper, .search_supplier_wrapper span{margin: 3px 4px;}
    .ui_icon_close_shipping_home,
    .ui_icon_close_register_supplier {display: block;float: right;cursor: pointer;}
    .ui_icon_close_register_product {display: block;float: right;cursor: pointer;}
    .ui_icon_close_register_product  span{float: right;}
    .ui_icon_close_register_supplier span{float: right;}
    .register_product_text, .register_supplier_text{line-height: 16px;margin-right: 5px;}
    .ui-autocomplete-input{height: auto;}
    input.inputData{width: 160px;}
    input.select {width:180px;}
    input.inputAddress{width: 240px;}
    input.inputQuantity{width: 100px;text-align: right;}
    input.inputUnit{width: 110px; text-align: right;}
    input.inputAmountUnit{width: 110px; text-align: right;}
    input.inputAmountTotal{width: 110px; height: 20px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    textarea.description_note{width: 242px;height: 103px;resize: none;}
    #formPurchaseOrders ul{position: relative;}
    .divDocInfo{float:right;}
    .register_product_title, .register_supplier_title{padding: 5px;margin-bottom: 10px;line-height: 16px;}
    .arrow_left{
        position: absolute;
        width: 18px;
        height: 28px;
        background-position: 0 -125px;
        background-image: url(/media/images/arrows.png);        
    }
    div.divDocInfo{display:none;font-size: 20px;line-height: 36px;padding: 5px;width: 300px;text-align: center;float: right;}
    .ui-widget input.subPrice {font-size: 25px;background-image: none;}
    #addPurchase{margin: 5px 0;}
    div.divLeft{float: left;}
    div.divRight{float: left;}
    input.input_middle{width:150px;margin-right:10px;}
    .priceTax{display:none;}
    .ui-autocomplete {
        max-height: 120px;
        overflow-x: hidden;
        overflow-y: auto;
        padding-right: 20px;
    }
    * html .ui-autocomplete {
        height: 120px;
    }
    input[type="text"], input[type="password"], textarea, select {
        padding: 5px;
    }
    label{line-height: 16px;}
    .container_shipping_home dl{float: left;}
    .container_shipping_home dl.space_dl{margin-right: 15px;}
    ol{list-style: none;}
    #subcontent{position: relative;}
    input.helptext{font-style: italic;color: #CCCCCC;}
</style> 
<script type="text/javascript">
    
    /* URL */
    var url_getSupplier = '/private/purchaseorders/getSupplier'+jquery_params;    
    var url_getProduct = '/private/purchaseorders/getProduct'+jquery_params;
    var url_createSupplier = '/private/supplier/createorUpdateSupplier'+jquery_params; 
    var url_createProduct = '/private/products/createOrUpdateProduct'+jquery_params;
    var url_getSubCategoryById = '/private/category/getSubCategoryById';
    var url_createPurchaseOrder = '/private/purchaseorders/createPurchaseOrder'+jquery_params;
    
    /* MENSAJES */       
    var supplier_name_required = '<?php echo __("El nombre del Proveedor es obligatorio."); ?>';
    var supplier_code_required = '<?php echo __("El RUC es obligatorio."); ?>';
    var supplier_email_required = '<?php echo __("El Correo Electrónico es obligatorio."); ?>';
    var supplier_email_incorrect = '<?php echo __("El Correo Electrónico es incorrecto."); ?>';
    var msg_supplier_register = '<?php echo __('El Proveedor se registro con éxito'); ?>';
    
    var product_name_required = '<?php echo __("El nombre del Producto es obligatorio."); ?>';
    var product_brand_required = '<?php echo __("Debe seleccionar una Marca."); ?>';
    var product_category_required = '<?php echo __("Debe seleccionar una Categoría."); ?>';
    var product_measure_required = '<?php echo __("Debe seleccionar una Unidad de Medida."); ?>';
    var product_currency_required = '<?php echo __("Debe seleccionar una Moneda."); ?>';
    var product_priceBuy_required = '<?php echo __("El Precio de Compra es obligatorio."); ?>';
    var product_priceSell_required = '<?php echo __("El Precio de Venta es obligatorio."); ?>';    
    var product_maximunStock_required = '<?php echo __("El Stock máximo debe ser mayor al Stock mínimo."); ?>';    
    var msg_product_register = '<?php echo __("El Producto se registro con éxito"); ?>';
    
    var purchaseorder_supplier_required = '<?php echo __("El Proveedor es obligatorio."); ?>';
    var purchaseorder_purchaseDate_required = '<?php echo __("La Fecha de Emision es obligatoria."); ?>';
    var purchaseorder_document_required = '<?php echo __("Seleccione el Tipo de Documento."); ?>';
    var purchaseorder_serie_required = '<?php echo __("La Serie es obligatorio."); ?>';
    var purchaseorder_correlative_required = '<?php echo __("El Correlativo es obligatorio."); ?>';
    
    var purchase_detail_description = '<?php echo __("La Descripcion del Producto es obligatoria"); ?>';
    var purchase_detail_quantity = '<?php echo __("La Cantidad es obligatoria."); ?>';
    var purchase_detail_amountUnit = '<?php echo __("EL Precio Unitario es requerido."); ?>';
    var purchase_detail_note = '<?php echo __("La Nota es obligatoria"); ?>';
    var purchase_detail_register = '<?php echo __("Alerta"); ?>';
    var purchase_detail_confirm = '<?php echo __("Usted debe agregar un detalle a la Orden de Compra."); ?>';
    
    var deleteSubTitle = "<?php echo __("Alerta"); ?>";
    var msgConfirmRegister = "<?php echo __("¿Desea confirmar el registro de una orden de compra?"); ?>";
    var titleConfirmRegister = "<?php echo __("Orden de Compra"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro que desea eliminar el producto seleccionado?") ?>';
    var sTxtSearchSupplier = '<?php echo __('Búsqueda de Proveedor') ?>';
    var sTxtSearchProduct = '<?php echo __('Búsqueda de Producto') ?>';
    
    /*--grid--*/   
    var tblPurchaseDetail = '';
    
    /*--global--*/
    var formSupplier;
    var formProduct;
    var formPurchaseOrdersDetail;
    var formPurchaseOrders;
    var arrayDetails = new Object();
    var iCountItem = 1;
    var edit_product = 0;
    var countarray = 0;
    var keycountarray = 0;
    var dt_OrdenCompra = '<?php echo $documentType; ?>';
    var aIDs='';
    var aux_fir_aliasProduct;
    var new_project = false;
    var aux_correlative;
    $(document).on("ready", function(){
        
        $("#amountUnit").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });      
        
        $('.buttonsContainer').buttonset();
        fnLoadCheck(); 
        
        /*--Formulario de Orden de Compras--*/
        formPurchaseOrders = $("#formPurchaseOrders").validate({
            rules: {
                idSupplier: {required: true},
                serie: {required: true},
                correlative: {required: true},
                purchaseDate: {required: true},
                'purchaseDocuments-combobox':{required:true}
            },
            messages:{
                idSupplier: {required: purchaseorder_supplier_required},
                serie: {required: purchaseorder_serie_required},
                correlative: {required: purchaseorder_correlative_required},
                purchaseDate: {required: purchaseorder_purchaseDate_required},
                'purchaseDocuments-combobox': {required: purchaseorder_document_required}
                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(){
                
            }
        });
        /*--Formulario de Detalle de Compras--*/
        formPurchaseOrdersDetail = $("#formPurchaseOrdersDetail").validate({
            rules: {
                idProduct: {required: true},
                chars_product: {required: true},
                quantity: {required: true},
                amountUnit: {required: true}
             //   description_note: {required:true}
            },
            messages:{
                idProduct: {required: purchase_detail_description},
                chars_product: {required: purchase_detail_description},
                quantity: {required: purchase_detail_quantity},
                amountUnit: {required: purchase_detail_amountUnit}
              //  description_note:{required: purchase_detail_note}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                var itemDetail = new Object();
                itemDetail.product = $('#chars_product').val();
                itemDetail.productUnitMeasures = $('#productUnitMeasures').val();
                itemDetail.quantity = $('#quantity').val();
                itemDetail.amountUnit = $('#amountUnit').val();
                itemDetail.amountTotal = $('#amountTotal').val();
                itemDetail.idSupplier = $('#idSupplier').val().split(' ').join('');
                itemDetail.idProduct = $('#idProduct').val().split(' ').join('');
                returnarray(itemDetail.idSupplier,itemDetail.idProduct);
            
                if(edit_product==1){
                    arrayDetails[keycountarray] = itemDetail;
                    tblPurchaseDetail.setRowData(keycountarray,{
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:itemDetail.quantity,
                        amountUnitOut:itemDetail.amountUnit,
                        amountTotalOut:itemDetail.amountTotal,
                        idProductOut:itemDetail.idProduct,
                        idSupplierOut:itemDetail.idSupplier
                    });
                    edit_product = 0;
                    keycountarray = 0;  
                }else if(countarray == 0 && keycountarray==0 ){
                    arrayDetails[iCountItem] = itemDetail;
                    tblPurchaseDetail.addRowData(iCountItem, {
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:itemDetail.quantity,
                        amountUnitOut:parseFloat(itemDetail.amountUnit).toFixed(2),
                        amountTotalOut:parseFloat(itemDetail.amountTotal).toFixed(2),
                        idProductOut:itemDetail.idProduct,
                        idSupplierOut:itemDetail.idSupplier               
                    });
                    iCountItem++;
                }else{
                    arrayDetails[keycountarray].quantity = parseInt(arrayDetails[keycountarray].quantity) + parseInt(itemDetail.quantity);
                    arrayDetails[keycountarray].amountUnit = (parseFloat(arrayDetails[keycountarray].amountUnit)+parseFloat(itemDetail.amountUnit)).toFixed(2);
                    arrayDetails[keycountarray].amountTotal = (parseFloat(arrayDetails[keycountarray].amountTotal)+parseFloat(itemDetail.amountTotal)).toFixed(2);
                    tblPurchaseDetail.setRowData(keycountarray, {
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:arrayDetails[keycountarray].quantity,
                        amountUnitOut:arrayDetails[keycountarray].amountUnit,                    
                        amountTotalOut:arrayDetails[keycountarray].amountTotal,
                        idProductOut:itemDetail.idProduct,
                        idSupplierOut:itemDetail.idSupplier
                    });
                    countarray = 0;
                    keycountarray = 0;
                }
                tblPurchaseDetail.trigger("reloadGrid"); 
                setTimeout(function(){
                    formPurchaseOrdersDetail.resetForm();
                    $('#chars_product').focus();                   
                }, 100);
                
                   $('#idProduct').val('');   
            }
        });
        function returnarray(idProduct,idSupplier){
            $.each(arrayDetails, function(key, value) { 
                if($('#idProduct').val() == value.idProduct && $('#idSupplier').val()==value.idSupplier){
                    countarray = 1;
                    keycountarray = key;
                    quantity = value.quantity;
                }
            });
        }
        tblPurchaseDetail = $("#purchaseDetail_jqGrid").jqGrid({            
            height: '100%',                        
            width:'840',
            datatype: 'local',
            sortable: false,
            colNames:[                
                'IdSupplier',
                'IdProduct',
                '<?php echo __("Unidad") ?>',
                '<?php echo __("Descripción") ?>',
                '<?php echo __("Cantidad") ?>',
                '<?php echo __("Precio Unitario") ?>',                
                '<?php echo __("Importe") ?>',
                '<?php echo __("Opción") ?>'
            ],
            colModel:[                
                {name:'idSupplierOut',index:'idSupplierOut',hidden:true},
                {name:'idProductOut',index:'idProductOut',hidden:true},    
                {name:'productUnitMeasuresOut',index:'productUnitMeasuresOut',width:60,align:'left'},
                {name:'descriptionOut',index:'descriptionOut',width:200},
                {name:'quantityOut',index:'quantityOut',width:80, align: 'center'},
                {name:'amountUnitOut',index:'amountUnitOut', width:80, align: 'right'},             
                {name:'amountTotalOut',index:'amountTotalOut', width:100, align: 'right'},
                {name:'actions',index:'actions', width:50, align: 'center'}
            ],
            viewrecords: true,
            sortname: 'idOut', 
            
        //    gridview : true,
            rownumbers: true,      
            rownumWidth: 40,            
            footerrow:true,
            afterInsertRow: function(rowid, rowdata, rowelem) {
                editOption = "<button class='editItem' type='button' data-id='"+rowid+"'>Editar</button>"
                deleteOption = "<button class='deleteItem' type='button' data-id='"+rowid+"'>Eliminar</button>"
                tblPurchaseDetail.setRowData(rowid,{actions: editOption + deleteOption});
            },
            gridComplete:function(){
                try{         
                    $('.deleteItem').button({icons: {primary: 'ui-icon-trash'},text: false})
                    .click(function(){
                        var id_delete = $(this).data('id');                        
                        confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                            if(response){                                
                                delete arrayDetails[id_delete];
                                tblPurchaseDetail.delRowData(id_delete); 
                                keycountarray = 0; 
                                countarray = 0;
                                
                            }                            
                        });                        
                    });

                    $('.editItem').button({icons: {primary: 'ui-icon-pencil'},text: false})
                    .click(function(){
                        var id_edit = $(this).data('id');
                        $('#idProduct').val(arrayDetails[id_edit].idProduct);
                        $('#idSupplier').val(arrayDetails[id_edit].idSupplier);
                        $('#productUnitMeasures').val(arrayDetails[id_edit].productUnitMeasures);
                        $('#chars_product').val(arrayDetails[id_edit].product);
                        $('#amountUnit').val(arrayDetails[id_edit].amountUnit);
                        $('#amountTotal').val(arrayDetails[id_edit].amountTotal);
                        $('#quantity').val(arrayDetails[id_edit].quantity);
                        
                        edit_product=1;
                        calculationPriceDetail();
                    });
                
                    fPreTotal= 0;
                    $.each(arrayDetails, function(){
                        fPreTotal += parseFloat(this.preSubTotal);
                    });
                
                    var aGridData = tblPurchaseDetail.getRowData(),
                    fTotalAmountSum=0;
                    $.each(aGridData,function(i, oRowData){                    
                        fTotalAmountSum += parseFloat(oRowData.amountTotalOut);                    
                    });
                   
                    tblPurchaseDetail.footerData('set',{amountUnitOut:'<?php echo __("Sub-Total") ?>',amountTotalOut:fTotalAmountSum.toFixed(2)});
                    
                    formPurchaseOrdersDetail.currentForm.reset();
            
                }catch(e){
                    
                }
            }
        });
        
        /*--Agregar Proveedor--*/
        $('.addSupplier').button({ icons: { primary: 'ui-icon-plus'},text:false}).click(function(){
            o_btn_add_supplier_position = $(this).offset();            
            formSupplier.currentForm.reset();
            formSupplier.resetForm();
            $('input, select, textarea', formSupplier.currentForm).removeClass('ui-state-error');  
            $('#div_newSupplier').css({
                left: (o_btn_add_supplier_position.left+60)+'px'
                , top: (o_btn_add_supplier_position.top-30)+'px'
            });
            
                !$('#div_newSupplier').is(':visible')?$('#div_newSupplier').show('slide'):null;
        });
        $('.ui_icon_close_register_supplier').click(function(){
            formSupplier.currentForm.reset();
            formSupplier.resetForm();
            $('input, select, textarea', formSupplier.currentForm).removeClass('ui-state-error');
            $('#div_newSupplier').hide('slide');
        });
        /*--Formulario de Proveedores--*/
        formSupplier = $("#frmSupplier").validate({
            debug:true,
            rules: {
                s_name: {required: true},
                s_code: {required: true, maxlength: 11 , minlength: 11},            
                s_email: {required: true,
                    email:true}
    
            },
            messages:{
                s_name: {required: supplier_name_required},
                s_code: {required: supplier_code_required,
                    maxlength: '<?php echo __("El RUC no es correcto"); ?>',
                    minlength: '<?php echo __("El RUC no es correcto"); ?>'},
                s_email: {required: supplier_email_required, email: supplier_email_incorrect}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                $.ajax({
                    type:"POST",
                    dataType: "jsonp",
                    url:url_createSupplier,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend:function(){
                        $("#loading").dialog("open");
                    },
                    complete:function(){
                        $("#loading").dialog("close");
                    },
                    success:function(res){
                        if(evalResponse(res)){
                            var data=res.data;
                            $('#idSupplier').val(data.idSupplier);
                            $('#chars_supplier').val(data.supplier);
                            $('#deliveryAddress').val(data.address);
                           
                            msgBox(msg_supplier_register);
                            $('#div_newSupplier').hide('slide');
                            formSupplier.currentForm.reset();
                        }
                    }
                })
            }
        });
        
        /*--Agregar Producto--*/
        $('.addProduct').button({ icons: { primary: 'ui-icon-plus'},text:false}).click(function(){
            o_btn_add_product_position = $(this).offset();            
            formProduct.currentForm.reset();
            formProduct.resetForm();
                   
            $('input, select, textarea', formProduct.currentForm).removeClass('ui-state-error'); 
            $('#div_newProduct').css({
                left: (o_btn_add_product_position.left+60)+'px'
                , top: (o_btn_add_product_position.top-30)+'px'
            });
                !$('#div_newProduct').is(':visible')?$('#div_newProduct').show('slide'):null;
        });
        $('.ui_icon_close_register_product').click(function(){
            formProduct.currentForm.reset();
            formProduct.resetForm();
            $('input, select, textarea', formProduct.currentForm).removeClass('ui-state-error');
            $('#div_newProduct').hide('slide');
        });
        
        /*--Formulario de Productos--*/
        $("#idCategory").combobox({
            selected: function(event,ui){
                var category = $(this).val();
                var codigoCategoria = $('#idCategory option:selected').data('codigo');
                $('input#fir_aliasProduct').val(codigoCategoria);
                $('#idSubCategory').val("");
                $("#idSubCategory-combobox").val($("#idSubCategory option:selected").text());                
                generate_subCategory(category);
            }
        });
        $("#idSubCategory").combobox({
            selected: function(event,ui){
                var s_code = '';
                s_code = $('#idSubCategory option:selected').data('s_codigo');
                $('input#sec_aliasProduct').val(s_code);
            }
        });
        $("#idbrand,#purchaseDocuments").combobox();
        
        generate_subCategory = function(idCategory){
            $('#th_aliasProduct').val('');
            var select_subcategory = $('#idSubCategory');
            $.ajax({
                url:url_getSubCategoryById + jquery_params,
                data:{
                    id:function(){
                        return idCategory;
                    }
                },
                type:"POST",
                dataType:"json",
                async:false,
                success:function(response){
                    if(parseInt($('#idCategory option:selected').data('codigo'))!= parseInt(aux_fir_aliasProduct) || new_project ==true)
                        $('#th_aliasProduct').val(response.correlative);
                    else
                         $('#th_aliasProduct').val(aux_correlative)
                    if(response.data != ''){
                        $('#subCategory').show();
                        var option = '<option data-s_codigo="[CODE]" value="[VALUE]">[LABEL]</option>';
                        $('option', select_subcategory).remove();
                        $(select_subcategory).append(option.replace('[VALUE]', "",'g').replace('[LABEL]','Seleccione Sub Categoria', 'g'));
                        $.each(response.data, function(index, array) {                        
                            $(select_subcategory).append(option.replace('[VALUE]', array['idCategory'],'g').replace('[LABEL]',array['superCategoryName'], 'g').replace('[CODE]',array['codigo'], 'g'));
                        });
                    }else {
                        $('#subCategory').hide();
                        msgBox('<?php echo __("No se ha asignado una SubCategoria"); ?>','Error');
                    }
                }
            });
        }
        
        $("#unitMeasures").combobox();
        $("#currency").combobox();
        $("#haveDetail").button();
        $("#subCategory").hide();
        formProduct = $("#frmProduct").validate({
            debug:true,
            rules: {
                'productName' : {required: true},
                'idbrand-combobox' :{required:true},
                'idCategory-combobox' :{required:true},             
                'unitMeasures-combobox' :{required:true}, 
                'currency-combobox' :{required:true}, 
                'productPriceBuy' :{required:true},
                'productPriceSell' :{required:true}
//                'maximunStock':{
//                    min:function(){
//                        var max = $("minimunStock").val();
//                        return parseInt(max);
//                    }
//                }
                
            },
            messages:{
                'productName' : {required: product_name_required},
                'idbrand-combobox': {required: product_brand_required},
                'idCategory-combobox' : {required: product_category_required},
                'unitMeasures-combobox': {required: product_measure_required},
                'currency-combobox' :{required:product_currency_required},
                'productPriceBuy' :{required:product_priceBuy_required},
                'productPriceSell' :{required:product_priceSell_required}
//                'maximunStock':{min: product_maximunStock_required}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                $.ajax({
                    type:"POST",
                    url: url_createProduct,
                    data:$.extend($(form).serializeObject(), $(form).serializeDisabled()), 
                    beforeSend: function(){
                        $("#loading").dialog("open");
                    },
                    complete: function(){
                        $("#loading").dialog("close");
                    },
                    success: function(res){
                        if(evalResponse(res)){
                            var data= res.data;
                            $('#idProduct').val(data.idProduct);
                            $('#chars_product').val(data.product);
                            $('#productUnitMeasures').val(data.productUnitMeasures);
                            msgBox(msg_product_register);
                            $('#div_newProduct').hide('slide');
                            formProduct.currentForm.reset();
                        }
                    }
                })
            }
        });
        
        /*--datepicker--*/
        $('#purchaseOrderDate').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>'
        });
        $('#deliveryDate').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>',
            beforeShow: function(){
                return {
                    minDate:$("#purchaseOrderDate").datepicker("getDate")
                }
            }
            
        });
        /* --setMask y priceFormat--*/
        $('#s_code,#quantity,#serie,#correlative,#minimunStock, #maximunStock').setMask();        
        $('#productPriceBuy,#productPriceSell,#amount').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* -- Evento de la cantidad y el monto --*/
        $('#quantity').keyup(function(key){
            if($('#amountUnit').val()!=""){
                calculationPriceDetail();
            }
        }); 
        
        $('#amountUnit').keyup(function(key){
            if($('#quantity').val()!=""){
                calculationPriceDetail();
            }
        }); 
        /*--event click--*/
        $(".buttonsContainer").buttonset();
        $('#addPurchase').button({icons:{primary: 'ui-icon-plus'}});
        $('#savePurchaseOrder').button({icons:{primary: 'ui-icon-disk'}});
        $('#btn_save_supplier,#btn_save_product').button({icons:{primary: 'ui-icon-check'}});
        $('#chars_supplier').helptext({value: sTxtSearchSupplier, customClass: 'lightSearch'});
        $('#chars_product').helptext({value: sTxtSearchProduct, customClass: 'lightSearch'});
        
        /*--Búsqueda de Proveedor--*/
        $( "#chars_supplier" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_supplier',
            source:function(req,response){
                $.ajax({
                    url:url_getSupplier,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.supplier + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.supplier,
                                    id : item.idSupplier,
                                    dir : item.address
                                }
                            }));
                        }
                    }
                });
            },
            focus: function (event, ui){
                $('#deliveryAddress').val(ui.item.dir);
            },
            select: function( event, ui ) {
                $('#idSupplier').val(ui.item.id); 
                $('#deliveryAddress').val(ui.item.dir);
            }
            
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#chars_supplier" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 290, 'margin-bottom': 5});            
        };
        /*--Búsqueda de Producto--*/
        $( "#chars_product" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_product',
            source:function(req,response){
                $.ajax({
                    url:url_getProduct,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.product + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.product,
                                    id : item.idProduct,
                                    unit : item.productUnitMeasures
                                }
                            }));
                        }
                    }
                });
            },
            focus: function (event, ui){
                $('#productUnitMeasures').val(ui.item.unit);
            },
            select: function( event, ui ) {
                $('#idProduct').val(ui.item.id); 
                $('#productUnitMeasures').val(ui.item.unit);
            }
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#chars_product" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 290, 'margin-bottom': 5});            
        };
        
        fnLoadCheck();
       
        $("#savePurchaseOrder").on('click',function(e){
            e.preventDefault();
            if(!$("#formPurchaseOrders").valid())
                return false;
            var arrayDataGrid = $("#purchaseDetail_jqGrid").jqGrid("getRowData");
            if(arrayDataGrid.length>0){
                confirmBox(msgConfirmRegister,titleConfirmRegister,function(event){
                    if(event){
                        var dataPurchaseOrder = new Object();
                        dataPurchaseOrder.header = $("#formPurchaseOrders").serializeObject();
                        dataPurchaseOrder.note = $("#description_note").val();
                        dataPurchaseOrder.detail = arrayDetails;
                        showLoading=1;
                        $.ajax({
                            url:url_createPurchaseOrder,
                            type:"POST",
                            dataType:'json',
                            data: dataPurchaseOrder,
                            async:false,
                            success: function(response){
                                if(evalResponse(response)) {
                                    window.location.reload(); 
                                }
                            }
                        });                        
                    }
                });                                 
            }else{
                msgBox(purchase_detail_confirm,purchase_detail_register);
            }
            return false;
        });
        /*--Función para calcular el Precio SubTotal--*/
        calculationPriceDetail = function(){
            amountUnit = $('#amountUnit').val().split(' ').join('');
            amountUnit = amountUnit==''?0:parseFloat(amountUnit);
            
            fPreTotal = amountUnit;
            
            quantity = $('#quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            amountTotal = amountUnit * quantity;
            $('#amountTotal').val(amountTotal.toFixed(2));
        }
        
<?php if ($bAvailableDocumentType): ?>    
            successDocument = false;
            fnUpdatePurchaseOrders = function(sType){
                try{
                    oDocumentType = getCurrentCorrelative(sType);
                    if(oDocumentType.serie != null)
                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.serie + '-' + oDocumentType.correlative;
                    else
                        displayDocument = oDocumentType.documentType.display + ' ' + oDocumentType.correlative;
                                                                                       
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-highlight')
                    .html(displayDocument);
                    successDocument = true;
                                                                                                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                                                            
                }catch(e){
                    successDocument = false;
                    $('div.divDocInfo')
                    .removeClass('ui-state-highlight')
                    .removeClass('ui-state-error')
                    .addClass('ui-state-error')
                    .html('<?php echo __("Documento No Asignado") ?>');
                }
            };
            fnUpdatePurchaseOrders(dt_OrdenCompra);
            $('div.divDocInfo').css('display', 'block');
            $('input[name=document_type]').change(function(){
                fnUpdatePurchaseOrders($(this).val());
            });                                                
<?php else: ?>


<?php endif ?>  
            
    });
   
</script>

<div class="divTitle">
    <div class="divTitlePadding1 ui-widget ui-widget-content ui-corner-all"<?php if ($bAvailableDocumentType): ?> style="width:550px;float:left;" <?php endif ?>>
        <div class="divTitlePadding2 ui-state-default ui-widget-header ui-corner-all ui-helper-clearfix">
            <h4> <span class="title"><?php echo __("Registro de Orden de Compra"); ?></span>
            </h4>
        </div>
    </div>
    <?php if ($bAvailableDocumentType): ?>
        <div class="divDocInfo ui-state-highlight ui-widget-header ui-corner-all ui-helper-clearfix"></div>
    <?php endif ?>
    <div class="clear"></div>
</div>

<div id="div_newSupplier" class="ui-widget ui-widget-content ui-corner-all none">
    <div class="arrow_left"></div>
    <form id="frmSupplier" method="post" action="" class="frm" >            
        <h4 class="register_supplier_title ui-widget-header ui-corner-all">
            <?php echo __("Proveedor") ?>
            <b class="ui_icon_close_register_supplier ui-state-highlight ui-corner-all" role="button">
                <span class="ui-corner-all ui-icon ui-icon-close register_supplier_close"></span>
                <span class="register_supplier_text"><?php echo __("Cerrar") ?></span>
            </b>
        </h4>
        <div class="label_output" style="width:425px; margin: auto; padding: 5px;">
            <div>
                <label><?php echo __("Razón Social"); ?>:</label>
                <input id="s_name"  name="s_name" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("RUC"); ?>:</label>
                <input id="s_code" name="s_code" value="" type="text" maxlength="150" size="30" alt="number"/>
            </div>
            <div>
                <label><?php echo __("Email"); ?>:</label>
                <input id="s_email" name="s_email" value="" type="text" maxlength="150" size="30"/>
            </div>
            <div>
                <label><?php echo __("Producto"); ?>:</label>
                <textarea id="s_service" name="s_service" ></textarea>
            </div>

            <div>
                <label><?php echo __("Dirección"); ?>:</label>
                <input id="s_address" name="s_address" value="" type="text" maxlength="150" size="30"/>
            </div>
            <div>
                <label><?php echo __("Teléfono"); ?>:</label>
                <input id="s_phone" name="s_phone" value="" type="text" maxlength="150" size="30" alt="number"/>
            </div>
            <div>
                <label><?php echo __("Fax"); ?>:</label>
                <input id="s_fax" name="s_fax" value="" type="text" maxlength="150" size="30" alt="number"/>
            </div>
            <div>
                <label><?php echo __("Celular"); ?>:</label>
                <input id="s_cellphone" name="s_cellphone" value="" type="text" maxlength="150" size="30" alt="number"/>
            </div>
            <div class="divLine div_buttons">
                <button id="btn_save_supplier" type="submit"><?php echo __('Guardar') ?></button>                        
            </div>
        </div>

    </form>
</div>

<div id="div_newProduct" class="ui-widget ui-widget-content ui-corner-all none">
    <div class="arrow_left"></div>
    <form id="frmProduct" method="post" action="" class="frm" >            
        <h4 class="register_product_title ui-widget-header ui-corner-all">
            <?php echo __("Producto") ?>
            <b class="ui_icon_close_register_product ui-state-highlight ui-corner-all" role="button">
                <span class="ui-corner-all ui-icon ui-icon-close register_product_close"></span>
                <span class="register_product_text"><?php echo __("Cerrar") ?></span>
            </b>
        </h4>
        <div class="label_output" style="width:450px; margin: auto; padding: 5px;">
            <?php if ($bTagGenerator): ?>
                <div>
                    <label><?php echo __("Código Generado"); ?>:</label>
                    <input type="text" id="fir_aliasProduct" name="fir_aliasProduct" readonly="true" class="input_codigo" size="5"/>
                    <input type="text" id="sec_aliasProduct" name="sec_aliasProduct"  readonly="true" class="input_codigo" size="5"/>
                    <input type="text" id="th_aliasProduct" name="th_aliasProduct" readonly="true" class="input_codigo" size="5"/>
                </div>
            <?php else: ?>
                <div>
                    <label><?php echo __("Código"); ?>:</label>
                    <input id='aliasProduct'  name="aliasProduct" value="" type="text" maxlength="150" size="30" />
                </div>
            <?php endif; ?>
            <div>
                <label><?php echo __("Producto"); ?>:</label>
                <input id="productName"  name="productName" value="" type="text" maxlength="150" size="30" />
            </div>
            <div>
                <label><?php echo __("Marca"); ?>:</label>
                <select id="idbrand" name="idbrand">
                    <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                    <?php
                    foreach ($brand as $v) {
                        ?>
                        <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                        <?php
                    }
                    ?>
                </select> 
            </div> 
            <div>
                <label><?php echo __("Categoría"); ?>:</label>
                <select id="idCategory" name="idCategory">
                    <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                    <?php
                    foreach ($category as $v) {
                        ?>
                        <option data-codigo="<?php echo $v['codigo'] ?>" value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                        <?php
                    }
                    ?>
                </select> 
            </div>
            <div id="subCategory" class="none">
                <label><?php echo __('Sub-Categoría') ?>:</label>
                <select id="idSubCategory" name="idSubCategory">
                    <option value=""><?php echo __("Seleccina Sub Categoria"); ?></option>
                </select>
            </div>
            <div>
                <label><?php echo __('Unidad de Medida') ?>:</label>
                <select id="unitMeasures" name="unitMeasures">
                    <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                    <?php
                    foreach ($a_unitmeasures as $a_UNIT_MEASURES) {
                        ?>
                        <option value="<?php echo $a_UNIT_MEASURES['value'] ?>"><?php echo $a_UNIT_MEASURES['display'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div>
                <label><?php echo __('Tiene Detalle') ?>:</label>
                <input type="checkbox" id="haveDetail" name="haveDetail" value="1" /><label for="haveDetail">Detalle</label>
                <div class="clear"></div>
            </div>
            <div>
                <label><?php echo __('Stock Minimo') ?>:</label>
                <input type="text" id="minimunStock" name="minimunStock" alt="number"/>
            </div>
            <div>
                <label><?php echo __('Stock Maximo') ?>:</label>
                <input type="text" id="maximunStock" name="maximunStock" alt="number"/>
            </div>
            <div>
                <label><?php echo __('Precio Venta'); ?></label>
                <input type="text" id="productPriceSell" name="productPriceSell" />
            </div>

            <div class="divLine div_buttons">
                <button id="btn_save_product" type="submit"><?php echo __('Guardar') ?></button>                        
            </div>
        </div>

    </form>
</div>

<div class="divContent">
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="divContentPadding">
            <form action="#" class="orderForm" id="formPurchaseOrders">
                <ul>
                    <li>
                        <div class="left">
                            <div class="divLine divLineShort">
                                <dl>
                                    <dd>
                                        <label for="chars_supplier" class="frmlbl"><?php echo __("Proveedor"); ?>:</label>
                                    </dd>
                                    <dd class="relative">
                                        <div class="clear"></div>
                                        <div class="search_supplier_wrapper ui-corner-all ui-state-default">
                                            <input class="autocompleteInput inputData" type="text" id="chars_supplier" name="chars_supplier" value="<?php echo 'Búsqueda de Proveedor' ?>"/>
                                            <input type="hidden" id="idSupplier" name="idSupplier"/>
                                            <div class="clear"></div>
                                        </div>
                                        <button type="button" class="addSupplier"><?php echo __("Agregar Proveedor") ?></button>
                                        <div id="auto_div_supplier"></div>
                                        <div class="clear"></div>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __("Dirección") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputAddress" id="deliveryAddress" name="deliveryAddress" type="text"/>
                                    </dd> 
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __('Documento') ?>:</label> 
                                    </dd> 
                                    <dd>
                                        <div class="clear"></div>
                                        <select id="purchaseDocuments" name="purchaseDocuments" >
                                            <option value=""><?php echo __("Seleccione una Opción..."); ?></option>
                                            <?php
                                            foreach ($a_purchase_documents as $v) {
                                                ?>
                                                <option value="<?php echo $v['value'] ?>"><?php echo $v['display'] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="divLeft">
                            <div class="divLine divLineShort">
                                <dl>
                                    <dd>
                                        <label><?php echo __("Fecha de Emisión") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" name="purchaseOrderDate" id="purchaseOrderDate" type="text" value="<?php echo date($date_format_php) ?>"/>
                                    </dd>                               
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __("Fecha de Vencimiento") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputData" name="deliveryDate" id="deliveryDate" type="text" value="<?php echo date($date_format_php) ?>"/>
                                    </dd>                               
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label><?php echo __("Serie-Correlativo") ?>:</label>
                                    </dd>
                                    <dd>
                                        <input class="serie ui-state-highlight"  id="serie" name="serie" value="" type="text" alt="number"/>
                                        <label><?php echo __("-") ?></label>
                                        <input class="correlative ui-state-highlight"  id="correlative" name="correlative" value="" type="text" alt="number"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div> 
                            </div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <br/>
                </ul>
                <ul>
                    <li>
                        <hr class="hr_separate ui-widget-content"/>
                    </li>
                    <div class="clear"></div> 
                    <div class="left">
                        <div class="divLine divLineShort">  
                            <div class="clear"></div> 
                            <dl>
                                <dd>
                                    <label><?php echo __("Tipo de Moneda"); ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div> 
                                    <?php foreach ($a_currency as $sKey => $aCurrencyPayment): ?>                                            
                                        <?php echo Helper::fnGetDrawJqueryButton($aCurrencyPayment, 'currency_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aCurrencyPayment['display'])))) ?>                                            
                                    <?php endforeach ?>
                                </dd>
                            </dl>
                            <div class="clear"></div> 
                        </div>
                    </div>
                    <div class="divLeft">
                        <div class="divLine divLineShort"
                             <dl>
                                <dd>
                                    <label><?php echo __("Tipo de Pago"); ?>:</label>
                                </dd>
                                <dd class="buttonsContainer">
                                    <div class="clear"></div> 
                                    <?php foreach ($a_purchaseorder_payment as $sKey => $aPurchasePayment): ?>                                            
                                        <?php echo Helper::fnGetDrawJqueryButton($aPurchasePayment, 'payment_type', 'radio', array(array(Rms_Constants::ARRAY_KEY_NAME => 'display', Rms_Constants::ARRAY_KEY_VALUE => __($aPurchasePayment['display'])))) ?>                                            
                                    <?php endforeach ?>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <br/>
                </ul>
            </form>

            <ul>
                <li>
                    <hr class="hr_separate ui-widget-content"/>
                </li>
                </br>
                <form class="orderForm" id="formPurchaseOrdersDetail" method="POST" action="#">
                    <li>
                        <div class="left">
                            <div class="divLine divLineShort">
                                <dl>
                                    <dd>
                                        <label for="chars_product"><?php echo __("Producto"); ?>:</label>
                                    </dd>
                                    <dd class="relative">
                                        <div class="search_product_wrapper ui-corner-all ui-state-default">
                                            <input class="autocompleteInput inputData" type="text" id="chars_product" name="chars_product" value="<?php echo 'Búsqueda de Producto' ?>"/>
<!--                                           <input type="hidden" id="idProduct" name="idProduct"/>-->
                                            <div class="clear"></div>
                                        </div>
                                        <button type="button" class="addProduct"><?php echo __("Agregar Producto") ?></button>
                                        <input type="hidden" id="idProduct" name="idProduct"/>
                                        <div id="auto_div_product"></div>
                                        <div class="clear"></div>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="description_note" class="frmlbl label_right"><?php echo __("Nota") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <textarea id="description_note" name="description_note" class="description_note"></textarea>
                                        <input type="hidden" name="idProduct" id="idProduct"/>
                                    </dd>                               
                                </dl>
                                </br>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <button type="submit" id="addPurchase" class="ui-state-highlight"><?php echo __("Agregar Compra") ?></button>
                                    </dd>
                                </dl>
                                </br>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>  
                        <div class="divLeft">
                            <div class="divLine divLineShort">
                                <dl>
                                    <dd>
                                        <label for="productUnitMeasures" class="frmlbl label_productUnit"><?php echo __("Medida") ?>:</label>                    
                                    </dd>
                                    <dd>
                                        <div class="clear"></div>
                                        <input class="inputUnit ui-state-highlight" type="text" id="productUnitMeasures" name="productUnitMeasures" disabled/>
                                    </dd> 
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="quantity" class="frmlbl"><?php echo __("Cantidad") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputQuantity ui-state-highlight" type="text" id="quantity" name="quantity" alt="number"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="amountUnit" class="frmlbl"><?php echo __("Precio Unitario") ?>:</label>
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputAmountUnit ui-state-highlight" type="text" id="amountUnit" name="amountUnit" />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="amountTotal" class="frmlbl label_amountTotal"><?php echo __("SubTotal") ?>:</label>
                                    </dd>
                                    <dd>
                                        <div class="clear"></div>
                                        <input class="inputAmountTotal ui-state-highlight" type="text" id="amountTotal" name="amountTotal" disabled/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>                                               
                            <div class="clear"></div>
                        </div>
                    </li>
                </form>
                <li>
                    <div class="clear"></div>
                    <table id="purchaseDetail_jqGrid"></table>
                    <div class="clear"></div>
                    <br/>
                </li>
                <li>
                    <div class="clear"></div>
                    <button id="savePurchaseOrder" type="button" name="savePurchaseOrder" class="ui-state-error"><?php echo __('Guardar Compra') ?></button>
                    <div class="clear"></div>
                </li>
                </br>
            </ul>        
            <div class="clear"></div>

        </div>
    </div>
</div>
