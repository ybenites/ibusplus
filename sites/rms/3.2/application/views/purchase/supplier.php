<style type="text/css">
    .div_newSupplier{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
    }
    .div_gridSupplier{        
        padding-top: 10px;
    }
    .label_output{clear: both;margin-bottom: 5px;float: left;}
    textarea.s_service{width: 180px;height: 50px;resize: none;}
    .cFieldSet{
        padding: 10px;
    }
    .cFieldSet>div{
        padding-top: 10px;
    }
    .cFieldSet>div>label{
        float: left;
        font-weight: bolder;
        margin-right: 5px;
        margin-top: 5px;
        text-align: right;
        width: 160px;
    }
    .cFieldSet>div input[type="text"], input[type="password"], textarea {
        padding: 3px; 
        width: 180px;
    }
    .ui-jqgrid tr.jqgrow td{ white-space: normal;}
</style>

<script type="text/javascript">
    /*--url--*/
    var url_createSupplier = '/private/supplier/createorUpdateSupplier'+jquery_params; 
    var url_editSupplier= '/private/supplier/getSupplierById'+jquery_params;
    var url_updateStatus= '/private/supplier/updateStatus'+jquery_params;
    var url_deleteSupplier = '/private/supplier/deleteSupplier'+jquery_params;
    var url_supplier_list= '/private/supplier/listSupplier'+jquery_params;
    var ico_on = '<img src="/media/ico/turn_on_cp.png" />';
    var ico_off = '<img src="/media/ico/turn_off_cp.png" />';
    /*--text--*/       
    var supplier_form_Title = "<?php echo __("Proveedor"); ?>";
    var supplier_newTitle = "<?php echo __("Nuevo Proveedor"); ?>";
    var supplier_editTitle = "<?php echo __("Editar Proveedor"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Proveedor"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de eliminar este proveedor?"); ?>';
    var supplier_name_required = '<?php echo __("El nombre del Proveedor es obligatorio."); ?>';
    //    var supplier_contactname_required = '<?php echo __("El nombre del Contacto Legal es obligatorio."); ?>';
    var supplier_code_required = '<?php echo __("El RUC es obligatorio."); ?>';
    //    var supplier_product_required = '<?php echo __("El Producto es obligatorio."); ?>';
    //    var supplier_address_required = '<?php echo __("La Dirección es obligatorio."); ?>';
    //    var supplier_phone_required = '<?php echo __("El Nro de Teléfono es obligatorio."); ?>';
    //    var supplier_cellphone_required = '<?php echo __("El Nro de Celular  es obligatorio."); ?>';
    var supplier_email_required = '<?php echo __("El Correo Electrónico es obligatorio."); ?>';
    var supplier_email_incorrect = '<?php echo __("El Correo Electrónico es incorrecto."); ?>';
    
    /*--grid--*/   
    var grid_supplier;

    /*--global--*/
    var g_demo;

    $(document).on("ready", function(){
       
        /*--jqgrid--*/
        grid_supplier = $('#supplier_jqGrid').jqGrid({
            url: url_supplier_list,
            datatype: 'jsonp',
            mtype: 'POST', 
            width:1200,
            height:'auto',
            colNames:[
                'ID',
                '<?php echo __("Razón Social") ?>',
                '<?php echo __("RUC") ?>',
                '<?php echo __("Producto") ?>',
                '<?php echo __("Estado") ?>',
                '<?php echo __("Dirección") ?>',
                '<?php echo __("Fax") ?>',
                '<?php echo __("Teléfono") ?>',
                '<?php echo __("Celular") ?>',
                '<?php echo __("Email") ?>',
                '<?php echo __("Acciones") ?>'
            ],
            colModel:[
                {name:'idSupplier',index:'idSupplier',key:true,hidden:true,hidedlg:true},
                {name:'name',index:'rms_supplier.name',width:150,align:'left',search:true},
                {name:'code',index:'rms_supplier.code',search:true,width:80,align:'center'},
                {name:'service',index:'service',search:false,width:150,align:'center'},
                {name:'status',index:'status',search:false,width:40,align:'center'},
                {name:'address',index:'address',search:false,width:160,align:'center'},
                {name:'fax',index:'fax',search:false,width:80,align:'center'},
                {name:'phone',index:'phone',search:false,width:60,align:'center'},
                {name:'cellphone',index:'cellphone',search:false,width:80,align:'center'},
                {name:'email',index:'email',search:false,width:160,align:'center'},
                {name:'actions',index:'actions',width:65,search:false,align:'center'}
            ],
            pager:"#supplier_jqGrid_pager",
            sortname: 'idSupplier',
            sortable: true,  
            gridview: false,
            rowNum: 15,
            rowList:[15,30,50],
            sortorder: "asc",
            rownumbers: true,  
            width:1200,
            height:420,
            rownumWidth: 30, 
            shrinkToFit:true,
            viewrecords: true, 
            caption: '<?php echo __("Lista de Proveedores"); ?>',
            afterInsertRow:function(rowid,rowdata,rowelem){
                if(rowdata.status == 1){
                    $("#supplier_jqGrid").jqGrid('setRowData',rowid,{status: '<div class="s_status" data-id="'+rowid+'">'+ico_on+'</div>'});
                } else {
                    $("#supplier_jqGrid").jqGrid('setRowData',rowid,{status:'<div class="s_status" data-id="'+rowid+'">'+ico_off+'</div>'});
                }
            },
            gridComplete:function(){
                var ids = $('#supplier_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                   
                    edit = "<a class=\"edit\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#supplier_jqGrid").jqGrid('setRowData',ids[i],{actions:edit + trash });
                }
                $('.edit').button({icons:{primary: 'ui-icon-pencil'},text: false});
                $('.trash').button({icons:{primary: 'ui-icon-trash'},text: false});
            }
        });
        $("#supplier_jqGrid").jqGrid('navGrid','#grid_list_brand_pager',{add:false,edit:false,del:false,search:false,refresh:false});                        
        jQuery("#supplier_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        $('table#supplier_jqGrid').on('click','.s_status',function(){            
            var id=$(this).data('id');
            $.ajax({
                url:url_updateStatus,
                data:{ids:id},
                success: function(response){                            
                    if (response.code == code_success)
                    {
                        $("#supplier_jqGrid").trigger("reloadGrid");
                    }else{
                        msgBox(response.msg, response.code);
                    }
                }
            });          
        });
        /*--validate--*/
        $('#s_code').setMask();
        var validator_Supplier= $("#frmSupplier").validate({
            debug:true,
            rules: {
                s_name: {required: true},
                s_code: {required: true, maxlength: 11 , minlength: 11},
                //               s_address: {required: true}              
                s_email: {required: true, email:true}
            },
            messages:{
                s_name: {required: supplier_name_required},
                s_code: {required: supplier_code_required,
                    maxlength: '<?php echo __("El RUC no es correcto"); ?>',
                    minlength: '<?php echo __("El RUC no es correcto"); ?>'
                },
                //              s_address: {required: supplier_address_required}
                s_email: {required: supplier_email_required, 
                    email: supplier_email_incorrect}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            },
            submitHandler:function(form){
                loadingcarga = 1;
                $.ajax({
                    type:"POST",
                    url:url_createSupplier,
                    data:$(form).serialize(),
                    dataType:'jsonp',
                    success: function(response){
                        if(evalResponse(response)){
                            $('#supplier_jqGrid').trigger('reloadGrid');
                            $('#frmPopup').dialog('close');
                        }
                    }
                })
            }
        });

        /*--dialog--*/
        $('#frmPopup').dialog({
            title: supplier_form_Title,
            autoOpen:false,
            autoSize:true,
            modal:true,
            width:500,
            resizable:false,
            closeOnEscape:true,
            dragable: false,
            buttons: {
                "<?php echo __("Guardar"); ?>":function(){
                    $('#frmSupplier').submit();
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
            ,beforeClose: function(event, ui) {
                validator_Supplier.currentForm.reset();
                validator_Supplier.resetForm();
                $('input, select, textarea', validator_Supplier.currentForm).removeClass('ui-state-error');
            }
        });
        /*--fn para editar Proveedor--*/
        $('#supplier_jqGrid').on('click','.trash',function(){            
            var id=$(this).attr("rel");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){                    
                    $.ajax({
                        url:url_deleteSupplier,
                        data:{ids:id},
                        success: function(response){                            
                            if (response.code == code_success){
                                $("#supplier_jqGrid").trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code);
                            }
                        }
                    });          
                }
            });
            return false
        });
        /*--fn para eliminar Proveedor--*/

        $('#supplier_jqGrid').on('click','.edit',function(){     
            $('.frmSubtitle').text(supplier_editTitle);
            var id=$(this).attr("rel");   
            loadingcarga=1;
            $.ajax({
                data:{ids: id},                    
                type:"POST",
                dataType:"jsonp",
                url:url_editSupplier,
                beforeSend:function(){
                    $("#loading").dialog("open");},
                complete:function(){
                    $("#loading").dialog("close");                                               
                },
                success:function(res){
                    if(evalResponse(res)){
                        var data=res.data;
                        $("#supplier_id").val(data.idSupplier);
                        $("#s_name").val(data.name);
                        $("#s_contactName").val(data.contactName);
                        $("#s_code").val(data.code);
                        $("#s_address").val(data.address);
                        $("#s_service").val(data.service);
                        $("#s_phone").val(data.phone);
                        $("#s_fax").val(data.fax);
                        $("#s_cellphone").val(data.cellphone);
                        $("#s_email").val(data.email);
                        $("#frmPopup").dialog('open');                        
                    } else {
                        msgBox(res.msg,res.code);
                    }                                        
                }
            });
            return false
        });
        /*--event click--*/
        $("#btnNewSupplier").button({icons:{primary: 'ui-icon-plusthick'},text: true});
        $('#s_status').buttonset();
        $('#btnNewSupplier').on('click',function(){
            $('.frmSubtitle').text(supplier_newTitle);
            $('#frmPopup').dialog('open');
        });
    });
</script>

<div>
    <center>    
        <div class="titleTables">
            <span class="title"><?php echo __("Administración de Proveedores"); ?></span>
        </div>
    </center>
</div>
<div>
    <div class="div_newSupplier">
        <button id="btnNewSupplier"><?php echo __("Nuevo Proveedor"); ?></button>
    </div>
    <div class="div_gridSupplier">
        <center>
            <div>
                <table id="supplier_jqGrid"></table>
                <div id="supplier_jqGrid_pager"></div>
            </div>
        </center>
    </div>
</div>

<div id="frmPopup" class="dialog-modal">
    <form id="frmSupplier" method="post">
        <fieldset class="ui-corner-all cFieldSet">
            <legend class="frmSubtitle"></legend>
            <div class="label_output">
                <div>
                    <label><?php echo __("Razón Social"); ?>:</label>
                    <input id="s_name"  name="s_name" value="" type="text" maxlength="100" size="30" />
                </div>
                <div>
                    <label><?php echo __("RUC"); ?>:</label>
                    <input id="s_code" name="s_code" value="" type="text" maxlength="11" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("E-mail"); ?>:</label>
                    <input id="s_email" name="s_email" value="" type="text" maxlength="40" size="40"/>
                </div>
                <div>
                    <label><?php echo __("Producto"); ?>:</label>
                    <textarea id="s_service" name="s_service" class="s_service"></textarea>
                </div>
                <!--                <div>
                                    <label style="margin-top: 5px;"><?php echo __("Estado"); ?>:</label>
                                    <div id="s_status">
                                        <input type="radio" id="s_statusActive" name="s_status" checked="checked" value="1"/><label for="s_statusActive" style="min-width: 20px; padding: 0px;"><?php echo __("Activo"); ?></label>
                                        <input type="radio" id="s_statusInactive" name="s_status"  value="0"/><label for="s_statusInactive" style="min-width: 20px; padding: 0px;"><?php echo __("Inactivo"); ?></label>
                                    </div> 
                                </div>-->
                <div>
                    <label><?php echo __("Dirección"); ?>:</label>
                    <input id="s_address" name="s_address" value="" type="text" maxlength="50" size="30"/>
                </div>
                <div>
                    <label><?php echo __("Teléfono"); ?>:</label>
                    <input id="s_phone" name="s_phone" value="" type="text" maxlength="30" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Fax"); ?>:</label>
                    <input id="s_fax" name="s_fax" value="" type="text" maxlength="30" size="30" alt="number"/>
                </div>
                <div>
                    <label><?php echo __("Celular"); ?>:</label>
                    <input id="s_cellphone" name="s_cellphone" value="" type="text" maxlength="30" size="30" alt="number"/>
                </div>
            </div>
        </fieldset>
        <input type="hidden" name="supplier_id" id="supplier_id" value="0" />
    </form>
</div>

