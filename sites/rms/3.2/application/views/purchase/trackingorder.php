<style type="text/css"> 
    div.divTable{width: 350px;margin: 0 auto;}
    .div_gridPurchaseOrder{
        margin-left: auto;
        margin-right: auto;
        width: 1200px;
        padding-top: 15px;
    }
    .div_gridPurchaseOrder{        
        padding-top: 25px;
    }
    .label_output{clear: both;margin-bottom: 10px;float: left;}
    label.frmLabel {
        float: left;
        font-weight: bold;
        line-height: 16px;
        width: 150px;
    }
    .label_right{float: left;width: 150px;margin-right: 15px;}
    .buttonsContainer{
        width: 600px;
    }
    input.date{line-height: 16px; width: 160px;}
    .frmLabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 16px;
        padding-top: 5px;
        width: 200px;
    }
    #search_type>div{
        padding-top:5px;
    }
    .ui-jqgrid tr.jqgrow td {white-space: normal; padding: 3px;}
    .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column{
        white-space: normal;
    }
    .ui-state-highlight  input[type=text], .ui-widget-content textarea.ui-widget-content
    {background-image: none;}
    .ui-jqgrid .ui-jqgrid-htable th div { height: 35px;}
    #dlg_purchaseorder_detail ul{position: relative;}
    #dlg_purchaseorder_detail ul li{padding: 0 0 5px 0;position: relative;}
    #dlg_purchaseorder_detail label{font-weight: bold;}
    #dlg_purchaseorder_detail dd {padding-left: 10px;}
    #dlg_purchaseorder_detail dd {padding-top: 2px;padding-bottom: 2px;line-height: 16px;}
    #purchaseorder_detail ul{position: relative;}
    #purchaseorder_detail ul li{padding: 0 0 5px 0;position: relative;}
    #purchaseorder_detail label{font-weight: bold;width: 100px;}
    #purchaseorder_detail input{font-weight: bold;width: 100px;}
    #purchaseorder_detail dd{padding-left: 10px;}
    #purchaseorder_detail dd,#purchaseorder_detail dt{padding-top: 2px;padding-bottom: 2px;line-height: 16px;}
    .codePurchaseOrder {font-size: 20px;text-align: center;}
    .codePurchaseOrder,.amountDisplay{font-size: 18px;text-align: center;padding: 5px;}
    .divLeft{float: left; width: 150px; }
    .divLine.divLineShort{width:370px;height: 153px;}
    .divRigth{float: right; width: 150px;}
    .right{float: right;}
    #quantityProduct, #priceUnit,#priceTotal{width: 70px;text-align: right;margin-right: 10px;padding: 5px;}
    #addPurchase{margin: 5px 47px;}
    .search_product_wrapper input{ 
        width: 240px;        
        border-top: transparent;
        border-bottom: transparent;
        border-right: transparent;
        -moz-border-radius-bottomleft: 0; -webkit-border-bottom-left-radius: 0; -khtml-border-bottom-left-radius: 0; border-bottom-left-radius: 0;
        -moz-border-radius-topleft: 0; -webkit-border-top-left-radius: 0; -khtml-border-top-left-radius: 0; border-top-left-radius: 0;
    }
    .search_product_wrapper{background-image: none;margin-right: 5px; float:left;}
    .search_product_wrapper span{margin: 3px 4px;}
    input.inputQuantity{width: 100px;text-align: right;}
    input.inputUnit{width: 110px; text-align: right;}
    input.inputAmountUnit{width: 110px; text-align: right;}
    input.inputAmountTotal{width: 110px; height: 20px; text-align: right;float: left;margin-top: 5px;margin-right: 5px;}
    input.inputData{width: 253px;}
    input.helptext {
    color: #CCCCCC;
    font-style: italic;
    }   
    .ui-jqgrid .ui-pg-selbox{
        height: 21px;
    }
</style> 
<script type="text/javascript">
    /*--url--*/
    var url_listPurchaseOrder = '/private/trackingorder/listPurchaseOrder'+jquery_params;
    var url_listPurchaseOrderDetail = '/private/trackingorder/listPurchaseOrderDetail'+jquery_params;
    var url_getPurchaseOrderDetail = '/private/trackingorder/getPurchaseOrderDetail'+jquery_params;
    var url_cancelPurchaseOrder = '/private/trackingorder/deletePurchaseOrder'+jquery_params;
    var url_getPurchaseOrderDetailById = '/private/trackingorder/getPurchaseOrderDetailById'+jquery_params;
    var url_editPurchaseOrderDetail = '/private/trackingorder/createorUpdateOrderDetail'+jquery_params;
    var url_deletePurchaseOrderDetail = '/private/trackingorder/deletePurchaseOrderDetail'+jquery_params;
    var url_undoPurchaseOrder = '/private/trackingorder/updatePurchaseOrder'+jquery_params;
    var url_updatePurchaseOrder = '/private/trackingorder/updateStatus'+jquery_params;
    var url_getHistoryPurchaseOrderByIdPurchaseOrder = '/private/trackingorder/getHistoryPurchaseOrderByIdPurchaseOrder'+jquery_params;
    var url_annularPurchaseOrder = '/private/trackingorder/annularPurchaseOrder'+jquery_params;
    var url_getProduct = '/private/purchaseorders/getProduct'+jquery_params;
    var url_addPurchaseOrder = '/private/trackingorder/addDetailPurchaseOrder'+jquery_params;
    /*--text--*/     
    var cancelSubTitle = "<?php echo __("Anular Compra"); ?>";
    var closeSubTitle = "<?php echo __("Cancelar Compra"); ?>";
    var confirmCloseMessage = "<?php echo __("¿Está seguro de cancelar esta Orden de Compra?"); ?>";
    var confirmCancelMessage= "<?php echo __("¿Está seguro de eliminar esta Orden de Compra?"); ?>";
    var deleteSubTitle = "<?php echo __("Eliminar Detalle"); ?>";
    var confirmDeleteMessage = "<?php echo __("¿Está seguro de eliminar el Detalle de Compra?"); ?>";
    var undoSubTitle = "<?php echo __("Recuperar Compra"); ?>";
    var confirmUndoMessage = "<?php echo __("¿Está seguro de recuperar esta Orden de Compra?"); ?>";
    var updateSubTitle = "<?php echo __("Orden de Compra"); ?>";
    var confirmUpdateMessage = "<?php echo __("¿Está seguro de aprobar esta Orden de Compra?"); ?>";
    var confirmUpdateDiscouragedMessage = "<?php echo __("¿Está seguro de desaaprobar esta Orden de Compra?"); ?>";
    var confirmUpdateAnnularMessage = "<?php echo __("¿Está seguro de anular esta Orden de Compra?"); ?>";
    var purchaseorder_frm_title = "<?php echo __("Orden de Compra"); ?>";
    var editSubtitle = "<?php echo __("Detalle de Compras"); ?>";
    var quantityProduct_required = "<?php echo __("La Cantidad es obligatoria."); ?>";
    var quantity_required = "<?php echo __("La Cantidad debe ser mayor a cero."); ?>";
    var priceUnit_required = "<?php echo __("El Precio Unitario es obligatorio."); ?>";
    var price_required = "<?php echo __("El Precio debe ser mayor a cero."); ?>";
    /*--grid--*/   
    var grid_PurchaseOrder;
    var historyPurchaseOrders_jqGrid;
    var grid_GlobalPurchaseOrder;
    var validator_PurchaseOrderDetail;
    var tbd = '';
    var formPurchaseOrdersDetail;
    var supplier= '';
    var arrayDetails = new Object();
    var iCountItem = 1;
    var edit_product = 0;
    var countarray = 0;
    var keycountarray = 0;
    var aIDs;
    var nrodocument= '';
    var sd= '';
    var ed= '';
    var a= '';
    var m= '';
    var d= '';
    var tb = '';
    var f;
    
    var sTxtSearchProduct = '<?php echo __('Búsqueda de Producto') ?>';
    var purchase_detail_description = '<?php echo __("El Producto es obligatorio"); ?>';
    var purchase_detail_quantity = '<?php echo __("La Cantidad es obligatoria."); ?>';
    var purchase_detail_amountUnit = '<?php echo __("EL Precio Unitario es requerido."); ?>';
    var purchase_detail_confirm = '<?php echo __("Usted debe agregar un detalle a la Orden de Compra."); ?>';

    var msgConfirmRegister = "<?php echo __("¿Desea confirmar el registro de una orden de compra?"); ?>";
    var titleConfirmRegister = "<?php echo __("Orden de Compra"); ?>";
    
    $(document).on("ready", function(){
        /*--datepicker--*/
        $( "#accountsreceivable_date,#accountsreceivable_bdaterango,#accountsreceivable_fdaterango" ).datepicker(
        {
            dateFormat: '<?php echo $jquery_date_format; ?>'       
        });
        $('#accountsreceivable_listMonth,#anio').combobox();
       
       /*--jqgrid de LISTA DE ORDENES DE COMPRA--*/
        grid_PurchaseOrder = $('#purchaseOrders_jqGrid').jqGrid({
            url: url_listPurchaseOrder,
            datatype: 'json',
            mtype: 'POST', 
            width:1200,
            height:'auto',
            rowList: [10,20,50,100],
            colNames: [
                'idOrder',
                '<?php echo __("Nro de Orden") ?>',
                '<?php echo __("Nro Documento") ?>',
                '<?php echo __("Proveedor") ?>',
                '<?php echo __("Fecha de Emisión") ?>',
                '<?php echo __("Tipo de Pago") ?>',
                '<?php echo __("Monto Total") ?>',
                '<?php echo __("Estado de Seguimiento") ?>',
                '<?php echo __("Estado ") ?>',
                '<?php echo __("Detalle") ?>'                
            ],
            colModel:[
                {name:'idOrder',index:'idOrder',key:true,hidden:true,hidedlg:true},
                {name:'PurchaseOrder',index:'PurchaseOrder',width:100,align:'center',search:false},
                {name:'DocumentNumber',index:'Document',width:70,align:'center',search:true},
                {name:'Proveedor',index:'Proveedor',width:140,align:'center',search:true},
                {name:'DatePurchaseOrder',index:'DatePurchaseOrder',width:75,align:'center',search:true},
                {name:'PurchasePayment',index:'PurchasePayment',width:70,align:'center',search:true},
                {name:'TotalAmount',index:'TotalAmount',width:65,align:'center',search:true},
                {name:'StatusPurchase',index:'StatusPurchase',search:false,width:65,align:'center'},
                {name:'status',index:'status',hidden:true},
                {name:'actions',index:'actions',width:80,search:false,align:'center'}
            ],
            pager:"#purchaseOrders_jqGrid_pager",
            gridview: true,
            rowNum: 100,
            rownumbers: true,
            rowList:[10,25,50,100],
            sortname: 'DatePurchaseOrder',
            viewrecords: true,
            sortorder:"desc",
            multiselect: false,
            subGrid:true,
            caption: '<?php echo __("Lista de Ordenes de Compra"); ?>',
            subGridRowExpanded: function(subgrid_id, row_id){
               
                var subgrid_purchaseorder, subgrid_purchaseorder_pager;
               
                subgrid_purchaseorder = subgrid_id+"_t";
               
                subgrid_purchaseorder_pager = "p_"+subgrid_purchaseorder; 
               
                $("#"+subgrid_id).html("<table id='"+subgrid_purchaseorder+"' class='scroll'></table><div id='"+subgrid_purchaseorder_pager+"' class='scroll'></div>");
               
                jQuery("#"+subgrid_purchaseorder).jqGrid({
                    url:url_listPurchaseOrderDetail,
                    datatype:"jsonp",
                    mtype:'POST',
                    postData:{
                        idOrder: function(){
                            return row_id;
                        }
                    },
                    colNames:[
                        'idOrderDetail',
                        'idOrder',  
                        '<?php echo __("Código de Producto") ?>',
                        '<?php echo __("Descripción") ?>',
                        '<?php echo __("Unidad") ?>',
                        '<?php echo __("Cantidad") ?>',
                        '<?php echo __("Precio Unitario") ?>',
                        '<?php echo __("Importe") ?>',
                        '<?php echo __("Status") ?>',
                        '<?php echo __("Opciones") ?>'
                    ],
                    colModel:[
                        {name:"idOrderDetail", index:"idOrderDetail", hidden:true,width:80},
                        {name:"idOrder", index:"idOrder", hidden:true,width:80},
                        {name:"CodeProduct", index:"CodeProduct",width:100, align:"center"},
                        {name:"Description",index:"Description",width:210,align:"center"},
                        {name:"UnitMeasures",index:"UnitMeasures",width:115,align:"center"},
                        {name:"Quantity",index:"UnitMeasures",width:90,align:"center"},
                        {name:"PriceUnit",index:"PriceUnit",width:100,align:"right"},
                        {name:"TotalAmount",index:"TotalAmount",width:100,align:"right"},
                        {name:'Status',index:'Status',hidden:true},
                        {name:"Opciones",index:"Opciones",width:90,align:"center"}
                        
                    ],
                    rowNum:20,
                    pager:subgrid_purchaseorder_pager,
                    sortname: 'idOrderDetail',
                    sortorder: "asc",
                    height: '100%',
                    rownumbers: true,
                    gridComplete:function(){
                        var idsub = $("#"+subgrid_purchaseorder).jqGrid('getDataIDs');
                        for(var i=0 ; i<idsub.length;i++){
                            var optionSub = '';
                            var spSub = $("#"+subgrid_purchaseorder).getRowData(idsub[i]);
                            editSub = "<a class=\"editSub\" style=\"cursor: pointer;\" rel=\""+idsub[i]+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";                        
                            trashSub = "<a class=\"trashSub\" style=\"cursor: pointer;\" rel=\""+idsub[i]+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                            if(spSub.Status == '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_EMITIDO; ?>'){
                                optionSub = editSub + trashSub;
                            }else{
                                optionSub= '';
                            }
                            $("#"+subgrid_purchaseorder).jqGrid('setRowData',idsub[i],{Opciones: optionSub});                            
                        }
                        $('.editSub').button({
                            icons:{primary: 'ui-icon-pencil'},
                            text:false
                        });
                        $('.trashSub').button({
                            icons:{primary: 'ui-icon-trash'},
                            text:false 
                        });
                        $('.trashSub').click(function(){
                            var id=$(this).attr("rel");
                            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                                if(response){
                                    $.ajax({
                                        url:url_deletePurchaseOrderDetail,
                                        data:{idpod:id},
                                        success: function(response){
                                            if(response.code == code_success){
                                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                                            }else{
                                                msgBox(response.msg, response.code)
                                            }
                                        }
                                    });
                                }
                            });
                        });
                        $('.editSub').on('click',function(){
                            var id=$(this).attr('rel');
                            $.ajax({
                                url:url_getPurchaseOrderDetailById,                        
                                type:'POST',
                                dataType:'json',
                                data:{idpod:id},
                                beforeSend:function(){
                                    $("#loading").dialog("open");
                                },
                                complete:function(){
                                    $("#loading").dialog("close");
                                },
                                success:function(response){
                                    if(evalResponse(response)){
                                        var data=response.data;
                                        $(".detailProduct").text(data.product);
                                        $(".unitProduct").text(data.productUnitMeasures);
                                        $("#quantityProduct").val(data.quantity);
                                        $("#priceUnit").val(data.amountUnit);
                                        $("#priceTotal").val(data.totalAmount);
                                        $("#idPurchaseOrderDetail").val(data.idPurchaseOrderDetail);
                                        $("#idPurchaseOrder").val(data.idPurchaseOrder);
                                        $("#idProduct").val(data.idProduct);
                                        calculationPriceDetail();
                                        $('#purchaseorder_detail').dialog('open');
                                    }
                                }
                            });                            
                        });
                    }
                });                 
            },
            gridComplete:function(){
                var ids = $('#purchaseOrders_jqGrid').jqGrid('getDataIDs');
                for(var i=0 ; i<ids.length;i++){                   
                    var options = '';
                    var sp = $('#purchaseOrders_jqGrid').getRowData(ids[i]);
                    //console.log(sp);
                    add= "<a class=\"add\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Agregar'); ?>\" ><?php echo __('Agregar'); ?></a>";
                    cancel = "<a class=\"cancel\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Cancelar'); ?>\" ><?php echo __('Cancelar'); ?></a>";
                    //falta  .on(annular)
                    annular = "<a class=\"annular\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Anular'); ?>\" ><?php echo __('Anular'); ?></a>";
                    undo = "<a class=\"undo\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Recuperar'); ?>\" ><?php echo __('Recuperar'); ?></a>";
                    approved = "<a class=\"approved\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Aprobar'); ?>\" ><?php echo __('Aprobar'); ?></a>";
                    discouraged = "<a class=\"discouraged\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Desaprobar'); ?>\" ><?php echo __('Desaprobar'); ?></a>";
                    detail = "<a class=\"detail\" style=\"cursor: pointer;\" data-id=\""+ids[i]+"\" title=\"<?php echo __('Historial'); ?>\" ><?php echo __('Historial'); ?></a>";
                    close = "<a class=\"close\" style=\"cursor: pointer;\" rel=\""+ids[i]+"\" title=\"<?php echo __('Cancelar'); ?>\" ><?php echo __('Cancelar'); ?></a>";
                    options = approved;
//                    console.log(options);
                    switch(sp.status){
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_EMITIDO; ?>':
                            options += cancel+add+detail;
                            break;
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_CANCELADO; ?>':
                            $('#purchaseOrders_jqGrid tr#'+ids[i]).addClass('ui-state-error');
                            options = undo+detail;
                            break;
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_APROBADO; ?>':
                            $('#purchaseOrders_jqGrid tr#'+ids[i]).addClass('ui-state-highlight');
                        //Agregar 
                            options = discouraged+cancel+detail;
                            break;
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_RECEPCIONADO; ?>':
                            options = annular+detail;
                            break;
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_DESAPROBADO; ?>':
                            options = approved+detail;
                            break;
                        case '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_FACTURADO; ?>':
                            options = '';
                            break;
                        case '<?php  echo Rms_Constants::PURCHASE_ORDER_STATUS_ANULADO; ?>':
                            options = undo+detail;
                            break;
                        default: 
                            break;
                    }
       
                    $("#purchaseOrders_jqGrid").jqGrid('setRowData',ids[i],{actions: options });
                  
                }
                $('.edit').button({icons:{primary: 'ui-icon-pencil'},text: false});
                $('.add').button({icons:{primary: 'ui-icon-plus'},text: false});
                $('.cancel').button({icons:{primary: 'ui-icon-cancel'},text: false});
                $('.annular').button({icons:{primary: 'ui-icon-cancel'},text: false});
                $('.undo').button({icons:{primary: 'ui-icon-arrowreturnthick-1-s'},text: false});
                $('.approved').button({icons:{primary: 'ui-icon-transferthick-e-w'},text: false});
                $('.discouraged').button({icons:{primary: 'ui-icon-trash'},text: false});
                $('.detail').button({icons:{primary: 'ui-icon-suitcase'},text: false});
                $('.close').button({icons:{primary: 'ui-icon-close'},text: false});
                
            }
        });
        
//        function loadJqgrid(id){
//        
//        console.log(id);return false;
        historyPurchaseOrders_jqGrid = $('#historyPurchaseOrders_jqGrid').jqGrid({
            url: url_getHistoryPurchaseOrderByIdPurchaseOrder,
            datatype: 'json',
            mtype: 'POST',
            width:700,
            height:'auto',
            rowList: [10,20,50,100],
            colNames: [
                'idhistory',
                'idpurchaseorder',
                '<?php echo __("Estado") ?>',
                '<?php echo __("Fecha") ?>',
                'iduser',
                '<?php echo __("Usuario") ?>'
                      
            ],
            colModel:[
                {name:'idhistory',index:'idhistory',key:true,hidden:true,hidedlg:true},
                {name:'idpurchaseorder',index:'idpurchaseorder',hidden:true,hidedlg:true},
                {name:'status',index:'status',width:70,align:'center',search:true},
                {name:'date',index:'date',width:140,align:'center',search:true},
                {name:'iduser',index:'iduser',hidden:true,hidedlg:true},
                {name:'nameuser',index:'nameuser',width:70,align:'center',search:true}
            ],
            pager:"#historyPurchaseOrders_jqGrid_pager",
            gridview: true,
            rowNum: 10,
            rownumbers: true,
            rowList:[10,25,50,100],
            sortname: 'idhistory',
            viewrecords: true,
            sortorder:"asc",
            multiselect: false,
            afterInsertRow:function(){},
            gridComplete:function(){}
        });
//      }
        /*--event click--*/
        $("#idSupplier").combobox();
        $('.buttonsContainer').buttonset();
        fnLoadCheck();
        
        //fecha
        $('.tbd_group').hide();
        $('.tbm_group').hide();
        $('.tbr_group').hide();
        $("#listSearchType").combobox({
            selected: function(){
                tipob=$(this).val();
                if(tipob=='tbd'){
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if(tipob=='tbm'){
                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if(tipob=='tbr'){
                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }
                if(tipob=='0'){   
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                }
            }
        });
        //fin fecha
        $("#btn_search").button({
            icons: {
                primary: "ui-icon-search"                
            },
            text:true
        }).click(function(){
            $("#purchaseOrders_jqGrid").jqGrid("clearGridData");
            searchType = $('input[name=for_purchase]:checked').val();
            nrodocument = $("#document").val(); //documento
            supplier = $('#idSupplier').val(); //proveedor  
            tb = $('#listSearchType').val();
            d = $("#accountsreceivable_date").val();
            sd = $("#accountsreceivable_bdaterango").val();
            ed = $("#accountsreceivable_fdaterango").val();
            a = $("#anio").val();
            m = $("#accountsreceivable_listMonth").val();  
           
            
            if(nrodocument!=''){
                    grid_PurchaseOrder.setGridParam({
                        postData: {
                              searchType:function(){
                                return searchType;
                            },
                             nrodocument:function(){
                                return nrodocument;
                            }
                        }
                    }).trigger("reloadGrid");
                } else{
                if(supplier!=''){
                    grid_PurchaseOrder.setGridParam({
                        postData: {
                              supplier:function(){
                                return supplier;
                            }
                        }
                    }).trigger("reloadGrid");
                }
                
             if(tb=="tbd"){
                    grid_PurchaseOrder.setGridParam({
                        postData: {
                            supplier:function(){
                                return supplier;
                            },
                            d:function(){
                                return d;
                            },
                            searchType:function(){
                                return searchType;
                            },
                            nrodocument:function(){
                                return nrodocument;
                            }
                        }
                    }).trigger("reloadGrid");
                }
                if(tb=="tbr"){
                    grid_PurchaseOrder.setGridParam({
                        postData: {
                            supplier:function(){
                                return supplier;
                            },
                            sd:function(){
                                return sd;
                            },
                            ed: function(){
                                return ed;
                            },
                            searchType:function(){
                                return searchType;
                            },
                            nrodocument:function(){
                                return nrodocument;
                            }                        
                        }
                    }).trigger("reloadGrid");
                }
                if(tb=="tbm"){
                    if ($("#accountsreceivable_listMonth").val()  == 0 || $('#anio').val() == 0){
                        alert ('<?php echo __("Seleccione Mes y Año"); ?>');
                    }else{
                        grid_PurchaseOrder.setGridParam({
                            postData: {
                                supplier:function(){
                                    return supplier;
                                },
                                m:function(){
                                    return m;
                                },
                                a: function(){
                                    return a;
                                },
                                searchType:function(){
                                    return searchType;
                                },
                                nrodocument:function(){
                                return nrodocument;
                                }  
                            }
                        }).trigger("reloadGrid");
                    }
                }
                 }
                $("#purchaseOrders_jqGrid").trigger('reloadGrid');
            });
      
        
        /*-- opción de agregar detalle --*/
        var id;
        $('#purchaseOrders_jqGrid').on('click','.add',function(){
            $("#div_newProductDetail").dialog('open');
            id=$(this).attr("rel");
        });
        
        $('#purchaseOrders_jqGrid').on('click','.detail',function(){
            id=$(this).data('id');
            paramGrid(id)

            $("#div_historyPurchaseOrder").dialog('open');
        });
        
        
        $("#div_historyPurchaseOrder").dialog({
            title: "Seguimiento de Orden de Compra",
            autoOpen:false,
//            autoSize:true,
//            modal:true,discouraged
            width:890,
            height:300,
//            resizable:false,
            closeOnEscape:true,
//            dragable: false,
            buttons: {
                "<?php echo __("Aceptar"); ?>":function(){
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
        });
        
        $("#div_newProductDetail").dialog({
            title: "Agregar Detalle",
            autoOpen:false,
//            autoSize:true,
//            modal:true,discouraged
            width:860,        
//            resizable:false,
            closeOnEscape:true,
//            dragable: false,
            buttons: {
                "<?php echo __("Guardar"); ?>":function(){
                      var arrayDataGrid = $("#purchaseDetail_jqGrid").jqGrid("getRowData");
                  //  console.log(arrayDataGrid);
                      if(arrayDataGrid.length>0){
                      confirmBox(msgConfirmRegister,titleConfirmRegister,function(event){
                          if(event){
                              var dataPurchaseOrder = new Object();
                              dataPurchaseOrder.header = id;
                              dataPurchaseOrder.detail = arrayDetails;
                              showLoading=1;
                              $.ajax({
                                  url:url_addPurchaseOrder,
                                  type:"POST",
                                  dataType:'json',
                                  data: dataPurchaseOrder,
                                  async:false,
                                  success: function(response){
                                      if(evalResponse(response)) {
                                          window.location.reload(); 
                                      }
                                  }
                              });                        
                          }
                      });                                 
                  }else{
                      msgBox(purchase_detail_confirm);
                  }
                  return false;
                },
                "<?php echo __("Cancelar"); ?>": function() {
                    formPurchaseOrdersDetail.resetForm();
                    tblPurchaseDetail.clearGridData();
                    $(this).dialog('close');
                    $('#errorMessages').hide();
                }
            }
        });
        
        // click
        $('#addPurchase').button({icons:{primary: 'ui-icon-plus'}});
        $('#chars_product').helptext({value: sTxtSearchProduct, customClass: 'lightSearch'});
        $("#chars_product" ).autocomplete({
            minLength: 0,
            autoFocus: true,
            position: {offset: "-25px 2px"},
            appendTo: '#auto_div_product',
            source:function(req,response){
                $.ajax({
                    url:url_getProduct,
                    dataType:'jsonp',
                    data: {
                        term : req.term
                    },
                    success:function(resp){
                        if(evalResponse(resp)){
                            response( $.map( resp.data, function( item ) {
                                item_icon = 'ui-icon-help';
                                var text = "<span class='spanSearchLeft ui-icon " + item_icon + "'></span>";
                                text += "<span class='spanSearchLeft'>" + item.product + "</span>";
                                text += "<div class='clear'></div>";
                                return {
                                    label:  text.replace(
                                    new RegExp(
                                    "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(req.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                ), "<strong>$1</strong>" ),
                                    value:  item.product,
                                    id : item.idProduct,
                                    unit : item.productUnitMeasures
                                }
                            }));
                        }else{
                            msgBox(resp.msg,resp.code);
                        }
                    }
                });
            },
            focus: function (event, ui){
                $('#productUnitMeasures').val(ui.item.unit);
            },
            select: function( event, ui ) {
                $('#idProduct').val(ui.item.id); 
                $('#productUnitMeasures').val(ui.item.unit);
            }
        }).data( "autocomplete" )._renderItem = function( ul, item ) {            
            return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.label + "</a>" )
            .appendTo( ul );
        };
        
        $( "#chars_product" ).data( "autocomplete" )._resizeMenu = function () {
            var ul = this.menu.element;
            ul.css({width: 290, 'margin-bottom': 5});            
        };
        /*--Función para calcular el Precio SubTotal--*/
        $("#amountUnit, #amountTotal").priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        }); 
        $("#quantity").setMask();
        $('#quantity').keyup(function(key){
            if($('#amountUnit').val()!=""){
                calculationPriceDetail2();
            }
        }); 
        
        $('#amountUnit').keyup(function(key){
            if($('#quantity').val()!=""){
                calculationPriceDetail2();
            }
        }); 
        // Funcion para calcular el precio
   
        calculationPriceDetail2 = function(){
            amountUnit = $('#amountUnit').val().split(' ').join('');
            amountUnit = amountUnit==''?0:parseFloat(amountUnit);
            
            fPreTotal = amountUnit;
            
            quantity = $('#quantity').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            amountTotal = amountUnit * quantity;
            $('#amountTotal').val(amountTotal.toFixed(2));
        }
         /*--Formulario de Detalle de Compras--*/
        // Validar los datos 
        formPurchaseOrdersDetail = $("#formPurchaseOrdersDetail").validate({
            rules: {
                idProduct: {required: true},
                quantity: {required: true},
                amountUnit: {required: true}
                //            description_note: {required:true}
            },
            messages:{
                idProduct: {required: purchase_detail_description},
                quantity: {required: purchase_detail_quantity},
                amountUnit: {required: purchase_detail_amountUnit}
                //      description_note:{required: purchase_detail_note}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer:"#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass){
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass){
                $(element).removeClass('ui-state-error');
            } 
        });
        // CLick agregar   
        $("#addPurchase").click(function(){
                if(!$("#formPurchaseOrdersDetail").valid())
                return false;
                var itemDetail = new Object();
                itemDetail.product = $('#chars_product').val();
                itemDetail.productUnitMeasures = $('#productUnitMeasures').val();
                itemDetail.quantity = $('#quantity').val();
                itemDetail.amountUnit = $('#amountUnit').val();
                itemDetail.amountTotal = $('#amountTotal').val();
                itemDetail.idProduct = $('#idProduct').val().split(' ').join('');
                returnarray(itemDetail.idProduct);
            
                if(edit_product==1){
                    arrayDetails[keycountarray] = itemDetail;
                    tblPurchaseDetail.setRowData(keycountarray,{
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:itemDetail.quantity,
                        amountUnitOut:itemDetail.amountUnit,
                        amountTotalOut:itemDetail.amountTotal,
                        idProductOut:itemDetail.idProduct
                    });
                    edit_product = 0;
                    keycountarray = 0;  
                }else if(countarray == 0 && keycountarray==0 ){
                    arrayDetails[iCountItem] = itemDetail;
                    tblPurchaseDetail.addRowData(iCountItem, {
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:itemDetail.quantity,
                        amountUnitOut:parseFloat(itemDetail.amountUnit).toFixed(2),
                        amountTotalOut:parseFloat(itemDetail.amountTotal).toFixed(2),
                        idProductOut:itemDetail.idProduct            
                    });
                    iCountItem++;
                }else{
                    arrayDetails[keycountarray].quantity = parseInt(arrayDetails[keycountarray].quantity) + parseInt(itemDetail.quantity);
                    arrayDetails[keycountarray].amountUnit = (parseFloat(arrayDetails[keycountarray].amountUnit)+parseFloat(itemDetail.amountUnit)).toFixed(2);
                    arrayDetails[keycountarray].amountTotal = (parseFloat(arrayDetails[keycountarray].amountTotal)+parseFloat(itemDetail.amountTotal)).toFixed(2);

                    tblPurchaseDetail.setRowData(keycountarray, {
                        descriptionOut:itemDetail.product,
                        productUnitMeasuresOut:itemDetail.productUnitMeasures,
                        quantityOut:arrayDetails[keycountarray].quantity,
                        amountUnitOut:arrayDetails[keycountarray].amountUnit,                    
                        amountTotalOut:arrayDetails[keycountarray].amountTotal,
                        idProductOut:itemDetail.idProduct
                    });
                    countarray = 0;tbm
                    keycountarray = 0;
                }
                tblPurchaseDetail.trigger("reloadGrid"); 
                setTimeout(function(){
                    formPurchaseOrdersDetail.resetForm();
                    $('#chars_product').focus();                   
                }, 100);
                
                $('#idProduct').val('');
        });
        //  Llena el jqGrid    
        function returnarray(idProduct){
            $.each(arrayDetails, function(key, value) { 
                if($('#idProduct').val() == value.idProduct){
                    countarray = 1;
                    keycountarray = key;
                    quantity = value.quantity;
                }
            });
        
        }
        tblPurchaseDetail = $("#purchaseDetail_jqGrid").jqGrid({            
            height: '100%',                        
            width:'840',
            datatype: 'local',
            sortable: false,
            colNames:[                
                'IdProduct',
                '<?php echo __("Unidad") ?>',
                '<?php echo __("Descripción") ?>',
                '<?php echo __("Cantidad") ?>',
                '<?php echo __("Precio Unitario") ?>',                
                '<?php echo __("Importe") ?>',
                '<?php echo __("Opción") ?>'
            ],
            colModel:[                
                {name:'idProductOut',index:'idProductOut',hidden:true},    
                {name:'productUnitMeasuresOut',index:'productUnitMeasuresOut',width:60,align:'left'},
                {name:'descriptionOut',index:'descriptionOut',width:200},
                {name:'quantityOut',index:'quantityOut',width:80, align: 'center'},
                {name:'amountUnitOut',index:'amountUnitOut', width:80, align: 'right'},             
                {name:'amountTotalOut',index:'amountTotalOut', width:100, align: 'right'},
                {name:'actions',index:'actions', width:50, align: 'center'}
            ],
            viewrecords: true,
            sortname: 'idOut',            
            gridview : true,
            rownumbers: true,      
            rownumWidth: 40,            
            footerrow:true,
            gridComplete:function(){
                try{
                    aIDs = tblPurchaseDetail.getDataIDs();
                    $.each(aIDs,function(){                   
                        editOption = "<button class='editItem' type='button' data-id='"+this+"'>Editar</button>"
                        deleteOption = "<button class='deleteItem' type='button' data-id='"+this+"'>Eliminar</button>"
                        tblPurchaseDetail.setRowData(this,{                        
                            actions: editOption + deleteOption
                        });
                    });
                
                    $('.deleteItem').button({icons: {primary: 'ui-icon-trash'},text: false});
                    $('.editItem').button({icons: {primary: 'ui-icon-pencil'},text: false});
                    $('.deleteItem').click(function(){
                        var id_delete = $(this).data('id');                        
                        confirmBox(confirmDeleteMessage,deleteSubTitle, function(response){
                            if(response){                                
                                delete arrayDetails[id_delete];
                                tblPurchaseDetail.delRowData(id_delete);                            
                            }                            
                        });                        
                    });
                    $('.editItem').click(function(){
                        var id_edit = $(this).data('id');
                        $('#idProduct').val(arrayDetails[id_edit].idProduct);
                        $('#idSupplier').val(arrayDetails[id_edit].idSupplier);
                        $('#productUnitMeasures').val(arrayDetails[id_edit].productUnitMeasures);
                        $('#chars_product').val(arrayDetails[id_edit].product);
                        $('#amountUnit').val(arrayDetails[id_edit].amountUnit);
                        $('#amountTotal').val(arrayDetails[id_edit].amountTotal);
                        $('#quantity').val(arrayDetails[id_edit].quantity);
                        edit_product=1;
                        calculationPriceDetail2();
                    });
                
                    fPreTotal= 0;
                    $.each(arrayDetails, function(){
                        fPreTotal += parseFloat(this.preSubTotal);
                    })
                
                    var aGridData = tblPurchaseDetail.getRowData(),
                    fTotalAmountSum=0;
                    $.each(aGridData,function(i, oRowData){                    
                        fTotalAmountSum += parseFloat(oRowData.amountTotalOut);                    
                    });
                   
                    tblPurchaseDetail.footerData('set',{amountUnitOut:'<?php echo __("Sub-Total") ?>',amountTotalOut:fTotalAmountSum.toFixed(2)});
                    formPurchaseOrdersDetail.currentForm.reset();
            
                }catch(e){
                   
                }
            }
        });
        
              
        /*-- opción de cancelar --*/
        $('#purchaseOrders_jqGrid').on('click','.cancel',function(){
            var id=$(this).data("id");
            confirmBox(confirmCancelMessage,cancelSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_cancelPurchaseOrder,
                        data:{
                            idpo:id
                            , status: '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_CANCELADO; ?>'
                        },
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        });
        
        /*opcion de eliminar*/
        $('#purchaseOrders_jqGrid').on('click','.close',function(){
        var id=$(this).attr("rel");
            confirmBox(confirmCloseMessage,closeSubTitle,function(response){
                if(response){
                 
                }
                 
                });
        });
        
        
        /*-- opción de recuperar --*/
        $('#purchaseOrders_jqGrid').on('click','.undo',function(){
            var id=$(this).data("id");
            confirmBox(confirmUndoMessage,undoSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_undoPurchaseOrder,
                        data:{
                            idpo:id
                            , status: '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_EMITIDO; ?>'
                        },
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        });
        /*-- opción de aprobar --*/
        $('#purchaseOrders_jqGrid').on('click','.approved',function(){
            var id=$(this).data("id");
            confirmBox(confirmUpdateMessage,updateSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_updatePurchaseOrder,
                        data:{
                            idpo:id
                            , status: '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_APROBADO; ?>'
                        },
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        });
        //Opcion de Desaprobar
        $('#purchaseOrders_jqGrid').on('click','.discouraged',function(){
            var id=$(this).data("id");
            confirmBox(confirmUpdateDiscouragedMessage,updateSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_updatePurchaseOrder,
                        data:{
                            idpo:id
                            , status: '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_DESAPROBADO; ?>'
                        },
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        })
        /*
        *Luego de Opcion Recepcionado
        *La pcon de Anular
        */
        $('#purchaseOrders_jqGrid').on('click','.annular',function(){
            var id=$(this).data("id");
            confirmBox(confirmUpdateAnnularMessage,updateSubTitle,function(response){
                if(response){
                    $.ajax({
                        url:url_annularPurchaseOrder,
                        data:{
                            idpo:id
                            , status: '<?php echo Rms_Constants::PURCHASE_ORDER_STATUS_ANULADO; ?>'
                        },
                        success: function(response){
                            if(evalResponse(response)){
                                $('#purchaseOrders_jqGrid').trigger("reloadGrid");
                            }else{
                                msgBox(response.msg, response.code)
                            }
                        }
                    });
                }
            });
        });
        
        /*-- opción detalle de orden de compra --*/

        /*-- dialog del detalle de orden de compra --*/
        $('#dlg_purchaseorder_detail').dialog({
            autoOpen: false,
            resizable: false,
            dragable: false,
            height:'auto',
            width:650,
            modal: true,
            hide: 'drop',
            show: 'drop',
            buttons: {                
                "<?php echo __("Aceptar") ?>": function() {
                    $( this ).dialog( "close" );
                }
            }
        });
        $('#purchaseorder_detail').dialog({
            title: editSubtitle, 
            autoOpen: false,
            resizable: false,
            dragable: false,
            height:'auto',
            width:400,
            modal: true,
            hide: 'drop',
            show: 'drop',
            buttons: {                
                "<?php echo __("Aceptar") ?>": function() {
                    $("#frmPurchaseOrderDetail").submit();
                    $( this ).dialog( "close" );
                }
            }
        });
                
        /* --setMask y priceFormat--*/
        $('#quantityProduct').setMask();        
          
        $('#priceUnit,#priceTotal').priceFormat({
            prefix: '',
            centsSeparator: '.',
            thousandsSeparator: ''
        });
        /* -- Evento de la cantidad y el monto --*/
        $('#quantityProduct').keyup(function(key){
            if($('#priceUnit').val()!=""){
                calculationPriceDetail();
            }
        }); 
        
        $('#priceUnit').keyup(function(key){
            if($('#quantityProduct').val()!=""){
                calculationPriceDetail();
            }
        }); 
        /*--Función para calcular el Precio SubTotal--*/
        calculationPriceDetail = function(){
            amountUnit = $('#priceUnit').val().split(' ').join('');
            amountUnit = amountUnit==''?0:parseFloat(amountUnit);
            
            fPreTotal = amountUnit;
            
            quantity = $('#quantityProduct').val().split(' ').join('');
            quantity = quantity==''?0:parseFloat(quantity);
            
            amountTotal = amountUnit * quantity;
            
            $('#priceTotal').val(amountTotal.toFixed(2));
        }
        /*Funcion de RELOADGrid*/
        function paramGrid(id){
        $('#historyPurchaseOrders_jqGrid').setGridParam({
            postData:{
                idpo:function(){return id}
            }}).trigger("reloadGrid");      
    }
    });
</script>
<div>
    <center>
        <div class="titleTables">
            <span class="title"><?php echo __("Seguimiento de Ordenes de Compra"); ?></span>
        </div>
    </center>
</div>  
<div class="clear"></div> 
<div class="ui-corner-all ui-widget-content" style="margin: 4px; width: 800px; margin: 0 auto;">    
    <div class="padding10">
        <div>
            <dl>
                <dd>
                    <label class="frmLabel"><?php echo __("Tipo de Documento"); ?>:</label>
                </dd>
                <dd class="buttonsContainer">
                    <input type="radio" id="purchaseorder" name="for_purchase" value="purchaseorder" checked="checked"/><label for="purchaseorder">Orden de Compra</label>
                    <input type="radio" id="codepurchase" name="for_purchase" value="codeorder" /><label for="codepurchase">Serie-Correlativo</label>
                </dd>
            </dl>
            <br/>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div>
            <div class="for_purchaseorder">
                <label class="frmLabel"><?php echo __("Documento") ?>:</label>
                <input name="document" id="document" type="text"/>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
             <br/>
        </div>
        <div>
            <label class="frmLabel"><?php echo __("Proveedor"); ?>:</label>
            <select id="idSupplier" name="idSupplier">
                <option value=""><?php echo __("Seleccione una Opción"); ?></option>
                <?php
                foreach ($supplier as $v) {
                    ?>
                    <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
                    <?php
                }
                ?>
            </select> 
            <div class="clear"></div>  
            <br/>
        </div>
        <div class="clear"></div>  
        <div id="search_type">
            <label class="frmLabel"><?php echo __("Tipo de Búsqueda") ?>:</label>
            <div class="item_content">
                <select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival" >
                    <option value="0"><?php echo __("Seleccione una Opción") ?></option>  
                    <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                    <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                    <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                </select>
            </div>
            <div class="clear"></div>
            <div>
                <dl>
                    <dd>
                        <label class="tbd_group frmLabel"><?php echo __("Día:") ?></label>
                        <div class="item_content">
                            <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                        </div>
                    </dd>
                    <dd>
                        <label class="tbm_group frmLabel"><?php echo __("Mes") ?>:</label>
                        <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                            <option value="0"><?php echo __("Seleccione Mes") ?></option>
                            <?php foreach ($a_month_report as $a_dt): ?>
                                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                            <?php endforeach; ?>
                        </select>
                    </dd>
                    <dd>
                        <label class="tbm_group frmLabel"><?php echo __("Año") ?>:</label>
                        <select class="tbm_group" id="anio" name="anio">
                            <option value="0"><?php echo __("Seleccione Año") ?></option>
                            <?php foreach ($a_year_report as $a_dt): ?>
                                <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                            <?php endforeach; ?>
                        </select>
                    </dd>
                    <dd>   
                        <label class="tbr_group frmLabel"><?php echo __("Fecha Inicial") ?>:</label>
                        <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                    </dd>
                    <br/>
                    <dd>
                        <label class="tbr_group frmLabel"><?php echo __("Fecha Final") ?>:</label>
                        <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                    </dd>     
                </dl>
            </div>
            <div class="clear"></div>
        </div>
        <div>
            <button id="btn_search"><?php echo __("Buscar"); ?></button>
        </div>
    </div>
    <div class="clear"></div>  
</div>
<br/>
<div>
 <div class="div_gridPurchaseOrder">
        <center>
            <table id="purchaseOrders_jqGrid"></table>
            <div id="purchaseOrders_jqGrid_pager"></div>
        </center>
    </div>
</div>


<div id="purchaseorder_detail" class="none">
    <form id="frmPurchaseOrderDetail" method="post">
        <ul>
            <li>
                <div class="divLeft">
                    <dl>
                        <dt>
                        <label><?php echo __("Producto") ?>:</label>
                        </dt>
                        <dd class="detailProduct"></dd>
                    </dl>
                    <dl>
                        <dt>
                        <label><?php echo __("Unidad") ?>:</label>
                        </dt>
                        <dd class="unitProduct"></dd>
                    </dl>
                </div>
                <div class="clear"></div>
            </li>
            <li>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="quantity" class="labelprice"><?php echo __("Cantidad") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input id="quantityProduct" name="quantityProduct" class="quantityProduct ui-state-error" type="text" alt="number"/>
                        </dd>
                        </dd>
                    </dl>

                </div>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="amountUnit" class="labelprice"><?php echo __("Precio Unitario") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input class="priceUnit ui-state-highlight" type="text" id="priceUnit" name="priceUnit" />
                        </dd>
                    </dl>

                </div>
                <div class="divLeft">
                    <dl>
                        <dd>
                            <label for="amountTotal" class="labelprice"><?php echo __("Precio Total") ?></label>                    
                        </dd>
                        <dd>                
                            <div class="clear"></div>
                            <input id="priceTotal" name="priceTotal" class="priceTotal ui-state-hover" type="text"/>
                        </dd>
                    </dl>
                </div>
            </li>
        </ul>
        <input name="idPurchaseOrderDetail" id="idPurchaseOrderDetail" type="hidden" value="0"/>
        <input name="idPurchaseOrder" id="idPurchaseOrder" type="hidden" value="0"/>
        <input name="idProduct" id="idProduct" type="hidden" value="0"/>
    </form>
</div>






<!--Agregar un detalle mas-->
<div id="div_newProductDetail">
<form class="orderForm" id="formPurchaseOrdersDetail">

    <li style="list-style: none;">
         <div class="left">
             <div class="divLine divLineShort">
                   <dl>
                        <dd>
                            <label for="chars_product"><?php echo __("Producto"); ?>:</label>
                        </dd>
                        <dd class="relative">
                            <div class="search_product_wrapper ui-corner-all ui-state-default">
                                <input class="autocompleteInput inputData" type="text" id="chars_product" name="chars_product" value="<?php echo 'Búsqueda de Producto' ?>"/>
                                <input type="hidden" id="idProduct" name="idProduct"/>
                                <div class="clear"></div>
                            </div>
                            <div id="auto_div_product"></div>
                            <div class="clear"></div>
                        </dd>
                   </dl>
                  <div class="clear"></div>
                  <dl>
                            <dd>
                                <label for="productUnitMeasures" class="frmlbl label_productUnit"><?php echo __("Medida") ?>:</label>                    
                                 
                            <dd>
                            <dd>
                                <div class="clear"></div>
                                <input class="inputUnit ui-state-highlight" type="text" id="productUnitMeasures" name="productUnitMeasures" disabled/>
                            </dd>
                        </dl>
                        </div>  
                        <div class="divLeft">
                            <div class="divLine divLineShort">

                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="quantity" class="frmlbl"><?php echo __("Cantidad") ?>:</label>                    
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputQuantity ui-state-highlight" type="text" id="quantity" name="quantity" alt="number"/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="amountUnit" class="frmlbl"><?php echo __("Precio Unitario") ?>:</label>
                                    </dd>
                                    <dd>                
                                        <div class="clear"></div>
                                        <input class="inputAmountUnit ui-state-highlight" type="text" id="amountUnit" name="amountUnit" />
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                                <dl>
                                    <dd>
                                        <label for="amountTotal" class="frmlbl label_amountTotal"><?php echo __("SubTotal") ?>:</label>
                                    </dd>
                                    <dd>
                                        <div class="clear"></div>
                                        <input class="inputAmountTotal ui-state-highlight" type="text" id="amountTotal" name="amountTotal" disabled/>
                                    </dd>
                                    <dd>
                                        <button type="button" id="addPurchase" class="ui-state-highlight"><?php echo __("Agregar Compra") ?></button>

                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
    <div class="clear"></div>
    <div class="clear"></div>
    <table id="purchaseDetail_jqGrid"></table>
    <div class="clear"></div>
    <br/>
</li>
<br>      
<div class="clear"></div>

</form>
</div>

<!-- Div del historial de una Orden de Compra -->
<div id="div_historyPurchaseOrder">
<!--    <div class="div_gridHistoryPurchaseOrder">-->
        <center>
            <table id="historyPurchaseOrders_jqGrid"></table>
            <div id="historyPurchaseOrders_jqGrid_pager"></div>
        </center>

</div>
