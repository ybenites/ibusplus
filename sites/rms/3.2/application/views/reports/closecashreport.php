<style type="text/css">
    #div-wapper-button-add{
        position: absolute;
        top: 60px;;
        width: 360px;
        /*        height: 200px;*/
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;

    } 
    #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        /*margin-right: -80px;*/
        padding-left: 5px;
    }
    .option {
        -moz-transform: rotate(270deg);
        -webkit-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }

    #ticketCanceled{float: left;}
    .label_output div label{
        min-width: 90px;
    }
    .label_output div *{
        float: none;
    }
    .ui-autocomplete-input{
        line-height: 15px;
    }
    .ui-button-text-only, .ui-button-text {
        padding: 0.2em 0.7em;
    }
    .selected_office, .selected_user, .selected_city {
        width: 80%;
        float: left;
    }
    .selected_office  div, .selected_user div, .selected_city div{
        padding: 2px;
        float: left;
    }
    .selected_office  div .ui-button-text, .selected_user div .ui-button-text, .selected_city div .ui-button-text{
        float: left;
    }
    .selected_office  div .ui-button-icon-primary, .selected_user div .ui-button-icon-primary, .selected_city div .ui-button-icon-primary{
        float: right;
        cursor: pointer;
    }

    .clear_all, .clear_all_user , .clear_all_city {
        cursor: pointer;
    }

    .icon_luggage_search{  margin: 6px 5px 6px 0;}
    .span_luggage_search{  margin-top: 5px;}
    #mySearch{width:1200px; margin: 0 auto;}

    #msc, #mso, #msu{width: 100%; margin-left:10px; display: none;}

    .space_div{float:left; margin-left: 10px;}

    .luggage_header_tbt{margin:0 auto; margin-top: 10px; padding: 8px 10px; width: 285px;}

    /*    .report_Options{width: 350px;margin-right: 10px;float: left;}*/
    .label_text{width: 120px;float: left;margin-right: 10px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    input.tbd_group {width: 150px;}
    dl dd{ margin: 10px 0;}

    #accountsreceivable_listMonth-combobox{width: 150px;}
</style>

<script type="text/javascript">

    //oficinas
    var url_office_list = '/reports/closecashreport/listOffices' + jquery_params;
    var option_template = '<option value="[VALUE]">[LABEL]</option>';
    var msgSelectEmpty = '<?php echo __("No se encontró información"); ?>';
    var msgSelectDefaultOption = '<?php echo __("Seleccione una opción"); ?>';
    var sel_Office = '<div class="ui-corner-all ui-state-highlight" data-ido="[ID]"><span class="ui-button-text">[OFFICE_NAME]</span><span class="ui-button-icon-primary ui-icon ui-icon-close"></span></div>';
    var aSelectedOffice = [];

    //usuarios
    var url_salesUser = "/reports/closecashreport/getUsersGroupSellers" + jquery_params;
    var sel_user = '<div class="ui-corner-all ui-state-highlight" data-idu="[ID]"><span class="ui-button-text">[USER_NAME]</span><span class="ui-button-icon-primary ui-icon ui-icon-close"></span></div>';
    var aSelectedUsers = [];
    var rer = false;
    var listSearchType = '';

    var of = '';
    var us = '';
    var d = '';
    var sd = '';
    var ed = '';
    var a = '';
    var m = '';
    //fin usuario

    var numero = 0;
    var numero2 = 0;


    $.removeFromArray = function(value, arr) {
        return $.grep(arr, function(elem, index) {
            return elem !== value;
        });
    };

    $(document).ready(function() {
        var $a_office = [];

        $('#btn-add').toggle(
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '+=360'
                    }, 500);
                    $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');

                },
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '-=360'
                    }, 500);
                    eventEditOpen = false;
                    $("#labelChange").text("<?php echo __('Buscar'); ?>");
                    $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');

                }).trigger('click');

        $('#accountsreceivable_listMonth').combobox();
        $('#anio').combobox();

        //lista oficinas
        function listOfficeSellers() {
            $.ajax({
                url: url_office_list,
                data: {},
                async: false,
                success: function(response) {
                    $('option', '#cmbOfficeSellers').remove();
                    if (evalResponse(response)) {
                        if (response.data.length > 0) {
                            $('#cmbOfficeSellers').append(option_template.replace('[VALUE]', '', 'g').replace('[LABEL]', msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#cmbOfficeSellers').append(option_template.replace('[VALUE]', array.id, 'g').replace('[LABEL]', array.value, 'g'));
                            });
                        } else {
                            $('#cmbOfficeSellers').append(option_template.replace('[VALUE]', '', 'g').replace('[LABEL]', msgSelectEmpty, 'g'));
                        }
                    } else {
                        msgBox(response.msg, response.code);
                    }
                    $('#cmbOfficeSellers').combobox({
                        selected: function(event, ui) {
                            if ($.inArray(ui.item.value, aSelectedOffice) < 0) {
                                var str = sel_Office.replace('[ID]', ui.item.value, 'g').replace('[OFFICE_NAME]', ui.item.label, 'g');
                                if ($('.selected_office div').length == 0) {
                                    $('.selected_office').append(str);
                                    $('#mso').fadeIn(500);
                                    $(str).fadeIn(500);
                                } else {
                                    $('.selected_office').append(str);
                                }
                                aSelectedOffice.push(ui.item.value);
                            }
                            listUserSellers();
                            setTimeout(function() {
                                $('#cmbOfficeSellers').val([]);
                                $('#cmbOfficeSellers-combobox').val('');
                            }, 0);
                        }
                    });
                }
            });
        }

        listOfficeSellers();
        //fin funcion lista oficina

        //lista vendedores
        function listUserSellers() {
            $.ajax({
                url: '/reports/closecashreport/getUsersGroupSellers' + jquery_params,
                data: {ido: aSelectedOffice},
                async: false,
                success: function(response) {
                    $('option', '#cmbUserSellers').remove();
                    if (evalResponse(response)) {
                        if (response.data.length > 0) {
                            $('#cmbUserSellers').append(option_template.replace('[VALUE]', '', 'g').replace('[LABEL]', msgSelectDefaultOption, 'g'));
                            $.each(response.data, function(index, array) {
                                $('#cmbUserSellers').append(option_template.replace('[VALUE]', array.id, 'g').replace('[LABEL]', array.name, 'g'));
                            });
                        } else {
                            $('#cmbUserSellers').append(option_template.replace('[VALUE]', '', 'g').replace('[LABEL]', msgSelectEmpty, 'g'));
                        }
                    } else {
                        msgBox(response.msg, response.code);
                    }
                    $('#cmbUserSellers').combobox({
                        selected: function(event, ui) {
                            if ($.inArray(ui.item.value, aSelectedUsers) < 0) {
                                var str = sel_user.replace('[ID]', ui.item.value, 'g').replace('[USER_NAME]', ui.item.label, 'g');
                                if ($('.selected_user div').length == 0) {
                                    $('.selected_user').append(str);
                                    $('#msu').fadeIn(500);
                                    $(str).fadeIn(500);
                                } else {
                                    $('.selected_user').append(str);
                                }
                                aSelectedUsers.push(ui.item.value);

                            }
                            setTimeout(function() {
                                $('#cmbUserSellers').val([]);
                                $('#cmbUserSellers-combobox').val('');
                            }, 0);

                        }
                    });
                }
            });
        }
        listUserSellers();

        //fecha
        $('.tbd_group').hide();
        $('.tbm_group').hide();
        $('.tbr_group').hide();
        $("#listSearchType").combobox({
            selected: function() {
                tipob = $(this).val();
                if (tipob == 'tbd') {
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if (tipob == 'tbm') {

                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if (tipob == 'tbr') {

                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }

                if (tipob == '0') {
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                }
            }
        });

        //darle datepicker a los texbox
        $("#accountsreceivable_date").datepicker(
                {
                    dateFormat: '<?php echo $jquery_date_format; ?>'
                });
        $('#btnSearch').button({
            icons: {primary: 'ui-icon-search'},
            text: true
        });
        $("#accountsreceivable_bdaterango,#accountsreceivable_fdaterango").datepicker(
                {
                    dateFormat: '<?php echo $jquery_date_format; ?>'
                });
        //fin darle datepicker a los texbox

        $('.ui-button-icon-primary').live('click', (function() {
            var e = $(this).parent();
            setTimeout(function() {

                if ($(e).parent().hasClass('selected_office')) {
                    aSelectedOffice = $.removeFromArray($(e).data('ido').toString(), aSelectedOffice);
                    $(e).remove();

                    if (($('.selected_office div').length == 0)) {
                        $('#mso').fadeOut(500);
                    }

                }

                if ($(e).parent().hasClass('selected_user')) {
                    aSelectedUsers = $.removeFromArray($(e).data('idu').toString(), aSelectedUsers);
                    $(e).remove();

                    if (($('.selected_user div').length == 0)) {
                        $('#msu').fadeOut(500);
                    }
                }
            }, 600);
        }));

        $('.clear_all').live('click', (function() {
            var me = $(this);
            var c = $('.selected_office div').length;
            var i = 0;
            $.each($('.selected_office div'), function() {
                $(this).fadeOut(500);
                var e = $(this)
                setTimeout(function() {
                    $(e).remove();
                }, 600);
                i++;
                if (c == i) {
                    $(me).parent().fadeOut(500);
                }
            });

            aSelectedOffice = [];
            //$('#btnSearch').trigger('click');

        }));

        $('.clear_all_user').live('click', (function() {
            var me = $(this);
            var c = $('.selected_user div').length;
            var i = 0;
            $.each($('.selected_user div'), function() {
                $(this).fadeOut(500);
                var e = $(this)
                setTimeout(function() {
                    $(e).remove();
                }, 600);
                i++;
                if (c == i) {
                    $(me).parent().fadeOut(500);
                }
            });

            aSelectedUsers = [];

        }));

        //boton buscar
        $('#btnSearch').click(function() {
            rer = true;
            listSearchType = $('#listSearchType').val();
            idOffice = aSelectedOffice;

            idUser = aSelectedUsers;
            d = $("#accountsreceivable_date").val();
            sd = $("#accountsreceivable_bdaterango").val();
            ed = $("#accountsreceivable_fdaterango").val();
            a = $("#anio").val();
            m = $("#accountsreceivable_listMonth").val();

            if (listSearchType == "0") {
                msgBox('<?php echo __("Seleccione Tipo de Búsqueda"); ?>');
                return false;
            } else {
                var params = new Object();
                if (idOffice.length != 0) {
                    params.idOffice = idOffice;
                }
                if (idUser.length != 0) {
                    params.idUser = idUser;
                }
                if (listSearchType == "tbd")
                    params.d = d;
                if (listSearchType == "tbr") {
                    params.sd = sd;
                    params.ed = ed;
                }
                if (listSearchType == "tbm") {
                    if (m == 0 || a == 0) {
                        msgBox('<?php echo __("Seleccione Mes y Año"); ?>');
                        return false;
                    } else {
                        params.a = a;
                        params.m = m;
                    }
                }
                $('#report_options').iReportingPlus('option', 'dynamicParams', params);
                $('#report_options').iReportingPlus('generateReport', 'html');

                $('#btn-add').trigger('click');
            }
        });
        //fin boton buscar
        
        $('#report_options').iReportingPlus({
            domain: domain,
            staticParams: rParams,
            repopt: ['html', 'pdf', 'xlsx'],
            clientFolder: 'rms',
            clientFile: 'RMS_MY_DAYLY_CASHBOX_OFFICE',
            htmlVisor: true,
            urlData: '/reports/closecashreport/reportCashReportByOffice',
            xpath: '/report',
            orientation: 'horizontal',
            jqUI: true,
            responseType: 'jsonp',
            reportZoom: 1.2,
            jqCallBack: jquery_params
        });
    });
</script>

<center><div class="titleTables"><?php echo __("Venta de Boletos"); ?>
        <span>
            <ul class="menu_right">
                <div id="help" class="ui-state-default ui-corner-all" style="padding: 5px;height: 16px;width: 16px;float: left;cursor: pointer; display: none;">
                    <img title="<?php echo __("Ayuda de Usuario"); ?>" alt="<?php echo __("Ayuda de Usuario"); ?>"  src="/media/ico/stock_help-agent.png">
                </div>
            </ul>
        </span>
    </div></center>

<div id="div-wapper-button-add" class="ui-corner-all ui-state-default" >

    <center><div  style=" font-size:20px "><?php echo __("Filtro de Busqueda"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedao"> 
                    <div>
                        <label class="label_text"><?php echo __("Oficina"); ?>:</label>
                        <div class="item_content">
                            <select id="cmbOfficeSellers" name="cmbOfficeSellers" style="width: 190px;">
                                <option value=""><?php echo __('Seleccione una opción'); ?></option>
                            </select>
                        </div>

                        <div id="mso">
                            <div class="clear"></div>
                            <img class="clear_all" src="/media/ico/ico_del_sel.png" alt="[<?php echo __("Eliminar Selección"); ?>]" title="[<?php echo __("Eliminar Selección"); ?>]" style="float: left;" />
                            <div class="selected_office"></div>
                        </div>
                    </div>                                
                </div>                      
            </li>

            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedau"> 
                    <div>
                        <label class="label_text"><?php echo __("Usuarios"); ?>:</label>
                        <div class="item_content">
                            <select id="cmbUserSellers" name="cmbUserSellers" style="width: 190px;">
                                <option value=""><?php echo __('Seleccione una opción'); ?></option>
                            </select>
                        </div>
                        <div id="msu">
                            <div class="clear"></div>
                            <img class="clear_all_user" src="/media/ico/ico_del_sel.png" alt="[<?php echo __("Eliminar Selección"); ?>]" title="[<?php echo __("Eliminar Selección"); ?>]" style="float: left;" />
                            <div class="selected_user"></div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedaf">
                    <label class="label_text"><?php echo __("Tipo de Búsqueda") ?>:</label>
                    <div class="item_content"><select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival" >
                            <option value="0"><?php echo __("Seleccione una opción") ?></option>
                            <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                            <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                            <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <dl>
                            <dd class="tbd_group">
                                <label class="tbd_group label_text"><?php echo __("Día:") ?></label>
                                <div class="item_content">
                                    <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                                </div>
                            </dd>
                            <dd class="tbm_group">
                                <label class="tbm_group label_text"><?php echo __("Mes:") ?></label>

                                <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                                    <option value="0"><?php echo __("Seleccione Mes") ?></option>
                                    <?php foreach ($a_month_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                            <dd class="tbm_group">
                                <label class="tbm_group label_text"><?php echo __("Año:") ?></label>
                                <select class="tbm_group" id="anio" name="anio">
                                    <option value="0"><?php echo __("Seleccione Año") ?></option>
                                    <?php foreach ($a_year_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                            <dd>   
                                <label class="tbr_group label_text"><?php echo __("Fecha Inicial:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>
                            <dd>
                                <label class="tbr_group label_text"><?php echo __("Fecha Final:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_fdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>     
                            <dd>
                                <button id="btnSearch"><?php echo __('Buscar'); ?></button>   
                            </dd>
                        </dl>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div id="report_options"></div>
