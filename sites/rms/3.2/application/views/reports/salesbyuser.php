<style type="text/css">
    #div-wapper-button-add{
        position: absolute;
        top: 60px;;
        width: 360px;
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;
    } 
    #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        padding-left: 5px;
    }
    .option {
        -moz-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }
    .label_output div label{
        min-width: 90px;
    }
    .label_output div *{
        float: none;
    }
    .ui-autocomplete-input{
        line-height: 15px;
    }
    .ui-button-text-only .ui-button-text {
        padding: 0.2em 0.7em;
    }
    /*nuevo filtro*/
    .label_text{width: 160px; float: left; margin-right: 10px; font-weight: bold; font-size: 12px; margin-top: 5px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    dl dd{ margin: 10px 0;}

</style>

<script type="text/javascript">
    
    var d = '';
    var rer = false;
    
    $(document).ready(function(){
        $('#btn-add').toggle(
        function(){            
            $("div#div-wapper-button-add").animate({
                left: '+=360'
            },500);
            $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');                
            
        },
        function(){
            $("div#div-wapper-button-add").animate({
                left: '-=360'
            },500);                 
            eventEditOpen=false;
            $("#labelChange").text("<?php echo __('Buscar'); ?>");
            $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');  

        });
      
        $('#report_options').iReportingPlus({
            domain : domain,
            staticParams:rParams,
            repopt:['html','pdf','xlsx'],
            clientFolder : 'rms',
            clientFile:'RMS_SALESALLUSER_REPORT',
            htmlVisor:true,                      
            urlData:'/reports/salesbyuser/reportSalesAllUser',            
            xpath:'/report',
            orientation:'vertical',
            jqUI:true,
            responseType:'jsonp',
            jqCallBack:jquery_params,
            reportZoom:1.2,            
            beforeGetReport:function(){
                if(rer == true){
                    listSearchType = $('#listSearchType').val();
                    d = $('#accountsreceivable_date').val();                   
               }
                
                var params = new Object();
                params.d = d;
                this.dynamicParams = params; 
                      
                if(rer==true){
                    $('#btn-add').trigger('click');
                    rer =false;
                }
            }
            
        }); 
        
        //boton buscar
        $('#btnSearch').click(function(){
            rer=true;
            $('#report_options').iReportingPlus('generateReport','html');
            $('#report_options').css('display', 'block');
            
        });
        
        //fin boton buscar
        $("#buttonTypeSearch").buttonset(); 
             
        $( "#accountsreceivable_date" ).datepicker(
        {
            dateFormat: '<?php echo $jquery_date_format; ?>'       
        });
        $('#btnSearch').button({
            icons:{primary: 'ui-icon-search'},
            text: true
        });      
    });
</script>

<center><div class="titleTables"><?php echo __("Liquidación de Ventas por Usuarios"); ?>
        <span>
            <ul class="menu_right">
                <div id="help" class="ui-state-default ui-corner-all" style="padding: 5px;height: 16px;width: 16px;float: left;cursor: pointer; display: none;">
                    <img title="<?php echo __("Ayuda de Usuario"); ?>" alt="<?php echo __("Ayuda de Usuario"); ?>"  src="/media/ico/stock_help-agent.png">
                </div>
            </ul>
        </span>
    </div>
</center>
<div id="div-wapper-button-add" class="ui-corner-all ui-state-default" >

    <center><div  style=" font-size:20px "><?php echo __("Búsqueda"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedaf">
                    <div class="clear"></div>
                    <div>
                        <dl>
                            <dd>
                                <label class="tbd_group label_text"><?php echo __("Fecha de Búsqueda") ?>:</label>
                                <div class="item_content">
                                    <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                                </div>
                            </dd>
                            <br/>
                            <dd>
                                <button id="btnSearch"><?php echo __('Generar Reporte'); ?></button>   
                            </dd>
                        </dl>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<div id="report_options" style="display: none;"></div>