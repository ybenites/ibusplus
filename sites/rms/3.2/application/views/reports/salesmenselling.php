<style type="text/css"> 
  #div-wapper-button-add{
        position: absolute;
        top: 80px;;
        width: 360px;
        /*        height: 200px;*/
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;

    } 
     #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        /*margin-right: -80px;*/
        padding-left: 5px;
    }

    .option {
        -moz-transform: rotate(270deg);
        -webkit-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }
    .label_text{width: 120px;float: left;margin-right: 10px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    input.tbd_group {width: 150px;}
    dl dd{ margin: 10px 0;}

</style>

<script type="text/javascript"> 
var rer = false;
var d = '';
var sd = '';
var ed = '';
var a = '';
var m = ''; 
var listSearchType = '';    
var vendedor = '';
    
    $(document).ready(function(){
        
        $('#accountsreceivable_date').datepicker({
          dateFormat: '<?php echo $jquery_date_format; ?>' 
        });
        
        $('#accountsreceivable_bdaterango').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>' 
        });
        
        $('#accountsreceivable_fdaterango').datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>' 
        });
        
        $('#accountsreceivable_listMonth').combobox();
        $('#anio').combobox();
        $('#vendedor').combobox();
        
        
        //fecha
        $('.tbd_group').hide();
        $('.tbm_group').hide();
        $('.tbr_group').hide();
        $("#listSearchType").combobox({
            selected: function(){
                tipob=$(this).val();
                if(tipob=='tbd'){
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if(tipob=='tbm'){
                    
                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if(tipob=='tbr'){
                    
                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }
                
                if(tipob=='0'){   
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();

                }
            }
        });
          
         $('#btnSearch').button({
            icons:{primary: 'ui-icon-search'},
            text: true
        });
        
        $('#btn-add').toggle(
        function(){            
            $("div#div-wapper-button-add").animate({
                left: '+=360'
            },500);
            $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');                
            
        },
        function(){
            $("div#div-wapper-button-add").animate({
                left: '-=360'
            },500);                 
            eventEditOpen=false;
            $("#labelChange").text("<?php echo __('Buscar'); ?>");
            $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');  
            
        }).trigger('click');
        
        
        //boton buscar
        $('#btnSearch').click(function(){
            rer=true;

            listSearchType = $('#listSearchType').val();
            d = $("#accountsreceivable_date").val();
            sd = $("#accountsreceivable_bdaterango").val();
            ed = $("#accountsreceivable_fdaterango").val();
            a = $("#anio").val();
            m = $("#accountsreceivable_listMonth").val();
            vendedor = $('#vendedor').val();
            
                
            if( listSearchType=="0"){
                msgBox('<?php echo __("Seleccione Tipo de Búsqueda"); ?>');
                return false;
            } else{
                var params = new Object();
                params.sales = vendedor;

                if(listSearchType=="tbd") params.d = d;
                if(listSearchType=="tbr") {
                    params.sd = sd;
                    params.ed = ed;
                }
                if(listSearchType=="tbm") {
                    if (m  == 0 || a == 0){
                        alert ('<?php echo __("Seleccione Mes y Año"); ?>');
                        return false;
                    }else {
                        params.a = a;
                        params.m = m;
                    }
                }  
                $('#report_options').iReportingPlus('option','dynamicParams',params);
                $('#report_options').iReportingPlus('generateReport','html');
                $('#btn-add').trigger('click');        
            }  
        });
        
        
        $('#report_options').iReportingPlus({
            domain : domain,
            staticParams:rParams,
            repopt:['html','pdf','xlsx'],
            clientFolder : 'rms',
            clientFile:'RMS_SALES_SELLER',
            htmlVisor:true,                      
            urlData:'/reports/salesmenselling/reportSalesmen',            
            xpath:'/report/response/row',
            orientation:'vertical',
            jqUI:true,
            reportZoom:1.2,
            responseType:'jsonp',
            jqCallBack:jquery_params,
            waitMessage:'Generando su Reporte...',
            beforeGetReport:function(){
                //of = aSelectedOffice;
                
            }
        });
        
        
        
        
    });

    
</script>

<center><div class="titleTables"><?php echo __("REPORTE DE VENDEDORES POR PEDIDOS"); ?></div></center>

<div id="div-wapper-button-add" class="ui-corner-all ui-state-default" >
  
    <center><div  style=" font-size:20px "><?php echo __("Filtro"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
            <div class="clear"></div>
                <div class="padding10" id="gbusquedaf">
                    
                    <label class="label_text"><?php echo __("Vendedor") ?>:</label>
                    <select id="vendedor" name="vendedor">
                        <option><?php echo __("Seleccione Vendedor") ?></option>
                        <?php foreach ($vendedores as $a_dt): ?>
                            <option value="<?php echo $a_dt['idUser']; ?>"><?php echo $a_dt['userName']; ?></option>                
                        <?php endforeach; ?>   
                    </select>
                    <br/><br/>
                    <label class="label_text"><?php echo __("Tipo de Búsqueda") ?>:</label>
                    <div class="item_content"><select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival" >
                            <option value="0"><?php echo __("Seleccione una Opción") ?></option>
                            <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                            <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                            <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <dl>
                            <dd>
                                <label class="tbd_group label_text"><?php echo __("Día:") ?></label>
                                <div class="item_content">
                                    <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                                </div>
                            </dd>
                            <dd>
                                <label class="tbm_group label_text"><?php echo __("Mes:") ?></label>

                                <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                                    <option value="0"><?php echo __("Seleccione Mes") ?></option>
                                    <?php foreach ($a_month_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                                 <dd>
                                <label class="tbm_group label_text"><?php echo __("Año:") ?></label>
                                <select class="tbm_group" id="anio" name="anio">
                                    <option value="0"><?php echo __("Seleccione Año") ?></option>
                                    <?php foreach ($a_year_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                            <dd>   
                                <label class="tbr_group label_text"><?php echo __("Fecha Inicial:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>
                            <dd>
                                <label class="tbr_group label_text"><?php echo __("Fecha Final:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_fdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>
                                
                            </dd>
                            
                        </dl>
                    <dd>
                        <button id="btnSearch"><?php echo __('Buscar'); ?></button>   
                    </dd>
                    </div>
                </div>  
            </li>
        </ul>  
    </div>
</div>
<div id="report_options"></div>