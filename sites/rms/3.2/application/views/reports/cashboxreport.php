<style type="text/css"> 
    #div-wapper-button-add{
        position: absolute;
        top: 80px;;
        width: 360px;
        /*        height: 200px;*/
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;      
    }    
    #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        /*margin-right: -80px;*/
        padding-left: 5px;
    }

    .option {
        -moz-transform: rotate(270deg);
        -webkit-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }

    /*    .report_Options{width: 350px;margin-right: 10px;float: left;}*/
    .label_text{width: 120px;float: left;margin-right: 10px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    input.tbd_group {width: 150px;}
    dl dd{ margin: 10px 0;}

    #accountsreceivable_listMonth-combobox{width: 150px;}    
</style>

<script type="text/javascript">
    var option_template = '<option value="[VALUE]">[LABEL]</option>';
    var msgSelectEmpty='<?php echo __("No se encontró información"); ?>';
    var msgSelectDefaultOption='<?php echo __("Seleccione una opción"); ?>';
    var d = '';
    var idCashBox = '';
    var rer = false;
    var url_GetCash = '/reports/cashboxreport/loadCashByDay' + jquery_params;

    $(document).ready(function() {

        $('#btnSearch').button({
            icons: {primary: 'ui-icon-search'},
            text: true
        });

        $("#btnGoCash").button({
            icons: {primary: 'ui-icon-plus'},
            text: false
        });

        //funcionalidad del boton Buscar
        $('#btn-add').toggle(
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '+=360'
                    }, 500);
                    $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');

                },
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '-=360'
                    }, 500);
                    eventEditOpen = false;
                    $("#labelChange").text("<?php echo __('Buscar'); ?>");
                    $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');

                }).trigger('click');

        $("#accountsreceivable_date").datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>',
            onSelect:function(date){
                var date=$("#accountsreceivable_date").val();
                var user_id=$("#user_id").val();
                $.ajax({
                    url:url_GetCash,
                    data: {
                       date:function(){
                            return date;
                        },
                        user_id:function(){
                            return user_id;
                        }
                    },
                    async:false,
                    success:function(response){
                        $('option', '#listCashBox').remove();
                        if(evalResponse(response)){
                            if(response.data.length>0){
                                $('#listCashBox').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                                $.each(response.data, function(index, array) {
                                    $('#listCashBox').append(option_template.replace('[VALUE]', array.idCashBox,'g').replace('[LABEL]',array.cashBox, 'g'));
                                });
                            } else {
                                $('#listCashBox').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                            }
                        }else {
                            msgBox(response.msg,response.code);
                        }
                    }
                });
            }
        });

        //boton buscar
        $('#btnSearch').click(function() {
            rer = true;
            $('#report_options').iReportingPlus('generateReport', 'html');
            $('#report_options').css('display', 'block');
        });

        $('#listCashBox').combobox();

        $('#report_options').iReportingPlus({
            domain: domain,
            staticParams: rParams,
            repopt: ['html', 'pdf', 'xlsx'],
            clientFolder: 'rms',
            clientFile: 'RMS_MY_DAYLY_CASHBOX',
            htmlVisor: true,
            urlData: '/reports/cashboxreport/reportCashReportDayli',
            xpath: '/report',
            orientation: 'vertical',
            jqUI: true,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            reportZoom: 1.2,
            beforeGetReport: function() {
                if (rer == true) {
                    d = $('#accountsreceivable_date').val();
                    idCashBox = $('#listCashBox').val();
                    user_id = $('#user_id').val();
                }
                if (d != null && d==''){
                        msgBox('<?php echo __("Seleccione la Fecha de Caja"); ?>');
                        return false;
                    }else{
                var params = new Object();
                params.d = d;
                if (idCashBox != null && idCashBox==''){
                        msgBox('<?php echo __("Seleccione el Turno de Caja"); ?>');
                        return false;
                    }
                params.idCashBox = idCashBox;
                params.user_id = user_id;
                this.dynamicParams = params;
                    }
                if (rer == true) {
                    $('#btn-add').trigger('click');
                    rer = false;
                }
            }

        });




    });

</script>

<center><div class="titleTables"><?php echo __("Reporte de Cierre de Caja Diario"); ?></div></center>

<div id="div-wapper-button-add" class="ui-corner-all ui-state-default">
    <center><div  style=" font-size:20px "><?php echo __("Filtro de Busqueda"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedaf">
                    <label class="label_text"><?php echo __("Fecha") ?>:</label>
                    <div class="item_content">
                        <input  id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                    </div>
                    <input id="user_id" type="hidden" value="<?php echo $user_id; ?>"/>
                    <div class="clear"></div>
                    <br/>
                    <div>
                        <div>
                            <label class="label_text"><?php echo __("Cajas Aperturadas") ?>:</label>
                            <div class="item_content" >
                                <select style="width: 200px" size="1" id="listCashBox" name="listCashBox" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival">
                                    <option><?php echo __("Seleccione una Opcion...") ?></option>
                                </select>
                            </div>
                        </div>

                        <button id="btnSearch"><?php echo __('Buscar'); ?></button>   
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div id="report_options" style="display: none;"></div>