<style type="text/css">
    #div-wapper-button-add{
        position: absolute;
        top: 60px;;
        width: 360px;
        /*        height: 200px;*/
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;

    } 
    #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        /*margin-right: -80px;*/
        padding-left: 5px;
    }
    .option {
        -moz-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }
    .label_output div label{
        min-width: 90px;
    }
    .label_output div *{
        float: none;
    }
    .ui-autocomplete-input{
        line-height: 15px;
    }
    .ui-button-text-only .ui-button-text {
        padding: 0.2em 0.7em;
    }
    /*nuevo filtro*/
    .label_text{width: 120px; float: left; margin-right: 10px; font-weight: bold; font-size: 12px; margin-top: 5px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    dl dd{ margin: 10px 0;}
    input[type="text"], input[type="password"], textarea, select {    padding: 4px;}
    .padding5{margin:4px}
</style>

<script type="text/javascript">

    var store = '';
    var date_search = '';
    
    var rer = false;
    $(document).ready(function() {
        $('#btn-add').toggle(
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '+=360'
                    }, 500);
                    $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');

                },
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '-=360'
                    }, 500);
                    eventEditOpen = false;
                    $("#labelChange").text("<?php echo __('Buscar'); ?>");
                    $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');

                });

         $('#cmbStore').combobox();
        //fin de lista de Rutas
        $('#report_options').iReportingPlus({
            domain: domain,
            staticParams: rParams,
            repopt: ['html', 'pdf', 'xlsx'],
            clientFolder: 'rms',
            clientFile: 'RMS_STOCK_PRODUCTS',
            htmlVisor: true,
            urlData: '/reports/reportstockproduct/reportStockProduct',
            xpath: '/report/response/row',
            orientation: 'horizontal',
            jqUI: true,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            reportZoom: 1.2,
            beforeGetReport: function() {
            }
        });

        //boton buscar
        $('#btnSearch').click(function() {
            rer = true;
            store = $("#cmbStore").val();
            date_search = $("#txt_date_search").val();
             if ($("#cmbStore").val() == 0){
                 msgBox('<?php echo __("Seleccione un Almacen");?>');
                 return  false;
             } 
             
             var params = new Object();
             params.store= store;
             params.date_search = date_search;
            $('#report_options').iReportingPlus('option','dynamicParams',params);
            $('#report_options').iReportingPlus('generateReport', 'html');
            $('#btn-add').trigger('click');

        });
        //fin boton buscar

        $('#btnSearch').button({
            icons: {primary: 'ui-icon-search'},
            text: true
        });
        $("#txt_date_search").datepicker({
            dateFormat: '<?php echo $jquery_date_format; ?>'
        });

    });
</script>

<center><div class="titleTables"><?php echo __("Inventario de Productos"); ?></div></center>
<div id="div-wapper-button-add" class="ui-corner-all ui-state-default" >

    <center><div  style=" font-size:20px "><?php echo __("Búsqueda"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
                <div class="padding5"> 
                    <label class="label_text"><?php echo __("Almacén") ?>: </label>
                    <select id="cmbStore" name="cmbStore" class="item_content" style="width: 240px;">
                        <option value="0"><?php echo __("Seleccione un Almacén") ?></option>                                
                        <?php foreach ($array_store as $va): ?>                                   
                            <option value="<?php echo $va->idStore ?>"><?php echo $va->storeShortName ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="clear"></div>
                </div> 
            </li>
            <li>
                <div class="padding5"> 
                    <label class="label_text"><?php echo __("Fecha") ?>: </label>
                    <input type="text" id="txt_date_search" name="txt_date_search" />
                    <div class="clear"></div>
                </div> 
            </li>
            <li>
                <button id="btnSearch"><?php echo __('Buscar'); ?></button>   
            </li>
        </ul>
    </div>
</div>

<div id="report_options"></div>


