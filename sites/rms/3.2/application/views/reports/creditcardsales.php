<style type="text/css">
    #div-wapper-button-add{
        position: absolute;
        top: 60px;;
        width: 360px;
        margin-left: -360px;
        margin-top: -35px;
        z-index: 30;
    } 
    #btn-add{
        position: relative;
        width: 70px;
        height: 25px;
        top:50%;
        margin-top: -5px;
        margin-right: 0px;
        left: 335px;
        padding-left: 5px;
    }
    .option {
        -moz-transform: rotate(270deg);
        -webkit-transform: rotate(270deg);
        cursor: pointer;
        float: left;
        font-size: 14px;
        height: 24px;
        margin-right: -22px;
        margin-top: 22px;
        text-align: center;
        width: 68px;
    }

    #ticketCanceled{float: left;}
    .label_output div label{
        min-width: 90px;
    }
    .label_output div *{
        float: none;
    }
    .ui-autocomplete-input{
        line-height: 15px;
    }
    .ui-button-text-only, .ui-button-text {
        padding: 0.2em 0.7em;
    }
    #msa{width: 100%; margin-left:10px; display: none;}

    .label_text{width: 120px;float: left;margin-right: 10px;}
    .item_content{float: left;}
    .item_content input,.item_content button{float: left;}
    .item_content input{width: 150px;}
    input.tbd_group {width: 150px;}
    dl dd{ margin: 10px 0;}
    #accountsreceivable_listMonth-combobox{width: 150px;}
</style>

<script type="text/javascript">

    //agencias
    var option_template = '<option value="[VALUE]">[LABEL]</option>';
    var msgSelectEmpty = '<?php echo __("No se encontró información"); ?>';
    var msgSelectDefaultOption = '<?php echo __("Seleccione una opción"); ?>';
    var sel_agency = '<div class="ui-corner-all ui-state-highlight" data-ida="[ID]"><span class="ui-button-text">[AGENCY_NAME]</span><span class="ui-button-icon-primary ui-icon ui-icon-close"></span></div>';

    var rer = false;
    var listSearchType = '';

    var d = '';
    var sd = '';
    var ed = '';
    var a = '';
    var m = '';

    var numero = 0;
    var numero2 = 0;

    $(document).ready(function() {

        $('#btn-add').toggle(
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '+=360'
                    }, 500);
                    $('#icon_search').removeClass('ui-icon-circle-arrow-s').addClass('ui-icon-circle-arrow-n');

                },
                function() {
                    $("div#div-wapper-button-add").animate({
                        left: '-=360'
                    }, 500);
                    eventEditOpen = false;
                    $("#labelChange").text("<?php echo __('Buscar'); ?>");
                    $('#icon_search').removeClass('ui-icon-circle-arrow-n').addClass('ui-icon-circle-arrow-s');

                }).trigger('click');

        $('#accountsreceivable_listMonth').combobox();
        $('#anio').combobox();

        $('.tbd_group').hide();
        $('.tbm_group').hide();
        $('.tbr_group').hide();
        $("#listSearchType").combobox({
            selected: function() {
                tipob = $(this).val();
                if (tipob == 'tbd') {
                    $('.tbd_group').show();
                    $('.tbm_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_date').focus();
                }
                if (tipob == 'tbm') {
                    $('.tbm_group').show();
                    $('.tbd_group').hide();
                    $('.tbr_group').hide();
                    $('#accountsreceivable_listMonth').focus();
                }
                if (tipob == 'tbr') {
                    $('.tbr_group').show();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                    $('#accountsreceivable_bdaterango').focus();
                }
                if (tipob == '0') {
                    $('.tbr_group').hide();
                    $('.tbd_group').hide();
                    $('.tbm_group').hide();
                }
            }
        });
        //fin fecha

        //darle datepicker a los texbox
        $("#accountsreceivable_date").datepicker(
                {
                    dateFormat: '<?php echo $jquery_date_format; ?>'
                });
        $('#btnSearch').button({
            icons: {primary: 'ui-icon-search'},
            text: true
        });
        $("#accountsreceivable_bdaterango,#accountsreceivable_fdaterango").datepicker(
                {
                    dateFormat: '<?php echo $jquery_date_format; ?>'
                });
        //fin darle datepicker a los texbox

        $('#report_options').iReportingPlus({
            domain: domain,
            staticParams: rParams,
            repopt: ['html', 'pdf', 'xlsx'],
            clientFolder: 'rms',
            clientFile: 'RMS_CREDITCARD_SALES',
            htmlVisor: true,
            urlData: '/reports/creditcardsales/reportCreditcardsales',
            xpath: '/report',
            orientation: 'horizontal',
            jqUI: true,
            responseType: 'jsonp',
            reportZoom: 1.2,
            jqCallBack: jquery_params
        });

        //boton buscar
        $('#btnSearch').click(function() {
            rer = true;
            listSearchType = $('#listSearchType').val();
            d = $("#accountsreceivable_date").val();
            sd = $("#accountsreceivable_bdaterango").val();
            ed = $("#accountsreceivable_fdaterango").val();
            a = $("#anio").val();
            m = $("#accountsreceivable_listMonth").val();

            if (listSearchType == "0") {
                msgBox('<?php echo __("Seleccione Tipo de Búsqueda"); ?>');
                return false;
            } else {
                var params = new Object();
                if (listSearchType == "tbd")
                    params.d = d;
                if (listSearchType == "tbr") {
                    params.sd = sd;
                    params.ed = ed;
                }
                if (listSearchType == "tbm") {
                    if (m == 0 || a == 0) {
                        alert('<?php echo __("Seleccione Mes y Año"); ?>');
                        return false;
                    } else {
                        params.a = a;
                        params.m = m;
                    }
                }
                $('#report_options').iReportingPlus('option', 'dynamicParams', params);
                $('#report_options').iReportingPlus('generateReport', 'html');
                $('#btn-add').trigger('click');
            }

        });

    });

</script>

<center><div class="titleTables"><?php echo __("Ventas - Tarjeta de Credito"); ?>
    </div></center>

<div id="div-wapper-button-add" class="ui-corner-all ui-state-default" >

    <center><div  style=" font-size:20px "><?php echo __("Filtro de Busqueda"); ?></div></center>
    <hr class="ui-state-active" style="height: 5px">
    <div id="btn-add" class="ui-corner-bottom option ui-state-active box_shadow">        
        <span style="-moz-transform: rotate(180deg);" id="labelChange"><?php echo __("Buscar") ?></span>        
        <span id="icon_search" class="ui-icon ui-icon-circle-arrow-s left"></span>
    </div>
    <div class="clear"></div>
    <div class="report_Options ui-corner-all ui-widget ui-widget-content" id="filter">
        <ul>
            <li>
                <div class="clear"></div>
                <div class="padding10" id="gbusquedaf">
                    <label class="label_text"><?php echo __("Tipo de Búsqueda") ?>:</label>
                    <div class="item_content"><select style="width: 200px" size="1" id="listSearchType" name="listSearchType" rel="0" class="ui-widget ui-widget-content ui-corner-all route_city_arrival" >
                            <option value="0"><?php echo __("Seleccione una opción") ?></option>
                            <option value="tbd"><?php echo __("Búsqueda por Día") ?></option>  
                            <option value="tbr"><?php echo __("Búsqueda por Rango de Fechas") ?></option>  
                            <option value="tbm"><?php echo __("Búsqueda por Mes") ?></option>  
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <dl>
                            <dd class="tbd_group">
                                <label class="tbd_group label_text"><?php echo __("Día:") ?></label>
                                <div class="item_content">
                                    <input class="tbd_group" id="accountsreceivable_date" name="accountsreceivable_date" value="" type="text" maxlength="25" size="20" />  
                                </div>
                            </dd>
                            <dd class="tbm_group">
                                <label class="tbm_group label_text"><?php echo __("Mes:") ?></label>

                                <select class="tbm_group" id="accountsreceivable_listMonth" name="accountsreceivable_listMonth">
                                    <option value="0"><?php echo __("Seleccione Mes") ?></option>
                                    <?php foreach ($a_month_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                            <dd class="tbm_group">
                                <label class="tbm_group label_text"><?php echo __("Año:") ?></label>
                                <select class="tbm_group" id="anio" name="anio">
                                    <option value="0"><?php echo __("Seleccione Año") ?></option>
                                    <?php foreach ($a_year_report as $a_dt): ?>
                                        <option value="<?php echo $a_dt['value']; ?>"><?php echo $a_dt['display']; ?></option>                
                                    <?php endforeach; ?>
                                </select>
                            </dd>
                            <dd>   
                                <label class="tbr_group label_text"><?php echo __("Fecha Inicial:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_bdaterango" name="accountsreceivable_bdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>
                            <dd>
                                <label class="tbr_group label_text"><?php echo __("Fecha Final:") ?></label>
                                <input class="tbr_group" id="accountsreceivable_fdaterango" name="accountsreceivable_fdaterango" value="" type="text" maxlength="25" size="20" />
                            </dd>     
                            <dd>
                                <button id="btnSearch"><?php echo __('Buscar'); ?></button>   
                            </dd>
                        </dl>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div id="report_options"></div>

