<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of move
 *
 * @author Diana 
 */
class Model_HistorySalesOrder extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_historySalesOrder';
    protected $_primary_key ='idHistorySalesOrder';
    
    public function saveHistorySalesOrder($obj_salesOrder){
      $idSalesOrder = $obj_salesOrder['idSalesOrder']; 
      $status = $obj_salesOrder['status'];
      $dateHistorySalesOrder = $obj_salesOrder['dateHistorySalesOrder'];
      $idUser = $obj_salesOrder['idUser'];
       
      if (empty($obj_salesOrder['idHistory'])) {
            $o_model_history = new Model_HistorySalesOrder();
            
      }else{
        $o_model_history = new Model_HistorySalesOrder($obj_salesOrder['idHistorySalesOrder']);   
        if (!$o_model_history->loaded())
            throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
      }
      $o_model_history->idSalesOrder = $idSalesOrder;
      $o_model_history->status = $status;
      $o_model_history->dateHistorySalesOrder = $dateHistorySalesOrder;
      $o_model_history->idUser = $idUser;
      $o_model_history->save();
        
      return $o_model_history; 
  }
}

?>
