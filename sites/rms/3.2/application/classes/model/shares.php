<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diana 
 */
class Model_Shares extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_sharessales';
    protected $_primary_key ='idShare';
    
    public function saveShares($obj_shares){

      $idSalesOrder = $obj_shares['idSalesOrder']; 
      $status = $obj_shares['status'];
      $dateShares = DateHelper::getFechaFormateadaActual();
      $idUser = $obj_shares['idUser'];
      $totalAmount = $obj_shares['totalAmount'];
      $amountPaid = $obj_shares['amountPaid'];
      $amountToPaid = $obj_shares['amountToPaid'];
       
      if (empty($obj_shares['idShare'])) {
            $o_model_share = new Model_Shares();
            
      }else{
        $o_model_share = new Model_Shares($obj_shares['idShare']);   
        if (!$o_model_share->loaded())
            throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
      }
      $o_model_share->idSalesOrder = $idSalesOrder;
      $o_model_share->status = $status;
      $o_model_share->dateShare = $dateShares;
      $o_model_share->idUser = $idUser;
      $o_model_share->totalAmount = $totalAmount;
      $o_model_share->amountPaid = $amountPaid;
      $o_model_share->amountToPaid = $amountToPaid;
      $o_model_share->save();
        
      return $o_model_share; 
  }
}

?>
