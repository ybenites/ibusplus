<?php

class Model_Cashbox extends Kohana_Btsorm{
    
    protected $_table_names_plural = false; 
    protected $_primary_key = 'idCashBox';
    protected $_table_name = 'rms_cashbox';
}