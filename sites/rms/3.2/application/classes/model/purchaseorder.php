<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Purchaseorder extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_purchaseorder';
    protected $_primary_key ='idPurchaseOrder';
    protected $_belongs_to = array(
        'supplier' => array('model' => 'Supplier', 'foreign_key' => 'idSupplier'),
    );
    protected $_has_many = array(
        'purchase' => array(
            'model' => 'Purchase',
            'foreign_key' => 'idPurchaseOrder',
        ),
        'purchaseorderdetail' => array(
            'model' => 'Purchaseorderdetail',
            'foreign_key' => 'idPurchaseOrder',
        ),
    );

    
    
}
