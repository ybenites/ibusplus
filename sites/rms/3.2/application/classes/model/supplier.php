<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Supplier extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_supplier';
    protected $_primary_key ='idSupplier';
    protected $_has_many = array(
        'purchase_supplier' => array(
            'model' => 'Purchaseorders',
            'foreign_key' => 'idSupplier',
        ),
    );
}