<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of registerlot
 *
 * @author Yime
 */
class Model_Lot extends Kohana_Btsorm {    
    //put your code here
    protected $_table_names_plural = false;
    protected $_primary_key = 'idLot';
    protected $_table_name = 'rms_lot';
    protected $_belongs_to = array(
        'product' => array('model' => 'Products', 'foreign_key' => 'idProduct')        
    );
}

?>
