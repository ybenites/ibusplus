<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */
class Model_Sales extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_primary_key = 'idSales';
    protected $_table_name = 'rms_sales';
    protected $_has_many = array(
        'salesdetail' => array(
            'model' => 'salesdetail',
            'foreign_key' => 'idSales',
        )
    );
    protected $_belongs_to = array(
        'customers' => array('model' => 'customers', 'foreign_key' => 'idCustomers'),
        'user' => array('model' => 'user', 'foreign_key' => 'idUser')
        );
}

?>
