<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of move
 *
 * @author Harold
 */
class Model_Historypurchase extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_historypurchase';
    protected $_primary_key ='idHistory';
    
    public function saveH($a_purch){
      $idPurchaseOrder = $a_purch['idPurchaseOrder']; 
      $status = $a_purch['status'];
      $dateHistoryPurchase = $a_purch['dateHistoryPurchase'];
      $idUser = $a_purch['idUser'];
       
      if (empty($a_purch['idHistory'])) {
            $o_model_history = new Model_Historypurchase();
            
        } else {
            $o_model_history = new Model_Historypurchase($a_purch['idHistory']);
            
            if (!$o_model_history->loaded())
                throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
        }
        $o_model_history->idPurchaseOrder = $idPurchaseOrder;
        $o_model_history->status = $status;
        $o_model_history->dateHistoryPurchase = $dateHistoryPurchase;
        $o_model_history->idUser = $idUser;

        $o_model_history->save();
        
      return $o_model_history;
      
      
  }
}

?>
