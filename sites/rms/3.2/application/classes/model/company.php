<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */
class Model_Company extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_primary_key = 'idCustomersCompany';
    protected $_table_name = 'rms_customerscompany';
    protected $_has_many = array(
        'address' => array(
            'model' => 'address',
            'foreign_key' => 'idCustomersCompany',
        )
    );
}
