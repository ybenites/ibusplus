<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */
class Model_Customers extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_primary_key = 'idCustomers';
    protected $_table_name = 'rms_customers';
    protected $_belongs_to = array(
        'person' => array('model' => 'person', 'foreign_key' => 'idPerson'),
        'company' => array('model' => 'company', 'foreign_key' => 'idCustomersCompany')
        );
}

?>
