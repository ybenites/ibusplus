<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of movedetail
 *
 * @author Yime
 */
class Model_Movedetail extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'rms_movedetail';
    protected $_primary_key = 'idMoveDetail';

    public function getMoveByIdDetailSale($id,$type) {
       
        try {           
            $o_movedetail = DB::select(
                                  array('movedetail.idMoveDetail','id')
                            )
                            ->from(array('rms_movedetail', 'movedetail'))
                            ->join(array('rms_move', 'move'), 'inner')->on('movedetail.idMove', '=', 'move.idMove')
                            ->where('idProductDetail', '=', $id)
                            ->and_where('move.typeMoveStore', '=', $type)
                            ->execute()->current();
           
            $o_model = new Model_Movedetail($o_movedetail['id']);
            
            return $o_model;
            
        } catch (Exception $exc) {           
            $exc->getMessage();
        }
        
    }

}