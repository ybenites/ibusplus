<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */
class Model_Salesdetail extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_primary_key = 'idSalesDetail';
    protected $_table_name = 'rms_salesdetail';
    protected $_belongs_to = array(
        'sales' => array('model' => 'sales', 'foreign_key' => 'idSales'),        
        'stores' => array('model' => 'stores', 'foreign_key' => 'idStore'),
        );
}

?>
