<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */
class Model_Salesorderdetail extends Kohana_Btsorm {
    protected $_table_names_plural = false;
    protected $_primary_key = 'idSalesOrderDetail';
    protected $_table_name = 'rms_salesorderdetail';
    protected $_belongs_to = array(
        'salesorder' => array('model' => 'salesorder', 'foreign_key' => 'idSalesOrder'),
        'products' => array('model' => 'products', 'foreign_key' => 'idProduct'),
        'stores' => array('model' => 'stores', 'foreign_key' => 'idStore'),
        );
}

?>
