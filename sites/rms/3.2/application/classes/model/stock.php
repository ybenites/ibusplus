<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stock
 *
 * @author Yime
 */
class Model_Stock extends Kohana_Btsorm {    
    protected $_table_names_plural = false;
    protected $_primary_key = 'idStock';
    protected $_table_name = 'rms_stock';
    protected $_belongs_to = array(
        'product' => array('model' => 'Products', 'foreign_key' => 'idProduct'),
        'store' => array('model' => 'Stores', 'foreign_key' => 'idStore'),
    );
    
    public function getIdStockByProductAndStore($Product,$Store){
        
        $o_response = DB::Select(
                'st.idStock',
                'st.amount'
                )
                    ->from(array('rms_stock','st'))
                    ->where('st.idProduct','=',$Product)
                    ->and_where('st.idStore','=',$Store)
                    ->as_object()
                    ->execute()
                    ->current();
        
        return $o_response;
    }
}

?>
