<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_Purchase extends Kohana_Btsorm{
    protected $_table_names_plural = false;
    protected $_table_name = 'rms_purchase';
    protected $_primary_key ='idPurchase';
    protected $_belongs_to = array(
        'purchaseorder' => array('model' => 'Purchaseorder', 'foreign_key' => 'idPurchaseOrder'),
    );
}
