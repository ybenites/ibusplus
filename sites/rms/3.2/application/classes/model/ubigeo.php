<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of typemove
 *
 * @author Yime
 */
class Model_Ubigeo extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_primary_key = 'ubigeo';
    protected $_table_name = 'rms_ubigeo';
    protected $_has_many = array(
        'person' => array('model' => 'person', 'foreign_key' => 'idUbigeo'),
    );

}

?>
