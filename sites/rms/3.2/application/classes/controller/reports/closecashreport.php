<?php

/**
 * Description of customers
 *
 * @author Escritorio
 */
class Controller_Reports_Closecashreport extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('closecashreport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('closecashreport', self::FILE_TYPE_JS));
        $view_closecashReport = new View('reports/closecashreport');
        $view_closecashReport->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_closecashReport->a_year_report = $this->a_year_report();
        $view_closecashReport->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $this->template->content = $view_closecashReport;
    }

    public function action_reportCashReportByOffice() {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);
            
            $d = $this->request->query('d');
            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $idUser = $this->request->query('idUser');
            $idOffice = $this->request->query('idOffice');

            $reportSales = array();
            $reportSales['qlang'] = $this->request->query(self::RQ_LANG);
            $reportSales['searchDate'] = DateHelper::getFechaFormateadaActual();

            $amount = array(DB::expr("SUM(`cb`.`initAmount`) AS initAmount"),
                DB::expr("SUM(`cb`.`endAmount`) AS endAmount"));
            
            $amountCashBox = DB::select_array($amount)
                            ->from(array('rms_cashbox', 'cb'));
            if ($idOffice != NULL AND is_array($idOffice)) {
                $amountCashBox->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $amountCashBox->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $amountCashBox->and_where(DB::expr("DATE_FORMAT(cb.openDate,'$date_format')"), 'LIKE', $d);
            }

            if ($m != 0) {
                $amountCashBox->and_where(DB::expr("MONTH(cb.openDate)"), '=', $m);
                $amountCashBox->and_where(DB::expr("YEAR(cb.openDate)"), '=', $a);
            }

            if ($ed != "") {
                if ($sd != "") {
                    $amountCashBox->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $amountCashBox->and_where(DB::expr("DATE_FORMAT(cb.openDate,'$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $amountCashBox->and_where(DB::expr("DATE_FORMAT(cb.openDate,'$date_format')"), 'LIKE', $sd);
                }
            }
            $cashbox = $amountCashBox->as_object()->execute()->current();
            
                    
            $reportSales['iamount'] = $cashbox->initAmount;
            $reportSales['eamount'] = $cashbox->endAmount;

            if ($d != "") {
                $reportSales['date'] = $d;
            }

            if ($m != 0) {
                $reportSales['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $reportSales['date'] = $sd . " - " . $ed;
                } else {
                    $reportSales['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $reportSales['date'] = $sd;
                }
            }


            $data_reportSales = array();
            $data_reportSales['salesCash'] = $this->getSalesCash($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $data_reportSales['salesCredit'] = $this->getSalesCredit($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $a_ticket = $this->getSalesMixto($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $i_id_paypal = null;
            $a_paypal_validator = array();
            $a_ticket_result = array();
            foreach ($a_ticket as $a_item) {

                if (!in_array($a_item['idPayment'], $a_paypal_validator)) {
                    array_push($a_paypal_validator, $a_item['idPayment']);
                    $a_item['amountCardSum'] = $a_item['amountCard'];
                } else {
                    $a_item['amountCard'] = 0;
                }
                $i_id_paypal = $a_item['idPayment'];
                $a_ticket_result[] = $a_item;
            }

            $data_reportSales['salesMixto'] = $a_ticket_result;
            $data_reportSales['creditNoteUsed'] = $this->getCreditNoteUsed($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $data_reportSales['creditNoteCreated'] = $this->getCreditNoteCreated($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $data_reportSales['reservation'] = $this->getReservation($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $data_reportSales['reservationSold'] = $this->getReservationSold($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();
            $data_reportSales['voucher'] = $this->getVoucher($d, $sd, $ed, $m, $a, $idUser, $idOffice)->execute()->as_array();

            $this->fnResponseFormat($data_reportSales, self::REPORT_RESPONSE_TYPE_XML, $reportSales);
           // print_r(Database::instance()->last_query);
           // die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    // Vales de Ingresos e Egresos
    public function getVoucher($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );
            $cols = array(
                array('v.idVoucher', 'idVoucher')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('v.documentType', 'documentType')
                , DB::expr("CONCAT(v.serie,'-',v.correlative) as voucherNumber")
                , DB::expr("DATE_FORMAT(v.voucherDate,:date_time) as transactionDate")
                , array('v.concept', 'description')
                , array('v.amount', 'amount')
                , array('pe.fullName', 'authorizesBy')
                , array('v.idCashBox', 'idCashBox')
                , array('v.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(v.idCashboxCancel IS NOT NULL and v.status = :s_deactive,:s_active,:s_deactive) AS isCancel")
            );
            $sql_voucher = DB::select_array($cols)
                    ->from(array('rms_voucher', 'v'))
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('v.idCashBox OR cb.idCashBox=v.idCashBoxCancel'))
                    ->join(array('bts_person', 'pe'))->on('pe.idPerson', '=', 'v.idAuthorizes')
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('v.status', '=', ':s_active');

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_voucher->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_voucher->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_voucher->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_voucher->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_voucher->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_voucher->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_voucher->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_vouchers = $sql_voucher->parameters($parameters);
//            print_r(Database::instance()->last_query);die();
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_vouchers;
    }

    //Notas de Credito - Creadas
    public function getCreditNoteCreated($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_limit' => $time_limit,
                ':time_raw' => $time_raw,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':created' => self::CREDITNOTE_CREATE,
                ':used' => self::CREDITNOTE_USED,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );

            $data_creditNote = array(
                array('cn.idCreditNote', 'idCreditNote')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('cn.amountSale', 'amountCreditNote')
                , array('sa.totalPrice', 'amountSales')
                , array('cn.description', 'description')
                , DB::expr("DATE_FORMAT(cn.cancelDate,:date_time) as transactionDate")
                , array('pro.productName', 'productName')
                , array('cb.idCashBox', 'idCashBox')
                , array('cn.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(cn.idCashboxCancel IS NOT NULL,:s_active,:s_deactive ) AS isCancel"),
            );

            $sql_creditNote = DB::select_array($data_creditNote)
                    ->from(array('rms_credit_note', 'cn'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'cn.idSales')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('cn.idCashBox OR cb.idCashBox=cn.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('cn.status', 'IN', DB::expr("(:created,:used)"));

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_creditNote->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_creditNote->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_creditNote->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_creditNote->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $creditNoteCreated = $sql_creditNote->parameters($parameters);
        } catch (Exception $e) {
            echo $this->handlingError($e);
        }
        return $creditNoteCreated;
    }

    //Notas de Credito - Canjeadas
    public function getCreditNoteUsed($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':used' => self::CREDITNOTE_USED,
                ':created' => self::CREDITNOTE_CREATE,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );
            $data_creditNote = array(
                array('cn.idCreditNote', 'idCreditNote')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('cn.amountSale', 'amountCreditNote')
                , array('sa.totalPrice', 'amountSales')
                , array('cn.description', 'description')
                , DB::expr("DATE_FORMAT(cn.confirmDate,:date_time) as transactionDate")
                , array('pro.productName', 'productName')
                , array('cb.idCashBox', 'idCashBox')
                , array('cn.idCashBoxConfirm', 'idCashBoxConfirm')
                , array('cn.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(cn.idCashboxCancel IS NOT NULL,:s_active,:s_deactive ) AS isCancel"),
            );

            $sql_creditNote = DB::select_array($data_creditNote)
                    ->from(array('rms_credit_note', 'cn'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'cn.idNewSales')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('cn.idCashBoxConfirm OR cb.idCashBox=cn.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('cn.status', 'IN', DB::expr("(:used,:created)"));


            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_creditNote->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_creditNote->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_creditNote->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_creditNote->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_creditNote->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $creditNoteUsed = $sql_creditNote->parameters($parameters);
        } catch (Exception $e) {
            echo $this->handlingError($e);
        }

        return $creditNoteUsed;
    }

    // Ventas Efectivo
    public function getSalesCash($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_PAYMENT_CASH,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD,
                ':sales_direct'=>self::SALES_DIRECT,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );

            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale')
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':cash')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectLot->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectLot->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectLot->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_selectLot->parameters($parameters)->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [6] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':cash')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectProductDetail->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectProductDetail->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectProductDetail->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_selectProductDetail->parameters($parameters)->group_by('sd.idSalesDetail');

            $sql_salesCash = $sql_selectProductDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesCash;
    }

    //Venta Tarjeta de Credito
    public function getSalesCredit($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':credit' => self::SALES_PAYMENT_CREDITCARD,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD,
                ':sales_direct'=>self::SALES_DIRECT,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );
            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale')
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':credit')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectLot->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectLot->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectLot->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_selectLot->parameters($parameters)->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [6] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':credit')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectProductDetail->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectProductDetail->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectProductDetail->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_selectProductDetail->parameters($parameters)->group_by('sd.idSalesDetail');

            $sql_salesCreditCard = $sql_selectProductDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesCreditCard;
    }

    //Ventas Mixto : Efectivo-Tarjeta Credito
    public function getSalesMixto($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':mixto' => self::SALES_PAYMENT_MIXTO,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD,
                ':sales_direct'=>self::SALES_DIRECT,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );
            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('sa.idPaymentCard','idPayment')
                , array('us.fullName','user')
                , array('of.name','office')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale')
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':mixto')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));
                   
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectLot->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_selectLot->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectLot->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            
            $sql_selectLot->parameters($parameters)->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [7] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('sa.idCashBox OR cb.idCashBox=sa.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->where('sa.methodPayment', '=', ':mixto')
                    ->and_where('sa.salesType', '=', ':sales_direct')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));
                   
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectProductDetail->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }

            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectProductDetail->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }

            if ($d != "") {
                $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectProductDetail->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectProductDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            
            $sql_salesMixto = $sql_selectProductDetail->parameters($parameters)->group_by('sd.idSalesDetail');
            
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesMixto;
    }

    //Venta de Reservas
    public function getReservation($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW,$rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT,$rDateFormat);
            
            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_ORDER_PAYMENT_CASH,
                ':credit' => self::SALES_ORDER_PAYMENT_CREDIT,
                ':canceled' => self::SALES_ORDER_STATUS_CANCELED,
                ':expired' => self::SALES_ORDER_STATUS_EXPIRED,
                ':reserved' => self::SALES_ORDER_STATUS_RESERVED,
                ':sold' => self::SALES_ORDER_STATUS_SOLD,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );

            $data_reservesLot = array(
                array('so.idSalesOrder', 'idSalesOrder')
                , array('us.fullName', 'user')
                , array('of.name', 'office')
                , array('so.documentType', 'documentType')
                , DB::expr("CONCAT(so.serie,'-',so.correlative) as correlative")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'lotName')
                , DB::expr("DATE_FORMAT(so.orderSalesDate,:date_time) as transactionDate")
                , array('sod.unitPrice', 'unitPrice')
                , array('sod.quantity', 'quantity')
                , array('sod.discount', 'discount')
                , array('so.methodPayment', 'methodPayment')
                , array('so.amountToPaid', 'amountToPaid')
                , array('so.amountPaid', 'amountPaid')
                , array('so.idCashBox', 'idCashBox')
                , array('so.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(so.status=:expired ,:s_active,:s_deactive) as isExpired")
                , DB::expr("IF(so.methodPayment=:cash ,:s_active,:s_deactive) as isCash")
                , DB::expr("IF(so.methodPayment=:credit,:s_active,:s_deactive) as isCredit")
                , DB::expr("IF(so.idCashboxCancel IS NOT NULL AND so.status=:canceled,:s_active,:s_deactive) as isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectReservesLot = DB::select_array($data_reservesLot)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sod.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('so.idCashBox OR cb.idCashBox=so.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.status', 'IN', DB::expr("(:expired,:canceled,:reserved,:sold)"));
                    
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectReservesLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectReservesLot->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_selectReservesLot->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectReservesLot->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            
            $sql_selectReservesLot->parameters($parameters)->group_by('so.idSalesOrder');

            $data_reservesDetail = $data_reservesLot;
            $data_reservesDetail [6] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectReservesDetail = DB::select_array($data_reservesDetail)
                    ->union($sql_selectReservesLot, TRUE)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sod.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('so.idCashBox OR cb.idCashBox=so.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.status', 'IN', DB::expr("(:expired,:canceled,:reserved,:sold)"));
            
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectReservesDetail->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectReservesDetail->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_selectReservesDetail->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectReservesDetail->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_reservation = $sql_selectReservesDetail->parameters($parameters)->group_by('so.idSalesOrder');
            
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_reservation;
    }

    //Venta de Reservas Confirmadas
    public function getReservationSold($d, $sd, $ed, $m, $a, $idUser, $idOffice) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW,$rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW,$rDateFormat);
            
            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_ORDER_PAYMENT_CASH,
                ':credit' => self::SALES_ORDER_PAYMENT_CREDIT,
                ':canceled' => self::SALES_ORDER_STATUS_CANCELED,
                ':expired' => self::SALES_ORDER_STATUS_EXPIRED,
                ':reserved' => self::SALES_ORDER_STATUS_RESERVED,
                ':sales_order'=>self::SALES_ORDER,
                ':sd' => $sd,
                ':ed' => $ed,
                ':a' => $a,
                ':m' => $m,
                ':d' => $d
            );

            $data_reservesLot = array(
                array('so.idSalesOrder', 'idSalesOrder')
                , array('us.fullName','user')
                , array('of.name','office')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(so.serie,'-',so.correlative) as correlative")
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'lotName')
                , DB::expr("DATE_FORMAT(so.confirmDate,:date_time) as transactionDate")
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.discount', 'discount')
                , array('sd.quantity', 'quantity')
                , array('so.amountToPaid', 'amountToPaid')
                , array('so.amountPaid', 'amountPaid')
                , array('so.idCashBox', 'idCashBox')
                , array('so.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(so.status=:expired ,:s_active,:s_deactive) as isExpired")
                , DB::expr("IF(so.methodPayment=:cash ,:s_active,:s_deactive) as isCash")
                , DB::expr("IF(so.methodPayment=:credit,:s_active,:s_deactive) as isCredit")
                , DB::expr("IF(so.idCashboxCancel IS NOT NULL AND so.status=:canceled,:s_active,:s_deactive) as isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectReservesLot = DB::select_array($data_reservesLot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_salesorder', 'so'))->on('so.idSalesOrder', '=', 'sa.idSalesOrder')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('so.idCashBoxConfirm OR cb.idCashBox=so.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->where('so.status', 'NOT IN', DB::expr("(:expired,:canceled,:reserved)"))
                    ->and_where('sa.salesType','=',':sales_order');
            
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectReservesLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectReservesLot->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_selectReservesLot->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectReservesLot->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectReservesLot->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            
            $sql_selectReservesLot->parameters($parameters)->group_by('so.idSalesOrder');

            $data_reservesDetail = $data_reservesLot;
            $data_reservesDetail [7] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectReservesDetail = DB::select_array($data_reservesDetail)
                    ->union($sql_selectReservesLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_salesorder', 'so'))->on('so.idSalesOrder', '=', 'sa.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'cb'))->on('cb.idCashBox', '=', DB::expr('so.idCashBoxConfirm OR cb.idCashBox=so.idCashBoxCancel'))
                    ->join(array('bts_office', 'of'))->on('of.idOffice', '=', 'cb.idOffice')
                    ->join(array('bts_person', 'us'))->on('us.idPerson', '=', 'cb.idUser')
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.status', 'NOT IN', DB::expr("(:expired,:canceled,:reserved)"))
                    ->and_where('sa.salesType','=',':sales_order');
            
            if ($idOffice != NULL AND is_array($idOffice)) {
                $sql_selectReservesLot->and_where('cb.idOffice', 'IN', DB::expr("(" . implode(',', $idOffice) . ")"));
            }
            if ($idUser != NULL AND is_array($idUser)) {
                $sql_selectReservesDetail->and_where('cb.idUser', 'IN', DB::expr("(" . implode(',', $idUser) . ")"));
            }
            if ($d != "") {
                $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_selectReservesDetail->and_where(DB::expr("MONTH(cb.openDate)"), '=', DB::expr(":m AND YEAR(cb.openDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectReservesDetail->and_where('cb.openDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectReservesDetail->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }       

            $sql_reservationSold = $sql_selectReservesDetail->parameters($parameters)->group_by('so.idSalesOrder');
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_reservationSold;
    }

}