<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
defined('SYSPATH') OR die('No direct script access.');

class Controller_Reports_Creditcardsales extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('creditcardsales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('creditcardsales', self::FILE_TYPE_JS));
        $view_creditsales = new View('reports/creditcardsales');
        $view_creditsales->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_creditsales->a_year_report = $this->a_year_report();
        $view_creditsales->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $this->template->content = $view_creditsales;
    }

    public function action_reportCreditcardsales() {
        $this->auto_render = false;
        try {
            $creditsales = array();
            $creditsales['qlang'] = $this->request->query(self::RQ_LANG);
            $creditsales['searchDate'] = DateHelper::getFechaFormateadaActual();
            $creditsales['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];

            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $d = $this->request->query('d');

            if ($d != "") {
                $creditsales['date'] = $d;
            }

            if ($m != 0) {
                $creditsales['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $creditsales['date'] = $sd . " - " . $ed;
                } else {
                    $creditsales['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $creditsales['date'] = $sd;
                }
            }

            $total_creditsales = array();

            $a_ticket = $this->getSalesCredit($d, $sd, $ed, $m, $a)->execute()->as_array();
            $i_id_paypal = null;
            $a_paypal_validator = array();
            $a_ticket_result = array();
            foreach ($a_ticket as $a_item) {

                if (!in_array($a_item['idPayment'], $a_paypal_validator)) {
                    array_push($a_paypal_validator, $a_item['idPayment']);
                    $a_item['amountCardSum'] = $a_item['amountCard'];
                } else {
                    $a_item['amountCard'] = 0;
                }
                $i_id_paypal = $a_item['idPayment'];
                $a_ticket_result[] = $a_item;
            }

            $total_creditsales['ticket'] = $a_ticket_result;


//             print_r(Database::instance()->last_query);
//             die();
            $this->fnResponseFormat($total_creditsales, self::REPORT_RESPONSE_TYPE_XML, $creditsales);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    //Ventas - Tarjeta de Credito
    public function getSalesCredit($d, $sd, $ed, $m, $a) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $rDateFormat);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $rDateFormat);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':credit' => self::SALES_PAYMENT_CREDITCARD,
                ':mixto' => self::SALES_PAYMENT_MIXTO,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_inactive' => self::STATUS_DEACTIVE,
                ':time_format' => $time_format,
                ':date_format' => $date_format,
                ':date_time' => $date_time,
                ':d' => $d,
                ':m' => $m,
                ':a' => $a,
                ':sd' => $sd,
                ':ed' => $ed
            );
            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('pc.idPaymentCard', 'idPaymentCard')
                , array('sa.idPaymentCard', 'idPayment')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , DB::expr("DATE_FORMAT(pc.dateRegister,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale')
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , DB::expr("((sd.unitPrice * sd.quantity) - sd.discount) as amountSales")
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.methodPayment', 'methodPayment')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_paymentcard', 'pc'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idPaymentCard', '=', 'pc.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->where('sa.methodPayment', 'IN', DB::expr("(:credit,:mixto)"))
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($d != "") {
                $sql_selectLot->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_selectLot->and_where(DB::expr("MONTH(pc.dateRegister)"), '=', DB::expr(":m AND YEAR(pc.dateRegister) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_selectLot->and_where('pc.dateRegister', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_selectLot->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_productLot = $sql_selectLot->group_by('sd.idSalesDetail')->parameters($parameters);

            $data_sales_detail = $data_sales_lot;

            $sql_productDetail = DB::select_array($data_sales_detail)
                    ->union($sql_productLot, TRUE)
                    ->from(array('rms_paymentcard', 'pc'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idPaymentCard', '=', 'pc.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                     ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->where('sa.methodPayment', 'IN', DB::expr("(:credit,:mixto)"))
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($d != "") {
                $sql_productDetail->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql_productDetail->and_where(DB::expr("MONTH(pc.dateRegister)"), '=', DB::expr(":m AND YEAR(pc.dateRegister) = :a"));
            }

            if ($ed != "") {
                if ($sd != "") {
                    $sql_productDetail->and_where('pc.dateRegister', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_productDetail->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_productDetail->and_where(DB::expr("DATE_FORMAT(pc.dateRegister,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }

            $sql_creditCardSales = $sql_productDetail->group_by('sd.idSalesDetail')->parameters($parameters);
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }

        return $sql_creditCardSales;
    }

}
