<?php

class Controller_Reports_SalesmenSelling extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('salesmenselling', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('salesmenselling', self::FILE_TYPE_JS));
        $view_salesmenselling = new View('reports/salesmenselling');

        $vendedores = DB::select('us.idUser', 'us.userName')
                        ->from(array('bts_user', 'us'))
                        ->join(array('bts_group', 'go'))->on('go.idGroup', '=', 'us.idGroup')
                        ->where('go.idGroup', '=', 2)
                        ->execute()->as_array();

        $view_salesmenselling->vendedores = $vendedores;

        $view_salesmenselling->a_year_report = $this->a_year_report();
        $view_salesmenselling->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_salesmenselling->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_salesmenselling->user_id = $this->getSessionParameter(self::USER_ID);
        $this->template->content = $view_salesmenselling;
    }

    public function action_reportSalesmen() {
        $this->auto_render = false;
        try {

            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $salesData = array();
            $salesData ['rDate'] = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE, $this->request->query(self::RQ_DATE_FORMAT)));
            $salesData ['qlang'] = $this->request->query(self::RQ_LANG);
            $salesData ['searchDate'] = DateHelper::getFechaFormateadaActual();

            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $d = $this->request->query('d');
            $sales = $this->request->query('sales');

            if ($d != "") {
                $salesData['date'] = $d;
            }

            if ($m != 0) {
                $salesData['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $salesData['date'] = $sd . " - " . $ed;
                } else {
                    $salesData['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $salesData['date'] = $sd;
                }
            }


            $parameter = array(
                array('sa.idSales', 'ID_DE_VENTA'),
                array('us.userName', 'VENDEDOR'),
                array('po.idProduct', 'ID_DE_PRODUCTO'),
                array('po.productName', 'PRODUCTO')
            );


            $consul = DB::select_array($parameter)
                    ->from(array('rms_products', 'po'))
                    ->join(array('rms_lot', 'lo'))->on('lo.idProduct', '=', 'po.idProduct')
                    ->join(array('rms_salesdetail', 'sad'))->on('sad.idLot', '=', 'lo.idLot')
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'sad.idSales')
                    ->join(array('bts_user', 'us'))->on('us.idUser', '=', 'sa.idUser')
                    ->where('us.idUser', '=', $sales);


            if ($d != "") {
                $consul->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $d);
            }
            if ($m != 0) {
//                print_r($m);
                $consul->and_where(DB::expr("MONTH(sa.salesDate)"), '=', $m);
                $consul->and_where(DB::expr("YEAR(sa.salesDate)"), '=', $a);
            }
            if ($ed != "") {
                if ($sd != "") {
                    $consul->and_where('sa.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $consul->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $consul->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $ed);
                }
            }

//            $consul = $consul->order_by('sa.idSales')->parameters($parameter)->execute()->as_array();
            $sql = $consul->order_by('sa.idSales')->execute()->as_array();

            $this->fnResponseFormat($sql, self::REPORT_RESPONSE_TYPE_XML, $salesData);
            //print_r(Database::instance()->last_query);
//             die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
