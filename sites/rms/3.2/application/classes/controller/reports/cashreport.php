<?php

/**
 * Description of customers
 *
 * @author Escritorio
 */
class Controller_Reports_Cashreport extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('cashreport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('cashreport', self::FILE_TYPE_JS));
        $view_cashReport = new View('reports/cashreport');
        $view_cashReport->a_year_report = $this->a_year_report();
        $view_cashReport->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);;
        $view_cashReport->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $obj_store=ORM::factory('stores')
                ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $view_cashReport->array_store=$obj_store; 
        $this->template->content = $view_cashReport;
    }

    public function action_reportSales() {
        try {
            $d = $this->request->query('d');
            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $a = $this->request->query('a');
            $m = $this->request->query('m');
            $store = $this->request->query('store');
            
            $reportSales = array();
            $reportSales['qlang'] = $this->request->query(self::RQ_LANG);
            $reportSales['searchDate'] = DateHelper::getFechaFormateadaActual();
            $storeName = array('s.storeShortName','storeShortName');
            $dataStore = DB::select_array($storeName)
                            ->from(array('rms_store', 's'))
                             ->where('s.idStore', '=', $store)
                            ->as_object()->execute()->current();
            $reportSales['store'] = $dataStore->storeShortName;
            if ($d != "") {
                $reportSales['date'] = $d;
            }
            if ($m != 0) {
                $reportSales['date'] = $this->getMonthName($m) . " - " . $a;
            }
            if ($ed != "") {
                if ($sd != "") {
                    $reportSales['date'] = $sd . " - " . $ed;
                } else {
                    $reportSales['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $reportSales['date'] = $sd;
                }
            }
            $data_reportSales = array();

            $data_reportSales['incomes'] = $this->getIncomeSales($d, $ed, $sd, $m, $a,$store)->execute()->as_array();
            $data_reportSales['expenses'] = $this->getExpensesSales($d, $ed, $sd, $m, $a,$store)->execute()->as_array();

            $this->fnResponseFormat($data_reportSales, self::REPORT_RESPONSE_TYPE_XML, $reportSales);
            //print_r(Database::instance()->last_query);
            //die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function getIncomeSales($d, $ed, $sd, $m, $a,$store) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            // $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));
            
            $a_dataSalesDetail = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                DB::expr("CONCAT(s.serie,'-',s.correlative) AS NRO_DOCUMENTO"),
                array('po.productName','DESCRIPCION'),
                DB::expr("GROUP_CONCAT(DISTINCT pd.serie_electronic) AS DETAIL"),
                DB::expr("COUNT(sd.quantity) AS CANTIDAD"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('po.idCategory', 'CATEGORIA'),
                array('pd.unitPrice', 'PRECIO_COMPRA_UNITARIO')
            );
            $selectSalesDetail = DB::select_array($a_dataSalesDetail)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'po'))->on('pd.idProduct', '=', 'po.idProduct')
                    ->where('s.status', 'IN', DB::expr("('" . self::SALES_STATUS_SOLD . "')"))
                    ->and_where_open()
                    ->and_where('po.status', '=', '1')
                    ->and_where_close();
            $selectSalesDetail=$selectSalesDetail->group_by('sd.idSalesDetail');
            
            $a_dataSalesLot = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                DB::expr("CONCAT(s.serie,'-',s.correlative) AS NRO_DOCUMENTO"),
                array('po.productName','DESCRIPCION'),                
                array('lot.lotName', 'DETAIL'),
                DB::expr("SUM(sd.quantity)  AS CANTIDAD"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('po.idCategory', 'CATEGORIA'),
                array('lot.unitPrice', 'PRECIO_COMPRA_UNITARIO')
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->union($selectSalesDetail, TRUE)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'po'))->on('po.idProduct', '=', 'lot.idProduct')
                    ->where('s.status', 'IN', DB::expr("('" . self::SALES_STATUS_SOLD . "')"))
                    ->and_where_open()
                    ->and_where('po.status', '=', '1') 
                    ->and_where_close();
            if ($store != "") {
                $selectSalesDetail->and_where('sd.idStore', '=', $store);
                $selectSalesLot->and_where('sd.idStore', '=', $store);
            }
            if ($d != "") {
                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
            }
            if ($m != 0) {
                $selectSalesDetail->and_where(DB::expr("MONTH(s.salesDate)"), '=', $m);
                $selectSalesDetail->and_where(DB::expr("YEAR(s.salesDate)"), '=', $a);
                $selectSalesLot->and_where(DB::expr("MONTH(s.salesDate)"), '=', $m);
                $selectSalesLot->and_where(DB::expr("YEAR(s.salesDate)"), '=', $a);
            }
            if ($ed != "") {
                if ($sd != "") {
                    $selectSalesDetail->and_where('s.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                    $selectSalesLot->and_where('s.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $ed);
                    $selectSalesLot->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $sd);
                    $selectSalesLot->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $sd);
                }
            }
            
            $selectSalesLot=$selectSalesLot->group_by('sd.idSalesDetail');
            
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }

    public function getExpensesSales($d, $ed, $sd, $m, $a,$store) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
           // $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $dataExpensesSales = array(
                array('v.idVoucher', 'ID_VOUCHER'),
                array('v.documentType', 'TIPO_DOCUMENTO'),
                DB::expr("CONCAT(v.serie,'-',v.Correlative) AS NRO_DOCUMENTO"),
                array('pe.fullName', 'AUTHORIZES'),
                array('v.concept', 'DESCRIPCION'),
                array('v.amount', 'MONTO')
            );
            $selectExpensesSales = DB::select_array($dataExpensesSales)
                    ->from(array('rms_voucher', 'v'))
                    ->join(array('bts_person','pe'))->on('pe.idPerson','=','v.idAuthorizes')
                    ->join(array('bts_user','us'))->on('us.idUser','=','v.userCreate')
                    ->join(array('bts_office','of'))->on('of.idOffice','=','us.idOffice')
                    ->join(array('rms_store','st'))->on('st.idStore','=','v.idStore')
                    ->where('v.status', '=', '1');
            if ($store != "") {
                $selectExpensesSales->and_where('st.idStore', '=', $store);
              
            }
            if ($d != "") {
                $selectExpensesSales->and_where(DB::expr("DATE_FORMAT(v.voucherDate,'$date_format')"), 'LIKE', $d);
            }
            if ($m != 0) {
                $selectExpensesSales->and_where(DB::expr("MONTH(v.voucherDate)"), '=', $m);
                $selectExpensesSales->and_where(DB::expr("YEAR(v.voucherDate)"), '=', $a);
            }
            if ($ed != "") {
                if ($sd != "") {
                    $selectExpensesSales->and_where('v.voucherDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $selectExpensesSales->and_where(DB::expr("DATE_FORMAT(v.voucherDate,'$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $selectExpensesSales->and_where(DB::expr("DATE_FORMAT(v.voucherDate,'$date_format')"), 'LIKE', $sd);
                }
            }
            $selectExpensesSales = $selectExpensesSales->group_by('v.idVoucher')
                    ->order_by('v.idVoucher');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectExpensesSales;
    }

}

?>