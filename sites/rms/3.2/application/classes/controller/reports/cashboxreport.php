<?php

class Controller_Reports_Cashboxreport extends Controller_Private_Admin implements Rms_Constants {
   
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('cashboxreport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('cashboxreport', self::FILE_TYPE_JS));
        $view_cashboxreport = new View('reports/cashboxreport');
        $view_cashboxreport->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_cashboxreport->date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_cashboxreport->user_id = $this->getSessionParameter(self::USER_ID);
        $this->template->content = $view_cashboxreport;
    }

    public function action_loadCashByDay() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $user_id = $this->request->post('user_id');
            $date = $this->request->post('date');
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            
            $cashBox = DB::select(array('cb.idCashbox', 'idCashBox'),array(DB::expr("CONCAT( DATE_FORMAT(cb.openDate,'$date_format $time_format'),' - ',(IF(cb.closeDate IS NULL,'No Cerrada',DATE_FORMAT(cb.closeDate,'$date_format $time_format'))))"),'cashBox'))
                            ->from(array('rms_cashbox', 'cb'))
                            ->where('cb.idUser', '=', $user_id)
                            ->and_where(DB::expr("DATE_FORMAT(cb.openDate,'$date_format')"), '=', $date)
                            ->execute()->as_array();
            $aResponse['data'] = $cashBox;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_reportCashReportDayli() {
        try {

            $reportSales = array();
            $d = $this->request->query('d');
            $user_id = $this->request->query('user_id');
            $idCashBox = $this->request->query('idCashBox');
           

            $reportSales['qlang'] = $this->request->query(self::RQ_LANG);
            $reportSales['searchDate'] = DateHelper::getFechaFormateadaActual();
            $reportSales['seller'] = ORM::factory('person', $user_id)->fullName;

            $openDate = array(DB::expr("MIN(DATE_FORMAT(`cb`.`openDate`,' %H:%i %p')) AS openDate"),
                DB::expr("MAX(DATE_FORMAT(`cb`.`closeDate`,' %H:%i %p')) AS closeDate"));

            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));

            $dateCashBox = DB::select_array($openDate)
                            ->from(array('rms_cashbox', 'cb'))
                            ->where(DB::expr("DATE_FORMAT(cb.openDate, '$date_format')"), 'LIKE', $this->request->query('d'))
                            ->and_where('cb.idUser', '=', $user_id)
                            ->as_object()->execute()->current();

            $reportSales['openDate'] = $dateCashBox->openDate;
            $reportSales['closeDate'] = $dateCashBox->closeDate;
            
            $amount = array(DB::expr("SUM(`cb`.`initAmount`) AS initAmount"),
                DB::expr("SUM(`cb`.`endAmount`) AS endAmount"));
            
            $amountCashBox = DB::select_array($amount)
                            ->from(array('rms_cashbox', 'cb'))
                            ->where(DB::expr("DATE_FORMAT(cb.openDate, '$date_format')"), 'LIKE', $this->request->query('d'))
                            ->and_where('cb.idUser', '=', $user_id)
                            ->as_object()->execute()->current();
            
            $reportSales['iamount'] = $amountCashBox->initAmount;
            $reportSales['eamount'] = $amountCashBox->endAmount;

            $data_reportSales = array();
            $data_reportSales['salesCash'] = $this->getSalesCash($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['salesCredit'] = $this->getSalesCredit($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['salesMixto'] = $this->getSalesMixto($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['creditNoteUsed'] = $this->getCreditNoteUsed($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['creditNoteCreated'] = $this->getCreditNoteCreated($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['reservation'] = $this->getReservation($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['reservationSold'] = $this->getReservationSold($d,$user_id, $idCashBox)->execute()->as_array();
            $data_reportSales['voucher'] = $this->getVoucher($d,$user_id, $idCashBox)->execute()->as_array();

            $this->fnResponseFormat($data_reportSales, self::REPORT_RESPONSE_TYPE_XML, $reportSales);
            // print_r(Database::instance()->last_query);
            // die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    // Vales de Ingresos e Egresos
    public function getVoucher($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
            );
            $cols = array(
                array('v.idVoucher', 'idVoucher')
                , array('v.documentType', 'documentType')
                , DB::expr("CONCAT(v.serie,'-',v.correlative) as voucherNumber")
                , DB::expr("DATE_FORMAT(v.voucherDate,:date_time) as transactionDate")
                , array('v.concept', 'description')
                , array('v.amount', 'amount')
                , array('pe.fullName', 'authorizesBy')
                , array('v.idCashBox', 'idCashBox')
                , array('v.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(v.idCashboxCancel IS NOT NULL and v.status = :s_deactive,:s_active,:s_deactive) AS isCancel")
            );
            $sql_voucher = DB::select_array($cols)
                    ->from(array('rms_cashbox', 'cb'))
                    ->join(array('rms_voucher', 'v'))->on('cb.idCashBox', '=', DB::expr('v.idCashBox OR cb.idCashBox=v.idCashBoxCancel'))
                    ->join(array('bts_person', 'pe'))->on('pe.idPerson', '=', 'v.idAuthorizes')
                    ->where('cb.idCashbox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('v.userCreate', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(cb.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters);

            //print_r(Database::instance()->last_query);die();
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_voucher;
    }

//Notas de Credito - Creadas
    public function getCreditNoteCreated($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':created' => self::CREDITNOTE_CREATE
            );
            $data_creditNote = array(
                array('cn.idCreditNote', 'idCreditNote')
                , array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('cn.amountSale', 'amountCreditNote')
                , array('sa.totalPrice', 'amountSales')
                , array('cn.description', 'description')
                , DB::expr("DATE_FORMAT(cn.cancelDate,:date_time) as transactionDate")
                , array('pro.productName', 'productName')
                , array('ca.idCashBox', 'idCashBox')
                , array('cn.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(cn.idCashboxCancel IS NOT NULL,:s_active,:s_deactive ) AS isCancel"),
            );

            $sql_creditNote = DB::select_array($data_creditNote)
                    ->from(array('rms_credit_note', 'cn'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'cn.idSales')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('cn.idCashBox OR ca.idCashBox=cn.idCashBoxCancel'))
                    ->where('cn.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->where('cn.status', 'IN', DB::expr("(:created)"))
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->and_where('cn.userCreate', '=', $user_id)
                    ->parameters($parameters);

            $creditNoteCreated = $sql_creditNote;
        } catch (Exception $e) {
            echo $this->handlingError($e);
        }

        return $creditNoteCreated;
    }

    //Notas de Credito - Canjeadas
    public function getCreditNoteUsed($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':used' => self::CREDITNOTE_USED
            );
            $data_creditNote = array(
                array('cn.idCreditNote', 'idCreditNote')
                , array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('cn.amountSale', 'amountCreditNote')
                , array('sa.totalPrice', 'amountSales')
                , array('cn.description', 'description')
                , DB::expr("DATE_FORMAT(cn.confirmDate,:date_time) as transactionDate")
                , array('pro.productName', 'productName')
                , array('ca.idCashBox', 'idCashBox')
                , array('cn.idCashBoxConfirm', 'idCashBoxConfirm')
                , array('cn.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(cn.idCashboxCancel IS NOT NULL,:s_active,:s_deactive ) AS isCancel"),
            );

            $sql_creditNote = DB::select_array($data_creditNote)
                    ->from(array('rms_credit_note', 'cn'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'cn.idNewSales')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('cn.idCashBoxConfirm OR ca.idCashBox=cn.idCashBoxCancel'))
                    ->where('cn.idCashBoxConfirm', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->where('cn.status', 'IN', DB::expr("(:used)"))
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->and_where('cn.userConfirm', '=', $user_id)
                    ->parameters($parameters);

            $creditNoteUsed = $sql_creditNote;
        } catch (Exception $e) {
            echo $this->handlingError($e);
        }

        return $creditNoteUsed;
    }

    // Ventas Efectivo
    public function getSalesCash($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_PAYMENT_CASH,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD
            );

            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale')    
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':cash')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [4] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':cash')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $sql_salesCash = $sql_selectProductDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesCash;
    }

    //Venta Tarjeta de Credito
    public function getSalesCredit($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':credit' => self::SALES_PAYMENT_CREDITCARD,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD
            );
            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale') 
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':credit')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [4] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':credit')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $sql_salesCreditCard = $sql_selectProductDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesCreditCard;
    }

    //Ventas Mixto : Efectivo-Tarjeta Credito
    public function getSalesMixto($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':mixto' => self::SALES_PAYMENT_MIXTO,
                ':canceled' => self::SALES_STATUS_CANCELED,
                ':sold' => self::SALES_STATUS_SOLD
            );
            $data_sales_lot = array(
                array('sa.idSales', 'idSales')
                , array('sa.documentType', 'documentType')
                , DB::expr("CONCAT(sa.serie,' - ',sa.correlative) AS ticketNumber")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'productDetail')
                , DB::expr("DATE_FORMAT(sa.salesDate,:date_time) as transactionDate")
                , array('sa.salesType', 'typeSale') 
                , array('sd.unitPrice', 'unitPrice')
                , array('sd.quantity', 'quantity')
                , array('sd.discount', 'discount')
                , array('sd.taxpercentage', 'taxPercentage')
                , array('sd.totaltax', 'totalTax')
                , array('sa.documentType', 'documentType')
                , array('sa.methodPayment', 'methodPayment')
                , array('sa.idCashBox', 'idCashBox')
                , array('sa.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(sa.idCashboxCancel IS NOT NULL and sa.status=:canceled ,:s_active,:s_deactive) AS isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectLot = DB::select_array($data_sales_lot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':mixto')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $data_sales_detail = $data_sales_lot;
            $data_sales_detail [4] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectProductDetail = DB::select_array($data_sales_detail)
                    ->union($sql_selectLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 'sa.idPaymentCard')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('sa.idCashBox OR ca.idCashBox=sa.idCashBoxCancel'))
                    ->where('sa.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('sa.methodPayment', '=', ':credit')
                    ->and_where('sa.status', 'IN', DB::expr("(:sold,:canceled)"))
                    ->and_where('sa.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('sd.idSalesDetail');

            $sql_salesMixto = $sql_selectProductDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_salesMixto;
    }

    //Venta de Reservas
    public function getReservation($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_ORDER_PAYMENT_CASH,
                ':credit' => self::SALES_ORDER_PAYMENT_CREDIT,
                ':canceled' => self::SALES_ORDER_STATUS_CANCELED,
                ':expired' => self::SALES_ORDER_STATUS_EXPIRED,
                ':reserved' => self::SALES_ORDER_STATUS_RESERVED,
                ':sold'=>self::SALES_ORDER_STATUS_SOLD
            );

            $data_reservesLot = array(
                array('so.idSalesOrder', 'idSalesOrder')
                , array('so.documentType', 'documentType')
                , DB::expr("CONCAT(so.serie,'-',so.correlative) as correlative")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'lotName')
                , DB::expr("DATE_FORMAT(so.orderSalesDate,:date_time) as transactionDate")
                , array('sod.unitPrice', 'unitPrice')
                , array('sod.quantity', 'quantity')
                , array('sod.discount', 'discount')
                , array('so.methodPayment', 'methodPayment')
                , array('so.amountToPaid', 'amountToPaid')
                , array('so.amountPaid', 'amountPaid')
                , array('so.idCashBox', 'idCashBox')
                , array('so.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(so.status=:expired ,:s_active,:s_deactive) as isExpired")
                , DB::expr("IF(so.methodPayment=:cash ,:s_active,:s_deactive) as isCash")
                , DB::expr("IF(so.methodPayment=:credit,:s_active,:s_deactive) as isCredit")
                , DB::expr("IF(so.idCashboxCancel IS NOT NULL AND so.status=:canceled,:s_active,:s_deactive) as isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectReservesLot = DB::select_array($data_reservesLot)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sod.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('so.idCashBox OR ca.idCashBox=so.idCashBoxCancel'))
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('so.status', 'IN', DB::expr("(:expired,:canceled,:reserved,:sold)"))
                    ->and_where('so.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('so.idSalesOrder');

            $data_reservesDetail = $data_reservesLot;
            $data_reservesDetail [4] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectReservesDetail = DB::select_array($data_reservesDetail)
                    ->union($sql_selectReservesLot, TRUE)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sod.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('so.idCashBox OR ca.idCashBox=so.idCashBoxCancel'))
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('so.status', 'IN', DB::expr("(:expired,:canceled,:reserved,:sold)"))
                    ->and_where('so.idUser', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('so.idSalesOrder');

            $sql_reservation = $sql_selectReservesDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_reservation;
    }

    //Venta de Reservas
    public function getReservationSold($d,$user_id, $idCashBox) {
        try {
            $rDateFormat = $this->request->query(self::RQ_DATE_FORMAT);
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $rDateFormat);
            $date_time = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE_TIME, $rDateFormat);

            $parameters = array(
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':s_active' => self::STATUS_ACTIVE,
                ':s_deactive' => self::STATUS_DEACTIVE,
                ':cash' => self::SALES_ORDER_PAYMENT_CASH,
                ':credit' => self::SALES_ORDER_PAYMENT_CREDIT,
                ':canceled' => self::SALES_ORDER_STATUS_CANCELED,
                ':expired' => self::SALES_ORDER_STATUS_EXPIRED,
                ':reserved' => self::SALES_ORDER_STATUS_RESERVED
            );

            $data_reservesLot = array(
                array('so.idSalesOrder', 'idSalesOrder')
                , array('so.documentType', 'documentType')
                , DB::expr("CONCAT(so.serie,'-',so.correlative) as correlative")
                , array('pro.productName', 'productName')
                , array('lo.lotName', 'lotName')
                , DB::expr("DATE_FORMAT(so.confirmDate,:date_time) as transactionDate")
                , array('sod.unitPrice', 'unitPrice')
                , array('sod.discount', 'discount')
                , array('sod.quantity', 'quantity')
                , array('so.amountToPaid', 'amountToPaid')
                , array('so.amountPaid', 'amountPaid')
                , array('so.idCashBox', 'idCashBox')
                , array('so.idCashBoxCancel', 'idCashBoxCancel')
                , DB::expr("IF(so.status=:expired ,:s_active,:s_deactive) as isExpired")
                , DB::expr("IF(so.methodPayment=:cash ,:s_active,:s_deactive) as isCash")
                , DB::expr("IF(so.methodPayment=:credit,:s_active,:s_deactive) as isCredit")
                , DB::expr("IF(so.idCashboxCancel IS NOT NULL AND so.status=:canceled,:s_active,:s_deactive) as isCancel")
                , array('pc.idTypeCard', 'typeCard')
                , array('pc.cardNumber', 'cardNumber')
                , array('pc.operationNumber', 'operationNumber')
                , array('pc.amount', 'amountCard')
            );

            $sql_selectReservesLot = DB::select_array($data_reservesLot)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_lot', 'lo'))->on('lo.idLot', '=', 'sod.idLot')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'lo.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('so.idCashBoxConfirm OR ca.idCashBox=so.idCashBoxCancel'))
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('so.status', 'NOT IN', DB::expr("(:expired,:canceled,:reserved)"))
                    ->and_where('so.userConfirm', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('so.idSalesOrder');

            $data_reservesDetail = $data_reservesLot;
            $data_reservesDetail [4] = DB::expr("GROUP_CONCAT(pd.serie_electronic) AS productDetail");

            $sql_selectReservesDetail = DB::select_array($data_reservesDetail)
                    ->union($sql_selectReservesLot, TRUE)
                    ->from(array('rms_salesorder', 'so'))
                     ->join(array('rms_salesorderdetail', 'sod'))->on('sod.idSalesOrder', '=', 'so.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sod.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('pro.idProduct', '=', 'pd.idProduct')
                    ->join(array('rms_cashbox', 'ca'))->on('ca.idCashBox', '=', DB::expr('so.idCashBoxConfirm OR ca.idCashBox=so.idCashBoxCancel'))
                    ->join(array('rms_paymentcard', 'pc'), 'LEFT')->on('pc.idPaymentCard', '=', 'so.idPaymentCard')
                    ->where('so.idCashBox', 'IN', DB::expr('(' . $idCashBox . ')'))
                    ->and_where('so.status', 'NOT IN', DB::expr("(:expired,:canceled,:reserved)"))
                    ->and_where('so.userConfirm', '=', $user_id)
                    ->and_where(DB::expr("DATE_FORMAT(ca.openDate,:date_format)"), 'LIKE', $d)
                    ->parameters($parameters)
                    ->group_by('so.idSalesOrder');

            $sql_reservationSold = $sql_selectReservesDetail;
        } catch (Exception $e_exc) {
            echo $this->errorHandling($e_exc);
        }

        return $sql_reservationSold;
    }

}
