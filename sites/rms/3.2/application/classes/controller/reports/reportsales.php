<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reportpurchase
 *
 * @author Escritorio
 */
class Controller_Reports_Reportsales extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportsales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportsales', self::FILE_TYPE_JS));
        $view_reportSales = new View('reports/reportsales');
        $view_reportSales->a_year_report = $this->a_year_report();
        $view_reportSales->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_reportSales->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $view_reportSales;
    }

    public function action_reportSales() {
        $this->auto_render = false;
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $salesData = array();
            $salesData ['rDate'] = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE, $this->request->query(self::RQ_DATE_FORMAT)));
            $salesData ['qlang'] = $this->request->query(self::RQ_LANG);
            $salesData ['searchDate'] = DateHelper::getFechaFormateadaActual();

            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $d = $this->request->query('d');

            if ($d != "") {
                $salesData['date'] = $d;
            }

            if ($m != 0) {
                $salesData['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $salesData['date'] = $sd . " - " . $ed;
                } else {
                    $salesData['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $salesData['date'] = $sd;
                }
            }

            $parameter = array(
                ':cash' => self::SALES_PAYMENT_CASH,
                ':credit' => self::SALES_PAYMENT_CREDITCARD,
                ':mixto' => self::SALES_PAYMENT_MIXTO,
                ':sold'=>self::SALES_STATUS_SOLD,
                ':canceled'=>self::SALES_STATUS_CANCELED
            );

            $selectSales = array(
                array('sa.idSales', 'idSales'),
                array('sa.documentType', 'CodeNumber'),
                DB::expr("DATE_FORMAT(sa.salesDate,'$date_format') AS SalesDate"),
                array('sa.serie', 'Serie'),
                array('sa.status', 'Status'),
                DB::expr("DATE_FORMAT(sa.salesDate,'%Y') AS YearDate"),
                array('sa.correlative', 'DocumentNumber'),
                array('sa.methodPayment', 'PaymentType'),
                array('sa.taxPercentage', 'TaxPercentage'),
                array('sa.totalTax', 'TotalTax'),
                array('sa.totalPrice', 'TotalAmount')
            );
            $sales = DB::select_array($selectSales)
                    ->from(array('rms_sales', 'sa'))
                    ->where('sa.methodPayment', 'IN', DB::expr("(:cash,:credit,:mixto)"))
                    ->where('sa.status', 'IN', DB::expr("(:sold,:canceled)"));

            if ($d != "") {
                $sales->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $d);
            }
            if ($m != 0) {
                $sales->and_where(DB::expr("MONTH(sa.salesDate)"), '=', $m);
                $sales->and_where(DB::expr("YEAR(sa.salesDate)"), '=', $a);
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sales->and_where('sa.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $sales->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $sales->and_where(DB::expr("DATE_FORMAT(sa.salesDate, '$date_format')"), 'LIKE', $ed);
                }
            }

            $sql_sales = $sales->order_by('sa.idSales')->parameters($parameter)->execute()->as_array();

            $this->fnResponseFormat($sql_sales, self::REPORT_RESPONSE_TYPE_XML, $salesData);

             //print_r(Database::instance()->last_query);
             //die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}

