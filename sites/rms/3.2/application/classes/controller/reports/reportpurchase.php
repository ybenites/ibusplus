<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of reportpurchase
 *
 * @author Escritorio
 */
class Controller_Reports_Reportpurchase extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportpurchase', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportpurchase', self::FILE_TYPE_JS));
        $view_reportPurchase = new View('reports/reportpurchase');
        $view_reportPurchase->a_year_report = $this->a_year_report();
        $view_reportPurchase->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_reportPurchase->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $view_reportPurchase;
    }

    public function action_reportPurchaseOrders() {
        $this->auto_render = false;
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $purchaseData = array();
            $purchaseData ['rDate'] = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE, $this->request->query(self::RQ_DATE_FORMAT)));
            $purchaseData ['qlang'] = $this->request->query(self::RQ_LANG);
            $purchaseData ['searchDate'] = DateHelper::getFechaFormateadaActual();

            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $d = $this->request->query('d');

            if ($d != "") {
                $purchaseData['date'] = $d;
            }

            if ($m != 0) {
                $purchaseData['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $purchaseData['date'] = $sd . " - " . $ed;
                } else {
                    $purchaseData['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $purchaseData['date'] = $sd;
                }
            }

            $parameter = array(
                ':recepcionado' => self::PURCHASE_ORDER_STATUS_RECEPCIONADO,
                ':anulado' => self::PURCHASE_ORDER_STATUS_CANCELADO,
                ':aprobado' => self::PURCHASE_ORDER_STATUS_APROBADO,
                ':facturado' => self::PURCHASE_ORDER_STATUS_FACTURADO,
                ':emitido' => self::PURCHASE_ORDER_STATUS_EMITIDO
            );

            $selectPurchaseOrder = array(
                array('po.idPurchaseOrder', 'idPurchaseOrder'),
                DB::expr("CASE 
                            WHEN po.purchaseDocument = '" . self::OC_FACTURA . "' THEN '01'
                            WHEN po.purchaseDocument = '" . self::OC_RECIBOPORHONORARIOS . "' THEN '02'
                            WHEN po.purchaseDocument = '" . self::OC_BOLETA . "' THEN '03'
                            WHEN po.purchaseDocument = '" . self::OC_NOTACREDITO . "' THEN '07'
                            WHEN po.purchaseDocument = '" . self::OC_NOTADEBITO . "' THEN '08'
                            WHEN po.purchaseDocument = '" . self::OC_GUIAREMISION . "' THEN '09'
                            WHEN po.purchaseDocument = '" . self::OC_TICKETBOLETA . "' THEN '12'
                            WHEN po.purchaseDocument = '" . self::OC_TICKETFACTURA . "' THEN '12'
                            ELSE po.purchaseDocument END AS CodeNumber"),
                DB::expr("DATE_FORMAT(po.datePurchaseOrder,'$date_format') AS IssuedDate"),
                DB::expr("DATE_FORMAT(po.dateDeliveryOrder,'$date_format') AS DeliveryDate"),
                array('po.purchaseDocument', 'DocumentType'),
                array('po.serie', 'Serie'),
                DB::expr("DATE_FORMAT(po.datePurchaseOrder,'%Y') AS YearDate"),
                array('po.correlative', 'DocumentNumber'),
                array('s.name', 'CompanyName'),
                array('s.code', 'CompanyNumber'),
                array('po.methodPayment', 'PaymentType'),
                array('po.currency', 'Currency'),
                array('po.totalTax', 'Tax'),
                array('po.totalAmountWithOutTax', 'SalesValue'),
                array('po.totalAmountWithTax', 'SalesValueTax'),
                array('po.totalAmount', 'TotalAmount')
            );
            $purchaseOrder = DB::select_array($selectPurchaseOrder)
                    ->from(array('rms_purchaseorder', 'po'))
                    ->join(array('rms_supplier', 's'))->on('s.idSupplier', '=', 'po.idSupplier')
                    ->where('po.statusPurchase', 'IN', DB::expr("(:recepcionado,:emitido,:aprobado)"));

            if ($d != "") {
                $purchaseOrder->and_where(DB::expr("DATE_FORMAT(po.datePurchaseOrder, '$date_format')"), 'LIKE', $d);
            }
            if ($m != 0) {
                $purchaseOrder->and_where(DB::expr("MONTH(po.datePurchaseOrder)"), '=', $m);
                $purchaseOrder->and_where(DB::expr("YEAR(po.datePurchaseOrder)"), '=', $a);
            }
            if ($ed != "") {
                if ($sd != "") {
                    $purchaseOrder->and_where('po.datePurchaseOrder', 'BETWEEN', DB::expr("STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')"));
                } else {
                    $purchaseOrder->and_where(DB::expr("DATE_FORMAT(po.datePurchaseOrder, '$date_format')"), 'LIKE', $ed);
                }
            } else {
                if ($sd != "") {
                    $purchaseOrder->and_where(DB::expr("DATE_FORMAT(po.datePurchaseOrder, '$date_format')"), 'LIKE', $ed);
                }
            }

            $sql_purchaseOrder = $purchaseOrder->order_by('po.datePurchaseOrder')->parameters($parameter)->execute()->as_array();

            $this->fnResponseFormat($sql_purchaseOrder, self::REPORT_RESPONSE_TYPE_XML, $purchaseData);

//              print_r(Database::instance()->last_query);
//              die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}

