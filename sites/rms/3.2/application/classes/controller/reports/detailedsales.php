<?php


class Controller_Reports_Detailedsales extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('detailedsales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('detailedsales', self::FILE_TYPE_JS));
        $view_reportSales = new View('reports/detailedsales');
        $view_reportSales->a_year_report = $this->a_year_report();
        $view_reportSales->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);;
        $view_reportSales->a_documents_type = $this->getDocumentsByFilter('sales', true);
        $view_reportSales->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $view_reportSales;
    }

    public function action_listProducts() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $route = DB::select(array('rms_products.productName', 'value'), array('rms_products.idProduct', 'id'))
                            ->from('rms_products')
                            ->where('rms_products.status', '=', '1')
                            ->order_by('rms_products.productName')
                            ->execute()->as_array();
            $aResponse['data'] = $route;
        } catch (Exception $e) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['data'] = $e->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_reportSalesByProducts() {
        $this->auto_render = false;
        try {

            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $salesDetail = array();
            $salesDetail['rDate'] = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE, $this->request->query(self::RQ_DATE_FORMAT)));
            $salesDetail['qlang'] = $this->request->query(self::RQ_LANG);
            $salesDetail['searchDate'] = DateHelper::getFechaFormateadaActual();

            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');
            $d = $this->request->query('d');
            $product = $this->request->query('product');

            if ($d != "") {
                $salesDetail['date'] = $d;
            }

            if ($m != 0) {
                $salesDetail['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $salesDetail['date'] = $sd . " - " . $ed;
                } else {
                    $salesDetail['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $salesDetail['date'] = $sd;
                }
            }

            $parameter = array(
                ':dateformat' => $date_format,
                ':time_format' => $time_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':sd' => $sd,
                ':ed' => $ed,
                ':m' => $m,
                ':a' => $a,
                ':d' => $d,
                ':product' => $product
            );

            //consulta de productos x lote
            $cols_productLot = array(
                array('sa.idSales', 'id')
                , array('per.fullName', 'seller')
                , DB::expr("COALESCE(sa.serie,'000') AS serie")
                , DB::expr("LPAD(COALESCE(sa.correlative),8,'0') AS correlative")
                , DB::expr("DATE_FORMAT(sa.salesDate,:dateformat) AS salesDate")
                , DB::expr("DATE_FORMAT(sa.salesDate,:time_format) AS salesTime")
                , array('sa.totaltax', 'tax')
                , array('sa.MethodPayment', 'payment')
                , array('sa.status', 'status')
                , array('lot.lotName', 'productDetail')
                , array('pro.productName', 'description')
                , array('sad.quantity', 'quantity')
                , array('sad.totaltax', 'totalTax')
                , array('sad.discount', 'discount')
                , array('sad.unitPrice', 'priceUnit')
                , array('sad.totalPrice', 'amountTotal')
            );
            $sql_productLot = DB::select_array($cols_productLot)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sad'))->on('sa.idSales', '=', 'sad.idSales')
                    ->join(array('rms_lot', 'lot'))->on('sad.idLot', '=', 'lot.idLot')
                    ->join(array('rms_products', 'pro'))->on('lot.idProduct', '=', 'pro.idProduct')
                    ->join(array('bts_person', 'per'))->on('sa.idUser', '=', 'per.idPerson')
                    ->where('pro.status', '=', '1');

            if ($d != "") {
                $sql_productLot->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':d');
            }
            if ($m != 0) {
                $sql_productLot->and_where(DB::expr("MONTH(sa.salesDate)"), '=', ':m');
                $sql_productLot->and_where(DB::expr("YEAR(sa.salesDate)"), '=', ':a');
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_productLot->and_where('sa.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd, :dateformat) AND STR_TO_DATE(:ed :time_limit, :dateformat :time_raw)"));
                } else {
                    $sql_productLot->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':ed');
                }
            } else {
                if ($sd != "") {
                    $sql_productLot->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':sd');
                }
            }
            if ($product != NULL AND is_array($product)) {
                $sql_productLot->and_where('pro.idProduct', 'IN', DB::expr("(" . implode(',', $product) . ")"));
            }
            $sql_productLot->parameters($parameter);

            //consulta de productos con detalle
            $cols_productDetail = $cols_productLot;
            
            $cols_productDetail[9] = array('prod.serie_electronic', 'productDetail');

            $sql_productDetail = DB::select_array($cols_productDetail)
                    ->union($sql_productLot, TRUE)
                    ->from(array('rms_sales', 'sa'))
                    ->join(array('rms_salesdetail', 'sad'))->on('sa.idSales', '=', 'sad.idSales')
                    ->join(array('rms_product_detail', 'prod'))->on('sad.idProductDetail', '=', 'prod.idProductDetail')
                    ->join(array('rms_products', 'pro'))->on('prod.idProduct', '=', 'pro.idProduct')
                    ->join(array('bts_person', 'per'))->on('sa.idUser', '=', 'per.idPerson')
                    ->where('pro.status', '=', '1');

            if ($d != "") {
                $sql_productDetail->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':d');
            }
            if ($m != 0) {
                $sql_productDetail->and_where(DB::expr("MONTH(sa.salesDate)"), '=', ':m');
                $sql_productDetail->and_where(DB::expr("YEAR(sa.salesDate)"), '=', ':a');
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_productDetail->and_where('sa.salesDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd, :dateformat) AND STR_TO_DATE(:ed :time_limit, :dateformat :time_raw)"));
                } else {
                    $sql_productDetail->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':ed');
                }
            } else {
                if ($sd != "") {
                    $sql_productDetail->and_where(DB::expr("DATE_FORMAT(sa.salesDate, :dateformat)"), 'LIKE', ':sd');
                }
            }
            if ($product != NULL AND is_array($product)) {
                $sql_productDetail->and_where('pro.idProduct', 'IN', DB::expr("(" . implode(',', $product) . ")"));
            }
            $sql_productDetail->parameters($parameter);
            
            $sql_data = $sql_productDetail->execute()->as_array();
            // echo Database::instance()->last_query;
            // die();
            $this->fnResponseFormat($sql_data, self::REPORT_RESPONSE_TYPE_XML, $salesDetail);
            
        } catch (Exception $exc) {
            echo $this->handlingError($exc);
        }
    }

}
