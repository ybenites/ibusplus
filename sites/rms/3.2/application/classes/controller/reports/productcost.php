<?php

/**
 * Description of customers
 *
 * @author G@
 */
class Controller_Reports_Productcost extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('productcost', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('productcost', self::FILE_TYPE_JS));
        $view_prod_cost = new View('reports/productcost');
        $view_prod_cost->a_year_report = $this->a_year_report();
        $view_prod_cost->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_prod_cost->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $obj_store = ORM::factory('stores')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $view_prod_cost->a_person_document_type = $this->a_PERSON_DOCUMENT_TYPE();
        $view_prod_cost->array_store = $obj_store;
        $view_prod_cost->a_ALL_TYPE_MOVE = $this->a_ALL_TYPE_MOVE();
        $this->template->content = $view_prod_cost;
    }

    public function action_reportProductCost() {
        $this->auto_render = false;
        try {
            $stockProduct = array();
            $stockProduct ['rDate'] = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE, $this->request->query(self::RQ_DATE_FORMAT)));
            $stockProduct ['qlang'] = $this->request->query(self::RQ_LANG);
            $stockProduct ['searchDate'] = DateHelper::getFechaFormateadaActual();
            $store = $this->request->query('store');
            $move = $this->request->query('move');

            $storeName = array('s.storeShortName', 'storeShortName');
            $dataStore = DB::select_array($storeName)
                            ->from(array('rms_store', 's'))
                            ->where('s.idStore', '=', $store)
                            ->as_object()->execute()->current();
            $stockProduct['store'] = $dataStore->storeShortName;

            $selectProducts = array(
                array('sk.idStock', 'idStock'),
                array('po.idProduct', 'idProduct'),
                array('po.aliasProduct', 'aliasProduct'),
                array('po.productName', 'productName'),
                array('br.brandNameCommercial', 'brandNameCommercial'),
                array('ca.categoryName', 'categoryName'),
                array('st.storeName', 'storeName'),
                array('mod.unitPrice', 'unitPrice'),
                array('sk.amount', 'quantity')
            );
            $stockProducts = DB::select_array($selectProducts)
                    ->from(array('rms_move', 'mo'))
                    ->join(array('rms_movedetail', 'mod'))->on('mo.idMove', '=', 'mod.idMove')
                    ->join(array('rms_store', 'st'))->on('mo.idStore', '=', 'st.idStore')
                    ->join(array('rms_product_detail', 'pod'), 'LEFT')->on('mod.idProductDetail', '=', 'pod.idProductDetail')
                    ->join(array('rms_lot', 'lo'), 'LEFT')->on('mod.idLot', '=', 'lo.idLot')
                    ->join(array('rms_products', 'po'), 'LEFT')->on('po.idProduct', '=', DB::expr('lo.idProduct OR po.idProduct = pod.idProduct'))
                    ->join(array('rms_brands', 'br'))->on('po.idBrand', '=', 'br.idBrand')
                    ->join(array('rms_category', 'ca'))->on('po.idCategory', '=', 'ca.idCategory')
                    ->join(array('rms_stock', 'sk'), 'LEFT')->on('sk.idProduct', '=', DB::expr('po.idProduct AND sk.idStore = st.idStore'))
                    ->where('mo.status', '=', 1)
                    ->and_where('mod.status', '=', 1)
                    ->and_where('sk.amount', '>', 0)
                    ->and_where('st.idStore', '=', $store)
                    ->and_where('mo.typeMoveStore', '=', $move);

            $sql_stockProducts = $stockProducts->group_by('po.idProduct')
                            ->group_by('st.idStore')->execute()->as_array();

            $this->fnResponseFormat($sql_stockProducts, self::REPORT_RESPONSE_TYPE_XML, $stockProduct);

//            echo Database::instance()->last_query;
//            die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}