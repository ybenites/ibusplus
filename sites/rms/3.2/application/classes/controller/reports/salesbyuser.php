<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of salesbyuser
 *
 * @author harby
 */
class Controller_Reports_Salesbyuser extends Controller_Private_Admin implements Rms_Constants {
    //put your code here
    public function action_index(){
        $this->mergeStyles(ConfigFiles::fnGetFiles('salesbyuser',self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('salesbyuser',self::FILE_TYPE_JS));
        $view_reportSalesbyuser = new View('reports/salesbyuser');
        $view_reportSalesbyuser->a_year_report =  $this->a_year_report();
        $view_reportSalesbyuser->a_month_report =  $this->fnGetClassVariable(self::MONTH_NAME);
        $view_reportSalesbyuser->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $view_reportSalesbyuser;
    }
    
    public function action_reportSalesAllUser() {
        try {
            $d = $this->request->query('d');      
            $reportSales = array();
            $reportSales['qlang'] = $this->request->query(self::RQ_LANG);
            $reportSales['searchDate'] = DateHelper::getFechaFormateadaActual();
          
            if ($d != "") {
                $reportSales['date'] = $d;
            }

            $data_reportSales = array();
            $data_reportSales['sales'] = $this->getSalesCash($d)->execute()->as_array();
            $data_reportSales['credit'] = $this->getSalesCredit($d)->execute()->as_array();
            $data_reportSales['voucher'] = $this->getVoucher($d)->execute()->as_array();
            $data_reportSales['reserved'] = $this->getReserved($d)->execute()->as_array();
            $data_reportSales['salesreserved'] = $this->getSalesReserved($d)->execute()->as_array();
            $data_reportSales['creditnote'] = $this->getCreditNote($d)->execute()->as_array();
            $this->fnResponseFormat($data_reportSales, self::REPORT_RESPONSE_TYPE_XML, $reportSales);
           //  print_r(Database::instance()->last_query);
            // die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function getSalesCash($d) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));

            $a_dataSalesDetail = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                array('s.status', 'ESTADO'),
                DB::expr("COALESCE(s.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(s.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,' - ',po.productName) AS DESCRIPCION"),
                DB::expr("GROUP_CONCAT(DISTINCT pd.serie_electronic) AS DETAIL"),
                DB::expr("COUNT(sd.quantity) AS CANTIDAD"),
                DB::expr("SUM(sd.discount) AS DESCUENTO"),
                array('sd.taxpercentage', 'IMPUESTO'),
                DB::expr("SUM(sd.totaltax) AS IMPUESTO"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('s.methodPayment', 'TIPO_PAGO'),
                array('s.paymentCash', 'PAGO_EFECTIVO'),
                array('s.paymentCard', 'PAGO_CREDITO')
            );
            $selectSalesDetail = DB::select_array($a_dataSalesDetail)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'po'))->on('pd.idProduct', '=', 'po.idProduct')
                    ->where('po.status', '=', '1');

            $selectSalesDetail = $selectSalesDetail->group_by('sd.idSalesDetail');

            $a_dataSalesLot = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                array('s.status', 'ESTADO'),
                DB::expr("COALESCE(s.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(s.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,'-',po.productName) AS DESCRIPCION"),
                array('lot.lotName', 'DETAIL'),
                DB::expr("SUM(sd.quantity)  AS CANTIDAD"),
                DB::expr("SUM(sd.discount) AS DESCUENTO"),
                array('sd.taxpercentage', 'IMPUESTO'),
                DB::expr("SUM(sd.totaltax) AS IMPUESTO_VENTA"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('s.methodPayment', 'TIPO_PAGO'),
                array('s.paymentCash', 'PAGO_EFECTIVO'),
                array('s.paymentCard', 'PAGO_CREDITO')
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->union($selectSalesDetail, TRUE)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'po'))->on('po.idProduct', '=', 'lot.idProduct')
                    ->where('po.status', '=', '1');

            if ($d != "") {
                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
            }
            $selectSalesLot = $selectSalesLot->group_by('sd.idSalesDetail');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }

    public function getSalesCredit($d) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));

            $a_dataSalesDetail = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                array('s.status', 'ESTADO_VENTA'),
                DB::expr("COALESCE(s.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(s.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,' - ',po.productName) AS DESCRIPCION"),
                array('pc.idTypeCard', 'TIPO_TARJETA'),
                array('pc.status', 'ESTADO_PAGO'),
                array('sd.unitPrice', 'MONTO'),
                array('pc.operationNumber', 'NRO_OPERACION'),
                array('pc.cardNumber', 'NRO_TARJETA'),
                DB::expr("COUNT(sd.quantity) AS CANTIDAD"),
                DB::expr("SUM(sd.discount) AS DESCUENTO"),
                DB::expr("SUM(sd.totaltax) AS IMPUESTO"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('s.methodPayment', 'TIPO_PAGO')
            );
            $selectSalesDetail = DB::select_array($a_dataSalesDetail)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sd.idProductDetail')
                    ->join(array('rms_products', 'po'))->on('pd.idProduct', '=', 'po.idProduct')
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 's.idPaymentCard')
                    ->where('po.status', '=', '1')                  
                    ->and_where('s.methodPayment', '=', self::SALES_PAYMENT_CREDITCARD);
              

            $selectSalesDetail = $selectSalesDetail->group_by('sd.idSalesDetail');

            $a_dataSalesLot = array(
                array('s.documentType', 'TIPO_DOCUMENTO'),
                array('s.status', 'ESTADO'),
                DB::expr("COALESCE(s.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(s.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,'-',po.productName) AS DESCRIPCION"),
                array('pc.idTypeCard', 'TIPO_TARJETA'),
                array('pc.status', 'ESTADO_PAGO'),
                array('sd.unitPrice', 'MONTO'),
                array('pc.operationNumber', 'NRO_OPERACION'),
                array('pc.cardNumber', 'NRO_TARJETA'),
                DB::expr("SUM(sd.quantity)  AS CANTIDAD"),
                DB::expr("SUM(sd.discount) AS DESCUENTO"),
                DB::expr("SUM(sd.totaltax) AS IMPUESTO_VENTA"),
                array('sd.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('s.methodPayment', 'TIPO_PAGO')
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->union($selectSalesDetail, TRUE)
                    ->from(array('rms_sales', 's'))
                    ->join(array('rms_salesdetail', 'sd'))->on('s.idSales', '=', 'sd.idSales')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'po'))->on('po.idProduct', '=', 'lot.idProduct')
                    ->join(array('rms_paymentcard', 'pc'))->on('pc.idPaymentCard', '=', 's.idPaymentCard')
                    ->where('po.status', '=', '1')                    
                    ->and_where('s.methodPayment', '=', self::SALES_PAYMENT_CREDITCARD);                
              
            if ($d != "") {
                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(s.salesDate,'$date_format')"), 'LIKE', $d);
            }

            $selectSalesLot = $selectSalesLot->group_by('sd.idSalesDetail');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }

    public function getVoucher($d) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $dataExpensesSales = array(
                array('v.documentType', 'TIPO_DOCUMENTO'),
                DB::expr("COALESCE(v.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(v.correlative),8,'0') AS CORRELATIVO"),
                array('pe.fullName', 'AUTHORIZES'),
                array('v.concept', 'DESCRIPCION'),
                array('v.amount', 'MONTO')
            );
            $selectExpensesSales = DB::select_array($dataExpensesSales)
                    ->from(array('rms_voucher', 'v'))
                    ->join(array('bts_person', 'pe'))->on('pe.idPerson', '=', 'v.idAuthorizes')
                    ->where('v.status', '=', '1');
            
            if ($d != "") {
                $selectExpensesSales->and_where(DB::expr("DATE_FORMAT(v.voucherDate,'$date_format')"), 'LIKE', $d);
            }
            $selectExpensesSales = $selectExpensesSales->group_by('v.idVoucher')
                    ->order_by('v.idVoucher');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectExpensesSales;
    }

    public function getReserved($d) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            
            $a_dataSalesDetail = array(
                array('so.documentType', 'TIPO_DOCUMENTO'),
                array('so.status', 'ESTADO'),
                DB::expr("LPAD(COALESCE(so.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("GROUP_CONCAT(po.productName) AS DESCRIPCION"),
                DB::expr("GROUP_CONCAT(DISTINCT pd.serie_electronic) AS DETAIL"),
                DB::expr("COUNT(sod.quantity) AS CANTIDAD"),
                DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format $time_raw') AS ORDER_DATE"),
                array('sod.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('sod.totalPrice', 'PRECIO_VENTA'),
                array('so.totalAmount', 'PRECIO_VENTA_TOTAL'),
                array('so.amountToPaid', 'SALDO_A_CUENTA'),
                array('so.amountPaid', 'MONTO_PAGADO'),
                array('so.methodPayment', 'TIPO_PAGO')
            );
            $selectSalesDetail = DB::select_array($a_dataSalesDetail)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('so.idSalesOrder', '=', 'sod.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sod.idProductDetail')
                    ->join(array('rms_products', 'po'))->on('pd.idProduct', '=', 'po.idProduct')
                    ->where('po.status', '=', '1')                  
                    ->and_where('so.status', '=', self::SALES_ORDER_STATUS_RESERVED);

            $selectSalesDetail = $selectSalesDetail->group_by('so.idSalesOrder');

            $a_dataSalesLot = array(
                array('so.documentType', 'TIPO_DOCUMENTO'),
                array('so.status', 'ESTADO'),
                DB::expr("LPAD(COALESCE(so.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("GROUP_CONCAT(po.productName) AS DESCRIPCION"),
                array('lot.lotName', 'DETAIL'),
                DB::expr("SUM(sod.quantity)  AS CANTIDAD"),
                DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format $time_raw') AS ORDER_DATE"),
                array('sod.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('sod.totalPrice', 'PRECIO_VENTA'),
                array('so.totalAmount', 'PRECIO_VENTA_TOTAL'),
                array('so.amountToPaid', 'SALDO_A_CUENTA'),
                array('so.amountPaid', 'MONTO_PAGADO'),
                array('so.methodPayment', 'TIPO_PAGO')
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->union($selectSalesDetail, TRUE)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('so.idSalesOrder', '=', 'sod.idSalesOrder')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sod.idLot')
                    ->join(array('rms_products', 'po'))->on('po.idProduct', '=', 'lot.idProduct')
                    ->where('po.status', '=', '1')                                    
                    ->and_where('so.status', '=', self::SALES_ORDER_STATUS_RESERVED);

            if ($d != "") {
                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format')"), 'LIKE', $d);
            }
            $selectSalesLot = $selectSalesLot->group_by('so.idSalesOrder');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }

    public function getSalesReserved($d) {
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));

            $a_dataSalesDetail = array(
                array('so.documentType', 'TIPO_DOCUMENTO'),
                array('so.status', 'ESTADO'),
                DB::expr("LPAD(COALESCE(so.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,' - ',po.productName) AS DESCRIPCION"),
                DB::expr("GROUP_CONCAT(DISTINCT pd.serie_electronic) AS DETAIL"),
                DB::expr("COUNT(sod.quantity) AS CANTIDAD"),
                array('sod.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('sod.totalPrice', 'PRECIO_VENTA'),
                array('so.totalAmount', 'PRECIO_VENTA_TOTAL'),
                array('so.amountToPaid', 'SALDO_A_CUENTA'),
                array('so.amountPaid', 'MONTO_PAGADO'),
                array('so.methodPayment', 'TIPO_PAGO')
            );
            $selectSalesDetail = DB::select_array($a_dataSalesDetail)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('so.idSalesOrder', '=', 'sod.idSalesOrder')
                    ->join(array('rms_product_detail', 'pd'))->on('pd.idProductDetail', '=', 'sod.idProductDetail')
                    ->join(array('rms_products', 'po'))->on('pd.idProduct', '=', 'po.idProduct')
                    ->where('po.status', '=', '1')          
                    ->and_where('so.status', '=', self::SALES_ORDER_STATUS_SOLD);
         
            $selectSalesDetail = $selectSalesDetail->group_by('sod.idSalesOrderDetail');

            $a_dataSalesLot = array(
                array('so.documentType', 'TIPO_DOCUMENTO'),
                array('so.status', 'ESTADO'),
                DB::expr("LPAD(COALESCE(so.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,'-',po.productName) AS DESCRIPCION"),
                array('lot.lotName', 'DETAIL'),
                DB::expr("SUM(sod.quantity)  AS CANTIDAD"),
                array('sod.unitPrice', 'PRECIO_VENTA_UNITARIO'),
                array('sod.totalPrice', 'PRECIO_VENTA'),
                array('so.totalAmount', 'PRECIO_VENTA_TOTAL'),
                array('so.amountToPaid', 'SALDO_A_CUENTA'),
                array('so.amountPaid', 'MONTO_PAGADO'),
                array('so.methodPayment', 'TIPO_PAGO'),
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->union($selectSalesDetail, TRUE)
                    ->from(array('rms_salesorder', 'so'))
                    ->join(array('rms_salesorderdetail', 'sod'))->on('so.idSalesOrder', '=', 'sod.idSalesOrder')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sod.idLot')
                    ->join(array('rms_products', 'po'))->on('po.idProduct', '=', 'lot.idProduct')
                    ->where('po.status', '=', '1')
                    ->and_where('so.status', '=', self::SALES_ORDER_STATUS_SOLD);
               

            if ($d != "") {
                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format')"), 'LIKE', $d);
            }
            $selectSalesLot = $selectSalesLot->group_by('sod.idSalesOrderDetail');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }

    public function getCreditNote($d) {
        try {

            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));

            $a_dataSalesLot = array(
                array('sa.documentType', 'TIPO_DOCUMENTO'),
                DB::expr("COALESCE(sa.serie) AS SERIE"),
                DB::expr("LPAD(COALESCE(sa.correlative),8,'0') AS CORRELATIVE"),
                DB::expr("CONCAT(po.aliasProduct,' - ',po.productName) AS DESCRIPCION"),
                array('lot.lotName', 'DETAIL'),
                array('sa.salesDate', 'FECHA_VENTA'),
                array('cd.amountSale', 'MONTO_VENTA'),
                array('cd.description', 'CONCEPT')
            );
            $selectSalesLot = DB::select_array($a_dataSalesLot)
                    ->from(array('rms_credit_note', 'cd'))
                    ->join(array('rms_sales', 'sa'))->on('sa.idSales', '=', 'cd.idSales')
                    ->join(array('rms_salesdetail', 'sd'))->on('sd.idSales', '=', 'sa.idSales')
                    ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'sd.idLot')
                    ->join(array('rms_products', 'po'))->on('lot.idProduct', '=', 'po.idProduct')
                    ->where('po.status', '=', '1')                    
                    ->and_where('cd.status', '=', self::CREDITNOTE_USED);

            if ($d != "") {
//                $selectSalesDetail->and_where(DB::expr("DATE_FORMAT(so.orderSalesDate,'$date_format')"), 'LIKE', $d);
                $selectSalesLot->and_where(DB::expr("DATE_FORMAT(cd.creationDate,'$date_format')"), 'LIKE', $d);
            }
            $selectSalesLot = $selectSalesLot->group_by('sd.idSalesDetail');
        } catch (Exception $e_exc) {
            echo $this->handlingError($e_exc);
        }
        return $selectSalesLot;
    }
}

?>
