<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Public_Login extends Public_Login {

    public function action_index() {
        $login_view = new View('/public/login');
        $login_view->skin = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
        $login_view->website = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['website'];
        $login_view->siteName = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['siteName'];
        $this->template->content = $login_view;
        if ($this->getSessionParameter(self::USER_SESSION_ID) == NULL) {
            $this->destroySession();
        } else {
            Request::initial()->redirect($this->getSessionParameter(self::USER_DEFAULT_HOME));
        }
    }

}