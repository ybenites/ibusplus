<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Public_Welcome extends Public_Welcome {

   public $template = 'public/index_html';

    public function action_index() {
        $skin = $this->template->skin;
        $welcome_view_file = DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . 'index_html';

        $index_view = new View($welcome_view_file);
        $index_view->skin = $skin;
        $this->template->body = $index_view;
    }

}