<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stocktransfer
 *
 * @author Yime
 */
class Controller_Private_Stocktransfer extends Controller_Private_Admin implements Rms_Constants {

    //put your code here
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('stocktransfer', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('stocktransfer', self::FILE_TYPE_JS));
        $inventory_stocktransfer = new View('inventory/stocktransfer');

//        $user_incharge=$this->getSessionParameter(self::USER_ID);
        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', self::STATUS_ACTIVE)
//                ->and_where('idUser', '=',':INCHARGE')
//                ->param(':STATUS',self::STATUS_ACTIVE)
//                ->param(':INCHARGE',$user_incharge)
                        ->execute()->as_array();
        $inventory_stocktransfer->today = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE));
        $inventory_stocktransfer->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $inventory_stocktransfer->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;

        $inventory_stocktransfer->store = $store;
        $inventory_stocktransfer->type_document = self::DT_GUIA_REMISION;
        $inventory_stocktransfer->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $inventory_stocktransfer->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $this->template->content = $inventory_stocktransfer;
    }

    public function action_getAllStore() {
        $a_response = $this->json_array_return;
        try {
            $check_data = Validation::factory($this->request->post());
            if ($check_data->check()) {
                $check_data_post = $check_data->data();
                if ($check_data_post['idStore'] != NULL) {
                    $store = DB::select(
                                            'idStore', 'storeShortName'
                                    )->from('rms_store')
                                    ->where('idStore', '<>', $check_data_post['idStore'])
                                    ->and_where('status', '=', self::STATUS_ACTIVE)
                                    //                        ->param(':ID_STORE_ACTUAL',$check_data_post['idStore'])
                                    //                        ->param(':ACTIVE',self::STATUS_ACTIVE)
                                    ->execute()->as_array();
                } else {
                    $store = DB::select(
                                            'idStore', 'storeShortName'
                                    )->from('rms_store')
                                    ->where('status', '=', self::STATUS_ACTIVE)
                                    ->execute()->as_array();
                }


                $a_response['data'] = $store;
            }
//            else{
//                throw new Exception('Asignese un Almacen por favor', self::CODE_SUCCESS);
//            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createTransferStock() {
        $a_response = $this->json_array_return;
        try {
            $array_grid = $this->request->post('array_grid');
            $obj_frm = $this->request->post('obj_frm');
            $array_grid_detail = $this->request->post('array_grid_detail');
            $array_grid_detail_l = $this->request->post('array_grid_detail_l');

            $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
            $dateTransfer = DateTime::createFromFormat($format, $obj_frm['input_date_transfer']);

            // Correr correlativo 
            if ($this->getOptionValue(self::CORRELATIVO_IMPRESION) == true) {
                $oSerieUpdate = $this->getNextCorrelative(self::DT_GUIA_REMISION);
            } else {
                $oSerieUpdate = $this->getNextCorrelative(self::DT_DEFAULT_CORRELATIVE);
            }

            Database::instance()->begin();
            $obj_move_TI = new Model_Move();
            // Movimiento de Entrada - Correlativo 
//            echo $oSerieUpdate->serie.' - '$oSerieUpdate->co; 
            $obj_move_TI->serie = $oSerieUpdate->serie;
            $obj_move_TI->correlative = $oSerieUpdate->correlative;
            $obj_move_TI->typeMoveStore = self::TYPE_MOVE_TRAS_INGRESO;
            $obj_move_TI->moveReference = $obj_frm['text_doc_ref'];
            $obj_move_TI->idStore = $obj_frm['store_send'];
            $obj_move_TI->dateIncome = DateHelper::fnPHPDate2MySQLDatetime($dateTransfer);
            $obj_move_TI->status = self::STATUS_ACTIVE;


            $obj_move_TS = new Model_Move();
            $obj_move_TS->typeMoveStore = self::TYPE_MOVE_TRAS_SALIDA;
            // Movimiento de Salida - Correlativo 
            $obj_move_TS->serie = $oSerieUpdate->serie;
            $obj_move_TS->correlative = $oSerieUpdate->correlative;
            $obj_move_TS->moveReference = $obj_frm['text_doc_ref'];
            $obj_move_TS->idStore = $obj_frm['idStore'];
            $obj_move_TS->dateOutcome = DateHelper::fnPHPDate2MySQLDatetime($dateTransfer);
            $obj_move_TS->status = self::STATUS_ACTIVE;
            $obj_move_TI->save();
            $obj_move_TS->save();

            $idMov_TI = $obj_move_TI->idMove;
            $idStore_TI = $obj_move_TI->idStore;

            $idMov_TS = $obj_move_TS->idMove;
            $idStore_TS = $obj_move_TS->idStore;

//              print_r($array_grid);die();
            foreach ($array_grid as $v) {
                //Con el codigo de aqui actualizamos las tablas del stock
                $obj_stock_TI = new Model_Stock();
                $obj_stock_TI->where('idProduct', '=', $v['id'])
                        ->and_where('idStore', '=', $idStore_TI)
                        ->find();
                $obj_stock_TS = new Model_Stock();
                $obj_stock_TS->where('idProduct', '=', $v['id'])
                        ->and_where('idStore', '=', $idStore_TS)
                        ->find();
                if ($obj_stock_TI->loaded()) {
                    $obj_stock_TI->amount = $obj_stock_TI->amount + $v['amount'];
                } else {
                    $obj_stock_TI->idProduct = $v['id'];
                    $obj_stock_TI->idStore = $idStore_TI;
                    $obj_stock_TI->amount = $v['amount'];
                }

                if ($obj_stock_TS->loaded()) {
                    $obj_stock_TS->amount = $obj_stock_TS->amount - $v['amount'];
                }
                $obj_stock_TI->save();
                $obj_stock_TS->save();
                //Finalizamos el codigo de actualizacion de tablas de stock                
                if ($v['haveDetail'] == self::STATUS_ACTIVE) {
                    // PRODUCTO DETALLE
                    foreach ($array_grid_detail as $pdet) {
                        $obj_movedetail_TI = new Model_Movedetail();
                        $obj_movedetail_TS = new Model_Movedetail();
                        // Movimiento detalles de las entradas
                        $obj_movedetail_TI->idMove = $idMov_TI;
                        $obj_movedetail_TI->amount = 1;
                        $obj_movedetail_TI->status = self::STATUS_ACTIVE;
                        $obj_movedetail_TI->dateIncomeDetail = DateHelper::fnPHPDate2MySQLDatetime($dateTransfer);
                        //Movimiento detalles de las salidas
                        $obj_movedetail_TS->idMove = $idMov_TS;
                        $obj_movedetail_TS->amount = 1;
                        $obj_movedetail_TS->status = self::STATUS_ACTIVE;
                        $obj_movedetail_TS->dateOutcomeDetail = DateHelper::fnPHPDate2MySQLDatetime($dateTransfer);

                        $obj_product_d_a = new Model_Productdetail($pdet);

                        if ($obj_product_d_a->idProduct == $v['id']) {
                            $obj_movedetail_TI->unitPrice = $obj_product_d_a->unitPrice;
                            $obj_movedetail_TI->idProductDetail = $obj_product_d_a->idProductDetail;

                            $obj_movedetail_TS->unitPrice = $obj_product_d_a->unitPrice;
                            $obj_movedetail_TS->idProductDetail = $obj_product_d_a->idProductDetail;

                            $obj_product_d_a->save();
                            $obj_movedetail_TI->save();
                            $obj_movedetail_TS->save();
                        }
                    }
                } else {
                    // LOTES 
                    foreach ($array_grid_detail_l as $pd) {
                        $obj_movedetail_TI = new Model_Movedetail();
                        $obj_movedetail_TS = new Model_Movedetail();

                        $obj_movedetail_TI->idMove = $idMov_TI;
                        $obj_movedetail_TI->amount = $pd['amount'];
                        $obj_movedetail_TI->status = self::STATUS_ACTIVE;

                        $obj_movedetail_TS->idMove = $idMov_TS;
                        $obj_movedetail_TS->amount = $pd['amount'];
                        $obj_movedetail_TS->status = self::STATUS_ACTIVE;

                        $obj_product_l_s = new Model_Lot($pd['idLot']);

                        $obj_product_l_i = ORM::factory('Lot')
                                ->where('idStore', '=', ':ID_STORE')
                                ->and_where('idProduct', '=', ':ID_PRODUCT')
                                ->and_where('lotName', '=', ':LOT_NAME')
                                ->param(':LOT_NAME', $obj_product_l_s->lotName)
                                ->param(':ID_PRODUCT', $obj_product_l_s->idProduct)
                                ->param(':ID_STORE', $idStore_TI)
                                ->find();


                        if ($obj_product_l_s->idProduct == $v['id'] && trim($obj_product_l_s->lotName) == $pd['lotName']) {

                            $obj_product_l_s->amount = $obj_product_l_s->amount - $pd['amount'];

                            $obj_movedetail_TI->unitPrice = $obj_product_l_s->unitPrice;
                            $obj_movedetail_TS->unitPrice = $obj_product_l_s->unitPrice;
                            $obj_movedetail_TS->idLot = $obj_product_l_s->idLot;
                            $obj_movedetail_TS->save();
                            if ($obj_product_l_i->loaded()) {
                                $obj_product_l_i->amount = $obj_product_l_i->amount + $pd['amount'];
                            } else {
                                $obj_product_l_i = new Model_Lot();
                                $obj_product_l_i->idProduct = $obj_product_l_s->idProduct;
                                $obj_product_l_i->lotName = $obj_product_l_s->lotName;
                                $obj_product_l_i->description = $obj_product_l_s->description;
                                $obj_product_l_i->unitPrice = $obj_product_l_s->unitPrice;
                                $obj_product_l_i->idStore = $idStore_TI;
                                $obj_product_l_i->amount = $pd['amount'];
                             $obj_product_l_i->save();
                             
                             }
                           
                            $obj_product_l_s->save();

                            $obj_movedetail_TI->idLot = $obj_product_l_i->idLot;
                            $obj_movedetail_TI->save();
                        }
                    }
                }
            }
            $id_mov = array('idmov_ts' => $obj_move_TS->idMove, 'idmov_ti' => $obj_move_TI->idMove);
            Database::instance()->commit();
            // Movimiento de transferencia

            $a_response['idMove'] = $id_mov;
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProductById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_data_product = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('idStore', 'not_empty');

            if ($o_post_data_product->check()) {
                $data_post_product = $o_post_data_product->data();

                $obj_stock = new Model_Stock();
                $obj_stock->where('idProduct', '=', $data_post_product['id'])
                        ->and_where('idStore', '=', $data_post_product['idStore'])->find();

                $obj_product = new Model_Products($data_post_product['id']);

                if ($obj_stock->loaded()) {
                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = $obj_stock->amount;
                } else {

                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = 0;
                }
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProductLotById() {
        $a_response = $this->json_array_return;
        try {
            $id_product = $this->request->post('idpg');
            $id_store = $this->request->post('idstore');
            //con esto traemos el id del almacen
            //       $user_incharge=$this->getSessionParameter(self::USER_ID);
//            $obj_store=  ORM::factory('stores')
//                    ->where('status', '=', ':STATUS')
//                    ->and_where('idUser', '=', ':INCHARGE')
//                    ->param(':STATUS',self::STATUS_ACTIVE)
////                    ->param(':INCHARGE',$user_incharge)
//                    ->find();     


            if ($id_store != '' && $id_product != '') {
                $query_pd = "SELECT   rpd.idLot AS idLot,  rp.idProduct AS idProduct,
                    rp.productName AS productName,  rpd.lotName AS lotName,  mvdi.unitPrice AS unitPrice,
                    rpd.amount AS amount,  rpd.description AS description,
                    DATE_FORMAT(    rpd.creationDate,    '%d/%m/%Y %h:%i %p'  ) AS creationDate 
                    FROM  rms_products rp 
                    INNER JOIN rms_lot rpd     ON rp.idProduct = rpd.idProduct 
                    INNER JOIN rms_movedetail mvdi     ON mvdi.idLot = rpd.idLot 
                    INNER JOIN rms_move mv     ON mvdi.idMove = mv.idMove 
                    WHERE rp.idProduct =$id_product   AND mv.idStore =$id_store  AND rpd.amount > 0   
                        AND mv.typeMoveStore <> 'VENTA_DIRECTA'   AND mv.typeMoveStore <> 'TRAS_SALIDA'  
                        AND mv.typeMoveStore <> 'SALIDA' 
                    GROUP BY rpd.idLot ORDER BY idProduct DESC ";
                $rs = DB::query(Database::SELECT, $query_pd)->execute()->as_array();
                //echo Database::instance()->last_query;die();
                $a_response['data'] = $rs;
            } else {
                throw new Exception('ERROR-' . __('Seleccione un Almacén'), self::CODE_ERROR);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getPrintGuiaRemision() {
        //    ?mov_ts=1966&mov_ti=1965
        $this->auto_render = FALSE;
        try {
            $o_post_mov = Validation::factory($_GET)
                    ->rule('mov_ts', 'not_empty')
                    ->rule('mov_ti', 'not_empty');

            if (!$o_post_mov->check()) {
                throw new Exception('ERROR -' . __('Empty Data'), self::CODE_ERROR);
            } else {
                $i_mov_ts = $this->request->query('mov_ts');
                $i_mov_ti = $this->request->query('mov_ti');

                $this->printDocumentTransfer($i_mov_ts, $i_mov_ti);
            }
        } catch (Exception $exc) {
            echo __('Impresión Inválida');
        }
    }

}