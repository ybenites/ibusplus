<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stockoutcome
 *
 * @author Yime
 */
class Controller_Private_Stockoutcome extends Controller_Private_Admin implements Rms_Constants {

    //put your code here
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('stockoutcome', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('stockoutcome', self::FILE_TYPE_JS));
        $inventory_stockoutcome = new View('inventory/stockoutcome');
        $typeMove = DB::select(
                                array('idTypeMove', 'id'), array('typeMoveName', 'display')
                        )->from('rms_typemove')
                        ->where('typeMoveState', '=', ':MOVE')
                        ->and_where('status', '=', ':STATUS')
                        ->param(':MOVE', self::TYPE_MOVE_SALIDA)
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->execute()->as_array();

        $currency = $this->a_TYPE_CURRENCY();
        $user_incharge = $this->getSessionParameter(self::USER_ID);
        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->as_array();

        $inventory_stockoutcome->today = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE));
        $inventory_stockoutcome->typeMove = $typeMove;
        $inventory_stockoutcome->currency = $currency;
        $inventory_stockoutcome->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $inventory_stockoutcome->type_document = self::DT_NOTA_SALIDA;
        $inventory_stockoutcome->store = $store;
        $inventory_stockoutcome->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $inventory_stockoutcome;
    }

    public function action_getAllSalesOrder() {
        $a_response = $this->json_array_return;
        try {
            $sales_orders = DB::select(
                                    array('idSalesOrder', 'id'), array('correlative', 'display'))
                            ->from('rms_salesorder')
                            ->where('documentType', '=', ':DOCUMENT')
                            ->param(':DOCUMENT', self::DT_NOTA_PEDIDO)
                            ->execute()->as_array();
            $a_response['data'] = $sales_orders;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getDataSearchAutocompleteProduct() {
        $a_response = $this->json_array_return;
        try {
            $o_post_data_product = Validation::factory($this->request->post())
                    ->rule('term', 'not_empty')
                    ->rule('typeSearch', 'not_empty');
            if ($o_post_data_product->check()) {
                $data_post_product = $o_post_data_product->data();
                $sql = DB::select(
                                array('productName', 'dataMostrar')
                                , array('aliasProduct', 'otraData')
                                , array('idProduct', 'id')
                        )
                        ->from('rms_products');
                if ($data_post_product['typeSearch'] == 'search_description') {
                    $sql->where('productName', 'like', ':TERM');
                } elseif ($data_post_product['typeSearch'] == 'search_code') {
                    $sql->where('aliasProduct', 'like', ':TERM');
                }
                $sql->where('status', '=', self::STATUS_ACTIVE);
                $sql->param(':TERM', '%' . $data_post_product['term'] . '%');
                $a_response['data'] = $sql->execute()->as_array();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProductById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_data_product = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('idStore', 'not_empty');

            if ($o_post_data_product->check()) {
                $data_post_product = $o_post_data_product->data();
                $obj_stock = new Model_Stock();
                $obj_stock->where('idProduct', '=', $data_post_product['id'])
                        ->and_where('idStore', '=', $data_post_product['idStore'])->find();
                $obj_product = new Model_Products($data_post_product['id']);
                if ($obj_stock->loaded()) {
                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = $obj_stock->amount;
                } else {
                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = 0;
                }
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getDataGeneralBySalesOrder() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_sales_order = Validation::factory($this->request->post())
                    ->rule('id_sales_order', 'not_empty');
            if ($o_post_id_sales_order->check()) {
                $data_post_id_sales_order = $o_post_id_sales_order->data();
                Database::instance()->begin();
                $user_incharge = $this->getSessionParameter(self::USER_ID);
                $idStoreAsigned = DB::select(
                                        array('idStore', 'idStoreAsigned'), array('storeName', 'display')
                                )
                                ->from('rms_store')
                                ->where('status', '=', ':STATUS')
                                ->and_where('idUser', '=', ':INCHARGE')
                                ->param(':STATUS', self::STATUS_ACTIVE)
                                ->param(':INCHARGE', $user_incharge)
                                ->execute()->current();

                $sql_sales_order = DB::select(
                                        array('rs.idSalesOrder', 'idSalesOrder'), array('rc.idCustomers', 'idCustomer'), array(DB::expr("CONCAT(IF(rcc.idCustomersCompany IS NULL,bp.fullName, rcc.name) ,' - ', IF(rcc.idCustomersCompany IS NULL,bp.document, rcc.code))"), 'customersName'), array(DB::expr("IF(rcc.idCustomersCompany IS NULL,:typeperson, :typecompany)"), 'type')
                                )
                                ->from(array('rms_salesorder', 'rs'))
                                ->join(array('rms_customers', 'rc'))->on('rs.idCustomers', '=', 'rc.idCustomers')
                                ->join(array('bts_person', 'bp'), 'LEFT')->on('rc.idPerson', '=', 'bp.idPerson')
                                ->join(array('rms_customerscompany', 'rcc'), 'LEFT')->on('rc.idCustomersCompany', '=', 'rcc.idCustomersCompany')
                                ->where('rs.idSalesOrder', '=', ':ID_SALES_ORDER')
                                ->and_where('rs.status', '=', self::SALES_ORDER_STATUS_RESERVED)
                                ->param(':typeperson', self::CLIENT_TYPE_PERSON)
                                ->param(':typecompany', self::CLIENT_TYPE_COMPANY)
                                ->param(':ID_SALES_ORDER', $data_post_id_sales_order['id_sales_order'])
                                ->execute()->current();
               
                $sql_detail_sales_order = DB::select(
                                        array('rp.idProduct', 'idProduct'), array('rp.aliasProduct', 'codigo'), array('rp.haveDetail', 'haveDetail'), array('rp.productName', 'producto'), array('rsd.quantity', 'amount'), array('rsd.unitPrice', 'price'), array('rsd.totalPrice', 'importe')
                                )
                                ->from(array('rms_salesorder', 'rs'))
                                ->join(array('rms_salesorderdetail', 'rsd'))->on('rs.idSalesOrder', '=', 'rsd.idSalesOrder')
                                ->join(array('rms_lot', 'lot'))->on('lot.idLot', '=', 'rsd.idLot')
                                ->join(array('rms_products', 'rp'))->on('lot.idProduct', '=', 'rp.idProduct')
                                ->join(array('rms_store', 'rst'))->on('rsd.idStore', '=', 'rst.idStore')
                                ->where('rs.idSalesOrder', '=', ':ID_SALES_ORDER')
                                ->and_where('rs.status', '=', ':SALES_ORDER_STATUS')
                                ->and_where('rst.idStore', '=', ':ID_STORE_ASIGNED')
                                ->param(':ID_SALES_ORDER', $data_post_id_sales_order['id_sales_order'])
                                ->param(':SALES_ORDER_STATUS', self::SALES_ORDER_STATUS_RESERVED)
                                ->param(':ID_STORE_ASIGNED', $idStoreAsigned['idStoreAsigned'])
                                ->param(':IS_GIVE', self::STATUS_DEACTIVE)
                                ->execute()->as_array();
                Database::instance()->commit();
                $a_response['dataGeneral'] = $sql_sales_order;
                $a_response['dataDetail'] = $sql_detail_sales_order;
            } else {
                throw new Exception('Seleccione Una Orden de Venta', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getClients() {
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':typeperson' => self::CLIENT_TYPE_PERSON,
                ':typecompany' => self::CLIENT_TYPE_COMPANY
            );
            $o_select_company = DB::select(
                                    array('cus.idCustomers', 'id'), array(DB::expr("CONCAT(IF(cus.idCustomersCompany IS NULL,pe.fullName, cuscom.name) ,' - ', IF(cus.idCustomersCompany IS NULL,pe.document, cuscom.code))"), 'customers'), array(DB::expr("IF(cus.idCustomersCompany IS NULL,:typeperson, :typecompany)"), 'type')
                            )
                            ->from(array('rms_customers', 'cus'))
                            ->join(array('bts_person', 'pe'), 'LEFT')->on('cus.idPerson', '=', 'pe.idPerson')
                            ->join(array('rms_customerscompany', 'cuscom'), 'LEFT')->on('cus.idCustomersCompany', '=', 'cuscom.idCustomersCompany')
                            ->where(DB::expr("CONCAT(pe.fullName, pe.document)"), 'LIKE', ':search')
                            ->or_where(DB::expr("CONCAT(cuscom.name, cuscom.code)"), 'LIKE', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $a_response['data'] = $o_select_company;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createOutcomeStock() {
        $a_response = $this->json_array_return;
        try {
            $array_grid = $this->request->post('array_grid');
            $obj_frm = $this->request->post('obj_frm');
            $array_grid_detail = $this->request->post('array_grid_detail');
            $array_detail_id_pl = $this->request->post('array_detail_id_pl');

            $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
            $dateOutcome = DateTime::createFromFormat($format, $obj_frm['input_date_outcome']);
            Database::instance()->begin();
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_SALIDA;
            $obj_move->dateOutcome = DateHelper::fnPHPDate2MySQLDatetime($dateOutcome);
            $obj_move->moveReference = $obj_frm['text_doc_ref'];
            $obj_move->idCustomer = $obj_frm['idClient'];
            $obj_move->idTypeMove = $obj_frm['select_typeMove'];
            $obj_move->idStore = $obj_frm['idStore'];
            $obj_move->currency = $obj_frm['currency'];
            $obj_move->idSalesOrder = ($obj_frm['select_salesOrders'] != '') ? $obj_frm['select_salesOrders'] : NULL;
            $obj_move->status = self::STATUS_ACTIVE;

            $obj_move->save();
            $idMov = $obj_move->idMove;
            $idStore = $obj_move->idStore;  
            
             
            foreach ($array_grid as $v) {
                
                
                if ($obj_frm['select_salesOrders'] == '' && $obj_frm['for_outcome'] != 'sell') {
                    $obj_stock = new Model_Stock();
                    $obj_stock->where('idProduct', '=', $v['id'])
                            ->and_where('idStore', '=', $idStore)->find();
                    if ($obj_stock->loaded()) {
                        $obj_stock->amount = $obj_stock->amount - $v['amount'];
                    }      
                    $obj_stock->save();
                   
//                    else {
//                       // $obj_stock = new Model_Stock();
//                        $obj_stock->idProduct = $v['id'];
//                        $obj_stock->idStore = $idStore;
//                        $obj_stock->amount = $v['amount'];
//                    }
                }
                if ($v['haveDetail'] == self::STATUS_ACTIVE) {
                    //Producto detalle
                    foreach ($array_grid_detail as $pd) {
                        $obj_movedetail = new Model_Movedetail();
                        $obj_product_d = new Model_Productdetail($pd);   
                        if ($obj_product_d->idProduct == $v['id']) {
                            //Modificar la cantidad del producto detalle a 0
                            $obj_product_d->amount = $obj_product_d->amount  - 1;
                //            $obj_product_d->status = self::STATUS_DESACTIVE;
                            $obj_product_d->save();
                            $obj_movedetail->idMove = $idMov;
                            $obj_movedetail->amount = 1;
                            $obj_movedetail->unitPrice = $v['price'];
                            $obj_movedetail->status = self::STATUS_ACTIVE;
                            $obj_movedetail->dateOutcomeDetail = DateHelper::fnPHPDate2MySQLDatetime($dateOutcome);
                            $obj_movedetail->idProductDetail = $obj_product_d->idProductDetail;
                        }
                    }                     
                }
                else {
                    
                    //Lotes 
                    foreach ($array_detail_id_pl as $pd) {
                        
                        $obj_movedetail = new Model_Movedetail();
                        $obj_lot_d =  new Model_Lot($pd['idLot']);
                        if($obj_lot_d->idProduct == $v['id']){
                            // Modificar la cantidad del lot 
                            $obj_lot_d->amount = $obj_lot_d->amount - $pd['amount'];
                            $obj_lot_d->save();
                            $obj_movedetail->idLot = $obj_lot_d->idLot;
                            $obj_movedetail->idMove = $idMov;
                            $obj_movedetail->amount = $pd['amount'];
                            $obj_movedetail->unitPrice = $v['price'];
                            $obj_movedetail->status = self::STATUS_ACTIVE;
                            $obj_movedetail->dateOutcomeDetail = DateHelper::fnPHPDate2MySQLDatetime($dateOutcome);                         
                        }
                    }
                } 
                $obj_movedetail->save();
            }

            /*                ----------------------------------------------------------      CAMBIOS          -----------------------------------             */
//            if ($obj_frm['select_salesOrders'] != '' && $obj_frm['for_outcome'] = 'sell') {
//                $obj_order_sales_detail = ORM::factory('Salesorderdetail')
//                        ->where('idSalesOrder', '=', $obj_frm['select_salesOrders'])
//                        ->and_where('idStore', '=', $idStore)
//                        ->find_all();
//                foreach ($obj_order_sales_detail as $val) {
//                    $obj_sales_order_detail = new Model_Salesorderdetail($val->idSalesOrderDetail);
//                    $obj_sales_order_detail->isGive = self::STATUS_ACTIVE;
//                    $obj_sales_order_detail->save();
//                }
//                $obj_order_sales_detail2 = ORM::factory('Salesorderdetail')
//                        ->where('idSalesOrder', '=', $obj_frm['select_salesOrders'])
//                        ->and_where('idStore', '<>', $idStore)
//                        ->find_all();
//                $i = 0;
//                foreach ($obj_order_sales_detail2 as $va) {
//                    if ($va->isGive == 0) {
//                        $i = $i + 1;
//                    }
//                }
//                if ($i == 0) {
//                    $obj_order_sales = new Model_Salesorder($obj_frm['select_salesOrders']);
//                    $obj_order_sales->status = self::SALES_ORDER_STATUS_GIVE;
//                    $obj_order_sales->save();
//                }
//            }
            
            
            
            Database::instance()->commit();
        
            
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getDateilByProductId() {
        $a_response = $this->json_array_return;
        try {
            $id_store = $this->request->post('idstore');
            $id_product = $this->request->post('idpg');

//            $query_data_not_show="SELECT mds.idProductDetail FROM `rms_movedetail` mds 
//                WHERE mds.idMoveDetail IN (SELECT MAX(mvss.idMoveDetail) FROM rms_product_detail pdss 
//		INNER JOIN rms_movedetail mvss ON mvss.idProductDetail=pdss.idProductDetail
//		INNER JOIN rms_move mss ON mvss.idMove=mss.idMove
//		WHERE mss.idStore=$id_store AND pdss.amount<>0 AND (mss.typeMoveStore='VENTA_DIRECTA' OR mss.typeMoveStore='TRAS_SALIDA' OR mss.typeMoveStore='SALIDA')
//		GROUP BY pdss.idProductDetail)";
//                $rs=DB::query(Database::SELECT,$query_data_not_show)->execute()->as_array();
//                $cad='';                
//                if(count($rs)>0){                    
//                    foreach ($rs as $key=>$v) {                    
//                        $a[$key]=$v['idProductDetail'];                        
//                    }
//                    $cad= implode(',',$a);                    
//                }else{
//                    $cad=0;
//                }

            $query_pd = "SELECT rp.*,rpd.* FROM  rms_products rp 
                    INNER JOIN rms_product_detail rpd  ON rp.idProduct = rpd.idProduct 
                    INNER JOIN rms_movedetail mvdi ON mvdi.idProductDetail = rpd.idProductDetail 
                    INNER JOIN rms_move mv ON mvdi.idMove = mv.idMove 
                    WHERE rp.idProduct =$id_product   AND mv.idStore =$id_store AND rpd.amount <> 0 GROUP BY rpd.idProductDetail HAVING MOD(COUNT(rpd.idProductDetail),2)<>0 ORDER BY rp.idProduct DESC ";
            $rs = DB::query(Database::SELECT, $query_pd)->execute()->as_array();
            // echo Database::instance()->last_query;
            // die();
            $a_response['data'] = $rs;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}