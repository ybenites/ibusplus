<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Controller_Private_Stockincome extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('stockincome', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('stockincome', self::FILE_TYPE_JS));
        $inventory_stockincome = new View('inventory/stockincome');

        $typeMove = DB::select(
                                array('idTypeMove', 'id'), array('typeMoveName', 'display')
                        )->from('rms_typemove')
                        ->where('typeMoveState', '=', ':MOVE')
                        ->and_where('status', '=', ':STATUS')
                        ->param(':MOVE', self::TYPE_MOVE_INGRESO)
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->execute()->as_array();
        $suplier = DB::select(
                                array('idSupplier', 'id'), array('name', 'display')
                        )
                        ->from('rms_supplier')
                        ->where('status', '=', ':STATUS')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->execute()->as_array();
        $currency = $this->a_TYPE_CURRENCY();
        $user_incharge = $this->getSessionParameter(self::USER_ID);
        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->as_array();

        $brand = DB::select(array('idBrand', 'id'), array('brandName', 'name'))
                        ->from('rms_brands')
                        ->execute()->as_array();
        $category = DB::select(array('idCategory', 'id'), array('categoryName', 'name'), array('codigo', 'codigo'))
                        ->from('rms_category')
                        ->where('idSuperCategory', 'IS', NULL)
                        ->and_where('status', '=', self::STATUS_ACTIVE)
                        ->execute()->as_array();

        // $measures = $this->a_UNIT_MEASURES();
        $inventory_stockincome->brand = $brand;
        $inventory_stockincome->category = $category;
        //  $inventory_stockincome->measures = $measures;

        $inventory_stockincome->today = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE));
        $inventory_stockincome->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $inventory_stockincome->type_document = self::DT_NOTA_INGRESO;
        $inventory_stockincome->typeMove = $typeMove;
        $inventory_stockincome->supplier = $suplier;
        $inventory_stockincome->currency = $currency;
        $inventory_stockincome->store = $store;
        $inventory_stockincome->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $inventory_stockincome->bTagGenerator = $this->getOptionValue(self::INVENTORY_TAG_GENERATOR);

        $obj_store = ORM::factory('stores')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $inventory_stockincome->array_store = $obj_store;

        $this->template->content = $inventory_stockincome;
    }

    public function action_getProductAutocomplete() {
        $a_response = $this->json_array_return;
        try {
            $o_post_data_product = Validation::factory($this->request->post())
                    ->rule('term', 'not_empty')
                    ->rule('typeSearch', 'not_empty');
            if ($o_post_data_product->check()) {
                $data_post_product = $o_post_data_product->data();
                $sql = DB::select(
                                array('productName', 'dataMostrar')
                                , array('aliasProduct', 'otraData')
                                , array('idProduct', 'id')
                        )
                        ->from('rms_products');
                if ($data_post_product['typeSearch'] == 'search_description') {
                    $sql->where('productName', 'like', ':TERM');
                } elseif ($data_post_product['typeSearch'] == 'search_code') {
                    $sql->where('aliasProduct', 'like', ':TERM');
                }
                $sql->where('status', '=', self::STATUS_ACTIVE);
                $sql->param(':TERM', '%' . $data_post_product['term'] . '%');
                $a_response['data'] = $sql->execute()->as_array();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProductById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_data_product = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('idStore', 'not_empty');
            if ($o_post_data_product->check()) {
                $data_post_product = $o_post_data_product->data();
                $obj_stock = new Model_Stock();
                $obj_stock->where('idProduct', '=', $data_post_product['id'])
                        ->and_where('idStore', '=', $data_post_product['idStore'])->find();
                $obj_product = new Model_Products($data_post_product['id']);
                if ($obj_stock->loaded()) {
                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = $obj_stock->amount;
                } else {
                    $a_response['data'] = $obj_product->as_array();
                    $a_response['amount'] = 0;
                }
            } else {
                throw new Exception('Hay Datos Faltantes. Seleccione un Almacén.', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getDataGeneralByPurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_purchase_order = Validation::factory($this->request->post())
                    ->rule('id_purchase_order', 'not_empty');
            if ($o_post_id_purchase_order->check()) {
                $data_post_id_purchase_order = $o_post_id_purchase_order->data();
                
                

                Database::instance()->begin();
                $sql_purchase_order = DB::select(
                                        array('sp.idSupplier', 'idSuplier')
                                        , array('sp.name', 'supplierName')
                                        , array('po.currency', 'currency'), array('po.idPurchaseOrder', 'idPurchaseOrder')
                                )
                                ->from(array('rms_purchaseorder', 'po'))
                                ->join(array('rms_supplier', 'sp'))->on('po.idSupplier', '=', 'sp.idSupplier')
                                ->where('po.statusPurchase', '=', ':PURCHASE_ORDER_STATUS')
                                ->where('po.idPurchaseOrder', '=', ':ID_PURCHASE_ORDER')
                                ->param(':PURCHASE_ORDER_STATUS', self::PURCHASE_ORDER_STATUS_APROBADO)
                                ->param(':ID_PURCHASE_ORDER', $data_post_id_purchase_order['id_purchase_order'])
                                ->execute()->current();

                $sql_detail_purchase_order = DB::select(
                                        array('p.idProduct', 'id')
                                        , array('p.aliasProduct', 'codigo')
                                        , array('p.haveDetail', 'haveDetail')
                                        , array('p.productName', 'producto')
                                        , array('pod.quantity', 'amount')
                                        , array('pod.totalUnit', 'price')
                                        , array('p.productPriceSell', 'saleprice')
                                        , array('pod.totalAmount', 'importe')
                                        , DB::expr("(pod.quantity * p.productPriceSell) AS importesale")
                                )->from(array('rms_purchaseorder', 'po'))
                                ->join(array('rms_purchaseorderdetail', 'pod'))->on('po.idPurchaseOrder', '=', 'pod.idPurchaseOrder')
                                ->join(array('rms_products', 'p'))->on('pod.idProduct', '=', 'p.idProduct')
                                ->where('po.statusPurchase', '=', ':PURCHASE_ORDER_STATUS')
                                ->where('po.idPurchaseOrder', '=', ':ID_PURCHASE_ORDER')
                                ->and_where('pod.status','=',  self::STATUS_ACTIVE)
                                ->param(':PURCHASE_ORDER_STATUS', self::PURCHASE_ORDER_STATUS_APROBADO)
                                ->param(':ID_PURCHASE_ORDER', $data_post_id_purchase_order['id_purchase_order'])
                                ->execute()->as_array();
                Database::instance()->commit();
                $a_response['dataGeneral'] = $sql_purchase_order;
                $a_response['dataDetail'] = json_encode($sql_detail_purchase_order);
            } else {
                throw new Exception('Seleccione Una Orden de Compra', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createIncomeStock() {
        $a_response = $this->json_array_return;
        try {
            $array_product = $this->request->post('array_product'); // Del producto detalle
            $array_detail = $this->request->post('array_detail_product'); // Del producto detalle
            $array_lot = $this->request->post('array_lot_product'); // Del producto lote 
            $obj_frm = $this->request->post('obj_frm');

            //Creo el movimiento de Ingreso
            $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
            $dateIncome = DateTime::createFromFormat($format, $obj_frm['input_date_income']);
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_INGRESO;
            $obj_move->dateIncome = DateHelper::fnPHPDate2MySQLDatetime($dateIncome);
            $obj_move->moveReference = $obj_frm['text_doc_ref'];
            $obj_move->idSupplier = $obj_frm['select_supplier'];
            $obj_move->idTypeMove = $obj_frm['select_typeMove'];
            $obj_move->idStore = $obj_frm['select_store'];
            $obj_move->currency = $obj_frm['currency'];
            $obj_move->status = self::STATUS_ACTIVE;
            $obj_move->idPurchaseOrders = ($obj_frm['select_purchaseOrders'] != '') ? $obj_frm['select_purchaseOrders'] : NULL;

            Database::instance()->begin();
            $obj_move->save();
            $idMov = $obj_move->idMove;
            $idStore = $obj_move->idStore;
            foreach( $array_product as $v)
            {
                //Modifico el Stock
                $obj_stock = new Model_Stock();
                $obj_stock->where('idProduct', '=', $v['id'])
                          ->and_where('idStore', '=', $idStore)->find();
                if ($obj_stock->loaded()){
                     $obj_stock->amount = $obj_stock->amount+$v['amount'];
                }else{
                     $obj_stock = new Model_Stock();
                     $obj_stock->idProduct = $v['id'];
                     $obj_stock->idStore = $idStore;
                     $obj_stock->amount = $v['amount'];                 
                }
                $obj_stock->save();
                /* Producto con Detalles */
                if($v['haveDetail'] == '1'){
                    foreach($array_detail as $key => $item){
                        if($key == $v['id']){
                            foreach($item as $detail){
                                
                                $obj_movedetail = new Model_Movedetail();
                                $obj_movedetail->idMove = $idMov;
                                $obj_movedetail->amount = 1;
                                $obj_movedetail->unitPrice = $v['price'];
                                $obj_movedetail->salePrice = $v['saleprice'];
                                $obj_movedetail->dateIncomeDetail = DateHelper::fnPHPDate2MySQLDatetime($dateIncome);
                                $obj_movedetail->status = self::STATUS_ACTIVE;

                                $obj_product_detail = new Model_Productdetail();
                                $obj_product_detail->idProduct = $v['id'];
                                $obj_product_detail->amount = 1;
                                $obj_product_detail->unitPrice = $v['price'];
                                $obj_product_detail->salePrice = $v['saleprice'];
                                $obj_product_detail->description = $detail['description'];
                                $obj_product_detail->serie_electronic = $detail['serie_electronica'];
                                $obj_product_detail->save();
                                $obj_movedetail->idProductDetail = $obj_product_detail->idProductDetail;
                                $obj_movedetail->save();
                           }
                        }
                    //else ... No recorrera el resto de los detalles que no sea ese producto
                    //Creo los movimiento detalles    
                    }
                       
                /* Producto con Lotes */
                }else{
                    foreach($array_lot as $lot){
                        if($lot['idGeneral'] ==  $v['id']){
                            $obj_movedetail = new Model_Movedetail();
                            $obj_movedetail->idMove = $idMov;
                            $obj_movedetail->amount = $v['amount'];
                            $obj_movedetail->unitPrice = $v['price'];
                            $obj_movedetail->salePrice = $v['saleprice'];
                            $obj_movedetail->status = self::STATUS_ACTIVE;

                            $obj_lot = new Model_Lot();
                            $obj_lot->idProduct =  $v['id'];
                            $obj_lot->lotName = "LT_" . $lot['nameLot'];
                            $obj_lot->description = "LOTE";
                            $obj_lot->amount = $v['amount'];
                            $obj_lot->unitPrice = $v['price'];
                            $obj_lot->salePrice = $v['saleprice'];
                            $obj_lot->idStore = $idStore;
                            $obj_lot->status = self::STATUS_ACTIVE;
                            $obj_lot->save();
                            $obj_movedetail->idLot = $obj_lot->idLot;
                            $obj_movedetail->save(); 
                       }
                    }
                }
            }
            if ($obj_frm['select_purchaseOrders'] != '' && $obj_frm['for_income'] = 'buy') {
                $obj_order_purchase = new Model_Purchaseorder($obj_frm['select_purchaseOrders']);
                
                
                $obj_order_purchase->statusPurchase = self::PURCHASE_ORDER_STATUS_RECEPCIONADO;
                $obj_order_purchase->save();
                
                $this->createHistory($obj_frm['select_purchaseOrders']);
            }

            Database::instance()->commit();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    //Creando el registro del historial de las ordenes de compra.
    public function createHistory($data_purchaseorder){    
            $id_User = $this->getSessionParameter(self::USER_ID);
        
            $historypurchase = New Model_Historypurchase();
            $data_purchaseorders['idPurchaseOrder']=$data_purchaseorder;
            $data_purchaseorders['status']=self::PURCHASE_ORDER_STATUS_RECEPCIONADO;
            $data_purchaseorders['dateHistoryPurchase']=  DateHelper::getFechaFormateadaActual();
            $data_purchaseorders['idUser']=$id_User;
            $historypurchase->saveH($data_purchaseorders);
    }

    public function action_getAllPurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $purchase_orders = DB::select(
                                    array('idPurchaseOrder', 'id'), array(DB::expr("CONCAT(serie,'-',correlative)"), 'display')
                            )
                            ->from('rms_purchaseorder')
                            ->where('statusPurchase', '=', ':STATUSPURCHASE')
                            ->param(':STATUSPURCHASE', self::PURCHASE_ORDER_STATUS_APROBADO)
                            ->execute()->as_array();
            $a_response['data'] = $purchase_orders;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_validateSerie(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
              $v_serie = $this->request->post('val_serie');                     
              
              $a_cols = array(
                     array('pd.serie_electronic', 'serie')
                 );
        
             $serie = DB::select_array($a_cols)
                  ->from(array('rms_product_detail','pd' ))
                  ->where('pd.serie_electronic', '=', $v_serie)
                  ->execute()->current();  
    
              //print_r(Database::instance()->last_query);die();
          
             $a_response['data'] = $serie['serie'];
         
        }catch (Exception $e_exc) {
             $a_response['code'] = self::CODE_ERROR;
             $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}
