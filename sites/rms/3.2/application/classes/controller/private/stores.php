<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stores
 *
 * @author Yime
 */
class Controller_Private_Stores extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('stores', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('stores', self::FILE_TYPE_JS));
        $inventory_stores = new View('inventory/stores');
        $all_office = DB::select(
                                array('bo.idOffice', 'id'), array(DB::expr("CONCAT(bo.name,' // ',bo.address)"), 'display')
                        )->from(array('bts_office', 'bo'))->where('bo.status', '=', ':STATUS')
                        ->param(':STATUS', self::STATUS_ACTIVE)->execute()->as_array();

        $inventory_stores->all_office = $all_office;
        $this->template->content = $inventory_stores;
    }

    public function action_createNewStore() {
        $a_response = $this->json_array_return;
        try {
            $o_post_store = Validation::factory($this->request->post())
                    ->rule('storeName', 'not_empty')
                    ->rule('storeShortName', 'not_empty')
                    ->rule('storeOffice', 'not_empty');
            if ($o_post_store->check()) {
                $data_post_store = $o_post_store->data();
                $obj_store = new Model_Stores();
                $obj_store->storeName = $data_post_store['storeName'];
                $obj_store->storeShortName = $data_post_store['storeShortName'];
                $obj_store->idOffice = $data_post_store['storeOffice'];
                Database::instance()->begin();
                if ($data_post_store['storeUser'] != '') {
                    $idIncharge = $data_post_store['storeUser'];
                    $exist_inCharge = DB::select('*')->from('rms_store')
                                    ->where('idUser', '=', ':INCHARGE')
                                    ->param(':INCHARGE', $idIncharge)
                                    ->execute()->as_array();
                    if (count($exist_inCharge) > 0) {
                        foreach ($exist_inCharge as $v) {
                            $obj_update_store = new Model_Stores($v['idStore']);
                            $obj_update_store->idUser = NULL;
                            $obj_update_store->save();
                        }
                    }
                    $obj_store->idUser = $idIncharge;
                } else {
                    $obj_store->idUser = NULL;
                }
                $obj_store->status = self::STATUS_ACTIVE;
                $obj_store->save();
                Database::instance()->commit();
                $this->cleaningCache();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listAllStores() {
        $this->auto_render = FALSE;
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $a_array_cols = array(
                "id" => "st.idStore",
                "storeName" => "st.storeName",
                "storeShortName" => "st.storeShortName",
                "officeName" => "bo.name",
                "address" => "bo.address",
                "phone" => "bo.phone",
                "encargado" => "bp.fullName",
                "status" => "st.status",
                "creationDate" => "DATE_FORMAT(st.creationDate,'$s_date_format $s_time_format')"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);

            $s_tables_join = 'rms_store st ' .
                    'INNER JOIN `bts_office` bo ON bo.`idOffice`=st.`idOffice` ' .
                    'LEFT JOIN bts_user bu ON bu.idUser=st.idUser ' .
                    'LEFT JOIN `bts_person` bp ON bp.`idPerson`=bu.`idUser` ';

            $s_where_conditions = "1=1 and st.status=" . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

    public function action_getStoreById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_store = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($o_post_id_store->check()) {
                $data_post_id_store = $o_post_id_store->data();
                $obj_store = new Model_Stores($data_post_id_store['id']);
                $a_response['data'] = $obj_store->as_array();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_updateNewStore() {
        $a_response = $this->json_array_return;
        try {
            $o_post_store = Validation::factory($this->request->post())
                    ->rule('idStore', 'not_empty')
                    ->rule('storeName', 'not_empty')
                    ->rule('storeOfficee', 'not_empty')
                    ->rule('storeShortName', 'not_empty');
            if ($o_post_store->check()) {
                $data_post_store = $o_post_store->data();
                $obj_store = new Model_Stores($data_post_store['idStore']);
                $obj_store->storeName = $data_post_store['storeName'];
                $obj_store->storeShortName = $data_post_store['storeShortName'];
                $obj_store->idOffice = $data_post_store['storeOfficee'];
                Database::instance()->begin();
                if ($data_post_store['storeUsere'] != '') {
                    $idIncharge = $data_post_store['storeUsere'];
                    $exist_inCharge = DB::select('*')->from('rms_store')
                                    ->where('idUser', '=', ':INCHARGE')
                                    ->param(':INCHARGE', $idIncharge)
                                    ->execute()->as_array();
                    if (count($exist_inCharge) > 0) {
                        foreach ($exist_inCharge as $v) {
                            $obj_update_store = new Model_Stores($v['idStore']);
                            $obj_update_store->idUser = NULL;
                            $obj_update_store->update();
                        }
                    }
                    $obj_store->idUser = $idIncharge;
                } else {
                    $obj_store->idUser = NULL;
                }
                $obj_store->status = self::STATUS_ACTIVE;
                $obj_store->update();
                Database::instance()->commit();
                $this->cleaningCache();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteStore() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_store = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($o_post_id_store->check()) {
                $data_post_id_store = $o_post_id_store->data();
                $obj_store = New Model_Stores($data_post_id_store['ids']);
                $obj_store->status = self::STATUS_DEACTIVE;
                $obj_store->save();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getCompleteStoreUser() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_office = Validation::factory($this->request->post())
                    ->rule('idOffice', 'not_empty');
            if ($o_post_id_office->check()) {
                $data_post_id_office = $o_post_id_office->data();
                $id_office = $data_post_id_office['idOffice'];
                $sql = DB::select(
                                        array('bp.idPerson', 'id'), array('fullName', 'display')
                                )
                                ->from(array('bts_user', 'bu'))
                                ->join(array('bts_person', 'bp'))->on('bu.idUser', '=', 'bp.idPerson')
                                ->execute()->as_array();
                $a_response['data'] = $sql;
            } else {
                throw new Exception('No haz Seleccionado una oficina', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}