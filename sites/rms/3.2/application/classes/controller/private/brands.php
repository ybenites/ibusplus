<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of brands
 *
 * @author Yime
 */

class Controller_Private_Brands extends Controller_Private_Admin {
    public function action_index(){
        $this->mergeStyles(ConfigFiles::fnGetFiles('brands', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('brands', self::FILE_TYPE_JS));
        $inventory_brands = new View('inventory/brands');
        $this->template->content = $inventory_brands;        
    }
    public function action_createNewBrand(){
        $a_response = $this->json_array_return;
        try {            
            $o_post_brand = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty')
                    ->rule('nameCommercial', 'not_empty');
            if($o_post_brand->check()){
                $data_post_brand=$o_post_brand->data();
                $obj_brand=New Model_Brands(); 
                $obj_brand->brandName=$data_post_brand['name'];
                $obj_brand->brandNameCommercial=$data_post_brand['nameCommercial'];
                $obj_brand->status=self::STATUS_ACTIVE;
                $obj_brand->save();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }        
        $this->fnResponseFormat($a_response);
    }
    public function action_updateNewBrand(){
        $a_response = $this->json_array_return;
        try {            
            $o_post_brand = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('name', 'not_empty')
                    ->rule('nameCommercial', 'not_empty');
            if($o_post_brand->check()){
                $data_post_brand=$o_post_brand->data();
                $obj_brand=New Model_Brands($data_post_brand['id']); 
                $obj_brand->brandName=$data_post_brand['name'];
                $obj_brand->brandNameCommercial=$data_post_brand['nameCommercial'];
                $obj_brand->status=self::STATUS_ACTIVE;
                $obj_brand->save();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }        
        $this->fnResponseFormat($a_response);
    }
    public function action_listAllBrands(){
        $this->auto_render = FALSE;        
        try {            
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');             
            
            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
                       
            if (!$i_idx)
                $i_idx = 1;
                        
            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search')); 
            
            $a_array_cols = array(
                "id" => "br.idBrand",
                "brandName" => "br.brandName",
                "brandNameCommercial" => 'br.brandNameCommercial',
                "userCreate" => "p.fullName",
                "status" => "br.status",
                "creationDate" => "DATE_FORMAT(br.creationDate,'$s_date_format $s_time_format')"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);
            
            
            if ($b_search_on == 'true') {
                if ($this->request->post('filters')!='') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));                    
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    
                    foreach ($this->request->post() as $i_key => $s_value) {
 
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }
                     
            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);
            
            $s_tables_join ='rms_brands br '.
                    'INNER JOIN bts_user u ON u.idUser=br.userCreate '.
                    'INNER JOIN `bts_person` p ON p.`idPerson`=u.`idUser` ';

            $s_where_conditions ="1=1 and br.status=".self::STATUS_ACTIVE;                        
            
            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);                        
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true),self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }
    public function action_deleteBrands(){
        $a_response = $this->json_array_return;
        try {
            $o_post_id_brand = Validation::factory($this->request->post())
                    ->rule('idb', 'not_empty');                    
            if($o_post_id_brand->check()){
                $data_post_id_brand=$o_post_id_brand->data();
                $obj_brand=New Model_Brands($data_post_id_brand['idb']);                 
                $obj_brand->status=self::STATUS_DEACTIVE;
                $obj_brand->save();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    public function action_getBrandById(){
        $a_response = $this->json_array_return;
        try {
            $o_post_id_brand = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');                    
            if($o_post_id_brand->check()){
                $data_post_id_brand=$o_post_id_brand->data();
                $obj_brand=New Model_Brands($data_post_id_brand['id']);
                $a_response['data']=$obj_brand->as_array();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
}