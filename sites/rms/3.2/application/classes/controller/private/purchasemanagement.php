<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of purchasemanagement
 *
 * @author Escritorio
 */
class Controller_Private_Purchasemanagement extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('purchasemanagement', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('purchasemanagement', self::FILE_TYPE_JS));
        $purchasemanagement_view = new View('purchase/purchasemanagement');
        $this->template->content = $purchasemanagement_view;
    }

    public function action_getListPurchase() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            $boleta = self::DT_BOLETA;

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'ID' => "p.idPurchaseOrder",
                'document' => "p.documentType",
                'nrodocument' => "CONCAT(p.serie,' - ',LPAD(p.correlative,8,'0'))",
                'currency' => 'p.currency',
                'supplier' => "CONCAT(s.name,'-',s.code)",
                'paymentType' => 'p.methodPayment',
                'subtotal' => "IF(p.documentType='$boleta',p.totalAmountWithOutTax,p.totalAmountWithTax)",
                'tax' => "p.totalTax",
                'total' => "p.totalAmount",
                'status'=>'p.statusPurchase'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_purchaseorder p' .
                    ' INNER JOIN rms_supplier s ON s.idSupplier = p.idSupplier';

            $status1 = self::PURCHASE_ORDER_STATUS_FACTURADO;
            $status2 = self::PURCHASE_ORDER_STATUS_RECEPCIONADO;

            $where_conditions = "p.statusPurchase IN ('$status1','$status2')";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);

            //print_r(Database::instance()->last_query);
            //  die(); 
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_deletePurchase() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchase = Validation::factory($this->request->post())
                    ->rule('idp','not_empty');
            if($post_idPurchase->check()){
                $data_purchase = $post_idPurchase->data();
                $purchase = new Model_Purchase($data_purchase['idp']);
                $purchase->purchaseStatus = self::PURCHASE_ORDER_STATUS_CANCELADO;
                $purchase->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>
