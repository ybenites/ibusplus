<?php

/**
 * Description of customers
 *
 * @author G@
 */
class Controller_Private_Salesmanagement extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->checkCashBox();
        $this->mergeStyles(ConfigFiles::fnGetFiles('ordersales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('ordersales', self::FILE_TYPE_JS));
        $view_customers = new View('sales/salesmanagement');
        $obj_store = ORM::factory('stores')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $view_customers->array_store = $obj_store;
        $view_customers->a_year_report = $this->a_year_report();
        $view_customers->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_customers->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_customers->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $view_customers->bBarcodeReader = $this->getOptionValue(self::INVENTORY_BARCODE_READER);
        $view_customers->bTaxEnabled = $this->getOptionValue(self::SO_TAX_ENABLED);
        $view_customers->sTaxName = $this->getOptionValue(self::SO_TAX_NAME);
        $view_customers->fTax = $this->getOptionValue(self::SO_TAX_VALUE);
        $view_customers->activeSeller = $this->getOptionValue(self::SO_ACTIVE_SELLER);
        
        $view_customers->CLIENT_TYPE_PERSON = self::CLIENT_TYPE_PERSON;
        $view_customers->CLIENT_TYPE_COMPANY = self::CLIENT_TYPE_COMPANY;
        
        $v_userCode = DB::select(
                                array('us.idUser','id'), array('us.userName','nameUser')
                        )
                        ->from(array('bts_user','us'))
                        ->join(array('bts_group','gr'))->on('gr.idgroup','=','us.idgroup')
                        ->where('us.idgroup', '=', 2)
                        ->execute()->as_array();
        $view_customers->listUserCode =$v_userCode;
        
        $this->template->content = $view_customers;
    }

    public function action_getListSalesOrder() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $sd = $this->request->post('sd');
            $ed = $this->request->post('ed');
            $m = $this->request->post('m');
            $a = $this->request->post('a');
            $d = $this->request->post('d');
            $store = $this->request->post('store');
            $nrodocument = $this->request->post('document');

            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);

            $a_salesDocument = $this->fnGetClassVariable(self::VAR_NAME_A_DOCUMENT_TYPE);

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $date = date($date_format);

            $array_cols = array(
                'idSales' => "rs.idSales",
                'serie' => "rs.serie",
                'correlative' => "LPAD(rs.correlative,8,'0')",
                'methodPayment' => "
                    CASE 
                        WHEN rs.methodPayment = '" . self::SALES_PAYMENT_CASH . "' THEN '" . __(self::SALES_ORDER_PAYMENT_CASH) . "' 
                        WHEN rs.methodPayment = '" . self::SALES_PAYMENT_CREDITCARD . "' THEN '" . __(self::SALES_ORDER_PAYMENT_CREDIT) . "'              
                        WHEN rs.methodPayment = '" . self::SALES_PAYMENT_MIXTO . "' THEN '" . __(self::SALES_PAYMENT_MIXTO) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'customers' => "IF(pe.fullName IS NULL,CONCAT(cco.name,' - ',cco.code),CONCAT(pe.fullName,' - ',pe.document))",
                'storeName' => "st.storeName",
                'documentP' => "
                   CASE 
                        WHEN rs.documentType = '" . self::DT_BOLETA . "' THEN '" . __($a_salesDocument[self::DT_BOLETA]['value']) . "' 
                        WHEN rs.documentType = '" . self::DT_FACTURA . "' THEN '" . __($a_salesDocument[self::DT_FACTURA]['value']) . "'              
                        WHEN rs.documentType = '" . self::DT_RECIBO . "' THEN '" . __($a_salesDocument[self::DT_RECIBO]['value']) . "'
                        WHEN rs.documentType = '" . self::DT_TICKET . "' THEN '" . __($a_salesDocument[self::DT_TICKET]['value']) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'status' => "
                    CASE 
                        WHEN rs.status = '".self::SALES_STATUS_CANCELED."' THEN 'Anulado'
                        WHEN rs.status = '".self::SALES_STATUS_SOLD."' THEN 'Vendido'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'tax' => "rs.totaltax",
                'totalPrice' => "rs.totalPrice",
                'paymentAmount' => "rs.paymentAmount",
                'returnCash' => "rs.returnCash",
                'salesDate' => "DATE_FORMAT(rs.salesDate,'%d/%m/%Y')",
                'seller' => "se.fullName",
                'idUsercode' => "rs.idUserCode ",
                'UserCode' => "us.userName "
            );

            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
//BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
//BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {
                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = ' rms_sales rs' .
                    ' INNER JOIN rms_customers cu ON rs.idCustomers = cu.idCustomers' .
                    ' INNER JOIN rms_salesdetail sd ON rs.idSales = sd.idSales ' .
                    ' INNER JOIN rms_store st ON sd.idStore = st.idStore ' .
                    ' LEFT JOIN bts_user us ON us.idUser = rs.idUserCode ' .
                    ' LEFT JOIN rms_customerscompany cco ON cu.idCustomersCompany = cco.idCustomersCompany' .
                    ' LEFT JOIN bts_person pe ON cu.idPerson = pe.idPerson' .
                    ' LEFT JOIN bts_person se ON rs.userCreate = se.idPerson';



            $grouping = "GROUP BY sd.idSales";

            $where_conditions = " 1=1 ";

            if ($this->request->post('nrodocument') != NULL) {
                $temp = explode('-', $this->request->post('nrodocument'));
                $where_conditions .= " AND rs.serie LIKE '%" . trim($temp[0]) . "%'";
                $where_conditions .= " AND rs.correlative LIKE '%" . trim($temp[1]) . "%' ";
            }
            if ($this->request->post('store') != NULL) {
                $where_conditions .= " AND st.idStore IN (" . $this->request->post('store') . ") ";
            }
            if ($d != "") {
                $where_conditions .= " AND DATE_FORMAT(rs.salesDate,'$date_format') LIKE '" . $d . "' ";
            }
            if ($m != 0) {
                $where_conditions .= " AND MONTH(rs.salesDate) = " . $m . " AND YEAR(rs.salesDate) = " . $a . "";
            }
            if ($ed != "") {
                if ($sd != "") {
                    $where_conditions .= " AND rs.salesDate BETWEEN STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')";
                } else {
                    $where_conditions .= " AND DATE_FORMAT(rs.salesDate,'$date_format') LIKE '" . $ed . "' ";
                }
            } else {
                if ($sd != "") {
                    $where_conditions .= " AND DATE_FORMAT(rs.salesDate,'$date_format') LIKE '" . $sd . "' ";
                }
            }
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search, $grouping);        
            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start, $grouping);

            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);

//            echo Database::instance()->last_query;
//            die();
       } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_getDetalleSalesOrder() {
        $this->auto_render = false;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $idSalesOrder = $this->request->post('idSal');
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);

            if (!$sidx)
                $sidx = 1;

            $filterLud = "";

            if ($idSalesOrder != "") {
                $filterLud = "rs.idSales = '$idSalesOrder'";
            }

            $where_search = "";

            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idSales' => 'rsd.idSalesDetail',
                'aliasProduct' => 'pro.aliasProduct',
                'productName' => 'pro.productName',
                'lote' => "
                    CASE
                        WHEN lt.idLot IS NOT NULL THEN CONCAT(lt.lotName,': ',lt.description)
                    ELSE CONCAT(prd.serie_electronic ,': ',prd.description)
                    END",
                'storeName' => 'st.storeName',
                'quantity' => 'rsd.quantity',
                'unitPrice' => 'rsd.unitPrice',
                'discount' => 'rsd.discount',
                'totalPrice' => 'rsd.totalPrice',
                'status' => 'rs.status'
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = 'rms_sales rs' .
                    ' INNER JOIN rms_salesdetail rsd ON rs.idSales = rsd.idSales ' .
                    ' LEFT JOIN rms_lot lt ON rsd.idLot = lt.idLot ' .
                    ' INNER JOIN rms_store st ON rsd.idStore = st.idStore ' .
                    ' LEFT JOIN rms_product_detail prd ON rsd.idProductDetail = prd.idProductDetail ' .
                    ' INNER JOIN `rms_products` pro ON (pro.`idProduct` = prd.`idProduct` OR pro.idProduct = lt.idProduct)';

            $where_conditions = "" . $filterLud . " AND (rsd.statusDetail IS NULL OR rsd.statusDetail != '" . self::SALES_DETAIL_CANCELED . "')";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);
            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
            $salesOrder = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

//            print_r(Database::instance()->last_query);
//             die();
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $salesOrder, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /* ANULACIÓN DE LA VENTA */ 
    public function action_createCancel() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {

            $post = Validation::factory($this->request->post())
                    ->rule('idl', 'not_empty')
                    ->rule('idCancel', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $user = $this->getSessionParameter(self::USER_ID);
                $idCashBox_sales = DB::select(
                                array('ca.idCashBox','idCashBox')
                                )
                         ->from(array('rms_cashbox','ca'))
                         ->where('ca.status','=',self::CASHBOX_STATUS_OPENED)
                         ->and_where(DB::expr("DATE_FORMAT(ca.openDate,'%Y-%m-%d')"),'=',DateHelper::getfechaActual())
                         ->and_where('ca.idUser','=',$user)
                         ->execute()->current();

                Database::instance()->begin();
                $data = $post->data();
                // Actualizar estado de la Venta a Cancelado
                $sales = new Model_Sales($data['idl']);
                $sales->status = self::SALES_STATUS_CANCELED;
                $sales->payback = $data['idCancel'];
                $sales->idCashBoxCancel = $idCashBox_sales['idCashBox'];
                $sales->save();
                // Buscar sus detalles y regresar al stock 
                $salDetail = DB::select('*')
                                ->from('rms_salesdetail')
                                ->where('idSales', '=', $sales->idSales)
                                ->execute()->as_array();   
                // Creo movimiento de entrada 
                $mov = new Model_Move();
                $mov->typeMoveStore = self::TYPE_MOVE_VENTA_ANULACION;
                $mov->moveReference = "Anulación";
                $mov->idSales = $data['idl'];
                $mov->status= self::STATUS_ACTIVE;
                $mov->save();  
                foreach ($salDetail as $key => $a_item) {
                    if ($a_item['idProductDetail'] != '') {
                        //Con detalle 
                        $productdetail = ORM::factory('productdetail')->
                                        where('idProductDetail', '=', $a_item['idProductDetail'])
                                        ->find();
                        $productdetail->amount = $productdetail->amount + $a_item['quantity'];
                        $productdetail->save();
                        // Actualizo Stock 
                        $stock = ORM::factory('stock')->
                                        where('idProduct', '=', $productdetail->idProduct)
                                        ->and_where('idStore', '=', $a_item['idStore'])->find();
                        $stock->amount = $stock->amount + $a_item['quantity'];
                        $stock->save();
                        // Creo movimiento detalle                 
                        $mov_detail = new Model_Movedetail();
                        $mov_detail->idMove = $mov->idMove;
                        $mov_detail->idProductDetail = $a_item['idProductDetail'];
                        $mov_detail->amount = $a_item['quantity'];
                        $mov_detail->status = self::STATUS_ACTIVE;
                        $mov_detail->save();
                        
                    } 
                    else if ($a_item['idLot'] != '') {
                        // Actualizo el lote
                        $lote = ORM::factory('lot')
                                ->where('idLot', '=', $a_item['idLot'])->find();
                        $lote->amount = $lote->amount + $a_item['quantity'];
                        $lote->save();
                        // Actualizo el stock
                        $stock = ORM::factory('stock')
                                ->where('idProduct', '=', $lote->idProduct)
                                ->and_where('idStore', '=', $a_item['idStore'])->find();
                        $stock->amount = $stock->amount + $a_item['quantity'];
                        $stock->save();
                        // Creo movimiento detalle
                        $mov_detail = new Model_Movedetail();
                        $mov_detail->idMove = $mov->idMove;
                        $mov_detail->idLot = $a_item['idLot'];
                        $mov_detail->amount = $a_item['quantity'];
                        $mov_detail->status = self::STATUS_ACTIVE;
                        $mov_detail->save();
                    }
                    $mov->idStore = $a_item['idStore'];
                    $mov->save();                    
                }
                Database::instance()->commit();
                $aResponse['idSales'] = $sales->idSales;
                $aResponse['sales'] = array('document' => $sales->documentType, 'totalPrice' => $sales->totalPrice, 'serie' => $sales->serie, 'correlative' => $sales->correlative);
            }
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }
    /* CREAR NOTA DE CREDITO */
    public function action_createCreditNote() {
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('creditnote_idSale', 'not_empty')
                    ->rule('creditnote_date', 'not_empty')
                    ->rule('creditnote_description', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                Database::instance()->begin();
                $data = $post->data();
                $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
                $dateCancel = DateTime::createFromFormat($format, $data['creditnote_date']);
                $user = $this->getSessionParameter(self::USER_ID);
                $idCashBox = DB::select(
                                array('ca.idCashBox','idCashBox')
                                )
                         ->from(array('rms_cashbox','ca'))
                         ->where('ca.status','=',self::CASHBOX_STATUS_OPENED)
                         ->and_where(DB::expr("DATE_FORMAT(ca.openDate,'%Y-%m-%d')"),'=',DateHelper::getfechaActual())
                         ->and_where('ca.idUser','=',$user)
                         ->execute()->current();
          
                $o_creditNote = new Model_Creditnote();
                $o_creditNote->idSales = $data['creditnote_idSale'];
                $o_creditNote->amountSale = $data['creditnote_amountSale'];
                $o_creditNote->description = $data['creditnote_description'];
                $o_creditNote->cancelDate = DateHelper::fnPHPDate2MySQLDatetime($dateCancel);
                $o_creditNote->status = self::CREDITNOTE_CREATE;
                $o_creditNote->idCashBox = $idCashBox;
                $o_creditNote->save();
                
                

                // Actualizo la venta 
                $o_sale = new Model_Sales($data['creditnote_idSale']);
                $o_sale->payback = 1;
                $o_sale->salesType = self::SALES_CREDIT_NOTE;
                $o_sale->idCashBox = $idCashBox;
                $o_sale->save();
                
                Database::instance()->commit();
                $a_response['data'] = $o_creditNote->idCreditNote;
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response, 'json');
    }
    
    
    /* BUSCAR DETALLE DE VENTA */
    public function action_getSalesDetailByID() {
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();
                $salesDetail = DB::select(
                                'sad.idSalesDetail',
                                array(db::expr('CASE WHEN sad.idLot IS NOT NULL THEN sad.idLot ELSE pd.idProductDetail END'),'idDetail'),
                                'po.haveDetail','sad.idStore','po.idProduct', 
                                array(DB::expr("CONCAT(UPPER(po.aliasProduct), ' - ',UPPER (po.productName))"), 'product'),'sad.quantity','sad.discount', 'sad.unitPrice', 
                                'sad.totalPrice', 'st.amount')
                                ->from(array('rms_salesdetail', 'sad'))
                                ->join(array('rms_lot', 'lot'), 'left')->on('sad.idLot', '=', 'lot.idLot')
                                ->join(array('rms_product_detail', 'pd'), 'left')->on('sad.idProductDetail', '=', 'pd.idProductDetail')
                                ->join(array('rms_products', 'po'))
                                ->on('po.idProduct', '=', DB::expr('lot.idProduct OR po.idProduct = pd.idProduct'))
                                ->join(array('rms_stock','st'))->on('po.idProduct','=','st.idProduct')
                                ->where('sad.idSalesDetail', '=', $data_salesDetail['id'])
                                ->and_where('st.idStore', '=', DB::expr('sad.idStore'))
                                ->as_object()->execute()->current();
         
           //    echo Database::instance()->last_query; die();

                $a_response['data'] = $salesDetail;
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    /* ADQUIRIR PRODUCTO */
    
    public function action_getProduct() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $idStore = $this->request->post('idStore');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
            );            
            $o_select_product = DB::select(
                                    'p.idProduct'
                                    , 'stor.idStore'
                                    , array(DB::expr("CONCAT(p.productName,' - ', rmb.brandName, ' ',p.aliasProduct,' (',st.Amount,') - ',stor.storeName)"), 'product')
                                    , 'st.Amount'
                                    , 'p.haveDetail'
                                    , 'p.productPriceSell'
                            )
                            ->from(array('rms_stock', 'st'))
                            ->join(array('rms_products', 'p'))->on('st.idProduct', '=', 'p.idProduct')
                            ->join(array('rms_store', 'stor'))->on('st.idStore', '=', 'stor.idStore')
                            ->join(array('rms_brands', 'rmb'))->on('p.idBrand', '=', 'rmb.idBrand')
                            ->where(DB::expr("CONCAT(p.productName,' ',p.aliasProduct)"), 'LIKE', ':search')
                            ->and_where('p.status', '=', self::STATUS_ACTIVE)
                            ->and_where('stor.idStore', '=', $idStore)
                            ->group_by('p.aliasProduct')
                            ->group_by('st.idStore')
                            ->parameters($parameters)
                            ->execute()->as_array();

            $a_response['data'] = $o_select_product;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
        
    }
    /* ADQUIRIR PRODUCTO DETALLE */
    public function action_getDetailByProductId() {
        $a_response = $this->json_array_return;
        try {
            $id_product = $this->request->post('id');
            $idStore = $this->request->post('idStore');
            $query_data_not_show = 'SELECT rpd.idProductDetail,rp.*,rpd.* FROM rms_products rp ' .
                        'INNER JOIN rms_product_detail rpd ON rp.idProduct=rpd.idProduct ' .
                        'INNER JOIN rms_movedetail mvdi ON mvdi.idProductDetail=rpd.idProductDetail ' .
                        'INNER JOIN rms_move mv ON mvdi.idMove=mv.idMove ' .
                        'WHERE rp.idProduct=' . $id_product . '  AND mv.idStore=' . $idStore . ' AND rpd.amount>0 ' .
                        'GROUP BY rpd.idProductDetail HAVING MOD(COUNT(rpd.idProductDetail),2)<>0 ';
            $rs = DB::query(Database::SELECT, $query_data_not_show)->execute()->as_array();
            $a_response['data'] = $rs;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    /* ADQUIRIR LOTE */
    public function action_getProductLotById() {
        $a_response = $this->json_array_return;
        try {
            $id_product = $this->request->post('id');
            $idStore = $this->request->post('idStore');

            $query_pd = "SELECT   rpd.idLot AS idLot,  rp.idProduct AS idProduct,
                rp.productName AS productName,  rpd.lotName AS lotName,  mvdi.unitPrice AS unitPrice,
                rpd.amount AS amount,  rpd.description AS description, rp.haveDetail , mv.idStore ,
                DATE_FORMAT(    rpd.creationDate,    '%d/%m/%Y %h:%i %p'  ) AS creationDate 
                FROM  rms_products rp 
                INNER JOIN rms_lot rpd     ON rp.idProduct = rpd.idProduct 
                INNER JOIN rms_movedetail mvdi     ON mvdi.idLot = rpd.idLot 
                INNER JOIN rms_move mv     ON mvdi.idMove = mv.idMove 
                WHERE rp.idProduct =$id_product   AND mv.idStore =$idStore  
                    AND rpd.amount <> 0   
                    AND mv.typeMoveStore <> 'VENTA_DIRECTA'   
                    AND mv.typeMoveStore <> 'TRAS_SALIDA'   
                    AND mv.typeMoveStore <> 'SALIDA' 
                GROUP BY rpd.idLot ORDER BY creationDate ASC ";

            $rs = DB::query(Database::SELECT, $query_pd)->execute()->as_array();

           //echo Database::instance()->last_query; die();
            $a_response['data'] = $rs;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    /* EDITAR DETALLE DE LA VENTA */ 
    public function action_editSalesDetail() {
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('discount', 'not_empty')
                    ->rule('quantity', 'not_empty')
                    ->rule('idDetail', 'not_empty')             
            ;
            if (!$post_idSalesDetail->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                Database::instance()->begin();
                $data = $post_idSalesDetail->data();
                $Optionbarcode = $this->getOptionValue(self::INVENTORY_BARCODE_READER);
                $o_model_salesDetail = new Model_Salesdetail($data['idSalesDetail']);
                $o_model_sales = new Model_Sales($o_model_salesDetail->idSales);

                    $quantity  =$o_model_salesDetail->quantity;
                    $o_model_salesDetail->quantity = $data['quantity'];
                    $o_model_salesDetail->unitPrice = $data['custom_price'];
                    $o_model_salesDetail->discount = $data['discount'];
                    // Modifico la venta 
                    $totalPrice = ($o_model_sales->totalPrice - $o_model_salesDetail->totalPrice) + $data['total_price'];
                    $o_model_sales->totalPrice =  $totalPrice;
                    $o_model_sales->returnCash = $o_model_sales->paymentAmount  - $totalPrice;

                    $o_model_salesDetail->totalPrice = $data['total_price'];

                    /* Guardar Log */

                    if($data['haveDetail'] == 0){
                        if($data['quantity'] != $quantity){
                              $dif = $data['quantity_lot'];
                              if ($dif > 0) { // La cantidad es positiva 
                                 //Creo un movimiento de salida para la diferencia                                     
                                 $a_lots = DB::select('amount', 'idLot')->from('rms_lot')
                                           ->where('idProduct','=', $data['idProduct'])
                                           ->and_where('idStore','=', $data['idStoreDetail'])
                                           ->execute()->as_array();
                                 $temp_dif =  $data['quantity_lot'];
                                 foreach( $a_lots AS $item){
                                     if($item['amount']>= $temp_dif){
                                         // Descuento en solo ese lote
                                         $o_model_lot = new Model_Lot($item['idLot']);
                                         $o_model_lot->amount =  $o_model_lot->amount - $temp_dif;
                                         $o_model_lot->save();
                                         break;
                                     }else{
                                         $temp_dif = $temp_dif - $item['amount'];
                                         $o_model_lot = new Model_Lot($item['idLot']);
                                         $o_model_lot->amount = 0;
                                         $o_model_lot->save();
                                     }
                                     $mov = new Model_Move();
                                     $mov->typeMoveStore = self::TYPE_MOVE_VENTA_DIRECTA;
                                     $mov->moveReference = 'Venta Editada';
                                     $mov->idStore = $data['idStoreDetail'];
                                     $mov->status = self::STATUS_ACTIVE;
                                     $mov->idSales = $o_model_salesDetail->idSales;
                                     $mov->save();

                                     $movDetail = new Model_Movedetail();
                                     $movDetail->idMove = $mov->idMove;
                                     $movDetail->idLot = $item['idLot'];
                                     $movDetail->amount = $temp_dif; // lo que venderé mas 
                                     $movDetail->unitPrice = $data['custom_price'];
                                     $movDetail->status = self::STATUS_ACTIVE;
                                     $movDetail->save();     
                                 }   
                             } 
                              else if ($dif < 0) {
                                 //Creo un movimiento de entrada para el producto                        
                                 $mov = new Model_Move();
                                 $mov->typeMoveStore = self::TYPE_MOVE_INGRESO;
                                 $mov->moveReference = 'Venta Editada';
                                 $mov->idStore = $data['idstore'];
                                 $mov->status = self::STATUS_ACTIVE;
                                 $mov->idSales = $o_model_salesDetail->idSales;
                                 $mov->save();

                                 $movDetail = new Model_Movedetail();
                                 $movDetail->idMove = $mov->idMove;
                                 $movDetail->idLot = $data['idDetail'];
                                 $movDetail->amount = abs($dif); // lo que venderé menos
                                 $movDetail->unitPrice = $data['custom_price'];
                                 $movDetail->status = self::STATUS_ACTIVE;
                                 $movDetail->save();

                                 // En el lote 
                                 $lot = new Model_Lot($data['idDetail']);
                                 $lot->amount = $lot->amount - $dif;
                                 $lot->save();
                         }
                         //Actualizo el stock 
                         $obj_stock = ORM::factory('stock')
                                 ->where('idProduct', '=', $data['idProduct'])
                                 ->and_where('idStore', '=', $data['idstore'])
                                 ->find();
                         $stock = new Model_Stock($obj_stock->idStock);
                         $stock->amount = $stock->amount - $data['quantity_lot'];
                         $stock->save();
                         // Cuando es igual ---  solo modifico el detalle de venta que seria el
                         // descuento, precio, y el total
                        }
                    }else{
                        // Cuando tiene detalle siempre es 1 y no hay que hacer movimientos.
                    }
                    $o_model_salesDetail->save();
                    $o_model_sales->save();

             Database::instance()->commit();
             $a_response['data'] = $o_model_salesDetail->idSalesDetail;
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    
    public function action_getSerieCorrelativeByTerm() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%'
            );
            $o_select_serie_correlative = DB::select(
                                    array('s.idSales', 'id'), array(DB::expr("CONCAT(s.serie, ' - ', s.correlative)"), 'name')
                            )
                            ->from(array('rms_sales', 's'))
                            ->where(DB::expr("CONCAT(s.serie, '-', s.correlative)"), 'LIKE', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $aResponse['data'] = $o_select_serie_correlative;
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }
    public function action_getCreditNoteByIdSale() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $s_search = $this->request->post('idSale');
            $parameters = array(
                ':search' => $s_search
            );
            $o_creditNote = DB::select(
                   
                                    ('cn.*'),db::expr('DATE_FORMAT(cn.cancelDate,"%d/%m/%Y") AS date_canceled'))
                            
                            ->from(array('rms_credit_note', 'cn'))
                            ->where('cn.idSales', '=', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $aResponse['data'] = $o_creditNote;
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    // Cancelar un Detalle de la Venta
    public function action_deleteSalesDetail() {
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('idsd', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();

                // el id de sales
                Database::instance()->begin();
                //Actualizar el Detalle de la Venta
                $salesDetail = New Model_Salesdetail($data_salesDetail['idsd']);
                $salesDetail->statusDetail = self::SALES_DETAIL_CANCELED;
                //Modificar la Venta
                $ids = $salesDetail->idSales;
                $sales = new Model_Sales($ids);
                $newTotal = $sales->totalPrice - $salesDetail->totalPrice;
                $sales->totalPrice = $newTotal;
                $sales->returnCash = $sales->returnCash - ($sales->paymentAmount - $newTotal);

                if ($salesDetail->idProductDetail != '') {
                    $productdetail = ORM::factory('productdetail')->
                                    where('idProductDetail', '=', $salesDetail->idProductDetail)->find();
                    
                    $productdetail->amount = 1;
                    $productdetail->save();
                    //Actualizar Stock
                    $stock = ORM::factory('stock')->
                                    where('idProduct', '=', $productdetail->idProduct)
                                    ->and_where('idStore', '=', $salesDetail->idStore)->find();
                    $stock->amount = $stock->amount + 1;
                    $stock->save();
                    
                    /* Creo movimiento de entradas */
                    $move = new Model_Move();
                    $move->typeMoveStore = self::TYPE_MOVE_VENTA_ANULACION;
                    $move->moveReference = 'Eliminacion Detalle';
                    $move->idStore =  $salesDetail->idStore;
                    $move->status = self::STATUS_ACTIVE;
                    $move->idSales =  $salesDetail->idSales;
                    $move->save();
                    
                    /* Creo detalle de movimiento */
                    $movedetail = new Model_Movedetail();
                    $movedetail->idMove = $move->idMove;
                    $movedetail->idProductDetail = $salesDetail->idProductDetail;
                    $movedetail->amount = 1; 
                    $movedetail->status = self::STATUS_ACTIVE;
                    $movedetail->save();
                    
                    

                } else if ($salesDetail->idLot != '') {

                    $lote = ORM::factory('lot')->
                                    where('idLot', '=', $salesDetail->idLot)->find();
                    $lote->amount = $lote->amount + $salesDetail->quantity;
                    $lote->save();
                    //Actualizar Stock
                    $stock = ORM::factory('stock')
                                    ->where('idProduct', '=', $lote->idProduct)
                                    ->and_where('idStore', '=', $salesDetail->idStore)->find();
                    $stock->amount = $stock->amount + $salesDetail->quantity;
                    $stock->save();
                    
                    /* Creo movimiento de entradas */
                    $move = new Model_Move();
                    $move->typeMoveStore = self::TYPE_MOVE_VENTA_ANULACION;
                    $move->moveReference = 'Eliminacion Detalle';
                    $move->idStore =  $salesDetail->idStore;
                    $move->status = self::STATUS_ACTIVE;
                    $move->idSales =  $salesDetail->idSales;
                    $move->save();
                    
                    /* Creo detalle de movimiento */
                    $movedetail = new Model_Movedetail();
                    $movedetail->idMove = $move->idMove;
                    $movedetail->idLot = $salesDetail->idLot;
                    $movedetail->amount =  $salesDetail->quantity; 
                    $movedetail->status = self::STATUS_ACTIVE;
                    $movedetail->save();
                }
                $salesDetail->save();
                $sales->save();
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }



    public function action_editSales(){ 
        $a_response = $this->json_array_return;
        try{
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('idSales', 'not_empty')
                    ->rule('idCustomers', 'not_empty')
                    ->rule('chars_Customers', 'not_empty')
                    ->rule('dateSales', 'not_empty');
//                    ->rule('cmb_listUserCode', 'not_empty');
//                    ->rule('cmb_listUserCode-combobox', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();
                
                $idCustomers = $data_salesDetail['idCustomers'];
                $date = $data_salesDetail['dateSales'];
                $idUserCode =$this->request->post('cmb_listUserCode');
                
                /* DIA DE VENTA */
                $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
                $dateSale = DateTime::createFromFormat($format, $date);
               
                $id = $data_salesDetail['idSales'];
                
                $obj_sale = new Model_Sales($id);
                $obj_sale->idCustomers = $idCustomers;
                $new_date = date_create_from_format("d/m/Y", $date);
                $obj_sale->salesDate = $new_date->format('Y-m-d H:i:s');
                $obj_sale->idUserCode = $idUserCode;
                $obj_sale->save();
                
            }else{
                throw new Exception("no esta haciendo nada",  self::CODE_SUCCESS);
            } 
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
        
    }
    public function action_getClients() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':typeperson' => self::CLIENT_TYPE_PERSON,
                ':typecompany' => self::CLIENT_TYPE_COMPANY
            );
            $o_select_company = DB::select(
                                    'cus.idCustomers', array(DB::expr("CONCAT(IF(cus.idCustomersCompany IS NULL,pe.fullName, cuscom.name) ,' - ', IF(cus.idCustomersCompany IS NULL,pe.document, cuscom.code))"), 'customers'), array(DB::expr("IF(cus.idCustomersCompany IS NULL,:typeperson, :typecompany)"), 'type')
                            )
                            ->from(array('rms_customers', 'cus'))
                            ->join(array('bts_person', 'pe'), 'LEFT')->on('cus.idPerson', '=', 'pe.idPerson')
                            ->join(array('rms_customerscompany', 'cuscom'), 'LEFT')->on('cus.idCustomersCompany', '=', 'cuscom.idCustomersCompany')
                            ->where(DB::expr("CONCAT(pe.fullName, pe.document)"), 'LIKE', ':search')
                            ->or_where(DB::expr("CONCAT(cuscom.name, cuscom.code)"), 'LIKE', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $a_response['data'] = $o_select_company;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
    }
        $this->fnResponseFormat($a_response);
        die();
    }
    
    
    public function action_getSalesById(){
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();
                
                $a_array_sql=array(
                        array('rs.idSales', 'idSales'), 
                        array('rs.idCustomers','idCustomers'),
                        array('pe.fullName','nameCustomers'),
                        array(db::expr('DATE_FORMAT(rs.salesDate,"%d/%m/%Y")'),'date'),
                        array('rs.idUserCode','idUsercode')
                );
                if($this->getOptionValue(self::SO_ACTIVE_SELLER)){
                    array_push($a_array_sql,array('us.userName','vendedor'));
                }
                $salesDetail = DB::select_array(
                        $a_array_sql
                        ) ->from(array('rms_sales', 'rs'))
                        ->join(array('rms_customers','cu'))->on('rs.idCustomers','=','cu.idCustomers');

                if($this->getOptionValue(self::SO_ACTIVE_SELLER)){
                    $salesDetail->join(array('bts_user','us'))->on('us.idUser','=','rs.idUserCode');
                }
                
                              $salesDetail= $salesDetail->join(array('bts_person','pe'))->on('cu.idPerson',' =','pe.idPerson')
                                ->where('rs.idSales', '=',$data_salesDetail['id'])
                                ->as_object()->execute()->current();

//                  echo Database::instance()->last_query; die();

                $a_response['data'] = $salesDetail;
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>
