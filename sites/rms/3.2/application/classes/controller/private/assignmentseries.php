<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Assignmentseries extends Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('assignmentseries', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('assignmentseries', self::FILE_TYPE_JS));
        $assignmentSeries_view = new View('series/assignmentseries');
        $oCity = new Model_City();
        
        $aCity = $oCity->select('city.idCity', 'city.name')
                ->where('city.status', '=', self::STATUS_ACTIVE)
                ->order_by('city.name')
                ->find_all();
        $aDocuments = $this->getDocumentsByFilter();
        $oOffice = new Model_Office();
        $aOffice = $oOffice->select('office.idOffice', 'office.name')
                ->where('office.state', '=', self::STATUS_ACTIVE)
                ->order_by('office.name')
                ->find_all();
        $assignmentSeries_view->aOffice = $aOffice;
        $assignmentSeries_view->aCity = $aCity;
        $assignmentSeries_view->adocuments = $aDocuments;
        $this->template->content = $assignmentSeries_view;
    }

    public function action_createorUpdateAssignmentSeries() {
        $a_response = $this->json_array_return;
        try {
            $idAssignmentSeries=$this->request->post('idAssignmentSeries');
            $o_post_idAssignmentSeries = Validation::factory($this->request->post())
                    ->rule('idAssignmentSeries', 'not_empty')
                    ->rule('idOffice', 'not_empty')
                    ->rule('documentType', 'not_empty')
                    ->rule('serie', 'not_empty')
                    ->rule('correlative', 'not_empty');
            if ($o_post_idAssignmentSeries->check()) {
                $data_post_idAssignmentSeries = $o_post_idAssignmentSeries->data();
                $aAssignmentSeries = new Model_Assignmentseries();
                if($idAssignmentSeries!=0){
                    $aAssignmentSeries= new Model_Assignmentseries($idAssignmentSeries);
                }
                $aAssignmentSeries->idOffice = $data_post_idAssignmentSeries['idOffice'];
                $aAssignmentSeries->serie = $data_post_idAssignmentSeries['serie'];
                $aAssignmentSeries->correlative = $data_post_idAssignmentSeries['correlative'];
                $aAssignmentSeries->documentType = $data_post_idAssignmentSeries['documentType'];
                $aAssignmentSeries->state = self::STATUS_ACTIVE;
                $aAssignmentSeries->type = 1;
                $aAssignmentSeries->save();
           }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listAssignmentSeries() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'id' => "rms_series.idAssignmentSeries",
                'documentType' => "rms_series.documentType",
                'serie' => 'rms_series.serie',
                'correlative' => 'rms_series.correlative',
                'maxLimit' => 'rms_series.sequence_max_value',
                'office' => "o.name",
                'city' => "c.name"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_series ' .
                    'INNER JOIN bts_office o ON o.idOffice = rms_series.idOffice ' .
                    'INNER JOIN bts_city c ON c.idCity = o.idCity ';

            $where_conditions = "rms_series.state = 1 and o.state = 1 ";
            if (isset($_REQUEST['selectOffice'])) {
                $where_conditions .= "AND o.idOffice='" . $_REQUEST['selectOffice'] . "'";
            }
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

    public function action_getAssignmentSeriesById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_idAssignmentSeries = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($o_post_idAssignmentSeries->check()) {
                $data_post_idAssignmentSeries = $o_post_idAssignmentSeries->data();
                $aAssignmentSeries = new Model_Assignmentseries($data_post_idAssignmentSeries['ids']);
                $aOffice= DB::select('bts_office.idCity')
                        ->from('bts_office')
                        ->where('bts_office.idOffice','=',$aAssignmentSeries->idOffice)
                        ->execute()->as_array();
                
                $a_response['data'] = array_merge($aAssignmentSeries->as_array(), $aOffice);
            } 
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
        public function action_findAssignmentSeriesById() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $idAssignmentSeries = $_REQUEST['idas'];
            $aAssignmentSeries = array();
            $oAssignmentSeries = ORM::factory('AssignmentSeries', $idAssignmentSeries);
            $oOffice = DB::select('office.idCity')->from('office')->where('office.idOffice', '=', $oAssignmentSeries->idOffice)->execute()->as_array();
            $aAssignmentSeries = array_merge($aAssignmentSeries, $oAssignmentSeries->as_array());
            $aAssignmentSeries = array_merge($aAssignmentSeries, $oOffice[0]);
            $aResponse['data'] = $aAssignmentSeries;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->handlingError($exc);
        }
        $this->fnResponseFormat($aResponse);
    }
    public function action_deleteAssignmentSeries() {
        $a_response = $this->json_array_return;
        try {
            $o_post_idAssignmentSeries = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($o_post_idAssignmentSeries->check()) {
                $data_post_idAssignmentSeries = $o_post_idAssignmentSeries->data();
                $aAssignmentSeries = new Model_Assignmentseries($data_post_idAssignmentSeries['ids']);
                $aAssignmentSeries->state = self::STATUS_DEACTIVE;
                $aAssignmentSeries->save();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}