<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of billingpurchase
 *
 * @author Escritorio
 */
class Controller_Private_Billingpurchase extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('billingpurchase', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('billingpurchase', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $billingpurchase_view = new View('purchase/billingpurchase');
        $billingpurchase_view->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $billingpurchase_view->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $billingpurchase_view->a_documents_type = $this->getDocumentsByFilter('sales', true);
        $billingpurchase_view->a_currency = $this->a_TYPE_CURRENCY();
        $billingpurchase_view->a_purchaseorder_payment = $this->fnGetClassVariable(self::VAR_NAME_PURCHASEORDER_PAYMENT);
        $billingpurchase_view->a_taxEnabled = $this->getOptionValue(self::SO_TAX_ENABLED);
        $billingpurchase_view->a_taxName = $this->getOptionValue(self::SO_TAX_NAME);
        $billingpurchase_view->a_tax = $this->getOptionValue(self::SO_TAX_VALUE);
        $this->template->content = $billingpurchase_view;
    }

    public function action_getPurchaseOrder() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $o_post_id = Validation::factory($this->request->post())
                    ->rule('idPurchaseOrder', 'not_empty')
                    ->rule('typeSearch', 'not_empty')
            ;
            if ($o_post_id->check()) {
                $data_post_id = $o_post_id->data();

                $parameter = array(
                    ':idPurchaseOrder' => $data_post_id['idPurchaseOrder'],
                    ':idCodeOrder' => $data_post_id['typeSearch'],
                    ':active' => self::PURCHASE_ORDER_STATUS_ACTIVE,
                    ':purchase_order_recepcionado' => self::PURCHASE_ORDER_STATUS_RECEPCIONADO
                );
                $data = DB::select(
                                        array(DB::expr("CONCAT(s.name,' - ',s.code)"), 'supplier')
                                        , 'po.idPurchaseOrder'
                                        , 'po.idSupplier'
                                        , 'pod.idPurchaseOrderDetail'
                                        , 'pod.idPurchaseOrder'
                                        , 'pod.idProduct'
                                        , array(DB::expr("CONCAT(p.productName,' - ',p.aliasProduct)"), 'description')
                                        , 'p.productUnitMeasures'
                                        , 'pod.quantity'
                                        , 'pod.totalUnit'
                                        , 'pod.totalAmount'
                                )
                                ->from(array('rms_purchaseorder', 'po'))
                                ->join(array('rms_purchaseorderdetail', 'pod'))->on('po.idPurchaseOrder', '=', 'pod.idPurchaseOrder')
                                ->join(array('rms_supplier', 's'))->on('s.idSupplier', '=', 'po.idSupplier')
                                ->join(array('rms_products', 'p'))->on('p.idProduct', '=', 'pod.idProduct');

                if ($data_post_id['typeSearch'] == 'purchaseorder') {
                    $data->where('po.nrodocument', '=', trim(':idPurchaseOrder'));
                } else {
                    $data->where('po.codePurchaseOrder', '=', trim(':idPurchaseOrder'));
                }
                $data = $data->and_where('po.statusPurchase', 'IN', DB::expr('(:purchase_order_recepcionado)'))
                                ->and_where('pod.status', '=', ':active')
                                ->parameters($parameter)
                                ->execute()->as_array();
//print_r(Database::instance()->last_query);
//die();
                $a_response['data'] = $data;
            } else {
                throw new Exception('Debe ingresar el Nro de Documento, para iniciar la búsqueda.', self::CODE_SUCCESS);
            }
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createPurchase() {
        $a_response = $this->json_array_return;
        try {
            $post_purchase = Validation::factory($this->request->post())
                    ->rule('headerPurchase', 'not_empty')
                    ->rule('detailPurchase', 'not_empty');
            if (!$post_purchase->check()) {
                throw new Exception("Existen Datos Faltantes", self::CODE_SUCCESS);
            }

            $a_headerPurchase = $this->request->post('headerPurchase');
            $a_detailPurchase = $this->request->post('detailPurchase');
            
            Database::instance()->begin();
            
            $id_user = $this->getSessionParameter(self::USER_ID);
            $sum_totalAmount = 0;
            $sum_totalAmountWithTax = 0;
            $sum_totalAmountWithOutTax = 0;
            $sum_totalTax = 0;
            $datePurchase = date('Y-m-d H:i:s');

            $arrayPurchase = array();

            if (count($arrayPurchase) > 0) {
                $a_response['data'] = $arrayPurchase;
            } else {
                $a_response['data'] = $arrayPurchase;

                $purchase = new Model_Purchase();
                $purchase->idPurchaseOrder = $a_headerPurchase['idPurchaseOrder'];
                $purchase->idUser = $id_user;
                $purchase->currency = $a_headerPurchase['currency'];
                $purchase->paymentType = $a_headerPurchase['payment_type'];
                $purchase->documentType = $a_headerPurchase['document_type'];
                $purchase->serie = $a_headerPurchase['serie'];
                $purchase->correlative = $a_headerPurchase['correlative'];
                $purchase->purchaseStatus = self::PURCHASE_ORDER_STATUS_FACTURADO;
                $purchase->datePurchase = $datePurchase;
                
                if($a_headerPurchase['idPurchaseOrder']!=0){
                    $purchase_order = new Model_Purchaseorder($a_headerPurchase['idPurchaseOrder']);
                    $purchase_order -> statusPurchase = self::PURCHASE_ORDER_STATUS_FACTURADO;
                    $purchase_order -> save();
                }
                if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                    $purchase->taxPercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                } else {
                    $purchase->taxPercentage = 0;
                }

                if ($a_headerPurchase['document_type'] == 'FACTURA') {
                    foreach ($a_detailPurchase as $key => $itemDetail) {
                        $sum_totalAmount = $sum_totalAmountWithTax + $itemDetail['totalAmount'];
                        $sum_totalTax = $sum_totalAmount * 0.18;
                        $sum_totalAmountWithOutTax = 0;
                        $sum_totalAmountWithTax =  $sum_totalAmount - $sum_totalTax;
                    }
                } else {
                    foreach ($a_detailPurchase as $key => $itemDetail) {
                        $sum_totalAmountWithOutTax = $sum_totalAmountWithOutTax + $itemDetail['totalAmount'];
                        $sum_totalAmountWithTax =0;
                        $sum_totalTax = 0;
                        $sum_totalAmount = $sum_totalAmountWithOutTax + $sum_totalTax;
                    }
                }
                $purchase->totalAmount = $sum_totalAmount;
                $purchase->totalAmountWithOutTax = $sum_totalAmountWithOutTax;
                $purchase->totalAmountWithTax = $sum_totalAmountWithTax;
                $purchase->totalTax = $sum_totalTax;
                $purchase->save();
                
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>