<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of product
 *
 * @author Yime
 */
class Controller_Private_Products extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('products', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('products', self::FILE_TYPE_JS));

        $inventory_products = new View('inventory/products');
        $inventory_products->bTagGenerator = $this->getOptionValue(self::INVENTORY_TAG_GENERATOR);

        $brand = DB::select(array('idBrand', 'id'), array('brandName', 'name'))
                        ->from('rms_brands')
                        ->execute()->as_array();
        $category = DB::select(array('idCategory', 'id'), array('categoryName', 'name'), array('codigo', 'codigo'))
                        ->from('rms_category')
                        ->where('idSuperCategory', 'IS', NULL)
                        ->and_where('status', '=', self::STATUS_ACTIVE)
                        ->execute()->as_array();
        $correlative = DB::select(array('idCategory', 'id'), array('categoryName', 'name'), array('codigo', 'codigo'))
                        ->from('rms_category')
                        ->where('idSuperCategory', 'IS', NULL)
                        ->and_where('status', '=', self::STATUS_ACTIVE)
                        ->execute()->as_array();

        //echo Database::instance()->last_query; die();
        $currency = $this->a_TYPE_CURRENCY();
        $measures = $this->a_UNIT_MEASURES();
        $inventory_products->brand = $brand;
        $inventory_products->category = $category;
        $inventory_products->currency = $currency;
        $inventory_products->measures = $measures;
        $this->template->content = $inventory_products;
    }

    public function action_listAllProducts() {
        $this->auto_render = FALSE;
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $measures = $this->a_UNIT_MEASURES();
            $case_when_measures = "CASE ";
            foreach ($measures as $v) {
                $case_when_measures = $case_when_measures . " 
                    WHEN p.productUnitMeasures='" . $v['value'] . "' 
                        THEN '" . $v['display'] . "'
                    ";
            }
            $case_when_measures = $case_when_measures . " ELSE '" . __("No especificado") . "' END";

            $a_array_cols = array(
                "id" => "p.idProduct",
                "aliasProduct" => "p.aliasProduct",
                "productName" => "p.productName",
                "brandNameCommercial" => "b.brandNameCommercial",
                "categoryName" => "c.categoryName",
                "productUnitMeasures" => $case_when_measures,
                "haveDetail" => "CASE WHEN p.haveDetail=1 THEN 'SI' ELSE 'NO' END",
                "productStock" => "s.amount",
                "storeName" => "st.storeName",
                "productPriceSell" => "p.productPriceSell",
                "userCreate" => "pe.fullName",
                "status" => "p.status",
                "creationDate" => "DATE_FORMAT(p.creationDate,'$s_date_format $s_time_format')"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);

            $s_tables_join = 'rms_products p ' .
                    'INNER JOIN rms_brands b ON p.idBrand=b.idBrand ' .
                    'INNER JOIN rms_category c ON p.idCategory=c.idCategory ' .
                    'INNER JOIN bts_user u ON u.idUser=p.userCreate ' .
                    'INNER JOIN bts_person pe ON pe.idPerson=u.idUser ' .
                    'LEFT JOIN rms_stock s ON p.idProduct= s.idProduct ' .
                    'LEFT JOIN rms_store st ON s.idStore= st.idStore ' ;

            $s_where_conditions = "1=1 AND p.status=" . self::STATUS_ACTIVE;
                    //" AND st.status=" . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);
            
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
//            echo Database::instance()->last_query;
//            die();
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

    public function action_deleteProduct() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_product = Validation::factory($this->request->post())
                    ->rule('idp', 'not_empty');
            if ($o_post_id_product->check()) {
                $data_post_id_product = $o_post_id_product->data();
                $obj_product = new Model_Products($data_post_id_product['idp']);
                $obj_product->status = self::STATUS_DEACTIVE;
                $obj_product->save();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listByIdProduct() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_product = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($o_post_id_product->check()) {
                $data_post_id_product = $o_post_id_product->data();
                $obj_product = new Model_Products($data_post_id_product['id']);
                $a_response['data'] = $obj_product->as_array();
                // Verifico aqui las categorias 
                $obj_category = new Model_Category($obj_product->idCategory);
                $obj_category->as_array();
                //print_r($obj_category->as_array());
                if($obj_category->idSuperCategory == null){
                   $a_response['category'] = array(
                                              'idCategory'=> $obj_category->idCategory,
                                              'idSuperCategory'=> null
                                            );
                }else{
                   //$obj_categoryPadre = new Model_Category($obj_category->idSuperCategory);
                   $a_response['category']= array(
                                              'idCategory'=> $obj_category->idSuperCategory,
                                              'idSuperCategory'=> $obj_category->idCategory
                                               
                                            ); 
                }
                
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createOrUpdateProduct() {
        $a_response = $this->json_array_return;
        try {
            $aliasProduct = $this->request->post('aliasProduct');
            $idProduct = $this->request->post('idProduct');
            $haveDetail = $this->request->post('haveDetail');
            $productName = $this->request->post('productName');
            $th_aliasProduct = $this->request->post('th_aliasProduct');
            
            $tmp = DB::select(db::expr('COUNT(rms_products.idProduct) AS c'))
                  ->from('rms_products')
                  ->where('rms_products.status', '=', self::STATUS_ACTIVE);
            $optionGenerator = $this->getOptionValue(self::INVENTORY_TAG_GENERATOR);
            
            if( $optionGenerator == '0'){
              $tmp = $tmp->and_where('rms_products.aliasProduct', 'LIKE', $aliasProduct)
                         ->and_where('rms_products.productName', 'LIKE', $productName);
            }else {
              $tmp = $tmp->and_where('rms_products.aliasProduct', 'LIKE', $aliasProduct); 
            }            
            
            if ($idProduct == 0) {
              //  echo 'new'; die();
                $obj_product = new Model_Products();
                
            } else {
                $obj_product = new Model_Products($idProduct);
                $tmp = $tmp->and_where('rms_products.idProduct', "<>", $idProduct);
            }
            $tmp = $tmp->as_object()->execute()->current();

            if ($tmp->c > 0) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = __("El codigo del producto ya se encuentra registrado");
            } else {

                $o_post_product = Validation::factory($this->request->post())
                        ->rule('productName', 'not_empty')
                        ->rule('idbrand', 'not_empty')
                        ->rule('idCategory', 'not_empty')
                        ->rule('unitMeasures', 'not_empty')
                        ->rule('productPriceSell', 'not_empty');
                if ($o_post_product->check()) {
                    $data_post_product = $o_post_product->data();
                    $obj_product = new Model_Products();
                    
                    Database::instance()->begin();
                    
                    $a_returnProduct = array();
                    if ($idProduct != 0) {
                        $obj_product = new Model_Products($idProduct);
                    }

                    if ($haveDetail == 1) {
                        $obj_product->haveDetail = self::STATUS_ACTIVE;
                    } else {
                        $obj_product->haveDetail = self::STATUS_DEACTIVE;
                    }
                    
                    if ($data_post_product['idSubCategory']!="") {
                        $idCategory = $data_post_product['idSubCategory']; 
                        $have_subCategory = true;
                    } else {
                        $idCategory = $data_post_product['idCategory'];
                        $have_subCategory = false;
                    }

                    if ($this->getOptionValue(self::INVENTORY_TAG_GENERATOR)) {
                        $aliasProduct = trim($data_post_product['fir_aliasProduct']) . '' . trim($data_post_product['sec_aliasProduct']) . '' . trim($data_post_product['th_aliasProduct']);
                    } else {
                        $aliasProduct = $data_post_product['aliasProduct'];
                    }

                    $obj_product->aliasProduct = trim($aliasProduct);
                    $obj_product->productName = $data_post_product['productName'];
                    $obj_product->idBrand = $data_post_product['idbrand'];
                    $o_category_past = new Model_Category($obj_product->idCategory);
                    $obj_product->idCategory = $idCategory;
                    $obj_product->productUnitMeasures = $data_post_product['unitMeasures'];
                    if ($data_post_product['minimunStock'] != '') {
                        $obj_product->minimumstock = $data_post_product['minimunStock'];
                    }
                    if ($data_post_product['maximunStock'] != '') {
                        $obj_product->maximumstock = $data_post_product['maximunStock'];
                    }
                    $obj_product->productPriceSell = $data_post_product['productPriceSell'];
                    $obj_product->status = self::STATUS_ACTIVE;
                    
                    $o_category = new Model_Category($idCategory);
                    if($o_category->idSuperCategory == null){
                        $category_serie = $idCategory;
                    }else{
                        $category_serie = $o_category->idSuperCategory;
                    }
                    
                    if($have_subCategory == true){
                        if($o_category_past->idSuperCategory!= $o_category->idSuperCategory || $idProduct == 0)
                             $this->updateCorrelative($category_serie); 
                    }
                    else{
                        if($o_category_past->idCategory!= $idCategory || $idProduct == 0)
                             $this->updateCorrelative($category_serie);
                    }
//                    if($obj_product->idCategory!=$idCategory || $idProduct == 0)
//                    $this->updateCorrelative($category_serie);
                    
                    $obj_product->save();
                                
                    $a_returnProduct = array('idProduct' => $obj_product->idProduct,
                        'product' => $obj_product->productName, 'productUnitMeasures' => $obj_product->productUnitMeasures,
                        'codeProduct' => $aliasProduct, 'haveDetail' => $haveDetail, 'productPriceSell' => $obj_product->productPriceSell);
                    $a_response['data'] = $a_returnProduct;
                    Database::instance()->commit();
                } else {
                    throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
                }
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    function updateCorrelative($idCat) {
        
        $o_correlative = new Model_Categoryseries($idCat);
        $o_correlative->correlativeCategory = $o_correlative->correlativeCategory + 1;
        $o_correlative->save();
    }

    public function action_printDataProduct() {

        $cantidad = $this->request->query('cantidad');
        $id = $this->request->query('id');
        try {
            $view = new View('/printer/FF/GEN/CODBAR_PRODUCTO');
            $producto = new Model_Products($id);
            $view->producto = $producto;
            $view->cantidad = $cantidad;
            $this->template = $view;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
    }
    
}
