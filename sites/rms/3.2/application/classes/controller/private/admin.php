<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Admin extends Private_Admin implements Rms_Constants, Kohana_Uconstants {
    /* lista todas las oficinas */

    public function action_listOffices() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        $cols = array(
            array('of.idOffice', 'id'),
            array('of.name', 'value')
        );

        try {
            $city = DB::select_array($cols)
                    ->from(array('bts_office', 'of'))
                    ->where('of.status', '=', self::STATUS_ACTIVE)
                    ->order_by('of.name');

            $aResponse['data'] = $city->execute()->as_array();
            //$aResponse['SQL'] = Database::instance()->last_query;
        } catch (Exception $e) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['data'] = $e->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_getUsersGroupSellers() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {

            $ido = $this->request->post('ido');
            $idc = $this->request->post('idc');

            $db = DB::select(array('pe.fullName', 'name'), array('us.idUser', 'id'))
                    ->from(array('bts_user', 'us'))
                    ->join(array('bts_person', 'pe'))->on('us.idUser', '=', 'pe.idPerson')
                    ->join(array('bts_office', 'of'))->on('us.idOffice', '=', 'of.idOffice')
                    ->join(array('bts_city', 'ci'))->on('of.idCity', '=', 'ci.idCity')
                    ->where('us.status', '=', self::STATUS_ACTIVE)
                    ->order_by('pe.fullName');

            if ($idc != NULL AND is_array($idc)) {
                $db = $db->where('ci.idCity', 'IN', DB::expr("(" . implode(',', $idc) . ")"));
            } else {
                if ($idc != NULL) {
                    $db = $db->where('ci.idCity', '=', $idc);
                }
            }

            if ($ido != NULL AND is_array($ido)) {
                $db = $db->where('us.idOffice', 'IN', DB::expr("(" . implode(',', $ido) . ")"));
            } else {
                if ($ido != NULL) {
                    $db = $db->where('us.idOffice', '=', $ido);
                }
            }

            $aResponse['data'] = $db->execute()->as_array();
            $aResponse['SQL'] = Database::instance()->last_query;
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    public function action_getCashBox() {
        $this->getCashBox($this->request->post('openDate'), $this->request->post('idUser'));
    }

    public function getCashBox($date, $idUser = NULL, $date_end = NULL) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $format = $this->getDATE_FORMAT_FOR(self::PHP_DATE);
        $list = DateTime::createFromFormat($format, $date);

        try {
            $no_closed = __("No Cerrada");
            $date_format = $this->getDATE_FORMAT_FOR(self::SQL_DATE);
            $time_format = $this->getDATE_FORMAT_FOR(self::SQL_TIME);
            $time_limit = $this->getDATE_FORMAT_FOR(self::SQL_TIME_LIMIT);
            $time_format_raw = $this->getDATE_FORMAT_FOR(self::SQL_TIME_RAW);
            $cols = array(
                'rms_cashbox.idCashBox',
                db::expr("IF(rms_cashbox.closeDate IS NOT NULL,CONCAT(DATE_FORMAT(rms_cashbox.openDate,'$date_format $time_format'),' - ',DATE_FORMAT(rms_cashbox.closeDate,'$date_format $time_format')),CONCAT(DATE_FORMAT(rms_cashbox.openDate,'$date_format $time_format'),' - ','$no_closed')) cashBox")
            );
            $cashbox = DB::select_array($cols)
                    ->from('rms_cashbox');            
            if (is_null($date_end))
                $cashbox->where(db::expr("DATE_FORMAT(openDate,'$date_format')"), '=', $date);
            else
                $cashbox->where('openDate', 'BETWEEN', DB::expr("STR_TO_DATE('$date','$date_format') AND STR_TO_DATE('$date_end $time_limit','$date_format $time_format_raw')"));
            if (!is_null($idUser)) {
                $cashbox->and_where('rms_cashbox.idUser', '=', $idUser);
            }
            $cashbox = $cashbox->order_by('openDate')->execute()->as_array();
            if (count($cashbox) == 0) {                                
                $cashboxObj = new Model_Cashbox();
                $cashboxObj->initAmount = 0;                
                $cashboxObj->openDate = DateHelper::fnPHPDate2MySQLDatetime($list);
                $cashboxObj->idOffice = $this->getSessionParameter(self::USER_OFFICE_ID);
                $cashboxObj->idUser = $this->getSessionParameter(self::USER_ID);
                $cashboxObj->status = self::CASHBOX_STATUS_OPENED;
                DB::query(Database::UPDATE, "UPDATE rms_cashbox SET closeDate = NOW(), status = :STATUS,pastCashBox=:STATUSPASTCASHBOX WHERE idUser = :USER AND (closeDate IS NULL OR pastCashBox=1)")
                        ->param(':USER', $this->getSessionParameter(self::USER_ID))
                        ->param(':STATUS', self::CASHBOX_STATUS_CLOSED)
                        ->param(':STATUSPASTCASHBOX', self::CASHBOX_STATUS_DEACTIVE)
                        ->execute();                                
                $cashboxObj->save();
                $cashbox = DB::select_array($cols)
                        ->from('rms_cashbox');
                if (is_null($date_end))
                    $cashbox->where(db::expr("DATE_FORMAT(openDate,'$date_format')"), '=', $date);
                else
                    $cashbox->where('openDate', 'BETWEEN', DB::expr("STR_TO_DATE('$date','$date_format') AND STR_TO_DATE('$date_end $time_limit','$date_format $time_format_raw')"));
                if (!is_null($idUser)) {
                    $cashbox->and_where('rms_cashbox.idUser', '=', $idUser);
                }
                $cashbox = $cashbox->order_by('openDate')->execute()->as_array();
            }
            $aResponse['data'] = $cashbox;
        } catch (Exception $e_exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = 'No se registro ninguna caja el ' . $date . '.';
        }
        $this->fnResponseFormat($aResponse);
    }

    public function checkCashBox() {
        $today = DateHelper::getfechaActual();
        $date_time_now = DateHelper::getFechaFormateadaActual();
        $idUser = $this->getSessionParameter(self::USER_ID);
        $idOffice = $this->getSessionParameter(self::USER_OFFICE_ID);

        $cashbox = DB::select(
                                array('ca.idCashBox', 'idCashBox')
                                , array('ca.initAmount', 'initAmount')
                                , array('ca.openDate', 'openDate')
                                , array('ca.status', 'status')
                        )
                        ->from(array('rms_cashbox', 'ca'))
                        ->where(DB::expr("DATE_FORMAT(openDate,'%Y-%m-%d')"), 'LIKE', $today)
                        ->where('ca.closeDate', 'IS', NULL)
                        ->and_where('ca.status', '=', self::CASHBOX_STATUS_OPENED)
                        ->and_where('ca.idUser', '=', $idUser)
                        ->and_where('ca.idOffice', '=', $idOffice)
                        ->execute()->current();

        if ($cashbox == NULL) {
            $this->setSessionParameter(self::CASHBOX_URL_REDIRECT, $this->getRealRequestURI());
            $cash_box_open = DB::select(array('ca.idCashbox', 'idCashBox'))
                            ->from(array('rms_cashbox', 'ca'))
                            ->where('ca.status', '=', self::CASHBOX_STATUS_OPENED)
                            ->and_where(DB::expr("DATE_FORMAT(openDate,'%Y-%m-%d')"), 'NOT LIKE', $today)
                            ->and_where('ca.closeDate', 'IS', NULL)
                            ->and_where('ca.idUser', '=', $idUser)
                            ->and_where('ca.idOffice', '=', $idOffice)
                            ->execute()->as_array();
            
            $cash_open = DB::select('*')
                        ->from(array('rms_cashbox', 'ca'))
                        ->where('ca.pastCashBox','=',1)
                        ->and_where('ca.status', '=', self::CASHBOX_STATUS_OPENED)
                        ->and_where('ca.idUser', '=', $idUser)
                        ->and_where('ca.idOffice', '=', $idOffice)
                        ->execute()->current();
            
        if($cash_open != NULL) {
                $this->setSessionParameter(self::CASHBOX_URL_REDIRECT, $this->getRealRequestURI());
            } else {
                foreach ($cash_box_open as $cash) {
                    $last_cash = $cash['idCashBox'];
                    $model_cash = new Model_Cashbox($last_cash);
                    $model_cash->closeDate = $date_time_now;
                    $model_cash->status = self::CASHBOX_STATUS_CLOSED;
                    $model_cash->save();
                }

                Request::initial()->redirect('/private/cashbox/index');
            }
        }else{$this->setSessionParameter(self::CASHBOX_ID, $cashbox["idCashBox"]);}
    }

    public function action_getCurrentCorrelative() {
        $this->getCorrelativeAjax();
    }

    /**
     * DEVUELVE EL SIGUIENTE CORRELATIVO PARA UN DOCUMENTO ASIGNADO A UN USUARIO
     * Y PARA LA OFICINA EN LA QUE SE ENCUENTRA
     * * */
    public function action_getNextCorrelative() {
        $this->getCorrelativeAjax(TRUE);
    }

    /**
     * Resuelve la peticion ajax del actual o siguiente correlativo
     */
    public function getCorrelativeAjax($next = FALSE) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        $documentType = $this->request->post('dt');
        $this->getNextCorrelative($documentType, $next, TRUE, $aResponse);
        echo jQueryHelper::JSON_jQuery_encode($aResponse);
    }

    /**
     *
     * Resuelve tanto la peticion del actual o siguiente correlativo ajax o php segun el parametro ajax.
     * @param type $documentType
     * @param type $next_correlative
     * @param type $ajax
     * @param type $response_array_ajax
     * @return type 
     */
    public function getNextCorrelative($documentType, $next_correlative = TRUE, $ajax = FALSE, &$response_array_ajax = NULL) {
        try {
            $serieCorrelativeActive = ($this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE);
            $serieCorrelativeVoucherAutomatico = ($this->getOptionValue(self::SO_CORRELATIVO_VOUCHER_AUTOMATICO) == self::STATUS_ACTIVE);
            $ncorrelative = NULL;

            if (($documentType == self::DT_RECIBO_EGRESO OR $documentType == self::DT_RECIBO_INGRESO) AND $serieCorrelativeVoucherAutomatico) {

                $tmp_documentType = $documentType;
                if ($documentType == self::DT_RECIBO_EGRESO)
                    $tmp_documentType = self::DT_DEFAULT_VOUCHER_EGRESO;
                else
                    $tmp_documentType = self::DT_DEFAULT_VOUCHER_INGRESO;

                $correlative = $this->getNextUniqueSequence($tmp_documentType);
                if ($correlative != NULL) {
                    if ($ajax)
                        $correlative = str_pad($correlative, 8, "0", STR_PAD_LEFT);
                    $ncorrelative ['documentType'] = $documentType;
                    if ($documentType == self::DT_DEFAULT_VOUCHER_CORRELATIVE OR !$serieCorrelativeActive) {
                        $ncorrelative ['serie'] = self::DEFAULT_VOUCHER_AUTOMATIC_VALUE;
                    } else {
                        $ncorrelative ['serie'] = NULL;
                    }
                    $ncorrelative ['correlative'] = $correlative;
                    $ncorrelative = (object) $ncorrelative;
                }
            } else {
                if (!$serieCorrelativeActive AND ($documentType == self::DT_RECIBO_EGRESO 
                        || $documentType == self::DT_RECIBO_INGRESO 
                        || $documentType == self::DT_NOTA_INGRESO 
                        || $documentType == self::DT_NOTA_PEDIDO 
                        || $documentType == self::DT_NOTA_SALIDA 
                        || $documentType == self::DT_RECIBO 
                        || $documentType == self::DT_ORDENCOMPRA 
                        || $documentType == self::DT_DEFAULT_CORRELATIVE)) {

                    $tmp_documentType = $documentType;
                    if (!$serieCorrelativeActive AND ($documentType != self::DT_VALE_BOLETO AND $documentType != self::DT_RECIBO)) {

                        $tmp_documentType = self::DT_DEFAULT_CORRELATIVE;
                    }
                    if ($next_correlative) {
                        $correlative = $this->getNextUniqueSequence($tmp_documentType);
                    } else {
                        $correlative = $this->getCurrentUniqueSequence($tmp_documentType);
                    }

                    if ($correlative != NULL) {
                        if ($ajax)
                            $correlative = str_pad($correlative, 8, "0", STR_PAD_LEFT);
                        $ncorrelative ['documentType'] = $documentType;
                        if ($documentType == self::DT_DEFAULT_CORRELATIVE OR !$serieCorrelativeActive) {
                            $ncorrelative ['serie'] = self::DEFAULT_SERIE_AUTOMATIC_VALUE;
                        } else {
                            $ncorrelative ['serie'] = NULL;
                        }
                        $ncorrelative ['correlative'] = $correlative;
                        $ncorrelative = (object) $ncorrelative;
                    }
                } else {

                    $ncorrelative = DB::select(
                                            array('rms_series.serie', 'serie'), array('rms_series.correlative', 'correlative'), array('rms_series.documentType', 'documentType'), array('rms_series.idAssignmentseries', 'idAssignmentseries'), array('rms_series.idAssignmentseries', 'idAssignmentseries'), array('rms_series.sequence_max_value', 'limit')
                                    )
                                    ->from('rms_documents')->join('rms_series')
                                    ->on('rms_series.idAssignmentSeries', '=', 'rms_documents.idAssignmentSeries')
                                    ->where('rms_documents.state', '=', self::STATUS_ACTIVE)
                                    ->and_where('rms_series.state', '=', self::STATUS_ACTIVE)
                                    ->and_where('rms_documents.idUser', '=', $this->getSessionParameter(self::USER_ID))
                                    ->and_where('rms_series.idOffice', '=', $this->getSessionParameter(self::USER_OFFICE_ID))
                                    ->and_where('rms_series.documentType', '=', $documentType)->as_object(true)->execute()->current();

                    if ($next_correlative) {

                        $correlative = DB::select(DB::expr("`NEXT_CORRELATIVE`(" . $this->getSessionParameter(self::USER_ID) . "," . $this->getSessionParameter(self::USER_OFFICE_ID) . ",'" . $documentType . "') AS correlative"))->execute()->as_array();

                        if ($correlative[0]['correlative'] == null || !is_numeric($correlative[0]['correlative'])) {
                            throw new Exception(__("Su Usuario no tiene asignado este tipo de documento") . ' (' . $documentType . ')', self::CODE_SUCCESS);
                        } else {
                            if (($correlative[0]['correlative'] + 1) > $ncorrelative->limit)
                                throw new Exception(__("Se ha alcanzado el límite del correlativo") . ' (' . $documentType . ')', self::CODE_SUCCESS);
                            else
                                $ncorrelative->correlative = str_pad($correlative[0]['correlative'], 8, '0', STR_PAD_LEFT);
                        }
                    }
                }
            }

            if ($ncorrelative != NULL) {
                if ($ajax) {
                    if ($documentType != self::DT_DEFAULT_CORRELATIVE) {

                        $aDocumentTypes = $this->getDocumentsByFilter('have_serie', true); //$this->a_documents_type;    
                        $aDocumentType = $aDocumentTypes[$ncorrelative->documentType];

                        $aDocumentType['display'] = __($aDocumentType['display']);
                        $ncorrelative->documentType = $aDocumentType;
                    }
                    $response_array_ajax['data'] = $ncorrelative;
                } else {

                    return $ncorrelative;
                }
            } else {

                throw new Exception(__("No existe serie o correlativo para este documento.") . ' (' . $documentType . ')', self::CODE_SUCCESS);
            }
        } catch (Exception $e) {
            if ($ajax) {
                $response_array_ajax['code'] = self::CODE_ERROR;
                $response_array_ajax['msg'] = $this->errorHandling($e);
            } else {
                throw new Exception($e->getMessage(), $e->getCode(), $e);
            }
        }
    }

    /* sdf */

    public function printDocument($idDocument, $documentType, $returnView = FALSE, $auto_render = TRUE) {
        $this->auto_render = $auto_render;
        $SKIN = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
        $SITE_NAME = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['siteName'];
        $print_error_view = new View('/printer/printError');
        $print_error_view->skin = $SKIN;
        if (!$returnView)
            $this->template = new View('/printer/emptyTemplate');

        try {

            $HTML_TEMPLATE = $BROWSER_FOLDER = 'printer' . DIRECTORY_SEPARATOR . 'FF' . DIRECTORY_SEPARATOR . 'GEN' . DIRECTORY_SEPARATOR . $documentType;
            if (implode(Kohana::find_file('views', $HTML_TEMPLATE, NULL, TRUE)) != '') {
                $documentPrint_view = new View($HTML_TEMPLATE);
                switch ($documentType) {
                    case self::DT_TICKET :
                        $ticket = new Model_Sales($idDocument);
                        $documentPrint_view->Serie = $ticket->serie;
                        $documentPrint_view->TotalPrice = $ticket->totalPrice;
                        $documentPrint_view->TotalPriceText = $this->num2letras($ticket->totalPrice);
                        $documentPrint_view->Correlative = $ticket->correlative;

                        $idUserCode = DB::select('u.userCode')
                                        ->from(array('bts_user', 'u'))
                                        ->where('u.idUser', '=', $ticket->idUserCode)
                                        ->as_object()->execute()->current();
                        $documentPrint_view->UserCode = $idUserCode;
                        $documentPrint_view->DocumentType = $documentType;
                        $documentPrint_view->PaymentAmount = $ticket->paymentAmount;
                        $documentPrint_view->ReturnCash = $ticket->returnCash;
                        $documentPrint_view->SalesDate = $ticket->salesDate;
                        $a_MethodPayment = $this->fnGetClassVariable(self::VAR_NAME_SALES_PAYMENT);
                        $documentPrint_view->MethodPayment = __($a_MethodPayment[$ticket->methodPayment]['display']);
                        $param = array(
                            ':idCustomers' => $ticket->idCustomers,
                            ':idSeller' => $ticket->userCreate
                        );
                        $documentPrint_view->Customers = DB::select(
                                                array(DB::expr("IF(c.idCustomersCompany IS NULL,p.fullName,cc.name)"), 'customers')
                                        )
                                        ->from(array('rms_customers', 'c'))
                                        ->join(array('bts_person', 'p'), 'LEFT')->on('c.idPerson', '=', 'p.idPerson')
                                        ->join(array('rms_customerscompany', 'cc'), 'LEFT')->on('c.idCustomersCompany', '=', 'cc.idCustomersCompany')
                                        ->join(array('rms_address', 'ad'), 'LEFT')->on('cc.idCustomersCompany', '=', 'ad.idCustomersCompany')
                                        ->where('c.idCustomers', '=', ':idCustomers')
                                        ->parameters($param)
                                        ->as_object()->execute()->current();

                        $documentPrint_view->Seller = DB::select(
                                                array(DB::expr("CONCAT(p.fName,' ',p.lName)"), 'seller')
                                        )
                                        ->from(array('bts_person', 'p'))
                                        ->where('p.idPerson', '=', ':idSeller')
                                        ->parameters($param)
                                        ->execute()->current();
//                      echo Database::instance()->last_query;
//                      die();
//print_r               ($documentPrint_view);
//                         die();
                        $parameter = array(
                            ':idSales' => $ticket->idSales
                        );
                        $documentPrint_view->detail = DB::select(
                                                'sd.quantity'
                                                , 'p.productName'
                                                , 'sd.unitPrice'
                                                , 'sd.discount'
                                                , 'sd.totalPrice')
                                        ->from(array('rms_salesdetail', 'sd'))
                                        ->join(array('rms_lot', 'lot'))->on('sd.idLot', '=', 'lot.idLot')
                                        ->join(array('rms_products', 'p'), 'LEFT')->on('p.idProduct', '=', 'lot.idProduct')
                                        ->where('sd.idSales', '=', ':idSales')
                                        ->and_where_open()
                                        ->and_where('sd.statusDetail', '<>', self::SALES_DETAIL_CANCELED)
                                        ->or_where('sd.statusDetail', 'IS', NULL)
                                        ->and_where_close()
                                        ->parameters($parameter)
                                        ->execute()->as_array();
//                        echo Database::instance()->last_query;die();
                        $this->template->printable_document = $documentPrint_view;
                        break;
                    case self::DT_BOLETA :
                        $ticket = new Model_Sales($idDocument);
                        $documentPrint_view->Serie = $ticket->serie;
                        $documentPrint_view->TotalPrice = $ticket->totalPrice;
                        $documentPrint_view->TotalPriceText = $this->num2letras($ticket->totalPrice);
                        $documentPrint_view->Correlative = $ticket->correlative;

                        $documentPrint_view->DocumentType = $documentType;
                        $documentPrint_view->PaymentAmount = $ticket->paymentAmount;
                        $documentPrint_view->ReturnCash = $ticket->returnCash;
                        $documentPrint_view->SalesDate = $ticket->salesDate;
                        $a_MethodPayment = $this->fnGetClassVariable(self::VAR_NAME_SALES_PAYMENT);
                        $documentPrint_view->MethodPayment = __($a_MethodPayment[$ticket->methodPayment]['display']);
                        $param = array(
                            ':idCustomers' => $ticket->idCustomers,
                            ':idSeller' => $ticket->userCreate
                        );
                        $documentPrint_view->Customers = DB::select(
                                                array(DB::expr("IF(c.idCustomersCompany IS NULL,p.fullName,cc.name)"), 'customers')
                                        )
                                        ->from(array('rms_customers', 'c'))
                                        ->join(array('bts_person', 'p'), 'LEFT')->on('c.idPerson', '=', 'p.idPerson')
                                        ->join(array('rms_customerscompany', 'cc'), 'LEFT')->on('c.idCustomersCompany', '=', 'cc.idCustomersCompany')
                                        ->join(array('rms_address', 'ad'), 'LEFT')->on('cc.idCustomersCompany', '=', 'ad.idCustomersCompany')
                                        ->where('c.idCustomers', '=', ':idCustomers')
                                        ->parameters($param)
                                        ->as_object()->execute()->current();

                        $documentPrint_view->Seller = DB::select(
                                                array(DB::expr("CONCAT(p.fName,' ',p.lName)"), 'seller')
                                        )
                                        ->from(array('bts_person', 'p'))
                                        ->where('p.idPerson', '=', ':idSeller')
                                        ->parameters($param)
                                        ->execute()->current();
//                      echo Database::instance()->last_query;
//                      die();
//print_r               ($documentPrint_view);
//                         die();
                        $parameter = array(
                            ':idSales' => $ticket->idSales
                        );
                        $documentPrint_view->detail = DB::select(
                                                'sd.quantity'
                                                , 'p.productName'
                                                , 'sd.unitPrice'
                                                , 'sd.discount'
                                                , 'sd.totalPrice')
                                        ->from(array('rms_salesdetail', 'sd'))
                                        ->join(array('rms_lot', 'lot'))->on('sd.idLot', '=', 'lot.idLot')
                                        ->join(array('rms_products', 'p'), 'LEFT')->on('p.idProduct', '=', 'lot.idProduct')
                                        ->where('sd.idSales', '=', ':idSales')
                                        ->and_where_open()
                                        ->and_where('sd.statusDetail', '<>', self::SALES_DETAIL_CANCELED)
                                        ->or_where('sd.statusDetail', 'IS', NULL)
                                        ->and_where_close()
                                        ->parameters($parameter)
                                        ->execute()->as_array();
//                        echo Database::instance()->last_query;die();
                        $this->template->printable_document = $documentPrint_view;
                        break;
                    case self::DT_FACTURA :
                        $ticket = new Model_Sales($idDocument);
                        $documentPrint_view->DocumentType = $documentType;
                        $documentPrint_view->Serie = $ticket->serie;
                        $documentPrint_view->TotalPrice = $ticket->totalPrice;
                        $documentPrint_view->tax = $ticket->totaltax;
                        $documentPrint_view->TotalPriceText = $this->num2letras($ticket->totalPrice);
                        $documentPrint_view->PaymentAmount = $ticket->paymentAmount;
                        $documentPrint_view->ReturnCash = $ticket->returnCash;
                        $documentPrint_view->Correlative = $ticket->correlative;
                        $documentPrint_view->SalesDate = $ticket->salesDate;
                        $a_MethodPayment = $this->fnGetClassVariable(self::VAR_NAME_SALES_PAYMENT);
                        $documentPrint_view->MethodPayment = __($a_MethodPayment[$ticket->methodPayment]['display']);
                        $param = array(
                            ':idCustomers' => $ticket->idCustomers,
                            ':idSeller' => $ticket->userCreate
                        );
                        $documentPrint_view->Customers = DB::select(
                                                array(DB::expr("IF(c.idCustomersCompany IS NULL,p.fullName,cc.name)"), 'customers')
                                        )
                                        ->from(array('rms_customers', 'c'))
                                        ->join(array('bts_person', 'p'), 'LEFT')->on('c.idPerson', '=', 'p.idPerson')
                                        ->join(array('rms_customerscompany', 'cc'), 'LEFT')->on('c.idCustomersCompany', '=', 'cc.idCustomersCompany')
                                        ->join(array('rms_address', 'ad'), 'LEFT')->on('cc.idCustomersCompany', '=', 'ad.idCustomersCompany')
                                        ->where('c.idCustomers', '=', ':idCustomers')
                                        ->parameters($param)
                                        ->as_object()->execute()->current();

                        $documentPrint_view->Seller = DB::select(
                                                array(DB::expr("CONCAT(p.fName,' ',p.lName)"), 'seller')
                                        )
                                        ->from(array('bts_person', 'p'))
                                        ->where('p.idPerson', '=', ':idSeller')
                                        ->parameters($param)
                                        ->execute()->current();
                        $parameter = array(
                            ':idSales' => $ticket->idSales
                        );
                        $documentPrint_view->detail = DB::select(
                                                'sd.quantity'
                                                , 'p.productName'
                                                , 'sd.unitPrice'
                                                , 'sd.discount'
                                                , 'sd.totalPrice')
                                        ->from(array('rms_salesdetail', 'sd'))
                                        ->join(array('rms_lot', 'lot'))->on('sd.idLot', '=', 'lot.idLot')
                                        ->join(array('rms_products', 'p'), 'LEFT')->on('p.idProduct', '=', 'lot.idProduct')
                                        ->where('sd.idSales', '=', ':idSales')
                                        ->and_where_open()
                                        ->and_where('sd.statusDetail', '<>', self::SALES_DETAIL_CANCELED)
                                        ->or_where('sd.statusDetail', 'IS', NULL)
                                        ->and_where_close()
                                        ->parameters($parameter)
                                        ->execute()->as_array();

                        $this->template->printable_document = $documentPrint_view;
                        break;

                    default:
                        break;
                }
            } else {

                $print_template_no_exist_view = new View('printer/printTemplateNoExist');
                $print_template_no_exist_view->ERROR_MESSAGE = __("No existe un formato para la configuración de su terminal y el documento que desea imprimir. Su Configuración es la siguiente:");
                $print_error_view->title_dialog = __('No Existe Formato');
                $print_error_view->content_dialog = $print_template_no_exist_view;
                $this->template->printable_document = $print_error_view;
                //echo (Database::instance()->last_query);
                //  die();
            }
        } catch (Exception $exc) {
            if (!$returnView)
                $this->template->printable_document = $exc->getTraceAsString();
            else
                throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

    /* funcion cambia numero a letras */

    static function num2letras($num, $fem = true, $dec = true) {
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande");
        $matuni[2] = "dos";
        $matuni[3] = "tres";
        $matuni[4] = "cuatro";
        $matuni[5] = "cinco";
        $matuni[6] = "seis";
        $matuni[7] = "siete";
        $matuni[8] = "ocho";
        $matuni[9] = "nueve";
        $matuni[10] = "diez";
        $matuni[11] = "once";
        $matuni[12] = "doce";
        $matuni[13] = "trece";
        $matuni[14] = "catorce";
        $matuni[15] = "quince";
        $matuni[16] = "dieciseis";
        $matuni[17] = "diecisiete";
        $matuni[18] = "dieciocho";
        $matuni[19] = "diecinueve";
        $matuni[20] = "veinte";
        $matunisub[2] = "dos";
        $matunisub[3] = "tres";
        $matunisub[4] = "cuatro";
        $matunisub[5] = "quin";
        $matunisub[6] = "seis";
        $matunisub[7] = "sete";
        $matunisub[8] = "ocho";
        $matunisub[9] = "nove";

        $matdec[2] = "veint";
        $matdec[3] = "treinta";
        $matdec[4] = "cuarenta";
        $matdec[5] = "cincuenta";
        $matdec[6] = "sesenta";
        $matdec[7] = "setenta";
        $matdec[8] = "ochenta";
        $matdec[9] = "noventa";
        $matsub[3] = 'mill';
        $matsub[5] = 'bill';
        $matsub[7] = 'mill';
        $matsub[9] = 'trill';
        $matsub[11] = 'mill';
        $matsub[13] = 'bill';
        $matsub[15] = 'mill';
        $matmil[4] = 'millones';
        $matmil[6] = 'billones';
        $matmil[7] = 'de billones';
        $matmil[8] = 'millones de billones';
        $matmil[10] = 'trillones';
        $matmil[11] = 'de trillones';
        $matmil[12] = 'millones de trillones';
        $matmil[13] = 'de trillones';
        $matmil[14] = 'billones de trillones';
        $matmil[15] = 'de billones de trillones';
        $matmil[16] = 'millones de billones de trillones';

        $num = trim((string) @$num);
        if ($num[0] == '-') {
            $neg = 'menos ';
            $num = substr($num, 1);
        }
        else
            $neg = '';
        while ($num[0] == '0')
            $num = substr($num, 1);
        if ($num[0] < '1' or $num[0] > 9)
            $num = '0' . $num;
        $zeros = true;
        $punt = false;
        $ent = '';
        $fra = '';
        for ($c = 0; $c < strlen($num); $c++) {
            $n = $num[$c];
            if (!(strpos(".,'''", $n) === false)) {
                if ($punt)
                    break;
                else {
                    $punt = true;
                    continue;
                }
            } elseif (!(strpos('0123456789', $n) === false)) {
                if ($punt) {
                    if ($n != '0')
                        $zeros = false;
                    $fra .= $n;
                }
                else
                    $ent .= $n;
            }
            else
                break;
        }
        $ent = '     ' . $ent;
        if ($dec and $fra and !$zeros) {
            $fin = ' coma';
            for ($n = 0; $n < strlen($fra); $n++) {
                if (($s = $fra[$n]) == '0')
                    $fin .= ' cero';
                elseif ($s == '1')
                    $fin .= $fem ? ' una' : ' un';
                else
                    $fin .= ' ' . $matuni[$s];
            }
        }
        else
            $fin = '';
        if ((int) $ent === 0)
            return 'Cero ' . $fin;
        $tex = '';
        $sub = 0;
        $mils = 0;
        $neutro = false;
        while (($num = substr($ent, -3)) != '   ') {
            $ent = substr($ent, 0, -3);
            if (++$sub < 3 and $fem) {
                $matuni[1] = 'una';
                $subcent = 'as';
            } else {
                $matuni[1] = $neutro ? 'un' : 'uno';
                $subcent = 'os';
            }
            $t = '';
            $n2 = substr($num, 1);
            if ($n2 == '00') {
                
            } elseif ($n2 < 21)
                $t = ' ' . $matuni[(int) $n2];
            elseif ($n2 < 30) {
                $n3 = $num[2];
                if ($n3 != 0)
                    $t = 'i' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }else {
                $n3 = $num[2];
                if ($n3 != 0)
                    $t = ' y ' . $matuni[$n3];
                $n2 = $num[1];
                $t = ' ' . $matdec[$n2] . $t;
            }
            $n = $num[0];
            if ($n == 1 AND $n2 == 0) {
                $t = ' cien' . $t;
            } elseif ($n == 1) {
                $t = ' ciento' . $t;
            } elseif ($n == 5) {
                $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
            } elseif ($n != 0) {
                $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
            }

            if ($sub == 1) {
                
            } elseif (!isset($matsub[$sub])) {
                if ($num == 1) {
                    $t = ' mil';
                } elseif ($num > 1) {
                    $t .= ' mil';
                }
            } elseif ($num == 1) {
                $t .= ' ' . $matsub[$sub] . '?n';
            } elseif ($num > 1) {
                $t .= ' ' . $matsub[$sub] . 'ones';
            }
            if ($num == '000')
                $mils++;
            elseif ($mils != 0) {
                if (isset($matmil[$sub]))
                    $t .= ' ' . $matmil[$sub];
                $mils = 0;
            }
            $neutro = true;
            $tex = $t . $tex;
        }
        $tex = $neg . substr($tex, 1) . $fin;
        return ucfirst($tex);
    }

// END FUNCTION    
    public function sendEmailOrMessageMinimunStock($string_product_min) {
        $asunto = __("Lista Productos Con Minimo Stock");
        $admin = DB::select(array('pe.fullName', 'fullName'), array('pe.email', 'email'))
                        ->from(array('bts_user', 'us'))
                        ->join(array('bts_group', 'gp'))
                        ->on('us.idGroup', '=', 'gp.idGroup')
                        ->join(array('bts_person', 'pe'))
                        ->on('us.idUser', '=', 'pe.idPerson')
                        ->where('gp.name', '=', 'Administradores')
                        ->and_where('us.status', '=', self::STATUS_ACTIVE)
                        ->as_object()->execute()->current();
//        echo Database::instance()->last_query;die();
        $mailRecipient = $admin->email;
        $nameRecipient = $admin->fullName;
        $view = "mail/minimum_stock";
        $arrayMsj = array(
            ":list_product" => $string_product_min
        );
        $emailSend = "soporte@ibusplus.com";
        $personSend = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
        $this->sendEmail($asunto, $mailRecipient, $nameRecipient, $view, $arrayMsj, $emailSend, $personSend);
    }

    public function fnsendMessageMinimumStock($sMessage, $iTo_City = 0, $toIdOffice = 0, $toIdUSer = 0, $toIdGroup = 0) {
        try {
            $oModel_Message = new Model_Messages;
//           $oModel_Message->toIdGroup = $toIdGroup;           
            $oModel_Message->toIdCity = $iTo_City;
            $oModel_Message->userCreate = self::STATUS_DEACTIVE;
            $oModel_Message->userLastChange = self::STATUS_DEACTIVE;
            $oModel_Message->toIdOffice = $toIdOffice;
            $oModel_Message->toIdUser = $toIdUSer;
            $oModel_Message->message = $sMessage;
            $oModel_Message->state = self::STATUS_ACTIVE;
            Fragment::delete($this->cacheSessionDynamicIndex, true);
            Cache::instance('apc')->delete_all();
//           $this->fncleaningFragmentCacheAndApc();
            $oModel_Message->save();
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode());
        }
    }

    //Funcion de Impresion de documento de guia de remision

    public function printDocumentTransfer($i_mov_ts, $i_mov_ti, $returnView = FALSE, $auto_render = TRUE) {
        $this->auto_render = $auto_render;
        $SKIN = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['skin'];
        $SITE_NAME = db_config::$db_conection_config[$_SERVER['HTTP_HOST']]['siteName'];

        $print_error_view = new View('/printer/printError');
        $print_error_view->skin = $SKIN;
        if (!$returnView)
            $this->template = new View('/printer/emptyTemplate');

        try {
            $HTML_TEMPLATE = $BROWSER_FOLDER = 'printer' . DIRECTORY_SEPARATOR . 'FF' . DIRECTORY_SEPARATOR . 'GEN' . DIRECTORY_SEPARATOR . 'GUIA_REMISION';

            if (implode(Kohana::find_file('views', $HTML_TEMPLATE, NULL, TRUE)) != '') {
                $documentPrint_view = new View($HTML_TEMPLATE);
                // Buscar Movimiento de Salida 
                $obj_MoveTS = new Model_Move($i_mov_ts);
                $documentPrint_view->seriecorrelative = $obj_MoveTS->serie . '  -  ' . $obj_MoveTS->correlative;
                $documentPrint_view->date = $obj_MoveTS->dateOutcome;
                // Usuario 
                $documentPrint_view->User = DB::select('*')
                                ->from(array('bts_person', 'p'))
                                ->where('p.idPerson', '=', $obj_MoveTS->userCreate)
                                ->as_object()->execute()->current();

                $documentPrint_view->moveReference = $obj_MoveTS->moveReference;
                $objStoreO = ORM::factory('stores')
                        ->where('idStore', '=', $obj_MoveTS->idStore)
                        ->find();
                $documentPrint_view->storeOrigen = $objStoreO->storeName;

                //Detalle de Movimiento de Salida
                $documentPrint_view->detail = DB::select('po.aliasProduct', 'po.productName', 'cat.categoryName', 'lt.lotName'
                                        , 'md.unitPrice', 'md.amount')
                                ->from(array('rms_move', 'm'))
                                ->join(array('rms_movedetail', 'md'))
                                ->on('m.idMove', '=', 'md.idMove')
                                ->join(array('rms_lot', 'lt'))
                                ->on('md.idLot', '=', 'lt.idLot')
                                ->join(array('rms_products', 'po'))
                                ->on('lt.idProduct', '=', 'po.idProduct')
                                ->join(array('rms_category', 'cat'))
                                ->on('po.idCategory', '=', 'cat.idCategory')
                                ->where('m.idMove', '=', $obj_MoveTS->idMove)
                                ->and_where('md.status', '=', '1')
                                ->and_where('md.amount', '!=', '0')
                                ->execute()->as_array();


                // Buscar Movimiento de Entrada
                $obj_MoveTI = new Model_Move($i_mov_ti);
                $objStoreD = ORM::factory('stores')
                        ->where('idStore', '=', $obj_MoveTI->idStore)
                        ->find();
                $documentPrint_view->storeDestino = $objStoreD->storeName;


                $this->template->printable_document = $documentPrint_view;
            } else {
                $print_template_no_exist_view = new View('printer/printTemplateNoExist');
                $print_template_no_exist_view->ERROR_MESSAGE = __("No existe un formato para la configuración de su terminal y el documento que desea imprimir. Su Configuración es la siguiente:");
                $print_error_view->title_dialog = __('No Existe Formato');
                $print_error_view->content_dialog = $print_template_no_exist_view;
                $this->template->printable_document = $print_error_view;
                //echo (Database::instance()->last_query);
                //die();
            }
        } catch (Exception $exc) {
            if (!$returnView)
                $this->template->printable_document = $exc->getTraceAsString();
            else
                throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }

    public function getRealRequestURI() {
        return Request::initial()->directory() . "/" . Request::initial()->controller() . "/" . Request::initial()->action();
    }

    public function getOptionValue($key) {
        try {
            $var = $this->getSessionParameter(self::SYSTEM_OPTIONS);
            return $var[$key]['value'];
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

}