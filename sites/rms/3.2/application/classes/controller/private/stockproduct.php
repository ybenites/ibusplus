<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of stockproduct
 *
 * @author Yime
 */
class Controller_Private_Stockproduct extends Controller_Private_Admin implements Rms_Constants {

    //put your code here
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('stockproduct', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('stockproduct', self::FILE_TYPE_JS));
        $stockproduct = new View('inventory/stockproduct');

        $obj_product = ORM::factory('products')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $obj_brand = ORM::factory('brands')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $obj_store = ORM::factory('stores')
                        ->where('status', '=', self::STATUS_ACTIVE)->find_all();
        $category = DB::select(array('idCategory', 'id'), array('categoryName', 'name'), array('codigo', 'codigo'))
                        ->from('rms_category')
                        ->where('idSuperCategory', 'IS', NULL)
                        ->and_where('status', '=', self::STATUS_ACTIVE)
                        ->execute()->as_array();
        $stockproduct->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $stockproduct->array_product = $obj_product;
        $stockproduct->array_brand = $obj_brand;
        $stockproduct->array_store = $obj_store;
        $stockproduct->category = $category;
        $this->template->content = $stockproduct;
    }

    public function action_listAllProduct() {
        $this->auto_render = FALSE;
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            $idProduct = $this->request->post('idProduct');
            $idStore = $this->request->post('idStore');
            $idBrand = $this->request->post('idBrand');
            $idCategory = $this->request->post('idCategory');
            $idSubCategory = $this->request->post('idSubCategory');


            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $a_array_cols = array(
                "idStock" => "sk.idStock",
                "idProduct" => "pr.idProduct",
                "aliasProduct" => "pr.aliasProduct",
                "productName" => "pr.productName",
                "brandNameCommercial" => "br.brandNameCommercial",
                "categoryName" => "ca.categoryName",
                "idStore" => "sr.idStore",
                "storeName" => "sr.storeName",
                "unitPrice" => "pr.productPriceSell",
                //"stock" => "SUM(IF(rm.typeMoveStore = 'INGRESO' || rm.typeMoveStore = 'TRAS_INGRESO' ,rmd.amount,0))- SUM(IF(rm.typeMoveStore = 'SALIDA' || rm.typeMoveStore = 'TRAS_SALIDA' || rm.typeMoveStore = 'VENTA_DIRECTA' || rm.typeMoveStore = 'VENTA_RESERVA',rmd.amount,0))"                
                "amount" => "sk.amount",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);
            
            $s_tables_join = " rms_products pr
                               INNER JOIN rms_brands br ON pr.idBrand = br.idBrand
                               INNER JOIN rms_stock sk ON sk.idProduct = pr.idProduct
                               INNER JOIN rms_store sr ON sr.idStore = sk.idStore
                               INNER JOIN rms_category ca ON pr.idCategory = ca.idCategory";         

           // $s_tables_join = "liststockproducts pro";
            
            $idProduct_s = "";
            $idStore_s = "";
            $idCategory_s = "";
            $idBrand_s = "";
            $idSubCategory_s = "";
            
  
            if ($idProduct != '') {
                $idProduct_s = " AND pr.idProduct=$idProduct";
            }
            if ($idStore != '') {
                $idStore_s = " AND sk.idStore=$idStore";
            }
            if ($idBrand != '') {
                $idBrand_s = " AND br.idBrand=$idBrand";
            }
            
            if($idSubCategory != '')
            {
                if($idCategory != '')
                {
                    $idCategory_s = " AND ca.idSuperCategory = $idCategory";
                    $idSubCategory_s= " AND ca.idCategory = $idSubCategory ";  
                }
//                }else{
//                    $idSubCategory_s = " AND ca.idCategory = $idSubCategory ";  
//                }
            }else  if ($idCategory != '') {
                $idCategory_s = " AND (ca.idCategory = ". $idCategory." OR ca.idSuperCategory = ".$idCategory.")";
            }     
            
            $s_where_conditions = " TRUE AND pr.status=1 AND sk.amount > 0 ".$idProduct_s. $idStore_s. $idCategory_s. $idBrand_s .$idSubCategory_s;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
//print_r(Database::instance()->last_query);die();
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
    }

    public function action_listAllDetailProduct() {
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            $idProduct = $this->request->post('product_id');
            $idStore = $this->request->post('idStore');

            $obj_product = new Model_Products($idProduct);
            $hd = $obj_product->haveDetail;

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $a_array_cols = array(
                "idProductDetail" => "rpd.idProductDetail",
                "haveDetail"=>"rp.haveDetail",
                "idProduct" => "rp.idProduct",
                "productName" => "rp.productName",
                "serie_electronic" => "rpd.serie_electronic",
                "unitPrice" => "rp.productPriceSell",
                "amount" => "1",
                "description" => "rpd.description",
                "creationDate" => "IF(mv.dateIncome IS NULL,DATE_FORMAT(mv.dateOutcome,'$s_date_format $s_time_format'),DATE_FORMAT(mv.dateIncome,'$s_date_format $s_time_format'))"
            );
            if (!$hd) {
                $a_array_cols['idProductDetail'] = 'rpd.idLot';
                $a_array_cols['serie_electronic'] = 'rpd.lotName';
                $a_array_cols['amount'] = 'rpd.amount';
            }
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);
            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);

            $store = '';
            if ($idStore != null) {
                $store = "AND mv.idStore=$idStore";
            }
            $grouping = NULL;
            if ($hd) {
//                $query_data_not_show="SELECT mds.idProductDetail FROM `rms_movedetail` mds 
//                WHERE mds.idMoveDetail IN (SELECT MAX(mvss.idMoveDetail) FROM rms_product_detail pdss 
//		INNER JOIN rms_movedetail mvss ON mvss.idProductDetail=pdss.idProductDetail
//		INNER JOIN rms_move mss ON mvss.idMove=mss.idMove
//		WHERE mss.idStore=$idStore AND pdss.amount<>0  AND (mss.typeMoveStore='VENTA_DIRECTA' OR mss.typeMoveStore='TRAS_SALIDA' OR mss.typeMoveStore='SALIDA')
//		GROUP BY pdss.idProductDetail)";
//                $rs=DB::query(Database::SELECT,$query_data_not_show)->execute()->as_array();
//                $cad='';                
//                if(count($rs)>0){                    
//                    foreach ($rs as $key=>$v) {                    
//                        $a[$key]=$v['idProductDetail'];                        
//                    }
//                    $cad= implode(',',$a);                    
//                }else{
//                    $cad=0;
//                }
                $grouping = "GROUP BY rpd.idProductDetail HAVING MOD(COUNT(rpd.idProductDetail),2)<>0";
                $s_tables_join = 'rms_products rp ' .
                        'INNER JOIN rms_product_detail rpd ON rp.idProduct=rpd.idProduct ' .
                        'INNER JOIN rms_movedetail mvdi ON mvdi.idProductDetail=rpd.idProductDetail ' .
                        'INNER JOIN rms_move mv ON mvdi.idMove=mv.idMove ';
                $s_where_conditions = " rp.idProduct=$idProduct $store AND rpd.amount<>0";
            } else {
                $grouping = " GROUP BY rpd.idLot";
                $s_tables_join = 'rms_products rp ' .
                        'INNER JOIN rms_lot rpd ON rp.idProduct=rpd.idProduct ' .
                        'INNER JOIN rms_movedetail mvdi ON mvdi.idLot=rpd.idLot ' .
                        'INNER JOIN rms_move mv ON mvdi.idMove=mv.idMove ';
                $s_where_conditions = " rp.idProduct=$idProduct $store 
                AND rpd.amount>0 AND mv.typeMoveStore<>'VENTA_DIRECTA' 
                AND mv.typeMoveStore<>'TRAS_SALIDA' 
                AND mv.typeMoveStore<>'SALIDA'
                AND mv.typeMoveStore<>'VENTA_RESERVA'
                AND mv.typeMoveStore<>'RESERVA'";
            }
            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start, $grouping);
//            
//            echo Database::instance()->last_query;
//            die();
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);

        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
    }

    public function action_listByDetailProduct() {
        try {
            $a_response = $this->json_array_return;
            $idp = $this->request->post('idp');
            $iddp = $this->request->post('iddp');
            $product = ORM::factory('products')->where('idProduct', '=', $idp)
                    ->find();
            $a_response['detail'] = $product->haveDetail;
            if ($product->haveDetail == self::STATUS_DEACTIVE) {
                $a_response['data'] = DB::select('*')
                                ->from('rms_lot')
                                ->where('idProduct', '=', $idp)
                                ->and_where('idLot', '=', $iddp)
                                ->execute()->as_array();
            } else {
                $a_response['data'] = DB::select('*')
                                ->from('rms_product_detail')
                                ->where('idProduct', '=', $idp)
                                ->and_where('idProductDetail', '=', $iddp)
                                ->execute()->as_array();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
        $this->fnResponseFormat($a_response);
    }
    
    
   

    public function action_editDetailProduct() {

        $a_response = $this->json_array_return;
        try {
            $o_post_productdetail = Validation::factory($this->request->post());
            $data_post_productdetail = $o_post_productdetail->data();
            if ($data_post_productdetail['detail'] == 1) {
                $obj_detailproduct = new Model_Productdetail($data_post_productdetail['idProductDetail']);
                $obj_detailproduct->serie_electronic = $data_post_productdetail['serie_electronic'];
                $obj_detailproduct->unitPrice = $data_post_productdetail['unitPrice'];
                $obj_detailproduct->description = $data_post_productdetail['description'];
                //    $obj_detailproduct->creationDate = $data_post_productdetail['creationDate'];
                $obj_detailproduct->save();
            } else {
                $obj_lot = new Model_Lot($data_post_productdetail['idProductDetail']);
                $obj_lot->lotName = $data_post_productdetail['serie_electronic'];
                $obj_lot->unitPrice = $data_post_productdetail['unitPrice'];
                //  $obj_detailproduct->amount = "1";
                $obj_lot->description = $data_post_productdetail['description'];
                //$obj_lot->creationDate = $data_post_productdetail['creationDate'];
                $obj_lot->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteDetailProduct() {
        $a_response = $this->json_array_return;
        try {
            $o_post_productdetail = Validation::factory($this->request->post());
            $data_post_productdetail = $o_post_productdetail->data();
            $iddp = $data_post_productdetail['iddp'];
            $idp = $data_post_productdetail['idp'];
            $ids = $data_post_productdetail['ids'];
            // Creo un movimiento de salida 
            $objMovSalida = new Model_Move();
            $objMovSalida->typeMoveStore = self::TYPE_MOVE_SALIDA;
            //$objMovSalida->moveReference = "Por eliminación ";
            $objMovSalida->idTypeMove = 4;
            $objMovSalida->status = self::STATUS_ACTIVE;
            $objMovSalida->idStore= $ids;
            $objMovSalida->save();
            $product = ORM::factory('products')->where('idProduct', '=', $idp)
                    ->find();
            $idMove = $objMovSalida->idMove;
            $objMoveDetail = new Model_Movedetail();
            $objMoveDetail->idMove = $idMove;
            $objMoveDetail->status = self::STATUS_ACTIVE;
            $stock = ORM::factory('stock')->where('idProduct','=',$idp)
                    ->and_where('idStore','=',$ids)->find();
            $objStock = new Model_Stock();           
            if ($product->haveDetail == self::STATUS_ACTIVE) {
                $productdetail = ORM::factory('productdetail')->where('idProduct', '=', $idp)
                                ->and_where('idProductDetail', '=', $iddp)->find();
                $objMoveDetail->idProductDetail = $iddp;
                $objMoveDetail->amount = $productdetail->amount;
                $objStock->amount = $objStock->amount - $productdetail->amount; 
            } else {
                $lot = ORM::factory('lot')->where('idProduct', '=', $idp)
                                ->and_where('idLot', '=', $iddp)->find();               
                $objMoveDetail->idLot = $iddp;
                $objMoveDetail->amount = $lot->amount;
                $objStock->amount = $objStock->amount - $lot->amount; 
            }
            $objStock->save();
            $objMoveDetail->save();
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }


        $this->fnResponseFormat($a_response);
    }

}