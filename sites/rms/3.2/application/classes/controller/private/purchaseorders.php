<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Purchaseorders extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->checkCashBox();
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('purchaseorders', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('purchaseorders', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $purchaseorders_view = new View('purchase/purchaseorders');
        $brand = DB::select(array('idBrand', 'id'), array('brandName', 'name'))
                        ->from('rms_brands')
                        ->and_where('status', '=', 1)
                        ->execute()->as_array();
        $category = DB::select(array('idCategory', 'id'), array('categoryName', 'name'), array('codigo', 'codigo'))
                        ->from('rms_category')
                        ->where('idSuperCategory', 'IS', NULL)
                        ->and_where('status', '=', 1)
                        ->execute()->as_array();
        $purchaseorders_view->brand = $brand;
        $purchaseorders_view->category = $category;
        $purchaseorders_view->a_currency = $this->a_TYPE_CURRENCY();
        $purchaseorders_view->a_unitmeasures = $this->a_UNIT_MEASURES();
        $purchaseorders_view->a_purchase_documents = $this->fnGetClassVariable(self::VAR_NAME_PURCHASE_DOCUMENTS);
        $purchaseorders_view->a_purchaseorder_payment = $this->fnGetClassVariable(self::VAR_NAME_PURCHASEORDER_PAYMENT);
        $purchaseorders_view->documentType = self::DT_ORDENCOMPRA;
        $purchaseorders_view->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $purchaseorders_view->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $purchaseorders_view->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $purchaseorders_view->bTagGenerator = $this->getOptionValue(self::INVENTORY_TAG_GENERATOR);
        $this->template->content = $purchaseorders_view;
    }

    public function action_getSupplier() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':active' => self::STATUS_ACTIVE
            );
            $select_supplier = DB::select(
                                    's.idSupplier', array(DB::expr("CONCAT(s.name,' - ',s.code)"), 'supplier'), 's.address'
                            )
                            ->from(array('rms_supplier', 's'))
                            ->where(DB::expr("CONCAT(s.name,'-',s.code)"), 'like', ':search')
                            ->and_where('s.status', '=', ':active')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $a_response['data'] = $select_supplier;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProduct() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':active' => self::STATUS_ACTIVE
            );
            $o_select_product = DB::select(
                                    'p.idProduct', array(DB::expr("CONCAT(p.productName,' - ',p.aliasProduct,' - ',b.brandNameCommercial)"), 'product'), 'p.productUnitMeasures'
                            )
                            ->from(array('rms_products', 'p'))
                            ->join(array('rms_brands', 'b'))->on('b.idBrand', '=', 'p.idBrand')
                            ->where(DB::expr("CONCAT(p.productName,' ',p.aliasProduct)"), 'LIKE', ':search')
                            ->and_where('p.status', '=', ':active')
                            ->and_where('b.status', '=', ':active')
                            ->parameters($parameters)
                            ->execute()->as_array();

            $a_response['data'] = $o_select_product;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createPurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $post_purchaseOrders = Validation::factory($this->request->post())
                    ->rule('header', 'not_empty')
                    ->rule('detail', 'not_empty');
            if (!$post_purchaseOrders->check()) {
                throw new Exception('Existen datos faltantes', self::CODE_SUCCESS);
            }
            $a_headerOrder = $this->request->post('header');
            $a_detailOrder = $this->request->post('detail');
            $a_noteOrder = $this->request->post('note');

           $idCashBox = $this->getSessionParameter(self::CASHBOX_ID);
           $id_User = $this->getSessionParameter(self::USER_ID);
           
            $arrayPurchaseOrders = array();
            $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
            $deliveryDate = DateTime::createFromFormat($format, $a_headerOrder['deliveryDate']);
            $purchaseDate = DateTime::createFromFormat($format, $a_headerOrder['purchaseOrderDate']);

            if (count($arrayPurchaseOrders) > 0) {
                $a_response['data'] = $arrayPurchaseOrders;
            } else {
                $a_response['data'] = $arrayPurchaseOrders;

                Database::instance()->begin();

                $purchaseorders = new Model_Purchaseorder();
                $purchaseorders->idSupplier = $a_headerOrder['idSupplier'];
                $purchaseorders->idUser = $id_User;
                $purchaseorders->deliveryAddress = $a_headerOrder['deliveryAddress'];
                $purchaseorders->purchaseDocument = $a_headerOrder['purchaseDocuments'];
                $purchaseorders->serie = $a_headerOrder['serie'];
                $purchaseorders->correlative = $a_headerOrder['correlative'];
                $purchaseorders->methodPayment = $a_headerOrder['payment_type'];
                $purchaseorders->currency = $a_headerOrder['currency_type'];
                $purchaseorders->datePurchaseOrder = DateHelper::fnPHPDate2MySQLDatetime($purchaseDate);
                $purchaseorders->dateDeliveryOrder = DateHelper::fnPHPDate2MySQLDatetime($deliveryDate);
                $purchaseorders->notes = $a_noteOrder;
                $purchaseorders->currency = self::CURRENCY_SOLES;
                $purchaseorders->statusPurchase = self::PURCHASE_ORDER_STATUS_EMITIDO;
                $purchaseorders->documentType = self::DT_ORDENCOMPRA;
                $purchaseorders->idCashBox = $idCashBox;


                if ($this->getOptionValue(self::CORRELATIVO_IMPRESION) == true) {
                    $a_serieUpdate = $this->getNextCorrelative(self::DT_ORDENCOMPRA);
                } else {
                    $a_serieUpdate = $this->getNextCorrelative(self::DT_DEFAULT_CORRELATIVE);
                }
                $purchaseorders->nrodocument = $a_serieUpdate->correlative;

                $purchaseorders->save();

                $totalAmountWithTax = 0;
                $totalAmountWithOutTax = 0;
                $totalTax = 0;
                $totalAmount = 0;

                foreach ($a_detailOrder as $key => $a_detailPurchaseOrder) {
                    $detailpurchaseorder = new Model_Purchaseorderdetail();
                    $detailpurchaseorder->idPurchaseOrder = $purchaseorders->idPurchaseOrder;
                    $detailpurchaseorder->idProduct = $a_detailPurchaseOrder['idProduct'];
                    $detailpurchaseorder->quantity = $a_detailPurchaseOrder['quantity'];
                    $detailpurchaseorder->totalUnit = $a_detailPurchaseOrder['amountUnit'];
                    $detailpurchaseorder->totalAmount = $a_detailPurchaseOrder['amountTotal'];
                    $detailpurchaseorder->save();
                    if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                        $detailpurchaseorder->taxPercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        $detailpurchaseorder->save();
                        $detailpurchaseorder->totalAmountWithOutTax = ($detailpurchaseorder->totalAmount / 1.18);
                        $detailpurchaseorder->totalTax = ($detailpurchaseorder->totalAmount - $detailpurchaseorder->totalAmountWithOutTax);
                        $detailpurchaseorder->totalAmountWithTax = ($detailpurchaseorder->quantity * $detailpurchaseorder->totalUnit);
                    } else {
                        $detailpurchaseorder->taxPercentage = 0;
                        $detailpurchaseorder->totalTax = 0;
                    }
                    $detailpurchaseorder->save();
                    $totalTax = $totalTax + $detailpurchaseorder->totalTax;
                    $totalAmountWithOutTax = $totalAmountWithOutTax + $detailpurchaseorder->totalAmountWithOutTax;
                    $totalAmountWithTax = $totalTax + $totalAmountWithOutTax;
                    $totalAmount = $totalAmount + $a_detailPurchaseOrder['amountTotal'];
                }

                if ($a_headerOrder['purchaseDocuments'] == self::OC_FACTURA || $a_headerOrder['purchaseDocuments'] == self::OC_NOTACREDITO || $a_headerOrder['purchaseDocuments'] == self::OC_NOTADEBITO || $a_headerOrder['purchaseDocuments'] == self::OC_TICKETFACTURA) {
                    $purchaseorders->taxPercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                    $purchaseorders->totalAmountWithOutTax = $totalAmountWithOutTax;
                    $purchaseorders->totalAmountWithTax = $totalAmountWithTax;
                    $purchaseorders->totalTax = $totalTax;
                    $purchaseorders->totalAmount = $totalAmount;
                } else {
                    $purchaseorders->taxPercentage = 0;
                    $purchaseorders->totalTax = 0;
                    $purchaseorders->totalAmountWithOutTax = 0;
                    $purchaseorders->totalAmountWithTax = 0;
                    $purchaseorders->totalAmount = $totalAmount;
                }
                $this->createHistory($purchaseorders->idPurchaseOrder);
                $purchaseorders->save();
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollBack();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
         
        }
        $this->fnResponseFormat($a_response);
    }

    //Creando el registro del historial de las ordenes de compra.
    public function createHistory($data_purchaseorder) {
        $id_User = $this->getSessionParameter(self::USER_ID);

        $historypurchase = New Model_Historypurchase();
        $data_purchaseorders['idPurchaseOrder'] = $data_purchaseorder;
        $data_purchaseorders['status'] = self::PURCHASE_ORDER_STATUS_EMITIDO;
        $data_purchaseorders['dateHistoryPurchase'] = DateHelper::getFechaFormateadaActual();
        $data_purchaseorders['idUser'] = $id_User;
        $historypurchase->saveH($data_purchaseorders);
    }

    public function action_getDetailPurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idp', 'not_empty');
            if ($post_idPurchaseOrder->check()) {

                $data_purchaseorder = $post_idPurchaseOrder->data();

                $parameter = array(
                    ':idPurchaseOrder' => $data_purchaseorder['idp']
                );

                $purchaseorder = DB::select(
                                        's.name', 's.service', 's.address', 'po.deliveryAddress', 'po.datePurchaseOrder', 'po.datePurchaseOrder', 'po.creationDate', array(DB::expr("CONCAT(pro.productName,' - ',pro.aliasProduct)"), 'description'), 'pod.idProduct', 'po.productUnitMeasures'
                                )
                                ->from(array('rms_purchaseorder', 'po'))
                                ->join(array('rms_purchaseorderdetail', 'pod'))->on('po.idPurchaseOrder', '=', 'pod.idPurchaseOrder')
                                ->join(array('rms_products', 'p'))->on('pod.idProduct', '=', 'p.idProduct')
                                ->join(array('rms_supplier', 's'))->on('po.idSupplier', '=', 's.idSupplier')
                                ->where('po.idPurchaseOrder', '=', ':idPurchaseOrder')
                                ->parameters($parameter)
                                ->execute()->as_array();

                $a_response ['data'] = $purchaseorder;
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>