<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of category
 *
 * @author Yime
 */
class Controller_Private_Category extends Controller_Private_Admin {

    public function action_index() {
         $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('category', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('category', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $inventory_category = new View('inventory/category');
        $inventory_category->bTagGenerator = $this->getOptionValue(self::INVENTORY_TAG_GENERATOR);
        $this->template->content = $inventory_category;
    }

    public function action_createNewCategory() {
        $a_response = $this->json_array_return;
        try {
            $idcp = $this->request->post('idc');
            $o_post_category = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty');
            if ($o_post_category->check()) {
                $data_post_category = $o_post_category->data();
                Database::instance()->begin();
                if ($this->getOptionValue(self::INVENTORY_TAG_GENERATOR)) {
                    $tmp = DB::select(db::expr('COUNT(idCategory) AS c ,  COUNT(idSuperCategory) AS s'))
                            ->from('rms_category')
                            ->where('codigo', '=', $data_post_category['codigo'])
                            ->and_where('status', '=', self::STATUS_ACTIVE);
                    $obj_category = new Model_Category();
                    if ($idcp != 0) {
                        $tmp->and_where('idSuperCategory', '=', $idcp);
                    }
                    $tmp = $tmp->as_object()->execute()->current();
                    $c = $tmp->c; // Numero de categorias
                    $s = $tmp->s; // Numero de subcategorias
                    if (($c - $s) == 0) { // Quiere decir que no existe un codigo en esa categoria o sub
                        $obj_category->categoryName = $data_post_category['name'];
                        $obj_category->codigo = $data_post_category['codigo'];
                        $obj_category->status = self::STATUS_ACTIVE;
                        if ($idcp != 0 && $s == 0) {
                            $obj_category->idSuperCategory = $idcp;
                            $obj_category->save();
                        }
                        if ($idcp != 0 && $s > 0) {
                            throw new Exception('La categoria ya ha sido registrada con ese codigo', self::CODE_SUCCESS);
                        }
                        if ($idcp == 0) {
                            $obj_category->save();
                            $obj_cateory_series = new Model_Categoryseries();
                            $obj_cateory_series->idCategory = $obj_category->idCategory;
                            $obj_cateory_series->codeCategory = $obj_category->codigo;
                            $obj_cateory_series->correlativeCategory = 1;
                            $obj_cateory_series->state = self::STATUS_ACTIVE;
                            $obj_cateory_series->sequence_increment = 1;
                            $obj_cateory_series->save();
                        }
                    } else {
                        throw new Exception('La categoria ya ha sido registrada con ese codigo', self::CODE_SUCCESS);
                    }
                }else{
                    $obj_category = new Model_Category();
                    $obj_category->categoryName = $data_post_category['name'];
                    $obj_category->codigo = $data_post_category['codigo'];
                    $obj_category->status = self::STATUS_ACTIVE;
                    if ($idcp != 0) {
                        $obj_category->idSuperCategory = $idcp;                        
                    }
                    $obj_category->save();
                }
                Database::instance()->commit();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getCategoryById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_category = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($o_post_id_category->check()) {
                $data_post_id_category = $o_post_id_category->data();
                $obj_category = New Model_Category($data_post_id_category['id']);
                $sc = array();
                if ($obj_category->idSuperCategory == null) {
                    $resp['haveSuperCategory'] = self::STATUS_DEACTIVE;
                } else {
                    $resp['haveSuperCategory'] = self::STATUS_ACTIVE;
//                    $obj_super_category=new Model_Category($obj_category->idSuperCategory);
                    $obj_super_category = DB::select(array('categoryName', 'superCategoryName'))
                                    ->from('rms_category')
                                    ->where('idCategory', '=', ':IDSUPER')
                                    ->param(':IDSUPER', $obj_category->idSuperCategory)->execute()->as_array();
                    $sc = $obj_super_category[0];
                }
                $a_response['data'] = array_merge($resp, $sc, $obj_category->as_array());
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_updateNewCategory() {
        $a_response = $this->json_array_return;
        try {
            $o_post_category = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('name', 'not_empty');
            if ($o_post_category->check()) {
                $data_post_category = $o_post_category->data();
//                $tmp = DB::select(db::expr('COUNT(idCategory) AS c ,  COUNT(idSuperCategory) AS s'))
//                        ->from('rms_category')
//                        ->where('codigo', '=', $data_post_category['codigo'])
//                        ->and_where('status', '=', self::STATUS_ACTIVE)
//                        ->and_where('idCategory', '<>', $data_post_category['id']);
                $obj_category = new Model_Category($data_post_category['id']);
//                $tmp = $tmp->as_object()->execute()->current();
//                $c = $tmp->c; // Numero de categorias
//                $s = $tmp->s; // Numero de subcategorias
//                if (($c - $s) == 0) { // Quiere decir que existe una categoria o sub
                $obj_category->categoryName = $data_post_category['name'];
                $obj_category->codigo = $data_post_category['codigo'];
                $obj_category->status = self::STATUS_ACTIVE;
                $obj_category->save();
            } else
                throw new Exception('La categoria ya ha sido registrada con ese codigo', self::CODE_SUCCESS);
//            } else {
//                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
//            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteCategory() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_category = Validation::factory($this->request->post())
                    ->rule('idc', 'not_empty');
            if ($o_post_id_category->check()) {
                $data_post_id_category = $o_post_id_category->data();
                $obj_category = New Model_Category($data_post_id_category['idc']);
                $obj_category->status = self::STATUS_DEACTIVE;
                $obj_category->save();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listCategoryXML() {
        $this->auto_render = FALSE;
        try {
            $node = (integer) $this->request->post('nodeid');
            $n_lvl = (integer) $this->request->post('n_level');
            $SQL_LEAF_NODES = 'SELECT cl.idSuperCategory FROM rms_category AS c LEFT JOIN rms_category cl ON c.idCategory=cl.idSuperCategory WHERE cl.idCategory IS NOT NULL AND cl.status=' . self::STATUS_ACTIVE . ' ORDER BY cl.idCategory';
            $results = DB::query(Database::SELECT, $SQL_LEAF_NODES)->execute()->as_array();
            $leaf_nodes = array();

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            foreach ($results as $r) {
                $leaf_nodes[$r['idSuperCategory']] = $r['idSuperCategory'];
            }

            header("Content-type: text/xml;charset=utf-8");
            $et = ">";
            echo "<?xml version='1.0' encoding='utf-8'?$et\n";
            echo "<rows>";
            echo "<page>1</page>";
            echo "<total>1</total>";
            echo "<records>1</records>";
            if ($node > 0) {
                $wh = 'rms_category.idSuperCategory=' . $node . ' AND rms_category.status=' . self::STATUS_ACTIVE;
                $n_lvl = $n_lvl + 1;
            } else {
                $wh = 'ISNULL(rms_category.idSuperCategory) AND rms_category.status=' . self::STATUS_ACTIVE;
            }
            if ($this->getOptionValue(self::INVENTORY_TAG_GENERATOR)) {
                $NEXT_NODE = "SELECT rms_category.idCategory, rms_category.codigo,rms_category.categoryName,rms_category.idSuperCategory,p.fullName,rms_category.status,DATE_FORMAT(rms_category.creationDate,'$s_date_format $s_time_format') creationDate " .
                        "FROM rms_category " .
                        " INNER JOIN bts_user u ON u.idUser=rms_category.userCreate " .
                        "INNER JOIN `bts_person` p ON p.`idPerson`=u.`idUser` " .
                        "WHERE " . $wh . " ORDER BY rms_category.idCategory";
            } else {
                $NEXT_NODE = "SELECT rms_category.idCategory,rms_category.categoryName,rms_category.idSuperCategory,p.fullName,rms_category.status,DATE_FORMAT(rms_category.creationDate,'$s_date_format $s_time_format') creationDate " .
                        "FROM rms_category " .
                        " INNER JOIN bts_user u ON u.idUser=rms_category.userCreate " .
                        "INNER JOIN `bts_person` p ON p.`idPerson`=u.`idUser` " .
                        "WHERE " . $wh . " ORDER BY rms_category.idCategory";
            }


            $results = DB::query(Database::SELECT, $NEXT_NODE)->execute()->as_array();

            foreach ($results as $row) {
                #VALIDACIONES
                if (!$row['idSuperCategory'])
                    $valp = 'NULL';
                else
                    $valp = $row['idSuperCategory'];
                if (array_search($row['idCategory'], $leaf_nodes) > 0)
                    $leaf = 'false';
                else
                    $leaf = 'true';




//                $add_option = htmlspecialchars('<button rel="'.$row['idCategory'].'">'.__("Agregar").'</button>');
                $add_option = htmlspecialchars('<a class="add" style="cursor: pointer;" data-id="' . $row['idCategory'] . '" title=' . __('Agregar Sub Cat.') . ' >' . __("Agregar") . '</a>');
                $edit_option = htmlspecialchars('<a class="edit" style="cursor: pointer;" data-id="' . $row['idCategory'] . '" title=' . __('Editar') . ' >' . __("Editar") . '</a>');
                $delete_option = htmlspecialchars('<a class="trash" style="cursor: pointer;" data-id="' . $row['idCategory'] . '" title=' . __('Eliminar') . ' >' . __("Eliminar") . '</a>');
//                $edit_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-edit" style="cursor: pointer;" rel="' . $row['idCategory'] . '" title="' . __('Editar') . '" ></a>');
//                $delete_option = htmlspecialchars('<a class="ui-jqgrid-cell-ico ui-jqgrid-ico-trash" style="cursor: pointer;" href="/admin/menu/removeMenu" rel="' . $row['idCategory'] . '" title="' . __("Eliminar") . '" ></a>');

                $options = $add_option . ' ' . $edit_option . ' ' . $delete_option;

                echo "<row>";
                echo "<cell>" . $row['idCategory'] . "</cell>";
                if ($this->getOptionValue(self::INVENTORY_TAG_GENERATOR)) {
                    echo "<cell>" . $row['codigo'] . " | " . __($row['categoryName']) . "</cell>";
                } else {
                    echo "<cell>" . $row['categoryName'] . "</cell>";
                }
                echo "<cell>" . __($row['fullName']) . "</cell>";
                echo "<cell>" . $row['status'] . "</cell>";
                echo "<cell>" . $row['creationDate'] . "</cell>";
                echo "<cell>" . $options . "</cell>";
                echo "<cell>" . $n_lvl . "</cell>";
                echo "<cell><![CDATA[" . $valp . "]]></cell>";
                echo "<cell>" . $leaf . "</cell>";
                echo "<cell>false</cell>";
                echo "</row>";
            } echo "</rows>";
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function action_getSubCategoryById() {
        $a_response = $this->json_array_return;
        try {

            $o_post_id_category = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($o_post_id_category->check()) {
                $obj_category = New Model_Category($o_post_id_category['id']);
                
                if ($this->getOptionValue(self::INVENTORY_TAG_GENERATOR)) {
                    $obj_super_category = DB::select(array('idCategory', 'idCategory'), array('codigo', 'codigo'), array('categoryName', 'superCategoryName'), array('idSuperCategory', 'idSuperCategory'))->from('rms_category')->where('idSuperCategory', '=', ':IDSUPER')
                                    ->param(':IDSUPER', $obj_category->idCategory)->execute()->as_array();

                    
                        $correlative = $this->getSerieCorrelativeByCategory(trim($o_post_id_category['id']));
                        $next_correlative = $correlative['data']->correlative+1;
                        $a_response['correlative'] = $this->zerofill($next_correlative, 3);

                } else {
                    $obj_super_category = DB::select(array('idCategory', 'idCategory'), array('categoryName', 'superCategoryName'), array('idSuperCategory', 'idSuperCategory'))->from('rms_category')->where('idSuperCategory', '=', ':IDSUPER')
                                    ->param(':IDSUPER', $obj_category->idCategory)->execute()->as_array();
                }
                
                $a_response['data'] = $obj_super_category;
                
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function getSerieCorrelativeByCategory($idCategory) {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;

        try {
            $aResponse['data'] = $serieCorrelative = DB::select(array('c_s.correlativeCategory', 'correlative'))
                            ->from(array('rms_category_series', 'c_s'))
                            ->where('c_s.idCategory', '=', $idCategory)
                            ->and_where('c_s.state', '=', self::STATUS_ACTIVE)->as_object()->execute()->current();
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->handlingError($exc);
        }
        return $aResponse;
    }

    function zerofill($entero, $largo) {
        // Limpiamos por si se encontraran errores de tipo en las variables
        $entero = (int) $entero;
        $largo = (int) $largo;

        $relleno = '';

        /**
         * Determinamos la cantidad de caracteres utilizados por $entero
         * Si este valor es mayor o igual que $largo, devolvemos el $entero
         * De lo contrario, rellenamos con ceros a la izquierda del número
         * */
        if (strlen($entero) < $largo) {
            $relleno = str_repeat('0', $largo - strlen($entero));
        }
        return $relleno . $entero;
    }

}
