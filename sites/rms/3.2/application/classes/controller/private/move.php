<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of move
 *
 * @author Yime
 */
class Controller_Private_Move extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('move', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('move', self::FILE_TYPE_JS));
        $inventory_move = new View('inventory/move');

        $inventory_move->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $this->template->content = $inventory_move;
    }

    public function action_listAllMoveGrid() {
        $this->auto_render = FALSE;
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $measures = $this->a_UNIT_MEASURES();
            $case_when_measures = "CASE ";
            foreach ($measures as $v) {
                $case_when_measures = $case_when_measures . " 
                    WHEN p.productUnitMeasures='" . $v['value'] . "' 
                        THEN '" . $v['display'] . "'
                    ";
            }
            $case_when_measures = $case_when_measures . " ELSE '" . __("No especificado") . "' END";

            $a_array_cols = array(
                "id" => "pe.idProduct",
                "aliasProduct" => "pe.aliasProduct",
                "serie" => "pd.serie_electronic",
                "typeMoveStore" => "m.typeMoveStore",
                "dateIncome" => "m.dateIncome",
                "dateOutcome" => "m.dateOutcome",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);

//            $s_tables_join = 'rms_products p ' .
//                    'INNER JOIN rms_brands b ON p.idBrand=b.idBrand ' .
//                    'INNER JOIN rms_category c ON p.idCategory=c.idCategory ' .
//                    'INNER JOIN bts_user u ON u.idUser=p.userCreate ' .
//                    'INNER JOIN bts_person pe ON pe.idPerson=u.idUser ';     
            $s_tables_join = 'rms_products pe ' .
                    'INNER JOIN rms_product_detail pd ON pe.idProduct= pd.idProduct ' .
                    'INNER JOIN rms_movedetail md ON pd.idProductDetail=md.idProductDetail' .
                    'INNER JOIN rms_move m ON md.idMove=m.idMove' ;

            $s_where_conditions = "1=1 and p.status=" . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

}