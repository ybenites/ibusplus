<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of customers
 *
 * @author Jhonatan
 */
class Controller_Private_Customers extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
         $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('customers', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('customers', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $view_customers = new View('sales/customers');
        $view_customers->CLIENT_TYPE_PERSON = self::CLIENT_TYPE_PERSON;
        $view_customers->CLIENT_TYPE_COMPANY = self::CLIENT_TYPE_COMPANY;
        $view_customers->a_clients_type = $this->a_clients_type();
        $view_customers->a_person_document_type = $this->a_PERSON_DOCUMENT_TYPE();
        $o_model_dpto = new Model_Ubigeo();
        
        $view_customers->a_dpto = $o_model_dpto->where('idpais', '=', 51)
                        ->and_where('iddpto', '<>', 0)
                        ->and_where('idprovincia', '=', 0)
                        ->and_where('iddistrito', '=', 0)
                        ->find_all()->as_array();
        $this->template->content = $view_customers;
    }

    public function action_createCustomers() {

        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    //->rule('passanger_name', 'not_empty')
                    // ->rule('passanger_lastname', 'not_empty')
                    ->rule('passanger_mail', 'not_empty')
            // ->rule('passanger_documento', 'not_empty')
            ;
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $ret = array();
                $data = $post->data();
                $dni = $data['passanger_documento'];
                $code = $data['company_code'];
                $tipoDoc = $data['passanger_tdocumento'];
                $dpto = $this->zerofill($data['passanger_dpto'], 2);
//                $prov = $this->zerofill($data['passanger_prov'], 2);
//                $dst = $this->zerofill($data['passanger_dst'], 2);
                $micodigo = (int) ('51' . $dpto . '00' . '00');


                if ($data['typeClient'] == self::CLIENT_TYPE_PERSON) {
                    $tmp = DB::select(db::expr('COUNT(rms_customers.idCustomers) AS c'))
                            ->from('rms_customers')
                            ->join('bts_person')
                            ->on('rms_customers.idPerson', '=', 'bts_person.idPerson')
                            ->where('bts_person.document', '=', $dni)
                            ->and_where('bts_person.idDocumentType', '=', $tipoDoc);

                    if ($data['id_customers'] == NULL) {
                        $o_model_person = new Model_Person();
                        $oModelCustomers = new Model_Customers();
                    } else {
                        $o_model_person = new Model_Person($data['id_person']);
                        $oModelCustomers = new Model_Customers($data['id_customers']);
                        $tmp = $tmp->and_where('rms_customers.idperson', '<>', $data['id_person']);
                    }
                    //Ejecuto mi consulta de DNI
                    $tmp = $tmp->as_object()->execute()->current();
                    if ($tmp->c > 0) {
                        $a_response['code'] = self::CODE_ERROR;
                        $a_response['msg'] = __("El DNI ya se encuentra registrado");
                    } else {
                        //para person
                        $o_model_person->fName = $data['passanger_name'];
                        $o_model_person->lName = $data['passanger_lastname'];
                        $o_model_person->fullName = $data['passanger_name'] . ' ' . $data['passanger_lastname'];
                        $o_model_person->sex = $data['passanger_sexo'];
                        $o_model_person->idDocumentType = $data['passanger_tdocumento'];
                        $o_model_person->document = $data['passanger_documento'];
                        $o_model_person->address2 = $data['passanger_dir1'];
                        $o_model_person->idUbigeo = $micodigo;
                        
                        
                        $o_model_person->phone1 = $data['passanger_phone'];
                        $o_model_person->cellPhone = $data['passanger_cellphone'];
                        $o_model_person->email = $data['passanger_mail'];
                        $o_model_person->type = self::USER_TYPE_CUSTOMERS;
                        $o_model_person->save();

                        //para tabla customers
                        $oModelCustomers->idPerson = $o_model_person->idPerson;
                        $oModelCustomers->status = self::STATUS_ACTIVE;
                        $oModelCustomers->save();

                        $ret = array('idCustomers' => $oModelCustomers->idCustomers, 'customers' => $o_model_person->fullName, 'typeCustomers' => self::CLIENT_TYPE_PERSON);
                    }
                } else if ($data['typeClient'] == self::CLIENT_TYPE_COMPANY) {
                    $tmp = DB::select(db::expr('COUNT(rms_customers.idCustomers) AS c'))
                            ->from('rms_customers')
                            ->join('rms_customerscompany')
                            ->on('rms_customers.idCustomersCompany', '=', 'rms_customerscompany.idCustomersCompany')
                            ->where('rms_customerscompany.code', '=', $code)
                            ->and_where('rms_customerscompany.status', '=', self::STATUS_ACTIVE);
                    if ($data['id_customers'] == NULL) {
                        $o_model_company = new Model_Company();
                        $oModelCustomers = new Model_Customers();
                        $o_model_company_address = new Model_Address();
                    } else {
                        $o_model_company = new Model_Company($data['id_company']);
                        $oModelCustomers = new Model_Customers($data['id_customers']);
                        $o_model_company_address = new Model_Address($data['id_address']);
                        $tmp = $tmp->and_where('rms_customers.idCustomersCompany', '<>', $data['id_company']);
                    }
                    $tmp = $tmp->as_object()->execute()->current();

                    if ($tmp->c > 0) {
                        $a_response['code'] = self::CODE_ERROR;
                        $a_response['msg'] = __("El RUC ya se encuentra registrado");
                    } else {

                        //company
                        $o_model_company->name = $data['company_name'];
                        $o_model_company->code = $data['company_code'];

                        $o_model_company->status = self::STATUS_ACTIVE;
                        $o_model_company->save();

                        //customers
                        $oModelCustomers->idCustomersCompany = $o_model_company->idCustomersCompany;
                        $oModelCustomers->status = self::STATUS_ACTIVE;
                        $oModelCustomers->save();

                        //address
                        $o_model_company_address->idCustomersCompany = $o_model_company->idCustomersCompany;
                        $o_model_company_address->address = $data['passanger_dir1'];
                        $o_model_company_address->phone = $data['passanger_phone'];
                        $o_model_company_address->email = $data['passanger_mail'];
                        $o_model_company_address->cellphone = $data['passanger_cellphone'];
                        $o_model_company_address->status = self::STATUS_ACTIVE;
                        $o_model_company_address->save();

                        $ret = array('idCustomers' => $oModelCustomers->idCustomers, 'customers' => $o_model_company->name, 'typeCustomers' => self::CLIENT_TYPE_COMPANY);
                    }
                }
            }
            $a_response['data'] = $ret;
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listAllCustomers() {
        $this->auto_render = FALSE;
        try {
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');
            $s_type_client = $this->request->post('type_client');

            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search'));

            $a_array_cols = array(
                "id" => "cus.idCustomers",
                "name" => "IF(cus.idPerson IS NULL,cuscom.name,pe.fullName)",
                "document" => 'IF(cus.idPerson IS NULL,cuscom.code,pe.document)',
                "address" => "IF(cus.idPerson IS NULL,adr.address,pe.address2)",
                "phone" => "IF(cus.idPerson IS NULL,adr.phone,pe.phone1)",
                "cellphone" => "IF(cus.idPerson IS NULL,adr.cellphone,pe.cellPhone)",
                "mail" => "IF(cus.idPerson IS NULL,adr.email,pe.email)",
                //Consulto el UBIGEO
                "city" => "IF(cus.idPerson IS NULL,ub.descripcion,ub.descripcion)",
                "type" => "IF(cus.idPerson IS NULL,'" . self::CLIENT_TYPE_COMPANY . "','" . self::CLIENT_TYPE_PERSON . "')"
            );

            $a_array_search = array(
                "name" => "coalesce(cuscom.name,pe.fullName)",
                "document" => 'coalesce(cuscom.code,pe.document)'
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);


            if ($b_search_on == 'true') {
                if ($this->request->post('filters') != '') {
//BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));
                    $s_where_search = jqGridHelper::constructWhere($s_search_str, $a_array_search);
                } else {
//BÚSQUEDA SIMPLE POR COLUMNA

                    foreach ($this->request->post() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND " . str_ireplace('_', '.', $i_key) . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);

            $s_tables_join = 'rms_customers cus  
                            LEFT JOIN bts_person pe ON pe.idPerson = cus.idPerson
                            LEFT JOIN rms_customerscompany cuscom ON cus.idCustomersCompany = cuscom.idCustomersCompany 
                            LEFT JOIN rms_address adr ON cuscom.idCustomersCompany = adr.idCustomersCompany
                            LEFT JOIN rms_ubigeo ub ON pe.idUbigeo = ub.idUbigeo';

            $s_where_conditions = " cus.status=" . self::STATUS_ACTIVE;

            switch ($s_type_client) {
                case self::CLIENT_TYPE_PERSON:
                    $s_where_conditions .= " AND cus.idPerson > 0 ";
                    break;
                case self::CLIENT_TYPE_COMPANY:
                    $s_where_conditions .= " AND cus.idCustomersCompany > 0 ";
                    break;
                default :
                    break;
            }


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

    public function action_deleteCustomers() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_brand = Validation::factory($this->request->post())
                    ->rule('idb', 'not_empty');
            if ($o_post_id_brand->check()) {
                $data_post_id_brand = $o_post_id_brand->data();
                $obj_Customers = New Model_Customers($data_post_id_brand['idb']);
                $obj_Customers->status = self::STATUS_DEACTIVE;
                $obj_Customers->save();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getCustomersById() {
        $a_response = $this->json_array_return;
        try {
            $o_post_id_customers = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if ($o_post_id_customers->check()) {
                $data_post_id_customers = $o_post_id_customers->data();
                $obj_Customers = new Model_Customers($data_post_id_customers['id']); 
                if ($obj_Customers->idPerson > 0) {
                    $obj_Customers->person;
                    $i_ubi = $obj_Customers->person->idUbigeo;
                    $o_ubigeo = new Model_Ubigeo();
                    $ubigeo = $o_ubigeo->where("idUbigeo", '=', $i_ubi)->find()->as_array();
                    $a_response['ubi'] = $ubigeo;
                } else if ($obj_Customers->idCustomersCompany > 0) {
                    $i_comp = $obj_Customers->company->idCustomersCompany;
                    $o_address = new Model_Address();
                    $a_dirs = $o_address->where("idCustomersCompany", '=', $i_comp)->find()->as_array();
                    $a_response['rr'] = $a_dirs;
                }

                $a_response['data'] = $obj_Customers->as_array();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_ubigeoCustomers() {
        $post = $this->request->post();
        $id = $post['id'];
        $type = $post['des'];
        if ($type == 'provincia') {
            $elemento = DB::select(array('idprovincia', 'valor'), 'descripcion')
                            ->from('rms_ubigeo')
                            ->where('idpais', '=', 51)
                            ->and_where('iddpto', '=', $id)
                            ->and_where('idprovincia', '<>', 0)
                            ->and_where('iddistrito', '=', 0)
                            ->execute()->as_array();
        } else { //sino es distrito
            $elemento = DB::select(array('iddistrito', 'valor'), 'descripcion')
                            ->from('rms_ubigeo')
                            ->where('idpais', '=', 51)
                            ->and_where('iddpto', '=', $type)
                            ->and_where('idprovincia', '=', $id)
                            ->and_where('iddistrito', '<>', 0)
                            ->execute()->as_array();
        }

        $this->fnResponseFormat($elemento);
    }

    function zerofill($entero, $largo) {
// Limpiamos por si se encontraran errores de tipo en las variables
        $entero = (int) $entero;
        $largo = (int) $largo;

        $relleno = '';

        /**
         * Determinamos la cantidad de caracteres utilizados por $entero
         * Si este valor es mayor o igual que $largo, devolvemos el $entero
         * De lo contrario, rellenamos con ceros a la izquierda del número
         * */
        if (strlen($entero) < $largo) {
            $relleno = str_repeat('0', $largo - strlen($entero));
        }
        return $relleno . $entero;
    }

}

?>
