<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of customers
 *
 * @author Diana
 */
class Controller_Private_Orderform extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->checkCashBox();
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('orderform', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('orderform', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $view_ordersales = new View('sales/orderform');
        $view_ordersales->CLIENT_TYPE_PERSON = self::CLIENT_TYPE_PERSON;
        $view_ordersales->CLIENT_TYPE_COMPANY = self::CLIENT_TYPE_COMPANY;
        $view_ordersales->document = self::DT_NOTA_PEDIDO;
        $view_ordersales->cash = self::SALES_PAYMENT_CASH;
        $view_ordersales->creditCard = self::SALES_PAYMENT_CREDITCARD;
        $view_ordersales->a_clients_type = $this->a_clients_type();
        $view_ordersales->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $view_ordersales->a_person_document_type = $this->a_PERSON_DOCUMENT_TYPE();
        $view_ordersales->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_ordersales->aSeller = $this->getOptionValue(self::SO_ACTIVE_SELLER);
        $view_ordersales->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $view_ordersales->orderSalesPayment = $this->fnGetClassVariable(self::VAR_NAME_ORDERSALES_PAYMENT);
        $view_ordersales->bBarcodeReader = $this->getOptionValue(self::INVENTORY_BARCODE_READER);
        $view_ordersales->bTaxEnabled = $this->getOptionValue(self::SO_TAX_ENABLED);
        $view_ordersales->sTaxName = $this->getOptionValue(self::SO_TAX_NAME);
        $view_ordersales->fTax = $this->getOptionValue(self::SO_TAX_VALUE);
        $o_model_dpto = new Model_Ubigeo();
        $view_ordersales->a_dpto = $o_model_dpto->where('idpais', '=', 51)
                        ->and_where('iddpto', '<>', 0)
                        ->and_where('idprovincia', '=', 0)
                        ->and_where('iddistrito', '=', 0)
                        ->find_all()->as_array();
        $user_incharge = $this->getSessionParameter(self::USER_ID);
        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->as_array();
        $view_ordersales->store = $store;
        $v_userCode = DB::select(
                                array('us.idUser','id'), array('us.userName','nameUser')
                        )
                        ->from(array('bts_user','us'))
                        ->join(array('bts_group','gr'))->on('gr.idgroup','=','us.idgroup')
                        ->where('us.idgroup', '=', 2)
                        ->execute()->as_array();
        $view_ordersales->listUserCode =$v_userCode;
        
        $view_ordersales->creditCardPayment = $this->fnGetClassVariable(self::VAR_NAME_CREDITCARD_PAYMENT);
        $view_ordersales->salesPayment = $this->fnGetClassVariable(self::VAR_NAME_METHODPAYMENT);
        $view_ordersales->methodPayment = $this->getOptionValue(self::SO_PAYMENT_FOR_SALES);

        $this->template->content = $view_ordersales;
    }

    public function action_getClients() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':typeperson' => self::CLIENT_TYPE_PERSON,
                ':typecompany' => self::CLIENT_TYPE_COMPANY
            );
            $o_select_company = DB::select(
                                    'cus.idCustomers', array(DB::expr("CONCAT(IF(cus.idCustomersCompany IS NULL,pe.fullName, cuscom.name) ,' - ', IF(cus.idCustomersCompany IS NULL,pe.document, cuscom.code))"), 'customers'), array(DB::expr("IF(cus.idCustomersCompany IS NULL,:typeperson, :typecompany)"), 'type')
                            )
                            ->from(array('rms_customers', 'cus'))
                            ->join(array('bts_person', 'pe'), 'LEFT')->on('cus.idPerson', '=', 'pe.idPerson')
                            ->join(array('rms_customerscompany', 'cuscom'), 'LEFT')->on('cus.idCustomersCompany', '=', 'cuscom.idCustomersCompany')
                            ->where(DB::expr("CONCAT(pe.fullName, pe.document)"), 'LIKE', ':search')
                            ->or_where(DB::expr("CONCAT(cuscom.name, cuscom.code)"), 'LIKE', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $a_response['data'] = $o_select_company;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProduct() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
            );
            
            
            $user_incharge = $this->getSessionParameter(self::USER_ID);

        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->current();
     
            
            $o_select_product = DB::select(
                                    'p.idProduct'
                                    , 'stor.idStore'
                                    , array(DB::expr("CONCAT(p.productName,' - ', rmb.brandName, ' ',p.aliasProduct,' (',st.Amount,') - ',stor.storeName)"), 'product')
                                    , 'st.Amount'
                                    , 'p.haveDetail'
                                    , 'p.productPriceSell'
                            )
                            ->from(array('rms_stock', 'st'))
                            ->join(array('rms_products', 'p'))->on('st.idProduct', '=', 'p.idProduct')
                            ->join(array('rms_store', 'stor'))->on('st.idStore', '=', 'stor.idStore')
                            ->join(array('rms_brands', 'rmb'))->on('p.idBrand', '=', 'rmb.idBrand')
                            ->where(DB::expr("CONCAT(p.productName,' ',p.aliasProduct)"), 'LIKE', ':search')
                            ->and_where('p.status', '=', self::STATUS_ACTIVE)
                            ->and_where('stor.idStore', '=', $store['id'])
                            ->group_by('p.aliasProduct')
                            ->group_by('st.idStore')
                            ->parameters($parameters)
                            ->execute()->as_array();

            $a_response['data'] = $o_select_product;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function action_createSalesOrder() {
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('header', 'not_empty')
                    ->rule('detail', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $a_header = $this->request->post('header');
                $a_amountPaid = $this->request->post('amountPaid');
                $a_detail = $this->request->post('detail');
                $a_store = $this->request->post('idStore');
                $a_detail_product = $this->request->post('detail_product');
                $a_detail_product_l = $this->request->post('detail_product_l');
                $v_idUserCode = $this->request->post('userCode');
                $a_methodPayment = $this->request->post('methodPayment');
                $payment = $this->request->post('payment');
                
                $arrayOrderSales = array();   
                $idCashBox = $this->getSessionParameter(self::CASHBOX_ID);
                $id_User = $this->getSessionParameter(self::USER_ID);
//                
//                $idCashBox = DB::select(
//                                array('ca.idCashBox','idCashBox')
//                                )
//                         ->from(array('rms_cashbox','ca'))
//                         ->where('ca.status','=',self::CASHBOX_STATUS_OPENED)
//                         ->and_where(DB::expr("DATE_FORMAT(ca.openDate,'%Y-%m-%d')"),'=',DateHelper::getfechaActual())
//                         ->and_where('ca.idUser','=',$id_User)
//                         ->execute()->current();
                
                $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
                //Fechas 
                $orderSalesDate = DateTime::createFromFormat($format, $a_header['orderSalesDate']);
                $expirationDate = DateTime::createFromFormat($format, $a_header['expirationDate']);
                $f_total_amount = 0;

                if ($this->getOptionValue(self::CORRELATIVO_IMPRESION) == true) {
                    $a_serieUpdate = $this->getNextCorrelative(self::DT_NOTA_PEDIDO);
                } else {
                    $a_serieUpdate = $this->getNextCorrelative(self::DT_DEFAULT_CORRELATIVE);
                }

                if (count($arrayOrderSales) > 0) {
                    $a_response['data'] = $arrayOrderSales;
                } else {
                    $a_response['data'] = $arrayOrderSales;
                    Database::instance()->begin();
                    // Reserva 
                    $obj_SalesOrder = new Model_Salesorder;
                    $obj_SalesOrder->idCustomers = $a_header['idCustomers'];
                    $obj_SalesOrder->idUser = $id_User;
                    $obj_SalesOrder->idUserCode = $v_idUserCode;
                    $obj_SalesOrder->status = self::SALES_ORDER_STATUS_RESERVED;
                    $obj_SalesOrder->orderSalesDate = DateHelper::fnPHPDate2MySQLDatetime($orderSalesDate);
                    $obj_SalesOrder->expirationDate = DateHelper::fnPHPDate2MySQLDatetime($expirationDate);
                    $obj_SalesOrder->serie = $a_serieUpdate->serie;
                    $obj_SalesOrder->correlative = $a_serieUpdate->correlative;
                    $obj_SalesOrder->documentType = self::DT_NOTA_PEDIDO;
                    $obj_SalesOrder->idCashBox = $idCashBox;
                    $obj_SalesOrder->save();

                    //Crear movimiento 
                    $obj_move = new Model_Move();
                    $obj_move->typeMoveStore = self::TYPE_MOVE_RESERVA;
                    $obj_move->moveReference = 'VENTA_RESERVA';
                    $obj_move->idCustomer = $a_header['idCustomers'];
                    $obj_move->idStore = $a_store;
                    $obj_move->status = self::STATUS_ACTIVE;
                    $obj_move->currency = self::CURRENCY_SOLES;
                    $obj_move->save();
                    $id_Move = $obj_move->idMove;

                    foreach ($a_detail as $key => $a_item) {
                        //Actualizo el stock de cada producto                        
                        $stock = ORM::factory('stock')
                                        ->where('idProduct', '=', $a_item['id'])
                                        ->and_where('idStore', '=', $a_store)->find();
                        if ($stock->loaded()) {
                            $stock->amount = $stock->amount - $a_item['quantityOut'];
                            $stock->save();
                        }

                        if ($a_item['haveDetail'] == self::STATUS_ACTIVE) {
                            //PRODUCTO DETALLE
                            foreach ($a_detail_product as $pd) {
                                // Creo reserva detalle 
                                $o_ordersalesdetail = new Model_Salesorderdetail();
                                $o_ordersalesdetail->idSalesOrder = $obj_SalesOrder->idSalesOrder;
                                $o_ordersalesdetail->idStore = $a_store;
                                $o_ordersalesdetail->quantity = 1;
                                $o_ordersalesdetail->unitPrice = $a_item['unitPriceOut'];
                                $o_ordersalesdetail->totalPrice = $a_item['subTotalOut'];

                                // Actualizo producto detalle 
                                $obj_product_detail = new Model_Productdetail($pd);

                                // Creo movimiento detalle 
                                $obj_movedetail = new Model_Movedetail();
                                $obj_movedetail->idMove = $id_Move;
                                $obj_movedetail->status = self::STATUS_ACTIVE;
                                $obj_movedetail->idProductDetail = $obj_product_detail->idProductDetail;
                                $obj_movedetail->amount = 1;
                                $obj_movedetail->unitPrice = $a_item['unitPriceOut'];
                                $obj_movedetail->save();

                                if ($obj_product_detail->idProduct == $a_item['id']) {
                                    $o_ordersalesdetail->idProductDetail = $obj_product_detail->idProductDetail;
                                    $obj_product_detail->amount = self::ZERO;
                                    $obj_product_detail->save();

                                    $obj_movedetail->idProductDetail = $obj_product_detail->idProductDetail;
                                    $obj_movedetail->save();
                                    $o_ordersalesdetail->save();
                                }
                            }
                        } 
                        else {
                            // LOTES
                            foreach ($a_detail_product_l as $pd) {
                                $o_ordersalesdetail = new Model_Salesorderdetail();
                                $o_ordersalesdetail->idSalesOrder = $obj_SalesOrder->idSalesOrder;
                                $o_ordersalesdetail->idStore = $a_store;
                                $o_ordersalesdetail->quantity = $pd['amount'];
                                $o_ordersalesdetail->unitPrice = $a_item['unitPriceOut'];
                                $o_ordersalesdetail->totalPrice = $a_item['subTotalOut'];
                                $o_ordersalesdetail->discount = $a_item['discountOut'];
                                $obj_product_lot = new Model_Lot($pd['id']);

                                if ($obj_product_lot->idProduct == $a_item['id']) {
                                    $obj_product_lot->amount = $obj_product_lot->amount - $pd['amount'];
                                    $obj_movedetail = new Model_Movedetail();
                                    $obj_movedetail->idMove = $id_Move;
                                    $obj_movedetail->status = self::STATUS_ACTIVE;
                                    $obj_movedetail->idLot = $pd['id'];
                                    $obj_movedetail->amount = $pd['amount'];
                                    $obj_movedetail->unitPrice = $a_item['unitPriceOut'];
                                    $obj_product_lot->save();
                                    $obj_movedetail->save();
                                    $o_ordersalesdetail->idLot = $obj_product_lot->idLot;
                                    $o_ordersalesdetail->save();
                                }
                            }
                        }
                        $f_total_amount = $f_total_amount + $a_item['subTotalOut'];
                    }

                    $obj_move->idSalesOrder = $obj_SalesOrder->idSalesOrder;
                    $obj_move->save();

//                    $obj_SalesOrder->totalAmount = $f_total_amount;
//                    $obj_SalesOrder->amountToPaid = $obj_SalesOrder->totalAmount - $obj_SalesOrder->amountPaid;
//                    $obj_SalesOrder->save();
                     
                    if ($a_methodPayment == self::SALES_PAYMENT_CASH) {
                        $obj_SalesOrder->methodPayment = self::SALES_PAYMENT_CASH;
                        $obj_SalesOrder->amountPaid = $a_amountPaid;
                        $obj_SalesOrder->totalAmount = $f_total_amount;
                        $obj_SalesOrder->amountToPaid = $obj_SalesOrder->totalAmount - $obj_SalesOrder->amountPaid;
                        $obj_SalesOrder->idCashBox = $idCashBox;
                        $obj_SalesOrder->save();
                        //relaciona la venta con la Aper de caja
                    } elseif ($a_methodPayment == self::SALES_PAYMENT_CREDITCARD) {
                        $paymentCard = new Model_Paymentcard();
                        $paymentCard->amount = $payment['amount_paymentCredit'];
                        $paymentCard->cardNumber = $payment['cardNumber'];
                        $paymentCard->operationNumber = $payment['operationNumber'];
                        $paymentCard->idTypeCard = $payment['typeCard'];
                        $paymentCard->status = self::SALES_STATUS_SOLD;
                        $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                        $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                        $paymentCard->save();
                        //relaciona la venta con la Aper de caja
                        $obj_SalesOrder->idCashBox = $idCashBox;
                        $obj_SalesOrder->idPaymentCard = $paymentCard->idPaymentCard;
                        $obj_SalesOrder->methodPayment = self::SALES_PAYMENT_CREDITCARD;
                        $obj_SalesOrder->save();
                    } 
                    
                    /* Creo Historial */
                    $a_SalesOrder = array();
                    $historySalesOrder = new Model_HistorySalesOrder();
                    $a_SalesOrder['idSalesOrder']=$obj_SalesOrder->idSalesOrder;
                    $a_SalesOrder['status']=  self::SALES_ORDER_STATUS_RESERVED;
                    $a_SalesOrder['dateHistorySalesOrder']=  DateHelper::getFechaFormateadaActual();
                    $a_SalesOrder['idUser']=$id_User; 
                    $historySalesOrder->saveHistorySalesOrder($a_SalesOrder);
                    /* Crear cuota */
                    $a_Shares = array();
                    $a_Shares['idSalesOrder'] = $obj_SalesOrder->idSalesOrder;
                    $a_Shares['status'] = self::SALES_ORDER_STATUS_SOLD;
                    $a_Shares['idUser'] = $id_User;
                    $a_Shares['totalAmount'] = $f_total_amount;
                    $a_Shares['amountPaid'] = $a_amountPaid;
                    $a_Shares['amountToPaid'] = $f_total_amount - $a_amountPaid;
                    
                    $o_model_shares =  new Model_Shares();
                    $o_model_shares->saveShares($a_Shares);
                    
                    Database::instance()->commit();
                }
            }
        } 
        catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getPrintOrderSales() {
        $this->auto_render = FALSE;
        try {
            $o_post_sales = Validation::factory($_GET)
                    ->rule('idSalesOrder', 'not_empty')
                    ->rule('document', 'not_empty');
            if (!$o_post_sales->check()) {
                throw new Exception('ERROR -' . __('Empty Data'), self::CODE_ERROR);
            } else {
                $i_ordersales_id = $this->request->query('idSalesOrder');
                $i_document = $this->request->query('document');

                $this->printDocument($i_ordersales_id, $i_document);
            }
        } catch (Exception $exc) {
            echo __('Impresión Inválida');
        }
    }

}

?>
