<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Controller_Private_Supportsales  extends Controller_Private_Admin implements Rms_Constants {
    
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('supportsales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('supportsales', self::FILE_TYPE_JS));
        $supportsales_view = new View('support/supportsales');
        $supportsales_view->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $supportsales_view->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $supportsales_view->bBarcodeReader = $this->getOptionValue(self::INVENTORY_BARCODE_READER);
        $supportsales_view->bTaxEnabled = $this->getOptionValue(self::SO_TAX_ENABLED);
        $supportsales_view->sTaxName = $this->getOptionValue(self::SO_TAX_NAME);
        $supportsales_view->fTax = $this->getOptionValue(self::SO_TAX_VALUE);
        
             
        $supportsales_view->a_year_report = $this->a_year_report();
        $supportsales_view->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        
        $this->template->content = $supportsales_view;
     
     }
     public function action_listSales(){
        $this->auto_render = false;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

             // FIltros       
            $nroSales= $this->request->post('nroSales');
            $datee= $this->request->post('date');
            $dateini= $this->request->post('dateini');
            $datefin= $this->request->post('datefin');
            $month= $this->request->post('month');
            $year= $this->request->post('year');

   
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);

            $a_salesDocument = $this->fnGetClassVariable(self::VAR_NAME_A_DOCUMENT_TYPE);

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);

            $array_cols = array(
                'idSales' => "rs.idSales",
                'serie' => "rs.serie",
                'correlative' => "LPAD(rs.correlative,8,'0')",
                'customers' => "IF(pe.fullName IS NULL,CONCAT(cco.name,' - ',cco.code),CONCAT(pe.fullName,' - ',pe.document))",
                'storeName' => "st.storeName",
                'documentP' => "
                   CASE 
                        WHEN rs.documentType = '" . self::DT_BOLETA . "' THEN '" . __($a_salesDocument[self::DT_BOLETA]['value']) . "' 
                        WHEN rs.documentType = '" . self::DT_FACTURA . "' THEN '" . __($a_salesDocument[self::DT_FACTURA]['value']) . "'              
                        WHEN rs.documentType = '" . self::DT_RECIBO . "' THEN '" . __($a_salesDocument[self::DT_RECIBO]['value']) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'document' => "                    
                    CASE 
                        WHEN rs.documentType = '" . self::DT_BOLETA . "' THEN '" . __($a_salesDocument[self::DT_BOLETA]['display']) . "' 
                        WHEN rs.documentType = '" . self::DT_FACTURA . "' THEN '" . __($a_salesDocument[self::DT_FACTURA]['display']) . "'              
                        WHEN rs.documentType = '" . self::DT_RECIBO . "' THEN '" . __($a_salesDocument[self::DT_RECIBO]['display']) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'tax' => "rs.totaltax",
                'totalPrice' => "rs.totalPrice",
                'paymentAmount' => "rs.paymentAmount",
                'returnCash' => "rs.returnCash",
                'salesDate' => "DATE_FORMAT(rs.salesDate, '$date_format $time_format')",
                'seller' => "se.fullName"
            );

            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if (isset($_REQUEST['filters'])) {
//BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($_REQUEST['filters']);
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
//BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($_REQUEST as $k => $v) {
                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = ' rms_sales rs' .
                    ' INNER JOIN rms_customers cu ON rs.idCustomers = cu.idCustomers' .
                    ' INNER JOIN rms_salesdetail sd ON rs.idSales = sd.idSales ' .
                    ' INNER JOIN rms_store st ON sd.idStore = st.idStore ' .
                    ' LEFT JOIN rms_customerscompany cco ON cu.idCustomersCompany = cco.idCustomersCompany' .
                    ' LEFT JOIN bts_person pe ON cu.idPerson = pe.idPerson' .
                    ' LEFT JOIN bts_person se ON rs.userCreate = se.idPerson';

            
            
            $grouping = "GROUP BY sd.idSales";

       
            $date_s= " ";
            $dateini_s= " ";
            $datefin_s= " ";
            $month_s= " ";
            $initiation = self::SALES_STATUS_SOLD;
            $where_conditions = " rs.status = '$initiation' ";       

            if ($nroSales != NULL) {
                $temp = explode('-', $this->request->post('nroSales'));
                $where_conditions .= " AND rs.serie LIKE '%" . trim($temp[0]) . "%'";
                $where_conditions .= " AND rs.correlative LIKE '%" . trim($temp[1]) . "%'  ";
            }

            if($datee!=''){
                $date_s = " AND DATE_FORMAT(rs.salesDate,'$date_format') ='". $datee."' ";
            }

            if($dateini!=''){            
                if($datefin!=''){
                       $dateini_s = " AND rs.salesDate BETWEEN STR_TO_DATE('" . $dateini. "', '$date_format') AND STR_TO_DATE('" . $datefin. " $time_limit', '$date_format $time_raw')";
                }else {
                        $datefin_s = " AND DATE_FORMAT(rs.salesDate, '$date_format') LIKE '" . $dateini . "' ";
                }    
            }else {
                if($datefin !='')
                {
                    $datefin_s = " AND DATE_FORMAT(rs.salesDate ,'$date_format') LIKE '" . $datefin. "' ";
                    
                }
            }
            if($month!=0){
                if($year != 0){
                    $month_s =  " AND MONTH(rs.salesDate) = " . $month . " AND YEAR(rs.salesDate) = " . $year. "";
                }
            }
            $where_conditions .= " $date_s $month_s $datefin_s $dateini_s";
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);
            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
            $assignmentSeries = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start, $grouping);
//
//            echo Database::instance()->last_query;
//            die();
//            
            
            header('Content-type: application/json');
            echo jQueryHelper::JSON_jQuery_encode(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentSeries, $array_cols, true), false);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
     }
     
     public function action_getDetalleSalesOrder() {
        $this->auto_render = false;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $idSalesOrder = $this->request->post('idSal');
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);

            if (!$sidx)
                $sidx = 1;

            $filterLud = "";

            if ($idSalesOrder != "") {
                $filterLud = "rs.idSales = '$idSalesOrder'";
            }

            $where_search = "";

            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idSales' => 'rsd.idSalesDetail',
                'aliasProduct' => 'pro.aliasProduct',
                'productName' => 'pro.productName',
                'lote' => "CASE
                WHEN lt.idLot IS NOT NULL 
                THEN CONCAT(lt.lotName,': ',lt.description)
                ELSE CONCAT(prd.serie_electronic ,': ',prd.description)
                END",
                'storeName' => 'st.storeName',
                'quantity' => 'rsd.quantity',
                'unitPrice' => 'rsd.unitPrice',
                'discount' => 'rsd.discount',
                'totalPrice' => 'rsd.totalPrice'
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = 'rms_sales rs' .
                    ' INNER JOIN rms_salesdetail rsd ON rs.idSales = rsd.idSales ' .
                    ' LEFT JOIN rms_lot lt ON rsd.idLot = lt.idLot ' .
                    ' INNER JOIN rms_store st ON rsd.idStore = st.idStore ' .
                    ' LEFT JOIN rms_product_detail prd ON rsd.idProductDetail = prd.idProductDetail ' .
                    ' INNER JOIN `rms_products` pro ON (pro.`idProduct` = prd.`idProduct` OR pro.idProduct = lt.idProduct)';

            $where_conditions = "" . $filterLud." AND (rsd.statusDetail IS NULL OR rsd.statusDetail != '".self::SALES_DETAIL_CANCELED."')";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);
            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
            $salesOrder = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            
//            print_r(Database::instance()->last_query);
//             die();
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $salesOrder, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
      // Cancelar un Detalle de la Venta
    public function action_deleteSalesDetail() {
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('idsd', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();
                
                // el id de sales
                Database::instance()->begin();
                //Actualizar el Detalle de la Venta
                $salesDetail = New Model_Salesdetail($data_salesDetail['idsd']);
                $salesDetail->statusDetail = self::SALES_DETAIL_CANCELED;
                
                
//                 Modificar la Venta
                $ids = $salesDetail->idSales;
                $sales = new Model_Sales($ids);
                $newTotal = $sales->totalPrice - $salesDetail->totalPrice; 
                $sales->totalPrice = $newTotal; 
                $sales->returnCash = $sales->returnCash - ($sales->paymentAmount - $newTotal);                                       
//                $a_item= DB::select('*'
//                                        , 'idStore'
//                                        , 'quantity'
//                                )
//                                ->from('rms_salesdetail')
//                                ->where('idSalesDetail', '=', $salesDetail->idSalesDetail)
//                                ->execute()->as_array();

                // print_r($dataDetail); die();
//                $a_item = $salesDetail;
                
//
//                foreach ($dataDetail as $key => $a_item) {

                    if ($salesDetail ->idProductDetail != '') {
                        $productdetail = ORM::factory('productdetail')->
                                        where('idProductDetail', '=', $salesDetail ->idProductDetail )->find();
                        
                        
                        $productdetail->amount = $productdetail->amount + $salesDetail ->quantity;
                        $productdetail->save();
                        //Actualizar Stock
                        $stock = ORM::factory('stock')->
                                        where('idProduct', '=', $productdetail->idProduct)
                                        ->and_where('idStore', '=',$salesDetail->idStore)->find();
                        $stock->amount = $stock->amount + $salesDetail->quantity;
                        $stock->save();

                        //Crear Detalle de Movimientos
                        $move_detail = new Model_Movedetail();
                        $o_move_detail = $move_detail->getMoveByIdDetailSale($salesDetail->idProductDetail, self::TYPE_MOVE_VENTA_DIRECTA);
                        $move_d = ORM::factory('movedetail')->
                                        where('idMoveDetail', '=', $o_move_detail->idMoveDetail)->find();
                        $move_d->amount = self::STATUS_DEACTIVE;
                        $move_d->status = self::STATUS_DEACTIVE;
                        $move_d->save();

                        //Actualizar Movimientos
                        $move = ORM::factory('move')->
                                        where('idMove', '=', $o_move_detail->idMove)->find();
                        $move->typeMoveStore = self::TYPE_MOVE_TRAS_SALIDA_CANCEL;
                        $move->save();
                    } else if ($salesDetail->idLot != '') {

                        $lote = ORM::factory('lot')->
                                        where('idLot', '=', $salesDetail->idLot)->find();
                        $lote->amount = $lote->amount + $salesDetail->quantity;
                        $lote->save();
                        //Actualizar Stock
                        $stock = ORM::factory('stock')
                                        ->where('idProduct', '=', $lote->idProduct)
                                        ->and_where('idStore', '=',$salesDetail->idStore)->find();
                        $stock->amount = $stock->amount + $salesDetail->quantity;
                        $stock->save();

                        //Actualizar Detalle de Movimientos
                        $o_move_detail = ORM::factory('movedetail')->
                                        where('idLot', '=', $salesDetail->idLot)->find();
                        $o_move_detail->status = self::STATUS_DEACTIVE;
                        $o_move_detail->save();

                        //Actualizar Movimientos
                        $move = ORM::factory('move')
                                        ->where('idMove', '=', $o_move_detail->idMove)->find();
                        $move->typeMoveStore = self::TYPE_MOVE_TRAS_SALIDA_CANCEL;
                        $move->save();
                    }
//                }
                $salesDetail->save();
                $sales->save();   
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    //Buscar un Detalle de Venta
    public function action_getSalesDetailByID() {
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('idsd', 'not_empty');
            if ($post_idSalesDetail->check()) {
                $data_salesDetail = $post_idSalesDetail->data();
                 
                
                $salesDetail = DB::select(
                                        'sad.idSalesDetail','sad.idLot','po.haveDetail','sad.idStore','po.idProduct', 
                        array(DB::expr("UPPER(po.productName)"), 'product'),'sad.quantity','sad.discount', 'sad.unitPrice', 
                        'sad.totalPrice', 'st.amount')
                                ->from(array('rms_salesdetail', 'sad'))
                                ->join(array('rms_lot', 'lot'))->on('sad.idLot', '=', 'lot.idLot')
                                ->join(array('rms_products', 'po'))->on('lot.idProduct', '=', 'po.idProduct')
                                ->join(array('rms_stock','st'))->on('po.idProduct','=','st.idProduct')
                                ->where('sad.idsalesDetail', '=', ':ID_SALESDETAIL')
                                ->param(':ID_SALESDETAIL', $data_salesDetail['idsd'])
                                ->execute()->current();
                
               //  echo Database::instance()->last_query; die();

                $a_response['data'] = $salesDetail;
                
                
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_editSalesDetail(){
        $a_response = $this->json_array_return;
        try {
            $post_idSalesDetail = Validation::factory($this->request->post())
                    ->rule('codigo', 'not_empty')
                    ->rule('descuento', 'not_empty')
                    ->rule('cb_quantity', 'not_empty')
                    ;
            if (!$post_idSalesDetail->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                Database::instance()->begin();
                $data = $post_idSalesDetail->data();
                $idProductOne = $data['idProduct'];
                $idProductTwo = $data['idproduct'];
                // Obtengo los datos anteriores a ese detalle, antes de actualizarlos 
                $obj_sales = new Model_Salesdetail($data['idSalesDetail']);
          
            if($data['haveDetail']== 0) // Son por lotes 
            {  

                //Comprobar si es el mismo producto 
                if($idProductOne == $idProductTwo)
                {
            
                    $dif = $data['cb_quantity'] - $obj_sales->quantity;
                    if( $dif > 0 ) // La cantidad es positiva 
                    {
                        
                        $obj_sales->quantity = $data['cb_quantity'];
                        //Creo un movimiento de salida de almacen para ese producto como venta
                        
                        $mov = new Model_Move();
                        $mov->typeMoveStore = self::TYPE_MOVE_VENTA_DIRECTA;
                        $mov->moveReference = 'Actualización de Venta';
                        $mov->idStore = $data['idstore'];
                        $mov->status = self::STATUS_ACTIVE; 
                        $mov->save();
                        
                        $movDetail = new Model_Movedetail();
                        $movDetail->idMove = $mov->idMove; 
                        $movDetail->idProduct = $data['idProduct'];
                        $movDetail->idLot = $data['idLot'];
                        $movDetail->amount = $dif; // lo que venderé mas 
                        $movDetail->unitPrice = $data['cb_precio'];
                        $movDetail->status = self::STATUS_ACTIVE; 
                        $movDetail->save();

                    }else if($dif < 0 )
                    {
                        $obj_sales->quantity = $data['cb_quantity'];
                        //Creo un movimiento de entrada para el producto                        
                        $mov = new Model_Move();
                        $mov->typeMoveStore = self::TYPE_MOVE_INGRESO;
                        $mov->moveReference = 'Ingreso por la Venta';
                        $mov->idStore = $data['idstore'];
                        $mov->status = self::STATUS_ACTIVE; 
                        $mov->save();
                        
                        $movDetail = new Model_Movedetail();
                        $movDetail->idMove = $mov->idMove; 
                        $movDetail->idProduct = $data['idProduct'];
                        $movDetail->idLot = $data['idLot'];
                        $movDetail->amount = abs($dif); // lo que venderé menos
                        $movDetail->unitPrice = $data['cb_precio'];
                        $movDetail->status = self::STATUS_ACTIVE; 
                        $movDetail->save();
  
                    }
                    //Actualizo el stock 
                    $obj_stock = ORM::factory('stock')
                            ->where('idProduct','=',$data['idProduct'])
                            ->and_where('idStore', '=',$data['idstore'])
                            ->find();

                    $stock = new Model_Stock($obj_stock->idStock);
                    $stock->amount =  $stock->amount - $dif;      
                    $stock->save();
                    // En el lote 
                    $lot = new Model_Lot($data['idLot']);
                    $lot->amount= $lot->amount - $dif;
                    $lot->save();
                    // Cuando es igual ---  solo modifico el detalle de venta que seria el
                    // descuento, precio, y el total
                    
                    //$obj_sales->quantity = $data['cb_quantity'];
          
                }else 
                {
                    // Cuando son diferentes productos

                    // Devuelvo el primer producto al almacen donde salio 
                            $obj_move = new Model_Move();
                            $obj_move->typeMoveStore = self::TYPE_MOVE_INGRESO;
                            $obj_move->moveReference = 'Ingreso por la Venta';
                            $obj_move->idStore = $data['idStoreDetail'];
                            $obj_move->status = self::STATUS_ACTIVE;
                            $obj_move->save();

                            $obj_moveDetail = new Model_Movedetail();
                            $obj_moveDetail->idMove = $obj_move->idMove; 
                            $obj_moveDetail->idLot = $obj_sales->idLot;
                            $obj_moveDetail->amount = $obj_sales->quantity;
                            $obj_moveDetail->unitPrice = $obj_sales->unitPrice;
                            $obj_moveDetail->status = self::STATUS_ACTIVE;
                            $obj_moveDetail->save();
                            
                            //Actualizo el stock 
                            $obj_stock = ORM::factory('stock')
                                    ->where('idProduct','=',$data['idProduct'])
                                    ->and_where('idStore', '=',$data['idStoreDetail'])
                                    ->find();

                            $stock = new Model_Stock($obj_stock->idStock);
                            $stock->amount =  $stock->amount - $obj_sales->quantity;
                            $stock->save();
                            
                            // En el lote 
                            $lot = new Model_Lot($data['idLot']);
                            $lot->amount= $lot->amount + $obj_sales->quantity;
                            $lot->save();
                            
                     // Saco el nuevo producto   
                          
                            $obj_moveTwo = new Model_Move();
                            $obj_moveTwo->typeMoveStore = self::TYPE_MOVE_VENTA_DIRECTA;
                            $obj_moveTwo->moveReference = 'Venta Directa';
                            $obj_moveTwo->idStore = $data['idstore'];
                            $obj_moveTwo->status = self::STATUS_ACTIVE;
                            $obj_moveTwo->save();

                            $obj_moveDetailTwo = new Model_Movedetail();
                            $obj_moveDetailTwo->idMove = $obj_moveTwo->idMove; 
                            $obj_moveDetailTwo->idProduct= $data['idproduct'];
                            $obj_moveDetailTwo->amount = $data['cb_quantity'];
                            $obj_moveDetailTwo->unitPrice = $data['cb_precio'];
                            $obj_moveDetailTwo->status = self::STATUS_ACTIVE;
                            $obj_moveDetailTwo->save();
                            //Actualizo el stock 
                            $obj_stockTwo = ORM::factory('stock')
                                    ->where('idProduct','=',$data['idproduct'])
                                    ->and_where('idStore', '=',$data['idstore'])
                                    ->find();

                            $stockTwo = new Model_Stock($obj_stockTwo->idStock);
                            $stockTwo->amount =  $stock->amount - $data['cb_quantity'];
                            $stockTwo->save();
                            
                            // En el lote 
                        

                    //Lo unico que cambiamos en detalle de venta
                    // Obtenemos el lote del producto nuevo 
                    $obj_lot = ORM::factory('lot') 
                            ->where('idProduct','=',$data['idproduct'])
                            ->and_where('idStore','=',$data['idstore'])
                            ->find();
                    $obj_sales->idLot = $obj_lot->idLot;
                    $obj_sales->idStore = $data['idstore'];
                    
                    //Actualizamos el lote
                    $lotTwo = new Model_Lot($obj_lot->idLot);
                    $lotTwo->amount= $lotTwo->amount - $data['cb_quantity'];
                    $lotTwo->save();
                }
            }
            $obj_sales->discount = $data['descuento'];
            $obj_sales->unitPrice = $data['cb_precio'];
            $totalPriceDetail = ($data['cb_precio']*$data['cb_quantity']) - $data['descuento'];
            
            
       
            
            
            //Modificar la venta en si 
            
            $sales = new Model_Sales($obj_sales->idSales);
            $totalPrice =($sales->totalPrice - $obj_sales->totalPrice)+ $totalPriceDetail;
            $sales->totalPrice =  $totalPrice;
            $sales->returnCash =  $sales->paymentAmount - $totalPrice;
            $obj_sales->totalPrice = $totalPriceDetail;
            
            $obj_sales->save();  
            $sales->save();

            Database::instance()->commit();
            $a_response['data'] = $obj_sales->idSalesDetail;

            }
        }catch (Exception $e_exc) {
                   Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
        
    }

}

?>
