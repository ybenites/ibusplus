<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of typemove
 *
 * @author Yime
 */
class Controller_Private_Typemove extends Controller_Private_Admin implements Rms_Constants {
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('typemove', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('typemove', self::FILE_TYPE_JS));
        $inventory_typemove = new View('inventory/typemove');
        $inventory_typemove->statusMove = $this->a_TYPE_MOVE_STOCK();;
        $this->template->content = $inventory_typemove; 
    }
    public function action_listAllMove() {
       $this->auto_render = FALSE;        
        try {            
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $i_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');             
            
            $s_date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $s_time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME);
            $s_time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $s_time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
                       
            if (!$i_idx)
                $i_idx = 1;
                        
            $s_where_search = "";
            $b_search_on = jqGridHelper::Strip($this->request->post('_search')); 
            
            $statusMove=$this->a_TYPE_MOVE_STOCK();            
            $case_when_statusMove="CASE ";
            foreach ($statusMove as $v) {                
                $case_when_statusMove=$case_when_statusMove." 
                    WHEN tm.typeMoveState='".$v['value']."' 
                        THEN '".$v['display']."'
                    ";                
            }
            $case_when_statusMove=$case_when_statusMove." ELSE '" .__("No especificado") . "' END";
            
            $a_array_cols = array(
                "id" => "tm.idTypeMove",
                "moveName" => "tm.typeMoveName",                
                "moveSate" => $case_when_statusMove,                               
                "userCreate" => "p.fullName",
                "status" => "tm.status",
                "creationDate" => "DATE_FORMAT(tm.creationDate,'$s_date_format $s_time_format')"                                
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);
                        
            if ($b_search_on == 'true') {
                if ($this->request->post('filters')!='') {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = jqGridHelper::Strip($this->request->post('filters'));                    
                    $s_where_search = jqGridHelper::constructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    
                    foreach ($this->request->post() as $i_key => $s_value) {
 
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }
                     
            $s_cols = jqGridHelper::arrayColsToSQL($a_array_cols);
            
            $s_tables_join ='rms_typemove tm '.                    
                    'INNER JOIN bts_user u ON u.idUser=tm.userCreate '.
                    'INNER JOIN `bts_person` p ON p.`idPerson`=u.`idUser` ';           

            $s_where_conditions ="1=1 and tm.status=".self::STATUS_ACTIVE;                        
            
            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);                        

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true),self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }
    public function action_createOrUpdateMove() {
        $a_response = $this->json_array_return;
        try { 
            $id=$this->request->post('id');
            $o_post_move = Validation::factory($this->request->post())
                    ->rule('typeMoveName', 'not_empty')                   
                    ->rule('typeMoveState', 'not_empty');                                       
            if($o_post_move->check()){
                $data_post_move=$o_post_move->data();
                $obj_store=new Model_Typemove(); 
                if($id!=0){
                    $obj_store=new Model_Typemove($id);
                }                
                $obj_store->typeMoveName=$data_post_move['typeMoveName'];                
                $obj_store->typeMoveState=$data_post_move['typeMoveState'];                                           
                $obj_store->status=self::STATUS_ACTIVE;
                $obj_store->save();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }        
        $this->fnResponseFormat($a_response);
    }
    public function action_deleteMove(){
        $a_response = $this->json_array_return;
        try {
            $o_post_id_move = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');                    
            if($o_post_id_move->check()){
                $data_post_id_move=$o_post_id_move->data();
                $obj_move=New Model_Typemove($data_post_id_move['id']);
                $obj_move->status=self::STATUS_DEACTIVE;
                $obj_move->save();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    public function action_listByIdMove(){
        $a_response = $this->json_array_return;
        try {
            $o_post_id_move = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');                    
            if($o_post_id_move->check()){
                $data_post_id_move=$o_post_id_move->data();
                $obj_move=new Model_Typemove($data_post_id_move['id']);                
                $a_response['data']=$obj_move->as_array();
            }else{
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
}
