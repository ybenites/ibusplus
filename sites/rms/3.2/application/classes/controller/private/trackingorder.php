<?php

class Controller_Private_Trackingorder extends Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('trackingorder', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('trackingorder', self::FILE_TYPE_JS));
        $trackingorder_view = new View('purchase/trackingorder');
        $supplier = DB::select(array('idSupplier', 'id'), array('name', 'name'))
                        ->from('rms_supplier')
                        ->where('status', '=', self::STATUS_ACTIVE)
                        ->order_by('name')
                        ->execute()->as_array();
        $trackingorder_view->supplier = $supplier;
        $trackingorder_view->a_purchaseOrder_status = $this->fnGetClassVariable(self::VAR_NAME_PURCHASEORDER_STATUS);
        $trackingorder_view->a_year_report = $this->a_year_report();
        $trackingorder_view->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);;
        $trackingorder_view->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $trackingorder_view->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $this->template->content = $trackingorder_view;
    }

    // Lista las ordenes de compra
    public function action_listPurchaseOrder() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $sd = $this->request->post('sd');
            $ed = $this->request->post('ed');
            $m = $this->request->post('m');
            $a = $this->request->post('a');
            $d = $this->request->post('d');

            $a_purchaseorders = $this->fnGetClassVariable(self::VAR_NAME_PURCHASEORDER_STATUS);
            $a_purchaseDocument = $this->fnGetClassVariable(self::VAR_NAME_PURCHASE_DOCUMENTS);
            $a_purchasePayment = $this->fnGetClassVariable(self::VAR_NAME_PURCHASEORDER_PAYMENT);
            if (!$sidx) {
                $sidx = 1;
            }
            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $date = date($date_format);

            $array_cols = array(
                'idOrder' => 'po.idPurchaseOrder',
                'PurchaseOrder' => "CONCAT(po.purchaseDocument,'-',po.nrodocument)",
                'DocumentNumber' => "CONCAT(po.serie,'-',po.correlative)",
                'Proveedor' => "UPPER(s.name)",
                'DatePurchaseOrder' => "DATE_FORMAT(po.datePurchaseOrder,'%d/%m/%Y %h:%i %p')",
                'PurchasePayment' => "
                    CASE 
                        WHEN po.methodPayment = '" . self::PURCHASE_ORDER_CASH . "' THEN '" . __($a_purchasePayment[self::PURCHASE_ORDER_CASH]['display']) . "' 
                        WHEN po.methodPayment = '" . self::PURCHASE_ORDER_CREDIT . "' THEN '" . __($a_purchasePayment[self::PURCHASE_ORDER_CREDIT]['display']) . "' 
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
//                'PurchaseCurrency' => "(po.currency)",
                'TotalAmount' => "po.totalAmount",
//                'DocumentPurchase' => "
//                    CASE 
//                        WHEN po.purchaseDocument = '" . self::OC_BOLETA . "' THEN '" . __($a_purchaseDocument[self::OC_BOLETA]['display']) . "' 
//                        WHEN po.purchaseDocument = '" . self::OC_FACTURA . "' THEN '" . __($a_purchaseDocument[self::OC_FACTURA]['display']) . "' 
//                        WHEN po.purchaseDocument = '" . self::OC_GUIAREMISION . "' THEN '" . __($a_purchaseDocument[self::OC_GUIAREMISION]['display']) . "' 
//                        WHEN po.purchaseDocument = '" . self::OC_NOTACREDITO . "' THEN '" . __($a_purchaseDocument[self::OC_NOTACREDITO]['display']) . "'                         
//                        WHEN po.purchaseDocument = '" . self::OC_NOTADEBITO . "' THEN '" . __($a_purchaseDocument[self::OC_NOTADEBITO]['display']) . "'
//                        WHEN po.purchaseDocument = '" . self::OC_RECIBOPORHONORARIOS . "' THEN '" . __($a_purchaseDocument[self::OC_RECIBOPORHONORARIOS]['display']) . "'
//                        WHEN po.purchaseDocument = '" . self::OC_TICKETFACTURA . "' THEN '" . __($a_purchaseDocument[self::OC_TICKETFACTURA]['display']) . "'
//                        WHEN po.purchaseDocument = '" . self::OC_TICKETBOLETA . "' THEN '" . __($a_purchaseDocument[self::OC_TICKETBOLETA]['display']) . "'
//                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'StatusPurchase' => "
                    CASE 
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_APROBADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_APROBADO]['display']) . "' 
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_CANCELADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_CANCELADO]['display']) . "' 
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_EMITIDO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_EMITIDO]['display']) . "' 
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_FACTURADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_FACTURADO]['display']) . "'                         
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_RECEPCIONADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_RECEPCIONADO]['display']) . "'
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_ANULADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_ANULADO]['display']) . "'
                        WHEN po.statusPurchase = '" . self::PURCHASE_ORDER_STATUS_DESAPROBADO . "' THEN '" . __($a_purchaseorders[self::PURCHASE_ORDER_STATUS_DESAPROBADO]['display']) . "'
                    ELSE '" . __("NO ESPECIFICADO") . "' END",
                'status' => "po.statusPurchase"
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_purchaseorder po
                            INNER JOIN rms_supplier s ON s.idSupplier = po.idSupplier';
            $where_conditions = "po.documentType IS NOT NULL AND po.statusPurchase <> '".self::PURCHASE_ORDER_STATUS_CANCELADO."'";

            if ($this->request->post('searchType')!=null) {
                if ($this->request->post('searchType') == 'purchaseorder') {
                    if ($this->request->post('nrodocument') != NULL) {
                        $where_conditions .= " AND po.nrodocument = '" . $this->request->post('nrodocument') . "' ";
                    }
                } else {
                    if ($this->request->post('nrodocument') != NULL) {
                        $temp = explode('-', $this->request->post('nrodocument'));
                        $where_conditions .= " AND po.serie LIKE (" . $temp[0] . ") ";
                        $where_conditions .= " AND po.correlative LIKE (" . $temp[1] . ") ";
                    }
                }
            }

            if ($this->request->post('supplier') != NULL) {
                $where_conditions .= " AND po.idSupplier = '" . $this->request->post('supplier') . "' ";
            }
            if ($d != "") {
                $where_conditions .= " AND DATE_FORMAT(po.datePurchaseOrder,'$date_format') LIKE '" . $d . "' ";
            }
            if ($m != 0) {
                $where_conditions .= " AND MONTH(po.datePurchaseOrder) = " . $m . " AND YEAR(po.datePurchaseOrder) = " . $a . "";
            }
            if ($ed != "") {
                if ($sd != "") {
                    $where_conditions .= " AND po.datePurchaseOrder BETWEEN STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')";
                } else {
                    $where_conditions .= " AND DATE_FORMAT(po.datePurchaseOrder,'$date_format') LIKE '" . $ed . "' ";
                }
            } else {
                if ($sd != "") {
                    $where_conditions .= " AND DATE_FORMAT(po.datePurchaseOrder,'$date_format') LIKE '" . $sd . "' ";
                }
            }
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
            
//            echo Database::instance()->last_query;die();
          //  die();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    // Lista el detalle de las ordenes de compra
    public function action_listPurchaseOrderDetail() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $idPurchaseOrder = $this->request->post('idOrder');

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idOrderDetail' => 'pod.idPurchaseOrderDetail',
                'idOrder' => 'po.idPurchaseOrder',
                'CodeProduct' => "UPPER(p.aliasProduct)",
                'Description' => "UPPER(p.productName)",
                'UnitMeasures' => 'p.productUnitMeasures',
                'Quantity' => 'pod.quantity',
                'PriceUnit' => 'pod.totalUnit',
                'TotalAmount' => 'pod.totalAmount',
                'Status' => 'po.statusPurchase'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_purchaseorder po
                            INNER JOIN rms_purchaseorderdetail pod ON pod.idPurchaseOrder = po.idPurchaseOrder
                            INNER JOIN rms_products p ON pod.idProduct = p.idProduct ';

            $where_conditions = "pod.status = 1 AND po.idPurchaseOrder IN (" . $this->request->post('idOrder') . ")";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
            // echo Database::instance()->last_query;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_getPurchaseOrderDetail() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idOrderDetail' => 'pod.idPurchaseOrderDetail',
                'idOrder' => 'po.idPurchaseOrder',
                'Description' => "UPPER(p.productName)",
                'UnitMeasures' => 'p.productUnitMeasures',
                'Quantity' => 'pod.quantity',
                'PriceUnit' => 'pod.totalUnit',
                'TotalAmount' => 'pod.totalAmount'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_purchaseorder po
                            INNER JOIN rms_purchaseorderdetail pod ON pod.idPurchaseOrder = po.idPurchaseOrder
                            INNER JOIN rms_products p ON pod.idProduct = p.idProduct ';

            $where_conditions = "po.idPurchaseOrder IN (" . $this->request->post('idOrder') . ")";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
            //   echo Database::instance()->last_query;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    // Buscar un Detalle de Compra
    public function action_getPurchaseOrderDetailById() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrderDetail = Validation::factory($this->request->post())
                    ->rule('idpod', 'not_empty');
            if ($post_idPurchaseOrderDetail->check()) {
                $data_purchaseorderdetail = $post_idPurchaseOrderDetail->data();
                $purchaseorderdetail = DB::select(
                                        'pod.idPurchaseOrderDetail', 'pod.idPurchaseOrder', 'p.idProduct', array(DB::expr("UPPER(p.productName)"), 'product'), 'p.productUnitMeasures', 'pod.quantity', 'pod.totalUnit', 'pod.totalAmount')
                                ->from(array('rms_purchaseorderdetail', 'pod'))
                                ->join(array('rms_purchaseorder', 'po'))->on('po.idPurchaseOrder', '=', 'pod.idPurchaseOrder')
                                ->join(array('rms_products', 'p'))->on('pod.idProduct', '=', 'p.idProduct')
                                ->where('pod.idPurchaseOrderDetail', '=', ':ID_PURCHASEORDERDETAIL')
                                ->param(':ID_PURCHASEORDERDETAIL', $data_purchaseorderdetail['idpod'])
                                ->execute()->current();
                $a_response['data'] = $purchaseorderdetail;
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    // Cancelar una Orden de Compra
    public function action_deletePurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpo', 'not_empty')
                    ->rule('status', 'not_empty');
            if ($post_idPurchaseOrder->check()) {
                $data_purchaseorder = $post_idPurchaseOrder->data();      
                $purchaseorder = New Model_Purchaseorder($data_purchaseorder['idpo']);
                $purchaseorder->statusPurchase = $data_purchaseorder['status'];
                $purchaseorder->save();
                $this->createHistory($data_purchaseorder);
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    // Cancelar una Detalle de la Orden de Compra
    public function action_deletePurchaseOrderDetail() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpod', 'not_empty');
            if ($post_idPurchaseOrder->check()) {
                $data_purchaseorder = $post_idPurchaseOrder->data();
                $purchaseorderdetail = New Model_Purchaseorderdetail($data_purchaseorder['idpod']);
                $purchaseorderdetail->status = self::STATUS_DEACTIVE;

                // Descuento del monto 
                $purchaseorden = New Model_Purchaseorder($purchaseorderdetail->idPurchaseOrder);
                $purchaseorden->totalAmount = $purchaseorden->totalAmount - $purchaseorderdetail->totalAmount;
                $purchaseorderdetail->save();
                $purchaseorden->save();
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    // Retornar una Orden de Compra a Estado Emitido
    public function action_updatePurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpo', 'not_empty')
                    ->rule('status', 'not_empty');
            if ($post_idPurchaseOrder->check()) {
                $data_purchaseorder = $post_idPurchaseOrder->data();
                $purchaseorder = New Model_Purchaseorder($data_purchaseorder['idpo']);
                $purchaseorder->statusPurchase = self::PURCHASE_ORDER_STATUS_EMITIDO;
                $purchaseorder->save();
                $this->createHistory($data_purchaseorder);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    // Crear y/o actualizar un Detalle de una Orden de Compra
    public function action_createorUpdateOrderDetail() {
        $a_response = $this->json_array_return;
        try {
            $orderdetail_id = $this->request->post('idPurchaseOrderDetail');
            $data_post_orderdetail = Validation::factory($this->request->post())
                    ->rule('quantityProduct', 'not_empty')
                    ->rule('priceUnit', 'not_empty')
                    ->rule('priceTotal', 'not_empty')
                    ->rule('idProduct', 'not_empty')
                    ->rule('idPurchaseOrder', 'not_empty')
            ;
            if ($data_post_orderdetail->check()) {

                $purchaseorderdetail = new Model_Purchaseorderdetail($orderdetail_id);
                $purchaseorden = New Model_Purchaseorder($data_post_orderdetail['idPurchaseOrder']);
                $purchaseorderdetail->quantity = $data_post_orderdetail['quantityProduct'];
                $purchaseorderdetail->totalUnit = $data_post_orderdetail['priceUnit'];
                $diferencia = $purchaseorderdetail->totalAmount - $data_post_orderdetail['priceTotal'];
                $purchaseorderdetail->totalAmount = $data_post_orderdetail['priceTotal'];


                //$purchaseorderdetail->idProduct = $data_post_orderdetail['idProduct'];
                // $purchaseorderdetail->idPurchaseOrder = $data_post_orderdetail['idPurchaseOrder'];
                // Descuento del monto 

                $purchaseorden->totalAmount = $purchaseorden->totalAmount - $diferencia;
                $purchaseorderdetail->save();
                $purchaseorden->save();
                Database::instance()->commit();
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    // Aprobar una Orden de Compra
    public function action_updateStatus() {
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpo', 'not_empty')
                    ->rule('status', 'not_empty');
            
            if (!$post_idPurchaseOrder->check()) 
                throw new Exception(__('Data Invalida'), self::CODE_SUCCESS);
            
            $data_purchaseorder = $post_idPurchaseOrder->data();
            $purchaseorder = New Model_Purchaseorder($data_purchaseorder['idpo']);
   
            if (!$purchaseorder->loaded()) 
                throw new Exception(__('Orden no encontrada'), self::CODE_SUCCESS);
            
            $purchaseorder->statusPurchase = $data_purchaseorder['status'];
            $purchaseorder->save();
            
            $this->createHistory($data_purchaseorder);
            
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    //Anular una Orden de Compra - Editar stock, y movimiento.
    public function action_annularPurchaseOrder(){
        $a_response = $this->json_array_return;
        try {
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpo', 'not_empty')
                    ->rule('status', 'not_empty');
            
            if (!$post_idPurchaseOrder->check()) 
                throw new Exception(__('Data Invalida'), self::CODE_SUCCESS);
            
             Database::instance()->begin();
             
            $data_purchaseorder = $post_idPurchaseOrder->data();
            $purchaseorder = New Model_Purchaseorder($data_purchaseorder['idpo']);
   
            if (!$purchaseorder->loaded()) 
                throw new Exception(__('Orden no encontrada'), self::CODE_SUCCESS);
            
            /* actualizar Stock move movedetail  */
            
            $select=DB::Select(
                   array('md.idMoveDetail','move_detail_id')
                   ,array('md.idLot','move_detail_idlot')
                   ,array('lot.idProduct','product_lot_id')
                   ,array('mo.idStore','move_store_id')
                   ,array('md.idProductdetail','move_detail_idproductdetail')
                   ,array('pd.idProduct','product_detail_id')
                    )
            ->from(array('rms_move', 'mo'))
            ->join(array('rms_movedetail', 'md'))->on('md.idMove','=','mo.idMove')
            ->join(array('rms_lot', 'lot'),'left')->on('lot.idLot','=','md.idLot')
            ->join(array('rms_product_detail','pd'),'left')->on('pd.idProductDetail','=','md.idProductDetail')
            ->where('mo.idPurchaseOrders','=',$data_purchaseorder['idpo'])->execute()->as_array();
            
            $idStore = DB::Select(array('mo.idStore','idStore'))
                    ->from(array('rms_move','mo'))
                    ->where('mo.idPurchaseOrders','=',$data_purchaseorder['idpo'])->as_object()->execute()->current();
             //print_r($idStore);die();         
            
            //Create Move
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_SALIDA;
            $obj_move->dateOutcome = DateHelper::getFechaFormateadaActual();
            $obj_move->idPurchaseOrders = $data_purchaseorder['idpo'];
            $obj_move->idStore = $idStore->idStore;
            $obj_move->status = self::STATUS_ACTIVE;

            $obj_move->save();
            $idMov = $obj_move->idMove;

            foreach ($select as $value) {

                if($value['move_detail_idproductdetail']!=null){
                    $o_select = DB::select(array(DB::expr('COUNT(idMoveDetail)'), 'total'))
                    ->from('rms_movedetail')
                    ->where('idProductDetail', '=', $value['move_detail_idproductdetail']);
                    
                }else if($value['move_detail_idlot']!=null){
                    $o_select = DB::select(array(DB::expr('COUNT(idMoveDetail)'), 'total'))
                    ->from('rms_movedetail')
                    ->where('idLot', '=', $value['move_detail_idlot']);
                }               
                $unnumero=$o_select->execute()
                                ->get('total');
                //print_r($unnumero);die();
                
                if($unnumero > 1)
                    throw new Exception(__('Esta orden no se puede anular'), self::CODE_SUCCESS);
            
                $i_amount = 0; 
                if($value['move_detail_idproductdetail']!=null){
                    $obj_movedetail = new Model_Movedetail();
                    $productdetail = New Model_Productdetail($value['move_detail_idproductdetail']);
                    
                    $i_amount = $productdetail->amount;
                    $productdetail->amount = 0;
                    $productdetail->save();
                    if($value['product_detail_id']!=null){
                        $stock = new Model_Stock();
                        $o_stock_data = $stock->getIdStockByProductAndStore($value['product_detail_id'],$value['move_store_id']);
                        $o_stock_data->amount = $o_stock_data->amount - $i_amount;
                        $stock_update = new Model_Stock($o_stock_data->idStock);
                        $stock_update->amount = $o_stock_data->amount;
                        $stock_update->save();
                        
                        //Detalle de Movimiento
                        $obj_movedetail->idMove = $idMov;
                        $obj_movedetail->amount = $i_amount;
                        //$obj_movedetail->unitPrice = $v['price'];
                        $obj_movedetail->status = self::STATUS_ACTIVE;
                        $obj_movedetail->dateOutcomeDetail = DateHelper::getFechaFormateadaActual();
                        $obj_movedetail->idProductDetail = $value['move_detail_idproductdetail'];
                    }
                }else if($value['move_detail_idlot']!=null){
                    $obj_movedetail = new Model_Movedetail();
                    $lote = New Model_Lot($value['move_detail_idlot']);
                    
                    $i_amount = $lote->amount;
                    $lote->amount = 0;
                    $lote->save();
                    if($value['product_lot_id']!=null){
                       $stock = new Model_Stock();
                        $o_stock_data = $stock->getIdStockByProductAndStore($value['product_lot_id'],$value['move_store_id']);
                        $o_stock_data->amount = $o_stock_data->amount - $i_amount;
                        $stock_update = new Model_Stock($o_stock_data->idStock);
                        $stock_update->amount = $o_stock_data->amount;
                        $stock_update->save();
                        
                        //Creando detalle de movimiento
                        $obj_movedetail->idLot = $value['move_detail_idlot'];
                        $obj_movedetail->idMove = $idMov;
                        $obj_movedetail->amount = $i_amount;
                        //$obj_movedetail->unitPrice = $v['price'];
                        $obj_movedetail->status = self::STATUS_ACTIVE;
                        $obj_movedetail->dateOutcomeDetail = DateHelper::getFechaFormateadaActual();
                    }
                    
                } //falta crear la opcion recepcionado en la base de datos 
                            
                $obj_movedetail->save();
            }        
            
            $purchaseorder->statusPurchase = $data_purchaseorder['status'];
            $purchaseorder->save();
            
            $this->createHistory($data_purchaseorder);
            Database::instance()->commit();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    //Creando el registro del historial de las ordenes de compra.
    public function createHistory($data_purchaseorder){    
            $id_User = $this->getSessionParameter(self::USER_ID);
        
            $historypurchase = New Model_Historypurchase();
            $data_purchaseorder['idPurchaseOrder']=$data_purchaseorder['idpo'];
            $data_purchaseorder['status']=$data_purchaseorder['status'];
            $data_purchaseorder['dateHistoryPurchase']=  DateHelper::getFechaFormateadaActual();
            $data_purchaseorder['idUser']=$id_User;
            $historypurchase->saveH($data_purchaseorder);
    }
    
    public function action_getHistoryPurchaseOrderByIdPurchaseOrder(){
//        $a_response = $this->json_array_return;
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            
            $post_idPurchaseOrder = Validation::factory($this->request->post())
                    ->rule('idpo', 'not_empty');
            
            if (!$post_idPurchaseOrder->check()) 
                throw new Exception(__('Data Invalida'), self::CODE_SUCCESS);
            
            $data_purchaseorder = $post_idPurchaseOrder->data();
            $idPurchOrder =$data_purchaseorder['idpo'];
            $purchaseorder = New Model_Purchaseorder($data_purchaseorder['idpo']);
   
            if (!$purchaseorder->loaded()) 
                throw new Exception(__('Orden no encontrada'), self::CODE_SUCCESS);
            if (!$sidx) {
                $sidx = 1;
            }       
            $where_search="";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
            
             $array_cols = array(
                'idhistory' => 'h.idHistory'
               ,'idpurchaseorder' => 'h.idPurchaseOrder'
               ,'status' => 'h.status'
               ,'date' => 'h.dateHistoryPurchase'
               ,'iduser' => 'h.idUser'
               ,'nameuser' => 'u.fullName'
              );
            
            $searchable_cols = array_keys($array_cols);
            
             if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
             }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            
            $tables_join = 'rms_historypurchase h
                            INNER JOIN bts_person u ON u.idPerson = h.idUser';
            
            $where_conditions = "h.idPurchaseOrder = '$idPurchOrder'";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
            
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
            
        } catch (Exception $e_exc) {
             echo $e_exc->getMessage();
        }
    }

    // Crear varios detalles de compra en una orden de comrpa ya creada
    public function action_addDetailPurchaseOrder() {
        $a_response = $this->json_array_return;
        try {
            $post_purchaseOrders = Validation::factory($this->request->post())
                    ->rule('header', 'not_empty')
                    ->rule('detail', 'not_empty');
            if (!$post_purchaseOrders->check()) {
                throw new Exception('Existen datos faltantes', self::CODE_SUCCESS);
            }
            $a_headerOrder = $this->request->post('header'); // es el codigo de la Orden de Compra
            $a_detailOrder = $this->request->post('detail'); // es el detalle 
            $a_noteOrder = $this->request->post('note');
            $arrayPurchaseOrders = array();

            if (count($arrayPurchaseOrders) > 0) {
                $a_response['data'] = $arrayPurchaseOrders;
            } else {
                $a_response['data'] = $arrayPurchaseOrders;
                $totalAmountWithTax = 0;
                $totalAmountWithOutTax = 0;
                $totalTax = 0;
                $totalAmount = 0;
                Database::instance()->begin();
                $purchaseorders = new Model_Purchaseorder($a_headerOrder);
                foreach ($a_detailOrder as $key => $a_detailPurchaseOrder) {
                    $detailpurchaseorder = new Model_Purchaseorderdetail();
                    $detailpurchaseorder->idPurchaseOrder = $a_headerOrder;
                    $detailpurchaseorder->idProduct = $a_detailPurchaseOrder['idProduct'];
                    $detailpurchaseorder->quantity = $a_detailPurchaseOrder['quantity'];
                    $detailpurchaseorder->totalUnit = $a_detailPurchaseOrder['amountUnit'];
                    $detailpurchaseorder->totalAmount = $a_detailPurchaseOrder['amountTotal'];
                    $detailpurchaseorder->save();
                    if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                        $detailpurchaseorder->taxPercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        $detailpurchaseorder->save();
                        $detailpurchaseorder->totalAmountWithOutTax = ($detailpurchaseorder->totalAmount / 1.18);
                        $detailpurchaseorder->totalTax = ($detailpurchaseorder->totalAmount - $detailpurchaseorder->totalAmountWithOutTax);
                        $detailpurchaseorder->totalAmountWithTax = ($detailpurchaseorder->quantity * $detailpurchaseorder->totalUnit);
                    } else {
                        $detailpurchaseorder->taxPercentage = 0;
                        $detailpurchaseorder->totalTax = 0;
                    }
                    $detailpurchaseorder->save();
                    $totalTax = $totalTax + $detailpurchaseorder->totalTax;
                    $totalAmountWithOutTax = $totalAmountWithOutTax + $detailpurchaseorder->totalAmountWithOutTax;
                    $totalAmountWithTax = $totalTax + $totalAmountWithOutTax;
                    $totalAmount = $totalAmount + $a_detailPurchaseOrder['amountTotal'];
                }
                $purchaseorders->totalAmount = $purchaseorders->totalAmount + $totalAmount;
                $purchaseorders->save();
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollBack();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>
