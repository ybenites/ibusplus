<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Assignmentdocuments extends Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('assignmentdocuments', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('assignmentdocuments', self::FILE_TYPE_JS));
        $oView_AssignmentDocument = new View('series/assignmentdocuments');
        $oModel_City = new Model_City();
        $aCitys = $oModel_City->select('city.idCity', 'city.name')->where('status', '=', self::STATUS_ACTIVE)->order_by('name')->find_all();
        $oView_AssignmentDocument->aCitys = $aCitys;
        $oOffice = new Model_Office();
        $aOffice = $oOffice->select('office.idOffice', 'office.name')->where('office.state', '=', self::STATUS_ACTIVE)->order_by('office.name')->find_all();
        $oView_AssignmentDocument->aOffice = $aOffice;
        $this->template->content = $oView_AssignmentDocument;
        
    }

    public function action_createorUpdateDocuments() {
        $a_response = $this->json_array_return;
        try {
            $oPost = Validation::factory($_POST)
                    ->rule('idUser', 'not_empty')
                    ->rule('checkDocuments', 'not_empty');
            if (!$oPost->check()) {
                throw new Exception(__('Invalid Data'), self::CODE_ERROR);
            }
            $i_user_id = $this->request->post('idUser');
            $i_office_id = $this->request->post('idOffice');
            $a_series_id = $this->request->post('checkDocuments');

            
            $a_cols = array(
                array('se.idAssignmentSeries', 'serie_id'),
                array('se.documentType', 'serie_docType')
            );
            $docType = DB::select_array($a_cols)
                            ->from(array('rms_series', 'se'))
                            ->where('se.idAssignmentSeries'
                                    , 'IN'
                                    , DB::expr('('
                                            . implode(',', $a_series_id)
                                            . ')')
                            )
                            ->execute()->as_array();

            $accumulator = 0;
            $repeated_document = FALSE;
            $a_response['repeated'] = false;

            foreach ($docType as $indice => $valor) {
                $aux = $valor['serie_docType'];
                foreach ($docType as $i => $val) {
                    if ($aux == $val['serie_docType'])
                        $accumulator++;
                    if ($accumulator > 1) {
                        $repeated_document = true;
                        $a_response['repeated'] = true;
                        break 2;
                    }
                }
                $accumulator = 0;
            }

            
            if ($repeated_document == FALSE) {
                Database::instance()->begin();
                $a_params = array(
                    '::ID_USER::' => $i_user_id
                    , '::ID_OFFICE::' => $i_office_id
                    , '::STATE_DEACTIVE::' => self::STATUS_DEACTIVE
                    , '::STATE_ACTIVE::' => self::STATUS_ACTIVE
                );

                $s_query = "
                    UPDATE rms_documents                     
                    INNER JOIN rms_series 
                        ON rms_documents.`idAssignmentSeries` = rms_series.`idAssignmentSeries`
                    SET rms_documents.state = ::STATE_DEACTIVE::
                    WHERE
                        rms_documents.idUser = ::ID_USER::
                        AND rms_series.idOffice = ::ID_OFFICE::
                        AND rms_documents.state = ::STATE_ACTIVE::
                    ";

                DB::query(Database::UPDATE, $s_query)->parameters($a_params)->execute();

                DB::update('rms_documents')
                        ->set(array('state' => self::STATUS_ACTIVE))
                        ->where('idUser', '=', $i_user_id)
                        ->where('idAssignmentSeries'
                                , 'IN'
                                , DB::expr('('
                                        . implode(',', $a_series_id)
                                        . ')')
                        )
                        ->execute();

                $a_ids_assignment_series_faltantes =
                        DB::select('idAssignmentSeries')
                        ->from('rms_documents')
                        ->where('idUser', '=', $i_user_id)
                        ->where('state', '=', self::STATUS_ACTIVE)
                        ->where('idAssignmentSeries'
                                , 'IN'
                                , DB::expr('('
                                        . implode(',', $a_series_id)
                                        . ')')
                        )
                        ->execute()
                        ->as_array();

                $a_ids_assignment_series_faltantes =
                        array_map(
                        function($aArray) {
                            return $aArray['idAssignmentSeries'];
                        }
                        , $a_ids_assignment_series_faltantes
                );

                $a_diff = array_diff($a_series_id, $a_ids_assignment_series_faltantes);

                foreach ($a_diff as $i_k => $i_assignment_serie_id) {
                    $o_model_assignment_document = new Model_Assignmentdocuments;
                    $o_model_assignment_document->idUser = $i_user_id;
                    $o_model_assignment_document->idAssignmentSeries = $i_assignment_serie_id;
                    $o_model_assignment_document->save();
                }
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = $e_exc->getCode();
            $a_response['msg'] = $e_exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getDocumentsByOffice() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        $oPost = Validation::factory($_POST)
                ->rule('idOffice', 'not_empty');

        if (!$oPost->check()) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = 'Invalid Data';
        } else {
            $idOffice = $_POST['idOffice'];
            $idUser = $_POST['idUser'];
            $aDocuments = DB::Select(
                               array('rms_series.idAssignmentSeries', 'id'), 
                               DB::expr("CONCAT". "(". "rms_series.documentType". ",' '". ",rms_series.serie". ",'-'". ",rms_series.correlative". ") as value"),
                               DB::expr("CASE "
                                            . "WHEN "
                                            . " ISNULL(rms_documents.idAssignmentDocument) "
                                            . "THEN "
                                            . " 0 "
                                            . "ELSE "
                                            . " 1 "
                                            . "END as checked"
                                    )
                               )
                            ->from('rms_series')
                            ->join('rms_documents', 'LEFT')
                            ->on('rms_series.idAssignmentSeries', '=', 'rms_documents.idAssignmentSeries')
                            ->on('rms_documents.idUser', '=', DB::expr($idUser))
                            ->on('rms_documents.state', '=', DB::expr(self::STATUS_ACTIVE))
                            ->where('rms_series.idOffice', '=', $idOffice)
                            ->and_where('rms_series.state', '=', self::STATUS_ACTIVE)
                            ->execute()->as_array();

            if (count($aDocuments) > 0) {
                $a_response['msg'] = 'Documentos - Serie de Oficina';
                $a_response['data'] = $aDocuments;
            } else {
                $a_response['msg'] = 'No se Encontraron Documentos';
            }
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteAssignmentDocument() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        $oPost = Validation::factory($_POST)
                ->rule('idAssignmentDocument', 'not_empty');
        if (!$oPost->check()) {
            $a_reponse['msg'] = 'Invalid Data';
            $a_reponse['code'] = self::CODE_ERROR;
        } else {
            $idAssignmentDocument = $_POST['idAssignmentDocument'];
            try {
                $oModel_AssignmentDocument = new Model_Assignmentdocuments($idAssignmentDocument);
                $oModel_AssignmentDocument->state = self::STATUS_DEACTIVE;
                $oModel_AssignmentDocument->save();
                $a_response['msg'] = 'OK';
            } catch (Exception $e_exc) {
                $a_response['code'] = $e_exc->getCode();
                $a_response['msg'] = $e_exc->getMessage();
            }
           $this->fnResponseFormat($a_response);
        }
    }

    public function action_listAssignmentDocuments() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            if (!$sidx)
                $sidx = 1;
            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'id' => "adoc.idAssignmentDocument"
                , 'city' => "c.name"
                , 'office' => "o.name"
                , 'user' => "p.fullName"
                , 'documentType' => "aser.documentType"
                , 'serie' => 'aser.serie'
                , 'correlative' => 'aser.correlative'
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters')!='') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_documents adoc ' .
                    ' INNER JOIN rms_series aser '
                    . ' ON adoc.idAssignmentSeries = aser.idAssignmentSeries ' .
                    ' INNER JOIN bts_office o '
                    . ' ON o.idOffice = aser.idOffice ' .
                    ' INNER JOIN bts_city c '
                    . ' ON c.idCity = o.idCity ' .
                    ' INNER JOIN bts_person p '
                    . ' ON adoc.idUser = p.idPerson ';

            $where_conditions = "aser.state = 1 and o.state = 1 and adoc.state= 1 ";
            if (isset($_REQUEST['selectOffice'])) {
                if ($_REQUEST['selectOffice'] != '')
                    $where_conditions .= "AND o.idOffice='" . $_REQUEST['selectOffice'] . "'";
            }
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $assignmentDocuments = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);
        
            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $assignmentDocuments, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
     
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }
    public function action_getOfficeByCity() {
        $this->auto_render = FALSE;         
        $a_return = $this->json_array_return;
        $oPost = Validation::factory($_POST)
                ->rule('idCity', 'not_empty');
        if (!$oPost->check()) {
            $a_return['code'] = self::CODE_ERROR;
            $a_return['msg'] = 'ERROR -' . __('Empty Data Post');
        } else {
            $idCity = $this->request->post('idCity');
            $aOffices = DB::Select(
                    array('bts_office.idOffice', 'id'),
                    array('bts_office.name', 'value'))
                    ->from('bts_office')
                    ->where('bts_office.idCity', '=' , $idCity)
                    ->and_where('bts_office.state', '=', self::STATUS_ACTIVE)
                    ->execute()->as_array();

            if (count($aOffices) > 0) {
                $a_return['msg'] = __('Documentos - Serie de Oficina');
                $a_return['data'] = $aOffices;
            } else {
                $a_return['code'] = 0;
                $a_return['msg'] = __('No se Encontraron Oficinas');
            }
        }
        $this->fnResponseFormat($a_return);
    }
    public function action_getUserByOffice() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $oPost = Validation::factory($this->request->post())
                    ->rule('idOffice', 'not_empty');
            if (!$oPost->check()) {
                throw new Exception('Empty Data Post', self::CODE_SUCCESS);
            } else {
                $idOffice = $this->request->post('idOffice');
                $aUser = DB::Select(
                                   array('bts_user.idUser', 'id'),
                                   array('bts_person.fullName', 'value'))
                                ->from('bts_person')
                                ->join('bts_user')->on('bts_person.idPerson', ' = ', 'bts_user.idUser')
                                ->where('bts_user.idOffice', '=', ":ID_OFFICE")
                                ->and_where('bts_user.status', '=', self::STATUS_ACTIVE)->param(':ID_OFFICE', $idOffice)
                                ->execute()->as_array();
                if (count($aUser) > 0) {
                    $a_response['data'] = $aUser;
                } else {
                    throw new Exception(__('No se Encontraron Usuarios'), self::CODE_SUCCESS);
                }
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
}
