<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Supplier extends Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('supplier', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('supplier', self::FILE_TYPE_JS));
        $supplier_view = new View('purchase/supplier');
        $this->template->content = $supplier_view;
    }

    public function action_listSupplier() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            if (!$sidx)
                $sidx = 1;
            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idsupplier' => 'rms_supplier.idsupplier',
                'name' => 'rms_supplier.name',
                'ruc' => 'rms_supplier.code',
                'service' => 'rms_supplier.service',
                'status' => 'rms_supplier.status',
                'address' => 'rms_supplier.address',
                'fax' => 'rms_supplier.fax',
                'phone' => 'rms_supplier.phone',
                'cellphone' => 'rms_supplier.cellphone',
                'email' => 'rms_supplier.email',
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_supplier';

            $where_conditions = " rms_supplier.status IS NOT NULL ";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $suppliers = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $suppliers, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
    }

    public function action_createorUpdateSupplier() {
        $a_response = $this->json_array_return;
        $supplier_id = $this->request->post('supplier_id');
        //Creo variable RUC
        $supplier_code = $this->request->post('s_code');
        //Busqueda de RUC en proveedor 
        $tmp = DB::select(db::expr('COUNT(rms_supplier.idSupplier) AS c'))
                ->from('rms_supplier')
                ->where('rms_supplier.code', '=', $supplier_code)
                ->and_where('rms_supplier.status', '=', self::STATUS_ACTIVE);

        if ($supplier_id == NULL) {
            $supplier = new Model_Supplier();
        } else {
            $supplier = new Model_Supplier($supplier_id);
            $tmp = $tmp->and_where('rms_supplier.idSupplier', '<>', $supplier_id);
        }
        $tmp = $tmp->as_object()->execute()->current();
        if ($tmp->c > 0) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = __("El RUC ya se encuentra registrado");
        } else {
            try {
                $post_supplier = Validation::factory($this->request->post())
                        ->rule('s_name', 'not_empty')
                        ->rule('s_code', 'not_empty')
                        ->rule('s_email', 'not_empty');
                if ($post_supplier->check()) {
                    $data_post_supplier = $post_supplier->data();
                    $a_returnSupplier = array();
                    $supplier->name = $data_post_supplier['s_name'];
                    $supplier->code = $data_post_supplier['s_code'];
                    $supplier->service = $data_post_supplier['s_service'];
                    $supplier->address = $data_post_supplier['s_address'];
                    $supplier->status = $supplier_id != 0 ? $supplier->status : self::STATUS_ACTIVE;
                    $supplier->phone = $data_post_supplier['s_phone'];
                    $supplier->fax = $data_post_supplier['s_fax'];
                    $supplier->cellphone = $data_post_supplier['s_cellphone'];
                    $supplier->email = $data_post_supplier['s_email'];
                    $supplier->save();

                    $a_returnSupplier = array('idSupplier' => $supplier->idSupplier,
                        'supplier' => $supplier->name,
                        'address' => $supplier->address);

                    $a_response ['data'] = $a_returnSupplier;
                } else {
                    throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
                }
            } catch (Exception $e_exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $this->errorHandling($e_exc);
            }
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getSupplierById() {
        $a_response = $this->json_array_return;
        try {
            $post_id_supplier = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($post_id_supplier->check()) {
                $data_supplier = $post_id_supplier->data();
                $supplier = New Model_Supplier($data_supplier['ids']);
                $a_response['data'] = $supplier->as_array();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_updateStatus() {
        $a_response = $this->json_array_return;
        try {
            $post_id_supplier = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($post_id_supplier->check()) {
                $data_supplier = $post_id_supplier->data();
                $supplier = New Model_Supplier($data_supplier['ids']);
                $supplier->status = $supplier->status == self::STATUS_DEACTIVE ? self::STATUS_ACTIVE : self::STATUS_DEACTIVE;
                $supplier->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteSupplier() {
        $a_response = $this->json_array_return;
        try {
            $post_id_supplier = Validation::factory($this->request->post())
                    ->rule('ids', 'not_empty');
            if ($post_id_supplier->check()) {
                $data_supplier = $post_id_supplier->data();
                $supplier = New Model_Supplier($data_supplier['ids']);
                $supplier->status = self::STATUS_DEACTIVE;
                $supplier->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}

?>
