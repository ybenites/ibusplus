<?php

 defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Cashbox extends Controller_Private_Admin{
    
    public function action_index(){
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('cashbox', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('cashbox', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        
        $user = $this->getSessionParameter(self::USER_ID);
        $data = DB::select(
                       array('us.idUser','idUser')
                       ,array('us.userName','userName')
                       ,array('per.fName','fName')
                       ,array('per.lName','lName')
                       ,array('per.email','email')
                       ,array('gro.name','namegro')
                       ,array('of.name','nameof')
                        )
                ->from(array('bts_user','us'))
                ->join(array('bts_person','per'))->on('per.idPerson','=','us.idUser')
                ->join(array('bts_group','gro'))->on('gro.idGroup','=','us.idGroup')
                ->join(array('bts_office','of'))->on('of.idOffice','=','us.idOffice')
                ->where('us.idUser','=',$user)
                ->execute()->as_array();
        
        $cashbox = DB::select(
                        array('ca.idCashBox','idCashBox')
                        ,array('ca.initAmount','initAmount')
                        ,array('ca.openDate','openDate')
                        ,array('ca.status','status')
                         )
                    ->from(array('rms_cashbox','ca'))
                    ->where(DB::expr("DATE_FORMAT(openDate,'%Y-%m-%d')"),'=',DateHelper::getfechaActual())
                    ->where('ca.closeDate', 'IS', NULL)
                    ->and_where('ca.idUser', '=', $user)
                    ->execute()->current();
        
    $status = DB::select(
                         array('ca.idCashBox','idCashBox')
                         ,array('ca.status','status')
                         ,array('ca.openDate','openDate')
                        )
                        ->from(array('rms_cashbox','ca'))
                        ->where('ca.idUser', '=', $user)
                        ->order_by('ca.status', 'ASC')
                        ->execute()->current();

        $date_format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $time_format = $this->getDATE_FORMAT_FOR(self::DF_PHP_TIME_INPUT);
  
        
        $sales_cashbox = new View('sales/cashbox');
        $sales_cashbox->typeUser = $this->getSessionParameter(self::USER_TYPE);
        $sales_cashbox->jquery_date_format = $this->getDATE_FORMAT_FOR(self::SO_JQUERY_DATE);
        $sales_cashbox->user = $data;
        $sales_cashbox->cashbox = $cashbox;
        $sales_cashbox->status = $status;
        $sales_cashbox->date_format = $date_format;
        $sales_cashbox->time_format = $time_format;
        $this->template->content = $sales_cashbox;
        
    }
    
    public function action_registerCashbox(){
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $cbixd = $this->request->post('cbixd');
            $mount = $this->request->post('mount');
            
            if($cbixd != NULL){
                $o_model_cashbox = new Model_Cashbox($cbixd);
                   if (!$o_model_cashbox->loaded())
                   throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                   $o_model_cashbox->status = self::CASHBOX_STATUS_CLOSED;
                   $o_model_cashbox->closeDate = DateHelper::getFechaFormateadaActual();
                   $o_model_cashbox->endAmount = $mount;
                   
             } else {
                    $o_model_cashbox = new Model_Cashbox();
                    $o_model_cashbox->status = self::CASHBOX_STATUS_OPENED;
                    $o_model_cashbox->openDate = DateHelper::getFechaFormateadaActual();
                    $o_model_cashbox->initAmount = $mount;
                    $o_model_cashbox->idOffice = $this->getSessionParameter(self::USER_OFFICE_ID);
                    $o_model_cashbox->idUser = $this->getSessionParameter(self::USER_ID);    
                }
                $o_model_cashbox->save();
                $o_model_cashbox->reload();
                $this->setSessionParameter(self::CASHBOX_ID, $o_model_cashbox->idCashBox);
                
//                $idCash = $this->getSessionParameter(self::CASHBOX_ID);
//                print_r($idCash);die();
                //print_r($o_model_cashbox->reload());die();
               //print_r($idCashBox);die();
               
                if ($this->getSessionParameter(self::CASHBOX_URL_REDIRECT) != '') {
                $aResponse['data'] = $this->getSessionParameter(self::CASHBOX_URL_REDIRECT);
            } else {
                $aResponse['data'] = 'noturl';
            }
            //$aResponse['data']='private/cashbox/index';
        } catch (Exception $e_exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($aResponse);
        
    }
    public function action_getCashBoxUser() {
        $idUser = $this->getSessionParameter(self::USER_ID);
        $dateOpenCash = $this->request->post('dateText');
        $this->getCashBox($dateOpenCash, $idUser);
    }
    public function action_fnOpenCashBox() {
        $aResponse = $this->json_array_return;
        try {
            $idCashBox = $this->request->post('idCashBox');
            //print_r($idCashBox);die();
            $check_post = Validation::factory($this->request->post())
                    ->rule('idCashBox', 'not_empty');
            if (!$check_post->check()) {
                throw new Exception('No Existe La Caja', self::CODE_SUCCESS);
            } else {
                $o_cashbox = new Model_Cashbox($idCashBox);
                if ($o_cashbox->loaded()) {
                        $o_cashbox->pastCashBox = self::STATUS_ACTIVE;
                        $o_cashbox->closeDate = null;
                        $o_cashbox->status = self::CASHBOX_STATUS_OPENED;
                        $o_cashbox->save();
                        $o_cashbox->reload();
                        $this->setSessionParameter(self::CASHBOX_ID, $o_cashbox->idCashBox);
                        if ($this->getSessionParameter(self::CASHBOX_URL_REDIRECT) != '') {
                        $aResponse['data'] = $this->getSessionParameter(self::CASHBOX_URL_REDIRECT);
                    } else {
                        $aResponse['data'] = 'noturl';
                    }
                } else {
                    throw new Exception('No Abrio Caja En Esta Fecha', self::CODE_SUCCESS);
                }
            }
        } catch (Exception $e_exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($aResponse);
    }
    public function action_fnOpenLastCashBox() {
        $aResponse = $this->json_array_return;
        try {
            $userId = $this->getSessionParameter(self::USER_ID);
//            $o_cashbox_ant = new Model_Cashbox();
//            $o_cashbox_ant->where('idUser', '=', ':USERID')
//                    ->param(':USERID', $userId)
//                    ->order_by('idCashBox', 'desc')
//                    ->find();
            $o_cashbox = new Model_Cashbox();
            $o_cashbox->where('idUser', '=', ':USERID')
                    ->param(':USERID', $userId)
                    ->order_by('idCashBox', 'desc')
                    ->find();
            if (!$o_cashbox->loaded()) {
                throw new Exception('No Hay Cajas', self::CODE_SUCCESS);
            } else {
                    $o_cashbox->pastCashBox = self::STATUS_ACTIVE;
                    $o_cashbox->closeDate = null;
                    $o_cashbox->status = self::CASHBOX_STATUS_OPENED;
                    $o_cashbox->save();
            }
        } catch (Exception $e_exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($aResponse);
    }
    public function action_findpasswordConfirm() {
        $this->auto_render = FALSE;
        $aResponse = $this->json_array_return;
        try {
            $password_confirm = $this->request->post('password_confirm');
            $o_check_post = Validation::factory($this->request->post())
                    ->rule('password_confirm', 'not_empty');
            if (!$o_check_post->check()) {
                throw new Exception(__("Información insuficiente para procesar su petición."), self::CODE_SUCCESS);
            } else {
                if ($this->getOptionValue(self::SO_AUTH_KEY) == $password_confirm) {
                    $aResponse['data'] = true;
                } else {
                    $aResponse['data'] = false;
                }
            }            
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        echo json_encode($aResponse);
    }
    
    
    
}