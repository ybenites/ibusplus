<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of customers
 *
 * @author Jhonatan CHOCMAN
 */
class Controller_Private_Sales extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->checkCashBox();
        $cacheInstance = Cache::instance('apc');
        $cacheKeyCss = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'css';
        $cacheKeyJs = Session::instance('database')->get('sessionKeyId') . $this->request->controller() . $this->request->action() . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, ConfigFiles::fnGetFiles('sales', self::FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, ConfigFiles::fnGetFiles('sales', self::FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->mergeStyles($incacheObjectCss);
        $this->mergeScripts($incacheObjectJs);
        $view_customers = new View('sales/sales');
        $view_customers->CLIENT_TYPE_PERSON = self::CLIENT_TYPE_PERSON;
        $view_customers->CLIENT_TYPE_COMPANY = self::CLIENT_TYPE_COMPANY;
        $view_customers->cash = self::SALES_PAYMENT_CASH;
        $view_customers->creditCard = self::SALES_PAYMENT_CREDITCARD;
        $view_customers->mixto = self::SALES_PAYMENT_MIXTO;
        $view_customers->a_clients_type = $this->a_clients_type();
        $view_customers->a_person_document_type = $this->a_PERSON_DOCUMENT_TYPE();
        $view_customers->a_documents_type = $this->getDocumentsByFilter('sales', true);
        
        $view_customers->BOLETA = self::DT_BOLETA;
        // Nuevos metodos de pagos 
        $view_customers->FACTURA = self::DT_FACTURA;
   //     $view_customers->TICKET = self::DT_TICKET;
        
        $view_customers->bTaxEnabled = $this->getOptionValue(self::SO_TAX_ENABLED);
        $view_customers->aSeller = $this->getOptionValue(self::SO_ACTIVE_SELLER);
        $view_customers->bBarcodeReader = $this->getOptionValue(self::INVENTORY_BARCODE_READER);
        $view_customers->sTaxName = $this->getOptionValue(self::SO_TAX_NAME);
        $view_customers->fTax = $this->getOptionValue(self::SO_TAX_VALUE);
        $view_customers->documentManual = $this->getOptionValue(self::SO_NRODOCUMENT_MANUAL);
        $view_customers->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_customers->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $user_incharge = $this->getSessionParameter(self::USER_ID);

        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->as_array();
        $view_customers->store = $store;
        $o_model_dpto = new Model_Ubigeo();
        $view_customers->bAvailableDocumentType = $this->getOptionValue(self::CORRELATIVO_IMPRESION) == self::STATUS_ACTIVE ? true : false;
        $view_customers->a_dpto = $o_model_dpto->where('idpais', '=', 51)
                        ->and_where('iddpto', '<>', 0)
                        ->and_where('idprovincia', '=', 0)
                        ->and_where('iddistrito', '=', 0)
                        ->find_all()->as_array();
        $view_customers->creditCardPayment = $this->fnGetClassVariable(self::VAR_NAME_CREDITCARD_PAYMENT);
        $view_customers->salesPayment = $this->fnGetClassVariable(self::VAR_NAME_METHODPAYMENT);
        $view_customers->methodPayment = $this->getOptionValue(self::SO_PAYMENT_FOR_SALES);
        
        $v_userCode = DB::select(
                                array('us.idUser','id'), array('us.userName','nameUser')
                        )
                        ->from(array('bts_user','us'))
                        ->join(array('bts_group','gr'))->on('gr.idgroup','=','us.idgroup')
                        ->where('us.idgroup', '=', 2)
                        ->execute()->as_array();
        $view_customers->listUserCode =$v_userCode;
        $view_customers->store = $store;
        
        $this->template->content = $view_customers;
    }
    
    public function action_getCodeOrderSales(){
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%'
            );
//            $o_select_code_ordersales = DB::select(array('so.idSalesOrder','id'), array(DB::expr("CONCAT(so.serie, ' - ', so.correlative)"), 'name'))
            $o_select_code_ordersales = DB::select(array('so.idSalesOrder','id'), array('so.correlative', 'name'))
                                        ->from(array('rms_salesorder','so'))
                                        ->where('so.correlative', 'LIKE', ':search')
                                        ->and_where('so.status','=',self::SALES_ORDER_STATUS_RESERVED)
                                        ->parameters($parameters)                    
                                        ->execute()->as_array();
            $aResponse['data'] = $o_select_code_ordersales;
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
        
    }

    public function action_getClients() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
                ':typeperson' => self::CLIENT_TYPE_PERSON,
                ':typecompany' => self::CLIENT_TYPE_COMPANY
            );
            $o_select_company = DB::select(
                                    'cus.idCustomers', array(DB::expr("CONCAT(IF(cus.idCustomersCompany IS NULL,pe.fullName, cuscom.name) ,' - ', IF(cus.idCustomersCompany IS NULL,pe.document, cuscom.code))"), 'customers'), array(DB::expr("IF(cus.idCustomersCompany IS NULL,:typeperson, :typecompany)"), 'type')
                            )
                            ->from(array('rms_customers', 'cus'))
                            ->join(array('bts_person', 'pe'), 'LEFT')->on('cus.idPerson', '=', 'pe.idPerson')
                            ->join(array('rms_customerscompany', 'cuscom'), 'LEFT')->on('cus.idCustomersCompany', '=', 'cuscom.idCustomersCompany')
                            ->where(DB::expr("CONCAT(pe.fullName, pe.document)"), 'LIKE', ':search')
                            ->or_where(DB::expr("CONCAT(cuscom.name, cuscom.code)"), 'LIKE', ':search')
                            ->parameters($parameters)
                            ->execute()->as_array();
            $a_response['data'] = $o_select_company;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
    }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function action_getProduct() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
            );
            
            
            $user_incharge = $this->getSessionParameter(self::USER_ID);

        $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->execute()->current();
     
            
            $o_select_product = DB::select(
                                    'p.idProduct'
                                    , 'stor.idStore'
                                    , array(DB::expr("CONCAT(p.productName,' - ', rmb.brandName, ' ',p.aliasProduct,' (',st.Amount,') - ',stor.storeName)"), 'product')
                                    , 'st.Amount'
                                    , 'p.haveDetail'
                                    , 'p.productPriceSell'
                            )
                            ->from(array('rms_stock', 'st'))
                            ->join(array('rms_products', 'p'))->on('st.idProduct', '=', 'p.idProduct')
                            ->join(array('rms_store', 'stor'))->on('st.idStore', '=', 'stor.idStore')
                            ->join(array('rms_brands', 'rmb'))->on('p.idBrand', '=', 'rmb.idBrand')
                            ->where(DB::expr("CONCAT(p.productName,' ',p.aliasProduct)"), 'LIKE', ':search')
                            ->and_where('p.status', '=', self::STATUS_ACTIVE)
                            ->and_where('stor.idStore', '=', $store['id'])
                            ->and_where('p.haveDetail','=',0)
                            ->group_by('p.aliasProduct')
                            ->group_by('st.idStore')
                            ->parameters($parameters)
                            ->execute()->as_array();
//            echo Database::instance()->last_query; die();
//            
            $a_response['data'] = $o_select_product;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
        
    }

    public function action_getProductByCode() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {

            $s_search = $this->request->post('code');
            $parameters = array(
                ':search' => '%' . $s_search . '%',
            );
            $user = $this->getSessionParameter(self::USER_ID);
            $store = DB::select(
                            array('st.idStore','idStore') 
                            )
                    ->from(array('bts_user','us'))
                    ->JOIN(array('rms_store','st'))->ON('st.idUser','=','us.idUser')
                    ->where('us.idUser','=',$user)
                    ->execute()->current();
                    
            
            $o_select_product = DB::select(
                                    'p.idProduct'
                                    , 'stor.idStore'
                                    , array(DB::expr("CONCAT(p.productName,' - ', rmb.brandName, ' ',p.aliasProduct,' (',SUM(st.Amount),') - ',stor.storeName)"), 'product')
                                    , array(DB::expr('SUM(st.Amount)'), 'Amount')
                                    , 'p.haveDetail'
                                    , 'p.productPriceSell'
                                    , array(DB::expr("IF(p.havedetail=1,pd.serie_electronic,p.aliasProduct)"),'CodeProduct')
                            )
                            ->from(array('rms_stock', 'st'))
                            ->join(array('rms_products', 'p'))->on('st.idProduct', '=', 'p.idProduct')
                            ->join(array('rms_product_detail', 'pd'),'LEFT')->on('pd.idProduct', '=', 'p.idProduct')
                            ->join(array('rms_store', 'stor'))->on('st.idStore', '=', 'stor.idStore')
                            ->join(array('rms_brands', 'rmb'))->on('p.idBrand', '=', 'rmb.idBrand')
                            ->where_open()
                            ->where(DB::expr("CONCAT(p.productName,' ',p.aliasProduct)"), 'LIKE', ':search')
                            ->or_where(DB::expr("CONCAT(p.productName,' ',pd.serie_electronic)"), 'LIKE', ':search')
                            ->where_close()
                            ->and_where('stor.idStore','=',$store)
                            ->group_by('st.idProduct')
                            ->parameters($parameters)
                            ->execute()->as_array();
            //print_r(Database::instance()->last_query); die();

            $a_response['data'] = $o_select_product;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function action_getSalesOrder() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $salesOrder = new Model_Salesorder();
            $salesOrder = $salesOrder->find_all()->as_array();

            $a_response['data'] = array_map(function($salesOrder) {
                        return $salesOrder->as_array();
                    }
                    , $salesOrder
            );
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function action_getDetailSalesOrder() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $o_post_id = Validation::factory($this->request->post())
//                    ->rule('id', 'not_empty')
                    ->rule('idOrder', 'not_empty')
            ;
            if ($o_post_id->check()) {
                $data_post_id = $o_post_id->data();

                $parameter = array(
             //       ':idSales' => $data_post_id['id'],
                    ':idOrder' => $data_post_id['idOrder'],
                    ':person' => self::CLIENT_TYPE_PERSON,
                    ':company' => self::CLIENT_TYPE_COMPANY,
                    ':status' => self::SALES_ORDER_STATUS_RESERVED,
                    ':active' => self::SALES_ORDERDETAIL_STATUS_ACTIVE
                );

                $data = DB::select(array(DB::expr("IF(cu.idPerson IS NULL,CONCAT(cuc.name,' - ', cuc.code),CONCAT(pe.fullName,' - ',pe.document))"), 'customers')
                                        , array(DB::expr("IF(cu.idPerson IS NULL,:company,:person)"), 'typeCustomers')
                                        , array(DB::expr("CONCAT(pro.productName,' - ',pro.aliasProduct)"), 'description')
                                        , 'sod.idSalesOrderDetail'
                                        , 'so.idSalesOrder'
                                        , 'so.idCustomers'
                                        , 'sod.quantity'
                                        , 'sod.unitPrice'
                                        , 'sod.totalPrice'
                                        , 'sod.discount'
                                        , 'pro.idProduct'
                                        , 'sod.idStore'
                                        , 'stoc.amount'
                                        , 'so.totalAmount'
                                        , 'so.amountPaid'
                                        , 'so.amountToPaid'
                                        , 'sod.idLot'
                                        ,'so.idUserCode')
                                ->from(array('rms_salesorder', 'so'))
                                ->join(array('rms_salesorderdetail', 'sod'))->on('so.idSalesOrder', '=', 'sod.idSalesOrder')
                                ->join(array('rms_customers', 'cu'))->on('so.idCustomers', '=', 'cu.idCustomers')
                                ->join(array('bts_person', 'pe'), 'LEFT')->on('cu.idPerson', '=', 'pe.idPerson')
                                ->join(array('rms_customerscompany', 'cuc'), 'LEFT')->on('cu.idCustomersCompany', '=', 'cuc.idCustomersCompany')
                                ->join(array('rms_lot', 'lot'))->on('sod.idLot', '=', 'lot.idLot')
                                ->join(array('rms_products', 'pro'))->on('lot.idProduct', '=', 'pro.idProduct')
                                ->join(array('rms_store', 'sto'))->on('sto.idStore', '=', 'sod.idStore')
                                ->join(array('rms_stock', 'stoc'))->on('stoc.idProduct', '=', DB::expr("pro.idProduct AND stoc.idStore = sto.idStore"));

   //             if ($data_post_id['id'] == '0') {
                    $data->where('so.correlative', '=', ':idOrder');
//                } else {
//                    $data->where('so.idSalesOrder', '=', ':idSales');
//                }

                $data = $data->and_where('so.status', '=', ':status')
                                ->and_where('sod.status', '=', ':active')
                                ->parameters($parameter)
                                ->execute()->as_array();

//            print_r(Database::instance()->last_query);
//                die();
                $a_response['data'] = $data;
                

                
                
            } else {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
            
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function action_createSales() {
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('header', 'not_empty')
                    ->rule('detail', 'not_empty');
            
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $a_header = $this->request->post('header');
                $a_detail = $this->request->post('detail');
                $a_detail_product = $this->request->post('detail_product');
                $a_methodPayment = $this->request->post('methodPayment');
                $a_payment= $this->request->post('payment');
                $a_detail_product_l = $this->request->post('detail_product_l');
                $idStore = $this->request->post('idStore');
                $a_userCode = $this->request->post('userCode');
                $a_type = $this->request->post('type');
                $id_User = $this->getSessionParameter(self::USER_ID);
                $array = array();
                
                if ($a_type == 'direct') {                   
                    if (count($array) > 0) {
                        $a_response['data'] = $array;
                    } else {
                        $a_response['id'] = $this->getRegister($a_header, $a_detail, $a_detail_product, $a_detail_product_l, $idStore,$a_userCode, $id_User, $array, $a_methodPayment, $a_payment, $a_type);
                    }
                } elseif ($a_type == 'creditnote') {
                    $a_response['id'] = $this->getRegister($a_header, $a_detail, $a_detail_product, $a_detail_product_l, $idStore,$a_userCode, $id_User, $array, $a_methodPayment,$a_payment, $a_type);
                } elseif ($a_type == 'salesorder') {
                    $a_response['id'] = $this->getRegister($a_header, $a_detail, $a_detail_product, $a_detail_product_l, $idStore,$a_userCode, $id_User, $array, $a_methodPayment,$a_payment, $a_type);
                }
                
                $a_response['data'] = $array;
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
        die();
    }

    public function getRegister($a_header, $a_detail, $a_detail_product, $a_detail_product_l, $idStore, $a_userCode, $id_User, $array, $a_methodPayment, $a_payment, $direc) {
        Database::instance()->begin();
        $sales = new Model_Sales();
        
        $idCash = $this->getSessionParameter(self::CASHBOX_ID);
         
        if ($this->getOptionValue(self::CORRELATIVO_IMPRESION) == true) {
                 if ($this->getOptionValue(self::SO_NRODOCUMENT_MANUAL) == true) {
                     $sales->serie = $a_header['serie'];
                     $sales->correlative = $a_header['correlative'];
     //                $sales->typeSales = self::STATUS_ACTIVE; 
                 } else {
                     $oSerieUpdate = $this->getNextCorrelative($a_header['document_type']);
                     $sales->serie = $oSerieUpdate->serie;
                     $sales->correlative = $oSerieUpdate->correlative;
     //                $sales->typeSales = self::STATUS_DEACTIVE; 
                 }                
        } else{
            $oSerieUpdate = $this->getNextCorrelative(self::DT_DEFAULT_CORRELATIVE);
        }          
        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
            $sales->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
        } else {
            $sales->taxpercentage = 0;
        }
        
        $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        $dateSale = DateTime::createFromFormat($format, $a_header['input_date_sale']);

        // Tipos de Venta
        if ($direc == 'direct') {
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_VENTA_DIRECTA;
            $obj_move->moveReference = 'Venta Directa';
            $obj_move->idCustomer = $a_header['idCustomers'];
            $obj_move->idStore = $idStore;
            $obj_move->status = self::STATUS_ACTIVE;
            $obj_move->save();
            $sales->status = self::SALES_STATUS_SOLD;
            $sales->salesDate = DateHelper::fnPHPDate2MySQLDatetime($dateSale);
            $sales->documentType = $a_header['document_type'];
            $sales->idCustomers = $a_header['idCustomers'];
            $sales->idUser = $id_User;
            $sales->salesType = self::SALES_DIRECT;
            $sales->idUserCode = $a_userCode;
            $sales->reprint = 0;
            $sales->save();
            $f_sum_total_tax = 0;
            $f_sum_total_amount = 0;

            $idMov = $obj_move->idMove;

            foreach ($a_detail as $key => $a_item) {
                $stock = ORM::factory('stock')->where('idProduct', '=', $a_item['id'])
                                ->and_where('idStore', '=', $idStore)->find(); // $inquiry  = Model_Inquiry($id) doesn't work, too
                if ($stock->loaded()) {
                    $stock->amount = $stock->amount - intval($a_item['quantityOut']);
                    $stock->save();
                }

                if ($a_item['haveDetail'] == self::STATUS_ACTIVE) {

                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = 1;
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];

                        $obj_movedetail = new Model_Movedetail();
                        $obj_movedetail->idMove = $idMov;
//                        $obj_movedetail->idProduct = $a_item['id'];                
                        $obj_movedetail->status = self::STATUS_ACTIVE;
                        $obj_movedetail->amount = 1;
                        $obj_movedetail->unitPrice = $a_item['unitPriceOut'];

                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0.0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];
                        $o_salesdetail->statusDetail = self::SALES_DETAIL_SOLD;

                        $obj_product_d = new Model_Productdetail($a_item['idOut']);

                        if ($obj_product_d->idProduct == $a_item['id']) {
                            $o_salesdetail->idProductDetail = $obj_product_d->idProductDetail;
                            $obj_product_d->amount = self::ZERO;
                            $obj_product_d->save();

                            $obj_movedetail->idProductDetail = $obj_product_d->idProductDetail;
                            $obj_movedetail->save();
                            $o_salesdetail->save();
                        }

                } else {
                    foreach ($a_detail_product_l as $pd) {
                        
                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = $pd['amount'];
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];
                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];
                        $o_salesdetail->statusDetail = self::SALES_DETAIL_SOLD;
                        
                        //Buscar lotes 
                        
                        $obj_product_lot = new Model_Lot($pd['id']);

                        if ($obj_product_lot->idProduct == $a_item['id']) {
                            $obj_product_lot->amount = $obj_product_lot->amount - $pd['amount'];

                            $obj_movedetail = new Model_Movedetail();
                            $obj_movedetail->idMove = $idMov;
//                            $obj_movedetail->idProduct = $a_item['id'];                
                            $obj_movedetail->status = self::STATUS_ACTIVE;
                            $obj_movedetail->idLot = $pd['id'];
                            $obj_movedetail->amount = $pd['amount'];
                            $obj_movedetail->unitPrice = $a_item['unitPriceOut'];
                            $obj_product_lot->save();
                            $obj_movedetail->save();

                            $o_salesdetail->idLot = $obj_product_lot->idLot;
                            $o_salesdetail->save();
                        }
                    }
                }

                $f_sum_total_tax = $f_sum_total_tax + $a_item['taxUnitOut'];
                $f_sum_total_amount = $f_sum_total_amount + $a_item['subTotalOut'];
            }

            $sales->totaltax = $f_sum_total_tax;
            $sales->totalPrice = $f_sum_total_amount;

            $id_sales = array('idsales' => $sales->idSales, 'document' => $sales->documentType);
            //Proceso de Registro de Tipo de Pago

            if ($a_methodPayment == self::SALES_PAYMENT_CASH) {
                $sales->methodPayment = self::SALES_PAYMENT_CASH;
                $sales->paymentAmount = $a_payment['post_confirm_payment'];
                // amount_paymentCash
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                //relaciona la venta con la Aper de caja
                $sales->idCashBox = $idCash;
            } elseif ($a_methodPayment == self::SALES_PAYMENT_CREDITCARD) {
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['amount_paymentCredit'];
                $paymentCard->cardNumber = $a_payment['cardNumber'];
                $paymentCard->operationNumber = $a_payment['operationNumber'];
                $paymentCard->idTypeCard = $a_payment['typeCard'];
                $paymentCard->status = self::SALES_STATUS_SOLD;
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $paymentCard->save();
                //relaciona la venta con la Aper de caja
                $sales->idCashBox = $idCash;
                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->methodPayment = self::SALES_PAYMENT_CREDITCARD;
            
            }
            elseif($a_methodPayment == self::SALES_PAYMENT_MIXTO){
                $sales->methodPayment = self::SALES_PAYMENT_MIXTO;
                // Con efectivo
                $sales->paymentAmount = $a_payment['amount_paymentCashMixto'];
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                // Con tarjeta de credito                
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['amount_paymentCreditMixto'];
                $paymentCard->cardNumber = $a_payment['cardNumberMixto'];
                $paymentCard->operationNumber = $a_payment['operationNumberMixto'];
                $paymentCard->idTypeCard = $a_payment['typeCardMixto'];
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $paymentCard->save();
                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->paymentCash = $f_sum_total_amount - $a_payment['amount_paymentCreditMixto'] ;
                $sales->paymentCard = $a_payment['amount_paymentCreditMixto'];  
                //relaciona la venta con la Aper de caja
                $sales->idCashBox = $idCash;
                        //$idCashBox_sales['idCashBox'];
            }
            $sales->save();
        } 
        
        else if ($direc == 'creditnote') {
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_VENTA_DIRECTA;
            $obj_move->moveReference = 'Venta por Credito';
            $obj_move->idCustomer = $a_header['idCustomers'];
            $obj_move->idStore = $idStore;
            $obj_move->status = self::STATUS_ACTIVE;
            $obj_move->save();
            $sales->status = self::SALES_STATUS_SOLD;
            $sales->salesDate = DateHelper::fnPHPDate2MySQLDatetime($dateSale);
            $sales->documentType = $a_header['document_type'];
            $sales->idCustomers = $a_header['idCustomers'];
            $sales->idUser = $id_User;
            $sales->idUserCode = $a_userCode;
            $sales->save();
            $f_sum_total_tax = 0;
            $f_sum_total_amount = 0;
            $idMov = $obj_move->idMove;
            $idStore = $obj_move->idStore;
            foreach ($a_detail as $key => $a_item) {
                $stock = ORM::factory('stock')->where('idProduct', '=', $a_item['id'])
                                ->and_where('idStore', '=', $idStore)->find(); // $inquiry  = Model_Inquiry($id) doesn't work, too
                if ($stock->loaded()) {
                    $stock->amount = $stock->amount - $a_item['quantityOut'];
                    $stock->save();
                } else {
                    /* $obj_stock = new Model_Stock();
                      $obj_stock->idProduct = $a_item['id'];
                      $obj_stock->idStore = $idStore;
                      //$obj_stock->amount = $a_item['quantity']; OBSERvACION, asi estaba pero creo k no debe ser asi.
                      $obj_stock->amount = $a_item['quantityOut'];
                     */
                    
                }
                if ($a_item['haveDetail'] == self::STATUS_ACTIVE) {
                    foreach ($a_detail_product as $pd) {
                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = 1;
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];

                        $obj_movedetail = new Model_Movedetail();
                        $obj_movedetail->idMove = $idMov;
//                        $obj_movedetail->idProduct = $a_item['id'];                
                        $obj_movedetail->status = self::STATUS_ACTIVE;
                        $obj_movedetail->amount = 1;
                        $obj_movedetail->unitPrice = $a_item['unitPriceOut'];

                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];
                        $o_salesdetail->statusDetail = self::SALES_DETAIL_SOLD;
                        $obj_product_d = new Model_Productdetail($pd);
                        //print_r($a_item['id']); die();
                        if ($obj_product_d->idProduct == $a_item['id']) {
                            $o_salesdetail->idProductDetail = $obj_product_d->idProductDetail;
                            $obj_product_d->amount = self::ZERO;
                            $obj_product_d->save();

                            $obj_movedetail->idProductDetail = $obj_product_d->idProductDetail;
                            $obj_movedetail->save();
                            $o_salesdetail->save();
                        }
                    }
                } else {

                    foreach ($a_detail_product_l as $pd) {
                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = $pd['amount'];
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];
                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];

                        $obj_product_lot = new Model_Lot($pd['id']);

                        if ($obj_product_lot->idProduct == $a_item['id']) {
                            $obj_product_lot->amount = $obj_product_lot->amount - $pd['amount'];

                            $obj_movedetail = new Model_Movedetail();
                            $obj_movedetail->idMove = $idMov;
//                            $obj_movedetail->idProduct = $a_item['id'];                
                            $obj_movedetail->status = self::STATUS_ACTIVE;
                            $obj_movedetail->idLot = $pd['id'];
                            $obj_movedetail->amount = $pd['amount'];
                            $obj_movedetail->unitPrice = $a_item['unitPriceOut'];
                            $obj_product_lot->save();
                            $obj_movedetail->save();

                            $o_salesdetail->idLot = $obj_product_lot->idLot;
                            $o_salesdetail->save();
                        }
                    }
                }

                $f_sum_total_tax = $f_sum_total_tax + $a_item['taxUnitOut'];
                $f_sum_total_amount = $f_sum_total_amount + $a_item['subTotalOut'];
            }

            $sales->totaltax = $f_sum_total_tax;
            $sales->totalPrice = $a_payment['post_confirm_payment'];

            $id_sales = array('idsales' => $sales->idSales, 'document' => $sales->documentType);
            //Proceso de Registro de Tipo de Pago

            if ($a_methodPayment == self::SALES_PAYMENT_CASH) {
                $sales->methodPayment = self::SALES_PAYMENT_CASH;
                $sales->paymentAmount = $a_payment['post_confirm_payment'];
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                $sales->idCashBox = $idCash;
                $sales->save();
            } 
            elseif ($a_methodPayment == self::SALES_PAYMENT_CREDITCARD) {
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['amount_paymentCredit'];
                $paymentCard->cardNumber = $a_payment['cardNumber'];
                $paymentCard->operationNumber = $a_payment['operationNumber'];
                $paymentCard->idTypeCard = $a_payment['typeCard'];
                $paymentCard->status = self::SALES_STATUS_SOLD;
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $sales->idCashBox = $idCash;
                $paymentCard->save();

                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->methodPayment = self::SALES_PAYMENT_CREDITCARD;
                $sales->idPaymentCard = $paymentCard->idPaymentCard;
            }
           elseif($a_methodPayment == self::SALES_PAYMENT_MIXTO){
                $sales->methodPayment = self::SALES_PAYMENT_MIXTO;
                // Con efectivo
                $sales->paymentAmount = $a_payment['amount_paymentCashMixto'];
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                $sales->idCashBox = $idCash;
                // Con tarjeta de credito                
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['amount_paymentCreditMixto'];
                $paymentCard->cardNumber = $a_payment['cardNumberMixto'];
                $paymentCard->operationNumber = $a_payment['operationNumberMixto'];
                $paymentCard->idTypeCard = $a_payment['typeCardMixto'];
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $paymentCard->save();
                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->paymentCash = $f_sum_total_amount - $a_payment['amount_paymentCreditMixto'] ;
                $sales->paymentCard = $a_payment['amount_paymentCreditMixto'];  
            }
            $sales->save();
            // Actualiza la Nota de Credito
            $creditnote = ORM::factory('creditnote')->
                            where('idCreditNote', '=', $a_header['idCreditNote'])->find();
            $creditnote->status = self::CREDITNOTE_USED;
            $creditnote->idCashBoxConfirm = $idCash;
            $creditnote->confirmDate = DateHelper::getFechaFormateadaActual();
            $creditnote->userConfirm = $this->getSessionParameter(self::USER_ID);
            $creditnote->idNewSales = $id_sales['idsales'];
            $creditnote->save();
        }
        
        else if ($direc == 'salesorder') {
            
            //Crear movimiento           
            $obj_move = new Model_Move();
            $obj_move->typeMoveStore = self::TYPE_MOVE_VENTA_RESERVA;
            $obj_move->moveReference = 'Venta de Reserva';
            $obj_move->idCustomer = $a_header['idCustomers'];
            $sales->salesType = self::SALES_ORDER;
            $obj_move->idStore = $idStore;
            $obj_move->status = self::STATUS_ACTIVE;
            $obj_move->save();
            
            $salesorderdetail = ORM::factory('salesorderdetail')
                    ->where('idSalesOrder', '=', $a_header['idOrderSales'])
                    ->find();
            $sales->status = self::SALES_STATUS_SOLD;
            $sales->idSalesOrder = $a_header['idOrderSales'];
            $sales->salesDate = DateHelper::fnPHPDate2MySQLDatetime($dateSale);
            $sales->documentType = $a_header['document_type'];

            $sales->idCustomers = $a_header['idCustomers'];
            $sales->idSalesOrder = $a_header['idOrderSales'];
            $sales->idUser = $id_User;
            $sales->idUserCode = $a_userCode;
            $sales->save();
   
            $f_sum_total_tax = 0;
            $f_sum_total_amount = 0;
            foreach ($a_detail as $key => $a_item) {

                if ($a_item['haveDetail'] == self::STATUS_ACTIVE) {
                    foreach ($a_detail_product as $pd) {
                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = 1;
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];

                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];
                        $o_salesdetail->idProducDetail = $salesorderdetail->idProductDetail;
                        $o_salesdetail->save();
                    }
                } 
                else {
                    //foreach ($a_detail_product_l as $pd) {
                        $o_salesdetail = new Model_Salesdetail();
                        $o_salesdetail->idSales = $sales->idSales;
                        $o_salesdetail->idStore = $idStore;
                        $o_salesdetail->quantity = $a_item['quantityOut'];
                        $o_salesdetail->unitPrice = $a_item['unitPriceOut'];
                        if ($this->getOptionValue(self::SO_TAX_ENABLED)) {
                            $o_salesdetail->taxpercentage = $this->getOptionValue(self::SO_TAX_VALUE);
                        } else {
                            $o_salesdetail->taxpercentage = 0;
                        }
                        $o_salesdetail->totaltax = $a_item['taxUnitOut'];
                        $o_salesdetail->discount = $a_item['discountOut'];
                        $o_salesdetail->totalPrice = $a_item['subTotalOut'];
                        $o_salesdetail->idLot = $salesorderdetail->idLot;
                        $o_salesdetail->save();
                    //}
                }

                $f_sum_total_tax = $f_sum_total_tax + $a_item['taxUnitOut'];
             //   $f_sum_total_amount = $f_sum_total_amount + $a_item['subTotalOut'];
            }

            $sales->totaltax = $f_sum_total_tax;
            $sales->totalPrice = $a_payment['post_confirm_payment'];

            $id_sales = array('idsales' => $sales->idSales, 'document' => $sales->documentType);

            //Proceso de Registro de Tipo de Pago
            if ($a_methodPayment == self::SALES_PAYMENT_CASH) {
                $sales->methodPayment = self::SALES_PAYMENT_CASH;
                $sales->paymentAmount = $a_payment['post_confirm_payment'];
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                $sales->idCashBox = $idCash;
                $sales->save();
            } elseif ($a_methodPayment == self::SALES_PAYMENT_CREDITCARD) {
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['post_confirm_payment'];
                $paymentCard->cardNumber = $a_payment['cardNumber'];
                $paymentCard->operationNumber = $a_payment['operationNumber'];
                $paymentCard->idTypeCard = $a_payment['typeCard'];
                $paymentCard->status = self::SALES_STATUS_SOLD;
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $sales->idCashBox = $idCash;
                $paymentCard->save();

                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->methodPayment = self::SALES_PAYMENT_CREDITCARD;
            }elseif($a_methodPayment == self::SALES_PAYMENT_MIXTO){
                $sales->methodPayment = self::SALES_PAYMENT_MIXTO;
                $sales->idCashBox = $idCash;
                // Con efectivo
                $sales->paymentAmount = $a_payment['amount_paymentCashMixto'];
                $sales->returnCash = $sales->paymentAmount - $sales->totalPrice;
                // Con tarjeta de credito                
                $paymentCard = new Model_Paymentcard();
                $paymentCard->amount = $a_payment['amount_paymentCreditMixto'];
                $paymentCard->cardNumber = $a_payment['cardNumberMixto'];
                $paymentCard->operationNumber = $a_payment['operationNumberMixto'];
                $paymentCard->idTypeCard = $a_payment['typeCardMixto'];
                $paymentCard->dateRegister = DateHelper::getFechaFormateadaActual();
                $paymentCard->idUser = $this->getSessionParameter(self::USER_ID);
                $paymentCard->save();
                $sales->idPaymentCard = $paymentCard->idPaymentCard;
                $sales->paymentCash = $f_sum_total_amount - $a_payment['amount_paymentCreditMixto'] ;
                $sales->paymentCard = $a_payment['amount_paymentCreditMixto'];  
            }
            $sales->save();
            $salesOrder = new Model_Salesorder($a_header['idOrderSales']);
            $salesOrder->status = self::SALES_ORDER_STATUS_SOLD;
            $salesOrder->idCashBoxConfirm = $idCash;
            $salesOrder->confirmDate = DateHelper::getFechaFormateadaActual();
            $salesOrder->userConfirm = $this->getSessionParameter(self::USER_ID);
            $salesOrder->save();

            /* Creo Historial */

            $historySalesOrder = new Model_HistorySalesOrder();
            $obj_SalesOrder['idSalesOrder']=$salesOrder->idSalesOrder;
            $obj_SalesOrder['status']=  self::SALES_ORDER_STATUS_SOLD;
            $obj_SalesOrder['dateHistorySalesOrder']=  DateHelper::getFechaFormateadaActual();
            $obj_SalesOrder['idUser']=$id_User; 
            $historySalesOrder->saveHistorySalesOrder($obj_SalesOrder);

        }
        
        $obj_move->idSales = $sales->idSales;
        $obj_move->save();

        if($this->getOptionValue(self::SO_SEND_EMAIL_STOCKMINIMUM)==true){             
           $msg = $this->checkStock($a_detail);
           if($msg != '')
             $this->sendEmailOrMessageMinimunStock($msg);
        }  

        Database::instance()->commit();
        return $id_sales;
    }        
    
    public function action_getPrintSales() {
        $this->auto_render = FALSE;
        try {
            $o_post_sales = Validation::factory($_GET)
                    ->rule('sales_id', 'not_empty')
                    ->rule('document', 'not_empty');
            if (!$o_post_sales->check()) {
                throw new Exception('ERROR -' . __('Empty Data'), self::CODE_ERROR);
            } else {
                $i_sales_id = $this->request->query('sales_id');
                $i_document = $this->request->query('document');
                
                $objSales = new Model_Sales($i_sales_id);
                $objSales->reprint += 1;
                $objSales->save(); 
                $this->printDocument($i_sales_id, $i_document);
            }
        } catch (Exception $exc) {
            echo __('Impresión Inválida');
        }
    }

    public function action_getDetailByProductId() {
        $a_response = $this->json_array_return;
        try {
            $id_product = $this->request->post('idpg');
            //con esto traemos el id del almacen
            $user_incharge = $this->getSessionParameter(self::USER_ID);
            $obj_store = ORM::factory('stores')
                            ->where('status', '=', ':STATUS')
                            ->and_where('idUser', '=', ':INCHARGE')
                            ->param(':STATUS', self::STATUS_ACTIVE)
                            ->param(':INCHARGE', $user_incharge)->find();
            if ($obj_store->idStore != '' && $id_product != '') {
                $id_store = $obj_store->idStore;
                /* $query_data_not_show = "SELECT mds.idProductDetail FROM `rms_movedetail` mds 
                  WHERE mds.idMoveDetail IN (SELECT MAX(mvss.idMoveDetail) FROM rms_product_detail pdss
                  INNER JOIN rms_movedetail mvss ON mvss.idProductDetail=pdss.idProductDetail
                  INNER JOIN rms_move mss ON mvss.idMove=mss.idMove
                  WHERE mss.idStore=$obj_store->idStore AND pdss.amount<>0 AND (mss.typeMoveStore='VENTA_DIRECTA' OR mss.typeMoveStore='TRAS_SALIDA' OR mss.typeMoveStore='SALIDA')
                  GROUP BY pdss.idProductDetail)";
                 * 
                 * 
                 */


                $query_data_not_show = 'SELECT rpd.idProductDetail,rp.*,rpd.* FROM rms_products rp ' .
                        'INNER JOIN rms_product_detail rpd ON rp.idProduct=rpd.idProduct ' .
                        'INNER JOIN rms_movedetail mvdi ON mvdi.idProductDetail=rpd.idProductDetail ' .
                        'INNER JOIN rms_move mv ON mvdi.idMove=mv.idMove ' .
                        'WHERE rp.idProduct=' . $id_product . '  AND mv.idStore=' . $id_store . ' AND rpd.amount<>0 ' .
                        'GROUP BY rpd.idProductDetail HAVING MOD(COUNT(rpd.idProductDetail),2)<>0 ';
                $rs = DB::query(Database::SELECT, $query_data_not_show)->execute()->as_array();
                //echo Database::instance()->last_query;
                //die();
                /* $cad = '';
                  if (count($rs) > 0) {
                  foreach ($rs as $key => $v) {
                  $a[$key] = $v['idProductDetail'];
                  }
                  $cad = implode(',', $a);
                  } else {
                  $cad = 0;
                  } */

                /* $query_pd = "SELECT rp.*,rpd.* FROM  rms_products rp 
                  INNER JOIN rms_product_detail rpd  ON rp.idProduct = rpd.idProduct
                  INNER JOIN rms_movedetail mvdi ON mvdi.idProductDetail = rpd.idProductDetail
                  INNER JOIN rms_move mv     ON mvdi.idMove = mv.idMove
                  WHERE rp.idProduct =$id_product   AND mv.idStore =$obj_store->idStore AND rpd.amount <> 0   AND rpd.idProductDetail NOT IN ($cad) ORDER BY rp.idProduct DESC";
                 * 
                 */
                //$rs = DB::query(Database::SELECT, $query_pd)->execute()->as_array();
                $a_response['data'] = $rs;
            } else {
                throw new Exception('ERROR-' . __('Seleccione un AlmacÃ©n'), self::CODE_ERROR);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getProductLotById() {
        $a_response = $this->json_array_return;
        try {
            $id_product = $this->request->post('idpg');
            //con esto traemos el id del almacen
            $user_incharge = $this->getSessionParameter(self::USER_ID);
            $obj_store = ORM::factory('stores')
                            ->where('status', '=', ':STATUS')
                            ->and_where('idUser', '=', ':INCHARGE')
                            ->param(':STATUS', self::STATUS_ACTIVE)
                            ->param(':INCHARGE', $user_incharge)->find();
            if ($obj_store->idStore != '' && $id_product != '') {
                $query_pd = "SELECT   rpd.idLot AS idLot,  rp.idProduct AS idProduct,
                    rp.productName AS productName,  rpd.lotName AS lotName,  mvdi.unitPrice AS unitPrice,
                    rpd.amount AS amount,  rpd.description AS description, rp.haveDetail , mv.idStore ,
                    DATE_FORMAT(    rpd.creationDate,    '%d/%m/%Y %h:%i %p'  ) AS creationDate 
                    FROM  rms_products rp 
                    INNER JOIN rms_lot rpd     ON rp.idProduct = rpd.idProduct 
                    INNER JOIN rms_movedetail mvdi     ON mvdi.idLot = rpd.idLot 
                    INNER JOIN rms_move mv     ON mvdi.idMove = mv.idMove 
                    WHERE rp.idProduct =$id_product   AND mv.idStore =$obj_store->idStore   
                        AND rpd.amount > 0   
                        AND mv.typeMoveStore <> 'VENTA_DIRECTA'   
                        AND mv.typeMoveStore <> 'TRAS_SALIDA'   
                        AND mv.typeMoveStore <> 'SALIDA' 
                    GROUP BY rpd.idLot ORDER BY creationDate ASC ";

                $rs = DB::query(Database::SELECT, $query_pd)->execute()->as_array();
                
               //echo Database::instance()->last_query; die();
                $a_response['data'] = $rs;
            } else {
                $a_response['data'] = 'not_store';
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getSerieCorrelativeCanceledByTerm() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $s_search = $this->request->post('term');
            $parameters = array(
                ':search' => '%' . $s_search . '%'
            );
            $o_select_serie_correlative = DB::select(
                                    array('s.idSales', 'id'), array(DB::expr("CONCAT(s.serie, ' - ', s.correlative)"), 'name')
                            )
                            ->from(array('rms_sales', 's'))
                            ->join(array('rms_credit_note','cn'),'LEFT')->on('s.idSales', '=','cn.idSales')
                            ->where(DB::expr("CONCAT(s.serie, '-', s.correlative)"), 'LIKE', ':search')
                            ->and_where('s.status', '=', self::SALES_STATUS_CANCELED)
                            ->and_where('cn.status', '=', self::CREDITNOTE_CREATE)
                            ->parameters($parameters)
                            ->execute()->as_array();
            $aResponse['data'] = $o_select_serie_correlative;
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($aResponse, 'json');
        die();
    }
 
    /* Verifica el Stock de Productos */
    public function checkStock( $a_detail){
        $mensaje = '';
        foreach($a_detail as $a_item){
            $product = new Model_Products($a_item['id']);
            $stock = DB::select(array(DB::expr("sum(amount)"),'stock'))
                    ->from('rms_stock')
                    ->group_by('idProduct')
                    ->where('idProduct','=',$product->idProduct)
                    ->as_object()->execute()->current();
            
            if($stock->stock <= $product->minimumstock){
                $mensaje = $mensaje."<p> * ".$product->aliasProduct.' => '.$product->productName."</b></p>";
            }  
        }
        
        return $mensaje;
    }
    
    /* Codigo de Barras Karina */
    public function action_getProductByDescription(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $s_search = $this->request->post('code');
            
            // Ver el almacen asignado el usuario            
            $user_incharge = $this->getSessionParameter(self::USER_ID);
            $store = DB::select(
                                array('idStore', 'id'), array('storeName', 'display')
                        )
                        ->from('rms_store')
                        ->where('status', '=', ':STATUS')
                        ->and_where('idUser', '=', ':INCHARGE')
                        ->param(':STATUS', self::STATUS_ACTIVE)
                        ->param(':INCHARGE', $user_incharge)
                        ->as_object()->execute()->current();            
            $o_select_product = DB::select(
                                    'rpd.serie_electronic'
                                    , 'rpd.idProductDetail'
                                    , 'rpd.idProduct'
                                    , 'rp.haveDetail'
                                    , array(DB::expr("CONCAT(rp.aliasProduct,' / ',rp.productName, ' - ', rpd.serie_electronic)"), 'productName')
                                    , 'rp.productPriceSell'
                                    // , 'rml.salePrice'
                            )
                            ->from(array('rms_products', 'rp'))
                            ->join(array('rms_product_detail', 'rpd'))->on('rp.idProduct', '=', 'rpd.idProduct')
                            ->join(array('rms_movedetail','mvdi'))->on('mvdi.idProductDetail','=','rpd.idProductDetail')
                            ->join(array('rms_move', 'mv'))->on('mvdi.idMove', '=', 'mv.idMove')
                            ->where('rpd.serie_electronic', '=', $s_search)
                            ->and_where('rpd.amount', '<>', '0')
                            ->and_where('rp.status', '=', self::STATUS_ACTIVE)
                            ->and_where('mv.idStore', '=', $store->id)
                            ->group_by('rpd.idProductDetail')
                            ->having(DB::expr('MOD(COUNT(rpd.idProductDetail),2)'),'<>',0)
                            ->as_object()->execute()->current();
//            
//            print_r(Database::instance()->last_query); die();
            
            $a_response['data'] = $o_select_product;
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        die(); 
    }
}

?>
