<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of customers
 *
 * @author GG
 */
class Controller_Private_Ordersales extends Controller_Private_Admin implements Rms_Constants {

    public function action_index() {
        $this->checkCashBox();
        $this->mergeStyles(ConfigFiles::fnGetFiles('ordersales', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('ordersales', self::FILE_TYPE_JS));
        $view_customers = new View('sales/ordersales');
        
        $view_customers->CLIENT_TYPE_PERSON = self::CLIENT_TYPE_PERSON;
        $view_customers->CLIENT_TYPE_COMPANY = self::CLIENT_TYPE_COMPANY;
        $view_customers->a_year_report = $this->a_year_report();
        $view_customers->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $view_customers->jquery_date_format = $this->getDATE_FORMAT_FOR(self::DF_JQUERY_DATE);
        $view_customers->date_format_php = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
        
        $this->template->content = $view_customers;
        
        //Actualizar Reservas Vencidas
        $a_sales_order_expired = DB::select(
                array('so.idSalesOrder', 'sales_order_id')
                )
                ->from(array('rms_salesorder', 'so'))
                ->where(DB::expr("CAST(`so`.`expirationDate` AS DATE)"), '<', DB::expr('CURDATE()'))
                ->and_where('so.status', '=', self::SALES_ORDER_STATUS_RESERVED)
                ->execute()->as_array();        
        if(is_array($a_sales_order_expired))        
            foreach($a_sales_order_expired as $a_item){
                $o_sales_order = new Model_Salesorder($a_item['sales_order_id']);
                $o_sales_order->status = self::SALES_ORDER_STATUS_EXPIRED;
                $this->cancelSalesOrder($a_item['sales_order_id']);
                /* Creo Historial */
                $this->createHistorySalesOrder(
                        array(
                            'id' => $a_item['sales_order_id']
                            , 'status' => self::SALES_ORDER_STATUS_EXPIRED
                        )
                );
                $o_sales_order->save();       
       }
    }
    
    public function action_getListSalesOrder() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $idDataSearch = $this->request->post('dataSearch');
            $typeSearch = $this->request->post('typeSearch');
            $serie = $this->request->post('serie');
            $cr1 = $this->request->post('cr1');
            $cr2 = $this->request->post('cr2');
            
            //Busquedad por fechas
            $sd = $this->request->post('sd');
            $ed = $this->request->post('ed');
            $m = $this->request->post('m');
            $a = $this->request->post('a');
            $d = $this->request->post('d');
            
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE);
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW);
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT);
            $date = date($date_format);
            $a_salesorder = $this->fnGetClassVariable(self::VAR_NAME_ORDERSALES_STATUS);
            if (!$sidx)
                $sidx = 1;

            $where_search = "";

            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idordersales' => "so.idSalesOrder",
                'document' => "LPAD(so.correlative,8,'0')",
                'customers' => "IF(cu.idPerson IS NULL,CONCAT(cuc.name,' - ', cuc.code),CONCAT(pe.fullName,' - ',pe.document))",
                'status' => "
                    CASE 
                        WHEN so.status = '" . self::SALES_ORDER_STATUS_CANCELED . "' THEN '" . __($a_salesorder[self::SALES_ORDER_STATUS_CANCELED]['display']) . "' 
                        WHEN so.status = '" . self::SALES_ORDER_STATUS_EXPIRED . "' THEN '" . __($a_salesorder[self::SALES_ORDER_STATUS_EXPIRED]['display']) . "' 
                        WHEN so.status = '" . self::SALES_ORDER_STATUS_RESERVED . "' THEN '" . __($a_salesorder[self::SALES_ORDER_STATUS_RESERVED]['display']) . "' 
                        WHEN so.status = '" . self::SALES_ORDER_STATUS_SOLD . "' THEN '" . __($a_salesorder[self::SALES_ORDER_STATUS_SOLD]['display']) . "'                         
                    ELSE '" . __("No Especificado") . "' END",
                'orderSalesDate'=>"DATE_FORMAT(so.orderSalesDate,'%d/%m/%Y %h:%i %p')",
                'expirationDate'=>"DATE_FORMAT(so.expirationDate,'%d/%m/%Y')",
                'totalAmount'=>"so.totalAmount",
                'amountPaid'=>"so.amountPaid",
                'amountToPaid'=>"so.amountToPaid"                
            );

            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                  if ($this->request->post('filters') != '') {
            //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
            //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {
                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = 'rms_salesorder so ' .
                    'INNER JOIN rms_customers cu ON so.idCustomers = cu.idCustomers ' .
                    'LEFT JOIN rms_customerscompany cuc ON cu.idCustomersCompany = cuc.idCustomersCompany ' .
                    'LEFT JOIN bts_person pe ON cu.idPerson = pe.idPerson';

            $sold = self::SALES_ORDER_STATUS_SOLD;
            $canceled = self::SALES_ORDER_STATUS_CANCELED;
            $where_conditions = "so.status <> '".$sold."'";
//                AND so.status <> '".$canceled." ' " ;
            if ($d == "" && $ed == "" && $sd == "") {
//                $where_conditions .= " AND DATE_FORMAT(so.orderSalesDate,'$date_format') LIKE '" . $date . "'";
               //   $where_conditions .= "1=1";
                
            }
            
            // Busquedad por: 
            if ($d != "") {
                $where_conditions .= " AND DATE_FORMAT(so.orderSalesDate,'$date_format') LIKE '" . $d . "' ";
            }
            if ($m != 0) {
                $where_conditions .= " AND MONTH(so.orderSalesDater) = " . $m . " AND YEAR(po.datePurchaseOrder) = " . $a . "";
            }
            if ($ed != "") {
                if ($sd != "") {
                    $where_conditions .= " AND so.orderSalesDate BETWEEN STR_TO_DATE('" . $sd . "', '$date_format') AND STR_TO_DATE('" . $ed . " $time_limit', '$date_format $time_raw')";
                } else {
                    $where_conditions .= " AND DATE_FORMAT(so.orderSalesDate,'$date_format') LIKE '" . $ed . "' ";
                }
            } else {
                if ($sd != "") {
                    $where_conditions .= " AND DATE_FORMAT(so.orderSalesDate,'$date_format') LIKE '" . $sd . "' ";
                }
            }
            
            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);
            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);
            $result_array = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $result_array, $array_cols,true),self::REPORT_RESPONSE_TYPE_JSON_JQGRID);

        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function action_getDetalleSalesOrder() {
             $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $idSalOrder = $this->request->post('idSal');

            if (!$sidx)
                $sidx = 1;

            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idSalesOrderDetail' => 'so.idSalesOrderDetail',
                'idSalesOrder' => 'so.idSalesOrder',
                'product' => 'CONCAT(pro.productName," - " ,pro.aliasProduct)',
                'storeName' => 'st.storeName',
                'quantity' => 'so.quantity',
                'unitPrice' => 'so.unitPrice',
                'discount' => 'so.discount', // Falta agregar el descuento
//                'so.discount',
                'totalPrice' => 'so.totalPrice'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }
            $cols = jqGridHelper::arrayColsToSQL($array_cols);
            $tables_join = 'rms_salesorderdetail so ' .
                    'INNER JOIN rms_lot l ON so.idLot = l.idLot ' .
                    'INNER JOIN rms_products pro ON pro.idProduct = l.idProduct '.                    
                    'INNER JOIN rms_store st ON l.idStore = st.idStore';
            
            
            $where_conditions = "so.status = 1 AND so.IdSalesOrder = (" . $idSalOrder. ")";

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $aResults = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $aResults, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
           // echo Database::instance()->last_query;
           // 
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    /* Actualizar el Estado de una Reserva */   
    public function action_updateStatusSalesOrder() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
           $post_salesOrder = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
                    ->rule('status', 'not_empty');
            
            if (!$post_salesOrder->check()) 
                throw new Exception(__('Data Invalida'), self::CODE_SUCCESS);
            $data_salesOrder = $post_salesOrder->data();
            Database::instance()->begin();
            // Cambio el estado EXPIRED o CANCELED o RESERVED 
            $salesOrder = new Model_Salesorder($data_salesOrder['id']);
            $salesOrder->status = $data_salesOrder['status'];     
            if ($data_salesOrder['status']== self::SALES_ORDER_STATUS_RESERVED){
                $data_salesOrder['status']= self::SALES_ORDER_STATUS_RECOVERED;
                $this->returnSalesOrder($data_salesOrder['id']);
                $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
                $dateSale = DateTime::createFromFormat($format, $data_salesOrder['txtFechaVencimiento']);
                $salesOrder->expirationDate = DateHelper::fnPHPDate2MySQLDatetime($dateSale);
            }else{
                $this->cancelSalesOrder($data_salesOrder['id']);
            }
            /* Creo Historial */
            $this->createHistorySalesOrder($data_salesOrder);
            $salesOrder->save();
            Database::instance()->commit();
           
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }
    /* Cancelar Reserva */
    public function cancelSalesOrder($idSalesOrder){
        // Obtengo detalle de la reserva 
        $salesOrderDetail  = DB::select('*')
                ->from('rms_salesorderdetail')
                ->where('idSalesOrder','=',$idSalesOrder)                  
                ->execute()->as_array();    
        //$id_User = $this->getSessionParameter(self::USER_ID);
        $idCashBox = $this->getSessionParameter(self::CASHBOX_ID);
//        $idCashBox = DB::select(
//                                array('ca.idCashBox','idCashBox')
//                                )
//                         ->from(array('rms_cashbox','ca'))
//                         ->where('ca.status','=',self::CASHBOX_STATUS_OPENED)
//                         ->and_where(DB::expr("DATE_FORMAT(ca.openDate,'%Y-%m-%d')"),'=',DateHelper::getfechaActual())
//                         ->and_where('ca.idUser','=',$id_User)
//                         ->execute()->current();
        // Crear movimiento de Entrada a Almacen 
        $objMov = new Model_Move();
        $objMov->typeMoveStore = 'TYPE_MOVE_RESERVA_ANULACION';
        $objMov->moveReference = 'Reserva Anulada';
        $objMov->idSalesOrder = $idSalesOrder;
        $objMov->status = self::STATUS_ACTIVE;
        $objMov->save();         
        $salesOrder = new Model_Salesorder($idSalesOrder);
        $salesOrder->idCashBoxCancel = $idCashBox;
        $salesOrder->save();
        
        foreach ($salesOrderDetail as $od => $a_item)
        {                            
            // Regreso productos a lotes 
            $objLot = new Model_Lot($a_item['idLot']);
            $objLot->amount = $objLot->amount + $a_item['quantity'];
            $objLot->save();
            // Regreso producto a stock
            $stock = DB::select('*')
                    ->from('rms_stock')
                    ->where('idProduct','=',$objLot->idProduct)   
                    ->and_where('idStore', '=', $a_item['idStore'])
                    ->as_object()->execute()->current();
            $objStock = new Model_Stock($stock->idStock);  
            $objStock->amount = $objStock->amount + $a_item['quantity'];
            $objStock->save();
            // Creo los movimientos detalles
            $objMovDetail = new Model_Movedetail();
            $objMovDetail->idMove = $objMov->idMove;
            $objMovDetail->idLot = $objLot->idLot; 
            $objMovDetail->amount = $a_item['quantity'];
            $objMovDetail->status = self::STATUS_ACTIVE;
            $objMovDetail->unitPrice = $objLot->unitPrice; 
            $objMovDetail->salePrice = $objLot->salePrice;
            $objMov->idStore =$a_item['idStore'];
            $objMovDetail->save();               
        }
        $objMov->save();
    }   
    /* Recuperar Reserva Vencida o Cancelada */
    public function returnSalesOrder($idSalesOrder){
        // Obtengo detalle de la reserva 
        $salesOrderDetail  = DB::select('*')
                ->from('rms_salesorderdetail')
                ->where('idSalesOrder','=',$idSalesOrder)                  
                ->execute()->as_array();              
        // Crear movimiento de Salida de Almacen 
        $objMov = new Model_Move();
        $objMov->typeMoveStore = self::TYPE_MOVE_RESERVA;
        $objMov->moveReference = 'Reserva Recuperada';
        $objMov->idSalesOrder = $idSalesOrder;
        $objMov->status = self::STATUS_ACTIVE;
        $objMov->save();         
        foreach ($salesOrderDetail as $od => $a_item)
        {     
            $objLot = new Model_Lot($a_item['idLot']);
            $objLot->amount = $objLot->amount - $a_item['quantity'];
            $objLot->save();
            // Regreso producto a stock
            $stock = DB::select('*')
                    ->from('rms_stock')
                    ->where('idProduct','=',$objLot->idProduct)   
                    ->and_where('idStore', '=', $a_item['idStore'])
                    ->as_object()->execute()->current();
            $objStock = new Model_Stock($stock->idStock); 
            if($objStock->amount  <= 0){
                throw new Exception(__('No se puede recuperar - Insuficiente stock'), self::CODE_SUCCESS);
               // die();
            }else{                
                $objStock->amount = $objStock->amount - $a_item['quantity'];
                $objStock->save();
                // Creo los movimientos detalles
                $objMovDetail = new Model_Movedetail();
                $objMovDetail->idMove = $objMov->idMove;
                $objMovDetail->idLot = $objLot->idLot; 
                $objMovDetail->amount = $a_item['quantity'];
                $objMovDetail->status = self::STATUS_ACTIVE;
                $objMovDetail->unitPrice = $objLot->unitPrice; 
                $objMovDetail->salePrice = $objLot->salePrice;
                $objMov->idStore =$a_item['idStore'];
                $objMovDetail->save();  
            }
        }
        $objMov->save();
    }
     
    public function action_createUpdateSalesOrderDetail() {
        $aResponse = $this->json_array_return;
        try {
            $idSal = $this->request->post('idl');

            $salesOrderDetail = new Model_Salesorderdetail($idSal);
            $salesOrderDetail->status = self::SALES_ORDERDETAIL_STATUS_DEACTIVE;

            // Actualizar la reserva 
            // 
            $objSalesOrder = new Model_Salesorder($salesOrderDetail->idSalesOrder);
            $objSalesOrder->totalAmount = $objSalesOrder->totalAmount - $salesOrderDetail->totalPrice;
            $objSalesOrder->amountToPaid = $objSalesOrder->amountToPaid - $salesOrderDetail->totalPrice;
            // Regresar producto al stock del lote
            $objLot = new Model_Lot($salesOrderDetail->idLot);
            $objLot->amount = $objLot->amount - $salesOrderDetail->quantity;
            // en la tabla stock 
            $stock = ORM::factory('stock')->where('idProduct','=',$objLot->idProduct)
                            ->and_where('idStore','=',$salesOrderDetail->idStore)->find();
            
            $objStock = new Model_Stock($stock);
            $objStock->amount = $objStock->amount - $salesOrderDetail->quantity;
            
            $objLot->save();
            $objStock->save();
            $objSalesOrder->save();
            $salesOrderDetail->save();
            Database::instance()->commit();
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        return $aResponse;
    }

    public function action_getSalesOrderForDetail() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('iddp', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $id = $this->request->post('iddp');
                $data = DB::select(array(DB::expr("CONCAT (pro.aliasProduct, ' ', pro.productName)"),'product'),'st.storeName',
                        'so.quantity', 'so.unitPrice', 'so.totalPrice', 'so.discount','pro.idProduct', 'so.idSalesOrderDetail','st.idStore', 'lot.idLot')
                        ->from(array('rms_salesorderdetail', 'so'))
                        ->join(array('rms_lot','lot'))
                        ->on('so.idLot', '=', 'lot.idLot')
                        ->join(array('rms_store','st'))
                        ->on('so.idStore','=','st.idStore')
                        ->join(array('rms_products','pro'))
                        ->on('lot.idProduct', '=', 'pro.idProduct')
                        ->where('so.idSalesOrderDetail', '=',$id)
                        ->execute()->current();

//                $param = array(
//                    ':store' => $data['idStore'],
//                    ':product' => $data['idProduct'],
//                    ':salesOrderDetail' => $data['idSalesOrderDetail'],
//                    ':statusOrderDetail' => self::SALES_ORDERDETAIL_STATUS_ACTIVE,
//                    ':initiate' => self::SALES_ORDER_STATUS_INITIATE
//                );
//
//                $reser = DB::select(array(DB::expr("SUM(rms_salesorderdetail.quantity)"), 'quantity'))
//                                ->from('rms_salesorderdetail')
//                                ->join('rms_salesorder')->on('rms_salesorderdetail.idSalesOrder', '=', 'rms_salesorder.idSalesOrder')
//                                ->where('rms_salesorderdetail.idStore', '=', ':store')
//                                ->and_where('rms_salesorderdetail.idProduct', '=', ':product')
//                                ->and_where('rms_salesorderdetail.idSalesOrderDetail', '<>', ':salesOrderDetail')
//                                ->and_where('rms_salesorderdetail.status', '=', ':statusOrderDetail')
//                                ->and_where('rms_salesorder.status', '=', ':initiate')
//                                ->group_by('idStore')
//                                ->group_by('idPRoduct')
//                                ->parameters($param)
//                                ->execute()->current();
                $a_response['data'] = $data;
//                $a_response['reserv'] = $reser;
            }
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_createUpdateDetail() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {

            $post = Validation::factory($this->request->post())
                    ->rule('quantitydet', 'not_empty')
                    ->rule('custom_pricedet', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                    $data = $post->data();
                    $idp = $data['idProductDetail']; 
                    $ids = $data['idStoreDetail'];
//                $array = array();
//
//                foreach ($stock as $key => $itemStock) {
//                    if ($itemStock['idProduct'] == $data['idProduct'] AND $itemStock['idStore'] == $data['idStore']) {
//                        if ($data['quantitydet'] > $itemStock['stock']) {
//                            array_push($array, array('stock' => $itemStock['amount'], 'reserv' => $itemStock['reserv']));
//                        }
//                    }
//                }
                    $detail = new Model_Salesorderdetail($data['idDetail']);
                    $detail->quantity = $data['quantitydet'];
                    $detail->unitPrice = $data['custom_pricedet'];
                    
                    $discount = $detail->totalPrice - $data['priceTotaldet'];
                    $detail->discount = $data['discountUnitdet'];
                    $detail->totalPrice = $data['priceTotaldet'];
                    
                    $stock = ORM::factory('stock')->where('idProduct','=',$idp)
                            ->and_where('idStore','=',$ids)->find();
                    // Modificamos el Stock
                    $objStock = new Model_Stock($stock);
                    $objStock->amount = $objStock->amount - $data['quantitydet'];
                    $objStock->save();
                    // Modificamos el Stock en el LOTE
                    $objLot = new Model_Lot($data['idLotDetail']);
                    $objLot->amount = $objLot->amount - $data['quantitydet'];
                    $objLot->save();
                    // En la orden detalle 
                    $objDetail = new Model_Salesorder($detail->idSalesOrder);
                    $objDetail->totalAmount = $objDetail->totalAmount - $discount; 
                    $objDetail->amountToPaid = $objDetail->totalAmount - $objDetail->amountPaid ;
                    

                    
                    $objDetail->save();
                    $detail->save();
                    
            }
        } catch (Exception $exc) {
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function action_createUpdateOrderSales() {
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {

            $post = Validation::factory($this->request->post())
                    ->rule('orderSales', 'not_empty')
//                    ->rule('detail', 'not_empty')
                    //Aveces no agrega detalle solo edita al reserva
                    ->rule('amountPaid', 'not_empty')
                    ->rule('idCustomers', 'not_empty');

            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {

                    Database::instance()->begin();  
                    
                    $idOrder = $this->request->post('orderSales');
                    $a_detail = $this->request->post('detail');
                    $idCustomers = $this->request->post('idCustomers');
                    $amountPaid = $this->request->post('amountPaid');
                    $objOrderSales = new Model_Salesorder($idOrder);
                    $objOrderSales->idCustomers = $idCustomers;
                    $saldo = $objOrderSales->amountPaid - $amountPaid;
                    $objOrderSales->amountPaid = $amountPaid; 
                    $array = array();
                    
                if($a_detail != '' ){
                    if (count($array) < 0) {
                        foreach ($a_detail as $key => $a_recipient) {
//                            if ($a_recipient['exist'] == 1) {
//                                $o_salesorderdetail = new Model_Salesorderdetail($a_recipient['idSalesOrderDetail']);
//                                if ($a_recipient['remove'] == 1) {
//                                    $o_salesorderdetail->status = 0;
//                                }
//                            } else {
                            $stock = ORM::factory('stock')
                                        ->where('idProduct', '=', $a_recipient['product_id'])
                                        ->and_where('idStore', '=', $a_recipient['store_id'])->find();
                            if ($stock->loaded()) {
                                $stock->amount = $stock->amount + $a_recipient['quantityOut'];
                                $stock->save();
                            }
                            
                            $o_salesorderdetail = new Model_Salesorderdetail();
                            $o_salesorderdetail->idSalesOrder = $idOrder;
//                            }

                            $o_salesorderdetail->idProduct = $a_recipient['product_id'];
                            $o_salesorderdetail->idStore = $a_recipient['store_id'];
                            $o_salesorderdetail->quantity = $a_recipient['quantity'];
                            $o_salesorderdetail->discount = $a_recipient['discount'];
                            $o_salesorderdetail->unitPrice = $a_recipient['priceUnit'];
                            $o_salesorderdetail->totalPrice = $a_recipient['priceTotal'];
                            // $o_salesorderdetail->status = self::SALES_ORDERDETAIL_STATUS_ACTIVE;
                            $o_salesorderdetail->save();

                            $objOrderSales->totalAmount = ($objOrderSales->totalAmount + $o_salesorderdetail->totalPrice) - $saldo; 

                            // Movimiento 
                            $obj_movedetail = new Model_Movedetail();
                            $obj_movedetail->idMove = $id_Move;
                            $obj_movedetail->status = self::STATUS_ACTIVE;
                            $obj_movedetail->amount = 1;
                            $obj_movedetail->unitPrice = $a_recipient['priceUnit'];

                            // echo Database::instance()->last_query;
                            //die();

                            //Modificar el stock 
                        }
                        Database::instance()->commit();
                    }else 
                    {
                        $aResponse['data'] = $array;
                    }
                }else {
                    $objOrderSales->amountToPaid = $amountPaid - $saldo; 
                }
                $objOrderSales->save();
            }
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }

    public function getStock($detail = FALSE, $sales = FALSE) {
        $stock = DB::select(
                        'rp.aliasProduct'
                        , 'rst.idProduct'
                        , 'rst.idStore'
                        , 'rstr.storeName'
                        , 'rst.amount'
                        , array(DB::expr("SUM(IF(rst.idProduct = rso.idProduct AND rst.idStore = rso.idStore,rso.quantity,0))"), 'reserv')
                        , array(DB::expr("rst.amount - SUM(IF(rst.idProduct = rso.idProduct AND rst.idStore = rso.idStore,rso.quantity,0))"), 'stock')
                )
                ->from(array('rms_stock', 'rst'))
                ->join(array('rms_products', 'rp'))->on('rp.idProduct', '=', 'rst.idProduct')
                ->join(array('rms_store', 'rstr'))->on('rst.idStore', '=', 'rstr.idStore')
                ->join(array('rms_salesorderdetail', 'rso'), 'LEFT')->on('rp.idProduct', '=', 'rso.idProduct')
                ->join(array('rms_salesorder', 'rsor'), 'LEFT')->on('rso.idSalesOrder', '=', 'rsor.idSalesOrder')
                ;//->where('rsor.status', '=', self::SALES_ORDER_STATUS_RESERVED);
        

        if ($detail) {
            $stock->and_where('rso.idSalesOrderDetail', '<>', $detail);
        } elseif ($sales) {
            $stock->and_where('rso.idSalesOrder', '<>', $sales);
        }
        $stock = $stock->and_where('rso.status', '=', '1')
                        ->group_by('rst.idProduct')
                        ->group_by('rst.idStore')
                        ->order_by('rst.idStore')
                        ->order_by('rst.idProduct')
                        ->execute()->as_array();
        return $stock;
    }
    
    public function action_getSalesOrder(){
        $a_response = $this->json_array_return;
        try{
             $o_post_id = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty')
            ;
            if ($o_post_id->check()) {
                $data_post_id = $o_post_id->data();
                
                $objSalesOrder = DB::select('so.idSalesOrder','so.serie','so.correlative','so.documentType', 'cus.idCustomers'
                        ,'pe.fullName', 'so.amountPaid' )
                        ->from(array('rms_salesorder','so'))
                        ->join(array('rms_customers','cus'))
                        ->on('so.idCustomers','=','cus.idCustomers')
                        ->join(array('bts_person','pe'))
                        ->on('cus.idPerson','=','pe.idPerson')
                        ->where('so.idSalesOrder','=',$data_post_id['id'])
                        ->as_object()->execute()->current();
                $a_response['so'] = $objSalesOrder;
            }
        }
        catch(Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($exc);
        }
        $this->fnResponseFormat($a_response);
        
    }
    
    /* Crear History Sales Order */
    public function createHistorySalesOrder($obj_SalesOrder){    
        $id_User = $this->getSessionParameter(self::USER_ID);

        $historySalesOrder = new Model_HistorySalesOrder();
        $obj_SalesOrder['idSalesOrder']=$obj_SalesOrder['id'];
        $obj_SalesOrder['status']=$obj_SalesOrder['status'];
        $obj_SalesOrder['dateHistorySalesOrder']=  DateHelper::getFechaFormateadaActual();
        $obj_SalesOrder['idUser']=$id_User;
        $historySalesOrder->saveHistorySalesOrder($obj_SalesOrder);
    }
    /* Lista el Historial de la Reserva */
    public function action_listHistorySalesOrder(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $id = $this->request->post('id');
                $data = DB::select(array('hs.status','status'),
                        array(db::expr('DATE_FORMAT(hs.dateHistorySalesOrder,"%d/%m/%Y")'),'date'),
                        'pe.fullName')
                        ->from(array('rms_historySalesOrder','hs'))
                        ->join(array('bts_person', 'pe'))
                        ->on('hs.idUser','=','pe.idPerson')
                        ->where('hs.idSalesOrder','=',$id)
                        ->and_where('hs.status', '!=', 'SHARES')
                        ->group_by('hs.idHistorySalesOrder')
                        ->order_by('hs.dateHistorySalesOrder')
                        ->execute()->as_array();
                $a_response['data'] = $data;
                
            }
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }
    /* Lista el Historial de la Cuotas */
    public function action_listHistoryShares(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('id', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $id = $this->request->post('id');
                $data = DB::select(array('sh.idShare','id'),array(db::expr('DATE_FORMAT(sh.dateShare,"%d/%m/%Y")'),'date'),
                        array('p.fullName','name'),
                        array('sh.amountPaid','cuota'))
                        ->from(array('rms_sharessales','sh'))
                        ->join(array('bts_person', 'p'))
                        ->on('sh.idUser','=','p.idPerson')
                        ->where('sh.idSalesOrder','=',$id)
                        ->and_where('sh.status', 'like', 'sold')
                        ->order_by('sh.dateShare')
                        ->execute()->as_array();                 
                $a_response['data'] = $data;
                
            }
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($a_response);
    }
    /* Crear cuota */
    public function action_createShares(){
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('obj_shares', 'not_empty');
            if (!$post->check())
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);       
            $obj_shares =$this->request->post('obj_shares');
            Database::instance()->begin();
            // Creo la cuota 
            $obj_shares['status'] = self::SALES_ORDER_STATUS_SOLD;
            $obj_shares['idUser'] =  $this->getSessionParameter(self::USER_ID);

            $o_model_shares = new Model_Shares();
            $o_model_shares->saveShares($obj_shares);
            // Modifico la Reserva 
            $a_salesOrder = new Model_Salesorder($obj_shares['idSalesOrder']);
            $a_salesOrder->amountPaid = $a_salesOrder->amountPaid + $obj_shares['amountPaid'];
            $a_salesOrder->amountToPaid =  $obj_shares['amountToPaid'];
            $a_salesOrder->save();
            // Creo Historial 
            $obj_SalesOrder = array();
            $obj_SalesOrder['id'] = $obj_shares['idSalesOrder'];
            $obj_SalesOrder['status'] = self::SALES_ORDER_STATUS_SHARES;       
            $this->createHistorySalesOrder($obj_SalesOrder);
            Database::instance()->commit();      
        } catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
        $this->fnResponseFormat($aResponse, 'json');
    }
    public function action_deleteShares(){
        $this->auto_render = false;
        $aResponse = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('arrayShares', 'not_empty');
            if (!$post->check())
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);       
            $arrayShares =$this->request->post('arrayShares');                                                   
            Database::instance()->begin();
            if(count($arrayShares)>0){
                for($e=0;$e<count($arrayShares);$e++){
                    //cancelamos la cuota registramos user.
                    $shareSales = new Model_Shares($arrayShares[$e]);
                    $shareSales->status = self::SALES_ORDER_STATUS_CANCELED;
                    $shareSales->idUserCancel = $this->getSessionParameter(self::USER_ID);
                    $shareSales->dateShareCancel = DateHelper::getFechaFormateadaActual();
                    $amountShare = $shareSales->amountPaid;
                    $idSalesShare = $shareSales->idSalesOrder;
                    
                    //Regresamos la cantidad al salesOrder
                    $salesOrder_share = new Model_Salesorder($idSalesShare);
                    $salesOrder_share->amountPaid = $salesOrder_share->amountPaid - $amountShare;
                    $salesOrder_share->amountToPaid = $salesOrder_share->amountToPaid + $amountShare;
                    
                    $shareSales->save();
                    $salesOrder_share->save();
                }
            }            
            Database::instance()->commit(); 
        }  catch (Exception $exc) {
            Database::instance()->rollback();
            $aResponse['code'] = self::CODE_ERROR;
            $aResponse['msg'] = $exc->getMessage();
        }
         $this->fnResponseFormat($aResponse, 'json');
    }
    

}

?>
