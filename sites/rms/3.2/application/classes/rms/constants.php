<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of constants
 *
 * @author Emmanuel
 */
interface Rms_Constants {

    const MODULE_SALES = 'SALE';
    const MODULE_PURCHASES = 'PURCHASES';
    const MODULE_INVENTORY = 'INVENTORY';   
    
    const SO_JQUERY_DATE = 'JQUERY_DATE';
// CONSTANTES PARA LA ASIGNACIÓN DE SERIES Y DOCUMENTO
    const SO_CORRELATIVO_VOUCHER_AUTOMATICO = 'CORRELATIVO_VOUCHER_AUTOMATICO';
    const CORRELATIVO_IMPRESION = 'CORRELATIVO_IMPRESION';
    const CORRELATIVO_MAXIMO = 'CORRELATIVO_MAXIMO';
    const SET_ALPHANUMERIC_SERIES = 'SET_ALPHANUMERIC_SERIES';
    const SO_NRODOCUMENT_MANUAL = 'DOCUMENT_MANUAL';
    const SO_SEND_EMAIL_STOCKMINIMUM='SEND_EMAIL_STOCKMINIMUM';
    const SO_MESSAGE_TO_COMPANY_STOCKMINIMUM='MESSAGE_TO_COMPANY_STOCKMINIMUM';

//documentos del sistema
    const DT_DEFAULT_CORRELATIVE = 'DEFAULT_CORRELATIVE';
    const DT_VALE_BOLETO = 'VALE';
    const DT_DEFAULT_VOUCHER_EGRESO = 'DEFAULT_VOUCHER_EGRESO';
    const DT_DEFAULT_VOUCHER_INGRESO = 'DEFAULT_VOUCHER_INGRESO';

//CONSTANTES PARA TIPO DE MONEDA
    const CURRENCY_SOLES = 'NUEVOS SOLES';
    const CURRENCY_DOLARES = 'DOLARES AMERICANOS';

//CONSTANTES PARA UNIDAD DE MEDIDA
    const UNIT_MEASURES_KILOGRAMOS = 'KILOGRAMOS';
    const UNIT_MEASURES_LIBRAS = 'LIBRAS';
    const UNIT_MEASURES_TONELADAS_LARGAS = 'TONELADAS LARGAS';
    const UNIT_MEASURES_TONELADAS_METRICAS = 'TONELADAS METRICAS';
    const UNIT_MEASURES_TONELADAS_CORTAS = 'TONELADAS CORTAS';
    const UNIT_MEASURES_GRAMOS = 'GRAMOS';
    const UNIT_MEASURES_UNIDADES = 'UNIDADES';
    const UNIT_MEASURES_LITROS = 'LITROS';
    const UNIT_MEASURES_GALONES = 'GALONES';
    const UNIT_MEASURES_BARRILES = 'BARRILES';
    const UNIT_MEASURES_LATAS = 'LATAS';
    const UNIT_MEASURES_CAJAS = 'CAJAS';
    const UNIT_MEASURES_MILLARES = 'MILLARES';
    const UNIT_MEASURES_METROS_CUBICOS = 'METROS CUBICOS';
    const UNIT_MEASURES_METROS = 'METROS';

//constante para estado u opcion(ingreso,salida) del tipo de movieminto
    const TYPE_MOVE_INGRESO = "INGRESO";
    const TYPE_MOVE_SALIDA = "SALIDA";
    const TYPE_MOVE_TRAS_SALIDA = "TRAS_SALIDA";
    const TYPE_MOVE_TRAS_INGRESO = "TRAS_INGRESO";
    const TYPE_MOVE_VENTA_DIRECTA = "VENTA_DIRECTA";
    const TYPE_MOVE_VENTA_ANULACION = "VENTA_ANULACION";
    const TYPE_MOVE_VENTA_RESERVA = "VENTA_RESERVA";
    const TYPE_MOVE_RESERVA = "RESERVA";
    const TYPE_MOVE_RESERVA_ANULACION = "RESERVA_ANULACION";


//contaste tipo de documento
    const PERSON_DOCUMENT_TYPE_DNI = 'DNI';
    const PERSON_DOCUMENT_TYPE_PASAPORTE = 'PASAPORTE';
    const PERSON_DOCUMENT_TYPE_CARNET = 'CARNET';

//constante tipo de cliente
    const CLIENT_TYPE_PERSON = 'PERSON';
    const CLIENT_TYPE_COMPANY = 'COMPANY';


// CONSTANTES DE TIPO DE DOCUMENTO
    const DP_SALES = 'SALES';
    const TIPO_DOC_NO_CONTABLE = 0;
    const TIPO_DOC_CONTABLE = 1;
    const DT_BOLETA = 'BOLETA';
    const DT_FACTURA = 'FACTURA';
    const DT_RECIBO = 'RECIBO';
    const DT_TICKET = 'TICKET';
    const DT_ORDENCOMPRA = 'ORDEN_COMPRA';
    const DT_NOTA_INGRESO = 'NOTA_INGRESO';
    const DT_NOTA_SALIDA = 'NOTA_SALIDA';
    const DT_GUIA_REMISION = 'GUIA_REMISION'; 
    const DT_NOTA_PEDIDO = 'NOTA_PEDIDO';
    const DT_RECIBO_INGRESO = 'RECIBO_INGRESO';
    const DT_RECIBO_EGRESO = 'RECIBO_EGRESO';
    
// constantes para impuestos
    const SO_TAX_ENABLED = 'TAX_ENABLED';
    const SO_TAX_NAME = 'TAX_NAME';
    const SO_TAX_VALUE = 'TAX_VALUE';

//contanste value name para generar radios o ckeck
    const ARRAY_KEY_NAME = 'NAME';
    const ARRAY_KEY_VALUE = 'VALUE';

//CONSTANTES DEL ESTADO DE ORDENES DE COMPRA
    const PURCHASE_ORDER_STATUS_APROBADO = 'APROBADO';
    const PURCHASE_ORDER_STATUS_CANCELADO = 'CANCELADO';
    const PURCHASE_ORDER_STATUS_EMITIDO = 'EMITIDO';
    const PURCHASE_ORDER_STATUS_FACTURADO = 'FACTURADO';
    const PURCHASE_ORDER_STATUS_RECEPCIONADO = 'RECEPCIONADO';
    const PURCHASE_ORDER_STATUS_DESAPROBADO = 'DESAPROBADO';
    const PURCHASE_ORDER_STATUS_ANULADO = 'ANULADO';
    const PURCHASE_ORDER_STATUS_ELIMINADO = 'ELIMINADO';
    const PURCHASE_ORDER_STATUS_ACTIVE = 1;
    const PURCHASE_ORDER_STATUS_DEACTIVE = 0;

//constantes sales order
    const SALES_ORDER_STATUS_SOLD = 'SOLD';
    const SALES_ORDER_STATUS_CANCELED = 'CANCELED';
    const SALES_ORDER_STATUS_RESERVED = 'RESERVED';
    const SALES_ORDER_STATUS_EXPIRED = 'EXPIRED';
    const SALES_ORDER_STATUS_RECOVERED = 'RECOVERED';
    const SALES_ORDER_STATUS_SHARES = 'SHARES';
    const SALES_ORDER_PAYMENT_CASH = 'EFECTIVO';
    const SALES_ORDER_PAYMENT_CREDIT = 'CREDITO';
    const SALES_ORDER_STATUS_GIVE= 'GIVE';
    const SALES_ORDERDETAIL_STATUS_ACTIVE = 1;
    const SALES_ORDERDETAIL_STATUS_DEACTIVE = 0;
    const VAR_NAME_ORDERSALES_PAYMENT = 'a_ordersales_payment';
    const VAR_NAME_ORDERSALES_STATUS = 'a_ordersales_status';
    

//constantes de ordenes de compras : tipo de pago
    const PURCHASE_ORDER_CASH = 'CASH';
    const PURCHASE_ORDER_CREDIT = 'CREDITCARD';

//constantes de Sales
    const SALES_STATUS_SOLD = 'sold';
    const SALES_STATUS_CANCELED = 'canceled';
    const VAR_NAME_A_DOCUMENT_TYPE = 'a_documents_type';
    const VAR_NAME_PURCHASEORDER_STATUS = 'a_purchaseOrder_status';
    const VAR_NAME_PURCHASEORDER_PAYMENT = 'a_purchaseorder_payment';

    const VAR_NAME_SALES_PAYMENT = 'a_sales_payment';
    const SALES_PAYMENT_CASH = 'CASH';
    const SALES_PAYMENT_CREDITCARD = 'CREDITCARD';
    const SALES_PAYMENT_MIXTO = 'MIXTO';
    const SO_PAYMENT_FOR_SALES = 'PAYMENT_FOR_SALES';
 

// CONSTANTE QUE PERMITE INGRESAR AL VENDEDOR 
    const SO_ACTIVE_SELLER ='ACTIVE_SELLER';
    
 //constantes de Detalle de Ventas 
   const SALES_DETAIL_SOLD = 'sold';
   const SALES_DETAIL_CANCELED = 'canceled';
    
 //constantes de Orden de Compra
   const OC_BOLETA = 'BOLETA DE VENTA';
   const OC_FACTURA = 'FACTURA';
   const OC_RECIBOPORHONORARIOS = 'RECIBO POR HONORARIOS';
   const OC_NOTACREDITO = 'NOTA DE CREDITO';
   const OC_NOTADEBITO = 'NOTA DE DEBIDO';
   const OC_GUIAREMISION = 'GUIA DE REMISION';
   const OC_TICKETFACTURA = 'TICKET FACTURA';
   const OC_TICKETBOLETA= 'TICKET BOLETA';
   const VAR_NAME_PURCHASE_DOCUMENTS= 'a_purchase_documents';
   
 //constantes de Tarjeta de Crédito
    const SALES_CARD_VISA = 'VISA';
    const SALES_CARD_MASTERCARD = 'MASTERCARD';
    const SALES_CARD_AMERICAN = 'AMERICAN_XPRESS';
    const SALES_CARD_CMR = 'CMR_FALABELLA';
    const SALES_CARD_RYPLEY = 'RIPLEY';
    const SALES_CARD_DINNERS = 'DINNERS_CLUB';
    const VAR_NAME_CREDITCARD_PAYMENT = 'a_creditcard_payment';
    const VAR_NAME_METHODPAYMENT = 'a_sales_payment';

 //constantes de Inventario
    const INVENTORY_TAG_GENERATOR = 'TAG_GENERATOR';
    const INVENTORY_BARCODE_READER = 'BARCODE_READER';
    
 //constantes de Credit Note
    const CREDITNOTE_CREATE = 'created';
    const CREDITNOTE_USED = 'used'; 
    
 //constantes de Abrir y cerrar caja
    
    const CASHBOX_STATUS_CLOSED = 'closed';
    const CASHBOX_STATUS_OPENED = 'opened';
    const CASHBOX_STATUS_ACTIVE = 1;
    const CASHBOX_STATUS_DEACTIVE = 0;
    const CASHBOX_URL_REDIRECT = 'ur_cbx_redirect';
    const CASHBOX_ID = 'idCashBox';
    const SO_AUTH_KEY = 'AUTH_KEY';
    const CBS_RECEIVED = 'RECEIVED';
    const SO_SUPPORT_CASHBOX = 'Cashbox';
  
   //constantes de las cajas  

    const USER_OFFICE = 'user_office';    
    const PHP_DATE = 'PHP_DATE';
    const SQL_TIME = 'SQL_TIME';
    const SQL_TIME_RAW = 'SQL_TIME_RAW';
    const SQL_TIME_LIMIT = 'SQL_TIME_LIMIT';
    const SQL_DATE = 'SQL_DATE';
    
    //constantes de tipo de venta
    
    const SALES_DIRECT = 'DIRECTA';
    const SALES_ORDER = 'RESERVA';
    const SALES_CREDIT_NOTE = 'NOTA DE CREDITO';
    
    //constantes de fecha
    CONST MONTH_NAME = 'a_month_report';
}
