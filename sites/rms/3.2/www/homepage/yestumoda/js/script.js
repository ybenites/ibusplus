$(document).ready(function () {
    $("body").queryLoader2({
        barColor: "#ff8c79",
        backgroundColor: "#fff493",
        percentage: true,
        barHeight: 1,
        completeAnimation: "grow",
        minimumTime: 100
    });
});