/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    //$('.titleForm').hide();
    //$('.titleConfirmBox').hide();
    //$('.msgConfirmBox').hide();
    //$('.frm fieldset').addClass('ui-widget ui-widget-content ui-corner-all');
    //$('.frm fieldset legend').addClass('ui-widget ui-widget-content ui-corner-all ui-widget-header');
    //$('.checkbox_label').button();
    //$('.tsmsoptions').attr('style','padding-top:40px;');
    $('.menu_right').show('fast');
    $('.titleTables').addClass('ui-state-default ui-helper-clearfix ui-corner-all');
    $('.btn').button();
    $('li[class="ui-state-default"]').hover(function(){
        if($(this).hasClass('ui-state-active')){
            $(this).removeClass('ui-state-active');
        }else {
            $(this).addClass('ui-state-active');
        }
    });
    $('fieldset').addClass('ui-widget-content ui-corner-all');    
    $('legend').addClass('ui-widget-header ui-corner-all');    
    $('input[type=text],input[type=password],textarea,select').addClass('ui-widget ui-widget-content ui-corner-all');
    $('input[type=text],input[type=password],textarea,select').focus(function(){
        $(this).addClass('ui-state-highlight');
    }).blur(function(){
        $(this).removeClass('ui-state-highlight');
    });
});
