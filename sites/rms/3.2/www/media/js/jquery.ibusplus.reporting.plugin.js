/*
 *  jquery.ibusplus.reporting.plugin
 *  Version: 2.0
 *  Creado por: Emmanuel Walter Cumpa Niño
 *  Desde el tutorial: http://docs.jquery.com/Plugins/Authoring
 *  Última Actualización: 20/02/2012
 *  
 *  Se define el conjunto de métodos del plugin
 *  init: Define la construcción del plugin
 *  downloadReportURL: Genera la URL para que se descargue el reporte
 *  
 *  Se comenta el conjunto de parámetros que recibe y utiliza el plugin
 *  domain              (String)    =   Es el dominio de donde se extrae la información XML.
 *  clientFolder        (String)    =   Es la carpeta en donde se encuentran los reportes para el dominio indicado.
 *  htmlVisor           (boolean)   =   Activa o desactiva el visor HTML del reporte.
 *  urlData             (String)    =   Es la URL que tiene la fuente de datos en XML.
 *  clientFile          (String)    =   Es el nombre del archivo .jasper para crear el reporte.
 *  xpath               (String)    =   Es el path de acceso a la data por cada estructura que 
 *                                      representa una fila en el archivo XML.
 *  dynamicParams       (Object)    =   Contiene los parámetros dinámicos para generar el reporte, 
 *                                      pueden cambiarse mediante el método setParam().
 *  staticParams        (Object)    =   Contiene los parámetros estáticos para generar el reporte, 
 *                                      se definen una sóla vez el momento de declarar el plugin.
 *  reportParams        (Array)     =   Es un array de valores que se utilizarán como parámetros
 *                                      cuando el valor de dataSourceType sea 'db'.
 *  responseType        (String)    =   Es la forma en la que se retorna la url del arhivo del reporte. 
 *                                      Por defecto es url directa pero el valor 'jsonp' devuelve la información 
 *                                      en formato JSON con los atributos url y fileType.
 *  outputFileName      (String)    =   Sobre escribe el nombre del archivo del reporte generado.
 *  waitMessage         (String)    =   Es el mensaje que se muestra en pantalla mientras se genera el reporte como JSON.
 *  connectionName      (String)    =   Es el nombre del recurso de conexión a la base de datos 
 *                                      definida en el archivo context.xml de la aplicación en el servidor de reportes
 *  dataSourceType      (String)    =   Es el tipo de fuente de datos por defecto es 'xml', 
 *                                      pero puede ser 'db' para base de datos.
 *  reportZoom          (String)    =   es el ratio de ampliación para visualizar mejor el archivo 
 *                                      por defecto es 1.5 en el servidor.
 *  debugMode           (boolean)   =   Variable que activa el modo depuración del código del plugin.
 *  visibleOnInit       (boolean)   =   Variable que define si se visualiza o no el plugin luego de inicializado.
 *  jqGridId            (String)    =   Es el id del contenedor jqGrid del cual se obtendrá parámetros para el reporte.
 *  lang                (String)    =   Es el parámetro del lenguaje del reporte.
 *  dformat             (String)    =   Es el formato de la fecha adoptado por la empresa y definida en las opciones de quatrobus.
 *  regpass             (String)    =   Es el password de acceso al reporte.
 *  reportingServiceURL (String)    =   Es la URL del servicio de reportes.
 *  repopt              (Array)     =   Es el conjunto de formato de reportes que se presenta al usuario.
 *  orientation         (String)    =   Es la forma en que se mostrarán los botones de descarga 'horizontal' o 'vertical'
 *  position            (String)    =   Es la posición en donde se mostrará la barra de descarga de reportes 'left' o 'right'
 *  iconSet             (String)    =   Es el set de íconos para los botones 'default','3d' o 'folder'.
 *  jqUI                (boolean)   =   Variable que activa o desactiva el tema integrado con jquery UI.
 *  beforeGetReport     (function)  =   Se define la función que se ejecutará antes de obtener el reporte
 *  afterGetReport      (function)  =   Se define la función que se ejecutará después de obtener el reporte
 *  protocol            (String)    =   Es el protocolo de acceso a la data cuando dataSourceType 'XML', por ejemplo 'http'o 'https',
 *  jqCallBack          (String)    =   Es el parametro callback que jquery requiere para aceptar una respuesta jsonp
 *  afterJSONResponse   (function)  =   Se define la función que se ejecutará después de obtener el reporte en formato jsonp
 *  noShowOptionsBar    (boolean)   =   Ocullta los botones de exportación de los formatos definidos en repopt
 */
var wMessage = 'Por favor espere...';
(function( $ ) {
    var aGlobalSettings = [];
    var instanceSettings;
    var aInstanceDefaultSettings = {
        domain : null,
        clientFile:null,
        clientFolder : null,
        connectionName:null,
        dataSourceType:'xml',
        debugMode:true,
        dynamicParams:null,
        htmlVisor:false,
        iconSet:'default',
        jqGridId:null,
        jqCallBack:null,
        jqUI:false,
        orientation:'horizontal',
        outputFileName:null,
        position:'left',
        protocol:'http',
        repopt:['xls','xlsx','docx','html','pdf','txt'],
        reportingServiceURL : 'http://jreports.mcets-inc.com:8080/ibusplus.com.reportRequest/faces/reportGenerator.jsp',
        //reportingServiceURL : 'http://localhost:8084/reportRequest/faces/reportGenerator.jsp',
        reportZoom:1.5,
        responseType:null,
        staticParams:null,
        noShowOptionsBar:false,
        urlData:null,
        visibleOnInit:false,
        waitMessage:wMessage,
        xpath:null,
        beforeGetReport:function(){},
        afterGetReport:function(){},
        afterJSONResponse:function(){}
    };
    
    var methods = {
        init : function(options) {
            if($(this).length==0){
                console.warn('selector doesn\'t exist ');
                return;
            }
            var rElement = $(this);
            var rElementClass = 'iReportPlus-container';
            
            aGlobalSettings[rElement.attr('id')] = $.extend({},aInstanceDefaultSettings,options);
            aGlobalSettings.push(rElement.attr('id'));
            instanceSettings = aGlobalSettings[rElement.attr('id')];
            
            
            
            if(!instanceSettings.visibleOnInit)rElementClass +=' iReportPlus-hide';
            rElement.addClass(rElementClass);
            
            if($('div.iReportPlus-dialog-overlay').length==0){
                var rjDialogOverlay = $(document.createElement('div'));
                rjDialogOverlay.addClass('iReportPlus-dialog-overlay iReportPlus-radius-border-all iReportPlus-hide');
                $(document.body).append(rjDialogOverlay);
                
                var rjDialog = $(document.createElement('div'));
                var rjDialogMsg = $(document.createElement('div'));
                var rjDialogMsgBlink = $(document.createElement('div'));
                
                rjDialogMsgBlink.addClass('blink-msg');
                rjDialogMsg.text(instanceSettings.waitMessage);
                rjDialogMsgBlink.text(instanceSettings.waitMessage);
                rjDialogMsg.append(rjDialogMsgBlink);

                rjDialog.attr('id','rjDialog');
                var rjDailogClass = 'iReportPlus-hide iReportPlus-dialog iReportPlus-radius-border-all';
                if(instanceSettings.jqUI) rjDailogClass +=' ui-widget-content';
                rjDialog.addClass(rjDailogClass);
                rjDialog.append(rjDialogMsg);
                $(document.body).append(rjDialog);
            }
            if(instanceSettings.htmlVisor){
                var iFrameClass ='iReportPlus-iFrame iReportPlus-radius-border-all';
                var iFrame = $(document.createElement('iframe'));
                
                iFrame.attr('id', rElement.attr('id')+'_ifrmReport');
                iFrame.attr('name', rElement.attr('id')+'_ifrmReport');
            }
            var reportBarClass ='iReportPlus-radius-border-all iReportPlus-'+instanceSettings.orientation+'-bar';
            
            var divReportBar = $(document.createElement('div'));
            divReportBar.attr('id',rElement.attr('id')+'_reportBar');
            if(instanceSettings.orientation=='vertical') reportBarClass +=' iReportPlus-bar-'+instanceSettings.position;
            if(instanceSettings.jqUI){
                reportBarClass +=' ui-widget-header';
                if(instanceSettings.htmlVisor) iFrameClass +=' ui-widget-content';
            } else{
                reportBarClass +=' iReportPlus-bar';
                if(instanceSettings.htmlVisor) iFrameClass +=' iReportPlus-iFrame-style';
            }
            if(!instanceSettings.htmlVisor) iFrameClass +=' iReportPlus-hide';
            if(instanceSettings.htmlVisor) iFrame.addClass(iFrameClass);
            if(instanceSettings.noShowOptionsBar) reportBarClass +=' iReportPlus-hide';
            divReportBar.addClass(reportBarClass);
            generateReportDownloadOptions(divReportBar, iFrame, instanceSettings,rElement); 
            
            rElement.append(divReportBar);  
            if(instanceSettings.htmlVisor) rElement.append(iFrame);
            
            
            $('div#'+divReportBar.attr('id')+' a').on('click',function(){
                var idInstance = $(this).data('from');
                var fileType = $(this).attr('title');
                $('#'+idInstance).iReportingPlus('generateReport',fileType,false);
            });
        }
        ,
        generateReport:function(fileType,onlyClick){
            if(typeof onlyClick == 'undefined') onlyClick = true;
            var instanceId = $(this).attr('id');
            var gReportInstance = aGlobalSettings[instanceId];
            if(typeof gReportInstance.beforeGetReport === 'function'){
                var responseFunction = gReportInstance.beforeGetReport();
                if(typeof responseFunction == 'undefined') responseFunction = true;
                if(!responseFunction) return;
            }else{
                if(gReportInstance.debugMode) console.warn('beforeGetReport is not a function');  
            }
            if(onlyClick) if(!gReportInstance.visibleOnInit) $(this).removeClass('iReportPlus-hide');
            if(gReportInstance.debugMode && (fileType==null || $.trim(fileType)=='')){
                console.warn('"fileType" var is not set');
                return;
            } else if(gReportInstance.debugMode && !(new RegExp(/xml|db|json/).test(gReportInstance.dataSourceType.toString()))){
                console.warn('"dataSourceType" var is not "xml" or "db or json"');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.responseType!=null) && !(new RegExp('jsonp').test(gReportInstance.responseType.toString()))){
                console.warn('"responseType" only acepts null and "jsonp" values');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.responseType=='jsonp') && (gReportInstance.jqCallBack==null || $.trim(gReportInstance.jqCallBack)=='')){
                console.warn('"jqCallBack"  var is not set');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.clientFile==null || $.trim(gReportInstance.clientFile)=='')){ 
                console.warn('"clientFile" var is not set');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.clientFolder==null || $.trim(gReportInstance.clientFolder)=='')){ 
                console.warn('"clientFolder" var is not set');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.outputFileName!=null) && !(new RegExp(/^[a-zA-Z0-9_\s-]+$/).test(gReportInstance.outputFileName.toString()))){
                console.warn('"outputFileName" only acepts a-z, A-Z and "_"');
                return;
            } else if(gReportInstance.debugMode && instanceSettings.reportZoom==null){
                console.warn('"reportZoom" var is not set');
                return;
            } else if(gReportInstance.debugMode && (gReportInstance.reportZoom!=null) && !(new RegExp(/^\d+(\.\d{1,2})?$/).test(gReportInstance.reportZoom.toString()))){
                console.warn('"reportZoom" only acepts decimal numbers');
                return;
            }

            var dataSource = new Object();
            
            dataSource.clientFile=gReportInstance.clientFile;
            dataSource.clientFolder=gReportInstance.clientFolder;
            if(gReportInstance.outputFileName!=null)
                dataSource.ofn = gReportInstance.outputFileName;
            dataSource.responseType = gReportInstance.responseType;
            dataSource.dst = gReportInstance.dataSourceType;
            dataSource.fileType = fileType;
            dataSource.z = gReportInstance.reportZoom;
            
            if((new RegExp(/xml|json/).test(gReportInstance.dataSourceType.toString()))){
                if(gReportInstance.debugMode && (gReportInstance.domain==null || $.trim(gReportInstance.domain)=='')){
                    console.warn('"domain" var is not set');
                    return;
                }else if(gReportInstance.debugMode && gReportInstance.dataSourceType.toString()=='xml' &&(gReportInstance.xpath==null || $.trim(gReportInstance.xpath)=='')){ 
                    console.warn('"xpath" var is not set');
                    return;
                }else if(gReportInstance.debugMode && (gReportInstance.urlData==null || $.trim(gReportInstance.urlData)=='')){ 
                    console.warn('"urlData" var is not set');
                    return;
                }
                if(gReportInstance.dataSourceType.toString()!='json') dataSource.xpath = gReportInstance.xpath;
                dataSource.domain=gReportInstance.domain;
                dataSource.data = gReportInstance.urlData+'?';
                if(gReportInstance.staticParams!=null && typeof gReportInstance.staticParams=='object') dataSource.data +=$.param(gReportInstance.staticParams);
                if(gReportInstance.dynamicParams!=null && typeof gReportInstance.dynamicParams=='object') { 
                    if(dataSource.data.substr(dataSource.data.length-1, dataSource.data.length)!='?') dataSource.data+='&';
                    if(gReportInstance.debugMode)console.log(gReportInstance.dynamicParams);
                    dataSource.data +=$.param(gReportInstance.dynamicParams);
                }
                
                if(gReportInstance.jqGridId!=null){
                    var jqGridSelector = ("#"+gReportInstance.jqGridId).toString();
                    if($(jqGridSelector).length>0){
                        if(!dataSource.data.toString().ends('?')) dataSource.data+='&';
                        var pd =$(jqGridSelector).jqGrid('getPostData');
                        $.each(pd,function(i){
                            dataSource.data += i+"="+pd[i]+"&";
                        })
                    } else if(gReportInstance.debugMode){
                        console.warn('"jqGridId" don\'t exist');
                        return;
                    }
                }
                if(gReportInstance.debugMode) console.log(gReportInstance.protocol+'://'+gReportInstance.domain+dataSource.data);
                
            } else if(gReportInstance.dataSourceType=='db'){
                
                if(gReportInstance.debugMode && (gReportInstance.connectionName==null || $.trim(gReportInstance.connectionName)=='')){
                    console.warn('"connectionName" var is not set');
                    return;
                }else{
                    dataSource.cn = gReportInstance.connectionName;
                }
                if(gReportInstance.debugMode && (gReportInstance.reportParams==null || gReportInstance.reportParams.length==0)){
                    console.warn('"reportParams" is empty');
                    return;
                } else{
                    dataSource.p = gReportInstance.reportParams;
                }
            }
            if(gReportInstance.debugMode) console.log(dataSource);
            
            var downloadURL = gReportInstance.reportingServiceURL;
            
            if(gReportInstance.responseType==null){
                downloadURL +='?'+$.param(dataSource);
                if(gReportInstance.debugMode) console.log(downloadURL);
                if(gReportInstance.htmlVisor){
                    if(fileType=='html'){
                        if(!gReportInstance.visibleOnInit)  $('#'+instanceId).removeClass('iReportPlus-hide');
                        $('#'+instanceId).iReportingPlus('showiReportDialog');
                        $('#'+instanceId+'_ifrmReport').attr('onload'," $('#"+instanceId+"').iReportingPlus('hideiReportDialog')");
                        $('#'+instanceId+'_ifrmReport').attr('src',downloadURL);
                    } else{
                        window.open(downloadURL,'','width=200,height=200,resizable=yes,location=no,status=yes');
                    }
                }else{
                    window.open(downloadURL,'','width=200,height=200,resizable=yes,location=no,status=yes');
                }
            } else {
                $('div.iReportPlus-json-report-dialog').fadeOut(500);
                if(fileType=='html' && gReportInstance.htmlVisor) {
                    delete dataSource.responseType;
                    downloadURL +='?'+$.param(dataSource);
                    if(gReportInstance.debugMode) console.log(downloadURL);
                    if(!gReportInstance.visibleOnInit)  $('#'+instanceId).removeClass('iReportPlus-hide');
                    $('#'+instanceId).iReportingPlus('showiReportDialog');
                    $('#'+instanceId+'_ifrmReport').attr('onload'," $('#"+instanceId+"').iReportingPlus('hideiReportDialog')");
                    $('#'+instanceId+'_ifrmReport').attr('src',downloadURL);
                } else {
                    downloadURL +=gReportInstance.jqCallBack+'&'+$.param(dataSource);
                    if(gReportInstance.debugMode) console.log(downloadURL);
                    resultAsJSONP(downloadURL,gReportInstance.afterJSONResponse,instanceId,gReportInstance.debugMode,onlyClick);
                }
            }
            
            if(typeof gReportInstance.afterGetReport === 'function'){
                responseFunction = gReportInstance.afterGetReport();
                if(typeof responseFunction == 'undefined') responseFunction = true;
                if(!responseFunction) return;
            }else{
                if(gReportInstance.debugMode) console.warn('afterGetReport is not a function');  
            }
        }
        ,
        option : function(property, value){
            var gReportInstance = aGlobalSettings[$(this).attr('id')];
            switch (property) {
                case 'domain':
                    gReportInstance.domain = value;
                    break;
                case 'clientFolder':
                    gReportInstance.clientFolder = value;
                    break;
                case 'clientFile':
                    gReportInstance.clientFile = value;
                    break;
                case 'urlData':
                    gReportInstance.urlData = value;
                    break;
                case 'xpath':
                    gReportInstance.xpath = value;
                    break;
                case 'dynamicParams':
                    gReportInstance.dynamicParams = value;
                    break;
                case 'reportZoom':
                    gReportInstance.reportZoom = value;
                    break;
                case 'jqGridId':
                    gReportInstance.jqGridId = value;
                    break;
                case 'reportingServiceURL':
                    gReportInstance.reportingServiceURL = value;
                    break;
                case 'responseType':
                    gReportInstance.responseType = value;
                    break;
                case 'outputFileName':
                    gReportInstance.outputFileName = value;
                    break;
                case 'dataSourceType':
                    gReportInstance.dataSourceType = value;
                    break;
                case 'protocol':
                    gReportInstance.dataSourceType = value;
                    break;
                default:
                    break;
            }

        }
        ,
        showiReportDialog:function(){
            $('div.iReportPlus-dialog-overlay,div#rjDialog').removeClass('iReportPlus-hide');
        }
        ,
        hideiReportDialog:function(){
            $('div.iReportPlus-dialog-overlay,div#rjDialog').addClass('iReportPlus-hide');
        }
    }
        
    $.fn.iReportingPlus =  function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jquery.ibusplus.reporting.plugin' );
        }        
    };
    
    function generateReportDownloadOptions(divReportBar,iFrame,tmpInstanceSettings,rElement){
        $.each(tmpInstanceSettings.repopt,function(idx,value){
            var uniqID;
            if(tmpInstanceSettings.htmlVisor) uniqID = iFrame.attr('name');
            else uniqID = rElement.attr('id');
            var linkElement = $(document.createElement('a'));
            
            linkElement.attr('id',uniqID+'_'+value);
            linkElement.data('from',rElement.attr('id'));
            linkElement.attr('title',value);
            if(tmpInstanceSettings.htmlVisor)linkElement.attr('target',uniqID);
            linkElement.addClass('iReportPlus-export-option-'+tmpInstanceSettings.orientation+' iReportPlus-radius-border-all iReportPlus-export-option-link iReportPlus-export-option-link-'+tmpInstanceSettings.iconSet+'-'+value);
            divReportBar.append(linkElement);
        });
        
        var wElement = $(document.createElement('p'));
        if(tmpInstanceSettings.repopt.length>2){
            wElement.text(tmpInstanceSettings.waitMessage);
            wElement.addClass('iReportPlus-radius-border-all iReportPlus-export-waiting ');
        } else{
            wElement.text('...');
            wElement.addClass('iReportPlus-radius-border-all iReportPlus-export-waiting iReportPlus-export-waiting-only-img');
        }
        divReportBar.append(wElement);
        if($('div.iReportPlus-json-report-dialog').length==0){
            var popupDownloadJSON = $(document.createElement('div')).css('display','none');
            var popupClass = 'iReportPlus-radius-border-all iReportPlus-json-report-dialog';
            if(tmpInstanceSettings.jqUI) {
                popupClass +=' ui-widget-content'
            } else {
                popupClass +=' iReportPlus-json-report-dialog-border';
            }
            popupDownloadJSON.addClass(popupClass);
            var popupClassCloseJSONDialog = 'iReportPlus-json-report-dialog-close';
            if(tmpInstanceSettings.jqUI) popupClassCloseJSONDialog='';
            var closeElement = 'div';
            if(tmpInstanceSettings.jqUI) closeElement = 'button'
            var popupCloseJSONDialog = $(document.createElement(closeElement)).addClass(popupClassCloseJSONDialog).text('X');
            popupCloseJSONDialog.click(function(){
                var dialog = $(this).parent().parent().fadeOut(500);
            });
            if(tmpInstanceSettings.jqUI){
                popupCloseJSONDialog.button({
                    icons:{
                        primary:'ui-icon-closethick'
                    },
                    text:false
                });
            }
            var popupDownloadJSONDiv = $(document.createElement('div')).append(popupCloseJSONDialog);
            popupDownloadJSONDiv.append($(document.createElement('ul')));
            popupDownloadJSON.append(popupDownloadJSONDiv);
            $(document.body).append(popupDownloadJSON);
        }
    }
    
    function showiReportDialog(){
        $('div.iReportPlus-dialog-overlay,div#rjDialog').removeClass('iReportPlus-hide');
    }
    
    function hideiReportDialog(){
        $('div.iReportPlus-dialog-overlay,div#rjDialog').addClass('iReportPlus-hide');
    }
    
    function showiReportJSONDialog(instanceId){
        $('div#'+instanceId+'_reportBar p.iReportPlus-export-waiting').show();
    }
    
    function hideiReportJSONDialog(instanceId){
        $('div#'+instanceId+'_reportBar p.iReportPlus-export-waiting').hide();
    }
    
    function resultAsJSONP(downloadURL,afterJSONResponse,instanceId,debugMode,onlyClick){
        $.ajax({
            type: "GET",
            url: downloadURL,
            dataType:'jsonp',
            async:false,
            beforeSend:function(){
                showiReportJSONDialog(instanceId);
                if(onlyClick) showiReportDialog();
            },
            success: function(response){
                //propiedades r.code, r.url,r.fileName y r.fileType
                response.url = decodeURI(response.url);
                if(debugMode) console.log(response);
                afterJSONResponse(response);
                $('.iReportPlus-json-report-dialog').find('li').remove();
                var text = response.fileName;
                var textClass = 'iReportPlus-json-report-success';
                var liElement = $(document.createElement('li'));
                if(!Boolean(eval(response.code))) {
                    text = 'Report Error: Click for details.';
                    textClass = 'iReportPlus-json-report-error';
                }
                liElement.addClass(textClass);
                var aElement = $(document.createElement('a')).attr('href',response.url).attr('target','_blank').text(text);
                liElement.append(aElement);
                aElement.click(function(){
                    var aLink = $(this);
                    setTimeout(function(){
                        aLink.parents('div.iReportPlus-json-report-dialog').fadeOut(500);
                    }, 5000);
                });
                $('.iReportPlus-json-report-dialog').find('ul').append(liElement);
                $('.iReportPlus-json-report-dialog').fadeIn(500);
            },
            complete:function(){
                hideiReportJSONDialog(instanceId);
                if(onlyClick) hideiReportDialog();
            },
            onError:function(){
                hideiReportJSONDialog(instanceId);
                if(onlyClick) hideiReportDialog();
            }
        });
    }
})( jQuery );