$(document).ready(function(){
    /**
     * Dialogos personalizados
     * no modal y modal
     * 
     **/
    $('.dialog').dialog({
        autoOpen:false,
        autoSize:true,
        modal:false,
        resizable:false,
        closeOnEscape:true,
        beforeClose:function(){
            $('#errorMessages').hide();
            $('input[type=text],input[type=password],textarea,select').removeClass('ui-state-error');
        }
    });
    
    $('.dialog-modal').dialog({
        autoOpen:false,
        autoSize:true,
        modal:true,
        resizable:false,
        closeOnEscape:true,
        beforeClose:function(){
            $('#errorMessages').hide();
            $('input[type=text],input[type=password],textarea,select').removeClass('ui-state-error');
        }
    });

    /**
 * Dialogos de interacción con el usuario en sesión
 **/
    $('#user_chpass').dialog({
        autoOpen:false,
        closeOnEscape:true,
        draggable:true,
        width:420,
        modal:true,
        resizable:false,
        title:chPassText,
        open:function(){
            $('#cp_info_msg').hide();
            $('#cp_error_msg').hide();
            $('#frmChPass').find('input').val('');
        }
    });
    

    $('#frmChPass').submit(function(event){
        $('#cp_info_msg').hide();
        $('#cp_error_msg').hide();
        event.preventDefault();
        var data=$(this).serialize();
        var cp = $.trim($('#chpass_current').val());
        var np = $.trim($('#chpass_new').val());
        var cpn = $.trim($('#chpass_confirm').val());
        if(cp==''||np==''||cpn==''){
            $('#cp-text-error').text(pass_blank);
            $('#cp_error_msg').show();
            return false;
        }
        
        if(np!=cpn){
            $('#cp-text-error').text(pass_confirm);
            $('#cp_error_msg').show();
            return false;
        }
        
        $.ajax({
            type:'POST',
            url:'/private/welcome/changePass'+jquery_params,
            data:data,
            dataType:'json',
            beforeSend:function(){
                $('#cp_info_msg').hide();
                $('#cp_error_msg').hide();
            },
            error: function(jqXHR, textStatus, errorThrown){            
                $('#cp-text-error').text(errorMessage);
                $('#cp_error_msg').show();
            },
            success:function(response){
                if(evalResponse(response)){
                    $('#frmChPass').find('input').val('');
                    $('#cp-text-info').text(response.msg);
                    $('#cp_info_msg').show();
                } else {
                    $('#cp-text-error').text(response.msg);
                    $('#cp_error_msg').show();
                }
            }
        });
    });
    
    $('#user_info').dialog({
        autoOpen:false,
        closeOnEscape:true,
        draggable:true,
        width:630,        
        modal:true,
        resizable:false,
        title:userInfoText,
        buttons:
        [{
            id:"btn-password",
            text: chPassText,
            click: function() {
                $('#user_chpass').dialog("open");
            }
        },{
            id:"btn-accept",
            text: okButtonText,
            click: function() {
                if($('#imgProfile_file').val()!=''){
                    $.ajax({
                        url:'/private/welcome/updateImageProfile'+jquery_params,
                        type:'POST',
                        data:{
                            img_p:$('#imgProfile_file').val()
                        },
                        dataType:'json',
                        success:function(response){
                            if(evalResponse(response)){
                                $('#user_info').dialog("close");
                            } 
                        }
                    });
                } else{
                    $(this).dialog("close");
                }
                
            }
        }]
    });

    $('#accountInfo').live('click',function(){
        showLoading = 1;
        $.ajax({
            url:'/private/welcome/getUserInfo'+jquery_params,
            success:function(response){
                if(evalResponse(response)){
                    response=response.data;
                    $('#user_fName').text(response.user_fName);
                    $('#user_lName').text(response.user_lName);
                    $('#user_group_name').text(response.user_group_name);
                    $('#user_name_info').text(response.user_name_info);
                    $('#user_email').text(response.user_email);
                    $('#user_office').val(response.user_office);
                    if(response.user_os==null || response.user_printer ==null || response.user_browser == null){
                        $('#user_terminal_os').text(msg_unknown);
                        $('#user_printer').text(msg_unknown);
                        $('#user_browser').text(msg_unknown);
                    } else {
                        $('#user_terminal_os').text(response.user_os);
                        $('#user_printer').text(response.user_printer);
                        $('#user_browser').text(response.user_browser);
                    }
                    $('#imgProfile_ui').css('background-repeat','none');
                    if(response.user_img!=null){
                        $('#imgProfile_ui').css('background-image','url('+response.user_img+')');
                    } else{
                        $('#imgProfile_ui').css('background-image','url(/upload/profile/128/02.png)');
                    }
                    $('#user_info').dialog('open');
                }
            }
        });
        
    });
    $('#user_office').live('change',function(){
        var office_id=$(this).val();
        confirmBox(changeOfficeText,confirmationText,function(response){
            if(response == true)
            {
                $.ajax(
                {
                    url: '/private/welcome/changeOffice',
                    data:{
                        office_id:office_id
                    },
                    success: function(response)
                    { 
                        if(evalResponse(response)){
                            location.reload();
                        } 
                    }
                });
            }else{
                $('#user_info').dialog('close');
            }
        });
  
    });

});
