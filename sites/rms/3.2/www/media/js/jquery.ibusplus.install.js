$(document).ready(function()
    {
        $.fn.serializeObject = function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    
        $('.dialog').dialog({
            autoOpen:false,
            autoSize:true,
            modal:false,
            resizable:false,
            closeOnEscape:true,
            beforeClose:function(){
                $('#errorMessages').hide();
                $('input[type=text],input[type=password],textarea,select').removeClass('ui-state-error');
            }
        });        
        $('.dialog-modal').dialog({
            autoOpen:false,
            autoSize:true,
            modal:true,
            resizable:false,
            closeOnEscape:true,
            beforeClose:function(){
                $('#errorMessages').hide();
                $('input[type=text],input[type=password],textarea,select').removeClass('ui-state-error');
            }
        });
        msgBox = function(text, title, after)
        {
            $("div.msgbox").each(function() {
                $(this).remove();
            });
            $("body").append("<div class='msgbox' >"+text+"</div>");
            var dlg = $("div.msgbox");
            $("div.msgbox").dialog({
                autoOpen: true,
                modal:true,
                title:title,
                position:'center',
                minWidth:350,
                buttons:
                {
                    "OK":function()
                    {
                        $(dlg).remove();
                        if ( typeof(after) == 'function' ) after();                    
                    }
                },
                close: function(event, ui)
                {
                    $(dlg).remove();
                    if ( typeof(after) == 'function' ) after();
                },
                open: function(event, ui)
                {
                    $(dlg).find("button").focus();
                }
            });       
        }
        $.ajaxSetup({
            type:'POST',
            dataType:'json'
        });
    });