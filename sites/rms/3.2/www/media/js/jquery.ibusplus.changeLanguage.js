/**********************************
 *Función para cambiar el idioma de las páginas
 **********************************/
$(document).ready(function(){
    $("#idioma_es").live('click',function(){
        $.ajax(
        {
            type:'POST',
            dataType:'json',
            url: '/public/internationalization/change2Es'+jquery_params,
            success: function(response)
            {
                if(response.code==code_success){                    
                    location.reload();
                } else{
                    alert(response.code+':'+response.msg);
                }

            }
        });

    });
    $("#idioma_en").live('click',function(){
        $.ajax(
        {
            type:'POST',
            dataType:'json',
            url: '/public/internationalization/change2En'+jquery_params,
            success: function(response)
            {
                if(response.code==code_success){                    
                    location.reload();
                } else{
                    alert(response.code+':'+response.msg);
                }
            }
        });
    });
});