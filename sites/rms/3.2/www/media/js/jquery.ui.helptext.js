/**
 * @class helptext
 * 
 * @example http://wiki.jqueryui.com/w/page/12138135/Widget%20factory
 *          https://github.com/scottgonzalez/widget-factory-docs
 * 
 * @autor -JD-
 */
$.widget( "jd.helptext", {
    options: {
        value: 'TEXTO DEFECTO',
        customClass: 'helptext'
    },
    _create: function() {
        var self = this;
        this.element        
        .focus(function(){            
            if($(this).val()==self.options.value){
                $(this).val("");
                self.element.removeClass('helptext');
            }
        }).blur(function(){
            if($(this).val()==''){
                $(this).val(self.options.value);
                self.element.addClass('helptext');
            }
        });
        this.refresh();
    },
    refresh: function() {
        this.element.addClass('helptext').val(this.options.value);
    },
    _destroy: function() {
        this.element
        .removeClass(this.options.customClass)
        .val( "" );
    }
});