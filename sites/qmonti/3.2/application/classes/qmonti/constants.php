<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of constants
 *
 * @author Emmanuel
 */
interface Qmonti_Constants {
    /*CONSTANTES DE AREADRIVER*/
    const AREADRIVER_AMBULATORY = 'AMBULATORY';
    const AREADRIVER_WHEELCHAIR = 'WHEELCHAIR';
    const AREADRIVER_STRETCHER  = 'STRETCHER';
    const DRIVER_DEDUCTION_A = 'A';
    const DRIVER_DEDUCTION_B = 'B';
    const STATE_ACTIVE = '1';
    const STATE_INACTIVE = '0';
    const INTERPRETING = 'INTERPRETING';
    const CONFERENCECALL = 'CONFERENCECALL';
    const DOCUMENTTRANSLATION = 'DOCUMENTTRANSLATION';
    const AMBULATORY = 'AMBULATORY';
    const WHEELCHAIR = 'WHEELCHAIR';
    const STRETCHER = 'STRETCHER';
    const LEGAL = 'LEGAL';
    const MEDICAL = 'MEDICAL';
    const OTHER = 'OTHER';
    const ONEWAY = 'OneWay';
    const ROUNDTRAVEL = 'RoundTravel';
    const CREATED_CONFIRMATION_ADJUSTER = 'CREATED_CONFIRMATION_ADJUSTER';
    const CALLING_LIST = 'CALLING_LIST';
    const CONFIRMED = 'CONFIRMED';
    const APP_CONFIRMED = 'APP_CONFIRMED';
    
    

}
//