<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of constants
 *
 * @author Emmanuel
 */
interface Monti_Constants {

    const FOLDER_UPLOAD_LOGO = 'bxlogo';
    const TRANSPORT_TYPE_PASSENGER_BUS = 2;
    const ROUTE_STATUS_TYPE_UPDATE = 2;
    const ROUTE_STATUS_TYPE_NEW = 4;
    const ROUTE_RESPONSE_DIRECT = 1;
    const ROUTE_RESPONSE_TRANSFER_DEPARTURE_AND_ARRIVAL = 2;
    const ROUTE_RESPONSE_TRANSFER_DEPARTURE_OR_ARRIVAL = 3;
    #PARAMETERS OF WEB SESSION
    const GROUP_WEB_CUSTOMER = 2;
    const GROUP_WEB_AGENT = 3;
    const GROUP_WEB_COMPANY = 4;
    const TYPE_WEB_CUSTOMER = 2;
    const TYPE_WEB_AGENT = 3;
    const TYPE_WEB_COMPANY = 4;
    const USER_WEB_SESSION_ID = 'session_web_id';
    const USER_WEB_NAME = 'user_web_name';
    const USER_WEB_FULL_NAME = 'user_web_fullname';
    const USER_WEB_ID = 'user_web_id';
    const USER_WEB_LOGEO_IN = 'user_web_logeo_in';
    const SC_KEY = "_QUATROBUS_ROCKS_";
    const SC_SHOPPINGCART_SID = 'shcart_id';
    const SC_NATIVE_SESSION_KEY = 'shcart_key';
    const SC_IV_ENCRYPT = 'iv_decrypt';
    const SC_STATUS_COMPLETED = 'COMPLETED';
    const SC_STATUS_UNCOMPLETED = 'UNCOMPLETED';
    const SC_STATUS_CANCELED = 'CANCELED';
    const SC_STATUS_RESET = 'RESET';
    const SC_STATUS_EXPIRED_SEAT_SELECTION = 'EXPIRED_SEAT_SELECTION';
    const SC_STATUS_EXPIRED_WAIT_TIME = 'EXPIRED_WAIT_TIME';
    const SC_SESSION_KEYS_ARRAY = 'SESSION_KEYS_ARRAY';
    const SC_SESSION_COUPONS_ARRAY = 'SESSION_COUPONS_ARRAY';
    const SC_TRAVEL_TYPE_ONLY_ONE_WAY = 0;
    const SC_TRAVEL_TYPE_ONE_WAY = 1;
    const SC_TRAVEL_TYPE_ROUND_TRIP = 2;
    const SC_TICKET_COUPON_VALIDATE = 'TICKET_COUPON_VALIDATE';
    const SO_PAIS_RESIDENTE = 'PAIS_RESIDENTE';
    /* CONSTANTES PARA INFORMACIÓN DE SESIÓN NATIVA */
    const COUNTRY_CODE_PERU = "PE";
    const PUBLIC_SESSION_DATA = 'public_session_data';
    const MODULE_WEB_SALE = 'WEB_SALE';

#Venta Web
    const SO_WEB_SALE_PARAMETER = 'wse';
    const SO_WEB_EXPIRATION_TIME = 'WEB_EXPIRATION_TIME';
    const SO_WEB_WAIT_EXPIRATION_TIME = 'WEB_WAIT_EXPIRATION_TIME';
    const SO_WEB_MAX_SELECTABLE_SEATS = 'WEB_MAX_SELECTABLE_SEATS';
    const SO_WEB_GENRE_ON_SELECT_SEAT = 'WEB_GENRE_ON_SELECT_SEAT';
    const SO_WEB_SALE_SUCCESS_EMAIL_TO_COMPANY = 'WEB_SALE_SUCCESS_EMAIL_TO_COMPANY';
    const SO_WEB_SALE_SUCCESS_MESSAGE_TO_COMPANY = 'WEB_SALE_SUCCESS_MESSAGE_TO_COMPANY';
    const VAR_NAME_WEB_COMPANY_STATUS = 'A_WEB_COMPANY_STATUS';
    const WEB_COMPANY_STATUS_PENDING = 'PENDING';
    const WEB_COMPANY_STATUS_ACTIVE = 'ACTIVE';
    const WEB_COMPANY_STATUS_STOPPED = 'STOPPED';
    const WEB_COMPANY_ACTION_RATING = 'RATING';
    const STATE_ACTIVE = '1';
    const STATE_INACTIVE = '0';
    const INTERPRETING = 'INTERPRETING';
    const CONFERENCECALL = 'CONFERENCECALL';
    const DOCUMENTTRANSLATION = 'DOCUMENTTRANSLATION';
    const AMBULATORY = 'AMBULATORY';
    const WHEELCHAIR = 'WHEELCHAIR';
    const STRETCHER = 'STRETCHER';
    const LEGAL = 'LEGAL';
    const MEDICAL = 'MEDICAL';
    const OTHER = 'OTHER';
    const ONEWAY = 'OneWay';
    const ROUNDTRAVEL = 'RoundTravel';
    const CREATED_CONFIRMATION_ADJUSTER = 'CREATED_CONFIRMATION_ADJUSTER';
    const CALLING_LIST = 'CALLING_LIST';
    const CONFIRMED = 'CONFIRMED';
    const APP_CONFIRMED = 'APP_CONFIRMED';
    const WEB_COMPANY_ACTION_SEND_EMAIL = 'SEND_EMAIL';
    const WEB_COMPANY_ACTION_STOP = 'STOP';
    const WEB_COMPANY_ACTION_RESUMEN = 'RESUMEN';

    /*   MONTI  * ******* */
    const AREADRIVER_AMBULATORY = 'AMBULATORY';
    const AREADRIVER_WHEELCHAIR = 'WHEELCHAIR';
    const AREADRIVER_STRETCHER = 'STRETCHER';
    const DRIVER_DEDUCTION_A = 'A';
    const DRIVER_DEDUCTION_B = 'B';
    
    




}

?>
