<?php

class Model_Locations extends Kohana_Btsorm {

    protected $_table_names_plural = true;
    protected $_table_name = 'locations';
    protected $_primary_key = 'idLocation';

    public function createUpdateLocation($a_Location) {

        $idLocation = $a_Location['idLocation'];
        if ($idLocation > 0) {
//Edicion
            $oLocation = new Model_Locations($idLocation);

            if (!$oLocation->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
        } else {
//Nuevo
            $oLocation = new Model_Locations();
        }

        $oLocation->alias = $a_Location['contact'];
        $oLocation->address = $a_Location['address'];
        $oLocation->suite = $a_Location['suite'];
        $oLocation->telephone = $a_Location['telephone'];
        $oLocation->save();
    }

}