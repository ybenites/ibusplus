<?php

class Model_Provider extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'provider';
    protected $_primary_key = 'idProvider';

    public function createProvider($post_data) {
        /* Ingreso los datos */
        $idProvider= $post_data['idProvider'];
        $name = $post_data['name'];
        $phone = $post_data['phone'];
        $email = $post_data['provider_email'];
        $status = $post_data['status'];

          if (empty($idProvider)) {
            
            $o_model_Provider = new Model_Provider();
            
        } else {
            
            $o_model_Provider = new Model_Provider($idProvider);
            
            if (!$o_model_Provider->loaded())
                throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
        }
        $o_model_Provider->name = $name;
        $o_model_Provider->phone = $phone;
        $o_model_Provider->email = $email;
        $o_model_Provider->status = $status;

        $o_model_Provider->save();
        
      return $o_model_Provider;
        

    }

}

?>
