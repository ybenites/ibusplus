<?php

class Model_Checks extends Kohana_Btsorm {
    protected $_table_names_plural = false;
   protected $_table_name = 'checks';
   protected $_primary_key = 'idCheck';  
   
  public function createUpdateCheck($a_check) {

        $idCheck = $a_check['idCheck'];
        if ($idCheck > 0) {
//Edicion
            $o_check = new Model_Checks($idCheck);

            if (!$o_check->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
        } else {
//Nuevo
            $o_check = new Model_Checks();
            $o_check->idCheck = $idCheck;
        }

        $o_check->idInsuranceCarrier = $a_check['idInsuranceCarriers'];
        $o_check->numbercheck = $a_check['numberCheck'];
        $o_check->amount = $a_check['amount'];
        $o_check->memo = $a_check['memo'];
        $o_check->status = 1;
        $o_check->state = 1;
        $o_check->paid = 0.00;
        $o_check->available = $a_check['amount'];
        $o_check->datecheck = $this->cambiafmysqlre($a_check['dateCheck']) ;
        $o_check->dateregistration = date('Y-m-d H:i:s');
//        $o_check->userCreate= $this->getSessionParameter(self::USER_ID);
        $o_check->creationDate = date('Y-m-d H:i:s');
        
        $o_check->save();
    }
    
    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }
}
 