<?php

class Model_TranslationSummary extends Kohana_Btsorm {

    protected $_table_names_plural = true;
    protected $_table_name = 'translationsummary';
    protected $_primary_key = 'idTranslationSummary';

    public function createUpdateTranslationSummary($a_translation_summary) {
       
        $idTranslationSummary = $a_translation_summary['idTranslation'];

        if ($idTranslationSummary > 0) {
//Edicion
            $o_translation = new Model_TranslationSummary($idTranslationSummary);

            if (!$o_translation->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
        } else {
//Nuevo
            $o_translation = new Model_TranslationSummary();
//            $o_translation->idTranslationSummary = $idTranslationSummary;
        }

        $o_translation->idInterpreter = $a_translation_summary['idInterpreter'];
        $o_translation->idAppointment = $a_translation_summary['idAppointment'];
        $o_translation->interpretationTimeFrom = $a_translation_summary['interpretationTimeFrom'];
        $o_translation->interpretationTimeTo = $a_translation_summary['interpretationTimeTo'];
        $o_translation->interpretationTimeTotalTime = $a_translation_summary['interpretationTimeTotalTime'];
        $o_translation->travelFrom = $a_translation_summary['travelFrom'];
        $o_translation->travelTo = $a_translation_summary['travelTo'];
        $o_translation->travelTotalTime = $a_translation_summary['travelTotalTime'];
        $o_translation->totalMileage = $a_translation_summary['totalMileage'];
        $o_translation->waitTime = $a_translation_summary['waitTime'];
        $o_translation->why = $a_translation_summary['why'];
        $o_translation->summaryofAssignment = $a_translation_summary['summaryofAssignment'];
        $o_translation->effectiveDate = $this->cambiafmysqlre($a_translation_summary['effectiveDate']);
        $o_translation->restrictions = $a_translation_summary['restrictions'];
        $o_translation->forHowLong = $this->cambiafmysqlre($a_translation_summary['forHowLong']);
        $o_translation->dateOfFollowUp = $this->cambiafmysqlre($a_translation_summary['dateOfFollowUp']);
        $o_translation->timeOfFollowUp = $a_translation_summary['timeOfFollowUp'];
        $o_translation->dateOfFollowUp1 = $this->cambiafmysqlre($a_translation_summary['dateOfFollowUp1']);
        $o_translation->timeOfFollowUp1 = $a_translation_summary['timeOfFollowUp1'];
        $o_translation->dateOfFollowUp2 = $this->cambiafmysqlre($a_translation_summary['dateOfFollowUp2']);
        $o_translation->timeOfFollowUp2 = $a_translation_summary['timeOfFollowUp2'];
        $o_translation->commentDateFollowUp = $a_translation_summary['commentDateFollowUp'];

//            $o_translation->interpreterName = $intTimeTo;
//            $o_translation->client = $intTimeTo;
//            $o_translation->otherExpenses = $intTimeTo;
//            $o_translation->transpages = $intTimeTo;
//            $o_translation->transwords = $intTimeTo;

        if ($a_translation_summary['confirmAble'] == 'yes') {
            $o_translation->ableToWork = 1;
            if ($a_translation_summary['duty'] == 'full')
                $o_translation->duty = 1;
            else
                $o_translation->duty = 0;
        }
        else
            $o_translation->ableToWork = 0;

        if ($a_translation_summary['FollowUpAppointment'] == 'yes')
            $o_translation->FollowUpAppointment = 1;
        else
            $o_translation->FollowUpAppointment = 0;

        $o_translation->save();
    }
      public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }
}