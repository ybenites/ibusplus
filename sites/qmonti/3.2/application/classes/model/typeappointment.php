<?php

class Model_Typeappointment extends Kohana_Btsorm {

    protected $_table_names_plural = false;
    protected $_table_name = 'Typeappointment';
    protected $_primary_key = 'idTypeappointment';
    
     public function createTypeappointment($post_data) {
        /* Ingreso los datos */
        $idTypeappointment= $post_data['idTypeappointment'];
        $name = $post_data['name'];
        $type = $post_data['type'];

          if (empty($idTypeappointment)) {
            
            $o_model_Typeappointment = new Model_Typeappointment();
            
        } else {
            
            $o_model_Typeappointment = new Model_Typeappointment($idTypeappointment);
            
            if (!$o_model_Typeappointment->loaded())
                throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
        }
        $o_model_Typeappointment->name = $name;
        $o_model_Typeappointment->type = $type;

        $o_model_Typeappointment->save();
        
      return $o_model_Typeappointment;
        

    }
    
}


?>
