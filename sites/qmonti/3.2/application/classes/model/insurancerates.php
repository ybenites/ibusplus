<?php

class Model_InsuranceRates extends Kohana_Btsorm {

    protected $_table_names_plural = true;
    protected $_table_name = 'insurancerates';
    protected $_primary_key = 'idInsuranceRates';

    public function saveInsuranceRates($a_insurance_rates) {
        if (!empty($a_insurance_rates['idInsuranceRates']))
            $objInsuranceRate = new Model_InsuranceRates($a_insurance_rates['idInsuranceRates']);
        else
            $objInsuranceRate = new Model_InsuranceRates();

        $objInsuranceRate->idInsuranceCarrier = $a_insurance_rates['idInsurancecarrier'];

        $objInsuranceRate->noShowIntMedicalRegular = $a_insurance_rates['noShowIntMedicalRegular'];
        $objInsuranceRate->noShowIntMedicalRegularMin = $a_insurance_rates['noShowIntMedicalRegularMin'];

        $objInsuranceRate->noShowIntMedicalRare = $a_insurance_rates['noShowIntMedicalRare'];
        $objInsuranceRate->noShowIntMedicalRareMin = $a_insurance_rates['noShowIntMedicalRareMin'];

        $objInsuranceRate->noShowIntMedicalOther = $a_insurance_rates['noShowIntMedicalOther'];
        $objInsuranceRate->noShowIntMedicalOtherMin = $a_insurance_rates['noShowIntMedicalOtherMin'];

        $objInsuranceRate->noShowIntLegalRegular = $a_insurance_rates['noShowIntLegalRegular'];
        $objInsuranceRate->noShowIntLegalRegularMin = $a_insurance_rates['noShowIntLegalRegularMin'];

        $objInsuranceRate->noShowIntLegalRare = $a_insurance_rates['noShowIntLegalRare'];
        $objInsuranceRate->noShowIntLegalRareMin = $a_insurance_rates['noShowIntLegalRareMin'];

        $objInsuranceRate->noShowIntLegalOther = $a_insurance_rates['noShowIntLegalOther'];
        $objInsuranceRate->noShowIntLegalOtherMin = $a_insurance_rates['noShowIntLegalOtherMin'];



        $objInsuranceRate->medicalRegular = $a_insurance_rates['medicalRegular'];
        $objInsuranceRate->medicalOther = $a_insurance_rates['medicalOther'];
        $objInsuranceRate->medicalRare = $a_insurance_rates['medicalRare'];
        $objInsuranceRate->legalRegular = $a_insurance_rates['legalRegular'];
        $objInsuranceRate->legalOther = $a_insurance_rates['legalOther'];
        $objInsuranceRate->legalRare = $a_insurance_rates['legalRare'];
        $objInsuranceRate->conferenceCallRegular = $a_insurance_rates['conferenceCallRegular'];
        $objInsuranceRate->conferenceCallOther = $a_insurance_rates['conferenceCallOther'];
        $objInsuranceRate->conferenceCallRare = $a_insurance_rates['conferenceCallRare'];
        $objInsuranceRate->travelMiles = $a_insurance_rates['travelMiles'];
        $objInsuranceRate->travelTime = $a_insurance_rates['travelTime'];
        $objInsuranceRate->cityType = $a_insurance_rates['cityType'];
        $objInsuranceRate->gasolinesurcharge = $a_insurance_rates['gasolinesurcharge'];
        $objInsuranceRate->ambulatoryRate = $a_insurance_rates['ambulatoryRate'];
        $objInsuranceRate->ambulatoryWaitingTime = $a_insurance_rates['ambulatoryWaitingTime'];
        $objInsuranceRate->minimumFlatRateRTAmbulatory = $a_insurance_rates['minimumFlatRateRTAmbulatory'];
        $objInsuranceRate->minimumFlatRateOWAmbulatory = $a_insurance_rates['minimumFlatRateOWAmbulatory'];
        $objInsuranceRate->ambulatoryNoShow = $a_insurance_rates['ambulatoryNoShow'];
        $objInsuranceRate->wheelChairLoadedMiles = $a_insurance_rates['wheelChairLoadedMiles'];
        $objInsuranceRate->wheelChairLiftBaseRate = $a_insurance_rates['wheelChairLiftBaseRate'];
        $objInsuranceRate->wheelChairWaitingTime = $a_insurance_rates['wheelChairWaitingTime'];
        $objInsuranceRate->wheelChairCarAssitance = $a_insurance_rates['wheelChairCarAssitance'];
        $objInsuranceRate->minimumFlatRateRTWheelChair = $a_insurance_rates['minimumFlatRateRTWheelChair'];
        $objInsuranceRate->minimumFlatRateOWWheelChair = $a_insurance_rates['minimumFlatRateOWWheelChair'];
        $objInsuranceRate->wheelChairNoShow = $a_insurance_rates['wheelChairNoShow'];
        $objInsuranceRate->stretcherLoadedMiles = $a_insurance_rates['stretcherLoadedMiles'];
        $objInsuranceRate->stretcherLiftBaseRate = $a_insurance_rates['stretcherLiftBaseRate'];
        $objInsuranceRate->stretcherWaitingTime = $a_insurance_rates['stretcherWaitingTime'];
        $objInsuranceRate->minimumFlatRateRTStretcher = $a_insurance_rates['minimumFlatRateRTStretcher'];
        $objInsuranceRate->minimumFlatRateOWStretcher = $a_insurance_rates['minimumFlatRateOWStretcher'];
        $objInsuranceRate->stretcherNoShow = $a_insurance_rates['stretcherNoShow'];
        $objInsuranceRate->earlyPickUpS = $a_insurance_rates['earlyPickUpS'];
        $objInsuranceRate->lateDropOffS = $a_insurance_rates['lateDropOffS'];
        $objInsuranceRate->weekEndFeeS = $a_insurance_rates['weekEndFeeS'];
        $objInsuranceRate->earlyPickUpW = $a_insurance_rates['earlyPickUpW'];
        $objInsuranceRate->lateDropOffW = $a_insurance_rates['lateDropOffW'];
        $objInsuranceRate->weekendFeeW = $a_insurance_rates['weekendFeeW'];
        $objInsuranceRate->earlyPickUpA = $a_insurance_rates['earlyPickUpA'];
        $objInsuranceRate->lateDropOffA = $a_insurance_rates['lateDropOffA'];
        $objInsuranceRate->weekendFeeA = $a_insurance_rates['weekendFeeA'];
        $objInsuranceRate->stretcherAssistanceS = $a_insurance_rates['stretcherAssistanceS'];

        $objInsuranceRate->save();
    }

}