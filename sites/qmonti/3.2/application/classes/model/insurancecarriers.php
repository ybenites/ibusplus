<?php

class Model_Insurancecarriers extends Kohana_Btsorm {

    protected $_table_names_plural = true;
    protected $_table_name = 'insurancecarrier';
    protected $_primary_key = 'idInsurancecarrier';

    public function saveInsurancecarriers($a_insurance_carrier) {
        $id_insurancecarrier = $a_insurance_carrier['idInsurancecarrier'];

        if ($id_insurancecarrier > 0) {
            
            $obj_insurancecarrier = new Model_Insurancecarriers($id_insurancecarrier);
            if (!$obj_insurancecarrier->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
        }
        else {
            $obj_insurancecarrier = new Model_Insurancecarriers();
        }

        /* Ingreso los datos */
        $obj_insurancecarrier->name = $a_insurance_carrier['name'];
        $obj_insurancecarrier->contactname = $a_insurance_carrier['phone'];
        $obj_insurancecarrier->address = $a_insurance_carrier['address'];
        $obj_insurancecarrier->phone = $a_insurance_carrier['phone'];
        $obj_insurancecarrier->fax = $a_insurance_carrier['fax'];
        $obj_insurancecarrier->city = $a_insurance_carrier['city'];
        $obj_insurancecarrier->state = $a_insurance_carrier['state'];
        $obj_insurancecarrier->zipcode = $a_insurance_carrier['zipcode'];
        $obj_insurancecarrier->email = $a_insurance_carrier['email'];
        $obj_insurancecarrier->daterecord = $a_insurance_carrier['daterecord'];
        $obj_insurancecarrier->extphonecarrier = $a_insurance_carrier['extphonecarrier'];
        $obj_insurancecarrier->terms = $a_insurance_carrier['terms'];
        $obj_insurancecarrier->discount = $a_insurance_carrier['discount'];
        $obj_insurancecarrier->status = $a_insurance_carrier['status'];
        $obj_insurancecarrier->havediscount = $a_insurance_carrier['radiodiscount'];

        $obj_insurancecarrier->save();

        if (is_array($a_insurance_carrier['o_rates'])) {

            foreach ($a_insurance_carrier['o_rates'] as $s_key => $a_item) {

                $a_temp = $a_item;
                $a_temp['idInsurancecarrier'] = $obj_insurancecarrier->idInsurancecarrier;
                $a_temp['cityType'] = $s_key;
                
                $o_model_insurance_rates = new Model_InsuranceRates();
                $o_model_insurance_rates->saveInsuranceRates($a_temp);
                
            }
        }
    }

}
