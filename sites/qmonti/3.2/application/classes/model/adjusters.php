<?php
class Model_Adjusters extends Kohana_Btsorm {
   protected $_table_names_plural = true;
  protected $_table_name = 'adjusters';
  protected $_primary_key = 'idAdjuster';  

  public function createAdjuster($a_post){
      
      $id_Adjuster = $a_post['idAjustador'];
      $name = $a_post['name_adjust']; 
      $email = $a_post['adjuster_email'];
      $phone = $a_post['phone_adjuster'];
      $extentionphone = $a_post['extentionPhone_adjuster'];
      $fax = $a_post['fax_adjuster'];
      
      $idInsurance = $a_post['idInsuranceCarriers'];
       
      if (empty($id_Adjuster)) {
            
            $o_model_Adjuster = new Model_Adjusters();
            
        } else {
            
            $o_model_Adjuster = new Model_Adjusters($id_Adjuster);
            
            if (!$o_model_Adjuster->loaded())
                throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
        }
        
        $o_model_Adjuster->name = $name;
        $o_model_Adjuster->phone = $phone;
        $o_model_Adjuster->email = $email;
        $o_model_Adjuster->extentionphone = $extentionphone;
        $o_model_Adjuster->fax = $fax;
        $o_model_Adjuster->status = 1;
        $o_model_Adjuster->idInsurancecarriers = $idInsurance;

        $o_model_Adjuster->save();
        
      return $o_model_Adjuster;
      
      
  }
  
}
