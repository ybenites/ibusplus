<?php
class Model_Casemanagers extends Kohana_Btsorm {
   protected $_table_names_plural = true;
  protected $_table_name = 'casemanagers';
  protected $_primary_key = 'idCasemanager';  

  public function createCaseManager($a_post){
        
        $i_caseManager_id = $a_post['idCasemanagers'];
        $name = $a_post['name_caseM'];
        $phone = $a_post['phone_caseM'];
        $email = $a_post['manager_email'];
        $extentionphone = $a_post['extentionPhone_caseM'];
        $fax = $a_post['fax_caseM'];
        
        $idProvider= $a_post['idProvider']; 
        
        if (empty($i_caseManager_id)) {
            
            $o_model_caseManager = new Model_Casemanagers();
            
        } else {
            
            $o_model_caseManager = new Model_Casemanagers($i_caseManager_id);
            
            if (!$o_model_caseManager->loaded())
                throw new Exception(__("El registro no existe."), self::CODE_SUCCESS);
        }
        
        
        $o_model_caseManager->name = $name;
        $o_model_caseManager->phone = $phone;
        $o_model_caseManager->email = $email;
        $o_model_caseManager->extentionphone = $extentionphone;
        $o_model_caseManager->fax = $fax;
        $o_model_caseManager->status = 1;
        $o_model_caseManager->idProvider = $idProvider;
        
        $o_model_caseManager->save();
        
          return  $o_model_caseManager;
   
  }
  
  
  
  
}

