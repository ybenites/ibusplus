<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Totalappslastweek extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reporttotalappslastweek', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reporttotalappslastweek', self::FILE_TYPE_JS));
        $view_totalappslastweek = new View('reports/totalappslastweek');
        $this->template->content = $view_totalappslastweek;
    }

}
