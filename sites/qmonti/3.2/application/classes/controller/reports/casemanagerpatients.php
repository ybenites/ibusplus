<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Casemanagerpatients extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportcasemanagerpatients', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportcasemanagerpatients', self::FILE_TYPE_JS));
        
           $casemanager = DB::select(array('idCasemanager', 'idCasemanager'), array('name', 'name'))
                        ->from("casemanagers")->where('status', '=', '1')
                        ->execute()->as_array();

        $view_casemanagerpatients = new View('reports/casemanagerpatients');
        $view_casemanagerpatients->casemanager = $casemanager;
        $this->template->content = $view_casemanagerpatients;
        
    }
    
    public function action_listCaseManagerPatients() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
        $idCaseManager= $this->request->query('idCaseManager');
        $dateIni=$this->request->query('dateIni');
        $dateEnd=$this->request->query('dateEnd');
        
        if($idCaseManager!='') $s_idCaseManager=' AND cm.idCasemanager='.$idCaseManager;
        else $s_idCaseManager=' and TRUE'; 
        if($dateIni!='' && $dateEnd!='' ) $s_idCaseManager .=" AND app.appointmentdate BETWEEN '".$this->cambiafmysqlre ($dateIni)."' AND '".$this->cambiafmysqlre ($dateEnd)."'";
        else $s_idCaseManager.=' and TRUE'; 
        
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                 'idPatient' => 'p.idPatient',
                'idCaseManager' => 'cm.name',
                 'firstname' => 'p.firstname',
            'lastname' => 'p.lastname',
            'phonenumber' => 'p.phonenumber',
            'address' => "CONCAT(p.address,' ',p.city)"
           
          
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  ' patients p
                INNER JOIN claims cl
                ON p.idPatient=cl.idPatient
                INNER JOIN casemanagers cm
                ON cm.idCasemanager = cl.idCasemanager
                INNER JOIN  appointment app
                ON cl.idClaim = app.idClaim';

            $s_where_conditions = ' cm.status = 1
                AND cl.status = 1 
                AND p.status =1
                AND
                CASE
                    WHEN app.idInterpreter>0 AND app.idDriverOneWay > 0
                        THEN ((app.billinginterp= 0 OR ISNULL(app.billinginterp)) AND (app.cancelinterpreter = 0 OR ISNULL(app.cancelinterpreter)))
                            OR ((app.billingtransp = 0 OR ISNULL(app.billingtransp)) AND (app.canceldriver = 0 OR ISNULL(app.canceldriver)))
                    WHEN app.idInterpreter>0
                        THEN (app.cancelinterpreter = 0 OR ISNULL(app.cancelinterpreter))
                    WHEN app.idDriverOneWay > 0
                        THEN (app.canceldriver = 0 OR ISNULL(app.canceldriver))
                    ELSE TRUE
                END ' . $s_idCaseManager. ' GROUP BY p.idPatient ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
     
    }
    
    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }
}
