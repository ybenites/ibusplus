<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Agingreport extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportagingreport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportagingreport', self::FILE_TYPE_JS));
        $view_agingreport = new View('reports/agingreport');
        $this->template->content = $view_agingreport;
    }

    public function action_listReportNoPayment(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
       
        $dateIni=$this->request->query('dateIni');
        $dateEnd=$this->request->query('dateEnd');
        
        if($dateEnd!='' AND $dateIni!='') $s_Fechas =" AND bi.dateBilling BETWEEN DATE_SUB( CURDATE(), INTERVAL " . $dateEnd . " DAY ) AND DATE_SUB( CURDATE(), INTERVAL " . $dateIni . " DAY )";
        else $s_Fechas=" AND  bi.dateBilling < DATE_SUB( CURDATE(), INTERVAL " . $dateEnd . " DAY )";
        
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                 "idOut" => "bi.idBilling",
                "insuranceOut" => "inc.name",
                "typeOut" => "CASE WHEN bi.typebilling = 1 THEN 'Transportation' ELSE 'Translation' END",
                "patientOut" => "CONCAT(pat.firstName, ' ', pat.lastName)",
                "dateOut" => "DATE_FORMAT(bi.dateBilling, '%m/%d/%Y')",
                "amountOut" => "CAST(bi.amount AS DECIMAL(10,2))",
                "paidOut" => "CAST(SUM(COALESCE(bc.amount,0)) AS DECIMAL(10,2))",
                "paidPartialOut" => "IF(SUM(COALESCE(bc.amount,0))> 0, 1,0)"
           
          
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  ' billing bi
            INNER JOIN insurancecarrier inc
                ON bi.idInsuranceCarriers = inc.idInsurancecarrier
            INNER JOIN patients pat
                ON bi.idPatients = pat.idPatient
            LEFT JOIN billingchecks bc
                ON bi.idBilling = bc.idBilling AND bc.state>0 ';

            $s_where_conditions = ' bi.status > 0
                AND
                bi.lostPayment = 0
                 ' . $s_Fechas. ' GROUP BY bi.idBilling,inc.name  ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $s_where_conditions.=' HAVING amountOut > paidOut ';
          
           $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);
  print_r(Database::instance()->last_query);
        die();
           
            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }
    
    public function action_printReportBilling(){
        
        $this->auto_render = FALSE;
        try {


            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            
            $idBilling = $this->request->query('idBilling');
            $stype=$this->request->query('type');
             if (isset($idBilling)) {


            $objBilling = new Model_Billing($idBilling);
           
            $arrayDetails = array();

            if ($objBilling->status >= 1) {

                if ($objBilling->typebilling == 1) {
                    $federalId = '26-0347129';
                } elseif ($objBilling->typebilling == 2) {
                    $federalId = '59-3578799';
                }

//                $aResultadoPagos = new Controller_Billings_Postingchecks();
//                $aResultadoPagos->action_getDescuentoAndBalanceById();

                $columns = array(array('ba.idBillingAppointments', 'idBillApp'),
                
                array('ba.countEdit','edition'), array('ba.idAppointment','idApp'),
               
            );
            
           
            $a_billingApp = DB::select_array($columns)
                    ->from(array("billingappointments", 'ba'))->where('idBilling', '=', $idBilling)
                    ->and_where('ba.estado', '=', '1')
                    ->execute()->as_array();
                
                foreach ($a_billingApp as $billingApps) {
                   

                        $objAppointment = new Model_Appointment($billingApps['idApp']);
                        $objClaim = new Model_Claims($objAppointment->idClaim);
                        $objAdjuster = new Model_Adjusters($objClaim->idAdjuster);
                        $objInsuranceCarrier = new Model_Insurancecarriers($objClaim->idInsuranceCarrier);
                        //Chanke el ultimo total es el mismo app
                        $objPatient = new Model_Patients($objAppointment->idPatient);
                        $apartment = ($objPatient->apartmentnumber != '0' AND $objPatient->apartmentnumber != 'n/a') ? ' ' . $objPatient->apartmentnumber . ' ' : '';
                        $dateRegister = date_create_from_format('Y-m-d', $objBilling->dateBilling);
                        $insuranceZipcode = $objInsuranceCarrier->zipcode != 0 ? ' ' . $objInsuranceCarrier->zipcode : '';
                        $patientZipcode = $objPatient->zipcode != 0 ? ' ' . $objPatient->zipcode : '';
                        $response = array(
                            'idBilling' => $idBilling,
                            'federalId' => $federalId,
                            'InsuranceName' => $objInsuranceCarrier->name,
                            'InsuranceAddress' => $objInsuranceCarrier->address
                            . '{br}' . $objInsuranceCarrier->city
                            . ', ' . $objInsuranceCarrier->state
                            . $insuranceZipcode,
                            'InsuranceTerms' => $objInsuranceCarrier->terms,
                            'AdjusterName' => $objAdjuster->name,
                            'PatientName' => $objPatient->firstname . ' ' . $objPatient->lastname,
                            'PatientAddress' =>
                            $objPatient->address . $apartment .
                            '{br}' . $objPatient->city . ', ' . $objPatient->state . $patientZipcode,
                            'ClaimNumber' => $objClaim->claimnumber,
                            'SocialSecurity' => $objPatient->socialsecurity,
                            'dateRegister' => date_format($dateRegister, 'm/d/Y'),
                            'result' => 0
                        );



                       
                        $details = DB::select_array(array(array('bd.detailType', 'detailType'),
                
                array('bd.title','title'),
                array('bd.qty','qty'),
                array('bd.ratio','ratio'),
                array('bd.amount','amount'),
                array('bd.idBillingDetails','id'),
               
            ))
                    ->from(array("billingdetails", 'bd'))->where('idBillingAppointments', '=', $billingApps['idBillApp'])
                    ->and_where('bd.estado', '=', '1')
                    ->execute()->as_array();

                        foreach ($details as $objBillingDetails) {
                            $explodeTitle = explode('|', $objBillingDetails['title']);

                            switch ($objBillingDetails['detailType']) {
                                case 'driverNormal':
                                    $title = $explodeTitle[0];
                                    $title .= '{br}From: ' . $explodeTitle[1] . ', ' . $explodeTitle[2];
                                    $title .= '{br}To: ' . $explodeTitle[3] . ', ' . $explodeTitle[4];
                                    break;
                                case 'interNormal':
                                    $title = $explodeTitle[0] . ' (' . $explodeTitle[1] . ')';
                                    $title .= '{br}' . $explodeTitle[2] . ', ' . $explodeTitle[3];
                                    break;
                                case 'driverNoShow':
                                  
                                    $objCourse = new Model_Course($objAppointment->idCourse);
                                    $objLocationFrom = new Model_Locations($objCourse->idLocationOrigen);
                                    $objLocationTo = new Model_Locations($objCourse->idLocationDestination);

                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1];
                                    $title .= '{br}From: ' . $objLocationFrom->alias . ', ' . $objLocationFrom->address;
                                    $title .= '{br}To: ' . $objLocationTo->alias . ', ' . $objLocationTo->address;
                                    break;
                                case 'interNormalDocTransWords':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                                    break;
                                case 'interNormalDocTransPages':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                                    break;
                                case 'intNoShow':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . ' Interpreter (' . $explodeTitle[2] . ')';
                             

                                    if ($objAppointment->idLocation > 0) {
                                        $objLocation= new Model_Locations($objAppointment->idLocation);
                                        $title .= '{br}Contact: ' . $objLocation->alias . ', ' . $objLocation->address;
                                    }
                                    break;
                                default:
                                    $title = $explodeTitle[1];
                                    break;
                            }

                            $appDate = $this->cambiafmysqlre($objAppointment->appointmentdate);
                            $appTime = $objAppointment->appointmenttime;
                           

                            if (
                                    ( strpos($objBillingDetails['detailType'], 'Normal') == 0 OR
                                    strpos($objBillingDetails['detailType'], 'Normal') == false OR
                                    is_null(strpos($objBillingDetails['detailType'], 'Normal')) ) AND
                                    ( strpos($objBillingDetails['detailType'], 'NoShow') == 0 OR
                                    strpos($objBillingDetails['detailType'], 'NoShow') == false OR
                                    is_null(strpos($objBillingDetails['detailType'], 'NoShow')) )
                            ) {

                                $appDate = null;
                                $appTime = null;
                            }

                            array_push($arrayDetails, array(
                                "idBillingDetails" => $objBillingDetails['id'],
                               
                                'appDate' => $appDate,
                                'appTime' => $appTime,
                                'title' => $title,
                                'qty' => ($objBillingDetails['qty']) * 1,
                                'ratio' => $objBillingDetails['ratio'],
                                'amount' => $objBillingDetails['amount'],
                                'detailType' => $objBillingDetails['detailType']
                                    )
                            );
                        }
                        
                        
                    $a_comment= DB::select_array(array(array('bc.idBillingComments', 'idBillCom'),array('bc.comment', 'comment')))
                    ->from(array("billingcomments", 'bc'))->where('idBillingAppointments', '=', $billingApps['idBillApp'])
                    ->and_where('bc.estado', '=', '1')
                    ->execute()->as_array();
                 foreach($a_comment as $comment){               
                if ($comment['idBillCom'] > 0) {
                            $title = 'Notes: ' . $comment['comment'];
                            array_push($arrayDetails, array(
                                "idBillingDetails" => null,
                                
                                'appDate' => null,
                                'appTime' => null,
                                'title' => $title,
                                'qty' => null,
                                'ratio' => null,
                                'amount' => null,
                                'detailType' => null
                                    )
                            );
                        }
                    }
                }
                if (isset($stype)) {
                    if ( $this->request->query('type') == '30') {
                        $response['type'] = '30 days';
                    } else if ( $this->request->query('type') == '60') {
                        $response['type'] = '60 days';
                    } else if ( $this->request->query('type') == '90') {
                        $response['type'] = '90 days';
                    } else if ( $this->request->query('type') == '120') {
                        $response['type'] = '120 days';
                    } else if ( $this->request->query('type') == 'more') {
                        $response['type'] = 'More 120 days';
                    }
                } else {
                    $response['type'] = '';
                }
            }

            $response['lostPayment'] = $objBilling->lostPayment;
            $response['result'] = $arrayDetails;
//            $response['monto'] = $aResultadoPagos['monto'];
//            $response['paidOut'] = $aResultadoPagos['paidOut'];
//            $response['discountInvoice'] = $aResultadoPagos['discountInvoice'];
//            $response['percentInvoice'] = $aResultadoPagos['percentInvoice'];
//            $response['discountInsurance'] = $aResultadoPagos['discountInsurance'];
//            $response['percentInsurance'] = $aResultadoPagos['percentInsurance'];
//            $response['balanceOut'] = $aResultadoPagos['balanceOut'];

            $this->fnResponseFormat($response, self::REPORT_RESPONSE_TYPE_XML, $data);
             }
             
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        
    }
    
    public function action_printReportAllBilling(){
        
        $this->auto_render = FALSE;
        try {


            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            
         
            $stype=$this->request->query('type');
            
             $response['data'] = DB::select_array(
                    array(
                        array('bi.idBilling', 'idOut'),
                        array('inc.name', 'insuranceOut'),
                        DB::expr('CASE WHEN bi.typebilling = 1 THEN "Transportation" ELSE "Translation" END  as typeOut'),
                        DB::expr('CONCAT(pat.firstName, " ", pat.lastName) as patientOut'),
                        DB::expr('DATE_FORMAT(bi.dateBilling, "%m/%d/%Y") as dateOut'),
                        DB::expr('CAST(bi.amount AS DECIMAL(10,2)) as amountOut'),
                        DB::expr('CAST(SUM(COALESCE(bc.amount,0)) AS DECIMAL(10,2)) as paidOut'),
                        DB::expr('IF(SUM(COALESCE(bc.amount,0))> 0, 1,0) as paidPartialOut')
                        )
                    )
                    ->from(array("billing", 'bi'))
                    ->join(array('insurancecarrier','inc'))->on('bi.idInsuranceCarriers','=','inc.idInsurancecarrier')
                    ->join(array('patients','pat'))->on('bi.idPatients','=','pat.idPatient')
                    ->join(array('billingchecks','bc'),'LEFT')->on('bi.idBilling','=','bc.idBilling')
                    ->where('bc.state', '>', '0')
                    ->and_where('bi.status', '>', '0')
                    ->and_where('bi.lostPayment', '=', '0');
                    
             
             
            if (isset($stype)) {
                    if ( $this->request->query('type') == '30') {
                        $response['type'] = '30 days';
                        $dateIni=0;
                        $dateEnd=30;
                    } else if ( $this->request->query('type') == '60') {
                        $response['type'] = '60 days';
                        $dateIni=31;
                        $dateEnd=60;
                    } else if ( $this->request->query('type') == '90') {
                        $response['type'] = '90 days';
                        $dateIni=61;
                        $dateEnd=90;
                    } else if ( $this->request->query('type') == '120') {
                        $response['type'] = '120 days';
                        $dateIni=91;
                        $dateEnd=120;
                    } else if ( $this->request->query('type') == '121') {
                        $response['type'] = 'More 120 days';
                        $dateEnd=121;
                    }
                    if($dateIni!='' && $dateEnd!='')
                    $response['data']=$response['data']->and_where('bi.dateBilling','BETWEEN',array(DB::expr('DATE_SUB( CURDATE(), INTERVAL ' . $dateEnd . ' DAY )'),DB::expr('DATE_SUB( CURDATE(), INTERVAL ' . $dateIni . ' DAY )')));
                    else 
                    $response['data']=$response['data']->and_where('bi.dateBilling','>',DB::expr('DATE_SUB( CURDATE(), INTERVAL ' . $dateEnd . ' DAY )') );

                } else {
                    $response['type'] = '';
                }                    
    
                $response['data']=$response['data']->group_by('bi.idBilling')
                    ->group_by('inc.name')
                    ->having('amountOut','>',DB::expr('paidOut'))
                    ->execute()->as_array();
                  print_r(Database::instance()->last_query);
        die();
            $this->fnResponseFormat($response, self::REPORT_RESPONSE_TYPE_XML, $data);
 
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        
    }
    
    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }
}