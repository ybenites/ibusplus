<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Interpreters extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportinterpreters', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportinterpreters', self::FILE_TYPE_JS));
        $view_interpreters = new View('reports/interpreters');
        $this->template->content = $view_interpreters;
    }
public function action_listReportInterpreter(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
        
        $dateIni=$this->request->query('dateIni');
        $dateEnd=$this->request->query('dateEnd');
        
       $swhere='';
         if (isset($dateEnd) AND isset($dateIni)) {
            if ($dateEnd != '' AND $dateIni != '')
                $swhere = ' AND i.creationDate BETWEEN "' . $this->cambiafmysqlre($dateIni) . '" AND "' . $this->cambiafmysqlre($dateEnd) . '"';
        }
        
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                
                 'idInter' => 'i.idInterpreter',
                 'letter' => 'UPPER(SUBSTRING(i.name,1,1))',
                'dname' => "CONCAT(i.name,' ',i.lastname)",
                 'dlanguage' => 'l.name',
            'daddress' => "CONCAT(i.address,', ',i.city,', ',i.state,', ',i.zipcode)",
            'dphone' => 'i.phone',
            'dfax' => "i.fax",
            'dcell' => "i.cellphone",
            'demail' => "i.email",
            'dnotes' => "i.interpreternotes",
            'ddateRecord' => "DATE_FORMAT(i.creationDate,'%m/%d/%Y')"

            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  '  interpreters i
            INNER JOIN detailinterpreterslanguages d
            ON i.idInterpreter = d.idInterpreter
            INNER JOIN languages l
            ON l.idLanguage = d.idLanguage';

            $s_where_conditions = ' i.status = 1 
               AND d.status=1 AND l.status ' . $swhere. ' ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
          
          
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        
           
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
        
    }
    public function action_reportInterpreter(){
         $this->auto_render = FALSE;
        try {
            $data = array();
            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            
         
            $dateIni=$this->request->query('dateIni');
            $dateEnd=$this->request->query('dateEnd');

            $data['from'] = $dateIni;
            $data['to'] = $dateEnd;
                        
            $cols = array(
                        array('i.idInterpreter', 'idInter'),
                        DB::expr('UPPER(SUBSTRING(i.name,1,1)) as letter'),
                        DB::expr("CONCAT(i.name,' ',i.lastname) as dname"),
                        array('l.name', 'dlanguage'),
                        DB::expr("CONCAT(i.address,', ',i.city,', ',i.state,', ',i.zipcode) as daddress"),
                        array('i.phone', 'dphone'),
                        array('i.fax', 'dfax'),
                        array('i.cellphone', 'dcell'),
                        array('i.email', 'demail'),
                        array('i.interpreternotes', 'dnotes'),
                        DB::expr("DATE_FORMAT(i.creationDate,'%m/%d/%Y') as ddateRecord")
                        );
            $sql_interpreters = DB::select_array($cols)        
                    ->from(array("interpreters", 'i'))
                    ->join(array('detailinterpreterslanguages','d'))->on('i.idInterpreter','=','d.idInterpreter')
                    ->join(array('languages','l'))->on('l.idLanguage','=','d.idLanguage')
                    ->where('i.status', '=', '1')
                    ->and_where('d.status', '=', '1')
                    ->and_where('l.status', '=', '1');
                    
            if ($dateIni!='' and $dateEnd!='') {
             $sql_interpreters->and_where('i.creationDate','BETWEEN',array($this->cambiafmysqlre($dateIni),$this->cambiafmysqlre($dateEnd)));
            }    
                
           $sql = $sql_interpreters->group_by('i.idInterpreter')->execute()->as_array();
 
            $this->fnResponseFormat($sql, self::REPORT_RESPONSE_TYPE_XML, $data);
 
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }
    
     public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }
}
