<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Underpaymentsreports extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportunderpaymentsreports', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportunderpaymentsreports', self::FILE_TYPE_JS));

        $insurance = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();

        $view_underpaymentsreports = new View('reports/underpaymentsreports');
        $view_underpaymentsreports->insurance = $insurance;
        $this->template->content = $view_underpaymentsreports;
    }

    public function action_listPayments() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
        
        $dateIni=$this->request->query('dateIni');
        $dateEnd=$this->request->query('dateEnd');
        $idInsurance=$this->request->query('idInsurance');
        
       $swhere='';
         if (isset($dateEnd) AND isset($dateIni)) {
            if ($dateEnd != '' AND $dateIni != '')
                $swhere = ' AND bi.dateBilling BETWEEN "' . $this->cambiafmysqlre($dateIni) . '" AND "' . $this->cambiafmysqlre($dateEnd) . '"';
        }
         if (isset($idInsurance)) {
            if ($idInsurance != '' )
                $swhere = ' AND ic.idInsurancecarrier = "' . $idInsurance. '"';
        }
        
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                
               'id' => 'bi.idBilling',
                'insurance_name' => "ic.name",
            'patient_name' => "CONCAT(pa.firstname, ' ', pa.lastname)",
            'billing_id' => 'bi.idBilling',
            'billing_date' => "DATE_FORMAT(bi.dateBilling, '%m/%d/%Y')",
            'billing_type' => "CASE 
                                            WHEN bi.typebilling = 1 THEN 'Transportation' 
                                            WHEN bi.typebilling = 2 THEN 'Translation'
                                            ELSE bi.typebilling 
                                    END",
            'billing_amount' => 'CAST(bi.amount AS DECIMAL(10,2))',
            'billing_paid' => 'SUM(bc.amount)'
            

            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  '  billing bi
                                
                                
                                INNER JOIN billingchecks bc
				 ON bc.idBilling = bi.idBilling
                                INNER JOIN insurancecarrier ic
                                ON bi.idInsuranceCarriers = ic.idInsurancecarrier
                                INNER JOIN patients pa
                                ON bi.idPatients = pa.idPatient ';

            $s_where_conditions = ' COALESCE(bi.lostPayment, 0 ) = 0
               ' . $swhere. ' ';
         

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
           $s_where_conditions .= ' GROUP BY insurance_name, patient_name, billing_id, billing_date, billing_type, billing_amount ';
    
          $s_where_conditions .= ' HAVING billing_amount > billing_paid ';
            
            
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
        
        
    }
    
     public function action_listChecks() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $idBilling=$this->request->query('billing');
  
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
      
                'billing_check_id' => "idBillingChecks",
            'check_number' => "ch.numbercheck",
            'check_memo' => 'ch.memo',
            'check_date' => "DATE_FORMAT(ch.datecheck, '%m/%d/%Y')",
            'check_available' => "ch.available",
            'billing_paid' => 'bc.amount'

            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  '  billingchecks bc
                    INNER JOIN checks ch
                        ON bc.idChecks = ch.idCheck ';

            $s_where_conditions = ' bc.state >0 AND ch.state > 0 AND  bc.idBilling ='. $idBilling.'';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                 
            
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
        
        
    }

}
