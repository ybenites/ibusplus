<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Accountspayable extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportaccountspayable', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportaccountspayable', self::FILE_TYPE_JS));
        $view_accountspayable = new View('reports/accountspayable');
        $this->template->content = $view_accountspayable;
    }

}