<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Report_Confirmassigment extends Controller_Private_Admin implements Monti_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('confofassignment', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('confofassignment', self::FILE_TYPE_JS));
        $view_report_confirmassigment = new View('patients/confofassignment');

        $this->template->content = $view_report_confirmassigment;
    }

    public function action_reportConfofassignment() {

        try {
            $this->auto_render = FALSE;
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $appointment = array();

            $appointment['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $appointment['qlang'] = $this->request->query(self::RQ_LANG);
            $appointment['searchDate'] = DateHelper::getFechaFormateadaActual();
            $appointment['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $id_appointment = $this->request->query('idAppointment');

            $parameters = array(
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':appointment' => $id_appointment,
                ':interpreting' => self::INTERPRETING,
                ':conferencecall' => self::CONFERENCECALL,
                ':documentaltranslation' => self::DOCUMENTTRANSLATION,
                ':ambulatory' => self::AMBULATORY,
                ':whellchair' => self::WHEELCHAIR,
                ':stretcher' => self::STRETCHER,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE
            );


            $columns = array(
                DB::expr("CONCAT(pa.firstname, ' ', pa.lastname) AS patient"),
                array('cla.claimnumber', 'claimNumber'),
                DB::expr("DATE_FORMAT(ap.appointmentdate,:date_format) AS appointmentDate"),
                DB::expr("DATE_FORMAT(ap.appointmenttime,:time_format) AS appointmentTime"),
                array('adj.name', 'adjusterName'),
                array('course.address', 'addressCourse'),
                array('course.telephone', 'telephoneCourse'),
                array('course.typeservice', 'typeServiceCourse'),
                array('course.name', 'languageCourse'),
                array('locations.address', 'addressLocation'),
                array('locations.telephone', 'telephoneLocation'),
                array('locations.typeservicesinterpreter', 'typeServicesInterpreter'),
                DB::expr("IF(ap.idCourse IS NOT NULL, :s_active, :s_inactive) AS isCourse"),
                DB::expr("IF(ap.idLocation IS NOT NULL, :s_active, :s_inactive) AS isLocation"),
                DB::expr("IF((ap.idDriverRoundTrip OR ap.idDriverOneWay) IS NOT NULL AND ap.idLanguage IS NOT NULL,:s_active, :s_inactive) AS isDriverLanguage"),
                DB::expr("IF((ap.idDriverRoundTrip OR ap.idDriverOneWay) IS NOT NULL AND ap.idLanguage IS NOT NULL AND ap.typeservicesinterpreter <> ':interpreting', :s_active, :s_inactive) AS isTypeServicesInterpreter")
            );

            $sql_sub_course = Db::select('locations.idLocation', 'locations.address', 'locations.telephone', 'appointment.typeservice', 'languages.name')
                    ->from('appointment')
                    ->join('course')->on('course.idCourse', '=', 'appointment.idCourse')
                    ->join('locations')->on('locations.idLocation', '=', 'course.idLocationDestination')
                    ->join('languages')->on('languages.idLanguage', '=', 'appointment.idLanguage')
                    ->where('appointment.idAppointment', '=', $id_appointment)
                    ->and_where_open()
                    ->or_where('appointment.idDriverRoundTrip', 'IS NOT', NULL)
                    ->or_where('appointment.idDriverOneWay', 'IS NOT', NULL)
                    ->and_where_close();

            $sql_sub_location = DB::select(
                            'locations.idLocation', 'locations.address', 'locations.telephone', 'appointment.typeservicesinterpreter'
                    )
                    ->from('appointment')
                    ->join('locations')->on('locations.idLocation', '=', 'appointment.idLocation')
                    ->where('appointment.idAppointment', '=', $id_appointment)
                    ->and_where_open()
                    ->and_where('appointment.idDriverRoundTrip', 'IS', NULL)
                    ->and_where('appointment.idDriverOneWay', 'IS', NULL)
                    ->and_where_close()
                    ->and_where('appointment.typeservicesinterpreter', '=', ':interpreting')
                    ->and_where('appointment.idLanguage', 'IS NOT', NULL)
                    ->parameters($parameters);


            $sql_appointments = DB::select_array($columns)
                            ->from(array('appointment', 'ap'))
                            ->join(array('patients', 'pa'))->on('pa.idPatient', '=', 'ap.idPatient')
                            ->join(array('claims', 'cla'))->on('cla.idClaim', '=', 'ap.idClaim')
                            ->join(array('adjusters', 'adj'))->on('adj.idAdjuster', '=', 'cla.idAdjuster')
                            ->join(array('course', 'cou'), 'LEFT')->on('cou.idCourse', '=', 'ap.idCourse')
                            ->join(array($sql_sub_course, 'course'), 'LEFT')->on('course.idLocation', '=', 'cou.idLocationDestination')
                            ->join(array('locations', 'loc'), 'LEFT')->on('loc.idLocation', '=', 'ap.idLocation')
                            ->join(array($sql_sub_location, 'locations'), 'LEFT')->on('locations.idLocation', '=', 'loc.idLocation')
                            ->where('ap.idAppointment', '=', $id_appointment)
                            ->parameters($parameters)
                            ->execute()->as_array();

            $this->fnResponseFormat($sql_appointments, self::REPORT_RESPONSE_TYPE_XML, $appointment);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}

?>
