<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Driversadmin extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportdriversadmin', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportdriversadmin', self::FILE_TYPE_JS));
        $view_driversadmin = new View('reports/driversadmin');
        $this->template->content = $view_driversadmin;
    }
public function action_listReportAdminDriver(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
        
        $dateIni=$this->request->query('dateIni');
        $dateEnd=$this->request->query('dateEnd');
        
       $swhere='';
         if (isset($dateEnd) AND isset($dateIni)) {
            if ($dateEnd != '' AND $dateIni != '')
                $swhere = ' AND d.creationDate BETWEEN "' . $this->cambiafmysqlre($dateIni) . '" AND "' . $this->cambiafmysqlre($dateEnd) . '"';
        }
        
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(


                 'idDriver' => 'd.idDriver',
                 'letter' => 'UPPER(SUBSTRING(d.name,1,1))',
                'name' => "CONCAT(d.name,' ',d.lastName)",
                'area' => "a.location",
                 'regdate' => "DATE_FORMAT(d.creationDate,'%m/%d/%Y')",
            'amb' => "FORMAT(d.ambulatoryRate, 2)",
            'wh' => "FORMAT(d.wheelchairRate, 2)",
            'st' => "FORMAT(d.scretcherRate, 2)",
            'liftWc' => "FORMAT(d.wheelchairLiftBase, 2)",
            'liftSt' => "FORMAT(d.stretcherLiftBase, 2)",
            'mrtMiles' => "FORMAT(0, 2)",
            'mrtRate' => "FORMAT(0, 2)",
            'nsRate' => "FORMAT(0, 2)",
            'waitTime' => "FORMAT(0, 2)"
            
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join =  ' drivers d
            INNER JOIN areasdriver a
            ON d.idAreasdriver = a.idAreasdriver';

            $s_where_conditions = ' d.status = 1  ' . $swhere. ' ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
          
          
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
        
    }
    public function action_reportAdminDriver(){
         $this->auto_render = FALSE;
        try {

            $data = array();
            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            
            $dateIni=$this->request->query('dateIni');
            $dateEnd=$this->request->query('dateEnd');
            
            $data['from'] = $dateIni;
            $data['to'] = $dateEnd;

            $cols= array(
                        array('d.idDriver', 'idDriver'),
                        DB::expr('UPPER(SUBSTRING(d.name,1,1)) as letter'),
                        DB::expr("CONCAT(d.name,' ',d.lastName) as name"),
                        array('a.location', 'area'),
                        DB::expr("DATE_FORMAT(d.creationDate,'%m/%d/%Y') as dateReg"),
                        DB::expr("FORMAT(d.ambulatoryRate, 2) as amb"),
                        DB::expr("FORMAT(d.wheelchairRate, 2) as wh"),
                        DB::expr("FORMAT(d.scretcherRate, 2) as sc"),
                        DB::expr("FORMAT(d.wheelchairLiftBase, 2) as liftWc"),
                        DB::expr("FORMAT(d.stretcherLiftBase, 2) as liftSt"),
                        DB::expr("FORMAT(0, 2) as mrtMiles"),
                        DB::expr("FORMAT(0, 2) as mrtRate"),
                        DB::expr("FORMAT(0, 2) as nsRate"),
                        DB::expr("FORMAT(0, 2) as waitTime")
                       
                        );
          $sql_driversAdmin = DB::select_array($cols)
                    ->from(array("drivers", 'd'))
                    ->join(array('areasdriver','a'))->on('d.idAreasdriver','=','a.idAreasdriver')
                    ->where('d.status', '=', '1');
                    
            if ($dateIni!='' and $dateEnd!='') {
                  $sql_driversAdmin->and_where('d.creationDate','BETWEEN',array($this->cambiafmysqlre($dateIni),$this->cambiafmysqlre($dateEnd)));
                
            }    
                
           $sql = $sql_driversAdmin->group_by('d.idDriver')->execute()->as_array();

            $this->fnResponseFormat($sql, self::REPORT_RESPONSE_TYPE_XML, $data);
 
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }
    
     public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }
}