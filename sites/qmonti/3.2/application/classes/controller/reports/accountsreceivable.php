<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Reports_Accountsreceivable extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('reportaccountsreceibable', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('reportaccountsreceivable', self::FILE_TYPE_JS));
        $view_accountsreceivable = new View('reports/accountsreceivable');
        $this->template->content = $view_accountsreceivable;
    }

}