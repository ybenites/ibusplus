<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Adjusters extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('adjusters', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('adjusters', self::FILE_TYPE_JS));
        $view_adjusters = new View('management/adjusters');

        $insuranceCarrier = DB::select('idInsurancecarrier', 'name')
                        ->from('insurancecarrier')
                        ->execute()->as_array();

        $view_adjusters->insuranceCarrier = $insuranceCarrier;
        $this->template->content = $view_adjusters;
    }

    public function action_listAdjusters() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');

            $s_all = $this->request->post('status');

            if (!$sidx)
                $sidx = 1;
            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));
//            echo $page;
//            die();           
            $array_cols = array(
                'idAdjuster' => 'adj.idAdjuster',
                'adname' => 'adj.name',
                'idInsuranceCarrier' => 'ins.idInsuranceCarrier',
                'insname' => 'ins.name',
                //'phone' => 'adj.phone',
                //'email' => 'adj.email',
                'status' => 'adj.status'
                    //'idInsuranceCarriers' => 'ins.name',
                    //'fax' => 'adj.fax',
                    //'extentionPhone' => 'adj.extentionPhone',
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr, $array_cols);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = 'adjusters adj INNER JOIN insurancecarrier ins ON adj.idInsuranceCarriers = ins.idInsuranceCarrier';
            
            $where_conditions = "TRUE";
            
            if ($s_all == 'false')
                $where_conditions = " adj.status = " . self::STATUS_ACTIVE;

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $adjusters = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $adjusters, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);

//            echo Database::instance()->last_query;
//            die();
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
    }

    public function action_getAdjusters() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Adjuster = new Model_Adjusters($post_data['id']);
                $a_response['data'] = $obj_Adjuster->as_array();
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

    public function action_createUpdateAdjuster() {
       $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $a_post = Validation::factory($this->request->post())
                    ->rule('name_adjust', 'not_empty');
            
           
            if (!$a_post->check()) {
            
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } 
            
            else {
            
                  $o_model_Adjuster = new Model_Adjusters();
                  $rpta = $o_model_Adjuster->createAdjuster($a_post); 
                  $rpta->reload(); 
                   $a_response['data'] = array(
                    'idAdjuster'=> $rpta->idAdjuster
                    , 'name'=> $rpta->name
                );
              
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_deleteAdjuster(){
     $a_response = $this->json_array_return;
     $post = Validation::factory($this->request->post())
             ->rule('id','not_empty');
     if(!$post->check())
     {
        throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
     }else{
       try {
           $post_data = $post->data();
           $obj_Adjuster = new Model_Adjusters($post_data['id']);
           $obj_Adjuster->status = self::STATUS_DEACTIVE;
           $obj_Adjuster->save();
           
           $a_response['data'] = 'OK';
           
       } catch (Exception $exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $exc->getMessage();
       }

       $this->fnResponseFormat($a_response);        
     }
       
       
   }

    public function action_activeAdjuster() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Adjuster = new Model_Adjusters($post_data['id']);
                $obj_Adjuster->status = self::STATUS_ACTIVE;
                $obj_Adjuster->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }
    
    public function action_saveInsurances() {
       $this->auto_render = FALSE;
       $a_response = $this->json_array_return;
       //die('adsfdgSADFS');
       try {
           $post = Validation::factory($this->request->post())
                   ->rule('name-insu', 'not_empty');
           if (!$post->check()) {
               throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
           } else {
               $post_data = $post->data();
               $obj_insurancecarrier = new Model_Insurancecarriers();
               $obj_insurancecarrier->name = $post_data['name-insu'];
               $obj_insurancecarrier->contactname = $post_data['contactname-insu'];
               $obj_insurancecarrier->address = $post_data['address-insu'];
               $obj_insurancecarrier->phone = $post_data['phone-insu'];
               $obj_insurancecarrier->fax = $post_data['fax-insu'];
               $obj_insurancecarrier->city = $post_data['city-insu'];
               $obj_insurancecarrier->zipcode = $post_data['zipcode-insu'];
               $obj_insurancecarrier->state = $post_data['state-insu'];
               $obj_insurancecarrier->email = $post_data['email-insu'];
               $obj_insurancecarrier->status = self::STATUS_ACTIVE;         
               $obj_insurancecarrier->save();
               

               $a_response['data'] = array(
                   
                   'idInsurancecarriers'=> $obj_insurancecarrier->idInsurancecarrier,
                   'name'=>$obj_insurancecarrier->name,
                   
               );
           }
       } catch (Exception $e_exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $this->errorHandling($e_exc);
       }
       $this->fnResponseFormat($a_response);
   }
    

}