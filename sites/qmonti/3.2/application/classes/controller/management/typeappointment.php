<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Typeappointment extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('typeappointment', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('typeappointment', self::FILE_TYPE_JS));
        $view_typeappointment = new View('management/typeappointment');
        $this->template->content = $view_typeappointment;
    }
     public function action_listTypeappointment(){
        
        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->query('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idTypeappointment' => "typ.idTypeappointment",
                'name' => "typ.name",
                'type'=>"typ.type",
                'status'=>"typ.status",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "typeappointment typ";

            $s_where_conditions = " TRUE ";
            if($s_all == 'false')
                $s_where_conditions = " typ.status = " . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }
    
     public function action_getTypeappointment(){
      $a_response = $this->json_array_return;
      $post = Validation::factory($this->request->post())
              ->rule('id','not_empty');
      if(!$post->check())
      {
         throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
      }else{
        try {
            $post_data = $post->data();
            $obj_Typeappointment = new Model_Typeappointment($post_data['id']);
            $a_response['data'] = $obj_Typeappointment->as_array();
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);        
      }
        
    }
    
     public function action_createUpdateTypeappointment(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post_data = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty')
                    ->rule('type','not_empty'); 
           
            if (!$post_data->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } 
            
                else {
                  
                  $o_model_Typeappointment = new Model_Typeappointment();
                  $rpta = $o_model_Typeappointment->createTypeappointment($post_data); 
                  $rpta->reload(); 
                  
                   $a_response['data'] = array(
                    'idTypeappointment'=> $rpta->idTypeappointment
                    , 'name'=> $rpta->name
                );
            }
        }
             catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
            
        $this->fnResponseFormat($a_response);
    }
    
    public function action_deleteTypeappointment(){
      $a_response = $this->json_array_return;
      $post = Validation::factory($this->request->post())
              ->rule('id','not_empty');
      if(!$post->check())
      {
         throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
      }else{
        try {
            $post_data = $post->data();
            $obj_Typeappointment = new Model_Typeappointment($post_data['id']);
            $obj_Typeappointment->status = self::STATUS_DEACTIVE;
            $obj_Typeappointment->save();
            
            $a_response['data'] = 'OK';
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);        
      }
        
        
    }
    
    
    
    public function action_activeTypeappointment() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        
        $post = Validation::factory($this->request->post())
                ->rule('idTypeappointment', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Typeappointment = new Model_Typeappointment($post_data['idTypeappointment']);
                $obj_Typeappointment->status = self::STATUS_ACTIVE;
                $obj_Typeappointment->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

}
?>