<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Insurancecarrier extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('insurancecarrier', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('insurancecarrier', self::FILE_TYPE_JS));
        $view_insurancecarrier = new View('management/insurancecarrier');
        $this->template->content = $view_insurancecarrier;
    }

    public function action_createUpdateInsurance() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty')
                    ->rule('status', 'not_empty');
            $post_data = $post->data();
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                
                Database::instance()->begin();

                $o_model_insurance_carrier = new Model_Insurancecarriers();
                $o_model_insurance_carrier->saveInsurancecarriers($post_data);
                                
                Database::instance()->commit();
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteInsurance() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Insurancecarrier = new Model_Insurancecarriers($post_data['id']);
                $obj_Insurancecarrier->status = self::STATUS_DEACTIVE;
                $obj_Insurancecarrier->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

    public function action_activeInsurance() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Insurancecarrier = new Model_Insurancecarriers($post_data['id']);
                $obj_Insurancecarrier->status = self::STATUS_ACTIVE;
                $obj_Insurancecarrier->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

    public function action_getInsurancecarrier() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Insurancecarrier = new Model_Insurancecarriers($post_data['id']);
                
                $a_insurance_rate = DB::select(
                        array('r.idInsuranceRates', 'insurance_rate')
                        //, array('r.idInsuranceRate', 'insurance_rate')
                        )
                        ->from(array('insurancerates', 'r'))
                        ->where('r.idInsuranceCarrier', '=', $obj_Insurancecarrier->idInsurancecarrier)
                        ->execute()
                        ->as_array();
                
                $a_response['rates'] = array_map(function($a_item){
                    $o_insurance_rate = new Model_InsuranceRates($a_item['insurance_rate']);
                    return $o_insurance_rate->as_array();
                }, $a_insurance_rate);
                $a_response['data'] = $obj_Insurancecarrier->as_array();
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

    public function action_listInsurancecarrier() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');
            $s_all = $this->request->query('all');
            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idInsurancecarrier' => "ins.idInsurancecarrier",
                'name' => "ins.name",
                'contactname' => "ins.contactname",
                'address' => "ins.address",
                'phone' => "ins.phone",
                'daterecord' => "ins.daterecord",
                'status' => "ins.status",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "insurancecarrier ins";

            $s_where_conditions = " TRUE ";
            if ($s_all == 'false')
                $s_where_conditions = " ins.status = " . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
