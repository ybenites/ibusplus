<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Contactlist extends Controller_Private_Admin {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('contactlist', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('contactlist', self::FILE_TYPE_JS));
        $view_contactlist = new View('management/contactlist');
        $this->template->content = $view_contactlist;
    }

    public function action_save() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('contact', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                /* Capturo el id Driver */
                $contactlist_id = $post_data['idContactlist'];
                if ($contactlist_id > 0) {
                    //Edicion
                    $obj_contactlist = new Model_Contactlist($contactlist_id);
                    
                    if (!$obj_contactlist->loaded())
                        throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                }
                                else {
                    $obj_contactlist = new Model_Contactlist();
                }
                $obj_contactlist->contact = $post_data['contact'];
                $obj_contactlist->address = $post_data['address'];
                $obj_contactlist->suite = $post_data['suite'];
                $obj_contactlist->telephone = $post_data['telephone'];
                $obj_contactlist->status = self::STATUS_ACTIVE;
                $obj_contactlist->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idContactlist = $this->request->post("idContactlist");
            $o_contactlist = new Model_Contactlist($idContactlist);
            $o_contactlist->as_array();

            $a_response['data'] = $o_contactlist->as_array();
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_idContactlist = $this->request->post("idContactlist");
            $o_contactlist = new Model_Contactlist($i_idContactlist);

            if (!$o_contactlist->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_contactlist->status = self::STATUS_DEACTIVE;
                $o_contactlist->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }
    
    
        public function action_active() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_idContactlist = $this->request->post("idContactlist");
            $o_contactlist = new Model_Contactlist($i_idContactlist);
            
            if (!$o_contactlist->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_contactlist->status = self::STATUS_ACTIVE;
                $o_contactlist->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }


    public function action_consults() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->post('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idContactlist' => "cl.idContactlist",
                'contact' => "cl.contact",
                'address' => 'cl.address',
                'suite' => 'cl.suite',
                'telephone' => 'cl.telephone',
                'status' => 'cl.status',
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "contactlist cl";

            $s_where_conditions = " TRUE ";
            if ($s_all == 'false')
                $s_where_conditions = " cl.status = " . self::STATUS_ACTIVE;



            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
