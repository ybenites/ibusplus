<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Casemanagers extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('casemanagers', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('casemanagers', self::FILE_TYPE_JS));
        $view_casemanagers = new View('management/casemanagers');
        $provider = DB::select('idProvider','name')
                        ->from('provider')
                        ->execute()->as_array();

        $view_casemanagers->provider = $provider;

        $this->template->content = $view_casemanagers;
    }

    public function action_listCasemanagers() {
        $this->auto_render = FALSE;
        try {
            $page = $this->request->post('page');
            $limit = $this->request->post('rows');
            $sidx = $this->request->post('sidx');
            $sord = $this->request->post('sord');
            
            $s_all = $this->request->post('status');
            if (!$sidx)
                $sidx = 1;
            $where_search = "";
            $searchOn = jqGridHelper::Strip($this->request->post('_search'));

            $array_cols = array(
                'idCasemanager' => 'cas.idCasemanager',
                'casname' => 'cas.name',
                'idProvider' => 'pro.idProvider',
                'proname' => 'pro.name',
                'status' => 'cas.status'
            );
            //COLUMNAS DE BUSQUEDA SIMPLE
            $searchable_cols = array_keys($array_cols);

            if ($searchOn == 'true') {
                if ($this->request->post('filters') != '') {
                    //BÚSQUEDA AVANZADA
                    $searchstr = jqGridHelper::Strip($this->request->post('filters'));
                    $where_search = jqGridHelper::constructWhere($searchstr);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $k => $v) {

                        if (in_array(str_ireplace('_', '.', $k), $searchable_cols)) {
                            $where_search .= " AND " . str_ireplace('_', '.', $k) . " LIKE '%" . $v . "%'";
                        }
                    }
                }
            }

            $cols = jqGridHelper::arrayColsToSQL($array_cols);

            $tables_join = 'casemanagers cas INNER JOIN provider pro ON cas.idProvider = pro.idProvider';
            $where_conditions = "TRUE";
            
          
            if ($s_all == 'false')
                $where_conditions = " cas.status = " . self::STATUS_ACTIVE;
            
//           echo $where_conditions;
//           die();

            $count = jqGridHelper::getCount($cols, $tables_join, $where_conditions, $where_search);

            $start = jqGridHelper::calculateStart($page, $count, $limit, $total_pages);

            $casemanager = jqGridHelper::generateSQL($cols, $tables_join, $where_conditions, $where_search, $sidx, $sord, $limit, $start);

            $this->fnResponseFormat(jqGridHelper::JSON4jqGrid($page, $total_pages, $count, $casemanager, $array_cols, true), self::REPORT_RESPONSE_TYPE_JSON_JQGRID);
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
            $this->fnResponseFormat($a_response);
        }
    }

    public function action_getCasemanagers() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {

                $post_data = $post->data();
                $obj_Casemanager = new Model_Casemanagers($post_data['id']);
                $a_response['data'] = $obj_Casemanager->as_array();
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }

    public function action_createUpdateCasemanager() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $a_post = Validation::factory($this->request->post())
                    ->rule('name_caseM', 'not_empty');
            
           
            if (!$a_post->check()) {
            
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } 
            
            else {
            
                  $o_model_caseManager = new Model_Casemanagers();
                  $rpta = $o_model_caseManager->createCaseManager($a_post); 
                  $rpta->reload(); 
                   $a_response['data'] = array(
                    'idCasemanager'=> $rpta->idCasemanager
                    , 'name'=> $rpta->name
                );
              
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_deleteCasemanager() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Casemanager = new Model_Casemanagers($post_data['id']);
                $obj_Casemanager->status = self::STATUS_DEACTIVE;
                $obj_Casemanager->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }
    public function action_activeCasemanager() {
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post())
                ->rule('id', 'not_empty');
        if (!$post->check()) {
            throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
        } else {
            try {
                $post_data = $post->data();
                $obj_Casemanager = new Model_Casemanagers($post_data['id']);
                $obj_Casemanager->status = self::STATUS_ACTIVE;
                $obj_Casemanager->save();

                $a_response['data'] = 'OK';
            } catch (Exception $exc) {
                $a_response['code'] = self::CODE_ERROR;
                $a_response['msg'] = $exc->getMessage();
            }

            $this->fnResponseFormat($a_response);
        }
    }
    
    
    public function action_saveProviders() {
       $this->auto_render = FALSE;
       $a_response = $this->json_array_return;
       //die('adsfdgSADFS');
       try {
           $post = Validation::factory($this->request->post())
                   ->rule('name-prov', 'not_empty');
           if (!$post->check()) {
               throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
           } else {
               $post_data = $post->data();

               $obj_providers = new Model_Provider();
               $obj_providers->name = $post_data['name-prov'];
               $obj_providers->email = $post_data[ 'email-prov'];
               $obj_providers->phone = $post_data['telefono-prov'];
               $obj_providers->status = self::STATUS_ACTIVE;
               
               $obj_providers->save();
               
               $a_response['data'] = array(
                   
                   'idProvider'=> $obj_providers->idProvider,
                   'name'=>$obj_providers->name,
 
                   
               );
           }
       } catch (Exception $e_exc) {
           $a_response['code'] = self::CODE_ERROR;
           $a_response['msg'] = $this->errorHandling($e_exc);
       }
       $this->fnResponseFormat($a_response);
   }
 
 

    

}