<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Languages extends Controller_Private_Admin {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('languages', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('languages', self::FILE_TYPE_JS));
        $view_languages = new View('management/languages');
        
        $view_languages->aaa = $this->fnGetClassVariable('a_confirmation_assigment');
        
        $this->template->content = $view_languages;
    }

    public function action_save() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $s_name = $this->request->post("name");
            $s_type = $this->request->post("type");
            $i_language_id = $this->request->post("idLanguage");

            if ($i_language_id > 0) {
                //Edicion
                $o_languages = new Model_Languages($i_language_id);

                if (!$o_languages->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_languages->name = $s_name;
                $o_languages->type = $s_type;
                $o_languages->save();
            } else {
                //Nuevo
                $o_languages = new Model_Languages();
                $o_languages->name = $s_name;
                $o_languages->type = $s_type;
                $o_languages->status = self::STATUS_ACTIVE;
                $o_languages->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idLanguage = $this->request->post("idLanguage");
            $o_languages = new Model_Languages($idLanguage);
            $o_languages->as_array();

            $a_response['data'] = $o_languages->as_array();
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);

    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_language_id = $this->request->post("idLanguage");
            $o_languages = new Model_Languages($i_language_id);

            if (!$o_languages->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {
                
                $o_languages->status = self::STATUS_DEACTIVE;
                $o_languages->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }   

        $this->fnResponseFormat($a_response);
    }
    
    public function action_active(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_language_id = $this->request->post("idLanguage");
            $o_languages = new Model_Languages($i_language_id);

            if (!$o_languages->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            }else{
                
                $o_languages->status = self::STATUS_ACTIVE;
                $o_languages->save();
            }
            
            } catch (Exception $e) {
               $a_response['code'] = self::CODE_ERROR;
               $a_response['msg'] = $e->getMessage();
        }   

        $this->fnResponseFormat($a_response);
          
            
    }

    
    public function action_consults() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->post('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idLanguage' => "l.idLanguage",
                'name' => "l.name",
                'type' => "l.type",
                'status'=>'l.status',
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "languages l";

            $s_where_conditions = " TRUE ";
            if($s_all == 'false')
                $s_where_conditions = " l.status = " . self::STATUS_ACTIVE;



            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
