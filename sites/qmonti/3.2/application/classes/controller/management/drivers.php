<?php

//defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Drivers extends Controller_Private_Admin implements Monti_Constants {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('drivers', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('drivers', self::FILE_TYPE_JS));
        $view_drivers = new View('management/drivers');

        $area_driver = DB::select(array('idAreasdriver', 'id'), array('location', 'area'))
                        ->from("areasdriver")->where('status', '=', '1')
                        ->execute()->as_array();
        
        $view_drivers->area_driver = $area_driver;
        $view_drivers->driver_deduction_a = self::DRIVER_DEDUCTION_A;
        $view_drivers->driver_deduction_b = self::DRIVER_DEDUCTION_B;

        $this->template->content = $view_drivers;
//        $consulta = DB::select()->from("drivers")->execute()->as_array();
//        $this->template->data = $consulta;
    }

    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idDriver = $this->request->post('id');
            $o_driver = new Model_Drivers($idDriver);
            $prueba = $o_driver->as_array();

            $a_response['data'] = $prueba;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_eliminar() {
        $this->auto_render = FALSE;
        $id = $this->request->post("id");
        DB::delete("drivers")->where("drivers.idDriver", "=", $id)->execute();
        echo "yaa se elimino";
    }

    public function action_listAllDrivers() {
        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');
            
            $s_all = $this->request->query('all');

            $i_idtask = $this->request->query('task_id');
            
            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'id' => "p.idDriver",
                'name' => "p.name",
                'last_name' => "p.lastName",
                'address' => "p.address",
                'phone' => "p.phone",
                'fax' => "p.fax",
                'cellphone' => "p.cellphone",
                'email' => "p.email",
                'specialNotes' => "p.specialNotes",
                'location' => "ad.location",
                'status' => "p.status",
                'user' => "per.fullName",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "drivers p" .
            $s_tables_join = " INNER JOIN areasdriver ad ON p.idAreasdriver = ad.idAreasdriver".
            $s_tables_join = " LEFT JOIN bts_person per ON per.idPerson = p.userCreate";
            $s_where_conditions = "TRUE";
            if ($s_all == 'false')
                $s_where_conditions = " p.status = " . self::STATUS_ACTIVE;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_driver_id = $this->request->post("id");

            $o_driver = new Model_Drivers($i_driver_id);

            if (!$o_driver->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_driver->status = self::STATUS_DEACTIVE;
                $o_driver->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_active() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_driver_id = $this->request->post("id");

            $o_driver = new Model_Drivers($i_driver_id);

            if (!$o_driver->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_driver->status = self::STATUS_ACTIVE;
                $o_driver->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveDrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                /* Capturo el id Driver */
                $driver_id = $post_data['idDrivers'];
                if ($driver_id > 0) {
                    //Edicion
                    $obj_drivers = new Model_Drivers($driver_id);
                    if (!$obj_drivers->loaded())
                        throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                }
                else {
                    $obj_drivers = new Model_Drivers();
                }
                $obj_drivers->name = $post_data['name'];
                $obj_drivers->lastName = $post_data['last_name'];
                $obj_drivers->idAreasdriver = $post_data['areas_drivers_id'];
                
                $obj_drivers->typeservices = $post_data['for_drive'];
                $obj_drivers->address = $post_data['address'];
                $obj_drivers->city = $post_data['city'];
                $obj_drivers->state = $post_data['state'];
                $obj_drivers->zipcode = $post_data['zip_code'];
                $obj_drivers->phone = $post_data['phone'];
                $obj_drivers->fax = $post_data['fax'];
                $obj_drivers->cellphone = $post_data['cell_phone'];
                $obj_drivers->carMake = $post_data['car_make'];
                $obj_drivers->carModel = $post_data['car_model'];
                $obj_drivers->email = $post_data['email'];
                $obj_drivers->insurance = $post_data['insurance_date'];
                $obj_drivers->identificationNumber = $post_data['identification_number'];
                $obj_drivers->specialNotes = $post_data['special_notes'];
                $obj_drivers->insuranceDeduction = $post_data['insurance_deduction'];
                $obj_drivers->gasolineSurcharge = $post_data['gasoline_surcharge'];

                $obj_drivers->ambulatoryRate = $post_data['ambulatory_rate'];
                $obj_drivers->ambulatoryWaitingTime = $post_data['ambulatory_waiting_time'];
                $obj_drivers->ambulatoryMiniFlatRateOW = $post_data['ambulatory_min_flat_rate_ow'];
                $obj_drivers->ambulatoryMiniFlatRateOT = $post_data['ambulatory_min_flat_rate_rt'];
                $obj_drivers->ambulatoryNoShow = $post_data['ambulatory_no_show'];
                $obj_drivers->ambulatoryEarlyPickUp = $post_data['ambulatory_early_pick_up'];
                $obj_drivers->ambulatoryLateDropOff = $post_data['ambulatory_late_drop_off'];

                $obj_drivers->wheelchairRate = $post_data['wheelchair_rate'];
                $obj_drivers->wheelchairLiftBase = $post_data['wheelchair_lift_base'];
                $obj_drivers->wheelchairWaitingTime = $post_data['wheelchair_waiting_time'];
                $obj_drivers->wheelchairCarAssistance = $post_data['wheelchair_car_assistance'];
                $obj_drivers->wheelchairMiniFlatRateOW = $post_data['wheelchair_min_flat_rate_ow'];
                $obj_drivers->wheelchairMiniFlatRateRT = $post_data['wheelchair_min_flat_rate_rt'];
                $obj_drivers->wheelchairNoShow = $post_data['wheelchair_no_show'];
                $obj_drivers->wheelchairEarlyPickUp = $post_data['wheelchair_early_pick_up'];
                $obj_drivers->wheelchairLateDropOff = $post_data['wheelchair_late_drop_off'];
                $obj_drivers->wheelchairWeekendFee = $post_data['wheelchair_weekend_fee'];

                $obj_drivers->stretcherRate = $post_data['stretcher_rate'];
                $obj_drivers->stretcherLiftBase = $post_data['stretcher_lift_base'];
                $obj_drivers->stretcherWaitingTime = $post_data['stretcher_waiting_time'];
                $obj_drivers->stretcherAssistance = $post_data['stretcher_assistance'];
                $obj_drivers->stretcherMinFlatRateOW = $post_data['stretcher_min_flat_rate_ow'];
                $obj_drivers->stretcherMinFlatRateRT = $post_data['stretcher_min_flat_rate_rt'];
                $obj_drivers->stretcherNoShow = $post_data['stretcher_no_show'];
                $obj_drivers->stretcherEarlyPickUp = $post_data['stretcher_early_pick_up'];
                $obj_drivers->stretcherLateDropOff = $post_data['stretcher_late_drop_off'];
                $obj_drivers->stretcherWeekendFee = $post_data['stretcher_weekend_fee'];
                
                $obj_drivers->terminationDate = $post_data['termination_date'];
                
                
                $obj_drivers->status = $post_data['status_driver'];


                $obj_drivers->commercialInsurance = $post_data['for_response'];
                //$obj_drivers->status = self::STATUS_ACTIVE;
                $obj_drivers->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    //FUNCION GUARDAR AREAS DRIVER

    public function action_saveAreadrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        //die('adsfdgSADFS');
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('location_driverarea', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();

                $obj_areadrivers = new Model_Areadriver();
                $obj_areadrivers->location = $post_data['location_driverarea'];
                $obj_areadrivers->status = self::STATUS_ACTIVE;
                $obj_areadrivers->save();
                $a_response['data'] = array(
                    'idAreasdriver'=> $obj_areadrivers->idAreasdriver
                    , 'location'=>$obj_areadrivers->location
                );
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

}