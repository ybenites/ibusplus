<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Interpreters extends Controller_Private_Admin implements Monti_Constants{

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('interpreters', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('interpreters', self::FILE_TYPE_JS));
        $view_interpreters = new View('management/interpreters');
        $language = DB::select(array('la.idLanguage', 'idLanguage'), array('la.name', 'name'))
                        ->from(array('languages', 'la'))
                        ->where('la.status', '=',  self::STATE_ACTIVE)
                        ->execute()->as_array();
        $view_interpreters->language = $language;


        $this->template->content = $view_interpreters;
    }

    public function action_save() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty');

            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();

                Database::instance()->begin(); //inicio de transaccion
                $i_interpreter_id = $post_data['idInterpreter'];



                if ($i_interpreter_id > 0) {
                    //Edicion
                    $o_interpreter = new Model_Interpreters($i_interpreter_id);

                    if (!$o_interpreter->loaded())
                        throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                } else {
                    //Nuevo guarda 
                    $o_interpreter = new Model_Interpreters();
                }

                $o_interpreter->name = $post_data["name"];
                $o_interpreter->lastname = $post_data["lastname"];
                $o_interpreter->phone = $post_data["phone"];
                $o_interpreter->cellphone = $post_data["cellphone"];
                $o_interpreter->email = $post_data["email"];
                $o_interpreter->city = $post_data["city"];
                $o_interpreter->address = $post_data['address'];
                $o_interpreter->state = $post_data['state'];
                $o_interpreter->zipcode = $post_data['zipcode'];
                $o_interpreter->fax = $post_data['fax'];
                $o_interpreter->identificationnumber = $post_data['identificationnumber'];
                $o_interpreter->interpreternotes = $post_data['interpreternotes'];
                
                $o_interpreter->noshowmedicalregular = $post_data['noshowmedicalregular'];
                $o_interpreter->noshowmedicalregularmintime = $post_data['noshowmedicalregular_mintime'];
                $o_interpreter->noshowmedicalrare = $post_data['noshowmedicalrare'];
                $o_interpreter->noshowmedicalraremintime = $post_data['noshowmedicalrare_mintime'];
                $o_interpreter->noshowmedicalother = $post_data['noshowmedicalother'];
                $o_interpreter->noshowmedicalothermintime = $post_data['noshowmedicalother_mintime'];
                $o_interpreter->noshowlegalregular = $post_data['noshowlegalregular'];
                $o_interpreter->noshowlegalregularmintime = $post_data['noshowlegalregular_mintime'];
                $o_interpreter->noshowlegalrare = $post_data['noshowlegalrare'];
                $o_interpreter->noshowlegalraremintime = $post_data['noshowlegalrare_mintime'];
                $o_interpreter->noshowlegalother = $post_data['noshowlegalother'];
                $o_interpreter->noshowlegalothermintime = $post_data['noshowlegalother_mintime'];
                
                $o_interpreter->intmedicalregular = $post_data['intmedicalregular'];
                $o_interpreter->intmedicalrare = $post_data['intmedicalrare'];
                $o_interpreter->intmedicalother = $post_data['intmedicalother'];
                $o_interpreter->intlegalregular = $post_data['intlegalregular'];
                $o_interpreter->intlegalrare = $post_data['intlegalrare'];
                $o_interpreter->intlegalother = $post_data['intlegalother'];
                $o_interpreter->minintmedical = $post_data['minintmedical'];
                $o_interpreter->minintlegal = $post_data['minintlegal'];
                
                $o_interpreter->interpretingconferencecall = $post_data['interpretingconferencecall'];
                $o_interpreter->minintconferencecall = $post_data['minintconferencecall'];
                $o_interpreter->translationperpage = $post_data['translationperpage'];
                $o_interpreter->translationperword = $post_data['translationperword'];
                $o_interpreter->mileagerate = $post_data['mileagerate'];
                $o_interpreter->traveltime = $post_data['traveltime'];
                
                $o_interpreter->license = $post_data['license'];
                $o_interpreter->status = $post_data['status'];
                $o_interpreter->terminationdate = $post_data['terminationdate'];
                $o_interpreter->terminationreason = $post_data['terminationreason'];

                $s_language = $this->request->post('selecLanguage');

                $o_interpreter->save();

                $idInterpreter = $o_interpreter->idInterpreter;

                $a_language = DB::select(
                                    array('d.idDetailInterpreterLanguage', 'id_det')
                                )
                                ->from(array('detailinterpreterslanguages', 'd'))                        
                                ->where('d.idInterpreter', '=', $idInterpreter)
                                ->and_where('d.status', '=', 1)
                                ->execute()->as_array();

                foreach ($a_language as $key => $a_item) {
                    $m_detailInterpreter = new Model_DetailInterpretersLanguages($a_item['id_det']);
                    $m_detailInterpreter->status = 0;                    
                    $m_detailInterpreter->save();                    
                }

                foreach ($s_language as $value) {
                    $m_dil = new Model_DetailInterpretersLanguages();
                    $m_dil->idInterpreter = $idInterpreter;
                    $m_dil->idLanguage = $value;
                    $m_dil->save();
                }
                Database::instance()->commit();  //fin de transaccion
            }
        } catch (Exception $e) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    
    public function action_saveLanguage(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        
        try{
            $post = Validation::factory($this->request->post())
                    ->rule('nameLanguage', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();

                $obj_language = new Model_Languages();
                $obj_language->name = $post_data['nameLanguage'];
                $obj_language->type = $post_data['type'];
                $obj_language->status = self::STATUS_ACTIVE;
                $obj_language->save();
                //die('');
                //$obj_language->reload();
             
                $consul = array(
                    'idLanguage' => $obj_language->idLanguage
                    , 'nameLanguage' => $obj_language->name
                );
                
                $a_response['data'] = $consul;
            }
            
        }catch(Exception $e_exc){
            $a_response['code']=  self::CODE_ERROR;
            $a_response['data'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response); //te manda los dx a la vista
        
    }
    
    
    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idInterpreter = $this->request->post('idInterpreter');
                 $a_language = DB::select(
                                    array('la.idLanguage', 'id_lan')
                                )
                                ->from(array('detailinterpreterslanguages','dil'))
                                ->join(array('languages','la'))
                                ->on('dil.idLanguage', '=', 'la.idLanguage')
                                ->where('dil.idInterpreter', '=', $idInterpreter)
                                ->and_where('la.status', '=', 1)
                                ->and_where('dil.status', '=', 1)
                                ->order_by('la.idLanguage')
                                ->execute()->as_array();
                 
                 // print_r($a_language);
                 
            $o_interpreter = new Model_Interpreters($idInterpreter);
//            $o_detail = new Model_DetailInterpretersLanguages($a_language);
            $prueba = $o_interpreter->as_array();
          //  $prueb = $o_detail->as_array();

            $a_response['data'] = $prueba;
            $a_response['dataLanguages'] = $a_language;
        
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_interpreter_id = $this->request->post("idInterpreter");

            $o_interpreter = new Model_Interpreters($i_interpreter_id);

            if (!$o_interpreter->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_interpreter->status = self::STATUS_DEACTIVE;
                $o_interpreter->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_active() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_interpreter_id = $this->request->post("idInterpreter");

            $o_interpreter = new Model_Interpreters($i_interpreter_id);

            if (!$o_interpreter->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_interpreter->status = self::STATUS_ACTIVE;
                $o_interpreter->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_consults() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->post('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idInterpreter' => "i.idInterpreter",
                'name' => "i.name",
                'lastname' => "i.lastname",
                'phone' => "i.phone",
                'creationDate' => "i.creationDate",
                'cellphone' => "i.cellphone",
                'email' => "i.email",
                'city' => "i.city",
                'language' => "GROUP_CONCAT(la.name)",
                'status' => "i.status",
                'user' => "per.fullName",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "interpreters i";
            $s_tables_join .= " INNER JOIN detailinterpreterslanguages de ON de.idInterpreter=i.idInterpreter";
            $s_tables_join .= ' INNER JOIN languages la ON la.idLanguage = de.idLanguage ';
            $s_tables_join .= ' LEFT JOIN bts_person per ON per.idPerson = i.userCreate';
            $s_where_conditions = " de.status = 1 ";
            if ($s_all == 'false')
                $s_where_conditions .= " AND i.status = " . self::STATUS_ACTIVE;

            $s_group = ' GROUP BY i.idInterpreter';

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start, $s_group);

            //echo Database::instance()->last_query;die();
            
            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}