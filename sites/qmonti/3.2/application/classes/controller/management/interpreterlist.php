<?php

class Controller_Management_Interpreterlist extends Controller_Private_Admin {

    public function action_index() {

        $this->mergeStyles(ConfigFiles::fnGetFiles('interpreterlist', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('interpreterlist', self::FILE_TYPE_JS));
        $view_interpreterlist = new View('management/interpreterlist');
        $language = DB::select(array('la.idLanguage', 'idLanguage'), array('la.name', 'name'))
                        ->from(array('languages', 'la'))
                        ->execute()->as_array();
        $view_interpreterlist->language = $language;
        $this->template->content = $view_interpreterlist;
    }

    public function action_consults() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->post('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idInterpreter' => "i.idInterpreter",
                'creationDate' => "i.creationDate",
                'name' => "i.name",
                'lastname' => "i.lastname",
                'phone' => "i.phone",
                'cellphone' => "i.cellphone",
                'email' => "i.email",
                'city' => "i.city",
                'language' => "GROUP_CONCAT(la.name)",
                'user' => "per.fullName",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "interpreters i";
            $s_tables_join .= " INNER JOIN detailinterpreterslanguages de ON de.idInterpreter=i.idInterpreter";
            $s_tables_join .= ' INNER JOIN languages la ON la.idLanguage = de.idLanguage ';
            $s_tables_join .= ' LEFT JOIN bts_person per ON per.idPerson = i.userCreate';
            $s_where_conditions = " de.status = 1 ";
            $s_where_conditions .= " AND i.status = " . self::STATE_ACTIVE;

            $s_group = ' GROUP BY i.idInterpreter';

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start, $s_group);

            //echo Database::instance()->last_query;die();

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idInterpreter = $this->request->post('idInterpreter');
            $a_language = DB::select(
                                    array('la.name', 'name')
                            )
                            ->from(array('detailinterpreterslanguages', 'dil'))
                            ->join(array('languages', 'la'))
                            ->on('dil.idLanguage', '=', 'la.idLanguage')
                            ->where('dil.idInterpreter', '=', $idInterpreter)
                            ->and_where('la.status', '=', 1)
                            ->and_where('dil.status', '=', 1)
                            ->order_by('la.idLanguage')
                            ->execute()->as_array();

            // print_r($a_language);

            $o_interpreter = new Model_Interpreters($idInterpreter);
//            $o_detail = new Model_DetailInterpretersLanguages($a_language);
            $prueba = $o_interpreter->as_array();
            //  $prueb = $o_detail->as_array();

            $a_response['data'] = $prueba;
            $a_response['dataLanguages'] = $a_language;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

}

?>
