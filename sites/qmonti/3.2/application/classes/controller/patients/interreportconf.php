<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Interreportconf extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('interreportconf', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('interreportconf', self::FILE_TYPE_JS));
        $view_interreportconf = new View('patients/interreportconf');
        $this->template->content = $view_interreportconf;
    }

    public function action_listInters() {

        $var = $this->request->post('nameinter');

        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE
        $a_response['data'] = DB::select(
                                array('i.idInterpreter', 'idInter')
                                , array('i.name', 'name')
                                , array('i.lastname', 'lastName')
                        )->distinct(true)
                        ->from(array("interpreters", 'i'))
                        ->where('i.status', '=', '1')
                        ->and_where_open()
                        ->or_where('i.name', 'like', DB::expr('"%' . $var . '%"'))
                        ->or_where('i.lastname', 'like', DB::expr('"%' . $var . '%"'))
                        ->and_where_close()
                        ->limit(20)
                        ->execute()->as_array();

        $this->fnResponseFormat($a_response);
    }

    public function action_listIntersWithApp() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');


        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idInter' => " DISTINCT (i.idInterpreter)",
                'idApp' => "app.idAppointment",
                'interpreter' => "CONCAT(i.name,' ',i.lastname)",
                'phone' => "i.phone",
                'fax' => "i.fax",
                
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "interpreters i" .
                    $s_tables_join = " JOIN appointment app ON i.idInterpreter=app.idInterpreter ";
               

            $s_where_conditions = " app.status=1  AND app.state='CONFIRMED' AND " .
            $s_where_conditions = " (app.confirmedsentinterpreter=0 OR ISNULL(app.confirmedsentinterpreter)) AND" .
            $s_where_conditions = " (app.cancelinterpreter=0 OR ISNULL(app.cancelinterpreter)) AND app.typeservicesinterpreter='Interpreting' ".
                $s_where_conditions=" GROUP BY i.idInterpreter ";
            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_listAppbyIdInter() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $all = $this->request->query('all');
        $idInter = $this->request->query('idInter');

        if ($all != 1) {
            $dateStart = $this->cambiafmysqlre($this->request->query('dateStart'));
            $dateFinish = $this->cambiafmysqlre($this->request->query('dateFinish'));
            $stringdate = " AND app.appointmentdate>='" . $dateStart . "' AND app.appointmentdate<='" . $dateFinish . "' "; 
            
        }
        else
            $stringdate = "";

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idApp' => " DISTINCT app.idAppointment",
                'appDate' => "app.appointmentdate",
                'appTime' => "app.appointmenttime",
                'droptime' => "app.droptime",
                'patient' => "CONCAT(p.firstname,' ',p.lastname)",
                'location' => "CONCAT(loc.address,' ',loc.telephone,'',IF(ISNULL(loc.suite) OR loc.suite=0,'',CONCAT(' Suite:',loc.suite))) "
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " appointment app " .
                    $s_tables_join = " INNER JOIN patients p ON app.idPatient=p.idPatient" .
                    $s_tables_join = " LEFT JOIN locations loc ON app.idLocation=loc.idLocation";

            $s_where_conditions = " app.status=1 AND app.state='CONFIRMED' AND " .
                    $s_where_conditions = " (app.confirmedsentinterpreter=0 OR ISNULL(app.confirmedsentinterpreter)) AND " .
                    $s_where_conditions = " (app.cancelinterpreter=0 OR ISNULL(app.cancelinterpreter)) AND app.typeservicesinterpreter='Interpreting'  AND app.idInterpreter=$idInter" .
                    $s_where_conditions = $stringdate;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);


            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_reportPrintAppbyInter() {

        $this->auto_render = FALSE;


        try {


            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $driver = array();

            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $idInter = $this->request->query('idInter');

            $o_inter = new Model_Interpreters($idInter);

            $data['intername'] = $o_inter->name . ' ' . $o_inter->lastname;
            $data['interfax'] = $o_inter->fax;

            $o_person = new Model_Person($this->getSessionParameter(self::USER_ID));
            $o_person_fullname = $o_person->fullName;
            $o_person_email = $o_person->email;
            $data['username'] = $o_person_fullname;
            $data['useremail'] = $o_person_email;


            $all = $this->request->query('all');
            if ($all == 0) {
                $dateStart = $this->request->query('dateStart');
                $dateFinish = $this->request->query('dateFinish');
//                $driver['dstart']=$dateStart;
//                $driver['dfinish']=$dateFinish;
            } else {
                
            }

            $parameters = array(
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE
            );

            $columns = array(
                array('ap.appointmentdate', 'appointmentDate'),
                array('ap.appointmenttime', 'appointmentTime'),
//                DB::expr("DATE_FORMAT(ap.appointmenttime,:time_format) AS appointmenttime"),
                DB::expr("CONCAT(pa.firstname, ' ', pa.lastname) AS patient"),
                array('l.name', 'language'),
                array('loc.alias', 'facility'),
                DB::expr("CONCAT(UPPER(SUBSTRING(i.name,1,2)),idAppointment) AS authorization"),
                array('loc.address', 'address'),
                array('loc.telephone', 'telephone'),
            );

            $sql_string = DB::select_array($columns)
                    ->from(array('appointment', 'ap'))
                    ->join(array('patients', 'pa'))->on('pa.idPatient', '=', 'ap.idPatient')
                    ->join(array('languages', 'l'))->on('ap.idLanguage', '=', 'l.idLanguage')
                    ->join(array('locations', 'loc'))->on('ap.idLocation', '=', 'loc.idLocation')
                    ->join(array('claims', 'c'))->on('ap.idClaim', '=', 'c.idClaim')
                    ->join(array('insurancecarrier', 'i'))->on('c.idInsuranceCarrier', '=', 'i.idInsurancecarrier')
                    ->where('ap.idInterpreter', '=', $idInter)
                    ->and_where('ap.status', '=', '1')
                    ->and_where('ap.state', '=', 'CONFIRMED')
                    ->and_where('ap.typeservicesinterpreter', '=', 'Interpreting')
                    ->and_where_open()
                    ->or_where('ap.appconfirmedconfirmationinterpreter', '=', '0')
                    ->or_where('ap.appconfirmedconfirmationinterpreter', 'IS', NULL)
                    ->and_where_close()
                    ->and_where_open()
                    ->or_where('ap.cancelinterpreter', '=', '0')
                    ->or_where('ap.cancelinterpreter', 'IS', NULL)
                    ->and_where_close();


            if ($all == 0) {
                $sql_string->and_where('ap.appointmentdate', '>=', $dateStart);
                $sql_string->and_where('ap.appointmentdate', '<=', $dateFinish);
            }

            $sql_string = $sql_string->parameters($parameters)->execute()->as_array();

            $this->fnResponseFormat($sql_string, self::REPORT_RESPONSE_TYPE_XML, $data);
            // print_r(Database::instance()->last_query);
            // die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_sendEmailInter() {
        try {
            $this->auto_render = FALSE;
            $url = $this->request->post('url');
            $idInter = $this->request->post('idInter');

            $o_inter = new Model_Interpreters($idInter);

            $email = $o_inter->email;
            $name = $o_inter->name + $o_inter->lastname;


            $company = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $contactCompany = db_config::$db_conection_config[SITE_DOMAIN]['contactCompany'];
            $contactAddress = db_config::$db_conection_config[SITE_DOMAIN]['contactAddress'];
            $contactPhone = db_config::$db_conection_config[SITE_DOMAIN]['contactPhone'];
            $skin = db_config::$db_conection_config[SITE_DOMAIN]['skin'];
            $website = $_SERVER['HTTP_HOST'];



            $sendEmail = array($email => "$name");
            $click = $url;



            $namepdf2 = preg_replace('/\W/', '_', 'MONTI_PRINT_CONF_INTER');
            $file = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'pdfreports' . DIRECTORY_SEPARATOR . "$namepdf2.pdf";
            copy($click, $file);
            $message = 'Monti Interpreting Assignment Report<br/>
                        Thank you for letting us serve you . <br/>
                        Please view attachment'; //' or click <a href="' . $click . '">here</a>';



            $this->fnaddNewModule('swift', 'swift');
            Email::instance(__($message) . $contactCompany)
                    ->to($sendEmail)
                    ->message('789456123')
                    ->from('no_reply@ibusplus.com')
                    ->attach($file)
                    ->send(FALSE);
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }
    
        public function action_comfirmSentInter() {
        try {
            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;
            $idInter = $this->request->post('idInter');
            $idApps = explode(",", $this->request->post('dataIdApp'));

            foreach ($idApps as $key => $value) {
                $o_model_app = new Model_Appointment($value);
                if ($idInter == $o_model_app->idInterpreter) {
                    $o_model_app->confirmedsentinterpreter = self::STATE_ACTIVE;
                $o_model_app->save();
                }
            }
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}