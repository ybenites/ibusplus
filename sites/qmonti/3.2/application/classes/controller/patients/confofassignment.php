<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Confofassignment extends Controller_Private_Admin implements Monti_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('confofassignment', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('confofassignment', self::FILE_TYPE_JS));
        $view_confofassignment = new View('patients/confofassignment');
        $this->template->content = $view_confofassignment;
    }

    public function getidAdjuster($idapp) {

        $cons = DB::select(array('cl.idAdjuster', 'idAdjuster'), array('adj.name', 'name')
                        )->distinct(true)
                        ->from(array('claims', 'cl'))
                        ->join(array('appointment', 'app'), 'LEFT')
                        ->on('cl.idClaim', '=', 'app.idClaim')
                        ->join(array('adjusters', 'adj'), 'LEFT')
                        ->on('adj.idAdjuster', '=', 'cl.idAdjuster')
                        ->where('app.idAppointment', '=', DB::expr($idapp))
                        ->execute()->current();
        return $cons;
    }

    public function getdataemail($idapp) {

        $cons_email = DB::select(
                                array(DB::expr("CONCAT( pat.firstname,' ',pat.lastname)"), 'name'), array('cl.claimnumber', 'claimnumber'), array('app.appointmentdate', 'appointmentdate'), array('app.appointmenttime', 'appointmenttime')
                        )
                        ->distinct(true)
                        ->from(array('appointment', 'app'))
                        ->join(array('patients', 'pat'), 'LEFT')
                        ->on('pat.idPatient', '=', 'app.idPatient')
                        ->join(array('claims', 'cl'), 'LEFT')
                        ->on('cl.idClaim', '=', 'app.idClaim')
                        ->where('app.idAppointment', '=', DB::expr($idapp))
                        ->execute()->current();
        return $cons_email;
    }

    public function action_savepdf() {
        try {

            $this->auto_render = FALSE;
            print_r($this->request->post());
//            $idAppointment = $this->request->post('idAppointment');
//            $nameadj=$this->getidAdjuster($idAppointment);
//            $data_email = $this->getdataemail($idAppointment);
//
//
//
//            $click = "http://192.168.1.101:8080/ibusplus.com.reportRequest/faces/temp/MONTI_CONFIRMATION_ASSIGMENT_18_04_2013_06_12_41.pdf";
//
//            $namepdf2 = preg_replace('/\W/', '_', $nameadj);
//            copy("$click", __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . 'pdfreports' . DIRECTORY_SEPARATOR . "$namepdf2.pdf");
//            $message = 'Monti Interpreting Assignment Report<br/>
//                        Thank you for letting us serve you . <br/>
//                        Please view attachment'; //' or click <a href="' . $click . '">here</a>';
//
//            return functions::sendEmailattach(
//                            array("file" => "/var/www/projects/sites/qmonti/3.2/aplication/pdfreports/$namepdf2.pdf",
//                                "fromName" => "Monti",
//                                "toEmail" => $data_email,
//                                "asunto" => "Interpreter Schedule",
//                                "mensaje" => $message)
//            );
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }

    public function action_sendemail() {
        try {
            $this->auto_render = FALSE;
            $url = $this->request->post('url');
            $idAppointment = $this->request->post('idAppointment');

            $idAdjuster = $this->getidAdjuster($idAppointment);
            $data_email = $this->getdataemail($idAppointment);
            $nameadj = $this->getidAdjuster($idAppointment);



            $company = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $contactCompany = db_config::$db_conection_config[SITE_DOMAIN]['contactCompany'];
            $contactAddress = db_config::$db_conection_config[SITE_DOMAIN]['contactAddress'];
            $contactPhone = db_config::$db_conection_config[SITE_DOMAIN]['contactPhone'];
            $skin = db_config::$db_conection_config[SITE_DOMAIN]['skin'];
            $website = $_SERVER['HTTP_HOST'];
            $patientname = $data_email['name'];
            $claimnumber = $data_email['claimnumber'];
            $appointmentdate = $data_email['appointmentdate'];
            $appointmenttime = $data_email['appointmenttime'];



            $adjuster = new Model_Adjusters($idAdjuster['idAdjuster']);

            $email = $adjuster->email;
            $name = $adjuster->name;
            $sendEmail = array($email => "$name");
            $click = $url;
           


            $namepdf2 = preg_replace('/\W/', '_', $nameadj['name']);
            $file = __DIR__ . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'pdfreports' . DIRECTORY_SEPARATOR . "$namepdf2.pdf";
            copy($click, $file);
            $message = 'Monti Interpreting Assignment Report<br/>
                        Thank you for letting us serve you . <br/>
                        Please view attachment'; //' or click <a href="' . $click . '">here</a>';



            $this->fnaddNewModule('swift', 'swift');
            Email::instance(__($message) . $contactCompany)
                    ->to($sendEmail)
                    ->message($message)
                    ->from('no_reply@ibusplus.com')
                    ->attach($file)
                    ->send(FALSE);
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }

    public function action_consult() {
        try {


            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');



            $i_idtask = $this->request->query('task_id');

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idAppointment' => 'app.idAppointment',
                'patient' => 'pat.firstname',
                'phone' => 'pat.phonenumber',
                'dateapp' => 'app.appointmentdate',
                'timeapp' => 'app.appointmenttime',
                'status' => 'app.status'
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "appointment app" .
                    $s_tables_join = " INNER JOIN patients pat ON app.idPatient = pat.idPatient ";
            $s_where_conditions = "TRUE";
            if ($s_all == 'false')
                $s_where_conditions = " app.status = " . self::STATUS_ACTIVE . " AND app.state = 'CREATED_CONFIRMATION_ADJUSTER ' ";

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);


            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_reportConfofassignment() {

        try {
            $this->auto_render = FALSE;
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));

            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $appointment = array();

            $appointment['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $appointment['qlang'] = $this->request->query(self::RQ_LANG);
            $appointment['searchDate'] = DateHelper::getFechaFormateadaActual();

            $appointment['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $appointment['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            $id_appointment = $this->request->query('idAppointment');

            $parameters = array(
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':appointment' => $id_appointment,
                ':interpreting' => self::INTERPRETING,
                ':conferencecall' => self::CONFERENCECALL,
                ':documentaltranslation' => self::DOCUMENTTRANSLATION,
                ':ambulatory' => self::AMBULATORY,
                ':whellchair' => self::WHEELCHAIR,
                ':stretcher' => self::STRETCHER,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE
            );


            $columns = array(
                DB::expr("CONCAT(pa.firstname, ' ', pa.lastname) AS patient"),
                array('cla.claimnumber', 'claimNumber'),
                DB::expr("DATE_FORMAT(ap.appointmentdate,:date_format) AS appointmentDate"),
                array('ap.appointmenttime', 'appointmentTime'),
                array('adj.name', 'adjusterName'),
                array('course.address', 'addressCourse'),
                array('course.telephone', 'telephoneCourse'),
                array('course.typeservice', 'typeServiceCourse'),
                array('course.name', 'languageCourse'),
                array('locations.address', 'addressLocation'),
                array('locations.telephone', 'telephoneLocation'),
                array('locations.typeservicesinterpreter', 'typeServicesInterpreter'),
                DB::expr("IF(ap.idCourse IS NOT NULL, :s_active, :s_inactive) AS isCourse"),
                DB::expr("IF(ap.idLanguage IS NOT NULL, :s_active, :s_inactive) AS isLanguage"),
                DB::expr("IF(ap.idLocation IS NOT NULL, :s_active, :s_inactive) AS isLocation"),
                DB::expr("IF((ap.idDriverRoundTrip OR ap.idDriverOneWay) IS NOT NULL AND ap.idLanguage IS NOT NULL,:s_active, :s_inactive) AS isDriverLanguage"),
                DB::expr("IF((ap.idDriverRoundTrip OR ap.idDriverOneWay) IS NOT NULL AND ap.idLanguage IS NOT NULL AND ap.typeservicesinterpreter <> :interpreting, :s_active, :s_inactive) AS isTypeServicesInterpreter")
            );

            $sql_sub_course = Db::select('locations.idLocation', 'locations.address', 'locations.telephone', 'appointment.typeservice', 'languages.name')
                    ->from('appointment')
                    ->join('course')->on('course.idCourse', '=', 'appointment.idCourse')
                    ->join('locations', 'LEFT')->on('locations.idLocation', '=', 'course.idLocationDestination')
                    ->join('languages', 'LEFT')->on('languages.idLanguage', '=', 'appointment.idLanguage')
                    ->where('appointment.idAppointment', '=', $id_appointment)
                    ->and_where_open()
                    ->or_where('appointment.idDriverRoundTrip', 'IS NOT', NULL)
                    ->or_where('appointment.idDriverOneWay', 'IS NOT', NULL)
                    ->and_where_close();

            $sql_sub_location = DB::select(
                            'locations.idLocation', 'locations.address', 'locations.telephone', 'appointment.typeservicesinterpreter'
                    )
                    ->from('appointment')
                    ->join('locations')->on('locations.idLocation', '=', 'appointment.idLocation')
                    ->where('appointment.idAppointment', '=', $id_appointment)
                    ->and_where_open()
                    ->and_where('appointment.idDriverRoundTrip', 'IS', NULL)
                    ->and_where('appointment.idDriverOneWay', 'IS', NULL)
                    ->and_where_close()
                    ->and_where('appointment.typeservicesinterpreter', '=', ':interpreting')
                    ->and_where('appointment.idLanguage', 'IS NOT', NULL)
                    ->parameters($parameters);


            $sql_appointments = DB::select_array($columns)
                            ->from(array('appointment', 'ap'))
                            ->join(array('patients', 'pa'))->on('pa.idPatient', '=', 'ap.idPatient')
                            ->join(array('claims', 'cla'))->on('cla.idClaim', '=', 'ap.idClaim')
                            ->join(array('adjusters', 'adj'))->on('adj.idAdjuster', '=', 'cla.idAdjuster')
                            ->join(array('course', 'cou'), 'LEFT')->on('cou.idCourse', '=', 'ap.idCourse')
                            ->join(array($sql_sub_course, 'course'), 'LEFT')->on('course.idLocation', '=', 'cou.idLocationDestination')
                            ->join(array('locations', 'loc'), 'LEFT')->on('loc.idLocation', '=', 'ap.idLocation')
                            ->join(array($sql_sub_location, 'locations'), 'LEFT')->on('locations.idLocation', '=', 'loc.idLocation')
                            ->where('ap.idAppointment', '=', $id_appointment)
                            ->parameters($parameters)
                            ->execute()->as_array();

            $this->fnResponseFormat($sql_appointments, self::REPORT_RESPONSE_TYPE_XML, $appointment);
//            print_r(Database::instance()->last_query);
//            die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}