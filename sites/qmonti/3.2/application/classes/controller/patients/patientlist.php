<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Patientlist extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('patientlist', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('patientlist', self::FILE_TYPE_JS));
        $view_patientlist = new View('patients/patientlist');

        $adminCasos = DB::select('idCasemanager', 'name')
                        ->from('casemanagers')
                        ->execute()->as_array();

        $adjuster = DB::select('idAdjuster', 'name')
                        ->from('adjusters')
                        ->execute()->as_array();

        $provider = DB::select('idProvider', 'name')
                        ->from('provider')
                        ->execute()->as_array();

        $insurance = DB::select('idInsurancecarrier', 'name')
                        ->from('insurancecarrier')
                        ->execute()->as_array();


        $view_patientlist->adminCasos = $adminCasos;
        $view_patientlist->adjuster = $adjuster;
        $view_patientlist->provider = $provider;
        $view_patientlist->insurance = $insurance;

        $this->template->content = $view_patientlist;
    }

    public function action_save() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        try {
            $post = Validation::factory($this->request->post())
                    ->rule('firstname', 'not_empty')
                    ->rule('lastname', 'not_empty');

            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                Database::instance()->begin(); //inicio de transaccion
                $i_patient_id = $post_data["idPatient"];

                if ($i_patient_id > 0) {
                    //Edicion
                    $o_patient = new Model_Patients($i_patient_id);
                    $o_claims = new Model_Claims();

                    if (!$o_patient->loaded())
                        throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                } else {
                    //Nuevo guarda 
                    $o_patient = new Model_Patients();
                    $o_claims = new Model_Claims();
                }
//                $format = $this->getDATE_FORMAT_FOR(self::DF_PHP_DATE);
//                $dob = DateTime::createFromFormat($format, $post_data["dob"]);
//                $accidentday = DateTime::createFromFormat($format, $post_data["accidentday"]);
                
//                print_r($post_data["unlimited"]);
//                die();
//                
                $o_patient->firstname = $post_data["firstname"];
                $o_patient->lastname = $post_data["lastname"];
                $o_patient->sex = $post_data["sex"];
                $o_patient->address = $post_data["address"];
                $o_patient->apartmentnumber = $post_data["apartmentnumber"];
                $o_patient->phonenumber = $post_data["phonenumber"];
                $o_patient->altnumber = $post_data["altnumber"];
                $o_patient->dob = DateHelper::cambiafmysqlre($post_data["dob"]);
                $o_patient->socialsecurity = $post_data["socialsecurity"];
                $o_patient->zipcode = $post_data["zipcode"];
                $o_patient->city = $post_data["city"];
                $o_patient->employer = $post_data["employer"];
                $o_patient->state = $post_data["state"];
                $o_patient->regiondefault = $post_data["regiondefault"];
                $o_patient->idInsurancecarrier = $post_data["idInsuranceCarriers_information"];
                $o_patient->status = self::STATUS_ACTIVE;
                $o_patient->save();

                $o_claims->claimnumber = $post_data["claimnumber"];
                $o_claims->accidentday = DateHelper::cambiafmysqlre($post_data["accidentday"]);
                $o_claims->authorizedby = $post_data["authorizedby"];
                $o_claims->injury = $post_data["injury"];
                //$o_claims->unlimited = $post_data["unlimited"];
                $o_claims->unlimited = $post_data["unlimited"];
                $o_claims->unlimitedapp = $post_data["unlimitedapp"];
                $o_claims->idPatient = $o_patient->idPatient;
                $o_claims->idInsuranceCarrier = $o_patient->idInsurancecarrier;
                $o_claims->idCasemanager = $post_data["selectCase"];
                $o_claims->idAdjuster = $post_data["selectAdjus"];
                $o_claims->status = self::STATUS_ACTIVE;
                $o_claims->save();

                //$a_response['data'] = $o_claims->idPatient;

//                Database::instance()->commit();
            }
        } catch (Exception $e) {
//            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            //$a_response['msg'] = $e->getMessage();
            $a_response['msg'] = $this->errorHandling($e);
            
        }



        $this->fnResponseFormat($a_response);
    }

    public function action_getPersonById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_idPatient = $this->request->post("idPatient");



            $o_patient = new Model_Patients($i_idPatient);
            $o_patient->as_array();

            $o_claims = DB::select('*')
                            ->from(array('claims', 'cla'))
                            ->where('cla.idPatient', '=', $i_idPatient)
                            ->as_object()->execute()->current();
//                            ->execute()->current();
            
            $accidentday = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE),strtotime($o_claims->accidentday));
            $dob = date($this->getDATE_FORMAT_FOR(self::DF_PHP_DATE),strtotime($o_patient->dob));
            $o_claims->accidentday = $accidentday;
            $o_patient->dob = $dob;
//           echo Database::instance()->last_query;
//           die();
            //$o_claims->as_array();

            $a_response['data'] = $o_patient->as_array();
            $a_response['dataC'] = $o_claims;
            
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_patient_id = $this->request->post("idPatient");

            $o_patient = new Model_Patients($i_patient_id);

            if (!$o_patient->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_patient->status = self::STATUS_DEACTIVE;
                $o_patient->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_active() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_patient_id = $this->request->post("idPatient");

            $o_patient = new Model_Patients($i_patient_id);

            if (!$o_patient->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_patient->status = self::STATUS_ACTIVE;
                $o_patient->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_consults() {

        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $i_idtask = $this->request->post('task_id');


            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idPatient' => "pa.idPatient",
                'firstname' => "pa.firstname",
                'lastname' => "pa.lastname",
                'socialsecurity' => 'pa.socialsecurity',
                'dob' => 'pa.dob',
                'employer' => 'pa.employer',
                'status' => 'pa.status',
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "patients pa ".
            $s_tables_join = " INNER JOIN claims cla ON pa.idPatient = cla.idPatient ";
//            $s_tables_join = " INNER JOIN casemanagers ca ON ca.idCasemanager = cla.idCasemanager ".
//            $s_tables_join = " INNER JOIN adjusters ad ON ad.idAdjuster = cla.idAdjuster";


            $s_where_conditions = " TRUE ";
            if ($s_all == 'false')
                $s_where_conditions = " pa.status = " . self::STATUS_ACTIVE;



            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);

            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}