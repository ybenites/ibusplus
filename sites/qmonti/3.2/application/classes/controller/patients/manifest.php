<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Manifest extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('manifest', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('manifest', self::FILE_TYPE_JS));
        $view_manifest = new View('patients/manifest');
        $case_manager = DB::select(array('idCasemanager', 'idCasemanager'), array('name', 'name'))
                        ->from("casemanagers")->where('status', '=', '1')
                        ->execute()->as_array();

        $adjuster = DB::select(array('idAdjuster', 'idAdjuster'), array('name', 'name'))
                        ->from("adjusters")->where('status', '=', '1')
                        ->execute()->as_array();

        $insurance_carrier = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();

        $drivers = DB::select(array('idDriver', 'idDriver'), array('name', 'name'), array('lastname', 'lastname'))
                        ->from("drivers")->where('status', '=', '1')
                        ->execute()->as_array();

        $type_appointment = DB::select(array('idTypeappointment', 'id'), array('name', 'nameapp'))
                        ->from("typeappointment")->where('status', '=', '1')
                        ->execute()->as_array();

        $languaje = DB::select(array('idLanguage', 'idLanguage'), array('name', 'name'))
                        ->from("languages")->where('status', '=', '1')
                        ->execute()->as_array();

        $interprete = DB::select(array('idInterpreter', 'idInterpreter'), array('name', 'name'))
                        ->from("interpreters")->where('status', '=', '1')
                        ->execute()->as_array();

        $area_driver = DB::select(array('idAreasdriver', 'id'), array('location', 'area'))
                        ->from("areasdriver")->where('status', '=', '1')
                        ->execute()->as_array();

        $view_manifest->area_driver = $area_driver;
        $view_manifest->insurance_carrier = $insurance_carrier;
        $view_manifest->case_manager = $case_manager;
        $view_manifest->adjuster = $adjuster;
        $view_manifest->drivers = $drivers;
        $view_manifest->type_appointment = $type_appointment;
        $view_manifest->languaje = $languaje;
        $view_manifest->interprete = $interprete;

        $this->template->content = $view_manifest;
    }

    public function action_saveappointment() {


        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post());
        try {
            $post_data = $post->data();
            /* Capturo el idClaim */
            $appointment_id = $post_data['idAppointment'];
            $until_app = $post_data['until_app'];


            //$valchecktransportation= $post_data['transportation'];

            if ($appointment_id > 0) {
//Edicion

                $obj_appointment = new Model_Appointment($appointment_id);
                if (!$obj_appointment->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                // $obj_appointment->idAlias = $post_data['idAlias_newappointment_appinfo'];
                if (isset($post_data['transportation'])) {
                    $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                    $obj_appointment->idDriverOneWay = $post_data['driver1'];
                    $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];

                    $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                    $obj_appointment->miles = $post_data['fmiles'];
                    $obj_appointment->typetravel = $post_data['roundtrip'];
                } else {
                    $obj_appointment->idDriverRoundTrip = NULL;
                    $obj_appointment->idDriverOneWay = NULL;
                    $obj_appointment->idCourse = NULL;
                    $obj_appointment->typeservice = NULL;
                    $obj_appointment->frate = NULL;
                    $obj_appointment->miles = NULL;
                    $obj_appointment->typetravel = NULL;
                }
                if (isset($post_data['traduction'])) {
                    if ($post_data['typeservice_trasl'] == 'Interpreting')
                        $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                    else
                        $obj_appointment->idLocation = NULL;

                    $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                    $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                    $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                } else {

                    $obj_appointment->idInterpreter = NULL;
                    $obj_appointment->idLanguage = NULL;
                    $obj_appointment->typeservicesinterpreter = NULL;
                }

                $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                //$obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                $obj_appointment->whocalls = $post_data['whocalls_app'];
                $obj_appointment->companyname = $post_data['companyname_app'];
                $obj_appointment->companyphone = $post_data['companyphone_app'];
                $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                $obj_appointment->status = self::STATUS_ACTIVE;
                $obj_appointment->save();

                $idApp = $obj_appointment->idAppointment;
                $name = $obj_appointment->companyname;
                $concat = $idApp . '' . substr($name, 0, 2);

                $obj_appointment->idAlias = $concat;
                $obj_appointment->save();


                //-----------------------NUEVO-----------------------------------               
            } else

            if (!isset($post_data['repeatapp'])) { //---------------mapea si no es cita repetida-------------------------- 
                $numapp = $this->countappointment($post_data['idPatient_newappointment_appinfo']);


                $obj_appointment = new Model_Appointment();
                if ($numapp > 0) {
                    $obj_appointment->state = 'CALLING_LIST';
                } else if ($numapp == 0) {
                    $obj_appointment->state = 'CREATED_CONFIRMATION_ADJUSTER';
                }

                if (isset($post_data['transportation'])) {
                    $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                    $obj_appointment->idDriverOneWay = $post_data['driver1'];
                    $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];

                    $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                    $obj_appointment->miles = $post_data['fmiles'];
                    $obj_appointment->typetravel = $post_data['roundtrip'];
                } else {
                    $obj_appointment->idDriverRoundTrip = NULL;
                    $obj_appointment->idDriverOneWay = NULL;
                    $obj_appointment->idCourse = NULL;
                    $obj_appointment->typeservice = NULL;
                    $obj_appointment->frate = NULL;
                    $obj_appointment->miles = NULL;
                    $obj_appointment->typetravel = NULL;
                }
                if (isset($post_data['traduction'])) {
                    if ($post_data['typeservice_trasl'] == 'Interpreting')
                        $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                    else
                        $obj_appointment->idLocation = NULL;

                    $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                    $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                    $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                } else {

                    $obj_appointment->idInterpreter = NULL;
                    $obj_appointment->idLanguage = NULL;
                    $obj_appointment->typeservicesinterpreter = NULL;
                }

                $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                //$obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                $obj_appointment->whocalls = $post_data['whocalls_app'];
                $obj_appointment->companyname = $post_data['companyname_app'];
                $obj_appointment->companyphone = $post_data['companyphone_app'];
                $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                $obj_appointment->status = self::STATUS_ACTIVE;
                $obj_appointment->save();

                $idApp = $obj_appointment->idAppointment;
                $name = $obj_appointment->companyname;
                $concat = $idApp . '' . substr($name, 0, 2);

                $obj_appointment->idAlias = $concat;
                $obj_appointment->save();
            }       //-----------------fin      
            else {

                $DateOfService = $post_data['dateregistration_appinfo'];

                if ($until_app == '0000-00-00' OR $until_app == '--')
                    $until_app = date('Y-m-d');


                if ($DateOfService == '0000-00-00' OR $DateOfService == '--')
                    $DateOfService = date('Y-m-d');
                $until = strtotime($until_app);
                $dateOfService = strtotime($DateOfService);
                $orden = 1;

                while ($dateOfService <= $until) {
                    foreach ($post_data['until'] as $k => $day) {

                        $atoday = getdate($dateOfService);

                        if ($atoday['wday'] == $day) {

                            $obj_appointment = new Model_Appointment();

                            if ($numapp > 0) {
                                $obj_appointment->state = 'CALLING_LIST';
                            } else if ($numapp == 0) {
                                $obj_appointment->state = 'CREATED_CONFIRMATION_ADJUSTER';
                            }
                            if (isset($post_data['transportation'])) {
                                $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                                $obj_appointment->idDriverOneWay = $post_data['driver1'];
                                $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];

                                $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                                $obj_appointment->miles = $post_data['fmiles'];
                                $obj_appointment->typetravel = $post_data['roundtrip'];
                            } else {
                                $obj_appointment->idDriverRoundTrip = NULL;
                                $obj_appointment->idDriverOneWay = NULL;
                                $obj_appointment->idCourse = NULL;
                                $obj_appointment->typeservice = NULL;
                                $obj_appointment->frate = NULL;
                                $obj_appointment->miles = NULL;
                                $obj_appointment->typetravel = NULL;
                            }
                            if (isset($post_data['traduction'])) {
                                if ($post_data['typeservice_trasl'] == 'Interpreting')
                                    $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                                else
                                    $obj_appointment->idLocation = NULL;

                                $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                                $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                                $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                            } else {

                                $obj_appointment->idInterpreter = NULL;
                                $obj_appointment->idLanguage = NULL;
                                $obj_appointment->typeservicesinterpreter = NULL;
                            }

                            $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                            $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                            $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                            $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                            //$obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                            $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                            $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                            $obj_appointment->whocalls = $post_data['whocalls_app'];
                            $obj_appointment->companyname = $post_data['companyname_app'];
                            $obj_appointment->companyphone = $post_data['companyphone_app'];
                            $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                            $obj_appointment->status = self::STATUS_ACTIVE;
                            $obj_appointment->save();

                            $idApp = $obj_appointment->idAppointment;
                            $name = $obj_appointment->companyname;
                            $concat = $idApp . '' . substr($name, 0, 2);

                            $obj_appointment->idAlias = $concat;
                            $obj_appointment->save();
                        }
                    }
                    $dateOfService += 24 * 60 * 60;
                }
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_listApp() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $date = $this->request->query('date');
        if (!empty($date))
            $q_date = $this->cambiafmysqlre($date);
        else
            $q_date = getdate();

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idAppointment' => "app.idAppointment",
                'appointmentdate' => "app.appointmentdate",
                'appointmenttime' => "app.appointmenttime",
                'namepatient' => "CONCAT(pat.firstname, ' ', pat.lastname)",
                'addresspatient' => "pat.address",
                'phonepatient' => "pat.phonenumber",
//                'contactname' => "loc2course.alias",
//                'contactaddress' => "loc2course.address",
//                'contactphone' => "loc2course.telephone",
                'contactname' => "IF(app.idLocation IS NULL,loc2course.alias,loc.alias)",
                'contactaddress' => "IF(app.idLocation IS NULL,loc2course.address,loc.address)",
                'contactphone' => "IF(app.idLocation IS NULL,loc2course.telephone, loc.telephone)",
                'namedriver1' => "CONCAT(dr1.name, ' ', dr1.lastname)",
                'phonedriver1' => "dr1.phone",
                'namedriver2' => "CONCAT(dr2.name, ' ', dr2.lastname)",
                'phonedriver2' => "dr2.phone",
                'nameinterprete' => "CONCAT(inter.name, ' ', inter.lastname)",
                'phoneinterprete' => "inter.phone",
                'miles' => 'app.miles',
                'transportation' => "IF(app.idCourse IS NULL,0,1)",
                'translation' => "IF(app.idLanguage IS NULL,0,1)",
                'typetravel' => 'app.typetravel',
                'showdriver' => 'app.showdriver',
                'showinterpreter' => 'app.showinterpreter'
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }
            /*             * **********  VARIABLE PARA EL WHERE   ***************** */
            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "appointment app" .
                    $s_tables_join = " INNER JOIN patients pat ON app.idPatient=pat.idPatient " .
                    $s_tables_join = " LEFT JOIN course c ON app.idCourse=c.idCourse  " .
                    $s_tables_join = " LEFT JOIN locations loc ON loc.idLocation=app.idLocation " .
                    $s_tables_join = " LEFT JOIN locations loc1course ON loc1course.idLocation=c.idLocationOrigen " .
                    $s_tables_join = " LEFT JOIN locations loc2course ON loc2course.idLocation=c.idLocationDestination " .
                    $s_tables_join = " LEFT JOIN drivers dr1 ON app.idDriverRoundTrip=dr1.idDriver AND app.idDriverOneWay=dr1.idDriver  " .
                    $s_tables_join = " LEFT JOIN drivers dr2 ON app.idDriverRoundTrip=dr2.idDriver AND app.idDriverOneWay=dr2.idDriver  " .
                    $s_tables_join = " LEFT JOIN interpreters inter ON app.idInterpreter=inter.idInterpreter ";
            $s_where_conditions = " app.state ='CONFIRMED' AND app.appointmentdate ='" . $q_date . "'";


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
//            echo Database::instance()->last_query;
//            die();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_listNotes() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        $tablename = $this->request->query('tname');
        $idApp = $this->request->query('idApp');

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idNote' => "t.idNotes",
                'description' => "t.description",
                'dateresgistration' => "t.dateRegistration",
                'user' => "u.userName",
            );
//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = $tablename . " t" .
                    $s_tables_join = " LEFT JOIN bts_user u ON t.idUsers=u.idUser";
            $s_where_conditions = " t.status=1 AND t.idAppointment=" . $idApp;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_getIdNotesPat() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notespatients = new Model_NotesPatients($idNote);
            $result = $o_notespatients->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesCon() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notescontacts = new Model_NotesContacts($idNote);
            $result = $o_notescontacts->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesDri() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesdrivers = new Model_NotesDrivers($idNote);
            $result = $o_notesdrivers->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesInt() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesinterpretes = new Model_NotesInterpreters($idNote);
            $result = $o_notesinterpretes->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesEmp() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesemployeers = new Model_NotesEmployeers($idNote);
            $result = $o_notesemployeers->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesPatients() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desPat = $this->request->post("descriptionPat");
            $idApp = $this->request->post("idAppointment");
            $idNotes = $this->request->post("idNotes");
            if ($idNotes > 0) {
//Edicion
                $o_notespatients = new Model_NotesPatients($idNotes);

                if (!$o_notespatients->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {
//Nuevo
                $o_notespatients = new Model_NotesPatients();
                $o_notespatients->dateRegistration = date('Y-m-d H:i:s');
                $o_notespatients->status = self::STATUS_ACTIVE;
                $o_notespatients->idUsers = $this->getSessionParameter(self::USER_ID);
            }
            $o_notespatients->description = $desPat;
            $o_notespatients->idAppointment = $idApp;
            $o_notespatients->save();

            $a_response['data'] = array(
                'idNote' => $o_notespatients->idNote
                , 'description' => $o_notespatients->description
                , 'idApp' => $o_notespatients->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesPat() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notespatients = new Model_NotesPatients($idNote);

            if (!$o_notespatients->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notespatients->status = self::STATUS_DEACTIVE;
            $o_notespatients->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesContacts() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desContact = $this->request->post("descriptionCon");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notescontacts = new Model_NotesContacts($idNote);

                if (!$o_notescontacts->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {
//Nuevo
                $o_notescontacts = new Model_NotesContacts();
                $o_notescontacts->dateRegistration = date('Y-m-d H:i:s');
                $o_notescontacts->status = self::STATUS_ACTIVE;
                $o_notescontacts->idUsers = $this->getSessionParameter(self::USER_ID);
            }
            $o_notescontacts->idAppointment = $idApp;
            $o_notescontacts->description = $desContact;
            $o_notescontacts->save();
            $a_response['data'] = array(
                'idNote' => $o_notescontacts->idNote
                , 'description' => $o_notescontacts->description
                , 'idApp' => $o_notescontacts->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesCon() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notescontacts = new Model_NotesContacts($idNote);

            if (!$o_notescontacts->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notescontacts->status = self::STATUS_DEACTIVE;
            $o_notescontacts->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesDrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desDrivers = $this->request->post("descriptionDri");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notesdrivers = new Model_NotesDrivers($idNote);

                if (!$o_notesdrivers->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesdrivers->description = $desDrivers;
                $o_notesdrivers->idAppointment = $idApp;
                $o_notesdrivers->save();
            } else {
//Nuevo
                $o_notesdrivers = new Model_NotesDrivers();
                $o_notesdrivers->description = $desDrivers;
                $o_notesdrivers->dateRegistration = date('Y-m-d H:i:s');
                $o_notesdrivers->idAppointment = $idApp;
                $o_notesdrivers->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesdrivers->status = self::STATUS_ACTIVE;
                $o_notesdrivers->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesdrivers->idNote
                , 'description' => $o_notesdrivers->description
                , 'idApp' => $o_notesdrivers->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_deleteNotesDri() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesdrivers = new Model_NotesDrivers($idNote);

            if (!$o_notesdrivers->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesdrivers->status = self::STATUS_DEACTIVE;
            $o_notesdrivers->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesInterpreters() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desInterpretes = $this->request->post("descriptionInt");
            $idApp = $this->request->post("idAppointment");
            $idNotes = $this->request->post("idNotes");

            if ($idNotes > 0) {
//Edicion
                $o_notesinterpretes = new Model_NotesInterpreters($idNotes);

                if (!$o_notesinterpretes->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesinterpretes->description = $desInterpretes;
                $o_notesinterpretes->idAppointment = $idApp;
                $o_notesinterpretes->save();
            } else {
//Nuevo
                $o_notesinterpretes = new Model_NotesInterpreters();
                $o_notesinterpretes->description = $desInterpretes;
                $o_notesinterpretes->dateRegistration = date('Y-m-d H:i:s');
                $o_notesinterpretes->idAppointment = $idApp;
                $o_notesinterpretes->status = self::STATUS_ACTIVE;
                $o_notesinterpretes->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesinterpretes->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesinterpretes->idNote
                , 'description' => $o_notesinterpretes->description
                , 'idApp' => $o_notesinterpretes->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesInt() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesinterpreters = new Model_NotesInterpreters($idNote);

            if (!$o_notesinterpreters->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesinterpreters->status = self::STATUS_DEACTIVE;
            $o_notesinterpreters->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesEmployeers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desEmployeers = $this->request->post("descriptionEmp");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notesemployeers = new Model_NotesEmployeers($idNote);

                if (!$o_notesemployeers->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesemployeers->description = $desEmployeers;
                $o_notesemployeers->idAppointment = $idApp;
                $o_notesemployeers->save();
            } else {
//Nuevo
                $o_notesemployeers = new Model_NotesEmployeers();
                $o_notesemployeers->description = $desEmployeers;
                $o_notesemployeers->dateRegistration = date('Y-m-d H:i:s');
                $o_notesemployeers->idAppointment = $idApp;
                $o_notesemployeers->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesemployeers->status = self::STATUS_ACTIVE;
                $o_notesemployeers->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesemployeers->idNote
                , 'description' => $o_notesemployeers->description
                , 'idApp' => $o_notesemployeers->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesEmp() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesemployeers = new Model_NotesInterpretes($idNote);

            if (!$o_notesemployeers->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesemployeers->status = self::STATUS_DEACTIVE;
            $o_notesemployeers->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

    public function getDataClaimforApp($idapp) {

        $cons = DB::select(array('cl.idAdjuster', 'idAdjuster'), array('cl.idCasemanager', 'idCasemanager'), array('cl.idPatient', 'idPatient'), array('cl.idClaim', 'idClaim'), array('cl.idInsuranceCarrier', 'idInsuranceCarrier')
                        )->distinct(true)
                        ->from(array('claims', 'cl'))
                        ->join(array('appointment', 'app'), 'LEFT')
                        ->on('cl.idClaim', '=', 'app.idClaim')
                        ->where('app.idAppointment', '=', DB::expr($idapp))
                        ->execute()->current();
        return $cons;
    }

    public function action_appointmentInformation() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idApointment_app = $this->request->post('idApp');


            $data_claim = $this->getDataClaimforApp($idApointment_app);

            $idPatient_app = $data_claim['idPatient'];
            $idClaim_app = $data_claim['idClaim'];
            $idInsurancecarrier_app = $data_claim['idInsuranceCarrier'];
            $idCasemanager_app = $data_claim['idCasemanager'];
            $idAdjuster_app = $data_claim['idAdjuster'];



            $o_patient = new Model_Patients($idPatient_app);
            $o_claim = new Model_Claims($idClaim_app);
            $o_insurancecarrier = new Model_Insurancecarriers($idInsurancecarrier_app);
            $o_casemanager = new Model_Casemanagers($idCasemanager_app);
            $o_adjuster = new Model_Adjusters($idAdjuster_app);


            $prueba = $o_patient->as_array();
            $prueba_claim = $o_claim->as_array();
            $prueba_insurancecarrier = $o_insurancecarrier->as_array();
            $prueba_casemanager = $o_casemanager->as_array();
            $prueba_adjuster = $o_adjuster->as_array();

            $a_response['data'] = $prueba;
            $a_response['data_claim'] = $prueba_claim;
            $a_response['data_insurancecarrier'] = $prueba_insurancecarrier;
            $a_response['data_casemanager'] = $prueba_casemanager;
            $a_response['data_adjuster'] = $prueba_adjuster;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdUpdateAppointment() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idAppointment = $this->request->post('idapp');
            $o_appointment = new Model_Appointment($idAppointment);
            $prueba = $o_appointment->as_array();

            $idlocation = $prueba['idLocation'];
            $idcourse = $prueba['idCourse'];
            if (isset($idcourse)) {
                $obj_course = new Model_Course($idcourse);
                $data_course = $obj_course->as_array();
                $obj_location1 = new Model_Locations($data_course['idLocationOrigen']);
                $data_location1 = $obj_location1->as_array();
                $a_response['location1'] = $data_location1;
                $obj_location2 = new Model_Locations($data_course['idLocationDestination']);
                $data_location2 = $obj_location2->as_array();
                $a_response['location2'] = $data_location2;
            }
            if (isset($idlocation)) {
                $obj_location = new Model_Locations($idlocation);
                $data_location = $obj_location->as_array();
                $a_response['location'] = $data_location;
            }

            $a_response['data'] = $prueba;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_reportPrintAppbyDate() {

        $this->auto_render = FALSE;
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));

            $manifest = array();
            $manifest_date = $this->request->query('date');

            $manifest['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $manifest['qlang'] = $this->request->query(self::RQ_LANG);
            $manifest['searchDate'] = date('l, F d, Y');
            $manifest['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $manifest['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            $manifest['search_filter'] = $manifest_date;



            $all = $this->request->query('all');

            $parameters = array(
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE,
                ':s_confimed' => self::CONFIRMED
            );

            $columns = array(
                array('user.userName', 'userName'),
                array('app.idAlias', 'alias'),
                DB::expr("CONCAT(pat.firstname, ' ', pat.lastname) AS namepatient"),
                array('app.pickuptime', 'pickuptime'),
                array('app.droptime', 'droptime'),
                array('app.appointmenttime', 'apptime'),
                array('app.typeservice', 'typeservice'),
                array('app.miles', 'millage'),
                array('app.notescalling', 'notescalling'),
                array('ic.name', 'insurance_name'),
                array('tapp.name', 'typeappointment'),
                array('courseDestination.address', 'addressCourseDestination'),
                array('courseDestination.telephone', 'telephoneCourseDestination'),
                array('courseOrigen.address', 'addressCourseOrigen'),
                array('courseOrigen.telephone', 'telephoneCourseOrigen'),
                array('locations.address', 'addressLocationDestination'),
                array('locations.telephone', 'telephoneLocationDestination'),
                DB::expr("CONCAT(dr1.name, ' ', dr1.lastname) AS namedriver1"),
                array('dr1.phone', 'phonedriver1'),
                DB::expr("CONCAT(dr2.name, ' ', dr2.lastname) AS namedriver2"),
                array('dr2.phone', 'phonedriver2'),
                DB::expr("CONCAT(inter.name, ' ', inter.lastname) AS nameinterprete"),
                array('inter.phone', 'phoneinterprete'),
                //DB::expr("COUNT(app.) AS isInterpreter"),
                DB::expr("IF(app.idInterpreter IS NOT NULL, :s_active, :s_inactive) AS isInterpreter"),
                DB::expr("IF(app.idDriverOneWay IS NOT NULL, :s_active, :s_inactive) AS isDriver"),
                DB::expr("IF(app.idCourse IS NOT NULL, :s_active, :s_inactive) AS isCourse"),
                DB::expr("IF(app.idLocation IS NOT NULL, :s_active, :s_inactive) AS isLocation")
            );


            $sql_sub_course_Dest = Db::select('locations.idLocation', 'locations.address', 'locations.telephone')
                    ->from('appointment')
                    ->join('course')->on('course.idCourse', '=', 'appointment.idCourse')
                    ->join('locations', 'LEFT')->on('locations.idLocation', '=', 'course.idLocationDestination')
                    ->where('appointment.appointmentdate', '=', $this->cambiafmysqlre($manifest_date))
                    ->and_where('appointment.state', '=', ':s_confimed')
                    ->and_where_open()
                    ->or_where('appointment.idDriverRoundTrip', 'IS NOT', NULL)
                    ->or_where('appointment.idDriverOneWay', 'IS NOT', NULL)
                    ->and_where_close()
                    ->parameters($parameters);

            $sql_sub_course_Orig = Db::select('locations.idLocation', 'locations.address', 'locations.telephone')
                    ->from('appointment')
                    ->join('course')->on('course.idCourse', '=', 'appointment.idCourse')
                    ->join('locations', 'LEFT')->on('locations.idLocation', '=', 'course.idLocationOrigen')
                    ->where('appointment.appointmentdate', '=', $this->cambiafmysqlre($manifest_date))
                    ->and_where('appointment.state', '=', ':s_confimed')
                    ->and_where_open()
                    ->or_where('appointment.idDriverRoundTrip', 'IS NOT', NULL)
                    ->or_where('appointment.idDriverOneWay', 'IS NOT', NULL)
                    ->and_where_close()
                    ->parameters($parameters);


            $sql_sub_location = DB::select(
                            'locations.idLocation', 'locations.address', 'locations.telephone')
                    ->from('appointment')
                    ->join('locations')->on('locations.idLocation', '=', 'appointment.idLocation')
                    ->where('appointment.appointmentdate', '=', $this->cambiafmysqlre($manifest_date))
                    ->and_where('appointment.state', '=', ':s_confimed')
                    ->and_where_open()
                    ->and_where('appointment.idDriverRoundTrip', 'IS', NULL)
                    ->and_where('appointment.idDriverOneWay', 'IS', NULL)
                    ->and_where_close()
                    ->parameters($parameters);

            $sql_manifest = DB::select_array($columns)
                            ->from(array('appointment', 'app'))
                            ->join(array('claims', 'cl'))->on('cl.idClaim', '=', 'app.idClaim')
                            ->join(array('insurancecarrier', 'ic'))->on('ic.idInsurancecarrier', '=', 'cl.idInsuranceCarrier')
                            ->join(array('patients', 'pat'))->on('pat.idPatient', '=', 'app.idPatient')
                            ->join(array('typeappointment', 'tapp'), 'LEFT')->on('app.idTypeAppointment', '=', 'tapp.idTypeappointment')
                            ->join(array('drivers', 'dr1'), 'LEFT')->on('app.idDriverOneWay', '=', 'dr1.idDriver')
                            ->join(array('drivers', 'dr2'), 'LEFT')->on('app.idDriverRoundTrip', '=', 'dr2.idDriver')
                            ->join(array('interpreters', 'inter'), 'LEFT')->on('app.idInterpreter', '=', 'inter.idInterpreter')
                            ->join(array('course', 'cou'), 'LEFT')->on('app.idCourse', '=', 'cou.idCourse')
                            ->join(array($sql_sub_course_Dest, 'courseDestination'), 'LEFT')->on('courseDestination.idLocation', '=', 'cou.idLocationDestination')
                            ->join(array($sql_sub_course_Orig, 'courseOrigen'), 'LEFT')->on('courseOrigen.idLocation', '=', 'cou.idLocationOrigen')
                            ->join(array('locations', 'loc'), 'LEFT')->on('app.idLocation', '=', 'loc.idLocation')
                            ->join(array($sql_sub_location, 'locations'), 'LEFT')->on('locations.idLocation', '=', 'loc.idLocation')
                            ->join(array('bts_user', 'user'))->on('user.idUser', '=', 'app.userCreate')
                            ->where('app.appointmentdate', '=', $this->cambiafmysqlre($manifest_date))
                            ->and_where('app.state', '=', ':s_confimed')
                            ->parameters($parameters)
                            ->execute()->as_array();

            $this->fnResponseFormat($sql_manifest, self::REPORT_RESPONSE_TYPE_XML, $manifest);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

}
