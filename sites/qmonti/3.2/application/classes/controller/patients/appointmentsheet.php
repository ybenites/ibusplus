<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Appointmentsheet extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('appointmentsheet', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('appointmentsheet', self::FILE_TYPE_JS));
        $view_appointmentsheet = new View('patients/appointmentsheet');

        $case_manager = DB::select(array('idCasemanager', 'idCasemanager'), array('name', 'name'))
                        ->from("casemanagers")->where('status', '=', '1')
                        ->execute()->as_array();

        $adjuster = DB::select(array('idAdjuster', 'idAdjuster'), array('name', 'name'))
                        ->from("adjusters")->where('status', '=', '1')
                        ->execute()->as_array();

        $insurance_carrier = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();

        $drivers = DB::select(array('idDriver', 'idDriver'), array('name', 'name'), array('lastname', 'lastname'))
                        ->from("drivers")->where('status', '=', '1')
                        ->execute()->as_array();

        $type_appointment = DB::select(array('idTypeappointment', 'id'), array('name', 'nameapp'))
                        ->from("typeappointment")->where('status', '=', '1')
                        ->execute()->as_array();

        $languaje = DB::select(array('idLanguage', 'idLanguage'), array('name', 'name'))
                        ->from("languages")->where('status', '=', '1')
                        ->execute()->as_array();

        $interprete = DB::select(array('idInterpreter', 'idInterpreter'), array('name', 'name'))
                        ->from("interpreters")->where('status', '=', '1')
                        ->execute()->as_array();

        $area_driver = DB::select(array('idAreasdriver', 'id'), array('location', 'area'))
                        ->from("areasdriver")->where('status', '=', '1')
                        ->execute()->as_array();

        $w_time_code = strtoupper(base_convert(rand(1, 9) . date('Hisymd'), 10, 32));


        $view_appointmentsheet->area_driver = $area_driver;
        $view_appointmentsheet->insurance_carrier = $insurance_carrier;
        $view_appointmentsheet->case_manager = $case_manager;
        $view_appointmentsheet->adjuster = $adjuster;
        $view_appointmentsheet->drivers = $drivers;
        $view_appointmentsheet->type_appointment = $type_appointment;
        $view_appointmentsheet->languaje = $languaje;
        $view_appointmentsheet->interprete = $interprete;
        $view_appointmentsheet->w_time_code = $w_time_code;

        $this->template->content = $view_appointmentsheet;
    }

    public function action_savelanguaje() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $s_name = $this->request->post("name");
            $s_type = $this->request->post("type");
            $i_language_id = $this->request->post("idLanguage");

            if ($i_language_id > 0) {
//Edicion
                $o_languages = new Model_Languages($i_language_id);

                if (!$o_languages->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_languages->name = $s_name;
                $o_languages->type = $s_type;
                $o_languages->save();
            } else {
//Nuevo
                $o_languages = new Model_Languages();
                $o_languages->name = $s_name;
                $o_languages->type = $s_type;
                $o_languages->status = self::STATUS_ACTIVE;
                $o_languages->save();
            }

            $a_response['data'] = array(
                'idLanguage' => $o_languages->idLanguage
                , 'name' => $o_languages->name
                , 'type' => $o_languages->type
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_consult() {
        try {


            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $s_var = $this->request->query('id');

            $i_idtask = $this->request->query('task_id');

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idClaim' => "c.idClaim",
                'status' => "c.status",
                'claimnumber' => "c.claimnumber",
                'injury' => "c.injury",
                'datefinish' => "c.datefinish",
                //'datefinish' => "IF(app.idLocation IS NULL,loc2course.alias,loc.alias)",
                'name' => "ic.name",
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            /*             * **********  VARIABLE PARA EL WHERE   ***************** */
            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "claims c" .
                    $s_tables_join = " INNER JOIN insurancecarrier ic ON c.idInsuranceCarrier = ic.idInsuranceCarrier" .
                    $s_tables_join = " INNER JOIN patients p ON c.idPatient = p.idPatient ";
            $s_where_conditions = "TRUE";
            if ($s_all == 'false')
                $s_where_conditions = " c.status = " . self::STATUS_ACTIVE . " AND p.idPatient = " . $s_var;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_consult2() {
        try {


            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            $s_all = $this->request->query('all');

            $s_var = $this->request->query('id');

            $i_idtask = $this->request->query('task_id');

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idAppointment' => "app.idAppointment",
                'status' => "app.status",
                'appointmentdate' => "app.appointmentdate",
                'appointmenttime' => "app.appointmenttime",
                'namepatient' => "CONCAT(pat.firstname, ' ', pat.lastname)",
                'phonepatient' => "pat.phonenumber",
                'contact' => "CONCAT('from: ',loc1course.address, ' ','to: ', loc2course.address)",
                'namedriver' => "CONCAT(dr.name, ' ', dr.lastname)",
                'phonedriver' => "dr.phone",
                'nameinterprete' => "CONCAT(inter.name, ' ', inter.lastname)",
                'phoneinterprete' => "inter.phone",
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "appointment app" .
                    $s_tables_join = " INNER JOIN patients pat ON app.idPatient=pat.idPatient " .
                    $s_tables_join = " LEFT JOIN course c ON app.idCourse=c.idCourse  " .
                    $s_tables_join = " LEFT JOIN locations loc ON loc.idLocation=app.idLocation " .
                    $s_tables_join = " LEFT JOIN locations loc1course ON loc1course.idLocation=c.idLocationOrigen " .
                    $s_tables_join = " LEFT JOIN locations loc2course ON loc2course.idLocation=c.idLocationDestination " .
                    $s_tables_join = " LEFT JOIN drivers dr ON app.idDriverRoundTrip=dr.idDriver AND app.idDriverOneWay=dr.idDriver  " .
                    $s_tables_join = " LEFT JOIN interpreters inter ON app.idInterpreter=inter.idInterpreter ";
            $s_where_conditions = "TRUE";
            if ($s_all == 'false')
                $s_where_conditions = " app.status = " . self::STATUS_ACTIVE . " AND app.idClaim = " . $s_var;

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

//            echo $i_start.'===='.$i_total_pages;
//            die();
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_search() {

        $var = $this->request->post('namepatient');

        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE
        $a_response['data'] = DB::select(
                                array('p.idPatient', 'idPatient'), array('p.firstname', 'firstname')
                                , array('p.lastname', 'lastname')
                                , array('p.socialsecurity', 'socialsecurity')
                        )->distinct(true)
                        ->from(array("claims", 'c'))
                        ->join(array("insurancecarrier", 'ic'))
                        ->on('c.idInsuranceCarrier', '=', 'ic.idInsuranceCarrier')
                        ->join(array("patients", "p"))
                        ->on("c.idPatient", "=", "p.idPatient")
                        ->where('c.status', '=', '1')
                        ->and_where_open()
                        ->or_where('p.firstname', 'like', DB::expr('"%' . $var . '%"'))
                        ->or_where('p.lastname', 'like', DB::expr('"%' . $var . '%"'))
                        ->or_where('p.socialsecurity', 'like', DB::expr('"%' . $var . '%"'))
                        ->and_where_close()
                        ->limit(20)
                        ->execute()->as_array();

        $this->fnResponseFormat($a_response);
    }

    public function action_search_location() {


        $query = $this->request->post('query');
        $idcontact = $this->request->post('idContact');
        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE
        $a_response['data'] = DB::select(
                                array('loc_or.idLocation', 'idLocation')
                                , array('loc_or.alias', 'alias')
                                , array('loc_or.address', 'address')
                                , array('loc_or.suite', 'suite')
                                , array('loc_or.telephone', 'telephone')
                                , array('c.idCourse', 'idCourse')
                        )->distinct(true)
                        ->from(array("locations", 'loc_or'))
                        ->join(array('locations', 'loc_dest'), 'LEFT')
                        ->on('loc_dest.idLocation', '=', DB::expr('"' . $idcontact . '"'))
                        ->join(array('course', 'c'), 'LEFT')
                        ->on('c.idLocationOrigen', '=', DB::expr('loc_or.idLocation AND c.idLocationDestination = loc_dest.idLocation'))
//->where('l.status', '=', '1')
                        ->and_where_open()
                        ->or_where('loc_or.address', 'like', DB::expr('"%' . $query . '%"'))
                        ->or_where('loc_or.alias', 'like', DB::expr('"%' . $query . '%"'))
                        ->and_where_close()
                        ->limit(20)
                        ->execute()->as_array();



        $this->fnResponseFormat($a_response);
    }

    public function action_search_contact() {

        $query = $this->request->post('query');
        $idcontact = $this->request->post('idLocation');
        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE
        $a_response['data'] = DB::select(
                                array('loc_dest.idLocation', 'idLocation')
                                , array('loc_dest.alias', 'alias')
                                , array('loc_dest.address', 'address')
                                , array('loc_dest.suite', 'suite')
                                , array('loc_dest.telephone', 'telephone')
                                , array('c.idCourse', 'idCourse')
                        )->distinct(true)
                        ->from(array("locations", 'loc_dest'))
                        ->join(array('locations', 'loc_or'), 'LEFT')
                        ->on('loc_or.idLocation', '=', DB::expr('"' . $idcontact . '"'))
                        ->join(array('course', 'c'), 'LEFT')
                        ->on('c.idLocationOrigen', '=', DB::expr('loc_or.idLocation AND c.idLocationDestination = loc_dest.idLocation'))
//->where('l.status', '=', '1')
                        ->and_where_open()
                        ->or_where('loc_dest.address', 'like', DB::expr('"%' . $query . '%"'))
                        ->or_where('loc_dest.alias', 'like', DB::expr('"%' . $query . '%"'))
                        ->and_where_close()
                        ->limit(20)
                        ->execute()->as_array();
        $this->fnResponseFormat($a_response);
    }

    public function action_getId() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idClaim = $this->request->post('idClaim');
            $o_claim = new Model_Claims($idClaim);
            $prueba = $o_claim->as_array();

            $a_response['data'] = $prueba;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdUpdateAppointment() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idAppointment = $this->request->post('idapp');
            $o_appointment = new Model_Appointment($idAppointment);
            $prueba = $o_appointment->as_array();

            $idlocation = $prueba['idLocation'];
            $idcourse = $prueba['idCourse'];
            if (isset($idcourse)) {
                $obj_course = new Model_Course($idcourse);
                $data_course = $obj_course->as_array();
                $obj_location1 = new Model_Locations($data_course['idLocationOrigen']);
                $data_location1 = $obj_location1->as_array();
                $a_response['location1'] = $data_location1;
                $obj_location2 = new Model_Locations($data_course['idLocationDestination']);
                $data_location2 = $obj_location2->as_array();
                $a_response['location2'] = $data_location2;
            }
            if (isset($idlocation)) {
                $obj_location = new Model_Locations($idlocation);
                $data_location = $obj_location->as_array();
                $a_response['location'] = $data_location;
            }

            $a_response['data'] = $prueba;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNewAppointment() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idPatient_app = $this->request->post('idPatient_app');
            $idClaim_app = $this->request->post('idClaim_app');
            $idInsurancecarrier_app = $this->request->post('idInsurancecarrier_app');
            $idCasemanager_app = $this->request->post('idCasemanager_app');
            $idAdjuster_app = $this->request->post('idAdjuster_app');

            $o_patient = new Model_Patients($idPatient_app);
            $o_claim = new Model_Claims($idClaim_app);
            $o_insurancecarrier = new Model_Insurancecarriers($idInsurancecarrier_app);
            $o_casemanager = new Model_Casemanagers($idCasemanager_app);
            $o_adjuster = new Model_Adjusters($idAdjuster_app);


            $prueba = $o_patient->as_array();
            $prueba_claim = $o_claim->as_array();
            $prueba_insurancecarrier = $o_insurancecarrier->as_array();
            $prueba_casemanager = $o_casemanager->as_array();
            $prueba_adjuster = $o_adjuster->as_array();

            $a_response['data'] = $prueba;
            $a_response['data_claim'] = $prueba_claim;
            $a_response['data_insurancecarrier'] = $prueba_insurancecarrier;
            $a_response['data_casemanager'] = $prueba_casemanager;
            $a_response['data_adjuster'] = $prueba_adjuster;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_delete() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_claim_id = $this->request->post("id");

            $o_claim = new Model_Claims($i_claim_id);

            if (!$o_claim->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_claim->status = self::STATUS_DEACTIVE;
                $o_claim->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_save() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post());
        try {
            $post_data = $post->data();
            /* Capturo el idClaim */
            $claim_id = $post_data['idClaim'];

            if ($claim_id > 0) {
//Edicion
                $obj_claim = new Model_Claims($claim_id);
                if (!$obj_claim->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                $obj_claim->idCasemanager = $post_data['idCasemanager'];
                $obj_claim->idAdjuster = $post_data['idAdjuster'];
                $obj_claim->unlimited = $post_data['unlimited'];
                $obj_claim->unlimitedapp = $post_data['unlimitedapp'];
                $obj_claim->datefinish = $post_data['datefinish'];
                $obj_claim->maxappointments = $post_data['maxappointments'];
                $obj_claim->save();
            }
            else {

                $obj_claim = new Model_Claims();
                $obj_claim->idInsuranceCarrier = $post_data['idInsuranceCarrier'];
                $obj_claim->idPatient = $post_data['idPatient'];
                $obj_claim->dateregistration = $post_data['dateregistration'];
                $obj_claim->claimnumber = $post_data['claimnumber'];
                $obj_claim->authorizedby = $post_data['authorizedby'];
                $obj_claim->injury = $post_data['injury'];
                $obj_claim->idCasemanager = $post_data['idCasemanager'];
                $obj_claim->idAdjuster = $post_data['idAdjuster'];
                $obj_claim->unlimited = $post_data['unlimited'];
                $obj_claim->unlimitedapp = $post_data['unlimitedapp'];
                $obj_claim->datefinish = $post_data['datefinish'];
                $obj_claim->maxappointments = $post_data['maxappointments'];
                $obj_claim->status = self::STATUS_ACTIVE;
                $obj_claim->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_getIdClaim() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idClaim = $this->request->post('id');
            $o_claim = new Model_Drivers($idClaim);
            $prueba = $o_claim->as_array();

            $a_response['data'] = $prueba;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_createUpdateTypeappointment() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post_data = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty')
                    ->rule('type', 'not_empty');

            if (!$post_data->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {

                $o_model_Typeappointment = new Model_Typeappointment();
                $rpta = $o_model_Typeappointment->createTypeappointment($post_data);
                $rpta->reload();

                $a_response['data'] = array(
                    'idTypeappointment' => $rpta->idTypeappointment
                    , 'name' => $rpta->name
                );
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }

        $this->fnResponseFormat($a_response);
    }

    public function countappointment($idpat) {

        $cons = DB::select(array(DB::expr('COUNT(*)'), 'count'))->distinct(true)
                        ->from(array('appointment', 'app'))
                        ->where('app.idPatient', '=', $idpat)
                        ->execute()->current();
        return $cons;
    }

    public function action_saveappointment() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $post = Validation::factory($this->request->post());
        try {
            $post_data = $post->data();
            $appointment_id = $post_data['idAppointment'];
            $until_app = $post_data['until_app'];

            if ($appointment_id > 0) {
//Edicion

                $obj_appointment = new Model_Appointment($appointment_id);
                if (!$obj_appointment->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                if (isset($post_data['transportation'])) {
                    $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                    $obj_appointment->idDriverOneWay = $post_data['driver1'];
                    $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];
                    $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                    $obj_appointment->miles = $post_data['fmiles'];
                    $obj_appointment->typetravel = $post_data['roundtrip'];
                    $obj_appointment->typeservice = $post_data['for_typeservice'];
                } else {
                    $obj_appointment->idDriverRoundTrip = NULL;
                    $obj_appointment->idDriverOneWay = NULL;
                    $obj_appointment->idCourse = NULL;
                    $obj_appointment->typeservice = NULL;
                    $obj_appointment->frate = NULL;
                    $obj_appointment->miles = NULL;
                    $obj_appointment->typetravel = NULL;
                }

                if (isset($post_data['traduction'])) {
                    if ($post_data['typeservice_trasl'] == 'Interpreting')
                        $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                    else {
                        $obj_appointment->idLocation = NULL;
                    }

                    $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                    $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                    $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                } else {

                    $obj_appointment->idInterpreter = NULL;
                    $obj_appointment->idLanguage = NULL;
                    $obj_appointment->typeservicesinterpreter = NULL;
                }

                $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                $obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                $obj_appointment->whocalls = $post_data['whocalls_app'];
                $obj_appointment->companyname = $post_data['companyname_app'];
                $obj_appointment->companyphone = $post_data['companyphone_app'];
                $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                $obj_appointment->pickuptime = $post_data['pick_up_time'];
                $obj_appointment->droptime = $post_data['final'];
                $obj_appointment->dateregistration = date('y/m/d H:i:s');
                $obj_appointment->callinglistconfirmationdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationpatient = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationcontact = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedtranslationsummary = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->billingtransp = self::STATUS_DEACTIVE;
                $obj_appointment->billinginterp = self::STATUS_DEACTIVE;
                $obj_appointment->showinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->showdriver = self::STATUS_DEACTIVE;
                $obj_appointment->status = self::STATUS_ACTIVE;
                if (isset($post_data['for_lbase'])) {
                    $obj_appointment->lbase = $post_data['for_lbase'];
                } else {
                    $obj_appointment->lbase = NULL;
                }
                if (isset($post_data['for_cassistance'])) {
                    $obj_appointment->cassistance = $post_data['for_cassistance'];
                } else {
                    $obj_appointment->cassistance = NULL;
                }
                if (isset($post_data['for_wtime']) && $post_data['for_wtime'] == 'on') {
                    $obj_appointment->wtime = self::STATUS_ACTIVE;
                    $obj_appointment->wtimecode = $post_data['wtime_code'];
                    $obj_appointment->wtimehours = $post_data['wtime_hours'];
                    $obj_appointment->wtimeminutes = $post_data['wtime_minutes'];
                } else {
                    $obj_appointment->wtime = self::STATUS_DEACTIVE;
                }

                $obj_appointment->save();

                $idApp = $obj_appointment->idAppointment;
                $name = $obj_appointment->companyname;
                $concat = $idApp . '' . substr($name, 0, 2);

                $obj_appointment->idAlias = $concat;
                $obj_appointment->save();

                //-----------------------NUEVO-----------------------------------  
            } else


            if (!isset($post_data['repeatapp'])) {
                //---------------mapea si no es cita repetida-------------------------- 
                $obj_appointment = new Model_Appointment();
                $numapp = $this->countappointment($post_data['idPatient_newappointment_appinfo']);
                if ($numapp > 0) {
                    $obj_appointment->state = 'CALLING_LIST';
                } else if ($numapp == 0) {
                    $obj_appointment->state = 'CREATED_CONFIRMATION_ADJUSTER';
                }
                if (isset($post_data['transportation'])) {
                    $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                    $obj_appointment->idDriverOneWay = $post_data['driver1'];
                    $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];
                    $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                    $obj_appointment->miles = $post_data['fmiles'];
                    $obj_appointment->typetravel = $post_data['roundtrip'];
                    $obj_appointment->typeservice = $post_data['for_typeservice'];
                } else {
                    $obj_appointment->idDriverRoundTrip = NULL;
                    $obj_appointment->idDriverOneWay = NULL;
                    $obj_appointment->idCourse = NULL;
                    $obj_appointment->typeservice = NULL;
                    $obj_appointment->frate = NULL;
                    $obj_appointment->miles = NULL;
                    $obj_appointment->typetravel = NULL;
                }
                if (isset($post_data['traduction'])) {
                    if (($post_data['typeservice_trasl'] == 'Interpreting') && (!isset($post_data['transportation'])) )
                        $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                    else
                        $obj_appointment->idLocation = NULL;

                    $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                    $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                    $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                } else {

                    $obj_appointment->idInterpreter = NULL;
                    $obj_appointment->idLanguage = NULL;
                    $obj_appointment->typeservicesinterpreter = NULL;
                }

                $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                $obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                $obj_appointment->whocalls = $post_data['whocalls_app'];
                $obj_appointment->companyname = $post_data['companyname_app'];
                $obj_appointment->companyphone = $post_data['companyphone_app'];
                $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                $obj_appointment->pickuptime = $post_data['pick_up_time'];
                $obj_appointment->droptime = $post_data['final'];
                $obj_appointment->dateregistration = date('y/m/d H:i:s');
                $obj_appointment->callinglistconfirmationdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationpatient = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->callinglistconfirmationcontact = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedsentdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->confirmedtranslationsummary = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationdriver1 = self::STATUS_DEACTIVE;
                $obj_appointment->appconfirmedconfirmationdriver2 = self::STATUS_DEACTIVE;
                $obj_appointment->billingtransp = self::STATUS_DEACTIVE;
                $obj_appointment->billinginterp = self::STATUS_DEACTIVE;
                $obj_appointment->showinterpreter = self::STATUS_DEACTIVE;
                $obj_appointment->showdriver = self::STATUS_DEACTIVE;
                $obj_appointment->status = self::STATUS_ACTIVE;

                if (isset($post_data['for_lbase'])) {
                    $obj_appointment->lbase = $post_data['for_lbase'];
                } else {
                    $obj_appointment->lbase = NULL;
                }
                if (isset($post_data['for_cassistance'])) {
                    $obj_appointment->cassistance = $post_data['for_cassistance'];
                } else {
                    $obj_appointment->cassistance = NULL;
                }
                if (isset($post_data['for_wtime']) && $post_data['for_wtime'] == 'on') {
                    $obj_appointment->wtime = self::STATUS_ACTIVE;
                    $obj_appointment->wtimecode = $post_data['wtime_code'];
                    $obj_appointment->wtimehours = $post_data['wtime_hours'];
                    $obj_appointment->wtimeminutes = $post_data['wtime_minutes'];
                } else {
                    $obj_appointment->wtime = self::STATUS_DEACTIVE;
                }


                switch ($post_data['typeservice_trasl']) {
                    case 'Interpreting':
                        break;
                    case 'Conference Call':

                        $obj_appointment->callinglistconfirmationpatient = self::STATUS_ACTIVE;
                        $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_ACTIVE;
                        $obj_appointment->state = 'CONFIRMED';
                        $obj_appointment->confirmedsentinterpreter = 1;

                        break;
                    case 'Document Translation':
                        $obj_appointment->callinglistconfirmationpatient = self::STATUS_ACTIVE;
                        $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_ACTIVE;
                        $obj_appointment->state = 'CONFIRMED';
                        $obj_appointment->confirmedsentinterpreter = 3;
                        break;
                    default:
                        break;
                }

                $obj_appointment->save();

                $idApp = $obj_appointment->idAppointment;
                $name = $obj_appointment->companyname;
                $concat = $idApp . '' . substr($name, 0, 2);

                $obj_appointment->idAlias = $concat;
                $obj_appointment->save();
            }
            //-----------------fin      
            else {

                $DateOfService = $post_data['dateregistration_appinfo'];

                if ($until_app == '0000-00-00' OR $until_app == '--')
                    $until_app = date('Y-m-d');


                if ($DateOfService == '0000-00-00' OR $DateOfService == '--')
                    $DateOfService = date('Y-m-d');
                $until = strtotime($until_app);
                $dateOfService = strtotime($DateOfService);
                $orden = 1;

                while ($dateOfService <= $until) {
                    foreach ($post_data['until'] as $k => $day) {

                        $atoday = getdate($dateOfService);

                        if ($atoday['wday'] == $day) {

                            $obj_appointment = new Model_Appointment();

                            if ($numapp > 0) {
                                $obj_appointment->state = 'CALLING_LIST';
                            } else if ($numapp == 0) {
                                $obj_appointment->state = 'CREATED_CONFIRMATION_ADJUSTER';
                            }
                            if (isset($post_data['transportation'])) {
                                $obj_appointment->idDriverRoundTrip = $post_data['driver2'];
                                $obj_appointment->idDriverOneWay = $post_data['driver1'];
                                $obj_appointment->idCourse = $post_data['idCourse_newappointment_appinfo'];
                                $obj_appointment->frate = $post_data['flatrate_serv_inf'];
                                $obj_appointment->miles = $post_data['fmiles'];
                                $obj_appointment->typetravel = $post_data['roundtrip'];
                                $obj_appointment->typeservice = $post_data['for_typeservice'];
                            } else {
                                $obj_appointment->idDriverRoundTrip = NULL;
                                $obj_appointment->idDriverOneWay = NULL;
                                $obj_appointment->idCourse = NULL;
                                $obj_appointment->typeservice = NULL;
                                $obj_appointment->frate = NULL;
                                $obj_appointment->miles = NULL;
                                $obj_appointment->typetravel = NULL;
                            }
                            if (isset($post_data['traduction'])) {
                                if ($post_data['typeservice_trasl'] == 'Interpreting')
                                    $obj_appointment->idLocation = $post_data['idLocation_newappointment_appinfo'];
                                else
                                    $obj_appointment->idLocation = NULL;

                                $obj_appointment->idInterpreter = $post_data['interprete_trasl'];
                                $obj_appointment->idLanguage = $post_data['languaje_trasl'];
                                $obj_appointment->typeservicesinterpreter = $post_data['typeservice_trasl'];
                            } else {

                                $obj_appointment->idInterpreter = NULL;
                                $obj_appointment->idLanguage = NULL;
                                $obj_appointment->typeservicesinterpreter = NULL;
                            }

                            $obj_appointment->idClaim = $post_data['idClaim_newappointment_appinfo'];
                            $obj_appointment->idPatient = $post_data['idPatient_newappointment_appinfo'];
                            $obj_appointment->idTypeAppointment = $post_data['appdescription_appinfo'];
                            $obj_appointment->appauthorized = $post_data['appauthorize_appinfo'];
                            $obj_appointment->appointmentdate = $post_data['dateregistration_appinfo'];
                            $obj_appointment->typeapp = $post_data['typeapp_appinfo'];
                            $obj_appointment->appointmenttime = $post_data['appointmenttime_appinfo'];
                            $obj_appointment->whocalls = $post_data['whocalls_app'];
                            $obj_appointment->companyname = $post_data['companyname_app'];
                            $obj_appointment->companyphone = $post_data['companyphone_app'];
                            $obj_appointment->notescalling = $post_data['notesFromCallingList'];
                            $obj_appointment->pickuptime = $post_data['pick_up_time'];
                            $obj_appointment->droptime = $post_data['final'];
                            $obj_appointment->dateregistration = date('y/m/d H:i:s');
                            $obj_appointment->callinglistconfirmationdriver1 = self::STATUS_DEACTIVE;
                            $obj_appointment->callinglistconfirmationdriver2 = self::STATUS_DEACTIVE;
                            $obj_appointment->callinglistconfirmationpatient = self::STATUS_DEACTIVE;
                            $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_DEACTIVE;
                            $obj_appointment->callinglistconfirmationcontact = self::STATUS_DEACTIVE;
                            $obj_appointment->confirmedsentinterpreter = self::STATUS_DEACTIVE;
                            $obj_appointment->confirmedsentdriver1 = self::STATUS_DEACTIVE;
                            $obj_appointment->confirmedsentdriver2 = self::STATUS_DEACTIVE;
                            $obj_appointment->confirmedtranslationsummary = self::STATUS_DEACTIVE;
                            $obj_appointment->appconfirmedconfirmationinterpreter = self::STATUS_DEACTIVE;
                            $obj_appointment->appconfirmedconfirmationdriver1 = self::STATUS_DEACTIVE;
                            $obj_appointment->appconfirmedconfirmationdriver2 = self::STATUS_DEACTIVE;
                            $obj_appointment->billingtransp = self::STATUS_DEACTIVE;
                            $obj_appointment->billinginterp = self::STATUS_DEACTIVE;
                            $obj_appointment->showinterpreter = self::STATUS_DEACTIVE;
                            $obj_appointment->showdriver = self::STATUS_DEACTIVE;
                            $obj_appointment->status = self::STATUS_ACTIVE;

                            if (isset($post_data['for_lbase'])) {
                                $obj_appointment->lbase = $post_data['for_lbase'];
                            } else {
                                $obj_appointment->lbase = NULL;
                            }
                            if (isset($post_data['for_cassistance'])) {
                                $obj_appointment->cassistance = $post_data['for_cassistance'];
                            } else {
                                $obj_appointment->cassistance = NULL;
                            }
                            if (isset($post_data['for_wtime']) && $post_data['for_wtime'] == 'on') {
                                $obj_appointment->wtime = self::STATUS_ACTIVE;
                                $obj_appointment->wtimecode = $post_data['wtime_code'];
                                $obj_appointment->wtimehours = $post_data['wtime_hours'];
                                $obj_appointment->wtimeminutes = $post_data['wtime_minutes'];
                            } else {
                                $obj_appointment->wtime = self::STATUS_DEACTIVE;
                            }

                            switch ($post_data['typeservice_trasl']) {
                                case 'Interpreting':
                                    break;
                                case 'Conference Call':
//Go To Translation Summary
                                    $obj_appointment->callinglistconfirmationpatient = self::STATUS_ACTIVE;
                                    $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_ACTIVE;
                                    $obj_appointment->state = 'CONFIRMED';
                                    $obj_appointment->confirmedsentinterpreter = 1;
                                    break;
                                case 'Document Translation':
                                    $obj_appointment->callinglistconfirmationpatient = self::STATUS_ACTIVE;
                                    $obj_appointment->callinglistconfirmationinterpreter = self::STATUS_ACTIVE;
                                    $obj_appointment->state = 'CONFIRMED';
                                    $obj_appointment->confirmedsentinterpreter = 3;
                                    break;
                                default:
                                    break;
                            }

                            $obj_appointment->save();

                            $idApp = $obj_appointment->idAppointment;
                            $name = $obj_appointment->companyname;
                            $concat = $idApp . '' . substr($name, 0, 2);

                            $obj_appointment->idAlias = $concat;
                            $obj_appointment->save();
                        }
                    }
                    $dateOfService += 24 * 60 * 60;
                }
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_saveDrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('name', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                /* Capturo el id Driver */
                $driver_id = $post_data['idDrivers'];
                if ($driver_id > 0) {
//Edicion
                    $obj_drivers = new Model_Drivers($driver_id);
                    if (!$obj_drivers->loaded())
                        throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
                }
                else {
                    $obj_drivers = new Model_Drivers();
                }
                $obj_drivers->name = $post_data['name'];
                $obj_drivers->lastName = $post_data['last_name'];
                $obj_drivers->idAreasdriver = $post_data['areas_drivers_id'];

                $obj_drivers->typeservices = $post_data['for_drive'];
                $obj_drivers->address = $post_data['address'];
                $obj_drivers->city = $post_data['city'];
                $obj_drivers->state = $post_data['state'];
                $obj_drivers->zipcode = $post_data['zip_code'];
                $obj_drivers->phone = $post_data['phone'];
                $obj_drivers->fax = $post_data['fax'];
                $obj_drivers->cellphone = $post_data['cell_phone'];
                $obj_drivers->carMake = $post_data['car_make'];
                $obj_drivers->carModel = $post_data['car_model'];
                $obj_drivers->email = $post_data['email'];
                $obj_drivers->insurance = $post_data['insurance_date'];
                $obj_drivers->identificationNumber = $post_data['identification_number'];
                $obj_drivers->specialNotes = $post_data['special_notes'];
                $obj_drivers->status = $post_data['status_driver'];


//$obj_drivers->status = self::STATUS_ACTIVE;
                $obj_drivers->save();
                $a_response['data'] = array(
                    'idDriver' => $obj_drivers->idDriver
                    , 'name' => $obj_drivers->name
                );
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_saveAreadrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
//die('adsfdgSADFS');
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('location_driverarea', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();

                $obj_areadrivers = new Model_Areadriver();
                $obj_areadrivers->location = $post_data['location_driverarea'];
                $obj_areadrivers->status = self::STATUS_ACTIVE;
                $obj_areadrivers->save();
                $a_response['data'] = array(
                    'idAreasdriver' => $obj_areadrivers->idAreasdriver
//, 'name' => $obj_areadrivers->name
                    , 'location' => $obj_areadrivers->location
                );
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_saveLocation() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        try {


            $post = Validation::factory($this->request->post())
                    ->rule('address', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            }
            $post_data = $post->data();
            if ($post_data['idLocation'] <= 0) {

                $obj_locations = new Model_Locations();
                $obj_locations->alias = $post_data['alias'];
                $obj_locations->address = $post_data['address'];
                $obj_locations->suite = $post_data['unit'];
                $obj_locations->telephone = $post_data['phone'];
                $obj_locations->idPatient = $post_data['idPatient_app'];

                $obj_locations->save();


                $a_response['data'] = array(
                    'idLocation' => $obj_locations->idLocation
                );
            } else {
                $a_response['data'] = array(
                    'idLocation' => $post_data['idLocation']);
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_saveCourse() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        try {

            $post = Validation::factory($this->request->post())
                    ->rule('idLocationDestination', 'not_empty');

            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                $idCourse = DB::select(array('idCourse', 'idCourse'))->distinct(true)
                                ->from(array('course', 'c'))
                                ->where('c.idLocationOrigen', '=', $post_data['idLocationOrigen'])
                                ->and_where('c.idLocationDestination', '=', $post_data['idLocationDestination'])
                                ->execute()->current();

                if ($idCourse['idCourse'] <= 0) {
                    $obj_course = new Model_Course();
                    $obj_course->idLocationDestination = $post_data['idLocationDestination'];
                    $obj_course->idLocationOrigen = $post_data['idLocationOrigen'];
                    $obj_course->status = self::STATUS_ACTIVE;
                    $obj_course->miles = $post_data['fmiles'];
                    $obj_course->save();
                    $a_response['data'] = array(
                        'idCourse' => $obj_course->idCourse);
                } else {
                    $a_response['data'] = array(
                        'idCourse' => $idCourse['idCourse']);
                }
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_delete_appointment() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $i_appointment_id = $this->request->post("id");

            $o_appointment = new Model_Appointment($i_appointment_id);

            if (!$o_appointment->loaded()) {
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {

                $o_appointment->status = self::STATUS_DEACTIVE;
                $o_appointment->save();
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getActiveClaim() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        try {

            $post = Validation::factory($this->request->post())
                    ->rule('idClaim', 'not_empty');

            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                $var = $post_data['idClaim'];

                $cons = DB::select(array(DB::expr('COUNT(*)'), 'count'))->distinct(true)
                                ->from(array('appointment', 'app'))
                                ->where('app.idClaim', '=', DB::expr($var))
                                ->execute()->current();

                $flag = DB::select(array(DB::expr('COUNT(*)'), 'count'))->distinct(true)
                                ->from(array("claims", 'c'))
                                ->where('c.status', '=', '1')
                                ->and_where('c.idClaim', '=', DB::expr($var))
                                ->and_where_open()
                                ->and_where('c.datefinish', '>', DB::expr('CURDATE()'))
                                ->or_where('c.unlimited', '=', '1')
                                ->and_where_close()
                                ->and_where_open()
                                ->and_where('c.maxAppointments', '>', DB::expr($cons['count']))
                                ->or_where('c.unlimitedapp', '=', '1')
                                ->and_where_close()
                                ->execute()->current();

                $a_response['data'] = array('flag' => $flag['count']);

                $this->fnResponseFormat($a_response);
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_expandClaim() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;

        try {

            $post = Validation::factory($this->request->post())
                    ->rule('idClaim', 'not_empty')
                    ->rule('maxDate', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                $obj_course = new Model_Claims($post_data['idClaim']);
                $obj_course->datefinish = $post_data['maxDate'];
                $obj_course->save();

                $this->fnResponseFormat($a_response);
            }
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

    public function getfechaAnterioraHoy() {
        //formateado para el insertado en la bd
        $dia = date("Y-m-d", time() - 86400);
        return $dia;
    }

    public function getFechaFormateadaActual() {
        $fechaA = strftime("%Y-%m-%d %H:%M:%S", time());
        return $fechaA;
    }

}
