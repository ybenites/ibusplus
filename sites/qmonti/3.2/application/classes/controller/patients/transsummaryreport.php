<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Transsummaryreport extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('transsummaryreport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('transsummaryreport', self::FILE_TYPE_JS));
        $view_transsummaryreport = new View('patients/transsummaryreport');
        $this->template->content = $view_transsummaryreport;
    }

    public function action_listAppWithPending() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');


        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idApp' => " DISTINCT app.idAppointment",
                'interpreter' => "CONCAT(i.name,' ',i.lastname)",
                'patient' => "CONCAT(p.firstname,' ',p.lastname)",
                'appDate' => "app.appointmentDate",
                'appTime' => "app.appointmentTime"
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "appointment app" .
                    $s_tables_join = " JOIN interpreters i ON app.idInterpreter=i.idInterpreter " .
                    $s_tables_join = " JOIN patients p ON app.idPatient=p.idPatient";


            $s_where_conditions = " app.status=1  AND app.state='CONFIRMED' AND TRIM(app.typeservicesInterpreter) NOT LIKE 'Document Translation' AND" .
                    $s_where_conditions = " (app.confirmedtranslationsummary=0 OR ISNULL(app.confirmedtranslationsummary)) AND" .
                    $s_where_conditions = " (app.cancelinterpreter=0 OR ISNULL(app.cancelinterpreter)) AND  " .
                    $s_where_conditions = " app.confirmedsentinterpreter=1 ";

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_getAppointmentbyIdAppointment() {

        $idApp = $this->request->post('idApp');

        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE

        $a_response['data'] = DB::select(array('app.idAppointment', 'idApp')
                                , array('app.idInterpreter', 'idInter')
                                , array('cla.authorizedby', 'authorized')
                                , array('app.idDriverRoundTrip', 'driverRoundTrip')
                                , array('app.idDriverOneWay', 'driverOneWay')
                                , array('pat.firstname', 'firstname')
                                , array('pat.lastname', 'lastname')
                                , array('pat.phonenumber', 'phonenumber')
                                , DB::expr("COALESCE(loc.address,'-') AS addressloc")
                                , DB::expr("COALESCE(loc.telephone,'-') AS phoneloc")
                                , DB::expr("DATE_FORMAT(app.appointmentdate,'%m/%d/%Y') AS appDate")
                                , array('app.appointmenttime', 'appTime')
                                , array('lan.name', 'language')
                                , array('pat.socialsecurity', 'socialsecurity')
                                , array('app.typeapp', 'typeapp')
                                , array('app.typeservicesinterpreter', 'typeservinterpreter')
                                , array('ti.name', 'typeapppurpose')
                        )->distinct(true)
                        ->from(array("appointment", 'app'))
                        ->join(array('claims', 'cla'))->on('app.idClaim', '=', 'cla.idClaim')
                        ->join(array('patients', 'pat'))->on('app.idPatient', '=', 'pat.idPatient')
                        ->join(array('languages', 'lan'))->on('app.idLanguage', '=', 'lan.idLanguage')
                        ->join(array('locations', 'loc'))->on('app.idLocation', '=', 'loc.idLocation')
                        ->join(array('typeappointment', 'ti'))->on('app.idTypeAppointment', '=', 'ti.idTypeappointment')
                        ->where('app.status', '=', '1')
                        ->and_where('app.idAppointment', '=', $idApp)
                        ->limit(20)
                        ->execute()->current();
//                print_r(Database::instance()->last_query);
//        die();
        $this->fnResponseFormat($a_response);
    }

    public function action_calculartiempotrasl() {
        $a_response = $this->json_array_return;

        $hora1 = $this->request->post('to');
        $hora2 = $this->request->post('from');

        $timeTo = explode(' ', $hora1);
        $timeFrom = explode(' ', $hora2);

        $hourTo = explode(':', $timeTo[0]);
        $hourFrom = explode(':', $timeFrom[0]);

        $toH = 0;
        $fromH = 0;
        $toM = 0;
        $fromM = 0;
        //To
        //echo ($timeTo[1]);// == 'pm';
        if ($timeTo[1] == 'pm') {
            $toH = $hourTo[0];
            $toM = $hourTo[1];
            if (!($hourTo[0] == '12')) {
                $toH += 12;
            }
        } elseif ($timeTo[1] == 'am') {
            $toH = $hourTo[0];
            $toM = $hourTo[1];
            if ($hourTo[0] == '12') {
                $toH = 0;
            }
        }
        //From
        if ($timeFrom[1] == 'pm') {
            $fromH = $hourFrom[0];
            $fromM = $hourFrom[1];
            if (!($hourFrom[0] == '12')) {
                $fromH += 12;
            }
        } elseif ($timeFrom[1] == 'am') {
            $fromH = $hourFrom[0];
            $fromM = $hourFrom[1];
            if ($hourFrom[0] == '12') {
                $fromH = 0;
            }
        }

        $dateTimeTo = date_create('2010-01-01');
        date_time_set($dateTimeTo, $toH, $toM);

        $dateTimeFrom = date_create('2010-01-01');
        date_time_set($dateTimeFrom, $fromH, $fromM);

        $interval = $dateTimeTo->diff($dateTimeFrom);
        $horas = (int) $interval->format('%H');
        $minutos = $interval->format('%i');


        $minutos = round(($minutos / 60), 2);

        $a_response['data'] = $horas + $minutos;
        $this->fnResponseFormat($a_response);
    }

    public function action_milesToTime() {
        $a_response = $this->json_array_return;
        $totalMileage = $this->request->post('totalMileage');
        $rangos = array(
            array(1.00, array(0, 40)),
            array(1.25, array(41, 45)),
            array(1.50, array(46, 55)),
            array(1.75, array(56, 69)),
            array(2.00, array(70, 79)),
            array(2.25, array(80, 99)),
            array(2.50, array(100, 119)),
            array(2.75, array(120, 139)),
            array(3.00, array(140, 159)),
            array(3.25, array(160, 179)),
            array(3.50, array(180, 199)),
            array(3.75, array(200, 219)),
            array(4.00, array(220, 239)),
            array(4.25, array(240, 259)),
            array(4.50, array(260, 279)),
            array(4.75, array(280, 299))
        );
        $timeTravel = 4.75;
        foreach ($rangos as $value) {
            $time = $value[0];
            $millaRango = $value[1];
            if ($totalMileage >= $millaRango[0] AND $totalMileage <= $millaRango[1]) {
                $timeTravel = $time;
                break;
            }
        }
        $a_response['data'] = $timeTravel;
        $this->fnResponseFormat($a_response);
    }

    public function action_confirmTranslationSummary() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('idInterpreter', 'not_empty')
                    ->rule('idAppointment', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                 Database::instance()->begin();
                $o_model_translation = new Model_TranslationSummary();
                $o_model_translation->createUpdateTranslationSummary($post_data);
                Database::instance() ->commit();
                } 
              
                $o_model_app = new Model_Appointment($post_data['idAppointment']);
                $o_model_app->confirmedtranslationsummary = self::STATE_ACTIVE;
                $o_model_app->save();
                $a_response['data'] = array(
                'idTranslationSum' => $o_model_translation->idTranslationSummary
            );
            } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }              


    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

}