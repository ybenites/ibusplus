<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Appconfirmation extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('appconfirmation', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('appconfirmation', self::FILE_TYPE_JS));
        $view_appconfirmation = new View('patients/appconfirmation');
        
        $case_manager = DB::select(array('idCasemanager', 'idCasemanager'), array('name', 'name'))
                        ->from("casemanagers")->where('status', '=', '1')
                        ->execute()->as_array();

        $adjuster = DB::select(array('idAdjuster', 'idAdjuster'), array('name', 'name'))
                        ->from("adjusters")->where('status', '=', '1')
                        ->execute()->as_array();

        $insurance_carrier = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();

        $drivers = DB::select(array('idDriver', 'idDriver'), array('name', 'name'), array('lastname', 'lastname'))
                        ->from("drivers")->where('status', '=', '1')
                        ->execute()->as_array();

        $type_appointment = DB::select(array('idTypeappointment', 'id'), array('name', 'nameapp'))
                        ->from("typeappointment")->where('status', '=', '1')
                        ->execute()->as_array();

        $languaje = DB::select(array('idLanguage', 'idLanguage'), array('name', 'name'))
                        ->from("languages")->where('status', '=', '1')
                        ->execute()->as_array();

        $interprete = DB::select(array('idInterpreter', 'idInterpreter'), array('name', 'name'))
                        ->from("interpreters")->where('status', '=', '1')
                        ->execute()->as_array();

        $area_driver = DB::select(array('idAreasdriver', 'id'), array('location', 'area'))
                        ->from("areasdriver")->where('status', '=', '1')
                        ->execute()->as_array();
        
        $view_appconfirmation->area_driver = $area_driver;
        $view_appconfirmation->insurance_carrier = $insurance_carrier;
        $view_appconfirmation->case_manager = $case_manager;
        $view_appconfirmation->adjuster = $adjuster;
        $view_appconfirmation->drivers = $drivers;
        $view_appconfirmation->type_appointment = $type_appointment;
        $view_appconfirmation->languaje = $languaje;
        $view_appconfirmation->interprete = $interprete;
        
        $this->template->content = $view_appconfirmation;
        
    }

        public function action_listAppConfirmation() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $date = $this->request->query('date');
        if (!empty($date))
            $q_date = $this->cambiafmysqlre($date);
        else
            $q_date = getdate();

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idAppointment' => "app.idAppointment",
                'appointmentdate' => "app.appointmentdate",
                'appointmenttime' => "app.appointmenttime",
                'namepatient' => "CONCAT(pat.firstname, ' ', pat.lastname)",
                'addresspatient' => "pat.address",
                'phonepatient' => "pat.phonenumber",
                'contactname' => "IF(app.idLocation IS NULL,loc2course.alias,loc.alias)",
                'contactaddress' => "IF(app.idLocation IS NULL,loc2course.address,loc.address)",
                'contactphone' => "IF(app.idLocation IS NULL,loc2course.telephone, loc.telephone)",
                'namedriver1' => "COALESCE(CONCAT(dr1.name, ' ', dr1.lastname),'-')",
                'phonedriver1' => "COALESCE(dr1.phone,'-')",
                'namedriver2' => "COALESCE(CONCAT(dr2.name, ' ', dr2.lastname),'-')",
                'phonedriver2' => "COALESCE(dr2.phone,'-')",
                'nameinterprete' => "COALESCE(CONCAT(inter.name, ' ', inter.lastname),'-')",
                'phoneinterprete' => "COALESCE(inter.phone,'-')",
                'miles' => "app.miles",
                'chdriv1' => "app.appconfirmedconfirmationdriver1",
                'chdriv2' => "app.appconfirmedconfirmationdriver2",
                'chinter' => "app.appconfirmedconfirmationinterpreter",
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "appointment app" .
                    $s_tables_join = " INNER JOIN patients pat ON app.idPatient=pat.idPatient " .
                    $s_tables_join = " LEFT JOIN course c ON app.idCourse=c.idCourse  " .
                    $s_tables_join = " LEFT JOIN locations loc ON loc.idLocation=app.idLocation " .
                    $s_tables_join = " LEFT JOIN locations loc1course ON loc1course.idLocation=c.idLocationOrigen " .
                    $s_tables_join = " LEFT JOIN locations loc2course ON loc2course.idLocation=c.idLocationDestination " .
                    $s_tables_join = " LEFT JOIN drivers dr1 ON app.idDriverOneWay=dr1.idDriver   " .
                    $s_tables_join = " LEFT JOIN drivers dr2 ON app.idDriverRoundTrip=dr2.idDriver   " .
                    $s_tables_join = " LEFT JOIN interpreters inter ON app.idInterpreter=inter.idInterpreter ";
            $s_where_conditions = " app.state ='CONFIRMED' AND app.status=1 AND".
            $s_where_conditions = "((app.idInterpreter!=0 AND NOT ISNULL(app.idInterpreter) AND app.confirmedsentinterpreter=1 AND (app.confirmedtranslationsummary=1 AND (TRIM(app.typeservicesInterpreter) NOT LIKE 'DOCUMENTTRANSLATION'))) OR ".
            $s_where_conditions = " (app.idDriverOneWay!=0 AND NOT ISNULL(app.idDriverOneWay) AND app.confirmedsentdriver1=1) OR   ".
            $s_where_conditions = " (app.idDriverRoundTrip!=0 AND NOT ISNULL(app.idDriverRoundTrip) AND app.confirmedsentdriver2=1)) ";



            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_listNotes() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        $tablename = $this->request->query('tname');
        $idApp = $this->request->query('idApp');

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idNote' => "t.idNotes",
                'description' => "t.description",
                'dateresgistration' => "t.dateRegistration",
                'user' => "u.userName",
            );
//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = $tablename . " t" .
                    $s_tables_join = " LEFT JOIN bts_user u ON t.idUsers=u.idUser";
            $s_where_conditions = " t.status=1 AND t.idAppointment=" . $idApp;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_getIdNotesPat() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notespatients = new Model_NotesPatients($idNote);
            $result = $o_notespatients->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesCon() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notescontacts = new Model_NotesContacts($idNote);
            $result = $o_notescontacts->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesDri() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesdrivers = new Model_NotesDrivers($idNote);
            $result = $o_notesdrivers->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesInt() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesinterpretes = new Model_NotesInterpreters($idNote);
            $result = $o_notesinterpretes->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_getIdNotesEmp() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post('idNote');
            $o_notesemployeers = new Model_NotesEmployeers($idNote);
            $result = $o_notesemployeers->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesPatients() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desPat = $this->request->post("descriptionPat");
            $idApp = $this->request->post("idAppointment");
            $idNotes = $this->request->post("idNotes");
            if ($idNotes > 0) {
//Edicion

                $o_notespatients = new Model_NotesPatients($idNotes);
              
                if (!$o_notespatients->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            } else {
//Nuevo
                $o_notespatients = new Model_NotesPatients();
                $o_notespatients->dateRegistration = date('Y-m-d H:i:s');
                $o_notespatients->status = self::STATUS_ACTIVE;
                $o_notespatients->idUsers = $this->getSessionParameter(self::USER_ID);
            }
            $o_notespatients->description = $desPat;
            $o_notespatients->idAppointment = $idApp;
            $o_notespatients->save();

            $a_response['data'] = array(
                'idNote' => $o_notespatients->idNote
                , 'description' => $o_notespatients->description
                , 'idApp' => $o_notespatients->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesPat() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notespatients = new Model_NotesPatients($idNote);

            if (!$o_notespatients->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notespatients->status = self::STATUS_DEACTIVE;
            $o_notespatients->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesContacts() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desContact = $this->request->post("descriptionCon");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notescontacts = new Model_NotesContacts($idNote);

                if (!$o_notescontacts->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notescontacts->description = $desContact;
                $o_notescontacts->idAppointment = $idApp;
                $o_notescontacts->save();
            } else {
//Nuevo
                $o_notescontacts = new Model_NotesContacts();
                $o_notescontacts->description = $desPat;
                $o_notescontacts->dateRegistration = date('Y-m-d H:i:s');
                $o_notescontacts->idAppointment = $idApp;
                $o_notescontacts->status = self::STATUS_ACTIVE;
                $o_notescontacts->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notescontacts->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notescontacts->idNote
                , 'description' => $o_notescontacts->description
                , 'idApp' => $o_notescontacts->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesCon() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notescontacts = new Model_NotesContacts($idNote);

            if (!$o_notescontacts->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notescontacts->status = self::STATUS_DEACTIVE;
            $o_notescontacts->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesDrivers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desDrivers = $this->request->post("descriptionDri");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notesdrivers = new Model_NotesDrivers($idNote);

                if (!$o_notesdrivers->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesdrivers->description = $desDrivers;
                $o_notesdrivers->idAppointment = $idApp;
                $o_notesdrivers->save();
            } else {
//Nuevo
                $o_notesdrivers = new Model_NotesDrivers();
                $o_notesdrivers->description = $desDrivers;
                $o_notesdrivers->dateRegistration = date('Y-m-d H:i:s');
                $o_notesdrivers->idAppointment = $idApp;
                $o_notesdrivers->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesdrivers->status = self::STATUS_ACTIVE;
                $o_notesdrivers->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesdrivers->idNote
                , 'description' => $o_notesdrivers->description
                , 'idApp' => $o_notesdrivers->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_deleteNotesDri() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesdrivers = new Model_NotesDrivers($idNote);

            if (!$o_notesdrivers->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesdrivers->status = self::STATUS_DEACTIVE;
            $o_notesdrivers->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesInterpreters() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desInterpretes = $this->request->post("descriptionInt");
            $idApp = $this->request->post("idAppointment");
            $idNotes = $this->request->post("idNotes");

            if ($idNotes > 0) {
//Edicion
                $o_notesinterpretes = new Model_NotesInterpreters($idNotes);

                if (!$o_notesinterpretes->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesinterpretes->description = $desInterpretes;
                $o_notesinterpretes->idAppointment = $idApp;
                $o_notesinterpretes->save();
            } else {
//Nuevo
                $o_notesinterpretes = new Model_NotesInterpreters();
                $o_notesinterpretes->description = $desInterpretes;
                $o_notesinterpretes->dateRegistration = date('Y-m-d H:i:s');
                $o_notesinterpretes->idAppointment = $idApp;
                $o_notesinterpretes->status = self::STATUS_ACTIVE;
                $o_notesinterpretes->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesinterpretes->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesinterpretes->idNote
                , 'description' => $o_notesinterpretes->description
                , 'idApp' => $o_notesinterpretes->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesInt() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesinterpreters = new Model_NotesInterpreters($idNote);

            if (!$o_notesinterpreters->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesinterpreters->status = self::STATUS_DEACTIVE;
            $o_notesinterpreters->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveNotesEmployeers() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $desEmployeers = $this->request->post("descriptionEmp");
            $idApp = $this->request->post("idAppointment");
            $idNote = $this->request->post("idNotes");

            if ($idNote > 0) {
//Edicion
                $o_notesemployeers = new Model_NotesEmployeers($idNote);

                if (!$o_notesemployeers->loaded())
                    throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

                $o_notesemployeers->description = $desEmployeers;
                $o_notesemployeers->idAppointment = $idApp;
                $o_notesemployeers->save();
            } else {
//Nuevo
                $o_notesemployeers = new Model_NotesEmployeers();
                $o_notesemployeers->description = $desEmployeers;
                $o_notesemployeers->dateRegistration = date('Y-m-d H:i:s');
                $o_notesemployeers->idAppointment = $idApp;
                $o_notesemployeers->idUsers = $this->getSessionParameter(self::USER_ID);
                $o_notesemployeers->status = self::STATUS_ACTIVE;
                $o_notesemployeers->save();
            }

            $a_response['data'] = array(
                'idNote' => $o_notesemployeers->idNote
                , 'description' => $o_notesemployeers->description
                , 'idApp' => $o_notesemployeers->idAppointment
            );
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_deleteNotesEmp() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idNote = $this->request->post("idNote");
            $o_notesemployeers = new Model_NotesInterpretes($idNote);

            if (!$o_notesemployeers->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);

            $o_notesemployeers->status = self::STATUS_DEACTIVE;
            $o_notesemployeers->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_confirm() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $campo = $this->request->post("confirm");
            $idapp = $this->request->post("idAppointment");
            $o_app = new Model_Appointment($idapp);

            if (!$o_app->loaded())
                throw new Exception(__("Registro no encontrado"), self::CODE_SUCCESS);
            if($o_app->$campo==0 or $o_app->$campo==NULL)
                $o_app->$campo = self::STATUS_ACTIVE;
            else $o_app->$campo = self::STATUS_DEACTIVE;
            $o_app->save();
            if ($o_app->idDriverRoundTrip == $o_app->idDriverOneWay) {
                $o_app->appconfirmedconfirmationdriver2 = $o_app->appconfirmedconfirmationdriver1;
                $o_app->save();
            }
            $int = $o_app->appconfirmedconfirmationinterpreter;
            $dri1 = $o_app->appconfirmedconfirmationdriver1;
            $dri2 = $o_app->appconfirmedconfirmationdriver2;

                 if ( ($o_app->typeservicesinterpreter == 'Interpreting' and (($o_app->idLocation != '0' and $o_app->idLocation != NULL and $int==1) or ($o_app->idCourse != '0' and $o_app->idCourse !=NULL))) or 
                (((($o_app->idDriverRoundTrip==$o_app->idDriverOneWay) and $dri1==1 ) or (($o_app->idDriverRoundTrip!=$o_app->idDriverOneWay) and $dri1==1 and $dri2==1 )) and ($o_app->idDriverRoundTrip>0 or $o_app->idDriverOneWay>0)) or 
                (($o_app->idInterpreter != '0' and $o_app->idInterpreter != NULL)  and $int==1)  )
            $o_app->state = 'APP_CONFIRMED';
            else  $o_app->state = 'CONFIRMED';
                $o_app->save();
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}