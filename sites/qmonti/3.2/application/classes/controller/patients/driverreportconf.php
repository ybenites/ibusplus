<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Driverreportconf extends Controller_Private_Admin implements Monti_Constants {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('driverreportconf', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('driverreportconf', self::FILE_TYPE_JS));
        $view_driverreportconf = new View('patients/driverreportconf');
        $this->template->content = $view_driverreportconf;
    }

    public function action_listDrivers() {

        $var = $this->request->post('namedriver');

        $a_response = $this->json_array_return;
//COLUMNAS DE BUSQUEDA SIMPLE
        $a_response['data'] = DB::select(
                                array('d.idDriver', 'idDriver')
                                , array('d.name', 'name')
                                , array('d.lastName', 'lastName')
                        )->distinct(true)
                        ->from(array("drivers", 'd'))
                        ->where('d.status', '=', '1')
                        ->and_where_open()
                        ->or_where('d.name', 'like', DB::expr('"%' . $var . '%"'))
                        ->or_where('d.lastName', 'like', DB::expr('"%' . $var . '%"'))
                        ->and_where_close()
                        ->limit(20)
                        ->execute()->as_array();

        $this->fnResponseFormat($a_response);
    }

    public function action_listDriversWithApp() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');


        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idDriver' => " DISTINCT d.idDriver",
                'drivername' => "d.drivername",
                'cellphone' => "d.cellphone",
                'fax' => "d.fax",
                'nextel' => "d.nextel",
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "listdriverswithapp d";

            $s_where_conditions = " TRUE ";

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_listAppbyIdDriver() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $all = $this->request->query('all');
        $idDriver = $this->request->query('idDriver');

        if ($all != 1) {
            $dateStart = $this->cambiafmysqlre($this->request->query('dateStart'));
            $dateFinish = $this->cambiafmysqlre($this->request->query('dateFinish'));
            $stringdate = "AND app.appointmentdate>='" . $dateStart . "' AND app.appointmentdate<='" . $dateFinish . "'";
        }
        else
            $stringdate = '';

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idApp' => " DISTINCT app.idAppointment",
                'appDate' => "app.appointmentdate",
                'pickuptime' => "app.pickuptime",
                'appTime' => "app.appointmenttime",
                'droptime' => "app.droptime",
                'patient' => "CONCAT(p.firstname,' ',p.lastname)",
                'lfrom' => "CONCAT(locfrom.address,' ',locfrom.telephone,'',IF(ISNULL(locfrom.suite) OR locfrom.suite=0,'',CONCAT(' Suite:',locfrom.suite))) ",
                'lto' => "CONCAT(locto.address,' ',locto.telephone,'',IF(ISNULL(locto.suite) OR locto.suite=0,'',CONCAT(' Suite:',locto.suite))) ",
                'miles' => "app.miles"
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " appointment app " .
                    $s_tables_join = " INNER JOIN patients p ON app.idPatient=p.idPatient" .
                    $s_tables_join = " INNER JOIN course c ON app.idCourse=c.idCourse" .
                    $s_tables_join = " INNER JOIN locations locfrom ON c.idLocationOrigen=locfrom.idLocation" .
                    $s_tables_join = " INNER JOIN locations locto ON c.idLocationDestination=locto.idLocation";
            $s_where_conditions = " app.status=1 AND app.state='CONFIRMED' AND (" .
                    $s_where_conditions = " (((app.confirmedsentdriver1=0) OR ISNULL(app.confirmedsentdriver1)) AND app.idDriverRoundTrip=" . $idDriver . ") OR" .
                    $s_where_conditions = " (((app.confirmedsentdriver2=0) OR ISNULL(app.confirmedsentdriver2)) AND app.idDriverOneWay=" . $idDriver . "))" .
                    $s_where_conditions = $stringdate;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);


            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);


            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_reportPrintAppbyDriver() {

        $this->auto_render = FALSE;

        try {


            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $driver = array();

            $driver['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $driver['qlang'] = $this->request->query(self::RQ_LANG);
            $driver['searchDate'] = date('l, F d, Y');
            $driver['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $driver['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $idDriver = $this->request->query('idDriver');

            $o_driver = new Model_Drivers($idDriver);

            $driver['drivername'] = $o_driver->name . ' ' . $o_driver->lastName;
            $driver['driverphone'] = $o_driver->phone;
            $driver['driverfax'] = $o_driver->fax;
            $driver['driveremail'] = $o_driver->email;
            $driver['driveraddress'] = $o_driver->address;


            $all = $this->request->query('all');
            if ($all == 0) {
                $dateStart = $this->request->query('dateStart');
                $dateFinish = $this->request->query('dateFinish');
//                $driver['dstart']=$dateStart;
//                $driver['dfinish']=$dateFinish;
            } else {
                
            }

            $parameters = array(
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE
            );

            $columns = array(
                array('ap.idAlias', 'appAlias'),
                array('ap.typeservice', 'appService'),
                array('ap.appointmentdate', 'appointmentDate'),
//                DB::expr("DATE_FORMAT(ap.appointmenttime,:time_format) AS appointmenttime"),
                array('ap.pickuptime', 'appointmentPickup'),
                array('ap.appointmenttime', 'appointmentTime'),
                array('ap.droptime', 'droptime'),
                DB::expr("CONCAT(pa.firstname, ' ', pa.lastname) AS patient"),
                DB::expr("CONCAT(locfrom.address,' ',IF(ISNULL(locfrom.suite) OR locfrom.suite=:s_inactive,'',CONCAT(' Suite:',locfrom.suite))) as locationFrom"),
                array('locfrom.telephone', 'telephonefrom'),
                DB::expr("CONCAT(locto.address,' ',IF(ISNULL(locto.suite) OR locto.suite=:s_inactive,'',CONCAT(' Suite:',locto.suite))) as locationTo"),
                array('locto.telephone', 'telephoneto'),
                array('ap.miles', 'miles'),
            );



            $sql_driver = DB::select_array($columns)
                    ->from(array('appointment', 'ap'))
                    ->join(array('patients', 'pa'))->on('pa.idPatient', '=', 'ap.idPatient')
                    ->join(array('course', 'cou'))->on('cou.idCourse', '=', 'ap.idCourse')
                    ->join(array('locations', 'locfrom'))->on('locfrom.idLocation', '=', 'cou.idLocationOrigen')
                    ->join(array('locations', 'locto'))->on('locto.idLocation', '=', 'cou.idLocationDestination')
                    ->where('ap.status', '=', '1')
                    ->and_where('ap.state', '=', 'CONFIRMED')
                    ->and_where_open()
                    ->where_open()
                    ->and_where_open()
                    ->or_where('ap.confirmedsentdriver1', '=', '0')
                    ->or_where('ap.confirmedsentdriver1', 'IS', NULL)
                    ->and_where_close()
                    ->and_where('ap.idDriverRoundTrip', '=', $idDriver)
                    ->where_close()
                    ->or_where_open()
                    ->and_where_open()
                    ->or_where('ap.confirmedsentdriver2', '=', '0')
                    ->or_where('ap.confirmedsentdriver2', 'IS', NULL)
                    ->and_where_close()
                    ->and_where('ap.idDriverOneWay', '=', $idDriver)
                    ->or_where_close()
                    ->and_where_close();

            if ($all == 0) {
                $sql_driver->and_where('ap.appointmentdate', '>=', $dateStart);
                $sql_driver->and_where('ap.appointmentdate', '<=', $dateFinish);
            }

            $sql_driver = $sql_driver->parameters($parameters)->execute()->as_array();

            $this->fnResponseFormat($sql_driver, self::REPORT_RESPONSE_TYPE_XML, $driver);
            // print_r(Database::instance()->last_query);
            // die();
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_sendEmailDriver() {
        try {
            $this->auto_render = FALSE;
            $url = $this->request->post('url');
            $idDriver = $this->request->post('idDriver');

            $o_driver = new Model_Drivers($idDriver);

            $email = $o_driver->email;
            $name = $o_driver->name + $o_driver->lastName;


            $company = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $contactCompany = db_config::$db_conection_config[SITE_DOMAIN]['contactCompany'];
            $contactAddress = db_config::$db_conection_config[SITE_DOMAIN]['contactAddress'];
            $contactPhone = db_config::$db_conection_config[SITE_DOMAIN]['contactPhone'];
            $skin = db_config::$db_conection_config[SITE_DOMAIN]['skin'];
            $website = $_SERVER['HTTP_HOST'];



            $sendEmail = array($email => "$name");
            $click = $url;



            $namepdf2 = preg_replace('/\W/', '_', 'MONTI_PRINT_CONF_DRIVER');
            $file = __DIR__ . DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR . '..'.DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'pdfreports' . DIRECTORY_SEPARATOR . "$namepdf2.pdf";
            copy($click, $file);
            $message = 'Monti Interpreting Assignment Report<br/>
                        Thank you for letting us serve you . <br/>
                        Please view attachment'; //' or click <a href="' . $click . '">here</a>';



            $this->fnaddNewModule('swift', 'swift');
            Email::instance(__($message) . $contactCompany)
                    ->to($sendEmail)
                    ->message('789456123')
                    ->from('no_reply@ibusplus.com')
                    ->attach($file)
                    ->send(FALSE);
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }

    public function action_comfirmSentDriver() {
        try {
            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;
            $idDriver = $this->request->post('idDriver');
            $idApps = explode(",", $this->request->post('dataIdApp'));

            foreach ($idApps as $key => $value) {
                $o_model_app = new Model_Appointment($value);
                if ($o_model_app->idDriverRoundTrip == $o_model_app->idDriverOneWay and $idDriver == $o_model_app->idDriverRoundTrip) {
                    $o_model_app->confirmedsentdriver1 = self::STATE_ACTIVE;
                    $o_model_app->confirmedsentdriver2 = self::STATE_ACTIVE;
                } else if ($o_model_app->idDriverRoundTrip == $idDriver)
                    $o_model_app->confirmedsentdriver2 = self::STATE_ACTIVE;
                else if ($o_model_app->idDriverOneWay == $idDriver)
                    $o_model_app->confirmedsentdriver1 = self::STATE_ACTIVE;
                $o_model_app->save();
            }
        } catch (Exception $e_exc) {

            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}
