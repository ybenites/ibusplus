<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Patients_Contactlist extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('contactlist', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('contactlist', self::FILE_TYPE_JS));
        $view_contactlist = new View('patients/contactlist');
        $this->template->content = $view_contactlist;
    }
    
    public function action_listLocations(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idLocation' => "loc.idLocation",
                'alias' => "loc.alias",
                'address' => "loc.address",
                'suite' => "loc.suite",
                'telephone' => "loc.telephone"
              
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " locations loc " ;

            $s_where_conditions = " loc.status=1 ";

            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }  
    
    public function action_deleteLocation(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            
            $idLocation = $this->request->post("idLocation");
            $oLocation = new Model_Locations($idLocation);
            $oLocation->status = 0;
           
            $oLocation->save();

        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }
    
    public function action_getLocationById(){
         $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            
            $idLocation = $this->request->post("idLocation");
            $oLocation = new Model_Locations($idLocation);
            $a_response['data']= $oLocation->as_array();

        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }
    
    public function action_saveLocation(){
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('address', 'not_empty')
                    ->rule('contact', 'not_empty')
                    ->rule('suite', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                 Database::instance()->begin();
                $o_model_location = new Model_Locations();
                $o_model_location->createUpdateLocation($post_data);
                Database::instance() ->commit();
                } 
              
            } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
            
}