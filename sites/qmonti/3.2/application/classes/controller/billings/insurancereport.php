<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Insurancereport extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('insurancereport', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('insurancereport', self::FILE_TYPE_JS));
        $view_insurancereport = new View('billings/insurancereport');
        $insurance_carrier = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();
        $view_insurancereport->insurance_carrier = $insurance_carrier;
        $this->template->content = $view_insurancereport;
    }

    public function action_reportInsuranceInvoices() {
        $this->auto_render = FALSE;
        try {

            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $idInsurance = $this->request->query('idInsurance');
            $oInsurance = new Model_Insurancecarriers($idInsurance);
            $data['Insurance'] = $oInsurance->name;
            $dateFrom = $this->request->query('dateFrom');
            $dateTo = $this->request->query('dateTo');

            $columns = array(DB::expr("CONCAT(ic.name,' for ', DATE_FORMAT(app.appointmentdate, '%M - %Y')) as titleOut"),
                DB::expr("CASE WHEN bi.typebilling = 1 THEN 'Transportation' ELSE 'Translation' END as typebillingOut"),
                DB::expr("SUBSTRING_INDEX(bd.title, '|', 1) as typeserviceOut"),
                DB::expr("CASE
			    WHEN bd.detailType = 'driverAssistance' THEN 'Car Assistance'
			    WHEN bd.detailType = 'driverEarly' THEN 'Early Pick Up'
			    WHEN bd.detailType = 'driverLate' THEN 'Late Drop off'
			    WHEN bd.detailType = 'driverLift' THEN 'Lift Base'
                            WHEN bd.qty =1 AND bi.typebilling = 1 AND bd.detailType = 'driverNormal' THEN 'Flate Rate'
			    WHEN bd.detailType = 'driverNormal' THEN 'Mileage'
			    WHEN bd.detailType = 'driverNoShow' THEN 'No Show'
			    WHEN bd.detailType = 'driverOther' THEN 'Other'
			    WHEN bd.detailType = 'driverSurcharge' THEN 'Fuel Surchage'
			    WHEN bd.detailType = 'driverWait' THEN 'Waiting Time'
			    WHEN bd.detailType = 'driverWeekend' THEN 'Weekend fee'
			    WHEN bd.detailType = 'interMileage' THEN 'Travel Mileage'
			    WHEN bd.detailType = 'interNormal' THEN ''
			    WHEN bd.detailType = 'interNormalDocTransPage' THEN 'Document Translation Pages'
			    WHEN bd.detailType = 'interNormalDocTransWord' THEN 'Document Translation Word'
			    WHEN bd.detailType = 'interTime' THEN 'Interpreting Time'
			    WHEN bd.detailType = 'intNormalConference' THEN 'Conference Call'
			    WHEN bd.detailType = 'intNoShow' THEN 'No Show'
			    ELSE bd.detailType
			END as detailOut"),
                DB::expr("CAST(AVG(CASE WHEN bd.qty =1 AND bi.typebilling = 1 AND bd.detailType = 'driverNormal' THEN bd.qty ELSE bd.ratio END) AS DECIMAL(11,2)) as rateOut"),
                DB::expr("pat.regiondefault as stateOut"),
                DB::expr("SUM(
			
			  CASE WHEN bd.qty =1 AND bi.typebilling = 1 AND bd.detailType = 'driverNormal' THEN bd.ratio ELSE bd.qty END
			) as quantityOut"),
                DB::expr("SUM(bd.amount) as totalOut")
            );


            $response = DB::select_array($columns)
                    ->from(array("appointment", 'app'))
                    ->join(array("patients", 'pat'))->on('app.idPatient', '=', 'pat.idPatient')
                    ->join(array("claims", 'cl'))->on('app.idClaim', '=', 'cl.idClaim')
                    ->join(array("insurancecarrier", 'ic'))->on('cl.idInsuranceCarrier', '=', 'ic.idInsurancecarrier')
                    ->join(array("billingappointments", 'ba'))->on('ba.idAppointment', '=', 'app.idAppointment')
                    ->join(array("billingdetails", 'bd'))->on('ba.idBillingAppointments', '=', 'bd.idBillingAppointments')
                    ->join(array("billing", 'bi'))->on('ba.idBilling', '=', 'bi.idBilling')
                    ->where('ic.idInsurancecarrier', '=', $idInsurance)
                    ->and_where('ba.estado', '>', '0')
                    ->and_where('bd.estado', '=', '1')
                    ->and_where('bi.status', '>', '0');
            if ($dateFrom != '' and $dateTo != '') {
                $response->and_where('app.appointmentdate', 'BETWEEN', array($this->cambiafmysqlre($dateFrom), $this->cambiafmysqlre($dateTo)));
            }

            $sql_response = $response->execute()->as_array();

            $this->fnResponseFormat($sql_response, self::REPORT_RESPONSE_TYPE_XML, $data);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

}
