<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Managerchecks extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('managerchecks', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('managerchecks', self::FILE_TYPE_JS));
        $view_managerchecks = new View('billings/managerchecks');
        $insurance_carrier = DB::select(array('idInsurancecarrier', 'idInsurancecarrier'), array('name', 'name'))
                        ->from("insurancecarrier")->where('status', '=', '1')
                        ->execute()->as_array();

        $view_managerchecks->insurance_carrier = $insurance_carrier;


        $this->template->content = $view_managerchecks;
    }

    public function action_listChecks() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');


        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                "check_id" => "ch.idCheck"
                , "insurance_id" => "i.idInsurancecarrier"
                , "insurance" => "i.name"
                , "numbercheck" => "ch.numberCheck"
                , "amount" => "ch.amount"
                , "paid" => "ch.paid"
                , "memo" => "ch.memo"
                , "datecheck" => "DATE_FORMAT(ch.dateCheck, '%m/%d/%Y')"
                , "dateregistration" => "DATE_FORMAT(ch.dateRegistration, '%m/%d/%Y')"
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " checks ch
                INNER JOIN insurancecarrier i
                    ON ch.idInsuranceCarrier = i.idInsurancecarrier";

            $s_where_conditions = 'ch.status=1 AND i.status=1   ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_getCheckById() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $idCheck = $this->request->post('idCheck');
            $o_check = new Model_Checks($idCheck);
            $result = $o_check->as_array();

            $a_response['data'] = $result;
        } catch (Exception $exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $exc->getMessage();
        }

        $this->fnResponseFormat($a_response);
    }

    public function action_saveCheck() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $post = Validation::factory($this->request->post())
                    ->rule('idCheck', 'not_empty')
                    ->rule('idInsuranceCarriers', 'not_empty')
                    ->rule('amount', 'not_empty')
                    ->rule('dateCheck', 'not_empty')
                    ->rule('numberCheck', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                Database::instance()->begin();
                $o_check = new Model_Checks($post_data['idCheck']);
                if ($post_data['amount'] > $o_check->paid) {
                    $o_check->createUpdateCheck($post_data);
                    Database::instance()->commit();
                    $a_response['response'] = $o_check->idCheck;
                } else {
                     $a_response['msg'] = 'Not';   
                     $a_response['response'] = 'Monto ingresado es menor al monto ya utilizado. Ingrese un Monto superior o igual a '.$o_check->paid;
                }
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_deleteCheckById() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
             $post = Validation::factory($this->request->post())
                    ->rule('idCheck', 'not_empty');
            if (!$post->check()) {
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } else {
                $post_data = $post->data();
                $o_check = new Model_Checks($post_data['idCheck']);
                $o_check->status = self::STATUS_DEACTIVE;   
                $o_check->save(); 
            }
        } catch (Exception $e_exc) {

            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
    }

}
