<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Interpreterlog extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('interpreterlog', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('interpreterlog', self::FILE_TYPE_JS));
        $view_interpreterlog = new View('billings/interpreterlog');
        $this->template->content = $view_interpreterlog;
    }

    public function action_listInterpreterLog() {
        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');

            //$s_all = $this->request->query('all');

            $i_idtask = $this->request->query('task_id');

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                "billing_interpreter_id" => "bi.idBillingInterpreter"
                , "interpreter_out" => "CONCAT(inte.name,' ' ,inte.lastName)"
                , "amount_out" => "bi.amount"
                , "date_billing" => "DATE_FORMAT( bi.dateBilling , '%m/%d/%Y' )"
                , "date_register" => "DATE_FORMAT( bi.creationDate , '%m/%d/%Y %h:%i %p' )"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = "billinginterpreter bi" .
                    $s_tables_join = " INNER JOIN interpreters inte ON bi.idInterpreter = inte.idInterpreter" .
                    $s_tables_join = " INNER JOIN billinginterpreterappointment bia ON bi.idBillingInterpreter = bia.idBillingInterpreter" .
                    $s_tables_join = " INNER JOIN appointment app ON bia.idAppointment = app.idAppointment";
            $s_where_conditions = " bi.status = " . self::STATUS_ACTIVE . " AND bia.status = " . self::STATUS_ACTIVE;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function action_reportInterpreterLog() {
        $this->auto_render = FALSE;
        try {
            $date_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_DATE, $this->request->query(self::RQ_DATE_FORMAT));
            $time_format = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME, $this->request->query(self::RQ_DATE_FORMAT));
            $time_raw = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_RAW, $this->request->query(self::RQ_DATE_FORMAT));
            $time_limit = $this->getDATE_FORMAT_FOR(self::DF_SQL_TIME_LIMIT, $this->request->query(self::RQ_DATE_FORMAT));

            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $idBillingInterpreter = $this->request->query('idBillingInterpreter');
            $a_details = array();
            $parameters = array(
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':date_format' => $date_format,
                ':time_format' => $time_format,
                ':s_active' => self::STATE_ACTIVE,
                ':s_inactive' => self::STATE_INACTIVE
            );

            $o_billing_interpreter = new Model_Billinginterpreter($idBillingInterpreter);

            $o_interpreter = new Model_Interpreters($o_billing_interpreter->idInterpreter);


            $data['inter_fullname'] = $o_interpreter->name.' '.$o_interpreter->lastname;
            $data['inter_address'] = $o_interpreter->address;
            $data['inter_phone'] = $o_interpreter->phone;
            $data['idBilling'] = $o_billing_interpreter->idBillingInterpreter;
            $data['totalAmount'] = $o_billing_interpreter->amount;
            $data['date'] = $this->cambiafmysql($o_billing_interpreter->datebilling);

            $columns = array(array('app.appointmentdate', 'appdate'),
                array('app.appointmenttime', 'apptime'),
                array('bid.detailType', 'detailType'),
                array('bid.title', 'title'),
                array('bid.qty', 'quanty'),
                array('bid.ratio', 'ratio'),
                array('bid.amount', 'amount'),
                DB::expr("IF(biac.status=1, biac.comment, '') as coment")
            );
            $sql_string = DB::select_array($columns)
                            ->from(array("billinginterpreter", 'bi'))
                            ->join(array('billinginterpreterappointment', 'bia'))->on('bi.idBillingInterpreter', '=', 'bia.idBillingInterpreter')
                            ->join(array('appointment', 'app'))->on('app.idAppointment', '=', 'bia.idAppointment')
                            ->join(array('billinginterpreterdetail', 'bid'))->on('bia.idBillingInterpreterAppointment', '=', 'bid.idBillingInterpreterAppointment')
                            ->join(array('billinginterpreterappointmentcomment', 'biac'), "LEFT")->on('biac.idBillingInterpreterAppointment', '=', 'bia.idBillingInterpreterAppointment')
                            ->where('bi.idBillingInterpreter', '=', $idBillingInterpreter)
                            ->and_where('bid.status', '=', '1')
                            ->parameters($parameters)
                            ->execute()->as_array();

            foreach ($sql_string as $abillingdetail) {
                $explodeTitle = explode('|', $abillingdetail['title']);
                switch ($abillingdetail['detailType']) {
                    case 'interNormal':
                        $title = $explodeTitle[0] . ' (' . $explodeTitle[1] . ')';
                        $title .= '{br}' . $explodeTitle[2] . ', ' . $explodeTitle[3];
                        break;
                    case 'interNormalDocTransWord':
                        $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                        break;
                    case 'interNormalDocTransPage':
                        $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                        break;
                    case 'intNoShow':
                        $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . ' Interpreter (' . $explodeTitle[2] . ')';
                        $o_app = $abillingdetail->BillingInterpreterAppointments->Appointments;
//                        $o_course = new Model_Course($o_app->idCourse);
//                        $o_location_from = new Model_Locations($o_course->idLocationOrigen);
//                        $o_location_to = new Model_Locations($o_course->idLocationDestination);
//                        if ($o_app->idTreatmentOffice > 0) {
//                            $objTreatmentOffice = new Treatmentoffice();
//                            $objTreatmentOffice->find(array('idTreatmentOffice' => $o_app->idTreatmentOffice));
//                            $title .= '{br}Contact: ' . $objTreatmentOffice->contact . ', ' . $objTreatmentOffice->address;
//                        }
                        break;
                    default:
                        $title = $explodeTitle[1];
                        break;
                }
                array_push($a_details, array(
                    'appDate' => $this->cambiafmysql($abillingdetail['appdate']),
                    'appTime' => $abillingdetail['apptime'],
                    'title' => $title,
                    'qty' => ($abillingdetail['quanty']) * 1,
                    'ratio' => $abillingdetail['ratio'],
                    'amount' => $abillingdetail['amount'],
                    'detailType' => $abillingdetail['detailType']
                        )
                );
            }
            $this->fnResponseFormat($a_details, self::REPORT_RESPONSE_TYPE_XML, $data);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}