<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Quickbooksexports extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('quickbooksexports', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('quickbooksexports', self::FILE_TYPE_JS));
        $view_quickbooksexports = new View('billings/quickbooksexports');
        $this->template->content = $view_quickbooksexports;
    }

}
