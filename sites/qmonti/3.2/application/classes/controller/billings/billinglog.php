<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Billinglog extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('billinglog', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('billinglog', self::FILE_TYPE_JS));
        
          $type_appointment = DB::select(array('idTypeappointment', 'id'), array('name', 'nameapp'))
                        ->from("typeappointment")->where('status', '=', '1')
                        ->execute()->as_array();
           $drivers = DB::select(array('idDriver', 'idDriver'), array('name', 'name'), array('lastname', 'lastname'))
                        ->from("drivers")->where('status', '=', '1')
                        ->execute()->as_array();
        $languaje = DB::select(array('idLanguage', 'idLanguage'), array('name', 'name'))
                        ->from("languages")->where('status', '=', '1')
                        ->execute()->as_array();

        $interprete = DB::select(array('idInterpreter', 'idInterpreter'), array('name', 'name'))
                        ->from("interpreters")->where('status', '=', '1')
                        ->execute()->as_array();
$area_driver = DB::select(array('idAreasdriver', 'id'), array('location', 'area'))
                        ->from("areasdriver")->where('status', '=', '1')
                        ->execute()->as_array();
  $view_billinglog = new View('billings/billinglog');
        $view_billinglog->area_driver = $area_driver;
        $view_billinglog->type_appointment = $type_appointment;
        $view_billinglog->drivers = $drivers;
        $view_billinglog->languaje = $languaje;
        $view_billinglog->interprete = $interprete;                
      
        $this->template->content = $view_billinglog;
    }

    public function action_listInvoices() {
        try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');
            $i_idtask = $this->request->query('task_id');

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idBilling' => 'b.idBilling',
                'invoiceNumber' => 'b.invoiceNumber',
                'patientName' => "CONCAT(p.firstname, ' ' , p.lastname)",
                'insuranceName' => 'inc.name',
                'typebilling' => "CASE WHEN b.typebilling = 1 THEN 'Transportation' ELSE 'Translation' END",
                'ntypebilling' => "b.typebilling",
                'fechaRegistro' => 'DATE_FORMAT(b.dateRegistration,\'%m/%d/%Y %h:%i %p\')',
                //'horaRegistro' => 'DATE_FORMAT(b.dateRegistration,\'%h:%i %p\')',
                'total' => 'SUM(COALESCE(bd.amount,0)) ',
                'isLost' => 'b.lostPayment',
                'paid' => "CAST(COALESCE(b.amount,0) AS DECIMAL(10,2)) total,
 (
	SELECT COALESCE(SUM(COALESCE(bc.amount, 0)) ,0)
	FROM billingchecks bc
        WHERE b.idBilling = bc.idBilling AND bc.state >= 1
 
 )",
//                'idPatient' => "pat.idPatient",
//                'namepatient' => "CONCAT(pat.firstname, ' ', pat.lastname)",
//                'totalapp' => "COUNT(DISTINCT app.idAppointment)",
//                'translation' => "COUNT(DISTINCT IF(app.idInterpreter > 0,app.idAppointment,NULL))",
//                'transportation' => "COUNT(DISTINCT IF(app.idDriverOneWay > 0,app.idAppointment,NULL))",
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " billing b
                LEFT JOIN billingchecks bc
                    ON b.idBilling = bc.idBilling AND bc.state >= 1
                INNER JOIN patients p
                    ON b.idPatients = p.idPatient
                INNER JOIN billingappointments ba
                    ON b.idBilling = ba.idBilling
                INNER JOIN billingdetails bd
                    ON ba.idBillingAppointments = bd.idBillingAppointments
                INNER JOIN appointment app
                    ON ba.idAppointment = app.idAppointment
                INNER JOIN claims cl
                    ON app.idClaim = cl.idClaim
                INNER JOIN insurancecarrier inc
                    ON cl.idInsuranceCarrier = inc.idInsurancecarrier ";

            $s_where_conditions = "b.status=1 AND ba.estado = 1 AND bd.estado = 1 ";
            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);
            $s_where_conditions .= ' GROUP BY b.idBilling';
//            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start, $s_group);
            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);
            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }
    
    public function action_getInfoAppForBilling() {
        try {

            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;

//           print_r($a_idsApp);
//           die();
            $idBilling = $this->request->post('idBilling');
            if (isset($idBilling)) {

                $idsApp = DB::select(DB::expr('GROUP_CONCAT(idBillingAppointments) as idsapp'))
                                ->from('billingappointments')->where('idBilling', '=', $idBilling)
                                ->execute()->current();
                
                $a_idsApp = $idsApp['idsapp'];
//                print_r($a_idsApp);
//                die();
//                $oBilling=new Model_Billing($idBilling);
//                $a_response['data']=$oBilling
            }
            else{
                
                $a_idsApp = implode(",", $this->request->post('appIds'));
               
            }
            $a_response['data'] = DB::select_array(array(
                                array("app.wtime", "app_wtime")
                                , array("app.wtimecode", "app_wtime_code")
                                , array("app.wtimehours", "app_wtime_hours")
                                , DB::expr('DATE_FORMAT(CURDATE(),"%m/%d/%Y") as d_current_date')
                                , array("app.wtimeminutes", "app_wtime_minutes")
//                     ,DB::expr(" CASE
//                WHEN app.typetravel = 'OneWay'
//                    THEN
//                        CASE
//                            WHEN app.typetravel = 'OneWay'
//                                THEN COALESCE(pt.milesBilling, 0)
//                            ELSE
//                                COALESCE(CAST((pt.milesBilling*2) AS UNSIGNED), 0)
//                        END
//                ELSE
//                    CASE
//                        WHEN app.typetravel = 'OneWay'
//                            THEN COALESCE(CAST((pt.milesBilling/2) AS UNSIGNED), 0)
//                        ELSE
//                            COALESCE(pt.milesBilling, 0)
//                    END
//            END AS milesBilling")  
                                , DB::expr("IF(app.lbase='on',1,0) as lbase")
                                , DB::expr("IF(app.cassistance='on',1,0) as cassistance")
                                , array("app.billingtransp", "billingtransp")
                                , array("app.billinginterp", "billinginterp")
                                , array("app.idAlias", "idAlias")
                                , array("app.appauthorized", "appauthorized")
                                , array("app.frate", "frate")
                                , array("app.lbase", "lbase")
                                , array("tapp.name", "typeAppointment")
                                , array("app.cassistance", "cassistance")
                                , array("app.whocalls", "whocalls")
                                , array("app.companyName", "companyName")
                                , array("app.appAuthorized", "appAuthorized")
                                , array("app.typeapp", "typeapp")
                                , DB::expr("COALESCE(p.regionDefault,'No Selected in Patient') as tipociudad")
                                , array("app.idAppointment", "idAppointment")
                                , DB::expr("DATE_FORMAT(app.appointmentDate,'%m/%d/%Y') AS appointmentDate")
                                , array("app.typeservicesInterpreter", "typeservicesInterpreter")
                                , array("app.idInterpreter", "idInterpreter")
                                , array("app.idDriverOneWay", "idDriverOneWay")
                                , array("app.appointmentTime", "appointmentTime")
                                , array("app.dropTime", "dropTime")
                                , array("app.pickUpTime", "pickUpTime")
                                , array("app.idClaim", "idClaims")
                                , array("app.idDriverRoundTrip", "idDriversDrop")
                                , array("app.idLanguage", "idLanguages")
                                , array("app.idPatient", "idPatients")
                                , array("app.idDriverOneWay", "idDriverOneWay")
                                , array("app.idDriverRoundTrip", "idDriverRoundTrip")
                                , array("app.idLocation", "idLocation")
                                , DB::expr("(CASE WHEN app.idDriverOneWay >0 THEN 'Yes' ELSE 'No' END) as transportation")
                                , DB::expr("(CASE WHEN app.idInterpreter>0 THEN 'Yes' ELSE 'No' END) as translation")
                                , DB::expr("(CASE WHEN COALESCE(app.typetravel,0)=1 THEN 'One Way' ELSE 'Round Trip' END)  as typetravel")
                                , DB::expr("COALESCE(driPick.name,'-') AS driverPickUpName")
                                , DB::expr("COALESCE(driPick.lastName,'-') AS  driverPickUpLastName")
                                , DB::expr("COALESCE(driDrop.name,'-') AS driverDropName")
                                , DB::expr("COALESCE(driDrop.lastName,'-') AS  driverDropLastName")
                                , DB::expr("COALESCE( inter.name,'-') AS interpreterName")
                                , DB::expr("COALESCE( inter.lastName,'-') AS interpreterLastName")
                                , array("lan.name", "languages")
                                , array("lan.type", "typelanguage")
                                , array("inc.name", "insurance")
                                , array("adj.name", "adjuster")
                                , array("app.idLocation", "idLocation")
                                , array("app.userCreate", "idUsers")
                                , array("app.miles", "miles")
                                , array("app.typeservicesInterpreter", "typeservicesInterpreter")
                                , array("app.typeservice", "typeservice")
                                , "c.*"
                                , array("p.socialsecurity", "socialsecurity")
                                , array("p.firstName", "firstName")
                                , array("p.lastName", "lastName")
                                , array("p.address", "address")
                                , array("loc.address", "caddress")
                                , array("loc.city", "ccity")
                                , array("loc.state", "cstate")
                                , array("loc.zipcode", "czipcode")
                                , array("loc.suite", "csuite")
                                , array("loc.alias", "calias")
                                , array("loc1.address", "oaddress1")
                                , array("loc1.city", "ocity1")
                                , array("loc1.state", "ostate1")
                                , array("loc1.suite", "osuite1")
                                , array("loc1.zipcode", "ozipcode1")
                                , array("loc1.alias", "oalias1")
                                , array("loc2.address", "oaddress2")
                                , array("loc2.city", "ocity2")
                                , array("loc2.state", "ostate2")
                                , array("loc2.suite", "osuite2")
                                , array("loc2.zipcode", "ozipcode2")
                                , array("loc2.alias", "oalias2")
                                , array("summ.idTranslationSummary", "idTranslationSummary")
                                , array("summ.interpretationTimeFrom", "interpretationTimeFrom")
                                , array("summ.interpretationTimeTo", "interpretationTimeTo")
                                , array("summ.interpretationTimeTotalTime", "interpretationTimeTotalTime")
                                , array("summ.travelFrom", "travelFrom")
                                , array("summ.travelTo", "travelTo")
                                , array("summ.totalMileage", "totalMileage")
                                , array("summ.travelTotalTime", "travelTotalTime")
                                , array("bid.idBillingAppointments", "idBillingApp")
                                , array("bi.idBilling", "idBilling")
                                , array("bi.invoiceNumber", "invoiceNumber")
                                , array("bi.typebilling", "typeBilling")
                                , DB::expr("COALESCE(app.showdriver,0) as noShowDriver")
                                , DB::expr("COALESCE(app.showinterpreter,0) as noShowInterpreter")
                            ))
                            ->from(array('appointment', 'app'))
                            ->join(array('claims', 'c'))->on('c.idClaim', '=', 'app.idClaim')
                            ->join(array('insurancecarrier', 'inc'))->on('inc.idInsurancecarrier', '=', 'c.idInsuranceCarrier')
                            ->join(array('adjusters', 'adj'))->on('adj.idAdjuster', '=', 'c.idAdjuster')
                            ->join(array('languages', 'lan'), 'LEFT')->on('lan.idLanguage', '=', 'app.idLanguage')
                            ->join(array('patients', 'p'))->on('p.idPatient', '=', 'app.idPatient')
                            ->join(array('drivers', 'driDrop'), 'LEFT')->on('app.idDriverRoundTrip', '=', 'driDrop.idDriver')
                            ->join(array('drivers', 'driPick'), 'LEFT')->on('app.idDriverOneWay', '=', 'driPick.idDriver')
                            ->join(array('interpreters', 'inter'), 'LEFT')->on('app.idInterpreter', '=', 'inter.idInterpreter')
                            ->join(array('course', 'cou'), 'LEFT')->on('app.idCourse', '=', 'cou.idCourse')
                            ->join(array('locations', 'loc2'), 'LEFT')->on('cou.idLocationDestination', '=', 'loc2.idLocation')
                            ->join(array('locations', 'loc1'), 'LEFT')->on('cou.idLocationOrigen', '=', 'loc1.idLocation')
                            ->join(array('locations', 'loc'), 'LEFT')->on('app.idLocation', '=', 'loc.idLocation')
                            ->join(array('billingappointments', 'bid'), 'LEFT')->on('bid.idAppointment', '=', 'app.idAppointment')
                            ->join(array('billing', 'bi'), 'LEFT')->on('bid.idBilling', '=', 'bi.idBilling')
                            ->join(array('typeappointment', 'tapp'))->on('app.idtypeAppointment', '=', 'tapp.idtypeAppointment')
                            ->join(array('translationsummary', 'summ'), 'LEFT')->on('app.idAppointment', '=', 'summ.idAppointment')
                            ->where('p.status', '=', '1');
                    if (isset($idBilling)) {
//                            ->and_where(DB::expr("FIND_IN_SET(bid.idAppointment,'$a_idsApp')"), '>', 0)
                                                                 $a_response['data']= $a_response['data']   ->and_where("bi.idBilling", '=', $idBilling)
                                                                    ->and_where("bid.estado", '=', '1');
                    } else 
                            $a_response['data']= $a_response['data']->and_where(DB::expr("FIND_IN_SET(app.idAppointment,'$a_idsApp')"), '>', 0); 
                            $a_response['data']= $a_response['data']->order_by('app.appointmentDate', 'ASC')
                            ->order_by(DB::expr("STR_TO_DATE(UPPER (app.appointmentTime), '%h:%i %p')"), 'ASC')
                            ->execute()->as_array();
//        echo database::instance()->last_query;
//        die();
////          
        } catch (Exception $e_exc) {
            //echo $e->getMessage();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_getDetailsByBilling() {
        try {

            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;
            
            $idBillingApp=$this->request->post('idBillingApp');
            
            $a_response['details']= DB::select_array(array(
                                       array("bd.idBillingAppointments", "idBillingApp")
                                     , array("bd.idBillingDetails", "idBillingDetail")
                                     , array("bd.title", "title")
                                     , array("bd.qty", "qty")
                                     , array("bd.ratio", "ratio")
                                     , array("bd.amount", "amount")
                                     , array("bd.typeTransDoc", "transDoc")
                                     , array("bd.detailType", "detailType")
                                     , array("bd.comment", "commentDetail")
                                    ))
                                 ->from(array('billingdetails','bd'))    
                                 ->where('bd.idBillingAppointments','=',$idBillingApp)
                                 ->and_where('bd.estado','=',1)
                                 ->execute()->as_array();
            
            $a_response['comments']= DB::select_array(array(
                                       array("bc.idBillingAppointments", "idBillingApp")
                                     , array("bc.idBillingComments", "idBillingComments")
                                     , array("bc.comment", "commentCom")
                                    ))
                                 ->from(array('billingcomments','bc'))    
                                 ->where('bc.idBillingAppointments','=',$idBillingApp)
                                 ->and_where('bc.estado','=',1)
                                 ->execute()->as_array();

        } catch (Exception $e_exc) {
            //echo $e->getMessage();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_listMoreApps() {
   
             try {
            $this->auto_render = FALSE;

            $i_page = $this->request->query('page');
            $i_limit = $this->request->query('rows');
            $i_idx = $this->request->query('sidx');
            $s_ord = $this->request->query('sord');
            $i_idtask = $this->request->query('task_id');

            $i_idPatient = $this->request->query('idPatient');

            if (!$i_idx)
                $i_idx = 1;

            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'idAppointment' => "app.idAppointment",
                'appointmentdate' => "DATE_FORMAT(app.appointmentdate, '%m/%d/%Y')",
                'namepatient' => "CONCAT(pat.firstname,' ',pat.lastname)",
                'addresspatient' => "pat.address",
                'phonepatient' => "pat.phonenumber",
                'contactname' => "IF(app.idLocation IS NULL,loc2.alias,loc.alias)",
                'contactaddress' => "IF(app.idLocation IS NULL,loc2.address,loc.address)",
                'contactphone' => "IF(app.idLocation IS NULL,loc2.telephone, loc.telephone)",
                'namedriver1' => "CONCAT(COALESCE(driPick.name,'-'),' ',COALESCE(driPick.lastname,'-'))",
                'phonedriver1' => "driPick.phone",
                'namedriver2' => "CONCAT(COALESCE(driDrop.name,'-'),' ',COALESCE(driDrop.lastname,'-'))",
                'phonedriver2' => "driDrop.phone",
                'nameinterprete' => "CONCAT(COALESCE(inter.name, '-'),' ',COALESCE(inter.lastname, '-'))",
                'phoneinterprete' => "COALESCE(inter.phone, '-')"
                
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
                    //B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
                    //B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " appointment app " .
                    $s_tables_join = " INNER JOIN claims c ON c.idClaim = app.idClaim " .
                    $s_tables_join = " INNER JOIN insurancecarrier inc ON inc.idInsurancecarrier = c.idInsurancecarrier " .
                    $s_tables_join = " INNER JOIN patients pat ON pat.idPatient = app.idPatient  " .
                    $s_tables_join = " LEFT JOIN drivers driDrop ON app.idDriverRoundTrip = driDrop.idDriver  " .
                    $s_tables_join = " LEFT JOIN drivers driPick ON app.idDriverOneWay = driPick.idDriver  " .
                    $s_tables_join = " LEFT JOIN interpreters inter ON app.idInterpreter = inter.idInterpreter  " .
                    $s_tables_join = " LEFT JOIN locations loc ON loc.idLocation = app.idLocation  " .
                    $s_tables_join = " LEFT JOIN course cou ON cou.idCourse = app.idCourse  " .
                    $s_tables_join = " LEFT JOIN course origen ON origen.idLocationOrigen = app.idLocation  " .
                    $s_tables_join = " LEFT JOIN course destination ON destination.idLocationdestination = app.idLocation  " .
                    $s_tables_join = " LEFT JOIN locations loc1 ON cou.idLocationOrigen = loc1.idLocation" .
                    $s_tables_join = " LEFT JOIN locations loc2 ON cou.idLocationDestination = loc2.idLocation " .
                    $s_tables_join = " LEFT JOIN translationsummary ts ON app.idAppointment = ts.idAppointment";

            $s_where_conditions = "app.state = 'APP_CONFIRMED' AND app.status=1 AND pat.status =" . self::STATUS_ACTIVE .
//                    $s_where_conditions = "  AND (CASE WHEN app.idInterpreter > 0 THEN app.appconfirmedconfirmationinterpreter = 1 AND  app.confirmedsentinterpreter = 3 " .
//                    $s_where_conditions = "  AND CASE WHEN app.typeservicesinterpreter = 'INTERPRETING' THEN ts.idTranslationSummary > 0 WHEN app.typeservicesinterpreter = 'CONFERENCECALL' " .
//                    $s_where_conditions = "  THEN ts.idTranslationSummary > 0 ELSE TRUE END ELSE TRUE END ) " .
//                    $s_where_conditions = "  AND (CASE WHEN (app.idDriverOneWay > 0 AND app.idDriverRoundTrip > 0 AND (app.idDriverOneWay = app.idDriverRoundTrip)) " .
//                    $s_where_conditions = "  THEN app.confirmedsentdriver1 = 1 AND app.appconfirmedconfirmationdriver1 = 1 WHEN (app.idDriverOneWay > 0 AND app.idDriverRoundTrip > 0) " .
//                    $s_where_conditions = "  THEN (app.confirmedsentdriver1 = 1 AND app.appconfirmedconfirmationdriver1 = 1) AND ( app.appconfirmedconfirmationdriver2 = 1 AND app.confirmedsentdriver2 = 1 )  " .
//                    $s_where_conditions = "  WHEN (app.idDriverOneWay > 0) THEN (app.confirmedsentdriver1 = 1 AND app.appconfirmedconfirmationdriver1 = 1) " .
//                    $s_where_conditions = "  WHEN (app.idDriverOneWay > 0) THEN (app.appconfirmedconfirmationdriver2 = 1 AND app.confirmedsentdriver2 = 1)ELSE TRUE  END ) " .

                    $s_where_conditions = "  AND ( ( (app.idInterpreter != 0  and NOT ISNULL(app.idInterpreter))  " .
                    $s_where_conditions = "  AND   (app.billinginterp = 0 OR ISNULL(app.billinginterp)) " .
                    $s_where_conditions = "  AND   (app.cancelinterpreter = 0 OR ISNULL(app.cancelinterpreter)) ) " .
                    $s_where_conditions = "  OR  ( (app.idCourse != 0  and NOT ISNULL(app.idCourse))  " .
                    $s_where_conditions = "  AND   (app.billingtransp = 0 OR ISNULL(app.billingtransp)) " .
                    $s_where_conditions = "  AND   (app.canceldriver = 0 OR ISNULL(app.canceldriver)) )) AND pat.idPatient = " . $i_idPatient .
                    $s_where_conditions = " GROUP BY app.idAppointment ";



            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
       
    }
    
    public function action_lostInvoice() {
        try {

            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;
            
            $idBilling=$this->request->post('idBilling');
            
            $oBilling= new Model_Billing($idBilling);
            $oBilling->lostPayment= 1 ;
            $oBilling->save();

        } catch (Exception $e_exc) {
            //echo $e->getMessage();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
    public function action_deleteInvoice() {
        try {

            $this->auto_render = FALSE;
            $a_response = $this->json_array_return;
           
            $idBilling=$this->request->post('idBilling');
            $oBilling= new Model_Billing($idBilling);
            $oBillingApp = new Model_BillingAppointments();
            $aBillingApp=$oBillingApp->where('idBilling','=',$idBilling)->and_where('estado','=',1)->find_all();
            
            foreach($aBillingApp as $billapp){

                $billapp->estado=0;
                $billapp->save();
                $oAppointment= new Model_Appointment($billapp->idAppointment);
                if($oBilling->typebilling == 1) $oAppointment->billingtransp=0;
   
                if($oBilling->typebilling == 2) $oAppointment->billinginterp=0;
               $oAppointment->save();
                }

            $oBilling->status = 0 ;
            $oBilling->save();

        } catch (Exception $e_exc) {

            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    
     public function action_getRatesInsurance() {
      $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $a_post = Validation::factory($this->request->post())
                    ->rule('idInsuranceCarrier', 'not_empty')
                    ->rule('tipociudad', 'not_empty');
            
           
            if (!$a_post->check()) {
            
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } 
            
            else {
            
                  $o_model_InsuranceRates = new Model_InsuranceRates(); 
                  $a_response['data']=$o_model_InsuranceRates->where('idInsuranceCarrier','=',$a_post['idInsuranceCarrier'])
                                                             ->and_where('cityType','=',$a_post['tipociudad'])->find()->as_array();   
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
        
    }
    
    public function action_updateRatesInsurance() {
      $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $a_post = Validation::factory($this->request->post())
                    ->rule('idInsurancecarrier', 'not_empty')
                    ->rule('idInsuranceRates', 'not_empty');
            
           
            if (!$a_post->check()) {
            
                throw new Exception('Hay Datos Faltantes', self::CODE_SUCCESS);
            } 
            
            else {
            
                  $o_model_InsuranceRates = new Model_InsuranceRates();
                  $o_model_InsuranceRates->saveInsuranceRates($a_post); 
                   Database::instance()->commit(); 
            }
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
        
    }

    public function fngetDescuentoAndBalanceById($idInsuranceCarrier){
         try {

            $subquery = DB::select(
                            array('b2.idInsuranceCarriers', 'idInsuranceCarrier')
                            , DB::expr('SUM(bc2.amount) as balanceTotal')
                    )->distinct(true)
                    ->from(array("billingchecks", 'bc2'))
                    ->join(array("billing", 'b2'))->on('bc2.idBilling', '=', 'b2.idBilling')
                    ->where('bc2.state', '>=', '1')
                    ->and_where('b2.idInsuranceCarriers', '=', $idInsuranceCarrier)
                    ->group_by('b2.idInsuranceCarriers');


            $result1 = DB::select(
                                    DB::expr('COALESCE(CAST(SUM(bd.amount*bi.discount/100) AS DECIMAL(10,2)),0.00) as descuentoTotal')
                                    , DB::expr('COALESCE(CAST(SUM(bd.amount) - COALESCE(bchecks.balanceTotal, 0) AS DECIMAL(10,2)),0.00) as balanceTotal')
                            )->distinct(true)
                            ->from(array("billing", 'bi'))
//                    no se usa   ->join(array("billingchecks",'bc'),'LEFT')->on('bi.idBilling', '=', 'bc.billingchecks')->on('bc.state','>=','1')
                            ->join(array($subquery, 'bchecks'), 'LEFT')->on('bi.idInsuranceCarriers', '=', 'bchecks.idInsuranceCarrier')
                            ->join(array('billingappointments', 'ba'))->on('bi.idBilling', '=', 'ba.idBilling')
                            ->join(array('billingdetails', 'bd'))->on('ba.idBillingAppointments', '=', 'bd.idBillingAppointments')
                            ->where('ba.estado', '>', 0)
                            ->and_where('bi.status', '>', 0)
                            ->and_where('bd.estado', '>', 0)
                            ->and_where('bi.idInsuranceCarriers', '=', $idInsuranceCarrier)
                            ->execute()->current();

            $subquery2 = DB::select(
                            array('ch.available', 'available')
                    )->distinct(true)
                    ->from(array("checks", 'ch'))
                    ->join(array("billingchecks", 'bc'), 'LEFT')->on('ch.idCheck', '=', 'bc.idChecks')
                    ->where('ch.amount', '>', 0)
                    ->and_where('ch.available', '>', 0)
                    ->and_where('ch.state', '=', 1)
                    ->and_where('ch.idInsuranceCarrier', '=', $idInsuranceCarrier);


            $result2 = DB::select(
                                    DB::expr('COALESCE(SUM(select_sub.available),0.00) as credit_insurance')
                            )->distinct(true)
                            ->from(array($subquery2, 'select_sub'))
                            ->execute()->current();

            $a_response['data'] = array(
                'descuentoTotal' => $result1['descuentoTotal']
                , 'balanceTotal' => $result1['balanceTotal']
                , 'credit_insurance' => $result2['credit_insurance']
            );
            
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        
        return $a_response;
    }
    
    public function action_reportBilling(){
        $this->auto_render = FALSE;
        try {

            $new_data = array();
            $new_data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $new_data['qlang'] = $this->request->query(self::RQ_LANG);
            $new_data['searchDate'] = date('l, F d, Y');
            $new_data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $new_data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";
            $new_data['paid'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/paid.jpg";
            
            $idBilling = $this->request->query('idBilling');
             
            if (isset($idBilling)) {

            $objBilling = new Model_Billing($idBilling);
           
            $arrayDetails = array();

            if ($objBilling->status >= 1) {

                if ($objBilling->typebilling == 1) {
                    $federalId = '26-0347129';
                } elseif ($objBilling->typebilling == 2) {
                    $federalId = '59-3578799';
                }

                $columns = array(array('ba.idBillingAppointments', 'idBillApp'),
                
                array('ba.countEdit','edition'), array('ba.idAppointment','idApp'),
               
            );
           
            $a_billingApp = DB::select_array($columns)
                    ->from(array("billingappointments", 'ba'))->where('idBilling', '=', $idBilling)
                    ->and_where('ba.estado', '=', '1')
                    ->execute()->as_array();
                
                foreach ($a_billingApp as $billingApps) {
                   
                        $objAppointment = new Model_Appointment($billingApps['idApp']);
                        $objClaim = new Model_Claims($objAppointment->idClaim);
                        $objAdjuster = new Model_Adjusters($objClaim->idAdjuster);
                        $objInsuranceCarrier = new Model_Insurancecarriers($objClaim->idInsuranceCarrier);
                        //Modifique el ultimo total es el mismo app
                        $objPatient = new Model_Patients($objAppointment->idPatient);
                        $apartment = ($objPatient->apartmentnumber != '0' AND $objPatient->apartmentnumber != 'n/a') ? ' ' . $objPatient->apartmentnumber . ' ' : '';
                        $dateRegister = date_create_from_format('Y-m-d', $objBilling->dateBilling);
                        $insuranceZipcode = $objInsuranceCarrier->zipcode != 0 ? ' ' . $objInsuranceCarrier->zipcode : '';
                        $patientZipcode = $objPatient->zipcode != 0 ? ' ' . $objPatient->zipcode : '';
                        $data = array(
                            'idBilling' => $idBilling,
                            'idFederal' => $federalId,
                            'insuranceName' => $objInsuranceCarrier->name,
                            'insuranceAddress' => $objInsuranceCarrier->address
                            . '{br}' . $objInsuranceCarrier->city
                            . ', ' . $objInsuranceCarrier->state
                            . $insuranceZipcode,
                            'insuranceTerms' => $objInsuranceCarrier->terms,
                            'typeDue' => $objInsuranceCarrier->terms,
                            'adjusterName' => $objAdjuster->name,
                            'patientName' => $objPatient->firstname . ' ' . $objPatient->lastname,
                            'patientAddress' =>
                            $objPatient->address . $apartment .
                            '{br}' . $objPatient->city . ', ' . $objPatient->state . $patientZipcode,
                            'claimNumber' => $objClaim->claimnumber,
                            'socialSecurity' => $objPatient->socialsecurity,
                            'dateRegister' => date_format($dateRegister, 'm/d/Y'),
                            'result' => 0
                        );

                        $details = DB::select_array(array(array('bd.detailType', 'detailType'),
                
                array('bd.title','title'),
                array('bd.qty','qty'),
                array('bd.ratio','ratio'),
                array('bd.amount','amount'),
                array('bd.idBillingDetails','id'),
            ))
                    ->from(array("billingdetails", 'bd'))->where('idBillingAppointments', '=', $billingApps['idBillApp'])
                    ->and_where('bd.estado', '=', '1')
                    ->execute()->as_array();

                        foreach ($details as $objBillingDetails) {
                            $explodeTitle = explode('|', $objBillingDetails['title']);

                            switch ($objBillingDetails['detailType']) {
                                case 'driverNormal':
                                    $title = $explodeTitle[0];
                                    $title .= '{br}From: ' . $explodeTitle[1] . ', ' . $explodeTitle[2];
                                    $title .= '{br}To: ' . $explodeTitle[3] . ', ' . $explodeTitle[4];
                                    break;
                                case 'interNormal':
                                    $title = $explodeTitle[0] . ' (' . $explodeTitle[1] . ')';
                                    $title .= '{br}' . $explodeTitle[2] . ', ' . $explodeTitle[3];
                                    break;
                                case 'driverNoShow':
                                  
                                    $objCourse = new Model_Course($objAppointment->idCourse);
                                    $objLocationFrom = new Model_Locations($objCourse->idLocationOrigen);
                                    $objLocationTo = new Model_Locations($objCourse->idLocationDestination);

                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] ;
                                    $title .= '{br}From: ' . $objLocationFrom->alias . ', ' . $objLocationFrom->address;
                                    $title .= '{br}To: ' . $objLocationTo->alias . ', ' . $objLocationTo->address;
                                    break;
                                case 'interNormalDocTransWords':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                                    break;
                                case 'interNormalDocTransPages':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . '(' . $explodeTitle[2] . ')';
                                    break;
                                case 'intNoShow':
                                    $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . ' Interpreter (' . $explodeTitle[2] . ')';
                             

                                    if ($objAppointment->idLocation > 0) {
                                        $objLocation= new Model_Locations($objAppointment->idLocation);
                                        $title .= '{br}Contact: ' . $objLocation->alias . ', ' . $objLocation->address;
                                    }
                                    break;
                                default:
                                    $title = $explodeTitle[1];
                                    break;
                            }

                            $appDate = $this->cambiafmysql($objAppointment->appointmentdate);
                            
                            $appTime = $objAppointment->appointmenttime;
                           
                            if (
                                    ( strpos($objBillingDetails['detailType'], 'Normal') == 0 OR
                                    strpos($objBillingDetails['detailType'], 'Normal') == false OR
                                    is_null(strpos($objBillingDetails['detailType'], 'Normal')) ) AND
                                    ( strpos($objBillingDetails['detailType'], 'NoShow') == 0 OR
                                    strpos($objBillingDetails['detailType'], 'NoShow') == false OR
                                    is_null(strpos($objBillingDetails['detailType'], 'NoShow')) )
                            ) {

                                $appDate = null;
                                $appTime = null;
                            }

                            array_push($arrayDetails, array(
                                "idBillingDetails" => $objBillingDetails['id'],
                               
                                'appDate' => $appDate,
                                'appTime' => $appTime,
                                'title' => $title,
                                'qty' => ($objBillingDetails['qty']) * 1,
                                'ratio' => $objBillingDetails['ratio'],
                                'amount' => $objBillingDetails['amount'],
                                'detailType' => $objBillingDetails['detailType']
                                    )
                            );
                        }
                        
                    $a_comment= DB::select_array(array(array('bc.idBillingComments', 'idBillCom'),array('bc.comment', 'comment')))
                    ->from(array("billingcomments", 'bc'))->where('idBillingAppointments', '=', $billingApps['idBillApp'])
                    ->and_where('bc.estado', '=', '1')
                    ->execute()->as_array();
                 foreach($a_comment as $comment){               
                if ($comment['idBillCom'] > 0) {
                            $title = 'Notes: ' . $comment['comment'];
                            array_push($arrayDetails, array(
                                "idBillingDetails" => null,
                                
                                'appDate' => null,
                                'appTime' => null,
                                'title' => $title,
                                'qty' => null,
                                'ratio' => null,
                                'amount' => null,
                                'detailType' => null
                                    )
                            );
                        }
                    }
                }
               
            }

            $data['lostPayment'] = $objBilling->lostPayment;

            $data['totalAmount'] = $objBilling->amount;
            $data['discountInvoice'] = $objBilling->amountDiscount;
            $paid = DB::select(DB::expr('COALESCE(SUM(amount),0) as totalpaid'))->from('billingchecks')->where('idBilling','=',$objBilling->idBilling)
                                                            ->and_where('state','=',1)->execute()->current();   
            $data['paidOut'] = $paid['totalpaid'];
            $data['balanceOut'] = $objBilling->amount - $objBilling->amountDiscount - $data['paidOut'] ;
            
            $all_data = array_merge($new_data,$data);
            $this->fnResponseFormat($arrayDetails, self::REPORT_RESPONSE_TYPE_XML, $all_data);
             }
             
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        
    }
    
    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }
    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }
    
}