<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Driverlog extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('driverlog', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('driverlog', self::FILE_TYPE_JS));
        $view_driverlog = new View('billings/driverlog');
        $this->template->content = $view_driverlog;
    }

    public function action_listDriversBilling() {

        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $idDriver = $this->request->query('idDriver');

        if ($idDriver == 0)
            $s_driver = "";
        else
            $s_driver = "AND dr.idDriver=" . $idDriver;

        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                "billing_id" => "bd.idBillingDriver"
                , "driver_id" => "dr.idDriver"
                , "driverfullname" => "CONCAT(dr.name,' ' ,dr.lastName)"
                , "amount" => "bd.amount"
                , "datebilling" => "DATE_FORMAT(bd.datebilling, '%m/%d/%Y')"
                , "registerdate" => "bd.creationDate"
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " billingdriver bd  INNER JOIN drivers dr ON bd.idDriver = dr.idDriver ";

            $s_where_conditions = 'bd.status = 1';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_reportBillingDriver() {
        $this->auto_render = FALSE;
        try {
            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $idBillingDriver = $this->request->query('idBillingDriver');
            $a_details = array();

            $o_billing_driver = new Model_BillingDriver($idBillingDriver);

            $o_driver = new Model_Drivers($o_billing_driver->idDriver);

            $data['dri_fullname'] = $o_driver->name + $o_driver->lastName;
            $data['dri_address'] = $o_driver->address;
            $data['dri_phone'] = $o_driver->phone;
            $data['idBilling'] = $o_billing_driver->idBillingDriver;
            $data['totalAmount'] = $o_billing_driver->amount;
            $data['date'] = $this->cambiafmysql($o_billing_driver->datebilling);

            $columns = array(
                array('app.appointmentdate', 'appdate'),
                array('app.appointmenttime', 'apptime'),
                array('bdd.detailType', 'detailType'),
                array('bdd.title', 'title'),
                array('bdd.qty', 'quanty'),
                array('bdd.ratio', 'ratio'),
                array('bdd.amount', 'amount'),
                DB::expr("IF(bdac.status=1, bdac.comment, '') as coment")
            );

            $sql_string = DB::select_array($columns)
                            ->from(array("billingdriver", 'bd'))
                            ->join(array('billingdriverappointment', 'bda'))->on('bd.idBillingDriver', '=', 'bda.idBillingDriver')
                            ->join(array('appointment', 'app'))->on('app.idAppointment', '=', 'bda.idAppointment')
                            ->join(array('billingdriverdetail', 'bdd'))->on('bda.idBillingDriverAppointment', '=', 'bdd.idBillingDriverAppointment')
                            ->join(array('billingdriverappointmentcomment', 'bdac'), "LEFT")->on('bdac.idBillingDriverAppointment', '=', 'bda.idBillingDriverAppointment')
                            ->where('bd.idBillingDriver', '=', $idBillingDriver)
                            ->and_where('bdd.status', '=', '1')
                            ->execute()->as_array();

            foreach ($sql_string as $abillingdetail) {

                $explodeTitle = explode('|', $abillingdetail['title']);
                switch ($abillingdetail['detailType']) {
                    case 'driverNormal':
                        $title = $explodeTitle[0] . ' ' . $explodeTitle[1];
                        $title .= '{br}From: ' . $explodeTitle[2] . ', ' . $explodeTitle[3];
                        $title .= '{br}To: ' . $explodeTitle[4] . ', ' . $explodeTitle[5];
                        $title .= '{br}{br}Notes: ' . $abillingdetail['coment'];
                        break;

                    case 'driverNoShow':
                        $o_app = $abillingdetail->BillingDriverAppointments->Appointments;
                        $o_course = new Model_Course($o_app->idCourse);
                        $o_location_from = new Model_Locations($o_course->idLocationOrigen);
                        $o_location_to = new Model_Locations($o_course->idLocationDestination);

                        $title = $explodeTitle[0] . ' ' . $explodeTitle[1] . ' ' . $explodeTitle[2];
                        $title .= '{br}From: ' . $o_location_from->alias . ', ' . $o_location_from->address;
                        $title .= '{br}To: ' . $o_location_to->alias . ', ' . $o_location_to->address;
                        $title .= '{br}{br}Notes: ' . $abillingdetail['coment'];
                        break;
                    default:
                        $title = $explodeTitle[1];
                        break;
                }

                array_push($a_details, array(
                    'appDate' => $this->cambiafmysql($abillingdetail['appdate']),
                    'appTime' => $abillingdetail['apptime'],
                    'title' => $title,
                    'qty' => ($abillingdetail['quanty']) * 1,
                    'ratio' => $abillingdetail['ratio'],
                    'amount' => $abillingdetail['amount'],
                    'detailType' => $abillingdetail['detailType']
                        )
                );
            }


            array_push($a_details, array(
                'appDate' => $this->cambiafmysql($abillingdetail['appdate']),
                'appTime' => $abillingdetail['apptime'],
                'title' => $title,
                'qty' => ($abillingdetail['quanty']) * 1,
                'ratio' => $abillingdetail['ratio'],
                'amount' => $abillingdetail['amount'],
                'detailType' => $abillingdetail['detailType']
                    )
            );

            $this->fnResponseFormat($a_details, self::REPORT_RESPONSE_TYPE_XML, $data);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}
