<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Apdrivers extends Controller_Private_Admin {

    const S_ITEM_TYPE_HEADER = 'item_header';
    const S_ITEM_TYPE_ROW = 'item_row';
    const S_ITEM_TYPE_NOTE = 'item_comment';
    const S_ITEM_TYPE_SUMMARY = 'item_summary';
    
    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('apdrivers', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('apdrivers', self::FILE_TYPE_JS));
        
        
        $drivers = DB::select(array('idDriver', 'idDriver'), array('name', 'name'), array('lastname', 'lastname'))
                        ->from("drivers")->where('status', '=', '1')
                        ->execute()->as_array();

        $view_apdrivers = new View('billings/apdrivers');

        $view_apdrivers->drivers = $drivers;

        $this->template->content = $view_apdrivers;
    }
    
    public function action_listDriversBilling(){
        
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');
        
        $idDriver = $this->request->query('idDriver');
        
        if($idDriver==0) $s_driver="";
        else $s_driver="AND dr.idDriver=".$idDriver;
            
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                "billing_id" => "CONCAT(app.idAppointment, '_',CASE WHEN app.idDriverOneWay = dr.idDriver THEN app.idDriverOneWay ELSE app.idDriverRoundTrip END)"
                , "driver_id" => "dr.idDriver"
                , "app_id" => "app.idAppointment"
                , "driverfullname" => "CONCAT(dr.name,' ' ,dr.lastName)"
                , "app_date" => "DATE_FORMAT(app.appointmentdate, '%m/%d/%Y')"
                , "app_time" => "app.appointmenttime"
                , "pat_fullname" => "CONCAT(pat.firstname, ' ', pat.lastname)"
                , "from_address" => "CONCAT(fromad.alias, ': ', fromad.address, IF( CHAR_LENGTH(TRIM(COALESCE(fromad.city, ''))) = 0, '', CONCAT(', ', fromad.city, IF(CHAR_LENGTH(TRIM(COALESCE(fromad.state, ''))) = 0, ' ', CONCAT(', ', fromad.state) ))))"
                , "to_address" => "CONCAT(toad.alias, ': ', toad.address, IF( CHAR_LENGTH(TRIM(COALESCE(toad.city, ''))) = 0, '', CONCAT(', ', toad.city, IF(CHAR_LENGTH(TRIM(COALESCE(toad.state, ''))) = 0, ' ', CONCAT(', ', toad.state) ))))"
                , "type_billing_driver" => "CASE 
                            WHEN app.typetravel = 'RoundTravel' 
                                THEN 
                                    CASE 
                                        WHEN app.idDriverOneWay = dr.idDriver AND app.idDriverRoundTrip = dr.idDriver
                                            THEN '(R) Round Trip' 
                                        ELSE 
                                            CASE 
                                                WHEN app.idDriverOneWay = dr.idDriver
                                                    THEN '(R) Trip' 
                                                ELSE
                                                    '(R) Journey Trip' 
                                            END
                                     END
                            ELSE
                                '(O) Trip'
                        END"
                
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = " appointment app
                INNER JOIN drivers dr
                    ON app.idDriverOneWay = dr.idDriver OR app.idDriverRoundTrip = dr.idDriver
                INNER JOIN patients pat
                        ON app.idPatient = pat.idPatient
                INNER JOIN course c
                        ON app.idCourse = c.idCourse
                INNER JOIN locations fromad
                        ON fromad.idLocation = c.idLocationOrigen
                INNER JOIN locations toad
                        ON toad.idLocation = c.idLocationDestination
                LEFT JOIN billingdriverappointment bda
                   ON app.idAppointment = bda.idAppointment AND bda.status = 1
                   AND
                   #EXISTE REGISTRO EN BILLING APP
                   CASE 
                        WHEN app.typetravel = 'RoundTravel'
                            THEN 
                                CASE 
                                    WHEN app.idDriverOneWay = dr.idDriver AND app.idDriverRoundTrip = dr.idDriver 
                                        THEN bda.journeyNum = 3
                                    ELSE 
                                        CASE 
                                            WHEN app.idDriverOneWay = dr.idDriver
                                                THEN bda.journeyNum = 1 
                                            ELSE
                                                bda.journeyNum = 2
                                        END
                                 END
                        ELSE
                            bda.journeyNum = 1
                    END
                LEFT JOIN billingdriver bd
                   ON bda.idBillingDriver = bd.idBillingDriver AND bd.status = 1";
  
            $s_where_conditions = '(app.cancelDriver = 0 OR ISNULL(app.cancelDriver))
                   AND app.appconfirmedconfirmationdriver1=1 AND app.appconfirmedconfirmationdriver2=1 AND
  
                   ISNULL(bda.idAppointment)' . $s_driver;


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }
    
     public function action_getnewCodeInvoice() {

        try {

            $a_response = $this->json_array_return;

            $a_response['newbilling'] = $this->getNewCodeInvoice();
        } catch (Exception $e_exc) {
            //echo $e->getMessage();
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    public function getNewCodeInvoice() {
        $result = DB::query(DATABASE::SELECT, " SHOW TABLE STATUS LIKE 'billingdriver' ")->execute()->current();
        return $result['Auto_increment'];
    }
    public function action_getAppointmentsToBilling(){
        
        $idDriver = $this->request->post('driver_id');
        $idApps = explode(",", $this->request->post('a_check_ids'));
        
        
//        try {
         
             foreach ($idApps as $key => $value) {
//               $o_app=new Model_Appointment($value);        
//               $a_response['data-app-'.$value]=$o_app->as_array();  
                 $result = DB::select(
                                array('dri1.idDriver', 'idDriver')
                                , DB::expr('DATE_FORMAT(CURDATE(),"%m/%d/%Y") as d_current_date')
                                , array('app.idAppointment', 'app_id')
                                , DB::expr('COALESCE(p.regionDefault,"No Selected in Patient") as pat_region')
                                , DB::expr('(CASE WHEN app.typetravel="RoundTravel" THEN "Round Trip" ELSE "One Way" END) as app_type_travel')
                                , array('app.lbase', 'app_lbase')
                                , array('app.cassistance', 'app_cassistance')
                                , DB::expr('COALESCE(app.typeservice,"") as app_type_service')
                                , array('app.frate', 'app_frate')
//                                , DB::expr('CASE
//                                    WHEN app.typetravel = "OneWay"
//                                        THEN
//                                            CASE
//                                                WHEN app.typetravel = 1
//                                                    THEN COALESCE(app.miles, 0)
//                                                ELSE
//                                                    COALESCE(CAST((pt.milesBilling*2) AS UNSIGNED), 0)
//                                            END
//                                    ELSE
//                                        CASE
//                                            WHEN app.typetravel = 1
//                                                THEN COALESCE(CAST((pt.milesBilling/2) AS UNSIGNED), 0)
//                                            ELSE
//                                                COALESCE(pt.milesBilling, 0)
//                                        END
//                                   END as app_miles_billing')
                                , array('app.idAlias', 'app_alias')
                                , DB::expr('CONCAT(p.firstname," ",p.lastname) as pat_full_name')
                                , array('app.appauthorized', 'app_auth')
                                , DB::expr('DATE_FORMAT(app.appointmentdate,"%m/%d/%Y") as app_date')
                                , array('app.appointmenttime', 'app_time')
                                , array('app.whocalls', 'app_who_calls')
                                , array('app.wtime', 'app_wtime')
                                , DB::expr('COALESCE(app.wtimehours, 0) as app_wtime_hours')
                                , DB::expr('COALESCE(app.wtimeminutes, 0) as app_wtime_minutes')
                                , array('app.companyname', 'app_company_name')
                                , array('app.typeapp', 'app_type')
                                , array('tapp.name', 'tapp_name')
                                , DB::expr('COALESCE(dri1.name,"-") as dr_pick_up_name')
                                , DB::expr('COALESCE(dri1.lastName,"-") as dr_pick_up_last_name')
                                , array('app.idDriverOneWay', 'app_driver_drop_id')
                                , DB::expr('COALESCE(dri2.name,"-") as dr_drop_name')
                                , DB::expr('COALESCE(dri2.lastName,"-") as dr_drop_last_name')
                                , array('locfrom.alias', 'pl_alias')
                                , array('locfrom.address', 'pl_address')
                                , array('locto.alias', 'tl_contact')
                                , array('locto.address', 'tl_address')
                                , array('app.miles', 'app_miles')
                                , array('c.idInsuranceCarrier', 'app_insurance_carriers_id')
                                , array('app.idPatient', 'pat_id')
                                , array('app.idClaim', 'cl_id')
                                , DB::expr('COALESCE(app.showdriver,0) as app_no_show_driver')
                                , DB::expr('CASE 
                                            WHEN app.typetravel = "RoundTravel"
                                                THEN 
                                                    CASE 
                                                        WHEN app.idDriverRoundTrip = app.idDriverOneWay
                                                            THEN "3|(R) Round Trip" 
                                                        ELSE 
                                                            CASE 
                                                                WHEN app.idDriverOneWay ='.$idDriver.
                                                                    ' THEN "1|(R) Trip" 
                                                                ELSE
                                                                    "2|(R) Journey Trip" 
                                                            END
                                                     END
                                            ELSE
                                                "1|(O) Trip"
                                        END as app_travel_billing')
                        )->distinct(true)
                        ->from(array("appointment", 'app'))
                        ->join(array('claims', 'c'))->on('c.idClaim', '=', 'app.idClaim')
                        ->join(array('insurancecarrier', 'inc'))->on('inc.idInsuranceCarrier', '=', 'c.idInsuranceCarrier')
                        ->join(array('adjusters', 'adj'))->on('adj.idAdjuster', '=', 'c.idAdjuster')
                        ->join(array('patients', 'p'))->on('p.idPatient', '=', 'app.idPatient')
                        ->join(array('typeappointment', 'tapp'))->on('app.idTypeAppointment', '=', 'tapp.idTypeappointment')
                        ->join(array('languages', 'lan'),'LEFT')->on('lan.idLanguage', '=', 'app.idLanguage')
                        ->join(array('drivers', 'dri1'))->on('app.idDriverOneWay', '=', 'dri1.idDriver')
                        ->join(array('drivers', 'dri2'),'LEFT')->on('app.idDriverRoundTrip', '=', 'dri2.idDriver')
                        ->join(array('interpreters', 'inter'),'LEFT')->on('app.idInterpreter', '=', 'inter.idInterpreter')
                        ->join(array('course', 'cou'))->on('app.idCourse', '=', 'cou.idCourse')
                        ->join(array('locations', 'locfrom'))->on('cou.idLocationOrigen', '=', 'locfrom.idLocation')
                        ->join(array('locations', 'locto'))->on('cou.idLocationDestination', '=', 'locto.idLocation')
                        ->join(array('billingdriverappointment', 'bda'),'LEFT')->on('app.idAppointment', '=', 'bda.idAppointment')
                        ->join(array('billingdriver', 'bd'),'LEFT')->on('bda.idBillingDriver', '=', 'bd.idBillingdriver')
//                        ->on('TRUE', '=',DB::expr("CASE 
//                                            WHEN app.typetravel = 'RoundTrip'
//                                               THEN 
//                                                   CASE 
//                                                        WHEN app.idDriverOneWay = app.idDriverRoundTrip AND app.idDriverOneWay = :driver
//                                                             THEN bda.typeTravel = 'RoundTrip' AND bda.journeyNum = 3
//                                                        ELSE 
//                                                            CASE 
//                                                                WHEN app.idDriverOneWay =:driver
//                                                                     THEN bda.typeTravel = 'RoundTrip' AND bda.journeyNum = 1
//                                                                ELSE
//                                                                    bda.typeTravel = 'RoundTrip' AND bda.journeyNum = 2
//                                                            END
//                                                    END
//                                            ELSE
//                                                bda.typeTravel = 'OneWay' AND bda.journeyNum = 1
//                                        END",array(":driver"=>$idDriver)))
//                        ->and_where('app.idDriver','=',$idDriver)
//                        ->and_where(DB::expr('CASE 
//                                            WHEN app.typetravel = "RoundTrip" 
//                                                THEN 
//                                                    CASE 
//                                                        WHEN app.idDriverOneWay = app.idDriverRoundTrip AND app.idDriverOneWay ='.$idDriver.
//                                                            ' THEN bda.typeTravel = "RoundTrip" AND bda.journeyNum = 3
//                                                        ELSE 
//                                                            CASE 
//                                                                WHEN app.idDriverOneWay ='.$idDriver.
//                                                                    ' THEN bda.typeTravel = "RoundTrip" AND bda.journeyNum = 1
//                                                                ELSE
//                                                                    bda.typeTravel = "RoundTrip" AND bda.journeyNum = 2
//                                                            END
//                                                    END
//                                            ELSE
//                                                bda.typeTravel = "OneWay" AND bda.journeyNum = 1
//                                        END',''))
                        ->and_where('app.idAppointment', '=', $value)
                        ->and_where('p.status','=', '1')
                        ->and_where('app.status','=', '1')
                        ->order_by('app.appointmentdate','ASC') 
                        ->order_by(DB::expr("STR_TO_DATE(UPPER (app.appointmenttime), '%h:%i %p')"), 'ASC')
                        ->limit(20)
                        ->execute()->current();
                  
                 $a_response['data_'.$value]=$result;
                      
             }
                
             $this->fnResponseFormat($a_response);
    }
    
    public function action_fnSaveBillingDriver() {
        try {
            //ini_set('display_errors', 1);
            setlocale(LC_MONETARY, 'en_US');
            $a_apps_id = explode(",", $this->request->post('a_apps_id'));
            $f_suma_total = 0;

            $i_driver_id = $this->request->post('driver_id');
            $o_driver = new Model_Drivers($i_driver_id);
            $o_billing_driver = new Model_BillingDriver();
            $iCountEdit = 0;
//            if ($_POST['save'] == 'true' && $_POST['idBilling'] > 0) {
//                $o_billing_driver = new Model_BillingDriver($this->request->post('billing_driver_id'));
//                $iCountEdit = ($o_billing_driver->countEdit) + 1;
//            } else {
//                // $o_billing_driver->invoiceNumber = $o_billing_driver->fnGetNewCode();
//            }
            $o_billing_driver->countEdit = $iCountEdit;
            $o_billing_driver->datebilling = $this->cambiafmysqlre($this->request->post('billing_driver_date'));
            $o_billing_driver->idDriver = $o_driver->idDriver;
            $o_billing_driver->creationDate = date('Y-m-d H:i:s');
            $o_billing_driver->datepayment = date('Y-m-d');
            $o_billing_driver->paymentdate = date('Y-m-d');
            $o_billing_driver->comment = '';
            $o_billing_driver->lostpayment = 0;
            $o_billing_driver->aliasbilling = '';
            $o_billing_driver->userpayment = $this->getSessionParameter(self::USER_ID);
            $o_billing_driver->status = 1;
            $o_billing_driver->amount = 0;
            $o_billing_driver->userCreate = $this->getSessionParameter(self::USER_ID);
         
//----el campo typebilling no hay en BD
//            if ($_REQUEST['type'] == 1) {
//                $o_billing_driver->typebilling = 1;
//            } elseif ($_REQUEST['type'] == 2) {
//                $o_billing_driver->typebilling = 2;
//            }
//-------

            if ($_POST['save'] == 'true' && $o_billing_driver->idBillingDriver > 0) {
                $o_billing_driver->save();
               
                //Deshabilitar Todos los COmentarios, Apps y tmb Detalles u_U

//                foreach ($o_billing_driver->BillingDriverAppointments as $o_billing_driver_app) {
//                    if ($o_billing_driver_app->status == 1) {
//                        $o_billing_driver_app->status = 0;
//
//                        $tmpApp = $o_billing_driver_app->Appointments;
//
//                        $o_billing_driver_app->update();
//
//                        $o_billing_driver_details = new Model_BillingDriverDetail();
//                        $a_billing_driver_details = $o_billing_driver_details
//                                ->getAll()
//                                ->WhereAnd('idBillingDriverAppointment=', $o_billing_driver_app->idBillingAppointment)
//                                ->WhereAnd('status=', 1)
//                                ->getArray();
//
//                        foreach ($a_billing_driver_details as $o_billing_driver_detail) {
//                            if ($o_billing_driver_detail->idBillingDriverDetail == 1) {
//                                $o_billing_driver_detail->status = 0;
//                                $o_billing_driver_detail->update();
//                            }
//                        }
//
//                        $o_billing_driver_appointment_comment = new Model_BillingDriverAppointmentComment();
//                        $comment = $o_billing_driver_appointment_comment
//                                ->getAll()
//                                ->WhereAnd('idBillingDriverAppointment=', $o_billing_driver_app->idBillingDriverAppointment)
//                                ->WhereAnd('status=', 1)
//                                ->get(0);
//                        if ($comment->idBillingDriverAppointmentComment > 0) {
//                            $comment->status = 0;
//                            $comment->save();
//                        }
//                    }
//                }
            } elseif ($_POST['save'] == 'true') {
                
                $o_billing_driver->save();
                $i_billing_id = $o_billing_driver->idBillingDriver;
                
            }
             $i_billing_id = $o_billing_driver->idBillingDriver;
           
            $a_sum_pre = array();

            //Details .... o.O
            foreach ($a_apps_id as $i_app_id) {
                $f_sum_pre_app = 0;
                $i_row_num = 0;
                
                $o_appointment = new Model_Appointment($i_app_id);
                $o_course = new Model_Course($o_appointment->idCourse);
                $o_locationOrigen = new Model_Locations($o_course->idLocationOrigen);
                $o_locationDestination = new Model_Locations($o_course->idLocationDestination);

                $a_billing_info = $this->fnGetBillingInfo($i_app_id, $i_driver_id);

                $app_travel_billing = $a_billing_info['app_travel_billing'];

                $a_travel_billing = explode('|', $app_travel_billing);

                $i_type_travel = $a_travel_billing[0];
                $s_type_travel = $a_travel_billing[1];

//                $pickuplocationTreatmentoffice = $o_appointment->PickuplocationTreatmentoffice;
//verificar esta parte 
//
//                $s_apartment = ($o_appointment->Patients->apartmentNumber != '0' AND $o_appointment->Patients->apartmentNumber != 'n/a') ? ' ' . $o_appointment->Patients->apartmentNumber . ' ' : '';

                //$o_billing_driver->idInsuranceCarriers = $o_appointment->Claims->idInsuranceCarriers;
//                if ($_POST['save'] == 'true') {
//                      $o_billing_driver->update();
//                }
                $o_billing_driver_appointment = new Model_BillingDriverAppointment();
                $o_billing_driver_appointment->idBillingDriver = $o_billing_driver->idBillingDriver;
                $o_billing_driver_appointment->idAppointment = $o_appointment->idAppointment;
                $o_billing_driver_appointment->typeTravel = $o_appointment->typetravel == 'RoundTravel' ? '2' : '1';
                $o_billing_driver_appointment->journeyNum = $i_type_travel;
                $o_billing_driver_appointment->status = 1;
                $o_billing_driver_appointment->countEdit = $iCountEdit;

                if ($this->request->post('save') == 'true') {
                    $o_billing_driver_appointment->save();
                }

                //Transportation
                $federalId = '26-0347129';

                $o_billing_appointment_comments = new Model_BillingDriverAppointmentComment();
                $o_billing_appointment_comments->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                $o_billing_appointment_comments->comment = $this->request->post('commentServDri' . $i_app_id);
                $o_billing_appointment_comments->status = 1;
                $o_billing_appointment_comments->countEdit = $iCountEdit;

                if ($this->request->post('save') AND $_REQUEST['commentServDri' . $i_app_id] != '') {
                    $o_billing_appointment_comments->save();
                }

                //$pickuplocationTreatmentoffice = new PickuplocationTreatmentoffice();
                //$pickuplocationTreatmentoffice = $pickuplocationTreatmentoffice->getAll()->WhereAnd("idAppointment=", $idApp)->get(0);
                $gasolinesurcharge = $o_driver->gasolineSurcharge;

                //$objBilling->comments = $_REQUEST['comment'];


                switch ($o_appointment->typeservice) {
                    case 'Ambulatory' :
                        $ratio = $o_driver->ambulatoryRate;

                        if ($i_type_travel == 1 OR $i_type_travel == 2) {
                            $minFlate = $o_driver->ambulatoryMiniFlatRateOW;
                        } elseif ($i_type_travel == 3) {
                            $minFlate = $o_driver->ambulatoryMiniFlatRateRT;
                        }

                        $noShow = $o_driver->ambulatoryNoShow;
                        $early = $o_driver->ambulatoryEarlyPickUp;
                        $late = $o_driver->ambulatoryLateDropOff;
                        $weekend = $o_driver->ambulatoryWeekendFee;
                        $wait = $o_driver->ambulatoryWaitingTime;
                        $serviceType = 'Ambulatory';
                        break;
                    case 'Wheelchair' :
                        $ratio = $o_driver->wheelchairRate;
                        if ($i_type_travel == 1 OR $i_type_travel == 2) {
                            $minFlate = $o_driver->wheelchairMiniFlatRateOW;
                        } elseif ($i_type_travel == 3) {
                            $minFlate = $o_driver->wheelchairMiniFlatRateRT;
                        }
                        $noShow = $o_driver->wheelchairNoShow;
                        $early = $o_driver->wheelchairEarlyPickUp;
                        $late = $o_driver->wheelchairLateDropOff;
                        $weekend = $o_driver->wheelchairWeekendFee;
                        $wait = $o_driver->wheelchairWaitingTime;
                        $lift = $o_driver->wheelchairLiftBase;
                        $assistance = $o_driver->wheelchairCarAssistance;
                        $serviceType = 'Wheelchair';
                        break;
                    case 'Stretcher' :
                        $ratio = $o_driver->stretcherRate;
                        if ($i_type_travel == 1 OR $i_type_travel == 2) {
                            $minFlate = $o_driver->stretcherMiniFlatRateOW;
                        } elseif ($i_type_travel == 3) {
                            $minFlate = $o_driver->stretcherMiniFlatRateRT;
                        }
                        $noShow = $o_driver->stretcherNoShow;
                        $early = $o_driver->stretcherEarlyPickUp;
                        $late = $o_driver->stretcherLateDropOff;
                        $weekend = $o_driver->stretcherWeekendFee;
                        $wait = $o_driver->stretcherWaitingTime;
                        $lift = $o_driver->stretcherLiftBase;
                        $assistance = $o_driver->stretcherAssistance;
                        $serviceType = 'Stretcher';
                        break;
                    default :
                        //Nada
                        break;
                }

                if ($o_appointment->showdriver == 0  AND $o_appointment->canceldriver == 0) {
                    //Normal
                    $quantity = $o_appointment->miles;

                    if ($this->request->post('haveFlatRate' . $i_app_id) == 'on') {
                        $quantity = 1;
                        $ratio = $this->request->post('tmiles' . $i_app_id);
                    } else {
                  //      $objPickUpDrop = $o_appointment->PickuplocationTreatmentoffice;
                        //Save miles Billing usa el mismo o.O!
//                        if ($o_appointment->typetravel != $objPickUpDrop->typetravel) {
//                            //Enfoque en el App
//                            if ($o_appointment->typetravel == 1) {
//                                $objPickUpDrop->milesBilling = $_REQUEST['tmiles' . $i_app_id] * 2;
//                            } else {
//                                $objPickUpDrop->milesBilling = $_REQUEST['tmiles' . $i_app_id] / 2;
//                            }
//                        } else {
//                            $objPickUpDrop->milesBilling = $_REQUEST['tmiles' . $i_app_id];
//                        }

//                        if ($_POST['save'] == 'true') {
//                            $objPickUpDrop->update();
//                        }

                        $quantity = $this->request->post('tmiles' . $i_app_id);
                    }
                    $msgFlate = '';
                    $amount = $quantity * $ratio;
                    if ($amount < $minFlate) {
                        $ratio = $minFlate / $quantity;
                        $amount = $minFlate;
                        $msgFlate = ', Min Amount (' . $minFlate . ')';
                    }

                    $title = $serviceType . ' <b>' . $s_type_travel . '</b>' . $msgFlate . '<br/><b>' . $o_locationOrigen->alias . '</b>: ' . $o_locationOrigen->address . '<br/>'
                            . '<b>' . $o_locationDestination->alias . '</b>: ' . $o_locationDestination->address;

                    $titleforbd = $serviceType . '|' . $s_type_travel . '|' . $o_locationOrigen->alias . '|' . $o_locationOrigen->address
                            . '|' . $o_locationDestination->alias . '|' . $o_locationDestination->address;


                    //Add Insert en el detalle
                    if ($this->request->post('save') == 'true') {
                        $o_billing_driver_detail = new Model_BillingDriverDetail();
                        $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                        $o_billing_driver_detail->title = $titleforbd;
                        $o_billing_driver_detail->qty = $quantity;
                        $o_billing_driver_detail->ratio = $ratio;
                        $o_billing_driver_detail->amount = $amount;
                        $o_billing_driver_detail->detailType = 'driverNormal';
                        $o_billing_driver_detail->status = 1;
                        $o_billing_driver_detail->countEdit = $iCountEdit;
                        $o_billing_driver_detail->save();
                    }

                    $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_HEADER, $title, $quantity, $ratio, $amount, $this->cambiafmysql($o_appointment->appointmentdate) . '<br/>'
                            . $o_appointment->appointmenttime);
                    $i_row_num++;

                    $f_suma_total+=$amount;

                    $f_sum_pre_app += $amount;


                    if ($o_appointment->lbase == '1') {
                        $title = 'Lift Base';
                        $titleforbd = $serviceType . '|' . $title;
                        $quantity = $this->request->post('lbase' . $i_app_id);
                        $ratio = $lift;
                        $amount = $quantity * $ratio;
                        //Add Insert en el detalle
                        if ($this->request->post('save') == 'true') {
                            $o_billing_driver_detail = new Model_BillingDriverDetails();
                            $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                            $o_billing_driver_detail->title = $titleforbd;
                            $o_billing_driver_detail->qty = $quantity;
                            $o_billing_driver_detail->ratio = $ratio;
                            $o_billing_driver_detail->amount = $amount;
                            $o_billing_driver_detail->detailType = 'driverLift';
                            $o_billing_driver_detail->status = 1;
                            $o_billing_driver_detail->countEdit = $iCountEdit;
                            $o_billing_driver_detail->save();
                        }
                        $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                        $f_suma_total+=$amount;
                        $f_sum_pre_app += $amount;
                        $i_row_num++;
                    }

                    if ($o_appointment->cassistance == '1') {
                        if ($o_appointment->typeservice == 'WHEELCHAIR') {
                            $title = 'Car Assistance';
                        } elseif ($o_appointment->typeservice == 'STRETCHER') {
                            $title = 'Assistance';
                        }
                        $titleforbd = $serviceType . '|' . $title;
                        $quantity = 1;
                        $ratio = $assistance;
                        $amount = $quantity * $ratio;
                        //Add Insert en el detalle
                        if ($this->request->post('save') == 'true') {
                            $o_billing_driver_detail = new Model_BillingDriverDetail();
                            $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                            $o_billing_driver_detail->title = $titleforbd;
                            $o_billing_driver_detail->qty = $quantity;
                            $o_billing_driver_detail->ratio = $ratio;
                            $o_billing_driver_detail->amount = $amount;
                            $o_billing_driver_detail->detailType = 'driverAssistance';
                            $o_billing_driver_detail->status = 1;
                            $o_billing_driver_detail->countEdit = $iCountEdit;
                            $o_billing_driver_detail->save();
                        }

                        $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                        $f_suma_total+=$amount;
                        $f_sum_pre_app += $amount;
                        $i_row_num++;
                    }
                    $extraaa=$this->request->post('extra' . $i_app_id);
                    if(isset($extraaa))
                    foreach ($this->request->post('extra' . $i_app_id) as $s_extra_payment) {
                        switch ($s_extra_payment) {
                            case 'fs' :
                                $title = 'Fuel Surchage';
                                $titleforbd = $serviceType . '|' . $title;
                                $quantity = 1;
                                $ratio = $gasolinesurcharge;
                                $amount = $quantity * $ratio;
                                //Add Insert en el detalle
                                if ($this->request->post('save') == 'true') {
                                    $o_billing_driver_detail = new Model_BillingDriverDetail();
                                    $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                                    $o_billing_driver_detail->title = $titleforbd;
                                    $o_billing_driver_detail->qty = $quantity;
                                    $o_billing_driver_detail->ratio = $ratio;
                                    $o_billing_driver_detail->amount = $amount;
                                    $o_billing_driver_detail->detailType = 'driverSurcharge';
                                    $o_billing_driver_detail->status = 1;
                                    $o_billing_driver_detail->countEdit = $iCountEdit;
                                    $o_billing_driver_detail->save();
                                }
                                $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                                $f_suma_total+=$amount;
                                $f_sum_pre_app += $amount;
                                $i_row_num++;
                                break;
                            case 'epu' :
                                $title = 'Early Pick up';
                                $titleforbd = $serviceType . '|' . $title;
                                $quantity = 1;
                                $ratio = $early;
                                $amount = $quantity * $ratio;
                                //Add Insert en el detalle
                                if ($this->request->post('save') == 'true') {
                                    $o_billing_driver_detail = new Model_BillingDriverDetail();
                                    $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                                    $o_billing_driver_detail->title = $titleforbd;
                                    $o_billing_driver_detail->qty = $quantity;
                                    $o_billing_driver_detail->ratio = $ratio;
                                    $o_billing_driver_detail->amount = $amount;
                                    $o_billing_driver_detail->detailType = 'driverEarly';
                                    $o_billing_driver_detail->status = 1;
                                    $o_billing_driver_detail->countEdit = $iCountEdit;
                                    $o_billing_driver_detail->save();
                                }
                                $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                                $f_suma_total+=$amount;
                                $f_sum_pre_app += $amount;
                                $i_row_num++;
                                break;
                            case 'ldo' :
                                $title = 'Late Drop off';
                                $titleforbd = $serviceType . '|' . $title;
                                $quantity = 1;
                                $ratio = $late;
                                $amount = $quantity * $ratio;
                                //Add Insert en el detalle
                                if ($this->request->post('save') == 'true') {
                                    $o_billing_driver_detail = new Model_BillingDriverDetail();
                                    $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                                    $o_billing_driver_detail->title = $titleforbd;
                                    $o_billing_driver_detail->qty = $quantity;
                                    $o_billing_driver_detail->ratio = $ratio;
                                    $o_billing_driver_detail->amount = $amount;
                                    $o_billing_driver_detail->detailType = 'driverLate';
                                    $o_billing_driver_detail->status = 1;
                                    $o_billing_driver_detail->countEdit = $iCountEdit;
                                    $o_billing_driver_detail->save();
                                }
                                $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                                $f_suma_total+=$amount;
                                $f_sum_pre_app += $amount;
                                $i_row_num++;
                                break;
                            case 'wend' :
                                $title = 'Weekend fee';
                                $titleforbd = $serviceType . '|' . $title;
                                $quantity = 1;
                                $ratio = $weekend;
                                $amount = $quantity * $ratio;
                                //Add Insert en el detalle
                                if ($this->request->post('save') == 'true') {
                                    $o_billing_driver_detail = new Model_BillingDriverDetail();
                                    $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                                    $o_billing_driver_detail->title = $titleforbd;
                                    $o_billing_driver_detail->qty = $quantity;
                                    $o_billing_driver_detail->ratio = $ratio;
                                    $o_billing_driver_detail->amount = $amount;
                                    $o_billing_driver_detail->detailType = 'driverWeekend';
                                    $o_billing_driver_detail->status = 1;
                                    $o_billing_driver_detail->countEdit = $iCountEdit;
                                    $o_billing_driver_detail->save();
                                }
                                $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                                $f_suma_total+=$amount;
                                $f_sum_pre_app += $amount;
                                $i_row_num++;
                                break;
                            default :
                                break;
                        }
                    }
                    if ($this->request->post('wtime' . $i_app_id) == 'on') {
                        $title = 'Waiting Time';
                        $title2 = $serviceType . '|' . $title;
                        $quantity = $this->request->post('wthour' . $i_app_id) + round($this->request->post('wtmin' . $i_app_id) / 60, 2);
                        $ratio = $wait;
                        $amount = $quantity * $ratio;
                        //Add Insert en el detalle
                        if ($this->request->post('save') == 'true') {
                            $o_billing_driver_detail = new Model_BillingDriverDetail();
                            $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                            $o_billing_driver_detail->title = $title2;
                            $o_billing_driver_detail->qty = $quantity;
                            $o_billing_driver_detail->ratio = $ratio;
                            $o_billing_driver_detail->amount = $amount;
                            $o_billing_driver_detail->detailType = 'driverWait';
                            $o_billing_driver_detail->status = 1;
                            $o_billing_driver_detail->countEdit = $iCountEdit;
                            $o_billing_driver_detail->save();
                        }
                        $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                        $f_suma_total+=$amount;
                        $f_sum_pre_app += $amount;
                        $i_row_num++;
                    }
                    if ($this->request->post('other' . $i_app_id) == 'on') {
                        $title = 'Other';
                        $titleforbd = $serviceType . '|' . $title;
                        $quantity = 1;
                        $ratio = $this->request->post('otherValue' . $i_app_id);
                        $amount = $quantity * $ratio;
                        //Add Insert en el detalle
                        if ($this->request->post('save') == 'true') {
                            $o_billing_driver_detail = new Model_BillingDriverDetail();
                            $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                            $o_billing_driver_detail->title = $titleforbd;
                            $o_billing_driver_detail->qty = $quantity;
                            $o_billing_driver_detail->ratio = $ratio;
                            $o_billing_driver_detail->amount = $amount;
                            $o_billing_driver_detail->detailType = 'driverOther';
                            $o_billing_driver_detail->status = 1;
                            $o_billing_driver_detail->countEdit = $iCountEdit;
                            $o_billing_driver_detail->save();
                        }
                        $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                        $f_suma_total+=$amount;
                        $f_sum_pre_app += $amount;
                        $i_row_num++;
                    }
                } elseif ($o_appointment->showdriver == 1) {
                    //No Show Driver
                    $i_row_num = 0;
                    $title = $serviceType . ' <b>' . $s_type_travel . '</b>' . ' <b>No Show Driver</b>';
                    $titleforbd = $serviceType . '|' . $s_type_travel . '|' . 'No Show Driver';
                    $quantity = 1;
                    $ratio = $noShow;
                    $amount = $quantity * $ratio;

                    //Add Insert en el detalle
                    if ($this->request->post('save') == 'true') {
                        $o_billing_driver_detail = new Model_BillingDriverDetail();
                        $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                        $o_billing_driver_detail->title = $titleforbd;
                        $o_billing_driver_detail->qty = $quantity;
                        $o_billing_driver_detail->ratio = $ratio;
                        $o_billing_driver_detail->amount = $amount;
                        $o_billing_driver_detail->detailType = 'driverNoShow';
                        $o_billing_driver_detail->status = 1;
                        $o_billing_driver_detail->countEdit = $iCountEdit;
                        $o_billing_driver_detail->save();
                    }
                    $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_HEADER, $title, $quantity, $ratio, $amount, $this->cambiafmysql($o_appointment->appointmentdate) . '<br/>'
                            . $o_appointment->appointmenttime);
                    $f_suma_total+=$amount;
                    $f_sum_pre_app += $amount;
                    $i_row_num++;
                    
                    
                    //ADD
                    if ($this->request->post('other' . $i_app_id) == 'on') {
                        $title = 'Other';
                        $titleforbd = $serviceType . '|' . $title;
                        $quantity = 1;
                        $ratio = $this->request->post('otherValue' . $i_app_id);
                        $amount = $quantity * $ratio;
                        //Add Insert en el detalle
                        if ($_POST['save'] == 'true') {
                            $o_billing_driver_detail = new Model_BillingDriverDetail();
                            $o_billing_driver_detail->idBillingDriverAppointment = $o_billing_driver_appointment->idBillingDriverAppointment;
                            $o_billing_driver_detail->title = $titleforbd;
                            $o_billing_driver_detail->qty = $quantity;
                            $o_billing_driver_detail->ratio = $ratio;
                            $o_billing_driver_detail->amount = $amount;
                            $o_billing_driver_detail->detailType = 'driverOther';
                            $o_billing_driver_detail->status = 1;
                            $o_billing_driver_detail->countEdit = $iCountEdit;
                            $o_billing_driver_detail->save();
                        }
                        $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_ROW, $title, $quantity, $ratio, $amount);
                        $f_suma_total+=$amount;
                        $f_sum_pre_app += $amount;
                        $i_row_num++;
                    }
                    //
                } elseif ($o_appointment->cancelDriver == 1) {
                    //Cancel Driver o.O!
                }
                if ($this->request->post('commentServDri' . $i_app_id) != '') {
                    //New Falta Commentario
                    $rowTable[] = array( 'row_type' => self::S_ITEM_TYPE_NOTE, 'row_date'=>'', 'row_description'=>$this->request->post('commentServDri' . $i_app_id) , 'row_quantity' => ''
                    , 'row_ratio' => ''
                    , 'row_amount' => '');
                }
                $a_sum_pre[$i_app_id] = money_format('%i', $f_sum_pre_app);
            }

          
            if ($this->request->post('save') == 'true') {
               
                
//                $new_o_billing_driver = new Model_BillingDriver();
//                $new_o_billing_driver->find(array("idBillingDriver" => $i_billing_id));
                $o_billing_driver->amount = $f_suma_total;

                $o_billing_driver->save();
                $o_appointment->save();
            }

            $rowTable[] = $this->fnGetRowColumnResponse(self::S_ITEM_TYPE_SUMMARY, 'TOTAL', 0, 0, money_format('%i', $f_suma_total));

            $a_response = array(
                'ok' => 1,
                'billing_driver_id' => $i_billing_id,
                'items_row' => $rowTable
                , 'pre_sum' => $a_sum_pre
                , 'pre_total' => money_format('%i', $f_suma_total)
            );
            
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }
    public function fnGetRowColumnResponse($s_type, $title, $quantity, $ratio, $amount, $firstColumn = '') {
        $a_row_response =
                array(
                    'row_type' => $s_type
                    , 'row_date' => $firstColumn
                    , 'row_description' => $title
                    , 'row_quantity' => $quantity
                    , 'row_ratio' => money_format('%i', $ratio)
                    , 'row_amount' => money_format('%i', $amount)
        );
        return $a_row_response;
    }
    
    public function fnGetBillingInfo($i_app_id, $i_driver_id){
        try{
        
        $response = DB::select(
            DB::expr('CASE 
                WHEN app.typetravel = 2 
                    THEN 
                        CASE 
                            WHEN app.idDriverOneWay = app.idDriverRoundTrip
                                THEN "3|(R) Round Trip" 
                            ELSE 
                                CASE 
                                    WHEN app.idDriverOneWay ='.$i_driver_id.
                                    '    THEN "1|(R) Trip"
                                    ELSE
                                        "2|(R) Journey Trip" 
                                END
                         END
                ELSE
                    "1|(O) Trip"
            END app_travel_billing')
            )->distinct(true)
            ->from(array("appointment", 'app'))
            ->where('app.idAppointment','=',$i_app_id)
            ->execute()->current();    
          
            
            return $response;
        } catch (PDOException $o_exception) {
            echo 'Connection failed: ' . $o_exception->getMessage();
        }
    }
    
    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}