<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Billings_Depositsummary extends Controller_Private_Admin {

    public function action_index() {
        $this->mergeStyles(ConfigFiles::fnGetFiles('depositsummary', self::FILE_TYPE_CSS));
        $this->mergeScripts(ConfigFiles::fnGetFiles('depositsummary', self::FILE_TYPE_JS));
        $view_depositsummary = new View('billings/depositsummary');

        $user = DB::select(array('idUser', 'idUser'), array('fullName', 'fullName'))
                        ->from("bts_person")
                        ->join("bts_user")->on('idPerson', '=', 'idUser')
                        ->where('status', '=', '1')
                        ->execute()->as_array();

        $view_depositsummary->user = $user;

        $this->template->content = $view_depositsummary;
    }

    public function action_listDepositSummary() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        $i_page = $this->request->query('page');
        $i_limit = $this->request->query('rows');
        $i_idx = $this->request->query('sidx');
        $s_ord = $this->request->query('sord');
        $i_idtask = $this->request->query('task_id');

        $dateFrom = $this->cambiafmysqlre($this->request->query('dateFrom'));
        $dateTo = $this->cambiafmysqlre($this->request->query('dateTo'));

        $idUser = $this->request->query('idUser');
        if ($idUser != '')
            $s_parametros = ' AND u.idUser=' . $idUser;
        else
            $s_parametros = ' and TRUE';
        try {
            $s_where_search = "";

            $s_searchOn = jqGridHelper::Strip($this->request->query('_search'));

            $a_trueArray_cols = array(
                'check_id' => 'bc.idBillingChecks',
                'select_list' => "''",
                'userCheckOut' => 'p.fullName',
                'numberCheckOut' => 'ch.numbercheck',
                'method' => "'Check'",
                'nameOut' => 'ic.name',
                'invoiceOut' => "GROUP_CONCAT(bi.invoiceNumber SEPARATOR ', ')",
                'memoOut' => 'ch.memo',
                'amountOut' => 'ch.amount',
                'dateRegistrationOut' => "DATE_FORMAT(ch.datecheck, '%m/%d/%Y')"
            );

//COLUMNAS DE BUSQUEDA SIMPLE
            $a_searcheable_cols = array_keys($a_trueArray_cols);

            if ($s_searchOn == 'true') {
                if ($this->request->query('filters') != '') {
//B��SQUEDA AVANZADA
                    $s_searchstr = jqGridHelper::Strip($this->request->query('filters'));

                    $s_where_search = jqGridHelper::constructWhere($s_searchstr, $a_trueArray_cols);
                } else {
//B��SQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->query() as $i_key => $s_value) {

                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_searcheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }


            /*             * **********  VARIABLE PARA EL WHERE   ***************** */

            $s_cols = jqGridHelper::arrayColsToSQL($a_trueArray_cols);

            $s_tables_join = ' checks ch
                INNER JOIN billingchecks bc
                ON ch.idCheck = bc.idChecks
                INNER JOIN  billing bi
                ON bc.idBilling=bi.idBilling
                INNER JOIN insurancecarrier ic
                ON bi.idInsuranceCarriers= ic.idInsuranceCarrier
                INNER JOIN bts_user u
                ON bc.userCreate= u.idUser
                INNER JOIN bts_person p
                ON u.idUser= p.idPerson';

            $s_where_conditions = ' ch.status = 1
                AND bc.state = 1 
                AND bi.status >0
                AND ch.datecheck BETWEEN "' . $dateFrom . '"  AND "' . $dateTo . '" ' . $s_parametros . ' 
                GROUP BY ch.idCheck ';


            $i_count = jqGridHelper::getCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
            $i_start = jqGridHelper::calculateStart($i_page, $i_count, $i_limit, $i_total_pages);

            $a_result = jqGridHelper::generateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $i_idx, $s_ord, $i_limit, $i_start);

            echo jqGridHelper::JSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_trueArray_cols, true);
        } catch (Exception $e) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $e->getMessage();
        }
    }

    public function action_confirmDepositSummary() {
        $this->auto_render = FALSE;
        $a_response = $this->json_array_return;
        try {
            $aChecks = explode(",", $this->request->post('aChecks'));
            
            $objCheckSummary = new Model_CheckSummary();
            $objCheckSummary->save();
            
            foreach ($aChecks as $idcheck) {
                $objBillingCheck = new Model_BillingCheck($idcheck);
                $objBillingCheck->state = 2;
                $objBillingCheck->idCheckSummary=$objCheckSummary->idCheckSummary;
                $objBillingCheck->save();
            }
        } catch (Exception $e_exc) {
            $a_response['code'] = self::CODE_ERROR;
            $a_response['msg'] = $this->errorHandling($e_exc);
        }
        $this->fnResponseFormat($a_response);
    }

    public function action_reportCheckSummary() {
        $this->auto_render = FALSE;
        try {
            $data['rDate'] = $this->request->query(self::RQ_DATE_FORMAT);
            $data['qlang'] = $this->request->query(self::RQ_LANG);
            $data['searchDate'] = date('l, F d, Y');
            $data['company'] = db_config::$db_conection_config[SITE_DOMAIN]['siteName'];
            $data['logo'] = "http://" . $_SERVER['HTTP_HOST'] . "/media/images/" . db_config::$db_conection_config[SITE_DOMAIN]['skin'] . "/mini_logo.png";

            $dateIni = $this->request->query('dateFrom');
            $dateEnd = $this->request->query('dateTo');
            $idUser = $this->request->query('idUser');
            $aChecks = implode(',', $this->request->query('arrayChecks'));

            $columns = array(array('ch.numbercheck', 'numbercheck'),
                DB::expr("'Check' as method"),
                array('ic.name', 'insurance'),
                array('bi.invoicenumber', 'invoice'),
                array('ch.memo', 'memo'),
                array('ch.amount', 'amount'),
            );

            $sql_columns = DB::select_array($columns)
                            ->from(array("checks", 'ch'))
                            ->join(array('billingchecks', 'bc'))->on('ch.idCheck', '=', 'bc.idChecks')
                            ->join(array('billing', 'bi'))->on('bi.idBilling', '=', 'bc.idBilling')
                            ->join(array('bts_person', 'p'))->on('bc.userCreate', '=', 'p.idPerson')
                            ->join(array('insurancecarrier', 'ic'))->on('ic.idInsurancecarrier', '=', 'ch.idInsuranceCarrier')
                            ->where('ch.status', '>', '0')
                            ->and_where('bc.state', '>', '0')
                            ->and_where('bi.status', '>', '0')
                            ->and_where(DB::expr("FIND_IN_SET(bc.idBillingChecks,'$aChecks')"), '>', 0)
                            ->execute()->as_array();


            $this->fnResponseFormat($sql_columns, self::REPORT_RESPONSE_TYPE_XML, $data);
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
    }

    public function cambiafmysqlre($dtm_fechainicial) {
        list($mes, $dia, $anio) = explode('/', $dtm_fechainicial);
        // reasignamos la fecha a $dtm_fechainicial con su nuevo formato
        $dtm_fechainicial = "$anio-$mes-$dia";
        return $dtm_fechainicial;
    }

    public function cambiafmysql($fecha) {
        list($anio, $mes, $dia) = explode('-', $fecha);
        $lafecha = $mes . "/" . $dia . "/" . $anio;
        return $lafecha;
    }

}
