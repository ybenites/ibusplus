<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uInterface
 *
 * @author Emmanuel
 */
class bxInterface implements Kohana_Uconstants, Qmonti_Constants {

    public $a_drivers_InsuranceDeduction =
            array(
        self::DRIVER_DEDUCTION_A => array(
            'display' => 'a'
            , 'value' => self::DRIVER_DEDUCTION_A
        ),
        self::DRIVER_DEDUCTION_B => array(
            'display' => 'b'
            , 'value' => self::DRIVER_DEDUCTION_B
        )
    );
    public $json_array_return = array(
        'code' => self::CODE_SUCCESS,
        'msg' => 'OK'
    );
    public $a_confirmation_assigment =
            array(
        self::INTERPRETING => array(
            'display' => 'INTERPRETING'
            , 'value' => self::INTERPRETING
        ),
        self::CONFERENCECALL => array(
            'display' => 'CONFERENCECALL'
            , 'value' => self::CONFERENCECALL
        ),
        self::DOCUMENTTRANSLATION => array(
            'display' => 'DOCUMENTTRANSLATION'
            , 'value' => self::DOCUMENTTRANSLATION
        ),
        self::AMBULATORY => array(
            'display' => 'AMBULATORY'
            , 'value' => self::AMBULATORY
        ),
        self::WHEELCHAIR => array(
            'display' => 'WHEELCHAIR'
            , 'value' => self::WHEELCHAIR
        ),
        self::STRETCHER => array(
            'display' => 'STRETCHER'
            , 'value' => self::STRETCHER
        ),
        self::LEGAL => array(
            'display' => 'LEGAL'
            , 'value' => self::LEGAL
        ),
        self::MEDICAL => array(
            'display' => 'MEDICAL'
            , 'value' => self::MEDICAL
        ),
        self::OTHER => array(
            'display' => 'OTHER'
            , 'value' => self::OTHER
        ),
        self::ONEWAY => array(
            'display' => 'OneWay'
            , 'value' => self::ONEWAY
        ),
        self::ROUNDTRAVEL => array(
            'display' => 'RoundTravel'
            , 'value' => self::ROUNDTRAVEL
        ),
        self::CREATED_CONFIRMATION_ADJUSTER => array(
            'display' => 'CREATED_CONFIRMATION_ADJUSTER'
            , 'value' => self::CREATED_CONFIRMATION_ADJUSTER
        ),
        self::CALLING_LIST => array(
            'display' => 'CALLING_LIST'
            , 'value' => self::CALLING_LIST
        ),
        self::CONFIRMED => array(
            'display' => 'CONFIRMED'
            , 'value' => self::CONFIRMED
        ),
        self::APP_CONFIRMED => array(
            'display' => 'APP_CONFIRMED'
            , 'value' => self::APP_CONFIRMED
        )
    );

    public function getLoadCustomConection($name, $conectionParams) {
        $connectionConfig = array();
        $connectionConfig['type'] = 'mysql';
        $connectionConfig['connection'] = $conectionParams;
        $connectionConfig['table_prefix'] = '';
        $connectionConfig['charset'] = 'utf8';
        $connectionConfig['caching'] = FALSE;
        $connectionConfig['profiling'] = TRUE;
        return Database::instance($name, $connectionConfig);
    }
    
    public function fnGetClassVariable($s_variable_name) {
       return $this->{$s_variable_name};
   }

}

return array(
    'uInterface' => new bxInterface()
);



