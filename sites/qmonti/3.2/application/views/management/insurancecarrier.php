<style type="text/css">
    .title{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }
    #buttons{
        margin-left: 0px;

    }
    dd{
        float: left;
        margin-top: 7px;
    }
    label
    {
        text-align: left;
    }
    input{
        margin-left: 5px;
    }

    .btnnew{
        margin-left: 80px;
        margin-bottom: 25px;
        float:left;
    }

    #radiodiscount{float: left;}

    #frmNew fieldset{

        padding: 2px 2px 5px 13px;

    }

    .tbl-new-ic .frmlbl{
        font-weight: bold;
        margin: 0px;
        padding: 0px;
        height: auto;
        display: inline;
        float: none;
    }
    .tbl-new-ic tr{
        line-height: 25px;
    }
    .tbl-new-ic td.label{
        text-align: left;
    }
    .tbl-new-ic td > input, .tbl-new-ic td > select{
        margin-left: 2px;
    }
    .btnprueba{display: inline;}

</style>


<script type="text/javascript">

    var url_gridListInsurance = '/management/insurancecarrier/listInsurancecarrier'
    var url_createUpdateInsurance = '/management/insurancecarrier/createUpdateInsurance' + jquery_params;
    var url_getInsurance = '/management/insurancecarrier/getInsurancecarrier' + jquery_params;
    var url_deleteInsurance = '/management/insurancecarrier/deleteInsurance' + jquery_params;
    var url_activeInsurance = '/management/insurancecarrier/activeInsurance' + jquery_params;
    var newInsurance = '<?php echo __('Nueva Compañía de Seguros'); ?>';
    var newInsurancerate = '<?php echo __('Nueva Compañía de Seguros'); ?>';
    var phone = '<?php echo __('Telefono'); ?>';
    var contactname = '<?php echo __('Nombre de Contacto'); ?>';
    var name = '<?php echo __('Nombre de Aseguradora'); ?>';
    var address = '<?php echo __('Direccion'); ?>';
    var phone = '<?php echo __('Telefono'); ?>';
    var daterecord = '<?php echo __('Registro de Fecha'); ?>';
    var status = '<?php echo __('Estado'); ?>';
    var actions = '<?php echo __('Acciones'); ?>';
    var msg_delete = '<?php echo __('Esta seguro de eliminar esta Aseguradora?'); ?>';
    var msg_active = '<?php echo __('Esta seguro de activar esta Aseguradora?'); ?>';
    var title_delete = '<?php echo __('Eliminar Proveedor'); ?>';
    var title_active = '<?php echo __('Activar Proveedor'); ?>';
    var listInsurancecarrier = '<?php echo __('Lista de Aseguradoras'); ?>';
    var validInsurance = '<?php echo __('El nombre de la Compañías de Seguros es requerido'); ?>';
    var txt_emailrequests = '<?php echo __('El E-mail es requerido.'); ?>';
    var txt_emailincorret = '<?php echo __('El E-mail es incorrecto.'); ?>';
    var o_rates = {};
    
    $(document).ready(function() {
        
        $("#radiodiscount").buttonset();
        $('#daterecord').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $("#terms").spinner({
            spin: function(event, ui) {
                if (ui.value > 31) {
                    $(this).spinner("value", 1);
                    return false;
                } else if (ui.value < 1) {
                    $(this).spinner("value", 31);
                    return false;
                }
            }
        });
        
        $('.btnprueba').buttonset();

        $('#newinsurance').button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
        
        $('#newinsurance').button().click(function() {
            frmNewInsurance.currentForm.reset();
            $('#frmNew').dialog('open');
        });
        
        $('#dialog_insurance_rate').dialog({
            autoOpen: false,
            title: newInsurancerate,
            height: 500,
            width: 500,
            buttons: {
                'Guardar': function() {
                    //Funcione el Validate 
                    if (!$('#frmNewInsuranceRates').valid()) {
                        return false;
                    }
                    $('#frmNewInsuranceRates').submit();
                },
                'Cancelar': function() {
                    /*Limpiar campos con reset */

                    $(this).dialog('close');
                }
            }
            , close: function() {
                frmNewInsuranceRates.currentForm.reset();
                $('#idInsuranceRates').val('');
                $('#ir_city').val('');
            }
        });

        frmNewInsuranceRates = $('#frmNewInsuranceRates').validate({
            submitHandler: function(o_form) {
                o_rates[$('#ir_city').val()] = $(o_form).serializeObject();
                $('#dialog_insurance_rate').dialog('close');
            }
        });

        //abre dialogo//
        $('#frmNew').dialog({
            autoOpen: false,
            title: newInsurance,
            height: 370,
            width: 650,
            buttons: {
                'Guardar': function() {
                    //Funcione el Validate 
                    if (!$('#frmNewInsurance').valid()) {
                        return false;
                    }
                    $('#frmNewInsurance').submit();
                },
                'Cancelar': function() {
                    /*Limpiar campos con reset */
                    //frmNewInsurance.currentForm.reset();
                    $(this).dialog('close');
                }
            },
            close: function() {
                frmNewInsurance.currentForm.reset();
                o_rates = {};
            }
        });




        /* VALIDATION DE NEW Insurancecarrier */
        frmNewInsurance = $('#frmNewInsurance').validate({
            rules: {
                name: {
                    required: true
                },
                //                idInsurancecarrier:{
                //                     required:true
                //               },

                email: {
                    required: true,
                    email: true
                }

            },
            messages: {
                name: {
                    required: validInsurance
                },
                der_email: {
                    required: txt_emailrequests,
                    email: txt_emailincorret
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {
                $.ajax({
                    url: url_createUpdateInsurance,
                    type: "post",
                    data:
                        $.extend($("#frmNewInsurance").serializeObject(), {o_rates: o_rates}),
                    dataType: "json",
                    success: function(r) {
                        if (r.code == code_success)
                        {
                            $("#gridListInsurance").trigger('reloadGrid');
                            $("#frmNew").dialog('close');
                            frmNewInsurance.currentForm.reset();
                        }

                    }
                });
            }

        });

        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function() {
            $("#gridListInsurance").trigger("reloadGrid");
        });

        /* COMBOBOX */
        //        $('#Status').combobox();

        /* JQGRID LIST Insurancecarrier */
        $("#gridListInsurance").jqGrid({
            url: url_gridListInsurance,
            datatype: 'JSON',
            postData: {
                all: function() {
                    return $('#status').is(':checked');
                }
            },
            colNames: [
                'idInsurancecarrier',
                '<?php echo __('Nombre'); ?>',
                '<?php echo __('Nombre Contacto'); ?>',
                '<?php echo __('Direccion'); ?>',
                '<?php echo __('Telefono'); ?>',
                '<?php echo __('Fecha Guardado'); ?>',
                '<?php echo __('Estado'); ?>',
                '<?php echo __('Acciones'); ?>'
            ],
            colModel: [
                {index: 'idInsurancecarrier', name: 'idInsurancecarrier', key: true, hidden: true},
                {index: 'name', name: 'name', width: 400, search: true,align:"center"},
                {index: 'contactname', name: 'contactname', width: 200, search: true,align:"center"},
                {index: 'address', name: 'address', width: 150, search: true,align:"center"},
                {index: 'phone', name: 'phone', width: 100, search: true,align:"center"},
                {index: 'daterecord', name: 'daterecord', width: 70, search: true,align:"center"},
                {index: 'status', name: 'status', width: 80, search: false,align:"center"},
                {index: 'actions', name: 'actions', width: 80, search: false,align:"center"}
            ],
            pager: '#gridListInsurancepaper',
            rowNum: 100,
            height: 200,
            width:1200,
            viewrecords: true,
            rowList: [100, 200, 300],
            caption: listInsurancecarrier,
            afterInsertRow: function(row_id, data_id)
            {
                if (data_id.status == 1)
                {
                    edit = "<a class=\"edit\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#gridListInsurance").jqGrid('setRowData', row_id, {actions: edit + trash, status: 'Activo'});
                }
                else
                {
                    active = "<a class=\"active\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#gridListInsurance").jqGrid('setRowData', row_id, {actions: active, status: 'Inactivo'});
                }
            },
            gridComplete: function() {
                //figuras de botones del dialog
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                })

                $('.trash').button({
                    icons: {primary: 'ui-icon-trash'},
                    text: false
                })

                $('.active').button({
                    icons: {primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }

        });
        //jQuery("#gridListInsurance").jqGrid('navGrid', "#gridListInsurancepaper", {edit: false, add: false, del: false});
        jQuery("#gridListInsurance").jqGrid('inlineNav', "#gridListInsurancepaper");
        $("#gridListInsurance").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});

        /* Eliminar Insurancecarrier */
        $("#gridListInsurance").on('click', '.trash', function() {
            var id = $(this).data('oid');
            confirmBox(msg_delete, title_delete, function(response) {
                if (response) {
                    $.ajax({
                        url: url_deleteInsurance,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListInsurance").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });
        /* Active Insurancecarrier */
        $("#gridListInsurance").on('click', '.active', function() {
            var id = $(this).data('oid');
            confirmBox(msg_active, title_active, function(response) {
                if (response) {
                    $.ajax({
                        url: url_activeInsurance,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListInsurance").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });
        /* Editar Insurancecarrier*/
        $("#gridListInsurance").on('click', '.edit', function() {
            var id = $(this).data('oid');
            $.ajax({
                url: url_getInsurance,
                type: "post",
                data: {id: id},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        /* DATOS AL DIALOG */
                        $("#idInsurancecarrier").val(data.idInsurancecarrier);
                        $("#name").val(data.name);
                        $("#contactname").val(data.contactname);
                        $("#address").val(data.address);
                        $("#phone").val(data.phone);
                        $("#fax").val(data.fax);
                        $("#city").val(data.city);
                        $("#state").val(data.state);
                        $("#zipcode").val(data.zipcode);
                        $("#email").val(data.email);
                        $("#daterecord").val(data.daterecord);
                        $("#terms").val(data.terms);
                        $("#discount").val(data.discount);
                        $("#extphonecarrier").val(data.extphonecarrier);
                        $('input[name=radiodiscount]').val([data.havediscount]);
                        $('input[name=radiodiscount]:checked').trigger('change');
                        $('#frmNew').dialog('open');


                        $.each(j_response.rates, function(i, o_data) {
                            o_rates[o_data.cityType] = o_data;
                        });
                    } else {
                        /* ERROR */
                    }

                }
            });
        });

        $("#cityType").combobox({
            selected: function() {
                cmbVal = $(this).val();
                //console.log(cmbVal);
                if (cmbVal == 'FLORIDA' || cmbVal == 'GEORGIA' || cmbVal == 'OTHER') {
                    $('#ir_city').val(cmbVal);
                    if (o_rates[cmbVal] != undefined) {
                        var o_data = o_rates[cmbVal];
                        $('#idInsuranceRates').val(o_data.idInsuranceRates);
                        $('#noShowIntMedicalRegular').val(o_data.noShowIntMedicalRegular);
                        $('#noShowIntMedicalRegularMin').val(o_data.noShowIntMedicalRegularMin);
                        $('#noShowIntMedicalRare').val(o_data.noShowIntMedicalRare);
                        $('#noShowIntMedicalRareMin').val(o_data.noShowIntMedicalRareMin);
                        $('#noShowIntMedicalOther').val(o_data.noShowIntMedicalOther);
                        $('#noShowIntMedicalOtherMin').val(o_data.noShowIntMedicalOtherMin);
                        $('#noShowIntLegalRegular').val(o_data.noShowIntLegalRegular);
                        $('#noShowIntLegalRegularMin').val(o_data.noShowIntLegalRegularMin);
                        $('#noShowIntLegalRare').val(o_data.noShowIntLegalRare);
                        $('#noShowIntLegalRareMin').val(o_data.noShowIntLegalRareMin);
                        $('#noShowIntLegalOther').val(o_data.noShowIntLegalOther);
                        $('#noShowIntLegalOtherMin').val(o_data.noShowIntLegalOtherMin);

                        $('#medicalRegular').val(o_data.medicalRegular);
                        $('#medicalOther').val(o_data.medicalOther);
                        $('#medicalRare').val(o_data.medicalRare);
                        $('#legalRegular').val(o_data.legalRegular);
                        $('#legalOther').val(o_data.legalOther);
                        $('#legalRare').val(o_data.legalRare);
                        $('#conferenceCallRegular').val(o_data.conferenceCallRegular);
                        $('#conferenceCallOther').val(o_data.conferenceCallOther);
                        $('#conferenceCallRare').val(o_data.conferenceCallRare);
                        $('#travelMiles').val(o_data.travelMiles);
                        $('#travelTime').val(o_data.travelTime);
                        $('#tipociudad').val(o_data.tipociudad);
                        $('#gasolinesurcharge').val(o_data.gasolinesurcharge);
                        $('#ambulatoryRate').val(o_data.ambulatoryRate);
                        $('#ambulatoryWaitingTime').val(o_data.ambulatoryWaitingTime);
                        $('#minimumFlatRateRTAmbulatory').val(o_data.minimumFlatRateRTAmbulatory);
                        $('#minimumFlatRateOWAmbulatory').val(o_data.minimumFlatRateOWAmbulatory);
                        $('#ambulatoryNoShow').val(o_data.ambulatoryNoShow);
                        $('#wheelChairLoadedMiles').val(o_data.wheelChairLoadedMiles);
                        $('#wheelChairLiftBaseRate').val(o_data.wheelChairLiftBaseRate);
                        $('#wheelChairWaitingTime').val(o_data.wheelChairWaitingTime);
                        $('#wheelChairCarAssitance').val(o_data.wheelChairCarAssitance);
                        $('#minimumFlatRateRTWheelChair').val(o_data.minimumFlatRateRTWheelChair);
                        $('#minimumFlatRateOWWheelChair').val(o_data.minimumFlatRateOWWheelChair);
                        $('#wheelChairNoShow').val(o_data.wheelChairNoShow);
                        $('#stretcherLoadedMiles').val(o_data.stretcherLoadedMiles);
                        $('#stretcherLiftBaseRate').val(o_data.stretcherLiftBaseRate);
                        $('#stretcherWaitingTime').val(o_data.stretcherWaitingTime);
                        $('#minimumFlatRateRTStretcher').val(o_data.minimumFlatRateRTStretcher);
                        $('#minimumFlatRateOWStretcher').val(o_data.minimumFlatRateOWStretcher);
                        $('#stretcherNoShow').val(o_data.stretcherNoShow);
                        $('#earlyPickUpS').val(o_data.earlyPickUpS);
                        $('#lateDropOffS').val(o_data.lateDropOffS);
                        $('#weekEndFeeS').val(o_data.weekEndFeeS);
                        $('#earlyPickUpW').val(o_data.earlyPickUpW);
                        $('#lateDropOffW').val(o_data.lateDropOffW);
                        $('#weekendFeeW').val(o_data.weekendFeeW);
                        $('#earlyPickUpA').val(o_data.earlyPickUpA);
                        $('#lateDropOffA').val(o_data.lateDropOffA);
                        $('#weekendFeeA').val(o_data.weekendFeeA);
                        $('#stretcherAssistanceS').val(o_data.stretcherAssistanceS);
                    }
                    $('#dialog_insurance_rate').dialog('open');
                }
            }
        });
    });

</script>
<!--codigo html-->
<div>
    <center>
        <div class="title ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTAS DE ASEGURADORAS') ?></div>
    </center>
</div>
<div class="show_content">
    <div id="buttons">
        <button id="newinsurance"><?php echo __('Nueva Aseguradora') ?></button>
        <div class="btnprueba">
            <input type="checkbox" id="status" name="status" />
            <label for="status"><?php echo __('Listar Todos') ?></label><br><br>
        </div>
    </div>

    <div>
        <center>
            <table id="gridListInsurance"></table>
            <div id="gridListInsurancepaper"></div>
        </center>
    </div>
</div>    

<div id="frmNew" class="none">
    <form id="frmNewInsurance" name="frmNewInsurance" action="" method="POST">
        <fieldset>
            <legend><? echo __('Crear Compañía de Seguros') ?></legend>
            <input type="text" name="idInsurancecarrier" id="idInsurancecarrier" hidden="true"/>
            <table class="tbl-new-ic">
                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Compañía de Seguros') ?></label></td>
                    <td><input type="text" name="name" id="name" size="30" value=""></td>
                     
                    <td class="label"><label class="frmlbl"><?php echo __('Nombre de Contacto') ?></label></td>
                    <td><input type="text" name="contactname" id="contactname" size="30" value=""></td>
                </tr>

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Direccion') ?></label></td>
                    <td><input type="text" name="address" id="address" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Telefono') ?></label></td>
                    <td><input type="text" name="phone" id="phone" size="30" value=""></td>
                </tr> 

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Fax') ?></label></td>
                    <td><input type="text" name="fax" id="fax" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Estado') ?></label></td>
                    <td><select name="status" id="status">
                            <option value="1"><?php echo __('Activo') ?></option>
                            <option value="0"><?php echo __('Desactivo') ?></option>
                        </select></td>
                </tr>  

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Ciudad') ?></label></td>
                    <td><input type="text" name="city" id="city" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Codigo Zip') ?></label></td>
                    <td><input type="text" name="zipcode" id="zipcode" size="30" value=""></td>
                </tr> 

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Estado') ?></label></td>
                    <td><input type="text" name="state" id="state" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Correo Electronico') ?></label></td>
                    <td><input type="text" name="email" id="email" size="30" value=""></td>
                </tr> 

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Fecha Guardado') ?></label></td>
                    <td><input type="text" name="daterecord" id="daterecord" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Terms') ?></label></td>
                    <td><input type="text" name="terms" id="terms" size="5" value=""></td>
                </tr> 

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Extension') ?></label></td>
                    <td><input type="text" name="extphonecarrier" id="extphonecarrier" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Tienen Descuento') ?></label></label></td>
                    <td><div id="radiodiscount">
                            <input type="radio" id="radiodiscount1" name="radiodiscount" value="1" /><label for="radiodiscount1"><?php echo __('Si') ?></label>
                            <input type="radio" id="radiodiscount2" name="radiodiscount" value="0"checked="checked" /><label for="radiodiscount2"><?php echo __('No') ?></label>
                        </div></td>
                </tr>  

                <tr>
                    <td class="label"><label class="frmlbl"><? echo __('Descuento') ?></label></td>
                    <td><input type="text" name="discount" id="discount" size="30" value=""></td>
                    <td class="label"><label class="frmlbl"><?php echo __('Region') ?></label></td>
                    <td><select name="cityType" id="cityType">
                            <option value=""><?php echo __('Select') ?></option>
                            <option value="FLORIDA"><?php echo __('Florida') ?></option>
                            <option value="GEORGIA"><?php echo __('Georgia') ?></option>
                            <option value="OTHER"><?php echo __('Other') ?></option>
                        </select></td>
                </tr>


            </table>

<!--<img id="addic" src="">-->
        </fieldset>
    </form>
</div>

<div id="dialog_insurance_rate" hidden="true">
    <form id="frmNewInsuranceRates" name="frmNewInsuranceRates" action="" method="POST">
        <input type="hidden" id="ir_city" name="ir_city"/>
        <input type="hidden" id="idInsuranceRates" name="idInsuranceRates"/>

        <fieldset>
            <legend><? echo __('Tasas de intérpretes') ?></legend>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Regular') ?></label>
                    <input type="text" name="noShowIntMedicalRegular" id="noShowIntMedicalRegular" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Regular Min Time') ?></label>
                    <input type="text" name="noShowIntMedicalRegularMin" id="noShowIntMedicalRegularMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Rare') ?></label>
                    <input type="text" name="noShowIntMedicalRare" id="noShowIntMedicalRare" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Rare Min Time') ?></label>
                    <input type="text" name="noShowIntMedicalRareMin" id="noShowIntMedicalRareMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Other') ?></label>
                    <input type="text" name="noShowIntMedicalOther" id="noShowIntMedicalOther" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Medical Other Min Time') ?></label>
                    <input type="text" name="noShowIntMedicalOtherMin" id="noShowIntMedicalOtherMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Regular') ?></label>
                    <input type="text" name="noShowIntLegalRegular" id="noShowIntLegalRegular" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Regular Min Time') ?></label>
                    <input type="text" name="noShowIntLegalRegularMin" id="noShowIntLegalRegularMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Rare') ?></label>
                    <input type="text" name="noShowIntLegalRare" id="noShowIntLegalRare" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Rare Min Time') ?></label>
                    <input type="text" name="noShowIntLegalRareMin" id="noShowIntLegalRareMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Other') ?></label>
                    <input type="text" name="noShowIntLegalOther" id="noShowIntLegalOther" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('No Show Legal Other Min Time') ?></label>
                    <input type="text" name="noShowIntLegalOtherMin" id="noShowIntLegalOtherMin" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Medical Regular') ?></label>
                    <input type="text" name="medicalRegular" id="medicalRegular" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Medical Other') ?></label>
                    <input type="text" name="medicalOther" id="medicalOther" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Medical Rare') ?></label>
                    <input type="text" name="medicalRare" id="medicalRare" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Legal Regular') ?></label>
                    <input type="text" name="legalRegular" id="legalRegular" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Legal Other') ?></label>
                    <input type="text" name="legalOther" id="legalOther" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Legal Rare') ?></label>
                    <input type="text" name="legalRare" id="legalRare" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Conference Call Regular') ?></label>
                    <input type="text" name="conferenceCallRegular" id="conferenceCallRegular" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Conference Call Other') ?></label>
                    <input type="text" name="conferenceCallOther" id="conferenceCallOther" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Conference Call Rare') ?></label>
                    <input type="text" name="conferenceCallRare" id="conferenceCallRare" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Travel Time') ?></label>
                    <input type="text" name="travelTime" id="travelTime" size="30" value="">
                </dd>
            </dll>
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Millaje') ?></label>
                    <input type="text" name="travelMiles" id="travelMiles" size="30" value="">
                </dd>
            </dll>
        </fieldset>

        <fieldset>
            <legend><? echo __('Conductores') ?></legend>        
            <dll>
                <dd>
                    <label class="frmlbl"><? echo __('Recargo de Gasolina') ?></label>
                    <input type="text" name="gasolinesurcharge" id="gasolinesurcharge" size="30" value="">
                </dd>
            </dll>

            <fieldset>
                <legend><? echo __(' Ambulatory') ?></legend>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Millas') ?></label>
                        <input type="text" name="ambulatoryRate" id="ambulatoryRate" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Waiting Time') ?></label>
                        <input type="text" name="ambulatoryWaitingTime" id="ambulatoryWaitingTime" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Min Flat Rate RT') ?></label>
                        <input type="text" name="minimumFlatRateRTAmbulatory" id="minimumFlatRateRTAmbulatory" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Min Flat Rate OW') ?></label>
                        <input type="text" name="minimumFlatRateOWAmbulatory" id="minimumFlatRateOWAmbulatory" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('No Show') ?></label>
                        <input type="text" name="ambulatoryNoShow" id="ambulatoryNoShow" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Early Pick Up') ?></label>
                        <input type="text" name="earlyPickUpA" id="earlyPickUpA" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Late Drop Off') ?></label>
                        <input type="text" name="lateDropOffA" id="lateDropOffA" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Weekend Fee') ?></label>
                        <input type="text" name="weekendFeeA" id="weekendFeeA" size="30" value="">
                    </dd>
                </dll>
            </fieldset>

            <fieldset>  
                <legend><? echo __('WheelChair Rates') ?></legend>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Loaded Miles') ?></label>
                        <input type="text" name="wheelChairLoadedMiles" id="wheelChairLoadedMiles" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Lift Base Rate') ?></label>
                        <input type="text" name="wheelChairLiftBaseRate" id="wheelChairLiftBaseRate" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Tiempo Espera') ?></label>
                        <input type="text" name="wheelChairWaitingTime" id="wheelChairWaitingTime" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Car Assistance') ?></label>
                        <input type="text" name="wheelChairCarAssitance" id="wheelChairCarAssitance" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Minimum Flat Rate RT') ?></label>
                        <input type="text" name="minimumFlatRateRTWheelChair" id="minimumFlatRateRTWheelChair" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Minimum Flat Rate OW') ?></label>
                        <input type="text" name="minimumFlatRateOWWheelChair" id="minimumFlatRateOWWheelChair" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('No Show') ?></label>
                        <input type="text" name="wheelChairNoShow" id="wheelChairNoShow" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Early Pick Up') ?></label>
                        <input type="text" name="earlyPickUpW" id="earlyPickUpW" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Late Drop Off') ?></label>
                        <input type="text" name="lateDropOffW" id="lateDropOffW" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Weekend Fee') ?></label>
                        <input type="text" name="weekendFeeW" id="weekendFeeW" size="30" value="">
                    </dd>
                </dll>
            </fieldset>


            <fieldset>
                <legend><? echo __('Stretcher Rates') ?></legend>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Millas') ?></label>
                        <input type="text" name="stretcherLoadedMiles" id="stretcherLoadedMiles" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Lift Base Rate') ?></label>
                        <input type="text" name="stretcherLiftBaseRate" id="stretcherLiftBaseRate" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Waiting Time') ?></label>
                        <input type="text" name="stretcherWaitingTime" id="stretcherWaitingTime" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Assistance') ?></label>
                        <input type="text" name="stretcherAssistanceS" id="stretcherAssistanceS" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Minimum Flat Rate RT') ?></label>
                        <input type="text" name="minimumFlatRateRTStretcher" id="minimumFlatRateRTStretcher" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Minimum Flat Rate OW') ?></label>
                        <input type="text" name="minimumFlatRateOWStretcher" id="minimumFlatRateOWStretcher" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('No Show') ?></label>
                        <input type="text" name="stretcherNoShow" id="stretcherNoShow" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Early Pick Up') ?></label>
                        <input type="text" name="earlyPickUpS" id="earlyPickUpS" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('Late Drop Off') ?></label>
                        <input type="text" name="lateDropOffS" id="lateDropOffS" size="30" value="">
                    </dd>
                </dll>
                <dll>
                    <dd>
                        <label class="frmlbl"><? echo __('WeekEnd Fee') ?></label>
                        <input type="text" name="weekEndFeeS" id="weekEndFeeS" size="30" value="">
                    </dd>
                </dll>
            </fieldset>
        </fieldset>

</div>

