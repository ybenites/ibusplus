<style type="text/css"> 

    .float_left{float: left;}
    .float_right{float: right;}
    .driver_info{width: 425px;float: left;margin-right: 5px;}
    .driver_rate_container{width: 425px;float: left;margin-left: 5px;}


    #divInsuranceDeduction{
        display: none;
    }

    #status{

        margin-bottom: 25px;
        float:left;
    }
    .btnNew{
        margin-left: 2px; 
        margin-bottom: 25px;
        float:left;
    }

    #formNew fieldset{

        padding: 3px 2px 4px 7px;

    }


    .title{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
        margin-top: 32px;
    }
    .titulos{
        padding: 8px;

    }
</style>


<script type="text/javascript">
    
    var titleNewDrivers = '<?php echo __('Nuevo Conductor') ?>';
    var titleNewAreaDrivers = '<?php echo __('Nueva Area') ?>';
    var btnGuardar = '<?php echo __('Guardar') ?>';
    var btnCancelar = '<?php echo __('Cancelar') ?>';
    var firstName = '<?php echo __('Nombre') ?>';
    var listAllDrivers = '/management/drivers/listAllDrivers';
    var saveDrivers = '/management/drivers/saveDrivers'+jquery_params;
    var saveAreadrivers = '/management/drivers/saveAreadrivers'+jquery_params;
    var delDrivers="/management/drivers/delete"+jquery_params;
    var findId="/management/drivers/getPersonById"+jquery_params;
    var activeDrivers = "/management/drivers/active"+jquery_params;
    var deleteSubTitle = "<?php echo __("Eliminar Conductor"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro que quiere eliminar este conductor?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro que quiere activar este conductor?"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    
     
    
    $(document).ready(function(){
        
        /* COMBOBOX */
        $('#areas_drivers_id').combobox();
        $('#insurance_deduction').combobox();
        
        
        /* BUTTONS */
        
        
        $('input[name=for_response]').change(function(){
            $('#divInsuranceDeduction').hide(); 
            if($(this).val() == '1')
            {
                $('#divInsuranceDeduction').show();
                
            }
            
        });
        
        
        $('input[name=status_driver]').change(function(){
            $('#divstatus').hide();
            if($(this).val() == '0')
            {
                $('#divstatus').show();
            }
            
        });
        
        $('.buttonsCheck').buttonset();
        
        
        $('#addarea').button({
            icons:{
                primary:'ui-icon-plus'
            }
            ,
            text:false
        });
        
        
        $('#newUser').button({
            icons:{
                primary:'ui-icon-plus'
            }
          
        });
        /*Boton chkckbox */
        $('.btnprueba').buttonset();
        
        
        
        /************************ CALENDARIO *****************/
        $('#termination_date').datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        
        
        $('#insurance_date').datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        
        /* DIALOG NUEVO DRIVERS */
        $("#formNew").dialog({
            autoOpen: false,
            title:titleNewDrivers,
            height: 500,
            width: 500,
            resizable:false,
            modal: true,
            close:function(){
                $('#validation').get(0).reset();
            },
            buttons:{
                Guardar:function(){                
                    if(!$('#validation').valid()){
                        return false;
                    } 
                    $('#validation').submit();
                },
                Cancelar:function(){
                    $( this ).dialog( "close" );
                }
            }
            
                     
        });
        
        /* ************************DIALOG NUEVO AREA DRIVERS *************************/
        $("#dialogo-areadriver").dialog({
            autoOpen: false,
            title:titleNewAreaDrivers,
            height: 200,
            width: 200,
            modal: true,
            close:function(){
                $('#formulario-areadriver').get(0).reset();
                $('#formulario-areadriver input[type=hidden]').val('');
            },
            buttons:{
                Guardar:function(){                
                    if(!$('#formulario-areadriver').valid()){
                        return false;
                    } 
                    $('#formulario-areadriver').submit();
                },
                Cancelar:function(){
                    $( this ).dialog( "close" );
                }
            }
            
                     
        });
        
        /*VALIDACION DEL DIALOG*/
        
        
        $('#formulario-areadriver').validate({
            rules:{
                location:{
                    required:true
                }
                
            } ,
             
          
            messages:{
                location:{
                    required:'<?php echo __("La locacion es requerida") ?>'
                }

            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                showLoading = 1;
                $.ajax({
                    url: saveAreadrivers,
                    type:"post",
                    data:$('#formulario-areadriver').serializeObject(),
                    dataType:"json",
                    success: function(j_response){ 
                        var data=j_response.data;

                        $('#areas_drivers_id').append(
                        $('<option/>').attr('value',data.idAreasdriver).text(data.location)
                    ).val(data.idAreasdriver);
                        
                        $('#areas_drivers_id-combobox').val($('#areas_drivers_id option:selected').text());
                               
                    }
                });
            }
                       
        });

        $('#validation').validate({
            rules:{
                name:{
                    required:true
                } ,
                
                     
                email:{
                    required: true,
                    email:true
                },
                
                areas_drivers_id:{
                    required: true
                },
                
                services:{
                    required:true  
                },
                
                last_name:{
                    required:true
                }

                
            },
            messages:{
                name:{
                    required:'<?php echo __("Nombre es requerido") ?>'
                },
                last_name:{
                    required:'<?php echo __("Apellido es requerido") ?>'
                },
                areas_drivers_id:{
                    required: '<?php echo __("Area es requerida") ?>'
                },
                
                services:{
                    required:'<?php echo __("Servicio es requerido") ?>'
                },
                email:{
                    required: '<?php echo __("Correo Electronico es requerido") ?>',
                    email:'<?php echo __("Correo Electronico incorrecto") ?>'
                }


            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                showLoading = 1;
                $.ajax({
                    url: saveDrivers,
                    type:"post",
                    data:$('#validation').serializeObject(),
                    dataType:"json",
                    success: function(r){               
                               
                        if(evalResponse(r))
                        {
                            $("#user_jqGrid").trigger('reloadGrid');
                            $("#formNew").dialog('close'); 
                        }

                    }
                });
            }
                       
        });
        
        
        /*BUTTON NEW USER*/
        
        $("#newUser").click(function() {
            $("#formNew").dialog( "open" );
        });   
        
        $("#addarea").click(function() {
            $("#dialogo-areadriver").dialog( "open" );
        });
        
                
        /*JQGRID LIST DRIVERS */     
        
        $("#user_jqGrid").jqGrid({ 
            url:listAllDrivers, 
            datatype: "json",
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['<?php echo __("id"); ?>',
                '<?php echo __("Nombre"); ?>',
                '<?php echo __("Apellido"); ?>',
                '<?php echo __("Direccion"); ?>',
                '<?php echo __("Telefono"); ?>',
                '<?php echo __("Fax"); ?>',
                '<?php echo __("Celular"); ?>',
                '<?php echo __("E-mail"); ?>',
                '<?php echo __("Notas"); ?>',
                '<?php echo __("Area"); ?>',
                '<?php echo __("Status"); ?>',
                '<?php echo __("Registrado por"); ?>',
                '<?php echo __("Acciones"); ?>',], 
            colModel:[ 
                {name:'id',index:'id', hidden:true}, 
                {name:'name',index:'name', width:100, align:"center",frozen : true}, 
                {name:'last_name',index:'last_name', width:100, align:"center",frozen : true}, 
                {name:'address',index:'address', width:200, editable:true, align:"center"}, 
                {name:'phone',index:'phone', width:100,editable:true, align:"center"}, 
                {name:'fax',index:'fax', width:100, align:"center",editable:true},
                {name:'cellphone',index:'cellphone', width:100, align:"center",editable:true},
                {name:'email',index:'email', width:200, align:"center",editable:true},
                {name:'specialNotes',index:'specialNotes', width:400, align:"center",editable:true},
                {name:'location',index:'location', width:200, align:"center",editable:true},
                {name:'status',index:'status', width:100, align:"center",editable:true},
                {name:'user',index:'user', width:200, align:"center",editable:true},
                {name:'actions',index:'actions', width:80, align:"center",search:false,frozen : true}
            ],
                        
            rowNum:100, 
            rowList:[100,200,300,500], 
            pager: 'user_jqGrid_pager', 
            sortname: 'id', 
            height: 200,
            width:1200,
            shrinkToFit:false,
            viewrecords: true, 
            sortorder: "desc",             
            caption: '<?php echo __("Lista de Conductores") ?>',
            afterInsertRow:function(row_id, data_id){
                
                if(data_id.status==1)
                {
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#user_jqGrid").jqGrid('setRowData',row_id,{actions:edit+trash, status: '<?php echo __("Activado") ?>'});
                }
                
                else 
                {
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#user_jqGrid").jqGrid('setRowData',row_id,{actions:active, status: '<?php echo __("Desactivado") ?>'});
                }
            },
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                
                   
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                })
                
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })

            }
                    
        }); 
        
        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function(){
            $("#user_jqGrid").trigger("reloadGrid");
        });
        
        jQuery("#user_jqGrid").jqGrid('inlineNav',"#user_jqGrid_pager"); 
        $("#user_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
 
        $("table#user_jqGrid").on('click','.edit',function(){    
            var id =$(this).data("oid");
            showLoading = 1;
            $.ajax({
                url: findId,
                type:"post",
                data:{id:id},
                dataType:"json",
                success: function(j_response){                               
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        
                        $("#idDrivers").val(data.idDriver);
                        $("#name").val(data.name);
                        $("#last_name").val(data.lastName);
                        
                        $("#areas_drivers_id").val(data.idAreasdriver);
                        $('#areas_drivers_id-combobox').val($('#areas_drivers_id option:selected').text());
                        
                        $('input[name=for_drive]').val([data.typeservices]);
                        $('input[name=for_drive]:checked').trigger('change');
                                              
                        
                        $("#address").val(data.address);
                        $("#city").val(data.city);
                        $("#state").val(data.state);
                        $("#zip_code").val(data.zipcode);
                        $("#phone").val(data.phone);
                        $("#fax").val(data.fax);
                        $("#cell_phone").val(data.cellphone);
                        $("#insurance_date").val(data.insurance);
                        $("#identification_number").val(data.identificationNumber);
                        $("#email").val(data.email);
                        $("#car_make").val(data.carMake);
                        $("#car_model").val(data.carModel);
                        $("#special_notes").val(data.specialNotes);
                        $("#location").val(data.location);
                        $("#gasoline_surcharge").val(data.gasolineSurcharge);
                        $("#insurance_deduction").val(data.insuranceDeduction);
                        $("#insurance_deduction-combobox").val($('#insurance_deduction option:selected').text());
                        
                        $("#ambulatory_rate").val(data.ambulatoryRate);
                        $("#ambulatory_waiting_time").val(data.ambulatoryWaitingTime);
                        $("#ambulatory_min_flat_rate_ow").val(data.ambulatoryMiniFlatRateOW );
                        $("#ambulatory_min_flat_rate_rt").val(data.ambulatoryMiniFlatRateOT);
                        $("#ambulatory_no_show").val(data.ambulatoryNoShow);
                        $("#ambulatory_early_pick_up").val(data.ambulatoryEarlyPickUp);
                        $("#ambulatory_late_drop_off").val(data.ambulatoryLateDropOff);
                        $("#ambulatory_weekend_fee").val(data.ambulatoryWeekendFee);
                        
                        $("#wheelchair_rate").val(data.wheelchairRate);
                        $("#wheelchair_lift_base").val(data.wheelchairLiftBase);
                        $("#wheelchair_waiting_time").val(data.wheelchairWaitingTime );
                        $("#wheelchair_car_assistance").val(data.wheelchairCarAssistance);
                        $("#wheelchair_min_flat_rate_ow").val(data.wheelchairMiniFlatRateOW);
                        $("#wheelchair_min_flat_rate_rt").val(data.wheelchairMiniFlatRateRT);
                        $("#wheelchair_no_show").val(data.wheelchairNoShow);
                        $("#wheelchair_early_pick_up").val(data.wheelchairEarlyPickUp);
                        $("#wheelchair_late_drop_off").val(data.wheelchairLateDropOff);
                        $("#wheelchair_weekend_fee").val(data.wheelchairWeekendFee);
                        
                        $("#stretcher_rate").val(data.stretcherRate);
                        $("#stretcher_lift_base").val(data.stretcherLiftBase);
                        $("#stretcher_waiting_time").val(data.stretcherWaitingTime );
                        $("#stretcher_assistance").val(data.stretcherAssistance);
                        $("#stretcher_min_flat_rate_ow").val(data.stretcherMinFlatRateOW);
                        $("#stretcher_min_flat_rate_rt").val(data.stretcherMinFlatRateRT);
                        $("#stretcher_no_show").val(data.stretcherNoShow);
                        $("#stretcher_early_pick_up").val(data.stretcherEarlyPickUp);
                        $("#stretcher_late_drop_off").val(data.stretcherLateDropOff);
                        $("#stretcher_weekend_fee").val(data.stretcherWeekendFee);
                        
                        
                        $('input[name=for_response]').val([data.commercialInsurance]);
                        $('input[name=for_response]:checked').trigger('change');
                        
                                               
                        $('input[name=status_driver]').val([data.status]);
                        $('input[name=status_driver]:checked').trigger('change');
                       
                       
                        $('#formNew').dialog('open');
                    }
                                                     
                }    
            })
        });
        
        $("table#user_jqGrid").on('click','.trash',function(){
            var id =$(this).data("oid");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){  
                    showLoading = 1;
                    $.ajax({                      
                        url:delDrivers,
                        type:"post",
                        data : {id:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#user_jqGrid").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
        });
        $("table#user_jqGrid").on('click','.active',function(){
            var id=$(this).data("oid");
            confirmBox(confirmActiveMessage,activeSubTitle,function(response){
                if(response){ 
                    showLoading = 1;
                    $.ajax({                      
                        url:activeDrivers,
                        type:"post",
                        data : {id:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                                $("#user_jqGrid").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
        
        });
        
        $('#showcontent').show();

    });

</script>
<div id="showcontent" class="none">
    <div>
        <center>
            <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('CONDUCTORES') ?></div>
        </center>
    </div>

    <div class="show_content">
        <br/> <button type="button" id="newUser" class="btnNew"><?php echo __('Nuevo Conductor') ?></button>

        <div class="btnprueba">
            <input type="checkbox" id="status" name="status"  />
            <label for="status"><?php echo __('Listar Todos') ?></label><br><br><br>
        </div>

        <center>
            <table id="user_jqGrid"></table>
            <div id="user_jqGrid_pager"></div>
        </center>
    </div>


    <!--NEW DRIVER -->

    <div id="formNew">
        <form id="validation" name="validation" action="" method="post">
            <fieldset> 
                <legend><?php echo __('Informacion del Conductor') ?></legend>    
                <input type="hidden" name="idDrivers"  id="idDrivers" value="">
                <ul>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Nombre') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="name" name="name" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Apellido') ?></label></dt>
                            <dd>
                                <input type="text" id="last_name" name="last_name" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Area') ?></label></dt>
                            <dd>
                                <select name="areas_drivers_id" id="areas_drivers_id">
                                    <option value=""><?php echo __('Select ...'); ?></option>                         
                                    <?php foreach ($area_driver as $ad) { ?>
                                        <option value="<?php echo $ad['id'] ?>"><?php echo $ad['area'] ?></option>

                                    <?php } ?>
                                </select> 
                                <button type="button" id="addarea" name="addarea"><?php echo __("Agregar") ?></button>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl" id="services"><?php echo __('Servicios') ?></label></dt>
                            <dd class="buttonsCheck" >
                                <input type="radio" id="AMBULATORY" name="for_drive" value="AMBULATORY" checked=""/><label for="AMBULATORY"><?php echo __("Ambulatory") ?></label>
                                <input type="radio" id="WHEELCHAIR" name="for_drive" value="WHEELCHAIR" /><label for="WHEELCHAIR"><?php echo __("Wheelchair") ?></label>
                                <input type="radio" id="STRETCHER" name="for_drive" value="STRETCHER" /><label for="STRETCHER"><?php echo __("Stretcher") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Direccion') ?></label></dt>
                            <dd>
                                <input type="text" name="address" id="address" />
                            </dd>
                        </dl>         
                        <div class="clear"></div>
                    </li>      

                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Ciudad') ?></label></dt>
                            <dd>
                                <input type="text" name="city" id="city" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Estado') ?></label></dt>
                            <dd>
                                <input type="text" name="state" id="state" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Codigo Zip') ?></label></dt>
                            <dd>
                                <input type="text" name="zip_code"  id="zip_code"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Telefono') ?></label></dt>
                            <dd>
                                <input type="text" name="phone" id="phone" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Fax') ?></label></dt>
                            <dd>
                                <input type="text" name="fax" id="fax" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Celular') ?></label></dt>
                            <dd>
                                <input type="text" name="cell_phone" id="cell_phone" />                
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Marca Carro') ?></label></dt>
                            <dd>
                                <input type="text" name="car_make" id="car_make" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Modelo Carro') ?></label></dt>
                            <dd>
                                <input type="text" name="car_model" id="car_model" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('E-mail') ?></label></dt>
                            <dd>
                                <input type="text" name="email" id="email" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Fecha ') ?></label></dt>
                            <dd>
                                <input type="text" name="insurance_date" id="insurance_date" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Numero de Identificacion') ?></label></dt>
                            <dd>
                                <input type="text" name="identification_number" id="identification_number" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Notas Especiales') ?></label></dt>
                            <dd>
                                <textarea name="special_notes" id="special_notes" cols="27" rows="6" ></textarea>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Seguros Comerciales') ?></label></dt>
                            <dd class="buttonsCheck">

                                <input type="radio" id="chkYes" name="for_response" value="1"><label for="chkYes"><?php echo __("Yes") ?></label>
                                <input type="radio" id="chkNo" name="for_response" value="0" checked="checked"/><label for="chkNo"><?php echo __("No") ?></label>


                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li id="divInsuranceDeduction">
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Deduccion de Seguros') ?></label></dt>
                            <dd>
                                <select name="insurance_deduction" id="insurance_deduction">
                                    <option value=""><?php echo __('Select .....') ?></option>

                                    <option value="<?php echo $driver_deduction_a ?>"><?php echo $driver_deduction_a ?></option>
                                    <option value="<?php echo $driver_deduction_b ?>"><?php echo $driver_deduction_b ?></option>
                                </select> 
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                </ul>        

            </fieldset>
            <br/>
            <fieldset>     
                <legend><?php echo __('Tarifas Generales') ?></legend> 
                <div class = "driver_rate_container">

                    <ul>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Gasoline Surcharge') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="gasoline_surcharge" name="gasoline_surcharge" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                    </ul>
                </div>
            </fieldset> <br/>

            <fieldset>
                <legend><?php echo __('Ambulatory Rates') ?></legend> 
                <div class="rate_column">
                    <ul>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Millas') ?></label>
                                </dt>
                                <dd>

                                    <input type="text" id="ambulatory_rate" name="ambulatory_rate" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Tiempo Espera') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_waiting_time" name="ambulatory_waiting_time" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Min Flat Rate OW') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_min_flat_rate_ow" name="ambulatory_min_flat_rate_ow" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Min Flat Rate RT') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_min_flat_rate_rt" name="ambulatory_min_flat_rate_rt" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>                                                        
                    </ul>
                    <ul>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('No Show') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_no_show" name="ambulatory_no_show" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Early Pick Up') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_early_pick_up" name="ambulatory_early_pick_up" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Late Drop Off') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_late_drop_off" name="ambulatory_late_drop_off" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                <label class="frmlbl"><?php echo __('Weekend Fee ') ?></label>
                                </dt>
                                <dd>
                                    <input type="text" id="ambulatory_weekend_fee" name="ambulatory_weekend_fee" value=""/>
                                </dd>
                            </dl>
                            <div class="clear"></div>
                        </li>
                    </ul>
                </div>
            </fieldset>   
            <br/>

            <fieldset>
                <legend><?php echo __('Wheelchair Rates') ?></legend> 
                <div class="driver_rate_container">     
                    <div class="rate_column">
                        <ul>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Millas') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_rate" name="wheelchair_rate" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Lift Base') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_lift_base" name="wheelchair_lift_base" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Time') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_waiting_time" name="wheelchair_waiting_time" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Car Assistance') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_car_assistance" name="wheelchair_car_assistance" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Min Flat Rate OW') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_min_flat_rate_ow" name="wheelchair_min_flat_rate_ow" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Min Flat Rate RT') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_min_flat_rate_rt" name="wheelchair_min_flat_rate_rt" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>                                                        
                        </ul>
                        <ul>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('No Show') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_no_show" name="wheelchair_no_show" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Early Pick Up') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_early_pick_up" name="wheelchair_early_pick_up" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Late Drop Off') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_late_drop_off" name="wheelchair_late_drop_off" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <dl>
                                    <dt>
                                    <label class="frmlbl"><?php echo __('Weekend Fee') ?></label>
                                    </dt>
                                    <dd>
                                        <input type="text" id="wheelchair_weekend_fee" name="wheelchair_weekend_fee" value=""/>
                                    </dd>
                                </dl>
                                <div class="clear"></div>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>
            </fieldset>           
            <br/>

            <fieldset>
                <legend><?php echo __('Stretcher Rates') ?></legend>  
                <div class="clear"></div>

                <ul>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Millas') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_rate" name="stretcher_rate" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Lift Base') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_lift_base" name="stretcher_lift_base" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Waiting Time') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_waiting_time" name="stretcher_waiting_time" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Assistance') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_assistance" name="stretcher_assistance" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Min Flat Rate OW') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_min_flat_rate_ow" name="stretcher_min_flat_rate_ow" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Min Flat Rate RT') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_min_flat_rate_rt" name="stretcher_min_flat_rate_rt" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>                                                        
                </ul>
                <ul>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('No Show') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_no_show" name="stretcher_no_show" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Early Pick Up') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_early_pick_up" name="stretcher_early_pick_up" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Late Drop Off') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_late_drop_off" name="stretcher_late_drop_off" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Weekend Fee') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="stretcher_weekend_fee" name="stretcher_weekend_fee" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                </ul>



                <div class="clear"></div>

            </fieldset>      
            <br/>

            <fieldset>
                <legend><?php echo __('Terminacion') ?></legend>  
                <ul>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Estado') ?></label></dt>
                            <dd class="buttonsCheck">                                    
                                <input type="radio" id="status_driver_active" name="status_driver" value="1" /><label for="status_driver_active"><?php echo __("Active") ?></label>
                                <input type="radio" id="status_driver_inactive" name="status_driver" value="0" checked="" /><label for="status_driver_inactive"><?php echo __("Inactive") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li id="divstatus">
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Fecha') ?></label>
                            </dt>
                            <dd>                                    
                                <input type="text" id="termination_date" name="termination_date" value=""/>
                            </dd>
                        </dl>
                        <div class="clear"></div>

                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Razon') ?></label>
                            </dt>
                            <dd>
                                <textarea name="termination_reason" id="termination_reason" cols="27" rows="6" ></textarea>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <div class="clear"></div>
            </fieldset>      
        </form>



    </div>
    <div id="dialogo-inactivo">     
        <input type="hidden" name="grid-listinactive" id="grid-listinactive" /> <br/>       
    </div>




    <!-- ********** AREA DRIVER *************** -->
    <div class="d-areadriver">
        <div id="dialogo-areadriver">
            <form method="POST" id="formulario-areadriver" name="formulario-areadriver">
                <fieldset>  
                    <label for="name" ><?php echo __(' Locacion') ?></label><input type="text" name="location_driverarea" id="location_driverarea"/> 
                    <br/>
                    <input type="hidden" name="idDriverarea" id="idDriverarea" value=""/> 
                    <br/>    
                </fieldset>
            </form>   
        </div> 
    </div> 
</div>
