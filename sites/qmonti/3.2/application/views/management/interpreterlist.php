<style type="text/css"> 

    .titleTables{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }
    .type_text_normal{
        width: 150px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;

    }
    #dialog-interprete  textarea{
        padding: 3px 2px 70px 2px;
    }

    #dialog-interprete fieldset{
        padding: 3px 2px 4px 7px;
    }
    .tpe_text{

        width: 40px; /* una longitud definida ancho */
        height:15px;
        padding: 5px; ;
        line-height: 12px;
    }

    .frmlabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 16px;
        padding-top: 5px;
        width: 175px;
    }

    .frmlabel_min{
        line-height: 12px;
        width: 150px;
        font-weight: bold;
        height: 16px;
        width: 150px;
    }

</style>

<script type="text/javascript">
    
    var listInterpreters = '/management/interpreterlist/consults';
    var findContact      = '/management/interpreterlist/getPersonById';
      
    $(document).ready(function(){
        
        $('#languages_available').combobox();
               
        $('#addlanguage').button({
            icons: {
                primary: "ui-icon-plus"
            }, text:false
        });
        
       
        
        $('#dialog-interprete').dialog({
            autoOpen: false,
            title:"Interprete",
            height:400,
            width:400,
            modal: true,
            resizable:false,
            buttons:{
                "Guardar":function(){
                    
                
                },
            
                "Cancelar":function(){
                    $( this ).dialog( "close" );
                }            
            }
        
        });   
        
        
        $('#newInterprete').click(function(){
            $('#dialog-interprete').dialog('open');      
               
        });
        

        
        $('#grid-listinterpreter').jqGrid({
            url:listInterpreters,
            datatype: "json", 
            colNames:['ID','<?php echo __('Fecha Registro'); ?>',
                '<?php echo __('Nombre'); ?>',
                '<?php echo __('Apellido'); ?>',
                '<?php echo __('Telefono'); ?>',
                '<?php echo __('Celular'); ?>',
                '<?php echo __('Correo Electronico'); ?>',
                '<?php echo __('Ciudad'); ?>',
                '<?php echo __('Lenguaje'); ?>',
                '<?php echo __('Registrado por'); ?>',
                '<?php echo __('Acciones'); ?>'
            ], 
            colModel:[ 
                {name:'idInterpreter',index:'idInterpreter',hidden: true},
                {name:'daterecord',index:'daterecord', width:120, align:"center"}, 
                {name:'name',index:'name', width:150, align:"center"},
                {name:'lastname',index:'lastname', width:150, align:"center"},
                {name:'phone',index:'phone', width:100, align:"center"},
                {name:'cellphone',index:'cellphone', width:100, align:"center"}, 
                {name:'email',index:'email', width:200, editable:true, align:"center"},
                {name:'city',index:'city', width:100, editable:true, align:"center"},
                {name:'language',index:'language',width:300, align:"center"},
                {name:'user',index:'user',width:300, align:"center"},
                {name:'actions',index:'actions', width:80, align:"center"}
                               
            ],
            rowNum:100, 
            height:350,
            rowList:[100,200,300,500], 
            pager: 'grid-listinterpreter-paper', 
            sortname: 'name', 
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __('Lista Interprete'); ?>",
            afterInsertRow:function(row_id, data_id){

                edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
               
                $("#grid-listinterpreter").jqGrid('setRowData',row_id,{actions:edit});  
                
            },
            
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-search'},
                    text: false
                })

            }
        });  
      
        //jQuery("#grid-listinterpreter").jqGrid('navGrid',"#grid-listinterpreter-paper",{edit:false,add:false,del:false}); 
        jQuery("#grid-listinterpreter").jqGrid('inlineNav',"#grid-listinterpreter-paper"); 
        
        $("#grid-listinterpreter").on('click','.edit',function(){
            
            
            $("#idInterpreter").val(($(this).data("oid")));
            var obj = new Object();
            obj.idInterpreter = $(this).data("oid");
            showLoading = 1; //para q salga q esta cargando los dx
            $.ajax({
                url: findContact,
                type:"post",
                data:obj,
                dataType:"json",
                success: function(j_response){
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        var datal=j_response.dataLanguages ;
                        
                        $("#name").val(data.name);
                        $("#lastname").val(data.lastname);
                        $("#phone").val(data.phone);
                        $("#cellphone").val(data.cellphone);
                        $("#email").val(data.email);
                        $("#city").val(data.city);
                        $('#address').val(data.address);
                        $('#state').val(data.state);
                        $('#zipcode').val(data.zipcode);
                        $('#fax').val(data.fax);
                        var languages = '';
                        $.each(datal, function(i,d){
                            languages += d.name+'   ';                     
                        });
                        $('#languages').val(languages);
                        //                        $('#languages').val(datal.idLanguage);
                        $( "#dialog-interprete" ).dialog( "open" ); 
                    }
                                                     
                }    
            })
        });
        
    });


</script>


<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA DE INTERPRETES') ?></div>
    </center>
</div>

<center>
    <table id="grid-listinterpreter"></table>
    <div id="grid-listinterpreter-paper"></div>
</center>



<div id="dialog-interprete" class="label_output">
    <form id="frmInterprete" name="frmInterprete" method="post">
        <fieldset>   
            <legend><?php echo __('Crear Interprete') ?></legend>
            <dl> 
                <dd>
                    <label for="name" class="frmlabel"><?php echo __('Nombre') ?></label>
                    <input class="type_text_normal" type="text" id="name" name="name" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="lastname" class="frmlabel"><?php echo __('Apellido') ?></label>
                    <input class="type_text_normal" type="text" id="lastname" name="lastname" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="address" class="frmlabel"><?php echo __('Direccion') ?></label>
                    <input class="type_text_normal" type="text" id="address" name="address" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="city" class="frmlabel"><?php echo __('Ciudad') ?></label>
                    <input class="type_text_normal" type="text" id="city" name="city"  disabled="true"/>
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="state" class="frmlabel"><?php echo __('Estado') ?></label>
                    <input class="type_text_normal" type="text" id="state" name="state" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="zipcode" class="frmlabel"><?php echo __('Codigo Postal') ?></label>
                    <input class="type_text_normal" type="text" id="zipcode" name="zipcode" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="phone" class="frmlabel"><?php echo __('Telefono') ?></label>
                    <input class="type_text_normal" type="text" id="phone" name="phone" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="fax" class="frmlabel"><?php echo __('Fax') ?></label>
                    <input class="type_text_normal" type="text" id="fax" name="fax" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="email" class="frmlabel"><?php echo __('Correo Electronico') ?></label>
                    <input class="type_text_normal" type="text" id="cellphone" name="cellphone" disabled="true" />
                </dd>
            </dl>

            <dl>
                <dd>
                    <label for="languages" class="frmlabel"><?php echo __('Lenguajes Disponibles') ?></label>
                    <input class="type_text_normal" type="text" id="languages" name="languages" disabled="true" />
                </dd>
            </dl>
        </fieldset>    

    </form>    
</div>




