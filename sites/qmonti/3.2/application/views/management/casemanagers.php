<style type="text/css">
    .title{
        font-size: 17px;
        font-weight: bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }
    
    

    #buttons{
        margin-left: 157px;

    }
    #newCasemanagers{
        margin-bottom: 13px;
        margin-left: -119px;
    }

    .btnprueba label{
        float:left;

    }

    dd
    {
        float: left;
        margin-top: 7px;
    }
    label
    {
        text-align: left;
    }
    input
    {
        margin-left: 5px;
    }

    .btnnew{
        margin-left: 80px;
        margin-bottom: 25px;
        float:left;
    }
    .btnprueba{
        display: inline;
    }
    
    #formNew fieldset{
        padding: 1px 1px 7px 8px;
    }
</style>
<script type="text/javascript">

    var url_listCasemanagers = '/management/casemanagers/listCasemanagers';
    var url_getCasemanagers = '/management/casemanagers/getCasemanagers' + jquery_params;
    var url_createUpdateCasemanager = '/management/casemanagers/createUpdateCasemanager' + jquery_params;
    var url_deleteCasemanager = '/management/casemanagers/deleteCasemanager' + jquery_params;
    var url_activeCasemanager = '/management/casemanagers/activeCasemanager' + jquery_params;
    var nameCasemanager = '<?php echo __('Nombre de Administrador de Casos'); ?>';
    var nameProvider = '<?php echo __('Nombre de Proveedor'); ?>';
    var nameAdmin = '<?php echo __('Nombre de Administrador de Casos'); ?>';
    var status = '<? echo __('Estado'); ?>';
    var accions = '<?php echo __('Acciones'); ?>';
    var ListCaseman = '<?php echo __('Lista de Administradores de Casos'); ?>';
    var validName = '<?php echo __('El nombre de administrador de casos es obligatorio'); ?>';
    var validProvider = '<?php echo __('Seleccione un proveedor'); ?>';
    var msg_delete = '<?php echo __('Esta seguro de eliminar este administrador de caso?'); ?>';
    var msg_active = '<?php echo __('Esta seguro de restaurar este administrador de caso?'); ?>';
    var txt_nuevoprov = '<?php echo __('Nuevo Proveedor'); ?>';
    var txt_nuevo_admin = '<?php echo __('Nuevo Administrador de Casos'); ?>';
    var title_delete = '<?php echo __('Eliminar Administrador de Caso'); ?>';
    var title_active = '<?php echo __('Activar Administrador de Caso'); ?>';
    var txt_emailrequests = '<?php echo __('El E-mail es requerido'); ?>';
    var txt_emailincorrecto = '<?php echo __('El E-mail es incorrecto'); ?>';
    var validPhone = '<?php echo __('El Telefono es incorrecto'); ?>';
    var url_createproviders = '/management/casemanagers/saveProviders' + jquery_params;


    $(document).ready(function() {

        /*BUTONS*/
        $('#newCasemanagers').button();
        $('.btnprueba').buttonset();
        $('#addprovider').button();

        /*NEW CASEMANAGER*/

        $('#newCasemanagers').button({
            icons: {
                primary: 'ui-icon-plus'
            }
            //text: false

        });



        /*******************************/
        $('#newCasemanagers').click(function() {
            frmNewCasemanagers.currentForm.reset();
            $('#formNew').dialog('open');

        });

        $('#addprovider').click(function() {
            frmProviders.currentForm.reset();
            $('#addproviders').dialog('open');

        });
        /********************************/




        $('input[name=status]').change(function() {
            $("#gridListCasemanagers").trigger('reloadGrid');
        });

        /*** DIALOGO PARA PROVIDERS ******/
        $('#addproviders').dialog({
            autoOpen: false,
            title: txt_nuevoprov,
            height: 300,
            width: 300,
            buttons: {
                'Guardar': function() {
                    if (!$('#frmProviders').valid()) {
                        return false;
                    }
                    $('#frmProviders').submit();

                },
                'Cancelar': function() {

                    /*Limpiar campos con reset*/
                    frmProviders.currentForm.reset();
                    $(this).dialog('close');
                }

            }


        })

        /*DIALOG NEW CASEMANAGER*/
        $('#formNew').dialog({
            autoOpen: false,
            title: txt_nuevo_admin,
            height: 300,
            width: 500,
            buttons: {
                'Guardar': function() {
                    if (!$('#frmNewCasemanagers').valid()) {
                        return false;
                    }
                    $('#frmNewCasemanagers').submit();

                },
                'Cancelar': function() {

                    /*Limpiar campos con reset*/
                    frmNewCasemanagers.currentForm.reset();
                    $(this).dialog('close');
                }

            }

        });

        /************* VALIDATION DE NEW PROVIDER ******************/
        frmProviders = $('#frmProviders').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true

                }
            },
            messages: {
                name: {
                    required: validName
                },
                email: {
                    required: txt_emailrequests,
                    email: txt_emailincorrecto
                },
                phone: {
                    required: validPhone
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {

                $.ajax({
                    url: url_createproviders,
                    type: "post",
                    data: $("#frmProviders").serializeObject(),
                    dataType: "json",
                    success: function(r) {
                        if (evalResponse(r))
                        {
                            var data = r.data;
                            console.log(data);
                            $('#idProvider').append(
                            $('<option/>').attr('value', data.idProvider).text(data.name)
                        ).val(data.idProvider);

                            $('#idProvider-combobox').val($('#idProvider option:selected').text());

                            if (r.data > 0)
                                $("#gridListCasemanagers").trigger('reloadGrid');
                            $("#addproviders").dialog('close');
                            frmProviders.currentForm.reset();

                            //$("#grid_list_product").trigger("reloadGrid");
                        }

                    }
                });
            }

        });



        /*VALIDATION DE NEW CASEMANAGER*/

        frmNewCasemanagers = $('#frmNewCasemanagers').validate({
            rules: {
                name: {
                    required: true
                },
                r: {
                    required: true
                },
                manager_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: {
                    required: validName
                },
                idProvider: {
                    required: validProvider
                },
                manager_email: {
                    required: txt_emailrequests,
                    email: txt_emailincorrecto
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {

                $.ajax({
                    url: url_createUpdateCasemanager,
                    type: "post",
                    data: $("#frmNewCasemanagers").serializeObject(),
                    dataType: "json",
                    success: function(j_response) {
                        if (evalResponse(j_response))
                        {
                            $("#gridListCasemanagers").trigger('reloadGrid');
                            $("#formNew").dialog('close');
                            frmNewCasemanagers.currentForm.reset();

                        }

                    }
                });
            }

        });

        /* COMBOBOX */
        $('#idProvider').combobox();

        $("#gridListCasemanagers").jqGrid({
            url: url_listCasemanagers,
            mtype: 'POST',
            datatype: 'JSON',
            postData: {
                status: function() {
                    return $('#status').is(':checked');
                }
            },
            colNames: [
                'IdCasemanager',
                nameAdmin,
                'idProvider',
                nameProvider,
                status,
                accions
            ],
            colModel: [
                {index: 'idCasemanager', name: 'idCasemanager', key: true, hidden: true},
                {index: 'cas.name', name: 'cas.name', width: '100px', search: true,align:'center'},
                {index: 'idProvider', name: 'idProvider', hidden: true,align:'center'},
                {index: 'pro.name', name: 'pro.name', width: '100px', search: true,align:'center'},
                {index: 'status', name: 'status', width: '100px', search: false,align:'center'},
                {index: 'actions', name: 'actions', width: '100px', search: false,align:'center'}
            ],
            pager: '#gridListCasemanagersPaper',
            rowNum: 100,
            height: 200,
            width:1125,
            rowlist: [100, 200, 300,500],
            viewrecords: true,
            caption: ListCaseman,
            afterInsertRow: function(row_id, data_id)
            {
                if (data_id.status == 1)
                {
                    edit = "<a class=\"edit\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#gridListCasemanagers").jqGrid('setRowData', row_id, {actions: edit + trash, status: 'activo'});

                }
                else
                {
                    active = "<a class=\"active\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#gridListCasemanagers").jqGrid('setRowData', row_id, {actions: active, status: 'Inactivo'});
                }
            },
            gridComplete: function() {
                //figuras de botones del dialog
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                })

                $('.trash').button({
                    icons: {primary: 'ui-icon-trash'},
                    text: false
                })

                $('.active').button({
                    icons: {primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });

        jQuery("#gridListCasemanagers").jqGrid('navGrid', "#gridListCasemanagersPaper", {edit: false, add: false, del: false});
        jQuery("#gridListCasemanagers").jqGrid('inlineNav', "#gridListCasemanagersPaper");
        $("#gridListCasemanagers").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});

        /* Editar Casemanager*/
        $("#gridListCasemanagers").on('click', '.edit', function() {
            var id = $(this).data('oid');
            $.ajax({
                url: url_getCasemanagers,
                type: "post",
                data: {id: id},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        /* DATOS AL DIALOG */
                        $("#idCasemanagers").val(data.idCasemanager);
                        $("#name").val(data.name);
                        $("#phone").val(data.phone);
                        $("#manager_email").val(data.email);
                        $("#extentionPhone").val(data.extentionphone);
                        $("#fax").val(data.fax);
                        $("#idProvider").val(data.idProvider);
                        $("#idProvider-combobox").val($("#idProvider option:selected").text());
                        $('#formNew').dialog('open');

                    } else {
                        /* ERROR */
                    }

                }
            });
        });


        /* Eliminar Adjuster */
        $("#gridListCasemanagers").on('click', '.trash', function() {
            var id = $(this).data('oid');
            confirmBox(msg_delete, title_delete, function(response) {
                if (response) {
                    $.ajax({
                        url: url_deleteCasemanager,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListCasemanagers").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });

        $("#gridListCasemanagers").on('click', '.active', function() {
            var id = $(this).data('oid');
            confirmBox(msg_active, title_active, function(response) {
                if (response) {
                    $.ajax({
                        url: url_activeCasemanager,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListCasemanagers").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });

    });

</script>
<!-- Codigo html -->
<div>
    <center>
        <div class="title ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('ADMINISTRADORES DE CASOS') ?></div>
    </center>
</div>

<div class="show_content">
    <div id="buttons">
        <button id="newCasemanagers" class="btnnew"><?php echo __('Nuevo Administrador de Casos') ?></button> 
        <div class="btnprueba">
            <input type="checkbox" id="status" name="status" />
            <label for="status"><?php echo __('Listar Todos') ?></label><br><br>
        </div>
    </div>

    <center>
        <div>
            <br><br>

            <table id="gridListCasemanagers"></table>
            <div id="gridListCasemanagersPaper"></div>
        </div>
    </center>
</div>

<div id="formNew">
    <form id="frmNewCasemanagers" name="frmNewCasemanagers" action="" method="post">
        <fieldset>
            <legend><?php echo __('Crear Administrador de Casos') ?></legend>
            <input type="hidden" name="idCasemanagers" id="idCasemanagers">

            <dl>
                <dd>                
                    <label class="frmlbl"><?php echo __('Administrador de Casos') ?></label>
                    <input type="text" name="name_caseM" id="name_caseM">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Proveedor') ?></label>
                    <select name="idProvider" id="idProvider">
                        <option value=""><?php echo __('Seleccionar ...') ?></option>
                        <?php foreach ($provider as $ic) { ?>
                            <option value="<?php echo $ic['idProvider'] ?>"><?php echo $ic['name'] ?></option>
                        <?php }; ?>
                    </select>
                    <button type="button" id="addprovider" name="addprovider"><?php echo __("agregar") ?></button>
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Correo Electronico ') ?></label>
                    <input type="text" name="manager_email" id="manager_email">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Telefono') ?></label>
                    <input type="text" name="phone_caseM" id="phone_caseM">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Extension') ?></label>
                    <input type="text" name="extentionPhone_caseM" id="extentionPhone_caseM">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Fax') ?></label> 
                    <input type="text" name="fax_caseM" id="fax_caseM">
                </dd>

                <!--<img id="addic" src="">-->
            </dl>

        </fieldset>
    </form>
</div>




<div id="addproviders">
    <form id="frmProviders" name="frmProviders" action="" method="post">
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Nombre') ?></label>
                <input type="text" name="name-prov" id="name-prov" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Correo Electronico') ?></label>
                <input type="text" name="email-prov" id="email-prov" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Telefono') ?></label>
                <input type="text" name="telefono-prov" id="telefono-prov" size="30" value="">
            </dd>
        </dl>

    </form>

</div>
