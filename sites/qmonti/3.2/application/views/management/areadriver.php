
<style type="text/css"> 

    .titleTables{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }

    #newareadriver{
        margin-left: 141px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;
    }

    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }
    #dialogo-areadriver input[type="text"]{

        width: 200px; /* una longitud definida ancho */
        height:15px;

        padding: 3px 2px 4px 5px;
        margin: 5px 0;

    }

    select#type{
        width: 200px; /* una longitud definida ancho */
        height:25px;
        padding: 3px 2px 4px 5px;
        margin: 5px 0;
    }

    #dialogo-areadriver fieldset{

        padding: 1px 1px 1px 8px;
        height: 90px;

    }
</style>
<script type="text/javascript">
    
    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro de eliminar el area?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro de activar el area?"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    var saveAreaDriver ="/management/areadriver/save"+jquery_params;
    var listAreaDriver = '/management/areadriver/consults';
    var editAreaDriver = '/management/areadriver/getPersonById'+jquery_params;
    var trashDriverArea = "/management/areadriver/delete"+jquery_params;
    var activeDriverArea = "/management/areadriver/active"+jquery_params;
    
    $(document).ready(function(){
       
        /*Boton chkckbox */
        $('.btnprueba').buttonset();
        
        $('#newareadriver').button({
            icons:{
                primary:"ui-icon-plus"
            }
          
        });
        
        
        /*Validar*/
        $("#formulario-areadriver").validate({
            rules: {  
                location: {required:true} 
                
            },   
            messages: {  
                location:{required:'<?php echo __("Locacion Requerida") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                var formulario= $(form).serialize();
                showLoading = 1;
                $.ajax({
                    url: saveAreaDriver,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        if (evalResponse(j_response)){
                              
                            $("#grid-listarea").trigger('reloadGrid');
                            $("#dialogo-areadriver").dialog('close');
                            
                        }
                    }
                });
            }    
                
        });
        
        /*Dialog para lenguaje new*/
        $("#dialogo-areadriver").dialog({
            autoOpen: false,
            title:"Area Driver",
            height:200,
            width: 250,
            modal: true,
            resizable:false,
            buttons:{
                "Guardar":function(){
                    $("#formulario-areadriver").submit();
                    $("#user_jqGrid").trigger("reloadGrid");
                
                },
            
                "Cancelar":function(){
                    $( this ).dialog( "close" );
                }            
            }
        });
        
        
        /*Boton para nuevo lenguaje*/
        $( "#newareadriver" ).click(function() {
            $("#idAreasdriver").val(0); //edita
            $('#location_driverarea').val('');
            $( "#dialogo-areadriver" ).dialog( "open" );
        });
        
    
        /*jgrid GENERAL*/
        $("#grid-listarea").jqGrid({ 
            url:listAreaDriver, 
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['ID',"<?php echo __("Locacion") ?>",
                "<?php echo __("Status"); ?>","<?php echo __("Acciones"); ?>"], 
            colModel:[ 
                {name:'idAreasdriver',index:'idAreasdriver',hidden: true, width:50}, 
                {name:'location',index:'location',search:true, width:200, align:"center"}, 
                {name:'status',index:'status',width:100,search:false, align:"center"},
                {name:'actions',index:'actions', width:100,search:false, align:"center"}
               
            ],
            rowNum:100, 
            rowList:[100,200,300], 
            pager: 'grid-listarea-paper', 
            sortname: 'status', 
            height: 200,
            width:920,
            
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __("Area Conductores"); ?>",
            afterInsertRow:function(row_id, data_id){
                //console.log(data_id.status); 
                if(data_id.status==1){
                    
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                    $("#grid-listarea").jqGrid('setRowData',row_id,{actions:edit+trash, status: 'Activo'});
                } 
                
                else{
                    console.log(data_id.status)
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#grid-listarea").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            },
                
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.trash').button({
                    icons:{primary: ' ui-icon-power'},
                    text: false
                })
                
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
                    
        }); 
        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function(){
            $("#grid-listarea").trigger("reloadGrid");
        });
        
        
        
        
        jQuery("#grid-listarea").jqGrid('inlineNav',"#grid-listarea-paper"); 
        
        /*Busqueda*/
        $("#grid-listarea").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        
        $("table#grid-listarea").on('click','.edit',function(){
            
            
            var idAreasdriver = $(this).data("oid");
            showLoading = 1;
            $.ajax({
                url: editAreaDriver,
                type:"post",
                data:{idAreasdriver:idAreasdriver},
                dataType:"json",
                success: function(j_response){
                    
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        $("#idAreasdriver").val(data.idAreasdriver)
                        $("#location").val(data.location);
                        $("#dialogo-areadriver").dialog("open"); 
                    }
                }
            })

        }),
        
        $("table#grid-listarea").on('click','.trash',function(){
        
        
            var id=$(this).attr("data-oid");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){     
                    showLoading = 1;
                    $.ajax({                      
                        url:trashDriverArea,
                        type:"post",
                        data : {idAreasdriver:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-listarea").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false

        }),
        
        $("table#grid-listarea").on('click','.active',function(){
        
            var id=$(this).attr("data-oid");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){ 
                    showLoading = 1;
                    $.ajax({                      
                        url:activeDriverArea,
                        type:"post",
                        data : {idAreasdriver:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                                $("#grid-listarea").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
        
        });     
            
    });       
        
</script>
<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA DE AREAS DE CONDUCTORES') ?></div>
    </center>
</div>

<input type="hidden" name="idAreasdriver" id="idAreasdriver" value="0"/> 

<div class="show_content">
    <br/> <button id="newareadriver" ><?php echo __('Nueva Area Conductor') ?></button>
    <div class="btnprueba">
        <input type="checkbox" id="status" name="status"  />
        <label for="status"><?php echo __('Listar Todos') ?></label><br><br>
    </div>
    <div id="dialogo-areadriver">
        <form method="POST" id="formulario-areadriver" name="formulario-areadriver">
            <fieldset>  
                <label for="name" class="frmlbl" ><?php echo __(' Locacion') ?></label>
                <input type="text" name="location" id="location"/> 
            </fieldset>
        </form>   
    </div> 
    <center>  
        <br><br>
        <table id="grid-listarea" ></table>
        <div id="grid-listarea-paper"></div>
    </center>
</div>