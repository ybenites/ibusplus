<style type="text/css">
    .title{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }
    .frmlbl{

        display: block;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 5px;
        width: 120px;
    }
    #frmNew fieldset{
        height: 144px;
        width: 283px;
        padding: 0px 0px 8px 9px;

    }

    #buttons{
        margin-left: 157px;

    }

    #newTypeappointment{
        margin-left: 37px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;
    }

    dd{
        float: left;
        margin-top: 7px;
    }

    #status{

        margin-bottom: 25px;
        float:left;
    }

    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }
    /*        #frmNew fieldset{
            width:288px; ancho
            height:130px;
            padding: 3px 2px 4px 7px;
    
        }*/



</style>
<script type="text/javascript">
    var url_listTypeappointment = '/management/typeappointment/listTypeappointment'
    var url_createUpdateTypeappointment = '/management/typeappointment/createUpdateTypeappointment'+jquery_params;
    var url_getTypeappointment = '/management/typeappointment/getTypeappointment'+jquery_params;
    var url_deleteTypeappointment = '/management/typeappointment/deleteTypeappointment'+jquery_params;
    var url_activeTypeappointmentr = '/management/typeappointment/activeTypeappointment'+jquery_params;
    var listTypeappointment = '<?php echo __('Lista de Tipos de Citas'); ?>';
    var nameTypeappointment = '<?php echo __('Nombre de el Tipo de Cita'); ?>';
    var type = '<?php echo __('Tipo'); ?>'; 
    var actions = '<?php echo __('Acciones'); ?>';
    var status = '<?php echo __('Estado'); ?>';
    var title_delete = '<?php echo __('Eliminar Tipo de Cita'); ?>';
    var msg_delete = '<?php echo __('Esta seguro de eliminar este Tipo De Cita?'); ?>';
    var title_active = '<?php echo __('Activar Tipo De Cita'); ?>';
    var msg_active = '<?php echo __('Esta seguro de activar Este Tipo de Cita?'); ?>';
    var validTypeappointment = '<?php echo __('El nombre de el Tipo de Cita es obligatorio'); ?>';
    var newttitle = '<?php echo __('Crear Nuevo Tipo de Cita'); ?>';

    $(document).ready(function(){

        /*BUTONS*/
        
        $('.btnprueba').buttonset();
        
        /*NEW typeapoinment*/
        $('#newTypeappointment').button().click(function(){
            
            frmNewTypeappointment.currentForm.reset();
            $('#frmNew').dialog('open');
        });
        $('#newTypeappointment').button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
        
        $('#type').combobox();
        
        
        
        //abre dialogo//
        $('#frmNew').dialog({
            autoOpen: false,
            title: newttitle ,
            height: 250,
            width: 320,
            buttons:{
                'Guardar':function(){                
                    //Funcione el Validate 
                    if(!$('#frmNewTypeappointment').valid()){
                        return false;
                    }
                    $('#frmNewTypeappointment').submit();
                },
                'Cancelar':function(){
                    /*Limpiar campos con reset */
                    frmNewTypeappointment.currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });
        
        /********************************/
        
        
        /* VALIDATION DE NEW Typeappoinment */
        frmNewTypeappointment = $('#frmNewTypeappointment').validate({
            rules:{
                name:{
                    required:true
                }

                
            },
            messages:{
                name:{
                    required:validTypeappointment
                }

            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                $.ajax({
                    url: url_createUpdateTypeappointment,
                    type:"post",
                    data:$("#frmNewTypeappointment").serializeObject(),
                    dataType:"json",
                    success: function(r){  
                        if(evalResponse(r))
                        { 
                            
                            $("#gridListTypeappointment").trigger('reloadGrid');
                            $("#frmNew").dialog('close'); 
                            frmNewTypeappointment.currentForm.reset();
                            
                        }

                    }
                });
            }
    
        });
        
        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function(){
            $("#gridListTypeappointment").trigger("reloadGrid");
        });
       
        /* COMBOBOX */
        //        $('#Status').combobox();
 
 
        /* JQGRID Typeappointment */
        $("#gridListTypeappointment").jqGrid({
            url:url_listTypeappointment,  
            datatype: 'JSON', 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:[
                'idTypeappointment',
                nameTypeappointment,
                type,
                status,
                actions
            ],
            colModel:[
                {index:'idTypeappointment', name:'idTypeappointment', key:true, hidden: true},
                {index:'name', name:'name', width:'200px', search:true , searchoptions: {sopt: ['cn']}, align:"center"},
                {index:'type', name:'type', width:'100px', search:true , searchoptions: {sopt: ['cn']}, align:"center"},               
                {index:'status', name:'status', width:'100px', search:false, align:"center"},
                {index:'actions', name:'actions', width:'100px', search:false, align:"center"}
            ],
            pager:'#gridListTypeappointmentpaper',             
            rowNum:100,        
            height: 200,
            width:1125,
            
            viewrecords: true, 
            rowList:[100,200,300],

            caption: listTypeappointment ,
            afterInsertRow:function(row_id, data_id)
            {
                if(data_id.status==1)
                {
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#gridListTypeappointment").jqGrid('setRowData',row_id,{actions:edit+trash,status: 'Activo'});     
                }
                else
                {
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#gridListTypeappointment").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            },
            
            gridComplete:function(){
                //figuras de botones del dialog
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                })
                 
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });
        
        //jQuery("#gridListTypeappointment").jqGrid('navGrid',"#gridListTypeappointmentpaper",{edit:false,add:false,del:false});
        jQuery("#gridListTypeappointment").jqGrid('inlineNav',"#gridListTypeappointmentpaper");
        $("#gridListTypeappointment").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});

        /* Editar Typeappointment*/
        $("#gridListTypeappointment").on('click','.edit',function(){
            var id = $(this).data('oid');
            $.ajax({
                url: url_getTypeappointment,
                type:"post",
                data:{id:id},
                dataType:"json",
                success: function(j_response){  
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        /* DATOS AL DIALOG */
                        $("#idTypeappointment").val(data.idTypeappointment);
                        $("#name").val(data.name);
                        $("#type").val(data.type);
                        $('#frmNew').dialog('open');  

                    }else{
                        /* ERROR */
                    }
                                                     
                }    
            });
        });
        
        /* Eliminar Typeappointment */
        $("#gridListTypeappointment").on('click','.trash',function(){
            var id = $(this).data('oid');
            confirmBox(msg_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:url_deleteTypeappointment,
                        type:"post",
                        data:{id:id},
                        dataType:"json",
                        success: function(response){  
                            if (response.code == code_success)
                            {
                                $("#gridListTypeappointment").trigger("reloadGrid");
                            }else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }   
                    });
                }
                return false;
            });
        });
        
        /* Activar Typeappointment */       
        $("#gridListTypeappointment").on('click','.active',function(){
            var id = $(this).data('oid');
            confirmBox(msg_active,title_active,function(response){
                if(response){                    
                    $.ajax({
                        url: url_activeTypeappointmentr ,
                        type:"post",
                        data:{idTypeappointment:id},
                        dataType:"json",
                        success: function(response){  
                            if (response.code == code_success)
                            {
                                $("#gridListTypeappointment").trigger("reloadGrid");
                            }else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }   
                    });
                }
                return false;
            });
        });
     
    });
</script>
<div>
    <center>
        <div class="title ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA TIPO DE CITAS') ?></div>
        <br>
    </center>
</div>
<div class="show_content">
<button id="newTypeappointment">
    <?php echo __('Nuevo Tipo de Cita') ?>
</button> 

<div class="btnprueba">
    <input type="checkbox" id="status" name="status" />
    <label for="status"><?php echo __('Listar Todos') ?></label><br><br>
</div>

<center>
    <br><br>
    <table id="gridListTypeappointment"></table>
    <div id="gridListTypeappointmentpaper"></div>

</center>
</div>
<div id="frmNew">
    <form id="frmNewTypeappointment" name="frmNewTypeappointment" action="" method="POST">
        <fieldset>
            <legend><? echo __('Crear Tipo Cita') ?></legend>

            <br><br>
            <label class="frmlbl"><? echo __('Nombre') ?></label>
            <input type="text" name="name" id="name">
            <br><br>
            <label class="frmlbl"><?php echo __('Tipo') ?></label>
            <select name="type" id="type">

                <option value="MEDICAL"><?php echo __('Medical') ?></option>
                <option value="LEGAL"><?php echo __('Legal') ?></option>
                <option value="OTHER"><?php echo __('Other') ?></option>
            </select>


                <!--<img id="addic" src="">-->
        </fieldset>
        <input type="text" name="idTypeappointment" id="idTypeappointment" hidden="true"/>
    </form>
</div>
