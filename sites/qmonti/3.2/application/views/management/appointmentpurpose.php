<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<style type="text/css"> 
    .frmlabel {

        display: block;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 5px;
        width: 120px;
    }


    #newAppointmentP{
        margin-left: 38px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;
    }

    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }

    #dialog-AppoP fieldset{
        height: 144px;
        width: 283px;
        padding: 0px 0px 8px 9px;

    }

    select#type{
        width: 145px; /* una longitud definida ancho */
        height:20px;
        margin: 4px 0;
    }


</style>

<script type="text/javascript"> 
    
    var listAppointment='/management/appointmentpurpose/consults';
    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro de desactivar el lenguaje?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro de activar el lenguaje?"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    var saveAppointmentP ="/management/appointmentpurpose/save" + jquery_params;
    var editAppointmentP = "/management/appointmentpurpose/getPersonById"+jquery_params;
    var trashAppointmentP = "/management/appointmentpurpose/delete"+jquery_params;
    var activeAppointmentP = "/management/appointmentpurpose/active"+jquery_params;
    
    $(document).ready(function(){
       
        $('#type').combobox();
       
       
        $('#newAppointmentP').button({
            icons: {
                primary: "ui-icon-plus"
            } 
        });
       
        $('.btnprueba').buttonset();
       
        $( "#newAppointmentP" ).click(function() {
            $( "#dialog-AppoP" ).dialog( "open" );
        });
        $("#dialog-AppoP").dialog({
            autoOpen: false,
            title:'<?php echo __("Motivo de Cita") ?>',
            height: 250,
            width: 320,
            modal: true,
            resizable:false,
            buttons:{
                "Guardar":function(){
                    $("#frmAppointmentP").submit();
                
                },
            
                "Cancelar":function(){
                    $( this ).dialog( "close" );
                }            
            }
        });
       
       
       
        /*Validar*/
        frmAppoiP=$("#frmAppointmentP").validate({
            rules: {  
                name: {required:true},  
                type: {required:true}  
            },   
            messages: {  
                name: {required:'<?php echo __("Nombre Motivo de Cita es Requerido") ?>'},  
                type:{required:'<?php echo __("Tipo del Motivo de Cita es Requerido") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                var formulario= $(form).serialize();
                showLoading = 1;
                $.ajax({
                    url: saveAppointmentP,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        if (evalResponse(j_response)){
                            var data=j_response.data;
                            console.log(data);  
                            $("#grid-appointmentpurpose").trigger('reloadGrid');
                            $("#dialog-AppoP").dialog('close');
                            frmAppoiP.currentForm.reset();
                                
                            
                        }
                    }
                });
            }    
                
        });
       
       
       
       
        /*jgrid GENERAL*/
        $("#grid-appointmentpurpose").jqGrid({ 
            url:listAppointment, 
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['Id','<?php echo __('Nombre'); ?>','<?php echo __('Tipo'); ?>','<?php echo __('Estado'); ?>','<?php echo __('Acciones'); ?>'], 
            colModel:[ 
                {name:'idAppointmentpurpose',index:'idAppointmentpurpose',hidden: true}, 
                {name:'name',index:'name',search:true, width:300, align:"center"}, 
                {name:'type',index:'type', width:300,search:false, editable:true, align:"center"},
                {name:'status',index:'status',width:100,search:false, align:"center"},
                {name:'actions',index:'actions', width:200,search:false, align:"center"}
               
            ],
            rowNum:10, 
            rowList:[10,20,30], 
            pager: 'grid-appointmentpurpose-paper', 
            sortname: 'status', 
            height: 200,
            width:1125,
            viewrecords: true, 
            sortorder: "desc", 
            caption: '<?php echo __("Lista de Conductores") ?>',
            afterInsertRow:function(row_id, data_id){
                if(data_id.status==1){
                    
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                    $("#grid-appointmentpurpose").jqGrid('setRowData',row_id,{actions:edit+trash, status: 'Activo'});
                } 
                
                else{
                 
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#grid-appointmentpurpose").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            },
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.trash').button({
                    icons:{primary: ' ui-icon-power'},
                    text: false
                })
                
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });
        
        
        $('input[name=status]').change(function(){
            $("#grid-appointmentpurpose").trigger("reloadGrid");
        });
        
        jQuery("#grid-appointmentpurpose").jqGrid('inlineNav',"#grid-appointmentpurpose-paper"); 
        
        /*Busqueda*/
        $("#grid-appointmentpurpose").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        
        $("table#grid-appointmentpurpose").on('click','.edit',function(){
        
            $("#idAppointmentpurpose").val(($(this).data("oid")));
            var obj = new Object();
            obj.idAppointmentpurpose = $(this).data("oid");
            showLoading = 1; 
            $.ajax({
                url: editAppointmentP,
                type:"post",
                data:obj,
                dataType:"json",
                success: function(j_response){
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        $("#name").val(data.name);
                        $("#type").val(data.type);
                        $( "#dialog-AppoP" ).dialog( "open" ); 
                    }
                                                     
                }    
            })
        }),
        
        $("table#grid-appointmentpurpose").on('click','.trash',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:trashAppointmentP,
                        type:"post",
                        data : {idAppointmentpurpose:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-appointmentpurpose").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
        }),
        
        $("table#grid-appointmentpurpose").on('click','.active',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmActiveMessage,activeSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:activeAppointmentP,
                        type:"post",
                        data : {idAppointmentpurpose:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                                $("#grid-appointmentpurpose").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
        
        });     
    
    });

</script>

<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('MOTIVO DE CITAS') ?></div>
        
    </center>
</div>

<div class="show_content">
    
<button id="newAppointmentP" ><?php echo __('Nuevo Motivo Cita') ?></button> 


<div class="btnprueba">

    <input type="checkbox" id="status" name="status" />
    <label for="status"><?php echo __('Listar Todos') ?></label><br><br><br>
</div>


<div id="dialog-AppoP">
    <form method="POST" id="frmAppointmentP">
        <fieldset> 
            <legend><?php echo __('Crear Motivo Cita') ?></legend>

            <br><br>
            <label class="frmlabel" for="name" ><?php echo __('Nombre') ?></label>
            <input type="text" name="name" id="name"/>
            <br><br>

            <label class="frmlabel" for="type"><?php echo __('Tipo') ?></label>
            <select id="type" name="type">
                <option value="MEDICAL"><?php echo __('MEDICAL') ?></option>
                <option value="OTHER"><?php echo __('OTHER') ?></option>
                <option value="RARE"><?php echo __('RARE') ?></option>
            </select> 

            <input type="hidden" name="idAppointmentpurpose" id="idAppointmentpurpose" value="0"/> <br/>    
        </fieldset>
    </form>
</div>
    </div>


<center>  
    <br>
    <table id="grid-appointmentpurpose" ></table>
    <div id="grid-appointmentpurpose-paper"></div>
</center>
