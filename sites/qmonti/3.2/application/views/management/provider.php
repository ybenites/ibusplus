<style type="text/css">

    .title{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;     
    }

    #buttons{
        margin-left: 157px;

    }

    #newProvider{
        margin-bottom: 13px;
        margin-left: 0px;
    }

    dd{
        float: left;
        margin-top: 7px;
    }

    label
    {
        text-align: left;
    }

    input{
        margin-left: 5px;
    }

/*    #status{
        margin-bottom: 25px;
        float:left;
    }*/

    .btnnew{
        margin-left: 80px;
        margin-bottom: 25px;
        float:left;
    }

    .btnprueba{display: inline;}
    #frmNew fieldset{

        padding: 1px 1px 1px 8px;
        height: 163px;
    }

</style>
<script type="text/javascript">
    
    var url_listProvider = '/management/provider/listProvider'
    var url_getProvider = '/management/provider/getProvider'+jquery_params;
    var url_createUpdateProvider = '/management/provider/createUpdateProvider'+jquery_params;
    var url_deleteProvider = '/management/provider/deleteProvider'+jquery_params;
    var url_activeProvider = '/management/provider/activeProvider'+jquery_params;
    var nameProvider = '<?php echo __('Nombre'); ?>';
    var newProvider = '<?php echo __('Nuevo Proveedor'); ?>';
    var listProvider = '<?php echo __('Lista de Proveedores'); ?>';
    var phone = '<?php echo __('Telefono'); ?>';
    var email = '<?php echo __('Correo Electronico'); ?>';
    var actions = '<?php echo __('Acciones'); ?>';
    var validProvider = '<?php echo __('El nombre del proveedor es obligatorio'); ?>';
    var validStatus = '<?php echo __('Seleccione un Estado'); ?>';
    var msg_delete = '<?php echo __('Esta seguro de eliminar este Proveedor?'); ?>';
    var title_active = '<?php echo __('Activar Proveedor'); ?>';
    var msg_active = '<?php echo __('Esta seguro de restaurar Proveedor?'); ?>';
    var title_delete = '<?php echo __('Eliminar Proveedor'); ?>';
    var validName = '<?php echo __('El nombre de ajustador es obligatorio'); ?>';
    var txt_emailrequests = '<?php echo __('El E-mail es requerido.'); ?>';
    var txt_emailincorret = '<?php echo __('El E-mail es incorrecto.'); ?>';
                  
    
    
    $(document).ready(function(){
        
        /*BUTONS*/
        
        $('.btnprueba').buttonset();
        
        /*NEW PROVIDER*/
        $('#newProvider').button().click(function(){
            frmNewProvider.currentForm.reset();
            $('#frmNew').dialog('open');
        });
        
        $('#newProvider').button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
        
        //abre dialogo//
        $('#frmNew').dialog({
            autoOpen: false,
            title: newProvider ,
            height: 300,
            width: 500,
            buttons:{
                'Guardar':function(){                
                    //Funcione el Validate 
                    if(!$('#frmNewProvider').valid()){
                        return false;
                    }
                    $('#frmNewProvider').submit();
                },
                'Cancelar':function(){
                    /*Limpiar campos con reset */
                    frmNewProvider.currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });
        
        /********************************/
        
        
        /* VALIDATION DE NEW Provider */
        frmNewProvider = $('#frmNewProvider').validate({
            rules:{
                name:{
                    required:true
                },
                //idProvider:{
                //    required:true
                //},
                provider_email:{
                    required: true,
                    email:true
                }
                
            },
            messages:{
                name:{
                    required:validProvider
                },
                //   idProvider:{
                //       required:validProvider
                //   },
                provider_email: {
                    required: txt_emailrequests,
                    email:txt_emailincorret
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                $.ajax({
                    url: url_createUpdateProvider,
                    type:"post",
                    data:$("#frmNewProvider").serializeObject(),
                    dataType:"json",
                    success: function(r){  
                        if(evalResponse(r))
                        { 
                            
                            $("#gridListProvider").trigger('reloadGrid');
                            $("#frmNew").dialog('close'); 
                            frmNewProvider.currentForm.reset();
                            
                        }

                    }
                });
            }
    
        });
        
        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function(){
            $("#gridListProvider").trigger("reloadGrid");
        });
       
        /* COMBOBOX */
        
        //                $('#status').combobox();
        
        /* JQGRID LIST Provider */
        $("#gridListProvider").jqGrid({
            url:url_listProvider,  
            datatype: 'JSON', 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:[
                'IdProvider',
                nameProvider,
                phone,
                'status',
                actions
            ],
            colModel:[
                {index:'idProvider', name:'idProvider', key:true, hidden: true},
                {index:'name', name:'name', width:'250px', search:true,align:'center'},
                {index:'phone', name:'phone',width:'100px',align:'center'},
                {index:'status', name:'status', width:'100px', search:false,align:'center'},
                {index:'actions', name:'actions', width:'80px', search:false,align:'center'}
            ],
            pager:'#gridListProviderpaper',             
            rowNum:100, 
            height: 200,
            width:1200,
            viewrecords: true, 
            rowList:[100,200,300,500],
            caption: listProvider ,
            afterInsertRow:function(row_id, data_id)
            {
                if(data_id.status==1)
                {
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#gridListProvider").jqGrid('setRowData',row_id,{actions:edit+trash,status: 'Activo'});     
                }
                else
                {
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#gridListProvider").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            },
            
            gridComplete:function(){
                //figuras de botones del dialog
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                
                $('.trash').button({
                    icons:{primary: 'ui-icon-trash'},
                    text: false
                })
                 
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });
        //jQuery("#gridListProvider").jqGrid('navGrid',"#gridListProviderpaper",{edit:false,add:false,del:false});
        jQuery("#gridListProvider").jqGrid('inlineNav',"#gridListProviderpaper");
       
        $("#gridListProvider").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        /* Editar Provider*/
        $("#gridListProvider").on('click','.edit',function(){
            var id = $(this).data('oid');
            $.ajax({
                url: url_getProvider,
                type:"post",
                data:{id:id},
                dataType:"json",
                success: function(j_response){  
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        /* DATOS AL DIALOG */
                        $("#idProvider").val(data.idProvider);
                        $("#name").val(data.name);
                        $("#provider_email").val(data.email);
                        $("#phone").val(data.phone);
                        $('#frmNew').dialog('open');  

                    }else{
                        /* ERROR */
                    }
                                                     
                }    
            });
        });

        $("#gridListProvider").on('click','.trash',function(){
            var id = $(this).data('oid');
            confirmBox(msg_delete,title_delete,function(response){
                if(response){                    
                    $.ajax({
                        url:url_deleteProvider,
                        type:"post",
                        data:{id:id},
                        dataType:"json",
                        success: function(response){  
                            if (response.code == code_success)
                            {
                                $("#gridListProvider").trigger("reloadGrid");
                            }else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }   
                    });
                }
                return false;
            });
        });

        $("#gridListProvider").on('click','.active',function(){
            var id = $(this).data('oid');
            confirmBox(msg_active,title_active,function(response){
                if(response){                    
                    $.ajax({
                        url: url_activeProvider ,
                        type:"post",
                        data:{idProvider:id},
                        dataType:"json",
                        success: function(response){  
                            if (response.code == code_success)
                            {
                                $("#gridListProvider").trigger("reloadGrid");
                            }else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }   
                    });
                }
                return false;
            });
        });
        
    });
    
    
</script>
<!--codigo html-->
<div>
    <center>
        <div class="title ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA PROVEEDORES') ?></div>
    </center>
</div>
<div class="show_content">
    <center> <button id="newProvider" class="btnnew"><?php echo __('Nuevo Proveedor') ?></button> </center>
    <div class="btnprueba">
        <input type="checkbox" id="status" name="status" />
        <label for="status"><?php echo __('Listar Todos') ?></label><br><br>
    </div>

    <center>

        <table id="gridListProvider"></table>
        <div id="gridListProviderpaper"></div>

    </center>

    <div id="frmNew">
        <form id="frmNewProvider" name="frmNewProvider" action="" method="POST">
            <fieldset>
                <legend><? echo __('Crear Proveedor') ?></legend>
                <input type="text" name="idProvider" id="idProvider" hidden="true"/>
                <dl>
                    <dd>
                        <label class="frmlbl"><? echo __('Proveedor') ?></label>
                        <input type="text" name="name" id="name" size="30" value="">
                    </dd>
                </dl>
                <dl>
                    <dd>
                        <label class="frmlbl"><?php echo __('Correo') ?></label>
                        <input type="text" name="provider_email" id="provider_email" size="30" value="">
                    </dd>
                </dl>
                <dl>
                    <dd>
                        <label class="frmlbl"><?php echo __('Telefono') ?></label>
                        <input type="text" name="phone" id="phone" size="30" value="">
                    </dd>
                </dl>
                <dl>
                    <dd>
                        <label class="frmlbl"><?php echo __('Estado') ?></label>
                        <select name="status" id="status">

                            <option value="1"><?php echo __('Activo') ?></option>
                            <option value="0"><?php echo __('Desactivo') ?></option>
                        </select>
                    </dd>
                </dl>
            </fieldset>
        </form>
    </div>
</div>

