<style type="text/css"> 

    .titleTables{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }

    #newInterprete{
        margin-left: 3px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }

    .frmlabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 5px;
        width: 175px;
    }

    .type_text_normal{

        width: 140px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;

    }

    #dialog-interprete  textarea{
        padding: 3px 2px 70px 2px;

    }

    #dialog-interprete fieldset{
        padding: 3px 2px 4px 7px;
    }


    .tpe_text{

        width: 50px; /* una longitud definida ancho */
        height:15px;
        padding: 5px; ;
        line-height: 12px;
        float:right;
    }

    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }

    .frmlabel_min{

        line-height: 12px;
        width: 150px;
        float:left;
        font-weight: bold;
        height: 15px;

    }
    hr.hr_separate {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        background: none repeat scroll 0 0 transparent;
        border-color: #C5C5C5 -moz-use-text-color -moz-use-text-color;
        border-image: none;
        border-right: medium none;
        border-style: dashed none none;
        border-width: 2px medium medium;
        opacity: 0.55;
        margin:3px;
    }
    .ui-widget-content {
        background: none repeat scroll 0 0 #FCFDFD;
        border: 1px solid #A6C9E2;
        color: #222222;
        margin:3px;
    }



</style>

<script type="text/javascript">
    var listInterpreters = '/management/interpreters/consults';
    var editInterpreter = "/management/interpreters/getPersonById"+jquery_params;
    var saveInterpreter = "/management/interpreters/save" + jquery_params;
    var trashInterpreter = "/management/interpreters/delete"+jquery_params;
    var activeInterpreter = "/management/interpreters/active"+jquery_params;
    var saveLanguageInterpreter = "/management/interpreters/saveLanguage" + jquery_params;
    var deleteSubTitle = "<?php echo __("Desactivado"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro de eliminar el interprete?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro de activar el interpreter?"); ?>';
    var activeSubTitle = "<?php echo __("Activado"); ?>";

      
    $(document).ready(function(){
        
        $('#languages_available').combobox();
        $('.btnLanguageSelect').buttonset();
        $('.btnprueba').buttonset();
        
        /*Muestre dx con activo e inactivo*/
        $('input[name=status]').change(function(){
            $('#interstatus').hide();
            if($(this).val() == '0')
            {
                $('#interstatus').show();
            }
            
        })
        /*check de lenguajes*/
        $('.buttonsCheck').buttonset();
        
        $('#terminationdate').datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth: true,
            changeYear: true 
        });
        
        $('#addlanguage').button({
            icons: {
                primary: "ui-icon-plus"
            }
        });
        
        $('#newInterprete').button({
            icons:{
                primary:'ui-icon-plus'
            }
            
        });
        
        /*Dialog para elegir lenguaje*/
        $('#addnewLanguage').dialog({
            autoOpen:false,
            title:'Lenguajes',
            height:200,
            width:200,
            modal:true,
            resizable:false,
            buttons:{
                'Guardar':function(){
                    $("#newLanguageInter").submit();
                },
            
                'Cancelar':function(){
                    $(this).dialog('close');
                }
            }
        });
        
        
        $('#dialog-interprete').dialog({
            autoOpen: false,
            title:"Interprete",
            height:750,
            width:450,
            modal: true,
            resizable:false,
            close:function(){
                $('#frmInterprete').get(0).reset();
                $('#frmInterprete input[type=hidden]').val('');
            },
            buttons:{
                "Guardar":function(){
                    $("#frmInterprete").submit();
                
                },
                "Cancelar":function(){
                    $( this ).dialog( "close" );
                }            
            }
        
        });   
        
        $('#newInterprete').click(function(){
            $('#idInterpreter').val(0);
            $('#dialog-interprete').dialog('open');      
               
        });
        
        /*agrega NUEVO LENGUAJE*/
        frmNewLang = $("#newLanguageInter").validate({
            rules: {  
                nameLanguage: {required:true}
            },   
            messages: {  
                nameLanguage: {required:'<?php echo __("Nombre Lenguaje Requerido") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){
                var formulario= $("#newLanguageInter").serialize();
                showLoading = 1;
                $.ajax({
                    url: saveLanguageInterpreter,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        var data=j_response.data;
                        console.log(data);
                        if (evalResponse(j_response)){
                            
                            $("#grid-listinterpreter").trigger('reloadGrid');
                            $("#addnewLanguage").dialog('close');
                            
                            checkbox = '<input checked name="selecLanguage[]" id="selecLanguage'+data.idLanguage+'"type="checkbox" value="'+data.idLanguage+'"/><label for="selecLanguage'+data.idLanguage+'">'+data.nameLanguage+'</label>';
                            console.log(checkbox);
                            
                            $(".btnLanguageSelect").append(checkbox);
                            //console.log($("#.btnLanguageSelect").append(checkbox));
                              
                            $('.btnLanguageSelect').buttonset();
                        }
                    }
                });
            }    
                
        });
        
        
        $('#addlanguage').click(function(){
            $('#addnewLanguage').dialog('open');
        });

        /*valida y guarda INTERPRETE*/
        frmInterpre =   $("#frmInterprete").validate({
            rules: {  
                name: {required:true},  
                lastname: {required:true},
                phone: {required:true},
                cellphone: {required:true},
                email: {required:true, email:true},
                city: {required:true}
            },   
            messages: {  
                name: {required:'<?php echo __("Nombre Interprete Requerido") ?>'},  
                lastname: {required:'<?php echo __("Apellido Interprete Requerido") ?>'},  
                phone: {required:'<?php echo __("Telefono Interprete Requerido") ?>'},  
                cellphone: {required:'<?php echo __("Celular Interprete Requerido") ?>'},  
                email: {required:'<?php echo __("Correo Electronico Interprete Requerido") ?>'},  
                city: {required:'<?php echo __("Ciudad Interprete Requerido") ?>'}
                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(form){
                var formulario= $(form).serialize();
                showLoading = 1; 
                $.ajax({
                    url: saveInterpreter,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        if (evalResponse(j_response)){
                            
                            
                            $("#dialog-interprete").dialog('close');
                            $("#grid-listinterpreter").trigger('reloadGrid');
                            frmInterpre.currentForm.reset();
                            $(frmInterpre.currentForm).find('input:checkbox').val([]);
                            
                        }
                    }
                });
            }    
                
        });
        
        $('#grid-listinterpreter').jqGrid({
            url:listInterpreters,
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['Id',"<?php echo __("Nombre"); ?>","<?php echo __("Apellido"); ?>","<?php echo __("Telefono"); ?>","<?php echo __("Fecha Creacion"); ?>","<?php echo __("Celular"); ?>","<?php echo __("Correo Electonico"); ?>","<?php echo __("Ciudad"); ?>","<?php echo __("Lenguajes"); ?>","<?php echo __("Estado"); ?>","<?php echo __("Registrado por"); ?>","<?php echo __("Acciones"); ?>"], 
            colModel:[ 
                {name:'idInterpreter',index:'idInterpreter', hidden: true}, 
                {name:'name',index:'name',search:true, width:150,align:"center",frozen : true}, 
                {name:'lastname',index:'lastname',search:true, width:150,align:"center",frozen : true}, 
                {name:'phone',index:'phone',search:false, width:100,align:"center"}, 
                {name:'creationDate',index:'creationDate',search:false, width:120,align:"center"}, 
                {name:'cellphone',index:'cellphone',search:false, width:100,align:"center"}, 
                {name:'email',index:'email',search:false, width:150,align:"center"},
                {name:'city',index:'city', width:100,search:true, editable:true,align:"center"},
                {name:'language',index:'language', width:300, search:true,editable:true,align:"center"},
                {name:'status',index:'status',width:80,search:true,align:"center"},
                {name:'user',index:'user',width:80,search:true,align:"center"},
                {name:'actions',index:'actions', width:150, search:false,align:"center"}
               
            ],
            rowNum:100, 
            rowList:[100,200,300,500], 
            pager: '#grid-listinterpreter-paper', 
            sortname: 'name', 
            height: 200,
            width:1200,
            shrinkToFit:false,
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __("Interpretes"); ?>",
            afterInsertRow:function(row_id, data_id){
                if(data_id.status==1){
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                    $("#grid-listinterpreter").jqGrid('setRowData',row_id,{actions:edit+trash,status: 'Activo'});  
                }
                else{
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#grid-listinterpreter").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            
            },
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.trash').button({
                    icons:{primary: ' ui-icon-power'},
                    text: false
                })
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });  
        
        //si hace el checked de los activos, recarga la pagina
        $('.btnprueba input[name=status]').change(function(){
            $("#grid-listinterpreter").trigger("reloadGrid");
        });
      
        //jQuery("#grid-listinterpreter").jqGrid('navGrid',"#grid-listinterpreter-paper",{edit:false,add:false,del:false}); 
        jQuery("#grid-listinterpreter").jqGrid('inlineNav',"#grid-listinterpreter-paper"); 
        
        /*Busqueda*/
        $("#grid-listinterpreter").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        
        $("table#grid-listinterpreter").on('click','.edit',function(){
        
            $("#idInterpreter").val(($(this).data("oid")));
            var obj = new Object();
            obj.idInterpreter = $(this).data("oid");
            showLoading = 1; //para q salga q esta cargando los dx
            $.ajax({
                url: editInterpreter,
                type:"post",
                data:obj,
                dataType:"json",
                success: function(j_response){
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        
                        $("#name").val(data.name);
                        $("#lastname").val(data.lastname);
                        $("#phone").val(data.phone);
                        $("#cellphone").val(data.cellphone);
                        $("#email").val(data.email);
                        $("#city").val(data.city);
                        $('#address').val(data.address);
                        $('#state').val(data.state);
                        $('#zipcode').val(data.zipcode);
                        $('#fax').val(data.fax);
                        $('#identificationnumber').val(data.identificationnumber);
                        $('#interpreternotes').val(data.interpreternotes);
                        $('#noshowmedicalregular').val(data.noshowmedicalregular);
                        $('#noshowmedicalregular_mintime').val(data.noshowmedicalregularmintime);
                        $('#noshowmedicalrare').val(data.noshowmedicalrare);
                        $('#noshowmedicalrare_mintime').val(data.noshowmedicalraremintime);
                        $('#noshowmedicalother').val(data.noshowmedicalother);
                        $('#noshowmedicalother_mintime').val(data.noshowmedicalothermintime);
                        $('#noshowlegalregular').val(data.noshowlegalregular);
                        $('#noshowlegalregular_mintime').val(data.noshowlegalregularmintime);
                        $('#noshowlegalrare').val(data.noshowlegalrare);
                        $('#noshowlegalrare_mintime').val(data.noshowlegalraremintime);
                        $('#noshowlegalother').val(data.noshowlegalother);
                        $('#noshowlegalother_mintime').val(data.noshowlegalothermintime);
                        $('#intmedicalregular').val(data.intmedicalregular);
                        $('#intmedicalrare').val(data.intmedicalrare);
                        $('#intmedicalother').val(data.intmedicalother);
                        $('#intlegalregular').val(data.intlegalregular);
                        $('#intlegalrare').val(data.intlegalrare);
                        $('#intlegalother').val(data.intlegalother);
                        $('#minintmedical').val(data.minintmedical);
                        $('#minintlegal').val(data.minintlegal);
                        $('#interpretingconferencecall').val(data.interpretingconferencecall);
                        $('#minintconferencecall').val(data.minintconferencecall);
                        $('#translationperpage').val(data.translationperpage);
                        $('#translationperword').val(data.translationperword);
                        $('#mileagerate').val(data.mileagerate);
                        $('#traveltime').val(data.traveltime);
                        $('#license').val(data.license);
                        var totalId;
                        var objlang;
                        $('input[name="selecLanguage[]"]').each(function() {
                            totalId =  $(this).val();
                            objlang = $(this);
                            objlang.val(totalId).attr('checked',false);
                            $(".btnLanguageSelect").buttonset("refresh");
                            $.each(j_response.dataLanguages, function ()
                            {
                                if(totalId==this.id_lan)
                                {
                                    console.log('unicos',this.id_lan);                                               
                                    objlang.val(this.id_lan).attr('checked',true);
                                    $(".btnLanguageSelect").buttonset("refresh");
                                }
                                                
                            });
                        });

                        $('form#frmInterprete input[name="status"]').val([data.status]);
                        $('form#frmInterprete input[name=status]:checked').trigger('change');
                        $('#terminationdate').val(data.terminationdate);
                        $('#terminationreason').val(data.terminationreason);
                        $( "#dialog-interprete" ).dialog( "open" ); 
                    }
                                                     
                }    
            })
        })
        
        $("table#grid-listinterpreter").on('click','.trash',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:trashInterpreter,
                        type:"post",
                        data : {idInterpreter:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-listinterpreter").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
            
        }),
        
        
        $("table#grid-listinterpreter").on('click','.active',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmActiveMessage,activeSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:activeInterpreter,
                        type:"post",
                        data : {idInterpreter:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                                $("#grid-listinterpreter").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
        
        });     
        
        
    });


</script>
<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA INTERPRETES') ?></div>
    </center>
</div>
<div class="show_content">
    <br/> <button id="newInterprete" ><?php echo __('Nuevo Interprete') ?></button>

    <div class="btnprueba">
        <input type="checkbox" id="status" name="status" />
        <label for="status"><?php echo __('Listar Todos') ?></label><br><br><br>
    </div>
    <center>     
        <br>
        <table id="grid-listinterpreter"></table>
        <div id="grid-listinterpreter-paper"></div>
    </center>
</div>
<div id="dialog-interprete" class="none">
    <form id="frmInterprete" name="frmInterprete" method="post">
        <div> 
            <fieldset>   
                <legend><?php echo __('Crear Interprete') ?></legend>
                <ul>
                    <dl> 
                        <dd>
                            <label for="name" class="frmlabel"><?php echo __('Nombre'); ?></label>
                            <input class="type_text_normal" type="text" id="name" name="name"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="lastname" class="frmlabel"><?php echo __('Apellido'); ?></label>
                            <input class="type_text_normal" type="text" id="lastname" name="lastname" />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="address" class="frmlabel"><?php echo __('Direccion'); ?></label>
                            <input class="type_text_normal" type="text" id="address" name="address"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="city" class="frmlabel"><?php echo __('Ciudad'); ?></label>
                            <input class="type_text_normal" type="text" id="city" name="city"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="state" class="frmlabel"><?php echo __('Estado') ?></label>
                            <input class="type_text_normal" type="text" id="state" name="state"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="zipcode" class="frmlabel"><?php echo __('Codigo ZIP') ?></label>
                            <input class="type_text_normal" type="text" id="zipcode" name="zipcode"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="phone" class="frmlabel"><?php echo __('Telefono') ?></label>
                            <input class="type_text_normal" type="text" id="phone" name="phone"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="fax" class="frmlabel"><?php echo __('Fax') ?></label>
                            <input class="type_text_normal" type="text" id="fax" name="fax"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="cellphone" class="frmlabel"><?php echo __('Celular') ?></label>
                            <input class="type_text_normal" type="text" id="cellphone" name="cellphone"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="email" class="frmlabel"><?php echo __('Correo Electronico') ?></label>
                            <input class="type_text_normal" type="text" id="email" name="email"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="identificationnumber" class="frmlabel"><?php echo __('Numero Identificacion') ?></label>
                            <input class="type_text_normal" type="text" id="identificationnumber" name="identificationnumber"  />
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="interpreternotes" class="frmlabel"><?php echo __('Numero Interprete') ?></label>
                            <textarea class="type_text_normal" id="interpreternotes" name="interpreternotes"  cols="27" rows="6"></textarea>
                        </dd>
                    </dl>

                    <dl>
                        <dd>
                            <label for="languages_available" class="frmlabel"><?php echo __('Lenguajes Disponibles') ?></label>

                            <button style="margin:0 3px" type="button" id="addlanguage" name="addlanguage"><?php echo __("Agregar") ?></button>

                            <div class="btnLanguageSelect">
                                <?php foreach ($language as $la) : ?>
                                    <input  type="checkbox" id="selecLanguage<?php echo $la['idLanguage'] ?>" name="selecLanguage[]"  value="<?php echo $la['idLanguage'] ?>"/>      
                                    <label for="selecLanguage<?php echo $la['idLanguage'] ?>"> <?php echo __($la['name']) ?></label>    
                                <?php endforeach ?>

                                </ul>    
                                </fieldset> 

                                <fieldset>
                                    <legend><?php echo __('Tarifas') ?></legend>

                                    <dl> 
                                        <dd>
                                            <label for="noshowmedicalregular" class="frmlabel"><?php echo __('No Show Medical Regular ') ?></label>
                                            <input class="type_text_normal" type="text" id="noshowmedicalregular" name="noshowmedicalregular" id="noshowmedicalregular" />
                                        </dd>

                                    </dl> 
                                    <dl>
                                        <dd>
                                            <label for="noshowmedicalregular_mintime" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowmedicalregular_mintime" id="noshowmedicalregular_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl> 
                                        <dd>
                                            <label for="noshowmedicalrare" class="frmlabel"><?php echo __('No Show Medical Rare') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowmedicalrare" id="noshowmedicalrare" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="noshowmedicalrare_mintime" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input  class="type_text_normal" type="text" name="noshowmedicalrare_mintime" id="noshowmedicalrare_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr>
                                        </dd>
                                    </dl>

                                    <dl> 
                                        <dd>
                                            <label for="noshowmedicalother" class="frmlabel"><?php echo __('No Show Medical Other ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowmedicalother" id="noshowmedicalother" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="noshowmedicalother_mintime" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowmedicalother_mintime" id="noshowmedicalother_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl> 
                                        <dd>
                                            <label for="noshowlegalregular" class="frmlabel"><?php echo __('No Show Legal Regular ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowlegalregular" id="noshowlegalregular" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="noshowlegalregular_mintime" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowlegalregular_mintime" id="noshowlegalregular_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl> 
                                        <dd>
                                            <label for="noshowlegalrare" class="frmlabel"><?php echo __('No Show Legal Rare ') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowlegalrare" id="noshowlegalrare" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="noshowlegalrare_mintime" class="frmlabel"><?php echo __('Minimum Time') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowlegalrare_mintime" id="noshowlegalrare_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr>
                                        </dd>
                                    </dl>
                                    <dl> 
                                        <dd>
                                            <label for="noshowlegalother" class="frmlabel"><?php echo __('No Show Legal Other') ?></label>
                                            <input class="type_text_normal" type="text"  name="noshowlegalother" id="noshowlegalother" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="noshowlegalother_mintime" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input class="type_text_normal" type="text"   name="noshowlegalother_mintime" id="noshowlegalother_mintime" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intmedicalregular" class="frmlabel"><?php echo __('Interpreting Medical Regular') ?></label>
                                            <input type="text" class="type_text_normal" id="intmedicalregular" name="intmedicalregular" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intmedicalrare" class="frmlabel"><?php echo __('Interpreting Medical Rare') ?></label>
                                            <input type="text" class="type_text_normal" id="intmedicalrare" name="intmedicalrare" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intmedicalother" class="frmlabel"><?php echo __('Interpreting Medical Other') ?></label>
                                            <input type="text" class="type_text_normal" id="intmedicalother" name="intmedicalother" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intlegalregular" class="frmlabel"><?php echo __('Interpreting Legal Regular ') ?></label>
                                            <input type="text" class="type_text_normal" id="intlegalregular" name="intlegalregular" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intlegalrare" class="frmlabel"><?php echo __('Interpreting Legal Rare ') ?></label>
                                            <input type="text" class="type_text_normal" id="intlegalrare" name="intlegalrare" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="intlegalother" class="frmlabel"><?php echo __('Interpreting Legal Other ') ?></label>
                                            <input type="text" class="type_text_normal" id="intlegalother" name="intlegalother" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="minintmedical" class="frmlabel"><?php echo __('Minimum Medical Time ') ?></label>
                                            <input type="text" class="type_text_normal" id="minintmedical" name="minintmedical" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="minintlegal" class="frmlabel"><?php echo __('Minimum Legal Time ') ?></label>
                                            <input type="text" class="type_text_normal" id="minintlegal" name="minintlegal" />
                                            <hr class="hr_separate ui-widget-content"></hr> 
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="interpretingconferencecall" class="frmlabel"><?php echo __('Conference Call ') ?></label>
                                            <input type="text" class="type_text_normal" id="interpretingconferencecall" name="interpretingconferencecall" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="minintconferencecall" class="frmlabel"><?php echo __('Minimum Time ') ?></label>
                                            <input type="text" class="type_text_normal" id="minintconferencecall" name="minintconferencecall" />
                                            <hr class="hr_separate ui-widget-content"></hr> 

                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="translationperpage" class="frmlabel"><?php echo __('Translation Per Page ') ?></label>
                                            <input type="text" class="type_text_normal" id="translationperpage" name="translationperpage" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="translationperword" class="frmlabel"><?php echo __('Translation Per Word ') ?></label>
                                            <input type="text" class="type_text_normal" id="translationperword" name="translationperword" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="mileagerate" class="frmlabel"><?php echo __('Mileage Rate ') ?></label>
                                            <input type="text" class="type_text_normal" id="mileagerate" name="mileagerate" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dd>
                                            <label for="traveltime" class="frmlabel"><?php echo __('Travel Time ') ?></label>
                                            <input type="text" class="type_text_normal" id="traveltime" name="traveltime" />
                                        </dd>
                                    </dl>
                                </fieldset>

                                <fieldset>
                                    <legend><?php echo __('Licencia') ?></legend>
                                    <dl>
                                        <dd>
                                            <label for="license" class="frmlabel"><?php echo __('Licencia') ?></label>
                                            <input type="text" class="type_text_normal" id="license" name="license" />
                                        </dd>
                                    </dl>

                                    <dl>
                                        <dt>
                                        <label class="frmlabel"><?php echo __('Estado') ?></label>
                                        </dt>
                                        <dd class="buttonsCheck">           
                                            <input type="radio" id="status_active" name="status" value="1" /><label for="status_active"><?php echo __("Activo") ?></label>
                                            <input type="radio" id="status_inactive" name="status" value="0" checked="" /><label for="status_inactive"><?php echo __("Inactivo") ?></label>
                                        </dd>
                                    </dl>

                                    <label id="interstatus">
                                        <dl>    
                                            <dt>
                                            <label class="frmlabel" for="terminationdate"><?php echo __('Fecha') ?></label>
                                            </dt>
                                            <dd>                                    
                                                <input class="type_text_normal" type="text" id="terminationdate" name="terminationdate"/>
                                            </dd>
                                        </dl                                        >

                                        <dl>
                                            <dt>
                                            <label class="frmlabel" for="terminationreason"><?php echo __('Razon') ?></label>
                                            </dt>
                                            <dd>
                                                <textarea class="type_text_normal" name="terminationreason" id="terminationreason" cols="27" rows="6" ></textarea>
                                            </dd>
                                        </dl>
                                    </label>
                                    <input type="hidden" name="idInterpreter" id="idInterpreter" value="0"/> <br/>    

                                </fieldset>    

                            </div>

                            <div id="addnewLanguage">
                                <form id="newLanguageInter" name="newLanguageInter" method="post">    
                                    <fieldset>
                                        <dl>
                                            <dd>
                                                <label for="nameLanguage"><?php echo __('Nombre') ?> </label>
                                                <input class="type_text_normal" type="text" id="nameLanguage" name="nameLanguage"/>
                                            </dd>
                                        </dl>

                                        <dl>
                                            <dd>
                                                <label class="frmlabel"><?php echo __('Tipo') ?> </label>
                                                <select id="type" name="type">
                                                    <option value="REGULAR"><?php echo __('REGULAR') ?></option>
                                                    <option value="RARE"><?php echo __('RARE') ?></option>
                                                    <option value="OTHER"><?php echo __('OTHER') ?></option>
                                                </select> 
                                                <input type="hidden" name="newLanguage" id="newLanguage" value=""/> 
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </form>
                            </div>
                            </form>                        
                            </div>  




