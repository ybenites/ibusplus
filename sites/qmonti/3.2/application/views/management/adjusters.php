<style type="text/css">
    .title{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }    
    #buttons{
        margin-left: 157px;
    }
    .frmlbl
    {
        text-align: left;
    }
    .newAdjuster{
        margin-left: -121px; 
        float:left;
        font-size: 11px;

    }
    dd{
        float: left;
        margin-top: 7px;
    }
    label
    {
        text-align: left;
    }
    input{
        margin-left: 5px;
    }
    #status{

        margin-bottom: 25px;
        float:left;
    }
    .btnnew{
        margin-left: 80px;
        margin-bottom: 25px;
        float:left;
    }

    .chkActive{display: inline;}

</style>
<script type="text/javascript">

    var url_listAdjusters = '/management/adjusters/listAdjusters' + jquery_params;
    var url_getAdjusters = '/management/adjusters/getAdjusters' + jquery_params;
    var url_createUpdateAdjuster = '/management/adjusters/createUpdateAdjuster' + jquery_params;
    var url_deleteAdjuster = '/management/adjusters/deleteAdjuster' + jquery_params;
    var url_activeAdjuster = '/management/adjusters/activeAdjuster' + jquery_params;
    var nameAdjuster = '<?php echo __('Nombre Ajustador'); ?>';
    var nameInsuranceCarriers = '<?php echo __('Nombre de Compania'); ?>';
    var status = '<?php echo __('Estado'); ?>';
    var accions = '<?php echo __('Acciones'); ?>';
    var validName = '<?php echo __('El nombre de ajustador es obligatorio'); ?>';
    var validPhone = '<?php echo __('El numero de telefono es obligatorio'); ?>';
    var ListAdjust = '<?php echo __('Lista de Ajustadores'); ?>';
    var validInsuranceCarries = '<?php echo __('Seleccione una compania de seguros'); ?>';
    var msg_delete = '<?php echo __('Esta seguro de eliminar este Ajustador?'); ?>';
    var msg_active = '<?php echo __('Esta seguro de restaurar este Ajustador?'); ?>';
    var title_delete = '<?php echo __('Eliminar Ajustador'); ?>';
    var title_active = '<?php echo __('Activar Ajustador'); ?>';
    var title_Ajustador = '<?php echo __('Nuevo Ajustador'); ?>';
    var txt_nuevoinsu = '<?php echo __('Nueva Aseguradora'); ?>';
    var txt_emailrequests = '<?php echo __('El E-mail es requerido'); ?>';
    var txt_emailincorret = '<?php echo __('El E-mail es incorrecto'); ?>';
    var url_createinsurances = '/management/adjusters/saveInsurances' + jquery_params;

    $(document).ready(function() {

        /* BUTTONS */
        $('#newAdjusters').button()
        $('.chkActive').buttonset();
        $('#addinsurance').button();


        /* NEW ADJUSTER */

        $('#newAdjusters').button({
            icons: {
                primary: 'ui-icon-plus'
            }

        });


        $('#newAdjusters').click(function() {
            frmNewAdjuster.currentForm.reset();
            $('#formNew').dialog('open');

        });

        /********************************/

        $('#addinsurance').click(function() {
            frminsurances.currentForm.reset();
            $('#addinsurances').dialog('open');

        });

        /********************************/

        $('input[name=status]').change(function() {
            $("#gridListAdjusters").trigger('reloadGrid');
        });
        /*** DIALOGO PARA insurancecarriers ******/
        $('#addinsurances').dialog({
            autoOpen: false,
            title: txt_nuevoinsu,
            height: 300,
            width: 300,
            buttons: {
                'Guardar': function() {
                    if (!$('#frminsurances').valid()) {
                        return false;
                    }
                    $('#frminsurances').submit();

                },
                'Cancelar': function() {

                    /*Limpiar campos con reset*/
                    frminsurances.currentForm.reset();
                    $(this).dialog('close');
                }

            }


        })





        /* DIALOG NEW AJUSTERS */

        $('#formNew').dialog({
            autoOpen: false,
            title: title_Ajustador,
            height: 300,
            width: 500,
            //            modal: true,
            //            close:function(){
            //                $('#validation').get(0).reset();
            //            },
            buttons: {
                'Guardar': function() {
                    //Funcione el Validate 
                    if (!$('#frmNewAdjusters').valid()) {
                        return false;
                    }
                    $('#frmNewAdjusters').submit();
                },
                'Cancelar': function() {
                    /*Limpiar campos con reset */
                    frmNewAdjuster.currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });

        /************* VALIDATION DE NEW INSURANCECARRIER ******************/
        frminsurances = $('#frminsurances').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true

                }
            },
            messages: {
                name: {
                    required: validName
                },
                email: {
                    required: txt_emailrequests,
                    email: txt_emailincorret
                },
                phone: {
                    required: validPhone
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {

                $.ajax({
                    url: url_createinsurances,
                    type: "post",
                    data: $("#frminsurances").serializeObject(),
                    dataType: "json",
                    success: function(r) {
                        if (evalResponse(r))
                        {
                            var data = r.data;

                            $('#idInsuranceCarriers').append(
                            $('<option/>').attr('value', data.idInsuranceCarriers).text(data.name)
                        ).val(data.idInsuranceCarriers);

                            $('#idInsuranceCarriers-combobox').val($('#idInsuranceCarriers option:selected').text());

                            if (r.data > 0)
                                $("#gridListAdjusters").trigger('reloadGrid');
                            $("#addinsurances").dialog('close');
                            frminsurances.currentForm.reset();

                            //$("#grid_list_product").trigger("reloadGrid");
                        }

                    }
                });
            }

        });

        /* VALIDATION DE NEW AJUSTER */

        frmNewAdjuster = $('#frmNewAdjusters').validate({
            rules: {
                name: {
                    required: true
                },
                idInsuranceCarriers: {
                    required: true
                },
                adjuster_email: {
                    required: true,
                    email: true
                }

            },
            messages: {
                name: {
                    required: validName
                },
                idInsuranceCarriers: {
                    required: validInsuranceCarries
                },
                adjuster_email: {
                    required: txt_emailrequests,
                    email: txt_emailincorret
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {

                $.ajax({
                    url: url_createUpdateAdjuster,
                    type: "post",
                    data: $("#frmNewAdjusters").serializeObject(),
                    dataType: "json",
                    success: function(r) {
                        if (evalResponse(r))
                        {
                            $("#gridListAdjusters").trigger('reloadGrid');
                            $("#formNew").dialog('close');
                            frmNewAdjuster.currentForm.reset();
                        }

                    }
                });
            }

        });

        /* COMBOBOX */
        $('#idInsuranceCarriers').combobox();

        /* JQGRID LIST ADJUSTERS */
        $("#gridListAdjusters").jqGrid({
            url: url_listAdjusters,
            mtype: 'POST',
            datatype: 'JSON',
            postData: {
                status: function() {
                    return $('#status').is(':checked');
                }
            },
            colNames: [
                'IdAdjuster',
                nameAdjuster,
                'idInsuranceCarriers',
                nameInsuranceCarriers,
                status,
                accions
            ],
            colModel: [
                {index: 'idAdjuster', name: 'idAdjuster', key: true, hidden: true},
                {index: 'adname', name: 'adname', width: '300px', search: true, align:"center"},
                {index: 'idInsuranceCarrier', name: 'idInsuranceCarriers', hidden: true, align:"center"},
                {index: 'insname', name: 'insname', width: '300px', search: true, align:"center"},
                {index: 'status', name: 'status', width: '80px', search: false, align:"center"},
                {index: 'actions', name: 'actions', width: '100px', search: false, align:"center"}
            ],
            pager: '#gridListAdjustersPaper',
            rowNum: 100,
            height: 200,
            width:1125,
            rowList: [100, 200, 300,500],            
            //rownumbers: true, 
            viewrecords: true,
            caption: ListAdjust,
            afterInsertRow: function(row_id, data_id)
            {
                if (data_id.status == 1)
                {
                    edit = "<a class=\"edit\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
                    $("#gridListAdjusters").jqGrid('setRowData', row_id, {actions: edit + trash, status: 'activo'});
                }
                else
                {
                    active = "<a class=\"active\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#gridListAdjusters").jqGrid('setRowData', row_id, {actions: active, status: 'Inactivo'});
                }
            },
            gridComplete: function() {
                //figuras de botones del dialog
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                })

                $('.trash').button({
                    icons: {primary: 'ui-icon-trash'},
                    text: false
                })
                $('.active').button({
                    icons: {primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });
        $("#gridListAdjusters").jqGrid('filterToolbar', {stringResult: true, searchOnEnter: false});
        /* Editar Adjuster*/
        $("#gridListAdjusters").on('click', '.edit', function() {
            var id = $(this).data('oid');
            $.ajax({
                url: url_getAdjusters,
                type: "post",
                data: {id: id},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        /* DATOS AL DIALOG */
                        $("#idAjustador").val(data.idAdjuster);
                        $("#name_adjust").val(data.name);
                        $("#phone_adjuster").val(data.phone);
                        $("#adjuster_email").val(data.email);
                        $("#extentionPhone_adjuster").val(data.extentionphone);
                        $("#fax_adjuster").val(data.fax);
                        $("#idInsuranceCarriers").val(data.idInsurancecarriers);
                        $("#idInsuranceCarriers-combobox").val($("#idInsuranceCarriers option:selected").text());
                        $('#formNew').dialog('open');

                    } else {
                        /* ERROR */
                    }

                }
            });
        });

        /* Eliminar Adjuster */
        $("#gridListAdjusters").on('click', '.trash', function() {
            var id = $(this).data('oid');
            confirmBox(msg_delete, title_delete, function(response) {
                if (response) {
                    $.ajax({
                        url: url_deleteAdjuster,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListAdjusters").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });

        $("#gridListAdjusters").on('click', '.active', function() {
            var id = $(this).data('oid');
            confirmBox(msg_active, title_active, function(response) {
                if (response) {
                    $.ajax({
                        url: url_activeAdjuster,
                        type: "post",
                        data: {id: id},
                        dataType: "json",
                        success: function(response) {
                            if (response.code == code_success)
                            {
                                $("#gridListAdjusters").trigger("reloadGrid");
                            } else
                            {
                                msgBox(response.msg, response.code);
                            }
                        }
                    });
                }
                return false;
            });
        });


    });

    //    function resetForm($form) {
    //           $form.find('input:text, input:password, input:file, select, textarea').val('');
    //           $form.find('input:radio, input:checkbox')
    //           .removeAttr('checked').removeAttr('selected');
    //               
    //       }
</script>
<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA DE AJUSTADORES') ?></div>
    </center>
</div>
<div class="show_content">
    <div id="buttons">
        <button id="newAdjusters" class="newAdjuster"><?php echo __('Nuevo Ajustador') ?></button>
        <div class="chkActive">
            <input type="checkbox" id="status" name="status"  />
            <label for="status"><?php echo __('Listar Todos') ?></label><br><br><br><br>
        </div>
    </div>
    <center>
        <div id="gridListAdjusters-div">
            <table id="gridListAdjusters"></table>
            <div id="gridListAdjustersPaper"></div>
        </div>
    </center>
</div>
<div id="formNew">
    <form id="frmNewAdjusters" name="frmNewAdjusters" action="" method="post">
        <fieldset>
            <legend><?php echo __('Crear Ajustador') ?></legend>
            <input type="text" name="idAjustador" id="idAjustador" hidden="true"/>
            <dl>
                <dd>                
                    <label class="frmlbl"><?php echo __('Ajustador') ?></label>
                    <input type="text" name="name_adjust" id="name_adjust" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Compania de Seguros') ?></label>
                    <select name="idInsuranceCarriers" id="idInsuranceCarriers">
                        <option value=""><?php echo __('Seleccionar ...') ?></option>
                        <?php foreach ($insuranceCarrier as $ic) { ?>
                            <option value="<?php echo $ic['idInsurancecarrier'] ?>"><?php echo $ic['name'] ?></option>
                        <?php }; ?>
                    </select>
                    <button type="button" id="addinsurance" name="addinsurance"><?php echo __("agregar") ?></button>
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Correo Electronico') ?></label>
                    <input type="text" name="adjuster_email" id="adjuster_email" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Telefono') ?></label>
                    <input type="text" name="phone_adjuster" id="phone_adjuster" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Ext') ?></label>
                    <input type="text" name="extentionPhone_adjuster" id="extentionPhone_adjuster" size="10" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Fax') ?></label> 
                    <input type="text" name="fax_adjuster" id="fax_adjuster" size="30" value="">
                </dd>

                <!--<img id="addic" src="">-->
            </dl>

        </fieldset>
    </form>
</div>

<div id="addinsurances">
    <form id="frminsurances" name="frminsurances" action="" method="post">
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Nombre') ?></label>
                <input type="text" name="name-insu" id="name-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Nombre de Contacto') ?></label>
                <input type="text" name="contactname-insu" id="contactname-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Direccion') ?></label>
                <input type="text" name="address-insu" id="address-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Telefono') ?></label>
                <input type="text" name="phone-insu" id="phone-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Fax') ?></label>
                <input type="text" name="fax-insu" id="fax-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Ciudad') ?></label>
                <input type="text" name="city-insu" id="city-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Codigo ZIP') ?></label>
                <input type="text" name="zipcode-insu" id="zipcode-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Estado') ?></label>
                <input type="text" name="state-insu" id="state-insu" size="30" value="">
            </dd>
        </dl>
        <dl>
            <dd>                
                <label class="frmlbl"><?php echo __('Correo Electronico') ?></label>
                <input type="text" name="email-insu" id="email-insu" size="30" value="">
            </dd>
        </dl>
    </form>
</div>
