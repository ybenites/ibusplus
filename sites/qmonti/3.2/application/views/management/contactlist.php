<style type="text/css"> 
    .titleTables{
        font-size:17px;
        font-weight:bold;
        margin-bottom: 10px;
        padding: 3px;
        width: 50%;
        text-align: center;
    }
    #newcontact{
        margin-left: 37px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;
    }
    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }
    select#type{
        width: 200px; /* una longitud definida ancho */
        height:25px;
        padding: 3px 2px 4px 5px;
        margin: 5px 0;
    }
    #formNew fieldset{
        width:288px; /*ancho*/
        height:130px;
        padding: 3px 2px 4px 7px;

    }
    .frmlabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 5px;
        width: 120px;
    }
    .type_text_normal{
        width: 120px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;
    }
</style>

<script type="text/javascript">
    
    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro de desactivar el area?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro de activar el area?"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    var titleContact= '<?php echo __("Contacto"); ?>';
    
    var saveContact ="/management/contactlist/save"+jquery_params;
    var listContact = '/management/contactlist/consults';
    var editContact = '/management/contactlist/getPersonById'+jquery_params;
    var trashContact = "/management/contactlist/delete"+jquery_params;
    var activeContact = "/management/contactlist/active"+jquery_params;
  
    $(document).ready(function(){
       
        /*Boton chkckbox */
        $('.btnprueba').buttonset();
        $('#newcontact').button({
            icons:{
                primary:"ui-icon-plus"
            }
          
        });
        
        
        
        /* DIALOG PARA EDITAR CONTACTOS */
        
        $("#formNew").dialog({
            autoOpen: false,
            title:titleContact,
            height: 250,
            width: 320,
            modal: true,
            close:function(){
                $('#validation').get(0).reset();
            },
            buttons:{
                Guardar:function(){                
                    if(!$('#validation').valid()){
                        return false;
                    } 
                    $('#validation').submit();
                    $("#grid-listarea").trigger("reloadGrid");
                },
                Cancelar:function(){
                    $( this ).dialog( "close" );
                }
            }
            
                     
        });
  
        $('#validation').validate({
            rules:{
                contact:{
                    required:true
                } 
                
            },
            messages:{
                contact:{
                    required:'<?php echo __("Nombre Contacto Requerido") ?>'
                }
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){ 
                showLoading = 1;
                $.ajax({
                    url: saveContact,
                    type:"post",
                    data:$('#validation').serializeObject(),
                    dataType:"json",
                    success: function(r){               
                               
                        if(evalResponse(r))
                        {
                            $("#formNew").dialog('close');
                            $("#grid-listarea").trigger("reloadGrid");
                        }

                    }
                });
            }
                       
        });
        
        $( "#newcontact" ).click(function() {
            $( "#formNew" ).dialog( "open" );
        });
        
        
    
        /*jgrid GENERAL*/
        $("#grid-listarea").jqGrid({ 
            url:listContact, 
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['ID','<?php echo __('Contacto'); ?>','<?php echo __('Direccion'); ?>','<?php echo __('Suite'); ?>','<?php echo __('Telefono'); ?>','<?php echo __('Estado'); ?>','<?php echo __('Acciones'); ?>'], 
            colModel:[ 
                {name:'idContactlist',index:'idContactlist',hidden: true, width:50}, 
                {name:'contact',index:'contact',search:true, width:300, align:"center"}, 
                {name:'address',index:'address',width:100,search:false, align:"center"},
                {name:'suite',index:'suite',width:100,search:false, align:"center"},
                {name:'telephone',index:'telephone',width:100,search:false, align:"center"},
                {name:'status',index:'status',width:100,search:false, align:"center"},
                {name:'actions',index:'actions', width:200,search:false, align:"center"}
               
            ],
            rowNum:10, 
            rowList:[10,20,30], 
            pager: 'grid-listarea-paper', 
            sortname: 'idContactlist',
            height: 200,
            width:1125,
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __("Lista Contactos"); ?>",
            afterInsertRow:function(row_id, data_id){
                console.log(data_id.status); 
                if(data_id.status==1){
                    
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                    $("#grid-listarea").jqGrid('setRowData',row_id,{actions:edit+trash, status: 'Activo'});
                } 
                
                else{
                    console.log(data_id.status)
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#grid-listarea").jqGrid('setRowData',row_id,{actions:active, status: 'Inactivo'});
                }
            },
                
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.trash').button({
                    icons:{primary: ' ui-icon-power'},
                    text: false
                })
                
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
                    
        }); 
        //si hace el checked de los activos, recarga la pagina
        $('input[name=status]').change(function(){
            $("#grid-listarea").trigger("reloadGrid");
        });
        
        
        
        //jQuery("#grid-listarea").jqGrid('navGrid',"#grid-listarea-paper",{edit:false,add:false,del:false}); 
        jQuery("#grid-listarea").jqGrid('inlineNav',"#grid-listarea-paper"); 
        
        /*Busqueda*/
        $("#grid-listarea").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        
        $("table#grid-listarea").on('click','.edit',function(){
            
                        
            var idContactlist = $(this).data("oid");
            showLoading = 1;
            $.ajax({
                url: editContact ,
                type:"post",
                data:{idContactlist:idContactlist},
                dataType:"json",
                success: function(j_response){
                    
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        console.log(data);
                        $("#idContactlist").val(data.idContactlist)
                        $("#contact").val(data.contact)
                        $("#address").val(data.address);
                        $("#suite").val(data.suite);
                        $("#telephone").val(data.telephone);
                        $("#formNew").dialog("open"); 
                        
                    }
                     
                }
            })
            
        }),
        
        $("table#grid-listarea").on('click','.trash',function(){
        
            var id=$(this).attr("data-oid");
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){  
                    showLoading = 1;
                    $.ajax({                      
                        url:trashContact,
                        type:"post",
                        data : {idContactlist:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-listarea").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false

        }),
        
        $("table#grid-listarea").on('click','.active',function(){
        
            var id=$(this).attr("data-oid");
            confirmBox(confirmActiveMessage,activeSubTitle,function(response){
                if(response){   
                    showLoading = 1;
                    $.ajax({                      
                        url:activeContact,
                        type:"post",
                        data : {idContactlist:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-listarea").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
             
        
        });     
            
    });       
        
</script>




<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA DE CONTACTOS') ?></div>
    </center>
</div>

<div class="show_content">
<br/> <button id="newcontact" ><?php echo __('Nuevo Contacto') ?></button>
<div class="btnprueba">
    <input type="checkbox" id="status" name="status"  />
    <label for="status"><?php echo __('Listar Todos') ?></label><br><br><br>
</div>
<div id="formNew">
    <form id="validation" name="validation" action="" method="post">

        <input type="hidden" name="idContactlist"  id="idContactlist" value="">

        <fieldset> 

            <legend><?php echo __('Informacion del Contacto') ?></legend>
            <ul>
                <li>
                    <dl>
                        <dt>
                        <label class="frmlbl"><?php echo __('Contacto') ?></label>
                        </dt>
                        <dd>
                            <input class="type_text_normal" type="text" id="contact" name="contact"  />
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </li>


                <li>
                    <dl>
                        <dt><label class="frmlbl"><?php echo __('Direccion') ?></label></dt>
                        <dd>
                            <input class="type_text_normal" type="text" id="address" name="address" />
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </li>
                <li>
                    <dl>
                        <dt><label class="frmlbl"><?php echo __('Suite') ?></label></dt>
                        <dd>
                            <input class="type_text_normal" type="text" name="suite" id="suite" />
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </li>
                <li>
                    <dl>
                        <dt><label class="frmlbl"><?php echo __('Telefono') ?></label></dt>
                        <dd>
                            <input class="type_text_normal" type="text" name="telephone" id="telephone" />
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </li>
        </fieldset> 
</div> 
</div> 
<center>  
    <br>
    <table id="grid-listarea" ></table>
    <div id="grid-listarea-paper"></div>
</center>