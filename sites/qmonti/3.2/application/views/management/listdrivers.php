<style type="text/css"> 
.frmlabel {
        display: block;
        width: 110px;
        float: left;
        font-weight: bold;
        height: 12px;
        padding-top: 8px;
        font-size:12px;
            
    }
    
.label-right {
        display: block;
        width: 110px;
        float: left;
        font-weight: bold;
        height: 12px;
        padding-top: 8px;
        font-size:12px;
        padding-left: 50px;
            
    }    
    
    legend, h3 {
        color: #0171B2;
        font-size: 12px;
        font-weight: bold;
    }
        
    .type_text_normal{
        
        width: 170px; /* una longitud definida ancho */
        height:15px;
        margin: 5px 0;
        float:left;
        padding-top: 8px;
    }
        
    .class_p{
        clear: left;
        float: left;
        margin: 1px 0;
    }
    
     #dialog-driver fieldset{
        padding: 3px 7px 4px 7px;
    }
    
</style>

<script type="text/javascript"> 
 var listAllDrivers = '/management/listdrivers/listAllDrivers';    
 var editDriver = '/management/listdrivers/getPersonById';    
    
    $(document).ready(function(){
        
         $('#dialog-driver').dialog({
        autoOpen: false,
            title:"<?php echo __("Lista Conductores"); ?>",
            height:'auto',
            width:'auto',
            modal: true,
            resizable:false,
            buttons:{
              
                "Cancelar":function(){
                    $( this ).dialog( "close" );
                }            
            }
        
     });   
        
        
        $("#user_jqGrid").jqGrid({ 
            url:listAllDrivers, 
            datatype: "json",
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['id',"<?php echo __("Nombre"); ?>","<?php echo __("Apellido"); ?>",
                "<?php echo __("Direccion"); ?>","<?php echo __("Telefono"); ?>", 
                "<?php echo __("Fax"); ?>","<?php echo __("Celular"); ?>",
                "<?php echo __("Email"); ?>","<?php echo __("Notas"); ?>",
                "<?php echo __("Locacion"); ?>","<?php echo __("Estado"); ?>", '<?php echo __("Registrado por"); ?>',"<?php echo __("Acciones"); ?>"], 
            colModel:[ 
                {name:'idDriver',index:'idDriver', hidden:true}, 
                {name:'name',index:'name', width:100,align:"center",frozen : true}, 
                {name:'last_name',index:'last_name', width:150,align:"center",frozen : true}, 
                {name:'address',index:'address', width:200, editable:true,align:"center"}, 
                {name:'phone',index:'phone', width:100,editable:true,align:"center"}, 
                {name:'fax',index:'fax', width:100, editable:true,align:"center"},
                {name:'cellphone',index:'cellphone', width:100,editable:true,align:"center"},
                {name:'email',index:'email', width:200, editable:true,align:"center"},
                {name:'specialNotes',index:'specialNotes', width:400, editable:true,align:"center"},
                {name:'location',index:'location', width:150, editable:true,align:"center"},
                {name:'status',index:'status', width:100, editable:true,align:"center"},
                {name:'user',index:'user', width:100, editable:true,align:"center"},
                {name:'actions',index:'actions', align:'right',width:80, search:false,align:"center"}
            ],
                        
            rowNum:100, 
            rowList:[100,200,300,500], 
            pager: 'user_jqGrid_pager', 
            sortname: 'id', 
            width:1200,
            height:400,
            shrinkToFit:false,
            viewrecords: true, 
            sortorder: "desc", 
            
            caption: "List Drivers",
            afterInsertRow:function(row_id, data_id){
                
                if(data_id.status==1)
                {
                    search = "<a class=\"search\" data-oid=\""+row_id+"\" title=\"<?php echo __('Buscar'); ?>\" ><?php echo __('Buscar'); ?></a>";
                    
                    $("#user_jqGrid").jqGrid('setRowData',row_id,{actions:search, status: 'Active'});
                }
                
                
            },
            gridComplete:function(){
                $('.search').button({
                    icons:{primary: 'ui-icon-search'},
                    text: false
                })
               
            }
                    
        }); 
        $("#user_jqGrid").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
         $("table#user_jqGrid").on('click','.search',function(){
        
            $("#idDriver").val(($(this).data("oid")));
            var obj = new Object();
            obj.idDriver = $(this).data("oid");
            showLoading = 1; //para q salga q esta cargando los dx
            $.ajax({
                url: editDriver,
                type:"post",
                data:obj,
                dataType:"json",
                success: function(j_response){
                      if (evalResponse(j_response)){
                        var data=j_response.data;
                        
                        console.log(data);
                         $("#name").val(data.name);
                         $("#last_name").val(data.lastName);
                      $("#areas_drivers_id").val(data.location);
                      $("#typeservices").val(data.typeservices);
                      
                         $("#address").val(data.address);
                         $("#city").val(data.city);
                         $("#state").val(data.state);
                         $("#zip_code").val(data.zipcode);
                         $("#phone").val(data.phone);
                         $("#fax").val(data.fax);
                         $("#cell_phone").val(data.cellphone);
                         $("#car_make").val(data.carMake);
                         $("#car_model").val(data.carModel);
                         $("#email").val(data.email);
                         $("#insurance_date").val(data.insurance);
                         $("#special_notes").val(data.specialNotes);
                         
                       
                        $( "#dialog-driver" ).dialog( "open" ); 
                    }
                                                     
                }    
            })
        })
        
    });
        

</script>
<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('LISTA CONDUCTORES') ?></div>
    </center>
</div>

<center>
    <table id="user_jqGrid"></table>
    <div id="user_jqGrid_pager"></div>
</center>

<div id="dialog-driver">
    <fieldset>
         <legend><?php echo __('Lista Conductores') ?></legend>
         <input  type="text" name="idDriver" id="idDriver" hidden="true"/>   
         
         <p class="class_p">
             <label for="name" class="frmlabel"><?php echo __('Nombre') ?></label>
             <input class="type_text_normal" disabled type="text" name="name" id="name" />    
 
             <label for="last_name" class="label-right"><?php echo __('Apellido') ?></label>
             <input class="type_text_normal" disabled type="text" name="last_name" id="last_name" />    
         </p>
         
         <p class="class_p">
             <label for="areas_drivers_id" class="frmlabel"><?php echo __('Area') ?></label>
             <input class="type_text_normal" disabled type="text" name="areas_drivers_id" id="areas_drivers_id" />    
         
             <label for="typeservices" class="label-right"><?php echo __('Servicios') ?></label>
             <input class="type_text_normal" disabled type="text" name="typeservices" id="typeservices" />    
         </p>
         
         <p class="class_p">
             <label for="address" class="frmlabel"><?php echo __('Direccion') ?></label>
             <input class="type_text_normal" disabled type="text" name="address" id="address" />    
         
             <label for="city" class="label-right"><?php echo __('Ciudad') ?></label>
             <input class="type_text_normal" disabled type="text" name="city" id="city" />    
         </p>
         
         <p class="class_p">
             <label for="state" class="frmlabel"><?php echo __('Estado') ?></label>
             <input class="type_text_normal" disabled type="text" name="state" id="state" />    
             
             <label for="zip_code" class="label-right"><?php echo __('Codigo Zip') ?></label>
             <input class="type_text_normal" disabled type="text" name="zip_code" id="zip_code" />    
         </p>
         
          <p class="class_p">
             <label for="phone" class="frmlabel"><?php echo __('Telefono') ?></label>
             <input class="type_text_normal" disabled type="text" name="phone" id="phone" />    
             
             <label for="fax" class="label-right"><?php echo __('Fax') ?></label>
             <input class="type_text_normal" disabled type="text" name="fax" id="fax" />    
         </p>
         
         <p class="class_p">
             <label for="cell_phone" class="frmlabel"><?php echo __('Celular') ?></label>
             <input class="type_text_normal" disabled type="text" name="cell_phone" id="cell_phone" />    
             
             <label for="car_make" class="label-right"><?php echo __('Marca Carro') ?></label>
             <input class="type_text_normal" disabled type="text" name="car_make" id="car_make" />    
         </p>
         
          <p class="class_p">
             <label for="car_model" class="frmlabel"><?php echo __('Modelo Carro') ?></label>
             <input class="type_text_normal" disabled type="text" name="car_model" id="car_model" />    
             
             <label for="email" class="label-right"><?php echo __('Correo Electronico') ?></label>
             <input class="type_text_normal" disabled type="text" name="email" id="email" />    
         </p>
         
          <p class="class_p">
             <label for="insurance_date" class="frmlabel"><?php echo __('Fecha Seguro') ?></label>
             <input class="type_text_normal" disabled type="text" name="insurance_date" id="insurance_date" />    
             
             <label for="special_notes" class="label-right"><?php echo __('Notas Especiales') ?></label>
             <input class="type_text_normal" disabled type="text" name="special_notes" id="special_notes" />    
         </p>
              
    </fieldset>
    
</div>
