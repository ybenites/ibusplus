<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo SITE_DOMAIN;?></title>
        <style type="text/css">
            body{
                font-family: 'Tahoma','Arial','Courier New';
            }
            h3{text-align: right;}
        </style>
    </head>
    <body>
        <h3><?php echo date('d-m-Y H:i:s'); ?></h3>
        <hr/>
        <h1><?php echo "config_db not found for ".SITE_DOMAIN; ?></h1>
        <h2><?php echo "Please create the configuration file ". SITE_DOMAIN ."_db.php"; ?></h2>
        
    </body>
</html>
