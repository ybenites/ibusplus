<script type="text/javascript">
    var listappassig= '/patients/confofassignment/consult';
    var sendemail= '/patients/confofassignment/sendemail';
    var isSendEmail = false;
    var idAppointmentEmail = null;

    $(document).ready(function(){
    
    
        $("#grid-confapp").jqGrid({ 
            url:listappassig, 
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['<?php echo __('ID') ?>',
                      '<?php echo __('Paciente') ?>',
                      '<?php echo __('Teléfono') ?>',
                      '<?php echo __('Fecha de Cita') ?>',
                      '<?php echo __('Hora de Cita') ?>',
                      '<?php echo __('Estado') ?>',
                      '<?php echo __('Acciones') ?>'], 
            colModel:[ 
                {name:'idAppointment',index:'idAppointment', width:50 , align:"center" }, 
                {name:'patient',index:'patient',search:true, width:150, align:"center" }, 
                {name:'phone',index:'phone', width:150,search:false, editable:true, align:"center" },
                {name:'dateapp',index:'dateapp', width:150,search:false, editable:true, align:"center" },
                {name:'timeapp',index:'timeapp', width:150,search:false, editable:true, align:"center" },
                {name:'status',index:'status',width:150,search:false, align:"center" },
                {name:'actions',index:'actions', width:150,search:false, align:"center"}
               
            ],
            rowNum:10, 
            rowList:[10,20,30], 
            pager: 'grid-confapp-paper', 
            sortname: 'status', 
            height: 200,
            width:1000,
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __('Confirmación de Asignamientos') ?>",
            afterInsertRow:function(row_id, data_id){
            
             
                genpdf = "<a class=\"genpdf\" data-oid=\""+row_id+"\" title=\"<?php echo __('Generar PDF'); ?>\" ><?php echo __('Generar PDF'); ?></a>";
                sendemailadjuster = "<a class=\"sendemail\" data-oid=\""+row_id+"\" title=\"<?php echo __('Enviar E-mail'); ?>\" ><?php echo __('Enviar E-mail'); ?></a>";
            
                $("#grid-confapp").jqGrid('setRowData',row_id,{actions:genpdf+sendemailadjuster, status: '<?php echo __('Activo'); ?>'});
           
                

            },
            gridComplete:function(){
                $('.genpdf').button({
                    icons:{primary: 'ui-icon-document'},
                    text: false
                })
                $('.sendemail').button({
                    icons:{primary: ' ui-icon-mail-closed'},
                    text: false
                })
             

            }
                    
        }); 
        
        
         

    
        $("table#grid-confapp").on('click','.sendemail',function(){
            isSendEmail = true;
            idAppointmentEmail = $(this).data("oid");
            var data = new Object();
            data.idAppointmentEmail = idAppointmentEmail;
            data.idAppointment = idAppointmentEmail;
            
            $('#report_options').iReportingPlus('option','dynamicParams',data);        
            $('#report_options').iReportingPlus('generateReport','pdf');
        });
        
        $('#report_options').iReportingPlus({
            domain:domain,
            repopt:['pdf'],
            clientFolder : 'qmonti',
            clientFile:'MONTI_CONFIRMATION_ASSIGMENT',
            htmlVisor:false,
            jqUI:true,
            urlData:'/patients/confofassignment/reportConfofassignment',
            xpath:'/report/response/row',
            orientation:'vertical',
            staticParams:rParams,
            reportZoom:1.2,
            responseType:'jsonp',
            jqCallBack:jquery_params,
            waitMessage:'<?php echo __('Generando su Reporte'); ?>...',
            afterJSONResponse:function(response){
                if(isSendEmail){
                    myAfterJSONResponse(response);
                }
            }
            
        });
              
        
        function myAfterJSONResponse(response){
            if(response.code == 1){
                showLoading = 1;
                response.idAppointment = idAppointmentEmail;
                $.ajax({
                    url: sendemail,
                    type:"post",
                    data:response,
                    dataType:"json",
                    success: function(j_response){
                         if (evalResponse(j_response)){
                        msgBox('<?php echo __('El mensaje se ha enviado satisfactoriamente.'); ?>','<?php echo __('Confirmación de E-mail'); ?>','');
                         }
                    }
                });
            }else
            {msgBox('<?php echo __('No se pudo enviar e-mail.'); ?>','<?php echo __('Error'); ?>','');}
        }
    
    });

</script>



<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Confirmación de Asignamientos') ?></div>
    </center>
</div>
<center>  
    <table id="grid-confapp" ></table>
    <div id="grid-confapp-paper"></div>
</center>
<div id="report_options"></div>
