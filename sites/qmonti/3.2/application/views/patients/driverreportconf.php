<script>
 var listDriversWithApp = "/patients/driverreportconf/listDriversWithApp";
 var listDrivers = "/patients/driverreportconf/listDrivers";
 var listAppbyIdDriver = "/patients/driverreportconf/listAppbyIdDriver";
 var sendEmailDriver = "/patients/driverreportconf/sendEmailDriver";
 var comfirmSentDriver = "/patients/driverreportconf/comfirmSentDriver";
 var reportDriver = "/patients/driverreportconf/reportPrintAppbyDriver";
 var idDriver=0;
 var all=1;
 var isSendEmail = false;
 var dataIdApp = new Array();
 var i=0;
 $(document).ready(function() {
    

    $('#print').button();
    $('#send').button();   
    $('#filter').button(); 
    $('#divOpenAppbyDriver').dialog({
           autoOpen: false,       
           height: "auto",
           width: 850 
       });
     
    $('#ndaystart').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($(this).val()!='' && $('#ndayfinish').val()){
                    all=0;
                    $("#grid-listAppbyIdDriver").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
                $( "#ndayfinish" ).datepicker( "option", "minDate", selectedDate );
            }        
                    
        });
    $('#ndayfinish').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($(this).val()!='' && $('#ndaystart').val()){
                    all=0;
                    $("#grid-listAppbyIdDriver").trigger("reloadGrid");
                }},
            onClose: function( selectedDate ) {
                $( "#ndaystart" ).datepicker( "option", "maxDate", selectedDate );
            }     
               
            
        });
    
    $("#namedriver").autocomplete({
            //define callback to format results
            source: function(req, response) {
                $.ajax({
                    url: listDrivers,
                    dataType: "json",
                    type: 'post',
                    data: {
                        namedriver: $('#namedriver').val()
                    },
                    success: function(j_response) {
                        response(
                                $.map(j_response.data, function(item) {
                            //                            var text = "<span id='left'>" + item.firstname+ " " + item.lastname + "</span><span id='right'>" + item.socialsecurity+ "</span>";
                            return {
                                //                                label:  text.replace(
                                //                                    new RegExp(
                                //                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                //                                        $.ui.autocomplete.escapeRegex(req.namepatient) +
                                //                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                //                                        ), "<strong>$1</strong>" ),
                                label: item.name+' '+item.lastName,
                                value: item.name+' '+item.lastName,
                                idDriver: item.idDriver
                            }
                        })
                                );
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                idDriver=ui.item.idDriver;
                $("#grid-listAppbyIdDriver").trigger("reloadGrid");
                $('#divOpenAppbyDriver').dialog('open');
            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {
                    // remove invalid value, as it didn't match anything
                    $(this).val("");
                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.label + "</a>")
                    .appendTo(ul);
            };
    
    
    $("#grid-listDriversWithApp").jqGrid({
        url: listDriversWithApp,
        datatype: "json",
        postData: {
           all: function() {
                    return '1';
                }
        },
        colNames: ['<?php echo __('ID Cita'); ?>', 
                   '<?php echo __('Nombre'); ?>', 
                   '<?php echo __('Celular'); ?>', 
                   '<?php echo __('Fax'); ?>', 
                   '<?php echo __('Nextel'); ?>', 
                   '<?php echo __('Acciones'); ?>'],
        colModel: [
          
            {name: 'idDriver', index: 'idDriver',hidden: true, width: 50},
            {name: 'drivername', index: 'drivername', search: true, width: 300},
            {name: 'cellphone', index: 'cellphone', search: true, width: 200},
            {name: 'fax', index: 'fax', width: 200, search: false},
            {name: 'nextel', index: 'nextel', width: 200, search: false},
            {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

        ],
        rowNum: 30,
        rowList: [30, 40, 50],
        height: 400,
        width: 1200,
        pager: 'grid-listDriversWithApp-paper',
        sortname: 'idDriver',
        viewrecords: true,
        sortorder: "desc",
        caption: "<?php echo __('Lista de Conductores'); ?>",
        afterInsertRow: function(row_id, data_id) {

                check = "<a class=\"check\" data-iddriver=\"" + row_id + "\" title=\"<?php echo __('Seleccionar'); ?>\" ><?php echo __('Seleccionar'); ?></a>";
               
                $("#grid-listDriversWithApp").jqGrid('setRowData', row_id, {actions: check});       
        },
        gridComplete: function() {
            $('.check').button({
                icons: {primary: 'ui-icon-check'},
                text: false
            });
            
   
        }
        });      
    
    $('#grid-listDriversWithApp').on('click', '.check',function() {
            idDriver=$(this).attr("data-iddriver");
            all=1;
            $('#divOpenAppbyDriver').dialog('open');
            $("#grid-listAppbyIdDriver").trigger("reloadGrid");
             
    });
 
    $("#grid-listAppbyIdDriver").jqGrid({
        url: listAppbyIdDriver,
        datatype: "json",
        postData: {
           all: function() {
                    return  all;
                },
           idDriver: function() {
                    return  idDriver;
                },
           dateStart: function() {
                    return  $('#ndaystart').val();
                },
           dateFinish: function() {
                    return  $('#ndayfinish').val();
                }     
        },
        colNames: ['<?php echo __('ID'); ?>', 
                   '<?php echo __('Fecha de Cita'); ?>', 
                   '<?php echo __('Partida'); ?>', 
                   '<?php echo __('Cita'); ?>', 
                   '<?php echo __('Retorno'); ?>', 
                   '<?php echo __('Paciente'); ?>', 
                   '<?php echo __('Desde'); ?>', 
                   '<?php echo __('A'); ?>', 
                   '<?php echo __('Millas'); ?>'],
        colModel: [
          
            {name: 'idApp', index: 'idApp',hidden: true, width: 20},
            {name: 'appDate', index: 'appDate', search: true, width: 70},
            {name: 'pickuptime', index: 'pickuptime', search: true, width: 50},
            {name: 'appTime', index: 'appTime', width: 50, search: false},
            {name: 'droptime', index: 'droptime', width: 50, search: false},
            {name: 'patient', index: 'patient', search: true, width: 100},
            {name: 'from', index: 'from', search: true, width: 200},
            {name: 'to', index: 'to', width: 200, search: false},
            {name: 'miles', index: 'miles', width: 50, search: false}
            

        ],
        rowNum: 30,
        rowList: [30, 40, 50],
        height: 250,
        pager: 'grid-listAppbyIdDriver-paper',
        sortname: 'idApp',
        viewrecords: true,
        sortorder: "desc",
        caption: "<?php echo __('Lista de Citas'); ?>",
        afterInsertRow: function(row_id, data_id) {
                
                dataIdApp[i]= row_id;
                i++;
                check = "<a class=\"check\" data-iddriver=\"" + row_id + "\" title=\"<?php echo __('Seleccionar'); ?>\" ><?php echo __('Seleccionar'); ?></a>";
                $("#grid-listAppbyIdDriver").jqGrid('setRowData', row_id, {actions: check});
  
        },
        gridComplete: function() {
            $('.check').button({
                icons: {primary: 'ui-icon-check'},
                text: false
            });
            
   
        }
        }); 
        
    $('#print').click(function(){
                var data= new Object();
                data.idDriver = idDriver;
                data.all = all;
                
                if (all == '0') {
                    data.dateStart = $('#ndaystart').val();
                    data.dateFinish = $('#ndayfinish').val();
                }
        
        $('#divReport').iReportingPlus('option','dynamicParams',data);        
        $('#divReport').iReportingPlus('generateReport','pdf');

    });
    
    $('#send').click(function(){
             isSendEmail = true;
             var data= new Object();
                data.idDriver = idDriver;
                data.all = all;

                if (all == '0') {
                    data.dateStart = $('#ndaystart').val();
                    data.dateFinish = $('#ndayfinish').val();
                }
            
            $('#divReport').iReportingPlus('option','dynamicParams',data);        
            $('#divReport').iReportingPlus('generateReport','pdf');
            
    });
    
    $('#divReport').iReportingPlus({
           domain:domain,
           repopt:['pdf'],
           clientFolder : 'qmonti',
           clientFile:'MONTI_PRINT_CONF_DRIVER',
           htmlVisor:false,
           jqUI:true,
           urlData:reportDriver,
           xpath:'/report/response/row',
           orientation:'horizontal',
           staticParams:rParams,
           reportZoom:1.2,
           responseType:'jsonp',
           jqCallBack:jquery_params,
           waitMessage:'<?php echo __('Generando su Reporte'); ?>...',
           afterJSONResponse:function(response){
               if(isSendEmail){
                   myAfterJSONResponse(response);
               }
           }
           
       });
       
     function myAfterJSONResponse(response){
            if(response.code == 1){
                showLoading = 1;
                response.idDriver=idDriver;
                response.dataIdApp = dataIdApp.toString();
                $.ajax({
                    url: sendEmailDriver,
                    type:"post",
                    data:response,
                    dataType:"json",
                    success: function(j_response){
                        msgBox('The message has been sent successfully','Confirmation E-mail','');
                       
                        $.ajax({
                            url: comfirmSentDriver,
                            type:"post",
                            data:response,
                            dataType:"json",
                            success: function(j_response){ }
                            });
                    }
                });
            }else
            {msgBox('Failed to send e-mail','Confirmation E-mail','');}
      }  
            
 });

</script>
<center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Citas Pendientes por Reporte de Conductor') ?></div>
</center>
<div id="show_content" class="show_content" >
    <h3><?php echo __('Lista de Conductores') ?></h3>
    <br>
    <label class="titles"><?php echo __('Nombre del Conductor') ?></label>
    <input type="text" name="namedriver" id="namedriver" />
    <input type="hidden" name="idDriver" id="idDriver"/>
    <center>  
        <br>
        <br>
        <table id="grid-listDriversWithApp" ></table>
        <div id="grid-listDriverWithApp-paper"></div>
    </center>
    
    <div id="divOpenAppbyDriver" title="<?php echo __('Citas'); ?>">
    <input type="button" id="filter" value="<?php echo __('Filtro por Rago de Fechas'); ?>"/>
    <label  for="ndaystart"><?php echo __('Desde'); ?></label></dd>
    <input  type="text" name="ndaystart" id="ndaystart"/></dd>
    <label for="ndayfinish"><?php echo __('Hasta'); ?></label></dd>
    <input type="text" name="ndayfinish" id="ndayfinish"/></dd>
    
    <div class="clear"></div>
    <center>  
        <br>
        <br>
        <table id="grid-listAppbyIdDriver" ></table>
        <div id="grid-listAppbyIdDriver-paper"></div>
    </center>
     <br>
    <button type="button" id="print"><?php echo __('Imprimir Todo') ?></button>
    <button type="button" id="send"><?php echo __('Enviar E-mail') ?></button>
    </div>
    <div id="divReport" >
    </div>    
</div>    