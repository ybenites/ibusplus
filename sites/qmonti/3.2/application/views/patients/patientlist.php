<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<style type="text/css"> 

    #btnNewPatient{
        margin-left: 39px; 
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }

    .btnListar label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }
    #dialogNewPatient fieldset{
        padding: 3px 7px 4px 7px;
    }

    .frmlabel {
        display: block;
        width: 170px;
        float: left;
        font-weight: bold;
        height: 12px;
        padding-top: 8px;
        font-size:12px;

    }
    legend, h3 {
        color: #0171B2;
        font-size: 12px;
        font-weight: bold;
    }

    .type_text_normal{

        width: 170px; /* una longitud definida ancho */
        height:15px;
        margin: 5px 0;
        float:left;
        padding-top: 8px;
    }

    .class_p{
        clear: left;
        float: left;
        margin: 1px 0;
    }


</style>

<script type="text/javascript"> 
    var listPatients = '/patients/patientlist/consults';    
    var savePatient = "/patients/patientlist/save" + jquery_params;
    var editPatient = "/patients/patientlist/getPersonById" + jquery_params;
    var trashPatient = "/patients/patientlist/delete"+jquery_params;
    var activePatient = "/patients/patientlist/active"+jquery_params;

    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Esta seguro que quiere desactivar este intérprete?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Esta seguro que quiere activar este intérprete?"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    
    $(document).ready(function(){
        
        $(".buttonSex").buttonset();
        $('#regiondefault').combobox();
        $("#selectCase").combobox();
        $("#selectAdjus").combobox();
        $("#idInsuranceCarriers_information").combobox();
        $(".buttonUnlimited").buttonset();
        $(".buttonUnlimitedAp").buttonset();
        $('.btnListar').buttonset();
        
        $('#dob').datepicker({
            
            dateFormat : 'mm/dd/yy',
            changeMonth: true,
            changeYear: true 
            
        });
        
        $('#accidentday').datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth:true,
            changeYear:true
           
        });
        
     
        
        $('#textunlimited').datepicker({
            dateFormat : 'mm/dd/yy',
            changeMonth:true,
            changeYear:true
            
        });
        
        $('[name=unlimited]').change(function(){
            $('#textunlimited').hide();
            if($(this).val()== '0'){
            
                $('#textunlimited').show();
            }
             
        });
        
        $('[name=unlimitedapp]').change(function(){
            $('#textunlimitedapp').hide();
            if($(this).val()== '0'){
            
                $('#textunlimitedapp').show();
            }
             
        });
        
        $('#dialogNewPatient').dialog({
            autoOpen:false,
            title:'<?php echo __("Paciente"); ?>',
            height:'auto',
            width:'400',
            modal:true,
            resizable:false,
            close:function(){
                $('#frmPatient').get(0).reset();
            },
            buttons:{
                '<?php echo __("Guardar"); ?>':function(){
                    $('#frmPatient').submit();
                },
            
                '<?php echo __("Cancelar"); ?>':function(){
                    $(this).dialog('close');
                    
                }
            }
        });
        
        $('#btnNewPatient').button({
            icons: {
                primary: "ui-icon-plus"
            }
        });   
            
        $('#addCase').button({
            icons:{
                primary : "ui-icon-plus"
                    
            },
            text:false
        });   
            
        $('#addAdjus').button({
            icons:{
                primary : "ui-icon-plus"
            } ,
            text:false
        });
        
        
        $('#btnNewPatient').click(function(){
            $('#dialogNewPatient').dialog('open');
        });
        
        
        frmNewPatient = $("#frmPatient").validate({
            rules: {  
                firstname: {required:true},
                lastname: {required:true},
                socialsecurity:{required:true},
                sex:{required:true},
                idInsuranceCarrier:{required:true},
                accidentday:{required:true},
                claimnumber:{required:true},
                selectCase:{required:true},
                selectAdjus:{required:true}
                
            },   
            messages: {  
                firstname: {required:'<?php echo __("Nombre es requerido") ?>'},
                lastname: {required:'<?php echo __("Apellido es requerido") ?>'},
                socialsecurity : {required:'<?php echo __("Seguridad Social es requerido") ?>'},
                sex : {required:'<?php echo __("Género es requerido") ?>'},
                idInsuranceCarrier : {required:'<?php echo __("Aseguradora es requerido") ?>'},
                accidentday : {required:'<?php echo __("Día de Incidente es requerido") ?>'},
                claimnumber : {required:'<?php echo __("Número de Caso es requerido") ?>'},
                selectCase : {required:'<?php echo __("Administrador de Caso es requerido") ?>'},
                selectAdjus : {required:'<?php echo __("Ajustador es requerido") ?>'}
                
                
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){
                var formulario= $("#frmPatient").serialize();
                showLoading = 1;
                $.ajax({
                    url: savePatient,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        var data=j_response.data;
                        console.log(data);
                        if (evalResponse(j_response)){
                            $("#grid-listpatients").trigger('reloadGrid');
                            $('#dialogNewPatient').dialog('close');
                            frmNewPatient.currentForm.reset();
                            
                        }
                    }
                })
            }
            
        });
        
        $('#grid-listpatients').jqGrid({
            url:listPatients,
            datatype: "json", 
            postData: {
                all: function(){
                    return $('#status').is(':checked');
                }
            },
            colNames:['<?php echo __("ID") ?>',
                '<?php echo __("Nombre") ?>',
                '<?php echo __("Apellidos") ?>',
                '<?php echo __("Seguridad Social") ?>',
                '<?php echo __("Fecha de Nacimiento") ?>',
                '<?php echo __("Empleado") ?>',
                '<?php echo __("Estado") ?>',
                '<?php echo __("Acciones") ?>'], 
            colModel:[ 
                {name:'idPatient',index:'idPatient', hidden: true,search:false,align:"center",width:40}, 
                {name:'firstname',index:'firstname',search:true,align:"center", width:150}, 
                {name:'lastname',index:'lastname',search:true,align:"center", width:150}, 
                {name:'socialsecurity',index:'socialsecurity',search:false,align:"center", width:70}, 
                {name:'dob',index:'dob',search:false,align:"center", width:120}, 
                {name:'employer',index:'employer',search:false,align:"center", width:100}, 
                {name:'status',index:'status',search:false, align:"center",width:150},
                {name:'actions',index:'actions', width:150, search:false,align:"center"}
                
            ],
            rowNum:100, 
            rowList:[100,200,300], 
            pager: '#grid-listpatients-paper', 
            sortname: 'firstname', 
            height: 200,
            width:1125,
            viewrecords: true, 
            sortorder: "desc", 
            caption: "<?php echo __("Pacientes") ?>",
            afterInsertRow:function(row_id, data_id){
                
                if(data_id.status==1){
                    edit = "<a class=\"edit\" data-oid=\""+row_id+"\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\""+row_id+"\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                    $("#grid-listpatients").jqGrid('setRowData',row_id,{actions:edit+trash,status: '<?php echo __("Activar") ?>'});  
                }
                else{
                    active = "<a class=\"active\" data-oid=\""+row_id+"\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
                    $("#grid-listpatients").jqGrid('setRowData',row_id,{actions:active, status: '<?php echo __("Inactivo") ?>'});
                }
                     
                
            },
            gridComplete:function(){
                $('.edit').button({
                    icons:{primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.trash').button({
                    icons:{primary: 'ui-icon-power'},
                    text: false
                })
                $('.active').button({
                    icons:{primary: 'ui-icon-arrowreturnthick-1-s'},
                    text: false
                })
            }
        });  
        
        jQuery("#grid-listpatients").jqGrid('inlineNav',"#grid-listpatients-paper"); 
        
        /*Busqueda*/
        $("#grid-listpatients").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
        
        //si hace el checked de los activos, recarga la pagina
        $('.btnListar input[name=status]').change(function(){
            $("#grid-listpatients").trigger("reloadGrid");
        });
        
        $("table#grid-listpatients").on('click','.edit',function(){
    
            $("#idPatient").val(($(this).data("oid")));
            var obj = new Object();
            obj.idPatient = $(this).data("oid");
            showLoading = 1; //para q salga q esta cargando los dx
            $.ajax({
                url: editPatient,
                type:"post",
                data:obj,
                dataType:"json",
                success: function(j_response){
                    if (evalResponse(j_response)){
                        var data=j_response.data;
                        var dataC= j_response.dataC;
                        console.log(dataC, data);
                
                        $("#firstname").val(data.firstname);
                        $("#lastname").val(data.lastname);
                        $("#socialsecurity").val(data.socialsecurity);
                        $('form#frmPatient input[name=sex]').val([data.sex]);
                        $('form#frmPatient input[name="sex"]:checked').trigger('change');
                        $("#address").val(data.address);
                        $("#apartmentnumber").val(data.apartmentnumber);
                        $("#city").val(data.city);
                        $("#state").val(data.state);
                        $("#zipcode").val(data.zipcode);
                        $("#phonenumber").val(data.phonenumber);
                        $("#altnumber").val(data.altnumber);
                        $("#dob").val(data.dob);
                        $("#employer").val(data.employer);
                        $("#idInsurancecarrier").val(dataC.idInsurancecarrier);
                        $("#authorizedby").val(dataC.authorizedby);
                        $("#accidentday").val(dataC.accidentday);
                        $("#claimnumber").val(dataC.claimnumber);
                        $("#injury").val(dataC.injury);
                        
                        $("#idInsuranceCarriers_information").val(dataC.idInsuranceCarrier);
                        $('#idInsuranceCarriers_information-combobox').val($('#idInsuranceCarriers_information option:selected').text());
                
                        $("#selectCase").val(dataC.idCasemanager);
                        $('#selectCase-combobox').val($('#selectCase option:selected').text());
                
                        $("#selectAdjus").val(dataC.idAdjuster);
                        $('#selectAdjus-combobox').val($('#selectAdjus option:selected').text());
                
                        $('form#frmPatient input[name=unlimited]').val([dataC.unlimited]);
                        $('form#frmPatient input[name="unlimited"]:checked').trigger('change');
                
                        $('form#frmPatient input[name=unlimitedapp]').val([dataC.unlimitedapp]);
                        $('form#frmPatient input[name="unlimitedapp"]:checked').trigger('change');
                
                        $( "#dialogNewPatient" ).dialog( "open" ); 
                    }
            
                }    
            })
        }),

        $("table#grid-listpatients").on('click','.trash',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmDeleteMessage,deleteSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:trashPatient,
                        type:"post",
                        data : {idPatient:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                        
                                $("#grid-listpatients").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
            return false
    
        }),


        $("table#grid-listpatients").on('click','.active',function(){
            var id=$(this).attr("data-oid");
            showLoading = 1; 
            confirmBox(confirmActiveMessage,activeSubTitle,function(response){
                if(response){                    
                    $.ajax({                      
                        url:activePatient,
                        type:"post",
                        data : {idPatient:id},
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                                $("#grid-listpatients").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });
    
        });    

        
        
    });


</script>
<div>
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Lista de Pacientes') ?></div>
    </center>
</div>
<div class="show_content">

    <button id="btnNewPatient" ><?php echo __('Nuevo Paciente') ?></button>

    <div class="btnListar">
        <input type="checkbox" id="status" name="status" />
        <label for="status"><?php echo __('Listar Todo') ?></label><br><br><br><br/>
    </div>

    <center>     
        <table id="grid-listpatients"></table>
        <div id="grid-listpatients-paper"></div>
    </center>

</div>

<div id="dialogNewPatient">
    <form id="frmPatient" name="frmPatient" method="POST">
        <div> 
            <fieldset>
                <legend><?php echo __('Crear Paciente') ?></legend>
                <input type="text" name="idPatient" id="idPatient" hidden="true"/>   

                <p class="class_p">
                    <label for="firstname" class="frmlabel"><?php echo __('Nombre') ?></label>
                    <input class="type_text_normal" type="text" id="firstname" name="firstname" id="firstname" />    
                </p>

                <p class="class_p">
                    <label for="lastname" class="frmlabel"><?php echo __('Apellido') ?></label>
                    <input class="type_text_normal" type="text" id="lastname" name="lastname" id="lastname" />   
                </p>

                <p class="class_p">
                    <label for="socialsecurity" class="frmlabel"><?php echo __('Seguridad Social') ?></label>
                    <input class="type_text_normal" type="text" id="socialsecurity" name="socialsecurity" id="socialsecurity" />    
                </p>

                <p class="class_p">
                    <label for="sex" class="frmlabel"><?php echo __('Género') ?></label>
                    <span class="buttonSex">
                        <input type="radio" id="sexM" name="sex" value="M"><label for="sexM"><?php echo __("M") ?></label>
                        <input type="radio" id="sexF" name="sex" value="F" /><label for="sexF"><?php echo __("F") ?></label>
                    </span>
                </p>

                <p class="class_p">
                    <label for="regiondefault" class="frmlabel"><?php echo __('Región') ?></label>
                    <select id="regiondefault" name="regiondefault">
                        <option value="FLORIDA"><?php echo __('FLORIDA') ?></option>
                        <option value="GEORGIA"><?php echo __('GEORGIA') ?></option>
                        <option value="OTRO"><?php echo __('OTRO') ?></option>
                    </select> 
                </p>

                <p class="class_p">
                    <label for="address" class="frmlabel"><?php echo __('Dirección') ?></label>
                    <input class="type_text_normal" type="text"  name="address" id="address" />    
                </p>

                <p class="class_p">
                    <label for="apartmentnumber" class="frmlabel"><?php echo __('Número de Apartamento') ?></label>
                    <input class="type_text_normal" type="text"  name="apartmentnumber" id="apartmentnumber" />    
                </p>

                <p class="class_p">
                    <label for="city" class="frmlabel"><?php echo __('Ciudad') ?></label>
                    <input class="type_text_normal" type="text"  name="city" id="city" />    
                </p>

                <p class="class_p">
                    <label for="state" class="frmlabel"><?php echo __('Estado') ?></label>
                    <input class="type_text_normal" type="text" id="state" name="state"  />    
                </p>

                <p class="class_p">
                    <label for="zipcode" class="frmlabel"><?php echo __('Codigo ZIP') ?></label>
                    <input class="type_text_normal" type="text" id="zipcode" name="zipcode"  />    
                </p>

                <p class="class_p">
                    <label for="phonenumber" class="frmlabel"><?php echo __('Télefono') ?></label>
                    <input class="type_text_normal" type="text"  name="phonenumber" id="phonenumber" />    
                </p>

                <p class="class_p">
                    <label for="altnumber" class="frmlabel"><?php echo __('Número Alternativo') ?></label>
                    <input class="type_text_normal" type="text"  name="altnumber" id="altnumber" />    
                </p>

                <p class="class_p">
                    <label for="dob" class="frmlabel"><?php echo __('Fecha de Nacimiento') ?></label>
                    <input class="type_text_normal" type="text"  name="dob" id="dob" />    
                </p>

                <p class="class_p">
                    <label for="employer" class="frmlabel"><?php echo __('Empleado') ?></label>
                    <input class="type_text_normal" type="text"  name="employer" id="employer" />    
                </p>

            </fieldset>



            <br/>  <fieldset>
                <legend><?php echo __('Información de Aseguradora y Casos') ?></legend>

                <p class="class_p">
                    <label for="InsuranceCarriers" class="frmlabel "><?php echo __('Compañía de Seguros') ?>:</label>
                    <select name="idInsuranceCarriers_information" id="idInsuranceCarriers_information">
                        <option value=""><?php echo __('Seleccionar') ?>...</option>
                        <?php foreach ($insurance as $insu) { ?> 
                            <option value="<?php echo $insu['idInsurancecarrier'] ?>"> <?php echo $insu['name'] ?> </option>
                        <?php }; ?>
                    </select>  
                </p>

                <p class="class_p">
                    <label for="authorizedby" class="frmlabel"><?php echo __('Autorizado por') ?></label>
                    <input class="type_text_normal" type="text" id="authorizedby" name="authorizedby"  />    
                </p>

                <p class="class_p">
                    <label for="accidentday" class="frmlabel"><?php echo __('Fecha de Incidente') ?></label>
                    <input class="type_text_normal" type="text" id="accidentday" name="accidentday"  />    
                </p>

                <p class="class_p">
                    <label for="claimnumber" class="frmlabel"><?php echo __('N{umero de Caso') ?></label>
                    <input class="type_text_normal" type="text" id="claimnumber" name="claimnumber"  />    
                </p>

                <p class="class_p">
                    <label for="injury" class="frmlabel"><?php echo __('Lesión') ?></label>
                    <input class="type_text_normal" type="text" id="injury" name="injury"  />    
                </p>

                <p class="class_p">
                    <label for="unlimited" class="frmlabel"><?php echo __('Fecha Ilimitada') ?></label>
                    <span class="buttonUnlimited">
                        <input type="radio" id="yesUn" name="unlimited" checked =" true" value="1"><label for="yesUn"><?php echo __("Si") ?></label>
                        <input type="radio" id="falseUn" name="unlimited" value="0" /><label for="falseUn"><?php echo __("No") ?></label>
                        <input hidden="true" style=" margin: 4px;" type="text" id="textunlimited" name="textunlimited"  />   
                    </span>
                </p>

                <p class="class_p">
                    <label for="unlimitedapp" class="frmlabel"><?php echo __('Cita Ilimitada') ?></label>
                    <span class="buttonUnlimitedAp">
                        <input type="radio" id="yesUnAp" name="unlimitedapp" checked="true" value="1"><label for="yesUnAp"><?php echo __("Si") ?></label>
                        <input type="radio" id="falseUnAp" name="unlimitedapp" value="0" /><label for="falseUnAp"><?php echo __("No") ?></label>
                        <input hidden="true" style=" margin: 4px;" type="text" id="textunlimitedapp" name="textunlimitedapp"  />   
                    </span>
                </p>

                <p class="class_p">
                    <label for="selectCase" class="frmlabel"><?php echo __('Administrador de Caso') ?></label>
                    <select name="selectCase" id="selectCase">
                        <option value=""><?php echo __('Seleccionar'); ?>...</option>   
                        <?php foreach ($adminCasos as $case) { ?>
                            <option value="<?php echo $case['idCasemanager'] ?>"><?php echo $case['name'] ?></option>
                        <?php }; ?>
                    </select>
                    <button type="button" id="addCase" name="addCase"><?php echo __("Agregar") ?></button>
                </p>

                <p class="class_p">
                    <label for="selectAdjus" class="frmlabel"><?php echo __('Ajustador') ?></label>
                    <select name="selectAdjus" id="selectAdjus">
                        <option value=""><?php echo __('Seleccionar'); ?>...</option>   
                        <?php foreach ($adjuster as $adj) { ?>
                            <option value="<?php echo $adj['idAdjuster'] ?>"><?php echo $adj['name'] ?></option>
                        <?php }; ?>
                    </select>
                    <button type="button" id="addAdjus" name="addAdjus"><?php echo __("Agregar") ?></button>
                </p>

            </fieldset>

        </div>

    </form>

</div>



