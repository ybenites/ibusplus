
<style type="text/css"> 

    .main-body {
        border: 1.5px solid #E5E5E3;
        border-radius: 6px 6px 6px 6px;
        color: #222222;
        /*margin: 4em 13em 5em 13em;*/
        height: auto;
        width:100%; /*ancho*/
        padding: 0px;
    }
    .class_p{
        clear: left;
        margin: 5px 0;
    }

    #until_app{
        float: left;
        margin: 3px;
    }
    #combotypeservice{
        margin-left: 130px;
        margin-top: 0;
    }

    .main-inbody {
        background: none repeat-x scroll 50% 0 #FEFEFE;
        border: 4.5px solid #E5E5E3;
        border-radius: 6px 6px 6px 6px;
        color: #222222;
        height: auto;
        margin: -1px;
        padding: 10px;
        width: auto;
    }

    .frmlabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 5px;
        width: 120px;
    }

    .buttonsCheck{

        padding-top: 27px;
    }
    .buttonsCheck_type
    {    padding-left: 130px;
         padding-top: 1px;
    }

    .buttonsCheck_days{
        padding-top: 18px;
    }

    .buttonsCheck_INF_SERV{
        padding-top: 2px;
    }

    .type_text_long{

        width: 240px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;

    }

    .type_text_normal{
        width: 120px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;

    }

    #formNew fieldset{
        width:408px; /*ancho*/
        height:187px;
        padding: 3px 2px 4px 7px;

    }

    select#type{
        width: 200px; /* una longitud definida ancho */
        height:25px;
        padding: 3px 2px 4px 5px;
        margin: 5px 0;
    }

    #newappointment fieldset{
        padding: 3px 2px 4px 7px;
        width: 415px;
    }

    #newappointment_claim fieldset{
        height: 180px;
        margin-right: 16px;
        width: 514px;
        padding: 3px 2px 4px 7px;
    }

    #newappointment_insurance fieldset{
        height: 148px;
        margin-right: 13px;
        margin-top: -147px;
        width: 516px;
        padding: 3px 2px 4px 7px;
    }

    #newappointment_appinfo fieldset{
        height: auto;
        margin-top: 4px;
        padding: 3px 2px 4px 7px;
        width: 1042px;
    }

    #newappointment_callinfo fieldset{
        width: 450px;
        height: 145px;
    }

    #newappointment_servinfo fieldset
    {
        height: auto;
        width: 422px;

    }

    #newappointment_pickupinfo fieldset
    {   height: auto;
        width: 452px;
    }

    #showbytransportation fieldset{
        height: auto;
        margin-left: -1px;
        margin-right: 8px;
        margin-top: 55px;
        padding: 3px 2px 4px 7px;
        width: 163px;
    } 
    #showbytransportation2 fieldset{
        height: auto;
        margin-left: 262px;
        margin-right: -3px;
        margin-top: -276px;
        padding: 3px 2px 4px 7px;
        width: 163px;

    } 

    #newappointment_pickupinfo{
        float:right;
    }

    #newappointment_callinfo{
        float:right;

    }
    #newappointment_claim{
        float:right;
    }

    #newappointment{
        float:left;
    }
    #newappointment_insurance{
        float:right;        
    }
    #newappointment_appinfo{
        float:left;  
    }
    #appinfo{
        float:left;  
    }
    #newappointment_servinfo
    {float:left; 
    }

    #showbytransportation{
        float:left; 
    }
    #showbytransportation2{
        float: left; 
    }

    #transp{
        float: left;
    }

    #trasl{
        float: left;
    }
    #showTypeMiles,#notesp{
        float: left;

    }

    .btnprueba label{
        margin-bottom: 25px;
        float:left;
        font-size: 12px;

    }
    .valclaim{
        padding:5px;
    }
</style>


<script type="text/javascript">

    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de desactivar el caso?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Está seguro de activar el caso?"); ?>';
    var newttitle = '<?php echo __("Nuevo Tipo de Cita"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    var titleEditClaim = "<?php echo __("Editar Caso"); ?>";
    var nuevacita = "<?php echo __("Nueva Cita"); ?>";
    var findId = "/patients/appointmentsheet/getId" + jquery_params;
    var findClientAppointment = "/patients/appointmentsheet/getIdNewAppointment" + jquery_params;
    var saveClaim = "/patients/appointmentsheet/save" + jquery_params;
    var listclaimsbyuser = '/patients/appointmentsheet/consult';
    var listappointmentbyclaimlist = '/patients/appointmentsheet/consult2';
    var delClaim = '/patients/appointmentsheet/delete';
    var saveAppointment = '/patients/appointmentsheet/saveappointment' + jquery_params;
    var url_createUpdateTypeappointment = '/patients/appointmentsheet/createUpdateTypeappointment' + jquery_params;
    var validTypeappointment = '<?php echo __('Nombre Motivo de Cita es Requerido'); ?>';
    var titleNewDrivers = '<?php echo __('Nuevo Conductor') ?>';
    var titleNewAreaDrivers = '<?php echo __('Nueva Area de Conductor') ?>';
    var saveAreadrivers = '/patients/appointmentsheet/saveAreadrivers' + jquery_params;
    var saveDrivers = '/patients/appointmentsheet/saveDrivers' + jquery_params;
    var findIdEditApp = "/patients/appointmentsheet/getIdUpdateAppointment" + jquery_params;
    var delAppointment = "/patients/appointmentsheet/delete_appointment" + jquery_params;
    var saveLanguages = "/patients/appointmentsheet/savelanguaje" + jquery_params;
    var saveLocation = "/patients/appointmentsheet/saveLocation" + jquery_params;
    var saveCourse = "/patients/appointmentsheet/saveCourse" + jquery_params;
    var expandClaim = "/patients/appointmentsheet/expandClaim" + jquery_params;
    var expandApp = "/patients/appointmentsheet/expandClaim" + jquery_params;

    $(document).ready(function() {
        
        $('#drawroute,#routesend').button();    
        $('.background').hide();
        $("#newappointment_claim").children().prop('disabled',true);
        $("#newappointment").children().prop('disabled',true);
        $("#newappointment_insurance").children().prop('disabled',true);
        //$("#newappointment_claim").children().prop('disabled',true);
        
        $('#createapp,#date_finish_claims,#max_appointment_claims,#date_finish,\n\
           #max_appointment,#newappointment,#newappointment_claim,#newappointment_insurance,\n\
           #newappointment_appinfo,#hasta,#transp,#trasl,\n\
           #showbytransportation,#showbytransportation2,#newAppointment,#showTypeMiles,#waiting_time,\n\
           #b_liftBase,#b_cassistance,#b_wtime,#newClaim').hide();
        
        //COMBOBOX
        $('#case_manager_id,#adjuster_id,#insurance_carrier,#case_manager_id_claim,\n\
        #adjuster_id_claim,#driver1,#driver2,#languaje_trasl,#interprete_trasl,#typeapp_appinfo,\n\
        #appdescription_appinfo').combobox();
        $('#typeservice_trasl').combobox({selected: function(){
                if(($('#typeservice_trasl').val())=="Interpreting" && $('input[name=transportation]').is(":checked")==false && $('input[name=traduction]').is(":checked")==true){
                    $('#transp').hide();
                    $('#showbytransportation').show();
                    $('#showbytransportation2').hide(); 
                    $('#showTypeMilesNotes').show();
                }}});
        //FIN COMBOBOX
       
        //DATE PICKER
        $('#date_finish_claims,#dateregistration,#dateregistration_appinfo,#insurance_date_driver,#until_app,#addmaxDate,#addmaxDateApp').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        // FIN DATE PICKER
        
        //BOTONES
        $('.buttonsCheck,.buttonsCheck_type,.buttonsCheck_days,.buttonsCheck_INF_SERV,#typetravelp').buttonset();
        $('#fsearch,#tsearch').button({icons: {primary: 'ui-icon-search'},text: false});
        $('#newClaim').button({icons: {primary: 'ui-icon-plus'}});
        $('#newAppointment').button({icons: { primary: 'ui-icon-plus' } });
        $('#newAppDescription').button({ icons: { primary: 'ui-icon-plus'},text: false});
        $('#NewDriver').button({icons: { primary: 'ui-icon-plus' } , text: false });
        $('#NewLanguage').button({icons: { primary: 'ui-icon-plus'} , text: false});
        $('#NewInterprete').button({icons: {primary: 'ui-icon-plus'} , text: false});
        $('#expandNumClaims,#expandMaxDate,#onlyAppointment,#expandTo,#expandToApp,#maps,#createapp').button({
            text: false
        });

        //FIN BOTONES
        //eventos de CLICK
        $("#newClaim").click(function(){$("#claim").dialog("open");});
        $("#newAppDescription").click(function() {$("#frmNew_apptype").dialog("open");});
        $("#NewDriver").click(function() {$("#formNew_driver").dialog("open");});
        $("#addarea").click(function() {$("#dialogo-areadriver").dialog("open");});
        $("#NewLanguage").click(function() {$("#dialogo-language").dialog("open");});
        
               
        $("#AMBULATORY").click(function() {
            $('input[name=for_wtime]').prop('checked', false).trigger('change');
            $('input[name=for_lbase]').prop('checked', false).trigger('change');
            $('input[name=for_cassistance]').prop('checked', false).trigger('change');
            $('#b_wtime').show();
            $('#b_liftBase,#b_cassistance').hide();
   
        });
        
        $("#WHEELCHAIR").click(function() {
            $('input[name=for_wtime]').prop('checked', false).trigger('change');
            $('input[name=for_lbase]').prop('checked', false).trigger('change');
            $('input[name=for_cassistance]').prop('checked', false).trigger('change');
            $('#b_liftBase,#b_cassistance,#b_wtime').show();
            
        });
        $("#STRETCHER").click(function() {
            $('input[name=for_wtime]').prop('checked', false).trigger('change');
            $('input[name=for_lbase]').prop('checked', false).trigger('change');
            $('input[name=for_cassistance]').prop('checked', false).trigger('change');            
            $('#b_liftBase,#b_cassistance,#b_wtime').show();
            
        });                                 
              
        $('input[name=for_wtime]').change(function() {
            if ($('#wtime').is(':checked') == true) {
                $('#waiting_time').show();
                
            } else {
                $('#waiting_time').hide();
            }
        });
        
               
        $('#expandTo').click(function() {
            $.ajax({
                url: expandClaim,
                type: "post",
                data: {maxDate: $('#addmaxDate').val(),
                    idClaim: $('#idClaim').val()},
                dataType: "json",
                success: function(j_response) {
                    $("#divExpandDate").dialog('close');
                    $('#newappointment').show();
                    $('#newappointment_claim').show();
                    $('#newappointment_insurance').show();
                    $('#newappointment_appinfo').show();
                    $('#createapp').show();
                }
            });
        });
        
        $('#expandToApp').click(function() {
            $('#divExpandDateApp').dialog('close');
            $('#newappointment').show();
            $('#newappointment_claim').show();
            $('#newappointment_insurance').show();
            $('#newappointment_appinfo').show();
            $('#createapp').show();
        });
        
        $("#newAppointment").click(function() {
            
        $('.background').show();
            $.ajax({
                url: "/patients/appointmentsheet/getActiveClaim",
                dataType: "json",
                type: 'post',
                data: 'idClaim=' + $("#idClaim").val(),
                success: function(j_response) {
                    var data = j_response.data;
                    var flag = data.flag;
                    
                    var id = $('#idPatient_app').val()
                    var id_claim = $('#idClaim_app').val();
                    var id_Insurancecarrier = $('#idInsurancecarrier_app').val();
                    var id_Casemanager = $('#idCasemanager_app').val();
                    var id_Adjuster = $('#idAdjuster_app').val();
                    showLoading = 1;
                    $.ajax({
                        url: findClientAppointment,
                        type: "post",
                        data:
                            {idPatient_app: id,
                            idClaim_app: id_claim,
                            idAdjuster_app: id_Adjuster,
                            idCasemanager_app: id_Casemanager,
                            idInsurancecarrier_app: id_Insurancecarrier
                        },
                        dataType: "json",
                        success: function(j_response) {
                            if (evalResponse(j_response)) {
                                var data = j_response.data;
                                var data_claim = j_response.data_claim;
                                var data_insurancecarrier = j_response.data_insurancecarrier;
                                var data_casemanager = j_response.data_casemanager;
                                var data_adjuster = j_response.data_adjuster;
                                $("#idPatient_app").val(data.idPatient);
                                $("#name_patient").val(data.firstname + ' ' + data.lastname);
                                $("#address_patient").val(data.address);
                                $("#socialsecurity_patient").val(data.socialsecurity);
                                $("#apartmentnumber_patient").val(data.apartmentnumber);
                                $("#phone_patient").val(data.phonenumber);
                                $("#city_patient").val(data.city);
                                $("#altnumber_patient").val(data.altnumber);
                                $("#state_patient").val(data.state);
                                $("#dob_patient").val(data.dob);
                                $("#zipcode_patient").val(data.zipcode);
                                $("#regiondefault_patient").val(data.regiondefault);
                                $("#claimnumber_app").val(data_claim.claimnumber);
                                $("#authorizedby_app").val(data_claim.authorizedby);
                                $("#dateregistration_app").val(data_claim.dateregistration);
                                $("#injury_app").val(data_claim.injury);
                                $("#Insurancecarrier_name_app").val(data_insurancecarrier.name);
                                $("#Casemanager_name_app").val(data_casemanager.name);
                                $("#Adjuster_name_app").val(data_adjuster.name);
                                $("#companyname_app").val(data_insurancecarrier.name);
                                $("#companyphone_app").val(data_insurancecarrier.phone);
                                if(data_claim.unlimited==1)
                                {$("#Fecha-Maxima").val('unlimited');}
                                else if (data_claim.unlimited==0)
                                {$("#Fecha-Maxima").val(data_claim.dateFinish);}
                            }
                        }
                    });
                    if (flag == '1') {
                        $('#newappointment').show();
                        $('#newappointment_claim').show();
                        $('#newappointment_insurance').show();
                        $('#newappointment_appinfo').show();
                        $('#createapp').show();
                    }
                    else
                    { $('#divAppointmentAdd').dialog('open');  }
                }
            });
        });
        
        $('#createapp').click(function() {

            frmappointment = $("#validationappointment").validate({
                rules: { 
                    dateregistration_appinfo:{
                        required:true
                    },    
                    typeapp_appinfo:{
                        required:true
                    },   
                    appointmenttime_appinfo:{
                        required:true
                    },    
                    appdescription_appinfo:{
                        required:true
                    },    
                    whocalls_app:{
                        required:true
                    },    
                    companyname_app:{
                        required:true
                    },    
                    companyphone_app:{
                        required:true
                    } 
 
                },
                messages: {
                    dateregistration_appinfo:{
                        required:'<?php echo __("Fecha de Servicio es requerido") ?>'
                    },    
                    typeapp_appinfo:{
                        required:'<?php echo __("Tipo de Cita es requerido") ?>'
                    },   
                    appointmenttime_appinfo:{
                        required:'<?php echo __("Hora de Cita es requerido") ?>'
                    },    
                    appdescription_appinfo:{
                        required:'<?php echo __("Descripción de Cita es requerido") ?>'
                    },    
                    whocalls_app:{
                        required:'<?php echo __("Llamado es requerido") ?>'
                    },    
                    companyname_app:{
                        required:'<?php echo __("Compañia es requerido") ?>'
                    },    
                    companyphone_app:{
                        required:'<?php echo __("Teléfono es requerido") ?>'
                    }
                    
                
                },
                errorContainer: '#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {
                    $(element).addClass('ui-state-error');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('ui-state-error');
                },
                submitHandler: function(form) {
                    if ($('#traduction').val() == '1' && $('#typeservice_trasl').val()=="Interpreting") {
                        var data_location = 'alias=' + $('#location1').val() + '&address=' + $('#location2').val() + '&unit=' + $('#unit').val() + '&phone=' + $('#phone_showbytransportation').val() + '&idPatient_app=' + $('#idPatient_app').val() + '&idLocation=' + $('#idLocation1').val();
                        $.ajax({
                            url: saveLocation,
                            type: "post",
                            data: data_location,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idLocation1').val(data.idLocation);
                                $('#idLocation_newappointment_appinfo').val(data.idLocation);
                            }
                        });
                    }
                    if ($('#transportation').val() == '1') {
                        var data_location = 'alias=' + $('#contact').val() + '&address=' + $('#address').val() + '&unit=' + $('#unit_showbytransportation2').val() + '&phone=' + $('#phone_showbytransportation2').val() + '&idPatient_app=' + $('#idPatient_app').val() + '&idLocation=' + $('#idLocation2').val();
                        $.ajax({
                            url: saveLocation,
                            type: "post",
                            data: data_location,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idLocation2').val(data.idLocation);
                            }
                        });
                        var data_course = 'idLocationDestination=' + $('#idLocation2').val() + '&idLocationOrigen=' + $('#idLocation1').val() + '&fmiles=' + $('#fmiles').val();
                        $.ajax({
                            url: saveCourse,
                            type: "post",
                            data: data_course,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idCourse_newappointment_appinfo').val(data.idCourse);
                            }
                        });
                    }
                    var formulario = $(form).serialize();
                    console.log(formulario);
                    $.ajax({
                        url: saveAppointment,
                        type: "post",
                        data: formulario,
                        async: false,
                        dataType: "json",
                        success: function(j_response) {
                            if (evalResponse(j_response)) {
                                var data = j_response.data;
                                $('.background').hide();
                                
                            }} });} }); });
                    
        $("a#routesend").click(function(e) {
            $("#fmiles").attr("value", $("#txtdistance").attr("value"));
            $("#location2").attr("value", $("#txtfcity").attr("value"));
            $("#address").attr("value", $("#txttcity").attr("value"));
            $("#frmMap").dialog('close');
            $("#oneway").prop('checked', true);
        });
        
        $("#maps").click(function(e)
        {
            $("#searchfaddress").val( $("#location2").val());
            $("#searchtaddress").val( $("#address").val());
            $("#frmMap").dialog('open');
            initialize();
            e.preventDefault();
        });
        
        $('#expandNumClaims').click(function() {
            $("#divAppointmentAdd").dialog('close') 
            $("#formNew").dialog('open');
        });
        
        $('#expandMaxDate').click(function() {
            $("#divAppointmentAdd").dialog('close')  
            $("#divExpandDate").dialog('open');
        });
        
        $('#onlyAppointment').click(function() {
            $("#divAppointmentAdd").dialog('close')  
            $("#divExpandDateApp").dialog('open');
             
        });
        //FIN EVENTOS CLICK
         
         
        $('input[name=repeatapp]').change(function() {
            if (($(this).is(":checked")) == true)
            {$('#hasta').show();}
            else
            {$('#hasta').hide();}

        });
                
        $('#typeservice_trasl ,input[name=transportation],input[name=traduction]').change(function() {
            //        $('input[name=for_typeservice]').prop('checked', false).trigger('change');
            //        $('#driver1-combobox').val('');
            //        $('#driver2-combobox').val('');
            //        $('#flatrate_serv_inf').val('');
            //        $('#typeservice_trasl-combobox').val('');
            //        $('#languaje_trasl-combobox').val('');
            //        $('#interprete_trasl-combobox').val('');
            if($('#typeservice_trasl').val()=="Interpreting"){
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show(); 
                $('#showTypeMilesNotes').show();
            }
        
            if(($('input[name=traduction]').is(":checked"))==true) {
                $('#trasl').show();
                $('#valtraduction').val("1");
                $('#traduction').val("1");
            }

            if (($('input[name=traduction]').is(":checked")) == false){
                $('#typeservice_trasl').val(" ");
                $('#trasl').hide();
                $('#valtraduction').val("0");
                $('#traduction').val("0");
            }

            if (($('input[name=transportation]').is(":checked")) == true){
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#valtransportation').val("1");
                $('#transportation').val("1");
                $('#showTypeMiles').show();
                $('#maps').show();
            }

            if (($('input[name=transportation]').is(":checked")) == false)
            {
                $('#transp').hide();
                $('#valtransportation').val("0");
                $('#transportation').val("0");
                $('#showbytransportation').hide();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
                $('#maps').hide();
            }

            if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=traduction]').is(":checked")) == true) {
                $('#trasl').show();
                $('#showbytransportation').show();
            }
            if ((($('#typeservice_trasl').val()) != "Interpreting") && ($('input[name=transportation]').is(":checked")) == true && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#trasl').show();
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#showTypeMiles').show();
            }
            
            if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=transportation]').is(":checked")) == false && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#showbytransportation').show();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
                $('#maps').hide();
            }});
     
        $("#frmMap").dialog({
            autoOpen: false,
            height: 680,
            width: 820,
            modal: true
        });


        $('input[name=roundtrip]').change(function() {
            if ($(this).val() == 'RoundTravel') {
                $('#fmiles').val($('#fmiles').val() * 2);
            } else {
                $('#fmiles').val($('#fmiles').val() / 2);
            }
        });

        $('input[name=date_finish]').change(function() {
            if ($(this).val() == '1')
                $('#date_finish_claims').hide();

            else {
                $('#date_finish_claims').show();
                $('#date_finish_claims').val('');
            }
        });
        
        $('input[name=max_appointment]').change(function() {
            if ($(this).val() == '1')
                $('#max_appointment_claims').hide();
            else
            {
                $('#max_appointment_claims').show();
                $('#max_appointment_claims').val('');
            }
        });
        
        $('input[name=date_finish_claim]').change(function() {

            $('#date_finish').hide();
            if ($(this).val() == '0')
            {
                $('#date_finish').show();
            }
            if ($(this).val() == '1')
            {
                $('#date_finish').hide();
                $('#date_finish_claims').val("");
            }
        });
        
        $('input[name=max_appointment_claim]').change(function() {
            $('#max_appointment').hide();
            if ($(this).val() == '1')
            {
                $('#max_appointment').show();
            }
        });
        
        /********************** autocomplete para buscar pacientes *****************************/

        $("#namepatient").autocomplete({
            //define callback to format results
            source: function(req, response) {
                $.ajax({
                    url: "/patients/appointmentsheet/search",
                    dataType: "json",
                    type: 'post',
                    data: {
                        namepatient: $('#namepatient').val()
                    },
                    success: function(j_response) {
                        response(
                        $.map(j_response.data, function(item) {
                            return {
                                label: item.firstname + " " + item.lastname,
                                value: item.firstname + " " + item.lastname,
                                idPatient: item.idPatient
                            }
                        })
                    );
                    }
                });
            },
            
            select: function(e, ui) {
                $("#idPatient").val(ui.item.idPatient);
                $("#idPatientClaim").val(ui.item.idPatient);
                $("#idPatient_app").val(ui.item.idPatient);
                $('#newClaim').show();
                $("#grid-claimbyuserlist").trigger("reloadGrid");
            },
            
            change: function(event, ui) {
                if (!ui.item) {
                    
                    $(this).val("");
                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);
        }

        /************************************ AUTOCOMPLETE PARA LOCATION 1 *******************************************/
        $("#location1").autocomplete({
            //define callback to format results
            source: function(req, response) {
                
                $.ajax({
                    url: "/patients/appointmentsheet/search_location",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idContact: $('#idLocation2').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        response(
                        $.map(j_response.data, function(item) {
                            
                            return {
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias
                            }
                        })
                    );
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation1").val(ui.item.idLocation);
                $('#location2').val(ui.item.address);
                $('#unit').val(ui.item.suite);
                $('#phone_showbytransportation').val(ui.item.telephone);
                
            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {

                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.alias + " " + item.label + "</a>")
            .appendTo(ul);
        }

        /************************************* AUTOCOMPLETE PARA CONTACT *********************************/

        $("#contact").autocomplete({
            //define callback to format results
            source: function(req, response) {
                $.ajax({
                    url: "/patients/appointmentsheet/search_contact",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idLocation: $('#idLocation1').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        response(
                        $.map(j_response.data, function(item) {

                            return {
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias
                            }
                        })
                    );
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation2").val(ui.item.idLocation);
                $('#address').val(ui.item.address);
                $('#unit_showbytransportation2').val(ui.item.suite);
                $('#phone_showbytransportation2').val(ui.item.telephone);
                
            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {
                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.alias + " " + item.label + "</a>")
            .appendTo(ul);
        }

        /************************************* Dialog Nuevo Claim  ******************************************/
        $("#claim").dialog({
            autoOpen: false,
            title: titleEditClaim,
            height: 420,
            width: 450,
            modal: true,
            close: function() {
                $('#validationclaim').get(0).reset();
            },
            buttons: {
<?php echo __('Guardar') ?>: function() {
                if (!$('#validationclaim').valid()) {
                    return false;
                }
                $('#validationclaim').submit();
                $("#grid-claimbyuserlist").trigger("reloadGrid");
            },
<?php echo __('Cancelar') ?>: function() {
            $(this).dialog("close");
        }
    }


});
$('#validationclaim').validate({
    rules: {
              
        dateregistration: {required: true},
        claimnumber: {required: true},
        case_manager_id_claim: {required: true},
        insurance_carrier: {required: true},
        adjuster_id_claim: {required: true}
    },
    messages: {
        case_manager_id_claim: {required: '<?php echo __("Administrador de Caso es requerido") ?>'},
        adjuster_id_claim: {required: '<?php echo __("Ajustador es requerido") ?>'},
        claimnumber: {required: '<?php echo __("Número de Caso es requerido") ?>'},
        dateregistration: {required: '<?php echo __("Fecha de Caso es requerido") ?>'},
        insurance_carrier: {required: '<?php echo __("Aseguradora es requerido") ?>'}
    },
    errorContainer: '#errorMessages',
    errorLabelContainer: "#errorMessages .content .text #messageField",
    wrapper: "p",
    highlight: function(element, errorClass) {
        $(element).addClass('ui-state-error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass('ui-state-error');
    },
    submitHandler: function() {
        showLoading = 1;
        $.ajax({
            url: saveClaim,
            type: "post",
            data: {
                idClaim: $('#idClaim_nuevo').val()
                , idInsuranceCarrier: $('#insurance_carrier').val()
                , idPatient: $('#idPatientClaim').val()
                , dateregistration: $('#dateregistration').val()
                , claimnumber: $('#claimnumber').val()
                , authorizedby: $('#authorizedby').val()
                , injury: $('#injury').val()
                , idCasemanager: $('#case_manager_id_claim').val()
                , idAdjuster: $('#adjuster_id_claim').val()
                , unlimited: $('input[name=date_finish_claim]:checked').val()
                , unlimitedapp: $('input[name=max_appointment_claim]:checked').val()
                , datefinish: $('#date_finish').val()
                , maxappointments: $('#max_appointment').val()

            },
            dataType: "json",
            success: function(r) {

                if (evalResponse(r))
                {
                    $("#grid-claimbyuserlist").trigger('reloadGrid');
                    $("#claim").dialog('close');
                }

            }
        });
    }

});
        
                /************************************* Dialog Editar Claim ******************************************/
                $("#formNew").dialog({
                    autoOpen: false,
                    title: titleEditClaim,
                    height: 300,
                    width: 450,
                    modal: true,
                    close: function() {
                        $('#validation').get(0).reset();
                    },
                    buttons: {
<?php echo __('Guardar') ?>: function() {
        if (!$('#validation').valid()) {
            return false;
        }
        $('#validation').submit();
        $("#grid-claimbyuserlist").trigger("reloadGrid");
    },
<?php echo __('Cancelar') ?>: function() {
    $(this).dialog("close");
}
}
});
        
$('#validation').validate({
rules: {
},
messages: {
},
errorContainer: '#errorMessages',
errorLabelContainer: "#errorMessages .content .text #messageField",
wrapper: "p",
highlight: function(element, errorClass) {
$(element).addClass('ui-state-error');
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass('ui-state-error');
},
submitHandler: function() {
showLoading = 1;
$.ajax({
    url: saveClaim,
    type: "post",
    data: {
        idClaim: $('#idClaim').val()
        , idCasemanager: $('#case_manager_id').val()
        , idAdjuster: $('#adjuster_id').val()
        , unlimited: $('input[name=date_finish]:checked').val()
        , unlimitedapp: $('input[name=max_appointment]:checked').val()
        , datefinish: $('#date_finish_claims').val()
        , maxappointments: $('#max_appointment_claims').val()

    },
    dataType: "json",
    success: function(r) {

        if (evalResponse(r))
        {
            $("#grid-claimbyuserlist").trigger('reloadGrid');
            $("#formNew").dialog('close');
        }
    }
});
}
});
        
/************************************ jgrid Claims By User List **************************************/
$("#grid-claimbyuserlist").jqGrid({
url: listclaimsbyuser,
datatype: "json",
postData: {
all: function() {
    return $('#status').is(':checked');
},
id: function() {
    return $('#idPatient').val();
}


},
colNames: ['<?php echo __('ID'); ?>', 
'<?php echo __('Estado'); ?>', 
'<?php echo __('Número de Caso'); ?>', 
'<?php echo __('Lesión'); ?>', 
'<?php echo __('Fecha Final'); ?>', 
'<?php echo __('Aseguradora'); ?>', 
'<?php echo __('Actions'); ?>'],
colModel: [
{name: 'idClaim', index: 'idClaim', hidden: true, width: 50},
{name: 'status', index: 'status', search: true, width: 100},
{name: 'claimnumber', index: 'claimnumber', width: 100, search: false},
{name: 'injury', index: 'injury', width: 300, search: false},
{name: 'datefinish', index: 'datefinish', width: 100, search: false},
{name: 'name', index: 'name', width: 300, search: false},
{name: 'actions', index: 'actions', width: 200, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 'auto',
            width: 1200,
            pager: 'grid-claimbyuserlist-paper',
            sortname: 'idClaim',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Lista de Casos por Paciente'); ?>",
            afterInsertRow: function(row_id, data_id) {

if (data_id.status == 1) {

    edit = "<a class=\"edit\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
    trash = "<a class=\"trash\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
    active = "<a class=\"active\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Activar'); ?>\" ><?php echo __('Activar'); ?></a>";
    $("#grid-claimbyuserlist").jqGrid('setRowData', row_id, {actions: active + edit + trash, status: 'Activo'});
}
},
gridComplete: function() {
$('.edit').button({
    icons: {primary: 'ui-icon-pencil'},
    text: false
})
$('.trash').button({
    icons: {primary: ' ui-icon-power'},
    text: false
})

$('.active').button({
    icons: {primary: 'ui-icon-check'},
    text: false
})
}
});
jQuery("#grid-claimbyuserlist").jqGrid('navGrid', "#grid-listarea-paper", {edit: false, add: false, del: false});
jQuery("#grid-claimbyuserlist").jqGrid('inlineNav', "#grid-listarea-paper");
        
        
        $("table#grid-claimbyuserlist").on('click', '.edit', function() {
            var id = $(this).data("oid");
            showLoading = 1;
            $.ajax({
                url: findId,
                type: "post",
                data: {idClaim: id},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        $("#idClaim").val(data.idClaim);
                        $("#case_manager_id").val(data.idCasemanager);
                        $('#case_manager_id-combobox').val($('#case_manager_id option:selected').text());
                        $("#adjuster_id").val(data.idAdjuster);
                        $('#adjuster_id-combobox').val($('#adjuster_id option:selected').text());
                        $('input[name=date_finish]').val([data.unlimited]);
                        $('input[name=date_finish]:checked').trigger('change');
                        $('input[name=max_appointment]').val([data.unlimitedapp]);
                        $('input[name=max_appointment]:checked').trigger('change');
                        $("#date_finish_claims").val(data.datefinish);
                        $("#max_appointment_claims").val(data.maxappointments);
                        $('#formNew').dialog('open');
                    }

}
})
});
        
$("table#grid-claimbyuserlist").on('click', '.trash', function() {

var id = $(this).data("oid");
confirmBox(confirmDeleteMessage, deleteSubTitle, function(response) {
if (response) {
    showLoading = 1;
    $.ajax({
        url: delClaim,
        type: "post",
        data: {id: id},
        dataType: "json",
        success: function(j_response) {
            if (evalResponse(j_response)) {

                $("#grid-claimbyuserlist").trigger('reloadGrid');
            }
        }
    });
}
});
return false
});
        
$("table#grid-claimbyuserlist").on('click', '.active', function() {
$('#newAppointment').show();
var id = $(this).data("oid");
showLoading = 1;
$.ajax({
url: findId,
type: "post",
data: {idClaim: id},
dataType: "json",
success: function(j_response) {
    if (evalResponse(j_response)) {
        var data = j_response.data;
        $("#idClaim").val(data.idClaim);
        $("#idClaim_app").val(data.idClaim);
        $("#idInsurancecarrier_app").val(data.idInsuranceCarrier);
        $("#idCasemanager_app").val(data.idCasemanager);
        $("#idAdjuster_app").val(data.idAdjuster);
        $("#idClaim_newappointment_appinfo").val(data.idClaim);
        $("#idPatient_newappointment_appinfo").val(data.idPatient);
        $("#grid-appointmentbyclaimlist").trigger('reloadGrid');
    }
}
})
});
        
/*********************   jgrid Appointment By Claim List    *********************/
$("#grid-appointmentbyclaimlist").jqGrid({
url: listappointmentbyclaimlist,
datatype: "json",
postData: {
all: function() {
    return $('#status').is(':checked');
},
id: function() {
    return $('#idClaim').val();
}
},
colNames: ['<?php echo __('ID'); ?>', 
'<?php echo __('Estado'); ?>', 
'<?php echo __('Fecha'); ?>', 
'<?php echo __('Hora'); ?>', 
'<?php echo __('Nombre'); ?>', 
'<?php echo __('Teléfono'); ?>', 
'<?php echo __('Contacto'); ?>', 
'<?php echo __('Nombre'); ?>', 
'<?php echo __('Teléfono'); ?>', 
'<?php echo __('Nombre'); ?>', 
'<?php echo __('Teléfono'); ?>', 
'<?php echo __('Acciones'); ?>'],
colModel: [
{name: 'idAppointment', index: 'idAppointment', width: 50},
{name: 'status', index: 'status', hidden: true, width: 50},
{name: 'appointmentdate', index: 'appointmentdate', search: true, width: 70},
{name: 'appointmenttime', index: 'appointmenttime', width: 70, search: false},
{name: 'namepatient', index: 'namepatient', width: 150, search: false},
{name: 'phonepatient', index: 'phonepatient', width: 50, search: false},
{name: 'contact', index: 'contact', width: 200, search: false},
{name: 'namedriver', index: 'namedriver', width: 150, search: false},
{name: 'phonedriver', index: 'phonedriver', width: 50, search: false},
{name: 'nameinterprete', index: 'nameinterprete', width: 150, search: false},
{name: 'phoneinterprete', index: 'phoneinterprete', width: 50, search: false},
{name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 'auto',
            width: 1200,
            pager: 'grid-appointmentbyclaimlist-paper',
            sortname: 'idAppointment',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Lista de Citas por Caso'); ?>",
            afterInsertRow: function(row_id, data_id) {

if (data_id.status == 1) {

                    edit = "<a class=\"edit\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    trash = "<a class=\"trash\" data-oid=\"" + row_id + "\" title=\"<?php echo __('Desactivar'); ?>\" ><?php echo __('Desactivar'); ?></a>";
                 
                    $("#grid-appointmentbyclaimlist").jqGrid('setRowData', row_id, {actions:  edit + trash});
                }


},
gridComplete: function() {
$('.edit').button({
    icons: {primary: 'ui-icon-pencil'},
    text: false
})
$('.trash').button({
    icons: {primary: ' ui-icon-power'},
    text: false
})

$('.active').button({
    icons: {primary: 'ui-icon-check'},
    text: false
})
}
});
        
$("table#grid-appointmentbyclaimlist").on('click', '.edit', function() {
            $('.background').show();
$('#newappointment').show();
$('#newappointment_claim').show();
$('#newappointment_insurance').show();
$('#newappointment_appinfo').show();
$('#createapp').show();
var id = $('#idPatient_app').val()
var id_claim = $('#idClaim_app').val();
var id_Insurancecarrier = $('#idInsurancecarrier_app').val();
var id_Casemanager = $('#idCasemanager_app').val();
var id_Adjuster = $('#idAdjuster_app').val();
showLoading = 1;
$.ajax({
url: findClientAppointment,
type: "post",
data:
    {idPatient_app: id,
    idClaim_app: id_claim,
    idAdjuster_app: id_Adjuster,
    idCasemanager_app: id_Casemanager,
    idInsurancecarrier_app: id_Insurancecarrier
},
dataType: "json",
success: function(j_response) {
    if (evalResponse(j_response)) {
        var data = j_response.data;
        var data_claim = j_response.data_claim;
        var data_insurancecarrier = j_response.data_insurancecarrier;
        var data_casemanager = j_response.data_casemanager;
        var data_adjuster = j_response.data_adjuster;
        $("#idPatient_app").val(data.idPatient);
        $("#name_patient").val(data.firstname + ' ' + data.lastname);
        $("#address_patient").val(data.address);
        $("#socialsecurity_patient").val(data.socialsecurity);
        $("#apartmentnumber_patient").val(data.apartmentnumber);
        $("#phone_patient").val(data.phonenumber);
        $("#city_patient").val(data.city);
        $("#altnumber_patient").val(data.altnumber);
        $("#state_patient").val(data.state);
        $("#dob_patient").val(data.dob);
        $("#zipcode_patient").val(data.zipcode);
        $("#regiondefault_patient").val(data.regiondefault);
        $("#claimnumber_app").val(data_claim.claimnumber);
        $("#authorizedby_app").val(data_claim.authorizedby);
        $("#dateregistration_app").val(data_claim.dateregistration);
        $("#injury_app").val(data_claim.injury);
        $("#Insurancecarrier_name_app").val(data_insurancecarrier.name);
        $("#Casemanager_name_app").val(data_casemanager.name);
        $("#Adjuster_name_app").val(data_adjuster.name);
        $("#companyname_app").val(data_insurancecarrier.name);
        $("#companyphone_app").val(data_insurancecarrier.phone);
        if(data_claim.unlimited==1)
        {$("#Fecha-Maxima").val('unlimited');}
        else if (data_claim.unlimited==0)
        {$("#Fecha-Maxima").val(data_claim.dateFinish);}
    }
}
})

var idapp = $(this).data("oid");
$.ajax({
url: findIdEditApp,
type: "post",
data: {idapp: idapp},
dataType: "json",
success: function(j_response) {
    if (evalResponse(j_response)) {
        var data = j_response.data;
        $("#idAppointment").val(data.idAppointment);
        $("#appauthorize_appinfo").val(data.appauthorized);
        $("#dateregistration_appinfo").val(data.appointmentdate);
        $("#typeapp_appinfo").val(data.typeapp);
        $('#typeapp_appinfo-combobox').val($('#typeapp_appinfo option:selected').text());
        $("#appdescription_appinfo").val(data.idTypeAppointment);
        $('#appdescription_appinfo-combobox').val($('#appdescription_appinfo option:selected').text());
        $("#appointmenttime_appinfo").val(data.appointmenttime);
        $("#whocalls_app").val(data.whocalls);
        $("#companyname_app").val(data.companyname);
        $("#companyphone_app").val(data.companyphone);
        $("#notesFromCallingList").val(data.notescalling);
        $("#pick_up_time").val(data.pickuptime);
        $("#final").val(data.droptime);
        $('input[name=traduction]')
        .prop('checked', false).trigger('change');
        $('input[name=transportation]')
        .prop('checked', false).trigger('change');     
                        
        if ((data.idDriverRoundTrip) != null)
        {
            $('input[name=transportation]').prop('checked', true).trigger('change');
            $("#driver1").val(data.idDriverOneWay);
            $('#driver1-combobox').val($('#driver1 option:selected').text());                           
            $("#driver2").val(data.idDriverRoundTrip);
            $('#driver2-combobox').val($('#driver2 option:selected').text()); 
                            
            $('input[name=for_typeservice]').val([data.typeservice]);
            $('input[name=for_typeservice]:checked').trigger('change');
                            
            if(data.typeservice=='Ambulatory')
            {
                $('#b_wtime').show();
                $('#b_liftBase,#b_cassistance').hide();
            }
            else if (data.typeservice=='Stretcher')
            {          
                $('#b_liftBase,#b_cassistance,#b_wtime').show();
                                
            }
            else if (data.typeservice=='Wheelchair'){
                $('#b_liftBase,#b_cassistance,#b_wtime').show();
                                
            }
    
            $('input[name=for_lbase]').val([data.lbase]);
            $('input[name=for_lbase]:checked').trigger('change');
    
            $('input[name=for_cassistance]').val([data.cassistance]);
            $('input[name=for_cassistance]:checked').trigger('change');
                            
            if(data.wtime == 1)
            {
                $('input[name=for_wtime]').val(['on']);
                $('input[name=for_wtime]:checked').trigger('change');
                $('#waiting_time').show();
                $('#wtime_code').val(data.wtimecode);
                $('#wtime_hours').val(data.wtimehours);
                $('#wtime_minutes').val(data.wtimeminutes);
            }
                                
            $("#flatrate_serv_inf").val(data.frate);
            $("#fmiles").val(data.miles);    
                           
            var dlocation1 = j_response.location1;
            $("#location1").val(dlocation1.alias);
            $("#location2").val(dlocation1.address);
            $("#unit").val(dlocation1.suite);
            $("#phone_showbytransportation").val(dlocation1.telephone); 
                            
            var dlocation2 = j_response.location2;
            $("#contact").val(dlocation2.alias);
            $("#address").val(dlocation2.address);
            $("#unit_showbytransportation2").val(dlocation2.suite);
            $("#phone_showbytransportation2").val(dlocation2.telephone);
                            
                                                      
        }

        if ((data.idInterpreter) != null)
        {
            $("#typeservice_trasl").val(data.typeservicesinterpreter);
            $('#typeservice_trasl-combobox').val($('#typeservice_trasl option:selected').text());
            $('input[name=traduction]')
            .prop('checked', true).trigger('change');
            $("#interprete_trasl").val(data.idInterpreter);
            $('#interprete_trasl-combobox').val($('#interprete_trasl option:selected').text());
            $("#languaje_trasl").val(data.idLanguage);
            $('#languaje_trasl-combobox').val($('#languaje_trasl option:selected').text()); 
            if(data.typeservicesinterpreter=='Interpreting')
            {
                var dlocation = j_response.location;
                $("#location1").val(dlocation.alias);
                $("#location2").val(dlocation.address);
                $("#unit").val(dlocation.suite);
                $("#phone_showbytransportation").val(dlocation.telephone);}
        }
        $("#valtraduction").val(data.showinterpreter);
    }
}
})
            
            
});
     
$("table#grid-appointmentbyclaimlist").on('click', '.trash', function() {
var id = $(this).data("oid");
confirmBox(confirmDeleteMessage, deleteSubTitle, function(response) {
if (response) {
    showLoading = 1;
    $.ajax({
        url: delAppointment,
        type: "post",
        data: {id: id},
        dataType: "json",
        success: function(j_response) {
            if (evalResponse(j_response)) {
                $("#grid-appointmentbyclaimlist").trigger('reloadGrid');
            }
        }
    });
}
});
return false
});
       
        
/******************************* DIALOGO NUEVO TIPO DE CITA ********************************************/
        
$('#frmNew_apptype').dialog({
autoOpen: false,
modal: true,
title: newttitle,
height: 300,
width: 300
,
close: function() {
$('#frmNewTypeappointment').get(0).reset();
$('#frmNewTypeappointment input[type=hidden]').val('');
},
buttons: {
'<?php echo __("Guardar"); ?>': function() {
    //Funcione el Validate 
    if (!$('#frmNewTypeappointment').valid()) {
        return false;
    }
    $('#frmNewTypeappointment').submit();
},
'<?php echo __("Cancelar"); ?>': function() {
    /*Limpiar campos con reset */
    //frmNewTypeappointment.currentForm.reset();
    $(this).dialog('close');
}
}
});
        
$('#frmNewTypeappointment').validate({
rules: {
name: {required: true}
},
messages: {
name: {
    required: validTypeappointment
}

},
errorContainer: '#errorMessages',
errorLabelContainer: "#errorMessages .content .text #messageField",
wrapper: "p",
highlight: function(element, errorClass) {
$(element).addClass('ui-state-error');
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass('ui-state-error');
},
submitHandler: function() {
$.ajax({
    url: url_createUpdateTypeappointment,
    type: "post",
    data: $("#frmNewTypeappointment").serializeObject(),
    dataType: "json",
    success: function(j_response) {

        var data = j_response.data;
        $('#appdescription_appinfo').append(
        $('<option/>').attr('value', data.idTypeappointment).text(data.name)
    ).val(data.idTypeappointment);
        $('#appdescription_appinfo-combobox').val($('#appdescription_appinfo option:selected').text());
        $("#frmNew_apptype").dialog('close');
    }
});
}

});

/**************************************** DIALOG NEWDRIVERS *************************************/

$("#formNew_driver").dialog({
autoOpen: false,
title: titleNewDrivers,
height: 500,
width: 500,
resizable: false,
modal: true,
close: function() {
$('#validation_driver').get(0).reset();
},
buttons: {
<?php echo __("Guardar"); ?>: function() {
if (!$('#validation_driver').valid()) {
    return false;
}
$('#validation_driver').submit();
},
<?php echo __("Cancelar"); ?>: function() {
$(this).dialog("close");
}
}
});
/*************************DIALOG NEW AREA DRIVERS *************************/
$("#dialogo-areadriver").dialog({
autoOpen: false,
title: titleNewAreaDrivers,
height: 200,
width: 200,
modal: true,
close: function() {
$('#formulario-areadriver').get(0).reset();
$('#formulario-areadriver input[type=hidden]').val('');
},
buttons: {
<?php echo __("Guardar"); ?>: function() {
if (!$('#formulario-areadriver').valid()) {
return false;
}
$('#formulario-areadriver').submit();
},
<?php echo __("Cancelar"); ?>: function() {
$(this).dialog("close");
}
}
});
/******************************* VALIDACION  DIALOG AREA DRIVER **********************/


$('#formulario-areadriver').validate({
rules: {
location: {
required: true
}

},
messages: {
location: {
required: '<?php echo __("Locación es requerido") ?>'
}

},
errorContainer: '#errorMessages',
errorLabelContainer: "#errorMessages .content .text #messageField",
wrapper: "p",
highlight: function(element, errorClass) {
$(element).addClass('ui-state-error');
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass('ui-state-error');
},
submitHandler: function() {
showLoading = 1;
$.ajax({
url: saveAreadrivers,
type: "post",
data: $('#formulario-areadriver').serializeObject(),
dataType: "json",
success: function(j_response) {
var data = j_response.data;
$('#areas_drivers_id').append(
$('<option/>').attr('value', data.idAreasdriver).text(data.location)
).val(data.idAreasdriver);
$('#areas_drivers_id-combobox').val($('#areas_drivers_id option:selected').text());
$("#dialogo-areadriver").dialog('close');
}
});
}

});
/********************************************** VALIDACION NEW DRIVER **************************/
$('#validation_driver').validate({
rules: {
name: {
required: true
},
email: {
required: true,
email: true
},
areas_drivers_id: {
required: true
},
services: {
required: true
},
last_name: {
required: true
}


},
messages: {
name: {
required: '<?php echo __("Nombre es requerido") ?>'
},
last_name: {
required: '<?php echo __("Apellido es requerido") ?>'
},
areas_drivers_id: {
required: '<?php echo __("Area es requerida") ?>'
},
services: {
required: '<?php echo __("Servicio es requerido") ?>'
},
email: {
required: '<?php echo __("E-mail es requerido") ?>',
email: '<?php echo __("E-mail es incorrecto") ?>'
}


},
errorContainer: '#errorMessages',
errorLabelContainer: "#errorMessages .content .text #messageField",
wrapper: "p",
highlight: function(element, errorClass) {
$(element).addClass('ui-state-error');
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass('ui-state-error');
},
submitHandler: function() {
showLoading = 1;
$.ajax({
url: saveDrivers,
type: "post",
data:
{
idDrivers: $('#idDrivers').val()
, name: $('#name_driver').val()
, lastName: $('#last_name_driver').val()
, idAreasdriver: $('areas_drivers_id').val()
, typeservices: $('input[name=services]:checked').val()
, address: $('#address_driver').val()
, city: $('#city_driver').val()

, state: $('#state_driver').val()
, zipcode: $('#zip_code_driver').val()
, phone: $('#phone_driver').val()
, fax: $('#fax_driver').val()

, cellphone: $('#cell_phone_driver').val()
, carMake: $('#car_make_driver').val()
, carModel: $('#car_model_driver').val()
, email: $('#email_driver').val()
, insurance: $('#insurance_date_driver').val()
, identificationNumber: $('#identification_number_driver').val()
, specialNotes: $('#special_notes_driver').val()

},
dataType: "json",
success: function(j_response) {

var data = j_response.data;
$('#driver1').append(
$('<option/>').attr('value', data.idDriver).text(data.name)
).val(data.idDriver);
$('#driver2').append(
$('<option/>').attr('value', data.idDriver).text(data.name)
).val(data.idDriver)

$('#driver1-combobox').val($('#driver1 option:selected').text());
$("#formNew_driver").dialog('close');
}
});
}

});
//dialogs de expand app
$("#divAppointmentAdd").dialog({
autoOpen: false,
title: "<?php echo __("Agregar Cita") ?>",
height: "auto",
width: "auto",
modal: true,
resizable: false

});
$("#divExpandDate").dialog({
autoOpen: false,
title: "<?php echo __("Expandir Fecha de Caso") ?>",
height: "auto",
width: "auto",
modal: true,
resizable: false

});
$("#divExpandDateApp").dialog({
autoOpen: false,
title: "<?php echo __("Expandir Fecha de Cita") ?>",
height: "auto",
width: "auto",
modal: true,
resizable: false

});
       
// botones expand app

     
 

        
/******************** VALIDACION Y DIALOG PARA LENGUAJE **********************/

/*Dialog para lenguaje new*/
$("#dialogo-language").dialog({
autoOpen: false,
title: "Lenguaje",
height: "auto",
width: "auto",
modal: true,
resizable: false,
buttons: {
"<?php echo __("Guardar") ?>": function() {
$("#formulario-language").submit();
},
"<?php echo __("Cancelar") ?>": function() {
$(this).dialog("close");
}
}
});
frmLanguageValidate = $("#formulario-language").validate({
rules: {
name: {required: true},
type: {required: true}
},
messages: {
name: {required: '<?php echo __("Apellido es requerido") ?>'},
type: {required: '<?php echo __("Tipo de Lenguaje es requerido") ?>'}
},
errorContainer: '#errorMessages',
errorLabelContainer: "#errorMessages .content .text #messageField",
wrapper: "p",
highlight: function(element, errorClass) {
$(element).addClass('ui-state-error');
},
unhighlight: function(element, errorClass, validClass) {
$(element).removeClass('ui-state-error');
},
submitHandler: function(form) {
var formulario = $(form).serialize();
$.ajax({
url: saveLanguages,
type: "post",
// data:formulario,
data: {
idLanguage: $('#idNewLanguage').val(),
type: $('#type_language').val(),
name: $('#name_languaje').val()

},
dataType: "json",
success: function(j_response) {

var data = j_response.data;
                        
$('#languaje_trasl').append(
$('<option/>').attr('value', data.idLanguages).text(data.name)
).val(data.idLanguages);
$('#languaje_trasl-combobox').val($('#languaje_trasl option:selected').text());
$("#dialogo-language").dialog('close');
frmLanguageValidate.currentForm.reset();
}
});
}

});
$("#show_content").show();
});

</script>




<center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Lista de Casos por Paciente') ?></div>
</center>


<div id="show_content" class="show_content">

    <dt><label class="frmlbl"><?php echo __('Nombre de Paciente') ?></label></dt>
    <input class="type_text_long" type="text" name="namepatient" id="namepatient" size="35">
  <br>
  <br>

    <div id="formNew">  
        <form id="validation" name="validation" action="" method="post" >
            <input type="hidden" name="idPatient"  id="idPatient" />
            <fieldset> 
                <legend><?php echo __('Actualizar Caso') ?></legend>
                <ul>
                    <input type="hidden" name="idClaim"  id="idClaim" />
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Administrador de Caso') ?></label>
                            <select name="case_manager_id" id="case_manager_id">
                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                <?php foreach ($case_manager as $ad) { ?>
                                    <option value="<?php echo $ad['idCasemanager'] ?>"><?php echo $ad['name'] ?></option>

                                <?php } ?>
                            </select>
                            </dt>

                        </dl>

                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Ajustador') ?></label>
                            <select name="adjuster_id" id="adjuster_id">
                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                <?php foreach ($adjuster as $adj) { ?>
                                    <option value="<?php echo $adj['idAdjuster'] ?>"><?php echo $adj['name'] ?></option>

                                <?php } ?>
                            </select>
                            </dt>

                        </dl>

                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Fecha Ilimitada') ?></label>
                            <dd class="buttonsCheck">                                    
                                <input type="radio" id="date_finish_active" name="date_finish" value="1" /><label for="date_finish_active"><?php echo __("Activo") ?></label>
                                <input type="radio" id="date_finish_inactive" name="date_finish" value="0" checked="checked"/><label for="date_finish_inactive"><?php echo __("Inactivo") ?></label>
                                <input class="type_text_normal" type="text" id="date_finish_claims" name="date_finish_claims" value=""/>
                            </dd>
                            </dt>

                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Cita Ilimitada') ?></label>
                            <dd class="buttonsCheck">                                    
                                <input type="radio" id="max_appointment_active" name="max_appointment" value="1" /><label for="max_appointment_active"><?php echo __("Active") ?></label>
                                <input type="radio" id="max_appointment_inactive" name="max_appointment" value="0"  checked="checked"/><label for="max_appointment_inactive"><?php echo __("Inactive") ?></label>
                                <input class="type_text_normal" type="text" id="max_appointment_claims" name="max_appointment_claims" />
                            </dd>
                            </dt>
                        </dl>
                    </li>
            </fieldset> 
        </form>
    </div>

    <div id="claim"  >
        <form id="validationclaim" name="validationclaim" action="" method="post" >
            <input type="hidden" name="idPatientClaim"  id="idPatientClaim" />
            <fieldset class="fieldset"> 
                <legend><?php echo __('Información de Aseguradora y Casos') ?></legend>

                <input type="hidden" name="idClaim_nuevo"  id="idClaim_nuevo" />


                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Compañía de Seguros') ?></label>
                    <select name="insurance_carrier" id="insurance_carrier">
                        <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                        <?php foreach ($insurance_carrier as $id) { ?>
                            <option value="<?php echo $id['idInsurancecarrier'] ?>"><?php echo $id['name'] ?></option>

                        <?php } ?>
                    </select>
                    </dt>

                </dl>

                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Autorizado por') ?></label>
                    <input class="type_text_normal" type="text" id="authorizedby" name="authorizedby" value=""/>
                    </select>
                    </dt>
                </dl>

                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Fecha de Registro') ?></label>
                    <input class="type_text_normal" type="text" id="dateregistration" name="dateregistration" value=""/>
                    </select>
                    </dt>
                </dl>

                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Número de Caso') ?></label>
                    <input class="type_text_normal" type="text" id="claimnumber" name="claimnumber" value=""/>
                    </select>
                    </dt>
                </dl>

                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Lesión') ?></label>
                    <input class="type_text_normal" type="text" id="injury" name="injury" value=""/>
                    </select>
                    </dt>
                </dl>


                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Fecha Ilimitada') ?></label>
                    <dd class="buttonsCheck">                                    
                        <input type="radio" id="date_finish_active_claim" name="date_finish_claim" checked="checked" value="1" /><label for="date_finish_active_claim"><?php echo __("Activo") ?></label>
                        <input type="radio" id="date_finish_inactive_claim" name="date_finish_claim" value="0" /><label for="date_finish_inactive_claim"><?php echo __("Inactivo") ?></label>
                        <input class="type_text_normal" type="text" id="date_finish" name="date_finish" value=""/>

                    </dd>

                    </dt>

                </dl>


                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Cita Ilimitada') ?></label>
                    <dd class="buttonsCheck">                                    
                        <input type="radio" id="max_appointment_active_claim" name="max_appointment_claim" value="1" checked="checked"/><label for="max_appointment_active_claim"><?php echo __("Activo") ?></label>
                        <input type="radio" id="max_appointment_inactive_claim" name="max_appointment_claim" value="0"  /><label for="max_appointment_inactive_claim"><?php echo __("Inactivo") ?></label>
                        <input class="type_text_normal" type="text" id="max_appointment" name="max_appointment" />
                    </dd>

                    </dt>

                </dl>


                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Administrador de Caso') ?></label>
                    <select name="case_manager_id_claim" id="case_manager_id_claim">
                        <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                        <?php foreach ($case_manager as $ad_claim) { ?>
                            <option value="<?php echo $ad_claim['idCasemanager'] ?>"><?php echo $ad_claim['name'] ?></option>

                        <?php } ?>
                    </select>
                    </dt>

                </dl>

                <dl>
                    <dt>
                    <label class="frmlbl"><?php echo __('Ajustador') ?></label>
                    <select name="adjuster_id_claim" id="adjuster_id_claim">
                        <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                        <?php foreach ($adjuster as $adj_claim) { ?>
                            <option value="<?php echo $adj_claim['idAdjuster'] ?>"><?php echo $adj_claim['name'] ?></option>

                        <?php } ?>
                    </select>
                    </dt>

                </dl>
            </fieldset> 
        </form>    
    </div>

    <center>  
        <table id="grid-claimbyuserlist" ></table>
        <div id="grid-claimbyuserlist-paper"></div>
    </center>
    <br>
  <button type="button" id="newClaim" class="btnClaim"><?php echo __('Nuevo Caso') ?></button>
    <center>  
        <br>
        <br>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Lista de Citas por Caso') ?></div>
    </center>  
<br>
   
    <center>  
        <table id="grid-appointmentbyclaimlist" ></table>
        <div id="grid-appointmentbyclaimlist-paper"></div>
    </center>
  
 <br><button type="button" id="newAppointment" class="btnAppointment"><?php echo __('Nueva Cita') ?></button>
<br>
<br>

    <div class="background">
        <div class="main-body">
            <div class="main-inbody">
                <form id="validationappointment">
                    <input type="hidden" name="idAppointment"  id="idAppointment" />
                    <input type="hidden" name="idAlias_newappointment_appinfo"  id="idAlias_newappointment_appinfo" />
                    <input type="hidden" name="idCourse_newappointment_appinfo"  id="idCourse_newappointment_appinfo" />
                    <input type="hidden" name="idLocation_newappointment_appinfo"  id="idLocation_newappointment_appinfo" />
                    <input type="hidden" name="idClaim_newappointment_appinfo"  id="idClaim_newappointment_appinfo" />
                    <input type="hidden" name="idPatient_newappointment_appinfo"  id="idPatient_newappointment_appinfo" />
                    <input type="hidden" name="idLocation1"  id="idLocation1" />
                    <input type="hidden" name="idLocation2"  id="idLocation2" />

                    <div id="newappointment" type="hidden" class="none">
                        <fieldset>

                            <legend><?php echo __('Información de Paciente') ?></legend>
                            <input type="hidden" name="idPatient_app"  id="idPatient_app" />
                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Nombre') ?></label>
                                <input class="type_text_normal" type="text" id="name_patient" name="name_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Dirección') ?></label>
                                <input class="type_text_normal" type="text" id="address_patient" name="address_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Seguridad Social') ?></label>
                                <input class="type_text_normal" type="text" id="socialsecurity_patient" name="socialsecurity_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Número de Apartamento') ?></label>
                                <input class="type_text_normal" type="text" id="apartmentnumber_patient" name="apartmentnumber_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Teléfono') ?></label>
                                <input class="type_text_normal" type="text" id="phone_patient" name="phone_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Ciudad') ?></label>
                                <input class="type_text_normal" type="text" id="city_patient" name="city_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Número Alternativo') ?></label>
                                <input class="type_text_normal" type="text" id="altnumber_patient" name="altnumber_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Estado') ?></label>
                                <input class="type_text_normal" type="text" id="state_patient" name="state_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Fecha de Nacimiento') ?></label>
                                <input class="type_text_normal" type="text" id="dob_patient" name="dob_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Codigo ZIP') ?></label>
                                <input class="type_text_normal" type="text" id="zipcode_patient" name="zipcode_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Región') ?></label>
                                <input class="type_text_normal" type="text" id="regiondefault_patient" name="regiondefault_patient" value=""/>
                                </select>
                                </dt>
                            </dl>

                        </fieldset> 


                    </div>

                    <div id="newappointment_claim" type="hidden" class="none">


                        <fieldset> 

                            <legend><?php echo __('Información de Caso') ?></legend>
                            <input type="hidden" name="idClaim_app"  id="idClaim_app" />
                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Número de Caso') ?></label>
                                <input class="type_text_normal" type="text" id="claimnumber_app" name="claimnumber_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Autorizado por') ?></label>
                                <input class="type_text_normal" type="text" id="authorizedby_app" name="authorizedby_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Fecha Ilimitada') ?></label>
                                <input class="type_text_normal" type="text" id="Fecha-Maxima" name="Fecha-Maxima" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Fecha de Registro') ?></label>
                                <input class="type_text_normal" type="text" id="dateregistration_app" name="dateregistration_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Lesión') ?></label>
                                <input class="type_text_normal" type="text" id="injury_app" name="injury_app" value=""/>
                                </select>
                                </dt>
                            </dl>
                        </fieldset> 
                    </div>
                    <div class="clear"></div>


                    <div id="newappointment_insurance" type="hidden" class="none">
                        <fieldset> 
                            <legend><?php echo __('Información de Aseguradora') ?></legend>
                            <input type="hidden" name="idInsurancecarrier_app"  id="idInsurancecarrier_app" />
                            <input type="hidden" name="idCasemanager_app"  id="idCasemanager_app" />
                            <input type="hidden" name="idAdjuster_app"  id="idAdjuster_app" />

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Nombre de Aseguradora') ?></label>
                                <input class="type_text_normal" type="text" id="Insurancecarrier_name_app" name="Insurancecarrier_name_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Administrador de Caso') ?></label>
                                <input class="type_text_normal" type="text" id="Casemanager_name_app" name="Casemanager_name_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                            <dl>
                                <dt class="class_p">
                                <label class="frmlbl"><?php echo __('Ajustador') ?></label>
                                <input class="type_text_normal" type="text" id="Adjuster_name_app" name="Adjuster_name_app" value=""/>
                                </select>
                                </dt>
                            </dl>

                        </fieldset> 

                    </div>

                    <div id="newappointment_appinfo" type="hidden" class="none">
                        <fieldset> 

                            <legend><?php echo __('Información de Cita') ?></legend>
                            <div id="appinfo">

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Autorizado por') ?></label>
                                    <input class="type_text_normal" type="text" id="appauthorize_appinfo" name="appauthorize_appinfo" value=""/>

                                    </dt>
                                </dl>

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Fecha de Servicio') ?></label>
                                    <input class="type_text_normal" type="text" id="dateregistration_appinfo" name="dateregistration_appinfo" value=""/>

                                    </dt>
                                </dl>

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Tipo de Cita') ?></label>

                                    <select name="typeapp_appinfo" id="typeapp_appinfo">
                                        <option value=""><?php echo __('Seleccionar') ?>...</option>
                                        <option value="Legal">Legal</option>
                                        <option value="Medical">Medical</option>
                                        <option value="Other">Other</option>
                                    </select> 
                                    </dt>
                                </dl>

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Hora') ?></label>
                                    <input class="type_text_normal" type="text" id="appointmenttime_appinfo" name="appointmenttime_appinfo" value=""/>

                                    </dt>
                                </dl>

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Descripción de Cita') ?></label>
                                    <select name="appdescription_appinfo" id="appdescription_appinfo">
                                        <option value=""><?php echo __('Select'); ?>...</option>                         
                                        <?php foreach ($type_appointment as $ta) { ?>
                                            <option value="<?php echo $ta['id'] ?>"><?php echo $ta['nameapp'] ?></option>

                                        <?php } ?>
                                    </select>
                                    <button type="button" id="newAppDescription" class="btnnewAppDescription"><?php echo __('Nueva Descripcion de la Cita') ?></button>
                                    </dt>
                                </dl>

                                <dl>
                                    <dt class="class_p">
                                    <label class="frmlbl"><?php echo __('Repetir Cita') ?></label>
                                    <input type="checkbox" id="repeatapp" name="repeatapp"  />
                                    </dt>

                                    <div id="hasta">
                                        <label class="frmlbl"><?php echo __('Hasta') ?></label>
                                        <input type="text" id="until_app" name="until_app" value=""/>
                                        <br>
                                        <dd class="buttonsCheck_days" >
                                            <input type="checkbox" id="MONDAY" name="until[]" value="1" /><label for="MONDAY"><?php echo __("Lunes") ?></label>
                                            <input type="checkbox" id="TUESDAY" name="until[]" value="2" /><label for="TUESDAY"><?php echo __("Martes") ?></label>
                                            <input type="checkbox" id="WEDNESDAY" name="until[]" value="3" /><label for="WEDNESDAY"><?php echo __("Miércoles") ?></label>
                                            <input type="checkbox" id="THURSDAY" name="until[]" value="4" /><label for="THURSDAY"><?php echo __("Jueves") ?></label>
                                            <input type="checkbox" id="FRIDAY" name="until[]" value="5" /><label for="FRIDAY"><?php echo __("Viernes") ?></label>
                                            <input type="checkbox" id="SATURDAY" name="until[]" value="6" /><label for="SATURDAY"><?php echo __("Sábado") ?></label>
                                            <input type="checkbox" id="SUNDAY" name="until[]" value="7" /><label for="SUNDAY"><?php echo __("Domingo") ?></label>
                                        </dd>
                                    </div>
                                </dl>
                            </div>


                            <div id="newappointment_callinfo">

                                <fieldset>
                                    <legend><?php echo __('Información de Llamada') ?></legend>

                                    <dl>
                                        <dt class="class_p">
                                        <label class="frmlbl"><?php echo __('Llamado') ?></label>
                                        <input class="type_text_normal" type="text" id="whocalls_app" name="whocalls_app" value=""/>

                                        </dt>
                                    </dl>

                                    <dl>
                                        <dt class="class_p">
                                        <label class="frmlbl"><?php echo __('Compañia') ?></label>
                                        <input class="type_text_normal" type="text" id="companyname_app" name="companyname_app" value=""/>

                                        </dt>
                                    </dl>

                                    <dl>
                                        <dt class="class_p">
                                        <label class="frmlbl"><?php echo __('Teléfono') ?></label>
                                        <input class="type_text_normal" type="text" id="companyphone_app" name="companyphone_app" value=""/>

                                        </dt>
                                    </dl>
                                </fieldset>

                            </div>

                            <div class="clear"></div>
                            <div id="newappointment_servinfo">

                                <fieldset>
                                    <legend><?php echo __('Información de Servicio') ?></legend>
                                    <dl>
                                        <dt class="class_p">
                                        <label class="frmlbl"><?php echo __('Transporte') ?></label>
                                        <input type="checkbox" id="transportation" name="transportation" value="" />
                                        <input type="hidden" name="valtransportation"  id="valtransportation" value="0"/>
                                        </dt>
                                    </dl>


                                    <div id="transp">
                                        <br>
                                        <dl>
                                            <dt><label class="frmlbl" id="typeservice"><?php echo __('Servicios') ?></label></dt>
                                            <dd class="buttonsCheck_type" >
                                                <input type="radio" id="AMBULATORY" name="for_typeservice" value="Ambulatory" /><label for="AMBULATORY"><?php echo __("Ambulatory") ?></label>
                                                <input type="radio" id="WHEELCHAIR" name="for_typeservice" value="Wheelchair" /><label for="WHEELCHAIR"><?php echo __("Wheelchair") ?></label>
                                                <input type="radio" id="STRETCHER" name="for_typeservice" value="Stretcher" /><label for="STRETCHER"><?php echo __("Stretcher") ?></label>
                                            </dd>
                                        </dl>

                                        <dl>

                                            <label class="frmlbl"><?php echo __('Conductor') ?> 1</label>
                                            <select name="driver1" id="driver1" value="0">
                                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                                <?php foreach ($drivers as $d_id) { ?>
                                                    <option value="<?php echo $d_id['idDriver'] ?>"><?php echo $d_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>

                                                <?php } ?>
                                            </select>
                                            <button type="button" id="NewDriver" class="btnNewDriver"><?php echo __('Nuevo Conductor') ?></button>
                                        </dl>


                                        <dl>
                                            <label class="frmlbl"><?php echo __('Conductor') ?> 2</label>
                                            <select name="driver2" id="driver2" value="0" >
                                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                                <?php foreach ($drivers as $d_id) { ?>
                                                    <option value="<?php echo $d_id['idDriver'] ?>"><?php echo $d_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>

                                                <?php } ?>
                                            </select>
                                        </dl>

                                        <dl>
                                            <dt class="class_p">
                                            <label class="frmlbl"><?php echo __('Tarifa Plana') ?></label>
                                            <input class="type_text_normal" type="text" id="flatrate_serv_inf" name="flatrate_serv_inf" value=""/>
                                            <br>
                                            </dt>
                                        </dl>

                                        <div id="b_liftBase">
                                            <p class="buttons_liftBase">
                                            <dd class="buttonsCheck_INF_SERV" >
                                                <input type="checkbox" id="lbase" name="for_lbase" /><label for="lbase"><?php echo __("Lift Base") ?></label>
                                            </dd>
                                            </p>
                                        </div>
                                        <div id="b_cassistance">
                                            <p class="buttons_cassistance">
                                            <dd class="buttonsCheck_INF_SERV" >
                                                <input type="checkbox" id="cassistance" name="for_cassistance"  /><label for="cassistance"><?php echo __("Car Assistance") ?></label>
                                            </dd>
                                            </p>
                                        </div>
                                        <div id="b_wtime">
                                            <p class="buttons_wtime">
                                            <dd class="buttonsCheck_INF_SERV" >
                                                <input type="checkbox" id="wtime" name="for_wtime" /><label for="wtime"><?php echo __("Waiting Time") ?></label>
                                            </dd>
                                            </p>
                                        </div>
                                        <div id="waiting_time">
                                            <dl>
                                                <label class="frmlbl"><?php echo __('Código') ?></label>
                                                <input class="type_text_normal" type="text" id="wtime_code" name="wtime_code" value="<?php echo $w_time_code ?>"/>
                                            </dl>
                                            <dl>
                                                <label class="frmlbl"><?php echo __('Horas') ?></label>
                                                <input class="type_text_normal" type="text" id="wtime_hours" name="wtime_hours" value=""/>
                                            </dl>
                                            <dl>
                                                <label class="frmlbl"><?php echo __('Minutos') ?></label>
                                                <input class="type_text_normal" type="text" id="wtime_minutes" name="wtime_minutes" value=""/>
                                            </dl>
                                        </div>

                                    </div>    
                                    <dl>
                                        <br>
                                        <dt class="class_p">
                                        <label class="frmlbl"><?php echo __('Traducción') ?></label>
                                        <input type="checkbox" id="traduction" name="traduction" value = "" />
                                        <input type="hidden" name="valtraduction"  id="valtraduction" value="0"/>

                                        </dt>
                                    </dl>

                                    <div id="trasl">
                                        <dl>

                                            <br><label class="frmlbl"><?php echo __('Tipo de Servicio') ?></label>
                                            <dd id="combotypeservice">
                                                <select name="typeservice_trasl" id="typeservice_trasl">
                                                    <option value=""><?php echo __('Seleccionar') ?>...</option>
                                                    <option value="Interpreting">Interpreting</option>
                                                    <option value="Conference Call">Conference Call</option>
                                                    <option value="Document Translation">Document Translation</option>
                                                </select> 
                                            </dd>

                                        </dl>


                                        <dl>
                                            <label class="frmlbl"><?php echo __('Lenguaje') ?></label>
                                            <select name="languaje_trasl" id="languaje_trasl">
                                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                                <?php foreach ($languaje as $l_id) { ?>
                                                    <option value="<?php echo $l_id['idLanguage'] ?>"><?php echo $l_id['name'] ?></option>

                                                <?php } ?>
                                            </select>
                                            <button type="button" id="NewLanguage" class="btnNewLanguage"><?php echo __('New Language') ?></button>
                                        </dl>

                                        <dl>
                                            <label class="frmlbl"><?php echo __('Intérprete') ?></label>
                                            <select name="interprete_trasl" id="interprete_trasl" >
                                                <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                                <?php foreach ($interprete as $i_id) { ?>
                                                    <option value="<?php echo $i_id['idInterpreter'] ?>"><?php echo $i_id['name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <button type="button" id="NewInterprete" class="btnNewInterprete"><?php echo __('New Interpreter') ?></button>

                                        </dl>



                                    </div> 
                                </fieldset>

                            </div>

                            <div id="newappointment_pickupinfo">

                                <fieldset>
                                    <legend><?php echo __('Información de Recojo') ?></legend>
                                    <div id="form_newappointment_pickupinfo">

                                        <dl>
                                            <dt class="class_p">
                                            <label class="frmlbl"><?php echo __('Recojo') ?></label>
                                            <input class="type_text_normal" type="text" id="pick_up_time" name="pick_up_time" value=""/>

                                            </dt>
                                        </dl>

                                        <dl>
                                            <dt class="class_p">
                                            <label class="frmlbl"><?php echo __('Final') ?></label>
                                            <input class="type_text_normal" type="text" id="final" name="final" value=""/>

                                            </dt>
                                        </dl>

                                    </div>


                                    <div class="clear"></div>

                                    <div id="showbytransportation">
                                        <fieldset>
                                            <legend><?php echo __('Desde') ?></legend>

                                            <button type="button" id="maps" ><?php echo __('Calculador de Millaje') ?></button>

                                            <dl>
                                                <dt class="class_p">

                                                <label class="frmlbl"><?php echo __('Nombre') ?></label><br>
                                                <input class="type_text_normal" type="text" id="location1" name="location1" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Dirección') ?></label><br>
                                                <input class="type_text_normal" type="text" id="location2" name="location2" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Unidad') ?></label><br>
                                                <input class="type_text_normal" type="text" id="unit" name="unit" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Teléfono') ?></label><br>
                                                <input class="type_text_normal" type="text" id="phone_showbytransportation" name="phone_showbytransportation" value=""/>

                                                </dt>
                                            </dl>

                                        </fieldset>
                                    </div>



                                    <div id="showbytransportation2">
                                        <fieldset>
                                            <legend><?php echo __('Hasta') ?></legend>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Contacto') ?></label><br>
                                                <input class="type_text_normal" type="text" id="contact" name="contact" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Dirección') ?></label><br>
                                                <input class="type_text_normal" type="text" id="address" name="address" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Unidad') ?></label><br>
                                                <input class="type_text_normal" type="text" id="unit_showbytransportation2" name="unit" value=""/>

                                                </dt>
                                            </dl>

                                            <dl>
                                                <dt class="class_p">
                                                <label class="frmlbl"><?php echo __('Teléfono') ?></label><br>
                                                <input class="type_text_normal" type="text" id="phone_showbytransportation2" name="phone_showbytransportation" value=""/>

                                                </dt>
                                            </dl>

                                        </fieldset>
                                    </div>
                                    <div id="showTypeMiles">
                                        <br>   
                                        <p id="typetravelp">
                                            <input type="radio" id="oneway" name="roundtrip" value="OneWay" checked/><label for="oneway">One way</label>
                                            <input type="radio" id="roundtrip" name="roundtrip" value="RoundTravel" /><label for="roundtrip">Round trip</label>
                                        </p>
                                        <br>
                                        <p id="milesp">
                                            <label class="frmlblshort"><?php echo __('Millas') ?>:</label>
                                            <input type="hidden" name="omiles" id="omiles" size="15" >
                                            <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="fmiles" id="fmiles" size="15" >
                                        </p>
                                        <br>
                                    </div>
                                    <p id="notesp">
                                        <label class="frmlbl"><?php echo __('Lista de Notas de Llamada') ?>:</label>
                                        <textarea class="ui-widget ui-widget-content ui-corner-all" name="notesFromCallingList" id="notesFromCallingList" cols="27" rows="6" > </textarea>
                                    </p>
                                    <div class="clear"></div>
                                </fieldset>
                            </div>
                        </fieldset> 
                    </div>
                    <div class="clear"></div>
                    <br/> <button id="createapp" ><?php echo __('Crear Cita') ?></button>
                </form>
            </div>
        </div>
    </div>


    <!--NUEVO TIPO DE CITA-->

    <div id="frmNew_apptype">
        <form id="frmNewTypeappointment" name="frmNewTypeappointment" action="" method="POST">
            <fieldset>
                <legend><? echo __('Crear Tipo de Cita') ?></legend>
                <input type="text" name="idTypeappointment" id="idTypeappointment" hidden="true"/>
                <dl>
                    <dd>
                        <label class="frmlbl"><? echo __('Nombre') ?></label>
                        <input type="text" name="name" id="name" size="20" value="">
                    </dd>
                </dl>    

                <dl>
                    <dd>
                        <label class="frmlbl"><?php echo __('Tipo') ?></label>
                        <select name="type" id="type">
                            <option value="Medical"><?php echo __('Medical') ?></option>
                            <option value="Legal"><?php echo __('Legal') ?></option>
                            <option value="Other"><?php echo __('Otro') ?></option>
                        </select>
                    </dd>
                </dl>

                <!--<img id="addic" src="">-->
            </fieldset>
        </form>
    </div>


    <!------------------------------NUEVO CONDUCTOR Y AREA DRIVER ------------------------------------->

    <div id="formNew_driver">
        <form id="validation_driver" name="validation_driver" action="" method="post">
            <fieldset> 
                <legend><?php echo __('Información de Conductor') ?></legend>    
                <input type="hidden" name="idDrivers"  id="idDrivers" value="">
                <ul>expandTo
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Nombre') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="name_driver" name="name_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Apellido') ?></label></dt>
                            <dd>
                                <input type="text" id="last_name_driver" name="last_name_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Area') ?></label></dt>
                            <dd>
                                <select name="areas_drivers_id" id="areas_drivers_id">
                                    <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                    <?php foreach ($area_driver as $ad) { ?>
                                        <option value="<?php echo $ad['id'] ?>"><?php echo $ad['area'] ?></option>

                                    <?php } ?>
                                </select> 
                                <button type="button" id="addarea" name="addarea"><?php echo __("Agregar") ?></button>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl" id="services"><?php echo __('Servicios') ?></label></dt>
                            <dd class="buttonsCheck" >
                                <input type="radio" id="A" name="for_drive" value="Ambulatory" checked=""/><label for="A"><?php echo __("Ambulatory") ?></label>
                                <input type="radio" id="W" name="for_drive" value="Wheelchair" /><label for="W"><?php echo __("Wheelchair") ?></label>
                                <input type="radio" id="S" name="for_drive" value="Stretcher" /><label for="S"><?php echo __("Stretcher") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Dirección') ?></label></dt>
                            <dd>
                                <input type="text" name="address_driver" id="address_driver" />
                            </dd>
                        </dl>         
                        <div class="clear"></div>
                    </li>      

                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Ciudad') ?></label></dt>
                            <dd>
                                <input type="text" name="city_driver" id="city_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Estado') ?></label></dt>
                            <dd>
                                <input type="text" name="state_driver" id="state_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Codigo ZIP') ?></label></dt>
                            <dd>
                                <input type="text" name="zip_code_driver"  id="zip_code_driver"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Teléfono') ?></label></dt>
                            <dd>
                                <input type="text" name="phone_driver" id="phone_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Fax') ?></label></dt>
                            <dd>
                                <input type="text" name="fax_driver" id="fax_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Celular') ?></label></dt>
                            <dd>
                                <input type="text" name="cell_phone_driver" id="cell_phone_driver" />                
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Marca de Auto') ?></label></dt>
                            <dd>
                                <input type="text" name="car_make_driver" id="car_make_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Modelo de Auto') ?></label></dt>
                            <dd>
                                <input type="text" name="car_model_driver" id="car_model_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('E-mail') ?></label></dt>
                            <dd>
                                <input type="text" name="email_driver" id="email_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Aseguradora') ?> <?php echo __('Fecha') ?></label></dt>
                            <dd>
                                <input type="text" name="insurance_date_driver" id="insurance_date_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Número de Identificación') ?></label></dt>
                            <dd>
                                <input type="text" name="identification_number_driver" id="identification_number_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Notas Especiales') ?></label></dt>
                            <dd>
                                <textarea name="special_notes_driver" id="special_notes_driver" cols="27" rows="6" ></textarea>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>


                </ul>        

            </fieldset>


        </form>
    </div>


    <!-- ********** AREA DRIVER *************** -->

    <div id="dialogo-areadriver">
        <form method="POST" id="formulario-areadriver" name="formulario-areadriver" action="">
            <fieldset>  
                <label for="name" ><?php echo __('Locación') ?>:</label>
                <input type="text" name="location_driverarea" id="location_driverarea"/> 
                <br/>
                <input type="hidden" name="idDriverarea" id="idDriverarea" value=""/> 
                <br/>    
            </fieldset>
        </form>   
    </div> 



    <!-- ********** NUEVO LENGUAJE *************** -->

    <div id="dialogo-language" class="label_output"> 
        <form method="POST" id="formulario-language">
            <fieldset> 
                <legend><?php echo __('Crear Lenguaje') ?></legend>
                <br><label class="frmlabel" for="name" ><?php echo __('Nombre') ?>:</label> <br>
                <input type="text" name="name" id="name_languaje"/> <br/><br>
                <label class="frmlabel" for="type"><?php echo __('Tipo') ?>:</label><br>

                <select id="type_language" name="type_language">
                    <option value="Regular"><?php echo __('Regular') ?></option>
                    <option value="Rare"><?php echo __('Rare') ?></option>
                    <option value="Other"><?php echo __('Other') ?></option>
                </select> 
                <input type="hidden" name="idNewLanguage" id="idNewLanguage" value="0"/> <br/>    


            </fieldset>
        </form>   

    </div>

    <!--    *******************div expand & add app*******************-->
    <div id="divAppointmentAdd" class="dialog"  width="300px" height="200px" title="Agregar Cita">
        <p align="justify">
            <?php echo __('La fecha máxima o el número máximo de citas ha expirado.') ?><br/>
            <?php echo __('Para agregar una cita seleccione una acción.') ?>
        </p>
        <p align="center">
            <input type="button" id="expandNumClaims" value="<?php echo __('Expandir Número Máximo de Citas') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
            <br/><br/>
            <input type="button" id="expandMaxDate" value="<?php echo __('Expandir Fecha Máxima de Caso') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
            <br/><br/>
            <input type="button" id="onlyAppointment" value="<?php echo __('Solo para esta Cita') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
        </p>
    </div>
    <div id="divExpandDate" class="dialog"  width="300px" height="200px" title="<?php echo __('Expandir Fecha de Caso') ?>">
        <form id="validationExpandDate" name=""  method="post" action="">
            <fieldset>

                <p><label class="frmlbl"><?php echo __('Fecha Máxima a') ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="addmaxDate" id="addmaxDate" size="10"> </p>
                <p><input  id="expandTo"  value="<?php echo __('Expadir') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
            </fieldset>
        </form>
    </div>
    <div id="divExpandDateApp" class="dialog"  width="300px" height="200px" title="<?php echo __('Expandir Fecha de Cita') ?>">
        <form id="validationExpandDateApp" name=""  method="post" action="">
            <fieldset>

                <p><label class="frmlbl"><?php echo __('Fecha Máxima a') ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="addmaxDateApp" id="addmaxDateApp" size="10"> </p>
                <p><input  id="expandToApp"  value="<?php echo __('Expandir') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
            </fieldset>
        </form>
    </div>


    <?php include("googlemaps2_1.php"); ?>


</div>
