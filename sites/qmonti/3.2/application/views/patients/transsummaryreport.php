<script>
    var listAppWithPending = '/patients/transsummaryreport/listAppWithPending';
    var getAppointmentbyIdAppointment = '/patients/transsummaryreport/getAppointmentbyIdAppointment';
    var calctranstime = "/patients/transsummaryreport/calculartiempotrasl";
    var calcmilestime = "/patients/transsummaryreport/milesToTime";
    var confirmTranslationSummary = '/patients/transsummaryreport/confirmTranslationSummary';
    $(document).ready(function() {
        
        $(".isNoShow").hide();
        
        $.mask.rules = {'~': /[ap]/,
                        'm': /[m]/};
        $('input[type="text"]').setMask();

        $(".effectiveYes").hide();
        $(".effectiveNo").hide();
        $(".followDate").hide();
        $("#interpretationTimeTo").mask("99/99/9999",{placeholder:" "});

        $("#effectiveDate,#dateOfFollowUp,#dateOfFollowUp1,#dateOfFollowUp2").datepicker({dateFormat: 'mm/dd/yy'});

//        $("#interpretationTimeFrom,#interpretationTimeTo,#timeOfFollowUp,#timeOfFollowUp1,#timeOfFollowUp2").mask("19:59 ~m");


        $("#calctimeint,#calctraveltimeint").button({
            icons: {primary: 'ui-icon-clock'},
            text: false
        });


        $("#calctimeint").click(function() {
            var from = $("#interpretationTimeFrom").val();
            var to = $("#interpretationTimeTo").val();
            var dataString = "from=" + from + "&to=" + to + "&ajax=ajax";
            $.ajax({
                type: "POST",
                url: calctranstime,
                data: dataString,
                dataType: 'json',
                success: function(response) {
                    if (response.msg == 'OK') {
                        switch ($('#typeservicesInterpreter').val()) {
                            case 'Conference Call':
                                if (response.data < 0.25) {
                                     msgBox('<?php echo __('Tiempo Calculado'); ?>: ' + response.data + ' <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Conferencia es: 15 minutos'); ?>', '<?php echo __('Alerta'); ?>');
                                    $("#interpretationTimeTotalTime").val('');
                                } else {
                                    $("#interpretationTimeTotalTime").val(response.data);
                                }
                                break;
                            case 'Interpreting':
                                if (response.data < 1.5) {
                                   msgBox('<?php echo __('Tiempo Calculado'); ?> : ' + response.data + ' <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Interpretación es: 1.5 horas'); ?>', '<?php echo __('Alerta'); ?>');
                                    $("#interpretationTimeTotalTime").val('');
                                } else {
                                    $("#interpretationTimeTotalTime").val(response.data);
                                }
                                break;
                            default:
                                msgBox('<?php echo __('Document Translation u otro tipo de servicio no requiere Summary Translation.'); ?> \n\
                                <?php echo __('Envie un e-mail al administrador del sistema para solucionar este error.'); ?>', '<?php echo __('Alerta'); ?>');
                                break;
                        }
                    }
                }
            });
        });
        $("#calctraveltimeint").click(function()
        {

            var dataString = "totalMileage=" + $('#totalMileage').val();
            $.ajax(
                    {
                        type: "POST",
                        url: calcmilestime,
                        data: dataString,
                        dataType: 'json',
                        success: function(response)
                        {
                            $("#travelTotalTime").val(response.data);
                            if ($('#totalMileage').val() < 40) {
                                $('#totalMileage').val('40');
                            }
                        }
                    });
        });

        $("#formNew").dialog({
            autoOpen: false,
            height: "auto",
            width: 850
            
//            close: function() {
//                $('#validation').get(0).reset();
//            },
//            buttons: {
//                '<?php echo __('Confirmar Cita'); ?>': function() {
//                    if (!$('#validation').valid()) {
//                        return false;
//                    }
//
//
//                    $('#validation').submit();
//                   
//                },
//                Cancel: function() {
//                    $("#formNew").dialog("close");
//                }
//            }
//

        });

        $("#buttonConfirm").buttonset();
        $("#buttonDuty").buttonset();
        $("#buttonFollow").buttonset();

        $("#confirmAppointment").button();
        

        $("#confirmAppointment").click(function(){
         
        $('#validation').submit();
        $("#grid-listAppWithPending").trigger("reloadGrid");
        });




        $("#validation").validate({
                rules: {
                },
                messages: {
                },
                errorContainer: '#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {
                    $(element).addClass('ui-state-error');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('ui-state-error');
                },
                submitHandler: function(form) {

                    var formulario = $(form).serialize();
                    $.ajax({
                        url: confirmTranslationSummary,
                        type: "post",
                        data: formulario,
                        async: false,
                        dataType: "json",
                        success: function(j_response) {
                            if (evalResponse(j_response)) {

                                $("grid-listAppWithPending").trigger('reloadGrid');
//                                validation.currentForm.reset();
                                $("#formNew").dialog('close');
                            }
                        }
                    });

                }

            });

        $("input:radio[name=confirmAble]").change(function() {
            if ($(this).val() == 'yes') {
                $('.effectiveYes').show();
                $('.effectiveNo').hide();
            } else {
                $('.effectiveYes').hide();
                $('.effectiveNo').show();
            }
        });

        $("input:radio[name=FollowUpAppointment]").change(function() {
            if ($(this).val() == 'yes') {
                $('.followDate').show();
            } else {
                $('.followDate').hide();
            }
        });

        $("#grid-listAppWithPending").jqGrid({
            url: listAppWithPending,
            datatype: "json",
            postData: {
            },
            colNames: [
                       '<?php echo __('ID'); ?>', 
                       '<?php echo __('Intérprete'); ?>', 
                       '<?php echo __('Paciente'); ?>', 
                       '<?php echo __('Fecha de Cita'); ?>', 
                       '<?php echo __('Hora de Cita'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
                {name: 'idApp', index: 'idApp', search: true, width: 150},
                {name: 'interpreter', index: 'interpreter', search: true, width: 200},
                {name: 'patient', index: 'patient', search: true, width: 200},
                {name: 'appDate', index: 'appDate', width: 200, search: false},
                {name: 'appTime', index: 'appTime', width: 200, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 350,
            pager: 'grid-listAppWithPending-paper',
            sortname: 'idApp',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Citas'); ?>",
            afterInsertRow: function(row_id, data_id) {

                check = "<a class=\"check\" data-idapp=\"" + row_id + "\" title=\"<?php echo __('Ver'); ?>\" ><?php echo __('Ver'); ?></a>";

                $("#grid-listAppWithPending").jqGrid('setRowData', row_id, {actions: check});
            },
            gridComplete: function() {
                $('.check').button({
                    icons: {primary: 'ui-icon-check'},
                    text: false
                });
            }
        });

        $('#grid-listAppWithPending').on('click', '.check', function() {

            dataString = "idApp=" + $(this).attr("data-idapp");
            $.ajax(
                    {
                        type: "POST",
                        url: getAppointmentbyIdAppointment,
                        data: dataString,
                        dataType: 'json',
                        success: function(j_response)
                        {
                             var data = j_response.data;
                              if (evalResponse(j_response)){
                            if(data.typeservinterpreter=='Interpreting')
                                 $(".isNoShow").show();
                            else $(".isNoShow").hide();
                            
                            $("#idAppointment").val(data.idApp);
                            $("#appointmentDate").val(data.appDate);
                            $("#appointmentTime").val(data.appTime);
                            $("#destaddress").val(data.addressloc);
                            $("#desttelephone").val(data.phoneloc);
                            $("#patientName").val(data.firstname + ' ' + data.lastname);
                            $("#language").val(data.language);
                            $("#socialSecurity").val(data.socialsecurity);
                            $("#authorization").val(data.authorized);
                            $("#typeApp").val(data.typeapp);
                            $("#idtypeAppointment").val(data.typeapppurpose);
                            $("#typeservicesInterpreter").val(data.typeservinterpreter);
                            $("#interpretationTimeFrom").val(data.appTime);
                            $("#idInterpreter").val(data.idInter);
                            
                              }
                                
                            
                            
                        }
                    });

            $("#formNew").dialog('open');

        });

    });

</script>
<center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Citas en espera de Reporte de Intérprete') ?></div>
</center>
<div id="show_content" class="show_content" >
    <center>  
        <br>
        <br>
        <table id="grid-listAppWithPending" ></table>
        <div id="grid-listAppWithPending-paper"></div>
    </center>
    <div id="formNew" title="<?php echo __('Resumen de Traducción de Monti'); ?>">
        <form id="validation">

            <table>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Fecha de Traducción') ?>:</label>
                            <input type="text" name="appointmentDate" id="appointmentDate" size="30" disabled/>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Hora') ?>:</label>
                            <input type="text" name="appointmentTime" id="appointmentTime" size="30" disabled/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Dirección') ?>:</label>
                            <input type="text" name="destaddress" id="destaddress" size="30" disabled/>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Teléfono') ?>:</label>
                            <input type="text" name="desttelephone" id="desttelephone" size="30" disabled/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Paciente') ?>:</label>
                            <input type="text" name="patientName" id="patientName" size="30" disabled/>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Lenguaje') ?>:</label>
                            <input type="text" name="language" id="language" size="30" disabled/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Seguridad Social') ?>:</label>
                            <input type="text" name="socialSecurity" id="socialSecurity" size="30" disabled/>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Autorización') ?>:</label>
                            <input type="text" name="authorization" id="authorization" size="30" disabled/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Tipo de Cita') ?>:</label>
                            <input type="text" name="typeApp" id="typeApp" size="30" disabled/>
                        </p>
                    </td>
                    <td>
                        <p>
                           <label class="frmlbl"><?php echo __('Propósito del Asignamiento') ?>:</label>
                            <input type="text" name="idtypeAppointment" id="idtypeAppointment" size="30" disabled/>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p>
                            <label class="frmlbl lLong"><?php echo __('Tipo de Servicio') ?>:</label>
                            <input type="text" name="typeservicesInterpreter" id="typeservicesInterpreter" size="30" disabled/>
                        </p>
                    </td>
                </tr>
            </table>
            <hr class="ui-widget-header"/>

<!--                <tr class="dontShow">
         <td colspan="3">
             <p class="isShowP">
                 <label for="isSHow">Interpreter Show</label>
                 <input type="radio" name="isShow" id="isSHow" value="yes" checked/>
                 <label for="isNoShow">No show Interpreter</label>
                 <input type="radio" name="isShow" id="isNoShow" value="no"/>
             </p>
         </td>
     </tr>
    
     <tr class="isNoShow">
         <td colspan="3">                        -->
            <br>        
            <h3><?php echo __('Tiempo de la Interpretación') ?></h3>                        
            <table>
                </td>
                </tr>
                <tr class="isNoShow">
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Desde') ?>:</label>
                            <input type="text" name="interpretationTimeFrom" id="interpretationTimeFrom" size="10"  />
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Hasta') ?>:</label>
                            <input type="text" name="interpretationTimeTo" id="interpretationTimeTo" size="10" alt="19:59 ~m"/>
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><button id="calctimeint" type="button"><?php echo __('Tiempo Total') ?></button> <?php echo __('Tiempo Total') ?>:</label>
                            <input type="text" readonly name="interpretationTimeTotalTime" id="interpretationTimeTotalTime" size="10" />
                        </p>
                    </td>
                </tr>
                <tr class="isNoShow travel">
                    <td colspan="3">
                        <br>
                        <h3><?php echo __('Viaje (Ciudad a Ciudad)') ?></h3>

                    </td>
                </tr>
                <tr class="isNoShow travel">
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Desde') ?>:</label>
                            <input type="text" name="travelFrom" id="travelFrom" size="20" >
                        </p>
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Hasta') ?>:</label>
                            <input type="text" name="travelTo" id="travelTo" size="20" >
                        </p>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr class="isNoShow travel">
                    <td>
                        <p>
                            <label class="frmlbl"><?php echo __('Millaje Total') ?>:</label>
                            <input type="text" name="totalMileage" id="totalMileage" size="10" >
                        </p>                        
                    </td>
                    <td>
                        <p>
                            <label class="frmlbl"><button id="calctraveltimeint" type="button"><?php echo __('Tiempo Total') ?></button> <?php echo __('Tiempo Total de Millaje') ?>:</label>
                            <input type="text" name="travelTotalTime" id="travelTotalTime" size="10" readonly />
                        </p>
                    </td>
                </tr>
                <tr class="isNoShow travel">
                    <td colspan="3">
                        <p>
                            <label class="frmlbl"><?php echo __('Otros Gastos') ?>s:</label>
                            <input type="text" name="otherExpenses" id="otherExpenses" size="30" >
                        </p>
                    </td>
                </tr>
                <tr class="isNoShow travel">
                    <td valign="top">
                        <p>
                            <label class="frmlbl"><?php echo __('Tiempo de Espera') ?>(<?php echo __('minutos') ?>):</label>
                            <input type="text" name="waitTime" id="waitTime" size=10">
                        </p>
                    </td>
                    <td valign="top">
                        <p>
                            <label class="frmlbl"><?php echo __('¿Por qué?') ?>:</label>
                            <textarea name="why" id="why" cols="27" rows="4" ></textarea>
                        </p>
                    </td>
                </tr>

            </table>
            <fieldset>
                <legend><?php echo __('Resumen de Asignamiento: (Breve Explicación de la cita)') ?></legend>
                <table>
                    <tr>
                        <td colspan="3">
                            <p>
                                <textarea name="summaryofAssignment" id="summaryofAssignment" cols="100" rows="4" ></textarea>
                            </p>
                        </td>
                    </tr>
                </table>
                <table id="work" class="isNoShow">
                    <tr class="confirmation">
                        <td colspan="3">
                            <label><?php echo __('¿Está el paciente apto para volver a trabajar?') ?></label>

                            <p id="buttonConfirm">
                                <label for="confirmAble1"><?php echo __('Si') ?></label>
                                <input type="radio" name="confirmAble" id="confirmAble1" value="yes"/>
                                <label for="confirmAble2"><?php echo __('No') ?></label><input type="radio" name="confirmAble" id="confirmAble2" value="no"/>
                            </p>
                        </td>
                    </tr>
                    <tr class="effectiveYes">
                        <td colspan="3">
                            <p>
                                <label class="frmlbl" for="effectiveDate">(<?php echo __('Si') ?>) <?php echo __('Día Efectivo') ?>:</label>
                                <input type="text" class="effectiveDate" name="effectiveDate" id="effectiveDate" size="30"  >
                            </p>
                        </td>                                                
                    </tr>
                    <tr class="effectiveYes">
                        <td colspan="3">
                            <p id="buttonDuty">
                                <label for="fullduty"><?php echo __('Completo') ?></label>
                                <input type="radio" name="duty" id="fullduty" size="30" value="full"/>
                                <label for="lightduty"><?php echo __('Poco') ?></label>
                                <input type="radio" name="duty" id="lightduty" size="30" value="light"/>
                            </p>
                        </td>                        
                    </tr>
                    <tr class="effectiveYes">
                        <td colspan="3">
                            <label class="frmlbl"><?php echo __('Restricciones(si algunas)') ?>:</label>
                            <textarea name="restrictions" id="restrictions" cols="60" rows="6" ></textarea>
                        </td>
                    </tr>
                    <tr class="effectiveNo">
                        <td colspan="3">
                            <p>
                                <label class="frmlbl" for="forHowLong">(<?php echo __('No') ?>) <?php echo __('¿Por cuanto tiempo?') ?>:</label>
                                <input type="text" name="forHowLong" id="forHowLong" size="30" >
                            </p>
                        </td>
                    </tr>
                    <tr class="confirmation">
                        <td colspan="2">
                            <label><?php echo __('Seguimiento de Cita') ?>:</label>
                            <p id="buttonFollow">
                                <label for="YesFollowUpAppointment"><?php echo __('Yes') ?></label><input type="radio" id="YesFollowUpAppointment" name="FollowUpAppointment"  size="30" value="yes" />
                                <label for="NoFollowUpAppointment"><?php echo __('No') ?></label><input type="radio" id="NoFollowUpAppointment" name="FollowUpAppointment" size="30" value="no" />
                            </p>
                        </td>
                        <td class="followDate">
                            <ul class="dateFollow">
                                <li>
                                    <dl>
                                        <dd><label class="frmlbl" for="dateOfFollowUp"><?php echo __('Fecha') ?>:</label></dd>
                                        <dd><input type="text" name="dateOfFollowUp" id="dateOfFollowUp" size="20" /></dd>
                                        <dd><label class="frmlbl" for="timeOfFollowUp"><?php echo __('Hora') ?>:</label></dd>
                                        <dd><input type="text" name="timeOfFollowUp" id="timeOfFollowUp" size="15" /></dd>
                                    </dl>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <dl>
                                        <dd><label class="frmlbl" for="dateOfFollowUp1"><?php echo __('Fecha') ?>:</label></dd>
                                        <dd><input type="text" name="dateOfFollowUp1" id="dateOfFollowUp1" size="20" /></dd>
                                        <dd><label class="frmlbl" for="timeOfFollowUp1"><?php echo __('Hora') ?>:</label></dd>
                                        <dd><input type="text" name="timeOfFollowUp1" id="timeOfFollowUp1" size="15" /></dd>
                                    </dl>
                                    <div class="clear"></div>
                                </li>
                                <li>
                                    <dl>
                                        <dd><label class="frmlbl" for="dateOfFollowUp2"><?php echo __('Fecha') ?>:</label></dd>
                                        <dd><input type="text" name="dateOfFollowUp2" id="dateOfFollowUp2" size="20" /></dd>
                                        <dd><label class="frmlbl" for="timeOfFollowUp2"><?php echo __('Hora') ?>:</label></dd>
                                        <dd><input type="text" name="timeOfFollowUp2" id="timeOfFollowUp2" size="15" /></dd>
                                    </dl>
                                    <div class="clear"></div>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    <tr class="confirmation">
                        <td>
                            <label for="commentDateFollowUp"><?php echo __('Comentario de Seguimiento') ?></label>
                        </td>
                        <td colspan="2">
                            <textarea name="commentDateFollowUp" id="commentDateFollowUp"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <p>
                <input type="button" id="confirmAppointment" value="<?php echo __('Confirmar Cita') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all"/>
            </p>
            </fieldset>
            <input type="hidden" id="idAppointment" name="idAppointment" value="0"/>
            <input type="hidden" id="idInterpreter" name="idInterpreter" value="0"/>
            <input type="hidden" id="idTranslation" name="idTranslation" value="0"/>
        </form>
    </div>
    <div id="divInsertEmail" class="dialog"  width="300px" height="150px" title="<?php echo __('Insertar E-mail') ?>">
        <form id="formEmail" action="">
            <label class="frmlbl" for="email"><?php echo __('E-mail') ?>:</label>
            <input type="text" id="email" name="email" />
            <br/>
            <button id="sendEmail"><?php echo __('Enviar') ?>:</button>
        </form>
    </div>   
</div>