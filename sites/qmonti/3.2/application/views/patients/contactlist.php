
<script type="text/javascript">
    $(document).ready(function() {
       
       var listLocations = '/patients/contactlist/listLocations';
       var getLocationById = '/patients/contactlist/getLocationById';
       var saveLocation = '/patients/contactlist/saveLocation';
       var deleteLocation = '/patients/contactlist/deleteLocation';
            
       $("#grid-listLocations").jqGrid({
        url: listLocations,
        datatype: "json",
        postData: {
        },
        colNames: ['<?php echo __('ID'); ?>',  
                   '<?php echo __('Contacto'); ?>', 
                   '<?php echo __('Dirección'); ?>', 
                   '<?php echo __('Número'); ?>',  
                   '<?php echo __('Teléfono'); ?>', 
                   '<?php echo __('Acciones'); ?>'],
        colModel: [
          
            {name: 'idLocation', index: 'idLocation',hidden: true, width: 10},
            {name: 'alias', index: 'alias', search: true, width: 250},
            {name: 'address', index: 'address', search: true, width: 350},
            {name: 'suite', index: 'suite', search: true, width: 150},
            {name: 'telephone', index: 'telephone', width: 200, search: true},
            {name: 'actions', index: 'actions', width: 150, search: false, align: "center"}
        ],
        rowNum: 30,
        rowList: [30, 40, 50],
        height: 400,
        width:1200,
        pager: 'grid-listLocations-paper',
        sortname: 'loc.alias',
        viewrecords: true,
        sortorder: "asc",
        caption: "<?php echo __('Lista de Contactos'); ?>",
        afterInsertRow: function(row_id, data_id) {
                
               
                edit = "<a class=\"edit\" idLocation=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                    delet = "<a class=\"delet\" idLocation=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listLocations").jqGrid('setRowData', row_id, { actions: edit + delet});
                
        },
        gridComplete: function() {
            $('.edit').button({
                icons: {primary: 'ui-icon-pencil'},
                text: false
            });
            $('.delet').button({
                icons: {primary: ' ui-icon-trash'},
                text: false
            });

        }

        });
        
        $("#grid-listLocations").jqGrid('navGrid','#grid-listLocations-paper',{edit:false,add:false,del:false});
        
             $('#grid-listLocations').on('click', '.edit',function() {
           
             idLocation=$(this).attr("idLocation");
              $.ajax({
                    url: getLocationById,
                    type: "post",
                    data: {
                        idLocation: idLocation
                        },
                    dataType: "json",
                    success: function(response) {
                        if (evalResponse(response))
                        {
                            
                            data=response.data;
                            $('#idLocation').val(data.idLocation);
                            $('#contact').val(data.alias);
                            $('#address').val(data.address);
                            $('#suite').val(data.suite);
                            $('#telephone').val(data.telephone);
                        }

                    }
                });
             $('#formNew').dialog('open');
           
             
        });
        
             $('#grid-listLocations').on('click', '.delet',function() {
           
             idLocation=$(this).attr("idLocation");
             $.ajax({
                    url: deleteLocation,
                    type: "post",
                    data: {
                        idLocation: idLocation
                        },
                    dataType: "json",
                    success: function(response) {

                        if (evalResponse(response))
                        {
                             msgBox("<?php echo __('Locación eliminada'); ?>","<?php echo __('Alerta'); ?>");
                        }

                    }
                });
             $("#grid-listLocations").trigger("reloadGrid");
             
        });
        
        $('#newLocation').button().click(function(){
             $("#formNew").dialog('open');
             
        });
        
        $("#formNew").dialog({
            autoOpen: false,
            title: "<?php echo __('Formulario Locación'); ?>",
            height: "auto",
            width: "auto",
            modal: true,
            resizable: false,
            buttons: {
                "<?php echo __('Guardar'); ?>": function() {
                    $("#validation").submit();
                },
                "<?php echo __('Cancelar'); ?>": function() {
                    $(this).dialog("close");
                }
            }
        });
        
             $('#validation').validate({
            rules: {
                contact: {required: true},
                address: {required: true},
                suite: {required: true}
             
            },
            messages: {
                contact: {required: '<?php echo __("Contacto es requerido") ?>'},
                address: {required: '<?php echo __("Dirección es requerido") ?>'},
                suite: {required: '<?php echo __("Número es requerido") ?>'}
                
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form) {
                
                var formulario = $(form).serialize();
                $.ajax({
                    url: saveLocation,
                    type: "post",
                    data: formulario,
                    dataType: "json",
                    success: function(response) {

                        if (evalResponse(response))
                        {
                            $("#grid-listLocations").trigger('reloadGrid');
                            $("#formNew").dialog('close');
                        }

                    }
                });
            }

        });
        });
</script>
<center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Lista de Contactos') ?></div>
</center>
<div class="show_content">
     <center>  
        <table id="grid-listLocations" ></table>
        <div id="grid-listLocations-paper"></div>
    </center>
    <br>
    <button id="newLocation"><b><?php echo __('Nuevo Contacto') ?></b></button>
    <div id="formNew">
        <form id="validation" name="validation" action="" method="post" >
            <input type="hidden" name="idLocation" id="idLocation"  value="">
             <fieldset class="fieldset">
            <dl>
                                <dt class="class_p">
           <label class="frmlbl"><?php echo __('Contacto'); ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="contact" id="contact" size="30" value=""> 
           </dt></dl> <dl>
                                <dt class="class_p">
           <label class="frmlbl"><?php echo __('Dirección'); ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="address" id="address" size="30" value=""> 
            </dt></dl> <dl>
                                <dt class="class_p">
           <label class="frmlbl"><?php echo __('Suite'); ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="suite" id="suite" size="30" value="">  </p>            
            </dt></dl> <dl>
                                <dt class="class_p">
           <label class="frmlbl"><?php echo __('Teléfono'); ?>:</label> <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="telephone" id="telephone" size="30" value="">  </p>
<dt class="class_p">
        </fieldset>
        </form>
    </div>    
</div>
