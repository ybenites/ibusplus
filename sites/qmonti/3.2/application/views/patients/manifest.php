<style>
    .frmlabel {
        width: 130px;
        display: block;
        float: left;
        height: 16px;
        padding-top: 1px;
        font-weight: bold;
        margin-left: 15px;

    }

    .class_p{
        clear: left;
        margin: 5px 0;
    }

    .type_text_normal{
        width: 120px; /* una longitud definida ancho */
        height:12px;
        margin: 3px 0;
    }

    #newappointment fieldset{
        width: 925px;
        padding: 8px;
    }

    #newappointment_claim fieldset{
        width: 925px;
        padding: 8px;
    }

    #newappointment_insurance fieldset{
        width: 925px;
        padding: 8px;
    }

    #newappointment_appinfo fieldset{
        width: 925px;
        padding: 8px;
    }

    #newappointment_callinfo fieldset{
        width: 925px;
        padding: 8px;
    }

    #newappointment_servinfo fieldset
    {
        width: 925px;
        padding: 8px;
    }

    #newappointment_pickupinfo fieldset
    {
        width: 925px;
        padding: 8px;
    }

    #showbytransportation fieldset{
        width: 163px;
        padding: 8px;
    } 
    #showbytransportation2 fieldset{
        width: 163px;
        padding: 8px;
    }  
    .buttonsCheck_type{
        margin-left: 130px;

    }
    #showbytransportation{
        float:right; 
        margin-right: 262px;
    }
    #showbytransportation2{
        float: right;
        margin-right: -410px;

    }
    #transp{
        float: left;
        margin-left: -129px;
    }

    #trasl{
        float: left;
        margin-left: -129px;

    }
    #showTypeMiles{
        float: left;

    }

    #notesp{
        margin-top: 101px;
    }

    #hasta{

    }

</style>
<script type="text/javascript">
    
    var listApp = '/patients/manifest/listApp';
    var listNotes = '/patients/manifest/listNotes';
    var listNotesContacts = '/patients/manifest/listNotesContacts';
    var saveNotesPatients = '/patients/manifest/saveNotesPatients';
    var deleteNotesPat = '/patients/manifest/deleteNotesPat';
    var saveNotesContacts = '/patients/manifest/saveNotesContacts';
    var deleteNotesCon = '/patients/manifest/deleteNotesCon';
    var saveNotesDrivers = '/patients/manifest/saveNotesDrivers';
    var deleteNotesDri = '/patients/manifest/deleteNotesDri';
    var saveNotesInterpreters = '/patients/manifest/saveNotesInterpreters';
    var deleteNotesInt = '/patients/manifest/deleteNotesInt';
    var saveNotesEmployeers = '/patients/manifest/saveNotesEmployeers';
    var deleteNotesEmp = '/patients/manifest/deleteNotesEmp';
    var getIdNotesPat = '/patients/manifest/getIdNotesPat';
    var getIdNotesCon = '/patients/manifest/getIdNotesCon';
    var getIdNotesDri = '/patients/manifest/getIdNotesDri';
    var getIdNotesInt = '/patients/manifest/getIdNotesInt';
    var getIdNotesEmp = '/patients/manifest/getIdNotesEmp';
    var confirm = '/patients/manifest/confirm';
    var Const_idAppointment = '0';
    var findClientAppointment = '/patients/manifest/appointmentInformation';
    var findIdEditApp = "/patients/manifest/getIdUpdateAppointment";
    
   
    var id_claim = '0';
    var id_Insurancecarrier = '0';
    var id_Casemanager = '0';
    var id_Adjuster = '0';
    
    var isSendEmail = false;
    var idAppointmentEmail = null;
    
    $(document).ready(function() {
        
        $('#print').button();
        $('#send').button(); 
        
        /************************************************************************ VISTA APPOINTMENT *******************************************************/
  
        $('#date_finish_claims').hide();
        $('#max_appointment_claims').hide();
        $('#date_finish').hide();
        $('#max_appointment').hide();
        $('#newappointment').hide();
        $('#newappointment_claim').hide();
        $('#newappointment_insurance').hide();
        $('#newappointment_appinfo').hide();
        $('#hasta').hide();
        $('#transp').hide();
        $('#btnswhell').hide();
        $('#btnsstret').hide();
        $('#btnsamb').hide();
        $('#trasl').hide();
        $('#showbytransportation').hide();
        $('#showbytransportation2').hide();
        //$('#newAppointment').hide();
        $('#showTypeMiles').hide(); 
         
        $('input[name=repeatapp]').change(function() {
            if (($(this).is(":checked")) == true)
            {
                $('#hasta').show();
            }
            else
            {
                $('#hasta').hide();
            }

        });
        $('#typeservice_trasl ,input[name=transportation],input[name=traduction]').change(function() {


            //            if(($('#typeservice_trasl').val())=="INTERPRETING" && $('input[name=transportation]').is(":checked")==false && $('input[name=traduction]').is(":checked")==true){
            if($('#typeservice_trasl').val()=="Interpreting"){
               
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show(); 
                $('#showTypeMilesNotes').show();
                
            }
            
                      
            if(($('input[name=traduction]').is(":checked"))==true)
            {
                $('#trasl').show();
                $('#valtraduction').val("1");
                $('#traduction').val("1");
            }

            if (($('input[name=traduction]').is(":checked")) == false)
            {
                $('#typeservice_trasl').val(" ");
                $('#trasl').hide();
                $('#valtraduction').val("0");
                $('#traduction').val("0");
            }

            if (($('input[name=transportation]').is(":checked")) == true)
            {
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#valtransportation').val("1");
                $('#transportation').val("1");
                $('#showTypeMiles').show();
               
            }

            if (($('input[name=transportation]').is(":checked")) == false)
            {
                $('#transp').hide();
                $('#valtransportation').val("0");
                $('#transportation').val("0");
                $('#showbytransportation').hide();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
                
            }

            if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=traduction]').is(":checked")) == true) {
                $('#trasl').show();
                $('#showbytransportation').show();
            }
            else if ((($('#typeservice_trasl').val()) != "Interpreting") && ($('input[name=transportation]').is(":checked")) == true && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#trasl').show();
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#showTypeMiles').show();
                //               
            }
            else
                if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=transportation]').is(":checked")) == false && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#showbytransportation').show();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
              
            }
            //           
        });
        

        $('#date_finish_claims').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#dateregistration').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#dateregistration_appinfo').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#insurance_date_driver').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#until_app').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        /****************** BOTONES *********************************/
        $('.buttonsCheck').buttonset();
        $('.buttonsCheck_type').buttonset();
        $('.buttonsCheck_days').buttonset();
        $('.buttonsCheck_INF_SERV').buttonset();
        $('#case_manager_id').combobox();
        $('#adjuster_id').combobox();
        $('#insurance_carrier').combobox();
        $('#case_manager_id_claim').combobox();
        $('#adjuster_id_claim').combobox();
        $('#driver1').combobox();
        $('#driver2').combobox({

        });
        $('#typeservice_trasl').combobox({
            selected: function(){
                if(($('#typeservice_trasl').val())=="Interpreting" && $('input[name=transportation]').is(":checked")==false && $('input[name=traduction]').is(":checked")==true){
                    $('#transp').hide();
                    $('#showbytransportation').show();
                    $('#showbytransportation2').hide(); 
                    $('#showTypeMilesNotes').show();
                
                }                
            }        
        });
        $('#languaje_trasl').combobox();
        $('#interprete_trasl').combobox();
        $('#typeapp_appinfo').combobox();
        $('#appdescription_appinfo').combobox();
        /****************** MOSTRAR Y OCULTAR ***********************/

        $('input[name=date_finish]').change(function() {
            if ($(this).val() == '1')
                $('#date_finish_claims').hide();

            else {
                $('#date_finish_claims').show();
                $('#date_finish_claims').val('');
            }
        });
        $('input[name=max_appointment]').change(function() {
            if ($(this).val() == '1')
                $('#max_appointment_claims').hide();
            else
            {
                $('#max_appointment_claims').show();
                $('#max_appointment_claims').val('');
            }
        });
        $('input[name=date_finish_claim]').change(function() {

            $('#date_finish').hide();
            if ($(this).val() == '1')
            {
                $('#date_finish').show();
            }
            if ($(this).val() == '0')
            {
                $('#date_finish').hide();
                $('#date_finish_claims').val("");
            }


        });
        $('input[name=max_appointment_claim]').change(function() {

            $('#max_appointment').hide();
            if ($(this).val() == '1')
            {
                $('#max_appointment').show();
            }

        });
        /******************************** MUESTRO LO OCULTO EN EL FIELDSET INFORMACION DEL SERVICIO ****************/
        $("#AMBULATORY").click(function() {

            $('#btnsamb').show();
            $('#btnswhell').hide();
            $('#btnsstret').hide();
        });
        $("#WHEELCHAIR").click(function() {

            $('#btnswhell').show();
            $('#btnsamb').hide();
            $('#btnsstret').hide();
        });
        $("#STRETCHER").click(function() {

            $('#btnsstret').show();
            $('#btnsamb').hide();
            $('#btnswhell').hide();
            
        });
        $("#location1").autocomplete({
            //define callback to format results
            source: function(req, response) {
                console.log(req);
                $.ajax({
                    url: "/patients/appointmentsheet/search_location",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idContact: $('#idLocation2').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        
                    if (evalResponse(j_response)){
                        response(
                        $.map(j_response.data, function(item) {
                            
                            return {
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias
                            }
                        })
                    );
                    }}
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation1").val(ui.item.idLocation);
                $('#location2').val(ui.item.address);
                $('#unit').val(ui.item.suite);
                $('#phone_showbytransportation').val(ui.item.telephone);
                
            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {

                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.alias + " " + item.label + "</a>")
            .appendTo(ul);
        }



        /************************************* AUTOCOMPLETE PARA CONTACT *********************************/

        $("#contact").autocomplete({
            //define callback to format results
            source: function(req, response) {
                $.ajax({
                    url: "/patients/appointmentsheet/search_contact",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idLocation: $('#idLocation1').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        if (evalResponse(j_response)){
                        response(
                        $.map(j_response.data, function(item) {

                            return {
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias
                            }
                        })
                    );
                    }
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation2").val(ui.item.idLocation);
                $('#address').val(ui.item.address);
                $('#unit_showbytransportation2').val(ui.item.suite);
                $('#phone_showbytransportation2').val(ui.item.telephone);
                
            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {
                    // remove invalid value, as it didn't match anything
                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.alias + " " + item.label + "</a>")
            .appendTo(ul);
        }

    
        /***************************************************************************************************************************************************/
        
        
        
        
        $( "#accordion-appinformation" ).accordion({
            heightStyle: "content"
            
        });
        
        $('#formNewPatNote').hide();
        $('#savePatNote').hide();

        $('#formNewConNote').hide();
        $('#saveConNote').hide();

        $('#formNewDriNote').hide();
        $('#saveDriNote').hide();
        
        $('#formNewIntNote').hide();
        $('#saveIntNote').hide();
        
        $('#formNewEmpNote').hide();
        $('#saveEmpNote').hide();
        
        $('#datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                $("#grid-listApp").trigger("reloadGrid");
            }
        });
   
   
        $('#accordion-appinformation-general').dialog({
            autoOpen: false,       
            height: "auto",
            width: 1000,
            buttons: {
                <?php echo __('Guardar'); ?>: function() {

                },
                <?php echo __('Cancelar'); ?>: function() {
                    $(this).dialog("close");
                }
            }
        });
   
        $('#divPatients').dialog({
            autoOpen: false,       
            height: "auto",
            width: 800 
        });
        $('#divContacts').dialog({
            autoOpen: false,       
            height: "auto",
            width: 800 
        });
        $('#divDrivers').dialog({
            autoOpen: false,       
            height: "auto",
            width: 800 
        });
        $('#divInterpreters').dialog({
            autoOpen: false,       
            height: "auto",
            width: 800 
        });
        $('#divEmployeers').dialog({
            autoOpen: false,       
            height: "auto",
            width: 800 
        });
        
        
        $('#grid-listApp').on('click', '.note',function() {
            $('#TitleEmp').text($(this).attr("data-namepat"));
            idAppointment=$(this).attr("data-idappointment");
            $("#grid-listNotesEmployeers").trigger("reloadGrid");
            $('#divEmployeers').dialog('open');
        }); 
        
        
        $('#grid-listNotesPatients').on('click', '.edit',function() {
            $("#formNewPatNote").slideDown(800);
            $("#closePatNote").show();
            $("#newPatNote").show();
            $('#savePatNote').val('<?php echo __('Actualizar Nota de Paciente'); ?>');
            $('#savePatNote').show();
            idNote=$(this).attr("data-idnote");
            tname="notespatients";
            var dataString="tname="+tname+"&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: getIdNotesPat,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    
                  if (evalResponse(j_response)){
                    var data=j_response.data;
                    $("#descriptionPat").val(data.description);
                    idAppointment=data.idAppointment;
                    idNote=data.idNotes;
                }
                }
            });

        }); 
        
        $('#grid-listNotesPatients').on('click', '.trash',function() {
            idNote=$(this).attr("data-idnote");
            tname="notespatients";
            var dataString="tname="+tname+"&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: deleteNotesPat,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    
                  if (evalResponse(j_response)){
                    $("#grid-listNotesPatients").trigger("reloadGrid");
                }}
            });

        }); 
        
        $('#grid-listNotesContacts').on('click', '.edit',function() {
            $("#formNewConNote").slideDown(800);
            $("#closeConNote").show();
            $("#newConNote").show();
            $('#saveConNote').val('<?php echo __('Actualizar Nota de Contacto'); ?>');
            $('#saveConNote').show();
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: getIdNotesCon,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    var data=j_response.data;
                    $("#descriptionCon").val(data.description);
                    idAppointment=data.idAppointment;
                    idNote=data.idNotes;
                }
            });

        }); 
        
        $('#grid-listNotesContacts').on('click', '.trash',function() {
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: deleteNotesCon,
                data:dataString,
                dataType:'json',
                success: function()
                {
                    $("#grid-listNotesContacts").trigger("reloadGrid");
                }
            });

        }); 
        
        $('#grid-listNotesDrivers').on('click', '.edit',function() {
            $("#formNewDriNote").slideDown(800);
            $("#closeDriNote").show();
            $("#newDriNote").show();
            $('#saveDriNote').val('<?php echo __('Actualizar Nota de Conductor'); ?>');
            $('#saveDriNote').show();
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: getIdNotesDri,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    var data=j_response.data;
                    $("#descriptionDri").val(data.description);
                    idAppointment=data.idAppointment;
                    idNote=data.idNotes;
                }
            });

        }); 
        
        $('#grid-listNotesDrivers').on('click', '.trash',function() {
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: deleteNotesDri,
                data:dataString,
                dataType:'json',
                success: function()
                {
                    $("#grid-listNotesDrivers").trigger("reloadGrid");
                }
            });

        });
        
        $('#grid-listNotesInterpreters').on('click', '.edit',function() {
            $("#formNewIntNote").slideDown(800);
            $("#closeIntNote").show();
            $("#newIntNote").show();
            $('#saveIntNote').val('<?php echo __('Actualizar Nota de Intérprete'); ?>');
            $('#saveIntNote').show();
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: getIdNotesInt,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    var data=j_response.data;
                    $("#descriptionInt").val(data.description);
                    idAppointment=data.idAppointment;
                    idNote=data.idNotes;
                }
            });

        }); 
        
        $('#grid-listNotesInterpreters').on('click', '.trash',function() {
            idNote=$(this).attr("data-idnote");
            var dataString="&idNote="+idNote;
            $.ajax(
            {
                type: "POST",
                url: deleteNotesInt,
                data:dataString,
                dataType:'json',
                success: function(j_response)
                {
                    $("#grid-listNotesInterpreters").trigger("reloadGrid");
                }
            });

        });
        
        $('#newPatNote').button();
        $('#closePatNote').button();
        $('#savePatNote').button();
        
        $('#newConNote').button();
        $('#closeConNote').button();
        $('#saveConNote').button();
        
        $('#newDriNote').button();
        $('#closeDriNote').button();
        $('#saveDriNote').button();
        
        $('#newIntNote').button();
        $('#closeIntNote').button();
        $('#saveIntNote').button();
        
        $('#newEmpNote').button();
        $('#closeEmpNote').button();
        $('#saveEmpNote').button();
        
        $("#newPatNote").click(function() {
            
            $("#formNewPatNote").slideDown(800);
            $("#newPatNote").hide();
            $("#closePatNote").show();
            $('#savePatNote').val('<?php echo __('Guardar Nota de Paciente'); ?>');
            $('#savePatNote').show();
            $('#descriptionPat').val('');
            idNote='0';
        });
        
        $("#closePatNote").click(function() {
            $("#formNewPatNote").slideUp(800);
            $("#closePatNote").hide();
            $("#newPatNote").show();
            $('#savePatNote').hide();
            $('#updatePatNote').hide();
        });
        
        $("#newConNote").click(function() {
            
            $("#formNewConNote").slideDown(800);
            $("#newConNote").hide();
            $("#closeConNote").show();
            $('#saveConNote').val('<?php echo __('Guardar Nota de Contacto'); ?>');
            $('#saveConNote').show();
            $('#descriptionCon').val('');
            idNote=0;
        });
        
        $("#closeConNote").click(function() {
            $("#formNewConNote").slideUp(800);
            $("#closeConNote").hide();
            $("#newConNote").show();
            $('#saveConNote').hide();
            $('#updateConNote').hide();
        });
        
        $("#newDriNote").click(function() {
            
            $("#formNewDriNote").slideDown(800);
            $("#newDriNote").hide();
            $("#closeDriNote").show();
            $('#saveDriNote').val('<?php echo __('Guardar Nota de Conductor'); ?>');
            $('#saveDriNote').show();
            $('#descriptionDri').val('');
            idNote=0;
        });
        
        $("#closeDriNote").click(function() {
            $("#formNewDriNote").slideUp(800);
            $("#closeDriNote").hide();
            $("#newDriNote").show();
            $('#saveDriNote').hide();
            $('#updateDriNote').hide();
        });
        
        $("#newIntNote").click(function() {
            
            $("#formNewIntNote").slideDown(800);
            $("#newIntNote").hide();
            $("#closeIntNote").show();
            $('#saveIntNote').val('<?php echo __('Guardar Nota de Intérprete'); ?>');
            $('#saveIntNote').show();
            $('#descriptionInt').val('');
            idNote=0;
        });
        
        $("#closeIntNote").click(function() {
            $("#formNewIntNote").slideUp(800);
            $("#closeIntNote").hide();
            $("#newIntNote").show();
            $('#saveIntNote').hide();
            $('#updateIntNote').hide();
        });
        
        $("#newEmpNote").click(function() {
            
            $("#formNewEmpNote").slideDown(800);
            $("#newEmpNote").hide();
            $("#closeEmpNote").show();
            $('#saveEmpNote').val('<?php echo __('Guardar Nota de Empleado'); ?>');
            $('#saveEmpNote').show();
            $('#descriptionEmp').val('');
            idNote=0;
        });
        
        $("#closeEmpNote").click(function() {
            $("#formNewEmpNote").slideUp(800);
            $("#closeEmpNote").hide();
            $("#newEmpNote").show();
            $('#saveEmpNote').hide();
            $('#updateEmpNote').hide();
        });
        
        $('#savePatNote').click(function(){
            var descriptionPat = $("#descriptionPat").val();
            var dataString = "descriptionPat="+descriptionPat + "&idAppointment=" + Const_idAppointment +"&idNotes=" + idNote;
            console.log(Const_idAppointment);
            $.ajax(
            {
                type: "POST",
                url: saveNotesPatients,
                data:dataString,
                dataType:'json',
                success: function()
                {
                      if (evalResponse(j_response)){
                    $("#formNewPatNote").slideUp(800);
                    $("#newPatNote").show();
                    $("#closePatNote").hide();
                    $("#grid-listNotesPatients").trigger("reloadGrid");
                    msgBox("<?php echo __('Nota de Paciente guardada con éxito'); ?>");
                }
                }
            });
            
        });
        
        $('#saveConNote').click(function(){
            var descriptionCon = $("#descriptionCon").val();
            var dataString = "descriptionCon="+descriptionCon + "&idAppointment=" + Const_idAppointment +"&idNotes=" + idNote;
            $.ajax(
            {
                type: "POST",
                url: saveNotesContacts,
                data:dataString,
                dataType:'json',
                success: function()
                {
                      if (evalResponse(j_response)){
                    $("#formNewConNote").slideUp(800);
                    $("#newConNote").show();
                    $("#closeConNote").hide();
                    $("#grid-listNotesContacts").trigger("reloadGrid");
                    msgBox("<?php echo __('Nota de Contacto guardada con éxito'); ?>");
                }}
            });
            
        });
        
        $('#saveDriNote').click(function(){
            var descriptionDri = $("#descriptionDri").val();
            var dataString = "descriptionDri="+descriptionDri + "&idAppointment=" + Const_idAppointment +"&idNotes=" + idNote;
            $.ajax(
            {
                type: "POST",
                url: saveNotesDrivers,
                data:dataString,
                dataType:'json',
                success: function()
                {
                      if (evalResponse(j_response)){
                    $("#formNewDriNote").slideUp(800);
                    $("#newDriNote").show();
                    $("#closeDriNote").hide();
                    $("#grid-listNotesDrivers").trigger("reloadGrid");
                    msgBox("<?php echo __('Nota de Conductor guardada con éxito'); ?>");
                }}
            });
            
        });
        
        $('#saveIntNote').click(function(){
            var descriptionInt = $("#descriptionInt").val();
            var dataString = "descriptionInt="+descriptionInt + "&idAppointment=" + Const_idAppointment +"&idNotes=" + idNote;
            $.ajax(
            {
                type: "POST",
                url: saveNotesInterpreters,
                data:dataString,
                dataType:'json',
                success: function()
                {
                      if (evalResponse(j_response)){
                    $("#formNewIntNote").slideUp(800);
                    $("#newIntNote").show();
                    $("#closeIntNote").hide();
                    $("#grid-listNotesInterpreters").trigger("reloadGrid");
                    msgBox("<?php echo __('Nota de Intérprete guardada con éxito'); ?>");
                }}
            });
            
        });
        
        $('#saveEmpNote').click(function(){
            var descriptionEmp = $("#descriptionEmp").val();
            var dataString = "descriptionEmp="+descriptionEmp + "&idAppointment=" + Const_idAppointment +"&idNotes=" + idNote;
            $.ajax(
            {
                type: "POST",
                url: saveNotesEmployeers,
                data:dataString,
                dataType:'json',
                success: function()
                {
                      if (evalResponse(j_response)){
                    $("#formNewEmpNote").slideUp(800);
                    $("#newEmpNote").show();
                    $("#closeEmpNote").hide();
                    $("#grid-listNotesEmployeers").trigger("reloadGrid");
                    msgBox("<?php echo __('Nota de Empleado guardada con éxito'); ?>");
                }}
            });
            
        });
        
        $("#grid-listApp").jqGrid({
            url: listApp,
            datatype: "json",
            postData: {
                date: function() {
                    return $('#datepicker').val();
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Fecha'); ?>', 
                       '<?php echo __('Hora'); ?>', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Dirección'); ?>', 
                       '<?php echo __('Teléfono'); ?>', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Dirección'); ?>', 
                       '<?php echo __('Teléfono'); ?>', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Teléfono'); ?>', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Teléfono'); ?>',
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Teléfono'); ?>',
                       'M',
                       'T',
                       'TS',
                       'TD',
                       'SD',
                       'SI',
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idAppointment', index: 'idAppointment',hidden: true},
                {name: 'appointmentdate', index: 'appointmentdate', search: true, width: 80, align: "center",frozen : true},
                {name: 'appointmenttime', index: 'appointmenttime', search: true, width: 80, align: "center",frozen : true},
                {name: 'namepatient', index: 'namepatient', width: 120, search: true,align: "center",formatter:'showlink'},
                {name: 'addresspatient', index: 'addresspatient', width: 120, search: true,align: "center"},
                {name: 'phonepatient', index: 'phonepatient', width: 80, search: true, align: "center"},
                {name: 'contactname', index: 'contactname', width: 120, search: false,align: "center",formatter:'showlink'},
                {name: 'contactaddress', index: 'contactaddress', width: 120, search: false,align: "center"},
                {name: 'contactphone', index: 'contactphone', width: 80, search: false, align: "center"},
                {name: 'namedriver1', index: 'namedriver1', width: 120, search: false, align: "center",formatter:'showlink'},
                {name: 'phonedriver1', index: 'phonedriver1', width: 80, search: false, align: "center"},
                {name: 'namedriver2', index: 'namedriver2', width: 120, search: false, align: "center",formatter:'showlink'},
                {name: 'phonedriver2', index: 'phonedriver2', width: 80, search: false, align: "center"},
                {name: 'nameinterprete', index: 'nameinterprete', width: 120, search: false,align: "center",formatter:'showlink'},
                {name: 'phoneinterprete', index: 'phoneinterprete', width: 80, search: false, align: "center"},
                {name: 'miles', index: 'miles', width: 50, search: false, align: "center"},
                {name: 'transportation', index: 'transportation', width: 50, search: false, align: "center"},
                {name: 'translation', index: 'translation', width: 50, search: false, align: "center"},
                {name: 'typetravel', index: 'typetravel', width: 50, search: false, align: "center"},
                {name: 'showdriver', index: 'showdriver', width: 50, search: false, align: "center"},
                {name: 'showinterpreter', index: 'showinterpreter', width: 50, search: false, align: "center"},
                
                {name: 'actions', index: 'actions', width: 80, search: false, align: "center",frozen : true}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 400,
            width:1200,
//            shrinkToFit:false,

            pager: 'grid-listApp-paper',
            sortname: 'idAppointment',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Lista de Citas'); ?>",
            
            afterInsertRow: function(row_id, data_id) {
           
                edit = "<a class=\"edit\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                note = "<a class=\"note\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Empleados'); ?>\" ><?php echo __('Notas de Empleados'); ?></a>";
                namepat= "<a href='#' class=\"namepat\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Paciente'); ?>\" >" +data_id.namepatient +"</a>";
                namecont="<a href='#' class=\"namecont\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Contacto'); ?>\" >" +data_id.contactname +"</a>";
                namedr1= "<a href='#' class=\"namedr1\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Conductor'); ?>\" >" +data_id.namedriver1 +"</a>";
                namedr2= "<a href='#' class=\"namedr2\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Conductor'); ?>\" >" +data_id.namedriver2 +"</a>";
                nameint= "<a href='#' class=\"nameint\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Intérprete'); ?>\" >" +data_id.nameinterprete +"</a>";
                $("#grid-listApp").jqGrid('setRowData', row_id, {actions: edit + note, namepatient: namepat,contactname:namecont,namedriver1:namedr1,namedriver2:namedr2,nameinterprete:nameint});
                
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                })
                $('.note').button({
                    icons: {primary: ' ui-icon-document-b'},
                    text: false
                })
            }

        });
        
        
        $("#grid-listApp").jqGrid('setGroupHeaders', {
            useColSpanStyle:true, 
            groupHeaders:[
                {startColumnName: 'appointmentdate', numberOfColumns:2 , titleText: '<?php echo __('Cita'); ?>'},
                {startColumnName: 'namepatient', numberOfColumns:3 , titleText: '<?php echo __('Paciente'); ?>'},
                {startColumnName: 'contactname', numberOfColumns:3 , titleText: '<?php echo __('Contacto'); ?>'},
                {startColumnName: 'namedriver1', numberOfColumns:4 , titleText: '<?php echo __('Conductor'); ?>'},
                {startColumnName: 'namepatient', numberOfColumns:2 , titleText: '<?php echo __('Intérprete'); ?>'},
                
            ]	
        });
        
        $("table#grid-listApp").on('click','.edit', function() {
            
            var idApp = $(this).data('idappointment');
            showLoading = 1;
            $.ajax({
                url: findClientAppointment,
                type: "post",
                data:{idApp: idApp},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        var data_claim = j_response.data_claim;
                        var data_insurancecarrier = j_response.data_insurancecarrier;
                        var data_casemanager = j_response.data_casemanager;
                        var data_adjuster = j_response.data_adjuster;
                        $("#idPatient_app").val(data.idPatient);
                        $("#name_patient").val(data.firstname + ' ' + data.lastname);
                        $("#address_patient").val(data.address);
                        $("#socialsecurity_patient").val(data.socialsecurity);
                        $("#apartmentnumber_patient").val(data.apartmentnumber);
                        $("#phone_patient").val(data.phonenumber);
                        $("#city_patient").val(data.city);
                        $("#altnumber_patient").val(data.altnumber);
                        $("#state_patient").val(data.state);
                        $("#dob_patient").val(data.dob);
                        $("#zipcode_patient").val(data.zipcode);
                        $("#regiondefault_patient").val(data.regiondefault);
                        $("#claimnumber_app").val(data_claim.claimnumber);
                        $("#authorizedby_app").val(data_claim.authorizedby);
                        $("#socialsecurity_patient").val(data_claim.socialsecurity);
                        $("#dateregistration_app").val(data_claim.dateregistration);
                        $("#injury_app").val(data_claim.injury);
                        $("#Insurancecarrier_name_app").val(data_insurancecarrier.name);
                        $("#Casemanager_name_app").val(data_casemanager.name);
                        $("#Adjuster_name_app").val(data_adjuster.name);
                        $("#companyname_app").val(data_insurancecarrier.name);
                        $("#companyphone_app").val(data_insurancecarrier.phone);
                        $('#accordion-appinformation-general').dialog('open');
                    }
                }
            })
            
            var idapp = $(this).data("idappointment");
            $.ajax({
                url: findIdEditApp,
                type: "post",
                data: {idapp: idapp},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        
                                
                        $("#idAppointment").val(data.idAppointment);
                        $("#appauthorize_appinfo").val(data.appauthorized);
                        $("#dateregistration_appinfo").val(data.appointmentdate);
                        $("#typeapp_appinfo").val(data.typeapp);
                        $('#typeapp_appinfo-combobox').val($('#typeapp_appinfo option:selected').text());
                        $("#appdescription_appinfo").val(data.idTypeAppointment);
                        $('#appdescription_appinfo-combobox').val($('#appdescription_appinfo option:selected').text());
                        $("#appointmenttime_appinfo").val(data.appointmenttime);
                        $("#whocalls_app").val(data.whocalls);
                        $("#companyname_app").val(data.companyname);
                        $("#companyphone_app").val(data.companyphone);
                        $("#valtransportation").val(data.showdriver);
                        $("#valtraduction").val(data.showinterpreter);
                        
                        if ((data.showdriver) == '1')
                        {
                            $('input[name=transportation]')
                            .prop('checked', true).trigger('change');
                            $('#driver1-combobox').val($('#driver1 option:selected').text());
                            $("#driver1").val(data.idDriverOneWay);
                            
                            $('#driver2-combobox').val($('#driver2 option:selected').text());
                            $("#driver2").val(data.idDriverRoundTrip);
                            
                            $('input[name=for_typeservice]').val([data.typeservice]);
                            $('input[name=for_typeservice]:checked').trigger('change');
                            $("#flatrate_serv_inf").val(data.frate);
                            $("#flatrate_serv_inf").val(data.frate);
                            var dlocation1 = j_response.location1;
                            $("#location1").val(dlocation1.alias);
                            $("#location2").val(dlocation1.address);
                            $("#unit").val(dlocation1.suite);
                            $("#phone_showbytransportation").val(dlocation1.telephone);
                            
                            var dlocation2 = j_response.location2;
                            $("#contact").val(dlocation2.alias);
                            $("#address").val(dlocation2.address);
                            $("#unit_showbytransportation2").val(dlocation2.suite);
                            $("#phone_showbytransportation2").val(dlocation2.telephone);
                        }

                        if ((data.showinterpreter) == '1')
                        {

                            $('input[name=traduction]')
                            .prop('checked', true).trigger('change');
                            $('#interprete_trasl-combobox').val($('#interprete_trasl option:selected').text());
                            $("#interprete_trasl").val(data.idInterpreter);
                            $('#languaje_trasl-combobox').val($('#languaje_trasl option:selected').text());
                            $("#languaje_trasl").val(data.idLanguage);
                            
                            var dlocation = j_responde.location;
                            $("#location1").val(dlocation.alias);
                            $("#location2").val(dlocation.address);
                            $("#unit").val(dlocation.suite);
                            $("#phone_showbytransportation").val(dlocation.telephone);
                        }

                        $("#valtraduction").val(data.showinterpreter);
                    }

                }
            })
            
            
        });

        $("#grid-listApp").on('click', '.namepat', function() {
            Const_idAppointment = $(this).data("idappointment");
            $("#grid-listNotesPatients").trigger("reloadGrid");
            $('#divPatients').dialog('open');

        });
        
            
        $("#grid-listApp").on('click', '.namecont', function() {
            Const_idAppointment = $(this).data("idappointment");
            $("#grid-listNotesContacts").trigger("reloadGrid");
            $('#divContacts').dialog('open');
        });
            
        $("#grid-listApp").on('click', '.namedr1', function() {
            Const_idAppointment = $(this).data("idappointment");
            $("#grid-listNotesDrivers").trigger("reloadGrid");
            $('#divDrivers').dialog('open');
        });
            
        $("#grid-listApp").on('click', '.namedr2', function() {
            Const_idAppointment = $(this).data("idappointment");
            $("#grid-listNotesDrivers").trigger("reloadGrid");
            $('#divDrivers').dialog('open');
        });
            
        $("#grid-listApp").on('click', '.nameint', function() {
            Const_idAppointment = $(this).data("idappointment");
            $("#grid-listNotesInterpreters").trigger("reloadGrid");
            $('#divInterpreters').dialog('open');
        });
        
        
        
    
        $("#grid-listNotesPatients").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return Const_idAppointment;
                },
                tname: function() {
                    return 'notespatients';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idNote', index: 'idNote',hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            pager: 'grid-listNotesPatients-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Paciente'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listNotesPatients").jqGrid('setRowData', row_id, {actions: edit + trash});       
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });
   
            }
        });
        
        $("#grid-listNotesContacts").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return Const_idAppointment;
                },
                tname: function() {
                    return 'notescontacts';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idNote', index: 'idNote',hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            pager: 'grid-listNotesContacts-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Contacto'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listNotesContacts").jqGrid('setRowData', row_id, {actions: edit + trash});       
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });
   
            }
        });
        
        
        $("#grid-listNotesDrivers").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return Const_idAppointment;
                },
                tname: function() {
                    return 'notesdrivers';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idNote', index: 'idNote',hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            pager: 'grid-listNotesDrivers-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Conductor'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listNotesDrivers").jqGrid('setRowData', row_id, {actions: edit + trash});       
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });
   
            }
        });
        
        $("#grid-listNotesInterpreters").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return Const_idAppointment;
                },
                tname: function() {
                    return 'notesinterpreters';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idNote', index: 'idNote',hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            pager: 'grid-listNotesInterpreters-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Intérprete'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listNotesInterpreters").jqGrid('setRowData', row_id, {actions: edit + trash});       
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });
   
            }
        });
        
        $("#grid-listNotesEmployeers").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return Const_idAppointment;
                },
                tname: function() {
                    return 'notesemployeer';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
          
                {name: 'idNote', index: 'idNote',hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            pager: 'grid-listNotesEmployeers-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Empleados'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";
               
                $("#grid-listNotesEmployeers").jqGrid('setRowData', row_id, {actions: edit + trash});       
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });
   
            }
        });
        
        $('#print').click(function(){
            isSendEmail = false;
            var date = $('#datepicker').val();
            var data = new Object();
            data.date=date;
            $('#divReport').iReportingPlus('option','dynamicParams',data);        
            $('#divReport').iReportingPlus('generateReport','pdf');
    
        });
            
        $('#divReport').iReportingPlus({
            domain:domain,
            repopt:['pdf'],
            clientFolder : 'qmonti',
            clientFile:'MONTI_REPORT_MANIFEST',
            htmlVisor:false,
            jqUI:true,
            urlData:'/patients/manifest/reportPrintAppbyDate',
            xpath:'/report/response/row',
            orientation:'vertical',
            staticParams:rParams,
            reportZoom:1.2,
            responseType:'jsonp',
            jqCallBack:jquery_params,
            waitMessage:'<?php echo __('Generando su Reporte'); ?>...',
            afterJSONResponse:function(response){
                if(isSendEmail){
                    myAfterJSONResponse(response);
                }
            }
            
        });
        
        
        function myAfterJSONResponse(response){
            if(response.code == 1){
                showLoading = 1;
                response.idAppointment = idAppointmentEmail;
                $.ajax({
                    url: sendemail,
                    type:"post",
                    data:response,
                    dataType:"json",
                    success: function(j_response){
                             if (evalResponse(j_response)) {
                        msgBox('<?php echo __('El mensaje ha sido enviado satisfactoriamente.'); ?>','<?php echo __('Confirmación de E-mail'); ?>','');
                             }
                    }
                });
            }else
            {msgBox('<?php echo __('No se pudo enviar e-mail.'); ?>','<?php echo __('Alerta'); ?>','');}
        } 
        
        
    });
</script>
<center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Manifiesto') ?></div>
</center>
<div id="show_content" class="show_content" >
    <label class="titles"><?php echo __('Seleccione fecha para listar citas') ?>:</label>
    <input id="datepicker" class="type_text_normal"  type="text" />
    <input type="hidden" name="date" id="date" >   
    <input type="hidden" id="appDateStartValidar" >
    <input type="hidden" id="appDateFinishValidar" >

    <br><br>
    <button type="button" id="print"><?php echo __('Imprimir Todo') ?></button>
    <button type="button" id="send"><?php echo __('Enviar E-mail') ?></button>

    <center>  
        <br>
        <br>
        <table id="grid-listApp" ></table>
        <div id="grid-listApp-paper"></div>
    </center>

    <div id="divReport" >
    </div>  

<div id="divPatients" title="<?php echo __('Notas de Paciente') ?>">
        <label class="titles"><?php echo __('Lista de Notas de Pacientes para ID de Cita') ?>:</label><label class="titles" id="TitlePat"></label>  
    <center>  
        <br>
        <br>
        <table id="grid-listNotesPatients" ></table>
        <div id="grid-listNotesPatients-paper"></div>
    </center>
   
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('Nueva Nota de Paciente') ?>" id="newPatNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar') ?>" id="closePatNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewPatNote" >
        <form id="validationPatNote">
            <fieldset>
                <legend><?php echo __('Formulario Creación de Nota de Paciente') ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción') ?>:</label> <textarea name="descriptionPat" id="descriptionPat" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="savePatNote" value="<?php echo __('Guardar Nota de Paciente') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p> 
            </fieldset>
            <input type="hidden" id="idnotesPatients" name="idnotesPatients">
        </form>
    </div>
     </div>

<div id="divContacts" title="<?php echo __('Notas de Contacto') ?>" >
        <label class="titles"><?php echo __('Lista de Notas de Contactos para ID de Cita') ?>:</label><label class="titles" id="TitleCon"></label>  
    <center>  
        <br>
        <br>
        <table id="grid-listNotesContacts" ></table>
        <div id="grid-listNotesContacts-paper"></div>
    </center>
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('Nueva Nota de Contacto') ?>" id="newConNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar') ?>" id="closeConNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewConNote" >
        <form id="validationConNote">
            <fieldset>
                <legend><?php echo __('Formulario Creación de Nota de Contacto') ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción') ?>:</label> <textarea name="descriptionCon" id="descriptionCon" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="saveConNote" value="<?php echo __('Guardar Nota de Contacto') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
                </fieldset>
            <input type="hidden" id="idnotesContacts" name="idnotesContacts">
        </form>
    </div>
</div>    
  
<div id="divInterpreters" title="<?php echo __('Notas de Intérprete') ?>" >
        <label class="titles"><?php echo __('Lista de Notas de Intérpretes para ID de Cita') ?>:</label><label class="titles" id="TitleInt"></label>   
    <center>  
        <br>
        <br>
        <table id="grid-listNotesInterpreters" ></table>
        <div id="grid-listNotesInterpreters-paper"></div>
        
    </center>
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('Nueva Nota de Intérprete') ?>" id="newIntNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar') ?>" id="closeIntNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewIntNote" >
        <form id="validationIntNote">
            <fieldset>
                <legend><?php echo __('Formulario Creación de Nota de Intérprete') ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción') ?>:</label> <textarea name="descriptionInt" id="descriptionInt" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="saveIntNote" value="<?php echo __('Guardar Nota de Intérprete') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
            </fieldset>
            <input type="hidden" id="idnotesInterpreters" name="idnotesInterpreters">

        </form>
    </div>
</div>
    
<div id="divDrivers" title="<?php echo __('Notas de Conductor') ?>" >
        <label class="titles"><?php echo __('Lista de Notas de Conductores para ID de Cita') ?>:</label><label class="titles" id="TitleDri"></label>  
    <center>  
        <br>
        <br>
        <table id="grid-listNotesDrivers" ></table>
        <div id="grid-listNotesDrivers-paper"></div>
    </center>
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('Nueva Nota de Conductor') ?>" id="newDriNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar') ?>" id="closeDriNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewDriNote" >
        <form id="validationDriNote">
            <fieldset>
                <legend><?php echo __('Formulario Creación de Nota de Conductor') ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción') ?>:</label> <textarea name="descriptionDri" id="descriptionDri" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="saveDriNote" value="<?php echo __('Guardar Nota de Conductor') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
                
            </fieldset>
            <input type="hidden" id="idnotesDrivers" name="idnotesDrivers">

        </form>
    </div>
</div>    
    
<div id="divEmployeers" title="<?php echo __('Notas de Empleados') ?>" >
        <label class="titles"><?php echo __('Lista de Notas de Empleados para ID de Cita') ?>:</label><label class="titles" id="TitleEmp"></label>  
    <center>  
        <br>
        <br>
        <table id="grid-listNotesEmployeers" ></table>
        <div id="grid-listNotesEmployeers-paper"></div>
    </center>
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('Nueva Nota de Empleado') ?>" id="newEmpNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar') ?>" id="closeEmpNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewEmpNote" >
        <form id="validationEmpNote">
            <fieldset>
                <legend><?php echo __('Formulario Creación de Nota de Empleado') ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción') ?>:</label> <textarea name="descriptionEmp" id="descriptionEmp" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="saveEmpNote" value="<?php echo __('Guardar Nota de Empleado') ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
                
            </fieldset>
            <input type="hidden" id="idnotesEmployeers" name="idnotesEmployeers">

        </form>
    </div>
</div>    

</div>  
<?php include ('appview.php'); ?>

