

<script type="text/javascript">

    var deleteSubTitle = "<?php echo __("Desactivar"); ?>";
    var confirmDeleteMessage = '<?php echo __("¿Está seguro de desactivar el área?"); ?>';
    var confirmActiveMessage = '<?php echo __("¿Está seguro de activar el área?"); ?>';
    var newttitle = '<?php echo __("Nuevo Tipo de Cita"); ?>';
    var activeSubTitle = "<?php echo __("Activar"); ?>";
    var titleEditClaim = "Editar Caso";
    var nuevacita = "Nueva Cita";
    var findId = "/patients/appointmentsheet/getId" + jquery_params;
    var findClientAppointment = "/patients/appointmentsheet/getIdNewAppointment" + jquery_params;
    var saveClaim = "/patients/appointmentsheet/save" + jquery_params;
    var listclaimsbyuser = '/patients/appointmentsheet/consult';
    var listappointmentbyclaimlist = '/patients/appointmentsheet/consult2';
    var delClaim = '/patients/appointmentsheet/delete';
    var saveAppointment = '/patients/appointmentsheet/saveappointment' + jquery_params;
    var url_createUpdateTypeappointment = '/patients/appointmentsheet/createUpdateTypeappointment' + jquery_params;
    var validTypeappointment = '<?php echo __('Tipo de Cita es requerido'); ?>';
    var titleNewDrivers = '<?php echo __('Nuevo Conductor') ?>';
    var titleNewAreaDrivers = '<?php echo __('Nueva Area Conductor') ?>';
    var saveAreadrivers = '/patients/appointmentsheet/saveAreadrivers' + jquery_params;
    var saveDrivers = '/patients/appointmentsheet/saveDrivers' + jquery_params;
    var findIdEditApp = "/patients/appointmentsheet/getIdUpdateAppointment" + jquery_params;
    var delAppointment = "/patients/appointmentsheet/delete_appointment" + jquery_params;
    var saveLanguages = "/patients/appointmentsheet/savelanguaje" + jquery_params;
    var saveLocation = "/patients/appointmentsheet/saveLocation" + jquery_params;
    var saveCourse = "/patients/appointmentsheet/saveCourse" + jquery_params;
    var expandClaim = "/patients/appointmentsheet/expandClaim" + jquery_params;
    var expandApp = "/patients/appointmentsheet/expandClaim" + jquery_params;
    var idAppointment = '0';
    $(document).ready(function() {
        
    
         $('#dialogAppEdit').dialog({
            autoOpen: false,     
            height: "auto",
            width: 900,
            buttons: {
                "<?php echo __('Actualizar Cita') ?>": function() {
                    if (frmappointmentappview.form()) {
                        $('#validationappointment').submit();
                       
                    }
                }  
            },
            close: function() {

            }
        });
    
        $("#validationappointment").find("select").attr("disabled",true).addClass('ui-state-disabled');
        $("#validationappointment").find("input").attr("disabled",true);
        $("#validationappointment").find("textarea").attr("disabled",true);
        $("#validationappointment").find("button").attr("disabled",true).addClass('ui-button-disabled ui-state-disabled');
        
        $("#EditAppEnable").click(function(){

            $("#validationappointment").find("select").attr("disabled",false).removeClass('ui-state-disabled');
            $("#validationappointment").find("input").attr("disabled",false);
            $("#validationappointment").find("textarea").attr("disabled",false);
            $("#validationappointment").find("button").attr("disabled",false).removeClass('ui-button-disabled ui-state-disabled');

        });
        $("#accordion").accordion({
            autoHeight: false
        });
        $('#typeapp_appinfo').combobox();

        $('#hasta').hide();
        $('#transp').hide();
        $('#btnswhell').hide();
        $('#btnsstret').hide();
        $('#btnsamb').hide();
        $('#trasl').hide();
        $('#showbytransportation').hide();
        $('#showbytransportation2').hide();
        $('#showTypeMiles').hide();
        
        $('#typeservice_trasl ,input[name=transportation],input[name=traduction]').change(function() {


//            if(($('#typeservice_trasl').val())=="INTERPRETING" && $('input[name=transportation]').is(":checked")==false && $('input[name=traduction]').is(":checked")==true){
            if ($('#typeservice_trasl').val() == "Interpreting") {

                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#showTypeMilesNotes').show();

            }


            if (($('input[name=traduction]').is(":checked")) == true)
            {
                $('#trasl').show();
                $('#valtraduction').val("1");
                $('#traduction').val("1");
            }

            if (($('input[name=traduction]').is(":checked")) == false)
            {
                $('#typeservice_trasl').val(" ");
                $('#trasl').hide();
                $('#valtraduction').val("0");
                $('#traduction').val("0");
            }

//            if(($('#typeservice_trasl').val())=="INTERPRETING")
//            {
//                $('#showbytransportation').show();
//        
//            }


            if (($('input[name=transportation]').is(":checked")) == true)
            {
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#valtransportation').val("1");
                $('#transportation').val("1");
                $('#showTypeMiles').show();
                $('#maps').show();
            }

            if (($('input[name=transportation]').is(":checked")) == false)
            {
                $('#transp').hide();
                $('#valtransportation').val("0");
                $('#transportation').val("0");
                $('#showbytransportation').hide();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
                $('#maps').hide();
            }

            if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=traduction]').is(":checked")) == true) {
                $('#trasl').show();
                $('#showbytransportation').show();
            }
            else if ((($('#typeservice_trasl').val()) != "Interpreting") && ($('input[name=transportation]').is(":checked")) == true && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#trasl').show();
                $('#transp').show();
                $('#showbytransportation').show();
                $('#showbytransportation2').show();
                $('#showTypeMiles').show();
//               
            }
            else
            if ((($('#typeservice_trasl').val()) == "Interpreting") && ($('input[name=transportation]').is(":checked")) == false && ($('input[name=traduction]').is(":checked")) == true)
            {
                $('#showbytransportation').show();
                $('#showbytransportation2').hide();
                $('#showTypeMiles').hide();
                $('#maps').hide();
            }
//           
        });
        $('#newAppDescription').button({ icons: { primary: 'ui-icon-plus'},text: false});
        $('#NewDriver').button({
            icons: {
                primary: 'ui-icon-plus'
            }
            ,
            text: false
        });
        $('#NewLanguage').button({
            icons: {
                primary: 'ui-icon-plus'
            }
            ,
            text: false
        });
        $('#NewInterprete').button({
            icons: {
                primary: 'ui-icon-plus'
            }
            ,
            text: false
        });


        /************************************* BOTON UPDATE CITA *****************************************/

            /*************** VALIDACION CREAR CITA **********/
            frmappointmentappview = $("#validationappointment").validate({
                rules: {
                },
                messages: {
                },
                errorContainer: '#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {
                    $(element).addClass('ui-state-error');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).removeClass('ui-state-error');
                },
                submitHandler: function(form) {
                    if ($('#traduction').val() == '1' && $('#typeservice_trasl').val()=="Interpreting") {
                        var data_location = 'alias=' + $('#location1').val() + '&address=' + $('#location2').val() + '&unit=' + $('#unit').val() + '&phone=' + $('#phone_showbytransportation').val() + '&idPatient_app=' + $('#idPatient_app').val() + '&idLocation=' + $('#idLocation1').val();
                        $.ajax({
                            url: saveLocation,
                            type: "post",
                            data: data_location,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idLocation1').val(data.idLocation);
                                $('#idLocation_newappointment_appinfo').val(data.idLocation);

                            }
                        });
                    }

                    if ($('#transportation').val() == '1') {
                        var data_location = 'alias=' + $('#contact').val() + '&address=' + $('#address').val() + '&unit=' + $('#unit_showbytransportation2').val() + '&phone=' + $('#phone_showbytransportation2').val() + '&idPatient_app=' + $('#idPatient_app').val() + '&idLocation=' + $('#idLocation2').val();
                        $.ajax({
                            url: saveLocation,
                            type: "post",
                            data: data_location,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idLocation2').val(data.idLocation);
                            }
                        });

                        var data_course = 'idLocationDestination=' + $('#idLocation2').val() + '&idLocationOrigen=' + $('#idLocation1').val() + '&fmiles=' + $('#fmiles').val();
                        $.ajax({
                            url: saveCourse,
                            type: "post",
                            data: data_course,
                            async: false,
                            dataType: "json",
                            success: function(j_response) {
                                var data = j_response.data;
                                $('#idCourse_newappointment_appinfo').val(data.idCourse);
                            }
                        });
                    }
                    var formulario = $(form).serialize();
                    $.ajax({
                        url: saveAppointment,
                        type: "post",
                        data: formulario,
                        async: false,
                        dataType: "json",
                        success: function(j_response) {
                            if (evalResponse(j_response)) {
                                var data = j_response.data;
                                $("grid-appointmentbyclaimlist").trigger('reloadGrid');
                                $('input[name=transportation]').prop('checked', false).trigger('change');
                                $('input[name=traduction]').prop('checked', false).trigger('change');
                                $('input[name=repeatapp]').prop('checked', false).trigger('change');
                                frmappointmentappview.currentForm.reset();
                                $("#dialogAppEdit").dialog('close');
                            }
                        }
                    });

                }

            });
       
        /************************************** BOTON New app type *******************************************/
   
    $("#newAppDescription").click(function() {

            $("#frmNew_apptype").dialog("open");
        });
        /************************************** BOTON NEW DRIVER *********************************************/
        $("#NewDriver").click(function() {

            $("#formNew_driver").dialog("open");
        });
        /************************************** BOTON new area driver ***************************************/
        $("#addarea").click(function() {

            $("#dialogo-areadriver").dialog("open");
        });
        /*************************************** BOTON new language *****************************************/

        $("#NewLanguage").click(function() {

            $("#dialogo-language").dialog("open");
        });
        /************************************** BOTON NUEVA CITA *******************************************/

        /************************ CALENDARIO *****************/

        $('#date_finish_claims').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#dateregistration').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#dateregistration_appinfo').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#insurance_date_driver').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        $('#until_app').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
        /****************** BOTONES *********************************/
        $('.buttonsCheck').buttonset();
        $('.buttonsCheck_type').buttonset();
        $('.buttonsCheck_days').buttonset();
        $('.buttonsCheck_INF_SERV').buttonset();
        $('#maps').button();
        $('#createapp').button();
        $("#frmMap").dialog({
            autoOpen: false,
            height: 680,
            width: 820,
            modal: true
        });
        $("a#routesend").click(function(e) {
            $("#fmiles").attr("value", $("#txtdistance").attr("value"));
            $("#location2").attr("value", $("#txtfcity").attr("value"));
            $("#address").attr("value", $("#txttcity").attr("value"));
            $("#frmMap").dialog('close');
            $("#oneway").prop('checked', true);
        });
        $('#fsearch,#tsearch').button({
            icons: {primary: 'ui-icon-search'},
            text: false
        });
        $("#typetravelp").buttonset();
        $('input[name=roundtrip]').change(function() {
            if ($(this).val() == 'RoundTravel') {
                $('#fmiles').val($('#fmiles').val() * 2);
            } else {
                $('#fmiles').val($('#fmiles').val() / 2);
            }
        });
        $("#maps").click(function(e)
        {
            $("#searchfaddress").attr("value", $("#adcontactAddress").attr("value"));
            $("#searchtaddress").attr("value", $("#fadcontactAddress").attr("value"));
            $("#frmMap").dialog('open');
            initialize();
            e.preventDefault();
        });
        /********************* COMBOBOX ********************************/
        $('#case_manager_id').combobox();
        $('#adjuster_id').combobox();
        $('#insurance_carrier').combobox();
        $('#case_manager_id_claim').combobox();
        $('#adjuster_id_claim').combobox();
        $('#driver1').combobox();
        $('#driver2').combobox({
        });
        $('#typeservice_trasl').combobox({
            selected: function() {
                if (($('#typeservice_trasl').val()) == "Interpreting" && $('input[name=transportation]').is(":checked") == false && $('input[name=traduction]').is(":checked") == true) {
                    $('#transp').hide();
                    $('#showbytransportation').show();
                    $('#showbytransportation2').hide();
                    $('#showTypeMilesNotes').show();

                }
            }
        });
        $('#languaje_trasl').combobox();
        $('#interprete_trasl').combobox();
        $('#typeapp_appinfo').combobox();
        $('#appdescription_appinfo').combobox();
        /******************************** MUESTRO LO OCULTO EN EL FIELDSET INFORMACION DEL SERVICIO ****************/
        $("#AMBULATORY").click(function() {

            $('#btnsamb').show();
            $('#btnswhell').hide();
            $('#btnsstret').hide();
        });
        $("#WHEELCHAIR").click(function() {

            $('#btnswhell').show();
            $('#btnsamb').hide();
            $('#btnsstret').hide();
        });
        $("#STRETCHER").click(function() {

            $('#btnsstret').show();
            $('#btnsamb').hide();
            $('#btnswhell').hide();
        });
        
        $('#idAppointmentView').change(function(){
            
        $.ajax({
                url: "/patients/appointmentsheet/getIdUpdateAppointment",
                type: "post",
                async:false,
                data: {idapp: $('#idAppointmentView').val()},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        $("#idAppointmentView").val(data.idAppointment);
                        $("#idAppointment").val(data.idAppointment);
                        $("#idClaim_newappointment_appinfo").val(data.idClaim);
                        $("#idPatient_newappointment_appinfo").val(data.idPatient);
                        $("#appauthorize_appinfo").val(data.appauthorized);
                        $("#dateregistration_appinfo").val(data.appointmentdate);
                        $("#typeapp_appinfo").val(data.typeapp);
                        $('#typeapp_appinfo-combobox').val($('#typeapp_appinfo option:selected').text());
                        $("#appdescription_appinfo").val(data.idTypeAppointment);
                        $('#appdescription_appinfo-combobox').val($('#appdescription_appinfo option:selected').text());
                        $("#appointmenttime_appinfo").val(data.appointmenttime);
                        $("#whocalls_app").val(data.whocalls);
                        $("#companyname_app").val(data.companyname);
                        $("#companyphone_app").val(data.companyphone);
                        $("#notesFromCallingList").val(data.notescalling);
                        $("#pick_up_time").val(data.pickuptime);
                        $("#final").val(data.droptime);
                        $('input[name=traduction]')
                        .prop('checked', false).trigger('change');
                        $('input[name=transportation]')
                        .prop('checked', false).trigger('change');     
                        
                        if ((data.idDriverRoundTrip) != null)
                        {
                            $('input[name=transportation]').prop('checked', true).trigger('change');
                            $("#driver1").val(data.idDriverOneWay);
                            $('#driver1-combobox').val($('#driver1 option:selected').text());                           
                            $("#driver2").val(data.idDriverRoundTrip);
                            $('#driver2-combobox').val($('#driver2 option:selected').text()); 
                            
                            $('input[name=for_typeservice]').val([data.typeservice]);
                            $('input[name=for_typeservice]:checked').trigger('change');
                            
                            if(data.typeservice=='AMBULATORY')
                            {
                                $('#b_wtime').show();
                                $('#b_liftBase,#b_cassistance').hide();
                            }
                            else if (data.typeservice=='STRETCHER')
                            {          
                                $('#b_liftBase,#b_cassistance,#b_wtime').show();
                                
                            }
                            else if (data.typeservice=='WHEELCHAIR'){
                                $('#b_liftBase,#b_cassistance,#b_wtime').show();
                                
                            }
    
                            $('input[name=for_lbase]').val([data.lbase]);
                            $('input[name=for_lbase]:checked').trigger('change');
    
                            $('input[name=for_cassistance]').val([data.cassistance]);
                            $('input[name=for_cassistance]:checked').trigger('change');
                            
                            if(data.wtime == 1)
                            {
                                $('input[name=for_wtime]').val(['on']);
                                $('input[name=for_wtime]:checked').trigger('change');
                                $('#waiting_time').show();
                                $('#wtime_code').val(data.wtimecode);
                                $('#wtime_hours').val(data.wtimehours);
                                $('#wtime_minutes').val(data.wtimeminutes);
                            }
                                
                            $("#flatrate_serv_inf").val(data.frate);
                            $("#fmiles").val(data.miles);    
                           
                            var dlocation1 = j_response.location1;
                            $("#location1").val(dlocation1.alias);
                            $("#location2").val(dlocation1.address);
                            $("#unit").val(dlocation1.suite);
                            $("#phone_showbytransportation").val(dlocation1.telephone); 
                            
                            var dlocation2 = j_response.location2;
                            $("#contact").val(dlocation2.alias);
                            $("#address").val(dlocation2.address);
                            $("#unit_showbytransportation2").val(dlocation2.suite);
                            $("#phone_showbytransportation2").val(dlocation2.telephone);
                            
                                                      
                        }

                        if ((data.idInterpreter) != null)
                        {
                            $("#typeservice_trasl").val(data.typeservicesinterpreter);
                            $('#typeservice_trasl-combobox').val($('#typeservice_trasl option:selected').text());
                            $('input[name=traduction]')
                            .prop('checked', true).trigger('change');
                            $("#interprete_trasl").val(data.idInterpreter);
                            $('#interprete_trasl-combobox').val($('#interprete_trasl option:selected').text());
                            $("#languaje_trasl").val(data.idLanguage);
                            $('#languaje_trasl-combobox').val($('#languaje_trasl option:selected').text()); 
                            if(data.typeservicesinterpreter=='Interpreting')
                            {
                                var dlocation = j_response.location;
                                $("#location1").val(dlocation.alias);
                                $("#location2").val(dlocation.address);
                                $("#unit").val(dlocation.suite);
                                $("#phone_showbytransportation").val(dlocation.telephone);}
                        }
                        $("#valtraduction").val(data.showinterpreter);
                    }
                }
            });
            
        $.ajax({
                url: findId,
                type: "post",
                async:false,
                data: {idClaim: $('#idClaim_newappointment_appinfo').val()},
                dataType: "json",
                success: function(j_response) {
                    if (evalResponse(j_response)) {
                        var data = j_response.data;
                        $("#idClaim_app").val(data.idClaim);
                        $("#idCasemanager_app").val(data.idCasemanager);
                        $("#idAdjuster_app").val(data.idAdjuster);
                        $("#idInsurancecarrier_app").val(data.idInsuranceCarrier);
                       
                        $('#formNew').dialog('open');
                    }

                }
            })   
            
        $.ajax({
                        url: findClientAppointment,
                        type: "post",
                        async:false,
                        data:
                            {idPatient_app: $("#idPatient_newappointment_appinfo").val(),
                            idClaim_app:  $("#idClaim_app").val(),
                            idAdjuster_app:   $("#idAdjuster_app").val(),
                            idCasemanager_app:   $("#idCasemanager_app").val(),
                            idInsurancecarrier_app: $("#idInsurancecarrier_app").val()
                        },
                        dataType: "json",
                        success: function(j_response) {
                            if (evalResponse(j_response)) {
                                var data = j_response.data;
                                var data_claim = j_response.data_claim;
                                var data_insurancecarrier = j_response.data_insurancecarrier;
                                var data_casemanager = j_response.data_casemanager;
                                var data_adjuster = j_response.data_adjuster;
                                $("#idPatient_app").val(data.idPatient);
                                $("#name_patient").val(data.firstname + ' ' + data.lastname);
                                $("#address_patient").val(data.address);
                                $("#socialsecurity_patient").val(data.socialsecurity);
                                $("#apartmentnumber_patient").val(data.apartmentnumber);
                                $("#phone_patient").val(data.phonenumber);
                                $("#city_patient").val(data.city);
                                $("#altnumber_patient").val(data.altnumber);
                                $("#state_patient").val(data.state);
                                $("#dob_patient").val(data.dob);
                                $("#zipcode_patient").val(data.zipcode);
                                $("#regiondefault_patient").val(data.regiondefault);
                                $("#claimnumber_app").val(data_claim.claimnumber);
                                $("#authorizedby_app").val(data_claim.authorizedby);
                                $("#dateregistration_app").val(data_claim.dateregistration);
                                $("#injury_app").val(data_claim.injury);
                                $("#Insurancecarrier_name_app").val(data_insurancecarrier.name);
                                $("#Casemanager_name_app").val(data_casemanager.name);
                                $("#Adjuster_name_app").val(data_adjuster.name);
                                $("#companyname_app").val(data_insurancecarrier.name);
                                $("#companyphone_app").val(data_insurancecarrier.phone);
                                if(data_claim.unlimited==1)
                                {$("#Fecha-Maxima").val('unlimited');}
                                else if (data_claim.unlimited==0)
                                {$("#Fecha-Maxima").val(data_claim.dateFinish);}
                            }
                        }
                    });
        
        });
       
  

        /************************************ AUTOCOMPLETE PARA LOCATION 1 *******************************************/



        $("#location1").autocomplete({
            //define callback to format results
            source: function(req, response) {
                console.log(req);
                $.ajax({
                    url: "/patients/appointmentsheet/search_location",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idContact: $('#idLocation2').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        response(
                                $.map(j_response.data, function(item) {
                            //                            var text = "<span id='left'>" + item.firstname+ " " + item.lastname + "</span><span id='right'>" + item.socialsecurity+ "</span>";
                            return {
                                //                                label:  text.replace(
                                //                                    new RegExp(
                                //                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                //                                        $.ui.autocomplete.escapeRegex(req.namepatient) +
                                //                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                //                                        ), "<strong>$1</strong>" ),
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias,
                            }
                        })
                                );
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation1").val(ui.item.idLocation);
                $('#location2').val(ui.item.address);
                $('#unit').val(ui.item.suite);
                $('#phone_showbytransportation').val(ui.item.telephone);

            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {

                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.alias + " " + item.label + "</a>")
                    .appendTo(ul);
        };



        /************************************* AUTOCOMPLETE PARA CONTACT *********************************/

        $("#contact").autocomplete({
            //define callback to format results
            source: function(req, response) {
                $.ajax({
                    url: "/patients/appointmentsheet/search_contact",
                    dataType: "json",
                    type: 'post',
                    data: {
                        idLocation: $('#idLocation1').val(),
                        query: req.term
                    },
                    success: function(j_response) {
                        response(
                                $.map(j_response.data, function(item) {

                            return {
                                label: item.address,
                                value: item.alias,
                                idLocation: item.idLocation,
                                suite: item.suite,
                                telephone: item.telephone,
                                idCourse: item.idCourse,
                                address: item.address,
                                alias: item.alias
                            }
                        })
                                );
                    }
                });
            },
            //define select handler
            select: function(e, ui) {
                $("#idLocation2").val(ui.item.idLocation);
                $('#address').val(ui.item.address);
                $('#unit_showbytransportation2').val(ui.item.suite);
                $('#phone_showbytransportation2').val(ui.item.telephone);

            },
            //define select handler
            change: function(event, ui) {
                if (!ui.item) {
                    // remove invalid value, as it didn't match anything
                    return false;
                }
                return true;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + item.alias + " " + item.label + "</a>")
                    .appendTo(ul);
        };

    /******************************* DIALOGO NUEVO TIPO DE CITA ********************************************/
        $('#frmNew_apptype').dialog({
            autoOpen: false,
            modal: true,
            title: newttitle,
            height: 300,
            width: 300
                    ,
            close: function() {
                $('#frmNewTypeappointment').get(0).reset();
                $('#frmNewTypeappointment input[type=hidden]').val('');
            },
            buttons: {
                '<?php echo __('Guardar') ?>': function() {
                    //Funcione el Validate 
                    if (!$('#frmNewTypeappointment').valid()) {
                        return false;
                    }
                    $('#frmNewTypeappointment').submit();
                },
                '<?php echo __('Cancelar') ?>': function() {
                    /*Limpiar campos con reset */
                    //frmNewTypeappointment.currentForm.reset();
                    $(this).dialog('close');
                }
            }
        });
        $('#frmNewTypeappointment').validate({
            rules: {
                name: {
                    required: true
                }


            },
            messages: {
                name: {
                    required: validTypeappointment
                }

            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {
                $.ajax({
                    url: url_createUpdateTypeappointment,
                    type: "post",
                    data: $("#frmNewTypeappointment").serializeObject(),
                    dataType: "json",
                    success: function(j_response) {

                        var data = j_response.data;
                        $('#appdescription_appinfo').append(
                                $('<option/>').attr('value', data.idTypeappointment).text(data.name)
                                ).val(data.idTypeappointment);
                        $('#appdescription_appinfo-combobox').val($('#appdescription_appinfo option:selected').text());
                        $("#frmNew_apptype").dialog('close');
                    }
                });
            }

        });
        /*****************************************************************************************************************/

        /**************************************** DIALOG NEWDRIVERS *************************************/

        $("#formNew_driver").dialog({
            autoOpen: false,
            title: titleNewDrivers,
            height: 500,
            width: 500,
            resizable: false,
            modal: true,
            close: function() {
                $('#validation_driver').get(0).reset();
            },
            buttons: {
                Guardar: function() {
                    if (!$('#validation_driver').valid()) {
                        return false;
                    }
                    $('#validation_driver').submit();
                },
                Cancelar: function() {
                    $(this).dialog("close");
                }
            }


        });
        /*************************DIALOG NEW AREA DRIVERS *************************/
        $("#dialogo-areadriver").dialog({
            autoOpen: false,
            title: titleNewAreaDrivers,
            height: 200,
            width: 200,
            modal: true,
            close: function() {
                $('#formulario-areadriver').get(0).reset();
                $('#formulario-areadriver input[type=hidden]').val('');
            },
            buttons: {
                <?php echo __('Guardar') ?>: function() {
                    if (!$('#formulario-areadriver').valid()) {
                        return false;
                    }
                    $('#formulario-areadriver').submit();
                },
                <?php echo __('Cancelar') ?>: function() {
                    $(this).dialog("close");
                }
            }


        });
        /******************************* VALIDACION  DIALOG AREA DRIVER **********************/


        $('#formulario-areadriver').validate({
            rules: {
                location: {
                    required: true
                }

            },
            messages: {
                location: {
                    required: '<?php echo __("Locación es requerido") ?>'
                }

            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {
                showLoading = 1;
                $.ajax({
                    url: saveAreadrivers,
                    type: "post",
                    data: $('#formulario-areadriver').serializeObject(),
                    dataType: "json",
                    success: function(j_response) {
                        var data = j_response.data;
                        $('#areas_drivers_id').append(
                                $('<option/>').attr('value', data.idAreasdriver).text(data.location)
                                ).val(data.idAreasdriver);
                        $('#areas_drivers_id-combobox').val($('#areas_drivers_id option:selected').text());
                        $("#dialogo-areadriver").dialog('close');
                    }
                });
            }

        });
        /********************************************** VALIDACION NEW DRIVER **************************/
        $('#validation_driver').validate({
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                areas_drivers_id: {
                    required: true
                },
                services: {
                    required: true
                },
                last_name: {
                    required: true
                }


            },
            messages: {
                name: {
                    required: '<?php echo __("Nombre es requerido") ?>'
                },
                last_name: {
                    required: '<?php echo __("Apellido es requerido") ?>'
                },
                areas_drivers_id: {
                    required: '<?php echo __("Area es requerida") ?>'
                },
                services: {
                    required: '<?php echo __("Servicio es requerido") ?>'
                },
                email: {
                    required: '<?php echo __("Correo Electronico es requerido") ?>',
                    email: '<?php echo __("Correo Electronico incorrecto") ?>'
                }


            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function() {
                showLoading = 1;
                $.ajax({
                    url: saveDrivers,
                    type: "post",
                    data:
                            {
                                idDrivers: $('#idDrivers').val()
                                        , name: $('#name_driver').val()
                                        , lastName: $('#last_name_driver').val()
                                        , idAreasdriver: $('areas_drivers_id').val()
                                        , typeservices: $('input[name=services]:checked').val()
                                        , address: $('#address_driver').val()
                                        , city: $('#city_driver').val()

                                        , state: $('#state_driver').val()
                                        , zipcode: $('#zip_code_driver').val()
                                        , phone: $('#phone_driver').val()
                                        , fax: $('#fax_driver').val()

                                        , cellphone: $('#cell_phone_driver').val()
                                        , carMake: $('#car_make_driver').val()
                                        , carModel: $('#car_model_driver').val()
                                        , email: $('#email_driver').val()
                                        , insurance: $('#insurance_date_driver').val()
                                        , identificationNumber: $('#identification_number_driver').val()
                                        , specialNotes: $('#special_notes_driver').val()

                            },
                    dataType: "json",
                    success: function(j_response) {

                        var data = j_response.data;
                        $('#driver1').append(
                                $('<option/>').attr('value', data.idDriver).text(data.name)
                                ).val(data.idDriver);
                        $('#driver2').append(
                                $('<option/>').attr('value', data.idDriver).text(data.name)
                                ).val(data.idDriver)

                        $('#driver1-combobox').val($('#driver1 option:selected').text());
                        $("#formNew_driver").dialog('close');
                    }
                });
            }

        });



        /******************** VALIDACION Y DIALOG PARA LENGUAJE **********************/

        /*Dialog para lenguaje new*/
        $("#dialogo-language").dialog({
            autoOpen: false,
            title: "<?php echo __("Lenguaje") ?>",
            height: "auto",
            width: "auto",
            modal: true,
            resizable: false,
            buttons: {
                "<?php echo __("Guardar") ?>": function() {
                    $("#formulario-language").submit();
                },
                "<?php echo __("Cancelar") ?>": function() {
                    $(this).dialog("close");
                }
            }
        });
        frmLanguageValidate = $("#formulario-language").validate({
            rules: {
                name: {required: true},
                type: {required: true}
            },
            messages: {
                name: {required: '<?php echo __("Nombre es requerido") ?>'},
                type: {required: '<?php echo __("Tipo de Lenguaje es requerido") ?>'}
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            submitHandler: function(form) {
                var formulario = $(form).serialize();
                $.ajax({
                    url: saveLanguages,
                    type: "post",
                    // data:formulario,
                    data: {
                        idLanguage: $('#idNewLanguage').val(),
                        type: $('#type_language').val(),
                        name: $('#name_languaje').val()

                    },
                    dataType: "json",
                    success: function(j_response) {

                        var data = j_response.data;

                        $('#languaje_trasl').append(
                                $('<option/>').attr('value', data.idLanguages).text(data.name)
                                ).val(data.idLanguages);
                        $('#languaje_trasl-combobox').val($('#languaje_trasl option:selected').text());
                        $("#dialogo-language").dialog('close');
                        frmLanguageValidate.currentForm.reset();
                        
                    }
                });
            }

        });
        $("#show_content").show();
        
    });

</script>


<div id="dialogAppEdit">

    <form id="validationappointment">  
        <div id="accordion">  
            <h3><?php echo __("Información de Paciente") ?></h3>
            <div id="newappointment" >

                <input type="hidden" name="idAppointmentView"  id="idAppointmentView" value="0"/>
                <input type="hidden" name="idAppointment"  id="idAppointment" value="0"/>
                <input type="hidden" name="idAlias_newappointment_appinfo"  id="idAlias_newappointment_appinfo" />
                <input type="hidden" name="idCourse_newappointment_appinfo"  id="idCourse_newappointment_appinfo" />
                <input type="hidden" name="idLocation_newappointment_appinfo"  id="idLocation_newappointment_appinfo" />
                <input type="hidden" name="idClaim_newappointment_appinfo"  id="idClaim_newappointment_appinfo" />
                <input type="hidden" name="idPatient_newappointment_appinfo"  id="idPatient_newappointment_appinfo" />
                <input type="hidden" name="idLocation1"  id="idLocation1" />
                <input type="hidden" name="idLocation2"  id="idLocation2" />
                <input type="hidden" name="idPatient_app"  id="idPatient_app" />    
                <input type="hidden" name="until_app"  id="until_app" value="0" />    
                <table>
                    <tr>
                        <td>
                            <label class="frmlbl"><?php echo __('Nombre de Paciente') ?></label>
                            <input class="type_text_normal" type="text" id="name_patient" name="name_patient" value=""/>
                        </td> 
                        <td>
                            <label class="frmlbl"><?php echo __('Dirección') ?></label>
                            <input class="type_text_normal" type="text" id="address_patient" name="address_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Seguro Social') ?></label>
                            <input class="type_text_normal" type="text" id="socialsecurity_patient" name="socialsecurity_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Número de Departamento') ?></label>
                            <input class="type_text_normal" type="text" id="apartmentnumber_patient" name="apartmentnumber_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Teléfono') ?></label>
                            <input class="type_text_normal" type="text" id="phone_patient" name="phone_patient" value=""/>
                        </td>

                    </tr>
                    <tr>
                        <td> 
                            <label class="frmlbl"><?php echo __('Ciudad') ?></label>
                            <input class="type_text_normal" type="text" id="city_patient" name="city_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Estado') ?></label>
                            <input class="type_text_normal" type="text" id="state_patient" name="state_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Fecha de Nacimiento') ?></label>
                            <input class="type_text_normal" type="text" id="dob_patient" name="dob_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Codigo ZIP') ?></label>
                            <input class="type_text_normal" type="text" id="zipcode_patient" name="zipcode_patient" value=""/>
                        </td>

                    </tr>
                </table>  


            </div>
            <h3><?php echo __('Información de Aseguradora') ?></h3>
            <div id="newappointment_insurance" >



                <input type="hidden" name="idInsurancecarrier_app"  id="idInsurancecarrier_app" />
                <input type="hidden" name="idCasemanager_app"  id="idCasemanager_app" />
                <input type="hidden" name="idAdjuster_app"  id="idAdjuster_app" />

                <table>
                    <tr>
                        <td>
                            <label class="frmlbl" ><?php echo __('Nombre de Aseguradora') ?></label>
                            <input class="type_text_normal" type="text" id="Insurancecarrier_name_app" name="Insurancecarrier_name_app" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Administrador de Caso') ?></label>
                            <input class="type_text_normal" type="text" id="Casemanager_name_app" name="Casemanager_name_app" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Ajustador') ?></label>
                            <input class="type_text_normal" type="text" id="Adjuster_name_app" name="Adjuster_name_app" value=""/>
                        </td></tr>           
                </table>     

            </div>

            <h3><?php echo __('Información de Caso') ?></h3>
            <div id="newappointment_claim" >

                <input type="hidden" name="idClaim_app"  id="idClaim_app" />
                <table>
                    <tr>
                        <td>
                            <label class="frmlbl"><?php echo __('Número de Caso') ?></label>
                            <input class="type_text_normal" type="text" id="claimnumber_app" name="claimnumber_app" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Autorizado por') ?></label>
                            <input class="type_text_normal" type="text" id="authorizedby_app" name="authorizedby_app" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Seguridad Social') ?></label>
                            <input class="type_text_normal" type="text" id="socialsecurity_patient" name="socialsecurity_patient" value=""/>
                        </td>
                        <td> 
                            <label class="frmlbl"><?php echo __('Fecha de Registro') ?></label>
                            <input class="type_text_normal" type="text" id="dateregistration_app" name="dateregistration_app" value=""/>
                        </td>
                        <td> 

                            <label class="frmlbl"><?php echo __('Lesión') ?></label>
                            <input class="type_text_normal" type="text" id="injury_app" name="injury_app" value=""/>
                        </td></tr>           
                </table>

            </div>
        </div>
        <br>
        <div id="newappointment_appinfo">
            <fieldset> 

                <legend><?php echo __('Información de la Cita') ?></legend>
                <div id="appinfo">

                    <table>
                        <tr>
                            <td >
                                <label class="frmlbl"><?php echo __('Autorizado por') ?></label>
                            </td><td>
                                <input class="type_text_normal" type="text" size="30" id="appauthorize_appinfo" name="appauthorize_appinfo" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td > 
                                <label class="frmlbl"><?php echo __('Fecha de Cita') ?></label>  </td><td>
                                <input class="type_text_normal" type="text" id="dateregistration_appinfo" name="dateregistration_appinfo" value=""/>

                            </td>
                            <td > 
                                <label class="frmlbl"><?php echo __('Hora de Cita') ?></label></td><td>
                                <input class="type_text_normal" type="text" id="appointmenttime_appinfo" name="appointmenttime_appinfo" value=""/>

                            </td>
                        </tr>
                        <tr>
                            <td> 
                                <label class="frmlbl"><?php echo __('Tipo de Cita') ?></label>
                            </td>
                            <td > 
                                <select name="typeapp_appinfo" id="typeapp_appinfo">
                                    <option value=""><?php echo __('Seleccionar') ?>...</option>
                                    <option value="Legal">Legal</option>
                                    <option value="MedicalL">Medical</option>
                                    <option value="Other">Other</option>
                                </select> 


                            </td>
                            <td> 
                                <label class="frmlbl"><?php echo __('Descripción de la Cita') ?></label></td><td>
                                <select name="appdescription_appinfo" id="appdescription_appinfo">
                                    <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                    <?php foreach ($type_appointment as $ta) { ?>
                                        <option value="<?php echo $ta['id'] ?>"><?php echo $ta['nameapp'] ?></option>

                                    <?php } ?>
                                </select></td><td>
                                <button type="button" id="newAppDescription" class="btnnewAppDescription"><?php echo __('Nueva Descripcion de la Cita') ?></button>
                            </td>

                        </tr>
                    </table>

                </div>

            </fieldset>
        </div>
        <br>
        <div id="newappointment_callinfo">

            <fieldset>
                <legend><?php echo __('Información de LLamada') ?></legend>

                <table>
                    <tr>
                        <td align="right">
                            <label class="frmlbl"><?php echo __('Llamado') ?></label>
                            <input class="type_text_normal" type="text" id="whocalls_app" name="whocalls_app" value=""/>
                        </td>
                        <td align="right">
                            <label class="frmlbl"><?php echo __('Compañia') ?></label>
                            <input class="type_text_normal" type="text" id="companyname_app" name="companyname_app" value=""/>
                        </td>
                        <td align="right">
                            <label class="frmlbl"><?php echo __('Teléfono') ?></label>
                            <input class="type_text_normal" type="text" id="companyphone_app" name="companyphone_app" value=""/>
                        </td>
                    </tr>
                </table>

            </fieldset>

        </div>
        <br>
        <div class="clear"></div>
        <div id="newappointment_servinfo">

            <fieldset>
                <legend><?php echo __('Información del Servicio') ?></legend>

                <dl>
                    <dt class="class_p">
                    <label class="frmlbl"><?php echo __('Transporte') ?></label>
                    <input type="checkbox" id="transportation" name="transportation" value="" />
                    <input type="hidden" name="valtransportation"  id="valtransportation" value="0"/>
                    </dt>
                </dl>


                <div id="transp">
                    <br>
                    <dl>
                        <dt><label class="frmlbl" id="typeservice"><?php echo __('Servicios') ?></label></dt>
                        <dd class="buttonsCheck_type" >
                            <input type="radio" id="AMBULATORY" name="for_typeservice" value="Ambulatory" /><label for="AMBULATORY"><?php echo __("Ambulatory") ?></label>
                            <input type="radio" id="WHEELCHAIR" name="for_typeservice" value="Wheelchair" /><label for="WHEELCHAIR"><?php echo __("Wheelchair") ?></label>
                            <input type="radio" id="STRETCHER" name="for_typeservice" value="Stretcher" /><label for="STRETCHER"><?php echo __("Stretcher") ?></label>
                        </dd>
                    </dl>

                    <dl>

                        <label class="frmlbl"><?php echo __('Conductor') ?> 1</label>
                        <select name="driver1" id="driver1" value="0">
                            <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                            <?php foreach ($drivers as $d_id) { ?>
                                <option value="<?php echo $d_id['idDriver'] ?>"><?php echo $d_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>

                            <?php } ?>
                        </select>
                        <button type="button" id="NewDriver" class="btnNewDriver"><?php echo __('Nuevo Conductor') ?></button>
                    </dl>


                    <dl>
                        <label class="frmlbl"><?php echo __('Conductor') ?> 2</label>
                        <select name="driver2" id="driver2" value="0" >
                            <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                            <?php foreach ($drivers as $d_id) { ?>
                                <option value="<?php echo $d_id['idDriver'] ?>"><?php echo $d_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>

                            <?php } ?>
                        </select>
                    </dl>

                    <dl>
                        <dt class="class_p">
                        <label class="frmlbl"><?php echo __('Tarifa Plana') ?></label>
                        <input class="type_text_normal" type="text" id="flatrate_serv_inf" name="flatrate_serv_inf" value=""/>
                        <br>
                        </dt>
                    </dl>


                    <div id="btnswhell">
                        <dd class="buttonsCheck_INF_SERV" >
                            <input type="radio" id="LIFT_BASE" name="for_service" value="LIFT_BASE"/><label for="LIFT_BASE"><?php echo __("Lift Base") ?></label>
                            <input type="radio" id="CAR ASSISTANCE" name="for_service" value="CAR ASSISTANCE" /><label for="CAR ASSISTANCE"><?php echo __("Car Assistance") ?></label>
                            <input type="radio" id="WAITING_TIME" name="for_service" value="WAITING_TIME" /><label for="WAITING_TIME"><?php echo __("Waiting Time") ?></label>
                        </dd>
                    </div>

                    <div id="btnsstret">
                        <dd class="buttonsCheck_INF_SERV" >
                            <input type="radio" id="LIFT_BASE_STRET" name="for_service" value="LIFT_BASE" /><label for="LIFT_BASE_STRET"><?php echo __("Lift Base") ?></label>
                            <input type="radio" id="ASSISTANCE" name="for_service" value="ASSISTANCE" /><label for="ASSISTANCE"><?php echo __("Assistance") ?></label>
                            <input type="radio" id="WAITING_TIME_STRET" name="for_service" value="WAITING_TIME" /><label for="WAITING_TIME_STRET"><?php echo __("Waiting Time") ?></label>
                        </dd>
                    </div>

                    <div id="btnsamb">
                        <dd class="buttonsCheck_INF_SERV" >
                            <input type="radio" id="WAITING_TIME_AMB" name="for_service" value="WAITING_TIME" /><label for="WAITING_TIME_AMB"><?php echo __("Waiting Time") ?></label>
                        </dd>
                    </div>
                </div>    
                <dl>
                    <br>
                    <dt class="class_p">
                    <label class="frmlbl"><?php echo __('Traducción') ?></label>
                    <input type="checkbox" id="traduction" name="traduction" value = "" />
                    <input type="hidden" name="valtraduction"  id="valtraduction" value="0"/>

                    </dt>
                </dl>

                <div id="trasl">
                    <dl>

                        <br><label class="frmlbl"><?php echo __('Tipo de Servicio') ?></label>
                        <dd id="combotypeservice">
                            <select name="typeservice_trasl" id="typeservice_trasl">
                                <option value=""><?php echo __('Seleccionar') ?>...</option>
                                <option value="Interpreting">Interpreting</option>
                                <option value="Conference Call">Conference Call</option>
                                <option value="Document Translation">Document Translation</option>
                            </select> 
                        </dd>

                    </dl>


                    <dl>
                        <label class="frmlbl"><?php echo __('Lenguaje') ?></label>
                        <select name="languaje_trasl" id="languaje_trasl">
                            <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                            <?php foreach ($languaje as $l_id) { ?>
                                <option value="<?php echo $l_id['idLanguage'] ?>"><?php echo $l_id['name'] ?></option>

                            <?php } ?>
                        </select>
                        <button type="button" id="NewLanguage" class="btnNewLanguage"><?php echo __('Nuevo Lenguaje') ?></button>
                    </dl>

                    <dl>
                        <label class="frmlbl"><?php echo __('Intérprete') ?></label>
                        <select name="interprete_trasl" id="interprete_trasl" >
                            <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                            <?php foreach ($interprete as $i_id) { ?>
                                <option value="<?php echo $i_id['idInterpreter'] ?>"><?php echo $i_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>
                            <?php } ?>
                        </select>
                        <button type="button" id="NewInterprete" class="btnNewInterprete"><?php echo __('Nuevo Intérprete') ?></button>

                    </dl>



                </div> 
            </fieldset>

        </div>
        <br>
        <div id="newappointment_pickupinfo">

            <fieldset>
                <legend><?php echo __('Información de Recojo') ?></legend>
                <div id="form_newappointment_pickupinfo">

                    <table>
                        <tr>
                            <td>
                                <label class="frmlbl"><?php echo __('Inicio') ?></label>
                                <input class="type_text_normal" type="text" id="pick_up_time" name="pick_up_time" value=""/>
                            </td>
                            <td>
                                <label class="frmlbl"><?php echo __('Final') ?></label>
                                <input class="type_text_normal" type="text" id="final" name="final" value=""/>
                            </td>
                        </tr>
                    </table>

                </div>


                <div class="clear"></div>

                <div id="showbytransportation">
                    <fieldset>
                        <legend><?php echo __('From') ?></legend>

                        <button type="button" id="maps" ><?php echo __('Cálculo de Millas') ?></button>

                        <dl>
                            <dt class="class_p">

                            <label class="frmlbl"><?php echo __('Alias') ?></label><br>
                            <input class="type_text_normal" type="text" id="location1" name="location1" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Dirección') ?></label><br>
                            <input class="type_text_normal" type="text" id="location2" name="location2" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Unidad') ?></label><br>
                            <input class="type_text_normal" type="text" id="unit" name="unit" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Teléfono') ?></label><br>
                            <input class="type_text_normal" type="text" id="phone_showbytransportation" name="phone_showbytransportation" value=""/>

                            </dt>
                        </dl>

                    </fieldset>
                </div>



                <div id="showbytransportation2">
                    <fieldset>
                        <legend><?php echo __('Hasta') ?></legend>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Contacto') ?></label><br>
                            <input class="type_text_normal" type="text" id="contact" name="contact" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Dirección') ?></label><br>
                            <input class="type_text_normal" type="text" id="address" name="address" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Unidad') ?></label><br>
                            <input class="type_text_normal" type="text" id="unit_showbytransportation2" name="unit" value=""/>

                            </dt>
                        </dl>

                        <dl>
                            <dt class="class_p">
                            <label class="frmlbl"><?php echo __('Teléfono') ?></label><br>
                            <input class="type_text_normal" type="text" id="phone_showbytransportation2" name="phone_showbytransportation" value=""/>

                            </dt>
                        </dl>

                    </fieldset>
                </div>
                <div id="showTypeMiles">
                    <br>
                    <table>
                        <tr>
                            <td>
                    <p id="typetravelp">
                        <input type="radio" id="oneway" name="roundtrip" value="OneWay" checked/><label for="oneway">One way</label>
                        <input type="radio" id="roundtrip" name="roundtrip" value="RoundTravel" /><label for="roundtrip">Round trip</label>
                    </p></td>
                    <br><td>
                    <p id="milesp">
                        <label class="frmlblshort"><?php echo __('Millas') ?>:</label>
                        <input type="hidden" name="omiles" id="omiles" size="15" >
                        <input class="ui-widget ui-widget-content ui-corner-all" type="text" name="fmiles" id="fmiles" size="15" >
                    </p></td></tr></table>
                    <br>
                </div>
                <p id="notesp">
                    <label class="frmlbl"><?php echo __('Lista de Notas de Llamada') ?>:</label>
                    <textarea class="ui-widget ui-widget-content ui-corner-all" name="notesFromCallingList" id="notesFromCallingList" cols="27" rows="6" > </textarea>
                </p>
                <div class="clear"></div>
            </fieldset>
        </div>
   
    </form>
   

<br>
<input type="checkbox" id="EditAppEnable" /><label> <?php echo __('Activar Edición') ?></label>

</div>


    <div id="formNew_driver">
        <form id="validation_driver" name="validation_driver" action="" method="post">
            <fieldset> 
                <legend><?php echo __('Información de Conductor') ?></legend>    
                <input type="hidden" name="idDrivers"  id="idDrivers" value="">
                <ul>expandTo
                    <li>
                        <dl>
                            <dt>
                            <label class="frmlbl"><?php echo __('Nombre') ?></label>
                            </dt>
                            <dd>
                                <input type="text" id="name_driver" name="name_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Apellido') ?></label></dt>
                            <dd>
                                <input type="text" id="last_name_driver" name="last_name_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Area') ?></label></dt>
                            <dd>
                                <select name="areas_drivers_id" id="areas_drivers_id">
                                    <option value=""><?php echo __('Seleccionar'); ?>...</option>                         
                                    <?php foreach ($area_driver as $ad) { ?>
                                        <option value="<?php echo $ad['id'] ?>"><?php echo $ad['area'] ?></option>

                                    <?php } ?>
                                </select> 
                                <button type="button" id="addarea" name="addarea"><?php echo __("Agregar") ?></button>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl" id="services"><?php echo __('Servicios') ?></label></dt>
                            <dd class="buttonsCheck" >
                                <input type="radio" id="A" name="for_drive" value="Ambulatory" checked=""/><label for="A"><?php echo __("Ambulatory") ?></label>
                                <input type="radio" id="W" name="for_drive" value="Wheelchair" /><label for="W"><?php echo __("Wheelchair") ?></label>
                                <input type="radio" id="S" name="for_drive" value="Stretcher" /><label for="S"><?php echo __("Stretcher") ?></label>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Dirección') ?></label></dt>
                            <dd>
                                <input type="text" name="address_driver" id="address_driver" />
                            </dd>
                        </dl>         
                        <div class="clear"></div>
                    </li>      

                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Ciudad') ?></label></dt>
                            <dd>
                                <input type="text" name="city_driver" id="city_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Estado') ?></label></dt>
                            <dd>
                                <input type="text" name="state_driver" id="state_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Codigo Zip') ?></label></dt>
                            <dd>
                                <input type="text" name="zip_code_driver"  id="zip_code_driver"/>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Teléfono') ?></label></dt>
                            <dd>
                                <input type="text" name="phone_driver" id="phone_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Fax') ?></label></dt>
                            <dd>
                                <input type="text" name="fax_driver" id="fax_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Celular') ?></label></dt>
                            <dd>
                                <input type="text" name="cell_phone_driver" id="cell_phone_driver" />                
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Marca de Carro') ?></label></dt>
                            <dd>
                                <input type="text" name="car_make_driver" id="car_make_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Modelo de Carro') ?></label></dt>
                            <dd>
                                <input type="text" name="car_model_driver" id="car_model_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Correo Electrónico') ?></label></dt>
                            <dd>
                                <input type="text" name="email_driver" id="email_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Fecha de Seguro') ?></label></dt>
                            <dd>
                                <input type="text" name="insurance_date_driver" id="insurance_date_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Número de Identificación') ?></label></dt>
                            <dd>
                                <input type="text" name="identification_number_driver" id="identification_number_driver" />
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>
                    <li>
                        <dl>
                            <dt><label class="frmlbl"><?php echo __('Notas Especiales') ?></label></dt>
                            <dd>
                                <textarea name="special_notes_driver" id="special_notes_driver" cols="27" rows="6" ></textarea>
                            </dd>
                        </dl>
                        <div class="clear"></div>
                    </li>


                </ul>        

            </fieldset>


        </form>
    </div>


    <!-- ********** AREA DRIVER *************** -->

    <div id="dialogo-areadriver">
        <form method="POST" id="formulario-areadriver" name="formulario-areadriver" action="">
            <fieldset>  
                <label for="name" ><?php echo __('Locación') ?>:</label>
                <input type="text" name="location_driverarea" id="location_driverarea"/> 
                <br/>
                <input type="hidden" name="idDriverarea" id="idDriverarea" value=""/> 
                <br/>    
            </fieldset>
        </form>   
    </div> 



    <!-- ********** NUEVO LENGUAJE *************** -->

    <div id="dialogo-language" class="label_output"> 
        <form method="POST" id="formulario-language">
            <fieldset> 
                <legend><?php echo __('Crear Lenguaje') ?></legend>
                <br><label class="frmlabel" for="name" ><?php echo __('Nombre') ?>:</label> <br>
                <input type="text" name="name" id="name_languaje"/> <br/><br>
                <label class="frmlabel" for="type"><?php echo __('Tipo:') ?></label><br>

                <select id="type_language" name="type_language">
                    <option value="Regular"><?php echo __('Regular') ?></option>
                    <option value="Rare"><?php echo __('Raro') ?></option>
                    <option value="Other"><?php echo __('Otro') ?></option>
                </select> 
                <input type="hidden" name="idNewLanguage" id="idNewLanguage" value="0"/> <br/>    


            </fieldset>
        </form>   

    </div>
 <!--NUEVO TIPO DE CITA-->

    <div id="frmNew_apptype">
        <form id="frmNewTypeappointment" name="frmNewTypeappointment" action="" method="POST">
            <fieldset>
                <legend><? echo __('Crear Tipo De Cita') ?></legend>
                <input type="text" name="idTypeappointment" id="idTypeappointment" hidden="true"/>
                <dl>
                    <dd>
                        <label class="frmlbl"><? echo __('Nombre') ?></label>
                        <input type="text" name="name" id="name" size="20" value="">
                    </dd>
                </dl>    

                <dl>
                    <dd>
                        <label class="frmlbl"><?php echo __('Tipo') ?></label>
                        <select name="type" id="type">
                            <option value="Medical"><?php echo __('Medical') ?></option>
                            <option value="Legal"><?php echo __('Legal') ?></option>
                            <option value="Other"><?php echo __('Otro') ?></option>
                        </select>
                    </dd>
                </dl>

                <!--<img id="addic" src="">-->
            </fieldset>
        </form>
    </div>


