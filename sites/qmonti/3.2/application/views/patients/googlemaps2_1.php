<script type="text/javascript" charset="UTF-8"  src="http://maps.google.com/maps/api/js?sensor=false" ></script>

<script type="text/javascript">
    
    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();
    var map;
    var marker1 = null;
    var marker2 = null;
    var geocoder = new google.maps.Geocoder();

    function initialize() {

         directionsDisplay = new google.maps.DirectionsRenderer();
                var chicago = new google.maps.LatLng(41.850033, -87.6500523);
                var myOptions = {
                    zoom:7,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: chicago
                }
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                directionsDisplay.setMap(map);
    }
   function searchFrom(){
        if (document.getElementById("searchfaddress").value.toString().length <2){
            return;
        }
        var address = document.getElementById("searchfaddress").value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                document.getElementById("txtfcity").value = results[0].formatted_address;
                if (marker1){
                    marker1.setPosition(results[0].geometry.location);
                }
                else{
                    marker1 = new google.maps.Marker({
                        icon : "/images/marker_from.png",
                        position: results[0].geometry.location,
                        map: map});
                }
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });

    }

    function searchTo(){
        if (document.getElementById("searchtaddress").value.toString().length <2){
            return;
        }
        var address = document.getElementById("searchtaddress").value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                document.getElementById("txttcity").value = results[0].formatted_address;
                if (marker2){
                    marker2.setPosition(results[0].geometry.location);
                }
                else{
                    marker2 = new google.maps.Marker({
                        icon : "/images/marker_to.png",
                        position: results[0].geometry.location,
                        map: map});
                }
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });}

    function route() {
        if (marker1==null || marker2 ==null){
            return;
        }
        var request = {
            origin:marker1.getPosition(),
            destination:marker2.getPosition(),
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                var miles;
                miles = response.routes[0].legs[0].distance.value * 621 / 1000000;
                document.getElementById("txtdistance").value = Math.round(miles,0);
                directionsDisplay.setDirections(response);
            }
        });
    }
    
</script>


<div id="frmMap" class="dialog" width="850px" height="700px">
 <table width="50%" border="0">
  <tr>
    <td width="10%">From:</td>
    <td width="34%"><input type="text" id="searchfaddress" size="40" /></td>
    <td width="9%"><a href="#frmAddress" id="fsearch" onclick="searchFrom()"><img  src="/images/search.png" alt="search" border="0" /></a></td>
    <td width="14%">Location:</td>
    <td width="33%"><input class="" name="fname" id="txtfcity" type="text" value="" size="40"  />   </td>
  </tr>
  <tr>
    <td>To:</td>
    <td><input type="text" id="searchtaddress" size="40"  />    </td>
    <td><a href="#frmAddress" id="tsearch" onclick="searchTo()"><img src="/images/search.png" alt="search" border="0" /></a></td>
    <td>Location:</td>
    <td><input class="" name="tname" id="txttcity" type="text" value="" size="40" />      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="4">&nbsp;</td>
  </tr>
</table>
        <p style="float: left; clear: none;"> <a id="drawroute" href="#frmAddress"  class="fg-button ui-state-default ui-priority-primary ui-corner-all" onclick="route();">Draw Route & Calculate Mileage</a> </p>
        <p style="float: left; clear: none;"> <a id="routesend" href="#addfromdiv"  class="fg-button ui-state-default ui-priority-primary ui-corner-all">Save Locations</a> </p>
        <p style="margin-top: 30px; clear: none;"> Mileage: <input type="text" id="txtdistance" readonly="true"/>
        <div id="map_canvas" style="float:left; width:800px; height:500px"></div>
</div>
