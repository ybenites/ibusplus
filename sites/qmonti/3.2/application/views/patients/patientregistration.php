<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<style type="text/css"> 

    .main-body {

        border: 1.5px solid #E5E5E3;
        border-radius: 6px 6px 6px 6px;
        color: #222222;
        margin: 4em 13em 5em 13em;
        height: auto;
        width:80%; /*ancho*/
        padding: 10px;
    }

    .main-inbody {
        background: repeat-x scroll 50% 0 #FEFEFE;
        border: 1.5px solid #E5E5E3;
        border-radius: 6px 6px 6px 6px;
        color: #222222;
        margin: 10px;
        height: auto;
        width: 96.5%;
        padding: 10px;
    }

    #dialoPatient fieldset{
        padding: 3px 2px 4px 15px;
    }

    legend, h3 {
        color: #0171B2;
        font-size: 14px;
        font-weight: bold;
    }

    .type_text_normal{

        width: 200px; /* una longitud definida ancho */
        height:15px;
        margin: 5px 0;
        float:left;
        padding-top: 8px;
    }

    #newCase fieldset{
        padding: 3px 2px 4px 7px;
    }

    #newAdjuster fieldset{
        padding: 3px 2px 4px 7px;
    }


    .frmlabel {
        display: block;
        width: 200px;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 8px;
        font-size:13px;

    }

    .frmlabel-dialog {
        display: block;
        width: 150px;
        float: left;
        font-weight: bold;
        height: 14px;
        padding-top: 8px;

        font-size:11px;

    }

    .type_text_normal_dialog{

        width: 130px; /* una longitud definida ancho */
        height:15px;
        margin: 5px 0;
        float:left;
        padding-top: 8px;
    }

    .class_p{
        clear: left;
        float: left;
        margin: 5px 0;
    }

    .titleTables{
        font-size: 17px;
        font-weight: bold;
        margin-bottom: 0;
        margin-top: 0;
        padding: 0;
        text-align: center;
        width: 50%;
    }


</style>

<script type="text/javascript">
    var savePatient = "/patients/patientregistration/save" + jquery_params;
    var saveCase = "/patients/patientregistration/saveCase" + jquery_params;
    var url_createUpdateCasemanager= '/management/casemanagers/createUpdateCasemanager'+jquery_params;
    var url_createUpdateAdjuster = '/management/adjusters/createUpdateAdjuster'+jquery_params;

    $(document).ready(function(){
     
        $("#selectCase").combobox();
        $("#idProvider").combobox();
        $("#idInsuranceCarriers").combobox();
        $("#selectAdjus").combobox();
        $("#regiondefault").combobox();
        $("#idInsuranceCarriers_information").combobox();
        $(".buttonSex").buttonset();
        $(".buttonUnlimited").buttonset();
        
        $('[name=unlimited]').change(function(){
            $('#textunlimited').hide();
            if($(this).val()== '0'){
            
                $('#textunlimited').show();
            }
             
        });
        
        
        $('[name=unlimitedapp]').change(function(){
            $('#textunlimitedapp').hide();
            if($(this).val()== '0'){
            
                $('#textunlimitedapp').show();
            }
             
        });
        
         
        $('#textunlimited').datepicker({
            dateFormat:'yy-mm-dd',
            changeMonth:true,
            changeYear:true
            
        });
        
        $("#createPatient").button({
            icons:{
                primary:'ui-icon-disk'
            }
            
        });
        
        //dialog para adjuster
        $('#newAdjuster').dialog({
            autoOpen:false,
            title:'Ajustadores',
            height:'auto',
            width:'auto',
            modal:true,
            resizable:false,
            buttons:{
                '<?php echo __("Guardar") ?>':function(){
                    $("#frmNewAdjuster").submit();
                },
                
                '<?php echo __("Cancelar") ?>':function(){
                    $(this).dialog('close');
                }
            }
        });
        
        $("#addAdjus").click(function(){
            $("#newAdjuster").dialog('open');
        });
        
        
        //dialog para case
        $('#newCase').dialog({
            autoOpen:false,
            title:'<?php echo __("Administrador de Caso") ?>',
            height:'auto',
            width:'auto',
            modal:true,
            resizable:false,
            buttons:{
                '<?php echo __("Guardar") ?>':function(){
                    $("#frmNewCasemanagers").submit();
                },
                
                '<?php echo __("Cancelar") ?>':function(){
                    $(this).dialog('close');
                }
            }
        });
        
        $('#addCase').click(function(){
            $('#newCase').dialog('open');  
        });
            
            
        //CREA ADJUSTER
        frmAdj = $('#frmNewAdjuster').validate({
            rules:{
                name:{required:true}
            },
            messages:{
                name:{required:'<?php echo __("Ajustador es requerido") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){
                var formulario= $("#frmNewAdjuster").serialize();
                showLoading = 1;
                $.ajax({
                    url: url_createUpdateAdjuster,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        console.log(j_response);
                        var data=j_response.data;
                                
                        if (evalResponse(j_response)){
                                         
                            $('#selectAdjus').append(
                            $('<option/>').attr('value',data.idAdjuster).text(data.name)).val(data.idAdjuster);
                            $('#selectAdjus-combobox').val($('#selectAdjus option:selected').text());
                            $('#newAdjuster').dialog('close');
                            frmAdj.currentForm.reset();
                        }
                                
                                
                    }
                })
            }
                    
        });
            
            
            
            
        // guarda CASE MANAGER
            
        frmCase = $('#frmNewCasemanagers').validate({
            rules:{
                name:{required:true}
            },
            messages:{
                name:{required:'<?php echo __("Administrador de Caso es requerido") ?>'}
            },
            errorContainer:'#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
            unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
            submitHandler:function(){
                var formulario= $("#frmNewCasemanagers").serialize();
                showLoading = 1;
                $.ajax({
                    url: url_createUpdateCasemanager,
                    type:"post",
                    data:formulario,
                    dataType:"json",
                    success: function(j_response){
                        console.log(j_response);
                        var data=j_response.data;
                                
                        if (evalResponse(j_response)){
                                         
                            $('#selectCase').append(
                            $('<option/>').attr('value',data.idCasemanager).text(data.name)).val(data.idCasemanager);
                            $('#selectCase-combobox').val($('#selectCase option:selected').text());
                            $('#newCase').dialog('close');
                            frmCase.currentForm.reset();
                        }
                                
                                
                    }
                })
            }
                    
        });
                
            
            
        //GUARDA patient y claim
        $("#createPatient").click(function(){
                
            frmNewPatient = $("#formPatient").validate({
                rules: {  
                    firstname: {required:true},
                    lastname: {required:true},
                    socialsecurity:{required:true},
                    sex:{required:true},
                    idInsuranceCarrier:{required:true},
                    accidentday:{required:true},
                    claimnumber:{required:true},
                    selectCase:{required:true},
                    selectAdjus:{required:true}
                
                },   
                messages: {  
   
                firstname: {required:'<?php echo __("Nombre es requerido") ?>'},
                lastname: {required:'<?php echo __("Apellido es requerido") ?>'},
                socialsecurity : {required:'<?php echo __("Seguridad Social es requerido") ?>'},
                sex : {required:'<?php echo __("Género es requerido") ?>'},
                idInsuranceCarrier : {required:'<?php echo __("Aseguradora es requerido") ?>'},
                accidentday : {required:'<?php echo __("Día de Incidente es requerido") ?>'},
                claimnumber : {required:'<?php echo __("Número de Caso es requerido") ?>'},
                selectCase : {required:'<?php echo __("Administrador de Caso es requerido") ?>'},
                selectAdjus : {required:'<?php echo __("Ajustador es requerido") ?>'}
                
                },
                errorContainer:'#errorMessages',
                errorLabelContainer: "#errorMessages .content .text #messageField",
                wrapper: "p",
                highlight: function(element, errorClass) {$(element).addClass('ui-state-error');},
                unhighlight:function(element, errorClass, validClass){$(element).removeClass('ui-state-error');},
                submitHandler:function(){
                    var formulario= $("#formPatient").serialize();
                    showLoading = 1;
                    $.ajax({
                        url: savePatient,
                        type:"post",
                        data:formulario,
                        dataType:"json",
                        success: function(j_response){
                            var data=j_response.data;
                            console.log(data);
                            if (evalResponse(j_response)){

                                frmNewPatient.currentForm.reset();
                            }
                        }
                    })
                }
            
            });
        });
            
        $('#addCase').button({
            icons:{
                primary:'ui-icon-plus'
            }
            ,
            text:false
        });
        
        
        $("#addAdjus").button({
            icons:{
                primary:'ui-icon-plus'
            }
            ,
            text:false
        });
        
        $("#dob").datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth: true,
            changeYear: true 
        });
        
        $("#accidentday").datepicker({
            dateFormat : 'yy-mm-dd',
            changeMonth: true,
            changeYear: true 
        });
    });

</script>



<div class="background">
    <div class="main-body">
        <center>    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Registro de Pacientes') ?></div></center>
        <div class="main-inbody">
            <div id="dialoPatient"> 
                <form method ="POST" name="formPatient" id="formPatient">
                    <fieldset>   
                        <legend><?php echo __('Información de Paciente') ?></legend>
                        <input type="text" name="idPatient" id="idPatient" hidden="true"/>   

                        <p class="class_p">
                            <label for="firstname" class="frmlabel"><?php echo __('Nombre') ?></label>
                            <input class="type_text_normal" type="text" id="firstname" name="firstname" id="firstname" />    
                        </p>

                        <p class="class_p">
                            <label for="lastname" class="frmlabel"><?php echo __('Apellido') ?></label>
                            <input class="type_text_normal" type="text" id="lastname" name="lastname" id="lastname" />   
                        </p>

                        <p class="class_p">
                            <label for="socialsecurity" class="frmlabel"><?php echo __('Seguridad Social') ?></label>
                            <input class="type_text_normal" type="text" id="socialsecurity" name="socialsecurity" id="socialsecurity" />    
                        </p>

                        <p class="class_p">
                            <label for="sex" class="frmlabel"><?php echo __('Género') ?></label>
                            <span class="buttonSex">
                                <input type="radio" id="sexM" name="sex" value="m"><label for="sexM"><?php echo __("M") ?></label>
                                <input type="radio" id="sexF" name="sex" value="f" /><label for="sexF"><?php echo __("F") ?></label>
                            </span>
                        </p>

                        <p class="class_p">
                            <label for="regiondefault" class="frmlabel"><?php echo __('Región') ?></label>
                            <select id="regiondefault" name="regiondefault">
                                <option value="FLORIDA"><?php echo __('FLORIDA') ?></option>
                                <option value="GEORGIA"><?php echo __('GEORGIA') ?></option>
                                <option value="OTRO"><?php echo __('OTRO') ?></option>
                            </select> 
                        </p>

                        <p class="class_p">
                            <label for="address" class="frmlabel"><?php echo __('Dirección') ?></label>
                            <input class="type_text_normal" type="text" id="address" name="address" id="address" />    
                        </p>

                        <p class="class_p">
                            <label for="apartmentnumber" class="frmlabel"><?php echo __('Número de Apartamento') ?></label>
                            <input class="type_text_normal" type="text" id="apartmentnumber" name="apartmentnumber" id="apartmentnumber" />    
                        </p>

                        <p class="class_p">
                            <label for="city" class="frmlabel"><?php echo __('Ciudad') ?></label>
                            <input class="type_text_normal" type="text" id="city" name="city" id="city" />    
                        </p>

                        <p class="class_p">
                            <label for="state" class="frmlabel"><?php echo __('Estado') ?></label>
                            <input class="type_text_normal" type="text" id="state" name="state" id="state" />    
                        </p>

                        <p class="class_p">
                            <label for="zipcode" class="frmlabel"><?php echo __('Código ZIP') ?></label>
                            <input class="type_text_normal" type="text" id="zipcode" name="zipcode" id="zipcode" />    
                        </p>

                        <p class="class_p">
                            <label for="phonenumber" class="frmlabel"><?php echo __('Teléfono') ?></label>
                            <input class="type_text_normal" type="text" id="phonenumber" name="phonenumber" id="phonenumber" />    
                        </p>

                        <p class="class_p">
                            <label for="altnumber" class="frmlabel"><?php echo __('Número Alternativo') ?></label>
                            <input class="type_text_normal" type="text" id="altnumber" name="altnumber" id="altnumber" />    
                        </p>

                        <p class="class_p">
                            <label for="dob" class="frmlabel"><?php echo __('Fecha de Nacimiento') ?></label>
                            <input class="type_text_normal" type="text" id="dob" name="dob" id="dob" />    
                        </p>

                        <p class="class_p">
                            <label for="employer" class="frmlabel"><?php echo __('Empleado') ?></label>
                            <input class="type_text_normal" type="text" id="employer" name="employer" id="employer" />    
                        </p>

                    </fieldset>  
                    <br>

                    <fieldset>
                        <legend><?php echo __('Información de Aseguradora y Casos') ?></legend>

                        <p class="class_p">
                            <label for="InsuranceCarriers" class="frmlabel "><?php echo __('Compañía de Seguros') ?></label>
                            <select name="idInsuranceCarriers_information" id="idInsuranceCarriers_information">
                                <option value=""><?php echo __('Seleccionar ...') ?></option>
                                <?php foreach ($insurance as $insu) { ?> 
                                    <option value="<?php echo $insu['idInsurancecarrier'] ?>"> <?php echo $insu['name'] ?> </option>
                                <?php }; ?>
                            </select>  
                        </p>

                        <p class="class_p">
                            <label for="authorizedby" class="frmlabel"><?php echo __('Autorizado por') ?></label>
                            <input class="type_text_normal" type="text" id="authorizedby" name="authorizedby"  />    
                        </p>

                        <p class="class_p">
                            <label for="accidentday" class="frmlabel"><?php echo __('Día de Accidente') ?></label>
                            <input class="type_text_normal" type="text" id="accidentday" name="accidentday"  />    
                        </p>

                        <p class="class_p">
                            <label for="claimnumber" class="frmlabel"><?php echo __('Número de Caso') ?></label>
                            <input class="type_text_normal" type="text" id="claimnumber" name="claimnumber"  />    
                        </p>

                        <p class="class_p">
                            <label for="injury" class="frmlabel"><?php echo __('Lesión') ?></label>
                            <input class="type_text_normal" type="text" id="injury" name="injury"  />    
                        </p>

                        <p class="class_p">
                            <label for="unlimited" class="frmlabel"><?php echo __('Fecha Ilimitada') ?></label>
                            <span class="buttonUnlimited">
                                <input type="radio" id="yesUn" name="unlimited" checked =" true" value="1"><label for="yesUn"><?php echo __("Yes") ?></label>
                                <input type="radio" id="falseUn" name="unlimited" value="0" /><label for="falseUn"><?php echo __("No") ?></label>
                                <input hidden="true" style=" margin: 4px;" type="text" id="textunlimited" name="textunlimited"  />   
                            </span>
                        </p>

                        <p class="class_p">
                            <label for="unlimitedapp" class="frmlabel"><?php echo __('Cita Ilimitada') ?></label>
                            <span class="buttonUnlimited">
                                <input type="radio" id="yesUnAp" name="unlimitedapp" checked="true" value="1"><label for="yesUnAp"><?php echo __("Yes") ?></label>
                                <input type="radio" id="falseUnAp" name="unlimitedapp" value="0" /><label for="falseUnAp"><?php echo __("No") ?></label>
                                <input hidden="true" style=" margin: 4px;" type="text" id="textunlimitedapp" name="textunlimitedapp"  />   
                            </span>
                        </p>

                        <p class="class_p">
                            <label for="selectCase" class="frmlabel"><?php echo __('Administrador de Caso') ?></label>
                            <select name="selectCase" id="selectCase">
                                <option value=""><?php echo __('Seleccionar'); ?>...</option>   
                                <?php foreach ($adminCasos as $case) { ?>
                                    <option value="<?php echo $case['idCasemanager'] ?>"><?php echo $case['name'] ?></option>
                                <?php }; ?>
                            </select>
                            <button type="button" id="addCase" name="addCase"><?php echo __("Agregar") ?></button>
                        </p>

                        <p class="class_p">
                            <label for="selectAdjus" class="frmlabel"><?php echo __('Ajustador') ?></label>
                            <select name="selectAdjus" id="selectAdjus">
                                <option value=""><?php echo __('Select ...'); ?></option>   
                                <?php foreach ($adjuster as $adj) { ?>
                                    <option value="<?php echo $adj['idAdjuster'] ?>"><?php echo $adj['name'] ?></option>
                                <?php }; ?>
                            </select>
                            <button type="button" id="addAdjus" name="addAdjus"><?php echo __("Agregar") ?></button>
                        </p>


                    </fieldset>    
                    <br><br> <center><input type="submit" value="Create Patient" name="createPatient" id="createPatient" ></button></center>
                </form>
            </div>
        </div>


    </div>    
</div>


<div id="newCase">
    <form id="frmNewCasemanagers" name="frmNewCasemanagers" action="" method="post">
        <fieldset>
            <legend><?php echo __('Crear Administrador de Caso') ?></legend>

            <dl>
                <dd>                
                    <label class="frmlabel-dialog "><?php echo __('Administrador de Caso') ?></label>

                    <input class="type_text_normal_dialog" type="text" name="name_caseM" id="name_caseM" size="30">

                    <input type="text" name="idCasemanagers" value="" id="idCasemanagers" hidden="true"/>   

                </dd>
            </dl>

            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Proveedor') ?></label>
                    <select name="idProvider" id="idProvider">
                        <option value=""><?php echo __('Seleccionar') ?>...</option>
                        <?php foreach ($provider as $prov) { ?>
                            <option value="<?php echo $prov['idProvider'] ?>"><?php echo $prov['name'] ?></option>
                        <?php }; ?>
                    </select>

                </dd>
            </dl>

            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('E-mail') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="manager_email" id="manager_email" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Teléfono') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="phone_caseM" id="phone_caseM" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Extension') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="extentionPhone_caseM" id="extentionPhone_caseM" size="10" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Fax') ?></label> 
                    <input class="type_text_normal_dialog" type="text" name="fax_caseM" id="fax_caseM" size="30" value="">
                </dd>

<!--<img id="addic" src="">-->
            </dl>

        </fieldset>
    </form>
</div>


<div id="newAdjuster">
    <form id="frmNewAdjuster" name="frmNewAdjuster" action="" method="post">
        <fieldset>
            <legend><?php echo __('Crear Ajustador') ?></legend>
            <input type="text" name="idAjustador" id="idAjustador" hidden="true"/>    
            <dl>
                <dd>                
                    <label class="frmlabel-dialog "><?php echo __('Ajustador') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="name_adjust" id="name_adjust" size="30">
                </dd>
            </dl>

            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Compañía de Seguros:') ?></label>
                    <select name="idInsuranceCarriers" id="idInsuranceCarriers">
                        <option value=""><?php echo __('Seleccionar') ?>...</option>
                        <?php foreach ($insurance as $insu) { ?> 
                            <option value="<?php echo $insu['idInsurancecarrier'] ?>"> <?php echo $insu['name'] ?> </option>
                        <?php }; ?>
                    </select>

                </dd>
            </dl>

            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('E-mail') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="adjuster_email" id="adjuster_email" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Teléfono:') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="phone_adjuster" id="phone_adjuster" size="30" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Extension') ?></label>
                    <input class="type_text_normal_dialog" type="text" name="extentionPhone_adjuster" id="extentionPhone_adjuster" size="10" value="">
                </dd>
            </dl>
            <dl>
                <dd>
                    <label class="frmlabel-dialog "><?php echo __('Fax') ?></label> 
                    <input class="type_text_normal_dialog" type="text" name="fax_adjuster" id="fax_adjuster" size="30" value="">
                </dd>

<!--<img id="addic" src="">-->
            </dl>

        </fieldset>
    </form>
</div>

