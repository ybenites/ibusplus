<script type="text/javascript">
    
    var listAdjustersPatients="/reports/adjusterspatients/listAdjustersPatients";
    
   $(document).ready(function() {
       
          $("#idAdjuster").combobox({
              selected:function() { $("#grid-listAdjustersPatients").trigger('reloadGrid');}
          });
          
          $("#clean").button({
              icon:{primary: 'ui-icon-refresh'}
          }).click(function(){
              $("#idAdjuster-combobox").val('');
              $("#dateIni,#dateEnd").val('');
              $("#grid-listAdjustersPatients").trigger('reloadGrid');
          });
          
          $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($('#dateIni').val()!='' && $('#dateEnd').val()!=''){
                    $("#grid-listAdjustersPatients").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
        
                if($( "#dateIni" ).val()!='') $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateEnd" ).val()!='') $( "#dateIni" ).datepicker( "option", "maxDate", selectedDate );
            } 
        });
          
          gridlistAdjustersPatients = $("#grid-listAdjustersPatients").jqGrid({
            url: listAdjustersPatients,
            datatype: "json",
            postData: {
                idAdjuster: function() {
                    return $("#idAdjuster").val();
                },
                dateIni: function() {
                    return $("#dateIni").val();
                },
                dateEnd: function() {
                    return $("#dateEnd").val();
                }
            },
            colNames: ['idPatient', 
                       '<?php echo __('Ajustador'); ?>', 
                       '<?php echo __('Nombres'); ?>', 
                       '<?php echo __('Apellidos'); ?>', 
                       '<?php echo __('Teléfono'); ?>', 
                       '<?php echo __('Dirección'); ?>', 
                       '<?php echo __('Opción'); ?>'],
            colModel: [
                {name: 'idPatient', index: 'idPatient', hidden: true, width: 70},
                {name: 'idAdjuster', index: 'idAdjuster', search: true, width: 70},
                {name: 'firstname', index: 'firstname', search: true, width: 150},
                {name: 'lastname', index: 'lastname', search: true, width: 150},
                {name: 'phonenumber', index: 'phonenumber', search: true, width: 120},
                {name: 'address', index: 'address', search: true, width: 250},
                {name: 'option', index: 'option', search: false, width: 70}
                
            ],
            grouping: true,
            groupingView: {
                groupField: ['idAdjuster'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'],
                groupCollapse: false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 450,
            pager: 'grid-listAdjustersPatients-paper',
            sortname: 'idAdjuster',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('Ajustadores de Pacientes'); ?>",
            gridComplete: function() {

                var aData = gridlistAdjustersPatients.getDataIDs();
                $.each(aData, function() {

                    check = "<input class=\"goTo\" ";
                    check += "data-id=\"" + gridlistAdjustersPatients.getCell(this, 'idPatient') + "\" ";
                    check += "name=\"check_" + gridlistAdjustersPatients.getCell(this, 'idPatient') + "[]\" ";
                    check += "id=\"check_" + this + "\" ";
                    check += "value=\"" + gridlistAdjustersPatients.getCell(this, 'idPatient') + "\" ";
                    check += "type=\"checkbox\" />";
                    check += "<label for=\"check_" + this + "\"><?php echo __('Seleccionar'); ?></label>";
                    gridlistAdjustersPatients.setRowData(this
                            , {
                        option: check
                    });

                });

               $('.goTo').button({
                    icons: {
                        primary: 'ui-icon-arrowreturnthick-1-e'
                    },
                    text: false
                }).click(function() {
                    window.location.href = 'http://qmonti/patients/appointmentsheet/index/appointments?patient_id=' + $(this).val();
                });
               
            }
        });
        
        
   });
</script>
<div id="show_content" class="show_content">
    <div class="float_right cont_filter">
    <div class="block1_theme content_filter ui-corner-all">
        <div class="padding10">
            <div>
                <div class="title_theme ui-corner-all">
                    <h2><?php echo __('Filter'); ?></h2>                
                </div>
                <div class="row_item">
                    <span class="li_item"></span>
                    <div class="item_1">
                        <label><?php echo __('Ajustador'); ?></label>
                    </div>
                    <div class="item_2">
                       <select name="idAdjuster" id="idAdjuster" value="0" >
                        <option value=""><?php echo __('Seleccionar'); ?></option>                         
                        <?php foreach ($adjusters as $a_id) { ?>
                            <option value="<?php echo $a_id['idAdjuster'] ?>"><?php echo $a_id['name'] ?></option>

                        <?php } ?>
                    </select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row_item">
                    <span class="li_item"></span>
                    <div class="item_1">
                        <label for="dateIni"><?php echo __('Desde'); ?></label>
                    </div>
                    <div class="item_2">
                        <input name="dateIni" id="dateIni" type="text"/>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row_item">
                    <span class="li_item"></span>
                    <div class="item_1">
                        <label for="dateEnd"><?php echo __('Hasta'); ?></label>
                    </div>
                    <div class="item_2">
                        <input name="dateEnd" id="dateEnd" type="text"/>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row_item">
                    <div class="item_2">
                        <button id="clean" title="Clean"><?php echo __('Limpiar'); ?></button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
    
    <table id="grid-listAdjustersPatients" ></table>
    <div id="grid-listAdjustersPatients-paper"></div>
</div>