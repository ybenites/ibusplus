<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script type="text/javascript">

   $(document).ready(function() {
   var listReportPatients="/reports/patient/listReportPatients";
    var reportPatients="/reports/patient/reportPatients";     
       
          gridlistReportPatient = $("#grid-listReportPatients").jqGrid({
            url: listReportPatients,
            datatype: "json",
            postData: {
              
                dateIni: function() {
                    return $("#dateIni").val();
                },
                dateEnd: function() {
                    return $("#dateEnd").val();
                }
            },
            colNames: ['id','letter', 
                       '<?php echo __('Nombre(s)'); ?>', 
                       '<?php echo __('Apellido'); ?>', 
                       'Call in by', 
                       '<?php echo __('Compania de Seguros'); ?>', 
                       '<?php echo __('Fecha'); ?>', 
                       'emp'],
            colModel: [
                {name: 'idPat', index: 'idPat', width: 70, hidden:true},
                {name: 'letter', index: 'letter', width: 70},
                {name: 'firstname', index: 'firstname', search: true, width: 150},
                {name: 'lastname', index: 'lastname', search: true, width: 150},
                {name: 'call', index: 'call', search: true, width: 150},
                {name: 'insurance', index: 'insurance', search: true, width: 250},
                {name: 'registrationDate', index: 'registrationDate', search: false, width: 150},
                {name: 'emp', index: 'emp', search: false, width: 150,hidden:true}
                
            ],
            grouping: true,
            groupingView: {
               groupField : ['letter'],
                groupColumnShow : [false],
                groupText : ['<b>{0}</b>'],
                groupCollapse : false,
                groupOrder: ['ASC'],
                groupSummary : [false],
                showSummaryOnHide: false,
                groupDataSorted : false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 500,
            pager: 'grid-listReportPatients-paper',
            sortname: 'idPat',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('Pacientes'); ?>",
            gridComplete: function() {
              var aData = gridlistReportPatient.getDataIDs();
                $.each(aData, function() {

//                   alert(a_insurance[0]);
                    summary='';
                    if(gridlistReportPatient.getCell(this, 'insurance')!='')
                     { 
                         var a_insurance=gridlistReportPatient.getCell(this, 'insurance').split(",");  
                    for( var i =  0 ; i< a_insurance.length;  ++i  )  { 
                    
                    var getData = a_insurance[i].split("|");
                    summary += '<hr/><?php echo __('Caso'); ?>:' + getData[0] + '<br/><?php echo __('Compañía de Seguros'); ?>:' + getData[1] + '<br/><?php echo __('Ajustador'); ?>:' + getData[2];
                    
                    }
                    summary = summary.substring(5);
                    }
                    gridlistReportPatient.setRowData(this
                            , {
                        insurance: summary,        
                        registrationDate: gridlistReportPatient.getCell(this, 'registrationDate') + "<br>" +  gridlistReportPatient.getCell(this, 'emp')
                    });

                });
            }
        });
        $("#grid-listReportPatients").jqGrid('navGrid', '#grid-listReportPatients-paper',
                            {edit: false, add: false, del: false},
                    {},
                            {},
                            {},
                            {multipleSearch: true, multipleGroup: true}
                    );
       $('#newpatient').button(); 
       $('#newpatient').change(function(){
            if($(this).is(':checked')){
                //$("#list").jqGrid('appendPostData',{newPatient: $(this).val()});
                $("#grid-listReportPatients").jqGrid('setGridParam',{postData:{newpatient: function(){ return $("#newpatient").val();}} });
            }else{
                //$("#list").jqGrid('removePostDataItem',"newPatient");                
                delete $("#grid-listReportPatients").jqGrid('getGridParam' ,'postData' )['newpatient'];
            }
            $("#grid-listReportPatients").trigger("reloadGrid");
        });
         
        $("#print").button({
              icons:{primary: 'ui-icon-print'}
          }).click(function(){
             
              var data = new Object();
            data.dateIni=$('#dateIni').val();
            data.dateEnd= $('#dateEnd').val();

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
          });
          
          $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($('#dateIni').val()!='' && $('#dateEnd').val()!=''){
                    $("#grid-listReportPatients").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
        
                if($( "#dateIni" ).val()!='') $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateEnd" ).val()!='') $( "#dateIni" ).datepicker( "option", "maxDate", selectedDate );
            } 
        });   
        
        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_REPORT_PATIENTS',
            htmlVisor: false,
            jqUI: true,
            urlData: reportPatients,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su reporte'); ?>...'
//            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
//            }

        });
   });
</script>    
<div id="show_content" class="show_content">
   
    <div class="divCont">
    <label class="frmlbl"><?php echo __('Filtro por Rango de Fechas'); ?></label>
 <div class="clear"></div>
      <label class="short_label" for="dateFrom"><?php echo __('Desde'); ?></label>
      <input name="dateIni" id="dateIni" type="text"/>
      <label class="short_label"  for="dateTo" ><?php echo __('Hasta'); ?></label>
      <input name="dateEnd" id="dateEnd" type="text"/>
        <label for="newpatient"><?php echo __('Nuevo Paciente'); ?></label>
<input type="checkbox" name="newpatient" id="newpatient" value="newpatient" alt="New Patients"/>
        <button id="print" title="Print"><?php echo __('Imprimir'); ?></button>
</div>
    <br>
    <center>
       <table id="grid-listReportPatients" ></table>
    <div id="grid-listReportPatients-paper"></div>
    </center>
    <div id="divReport">
    </div>    
</div>
