<script type="text/javascript">
    $(document).ready(function() {

        var listPayments = '/reports/underpaymentsreports/listPayments';
        var listChecks = '/reports/underpaymentsreports/listChecks';
        var reportPayments= '/reports/underpaymentsreports/listChecks';

        $("#idInsurance").combobox({
            selected: function() {
                $("#grid-listPayments").trigger('reloadGrid');
            }
        });

        $("#clean").button({
            icon: {primary: 'ui-icon-refresh'}
        }).click(function() {
            $("#idCaseManager-combobox").val('');
            $("#dateIni,#dateEnd").val('');
            $("#grid-listPayments").trigger('reloadGrid');
        });

        $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if ($('#dateIni').val() != '' && $('#dateEnd').val() != '') {
                    $("#grid-listPayments").trigger("reloadGrid");
                }
            },
            onClose: function(selectedDate) {

                if ($("#dateIni").val() != '')
                    $("#dateEnd").datepicker("option", "minDate", selectedDate);
                if ($("#dateEnd").val() != '')
                    $("#dateIni").datepicker("option", "maxDate", selectedDate);
            }
        });

       table_under_payments =  $("#grid-listPayments").jqGrid({
            url: listPayments,
            datatype: "json",
            postData: {
                dateIni: function() {
                    return $("#dateIni").val();
                },
                dateEnd: function() {
                    return $("#dateEnd").val();
                },
                idInsurance: function() {
                    return $("#idInsurance").val();
                }
            },
            height: 300,
            colNames: ['id', 
                       '<?php echo __('Aseguradora'); ?>', 
                       '<?php echo __('Paciente'); ?>', 
                       '<?php echo __('ID'); ?>', 
                       '<?php echo __('Fecha'); ?>', 
                       '<?php echo __('Tipo'); ?>', 
                       '<?php echo __('Monto'); ?>', 
                       '<?php echo __('Pagado'); ?>', 
                       '<?php echo __('Opción'); ?>'],
            colModel: [
                {name: 'id', index: 'id', width: 150, hidden:true},
                {name: 'insurance', index: 'insurance', width: 150},
                {name: 'patient', index: 'patient', width: 150},
                {name: 'billing', index: 'billing', width: 100, align: "right"},
                {name: 'date', index: 'date', width: 100, align: "center"},
                {name: 'type', index: 'type', width: 150, align: "center"},
                {name: 'amount', index: 'amount', width: 100, align: "right"},
                {name: 'paid', index: 'paid', width: 150, sortable: false, align: "right"},
                {name: 'option', index: 'option', width: 70, sortable: false, align: "right"}
            ],
            rowNum: 10,
            rowList: [10, 20, 30],
            pager: 'grid-listPayments-paper',
            sortname: 'insurance_name',
            viewrecords: true,
            sortorder: "desc",
            multiselect: false,
            subGrid: true,
//            subGridUrl: listChecks,
//            subCaption:"Check Information",
//            subGridModel: [{name: ['idCheck', 'Number', 'Memo', 'Date', 'Available', 'Paid'],
//                    width: [55, 200, 80, 80, 80,80],
//                    params: ['billing']}
//            ],
            caption: "<?php echo __('Forma de Pagos'); ?>",
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id+"_t";
               
                $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
                jQuery("#"+subgrid_table_id).jqGrid({
                    url:listChecks,
                    datatype: "json",
                    postData:{
                       
                        billing : row_id
                    },
                    height: '100%',
                    width : 600,
                    colNames:['idcheck',
                              '<?php echo __('Número'); ?>',
                              '<?php echo __('Memo'); ?>', 
                              '<?php echo __('Fecha de Cheque'); ?>', 
                              '<?php echo __('Disponible'); ?>',
                              '<?php echo __('Pagado'); ?>'],
                    colModel:[
                        {name:'idcheck',index:'idcheck', hidden:true},
                        {name:'number',index:'number', width:70,align:"center"},
                        {name:'memo',index:'memo', width:70,align:"center"},
                        {name:'date',index:'date', width:70,align:"center"},
                        {name:'available',index:'available', width:70, align:"center"},
                        {name:'paid',index:'paid', width:70, align:"center"}
                    ],
                    rowNum:20,
                    pager: pager_id,
                    sortname: 'billing_check_id',
                    sortorder: "asc",
                    caption: "<?php echo __('Información de Cheque'); ?>"
                });
              
            },
            gridComplete:function(){
               
                
                $.each(table_under_payments.getDataIDs(),function(){                    
                    btn_print_invoice  = "<button class=\"print_invoice\" ";                    
                    btn_print_invoice  += "data-billing_id=\""+this+"\"";                   
                    btn_print_invoice  += "type=\"button\" >";
                    btn_print_invoice  += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print_invoice  += "</button>";

                    table_under_payments.setRowData(this,{                        
                        option: btn_print_invoice 
                    });
                });
                
                $('.print_invoice').button({
                    icons: {
                        primary: 'ui-icon-print'
                    },
                    text:false
                }).click(function(){
                    
                     var data = new Object();
            data.idBilling=$(this).attr('data-billing_id');
            

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
                    
                });
                
            }            
        });
        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_REPORT_PAYMENTS',
            htmlVisor: false,
            jqUI: true,
            urlData: reportPayments,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...'
        });

    });
</script>
<div id="show_content" class='show_content'>
    <div class="float_right cont_filter">
        <div class="block1_theme content_filter ui-corner-all">
            <div class="padding10">
                <div>
                    <div class="title_theme ui-corner-all">
                        <h2><?php echo __('FILTRO'); ?></h2>                
                    </div>
                    <div class="row_item">
                        <span class="li_item"></span>
                        <div class="item_1">
                            <label><?php echo __('Compañía de Seguros'); ?></label>
                        </div>
                        <div class="item_2">
                            <select name="idInsurance" id="idInsurance" value="0" >
                                <option value=""><?php echo __('Seleccionar'); ?></option>                         
                                <?php foreach ($insurance as $a_id) { ?>
                                    <option value="<?php echo $a_id['idInsurancecarrier'] ?>"><?php echo $a_id['name'] ?></option>

                                <?php } ?>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row_item">
                        <span class="li_item"></span>
                        <div class="item_1">
                            <label for="dateIni"><?php echo __('Desde'); ?></label>
                        </div>
                        <div class="item_2">
                            <input name="dateIni" id="dateIni" type="text"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row_item">
                        <span class="li_item"></span>
                        <div class="item_1">
                            <label for="dateEnd"><?php echo __('Hasta'); ?></label>
                        </div>
                        <div class="item_2">
                            <input name="dateEnd" id="dateEnd" type="text"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="row_item">
                        <div class="item_2">
                            <button id="clean" title="<?php echo __('Limpiar'); ?>"><?php echo __('Limpiar'); ?></button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <br>

    <table id="grid-listPayments" ></table>
    <div id="grid-listPayments-paper"></div>

    <div id="divReport">
    </div>    
</div>
