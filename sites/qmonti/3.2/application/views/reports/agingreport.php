<style type="text/css">
    .agin_print img{cursor: pointer;}
    .agin_print{display: block;}
</style>
<script type="text/javascript">
    var sNoPayment = '';
    var noPayment30_jqGrid;
    var noPayment60_jqGrid;
    var noPayment90_jqGrid;
    var noPayment120_jqGrid;
    var noPaymentMore_jqGrid;
    
    $(document).ready(function() {      
        listReportNoPayment = '/reports/agingreport/listReportNoPayment';
        printReportBilling = '/reports/agingreport/printReportBilling';
        printReportAllBilling = '/reports/agingreport/printReportAllBilling';
        
        $( "#tabs" ).tabs();        
        
        noPayment30_jqGrid = $('#noPayment30_jqGrid').jqGrid({
            url: listReportNoPayment,
//            async:true,
            datatype: "json",
           
            height: 'auto',
            postData:{
                
                dateIni: function() {
                    return 0; },
                dateEnd: function() {
                    return 30;}
            },
            rowNum: 50,
            rowList: [50,100,200],  
            pager: 'noPayment30_jqGrid-paper',
                 
            colNames:[
                '<?php echo __('Factura'); ?>',
                '<?php echo __('Aseguradora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Monto'); ?>',
                '<?php echo __('Pagado'); ?>',
                '<?php echo __('Parcial'); ?>',
                '<?php echo __('Imprimir'); ?>'
            ],
            colModel:[
                {name:'idOut',index:'idOut',align: 'center',width: 50},
                {name:'insuranceOut',index:'insuranceOut', width: 200},
                {name:'typeOut',index:'typeOut', align: 'center', width: 100},
                {name:'patientOut',index:'patientOut', width: 150},
                {name:'dateOut', index: 'dateOut',width: 80, align: 'center'},
                {name:'amountOut',index:'amountOut',width: 80},
                {name:'paidOut', index: 'paidOut',width: 50, align: 'center'},
                {name:'paidPartialOut', index: 'paidPartialOut',width: 50, align: 'center', hidden:true},
                {name:'print', index: 'print',width: 50, align: 'center'}
            ],
           
            viewrecords: true,
            sortname: 'dateOut',
            sortorder: "asc",
            
            gridComplete:function(){
               
                var aData = noPayment30_jqGrid.getRowData();
                $.each(aData,function(){
                    btn_print = "<button class=\"btn_print1\" ";                 
                    btn_print += "id_print=\""+this.idOut+"\" ";                    
                    btn_print += "id=\"btn_print1"+this.idOut+"\" ";                    
                    btn_print += "type=\"button\" >";
                    btn_print += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print += "</button>";
                    noPayment30_jqGrid.setRowData(this.idOut,{
                        print: btn_print
                    });
                });   
                $('.btn_print1').button({
                    icons:{
                        primary: 'ui-icon-print'
                    },
                    text: false
                });
                
                $('.btn_print1').click(function(){
                 
                 var data = new Object();
            data.idBilling= $(this).attr("id_print");
            data.type= '30';

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
                });
            }
            ,afterInsertRow: function(rowid, aData){                
                if(Boolean(parseInt(aData.paidPartialOut))){
                    $('#noPayment30_jqGrid tr#'+rowid).addClass('ui-state-highlight');
                } 
            }
        });
        
        noPayment60_jqGrid = $('#noPayment60_jqGrid').jqGrid({
            url: listReportNoPayment,
            datatype: "json",
          
            height: 'auto',
            postData:{
                 dateIni: function() {
                    return 31; },
                dateEnd: function() {
                    return 60;}
            },
            rowNum: 50,
            rowList: [50,100,200],            
                  
            colNames:[
                '<?php echo __('Factura'); ?>',
                '<?php echo __('Aseguradora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Monto'); ?>',
                '<?php echo __('Pagado'); ?>',
                '<?php echo __('Parcial'); ?>',
                '<?php echo __('Imprimir'); ?>'
            ],
            colModel:[
                {name:'idOut',index:'idOut',align: 'center',width: 50},
                {name:'insuranceOut',index:'insuranceOut', width: 200},
                {name:'typeOut',index:'typeOut', align: 'center', width: 100},
                {name:'patientOut',index:'patientOut', width: 150},
                {name:'dateOut', index: 'dateOut',width: 80, align: 'center'},
                {name:'amountOut',index:'amountOut',width: 80},
                {name:'paidOut', index: 'paidOut',width: 50, align: 'center'},
                {name:'paidPartialOut', index: 'paidPartialOut',width: 50, align: 'center', hidden:true},
                {name:'print', index: 'print',width: 50, align: 'center'}
            ], 
            pager: "noPayment60_jqGrid-paper",
            viewrecords: true,
            sortname: 'dateOut',                       
            sortorder: 'asc',         
            gridComplete:function(){                
                var aData = noPayment60_jqGrid.getRowData();
                $.each(aData,function(){
                    btn_print = "<button class=\"btn_print2\" ";                 
                    btn_print += "id_print=\""+this.idOut+"\" ";                    
                    btn_print += "id=\"btn_print2"+this.idOut+"\" ";                    
                    btn_print += "type=\"button\" >";
                    btn_print += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print += "</button>";
                    noPayment60_jqGrid.setRowData(this.idOut,{
                        print: btn_print
                    });
                });   
                $('.btn_print2').button({
                    icons:{
                        primary: 'ui-icon-print'
                    },
                    text: false
                });
                
                $('.btn_print2').click(function(){
                      var data = new Object();
            data.idBilling= $(this).attr("id_print");
            data.type= '60';

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
                    
                     });
            }
            ,afterInsertRow: function(rowid, aData){                
                if(Boolean(parseInt(aData.paidPartialOut))){
                    $('#noPayment60_jqGrid tr#'+rowid).addClass('ui-state-highlight');
                } 
            }
        });
        
        noPayment90_jqGrid = $('#noPayment90_jqGrid').jqGrid({
            url: listReportNoPayment,
            datatype: "json",
//            mtype: 'POST',
            height: 'auto',
            postData:{
                
                  dateIni: function() {
                    return 61; },
                dateEnd: function() {
                    return 90;}
            },
            rowNum: 50,
            rowList: [50,100,200],            
            shrinkToFit: true,            
            colNames:[
                '<?php echo __('Factura'); ?>',
                '<?php echo __('Aseguradora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Monto'); ?>',
                '<?php echo __('Pagado'); ?>',
                '<?php echo __('Parcial'); ?>',
                '<?php echo __('Imprimir'); ?>'
            ],
            colModel:[
                {name:'idOut',index:'idOut',align: 'center',width: 50},
                {name:'insuranceOut',index:'insuranceOut', width: 200},
                {name:'typeOut',index:'typeOut', align: 'center', width: 100},
                {name:'patientOut',index:'patientOut', width: 150},
                {name:'dateOut', index: 'dateOut',width: 80, align: 'center'},
                {name:'amountOut',index:'amountOut',width: 80},
                {name:'paidOut', index: 'paidOut',width: 50, align: 'center'},
                {name:'paidPartialOut', index: 'paidPartialOut',width: 50, align: 'center', hidden:true},
                {name:'print', index: 'print',width: 50, align: 'center'}
            ],
            pager: "#noPayment90_jqGrid",
            viewrecords: true,
            sortname: 'dateOut',                       
            sortable: true,            
            gridComplete:function(){                
                var aData = noPayment90_jqGrid.getRowData();
                $.each(aData,function(){
                    btn_print = "<button class=\"btn_print3\" ";                 
                    btn_print += "id_print=\""+this.idOut+"\" ";                    
                    btn_print += "id=\"btn_print3"+this.idOut+"\" ";                    
                    btn_print += "type=\"button\" >";
                    btn_print += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print += "</button>";
                    noPayment90_jqGrid.setRowData(this.idOut,{
                        print: btn_print
                    });
                });   
                $('.btn_print3').button({
                    icons:{
                        primary: 'ui-icon-print'
                    },
                    text: false
                });
                
                $('.btn_print3').click(function(){
                    
                  var data = new Object();
            data.idBilling= $(this).attr("id_print");
            data.type= '90';

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                       
                }); 
            }
            ,afterInsertRow: function(rowid, aData){                
                if(Boolean(parseInt(aData.paidPartialOut))){
                    $('#noPayment90_jqGrid tr#'+rowid).addClass('ui-state-highlight');
                } 
            }
        });
        
        noPayment120_jqGrid = $('#noPayment120_jqGrid').jqGrid({
            url: listReportNoPayment,
            datatype: "json",
          
            height: 'auto',
            postData:{
                
                  dateIni: function() {
                    return 91; },
                dateEnd: function() {
                    return 120;}
            },
            rowNum: 50,
            rowList: [50,100,200],            
            shrinkToFit: true,            
            colNames:[
                '<?php echo __('Factura'); ?>',
                '<?php echo __('Aseguradora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Monto'); ?>',
                '<?php echo __('Pagado'); ?>',
                '<?php echo __('Parcial'); ?>',
                '<?php echo __('Imprimir'); ?>'
            ],
            colModel:[
                {name:'idOut',index:'idOut',align: 'center',width: 50},
                {name:'insuranceOut',index:'insuranceOut', width: 200},
                {name:'typeOut',index:'typeOut', align: 'center', width: 100},
                {name:'patientOut',index:'patientOut', width: 150},
                {name:'dateOut', index: 'dateOut',width: 80, align: 'center'},
                {name:'amountOut',index:'amountOut',width: 80},
                {name:'paidOut', index: 'paidOut',width: 50, align: 'center'},
                {name:'paidPartialOut', index: 'paidPartialOut',width: 50, align: 'center', hidden:true},
                {name:'print', index: 'print',width: 50, align: 'center'}
            ],
            pager: "#noPayment120_jqGrid",
            viewrecords: true,
            sortname: 'dateOut',                       
            sortable: true,         
            gridComplete:function(){                
                var aData = noPayment120_jqGrid.getRowData();
                $.each(aData,function(){
                    btn_print = "<button class=\"btn_print4\" ";                 
                    btn_print += "id_print=\""+this.idOut+"\" ";                    
                    btn_print += "id=\"btn_print4"+this.idOut+"\" ";                    
                    btn_print += "type=\"button\" >";
                    btn_print += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print += "</button>";
                    noPayment120_jqGrid.setRowData(this.idOut,{
                        print: btn_print
                    });
                });   
                $('.btn_print4').button({
                    icons:{
                        primary: 'ui-icon-print'
                    },
                    text: false
                });
                
                $('.btn_print4').click(function(){
                   
                    var data = new Object();
            data.idBilling= $(this).attr("id_print");
            data.type= '120';

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                     });   
            }
            ,afterInsertRow: function(rowid, aData){                
                if(Boolean(parseInt(aData.paidPartialOut))){
                    $('#noPayment120_jqGrid tr#'+rowid).addClass('ui-state-highlight');
                } 
            }
        });
        
        noPaymentMore_jqGrid = $('#noPaymentMore_jqGrid').jqGrid({
            url: listReportNoPayment,
            datatype: "json",
//            mtype: 'POST',
            height: 'auto',
            postData:{
                 
                dateEnd: function() {
                    return 121;}
            },
            rowNum: 50,
            rowList: [50,100,200],            
            shrinkToFit: true,            
            colNames:[
                '<?php echo __('Factura'); ?>',
                '<?php echo __('Aseguradora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Monto'); ?>',
                '<?php echo __('Pagado'); ?>',
                '<?php echo __('Parcial'); ?>',
                '<?php echo __('Imprimir'); ?>'
                
            ],
            colModel:[
                {name:'idOut',index:'idOut',align: 'center',width: 50},
                {name:'insuranceOut',index:'insuranceOut', width: 200},
                {name:'typeOut',index:'typeOut', align: 'center', width: 100},
                {name:'patientOut',index:'patientOut', width: 150},
                {name:'dateOut', index: 'dateOut',width: 80, align: 'center'},
                {name:'amountOut',index:'amountOut',width: 80},
                {name:'paidOut', index: 'paidOut',width: 50, align: 'center'},
                {name:'paidPartialOut', index: 'paidPartialOut',width: 50, align: 'center', hidden:true},
                {name:'print', index: 'print',width: 50, align: 'center'}
                
            ],
            pager: "#noPaymentMore_jqGrid",
            viewrecords: true,
            sortname: 'dateOut',                       
            sortable: true,          
            gridComplete:function(){                
                var aData = noPaymentMore_jqGrid.getRowData();
                 
                $.each(aData,function(){
                    
                    btn_print = "<button class=\"btn_print5\" ";                 
                    btn_print += "id_print=\""+this.idOut+"\" ";                    
                    btn_print += "id=\"btn_print5"+this.idOut+"\" ";                    
                    btn_print += "type=\"button\" >";
                    btn_print += "<?php echo __('Imprimir Factura'); ?>";
                    btn_print += "</button>";
                    noPaymentMore_jqGrid.setRowData(this.idOut,{
                        print: btn_print
                    });
                });   
                $('.btn_print5').button({
                    icons:{
                        primary: 'ui-icon-print'
                    },
                    text: false
                });
                
                $('.btn_print5').click(function(){
                    
                    var data = new Object();
            data.idBilling= $(this).attr("id_print");
            data.type= 'more';

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                     });  
            }
            ,afterInsertRow: function(rowid, aData){                
                if(Boolean(parseInt(aData.paidPartialOut))){
                    $('#noPaymentMore_jqGrid tr#'+rowid).addClass('ui-state-highlight');
                } 
            }
        });
        
           $('a.agin_print').click(function(){
              var data = new Object();
            data.type= $(this).attr("past_due");
            

            $('#divReportIcon').iReportingPlus('option', 'dynamicParams', data);
            $('#divReportIcon').iReportingPlus('generateReport', 'pdf');
              
           });
        
        
        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_BILLING_AGINREPORT',
            htmlVisor: false,
            jqUI: true,
            urlData: printReportBilling,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...'
//            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
//            }

        });
        
        $('#divReportIcon').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_AGIN_REPORT',
            htmlVisor: false,
            jqUI: true,
            urlData: printReportAllBilling,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: 'Generando su Reporte...'
//            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
//            }

        });
        
        
    });
        
</script>

<div id="show_content" class="show_content">
  <center>
<div id="tabs" style="width: 1000px;">
    <ul>
        <li><a href="#days30">30 <?php echo __('Días'); ?> </a></li>
        <li><a href="#days60">60 <?php echo __('Días'); ?> </a></li>
        <li><a href="#days90">90 <?php echo __('Días'); ?> </a></li>
        <li><a href="#days120">120 <?php echo __('Días'); ?> </a></li>
        <li><a href="#daysMore"><?php echo __('Más de'); ?> 120 <?php echo __('Días'); ?> </a></li>
    </ul>
    <div id="days30">
        <center>
            <a class="agin_print" title="Print" past_due="30" jq_grid="noPayment30_jqGrid"><img alt="<?php echo __('Imprimir'); ?>" src="/media/images/icon_print.png" /></a>
            <table id="noPayment30_jqGrid"></table>
            <div id="noPayment30_jqGrid-paper"></div>
        </center>
    </div>
    <div id="days60">
        <center>
            <a class="agin_print" title="<?php echo __('Imprimir'); ?>" past_due="60" jq_grid="noPayment60_jqGrid"><img alt="<?php echo __('Imprimir'); ?>" src="/media/images/icon_print.png" /></a>
            <table id="noPayment60_jqGrid"></table>
            <div id="noPayment60_jqGrid-paper"></div>
        </center>
    </div>
    <div id="days90">
        <center>
            <a class="agin_print" title="<?php echo __('Imprimir'); ?>" past_due="90" jq_grid="noPayment90_jqGrid"><img alt="<?php echo __('Imprimir'); ?>" src="/media/images/icon_print.png" /></a>
            <table id="noPayment90_jqGrid"></table>
            <div id="noPayment90_jqGrid-paper"></div>
        </center>
    </div>
    <div id="days120">
        <center>
            <a class="agin_print" title="<?php echo __('Imprimir'); ?>" past_due="120" jq_grid="noPayment120_jqGrid"><img alt="<?php echo __('Imprimir'); ?>" src="/media/images/icon_print.png" /></a>
            <table id="noPayment120_jqGrid"></table>
            <div id="noPayment120_jqGrid-paper"></div>
        </center>
    </div>
    <div id="daysMore">
        <center>
            <a class="agin_print" title="<?php echo __('Imprimir'); ?>" past_due="121" jq_grid="noPaymentMore_jqGrid"><img alt="<?php echo __('Imprimir'); ?>" src="/media/images/icon_print.png" /></a>
            <table id="noPaymentMore_jqGrid"></table>
            <div id="noPaymentMore_jqGrid-paper"></div>
        </center>
    </div>
</div>
   </center>
 <div id="divReport">   
 </div>    
 <div id="divReportIcon">   
 </div>    
</div>