<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script>
    
    var reportTotalAppRecorded="/reports/totalappslastweek/reportTotalAppRecorded";
    var reportTotalAppDate="/reports/totalappslastweek/reportTotalAppDate";
    
    $(document).ready(function()
    {
        $("#dateIni,#dateEnd").datepicker({
            dateFormat: "mm/dd/yy",
            beforeShow: dateRange
        });
        
        function dateRange(input) {
            if(input.id == "dateIni"){
                return {
                    maxDate: ($("#dateEnd").datepicker("getDate") == null ? null: $("#dateEnd").datepicker("getDate"))
                }
            }else{
                return {
                    minDate: ($("#dateIni").datepicker("getDate") == null ? null: $("#dateIni").datepicker("getDate"))
                }
            }
        }
        
        $('#refreshGrafCount').button({
            icons: {
                primary: 'ui-icon-refresh'
            }
        });
        
        var baseUrl = '/index.php/admin/reports/lastWeekApp?ajax=ajax';
        
        $('#refreshGrafCount').click(function(){
            $('#ie_chart').find('embed').get(0).reload(baseUrl+'&dateIni='+$("#dateIni").val()+'&dateEnd='+$("#dateEnd").val());
            $('#ie_chart_2').find('embed').get(0).reload('/index.php/admin/reports/lastWeekAppDate?ajax=ajax'+'&dateIni='+$("#dateIni").val()+'&dateEnd='+$("#dateEnd").val());
         
    });
       $('#viewSummary').button({ });
       $('#viewSummary').click(function(){

           

            window.open(url,'Interpreter','width=1000,height=700,resizable=yes,location=no,status=yes');
        });
    });
</script>
<style type="text/css">

    .divGrafico{margin: 0 auto;width: 1000px;}
    .divGrafCenter{margin: 0 auto;width: 800px;}
    .divGrafCenter h2{text-align: center;}

</style> 
<div id="show_content" class="show_content">
    
    
    <label class="short_label" for="dateIni"><?php echo __('Desde'); ?></label>
    <input type="text" name="dateIni" id="dateIni" class=""/>
    <label class="short_label"><?php echo __('Hasta'); ?></label>
    <input for="dateEnd" type="text" name="dateEnd" id="dateEnd" class=""/>
    <button id="refreshGrafCount"><?php echo __('Refrescar'); ?></button>
   
    
    <br><br>

    <div class="divGrafico ui-widget ui-widget-content ui-corner-all">    
        <?php
        include_once ('openFlash/open_flash_chart_object.php');
        echo open_flash_chart_object_str(1000, 400, "/reports/totalappslastweek/reportTotalAppRecorded", false, '/swf/openFlash/');        
        ?>
    </div>
    <div class="divGrafCenter">
        <br>
        
    <button id="viewSummary"><?php echo __('Ver Resumen'); ?></button>
    </div>
    <div class="divGrafico ui-widget ui-widget-content ui-corner-all">    
        <?php       
        echo open_flash_chart_object_str(1000, 400, "/reports/totalappslastweek/reportTotalAppDate", false, '/swf/openFlash/');        
        ?>
    </div>
</div>