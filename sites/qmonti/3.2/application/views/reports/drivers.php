<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script type="text/javascript">

   $(document).ready(function() {
   var listReportDriver="/reports/drivers/listReportDriver";
    var reportDriver="/reports/drivers/reportDriver";     
       
          gridlistReportDriver = $("#grid-listReportDriver").jqGrid({
            url: listReportDriver,
            datatype: "json",
          
            postData: {
              
                dateIni: function() {
                    return $("#dateIni").val();
                },
                dateEnd: function() {
                    return $("#dateEnd").val();
                }
            },
            colNames: ['id','letter', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Área'); ?>', 
                       '<?php echo __('Dirección'); ?>', 
                       '<?php echo __('Teléfono'); ?>', 
                       '<?php echo __('Fax'); ?>', 
                       '<?php echo __('Celular'); ?>', 
                       '<?php echo __('E-mail'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>'],
            colModel: [
                {name: 'idDriver', index: 'idDriver', width: 70, hidden:true},
                {name: 'letter', index: 'letter', width: 70},
                {name: 'name', index: 'name', search: true, width: 100},
                {name: 'area', index: 'area', search: true, width: 150},
                {name: 'address', index: 'address', search: true, width: 250},
                {name: 'phone', index: 'phone', search: true, width: 70},
                {name: 'fax', index: 'fax', search: true, width: 70},
                {name: 'cellphone', index: 'cellphone', search: false, width: 100},
                {name: 'email', index: 'email', search: false, width: 150},
                {name: 'note', index: 'note', search: false, width: 150},
                {name: 'datereg', index: 'datereg', search: false, width: 100}
                
            ],
            grouping: true,
            groupingView: {
                groupField: ['letter'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'],
                groupCollapse: false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 500,
            pager: 'grid-listReportDriver-paper',
            sortname: 'idDriver',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('Conductores'); ?>",
            gridComplete: function() {

               
            }
        });
        $("#grid-listReportDriver").jqGrid('navGrid', '#grid-listReportDriver-paper',
                            {edit: false, add: false, del: false},
                    {},
                            {},
                            {},
                            {multipleSearch: true, multipleGroup: true}
                    );
        
        $("#clean").button({
              icons:{primary: 'ui-icon-refresh'}
          }).click(function(){
             
              $("#dateIni,#dateEnd").val('');
              $("#grid-listReportDriver").trigger('reloadGrid');
          });
        $("#print").button({
              icons:{primary: 'ui-icon-print'}
          }).click(function(){
             
              var data = new Object();
            data.dateIni=$('#dateIni').val();
            data.dateEnd= $('#dateEnd').val();

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
          });
          
          $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($('#dateIni').val()!='' && $('#dateEnd').val()!=''){
                    $("#grid-listReportDriver").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
        
                if($( "#dateIni" ).val()!='') $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateEnd" ).val()!='') $( "#dateIni" ).datepicker( "option", "maxDate", selectedDate );
            } 
        });   
        
        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_REPORT_DRIVERS',
            htmlVisor: false,
            jqUI: true,
            urlData: reportDriver,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...'
//            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
//            }

        });
   });
</script>    
<div id="show_content" class="show_content">
   
    <div class="divCont">
    <label class="frmlbl"><?php echo __('Filtro por Rango de Fechas'); ?></label>
 <div class="clear"></div>
      <label class="short_label" for="dateFrom"><?php echo __('Desde'); ?></label>
      <input name="dateIni" id="dateIni" type="text"/>
      <label class="short_label"  for="dateTo" ><?php echo __('Hasta'); ?></label>
      <input name="dateEnd" id="dateEnd" type="text"/>
        <button class="clean" id="clean" name="clean" title="Clean"><?php echo __('Limpiar'); ?></button>
        <button class="print" id="print" title="<?php echo __('Imprimir'); ?>"><?php echo __('Imprimir'); ?></button>
</div>
    <br>
    <center>
       <table id="grid-listReportDriver" ></table>
    <div id="grid-listReportDriver-paper"></div>
    </center>
    <div id="divReport">
    </div>    
</div>
