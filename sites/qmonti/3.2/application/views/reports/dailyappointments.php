<script type="text/javascript">
    
    var listDailyApp='/reports/dailyappointments/listDailyApp';
    
    $(document).ready(function() {    
        
        gridlistDailyApp = $('#grid-listDailyApp').jqGrid({
            url: listDailyApp,
            datatype: "json",
            height: 'auto',
            postData:{
                
                date_filter: function() {
                    return $('#filter_date').val(); },
                id_user: function() {
                    return $("#user_id").val();}
            },
            rowNum: 50,
            rowList: [50,100,200],  
            pager: 'grid-listDailyApp-paper',
           
            colNames:[
                   
                '<?php echo __('ID'); ?>',
                '<?php echo __('Fecha'); ?>',
                '<?php echo __('Hora'); ?>',
                '<?php echo __('Tipo'); ?>',
                '<?php echo __('Paciente'); ?>',
                '<?php echo __('Conductor'); ?>',
                '<?php echo __('Intérprete'); ?>',
                '<?php echo __('Usuario'); ?>'
            ],
            colModel:[
         
                {name:'idApp',index:'idApp', width: 150},
                {name:'dateApp',index:'dateApp', align: 'center', width: 100},
                {name:'timeApp',index:'timeApp', width: 100, align: 'center'},
                {name:'typeApp', index: 'typeApp',width: 150, align: 'center'},
                {name:'patient',index:'patient',width: 150},
                {name:'driver', index: 'driver',width: 150},
                {name:'interpreter', index: 'interpreter',width: 150},
                {name:'iduser', index: 'iduser',width: 150, align: 'center',hidden:true},
                
            ],
            grouping: true,
            groupingView: {
                groupField: ['iduser'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'],
                groupCollapse: false
            },
            viewrecords: true,
            sortname: 'idApp',
            sortorder: "asc",
            
            gridComplete:function(){
               
//                var aData = gridlistDailyApp.getRowData();
//                $.each(aData,function(){
//                    btn_print = "<button class=\"btn_print1\" ";                 
//                    btn_print += "id_print=\""+this.idOut+"\" ";                    
//                    btn_print += "id=\"btn_print1"+this.idOut+"\" ";                    
//                    btn_print += "type=\"button\" >";
//                    btn_print += "Print Invoice";
//                    btn_print += "</button>";
//                    gridlistDailyApp.setRowData(this.idOut,{
//                        print: btn_print
//                    });
//                });   
//                $('.btn_print1').button({
//                    icons:{
//                        primary: 'ui-icon-print'
//                    },
//                    text: false
//                });
//                
//                $('.btn_print1').click(function(){
//                 
//                 var data = new Object();
//            data.idBilling= $(this).attr("id_print");
//            data.type= '30';
//
//            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
//            $('#divReport').iReportingPlus('generateReport', 'pdf');
//                    
//                });
            }
            
        });
        gridlistDailyApp.setGroupHeaders({
            useColSpanStyle: true
            ,groupHeaders:[
                {startColumnName: 'idApp', numberOfColumns: 4, titleText: '<i><?php echo __('Cita'); ?></i>'}                
            ]
        });
        
        $("#user_id").combobox({
              selected:function() { $("#grid-listDailyApp").trigger('reloadGrid');}
          });
          
          $("#clean").button({
              icon:{primary: 'ui-icon-refresh'}
          }).click(function(){
              $("#user_id-combobox").val('');
              $("#filter_date").val('');
              $("#grid-listDailyApp").trigger('reloadGrid');
          });
          
          $("#filter_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                
                    $("#grid-listDailyApp").trigger("reloadGrid");
                
            }
           
        });
    });
</script>
<div id="show_content" class="show_content">
    
<div class="float_right cont_filter">
    <div class="block1_theme content_filter ui-corner-all">
        <div class="padding10">
            <div>
                <div class="title_theme ui-corner-all">
                    <h2><?php echo __('FILTRO'); ?></h2>                
                </div>
                <div class="row_item">
                    <span class="li_item"></span>
                    <div class="item_1">
                        <label><?php echo __('Usuario'); ?></label>
                    </div>
                    <div class="item_2">
                        <select name="user_id" id="user_id">
                            <option value=""><?php echo __('Seleccionar'); ?></option>                            
                           <?php foreach ($users as $a_id) { ?>
                            <option value="<?php echo $a_id['idUser'] ?>"><?php echo $a_id['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row_item">
                    <span class="li_item"></span>
                    <div class="item_1">
                        <label for="filter_date"><?php echo __('Desde'); ?></label>
                    </div>
                    <div class="item_2">
                        <input name="filter_date" id="filter_date" type="text"/>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row_item">
                    <div class="item_2">
                        <button id="clean" title="<?php echo __('Limpiar'); ?>"><?php echo __('Limpiar'); ?></button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>    
    
    <center>
       
         <table id="grid-listDailyApp"></table>
         <div id="grid-listDailyApp-paper"></div>
    </center>
    
    
</div>