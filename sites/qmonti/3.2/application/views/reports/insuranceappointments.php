<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script type="text/javascript">
    
    var reportInsuranceApps="/reports/insuranceappointments/reportInsuranceApps";
    
   $(document).ready(function() {
       
        $("#idInsuranceCarriers").combobox();
       
           $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy"
        });
       
       $('#printInsurances').button({
           icons:{
                primary: 'ui-icon-print'
            }
       }).click(function(){
           
            var data = new Object();
            data.insuranceCarrier= $("#idInsuranceCarriers").val();
            data.dateIni=$("#dateIni").val();
            data.dateEnd=$("#dateEnd").val();

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
              
       });
       
       $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_REPORT_INSURANCECARRIER_APP',
            htmlVisor: false,
            jqUI: true,
            urlData: reportInsuranceApps,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...'
        });
   });
   
</script>  
<div id="show_content" class='show_content'>
<div class="divCont">
  <label class="frmlbl"><?php echo __('Filtro por Rango de Fechas'); ?></label>
            <div class="clear"></div>
       <label class="short_label"><?php echo __('Desde'); ?></label>
<input name="dateIni" id="dateIni" type="text"/>
<label class="short_label"><?php echo __('Hasta'); ?></label>
<input name="dateEnd" id="dateEnd" type="text"/>
<label class="short_label"><?php echo __('Compañía de Seguros'); ?></label>

                    <select name="idInsuranceCarriers" id="idInsuranceCarriers">  
                        <option value=""></option>
                       <?php foreach ($insurance as $a_id) { ?>
                            <option value="<?php echo $a_id['idInsurancecarrier'] ?>"><?php echo $a_id['name'] ?></option>

                        <?php } ?>
                    </select>                    

                    <a href="#" id="printInsurances">
                      <?php echo __('Imprimir'); ?>
                    </a>
               
            <div class="clear"></div>
        
</div>
<div id="divReport">        
</div>
</div>
    