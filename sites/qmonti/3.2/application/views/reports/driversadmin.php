<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script type="text/javascript">

   $(document).ready(function() {
   var listReportAdminDriver="/reports/driversadmin/listReportAdminDriver";
    var reportAdminDriver="/reports/driversadmin/reportAdminDriver";     
       
          gridlistReportAdminDriver = $("#grid-listReportAdminDriver").jqGrid({
            url: listReportAdminDriver,
            datatype: "json",
          
            postData: {
              
                dateIni: function() {
                    return $("#dateIni").val();
                },
                dateEnd: function() {
                    return $("#dateEnd").val();
                }
            },
            colNames: ['id','letter', 
                       '<?php echo __('Nombre'); ?>', 
                       '<?php echo __('Área'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       'Ambulatory', 
                       'Wheelchair', 
                       'Stretcher', 
                       'LiftWH', 
                       'LiftST', 
                       'MRT Miles', 
                       'MRT Rate', 
                       'NS Rate', 
                       'Wait Time'],
            colModel: [
                {name: 'idDriver', index: 'idDriver', width: 10, hidden:true},
                {name: 'letter', index: 'letter', width: 70},
                {name: 'name', index: 'name', search: true, width: 150},
                {name: 'area', index: 'area', search: true, width: 150},
                {name: 'date', index: 'date', search: true, width: 100, align:'center'},
                {name: 'amb', index: 'amb', search: true, width: 100, align:'right'},
                {name: 'wh', index: 'wh', search: true, width: 100, align:'right'},
                {name: 'st', index: 'st', search: false, width: 100, align:'right'},
                {name: 'littwh', index: 'littwh', search: false, width: 70, align:'right'},
                {name: 'littst', index: 'littst', search: false, width: 70, align:'right'},
                {name: 'mrtmiles', index: 'mrtmiles', search: false, width: 70, align:'right'},
                {name: 'mrtrate', index: 'mrtrate', search: false, width: 70, align:'right'},
                {name: 'nsrate', index: 'nsrate', search: false, width: 70, align:'right'},
                {name: 'waittime', index: 'waittime', search: false, width: 70, align:'right'}
                
            ],
            grouping: true,
            groupingView: {
                groupField: ['letter'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'],
                groupCollapse: false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 500,
            width: 1200,
            pager: 'grid-listReportAdminDriver-paper',
            sortname: 'idDriver',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('Admin de Conductores'); ?>",
            gridComplete: function() {

               
            }
        });
        $("#grid-listReportAdminDriver").jqGrid('navGrid', '#grid-listReportAdminDriver-paper',
                            {edit: false, add: false, del: false},
                    {},
                            {},
                            {},
                            {multipleSearch: true, multipleGroup: true}
                    );
        
        $("#clean").button({
              icons:{primary: 'ui-icon-refresh'}
          }).click(function(){
             
              $("#dateIni,#dateEnd").val('');
              $("#grid-listReportAdminDriver").trigger('reloadGrid');
          });
        $("#print").button({
              icons:{primary: 'ui-icon-print'}
          }).click(function(){
             
              var data = new Object();
            data.dateIni=$('#dateIni').val();
            data.dateEnd= $('#dateEnd').val();

            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
                    
          });
          
          $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($('#dateIni').val()!='' && $('#dateEnd').val()!=''){
                    $("#grid-listReportAdminDriver").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
        
                if($( "#dateIni" ).val()!='') $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateEnd" ).val()!='') $( "#dateIni" ).datepicker( "option", "maxDate", selectedDate );
            } 
        });   
        
        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_REPORT_DRIVER_ADMIN',
            htmlVisor: false,
            jqUI: true,
            urlData: reportAdminDriver,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...'
//            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
//            }

        });
   });
</script>    
<div id="show_content" class="show_content">
   
    <div class="divCont">
    <label class="frmlbl"><?php echo __('Filtro por Rango de Fechas'); ?></label>
 <div class="clear"></div>
      <label class="short_label" for="dateFrom"><?php echo __('Desde'); ?></label>
      <input name="dateIni" id="dateIni" type="text"/>
      <label class="short_label"  for="dateTo" ><?php echo __('Hasta'); ?></label>
      <input name="dateEnd" id="dateEnd" type="text"/>
        <button id="clean" title="<?php echo __('Limpiar'); ?>"><?php echo __('Limpiar'); ?></button>
        <button id="print" title="<?php echo __('Imprimir'); ?>"><?php echo __('Imprimir'); ?></button>
</div>
    <br>
    <center>
       <table id="grid-listReportAdminDriver" ></table>
    <div id="grid-listReportAdminDriver-paper"></div>
    </center>
    <div id="divReport">
    </div>    
</div>
