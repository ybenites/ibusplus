<style type="text/css"> 
    span.cover_option{width: 100%;height: 100%;position: absolute;z-index: 99;top: 0;left: 0;}
    #summary_f_amount_total, #summary_label_total{float: right;padding: 5px; width: 150px;font-size: 20px;text-align: right;height: 20px;line-height: 20px;}
    td::-moz-focus-inner,span::-moz-focus-inner {padding: 0;border: 0}
    #process_payment{float: left;margin-top: 4px;margin-left: 0;}
    #commentServ{
        width: 325px;
        height: 100px;
        resize: none;
    }
    .ui-dialog{box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);}

    div[aria-labelledby=ui-dialog-title-dialog_pre_view] .ui-dialog-titlebar{display: none;}
    div[aria-labelledby=ui-dialog-title-dialog_pre_view] .ui-dialog-titlebar-close{display: none;}    
    td[aria-describedby=table_interpreters_option_out]{position: relative;}

    .ui-dialog-content{}
    ul.option_list_pre_billing{position: relative;}

    .billing_options_left dl dt,.billing_options_left dl dd{float: left;}
    .noShowLabel{position: absolute;right: 0;top: 0;margin-right: 0;padding: 3px;}
    .no_show_label{padding: 3px;margin-bottom: 5px;font-weight: bold;text-align: center;}
    .ui-widget-content span.highlight_app,.ui-widget-content span.highlight_app a{color: #915608;font-weight: bold;font-size: 15px;line-height: 15px;}
    h4 span{color: #6EAC2C;font-size: 17px;}
    hr.hr_separate{ border: none; border-top: 4px #c5c5c5 dashed;opacity: 0.55;}
    .ui-jqgrid tr.jqgrow td {white-space: normal;}   
    #interpreter_id-combobox{margin: 0;float: left;width: 170px;}
    #interpreter_id-button-combobox{margin: 0;}
    .content_filter{width: 400px;}
    .ui-jqgrid .tree-wrap-ltr{margin-top: 4px;}
    .cont_filter{width: 405px;}

    .div_billing{display: none;width: 1200px;}
    h1,h2,h3,h4{margin: 0;padding: 0;}    
    .menutop h4{line-height: 15px;}
    .menutop{padding: 2px;}
    .menutop2{padding: 5px;}
    .menutop3{width: 527px;float: left}
    .menutop4{width: 560px;float: left}
    .menutop5{width: 559px;float: left}
    .menutop6{width: 955px;float: left}
    .billing_interpreter_number{width: 213px;float: left;line-height: 25px; padding: 0 5px;text-align: center;float: right;}

    .contTransl ul{list-style: none;margin: 0;padding: 0;}
    .contTransl ul li,.contTransl ul li ,.contApp ul li{list-style: none;padding: 5px 0;display: block;}    

    .contTransl ul li dt label.interpreter{width: 110px;margin-right: 5px;}
    .contTransl ul li dd label.interpreterName{width: 445px;font-weight: normal;}
    .contTransl ul li dd label.wthour,.contTransl ul li dd label.wtmin,.contTransl ul li dd label.time{font-weight: normal;padding: 5px 0;margin-right: 5px;}
    .contTransl ul li dd input{margin-right: 5px;}

    .contTransl ul li dt label.tservicest{width: 160px;margin-right: 5px;}
    .contTransl ul li dd label.tservicestName{width: 395px;font-weight: normal;}
    div.middle {
        float: left;
        width: 258px;
        margin-right: 10px
    }
    dd{line-height: 16px;}
    .custom_icon{float: left;margin-right: 3px;}
    .contApp {
        padding: 7px;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .contTransl {
        padding: 7px;
        margin-top: 5px;
    }

    .divTransl {
        float: left;
        width: 580px;
    }
    .bCommentServInt, a.editApps{margin: 0;float: right;}
    .editApps {display: none;}
    .none{display: none;}
    .cont_form{}
    .cont_app{width: 610px;float: left;margin-right: 10px;}
    .cont_app ul li dl dd,.contTransl ul li dl dd{padding-left: 5px;}
    .cont_app ul li dl dt,.contTransl ul li dl dt{padding-bottom: 5px;}
    .info_billing{padding: 7px;}
    .info_billing_wrapper{margin-top: 5px;margin-bottom: 5px;}
    #btn_back_billing{margin-bottom: 5px;}
    .cont_form form{float: left;}
    .wrapeer_item{position: relative;}

    .item_num_value{float: left;width: 45px;height: 45px;text-align: center;line-height: 32px;font-size: 30px;margin-right: 10px;}
    .item_num_value span{font-size: 12px;float: left;width: 100%;height: 12px;line-height: 12px;}

    .contTransl ul li dl.checkbox dd{margin: 0;float: left;padding: 0;}
    .contTransl ul li dl dd input{margin: 0;}
    .bis_label{float: right;width: 115px;font-size: 20px;line-height: 35px;color: #777;}
    .bis_amount{float: right;width: 125px;font-size: 35px;line-height: 35px;padding-right: 10px;text-align: right;color: #777;}
    .billing_item_summary{border-top: 5px #777 dashed;margin-top: 10px;padding: 10px;}
    .ul_items_pre_billing{list-style: none;}
    .ul_items_pre_billing li.item_header_pre_view{border-top: 2px dashed black;}
    .ul_items_pre_billing li:nth-child(2){border-top: none;}
    .ul_items_pre_billing li dl,.ul_items_pre_billing li dl dd{float: left;padding: 5px;}

    li.pre_billing_header dd{text-align: center;}
    dd.pre_billing_date{width: 80px;}
    dd.pre_billing_description{width: 317px;}
    dd.pre_billing_quantity{width: 65px;}
    dd.pre_billing_ratio{width: 65px;}
    dd.pre_billing_amount{width: 65px;}

    dd.dd_type_row{margin-left: 90px;}

    dd.dd_item_date{width: 80px;text-align: center;}
    dd.dd_item_description{width: 319px;}
    dd.dd_item_quantity{width: 65px;text-align: center;}
    dd.dd_item_ratio{width: 65px;text-align: right;}
    dd.dd_item_amount{width: 65px;text-align: right;}

    .wrapper_pre_billing_summary{margin-bottom: 10px;}
    .wrapper_pre_billing_summary div.label_pre_billing_summary,.wrapper_pre_billing_summary div.amount_pre_billing_summary{
        float: right;
        margin-left: 10px;
        padding: 10px;
        text-align: right;
        width: 120px;
        font-size: 20px;
    }
    .ul_items_pre_billing li.pre_billing_header{border: 1px solid #77D5F7;}

</style>
<script type="text/javascript">
    item_row_call_back_type_header = 'item_header';
    item_row_call_back_type_row = 'item_row';
    item_row_call_back_type_note = 'item_comment';
    item_row_call_back_type_summary = 'item_summary';
    var listIBilling="/billings/apinterpreters/listInterpreterBilling";
    var getAppointmentsToBillingInterpreter="/billings/apinterpreters/getAppointmentsToBilling";
    var getmilestotime="/billings/apinterpreters/milesToTime";
    var calculartiempotrasnc="/billings/apinterpreters/calculartiempotrasnc";
    var saveBillingInterpreter="/billings/apinterpreters/saveBillingInterpreter";
     var getNewCodeInvoice = "/billings/apinterpreters/getNewCodeInvoice";
    $(document).ready(function() {
        $.mask.rules = $.extend($.mask.rules, {'p':/[ap]/});        
        $.mask.rules = $.extend($.mask.rules, {'m':/[m]/});        
        $.mask.masks.clock_time = {mask: '19:59 pm'};
        $.mask.masks.num_integer = {mask:'9', type:'repeat', 'maxLength': 7};
        $.mask.masks.num_float = {mask:'99.9999', type:'reverse', 'maxLength': 7};
        
        $('#div_billing').hide();

        $("#btn_clean").button();
        
        $('.savComment').button({
            icons:{
                primary: 'ui-icon-disk'
            }
        });
        
        
        $('#process_payment').button({
            icons:{
                primary: 'ui-icon-check'
            }
        });
        
        $("#btn_clean").click(function() {

            $('#idInterpreter').val([]);
            $('#idInterpreter-combobox').val('');
            $("#grid-listInterpretersBilling").trigger("reloadGrid");

        });

        $('#btn_back_billing').button({
            icons: {
                primary: 'ui-icon-arrowreturnthick-1-w'
            }
        });
        $("#btn_back_billing").click(function() {

            $('.div_billing').hide();
            $('#pre_billing').show();

        });

        $('#preview_billing_interpreter').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });
        
        $('#idInterpreter').combobox({
            selected: function() {
                $("#grid-listInterpretersBilling").trigger('reloadGrid');
            }
        });
        
        $("#dialog_pre_view").dialog({
            autoOpen: false
            ,height: 'auto'
            ,width: '700'
            ,modal: true
            ,resizable: false
            ,draggable: false
        });
        
        $('.bCommentServInt').validate({
            submitHandler: function(form) {
                $($('#textareaid').val()).val($('#commentServ').val());
                $('#divComment').dialog('close');
            }
        });
        
        $('.bCommentServInt').live('click', function() {
            var app_id = $(this).data('app_id');
            $('#commentServ').val($('#commentServInt'+app_id).val());
            $('#textareaid').val('#commentServInt'+app_id);
            $('#divComment').dialog('open');
        });
        
        
        $('#preview_billing_interpreter').click(function(){            
            var s_data_forms = '';
            for(i=0;i<a_check_ids.length;i++){
                s_data_forms += '&'+$('#formTransl'+a_check_ids[i]).serialize();                
            }
            s_data_forms = 'a_apps_id='+a_check_ids.join(',')+s_data_forms;

            var s_url_data = "billing_interpreter_date="+$('#billing_interpreter_date').val()+'&interpreter_id='+$("#interpreter_id_hidden").val()+'&'+s_data_forms + '&save=' + $("#save_hidden").val()+"&ajax=ajax";
            $.ajax({
                type: "POST",
                url: saveBillingInterpreter,
                data: s_url_data,
                dataType:'json',
                success: function(response) {     
       
                    if(response.hasOwnProperty('items_row')){                        
                        $html_items = '';
                        $("ul.ul_items_pre_billing li:not(:eq(0))").remove();                        
                        $.each(response.items_row,function(){
                            
                          
                            if(this.row_type == item_row_call_back_type_header){
                                $html_items += '<li class="item_header_pre_view">';
                                $html_items += '    <dl>';
                                $html_items += '        <dd class="dd_item_date">'+this.row_date+'</dd>';
                                $html_items += '        <dd class="dd_item_description">'+this.row_description+'</dd>';
                                $html_items += '        <dd class="dd_item_quantity">'+this.row_quantity+'</dd>';
                                $html_items += '        <dd class="dd_item_ratio">'+this.row_ratio+'</dd>';
                                $html_items += '        <dd class="dd_item_amount"><b>'+this.row_amount+'</b></dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                            }else if(this.row_type == item_row_call_back_type_row){
                                $html_items += '<li>';
                                $html_items += '    <dl>';                                
                                $html_items += '        <dd class="dd_item_description dd_type_row">'+this.row_description+'</dd>';
                                $html_items += '        <dd class="dd_item_quantity">'+this.row_quantity+'</dd>';
                                $html_items += '        <dd class="dd_item_ratio">'+this.row_ratio+'</dd>';
                                $html_items += '        <dd class="dd_item_amount"><b>'+this.row_amount+'</b></dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                            
                            }else if(this.row_type == item_row_call_back_type_note){
                                $html_items += '<li>';
                                $html_items += '    <dl>';
                                $html_items += '        <dd class="dd_item_description dd_type_row">Note: '+this.row_description+'</dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                            
                            }else if(this.row_type == item_row_call_back_type_summary){                                
                                $('.label_pre_billing_summary').html(this.row_description);
                                $('.amount_pre_billing_summary').html(this.row_amount);
                            }
                        });
                    }                    
                    $($html_items).insertAfter("ul.ul_items_pre_billing li:eq(0)");
                    $("#dialog_pre_view").dialog('open');
                }
            });
        });
        
        $('#process_payment').click(function(){
            var s_data_forms = '';
            for(i=0;i<a_check_ids.length;i++){
                s_data_forms += '&'+$('#formTransl'+a_check_ids[i]).serialize();                
            }
            s_data_forms = 'a_apps_id='+a_check_ids.join(',')+s_data_forms;

            var s_url_data = "billing_interpreter_date="+$('#billing_interpreter_date').val()+'&interpreter_id='+$("#interpreter_id_hidden").val()+'&'+s_data_forms + "&save=true&ajax=ajax";
            console.log(s_data_forms);
            $.ajax({
                type: "POST",
                url: saveBillingInterpreter,
                data: s_url_data,
                dataType:'json',
                beforeSend : function(){
                    $("#loading").dialog('open');
                },
                complete : function(){
                        
                    $("#loading").dialog('close');
                },
                success: function(response) {
                    o_return = response;
                   
                        
                    if(o_return.billing_interpreter_id > 0){
                        //                        extraUrl =$.URLEncode('/index.php/admin/billing/fnGetBillingInterpreterPrint?publicKey={/literal}{$publicKey}{literal}&ajax=xml&billing_interpreter_id='+o_return.billing_interpreter_id);
                        //                        debug(extraUrl);
                        //                        url =  "http://reports.mcets-inc.com:8080/ibusplus.com.reportRequest/faces/reportGenerator.jsp?domain=monti.ibusplus.com&xpath=/report/result/row&filetype=pdf&clientfile=MONTI_PRINT_BILLING_INTERPRETER&clientfolder=monti&data="
                        //                            + extraUrl;
                        //                        window.open(url,'Billing Interpreter','width=1000,height=700,resizable=yes,location=no,status=yes');
                        $('.div_billing').slideUp(500, function(){$('.div_pre_billing').slideDown(500)});
                        gridlistInterpretersBilling.trigger('reloadGrid');
                        $("#dialog_pre_view").dialog('close');
                        $('div.div_billing').hide();
                        $('#pre_billing').show();
                    }                    
                }
            });
        });
        
        
        
        
        gridlistInterpretersBilling = $("#grid-listInterpretersBilling").jqGrid({
            url: listIBilling,
            datatype: "json",
            postData: {
                idInterpreter: function() {
                    return $("#idInterpreter").val()
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       'idInterpreter',
                       '<?php echo __('Intérprete'); ?>', 
                       '<?php echo __('Fecha'); ?>', 
                       '<?php echo __('Hora'); ?>', 
                       '<?php echo __('Paciente'); ?>', 
                       '<?php echo __('Opción'); ?>'],
            colModel: [
                {name: 'app_id', index: 'app_id', search: true, width: 150, align: "center"},
                {name: 'interpreter_id', index: 'interpreter_id', hidden: true},
                {name: 'interpreterfullname', index: 'interpreterfullname', hidden: false, align: "center"},
                {name: 'app_date', index: 'app_date', search: true, width: 150, align: "center"},
                {name: 'app_time', index: 'app_time', search: false, width: 150, align: "center"},
                {name: 'pat_full_name', index: 'pat_full_name', search: true, width: 150, align: "center"},
                {name: 'actions', index: 'actions', width: 100, align: "center", sortable: false, search: false}
            ],
            grouping: true,
            groupingView: {
                groupField: ['interpreterfullname'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'
                        + '<div class="titleRight"><a  class="selectbilling"><?php echo __('Facturar'); ?></a></div>'],
                groupCollapse: false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 450,
            pager: 'grid-listInterpretersBilling-paper',
            sortname: 'app_id',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('LISTA DE PAGOS PENDIENTES A INTÉRPRETE'); ?>",
            gridComplete: function() {

                var aData = gridlistInterpretersBilling.getDataIDs();
                $.each(aData, function() {

                    check = "<input class=\"check\" ";
                    check += "data-id=\"" + gridlistInterpretersBilling.getCell(this, 'app_id') + "\" ";
                    check += "data-interpreter_id=\"" + gridlistInterpretersBilling.getCell(this, 'interpreter_id') + "\" ";
                    check += "name=\"check_" + gridlistInterpretersBilling.getCell(this, 'interpreter_id') + "[]\" ";
                    check += "id=\"check_" + this + "\" ";
                    check += "value=\"" + gridlistInterpretersBilling.getCell(this, 'app_id') + "\" ";
                    check += "type=\"checkbox\" />";
                    check += "<label for=\"check_" + this + "\"><?php echo __('Seleccionar'); ?></label>";
                    gridlistInterpretersBilling.setRowData(this
                    , {
                        actions: check
                    });

                });

                $('.check').button({
                    icons: {primary: 'ui-icon-check'},
                    text: false
                })
                $('.selectbilling').button({
                    icons: {primary: 'ui-icon-check'}
                });
            }
        });
        
        
        
        $("#grid-listInterpretersBilling").on('click', 'a.selectbilling', function() {
            a_check_ids = [];
             $.ajax({
                type: "POST",
                url: getNewCodeInvoice,
                dataType: 'json',
                async: false,
                success: function(response) {

                    $("#billing_interpreter_number").text(response.newbilling);
                }

            });
            $("#grid-listInterpretersBilling input:checkbox").each(function() {

                if (this.checked) {
                    a_check_ids.push($(this).val());
                    $("#interpreter_id_hidden").val($(this).attr('data-interpreter_id'));
                    
                }
            });

            if (a_check_ids.length == 0) {
                          msgBox('<?php echo __('Seleccionar una cita para facturar.'); ?>'
                        , '<?php echo __('Alerta'); ?>');
            } else {
                $('#pre_billing').hide();
                $('div.div_billing').show();
                $('div.content_to_billing').html('');

                //$('input:hidden#driver_id_hidden').val(i_driver_id);
                
                //return false;
                $.ajax({
                    url: getAppointmentsToBillingInterpreter,
                    type: "POST",
                    data: {
                        a_check_ids: a_check_ids.toString(),
                        interpreter_id: $("#interpreter_id_hidden").val()},
                    dataType: "json",
                    success: function(j_response) {
                        

                        i_count = 0;
                        
                        $.each(j_response, function() {
                            $('#billing_interpreter_date').val(this.d_current_date);
                            i_count++;
                            
                            var hr = '<div class="clear"></div><hr class="hr_separate"/>';
                            i_billing_interpreter_id = this.app_id;

                            s_no_show_interpreter_html = '';

                            if (this.app_no_show_interpreter == 1) {
                                s_no_show_interpreter_html = '<div class="no_show_label ui-corner-all ui-state-error" id="noShowInterpreter'+this.app_id+'">NO SHOW</div>';
                            }

                            switch (this.pat_region) {
                                case 'FLORIDA':
                                    s_region = 'Florida';
                                    break;
                                case 'GEORGIA':
                                    s_region = 'Georgia';
                                    break;
                                case 'OTHER':
                                    s_region = 'Out state'
                                    break;
                                default:
                                    s_region = '<b class="balert">' + this.pat_region + '</b>';
                                    break;
                            }
                            
                            html_to_app = '';
                            if(this.app_type_service == 'Interpreting'){
                                html_to_app =  '<li>';
                                html_to_app += '    <dl>';
                                html_to_app += '        <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                                html_to_app += '        <dd><span class="custom_icon ui-icon ui-icon-arrowreturnthick-1-e"></span>'+this.tl_contact+': '+this.tl_address+'</dd>';
                                html_to_app += '        <dd class="clear"></dd>';
                                html_to_app += '    </dl>';
                                html_to_app += '    <div class="clear"></div>';
                                html_to_app += '</li>';                                
                            }
                            
                            html_add = '';
                            switch(this.app_type_service){
                                case 'Interpreting':
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Tiempo de Interpretación'); ?>:</label></dt>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Desde'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="interpretation_time_from'+this.app_id+'" id="interpretation_time_from'+this.app_id+'" size="10" class="text_time ui-widget ui-widget-content ui-corner-all text_update" value="" alt="clock_time"></dt>';
                                    html_add += '       <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="interpretation_time_to'+this.app_id+'" id="interpretation_time_to'+this.app_id+'" size="10" class="text_time ui-widget ui-widget-content ui-corner-all text_update" value="" alt="clock_time"></dt>';
                                    html_add += '       <dt><button class="calc_time_int" data-id_app="'+this.app_id+'" type="button"><?php echo __('Tiempo Total'); ?></button><?php echo __('Tiempo Total'); ?>:</dt>';
                                    html_add += '       <dt><input type="text" readonly="" name="interpretation_time_total_time'+this.app_id+'" id="interpretation_time_total_time'+this.app_id+'" size="10" class="ui-widget ui-widget-content ui-corner-all"></dt>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Viaje (Ciudad a Ciudad)'); ?></label></dt>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Desde'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="travel_from'+this.app_id+'" id="travel_from'+this.app_id+'" size="10" class="ui-widget ui-widget-content ui-corner-all text_update" value=""></dt>';
                                    html_add += '       <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="travel_to'+this.app_id+'" id="travel_to'+this.app_id+'" size="10" class="ui-widget ui-widget-content ui-corner-all text_update" value=""></dt>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Millaje Total'); ?></label></dt>';
                                    html_add += '       <dt><input type="text" name="total_mileage'+this.app_id+'" id="total_mileage'+this.app_id+'" size="10" class="text_numeric ui-widget ui-widget-content ui-corner-all text_update" value="" alt="num_float"></dt>';
                                    html_add += '       <dt><button class="calc_travel_time_int " data-id_app="'+this.app_id+'" type="button"><?php echo __('Tiempo Total'); ?></button>Total Time:</dt>';
                                    html_add += '       <dt><input type="text" readonly="" name="travel_total_time'+this.app_id+'" id="travel_total_time'+this.app_id+'" size="10" class="ui-widget ui-widget-content ui-corner-all"></dt>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '   <dl>';
                                    html_add += '       <dd class="checkbox"><input  type="checkbox" name="other'+this.app_id+'" id="other'+this.app_id+'"/><label for="other'+this.app_id+'"><?php echo __('Otro'); ?></label></dd>';
                                    html_add += '       <dd class="none ddOther"><input type="text" size="5" id="otherValue'+this.app_id+'" name="otherValue'+this.app_id+'" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                                    html_add += '       </dd>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '   </dl>';
                                    html_add += '   <div class="clear"></div>';
                                    html_add += '</li>';                                    
                                    break;
                                case 'Conference Call':                                    
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '    <dl>';
                                    html_add += '       <dt><label><?php echo __('Desde'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="interpretation_time_from'+this.app_id+'" id="interpretation_time_from'+this.app_id+'" size="10" class="text_time ui-widget ui-widget-content ui-corner-all text_update" value="" alt="clock_time"></dt>';
                                    html_add += '       <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                                    html_add += '       <dt><input type="text" name="interpretation_time_to'+this.app_id+'" id="interpretation_time_to'+this.app_id+'" size="10" class="text_time ui-widget ui-widget-content ui-corner-all text_update" value="" alt="clock_time"></dt>';
                                    html_add += '       <dt><button class="calc_time_int" data-id_app="'+this.app_id+'" type="button"><?php echo __('Tiempo Total'); ?></button>Total Time:</dt>';
                                    html_add += '       <dt><input type="text" readonly="" name="interpretation_time_total_time'+this.app_id+'" id="interpretation_time_total_time'+this.app_id+'" size="10" class="ui-widget ui-widget-content ui-corner-all"></dt>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '    </dl>';
                                    html_add += '    <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '   <dl>';
                                    html_add += '       <dd class="checkbox"><input  type="checkbox" name="other'+this.app_id+'" id="other'+this.app_id+'"/><label for="other'+this.app_id+'"><?php echo __('Otro'); ?></label></dd>';
                                    html_add += '       <dd class="none ddOther"><input type="text" size="5" id="otherValue'+this.app_id+'" name="otherValue'+this.app_id+'" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                                    html_add += '       </dd>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '   </dl>';
                                    html_add += '   <div class="clear"></div>';
                                    html_add += '</li>';                                    
                                    break;
                                case 'Document Translation':
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '   <dl>';
                                    html_add += '       <dd class="checkbox"><input value="page" type="radio" checked name="trans_doc_'+this.app_id+'" id="trans_doc_page'+this.app_id+'"/><label for="trans_doc_page'+this.app_id+'"><?php echo __('Página'); ?></label></dd>';
                                    html_add += '       <dd class="dd_doc_page">';
                                    html_add += '           <label for="trans_doc_page_value_'+this.app_id+'"># <?php echo __('Páginas'); ?>:</label><input type="text" size="5" id="trans_doc_page_value_'+this.app_id+'" name="trans_doc_page_value_'+this.app_id+'" class="text_numeric ui-widget ui-widget-content ui-corner-all text_update" alt="num_integer"/>';
                                    html_add += '           <label for="trans_doc_page_rate_'+this.app_id+'"><?php echo __('Precio por página'); ?>:</label><input type="text" size="5" id="trans_doc_page_rate_'+this.app_id+'" name="trans_doc_page_rate_'+this.app_id+'" class="text_numeric ui-widget ui-widget-content ui-corner-all text_update" alt="num_float"/>';
                                    html_add += '       </dd>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '   </dl>';
                                    html_add += '   <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '   <dl>';
                                    html_add += '       <dd class="checkbox"><input value="word" type="radio" name="trans_doc_'+this.app_id+'" id="trans_doc_word'+this.app_id+'"/><label for="trans_doc_word'+this.app_id+'"><?php echo __('Palabra'); ?></label></dd>';
                                    html_add += '       <dd class="none dd_doc_word">';
                                    html_add += '           <label for="trans_doc_word_value_'+this.app_id+'"># <?php echo __('Palabras'); ?>:</label><input type="text" size="5" id="trans_doc_word_value_'+this.app_id+'" name="trans_doc_word_value_'+this.app_id+'" class="text_numeric ui-widget ui-widget-content ui-corner-all text_update" alt="num_integer"/>';
                                    html_add += '           <label for="trans_doc_word_rate_'+this.app_id+'"><?php echo __('Precio por palabra'); ?>:</label><input type="text" size="5" id="trans_doc_word_rate_'+this.app_id+'" name="trans_doc_word_rate_'+this.app_id+'" class="text_numeric ui-widget ui-widget-content ui-corner-all text_update" alt="num_float"/>';
                                    html_add += '       </dd>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '   </dl>';
                                    html_add += '   <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="billing_options_left">';
                                    html_add += '   <dl>';
                                    html_add += '       <dd class="checkbox"><input  type="checkbox" name="other'+this.app_id+'" id="other'+this.app_id+'"/><label for="other'+this.app_id+'"><?php echo __('Otro'); ?></label></dd>';
                                    html_add += '       <dd class="none ddOther"><input type="text" size="5" id="otherValue'+this.app_id+'" name="otherValue'+this.app_id+'" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                                    html_add += '       </dd>';
                                    html_add += '       <dd class="clear"></dd>';
                                    html_add += '   </dl>';
                                    html_add += '   <div class="clear"></div>';
                                    html_add += '</li>';
                                    html_add += '<li class="clear"></li>';
                                    break;
                                default:
                                    break;
                                
                                }

                                var html = hr ;
                                html += '<div class="wrapeer_item">';                            
                                html += '    <input id="enableTransl'+this.app_id+'" name="enableTransl'+this.app_id+'" type="hidden" value="'+i_billing_interpreter_id+'" />';
                                html += '    <div class="cont_app">';
                                html += '       <div class="menutop ui-widget ui-widget-content ui-corner-all">';
                                html += '           <div class="menutop2 menutop5 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">';
                                html += '               <h4><?php echo __('Información de la Cita'); ?> : <span>'+this.app_alias+'</span></h4>';
                                html += '               <div class="clear"></div>';
                                html += '           </div>';
                                html += '           <a data-app_id="'+this.app_id+'" class="editApps"><?php echo __('Editar Cita'); ?></a>';
                                html += '           <div class="clear"></div>';
                                html += '        </div>';
                                html += '        <div class="ui-widget ui-widget-content ui-corner-all contApp">';
                                html += '           <div class="item_num_value ui-state-hover ui-corner-all"><span>Item</span>'+i_count+'</div>';
                                html += '           <div class="middle">';
                                html += '                <ul>';                            
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Nombre de Paciente'); ?>:</label></dt>';
                                html += '                           <dd>'+this.pat_first_name+' '+this.pat_last_name+'</dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Autorizado por'); ?>:</label></dt>';
                                html += '                           <dd>'+this.app_auth+'</dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Fecha de Servicio'); ?>:</label></dt>';
                                html += '                           <dd><span class="highlight_app">'+this.app_date+'</span></dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Hora'); ?>:</label></dt>';
                                html += '                           <dd><span class="highlight_app">'+this.app_time+'</span></dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Llamado por'); ?>:</label></dt>';
                                html += '                           <dd>'+this.app_who_calls+'</dd>';
                                html += '                           <dd class="clear"></dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                   <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Nombre de la Compañia'); ?>:</label></dd>';
                                html += '                           <dd>'+this.app_company_name+'</dd>';
                                html += '                           <dd class="clear"></dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </li>';
                                html += '                </ul>';
                                html += '           </div>';
                                html += '           <div class="middle">';
                                html += '                <ul>';
                                html += '                    <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Región'); ?>:</label></dt>';
                                html += '                           <dd>'+s_region+'</dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                    </li>';
                                html += '                    <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Tipo de Cita'); ?>:</label></dt>';
                                html += '                           <dd>'+this.app_type+'</dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                    </li>';
                                html += '                    <li>';
                                html += '                       <dl>';
                                html += '                           <dt><label><?php echo __('Descripción de la Cita'); ?>:</label></dt>';
                                html += '                           <dd>'+this.tapp_name+'</dd>';
                                html += '                       </dl>';
                                html += '                       <div class="clear"></div>';
                                html += '                    </li>';                            
                                html += '                </ul>';
                                html += '           </div>';
                                html += '           <div class="clear"></div>';
                                html += '           <div class="billing_item_summary">';
                                html += '               <div class="bis_amount" id="bis_amount'+this.app_id+'"></div>';
                                html += '               <div class="bis_label"><?php echo __('Pre Monto'); ?>:</div>';
                                html += '               <div class="clear"></div>';
                                html += '           </div>';
                                html += '           <div class="clear"></div>';
                                html += '        </div>';
                                html += '    </div>';
                                html += '    <div class="cont_form">';
                                html += '        <form name="formTransl'+this.app_id+'" id="formTransl'+this.app_id+'" action="" method="POST">';
                                html += '           <textarea class="none" name="commentServInt'+this.app_id+'" id="commentServInt'+this.app_id+'"></textarea>';
                                html += '           <div class="divTransl divTransl'+this.app_id+'">';
                                html += '               <div class="menutop ui-widget ui-widget-content  ui-corner-all">';
                                html += '                   <div class="menutop2 right menutop3 ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">';
                                html += '                       <h4 style="text-align: center"><?php echo __('Información de Traducción'); ?></h4>';
                                html += '                       <div class="clear"></div>';
                                html += '                   </div>';
                                html += '                   <button type="button" data-app_id="'+this.app_id+'" class="bCommentServInt"><?php echo __('Facturación de Traducción'); ?></button>';
                                html += '                   <div class="clear"></div>';
                                html += '               </div>';
                                html += '               <div class="ui-widget ui-widget-content ui-corner-all contTransl">';
                                html += '                   <ul class="option_list_pre_billing">';
                                html += '                       <li>';
                                html += '                           <dl style="width:100%;">';                            
                                html += '                               <dd class="checkbox noShowLabel">';
                                html += '                                   '+s_no_show_interpreter_html;                            
                                html += '                               </dd>';
                                html += '                               <dd class="clear"></dd>';
                                html += '                           </dl>';
                                html += '                           <div class="clear"></div>';
                                html += '                       </li>';                            
                                html += '                       <li>';
                                html += '                           <dl>';
                                html += '                               <dt><label><?php echo __('Servicio'); ?>:</label></dt>';
                                html += '                               <dd class="app_type_service'+this.app_id+'">'+this.app_type_service+'</dd>';
                                html += '                               <dd class="clear"></dd>';
                                html += '                           </dl>';
                                html += '                           <div class="clear"></div>';                            
                                html += '                       </li>';
                                html += html_to_app;
                                html += html_add;   
                                html += '                   </ul>';
                                html += '                   <label id="statustransl'+this.app_id+'"></label>';
                                html += '                   <div class="clear"></div>';
                                html += '               </div>';
                                html += '           </div>';
                                html += '        </form>';
                                html += '        <div class="clear"></div>';
                                html += '    </div>';
                                html += '    <div class="clear"></div>';
                                html += '</div>';
                                
                                $('div.content_to_billing').append(html);
                                
                                $('.checkbox').buttonset();
                                $('.text_time').setMask();
                                $('.text_numeric').setMask();
                                $('#formTransl'+this.app_id).validate({
                                    submitHandler: function(form) {
                                        alert($(form).serialize());
                                    }
                                });
                                i_app_insurance_carriers_id = this.app_insurance_carriers_id;
                                i_pat_id = this.pat_id;
                                i_cl_id = this.cl_id;
                                d_app_date = this.app_date;
                                //a_apps_transl.push(this.app_id);

                                $('[name=trans_doc_'+this.app_id+']').change(function () {
                                    
                                    if($(this).val()=='page'){
                                        $(this).parents('ul').find('dd.dd_doc_page').css('display','block');
                                        $(this).parents('ul').find('dd.dd_doc_word').css('display','none');
                                    }else{
                                        $(this).parents('ul').find('dd.dd_doc_word').css('display','block');
                                        $(this).parents('ul').find('dd.dd_doc_page').css('display','none');
                                    
                                    }
                                    fnUpdateAmount();
                                    return false;
                                });
                                
                                
                                $('[name=other'+this.app_id+']').change(function () {
                                    if($(this).is(':checked')){
                                        $(this).parents('dl').find('dd.ddOther').css('display','block');
                                    }else{
                                        $(this).parents('dl').find('dd.ddOther').css('display','none');
                                    }
                                    fnUpdateAmount();
                                    return false;
                                });
                                
                                $("#statustransl"+this.app_id).text('');
                                $("#savebilltransl"+this.app_id).removeAttr('disabled');
                                $("#statusbillingtransl"+this.app_id).text('');
                                $(".divTransl"+this.app_id+" input").removeAttr('disabled');
                                $('#current_interpreter').html(this.interpreter_name+' '+this.interpreter_last_name);
                  
                                $('.checkbox').buttonset();
                                $('.checkbox input[type=checkbox]').live('click', function() {
                                    fnUpdateAmount();
                                });                 

                                $( ".calc_time_int,.calc_travel_time_int" ).button({
                                    icons: {
                                        primary: "ui-icon-clock"
                                    },
                                    text: false
                                });
                            
                                $('.bCommentServInt').button({
                                    icons:{
                                        primary: 'ui-icon-comment'
                                    },
                                    text: false
                                });
                                $('.editApps').button({
                                    icons:{
                                        primary: 'ui-icon-zoomin'
                                    },
                                    text: false
                                });
                                $('input.text_update').live('keydown', function(e) {
                                    if (e.keyCode == 13)
                                        fnUpdateAmount();
                                });
                                
                            });//each
                            //                            $('.div_pre_billing').slideUp(500, function() {
                            //                                $('.div_billing').slideDown(500)
                            //                            });
                            
                            $(".calc_travel_time_int").click(function() {
                                var i_app_id = $(this).data('id_app');
                                var dataString = "totalMileage=" + $('#total_mileage'+i_app_id ).val() + "&ajax=ajax";
                                $.ajax({
                                    type: "POST",
                                    url: getmilestotime,
                                    data:dataString,
                                    dataType:'json',
                                    success: function(response)
                                    {
                                        $("#travel_total_time"+i_app_id ).val(response);
                                            
                                        fnUpdateAmount();
                                    }
                                });
                            });
                            
                            
                            
                            $(".calc_time_int").click(function(){
                                var i_app_id = $(this).data('id_app');
                                var from = $("#interpretation_time_from"+i_app_id).val();
                                var to = $("#interpretation_time_to"+i_app_id).val();
                                var servicesType = $(".app_type_service"+i_app_id).text();
                                var dataString = "from=" + from + "&to=" +to + "&ajax=ajax";
                                //console.log(from);
                                $.ajax({
                                    type: "POST",
                                    url: calculartiempotrasnc,
                                    data:dataString,
                                    dataType:"json",
                                    success: function(response) {
                                      
                                        
                                        switch(servicesType){
                                            case "Conference Call":

                                                $("#interpretation_time_total_time"+i_app_id).val(response);
                          
                                                break;
                                            case "Interpreting":
                          
                                                $("#interpretation_time_total_time"+i_app_id).val(response);
                                               
                                                break;
                                            default:
                                                msgBox("<?php echo __('Document Translation u otro tipo de servicio no requiere Summary Translation.'); ?>"
                                                    +"<?php echo __('Envie un e-mail al administrador del sistema para solucionar este error.'); ?>","<?php echo __('Alerta'); ?>");
                                                break;
                                        }
                                        fnUpdateAmount();
                                        
                                    }
                                });
                                                     
                            });
                            fnUpdateAmount();

                        }//succes
                    });
                }

            });
            
            function fnUpdateAmount(){
                var o_response = fnGetPreBilling();
                ;
            
                if(o_response.hasOwnProperty('pre_sum')){            
                    $.each(o_response.pre_sum, function(i_index_app, f_amount ){
                      
                        $('#bis_amount'+i_index_app).html(f_amount);
                    });
                    $('#summary_f_amount_total').html(o_response.pre_total);
                }
            }
            
            function fnGetPreBilling(){
                var s_data_forms = '';
                for(i=0;i<a_check_ids.length;i++){
                    s_data_forms += '&'+$('#formTransl'+a_check_ids[i]).serialize();                
                }
                s_data_forms = 'a_apps_id='+a_check_ids.join(',')+s_data_forms;

                var s_url_data = "billing_interpreter_date="+$('#billing_interpreter_date').val()+'&interpreter_id='+$("#interpreter_id_hidden").val()+'&'+s_data_forms + '&save=' + $("#save_hidden").val()+"&ajax=ajax";
                
                var o_return = new Object();
                $.ajax({
                    type: "POST"
                    ,url: saveBillingInterpreter
                    ,data: s_url_data
                    ,async: false
                    ,dataType:'json'
                    ,success: function(response) {                    
                        o_return = response;                    
                    }
                });
                return o_return;
            }
            $('.checkbox input[type=checkbox]').live('click', function(){        
                fnUpdateAmount();
            });
        
            $('input.text_update').live('keydown', function(e){
                if(e.keyCode == 13)
                    fnUpdateAmount();
            });
            
                   
        
  
        });

</script>


<div id="show_content" class="show_content">
    <div id="pre_billing">
        <table id="grid-listInterpretersBilling" ></table>
        <div id="grid-listInterpretersBilling-paper"></div>      

        <div class="float_right cont_filter">
            <div class="block1_theme content_filter ui-corner-all">
                <div class="padding10">
                    <div>
                        <div class="title_theme ui-corner-all">
                            <h2><?php echo __('Filtro'); ?></h2>                
                        </div>
                        <div class="row_item">
                            <span class="li_item"></span>
                            <div class="item_1">
                                <label><?php echo __('Intérprete'); ?></label>
                            </div>
                            <div class="item_2">
                                <select name="idInterpreter" id="idInterpreter" value="0" >
                                    <option value=''><?php echo __('Seleccionar ...'); ?></option>                         
                                    <?php foreach ($interpreters as $i_id) { ?>
                                        <option value="<?php echo $i_id['idInterpreter'] ?>"><?php echo $i_id['name'] ?><?php echo '  ' ?><?php echo $i_id['lastname'] ?></option>

                                    <?php } ?>

                                </select>


                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row_item">
                            <div class="item_2">
                                <button type="button" id="btn_clean"><?php echo __('Limpiar'); ?></button>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>

    </div>

    <div class="div_billing">
        <button type="button" id="btn_back_billing"><?php echo __('Atrás'); ?></button>
        <input type="hidden" id="interpreter_id_hidden" name="interpreter_id_hidden"/>
        <input type="hidden" id="save_hidden" name="save_hidden" value="false"/>
        <div class="menutop ui-widget ui-widget-content ui-corner-all">
            <div class="menutop2 menutop6 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">
                <h4><?php echo __('Pago a Intérprete'); ?></h4>
                <div class="clear"></div>
            </div>
            <span class="billing_interpreter_number ui-state-active ui-priority-primary ui-corner-all"><?php echo __('Número de Pago a Intérprete'); ?>:<label id="billing_interpreter_number"></label></span>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="ui-widget ui-widget-content ui-corner-all info_billing_wrapper">
            <div class="info_billing">
                <table width="50%" border="0" style="float: left;">
                    <tr>
                        <td>
                            <label for="billing_interpreter_date"><?php echo __('Fecha de Pago'); ?>:</label>
                        </td>
                        <td>
                            <input id="billing_interpreter_date" name="billing_interpreter_date" type="text" />
                        </td>            
                        <td>
                            <label><?php echo __('Intérprete'); ?>:</label>
                        </td>
                        <td id="current_interpreter">

                        </td>            

                    </tr>
                </table>   
                <div id="summary_f_amount_total" class="ui-state-error ui-corner-all"></div>
                <div id="summary_label_total"><?php echo __('TOTAL'); ?></div>
                <div class="clear"></div>            
            </div>
        </div>
        <div class="clear"></div>
        <div class="content_to_billing">

        </div>
        <div class="clear"></div>
        <hr class="hr_separate"/>
        <div class="buttons_container">
            <button type="button" id="preview_billing_interpreter"><?php echo __('Guardar'); ?></button>
        </div>
    </div>

    <div id="divComment" class="dialog" width="400px" title="<?php echo __('Notas'); ?>" resizable="false">
        <form id="formCommentServ">
            <input name="textareaid" id="textareaid" type="hidden"/>
            <label>Note:</label>
            <textarea name="commentServ" cols="30" rows="8" id="commentServ"></textarea>
            <button type="submit" class="savComment"><?php echo __('Guardar'); ?></button>
        </form>
    </div>

    <div id="dialog_pre_view" class="none">
        <div class="padding10">
            <div class="wrapper_pre_billing_summary">
                <button id="process_payment"><?php echo __('Procesar Pago'); ?></button>
                <div class="amount_pre_billing_summary ui-corner-all ui-state-active">

                </div>
                <div class="label_pre_billing_summary ui-corner-all">

                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <ul class="ul_items_pre_billing">
                <li class="pre_billing_header ui-widget-header ui-corner-all">
                    <dl>
                         <dd class="pre_billing_date"><?php echo __('Fecha'); ?></dd>
                         <dd class="pre_billing_description"><?php echo __('Descripción'); ?></dd>
                         <dd class="pre_billing_quantity"><?php echo __('Cantidad'); ?></dd>
                    <dd class="pre_billing_ratio"><?php echo __('Proporción'); ?></dd>
                    <dd class="pre_billing_amount"><?php echo __('Monto'); ?></dd>
                    </dl>
                    <div class="clear"></div>
                </li>
            </ul>
        </div>
    </div>

</div>
