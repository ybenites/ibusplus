<style type="text/css">
    .short_label{
        font-weight: bold;  
        width: 60px;
    }
</style>
<script type="text/javascript">
    
    var listDepositSummary = "/billings/depositsummary/listDepositSummary";
    var printCheckSummary = "/billings/depositsummary/reportCheckSummary";
    var confirmDepositSummary = "/billings/depositsummary/confirmDepositSummary";
    var aChecks = [];
    $(document).ready(function() {
    
        $("#dateFrom,#dateTo").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if($('#dateFrom').val()!='' && $('#dateTo').val()!=''){
                    $("#grid-listDepositSummary").trigger("reloadGrid");
                }
            },
            onClose: function( selectedDate ) {
        
                if($( "#dateFrom" ).val()!='') $( "#dateTo" ).datepicker( "option", "minDate", selectedDate );
                if($( "#dateTo" ).val()!='') $( "#dateFrom" ).datepicker( "option", "maxDate", selectedDate );
            } 
        });
        $("#idUser").combobox({
            selected: function() {
                $(this).val();
            }
        });
        $('#refreshReport').button({
            icons: {primary:'ui-icon-refresh'},
            text:true
        }).click(function (){
            aChecks = [];
            $("#grid-listDepositSummary").trigger("reloadGrid");
        });

        $('#printReport').button({
            icons: {primary:'ui-icon-print'},
            text:true
         }).click(function(){
                
            $("#grid-listDepositSummary input:checkbox").each(function() {

                if (this.checked) {
                    aChecks.push($(this).val());   
           }
            });
           var data = new Object();

            data.arrayChecks= aChecks;
            if (aChecks.length == 0) {
                msgBox('<?php echo __('Seleccione un registro'); ?>'
                        , '<?php echo __('Alerta'); ?>');
                } else {
//            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
//            $('#divReport').iReportingPlus('generateReport', 'pdf');
//            
             confirmBox('<?php echo __('¿Resumen de Depósitos es correcto?'); ?>','<?php echo __('Confirmación'); ?>',function(response){
                if(response){  
                    showLoading = 1;
                    $.ajax({                      
                        url:confirmDepositSummary,
                        type:"post",
                        data : 'aChecks=' + aChecks,
                        dataType: "json",
                        success: function(j_response){                            
                            if (evalResponse(j_response)){
                              
                                $("#grid-listDepositSummary").trigger("reloadGrid");
                            }
                        }
                    });          
                }
            });   
            }
       });
        $('#select_all').button({
            icons: {primary:'ui-icon-check'},
            text:true
        }).click(function(){
 
           $('.checkBilling').prop('checked', true).trigger('change');
        });

        $('#deselect_all').button({
            icons: {primary:'ui-icon-close'},
            text:true
        }).click(function(){
           
           $('.checkBilling').prop('checked', false).trigger('change');
        });
           gridlistBilling = $("#grid-listDepositSummary").jqGrid({
                url: listDepositSummary,
                datatype: "json",
                postData: {
                    dateFrom:   function() {  return $( "#dateFrom" ).val();} ,
                    dateTo:  function() {  return  $( "#dateTo" ).val(); },
                    idUser:  function() {  return  $( "#idUser" ).val(); }
                    
                },
                colNames: ['Billing',
                           '<?php echo __('Seleccionar'); ?>', 
                           '<?php echo __('Usuario'); ?>', 
                           '<?php echo __('Número de Cheque'); ?>', 
                           '<?php echo __('Método'); ?>', 
                           '<?php echo __('Compañía de Seguros'); ?>', 
                           '<?php echo __('Factura'); ?>', 
                           '<?php echo __('Comentarios'); ?>', 
                           '<?php echo __('Monto'); ?>', 
                           '<?php echo __('Fecha de Depósito'); ?>'],
                colModel: [
                    {name: 'billing', index: 'billing', hidden: true, width: 70},
                    {name: 'select', index: 'select', align: 'center', width: 70},
                    {name: 'user', index: 'user', width: 150, align: 'rigth'},
                    {name: 'numberCheck', index: 'numberCheck', width: 100, align: "rigth"},
                    {name: 'method', index: 'method', search: true, width: 150, align: 'center'},
                    {name: 'insuranceCarrier', index: 'insuranceCarrier', search: true, width: 250, align: "center"},
                    {name: 'invoice', index: 'invoice', search: true, width: 100, align: "center"},
                    {name: 'memo', index: 'memo', search: false, width: 150, align: "right"},
                    {name: 'amount', index: 'amount', search: false, width: 100, align: "right"}, 
                    {name: 'depositDate', index: 'depositDate', search: false, width: 100, align: "center"},

                ],
                rowNum: 50,
                rowList: [30, 40, 50],
                height: 450,
                pager: 'grid-listDepositSummary-paper',
                sortname: 'dateRegistrationOut',
    //            multiselect: true,
                viewrecords: true,
                sortorder: "asc",
                toolbar: [true, "top"],
                afterInsertRow: function(row_id, data_id) {

                     checkBilling = "<input class='checkBilling' name='listCheck' id='check_"+ row_id +"' type=\"checkbox\" value=\"" + row_id + "\" ><label for='check_"+ row_id + "' ><?php echo __('Seleccionar'); ?></label>";

                    $("#grid-listDepositSummary").jqGrid('setRowData', row_id, {select: checkBilling});

                },
                gridComplete: function() {


                    $('.checkBilling').button({
                        icons: {primary: 'ui-icon-check'},
                        text: false
                    });

                }
            });

    
    $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_BILLING_DEPOSITSUMMARY',
            htmlVisor: false,
            jqUI: true,
            urlData: printCheckSummary,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...',
            afterJSONResponse: function(response) {
                if (isSendEmail) {
                    myAfterJSONResponse(response);
                }
            }

        });
    });
</script>
<div id="show_content"  class="show_content" >
    <br><br><br><br>
    <label class="long_label"><?php echo __('Filtro por Rango de Fechas') ?>:</label>
    <br><div class="clear"></div>
    <dl>
    <label class="short_label" for="dateFrom" ><?php echo __('From:') ?></label>
    <input type="text" id="dateFrom" name="dateFrom">
    <label class="short_label"  for="dateTo"  ><b><?php echo __('To:') ?></b></label>
    <input type="text" id="dateTo" name="dateTo">
 
                    <a href="#" id='refreshReport'>
                        <?php echo __('Refrescar Reporte') ?>
                    </a>
               
                    <a href="#" id="printReport">
                            <?php echo __('Imprimir Reporte') ?>
                    </a>
                
                   
                     <select id="idUser" name="idUser">
                        <option value=""></option>
<?php foreach ($user as $user_id) { ?>
                            <option value="<?php echo $user_id['idUser'] ?>"><?php echo $user_id['fullName'] ?></option>

<?php } ?>
                    </select>
                  
               
                    <a href="#" id='select_all'><?php echo __('Seleccionar Todo') ?></a>
                
                    <a href="#" id='deselect_all'><?php echo __('Deseleccionar Todo') ?></a>
               
    <center>
        <br>
        <table id="grid-listDepositSummary" ></table>
        <div id="grid-listDepositSummary-paper"></div>
    </center>
                    
<div id="divReport">    
</div>
</div>
