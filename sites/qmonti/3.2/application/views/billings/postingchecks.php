<style type="text/css">

    .none label-none{display: none;}

    input[type=text].paid{text-align: right;width: 100px;} 
    input[type=text].nextbalance{text-align: right;width: 100px;}
    input[type=text].isPaid{text-align: center;width: 130px;}
    ul.ulItems{width: 500px;float: left;}
    ul.ulAmounts{float: right;width: 200px;list-style: none;}
    ul.ulLetras{width: 120px;}
    ul.ulAmounts div.amounts{padding: 5px;font-size: 18px;text-align: right;line-height: 100%;margin: 5px;}
    ul.ulAmounts div.amounts2{padding: 6px;font-size: 18px;text-align: left;line-height: 100%;margin: 5px;}
    ul.ulAmounts div.amounts span{float: left;}
    table.display td.right{text-align: right;}
    .submit{float: right;margin: 5px;}
    #txtsearch{width: 140px;}
    .title_wrapper{float: right;padding: 20px;text-align: center;color: #777777;text-transform: uppercase;font-size: 12px;font-weight: bold;}
    .title_insurance{width: 300px;}
    .title_checks{width: 300px;}
    .ui-button.icon .ui-icon{background-image: url("/media/css/img/glyphicons-halflings-white.png");}
    .ui-button .icon_fire{background-position: -72px -120px;}
    .ui-button .icon_gift{background-position: -24px -120px;}
    tr.ui_state_active{background-color: #6EAC2C;}
    tr.ui_state_focus{background-color: #81DDE4 ;}

    .slide-out-div {
        padding: 20px;
        width: 150px;
        background: #DDFFDD;
        border: 1px solid #29216d;
        z-index: 999;
    }  
</style>
<script type="text/javascript">

    var listBillingByIdInsurance = "/billings/postingchecks/listBillingByIdInsurance";
    var listChecksByIdInsurance = "/billings/postingchecks/listChecksByIdInsurance";
    var getCheckById = "/billings/managerchecks/getCheckById";
    var getDescuentoAndBalanceById = "/billings/postingchecks/getDescuentoAndBalanceById";
    var savePayment = "/billings/postingchecks/savePayment";
    var saveCheck = "/billings/managerchecks/saveCheck";
    var idInsurance;
    var flagdiscount = false;

    var idiscount = 0;
    var ibalance = 0;
    $(document).ready(function() {

        $("#idInsuranceCarriers,#cbo_idInsuranceCarrier").combobox();
        $("#cbo_idCheck").combobox();
        $("#addCheck").button();
        $('#dateCheck').datepicker({dateFormat: 'mm/dd/yy'});


        gridlistBilling = $("#grid-listBillingByIdInsurance").jqGrid({
            url: listBillingByIdInsurance,
            datatype: "json",
            postData: {
                idInsuranceCarrier: function() {
                    return idInsurance;
                }
            },
            colNames: ['Billing', 
                       '<?php echo __('Seleccionar') ?>', 
                       '<?php echo __('Factura') ?>', 
                       '<?php echo __('Fecha') ?>', 
                       '<?php echo __('Paciente') ?>', 
                       '<?php echo __('Tipo de Servicio') ?>', 
                       '<?php echo __('Monto') ?>', 
                       '<?php echo __('Pagado') ?>', 
                       'discountInvoice', 'discountInsurance', 'discountPercent', 'balanceOut', 
                       '<?php echo __('Descuento') ?>', 
                       '<?php echo __('Balance') ?>', 
                       '<?php echo __('A Pagar') ?>'],
            colModel: [
                {name: 'billing', index: 'billing', hidden: true, width: 70},
                {name: 'check', index: 'check', align: 'center', width: 70},
                {name: 'invoice', index: 'invoice', width: 100},
                {name: 'date', index: 'date', width: 100, align: "center"},
                {name: 'patient', index: 'patient', search: true, width: 150},
                {name: 'typeservice', index: 'typeservice', search: true, width: 150, align: "center"},
                {name: 'amount', index: 'amount', search: true, width: 120, align: "right"},
                {name: 'paid', index: 'paid', search: false, width: 100, align: "right"},
                {name: 'discountInvoice', index: 'discountInvoice', search: false, width: 100, align: "right", hidden: true}, {name: 'discountInsurance', index: 'discountInsurance', search: false, width: 100, align: "right", hidden: true},
                {name: 'discountPercent', index: 'discountPercent', search: false, width: 100, align: "right", hidden: true},
               
                {name: 'balanceOut', index: 'balanceOut', search: false, width: 100, align: "right", hidden: true},
                {name: 'discount', index: 'discount', search: false, width: 100, align: "right"},
                {name: 'balance', index: 'balance', width: 100, align: "right", sortable: false, search: false},
                {name: 'topaid', index: 'topaid', width: 200, align: "right", sortable: false, search: false}
            ],
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 450,
            pager: 'grid-listBillingByIdInsurance-paper',
            sortname: 'billing',
//            multiselect: true,
            viewrecords: true,
            sortorder: "asc",
            toolbar: [true, "top"],
            afterInsertRow: function(row_id, data_id) {

                if ((data_id.discountInvoice == 0) && (data_id.discountInsurance > 0)) {
                    buttondiscount = "<a class=\"discount\" id=\"discount_" + row_id + "\" value='0' idiscount=\"" + data_id.discountInsurance + "\" billing=\"" + row_id + "\" title=\"<?php echo __('Descuento'); ?>\" >$ " + data_id.discountInsurance + "</a>";
                } else {
                    buttondiscount = data_id.discountInvoice;
                }

                buttondiscount += "<input id='haveDiscount_" + row_id + "'  type='hidden' value='0' name='haveDiscount_" + row_id + "'/>" +
                        "<input id='amountDiscount_" + row_id + "'  type='hidden' value='0' name='amountDiscount_" + row_id + "'/>" +
                        "<input id='percentDiscount_" + row_id + "'  type='hidden' value='0' name='percentDiscount_" + row_id + "'/>";

                buttonpaid = "<a class=\"paid\" id=\"bpaid_" + row_id + "\"  data-billing=\"" + row_id + "\" title=\"<?php echo __('Pagar'); ?>\" ><?php echo __('Pagar'); ?></a>"


                textpaid = "<input class='ui-widget ui-widget-content ui-corner-all ui-state-disabled' align-text='right' id=\"paid_" + row_id + "\"  value=\"" + data_id.balanceOut + "\" />";
                textnextbalance = "<input readonly  class='none ui-widget ui-widget-content ui-corner-all ui-state-inactive' align='right' id=\"nextBalance_" + row_id + "\" value=\"\" />";
                buttoncredit = "<label id=\"label_credit_" + row_id + "\" class='none' for=\"check_credit_" + row_id + "\"><?php echo __('Crédito') ?></label><input type=\"checkbox\" class=\"check_credit none\"  name=\"checkCreditLost_" + row_id + "\"  id=\"check_credit_" + row_id + "\" billing=\"" + row_id + "\" />";
                buttonlost = "<label  id=\"label_paylost_" + row_id + "\" class='none' for=\"check_paylost_" + row_id + "\"><?php echo __('Pago Extraviado') ?></label><input class=\"check_lost none\" type=\"checkbox\" name=\"checkCreditLost_" + row_id + "\"  id=\"check_paylost_" + row_id + "\" billing=\"" + row_id + "\" />";
                checkBilling = "<input class='checkInvoice' name='listInvoices[]' type=\"checkbox\" value=\"" + row_id + "\" >";

                $('#jqg_grid-listBillingByIdInsurance_' + row_id).attr('billing', row_id);

                $("#grid-listBillingByIdInsurance").jqGrid('setRowData', row_id, {check: checkBilling, discount: buttondiscount, balance: data_id.balanceOut, topaid: textpaid + buttonpaid + '<br>' + textnextbalance + buttonlost + buttoncredit});

            },
            gridComplete: function() {


                $('.paid').button({
                    icons: {primary: 'ui-icon-refresh'},
                    text: false

                }).addClass('ui-state-disabled');
                $('.discount').button({
                    icons: {primary: 'ui-icon-plus'},
                    text: true

                }).addClass('ui-state-disabled');
                $('.check_credit').button({
                    icons: {primary: 'icon_gift'},
                    text: false
                }).addClass('none');
                $('.check_lost').button({
                    icons: {primary: 'icon_fire'},
                    text: false
                }).addClass('none');

            }
        });


        $("#t_grid-listBillingByIdInsurance").css({
            height: "37px",
        }).append('<button id="realSubmit" class="submit" type="submit" ><?php echo __('Procesar Pago') ?></button>');

        $('#realSubmit').button({
            icons: {
                primary: 'ui-icon-circle-check'
            }
        });

        $("#realSubmit").click(function() {
            $('#formPaidInvoice').submit();
        });

        $('#grid-listBillingByIdInsurance').on('change', '.checkInvoice', function() {

            idRow = $(this).val();
            oData = $("#grid-listBillingByIdInsurance").jqGrid('getRowData', idRow);
            if ($('#cbo_idCheck').val() > 0) {

                f_pay = parseFloat($('#paid_' + idRow).val());
                f_availableCheck = parseFloat($('#iAvailableCheck').val());
                f_pay = parseFloat($('#paid_' + idRow).val());
                f_toPay = parseFloat($('#toPayNow p').text());

                if (($(this).is(":checked")) == true) {


                    if (f_availableCheck != 0) {
                        $('#discount_' + idRow).removeClass('ui-state-disabled');
                        $('#paid_' + idRow).removeClass('ui-state-disabled');
                        $('#bpaid_' + idRow).removeClass('ui-state-disabled');


                        if (f_availableCheck >= f_pay) {

                            f_toPay = (f_toPay + f_pay);
                            f_availableCheck = (f_availableCheck - f_pay);

                        } else if (f_availableCheck < f_pay) {

                            f_toPay = (f_toPay + f_availableCheck);
                            f_pay = (f_availableCheck);
                            f_availableCheck = (0);
                            f_nextbalance = parseFloat(oData.balance) - f_pay;

                            $('#nextBalance_' + idRow).val(f_nextbalance.toFixed(2));
                            $('#nextBalance_' + idRow).removeClass('none');

                        }


                        $('#toPayNow p').text(f_toPay.toFixed(2));
                        $('#availableCheck p').text(f_availableCheck.toFixed(2));
                        $('#paid_' + idRow).val(f_pay.toFixed(2));
                        $('#iAvailableCheck').val(f_availableCheck.toFixed(2));

                    } else {
                        $(this).prop('checked', false);
                        msgBox("The check selected haven't enough credit", 'ALERTA');
                    }

                } else {


                    $('#paid_' + idRow).addClass('ui-state-disabled');
                    $('#bpaid_' + idRow).addClass('ui-state-disabled');
                    $('#discount_' + idRow).addClass('ui-state-disabled');
                    $('#nextBalance_' + idRow).addClass('none');
                    $('#check_paylost_' + idRow).addClass('none');
                    $('#check_credit_' + idRow).addClass('none');
                    $('#label_credit_' + idRow).addClass('none');
                    $('#label_paylost_' + idRow).addClass('none');
                    f_toPay = (f_toPay - f_pay);


                    f_availableCheck = (f_availableCheck + f_pay);

                    f_balance = oData.balance;


                    $('#toPayNow p').text(f_toPay.toFixed(2));
                    $('#availableCheck p').text(f_availableCheck.toFixed(2));
                    $('#iAvailableCheck').val(f_availableCheck.toFixed(2));
                    $('#paid_' + idRow).val(f_balance);


                }
            } else {
                $(this).prop('checked', false)
                msgBox('Select a check', 'ALERTA');

            }
        });

        $('#grid-listBillingByIdInsurance').on('click', 'a.discount', function() {


            idRow = $(this).attr('billing');
            flagdiscount = $(this).attr('value');
            oData = $("#grid-listBillingByIdInsurance").jqGrid('getRowData', idRow);
            f_paid = parseFloat($('#paid_' + idRow).val());
            f_balance = parseFloat(oData.balance);
            f_discount = parseFloat($(this).attr('idiscount'));
            f_nextbalance = parseFloat($('#nextBalance_' + idRow).val());
            f_toPay = parseFloat($('#toPayNow p').text());

            f_availableCheck = parseFloat($('#iAvailableCheck').val());
            if (flagdiscount == 0) {
                $('#haveDiscount_' + idRow).val('1');
                $('#percentDiscount_' + idRow).val(oData.discountPercent);
                $('#amountDiscount_' + idRow).val(f_discount);
                $(this).button({icons: {primary: 'ui-icon-close'}});
                flagdiscount = 1;
                f_balance = (f_balance - f_discount);

                if (f_balance > f_paid) {

                    f_nextbalance = f_balance - f_paid;

                    $('#nextBalance_' + idRow).removeClass('none');
                    $('#label_credit_' + idRow).removeClass('none');
                    $('#label_paylost_' + idRow).removeClass('none');
                    $('#check_credit_' + idRow).removeClass('none');
                    $('#check_paylost_' + idRow).removeClass('none');
                    $('#nextBalance_' + idRow).val(f_nextbalance.toFixed(2));
                } else {
//                    $('#nextBalance_' + idRow).removeClass('none');
//                    $('#label_credit_' + idRow).removeClass('none');
//                    $('#check_credit_' + idRow).removeClass('none');
//                    $('#check_paylost_' + idRow).removeClass('none');
//                    $('#label_paylost_' + idRow).removeClass('none');
                    f_paid = f_balance;

                    f_toPay = f_toPay - f_discount;
                    f_availableCheck = f_availableCheck + f_discount;
                }
                $('#paid_' + idRow).val(f_paid.toFixed(2));

            } else {
                $('#haveDiscount_' + idRow).val('0');
                $('#percentDiscount_' + idRow).val('0.00');
                $('#amountDiscount_' + idRow).val('0.00');
                $(this).button({icons: {primary: 'ui-icon-plus'}});
                flagdiscount = 0;

                if (f_balance == f_paid && f_availableCheck != 0) {

                    f_toPay = f_toPay + f_discount;
                    f_availableCheck = f_availableCheck - f_discount;
                    f_balance = (f_balance + f_discount);
                    $('#paid_' + idRow).val(f_balance.toFixed(2));
                } else if (f_availableCheck == 0) {

                    f_nextbalance = f_nextbalance + f_discount;
                    f_balance = f_balance + f_discount;
                    $('#nextBalance_' + idRow).val(f_nextbalance.toFixed(2));
                } else {
                    msgBox("The check selected haven't enough credit", 'ALERTA');
                    $(this).button({icons: {primary: 'ui-icon-close'}});
                    flagdiscount = 1;
                }
            }
            $('#toPayNow p').text(f_toPay.toFixed(2));
            $('#availableCheck p').text(f_availableCheck.toFixed(2));
            $('#iAvailableCheck').val(f_availableCheck.toFixed(2));
            $(this).attr('value', flagdiscount);
            $("#grid-listBillingByIdInsurance").jqGrid('setRowData', idRow, {balance: f_balance.toFixed(2)});

        });

        $('#grid-listBillingByIdInsurance').on('click', 'a.paid', function() {
            f_availableCheck = parseFloat($('#iAvailableCheck').val());
            if (f_availableCheck == 0) {
                $('#check_paylost_' + idRow).removeClass('none');
                $('#check_credit_' + idRow).removeClass('none');
                $('#label_credit_' + idRow).removeClass('none');
                $('#label_paylost_' + idRow).removeClass('none');
            }
        });

        $('#cbo_idInsuranceCarrier').combobox({
            selected: function() {

                idInsurance = $(this).val();
                $("#grid-listBillingByIdInsurance").trigger('reloadGrid');

                $.ajax({
                    type: "POST",
                    url: listChecksByIdInsurance,
                    data: 'idInsuranceCarrier=' + idInsurance,
                    dataType: 'json',
                    beforeSend: function() {
                        $("#loading").dialog('open');
                    },
                    complete: function() {
                        $("#loading").dialog('close');
                    },
                    success: function(response) {

                        $('#cbo_idCheck').empty();
                        try {
                            combo = "<option value=''><?php echo __('Seleccionar') ?>..</option>";

                            data = response.data;

                            $.each(data, function() {
                                combo += '<option value="' + this.idCheck + '">' + this.numberCheck + '</option>';
                            });

                            $('#cbo_idCheck').html(combo);
                            $('#cbo_idCheck-combobox').val('');
                            $('#cbo_idCheck').val([]);
                            $('#dateCheck').val('');
                            $('#memo').val('');


                            $('#totalCheck p, #dialog_totalCheck p').text('');
                            $('#paidCheck p, #dialog_paidCheck p').text('');
                            $('#toPayNow p, #dialog_toPayNow p').text('');
                            $('#availableCheck p, #dialog_availableCheck p').text('');
                        } catch (err) {
                            //$('#dialogError').dialog('open');                            
                        }

                    }
                });

                $.ajax({
                    type: "POST",
                    url: getDescuentoAndBalanceById,
                    data: 'idInsuranceCarrier=' + $(this).val(),
                    dataType: 'json',
                    beforeSend: function() {
                        $("#loading").dialog('open');
                    },
                    complete: function() {
                        $("#loading").dialog('close');
                    },
                    success: function(response) {
                        try {
                            data = response.data;

                            $('#descuentoTotal').text(data.descuentoTotal);
                            $('#balanceTotal').text(data.balanceTotal);
                            $('#credit_insurance').text(data.credit_insurance);
                        } catch (err) {
                            //$('#dialogError').dialog('open');                            
                        }

                    }
                });
            }

        });

        $('#cbo_idCheck').combobox({
            selected: function() {

                $.ajax({
                    type: "POST",
                    url: getCheckById,
                    data: 'idCheck=' + $(this).val(),
                    dataType: 'json',
                    beforeSend: function() {
                        $("#loading").dialog('open');
                    },
                    complete: function() {
                        $("#loading").dialog('close');
                    },
                    success: function(response) {
                        try {
                            data = response.data;

                            $('#iPaidCheck').val("0.00");
                            $('#iAvailableCheck').val(data.available);
                            $('#totalCheck p, #dialog_totalCheck p').text(data.amount);
                            $('#paidCheck p, #dialog_paidCheck p').text(data.paid);
                            $('#toPayNow p, #dialog_toPayNow p').text("0.00");
                            $('#availableCheck p, #dialog_availableCheck p').text(data.available);
                            $('#form_memo').val(data.memo);
                            $('#form_dateCheck').val(data.datecheck);
//                            $('#idCheck').val(data.idCheck);

                        } catch (err) {
                            //$('#dialogError').dialog('open');                            
                        }

                    }
                });

            }
        });

        $('#dlg_confirm_posting').dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 250,
            width: 345,
            buttons: {
                "OK": function() {
                    dataString = 'idChecks=' + $('#cbo_idCheck').val() + '&' + $("input[name='listInvoices[]']:checked").serialize();
                    $.each($("input[name='listInvoices[]']:checked"), function() {


                        var i_invoice_id = $(this).val();


                        dataString += ('&toPaid_' + i_invoice_id + '=' + $('input#paid_' + i_invoice_id).val());

                        if ($('#haveDiscount_' + i_invoice_id).length > 0) {
                            dataString += ('&haveDiscount_' + i_invoice_id + '=' + $('input#haveDiscount_' + i_invoice_id).val());
                        }

                        if ($('#amountDiscount_' + i_invoice_id).length > 0) {
                            dataString += ('&amountDiscount_' + i_invoice_id + '=' + $('input#amountDiscount_' + i_invoice_id).val());
                        }
                        if ($('#percentDiscount_' + i_invoice_id).length > 0) {
                            dataString += ('&percentDiscount_' + i_invoice_id + '=' + $('input#percentDiscount_' + i_invoice_id).val());
                        }

                        if ($('#check_paylost_' + i_invoice_id).is(':checked')) {
                            dataString += ('&check_lost_payment[]=' + $(this).val());
                        } else
                        if ($('#check_credit_' + i_invoice_id).is(':checked')) {
                            dataString += ('&check_credit[]=' + $(this).val());
                        }
                    });


                    $.ajax({
                        type: "POST",
                        url: savePayment,
                        data: dataString,
                        dataType: 'json',
                        beforeSend: function() {
                            $("#loading").dialog('open');
                        },
                        complete: function() {
                            $("#loading").dialog('close');
                        },
                        success: function(response) {

                            data = response;

                            if(data.saved == 1){
                                location.reload(true);
                            }else{
                                msgBox('<?php echo __('Recargue su Página. Algunas facturas no pudieron guardarse') ?>: '+(data.listNoSaved).join(', '), 'Error');
                            }

                            $('#dialogCheck').dialog("close");
                        }
                    });
                },
                "<?php echo __('Cancelar') ?>": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });

        $('#dialogCheck').dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 350,
            width: 490,
            buttons: {
                "<?php echo __('Registrar') ?>": function() {
                    if (formCheck.form()) {
                        $('#formCheck').submit();

                    }
                },
                "<?php echo __('Cancelar') ?>": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });

        formCheck = $('#formCheck').validate({
            rules: {
                idInsuranceCarriers: {
                    required: true
                },
                numberCheck: {
                    required: true
                },
                amount: {
                    required: true
                },
                dateCheck: {
                    required: true
                }
            },
            messages: {
                idInsuranceCarriers: {
                    required: '<?php echo __('La Compañía de Seguros es requerida.'); ?>'
                },
                numberCheck: {
                    required: '<?php echo __('El Número de Cheque es requerido.'); ?>'
                },
                amount: {
                    required: '<?php echo __('El Monto del cheque es requerido.'); ?>'
                },
                dateCheck: {
                    required: '<?php echo __('La Fecha del Cheque es requerida.'); ?>'
                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            errorElement: "em",
            submitHandler: function(form) {
                dataString = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: saveCheck,
                    data: dataString,
                    dataType: 'json',
                    beforeSend: function() {
                        $("#loading").dialog('open');
                    },
                    complete: function() {
                        $("#loading").dialog('close');
                    },
                    success: function(response) {

                        if (response.msg == 'OK') {
                            $('#dialogCheck').dialog("close");
                            $("#grid-listChecks").trigger('reloadGrid');
                        } else {
                            msgBox(response.response, '<?php echo __('Alerta') ?>');
                        }

                    }

                });
            }
        });

        $('#addCheck').click(function() {
            $('#idInsuranceCarriers-combobox').val($('#cbo_idInsuranceCarrier-combobox').val());
            $('#idInsuranceCarriers').val($('#cbo_idInsuranceCarrier').val());

            $('#dialogCheck').dialog('open');

            formCheck.resetForm();

        });

        formPaidInvoice = $('#formPaidInvoice').validate({
            rules: {
                cbo_idInsuranceCarrier: {
                    required: true
                },
                numberCheck: {
                    required: true
                }
            },
            messages: {
                cbo_idInsuranceCarrier: {
                    required: '<?php echo __('La Compañía de Seguros es requerida.'); ?>'
                },
                numberCheck: {
                    required: '<?php echo __('El Número de Cheque es requerido.'); ?>'

                }
            },
            errorContainer: '#errorMessages',
            errorLabelContainer: "#errorMessages .content .text #messageField",
            wrapper: "p",
            highlight: function(element, errorClass) {
                $(element).addClass('ui-state-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('ui-state-error');
            },
            errorElement: "em",
            submitHandler: function(form) {

                        $('#dialog_totalCheck').text( $('#totalCheck').text());
                            $('#dialog_paidCheck').text($('#paidCheck').text());
                            $('#dialog_toPayNow').text($('#toPayNow').text());
                            $('#dialog_availableCheck').text($('#availableCheck').text());
                $('#dlg_confirm_posting').dialog('open');


            }
        });
    });
</script>
    <center>
        <br><br>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all">
<?php echo __('Publicación de Cheques') ?>
        </div>    
        </br>
    </center>
<div id="show_content"  class="show_content" >

        <input id="iAvailableCheck" type="hidden" name="iAvailableCheck" value="0"/>
        <input id="iPaidCheck" type="hidden" name="iPaidCheck" value="0"/>    
        <form id="formPaidInvoice" action="#" method="GET">  
            <div>
                <div class="title_wrapper title_checks">
                    <?php echo __('Información del Cheque') ?>
                </div>
                <div class="title_wrapper title_insurance">
                    <?php echo __('Información de Aseguradora') ?>
                </div>
            </div>
            <div class="clear"></div>
            <ul class="ulAmounts">
                <li class="ui-widget">
                    <div id="totalCheck" class="amounts ui-state-highlight ui-corner-all">
                        <span>$</span>
                        <p>

                        </p>
                        <div class="clear"></div>
                    </div>
                </li>
                <li class="ui-widget">
                    <div id="paidCheck" class="amounts ui-state-error ui-corner-all">
                        <span>$</span>
                        <p>

                        </p>
                        <div class="clear"></div>
                    </div>
                </li>
                <li class="ui-widget">
                    <div id="toPayNow" class="amounts ui-state-hover ui-corner-all">
                        <span>$</span>
                        <p>

                        </p>
                        <div class="clear"></div>
                    </div>
                </li>
                <li class="ui-widget">
                    <div id="availableCheck" class="amounts ui-state-active ui-corner-all">
                        <span>$</span>
                        <p>

                        </p>                
                        <div class="clear"></div>
                    </div>            
                </li>        
            </ul>
            <ul class="ulAmounts ulLetras">
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Monto') ?>:</div>
                </li>        
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Pagado') ?>:</div>
                </li>        
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('A Pagar') ?>:</div>
                </li>        
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Crédito') ?>:</div>
                </li>        
            </ul>
            <ul class="ulItems">
                <li>
                    <dl>
                        <dd>
                            <label class="frmlbl"><?php echo __('Compañía de Seguros'); ?>:</label>
                        </dd>
                        <dd>
                            <select id="cbo_idInsuranceCarrier" name="cbo_idInsuranceCarrier"  validate="{ldelim}required: true{rdelim}">                        
                                <option value=""></option>
<?php foreach ($insurance_carrier as $ins_id) { ?>
                                    <option value="<?php echo $ins_id['idInsurancecarrier'] ?>"><?php echo $ins_id['name'] ?></option>
<?php } ?>
                            </select>
                        </dd>
                    </dl>
                    <div class="clear"></div>
                    <br>
                    <dl>
                        <dd>
                            <label class="frmlbl"><?php echo __('Número de Cheque'); ?>:</label>
                        </dd>
                        <dd>
                            <select id="cbo_idCheck" name="cbo_idCheck" class="cmbCheck" validate="{ldelim}required: true{rdelim}">                        
                                <option value=""></option>                        
                            </select>
                            <button type="button" id="addCheck"><?php echo __('Agregar Cheque') ?></button>
                        </dd>
                    </dl>
                    <div class="clear"></div>
                    <br>
                    <dl>
                        <dd>
                            <label class="frmlbl"><?php echo __('Fecha de Cheque'); ?>:</label>
                        </dd>
                        <dd>
                            <input id="form_dateCheck" type="text" name="form_dateCheck" class="cmbCheck"  disabled>                        
                        </dd>
                    </dl>
                    <div class="clear"></div>
                    <br>
                    <dl>
                        <dd>
                            <label class="frmlbl"><?php echo __('Nota'); ?>:</label>
                        </dd>
                        <dd>
                            <input id="form_memo" type="text" name="form_memo" class="cmbCheck"  disabled>                        
                        </dd>
                    </dl>
                    <div class="clear"></div>
                </li>
            </ul>
            <ul class="ulAmounts">            
                <li class="ui-widget">
                    <div class="amounts ui-state-highlight ui-corner-all">
                        <span>$</span>
                        <p id="descuentoTotal">

                        </p>
                        <div class="clear"></div>
                    </div>
                </li>
                <li class="ui-widget">
                    <div class="amounts ui-state-error ui-corner-all">
                        <span>$</span>
                        <p id="balanceTotal">

                        </p>                
                        <div class="clear"></div>
                    </div>            
                </li>        
                <li class="ui-widget">
                    <div class="amounts ui-state-active ui-corner-all">
                        <span>$</span>
                        <p id="credit_insurance">

                        </p>                
                        <div class="clear"></div>
                    </div>            
                </li>        
            </ul>
            <ul class="ulAmounts ulLetras">
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Descuento'); ?>:</div>
                </li>        
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Balance'); ?>:</div>
                </li>      
                <li class="ui-widget">
                    <div class="amounts2"><?php echo __('Crédito'); ?>:</div>
                </li>      
            </ul>

        </form>
  
    <div class="none divCont" id="dlg_confirm_posting"  title="<?php echo __('Confirmacion de Asignación de Cheque'); ?>">   

        <ul class="ulAmounts">
            <li class="ui-widget">
                <div id="dialog_totalCheck" class="amounts ui-state-highlight ui-corner-all">
                    <span>$</span>
                    <p>

                    </p>
                    <div class="clear"></div>
                </div>
            </li>
            <li class="ui-widget">
                <div id="dialog_paidCheck" class="amounts ui-state-error ui-corner-all">
                    <span>$</span>
                    <p>

                    </p>
                    <div class="clear"></div>
                </div>
            </li>
            <li class="ui-widget">
                <div id="dialog_toPayNow" class="amounts ui-state-hover ui-corner-all">
                    <span>$</span>
                    <p>

                    </p>
                    <div class="clear"></div>
                </div>
            </li>
            <li class="ui-widget">
                <div id="dialog_availableCheck" class="amounts ui-state-active ui-corner-all">
                    <span>$</span>
                    <p>

                    </p>                
                    <div class="clear"></div>
                </div>            
            </li>        
        </ul>
        <ul class="ulAmounts ulLetras">
            <li class="ui-widget">
                <div class="amounts2"><?php echo __('Monto'); ?>:</div>
            </li>        
            <li class="ui-widget">
                <div class="amounts2"><?php echo __('Pagado'); ?>:</div>
            </li>        
            <li class="ui-widget">
                <div class="amounts2"><?php echo __('A Pagar'); ?>:</div>
            </li>        
            <li class="ui-widget">
                <div class="amounts2"><?php echo __('Crédito'); ?>:</div>
            </li>        
        </ul>

    </div>

    <center>
        <br><br><br><br><br><br><br><br><br><br><br><br>
        <table id="grid-listBillingByIdInsurance" ></table>
        <div id="grid-listBillingByIdInsurance-paper"></div>
    </center>

    <div id="dialogCheck" class="divCont none" title="<?php echo __('Nuevo Cheque'); ?>">
        <form id="formCheck" action="#">
            <input type="hidden" id="idCheck" name="idCheck" value="0"/>

            <dl>
                <dd><label class="frmlbl" ><?php echo __('Número de Cheque'); ?>:</label></dd>
                <dd>
                    <input type="text" id="numberCheck" name="numberCheck"  />
                </dd>
            </dl>
            <div class="clear"></div>
            <br>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Compañía de Seguros'); ?>:</label>
                </dd>
                <dd>
                    <select id="idInsuranceCarriers" name="idInsuranceCarriers">
                        <option value=""></option>
<?php foreach ($insurance_carrier as $ins_id) { ?>
                            <option value="<?php echo $ins_id['idInsurancecarrier'] ?>"><?php echo $ins_id['name'] ?></option>

<?php } ?>
                    </select>
                </dd>
            </dl>
            <div class="clear"></div>
            <br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Monto'); ?>:</label></dd>
                <dd>
                    <input type="text" id="amount" name="amount" />
                </dd>
            </dl>
            <div class="clear"></div>
            <br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Fecha de Cheque'); ?>:</label></dd>
                <dd>
                    <input id="dateCheck" type="text" name="dateCheck" />
                </dd>
            </dl>
            <div class="clear"></div>
            <br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Nota'); ?>:</label></dd>
                <dd>
                    <textarea name="memo" id="memo" cols="27" rows="6" ></textarea>
                </dd>
            </dl>
            <div class="clear"></div>

        </form>
    </div>  

</div>    