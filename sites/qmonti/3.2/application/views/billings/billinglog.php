<style type="text/css">
    ul li{list-style: none;}
    #show_summary{float: right;}
    b.balert{color:#e10101;}
    ul#liApps{float: left; list-style: none;margin: 0;padding: 0;width: 100%;}
    .none{display: none;}
    #dataRates fieldset{display: inline;}
    #statusbillingtransl,#statusbillingtransp,#statustransp,#statustransl{color: red;font-style: oblique;}
    a{cursor: pointer;}
    div#MarginTable{min-height:140px;}

    div#MarginTableEMP{float:left;}
    #formNew{display:none;margin-top:30px;float:left;}
    #driConf{float:left;}
    #locConf{margin-left: 370px;height:70px;}
    #patConf{margin-left: 180px;width:80px;margin-top:-95px;}
    #formNewEmpNote{display:none;margin-top:30px;}
    #formNewIntNote{display:none;margin-top:30px;}
    #formNewDriNote{display:none;margin-top:30px;}
    #imgEdit, #imgEmployeer, #imgDriver ,#imgInterpreter{cursor:pointer;}
    #closeDriNote, #closeEmpNote, #closeIntNote,#appoitmentFORM{display:none;}
    #callingInfo{width:300px;}
    #pickUpInfo{width:540px;margin-left: 15px;margin-top: -247px;float: left;}
    #serviceInfo{float:left;width:300px;}
    #fieldsetfrom{float: left;margin-right:10px;}
    #fieldsetfrom,#fieldsetto{width:235px;}
    #formNewConNote{display:none;margin-top:30px;}
    #formNewPatNote{display:none;margin-top:30px;}
    #backMarginTable{display: none;}
    #a,#st,#w,.transPage,.transWord,#divbilling{display: none;}
    #a,#w,#st{width: 200px;}
    .tableCont ul li.travel{margin: 0;}
    .frmlbl,.floatLeft input{float: left;}
    .frmlbl{width: 120px;line-height: 15px;height: auto;}
    p{padding: 0;margin: 0;}
    .menutop{padding: 2px;}
    .menutop1{padding: 2px;width: 590px;}
    .menutop2{padding: 5px;}
    .tableOne label{float: left;margin-right: 5px;}
    .tableOne label.header{text-align: center;}
    .pickup{width: 210px;}
    .pick2{width: 120px;text-align: center;}
    .travel{width: 320px;text-align: center;}
    .miles{width: 80px;text-align: center;}
    .status{width: 85px;text-align: center;}
    .tableCont{margin-top: 5px;padding: 7px; width: 1183px;}
    h1,h2,h3,h4{margin: 0;padding: 0;}
    .content4Billing{margin-top: 10px;}
    .divTrans{float: left;width: 591px;margin-right: 10px;}
    .divInt{float: left;width: 591px;}
    .contTrans, .contInt{padding: 7px;margin-top: 5px;}
    .contApp {padding: 7px;margin-top: 5px; margin-bottom: 5px;}
    .contTrans ul,.contInt ul,.contApp ul{list-style: none;margin: 0;padding: 0;}
    .contTrans ul li,.contInt ul li ,.contApp ul li{list-style: none;padding: 5px 0;display: block;}
    .contTrans ul li dl,.contTrans ul li dd,.contTrans ul li dt,
    .contApp ul li dl,.contApp ul li dd,.contApp ul li dt,
    .contInt ul li dl,.contInt ul li dd,.contInt ul li dt{padding: 0;margin: 0 10px 0 0;float: left;}
    .contTrans ul li dt,.contTrans ul li dd,
    .contInt ul li dt,.contInt ul li dd{float: left;}
    .contTrans ul li dt label.driver{width: 110px;margin-right: 5px;}
    .contTrans ul li dd label.driverName{width: 445px;font-weight: normal;}
    .contTrans ul li dd label.wthour,.contTrans ul li dd label.wtmin,.contTrans ul li dd label.time{font-weight: normal;padding: 5px 0;margin-right: 5px;}
    .contTrans ul li dd input{margin-right: 5px;}

    .contTrans ul li dt label.tservicest{width: 160px;margin-right: 5px;}
    .contTrans ul li dd label.tservicestName{width: 395px;font-weight: normal;}

    .contInt ul li dt label.firstColumn{width: 140px;margin-right: 5px;}
    .contInt ul li dt label.secondColumn{width: 125px;margin-right: 5px;}
    .contInt ul li dd label.firstColumnName{width: 130px;margin-right: 5px;font-weight: normal;}
    .contInt ul li dd label.secondColumnName{width: 170px;margin-right: 5px;font-weight: normal;}
    .contInt ul li dd label.fullName{width: 440px;margin-right: 5px;font-weight: normal;}
    .clear{margin: 0;padding: 0;}

    .divTrans .ui-widget-content a,.divInt .ui-widget-content a{color: #026890;text-decoration: underline;}
    .updateRate{margin-left: 15px;}
    input.trans{margin-left: 10px;float: left;}

    .transPage input,.transPage label{float: left;margin-left: 5px;}
    .transPage label{line-height: 20px;padding: 5px;}
    .transWord input,.transWord label{float: left;margin-left: 5px;}
    .transWord label{line-height: 20px;padding: 5px;}
    .contInt ul li.forTransDoc{display: none;}
    #translationbill td.right{text-align: right;}
    #addCommentTransl,#addCommentTransp{cursor: pointer;float: left;margin: 2px;padding: 5px 7px 5px 0;position: relative;}
    #addCommentTransl span,#addCommentTransp span{float: left;margin: 0 4px;}

    dl.options{margin: 10px;float: left;}
    dl.options dd{float: left;}
    dl.options dd label{padding-left:5px;padding-right: 10px; }

    .menutop3{width: 540px;float: left}
    .menutop4{width: 540px;float: left}
    .menutop5{width: 1062px;float: left}
    .menutop7{width: 1150px;float: left}
    .menutop6{width: 854px;float: left}
    .invoiceNumber{width: 200px;float: left;line-height: 24px; padding: 0 5px;text-align: center;float: right;}
    .bCommentServDri, .bCommentServInt{float: right;}
    .contTrans ul li dl dd.plusMarginLeft{margin-left: 10px;}
    div.middle{float: left;width: 500px;margin-right: 10px;}
    .hidden{display: none;}
    .contTrans ul li dd.noShowLabel{float: right;color: #e10101;}
    .contInt ul li dd.noShowLabel{float: right;color: #e10101;}
    #formEdit{margin-top: 15px;width:1193px; display: none;}
    a.aRight{float: right;}
    #dialogAvailableApps{overflow: hidden;}
    a.moveUp, a.moveDown{float: left;}
</style>
<script type="text/javascript">

    var idPatient=0;
    var listInvoices = '/billings/billinglog/listInvoices';
    var getDetailsByBilling = '/billings/billinglog/getDetailsByBilling';
    var getInfoAppForBilling = '/billings/billinglog/getInfoAppForBilling';
    var listMoreApps = '/billings/billinglog/listMoreApps';
    var lostInvoice = '/billings/billinglog/lostInvoice';
    var deleteInvoice = '/billings/billinglog/deleteInvoice';
    var saveBilling = '/billings/billingconfirm/saveBilling';
    var printInvoice = '/billings/billinglog/reportBilling';
    var updateRatesInsurance = '/billings/billinglog/updateRatesInsurance';
    var getRatesInsurance = '/billings/billinglog/getRatesInsurance';

    $(document).ready(function() {

        $("#invoiceDate").datepicker({
            dateFormat: "mm/dd/yy"
        });

        $('.printInvoice').button({
            icons: {
                primary: "ui-icon-print"
            },
            text: false
        });

        gridlistInvoices = $('#grid-listInvoices').jqGrid({
            url: listInvoices,
            datatype: "json",
            colNames: ['idBilling', 
                       '<?php echo __('Número de Factura') ?>', 
                       '<?php echo __('Paciente') ?>', 
                       '<?php echo __('Aseguradora') ?>', 
                       '<?php echo __('Tipo de Factura') ?>',
                       'ntype', 
                       '<?php echo __('Fecha de Registro') ?>', 
                       '<?php echo __('Monto') ?>', 
                       'lost', 'total', 
                       '<?php echo __('Acciones') ?>'],
            colModel: [
                {name: 'idBilling', index: 'idBilling', hidden: true, width: 150},
                {name: 'invoice', index: 'invoice', width: 150},
                {name: 'patient', index: 'patient', align: 'left', width: 200},
                {name: 'insurance', index: 'insurance', width: 200, align: 'left'},
                {name: 'type', index: 'type', width: 170, align: "center"},
                {name: 'ntype', index: 'ntype', width: 170, align: "center", hidden: true},
                {name: 'regdate', index: 'regdate', search: true, width: 170, align: 'center'},
                {name: 'amount', index: 'amount', search: true, width: 150, align: 'right'},
                {name: 'isLost', index: 'isLost', search: false, hidden: true, width: 150, align: "right"},
                {name: 'totalpaid', index: 'totalpaid', search: false, hidden: true, width: 150, align: "right"},
                {name: 'actions', index: 'actions', search: true, width: 160, align: "right"},
            ],
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 'auto',
            width: 1166,
            pager: 'grid-listInvoices-paper',
            sortname: 'fechaRegistro',
            viewrecords: true,
            sortorder: "asc",
            toolbar: [true, "top"],
            afterInsertRow: function(row_id, data_id) {

                edit = "<a href='#' type='" + data_id.ntype + "' idBilling='" + row_id + "' class='editInvoice'><?php echo __('Editar Factura') ?></a>";
                print = "<a href='#' idBilling='" + row_id + "' class='printInvoice'><?php echo __('Imprimir Factura') ?></a>";
                lost="";

                if(data_id.isLost === '0')
                lost = "<a href='#' idBilling='" + row_id + "' class='lostInvoice'><?php echo __('Pago Extraviado') ?></a>";
                
                delet = "<a href='#' idBilling='" + row_id + "' class='deleteInvoice'><?php echo __('Eliminar Factura') ?></a>";

                $("#grid-listInvoices").jqGrid('setRowData', row_id, {actions: edit + print + lost + delet});

            },
            gridComplete: function() {

                $('.editInvoice').button({
                    icons: {
                        primary: "ui-icon-pencil"
                    },
                    text: false
                });
                $('.printInvoice').button({
                    icons: {
                        primary: "ui-icon-print"
                    },
                    text: false
                });
                $('.lostInvoice').button({
                    icons: {
                        primary: "ui-icon-alert"
                    },
                    text: false
                });
                $('.deleteInvoice').button({
                    icons: {
                        primary: "ui-icon-close"
                    },
                    text: false
                });
                $('.historyInvoice').button({
                    icons: {
                        primary: "ui-icon-clock"
                    },
                    text: false
                });
            }
        });

        $('#okFormLogBilling').button({
            icons: {
                primary: 'ui-icon-check'
            }
        });

        $('#saveBillingTransp').click(function() {
            actionInvoice = 1;
            $("#detailsbilltransl").html('');
            $("#detailsbilltranslfoot").html('');
            var stringData = '';
            var arrayAppIds = [];


            for (i = 0; i < idAppsTransp.length; i++) {
                if ($('#enableTransp' + idAppsTransp[i]).val() > 0) {
                    stringData += '&' + $('#formTrans' + idAppsTransp[i]).serialize();
                    arrayAppIds.push(idAppsTransp[i]);
                }
            }
            stringData = "invoiceDate=" + $('#invoiceDate').val() + '&idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "save=false&idPatients=" + idPatient + "&type=1&" + stringData;
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                      data= response.data;
                    $("#detailsbilltransl").html(data.table);
                    $("#detailsbilltranslfoot").html(data.totalTable);
                }
            });
            $("#divbillSave").dialog("open");
        });

         $('#saveBillingTransl').click(function() {
            actionInvoice = 2;
            $("#detailsbilltransl").html('');
            $("#detailsbilltranslfoot").html('');
            var stringData = '';
            var arrayAppIds = [];
            for (i = 0; i < idAppsTransl.length; i++) {
                if ($('#enableTransl' + idAppsTransl[i]).val() > 0) {
                    stringData += '&' + $('#formInt' + idAppsTransl[i]).serialize();
                    arrayAppIds.push(idAppsTransl[i]);
                }
            }
            stringData = "save=false&invoiceDate=" + $('#invoiceDate').val() + '&idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "idPatient=" + idPatient + "&type=2&" + stringData + "&ajax=ajax";
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                    data=response.data;
                    $("#detailsbilltransl").html(data.table);
                    $("#detailsbilltranslfoot").html(data.totalTable);
                }
            });
            $("#divbillSave").dialog("open");
        });

        $("#updateRate,#updateDriverRate,#updateInterpreterRate").button();

        $("#ratioInsurance").live("click", function() {
            
            var tipoCiudad = $("#tipociudad2").val();
            var dataString = "idInsuranceCarrier=" + idInsuranceCarrier + "&tipociudad=" + tipoCiudad ;
            $.ajax({
                type: "POST",
                url: getRatesInsurance,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                  if(evalResponse(response))  {
                      
                      insurancerates = response.data;

                    $("#idInsuranceRates").val(insurancerates.idInsuranceRates);
                    
                    $("#noShowIntMedicalRegular").val(insurancerates.noShowIntMedicalRegular);
                    $("#noShowIntMedicalRare").val(insurancerates.noShowIntMedicalRare);
                    $("#noShowIntMedicalOther").val(insurancerates.noShowIntMedicalOther);

                    $("#noShowIntLegalRegular").val(insurancerates.noShowIntLegalRegular);
                    $("#noShowIntLegalRare").val(insurancerates.noShowIntLegalRare);
                    $("#noShowIntLegalOther").val(insurancerates.noShowIntLegalOther);

                    $("#noShowIntMedicalRegularMin").val(insurancerates.noShowIntMedicalRegularMin);
                    $("#noShowIntMedicalRareMin").val(insurancerates.noShowIntMedicalRareMin);
                    $("#noShowIntMedicalOtherMin").val(insurancerates.noShowIntMedicalOtherMin);

                    $("#noShowIntLegalRegularMin").val(insurancerates.noShowIntLegalRegularMin);
                    $("#noShowIntLegalRareMin").val(insurancerates.noShowIntLegalRareMin);
                    $("#noShowIntLegalOtherMin").val(insurancerates.noShowIntLegalOtherMin);

                    $("#MedicalRegular").val(insurancerates.MedicalRegular);
                    $("#MedicalRare").val(insurancerates.MedicalRare);
                    $("#MedicalOther").val(insurancerates.MedicalOther);
                    $("#LegalRegular").val(insurancerates.LegalRegular);
                    $("#LegalOther").val(insurancerates.LegalOther);
                    $("#LegalRare").val(insurancerates.LegalRare);
                    $("#conferenceCallRegular").val(insurancerates.conferenceCallRegular);
                    $("#conferenceCallOther").val(insurancerates.conferenceCallOther);
                    $("#conferenceCallRare").val(insurancerates.conferenceCallRare);
                    $("#travelTime").val(insurancerates.travelTime);
                    $("#travelMiles").val(insurancerates.travelMiles);
                    $("#ambulatoryRate").val(insurancerates.ambulatoryRate);
                    $("#ambulatoryWaitingTime").val(insurancerates.ambulatoryWaitingTime);
                    $("#minimumFlatRateRTAmbulatory").val(insurancerates.minimumFlatRateRTAmbulatory);
                    $("#minimumFlatRateOWAmbulatory").val(insurancerates.minimumFlatRateOWAmbulatory);
                    $("#ambulatoryNoShow").val(insurancerates.ambulatoryNoShow);
                    $("#wheelChairLoadedMiles").val(insurancerates.wheelChairLoadedMiles);
                    $("#wheelChairLiftBaseRate").val(insurancerates.wheelChairLiftBaseRate);
                    $("#wheelChairWaitingTime").val(insurancerates.wheelChairWaitingTime);
                    $("#wheelChairCarAssitance").val(insurancerates.wheelChairCarAssitance);
                    $("#minimumFlatRateRTWheelChair").val(insurancerates.minimumFlatRateRTWheelChair);
                    $("#minimumFlatRateOWWheelChair").val(insurancerates.minimumFlatRateOWWheelChair);
                    $("#wheelChairNoShow").val(insurancerates.wheelChairNoShow);
                    $("#stretcherLoadedMiles").val(insurancerates.stretcherLoadedMiles);
                    $("#stretcherLiftBaseRate").val(insurancerates.stretcherLiftBaseRate);
                    $("#stretcherWaitingTime").val(insurancerates.stretcherWaitingTime);
                    $("#minimumFlatRateRTStretcher").val(insurancerates.minimumFlatRateRTStretcher);
                    $("#minimumFlatRateOWStretcher").val(insurancerates.minimumFlatRateOWStretcher);
                    $("#stretcherNoShow").val(insurancerates.stretcherNoShow);
                    $("#earlyPickUpS").val(insurancerates.earlyPickUpS);
                    $("#lateDropOffS").val(insurancerates.lateDropOffS);
                    $("#weekEndFeeS").val(insurancerates.weekEndFeeS);
                    $("#earlyPickUpW").val(insurancerates.earlyPickUpW);
                    $("#lateDropOffW").val(insurancerates.lateDropOffW);
                    $("#weekendFeeW").val(insurancerates.weekendFeeW);
                    $("#earlyPickUpA").val(insurancerates.earlyPickUpA);
                    $("#lateDropOffA").val(insurancerates.lateDropOffA);
                    $("#weekendFeeA").val(insurancerates.weekendFeeA);
                    $("#stretcherAssistanceS").val(insurancerates.stretcherAssistanceS);
                    $("#gasolinesurcharge").val(insurancerates.gasolinesurcharge);
                    }
                }
            });

            $("#divratioinsurance").dialog("open");
        });

        $("#grid-listInvoices").on('click', 'a.editInvoice', function() {
            $(this).parents('.show_content').find('.tableCont').slideUp();
            $('.tableShow').show();
            $('.tableHide').hide();
            $('#show_summary').hide();
            dataString='idBilling=' + $(this).attr('idBilling');
            idBilling = $(this).attr('idBilling');
            typeBilling = $(this).attr('type');
              html='';  
                idAppsTransl = [];
            idAppsTransp = [];
            getInformation(dataString);
            milesBilling = this.frate;


        });

        $("#grid-listInvoices").on('click', 'a.lostInvoice', function() {

            current_tr_billing_log = $(this).parents('tr');
            dataString = 'idBilling=' + $(this).attr('idBilling');
            confirmBox(
                    '<?php echo __('Extraviar eel pago de esta factura') ?>',
                    '<?php echo __('Confirmación de Pago Extraviado') ?>', function(response) {
                if (response) {
                    $.ajax({
                        type: "POST",
                        url: lostInvoice,
                        data: dataString,
                        dataType: 'json',
                        beforeSend: function() {
                            $("#loading").dialog('open');
                        },
                        success: function(response) {
                            if (response.msg == 'OK') {
                                current_tr_billing_log.find('a.lostInvoice').remove();
                                current_tr_billing_log.addClass('gradeX');
                                msgBox('<?php echo __('Factura Guardada') ?>', '<?php echo __('Alerta') ?>');
                            } else {
                                msgBox('<?php echo __('Error') ?>', '<?php echo __('Alerta') ?>');
                            }
                        },
                        complete: function() {
                            $("#loading").dialog('close');
                        }
                    });
                }
            });
        });
        
        $("#grid-listInvoices").on('click', 'a.printInvoice', function() {

            idBilling = $(this).attr('idBilling');
            
            var data = new Object();
            data.idBilling= $(this).attr('idBilling');
            
            
            $('#divReport').iReportingPlus('option', 'dynamicParams', data);
            $('#divReport').iReportingPlus('generateReport', 'pdf');
//             $.ajax({
//                        type: "POST",
//                        url: printInvoice,
//                        data: idBilling,
//                        dataType: 'json',
//                        beforeSend: function() {
//                            $("#loading").dialog('open');
//                        },
//                        success: function(response) {
//                    
//                          if(evalResponse(response))  
//                            if (response.msg === 'OK') {
//                                current_tr_billing_log.find('a.lostInvoice').remove();
//                                current_tr_billing_log.addClass('gradeX');
//                                msgBox('Invoice saved.', 'Alert');
//                            } else {
//                                msgBox('Error', 'Alert');
//                            }
//                        
//                        },
//                        complete: function() {
//                            $("#loading").dialog('close');
//                        }
//                    });

    
            
        });
        
        $("#grid-listInvoices").on('click', 'a.historyInvoice', function() {

            idBilling = $(this).attr('idBilling');
//            extraUrl =$.URLEncode('/index.php/admin/billing/printHistorialBillingChecksByInvoice?publicKey={/literal}{$publicKey}{literal}&ajax=xml&idBilling='+idBilling);
            window.open(url);
        });
        
        $("#grid-listInvoices").on('click', 'a.deleteInvoice', function() {

            currentTrDelete = $(this).parents('tr').get(0);
            idBilling = $(this).attr('idBilling');
            dataString = 'idBilling=' + idBilling;
            confirmBox(
                    '<?php echo __('¿Eliminar esta Factura?') ?>',
                    '<?php echo __('Confirmación de eliminación') ?>', function(response) {
                if (response == true) {
                    $.ajax({
                        type: "POST",
                        url: deleteInvoice,
                        data: dataString,
                        dataType: 'json',
                        beforeSend: function() {
                            $("#loading").dialog('open');
                        },
                        success: function(response) {
                            if (response.msg == 'OK') {
                                $("#grid-listInvoices").trigger('reloadGrid');
                                msgBox('<?php echo __('Factura Eliminada') ?>', '<?php echo __('Alerta') ?>');
                            } else {
                                msgBox('<?php echo __('Error') ?>', '<?php echo __('Alerta') ?>');
                            }
                        },
                        complete: function() {
                            $("#loading").dialog('close');
                        }
                    });
                }
            });

        });
        
        $('.tableShow').hide();
        $('.tableHide').hide();
        $('.tableShow').button({
            icons: {
                primary: 'ui-icon-triangle-1-w'
            },
            text: false
        });
        $('.tableHide').button({
            icons: {
                primary: 'ui-icon-triangle-1-s'
            },
            text: false
        });

        $('.tableHide').live('click', function() {
            $(this).parents('.show_content').find('.tableCont').slideUp();
            $(this).parents('.menutop').find('.tableShow').css('display', 'inline');
            $(this).css('display', 'none');
        });
        $('.tableShow').live('click', function() {
            $(this).parents('.show_content').find('.tableCont').slideDown();
            $(this).parents('.menutop').find('.tableHide').css('display', 'inline');
            $(this).css('display', 'none');
        });

        $('#actualizarApp').click(function() {
            $('.divTrans' + idAppointment + ' ul li.clear, .divTrans' + idAppointment + ' ul li.liLiftBase, .divTrans' + idAppointment + ' ul li.liCassistance').remove();
            roundTrip = $("input[name='roundtrip']:checked").val() * 1;
            txtExtra1 = '';
            txtExtra2 = '';
            txtOut = '';
            if ($('#lbase').is(':checked')) {
                txtExtra1 = '<li class="liLiftBase">\n\
                                <dl>\n\
                                    <dd><label for="lbase' + idAppointment + '">Lift Base:</label></dd>\n\
                                    <dd><input size="5" class="ui-widget ui-widget-content ui-corner-all" name="lbase' + idAppointment + '" id="lbase' + idAppointment + '" type="text" value="' + roundTrip + '"/></dd>\n\
                                </dl>\n\
                                <div class="clear"></div>\n\
                            </li>';
            }
            if ($('#cassistance').is(':checked')) {
                txtExtra2 = '<li class="liCassistance">\n\
                                <dl>\n\
                                    <dd><label>[CAR]Assitence</label></dd>\n\
                                </dl>\n\
                                <div class="clear"></div>\n\
                            </li>';
            }
            switch ($("input[name='services']:checked").val()) {
                case 'A':
                    txtOut = txtExtra1;
                    break;
                case 'W':
                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', 'Car ');
                    break;
                case 'ST':
                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', '');
                    break;
                default:
                    break;
            }
            $('.divTrans' + idAppointment + ' ul').append(txtOut + '<li class="clear"></li>');

            if ($('#frate').val() * 1 > 0) {
                $('#tmiles' + idAppointment).val($('#frate').val());
                $('#flateRateBilling' + idAppointment).val($('#frate').val());

                $('#haveFlatRate' + idAppointment).attr('checked', true);

            } else {
                $('#tmiles' + idAppointment).val($('#milesBilling' + idAppointment).val());
                $('#haveFlatRate' + idAppointment).removeAttr('checked');
            }
            $('.checkbox').buttonset();
         });

        $('#dlg_show_summary_billing_log').dialog({
            autoOpen: false
                    , modal: true
                    , title: '<?php echo __('Resumen de Facturación') ?>'
        });

        $('#show_summary').button({
            icons: {
                primary: 'ui-icon-calculator'
            }
            , text: false
        }).click(function() {
            $('#dlg_show_summary_billing_log').dialog('open');
        });

        $('#summary_button').button({
            icons: {
                primary: 'ui-icon-extlink'
            }
        }).click(function() {
//            extraUrl = '/index.php/admin/billing/fnGetSummaryTotal?publicKey={/literal}{$publicKey}{literal}&ajax=xml&'+'date_ini='+$('#summary_date_ini').val()+'&date_end='+$('#summary_date_end').val();
        });

        $('#summary_date_ini, #summary_date_end').datepicker({
            dateFormat: "mm/dd/yy"
        });

        $('.savComment').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });

        $('#saveBillingTransp').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });

        $('#saveBillingTransl').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });

        getInformation = function(dataString) {
          
            $.ajax({
                type: "POST",
                url: getInfoAppForBilling,
                data: dataString,
                dataType: 'json',
                async: false,
                success: function(response) {
                    if (response.msg === 'OK') {
                        var data = response.data;
                        $.each(data, function() {
                           
                            driverSurcharge = '';  
                            driverEarly = ''; 
                            driverLate = ''; 
                            driverWeekend = ''; 
                            driverWait = '';
                             wthour = '';
                             wtmin = '';
                             driverOther = '';
                            otherValue = '';
                            interNormalDocTrans = '';
                            
                            idApp = this.idAppointment;
                            if(this.idBilling > 0){
                           
                            $.ajax({
                                type: "POST",
                                url: getDetailsByBilling,
                                data: 'idBillingApp=' + this.idBillingApp,
                                dataType: 'json',
                                async: false,
                                success: function(response) {
                                    if (response.msg === 'OK') {

                                        datadetail = response.details;
                                        $.each(datadetail, function() {
                                            if (this.detailType === 'driverSurcharge')
                                               driverSurcharge = 'checked';
                                           
                                            if (this.detailType === 'driverEarly')
                                                driverEarly = 'checked';
                                           
                                              
                                            if (this.detailType === 'driverLate')
                                                driverLate = 'checked';
                                            
                                               
                                            if (this.detailType === 'driverWeekend')
                                                driverWeekend = 'checked';
                                            
                                            if (this.detailType === 'driverWait') {
                                                driverWait = 'checked';
                                                wthour = parseInt(this.qty);
                                                wtmin = parseInt((this.qty - wthour) * 60);
                                            }

                                            if (this.detailType === 'driverOther') {
                                                driverOther = 'checked';
                                                otherValue = this.ratio;
                                            } 
                                            if (this.detailType === 'interNormalDocTrans') {

                                                interNormalDocTrans = this.transDoc;
                                                if (this.transDoc == 'Pages') {
                                                    txtpage = this.qty;
                                                    txtpricePage = this.ratio;
                                                } else {
                                                    txtword = this.qty;
                                                    txtpriceWord = this.ratio;
                                                }

                                            }
                                        });
                                    }
                                }
                            });
                            
                            buttonsMoveUpDown = ' ';
                            buttonCancel = ' ';
                            }
                         
                            
                            noShowDriver = '';
                             if (this.noShowDriver == 1) {
                                noShowDriver = '<dd class="noShowLabel" id="noShowDriver' + this.idAppointment + '">NO SHOW</dd>';
                            }
                            roundTrip = this.typetravel == 'Round Trip' ? 2 : 1;
                            txtExtra1 = '';
                            txtExtra2 = '';
                            txtOut = '';
                            //console.log(this);
                            if (this.lbase == 'on') {
                                txtExtra1 = '<li class="liLiftBase">\n\
                                                <dl>\n\
                                                    <dd>Lift Base:</dd>\n\
                                                    <dd><input size="5" class="ui-widget ui-widget-content ui-corner-all" name="lbase' + this.idAppointment + '" id="lbase' + this.idAppointment + '" type="text" value="' + roundTrip + '"/></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                            }
                            if (this.cassistance == 'on') {
                                txtExtra2 = '<li class="liCassistance">\n\
                                                <dl>\n\
                                                    <dd>[CAR]Assitence</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                            }

                            switch (this.typeservice) {
                                case 'Ambulatory':
                                    txtOut = txtExtra1;
                                    break;
                                case 'Wheelchair':
                                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', 'Car ');
                                    break;
                                case 'Stretcher':
                                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', '');
                                    break;
                                default:
                                    break;
                            }

                            if (this.frate > 0) {
                                milesRate = 'Flate Rate';
                                isCheckedFlateRate = ' checked ';
                                milesFlateRate = this.frate;
                            } else {
                                milesRate = 'Miles';
                                isCheckedFlateRate = '';
                                milesFlateRate = this.miles;
                            }

                            interpretationTimeFrom = '';
                            interpretationTimeTo = '';
                            interpretationTimeTotalTime = '';
                            travelFrom = '';
                            travelTo = '';
                            totalMileage = '';
                            travelTotalTime = '';
                            if (this.idTranslationSummary > 0) {
                                interpretationTimeFrom = this.interpretationTimeFrom;
                                interpretationTimeTo = this.interpretationTimeTo;
                                interpretationTimeTotalTime = this.interpretationTimeTotalTime;
                                travelFrom = this.travelFrom;
                                travelTo = this.travelTo;
                                totalMileage = this.totalMileage;
                                travelTotalTime = this.travelTotalTime;
                            }

                            
                            noShowInterpreter = '';
                            if (this.noShowInterpreter == 1) {
                                noShowInterpreter = '<dd class="noShowLabel" id="noShowInterpreter' + this.idAppointment + '">NO SHOW</dd>';
                            }

                            switch (this.typeservicesInterpreter) {
                                case 'Conference Call':
                                    typeServicesInterpreter = 'Conference Call';
                                    addInt = '<li>\n\
                                                <dl>\n\
                                                    <dt><label>End Time:</label></dt>\n\
                                                    <dd>' + this.dropTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    summ = '<li>\n\
                                                <dl>\n\
                                                    <dt><label>Interpretation Time</label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>From:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeFrom' + this.idAppointment + '" id="interpretationTimeFrom' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeFrom + '"/></dt>\n\
                                                    <dt><label>To:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeTo' + this.idAppointment + '" id="interpretationTimeTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTo + '"/></dt>\n\
                                                    <dt><button class="calctimeint" idAppointment="' + this.idAppointment + '" type="button">Total Time</button>Total Time:</dt>\n\
                                                    <dt><input type="text" readonly name="interpretationTimeTotalTime' + this.idAppointment + '" id="interpretationTimeTotalTime' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTotalTime + '" /></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    break;
                                case 'Document Translation':
                                    typeServicesInterpreter = 'Document Translation';
                                    addInt = '';
                                    summ = '<li class="forTransDoc' + this.idAppointment + ' hidden">\n\
                                                <dl>\n\
                                                    <dt><label class="firstColumn">	Trans Page:</label></dt>\n\
                                                    <dd class="fullName transDoc">\n\
                                                        <input type="radio" name="typeTransDoc' + this.idAppointment + '" id="typeTransPage' + this.idAppointment + '" value="Pages" validate="{required: true, minlength:1}"/>\n\
                                                        <label for="typeTransPage' + this.idAppointment + '">\n\
                                                            Trans Page\n\
                                                        </label>\n\
                                                    </dd>\n\
                                                    <dd class="fullName transPage">\n\
                                                        <label for="txtpage"># Pages:</label>\n\
                                                        <input  size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpage' + this.idAppointment + '" id="txtpage' + this.idAppointment + '" />\n\
                                                        <label for="txtpricePage">Price for Page:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpricePage' + this.idAppointment + '" id="txtpricePage' + this.idAppointment + '" />\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="forTransDoc' + this.idAppointment + ' hidden">\n\
                                                <dl>\n\
                                                    <dt><label class="firstColumn">&nbsp;</label></dt>\n\
                                                    <dd class="transDoc fullName">\n\
                                                        <input type="radio" name="typeTransDoc' + this.idAppointment + '" id="typeTransWord' + this.idAppointment + '" value="Words"/>\n\
                                                        <label for="typeTransWord' + this.idAppointment + '">\n\
                                                            Trans Word\n\
                                                        </label>\n\
                                                    </dd>\n\
                                                    <dd class="transWord fullName">\n\
                                                        <label for="txtword"># Words:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtword' + this.idAppointment + '" id="txtword' + this.idAppointment + '" />\n\
                                                        <label for="txtpriceWord">Price for Word:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpriceWord' + this.idAppointment + '" id="txtpriceWord' + this.idAppointment + '" />\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    break;
                                case 'Interpreting':
                                    typeServicesInterpreter = 'Interpreting';
                                    addInt = '<li>\n\
                                                <dl>\n\
                                                    <dt><label>End Time:</label></dt>\n\
                                                    <dd>' + this.dropTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>Contact:</label></dt>\n\
                                                    <dd>' + this.calias + ': ' + this.caddress + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    summ = '<li>\n\
                                                <dl>\n\
                                                    <dt><label>Interpretation Time</label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>From:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeFrom' + this.idAppointment + '" id="interpretationTimeFrom' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeFrom + '"/></dt>\n\
                                                    <dt><label>To:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeTo' + this.idAppointment + '" id="interpretationTimeTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTo + '"/></dt>\n\
                                                    <dt><button class="calctimeint" idAppointment="' + this.idAppointment + '" type="button">Total Time</button>Total Time:</dt>\n\
                                                    <dt><input type="text" readonly name="interpretationTimeTotalTime' + this.idAppointment + '" id="interpretationTimeTotalTime' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTotalTime + '" /></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>Travel(City to City)</label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>From:</label></dt>\n\
                                                    <dt><input value="' + travelFrom + '" type="text" name="travelFrom' + this.idAppointment + '" id="travelFrom' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dt><label>To:</label></dt>\n\
                                                    <dt><input value="' + travelTo + '" type="text" name="travelTo' + this.idAppointment + '" id="travelTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label>Total Mileage:</label></dt>\n\
                                                    <dt><input value="' + totalMileage + '" type="text" name="totalMileage' + this.idAppointment + '" id="totalMileage' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dt><button class="calctraveltimeint" idAppointment="' + this.idAppointment + '" type="button">Total Time</button>Total Time:</dt>\n\
                                                    <dt><input value="' + travelTotalTime + '" type="text" name="travelTotalTime' + this.idAppointment + '" id="travelTotalTime' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>';
                                    break;
                                default:
                                    addInt = '';
                                    summ = '';
                                    break;
                            }
                          
                            $('#invoiceDate').val(this.d_current_date);
                            
                          
                                //console.log('Translation :'+this.idAppointment);
                                if (typeBilling == '1') {
                                   
                                    billingTranspId = this.idAppointment;
                                } else {
                                    billingTranspId = -1;
                                }
                           

                            
                           
                                //console.log('Translation :'+this.idAppointment);
                                 if (typeBilling == '2') {
                                    billingTranslId = this.idAppointment;
                                } else {
                                    billingTranslId = -1;
                                }
                           

                            switch (this.tipociudad) {
                                case 'florida':
                                    region = 'Florida';
                                    break;
                                case 'georgia':
                                    region = 'Georgia';
                                    break;
                                case 'other':
                                    region = 'Out state'
                                    break;
                                default:
                                    region = '<b class="balert">' + this.tipociudad + '</b>';
                                    break;
                            }

                             htmlpatient = '<li>\n\
                                <input id="enableTransl' + this.idAppointment + '" name="enableTransl' + this.idAppointment + '" type="hidden" value="' + billingTranslId + '" />\n\
                                <input id="enableTransp' + this.idAppointment + '" name="enableTransp' + this.idAppointment + '" type="hidden" value="' + billingTranspId + '" />\n\
                                <div class="menutop ui-widget ui-widget-content ui-corner-all">\n\
                                     <a class="moveUp" idAppointment="' + this.idAppointment + '" title="Move Up">Move Up</a><a class="moveDown" idAppointment="' + this.idAppointment + '" title="Move Down">Move Down </a>\n\
                                    <div class="menutop2 menutop5 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                        <h4><?php echo __('Información de la Cita'); ?> : ' + this.idAlias + '</h4>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <a class="cancelBilling aRight" idAppointment="' + this.idAppointment + '" title="Delete Appointment">Delete Appointment</a>\n\
                                             <a class="none returnBilling aRight" idAppointment="' + this.idAppointment + '" title="Return Appointment"><?php echo __('Retornar Cita'); ?></a>\n\
                                    <a idAppointment="' + this.idAppointment + '" class="editApps aRight"><?php echo __('Editar Cita'); ?></a>\n\
                                    <div class="clear"></div>\n\
                                </div>\n\
                                <div id="contDivBilling' + this.idAppointment + '">\n\
                                <div class="ui-widget ui-widget-content ui-corner-all contApp">\n\
                                    <div class="middle">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Nombre del Paciente'); ?>:</label></dt>\n\
                                                    <dd>' + this.firstName + ' ' + this.lastName + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Autorizado por'); ?>:</label></dt>\n\
                                                    <dd>' + this.appAuthorized + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Fecha del Servicio'); ?>:</label></dt>\n\
                                                    <dd>' + this.appointmentDate + '</dd>\n\
                                                    <dt><label><?php echo __('Hora'); ?>:</label></dt>\n\
                                                    <dd>' + this.appointmentTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Llamado por'); ?>:</label></dt>\n\
                                                    <dd>' + this.whocalls + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Nombre de la Compañía'); ?>:</label></dd>\n\
                                                    <dd>' + this.companyName + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                        </ul>\n\
                                    </div>\n\
                                    <div class="middle">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Región'); ?>:</label></dt>\n\
                                                    <dd>' + region + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Cita'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeapp + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Descripción de la Cita'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeAppointment + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                        </ul>\n\
                                    </div>\n\
                                    <div class="clear"></div>\n\
                                </div>';
                               
                               if(typeBilling==='1')
                               htmltrans = '<form name="formTrans' + this.idAppointment + '" id="formTrans' + this.idAppointment + '" action="" method="POST">\n\
                                <textarea class="hidden" name="commentServDri' + this.idAppointment + '" id="commentServDri' + this.idAppointment + '"></textarea>\n\
                                <input type="hidden" id="milesBilling' + this.idAppointment + '" class="milesBilling" name="milesBilling' + this.idAppointment + '" value="0" />\n\
                                <input type="hidden" id="flateRateBilling' + this.idAppointment + '" class="flateRateBilling" name="flateRateBilling' + this.idAppointment + '" value="' + this.frate + '" />\n\
                                <div class="divTrans divTrans' + this.idAppointment + '">\n\
                                    <div class="menutop ui-widget ui-widget-content  ui-corner-all">\n\
                                        <div class="menutop2 right menutop3 ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                            <h4 style="text-align: center"><?php echo __('Información del Transporte'); ?></h4>\n\
                                            <div class="clear"></div>\n\
                                        </div>\n\
                                        <button type="button" idAppointment="' + this.idAppointment + '" class="bCommentServDri">Bill Transportation</button>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <div class="ui-widget ui-widget-content ui-corner-all contTrans">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Conductor'); ?> 1:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioDriver1" idDriverOneWay="' + this.idDriverOneWay + '">' + this.driverPickUpName + ' ' + this.driverPickUpLastName + '</a>\n\
                                                    </dd>\n\
                                                    ' + noShowDriver + '\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Conductor'); ?> 2:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioDriver2" idDriversDrop="' + this.idDriverRoundTrip + '">' + this.driverDropName + ' ' + this.driverDropLastName + '</a>\n\
                                                    </dd>\n\
                                                    <dd class="checkbox noShowLabel">\n\
                                                        <input type="checkbox" name="haveFlatRate' + this.idAppointment + '" id="haveFlatRate' + this.idAppointment + '"' + isCheckedFlateRate + '/><label for="haveFlatRate' + this.idAppointment + '">Flate Rate</label>\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Servicio'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeservice + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Desde'); ?>:</label></dt>\n\
                                                    <dd>' + this.oalias1 + ': ' + this.oaddress1 + ' ' + this.osuite1 + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Hasta'); ?>:</label></dt>\n\
                                                    <dd>' + this.oalias2 + ': ' + this.oaddress2 + ' ' + this.osuite2 + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Viaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.typetravel + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Millas'); ?>:</label></dt>\n\
                                                    <dd>' + this.miles + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl class="checkbox">\n\
                                                    <dd><input ' + driverSurcharge + ' type="checkbox" name="extra' + this.idAppointment + '[]" id="fs' + this.idAppointment + '" value="fs"/><label for="fs' + this.idAppointment + '">Fuel Surcharge</label></dd>\n\
                                                    <dd><input ' + driverEarly + ' type="checkbox" name="extra' + this.idAppointment + '[]" id="epu' + this.idAppointment + '" value="epu"/><label for="epu' + this.idAppointment + '">Early Pick Up</label></dd>\n\
                                                    <dd><input ' + driverLate + ' type="checkbox" name="extra' + this.idAppointment + '[]"  id="ldo' + this.idAppointment + '" value="ldo"/><label for="ldo' + this.idAppointment + '">Late Drop Off</label></dd>\n\
                                                    <dd><input ' + driverWeekend + ' type="checkbox" name="extra' + this.idAppointment + '[]"  id="wend' + this.idAppointment + '" value="wend"/><label for="wend' + this.idAppointment + '">WEnd</label></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd class="checkbox"><input  type="checkbox" ' + driverWait + ' name="wtime' + this.idAppointment + '" id="wtime' + this.idAppointment + '"/><label for="wtime' + this.idAppointment + '"><?php echo __('Tiempo de Espera'); ?></label></dd>\n\
                                                    <dd class="none wtime plusMarginLeft"><label class="wthour" for="wthour">Hours:</label></dd>\n\
                                                    <dd class="none wtime"><input type="text" value="' + wthour + '" size="5" id="wthour' + this.idAppointment + '" name="wthour' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="none wtime"><label class="wtmin" for="wtmin">Minutes:</label></dd>\n\
                                                    <dd class="none wtime"><input type="text" value="' + wtmin + '" size="5" id="wtmin' + this.idAppointment + '" name="wtmin' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd><label id="lMiles' + this.idAppointment + '" class="lInput lMiles">' + milesRate + '</label></dd>\n\
                                                    <dd><input value="' + milesFlateRate + '" type="text" size="5" id="tmiles' + this.idAppointment + '" name="tmiles' + this.idAppointment + '" class="inputMiles ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            ' + txtOut + '\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd class="checkbox"><input  type="checkbox" ' + driverOther + ' name="other' + this.idAppointment + '" id="other' + this.idAppointment + '"/><label for="other' + this.idAppointment + '"><?php echo __('Otro'); ?></label></dd>\n\
                                                    <dd class="none ddOther"><input type="text" value="' + otherValue + '" size="5" id="otherValue' + this.idAppointment + '" name="otherValue' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                        </ul>\n\
                                        <label id="statustransp' + this.idAppointment + '"></label>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                </div>\n\
                                </form>';
                                if(typeBilling==='2')  htmltrasl='<form name="formInt' + this.idAppointment + '" id="formInt' + this.idAppointment + '" action="" method="POST">\n\
                                    <textarea class="hidden" name="commentServInt' + this.idAppointment + '" id="commentServInt' + this.idAppointment + '"></textarea>\n\
                                    <div class="divInt divInt' + this.idAppointment + '">\n\
                                    <div class="menutop ui-widget ui-widget-content  ui-corner-all">\n\
                                        <div class="menutop2 menutop4 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                            <h4 style="text-align: center">Interpretation Info</h4>\n\
                                            <div class="clear"></div>\n\
                                        </div>\n\
                                        <button type="button" idAppointment="' + this.idAppointment + '" class="bCommentServInt"><?php echo __('Facturación de Traducción'); ?></button>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <div class="ui-widget ui-widget-content ui-corner-all contInt">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Intérprete'); ?>:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioInterpreter" idInterpreter="' + this.idInterpreters + '" >' + this.interpreterName + ' ' + this.interpreterLastName + '</a>\n\
                                                    </dd>\n\
                                                    ' + noShowInterpreter + '\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Lenguaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.languages + '</dd>\n\
                                                    <dt><label><?php echo __('Tipo de Lenguaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.typelanguage + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Traducción'); ?>:</label></dt>\n\
                                                    <dd class="typeServicesInt' + this.idAppointment + '">' + this.typeservicesInterpreter + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            ' + addInt + summ + '\n\
                                        </ul>\n\
                                        <label id="statustransl' + this.idAppointment + '"></label>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                </div>\n\
                                </form>';   
                                        
                       if(this.typeBilling > 0 )
                            $('.invoiceNumber').text('<?php echo __('Número de Factura'); ?>: '+this.invoiceNumber);
                            if(typeBilling === '1' ){
                          html+= htmlpatient+htmltrans+'</div><div class="clear"></div></li><br>' ;
                          $('#saveBillingTransp').show();
                          $('#saveBillingTransl').hide();
                          idAppsTransp.push(this.idAppointment);
                       
                          }
                          
                            if(typeBilling === '2' ){
                          html+=htmlpatient + htmltrasl+ '</div><div class="clear"></div></li><br>';
                          $('#saveBillingTransl').show();
                          $('#saveBillingTransp').hide();
                          idAppsTransl.push(this.idAppointment);
                          }
                        

//                            if (this.billingtransp == 0) {
//                                $("#statustransp" + this.idAppointment).text('Not able to register again this bill, Transportation');
//                                $("#savebilltransp" + this.idAppointment).attr('disabled', true);
//                                $("#statusbillingtransp" + this.idAppointment).text('Not able to register again this bill, Transportation');
//                                $(".divTrans" + this.idAppointment + " input").attr('disabled', true);
//                                /*
//                                 //Ajax nomaz sera -.-
//                                 dataString = 'type=1'+'&idAppointment='+this.idAppointment;
//                                 $.ajax({
//                                 type: "POST",
//                                 url: '/index.php/admin/billing/getBillingRecord?ajax=ajax',
//                                 data: dataString ,
//                                 dataType:'json',
//                                 async: false,
//                                 success: function(rpta) {
//                                 
//                                 }
//                                 });*/
//                            } else {
//                                idAppsTransp.push(this.idAppointment);
//                                $("#statustransp" + this.idAppointment).text('');
//                                $("#savebilltransp" + this.idAppointment).removeAttr('disabled');
//                                $("#statusbillingtransp" + this.idAppointment).text('');
//                                $(".divTrans" + this.idAppointment + " input").removeAttr('disabled');
//                            }
//
//                            if (this.billinginterp == 0) {
//                                $("#statustransl" + this.idAppointment).text('Not able to register again this bill, Translation');
//                                $("#savebilltransl" + this.idAppointment).attr('disabled', true);
//                                $("#statusbillingtransl" + this.idAppointment).text('Not able to register again this bill, Translation');
//                                $(".divInt" + this.idAppointment + " input").attr('disabled', true);
//                                //Ajax?...
//                            } else {
//                                idAppsTransl.push(this.idAppointment);
//                                $("#statustransl" + this.idAppointment).text('');
//                                $("#savebilltransl" + this.idAppointment).removeAttr('disabled');
//                                $("#statusbillingtransl" + this.idAppointment).text('');
//                                $(".divInt" + this.idAppointment + " input").removeAttr('disabled');
//                            } 
                      
                            $('ul#liApps').html(html);
                            $('[name=other' + this.idAppointment + ']').trigger('change');   
                            $('#formEdit').show();
                        
                           
                            $('.cancelBilling').button({
                                icons: {
                                    primary: 'ui-icon-cancel'
                                },
                                text: false
                            });
                             $('.returnBilling').button({
                                        icons:{
                                            primary: 'ui-icon-arrowreturnthick-1-e'
                                        },
                                        text: false
                                    });
                            $('.addApps').button({
                                icons: {
                                    primary: 'ui-icon-plus'
                                }
                            });

                            $('.moveUp').button({
                                icons: {
                                    primary: 'ui-icon-carat-1-n'
                                },
                                text: false
                            });

                            $('.moveDown').button({
                                icons: {
                                    primary: 'ui-icon-carat-1-s'
                                },
                                text: false
                            });

                            $('#formInt' + this.idAppointment).validate({
                                 submitHandler: function(form) {
                                    alert($(form).serialize());
                                }
                            });
                            
                            $(".calctimeint,.calctraveltimeint").button({
                                icons: {
                                    primary: "ui-icon-clock"
                                },
                                text: false
                            });

                            $(".calctraveltimeint").click(function() {
                                var idAppointment = $(this).attr('idAppointment');
                                var dataString = "totalMileage=" + $('#totalMileage' + idAppointment).val() + "&ajax=ajax";
                                $.ajax({
                                    type: "POST",
                                    url: milesToTime,
                                    data: dataString,
                                    dataType: 'json',
                                    success: function(response)
                                    {
                                        $("#travelTotalTime" + idAppointment).val(response.data);
                                        if ($('#totalMileage' + idAppointment).val() < 40) {
                                            $('#totalMileage' + idAppointment).val('40');
                                        }
                                    }
                                });
                            });

                            $(".calctimeint").click(function() {
                                var idAppointment = $(this).attr('idAppointment');
                                var from = $("#interpretationTimeFrom" + idAppointment).val();
                                var to = $("#interpretationTimeTo" + idAppointment).val();
                                var dataString = "from=" + from + "&to=" + to + "&ajax=ajax";
                                var servicesType = $(".typeServicesInt" + idAppointment).text();
                                //console.log(idAppointment);
                                $.ajax({
                                    type: "POST",
                                    url: calculartiempotrasnc,
                                    data: dataString,
                                    dataType: "json",
                                    success: function(response) {
                                        if (response.msg == "OK") {
                                            switch (servicesType) {
                                                case "Conference Call":
                                                if (response.data < 0.25) {
                                                    msgBox("<?php echo __('Tiempo Calculado'); ?> : " + response.data + " <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Conferencia es: 15 minutos'); ?>", "<?php echo __('Alerta'); ?>");
                                                    $("#interpretationTimeTotalTime" + idAppointment).val("");
                                                } else {
                                                    $("#interpretationTimeTotalTime" + idAppointment).val(response);
                                                }
                                                break;
                                            case "Interpreting":
                                                if (response.data < 1.5) {
                                                    msgBox("<?php echo __('Tiempo Calculado'); ?> : " + response.data + " <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Interpretación es: 1.5 horas'); ?>", "<?php echo __('Alerta'); ?>");
                                                    $("#interpretationTimeTotalTime" + idAppointment).val("");
                                                } else {
                                                    $("#interpretationTimeTotalTime" + idAppointment).val(response.data);
                                                }
                                                break;
                                            default:
                                                msgBox("<?php echo __('Document Translation u otro tipo de servicio no requiere Summary Translation.'); ?>"
                                                        + "<?php echo __('Envie un e-mail al administrador del sistema para solucionar este error.'); ?>","<?php echo __('Alerta'); ?>");
                                                break;
                                            }
                                        }
                                    }
                                });
                            });

                            $('#formTrans' + this.idAppointment).validate({
                                submitHandler: function(form) {
                                    alert($(form).serialize());
                                }
                            });

                            $('[name=wtime' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('.wtime').css('display', 'inline');
                                } else {
                                    $(this).parents('dl').find('.wtime').css('display', 'none');
                                }
                                return false;
                            });

                            if (this.app_wtime == 1) {
                                $('[name=wtime' + this.idAppointment + ']').attr('checked', true).trigger('change');
                                $('input[name=wthour' + this.idAppointment + ']').val(this.app_wtime_hours);
                                $('input[name=wtmin' + this.idAppointment + ']').val(this.app_wtime_minutes)
                            }

                            $('[name=haveFlatRate' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('form').find('.lMiles').text('Flate Rate');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.flateRateBilling').val()
                                            );
                                } else {
                                    $(this).parents('form').find('.lMiles').text('Miles');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.milesBilling').val()
                                            );
                                }
                                return false;
                            });

                            $('[name=other' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'block');
                                } else {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'none');
                                }
                                return false;
                            });
                              $('[name=other' + this.idAppointment + ']').trigger('change');   
                            
                            $('[name=typeTransDoc' + this.idAppointment + ']').change(function() {
                                if ($(this).val() == 'Pages') {

                                    $(this).parents('ul').find('dd.transPage').css('display', 'block');
                                    $(this).parents('ul').find('dd.transWord').css('display', 'none');
                                }
                                if ($(this).val() == 'Words') {
                                    $(this).parents('ul').find('dd.transWord').css('display', 'block');
                                    $(this).parents('ul').find('dd.transPage').css('display', 'none');
                                }
                                return false;
                            });
                            $('[name=typeTransDoc' + this.idAppointment + ']').val([interNormalDocTrans]);
                            $('[name=typeTransDoc' + this.idAppointment + ']:checked').trigger('change');

                            $('.bCommentServDri, .bCommentServInt').button({
                                icons: {
                                    primary: 'ui-icon-comment'
                                },
                                text: false
                            });
                            $('.editApps').button({
                                icons: {
                                    primary: 'ui-icon-zoomin'
                                },
                                text: false
                            }).click(function() {
                                $('#idAppointmentView').val($(this).attr("idAppointment")).trigger('change');
                                ;
                                $('#dialogAppEdit').dialog('open');
                            });

                            idInsuranceCarrier = this.idInsuranceCarrier;
                            idDriversDrop = this.idDriversDrop;
                            idDriverOneWay = this.idDriverOneWay;
                            idInterpreter = this.idInterpreter;
                            idPatient = this.idPatients;
                            idClaim = this.idClaims;
                            appointmentDate = this.appointmentDate;


                            if (this.idInterpreter > 0) {
                                $('.divInt' + this.idAppointment).css('display', 'block');
                            } else {
                                $('.divInt' + this.idAppointment).css('display', 'none');
                            }

                            if (this.idDriverOneWay > 0) {
                                $('.divTrans' + this.idAppointment).css('display', 'block');
                            } else {
                                $('.divTrans' + this.idAppointment).css('display', 'none');
                            }

                            //Loading this Billing Trans e Int???

                            
                            $("#badjuster").text(this.adjuster);
                            $("#insurance").html("<a id='ratioInsurance'>" + this.insurance + "</a>");
                            //$("#driver1").html("");
                            if (this.idDriversDrop == this.idDriversPickUp) {
                                $("#driver2").html('---');
                            } else {
                                $("#driver2").html("<a id='ratioDriver2' idDriversPickUp='" + this.idDriversPickUp + "'>" + this.driverDropName + " " + this.driverDropLastName + "</a>");
                            }
                            //$("#languages").text(this.languages);
                            //$("#interpreter").html("");

                            if (this.typeservices == "W") {
                                $("#w").show();
                                typeservices = 'w';
                            } else if (this.typeservices == "A") {
                                $("#a").show();
                                typeservices = 'a';
                            } else if (this.typeservices == "ST") {
                                $("#st").show();
                                typeservices = 'st';
                            }

                        });
                    }
                }
            });
            $('.checkbox,.transDoc').buttonset();
        };
        
        gridlistMoreApps = $('#grid-listMoreApps').jqGrid({
            url: listMoreApps,
            datatype: "json",
            postData: {
                idPatient: function() {
                    return idPatient;
                }                       
                    },
            colNames: ['<?php echo __('ID') ?>', 
                       '<?php echo __('Fecha') ?>', 
                       '<?php echo __('Nombre') ?>', 
                       '<?php echo __('Dirección') ?>', 
                       '<?php echo __('Teléfono') ?>', 
                       '<?php echo __('Nombre') ?>', 
                       '<?php echo __('Dirección') ?>', 
                       '<?php echo __('Teléfono') ?>', 
                       '<?php echo __('Nombre') ?>', 
                       '<?php echo __('Teléfono') ?>',
                       '<?php echo __('Nombre') ?>', 
                       '<?php echo __('Teléfono') ?>',
                       '<?php echo __('Nombre') ?>', 
                       '<?php echo __('Teléfono') ?>',
                       '<?php echo __('Acciones') ?>'],
            colModel: [
                {name: 'idAppointment', index: 'idAppointment', width: 80},
                {name: 'appDate', index: 'appDate', width: 80},
                {name: 'patientName', index: 'patientName', align: 'left', width: 100},
                {name: 'patientAddress', index: 'patientAddress', width: 100, align: 'left'},
                {name: 'patientPhone', index: 'patientPhone', width: 80, align: "center"},
                {name: 'contactName', index: 'contactName', align: 'left', width: 100},
                {name: 'contactAddress', index: 'contactAddress', width: 100, align: 'left'},
                {name: 'contactPhone', index: 'contactPhone', width: 80, align: "center"},
                {name: 'driver1Name', index: 'driver1Name', align: 'left', width: 100},
                {name: 'driver1Phone', index: 'driver1Phone', width: 80, align: "center"},
                {name: 'driver2Name', index: 'driver2Name', align: 'left', width: 100},
                {name: 'driver2Phone', index: 'driver2Phone', width: 80, align: "center"},
                {name: 'interName', index: 'interName', align: 'left', width: 100},
                {name: 'interPhone', index: 'interPhone', width: 80, align: "center"},
                {name: 'actions', index: 'actions', search: true, width: 50, align: "center"}
            ],
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 'auto',
            width: 1166,
            pager: 'grid-listMoreApps-paper',
            sortname: 'appointmentdate',
            viewrecords: true,
            sortorder: "asc",
            toolbar: [true, "top"],
            afterInsertRow: function(row_id, data_id) {

                select = "<a href='#' idAppointment='" + row_id + "' class='selectInvoice'><?php echo __('Seleccione Cita para Facturar') ?></a>";
               

                $("#grid-listMoreApps").jqGrid('setRowData', row_id, {actions: select });

            },
            gridComplete: function() {

                $('.selectInvoice').button({
                    icons: {
                        primary: "ui-icon-plus"
                    },
                    text: false
                });
                
            }
        });
        
        $(".selectInvoice").live('click', function() {
            appIds = $(this).attr('idAppointment');
            idPatients = $(this).attr('idPatients');
            var dataString = 'appIds[]=' + appIds + '&idPatients=' + idPatients;
            // $("#divbilling").slideDown(900);
            $('#loading').dialog('open');
           
            getInformation(dataString);
            $('#loading').dialog('close');
            $('#dialogAvailableApps').dialog('close');
        });
        
        $('.addApps').click(function(){
            
            $("#grid-listMoreApps").trigger("reloadGrid");
                    $('#dialogAvailableApps').dialog('open');
                });
        
        $('.cancelBilling').live('click', function() {
            $('#enableTransp' + $(this).attr('idAppointment')).val('-1');
            $('#enableTransl' + $(this).attr('idAppointment')).val('-1');
            $('#contDivBilling' + $(this).attr('idAppointment')).slideUp();
            $(this).parents('.menutop').find('.menutop2').removeClass('ui-state-hover').addClass('ui-state-error');
            $(this).parents('.menutop').find('.returnBilling').css('display', 'inline');
            $(this).css('display', 'none');
        });
        $('.returnBilling').live('click', function() {
            $('#enableTransp' + $(this).attr('idAppointment')).val($(this).attr('idAppointment'));
            $('#enableTransl' + $(this).attr('idAppointment')).val($(this).attr('idAppointment'));
            $('#contDivBilling' + $(this).attr('idAppointment')).slideDown();
            $(this).parents('.menutop').find('.menutop2').removeClass('ui-state-error').addClass('ui-state-hover');
            $(this).parents('.menutop').find('.cancelBilling').css('display', 'inline');
            $(this).css('display', 'none');
        });


        $('#tipociudad2,#travel').combobox();

        $('.bCommentServDri').live('click', function() {
            var idApp = $(this).attr('idAppointment');
            $('#commentServ').val($('#commentServDri' + idApp).val());
            $('#textareaid').val('#commentServDri' + idApp);
            $('#divComment').dialog('open');
        });
        $('.bCommentServInt').live('click', function() {
            var idApp = $(this).attr('idAppointment');
            $('#commentServ').val($('#commentServInt' + idApp).val());
            $('#textareaid').val('#commentServInt' + idApp);
            $('#divComment').dialog('open');
        });

        $('#formCommentServ').validate({
            submitHandler: function(form) {
                $($('#textareaid').val()).val($('#commentServ').val());
                $('#divComment').dialog('close');
            }
        });

         $('#dialogAvailableApps').dialog({
            autoOpen: false,
            height: "auto",
            width: 1190,
            title:'Available Appointments'
        });
        
           $('#divbillSave').dialog({
            modal: true,
            title:'<?php echo __('Factura Previa') ?>',        
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 'auto',
            width: 550,
            buttons: {
                "<?php echo __('Guardar Factura') ?>": function() {
            
             var stringData = '';
            var arrayAppIds= [];
            var dataString;
           
            if(actionInvoice == 1){
                stringData = '';
                arrayAppIds= [];
                for(i=0;i<idAppsTransp.length;i++){
                    if($('#enableTransp'+idAppsTransp[i]).val()>0){
                        stringData += '&'+$('#formTrans'+idAppsTransp[i]).serialize();
                        arrayAppIds.push(idAppsTransp[i]);
                    }
                }
                stringData = 'idApps='+arrayAppIds.join(',')+stringData;
                dataString = "idPatient="+idPatient+"&type=1&"+stringData +"&ajax=ajax";
            }else{
                
                stringData = '';
                arrayAppIds= [];
                for(i=0;i<idAppsTransl.length;i++){
                    if($('#enableTransl'+idAppsTransl[i]).val()>0){
                        stringData += '&'+$('#formInt'+idAppsTransl[i]).serialize();
                        arrayAppIds.push(idAppsTransl[i]);
                    }
                }
                stringData = 'idApps='+arrayAppIds.join(',')+stringData;
                dataString = "idPatient="+idPatient+"&type=2&"+stringData +"&ajax=ajax";
            }
            dataString+= "&invoiceDate="+$('#invoiceDate').val()+"&save=true&idBilling="+idBilling;
            
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                    if (response.msg =='OK') {

                        $("#divbillSave").dialog("close");
//                        location.reload();

                    }
                }
            });
                }
               
            },
            close: function() {

            }
        });

           $('#divratioinsurance').dialog({
            modal: true,
            title:'<?php echo __('Tarifas de Aseguradora') ?>',        
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 'auto',
            width: 800,
            buttons: {
                "<?php echo __('Actualizar Tarifa') ?>": function() {
                         var dataString = $('#dataRates').serialize() + '&cityType=' + $('#tipociudad2').val() + '&idInsurancecarrier=' + idInsuranceCarrier;
                    $.ajax(
                    {
                        type: "POST",
                        url: updateRatesInsurance,
                        data: dataString,
                        dataType: 'json',
                        success: function(responseEdit)
                        {
                            $("#divratioinsurance").dialog("close");
                        }
                    });
            
            },
           
        }
        });
        

        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['pdf'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_BILLING',
            htmlVisor: false,
            jqUI: true,
            urlData: printInvoice,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte'); ?>...',
            afterJSONResponse: function(response) {
//                if (isSendEmail) {
//                    myAfterJSONResponse(response);
//                }
            }

        });
    });
</script>
<div >
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Registro de Facturación') ?></div>
    </center>
</div>
<div class="none" id="dlg_show_summary_billing_log">
    <ul>
        <li>
            <label for="summary_date_ini"><?php echo __('Fecha de Inicio') ?>:</label>
            <input name="summary_date_ini" id="summary_date_ini" type="text" />
        </li>
        <li>
            <label for="summary_date_end"><?php echo __('Fecha Final') ?>:</label>
            <input name="summary_date_end" id="summary_date_end" type="text" />
        </li>
        <li>
            <button id="summary_button"><?php echo __('Exportar Resumen') ?></button>
        </li>
    </ul>
</div>
<div class="show_content" >
    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="menutop2 menutop7 ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">
            <h4><?php echo __('Registro de Facturación') ?></h4>
            <div class="clear"></div>
        </div>

        <a class="tableShow aRight"><?php echo __('Mostrar Tabla de Facturas') ?></a>
        <a class="tableHide none aRight"><?php echo __('Ocultar Tabla de Facturas') ?></a>

        <a id="show_summary"><?php echo __('Mostrar Resumen') ?></a>


        <div class="clear"></div>
    </div>
    <div class="tableCont <?php // if ($mBilling->idBilling > 0)  ?><?php //if ?> ui-widget ui-widget-content ui-corner-all">

        <center>
            <table id="grid-listInvoices" ></table>
            <div id="grid-listInvoices-paper"></div>
        </center>

    </div>
    <div class="clear"></div>

    <div id="divratioinsurance" >
        <div id="divRates">
            <form id="dataRates">
                <fieldset id="datosDer" class="divRates">
                    <legend><?php echo __('Tarifas de Intérprete') ?></legend>
                    <table>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalRegular">No Show Medical Regular</label>
                                    <input  type="text" name="noShowIntMedicalRegular" id="noShowIntMedicalRegular" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalRegularMin">No Show Medical Regular Min Time</label>
                                    <input  type="text" name="noShowIntMedicalRegularMin" id="noShowIntMedicalRegularMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalRare">No Show Medical Rare</label>
                                    <input  type="text" name="noShowIntMedicalRare" id="noShowIntMedicalRare" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalRareMin">No Show Medical Rare Min Time</label>
                                    <input  type="text" name="noShowIntMedicalRareMin" id="noShowIntMedicalRareMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalOther">No Show Medical Other</label>
                                    <input  type="text" name="noShowIntMedicalOther" id="noShowIntMedicalOther" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntMedicalOtherMin">No Show Medical Other Min Time</label>
                                    <input  type="text" name="noShowIntMedicalOtherMin" id="noShowIntMedicalOtherMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalRegular">No Show Legal Regular</label>
                                    <input  type="text" name="noShowIntLegalRegular" id="noShowIntLegalRegular" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalRegularMin">No Show Legal Regular Min Time</label>
                                    <input  type="text" name="noShowIntLegalRegularMin" id="noShowIntLegalRegularMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalRare">No Show Legal Rare</label>
                                    <input  type="text" name="noShowIntLegalRare" id="noShowIntLegalRare" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalRareMin">No Show Legal Rare Min Time</label>
                                    <input  type="text" name="noShowIntLegalRareMin" id="noShowIntLegalRareMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalOther">No Show Legal Other</label>
                                    <input  type="text" name="noShowIntLegalOther" id="noShowIntLegalOther" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="noShowIntLegalOtherMin">No Show Legal Other Min Time</label>
                                    <input  type="text" name="noShowIntLegalOtherMin" id="noShowIntLegalOtherMin" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="MedicalRegular">Medical Regular:</label>
                                    <input  type="text" name="MedicalRegular" id="medicalRegular" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="MedicalRare">Medical Rare:</label>
                                    <input  type="text" name="MedicalRare" id="medicalRare" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="MedicalOther">Medical Other:</label>
                                    <input  type="text" name="MedicalOther" id="medicalOther" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="LegalRegular">Legal Regular:</label>
                                    <input  type="text" name="LegalRegular" id="legalRegular" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="LegalOther">Legal Other:</label>
                                    <input  type="text" name="LegalOther" id="legalOther" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="LegalRare">Legal Rare:</label>
                                    <input  type="text" name="LegalRare" id="legalRare" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="conferenceCallRegular">Conference Call Regular:</label>
                                    <input  type="text" name="conferenceCallRegular" id="conferenceCallRegular" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="conferenceCallOther">Conference Call Other:</label>
                                    <input  type="text" name="conferenceCallOther" id="conferenceCallOther" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="conferenceCallRare">Conference Call Rare:</label>
                                    <input  type="text" name="conferenceCallRare" id="conferenceCallRare" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p>
                                    <label class="frmlbl" for="travelTime"><?php echo __('Tiempo de Viaje') ?>:</label>
                                    <input type="text" name="travelTime" id="travelTime" size="6" value=""/>
                                </p>
                            </td>
                            <td>
                                <p>
                                    <label class="frmlbl" for="travelMiles"><?php echo __('Millaje') ?>:</label>
                                    <input  type="text" name="travelMiles" id="travelMiles" size="6" value=""/>
                                </p>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset id="fieldDrivers" class="divRates">
                    <legend><?php echo __('Conductores') ?></legend>
                    <table>
                        <tr><td>
                                <p><label class="frmlbl">Gasoline surcharge:</label> <input  type="text" name="gasolinesurcharge" id="gasolinesurcharge" size="6" value="">  </p>
                                <fieldset class="floatLeft">
                                    <legend id="amb">Ambulatory</legend>
                                    <p>
                                        <label class="frmlbl">Rate:</label>
                                        <input  type="text" name="ambulatoryRate" id="ambulatoryRate" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Waiting Time:</label>
                                        <input  type="text" name="ambulatoryWaitingTime" id="ambulatoryWaitingTime" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate RT:</label>
                                        <input  type="text" name="minimumFlatRateRTAmbulatory" id="minimumFlatRateRTAmbulatory" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate OW:</label>
                                        <input  type="text" name="minimumFlatRateOWAmbulatory" id="minimumFlatRateOWAmbulatory" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">No Show:</label>
                                        <input  type="text" name="ambulatoryNoShow" id="ambulatoryNoShow" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Early Pick Up:</label>
                                        <input  type="text" name="earlyPickUpA" id="earlyPickUpA" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Late Drop Off:</label>
                                        <input  type="text" name="lateDropOffA" id="lateDropOffA" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Weekend Fee:</label>
                                        <input  type="text" name="weekendFeeA" id="weekendFeeA" size="6" value=""/>
                                    </p>
                                </fieldset>
                            </td>
                            <td>
                                <fieldset>
                                    <legend id="wheel">WheelChair</legend>
                                    <p>
                                        <label class="frmlbl">Loaded Miles:</label>
                                        <input  type="text" name="wheelChairLoadedMiles" id="wheelChairLoadedMiles" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Lift Base Rate:</label>
                                        <input  type="text" name="wheelChairLiftBaseRate" id="wheelChairLiftBaseRate" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Waiting Time:</label>
                                        <input  type="text" name="wheelChairWaitingTime" id="wheelChairWaitingTime" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Car Assistance:</label>
                                        <input  type="text" name="wheelChairCarAssitance" id="wheelChairCarAssitance" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate RT:</label>
                                        <input  type="text" name="minimumFlatRateRTWheelChair" id="minimumFlatRateRTWheelChair" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate OW:</label>
                                        <input  type="text" name="minimumFlatRateOWWheelChair" id="minimumFlatRateOWWheelChair" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">No Show:</label>
                                        <input  type="text" name="wheelChairNoShow" id="wheelChairNoShow" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Early Pick Up:</label>
                                        <input  type="text" name="earlyPickUpW" id="earlyPickUpW" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Late Drop Off:</label>
                                        <input  type="text" name="lateDropOffW" id="lateDropOffW" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Weekend Fee:</label> <input  type="text" name="weekendFeeW" id="weekendFeeW" size="6" value=""/>
                                    </p>
                                </fieldset>
                            </td>
                            <td>
                                <fieldset>
                                    <legend id="stret">Stretcher</legend>
                                    <p>
                                        <label class="frmlbl">Loaded Miles:</label>
                                        <input  type="text" name="stretcherLoadedMiles" id="stretcherLoadedMiles" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Lift Base Rate:</label>
                                        <input  type="text" name="stretcherLiftBaseRate" id="stretcherLiftBaseRate" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Waiting Time:</label>
                                        <input  type="text" name="stretcherWaitingTime" id="stretcherWaitingTime" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Assistance:</label>
                                        <input  type="text" name="stretcherAssistanceS" id="stretcherAssistanceS" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate RT:</label>
                                        <input  type="text" name="minimumFlatRateRTStretcher" id="minimumFlatRateRTStretcher" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Minimum Flat Rate OW:</label>
                                        <input  type="text" name="minimumFlatRateOWStretcher" id="minimumFlatRateOWStretcher" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">No Show:</label>
                                        <input  type="text" name="stretcherNoShow" id="stretcherNoShow" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Early Pick Up:</label>
                                        <input  type="text" name="earlyPickUpS" id="earlyPickUpS" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">Late Drop Off:</label>
                                        <input type="text" name="lateDropOffS" id="lateDropOffS" size="6" value=""/>
                                    </p>
                                    <p>
                                        <label class="frmlbl">WeekEnd Fee:</label>
                                        <input type="text" name="weekEndFeeS" id="weekEndFeeS" size="6" value=""/>
                                    </p>
                                    <input type="hidden" name="idInsuranceRates" id="idInsuranceRates"  >
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                
            </form>
        </div>
    </div>
    <div id="formEdit">        
        <div class="menutop ui-widget ui-widget-content ui-corner-all">
            <div class="menutop2 menutop6 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">
                <h4>Billing</h4>
                <div class="clear"></div>
            </div>
            <span class="invoiceNumber ui-state-active ui-priority-primary ui-corner-all"><?php echo __('Número de Factura') ?>:</span>
            <a class="printInvoice aRight" idBilling="" title="Print Invoice"><?php echo __('Imprimir Factura') ?></a>
            <a class="addApps aRight"  title="Add App"><?php echo __('Agregar Cita') ?></a>
            <div class="clear"></div>
        </div>
        <table width="100%" border="0">
            <tbody>
                <tr>
                    <td>
                        <label for="invoiceDate"><?php echo __('Fecha de Factura') ?>:</label>
                    </td>
                    <td>
                        <input id="invoiceDate" name="invoiceDate" type="text" value=""/>
                    </td>
                    <td class="tituloNormal"><?php echo __('Aseguradora') ?>:</td>
                    <td id="insurance">
                        <a id="ratioInsurance" ></a>
                    </td>
                    <td>
                        <label for="tipociudad2" class="frmlbl regiones"><?php echo __('Región') ?>:</label>
                        <select id="tipociudad2" name="tipociudad2">
                            <option value="">Select...</option>
                            <option value="florida">Florida</option>
                            <option value="georgia">Georgia</option>
                            <option value="other">Out state</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="content4Billing">
            <div class="clear"></div>
            <hr class="ui-state-default"/>
            <ul id="liApps">


            </ul>

            <button type="button" id="saveBillingTransp" name="saveBillingTransp">
                <?php echo __('Vista Previa Transp.') ?>
            </button>

            <button type="button" id="saveBillingTransl" name="saveBillingTransl">
                <?php echo __('Vista Previa Traduc.') ?>
            </button>

            <div class="clear"></div>
            <br>
            <br>
            <br>
        </div>
        <div class="clear"></div>
        <div id="divComment" class="dialog" width="400px" title="<?php echo __('Notas') ?>">
            <form id="formCommentServ">
                <input name="textareaid" id="textareaid" type="hidden"/>
                <table>
                    <tr>
                        <td><label><?php echo __('Nota') ?>:</label></td>
                        <td><textarea name="commentServ" cols="30" rows="8" id="commentServ"></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" class="savComment"><?php echo __('Guardar') ?></button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="dialogAvailableApps" class="dialog" title="<?php echo __('Citas Disponibles') ?>" width="1200px">
            <div class="clear"></div>
            <center>
            <table id="grid-listMoreApps" ></table>
            <div id="grid-listMoreApps-paper"></div>
            </center>
            <div class="clear"></div>
        </div>
        <div id="divbillSave" class="dialog" width="800px">
            <form id="formtransl">
                <label id="statusbillingtransl"></label>
                <fieldset>
                    <legend><?php echo __('Factura Previa') ?></legend>

                    <table width="100%" border="0" class="dashboard" id="translationbill">
                        <thead>
                    <tr>
                        <th><?php echo __('Fecha') ?></th>
                        <th><?php echo __('Descripción') ?></th>
                        <th><?php echo __('Cantidad') ?></th>
                        <th><?php echo __('Precio') ?></th>
                        <th><?php echo __('Amount') ?> ($)</th>
                    </tr>
                    <tr><th><br></th></tr>
                </thead>
                <tbody id="detailsbilltransl">
                </tbody>
                <tfoot id="detailsbilltranslfoot">

                </tfoot>

                    </table>
                 
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div id="divReport">
<?php include ('appview.php'); ?>