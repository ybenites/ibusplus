<style type="text/css">

    .frmlabel {
        display: block;
        float: left;
        font-weight: bold;
        height: 16px;
        padding-top: 5px;
        width: 175px;
    }  
    #head{
        float:left;
        margin-left: 35px;
    }
    hr.hr_separate{ border: 2px #c5c5c5 dashed;opacity: 0.55;}
    h4{line-height: 15px;}
    .bCommentServDri, .bCommentServInt {margin: 0;float: right}
    .editApps{float: right}
    b.balert{color:#e10101;}
    .none{display: none;}
    #dataRates fieldset{display: inline;}
    #statusbillingtransl,#statusbillingtransp,#statustransp,#statustransl{color: red;font-style: oblique;}
    a{cursor: pointer;}
    div#MarginTable{min-height:300px;}
    #divbilling{width: 1190px;display: none; }
    div#MarginTableEMP{float:left;}
    #formNew{display:none;margin-top:30px;float:left;}
    #driConf{float:left;}
    #locConf{margin-left: 370px;height:70px;}
    #patConf{margin-left: 180px;width:80px;margin-top:-95px;}
    #formNewEmpNote{display:none;margin-top:30px;}
    #formNewIntNote{display:none;margin-top:30px;}
    #formNewDriNote{display:none;margin-top:30px;}
    #imgEdit, #imgEmployeer, #imgDriver ,#imgInterpreter{cursor:pointer;}
    #closeDriNote, #closeEmpNote, #closeIntNote,#appoitmentFORM{display:none;}
    #callingInfo{width:300px;}
    #pickUpInfo{width:540px;margin-left: 15px;margin-top: -247px;float: left;}
    #serviceInfo{float:left;width:300px;}
    #fieldsetfrom{float: left;margin-right:10px;}
    #fieldsetfrom,#fieldsetto{width:235px;}
    #formNewConNote{display:none;margin-top:30px;}
    #formNewPatNote{display:none;margin-top:30px;}
    #backMarginTable{display: none;}
    #a,#st,#w,.transPage,.transWord{display: none;}    
    #a,#w,#st{width: 200px;}
    .tableCont ul li.travel{margin: 0;}
    table.dashboard tbody th, tbody td{vertical-align: top;}
    .frmlbl,.floatLeft input{float: left;}
    .frmlbl{width: 120px;line-height: 15px;height: auto;}
    p{padding: 0;margin: 0;}
    .menutop{padding: 2px;}
    .menutop2{padding: 5px;}
    .tableOne label{float: left;margin-right: 5px;}
    .tableOne label.header{text-align: center;}
    .pickup{width: 210px;}
    .pick2{width: 120px;text-align: center;}
    .travel{width: 320px;text-align: center;}
    .miles{width: 80px;text-align: center;}
    .status{width: 85px;text-align: center;}
    .tableCont label{float: left;margin-right: 5px;font-weight: normal;}
    .tableCont{margin-top: 5px;padding: 7px;}
    .tableCont ul{margin: 0;padding: 0;list-style: none;float:left;}
    .tableCont ul li{list-style: none;float:left;margin: 5px 0;}
    h1,h2,h3,h4{margin: 0;padding: 0;}
    .content4Billing{margin-top: 10px;}
    .divTrans{float: left;width: 590px;margin-right: 10px;}
    .divInt{float: left;width: 590px;}
    .contTrans, .contInt{padding: 7px;margin-top: 5px;}
    .contApp {padding: 7px;margin-top: 5px; margin-bottom: 5px;}
    .contTrans ul,.contInt ul,.contApp ul{list-style: none;margin: 0;padding: 0;}
    .contTrans ul li,.contInt ul li ,.contApp ul li{list-style: none;padding: 5px 0;display: block;}
    .contTrans ul li dl,.contTrans ul li dd,.contTrans ul li dt,
    .contApp ul li dl,.contApp ul li dd,.contApp ul li dt,
    .contInt ul li dl,.contInt ul li dd,.contInt ul li dt{padding: 0;margin: 0 10px 0 0;float: left;}
    .contTrans ul li dt,.contTrans ul li dd,
    .contInt ul li dt,.contInt ul li dd{float: left;}
    .contTrans ul li dt label.driver{width: 110px;margin-right: 5px;}
    .contTrans ul li dd label.driverName{width: 445px;font-weight: normal;}
    .contTrans ul li dd label.wthour,.contTrans ul li dd label.wtmin,.contTrans ul li dd label.time{font-weight: normal;padding: 5px 0;margin-right: 5px;}
    .contTrans ul li dd input{margin-right: 5px;}

    .contTrans ul li dt label.tservicest{width: 160px;margin-right: 5px;}
    .contTrans ul li dd label.tservicestName{width: 395px;font-weight: normal;}

    .contInt ul li dt label.firstColumn{width: 140px;margin-right: 5px;}
    .contInt ul li dt label.secondColumn{width: 125px;margin-right: 5px;}
    .contInt ul li dd label.firstColumnName{width: 130px;margin-right: 5px;font-weight: normal;}
    .contInt ul li dd label.secondColumnName{width: 170px;margin-right: 5px;font-weight: normal;}
    .contInt ul li dd label.fullName{width: 440px;margin-right: 5px;font-weight: normal;}
    .clear{margin: 0;padding: 0;}
    .contTrans ul li dl.checkbox dd{margin: 0;}

    .divTrans .ui-widget-content a,.divInt .ui-widget-content a{color: #026890;text-decoration: underline;}
    .updateRate{margin-left: 15px;}
    input.trans{margin-left: 10px;float: left;}

    .transPage input,.transPage label{float: left;margin-left: 5px;}
    .transPage label{line-height: 20px;padding: 5px;}
    .transWord input,.transWord label{float: left;margin-left: 5px;}
    .transWord label{line-height: 20px;padding: 5px;}
    .contInt ul li.forTransDoc{display: none;}
    #translationbill td.right{text-align: right;}
    #addCommentTransl,#addCommentTransp{cursor: pointer;float: left;margin: 2px;padding: 5px 7px 5px 0;position: relative;}
    #addCommentTransl span,#addCommentTransp span{float: left;margin: 0 4px;}
    .divTrans,.divInt{display: none;}
    dl.options{margin: 10px;float: left;}
    dl.options dd{float: left;}
    dl.options dd label{padding-left:5px;padding-right: 10px; }
    table.display td.padding10{padding: 10px;}
    .menutop3{width: 540px;float: left}
    .menutop4{width: 540px;float: left}
    .menutop5{width: 1140px;float: left}
    .menutop6{width: 955px;float: left}
    .invoiceNumber{width: 200px;float: left;line-height: 25px; padding: 0 5px;text-align: center;float: right;}
    .bCommentServDri, .Int{float: right;}
    .contTrans ul li dl dd.plusMarginLeft{margin-left: 10px;}
    div.middle{float: left;width: 500px;margin-right: 10px;}
    .hidden{display: none;}
    .contTrans ul li dd.noShowLabel{float: right;color: #e10101;}
    .contInt ul li dd.noShowLabel{float: right;color: #e10101;}
</style>

<script type="text/javascript">
 var listNotes = '/patients/patientcallinglist/listNotes';
    var getNewCodeInvoice = '/billings/billingconfirm/getnewCodeInvoice';
    var getAllAppointmentsForBilling = '/billings/billingconfirm/getAllAppointmentsForBilling';
    var getAllBillingInfoPatients = '/billings/billingconfirm/getAllBillingInfoPatients';
    var getAllBillingInfoAppointment = '/billings/billingconfirm/getAllAppointmentsforBillingDetail';
    var getInfoAppForBilling = '/billings/billingconfirm/getInfoAppForBilling';
    var saveBilling = '/billings/billingconfirm/saveBilling';
    var milesToTime = "/patients/transsummaryreport/milesToTime";
    var calculartiempotrasnc = "/patients/transsummaryreport/calculartiempotrasl";
        var saveNotesEmployeers = '/patients/patientcallinglist/saveNotesEmployeers';
    var dataString;
    $(document).ready(function() {

        $('#insurance_carrier').combobox();
        $('#tipociudad2').combobox();
        $('#invoiceDate').datepicker();
        $('.savComment').button();
        $('#backMarginTable').button().click(function() {
//                $('#divbilling').css('display', 'none');
//            $("#MarginTable").show('slide', 500);
            $("#divbilling").hide('slide', null, 500, callback);
//            $("#MarginTable").show();
        });
        function callback() {
            setTimeout(function() {
                $("#MarginTable").show();
            }, 200);
        }
        ;
        jQuery("#listbillingconfirm").jqGrid({
            url: getAllAppointmentsForBilling,
            datatype: "json",
            height: "auto",
            width: 1200,
            colNames: ['id', 
                       '<?php echo __('Name'); ?>', 
                       '<?php echo __('Total de Citas'); ?>', 
                       '<?php echo __('Total de Serv. de Traducción'); ?>', 
                       '<?php echo __('Total de Serv. de Transporte'); ?>'
                      ],
            colModel: [
                {name: 'id', index: 'id', hidden: true},
                {name: 'name', index: 'name', width: 100, align: "center"},
                {name: 'totalapp', index: 'totalapp', width: 100, align: "center"},
                {name: 'totalservtransl', index: 'totalservtransl', width: 100, align: "center"},
                {name: 'totalservtransp', index: 'totalservtransp', width: 100, align: "center"},
            ],
            rowNum: 50,
            rowList: [50, 100, 150, 200],
            pager: '#pagerbillingconfirm',
            sortname: 'id',
            viewrecords: true,
            sortorder: "desc",
            multiselect: false,
            subGrid: true,
//            caption: "<?php // echo __('Citas por Facturar'); ?>",
            subGridRowExpanded: function(subgrid_id, row_id) {
                var subgrid_table_id, pager_id;
                subgrid_table_id = subgrid_id + "_t";
                pager_id = "p_" + subgrid_table_id;
                $("#" + subgrid_id).html("<table id='" + subgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>");
                jQuery("#" + subgrid_table_id).jqGrid({
                    url: getAllBillingInfoPatients,
                    datatype: "json",
                    height: "auto",
                    width: 1170,
                    postData: {
                        idPatient: row_id
                    },
                    colNames: ['idPatient',
                               '<?php echo __('Paciente'); ?>',
                               '<?php echo __('Total de Citas'); ?>', 
                               '<?php echo __('Traducción'); ?>', 
                               '<?php echo __('Transporte'); ?>'],
                    colModel: [
                        {name: 'idPatient', index: 'idPatient', hidden: true},
                        {name: 'namepatient', index: 'namepatient', width: 70, align: "center"},
                        {name: 'totalapp', index: 'totalapp', width: 70, align: "center"},
                        {name: 'translation', index: 'translation', width: 70, align: "center"},
                        {name: 'transportation', index: 'transportation', width: 70, align: "center"}
//                        {name: 'actions', index: 'actions', width: 100, align: "center"},
                    ],
                    rowNum: 20,
                    pager: pager_id,
                    sortname: 'idPatient',
                    sortorder: "asc",
                    subGrid: true,
                    afterInsertRow: function(row_id, data_id) {
//                      $("#" + subgrid_table_id).jqGrid('setRowData', row_id, {actions: active});
                    },
                    subGridRowExpanded: function(subsubgrid_id, row_id) {

                        var subsubgrid_table_id, pager_id;
                        subsubgrid_table_id = subsubgrid_id + "_t";
                        subsubpager_id = "p_" + subsubgrid_table_id;
                        $("#" + subsubgrid_id).html("<form id='formIdPatient" + row_id + "' class='formIdPatient' action='' >\n\
                                                     <button class='active_billing_group' idPatient='" + row_id + "' type='button'><?php echo __('Facturar'); ?></button>\n\
                                                    <br><table id='" + subsubgrid_table_id + "' class='scroll'></table><div id='" + pager_id + "' class='scroll'></div>\n\
                                                     </form>");
                        jQuery("#" + subsubgrid_table_id).jqGrid({
                            url: getAllBillingInfoAppointment,
                            datatype: "json",
                            postData: {
                                idPatient: row_id
                            },
                            colNames: ['Id', '', 
                                       '<?php echo __('Fecha'); ?>', 
                                       '<?php echo __('Nombre'); ?>', 
                                       '<?php echo __('Dirección'); ?>',
                                       '<?php echo __('Teléfono'); ?>', 
                                       '<?php echo __('Nombre'); ?>', 
                                       '<?php echo __('Dirección'); ?>', 
                                       '<?php echo __('Teléfono'); ?>', 
                                       '<?php echo __('Nombre'); ?>', 
                                       '<?php echo __('Teléfono'); ?>',
                                       '<?php echo __('Nombre'); ?>', 
                                       '<?php echo __('Teléfono'); ?>', 
                                       '<?php echo __('Nombre'); ?>', 
                                       '<?php echo __('Teléfono'); ?>', 
                                       'rdyBilling', 'callingList', 'idDriversPickUp', 'driverReport', 'idInterpreter', 'interpreterRerport', 'translationSummary', 'preBilling', 
                                       '<?php echo __('Acciones'); ?>'],
                            colModel: [
                                {name: 'idAppointment', index: 'idAppointment', hidden: true},
                                {name: 'selectcheck', index: 'selectcheck', width: 20, align: "center"},
                                {name: 'appointmentdate', index: 'appointmentdate', search: true, width: 70, align: "center", frozen: true},
                                {name: 'namepatient', index: 'namepatient', width: 80, search: true, align: "center", formatter: 'showlink'},
                                {name: 'addresspatient', index: 'addresspatient', width: 100, search: true, align: "center"},
                                {name: 'phonepatient', index: 'phonepatient', width: 80, search: true, align: "center"},
                                {name: 'contactname', index: 'contactname', width: 80, search: false, align: "center", formatter: 'showlink'},
                                {name: 'contactaddress', index: 'contactaddress', width: 100, search: false, align: "center"},
                                {name: 'contactphone', index: 'contactphone', width: 80, search: false, align: "center"},
                                {name: 'namedriver1', index: 'namedriver1', width: 80, search: false, align: "center", formatter: 'showlink'},
                                {name: 'phonedriver1', index: 'phonedriver1', width: 80, search: false, align: "center"},
                                {name: 'namedriver2', index: 'namedriver2', width: 80, search: false, align: "center", formatter: 'showlink'},
                                {name: 'phonedriver2', index: 'phonedriver2', width: 80, search: false, align: "center"},
                                {name: 'nameinterprete', index: 'nameinterprete', width: 80, search: false, align: "center", formatter: 'showlink'},
                                {name: 'phoneinterprete', index: 'phoneinterprete', width: 80, search: false, align: "center"},
                                {name: 'rdyBilling', index: 'rdyBilling', hidden: true},
                                {name: 'callingList', index: 'callingList', hidden: true},
                                {name: 'idDriversPickUp', index: 'idDriversPickUp', hidden: true},
                                {name: 'driverReport', index: 'driverReport', hidden: true},
                                {name: 'idInterpreter', index: 'idInterpreter', hidden: true},
                                {name: 'interpreterRerport', index: 'interpreterRerport', hidden: true},
                                {name: 'translationSummary', index: 'translationSummary', hidden: true},
                                {name: 'preBilling', index: 'preBilling', hidden: true},
                                {name: 'actions', index: 'actions', width: 140, search: false, align: "center", frozen: true}

                            ],
                            rowNum: 30,
                            rowList: [30, 40, 50],
                            height: "auto",
                            width: 1140,
                            shrinkToFit: false,
                            pager: subsubpager_id,
                            sortname: 'idAppointment',
                            viewrecords: true,
                            sortorder: "desc",
//                            caption: "Appointments for billing",
                            afterInsertRow: function(row_id, data_id) {
                                callinglist = '';
                                driverreport = '';
                                interpreterreport = '';
                                translationSummary = '';
                                prebilling = '';
                                edit = '';
                                note = '';
                                active = '';
                                checkSelect = '';

                                var data = data_id;

                                if (data.rdyBilling == 1) {
                                    edit = "<a class=\"edit\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Editar Cita'); ?>\" ><?php echo __('Editar Cita'); ?></a>";
                                    note = "<a class=\"note\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Notas de Empleados'); ?>\" ><?php echo __('Notas de Empleados'); ?></a>";
                                    active = "<a class=\"active_billing\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Seleccionar Cita para Facturar'); ?>\" ><?php echo __('Seleccionar Cita para Facturar'); ?></a>";
                                    checkSelect = "<input class='checkselect' name='appIds[]' type=\"checkbox\" value=\"" + row_id + "\" >";

                                } else {

                                    if (data.callingList === 1)
                                        callinglist = "<a class=\"callinglist\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Calling List'); ?>\" ><?php echo __('Calling List'); ?></a>";

                                    if (data.idDriversPickUp > 0)
                                        if (data.driverReport === 1)
                                            driverreport = "<br/><a class=\"driverreport\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Driver Report'); ?>\" ><?php echo __('Driver Report'); ?></a>";


                                    if (data.idInterpreter > 0) {
                                        if (data.interpreterRerport)
                                            interpreterreport = "<br/><a class=\"interpreterreport\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Interpreter Report'); ?>\" ><?php echo __('Interpreter Report'); ?></a>";

                                        if (data.translationSummary)
                                            translationSummary = "<br/><a class=\"translationSummary\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Translation Summary'); ?>\" ><?php echo __('Translation Summary'); ?></a>";
                                    }

                                    if (data.preBilling === 1)
                                        prebilling = "<br/><a class=\"prebilling\" data-idappointment=\"" + row_id + "\" title=\"<?php echo __('Pre Billing'); ?>\" ><?php echo __('Pre Billing'); ?></a>";


                                }
                                $("#" + subsubgrid_table_id).jqGrid('setRowData', row_id, {selectcheck: checkSelect, actions: edit + note + active + callinglist + driverreport + interpreterreport + translationSummary + prebilling});


                            },
                            gridComplete: function() {
                                $('.callinglist').button({
                                    icons: {primary: 'ui-icon-check'},
                                    text: 'Calling List'
                                });
                                $('.prebilling').button({
                                    icons: {primary: 'ui-icon-check'},
                                    text: 'Pre Billing'
                                });
                                $('.driverreport').button({
                                    icons: {primary: 'ui-icon-check'},
                                    text: 'Driver Report'
                                });
                                $('.interpreterreport').button({
                                    icons: {primary: 'ui-icon-check'},
                                    text: 'No Interpreter Report'
                                });
                                $('.translationSummary').button({
                                    icons: {primary: 'ui-icon-check'},
                                    text: 'Translation Summary'
                                });
                                $('.note').button({
                                    icons: {primary: ' ui-icon-document-b'},
                                    text: false
                                }).click(function(){
                                    
                                    idAppointment=$(this).attr('data-idappointment');
                                     $("#grid-listNotesEmployeers").trigger("reloadGrid");
                                    $('#divEmployeers').dialog('open');
                                });
                                $('.active_billing').button({
                                    icons: {primary: ' ui-icon-check'},
                                    text: false
                                }).click(function() {

                                    appIds = $(this).attr('data-idappointment');
                                    var dataString = 'appIds[]=' + appIds;

                                    getInformation(dataString);
                                });
                                $('.active_billing_group').button({
                                    icons: {primary: ' ui-icon-shuffle'},
                                    text: true
                                }).click(function() {

                                    if (formIdPatient.form()) {
                                        $('.formIdPatient').submit();
                                    }
                                });
                                $('.edit').button({
                                    icons: {primary: 'ui-icon-pencil'},
                                    text: false
                                }).click(function(){
                                  $('#idAppointmentView').val($(this).attr("data-idappointment")).trigger('change');;
                 $('#dialogAppEdit').dialog('open');
                                });
                                
                                
                                
                                formIdPatient = $(".formIdPatient").validate({
                                    highlight: function(input) {
                                        $(input).addClass("ui-state-error");
                                        $(input).removeClass("ui-state-highlight");
                                    },
                                    unhighlight: function(input) {
                                        $(input).removeClass("ui-state-error");
                                        $(input).removeClass("ui-state-highlight");
                                    },
                                    errorElement: "em",
                                    success: function(label) {
                                        label.text("ok!").addClass("success");
                                    },
                                    submitHandler: function(form) {
                                        idPatient = $(form).attr("idPatient");

                                        getInformation($(form).serialize());
                                    }
                                });


                            }
                        });
                        
                         $("#" + subsubgrid_table_id).jqGrid('setGroupHeaders', {
            useColSpanStyle: true, 
            groupHeaders:[
            {startColumnName: 'namepatient', numberOfColumns: 3, titleText: '<?php echo __('Paciente'); ?>'},
            {startColumnName: 'contactname', numberOfColumns: 3, titleText: '<?php echo __('Contacto'); ?>'},
            {startColumnName: 'namedriver1', numberOfColumns: 2, titleText: '<?php echo __('Conductor'); ?> 1'},
            {startColumnName: 'namedriver2', numberOfColumns: 2, titleText: '<?php echo __('Conductor'); ?> 2'},
            {startColumnName: 'nameinterprete', numberOfColumns: 2, titleText: '<?php echo __('Intérprete'); ?>'}
            ]	
         });
                        $("#" + subsubgrid_table_id).on('click', '.active', function() {
                            var id = $(this).data("oid");
                            $('#idPatient_find').val(id);
                            $('#grid-listAppforbilling').trigger("reloadGrid");
                            $('#dialog_qgrid').dialog('open');
                        });
                    },
                    gridComplete: function() {
                        $('.active').button({
                            icons: {primary: 'ui-icon-check'},
                            text: false
                        });

                    }
                });
                $("#" + subgrid_table_id).on('click', '.active', function() {
                    var id = $(this).data("oid");
                    $('#idPatient_find').val(id);
                    $('#grid-listAppforbilling').trigger("reloadGrid");
                    $('#dialog_qgrid').dialog('open');
                });
            }
        });
        formIdPatient = $(".formIdPatient").validate({
            highlight: function(input) {
                $(input).addClass("ui-state-error");
                $(input).removeClass("ui-state-highlight");
            },
            unhighlight: function(input) {
                $(input).removeClass("ui-state-error");
                $(input).removeClass("ui-state-highlight");
            },
            errorElement: "em",
            success: function(label) {
                label.text("ok!").addClass("success");
            },
            submitHandler: function(form) {
                idPatient = $(form).attr("idPatient");

                getInformation($(form).serialize());
            }
        });

        getInformation = function(dataString) {
            $.ajax({
                type: "POST",
                url: getNewCodeInvoice,
                data: dataString,
                dataType: 'json',
                async: false,
                success: function(response) {

                    $(".invoiceNumber").text('<?php echo __('Número de Factura'); ?>: ' + response.newinvoice);
                }

            });
            idAppsTransl = [];
            idAppsTransp = [];
            $('div.content4Billing').html('');
            $('#MarginTable').css('display', 'none');
            $('#backMarginTable').css('display', 'block');
            $("#tdmilesdescrp").remove();
            $("#divbilling").show('slide', null, 500);
            $.ajax({
                type: "POST",
                url: getInfoAppForBilling,
                data: dataString,
                dataType: 'json',
                async: false,
                success: function(response) {
                    if (response.msg === 'OK') {
                        var data = response.data;

                        $.each(data, function() {
                            //console.log(this);
                            var hr = '<div class="clear"></div><hr class="hr_separate"/>';
                            $('#invoiceDate').val(this.d_current_date);
                            billingTranspId = -1;
                            if (this.idDriverOneWay > 0) {
                                //console.log('Translation :'+this.idAppointment);
                                if (this.billingtransp == 1) {
                                    billingTranspId = -1;
                                } else {
                                    billingTranspId = this.idAppointment;
                                }
                            }

                            billingTranslId = -1;
                            if (this.idInterpreter > 0) {
                                //console.log('Translation :'+this.idAppointment);
                                if (this.billinginterp == 1) {
                                    billingTranslId = -1;
                                } else {
                                    billingTranslId = this.idAppointment;
                                }
                            }
                            interpretationTimeFrom = '';
                            interpretationTimeTo = '';
                            interpretationTimeTotalTime = '';
                            travelFrom = '';
                            travelTo = '';
                            totalMileage = '';
                            travelTotalTime = '';
                            if (this.idTranslationSummary > 0) {
                                interpretationTimeFrom = this.interpretationTimeFrom;
                                interpretationTimeTo = this.interpretationTimeTo;
                                interpretationTimeTotalTime = this.interpretationTimeTotalTime;
                                travelFrom = this.travelFrom;
                                travelTo = this.travelTo;
                                totalMileage = this.totalMileage;
                                travelTotalTime = this.travelTotalTime;
                            }

                            noShowDriver = '';
                            noShowInterpreter = '';
                            if (this.showdriver == 1) {
                                noShowDriver = '<dd class="noShowLabel" id="noShowDriver' + this.idAppointment + '">NO SHOW</dd>';
                            }
                            if (this.showinterpreter == 1) {
                                noShowInterpreter = '<dd class="noShowLabel" id="noShowInterpreter' + this.idAppointment + '">NO SHOW</dd>';
                            }



                            switch (this.typeservicesInterpreter) {
                                case 'Conference Call':
                                    typeServicesInterpreter = 'Conference Call';
                                    addInt = '<li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Hora de Término'); ?>:</label></dt>\n\
                                                    <dd>' + this.dropTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    summ = '<li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tiempo de Interpretación'); ?></label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Desde'); ?>:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeFrom' + this.idAppointment + '" id="interpretationTimeFrom' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeFrom + '"/></dt>\n\
                                                    <dt><label><?php echo __('Hasta'); ?>:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeTo' + this.idAppointment + '" id="interpretationTimeTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTo + '"/></dt>\n\
                                                    <dt><button class="calctimeint" idAppointment="' + this.idAppointment + '" type="button"><?php echo __('Tiempo Total'); ?></button><?php echo __('Tiempo Total'); ?>:</dt>\n\
                                                    <dt><input type="text" readonly name="interpretationTimeTotalTime' + this.idAppointment + '" id="interpretationTimeTotalTime' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTotalTime + '" /></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    break;
                                case 'Document Translation':
                                    typeServicesInterpreter = 'Document Translation';
                                    addInt = '';
                                    summ = '<li class="forTransDoc' + this.idAppointment + ' hidden">\n\
                                                <dl>\n\
                                                    <dt><label class="firstColumn"><?php echo __('Traducción por Página'); ?>:</label></dt>\n\
                                                    <dd class="fullName transDoc">\n\
                                                        <input type="radio" name="typeTransDoc' + this.idAppointment + '" id="typeTransPage' + this.idAppointment + '" value="page" validate="{required: true, minlength:1}"/>\n\
                                                        <label for="typeTransPage' + this.idAppointment + '">\n\
                                                            <?php echo __('Traducción por Página'); ?>\n\
                                                        </label>\n\
                                                    </dd>\n\
                                                    <dd class="fullName transPage">\n\
                                                        <label for="txtpage"># <?php echo __('Páginas'); ?>:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpage' + this.idAppointment + '" id="txtpage' + this.idAppointment + '" />\n\
                                                        <label for="txtpricePage"><?php echo __('Precio por página'); ?>:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpricePage' + this.idAppointment + '" id="txtpricePage' + this.idAppointment + '" />\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="forTransDoc' + this.idAppointment + ' hidden">\n\
                                                <dl>\n\
                                                    <dt><label class="firstColumn"><?php echo __('Traducción por Palabra'); ?>:</label></dt>\n\
                                                    <dd class="transDoc fullName">\n\
                                                        <input type="radio" name="typeTransDoc' + this.idAppointment + '" id="typeTransWord' + this.idAppointment + '" value="word"/>\n\
                                                        <label for="typeTransWord' + this.idAppointment + '">\n\
                                                            <?php echo __('Traducción por Palabra'); ?>\n\
                                                        </label>\n\
                                                    </dd>\n\
                                                    <dd class="transWord fullName">\n\
                                                        <label for="txtword"># <?php echo __('Palabras'); ?>:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtword' + this.idAppointment + '" id="txtword' + this.idAppointment + '" />\n\
                                                        <label for="txtpriceWord"><?php echo __('Precio por palabra'); ?>:</label>\n\
                                                        <input size="5" class="trans ui-widget ui-widget-content ui-corner-all" type="text" name="txtpriceWord' + this.idAppointment + '" id="txtpriceWord' + this.idAppointment + '" />\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    break;
                                case 'Interpreting':
                                    typeServicesInterpreter = 'Interpreting';
                                    addInt = '<li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tiempo de Término'); ?>:</label></dt>\n\
                                                    <dd>' + this.dropTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Contacto'); ?>:</label></dt>\n\
                                                    <dd>' + this.calias + ': ' + this.caddress + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                                    summ = '<li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tiempo de Interpretación'); ?></label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Desde'); ?>:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeFrom' + this.idAppointment + '" id="interpretationTimeFrom' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeFrom + '"/></dt>\n\
                                                    <dt><label><?php echo __('Hasta'); ?>:</label></dt>\n\
                                                    <dt><input type="text" name="interpretationTimeTo' + this.idAppointment + '" id="interpretationTimeTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTo + '"/></dt>\n\
                                                    <dt><button class="calctimeint" idAppointment="' + this.idAppointment + '" type="button"><?php echo __('Tiempo Total'); ?></button><?php echo __('Tiempo Total'); ?>:</dt>\n\
                                                    <dt><input type="text" readonly name="interpretationTimeTotalTime' + this.idAppointment + '" id="interpretationTimeTotalTime' + this.idAppointment + '" size="10" class="ui-widget ui-widget-content ui-corner-all" value="' + interpretationTimeTotalTime + '" /></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Viaje (Ciudad a Ciudad)'); ?></label></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Desde'); ?>:</label></dt>\n\
                                                    <dt><input value="' + travelFrom + '" type="text" name="travelFrom' + this.idAppointment + '" id="travelFrom' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dt><label><?php echo __('Hasta'); ?>:</label></dt>\n\
                                                    <dt><input value="' + travelTo + '" type="text" name="travelTo' + this.idAppointment + '" id="travelTo' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Millaje Total'); ?>:</label></dt>\n\
                                                    <dt><input value="' + totalMileage + '" type="text" name="totalMileage' + this.idAppointment + '" id="totalMileage' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dt><button class="calctraveltimeint" idAppointment="' + this.idAppointment + '" type="button"><?php echo __('Tiempo Total'); ?></button><?php echo __('Tiempo Total'); ?>:</dt>\n\
                                                    <dt><input value="' + travelTotalTime + '" type="text" name="travelTotalTime' + this.idAppointment + '" id="travelTotalTime' + this.idAppointment + '" size="10"  class="ui-widget ui-widget-content ui-corner-all"/></dt>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>';
                                    break;
                                default:
                                    addInt = '';
                                    summ = '';
                                    break;
                            }

                            switch (this.tipociudad) {
                                case 'florida':
                                    region = 'Florida';
                                    break;
                                case 'georgia':
                                    region = 'Georgia';
                                    break;
                                case 'other':
                                    region = 'Out state'
                                    break;
                                default:
                                    region = '<b class="balert">' + this.tipociudad + '</b>';
                                    break;
                            }

                            roundTrip = this.typetravel == 'Round Trip' ? 2 : 1;
                            txtExtra1 = '';
                            txtExtra2 = '';
                            txtOut = '';
                            //console.log(this);
                            if (this.lbase == 'on') {
                                txtExtra1 = '<li class="liLiftBase">\n\
                                                <dl>\n\
                                                    <dd>Lift Base:</dd>\n\
                                                    <dd><input size="5" class="ui-widget ui-widget-content ui-corner-all" name="lbase' + this.idAppointment + '" id="lbase' + this.idAppointment + '" type="text" value="' + roundTrip + '"/></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                            }
                            if (this.cassistance == 'on') {
                                txtExtra2 = '<li class="liCassistance">\n\
                                                <dl>\n\
                                                    <dd>[CAR]Assitence</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>';
                            }

                            switch (this.typeservice) {
                                case 'Ambulatory':
                                    txtOut = txtExtra1;
                                    break;
                                case 'Wheelchair':
                                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', 'Car ');
                                    break;
                                case 'Stretcher':
                                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', '');
                                    break;
                                default:
                                    break;
                            }

                            if (this.frate > 0) {
                                milesRate = '<?php echo __('Tarifa Plana'); ?>';
                                isCheckedFlateRate = ' checked ';
                                milesFlateRate = this.frate;
                            } else {
                                milesFlateRate = this.miles;
                                milesRate = '<?php echo __('Millas'); ?>';
                                isCheckedFlateRate = '';
//                                if (this.milesBilling > 0) {
//                                    milesFlateRate = this.milesBilling;
//                                } else {
//                                    milesFlateRate = this.miles;
//                                }
                            }

                            var html = hr + '\n\
                                <input id="enableTransl' + this.idAppointment + '" name="enableTransl' + this.idAppointment + '" type="hidden" value="' + billingTranslId + '" />\n\
                                <input id="enableTransp' + this.idAppointment + '" name="enableTransp' + this.idAppointment + '" type="hidden" value="' + billingTranspId + '" />\n\
                                <div class="menutop ui-widget ui-widget-content ui-corner-all">\n\
                                    <div class="menutop2 menutop5 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                        <h4><?php echo __('Información de la Cita'); ?> : ' + this.idAlias + '</h4>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <a idAppointment="' + this.idAppointment + '" class="editApps"><?php echo __('Editar Cita'); ?></a>\n\
                                    <div class="clear"></div>\n\
                                </div>\n\
                                <div class="ui-widget ui-widget-content ui-corner-all contApp">\n\
                                    <div class="middle">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Nombre de Paciente'); ?>:</label></dt>\n\
                                                    <dd>' + this.firstName + ' ' + this.lastName + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Autorizado por'); ?>:</label></dt>\n\
                                                    <dd>' + this.appAuthorized + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Fecha de Servicio'); ?>:</label></dt>\n\
                                                    <dd>' + this.appointmentDate + '</dd>\n\
                                                    <dt><label><?php echo __('Hora'); ?>:</label></dt>\n\
                                                    <dd>' + this.appointmentTime + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Llamado por'); ?>:</label></dt>\n\
                                                    <dd>' + this.whocalls + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Nombre de la Compañia'); ?>:</label></dd>\n\
                                                    <dd>' + this.companyName + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                        </ul>\n\
                                    </div>\n\
                                    <div class="middle">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Región'); ?>:</label></dt>\n\
                                                    <dd>' + region + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Cita'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeapp + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Descripción de la Cita'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeAppointment + '</dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                        </ul>\n\
                                    </div>\n\
                                    <div class="clear"></div>\n\
                                </div>\n\
                                <form name="formTrans' + this.idAppointment + '" id="formTrans' + this.idAppointment + '" action="" method="POST">\n\
                                <textarea class="hidden" name="commentServDri' + this.idAppointment + '" id="commentServDri' + this.idAppointment + '"></textarea>\n\
                                <input type="hidden" id="milesBilling' + this.idAppointment + '" class="milesBilling" name="milesBilling' + this.idAppointment + '" value="0" />\n\
                                <input type="hidden" id="flateRateBilling' + this.idAppointment + '" class="flateRateBilling" name="flateRateBilling' + this.idAppointment + '" value="' + this.frate + '" />\n\
                                <div class="divTrans divTrans' + this.idAppointment + '">\n\
                                    <div class="menutop ui-widget ui-widget-content  ui-corner-all">\n\
                                        <div class="menutop2 right menutop3 ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                            <h4 style="text-align: center"><?php echo __('Información de Transporte'); ?></h4>\n\
                                            <div class="clear"></div>\n\
                                        </div>\n\
                                        <button type="button" idAppointment="' + this.idAppointment + '" class="bCommentServDri"><?php echo __('Comentarios'); ?></button>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <div class="ui-widget ui-widget-content ui-corner-all contTrans">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Conductor'); ?> 1:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioDriver1" idDriverOneWay="' + this.idDriverOneWay + '">' + this.driverPickUpName + ' ' + this.driverPickUpLastName + '</a>\n\
                                                    </dd>\n\
                                                    ' + noShowDriver + '\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Conductor'); ?> 2:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioDriver2" idDriversDrop="' + this.idDriverRoundTrip + '">' + this.driverDropName + ' ' + this.driverDropLastName + '</a>\n\
                                                    </dd>\n\
                                                    <dd class="checkbox noShowLabel">\n\
                                                        <input type="checkbox" name="haveFlatRate' + this.idAppointment + '" id="haveFlatRate' + this.idAppointment + '"' + isCheckedFlateRate + '/><label for="haveFlatRate' + this.idAppointment + '"><?php echo __('Tarifa Plana'); ?></label>\n\
                                                    </dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Transporte'); ?>:</label></dt>\n\
                                                    <dd>' + this.typeservice + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Desde'); ?>:</label></dt>\n\
                                                    <dd>' + this.oalias1 + ': ' + this.oaddress1 + ' ' + this.osuite1 + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Hasta'); ?>:</label></dt>\n\
                                                    <dd>' + this.oalias2 + ': ' + this.oaddress2 + ' ' + this.osuite2 + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Viaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.typetravel + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Millas'); ?>:</label></dt>\n\
                                                    <dd>' + this.miles + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl class="checkbox">\n\
                                                    <dd><input  type="checkbox" name="extra' + this.idAppointment + '[]" id="fs' + this.idAppointment + '" value="fs"/><label for="fs' + this.idAppointment + '">Fuel Surcharge</label></dd>\n\
                                                    <dd><input  type="checkbox" name="extra' + this.idAppointment + '[]" id="epu' + this.idAppointment + '" value="epu"/><label for="epu' + this.idAppointment + '">Early Pick Up</label></dd>\n\
                                                    <dd><input  type="checkbox" name="extra' + this.idAppointment + '[]"  id="ldo' + this.idAppointment + '" value="ldo"/><label for="ldo' + this.idAppointment + '">Late Drop Off</label></dd>\n\
                                                    <dd><input  type="checkbox" name="extra' + this.idAppointment + '[]"  id="wend' + this.idAppointment + '" value="wend"/><label for="wend' + this.idAppointment + '">WEnd</label></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd class="checkbox"><input  type="checkbox" name="wtime' + this.idAppointment + '" id="wtime' + this.idAppointment + '"/><label for="wtime' + this.idAppointment + '">Wait Time</label></dd>\n\
                                                    <dd class="none wtime plusMarginLeft"><label class="wthour" for="wthour">Hours:</label></dd>\n\
                                                    <dd class="none wtime"><input type="text" size="5" id="wthour' + this.idAppointment + '" name="wthour' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="none wtime"><label class="wtmin" for="wtmin">Minutes:</label></dd>\n\
                                                    <dd class="none wtime"><input type="text" size="5" id="wtmin' + this.idAppointment + '" name="wtmin' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd><label id="lMiles' + this.idAppointment + '" class="lInput lMiles">' + milesRate + '</label></dd>\n\
                                                    <dd><input value="' + milesFlateRate + '" type="text" size="5" id="tmiles' + this.idAppointment + '" name="tmiles' + this.idAppointment + '" class="inputMiles ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            ' + txtOut + '\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dd class="checkbox"><input  type="checkbox" name="other' + this.idAppointment + '" id="other' + this.idAppointment + '"/><label for="other' + this.idAppointment + '"><?php echo __('Otro'); ?></label></dd>\n\
                                                    <dd class="none ddOther"><input type="text" size="5" id="otherValue' + this.idAppointment + '" name="otherValue' + this.idAppointment + '" class="ui-widget ui-widget-content ui-corner-all"/></dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li class="clear"></li>\n\
                                        </ul>\n\
                                        <label id="statustransp' + this.idAppointment + '"></label>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                </div>\n\
                                </form>\n\
                                <form name="formInt' + this.idAppointment + '" id="formInt' + this.idAppointment + '" action="" method="POST">\n\
                                    <textarea class="hidden" name="commentServInt' + this.idAppointment + '" id="commentServInt' + this.idAppointment + '"></textarea>\n\
                                    <div class="divInt divInt' + this.idAppointment + '">\n\
                                    <div class="menutop ui-widget ui-widget-content  ui-corner-all">\n\
                                        <div class="menutop2 menutop4 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">\n\
                                            <h4 style="text-align: center"><?php echo __('Información de Traducción'); ?></h4>\n\
                                            <div class="clear"></div>\n\
                                        </div>\n\
                                        <button type="button" idAppointment="' + this.idAppointment + '" class="bCommentServInt"><?php echo __('Comentarios'); ?></button>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                    <div class="ui-widget ui-widget-content ui-corner-all contInt">\n\
                                        <ul>\n\
                                            <li>\n\
                                                <dl style="width:100%;">\n\
                                                    <dt><label><?php echo __('Intérprete'); ?>:</label></dt>\n\
                                                    <dd>\n\
                                                        <a class="ratioInterpreter" idInterpreter="' + this.idInterpreters + '" >' + this.interpreterName + ' ' + this.interpreterLastName + '</a>\n\
                                                    </dd>\n\
                                                    ' + noShowInterpreter + '\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Lenguaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.languages + '</dd>\n\
                                                    <dt><label><?php echo __('Tipo de Lenguaje'); ?>:</label></dt>\n\
                                                    <dd>' + this.typelanguage + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            <li>\n\
                                                <dl>\n\
                                                    <dt><label><?php echo __('Tipo de Traducción'); ?>:</label></dt>\n\
                                                    <dd class="typeServicesInt' + this.idAppointment + '">' + this.typeservicesInterpreter + '</dd>\n\
                                                    <dd class="clear"></dd>\n\
                                                </dl>\n\
                                                <div class="clear"></div>\n\
                                            </li>\n\
                                            ' + addInt + summ + '\n\
                                        </ul>\n\
                                        <label id="statustransl' + this.idAppointment + '"></label>\n\
                                        <div class="clear"></div>\n\
                                    </div>\n\
                                </div>\n\
                                </form>';
                            $('div.content4Billing').append(html);
                            //Falta aca !! GoO
                            $('#formInt' + this.idAppointment).validate({
                                submitHandler: function(form) {


                                    $.ajax({
                                        type: "POST",
                                        url: saveBilling,
                                        data: dataString,
                                        dataType: 'json',
                                        success: function(response)
                                        {
                                            $("#detailsbilltransl").html(response.table);
                                            $("#detailsbilltranslfoot").html(response.totalTable);
                                        }
                                    });

                                    $("#divbillTransl").dialog("open");
                                }
                            });
                            $(".calctimeint,.calctraveltimeint").button({
                                icons: {
                                    primary: "ui-icon-clock"
                                },
                                text: false
                            });

                            $(".calctraveltimeint").click(function() {
                                var idAppointment = $(this).attr('idAppointment');
                                var dataString = "totalMileage=" + $('#totalMileage' + idAppointment).val() + "&ajax=ajax";
                                $.ajax({
                                    type: "POST",
                                    url: milesToTime,
                                    data: dataString,
                                    dataType: 'json',
                                    success: function(response)
                                    {
                                        $("#travelTotalTime" + idAppointment).val(response.data);
                                        if ($('#totalMileage' + idAppointment).val() < 40) {
                                            $('#totalMileage' + idAppointment).val('40');
                                        }
                                    }
                                });
                            });

                            $(".calctimeint").click(function() {
                                var idAppointment = $(this).attr('idAppointment');
                                var from = $("#interpretationTimeFrom" + idAppointment).val();
                                var to = $("#interpretationTimeTo" + idAppointment).val();
                                var dataString = "from=" + from + "&to=" + to + "&ajax=ajax";
                                var servicesType = $(".typeServicesInt" + idAppointment).text();
                                //console.log(idAppointment);
                                $.ajax({
                                    type: "POST",
                                    url: calculartiempotrasnc,
                                    data: dataString,
                                    dataType: "json",
                                    success: function(response) {
                                        if (response.msg == "OK") {
                                        switch (servicesType) {
                                            case "Conference Call":
                                                if (response.data < 0.25) {
                                                    msgBox("<?php echo __('Tiempo Calculado'); ?> : " + response.data + " <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Conferencia es: 15 minutos'); ?>", "<?php echo __('Alerta'); ?>");
                                                    $("#interpretationTimeTotalTime" + idAppointment).val("");
                                                } else {
                                                    $("#interpretationTimeTotalTime" + idAppointment).val(response);
                                                }
                                                break;
                                            case "Interpreting":
                                                if (response.data < 1.5) {
                                                    msgBox("<?php echo __('Tiempo Calculado'); ?> : " + response.data + " <?php echo __('horas'); ?><br/> <?php echo __('Tiempo mínimo para Interpretación es: 1.5 horas'); ?>", "<?php echo __('Alerta'); ?>");
                                                    $("#interpretationTimeTotalTime" + idAppointment).val("");
                                                } else {
                                                    $("#interpretationTimeTotalTime" + idAppointment).val(response.data);
                                                }
                                                break;
                                            default:
                                                msgBox("<?php echo __('Document Translation u otro tipo de servicio no requiere Summary Translation.'); ?>"
                                                        + "<?php echo __('Envie un e-mail al administrador del sistema para solucionar este error.'); ?>","<?php echo __('Alerta'); ?>");
                                                break;
                                        }
                                        }
                                    }
                                });
                            });

                            $('#formTrans' + this.idAppointment).validate({
                                submitHandler: function(form) {
                                    alert($(form).serialize());
                                }
                            });

                            $('[name=wtime' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('.wtime').css('display', 'inline');
                                } else {
                                    $(this).parents('dl').find('.wtime').css('display', 'none');
                                }
                                return false;
                            });

                            if (this.app_wtime == 1) {
                                $('[name=wtime' + this.idAppointment + ']').attr('checked', true).trigger('change');
                                $('input[name=wthour' + this.idAppointment + ']').val(this.app_wtime_hours);
                                $('input[name=wtmin' + this.idAppointment + ']').val(this.app_wtime_minutes)
                            }

                            //if(this.)
                            //$('input[name=wtime'+this.idAppointment+']').val(['on']).trigger('change');

                            $('[name=haveFlatRate' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('form').find('.lMiles').text('Flate Rate');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.flateRateBilling').val()
                                            );
                                } else {
                                    $(this).parents('form').find('.lMiles').text('Miles');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.milesBilling').val()
                                            );
                                }
                                return false;
                            });

                            $('[name=other' + this.idAppointment + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'block');
                                } else {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'none');
                                }
                                return false;
                            });

                            $('[name=typeTransDoc' + this.idAppointment + ']').change(function() {
                                if ($(this).val() == 'page') {
                                    $(this).parents('ul').find('dd.transPage').css('display', 'block');
                                    $(this).parents('ul').find('dd.transWord').css('display', 'none');
                                }
                                if ($(this).val() == 'word') {
                                    $(this).parents('ul').find('dd.transWord').css('display', 'block');
                                    $(this).parents('ul').find('dd.transPage').css('display', 'none');
                                }
                                return false;
                            });


                            $('.bCommentServDri, .bCommentServInt').button({
                                icons: {
                                    primary: 'ui-icon-comment'
                                },
                                text: false
                            });
                            $('.editApps').button({
                                icons: {
                                    primary: 'ui-icon-zoomin'
                                },
                                text: false
                            }).click(function(){
                            $('#idAppointmentView').val($(this).attr("idAppointment")).trigger('change');;
                 $('#dialogAppEdit').dialog('open');
                            });

                            idInsuranceCarriers = this.idInsuranceCarriers;
                            idDriversDrop = this.idDriversDrop;
                            idDriverOneWay = this.idDriverOneWay;
                            idInterpreter = this.idInterpreter;
                            idPatient = this.idPatient;
                            idClaim = this.idClaims;
                            appointmentDate = this.appointmentDate;


                            if (this.idInterpreter > 0) {
                                $('.divInt' + this.idAppointment).css('display', 'block');
                            } else {
                                $('.divInt' + this.idAppointment).css('display', 'none');
                            }

                            if (this.idDriverOneWay > 0) {
                                $('.divTrans' + this.idAppointment).css('display', 'block');
                            } else {
                                $('.divTrans' + this.idAppointment).css('display', 'none');
                            }

                            //Loading this Billing Trans e Int???

                            if (this.billingtransp == 1) {
                                $("#statustransp" + this.idAppointment).text('Not able to register again this bill, Transportation');
                                $("#savebilltransp" + this.idAppointment).attr('disabled', true);
                                $("#statusbillingtransp" + this.idAppointment).text('Not able to register again this bill, Transportation');
                                $(".divTrans" + this.idAppointment + " input").attr('disabled', true);
                                /*
                                 //Ajax nomaz sera -.-
                                 dataString = 'type=1'+'&idAppointment='+this.idAppointment;
                                 $.ajax({
                                 type: "POST",
                                 url: '/index.php/admin/billing/getBillingRecord?ajax=ajax',
                                 data: dataString ,
                                 dataType:'json',
                                 async: false,
                                 success: function(rpta) {
                                 
                                 }
                                 });*/
                            } else {
                                idAppsTransp.push(this.idAppointment);
                                $("#statustransp" + this.idAppointment).text('');
                                $("#savebilltransp" + this.idAppointment).removeAttr('disabled');
                                $("#statusbillingtransp" + this.idAppointment).text('');
                                $(".divTrans" + this.idAppointment + " input").removeAttr('disabled');
                            }


                            if (this.billinginter == 1) {
                                $("#statustransl" + this.idAppointment).text('Not able to register again this bill, Translation');
                                $("#savebilltransl" + this.idAppointment).attr('disabled', true);
                                $("#statusbillingtransl" + this.idAppointment).text('Not able to register again this bill, Translation');
                                $(".divInt" + this.idAppointment + " input").attr('disabled', true);
                                //Ajax?...
                            } else {
                                idAppsTransl.push(this.idAppointment);
                                $("#statustransl" + this.idAppointment).text('');
                                $("#savebilltransl" + this.idAppointment).removeAttr('disabled');
                                $("#statusbillingtransl" + this.idAppointment).text('');
                                $(".divInt" + this.idAppointment + " input").removeAttr('disabled');
                            }
                            $("#badjuster").text(this.adjuster);
                            $("#insurance").html("<a id='ratioInsurance'>" + this.insurance + "</a>");
                            //$("#driver1").html("");
                            if (this.idDriversDrop == this.idDriversPickUp) {
                                $("#driver2").html('---');
                            } else {
                                $("#driver2").html("<a id='ratioDriver2' idDriversPickUp='" + this.idDriversPickUp + "'>" + this.driverDropName + " " + this.driverDropLastName + "</a>");
                            }
                            //$("#languages").text(this.languages);
                            //$("#interpreter").html("");

                            if (this.typeservices == "W") {
                                $("#w").show();
                                typeservices = 'w';
                            } else if (this.typeservices == "A") {
                                $("#a").show();
                                typeservices = 'a';
                            } else if (this.typeservices == "ST") {
                                $("#st").show();
                                typeservices = 'st';
                            }

                        });
                    }
                }
            });
            $('.checkbox,.transDoc').buttonset();
        };


        $('#saveBillingTransp,#saveBillingTransl').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });


        $('.bCommentServDri').live('click', function() {
            var idApp = $(this).attr('idAppointment');
            $('#commentServ').val($('#commentServDri' + idApp).val());
            $('#textareaid').val('#commentServDri' + idApp);
            $('#divComment').dialog('open');
        });
        $('.bCommentServInt').live('click', function() {
            var idApp = $(this).attr('idAppointment');
            $('#commentServ').val($('#commentServInt' + idApp).val());
            $('#textareaid').val('#commentServInt' + idApp);
            $('#divComment').dialog('open');
        });

        $('#formCommentServ').validate({
            submitHandler: function(form) {
                $($('#textareaid').val()).val($('#commentServ').val());
                $('#divComment').dialog('close');
            }
        });


        $('#saveBillingTransp').click(function() {
            actionInvoice = 1;
            $("#detailsbilltrans").html('');
            $("#detailsbilltransfoot").html('');
            var stringData = '';
            var arrayAppIds = [];
           
            for (i = 0; i < idAppsTransp.length; i++) {
                if ($('#enableTransp' + idAppsTransp[i]).val() > 0) {
                    stringData += '&' + $('#formTrans' + idAppsTransp[i]).serialize();
                    arrayAppIds.push(idAppsTransp[i]);
                }
            }
            stringData = 'idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "save=false&invoiceDate=" + $('#invoiceDate').val() + "&idPatient=" + idPatient + "&type=1&" + stringData;
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                    data= response.data;
                    $("#detailsbilltrans").html(data.table);
                    $("#detailsbilltransfoot").html(data.totalTable);
                }
            });
            $("#divbillTransp").dialog("open");
        });

        $('#saveBillingTransl').click(function() {
            actionInvoice = 2;
            $("#detailsbilltransl").html('');
            $("#detailsbilltranslfoot").html('');
            var stringData = '';
            var arrayAppIds = [];
            for (i = 0; i < idAppsTransl.length; i++) {
                if ($('#enableTransl' + idAppsTransl[i]).val() > 0) {
                    stringData += '&' + $('#formInt' + idAppsTransl[i]).serialize();
                    arrayAppIds.push(idAppsTransl[i]);
                }
            }
            stringData = "save=false&invoiceDate=" + $('#invoiceDate').val() + '&idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "idPatient=" + idPatient + "&type=2&" + stringData + "&ajax=ajax";
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                    data=response.data;
                    $("#detailsbilltransl").html(data.table);
                    $("#detailsbilltranslfoot").html(data.totalTable);
                }
            });
            $("#divbillTransl").dialog("open");
        });

        $('.detailBilling').live('click', function() {
            var nTr = $(this).parents('tr').get(0);
            idInsuranceCarriers = $(this).attr('idInsuranceCarriers');
            patientId = $(this).attr('idPatient');
            if ($(this).find('span.ui-icon').hasClass('ui-icon-minus')) {
                /* This row is already open - close it */
                $(this).find('span.ui-icon').removeClass('ui-icon-minus');
                $(this).find('span.ui-icon').addClass('ui-icon-plus');
                listBilling.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).find('span.ui-icon').removeClass('ui-icon-plus');
                $(this).find('span.ui-icon').addClass('ui-icon-minus');
                listBilling.fnOpen(nTr, fnFormatDetails(idInsuranceCarriers, patientId), 'padding10');
            }
        });

        $('.detailPatients').live('click', function() {
            var nTr = $(this).parents('tr').get(0);
            idInsuranceCarriers = $(this).attr('idInsuranceCarriers');
            if ($(this).find('span.ui-icon').hasClass('ui-icon-minus')) {
                /* This row is already open - close it */
                $(this).find('span.ui-icon').removeClass('ui-icon-minus');
                $(this).find('span.ui-icon').addClass('ui-icon-plus');
                listBilling.fnClose(nTr);
            } else {
                /* Open this row */
                $(this).find('span.ui-icon').removeClass('ui-icon-plus');
                $(this).find('span.ui-icon').addClass('ui-icon-minus');
                listBilling.fnOpen(nTr, fnFormatDetailsPatient(idInsuranceCarriers), 'padding10');
            }
        });


        $('#actualizarApp').click(function() {
            $('.divTrans' + idAppointment + ' ul li.clear, .divTrans' + idAppointment + ' ul li.liLiftBase, .divTrans' + idAppointment + ' ul li.liCassistance').remove();
            roundTrip = $("input[name='roundtrip']:checked").val() * 1;
            txtExtra1 = '';
            txtExtra2 = '';
            txtOut = '';
            if ($('#lbase').is(':checked')) {
                txtExtra1 = '<li class="liLiftBase">\n\
                                <dl>\n\
                                    <dd>Lift Base:</dd>\n\
                                    <dd><input size="5" class="ui-widget ui-widget-content ui-corner-all" name="lbase' + idAppointment + '" id="lbase' + idAppointment + '" type="text" value="' + roundTrip + '"/></dd>\n\
                                </dl>\n\
                                <div class="clear"></div>\n\
                            </li>';
            }
            if ($('#cassistance').is(':checked')) {
                txtExtra2 = '<li class="liCassistance">\n\
                                <dl>\n\
                                    <dd>[CAR]Assitence</dd>\n\
                                </dl>\n\
                                <div class="clear"></div>\n\
                            </li>';
            }
            switch ($("input[name='services']:checked").val()) {
                case 'A':
                    txtOut = txtExtra1;
                    break;
                case 'W':
                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', 'Car ');
                    break;
                case 'ST':
                    txtOut = txtExtra1 + txtExtra2.replace('[CAR]', '');
                    break;
                default:
                    break;
            }
            $('.divTrans' + idAppointment + ' ul').append(txtOut + '<li class="clear"></li>');

            if ($('#frate').val() * 1 > 0) {
                $('#tmiles' + idAppointment).val($('#frate').val());
                $('#flateRateBilling' + idAppointment).val($('#frate').val());

                $('#haveFlatRate' + idAppointment).attr('checked', true);

            } else {
                $('#tmiles' + idAppointment).val($('#milesBilling' + idAppointment).val());
                $('#haveFlatRate' + idAppointment).removeAttr('checked');
            }
            $('.checkbox').buttonset();
        });

        $('#divbillTransp').dialog({
            modal: true,
            autoOpen: false,
            title:'<?php echo __('Factura Previa'); ?>', 
            resizable: false,
            draggable: false,
            height: 'auto',
            width: 550,
            buttons: {
                "<?php echo __('Guardar Factura'); ?>": function() {

                    var stringData = '';
            var arrayAppIds = [];
//            console.log(idAppsTransp);
            
            for (i = 0; i < idAppsTransp.length; i++) {
                if ($('#enableTransp' + idAppsTransp[i]).val() > 0) {
                    stringData += '&' + $('#formTrans' + idAppsTransp[i]).serialize();
                    arrayAppIds.push(idAppsTransp[i]);
                }
            }
            stringData = 'idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "save=true&invoiceDate=" + $('#invoiceDate').val() + "&idPatient=" + idPatient + "&type=1&" + stringData;
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                      if (response.msg =='OK') {

                        $("#divbillTransp").dialog("close");
//                        location.reload();

                    }
                }
            });
                }
               
            },
            close: function() {

            }
        });
        
        $('#divbillTransl').dialog({
            modal: true,
            title:'<?php echo __('Factura Previa'); ?>',        
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 'auto',
            width: 550,
            buttons: {
                "<?php echo __('Guardar Factura'); ?>": function() {
                    var stringData = '';
            var arrayAppIds = [];
            for (i = 0; i < idAppsTransl.length; i++) {
                if ($('#enableTransl' + idAppsTransl[i]).val() > 0) {
                    stringData += '&' + $('#formInt' + idAppsTransl[i]).serialize();
                    arrayAppIds.push(idAppsTransl[i]);
                }
            }
            stringData = "save=true&invoiceDate=" + $('#invoiceDate').val() + '&idApps=' + arrayAppIds.join(',') + stringData;

            var dataString = "idPatient=" + idPatient + "&type=2&" + stringData + "&ajax=ajax";
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: dataString,
                dataType: 'json',
                success: function(response)
                {
                    if (response.msg =='OK') {

                        $("#divbillTransl").dialog("close");
//                        location.reload();

                    }
                }
            });
                }
               
            },
            close: function() {

            }
        });

         $("#grid-listNotesEmployeers").jqGrid({
            url: listNotes,
            datatype: "json",
            postData: {
                idApp: function() {
                    return idAppointment;
                },
                tname: function() {
                    return 'notesemployeers';
                }
            },
            colNames: ['<?php echo __('ID'); ?>', 
                       '<?php echo __('Notas'); ?>', 
                       '<?php echo __('Fecha de Registro'); ?>', 
                       '<?php echo __('Usuario'); ?>', 
                       '<?php echo __('Acciones'); ?>'],
            colModel: [
                {name: 'idNote', index: 'idNote', hidden: true, width: 50},
                {name: 'notes', index: 'notes', search: true, width: 300},
                {name: 'dateregistration', index: 'dateregistration', search: true, width: 100},
                {name: 'user', index: 'user', width: 150, search: false},
                {name: 'actions', index: 'actions', width: 100, search: false, align: "right"}

            ],
            rowNum: 30,
            rowList: [30, 40, 50],
            height: 200,
            
            pager: 'grid-listNotesEmployeers-paper',
            sortname: 'idNote',
            viewrecords: true,
            sortorder: "desc",
            caption: "<?php echo __('Notas de Empleados'); ?>",
            afterInsertRow: function(row_id, data_id) {

                edit = "<a class=\"edit\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Editar'); ?>\" ><?php echo __('Editar'); ?></a>";
                trash = "<a class=\"trash\" data-idnote=\"" + row_id + "\" title=\"<?php echo __('Eliminar'); ?>\" ><?php echo __('Eliminar'); ?></a>";

                $("#grid-listNotesEmployeers").jqGrid('setRowData', row_id, {actions: edit + trash});
            },
            gridComplete: function() {
                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.trash').button({
                    icons: {primary: ' ui-icon-trash'},
                    text: false
                });

            }
        });
        
         $('#saveEmpNote').click(function() {
            var descriptionEmp = $("#descriptionEmp").val();
            var dataString = "descriptionEmp=" + descriptionEmp + "&idAppointment=" + idAppointment + "&idNotes=" + idNote;
            $.ajax(
                    {
                        type: "POST",
                        url: saveNotesEmployeers,
                        data: dataString,
                        dataType: 'json',
                        success: function()
                        {

                            $("#formNewEmpNote").slideUp(800);
                            $("#newEmpNote").show();
                            $("#closeEmpNote").hide();
                            $("#grid-listNotesEmployeers").trigger("reloadGrid");
                            msgBox("<?php echo __('Nota de Empleado guardada con éxito'); ?>");
                        }
                    });

        });
        
         $("#newEmpNote").click(function() {

            $("#formNewEmpNote").slideDown(800);
            $("#newEmpNote").hide();
            $("#closeEmpNote").show();
            $('#saveEmpNote').val('<?php echo __('Guardar Nota de Empleado'); ?>');
            $('#saveEmpNote').show();
            $('#descriptionEmp').val('');
            idNote = 0;
        });
          $('#newEmpNote').button();
        $('#closeEmpNote').button();
        $('#saveEmpNote').button();
        $("#closeEmpNote").click(function() {
            $("#formNewEmpNote").slideUp(800);
            $("#closeEmpNote").hide();
            $("#newEmpNote").show();
            $('#saveEmpNote').hide();
            $('#updateEmpNote').hide();
        });
        
         $('#divEmployeers').dialog({
            autoOpen: false,
            height: "auto",
            width: 800
        });

    });
</script>
<div >
    <center>
        <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('CITAS POR FACTURAR'); ?></div>
    </center>
</div>
<div class="show_content">

<div id="MarginTable" >
    <label class="frmlbl"><?php echo __('Compañía de Seguros'); ?></label>
    <select name="insurance_carrier" id="insurance_carrier">
        <option value=""><?php echo __('Select ...'); ?></option>                         
        <?php foreach ($insurance_carrier as $ic) { ?>
            <option value="<?php echo $ic['id'] ?>"><?php echo $ic['name'] ?></option>
        <?php } ?>
    </select> 

    <center>  <br> 
        <table id="listbillingconfirm"></table>
        <div id="pagerbillingconfirm"></div>
    </center>

    
</div>


<div id="divbilling" >

    <button type="button" id="backMarginTable"><?php echo __('Atrás'); ?></button>
    <br>


    <div class="menutop ui-widget ui-widget-content ui-corner-all">
        <div class="menutop2 menutop6 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">
            <h4><?php echo __('Facturación'); ?></h4>
            <div class="clear"></div>
        </div>
        <span class="invoiceNumber ui-state-active ui-priority-primary ui-corner-all"><?php echo __('Número de Factura'); ?>:</span>
        <div class="clear"></div>
    </div>
    <table width="100%" border="0">
        <tr>
            <td>
                <label for="invoiceDate" class="frmlbl"><?php echo __('Fecha de Factura'); ?>:</label>
            </td>
            <td>
                <input id="invoiceDate" name="invoiceDate" type="text" value=""/>
            </td>
            <td  class="tituloNormal frmlbl"><?php echo __('Aseguradora'); ?>:</td>
            <td id="insurance"></td>
            <td>
                <label class="frmlbl regiones"><?php echo __('Región'); ?>:</label>
                <select name="tipociudad2" id="tipociudad2">
                    <option value=""><?php echo __('Seleccionar'); ?>...</option>
                    <option value="florida">Florida</option>
                    <option value="georgia">Georgia</option>
                    <option value="other">Out state</option>
                </select>

            </td>
        </tr>
    </table>
    

    <div class="content4Billing">

    </div>

    <div class="clear"></div><hr class="ui-state-default"/>
    <br>

    <button name="saveBillingTransp" id="saveBillingTransp" type="button">
        <?php echo __('Vista Previa Transp.'); ?>
    </button>
    <button name="saveBillingTransl" id="saveBillingTransl" type="button">
         <?php echo __('Vista Previa Traduc.'); ?>
    </button>
    <br>
    <br>
    <br>
</div>
  <div id="divEmployeers">
   
        <center>
         <table id="grid-listNotesEmployeers" ></table>
            <div id="grid-listNotesEmployeers-paper"></div>
        </center>
   
    <p id="pseparator">&nbsp;</p>
    <input type="button" value="<?php echo __('New Employeer Note'); ?>" id="newEmpNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <input type="button" value="<?php echo __('Cerrar'); ?>" id="closeEmpNote" class="fg-button ui-state-default ui-priority-primary ui-corner-all">
    <div id="formNewEmpNote" >
        <form id="validationEmpNote">
            <fieldset>
                <legend><?php echo __('Descripción'); ?></legend>
                <p><label class="frmlbl"><?php echo __('Descripción'); ?>:</label> <textarea name="descriptionEmp" id="descriptionEmp" cols="27" rows="6" ></textarea>  </p>
                <p><input type="button" id="saveEmpNote" value="<?php echo __('Guardar Nota de Empleado'); ?>" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>
                <!--<p><input type="button" id="updateEmpNote" value="Update Employeer Note" class="fg-button ui-state-default ui-priority-primary ui-corner-all">  </p>-->
            </fieldset>
            <input type="hidden" id="idnotesEmployeers" name="idnotesEmployeers">
        </form>
    </div>
</div>
<div id="divratioinsurance" class="dialog" width="800px" height="890px" title="<?php echo __('Tarifas de Aseguradora'); ?>">
    <div id="divRates">
        <form id="dataRates">
            <fieldset id="datosDer" class="divRates">
                <legend><?php echo __('Intérprete'); ?></legend>
                <table>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalRegular">No Show Medical Regular</label>
                                <input  type="text" name="noShowIntMedicalRegular" id="noShowIntMedicalRegular" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalRegularMin">No Show Medical Regular Min Time</label>
                                <input  type="text" name="noShowIntMedicalRegularMin" id="noShowIntMedicalRegularMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalRare">No Show Medical Rare</label>
                                <input  type="text" name="noShowIntMedicalRare" id="noShowIntMedicalRare" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalRareMin">No Show Medical Rare Min Time</label>
                                <input  type="text" name="noShowIntMedicalRareMin" id="noShowIntMedicalRareMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalOther">No Show Medical Other</label>
                                <input  type="text" name="noShowIntMedicalOther" id="noShowIntMedicalOther" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntMedicalOtherMin">No Show Medical Other Min Time</label>
                                <input  type="text" name="noShowIntMedicalOtherMin" id="noShowIntMedicalOtherMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalRegular">No Show Legal Regular</label>
                                <input  type="text" name="noShowIntLegalRegular" id="noShowIntLegalRegular" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalRegularMin">No Show Legal Regular Min Time</label>
                                <input  type="text" name="noShowIntLegalRegularMin" id="noShowIntLegalRegularMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalRare">No Show Legal Rare</label>
                                <input  type="text" name="noShowIntLegalRare" id="noShowIntLegalRare" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalRareMin">No Show Legal Rare Min Time</label>
                                <input  type="text" name="noShowIntLegalRareMin" id="noShowIntLegalRareMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalOther">No Show Legal Other</label>
                                <input  type="text" name="noShowIntLegalOther" id="noShowIntLegalOther" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="noShowIntLegalOtherMin">No Show Legal Other Min Time</label>
                                <input  type="text" name="noShowIntLegalOtherMin" id="noShowIntLegalOtherMin" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="MedicalRegular">Medical Regular:</label>
                                <input  type="text" name="MedicalRegular" id="MedicalRegular" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="MedicalRare">Medical Rare:</label>
                                <input  type="text" name="MedicalRare" id="MedicalRare" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="MedicalOther">Medical Other:</label>
                                <input  type="text" name="MedicalOther" id="MedicalOther" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="LegalRegular">Legal Regular:</label>
                                <input  type="text" name="LegalRegular" id="LegalRegular" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="LegalOther">Legal Other:</label>
                                <input  type="text" name="LegalOther" id="LegalOther" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="LegalRare">Legal Rare:</label>
                                <input  type="text" name="LegalRare" id="LegalRare" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="conferenceCallRegular">Conference Call Regular:</label>
                                <input  type="text" name="conferenceCallRegular" id="conferenceCallRegular" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="conferenceCallOther">Conference Call Other:</label>
                                <input  type="text" name="conferenceCallOther" id="conferenceCallOther" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="conferenceCallRare">Conference Call Rare:</label>
                                <input  type="text" name="conferenceCallRare" id="conferenceCallRare" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <label class="frmlbl" for="travelTime">Travel Time:</label>
                                <input  type="text" name="travelTime" id="travelTime" size="6" value=""/>
                            </p>
                        </td>
                        <td>
                            <p>
                                <label class="frmlbl" for="travelMiles">Mileage:</label>
                                <input  type="text" name="travelMiles" id="travelMiles" size="6" value=""/>
                            </p>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset id="fieldDrivers" class="divRates">
                <legend><?php echo __('Conductor'); ?></legend>
                <table>
                    <tr><td>
                            <p><label class="frmlbl">Gasoline surcharge:</label> <input  type="text" name="gasolinesurcharge" id="gasolinesurcharge" size="6" value="">  </p>
                            <fieldset class="floatLeft">
                                <legend id="amb">Ambulatory</legend>
                                <p>
                                    <label class="frmlbl">Rate:</label>
                                    <input  type="text" name="ambulatoryRate" id="ambulatoryRate" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Waiting Time:</label>
                                    <input  type="text" name="ambulatoryWaitingTime" id="ambulatoryWaitingTime" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate RT:</label>
                                    <input  type="text" name="minimumFlatRateRTAmbulatory" id="minimumFlatRateRTAmbulatory" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate OW:</label>
                                    <input  type="text" name="minimumFlatRateOWAmbulatory" id="minimumFlatRateOWAmbulatory" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">No Show:</label>
                                    <input  type="text" name="ambulatoryNoShow" id="ambulatoryNoShow" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Early Pick Up:</label>
                                    <input  type="text" name="earlyPickUpA" id="earlyPickUpA" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Late Drop Off:</label>
                                    <input  type="text" name="lateDropOffA" id="lateDropOffA" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Weekend Fee:</label>
                                    <input  type="text" name="weekendFeeA" id="weekendFeeA" size="6" value=""/>
                                </p>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend id="wheel">WheelChair</legend>
                                <p>
                                    <label class="frmlbl">Loaded Miles:</label>
                                    <input  type="text" name="wheelChairLoadedMiles" id="wheelChairLoadedMiles" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Lift Base Rate:</label>
                                    <input  type="text" name="wheelChairLiftBaseRate" id="wheelChairLiftBaseRate" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Waiting Time:</label>
                                    <input  type="text" name="wheelChairWaitingTime" id="wheelChairWaitingTime" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Car Assistance:</label>
                                    <input  type="text" name="wheelChairCarAssitance" id="wheelChairCarAssitance" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate RT:</label>
                                    <input  type="text" name="minimumFlatRateRTWheelChair" id="minimumFlatRateRTWheelChair" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate OW:</label>
                                    <input  type="text" name="minimumFlatRateOWWheelChair" id="minimumFlatRateOWWheelChair" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">No Show:</label>
                                    <input  type="text" name="wheelChairNoShow" id="wheelChairNoShow" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Early Pick Up:</label>
                                    <input  type="text" name="earlyPickUpW" id="earlyPickUpW" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Late Drop Off:</label>
                                    <input  type="text" name="lateDropOffW" id="lateDropOffW" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Weekend Fee:</label> <input  type="text" name="weekendFeeW" id="weekendFeeW" size="6" value=""/>
                                </p>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend id="stret">Stretcher</legend>
                                <p>
                                    <label class="frmlbl">Loaded Miles:</label>
                                    <input  type="text" name="stretcherLoadedMiles" id="stretcherLoadedMiles" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Lift Base Rate:</label>
                                    <input  type="text" name="stretcherLiftBaseRate" id="stretcherLiftBaseRate" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Waiting Time:</label>
                                    <input  type="text" name="stretcherWaitingTime" id="stretcherWaitingTime" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Assistance:</label>
                                    <input  type="text" name="stretcherAssistanceS" id="stretcherAssistanceS" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate RT:</label>
                                    <input  type="text" name="minimumFlatRateRTStretcher" id="minimumFlatRateRTStretcher" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Minimum Flat Rate OW:</label>
                                    <input  type="text" name="minimumFlatRateOWStretcher" id="minimumFlatRateOWStretcher" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">No Show:</label>
                                    <input  type="text" name="stretcherNoShow" id="stretcherNoShow" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Early Pick Up:</label>
                                    <input  type="text" name="earlyPickUpS" id="earlyPickUpS" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">Late Drop Off:</label>
                                    <input type="text" name="lateDropOffS" id="lateDropOffS" size="6" value=""/>
                                </p>
                                <p>
                                    <label class="frmlbl">WeekEnd Fee:</label>
                                    <input type="text" name="weekEndFeeS" id="weekEndFeeS" size="6" value=""/>
                                </p>
                                <input type="hidden" name="idInsuranceRates" id="idInsuranceRates"  >
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <span class="updateRate">
                <a id="updateRate" href="#"><?php echo __('Actualizar Tarifas'); ?></a>
            </span>
        </form>
    </div>
</div>

<div id="divratioDriver" class="dialog" width="400px" height="560px" title="<?php echo __('Conductor'); ?>">
    <form action="/" method="POST" id="dataDriverRate" name="dataDriverRate">
        <fieldset id="DriverratesInfo">
            <legend><?php echo __('Tarifas'); ?></legend>
            <p>
                <label class="frmlbl">Ambulatory Rate:</label>
                <input type="text" name="dAmbulatoryRate" id="dAmbulatoryRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Wheel Chair Rate:</label>
                <input type="text" name="wheelChairRate" id="dWheelChairRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Lift Wheel Chair:</label>
                <input type="text" name="liftWheelChair" id="dLiftWheelChair" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Scretcher Rate:</label>
                <input type="text" name="scretcherRate" id="dScretcherRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Lift Stretcher:</label>
                <input type="text" name="liftStretcher" id="dLiftStretcher" size="6"/>
            </p>
            <p>
                <label class="frmlbl">MRT Miles:</label>
                <input type="text" name="MRTMiles" id="dMRTMiles" size="6"/>
            </p>
            <p>
                <label class="frmlbl">MRT Rate:</label>
                <input type="text" name="MRTRate" id="dMRTRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">NS Rate:</label>
                <input type="text" name="NSRate" id="dNSRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Wait Time:</label>
                <input type="text" name="waitTime" id="dWaitTime" size="6"/>
            </p>
            <p>
                <label class="frmlbl">wh MRT:</label>
                <input type="text" name="whMRT" id="dWhMRT" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Dead Head MRate:</label>
                <input type="text" name="deadHeadMRate" id="dDeadHeadMRate" size="6" value=""/>
            </p>
            <p>
                <label class="frmlbl">Flat Rate:</label>
                <input type="text" name="flatRate" id="dFlatRate" size="6" value=""/>
            </p>
            <input type="hidden" name="idDriver" id="idDriver" />
        </fieldset>
        <span class="updateDriverRate">
            <a id="updateDriverRate" href="#"><?php echo __('Actualizar Tarifas'); ?></a>
        </span>
    </form>
</div>

<div id="divratioInterpreter" class="dialog" width="300px" height="340px" title="<?php echo __('Intérprete'); ?>">
    <form action="/" method="POST" id="dataInterpreterRate" name="dataInterpreterRate">
        <fieldset id="InterpreterratesInfo">
            <legend><?php echo __('Tarifas'); ?></legend>
            <p>
                <label class="frmlbl">Interpreting Legal:</label>
                <input type="text" name="interpretingLegal" id="interpretingLegal" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Interpreting Regular:</label>
                <input type="text" name="interpretingRegular" id="interpretingRegular" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Translation Per Page:</label>
                <input type="text" name="translationPerPage" id="translationPerPage" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Translation Per Word:</label>
                <input type="text" name="translationPerWord" id="translationPerWord" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Mileage Rate:</label>
                <input type="text" name="mileageRate" id="mileageRate" size="6"/>
            </p>
            <p>
                <label class="frmlbl">Travel Time:</label>
                <input type="text" name="travelTimer" id="travelTimer" size="6"/>
            </p>
            <input type="hidden" name="idInterpreter" id="idInterpreter" />
        </fieldset>
        <span class="updateInterpreterRate">
            <a id="updateInterpreterRate" href="#"><?php echo __('Actualizar Tarifas'); ?></a>
        </span>
    </form>
</div>

<div id="divbillTransp">
    <form id="formtranspor">
        <label id="statusbillingtransp"></label>
        <fieldset>
            <table width="100%" border="0" class="dashboard" id="transportationbill">
                <thead>
                    <tr>
                        <th><?php echo __('Fecha'); ?></th>
                        <th><?php echo __('Descripción'); ?></th>
                        <th><?php echo __('Cantidad'); ?></th>
                        <th><?php echo __('Tarifa'); ?></th>
                        <th><?php echo __('Monto'); ?> ($)</th>
                    </tr>
                     <tr><th><br></th></tr>
                </thead>
                
                <tfoot id="detailsbilltransfoot">

                </tfoot>
                <tbody id="detailsbilltrans">
                </tbody>

            </table>            
          
        </fieldset>
    </form>
</div>

<div id="divComment" class="dialog" width="400px" title="<?php echo __('Nota'); ?>s">
    <form id="formCommentServ">
        <input name="textareaid" id="textareaid" type="hidden"/>
        <table>
            <tr>
                <td><label><?php echo __('Nota'); ?>:</label></td>
                <td><textarea name="commentServ" cols="30" rows="8" id="commentServ"></textarea></td>
            </tr>
            <tr>
                <td colspan="2"> <button type="submit" class="savComment"><?php echo __('Guardar'); ?></button></td>
            </tr>
        </table>
    </form>
</div>

<div id="divbillTransl" >
    <form id="formtransl">
        <label id="statusbillingtransl"></label>
        <fieldset>
            <table width="100%" border="0" class="dashboard" id="translationbill">
                <thead>
                    <tr>
                        <th><?php echo __('Fecha'); ?></th>
                        <th><?php echo __('Descripción'); ?></th>
                        <th><?php echo __('Cantidad'); ?></th>
                        <th><?php echo __('Tarifa'); ?></th>
                        <th><?php echo __('Monto'); ?> ($)</th>
                    </tr>
                    <tr><th><br></th></tr>
                </thead>
                <tbody id="detailsbilltransl">
                </tbody>
                <tfoot id="detailsbilltranslfoot">

                </tfoot>
               

            </table>   
           
        </fieldset>
    </form>
    
</div>
<?php include ('appview.php'); ?>
</div>