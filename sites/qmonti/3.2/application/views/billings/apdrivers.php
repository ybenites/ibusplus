<style>
    #summary_amount_total, #summary_label_total{float: right;padding: 5px; width: 150px;font-size: 20px;text-align: right;height: 20px;line-height: 20px;}
    td::-moz-focus-inner,span::-moz-focus-inner {padding: 0;border: 0}
    #process_payment{float: left;margin-top: 4px;margin-left: 0;}
    #commentServ{
        width: 270px;
        height: 100px;
        resize: none;
    }
    .block1_theme {
    color: #CCCCCC;
    }
    .ui-dialog{box-shadow: 0 0 5px rgba(0, 0, 0, 0.8);}

    div[aria-labelledby=ui-dialog-title-dialog_pre_view] .ui-dialog-titlebar{display: none;}
    div[aria-labelledby=ui-dialog-title-dialog_pre_view] .ui-dialog-titlebar-close{display: none;}    

    .ui-dialog-content{}
    ul.option_list_pre_billing{position: relative;}

    .billing_options_left dl dd{float: left;}
    .noShowLabel{position: absolute;right: 0;top: 0;margin-right: 0;padding: 3px;}
    .no_show_label{padding: 3px;margin-bottom: 5px;font-weight: bold;text-align: center;}
    .ui-widget-content span.highlight_app,.ui-widget-content span.highlight_app a{color: #915608;font-weight: bold;font-size: 15px;line-height: 15px;}
    h4 span{color: #6EAC2C;font-size: 17px;}
    hr.hr_separate{ border: none; border-top: 4px #c5c5c5 dashed;opacity: 0.55;}
    .ui-jqgrid tr.jqgrow td {white-space: normal;}   
    #driver_id-combobox{margin: 0;float: left;width: 170px;}
    #driver_id-button-combobox{margin: 0;}
    .content_filter{width: 400px;}
    .ui-jqgrid .tree-wrap-ltr{margin-top: 4px;}
    .cont_filter{width: 405px;}

    .div_billing{display: none;width: 1200px;}
    h1,h2,h3,h4{margin: 0;padding: 0;}    
    .menutop h4{line-height: 15px;}
    .menutop{padding: 2px;}
    .menutop2{padding: 5px;}
    .menutop3{width: 527px;float: left}
    .menutop4{width: 560px;float: left}
    .menutop5{width: 559px;float: left}
    .menutop6{width: 955px;float: left}
    .billing_driver_number{width: 213px;float: left;line-height: 25px; padding: 0 5px;text-align: center;float: right;}

    .contTrans ul{list-style: none;margin: 0;padding: 0;}
    .contTrans ul li,.contTrans ul li ,.contApp ul li{list-style: none;padding: 5px 0;display: block;}    

    .contTrans ul li dt label.driver{width: 110px;margin-right: 5px;}
    .contTrans ul li dd label.driverName{width: 445px;font-weight: normal;}
    .contTrans ul li dd label.wthour,.contTrans ul li dd label.wtmin,.contTrans ul li dd label.time{font-weight: normal;padding: 5px 0;margin-right: 5px;}
    .contTrans ul li dd input{margin-right: 5px;}

    .contTrans ul li dt label.tservicest{width: 160px;margin-right: 5px;}
    .contTrans ul li dd label.tservicestName{width: 395px;font-weight: normal;}
    div.middle {
        float: left;
        width: 258px;
        margin-right: 10px
    }
    dd{line-height: 16px;}
    .custom_icon{float: left;margin-right: 3px;}
    .contApp {
        padding: 7px;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .contTrans {
        padding: 7px;
        margin-top: 5px;
    }

    .divTrans {
        float: left;
        width: 580px;
    }
    .bCommentServDri, a.editApps{margin: 0;float: right;}
    .editApps {display: none;}
    .none{display: none;}
    .cont_form{}
    .cont_app{width: 610px;float: left;margin-right: 10px;}
    .cont_app ul li dl dd,.contTrans ul li dl dd{padding-left: 5px;}
    .cont_app ul li dl dt,.contTrans ul li dl dt{padding-bottom: 5px;}
    .info_billing{padding: 7px;}
    .info_billing_wrapper{margin-top: 5px;margin-bottom: 5px;}
    #btn_back_billing{margin-bottom: 5px;}
    .cont_form form{float: left;}
    .wrapeer_item{position: relative;}

    .item_num_value{float: left;width: 45px;height: 45px;text-align: center;line-height: 32px;font-size: 30px;margin-right: 10px;}
    .item_num_value span{font-size: 12px;float: left;width: 100%;height: 12px;line-height: 12px;}

    .contTrans ul li dl.checkbox dd{margin: 0;float: left;padding: 0;}
    .contTrans ul li dl dd input{margin: 0;}
    .bis_label{float: right;width: 115px;font-size: 20px;line-height: 35px;color: #777;}
    .bis_amount{float: right;width: 125px;font-size: 35px;line-height: 35px;padding-right: 10px;text-align: right;color: #777;}
    .billing_item_summary{border-top: 5px #777 dashed;margin-top: 10px;padding: 10px;}
    .ul_items_pre_billing{list-style: none;}
    .ul_items_pre_billing li.item_header_pre_view{border-top: 2px dashed black;}
    .ul_items_pre_billing li:nth-child(2){border-top: none;}
    .ul_items_pre_billing li dl,.ul_items_pre_billing li dl dd{float: left;padding: 5px;}

    li.pre_billing_header dd{text-align: center;}
    dd.pre_billing_date{width: 80px;}
    dd.pre_billing_description{width: 317px;}
    dd.pre_billing_quantity{width: 65px;}
    dd.pre_billing_ratio{width: 65px;}
    dd.pre_billing_amount{width: 65px;}

    dd.dd_type_row{margin-left: 90px;}

    dd.dd_item_date{width: 80px;text-align: center;}
    dd.dd_item_description{width: 319px;}
    dd.dd_item_quantity{width: 65px;text-align: center;}
    dd.dd_item_ratio{width: 65px;text-align: right;}
    dd.dd_item_amount{width: 65px;text-align: right;}

    .wrapper_pre_billing_summary{margin-bottom: 10px;}
    .wrapper_pre_billing_summary div.label_pre_billing_summary,.wrapper_pre_billing_summary div.amount_pre_billing_summary{
        float: right;
        margin-left: 10px;
        padding: 10px;
        text-align: right;
        width: 120px;
        font-size: 20px;
    }
    .ul_items_pre_billing li.pre_billing_header{border: 1px solid #77D5F7;}

</style>
<script type="text/javascript">
    item_row_call_back_type_header = 'item_header';
    item_row_call_back_type_row = 'item_row';
    item_row_call_back_type_note = 'item_comment';
    item_row_call_back_type_summary = 'item_summary';
    var listDriversBilling = "/billings/apdrivers/listDriversBilling";
    var getAppointmentsToBilling = "/billings/apdrivers/getAppointmentsToBilling";
    var saveBilling = "/billings/apdrivers/fnSaveBillingDriver";
    var getNewCodeInvoice = "/billings/apdrivers/getNewCodeInvoice";
    $(document).ready(function() {


        $('#div_billing').hide();

        $("#btn_clean").button();
        $("#btn_clean").click(function() {

            $('#idDriver').val([]);
            $('#idDriver-combobox').val('');
            $("#grid-listDriversBilling").trigger("reloadGrid");

        });

        $('#btn_back_billing').button({
            icons: {
                primary: 'ui-icon-arrowreturnthick-1-w'
            }
        });
        $("#btn_back_billing").click(function() {

            $('#div_billing').hide();
            $('#pre_billing').show();

        });

        $('#preview_billing_driver').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });
        
        $("#preview_billing_driver").click(function() {

            var s_data_forms = '';
            for(i=0;i<a_check_ids.length;i++){                
                s_data_forms += '&'+$('#formTransp'+a_check_ids[i]).serialize();
            }
            s_data_forms = 'a_apps_id='+a_check_ids.join(',')+s_data_forms;

            var s_url_data = "billing_driver_date=" + $('#billing_driver_date').val() + '&driver_id=' + $("#driver_id_hidden").val() + '&' + s_data_forms + '&save=' + $("#save_hidden").val() + "&ajax=ajax";
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: s_url_data,
                dataType:'json',
                success: function(response) {                    
                    if(response.hasOwnProperty('items_row')){      
                        
                                
                        $html_items = '';
                        $("ul.ul_items_pre_billing li:not(:eq(0))").remove();                        
                        $.each(response.items_row,function(){
                            
                            if(this.row_type == item_row_call_back_type_header){
                                $html_items += '<li class="item_header_pre_view">';
                                $html_items += '    <dl>';
                                $html_items += '        <dd class="dd_item_date">'+this.row_date+'</dd>';
                                $html_items += '        <dd class="dd_item_description">'+this.row_description+'</dd>';
                                $html_items += '        <dd class="dd_item_quantity">'+this.row_quantity+'</dd>';
                                $html_items += '        <dd class="dd_item_ratio">'+this.row_ratio+'</dd>';
                                $html_items += '        <dd class="dd_item_amount"><b>'+this.row_amount+'</b></dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                                
                            }else if(this.row_type == item_row_call_back_type_row){
                                $html_items += '<li>';
                                $html_items += '    <dl>';                                
                                $html_items += '        <dd class="dd_item_description dd_type_row">'+this.row_description+'</dd>';
                                $html_items += '        <dd class="dd_item_quantity">'+this.row_quantity+'</dd>';
                                $html_items += '        <dd class="dd_item_ratio">'+this.row_ratio+'</dd>';
                                $html_items += '        <dd class="dd_item_amount"><b>'+this.row_amount+'</b></dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                            
                            }else if(this.row_type == item_row_call_back_type_note){
                                $html_items += '<li>';
                                $html_items += '    <dl>';
                                $html_items += '        <dd class="dd_item_description dd_type_row">Note: '+this.row_description+'</dd>';
                                $html_items += '    </dl>';
                                $html_items += '    <div class="clear"></div>';
                                $html_items += '</li>';
                            
                            }else if(this.row_type == item_row_call_back_type_summary){                                
                                $('.label_pre_billing_summary').html(this.row_description);
                                $('.amount_pre_billing_summary').html(this.row_amount);
                            }
                        });
                    }                    
                    $($html_items).insertAfter("ul.ul_items_pre_billing li:eq(0)");
                    $("#dialog_pre_view").dialog('open');
                }
            });
        });

        $("#billing_driver_date").datepicker({
            dateFormat: "mm/dd/yy"
        });



        $('#idDriver').combobox({
            selected: function() {
                $("#grid-listDriversBilling").trigger('reloadGrid');
            }
        });

        gridlistDriversBilling = $("#grid-listDriversBilling").jqGrid({
            url: listDriversBilling,
            datatype: "json",
            postData: {
                idDriver: function() {
                    return $("#idDriver").val()
                }
            },
            colNames: ['idBilling', 'idDriver', 
                       '<?php echo __('ID'); ?>', 
                       '<?php echo __('Conductor'); ?>', 
                       '<?php echo __('Fecha'); ?>', 
                       '<?php echo __('Hora'); ?>', 
                       '<?php echo __('Paciente'); ?>', 
                       '<?php echo __('Desde'); ?>', 
                       '<?php echo __('A'); ?>', 
                       '<?php echo __('Tipo de Facturación'); ?>', 
                       '<?php echo __('Opción'); ?>', 
                      ],
            colModel: [
                {name: 'billing_id', index: 'billing_id', hidden: true, width: 70},
                {name: 'driver_id', index: 'driver_id', hidden: true, width: 70},
                {name: 'app_id', index: 'app_id', search: true, width: 70},
                {name: 'driverfullname', index: 'driverfullname', hidden: true, width: 0},
                {name: 'app_date', index: 'app_date', search: true, width: 70},
                {name: 'app_time', index: 'app_time', search: false, width: 70},
                {name: 'pat_fullname', index: 'pat_fullname', search: true, width: 150},
                {name: 'from_address', index: 'from_address', width: 200, search: false},
                {name: 'to_address', index: 'to_address', width: 200, search: false},
                {name: 'type_billing_driver', index: 'type_billing_driver', width: 100, search: false},
                {name: 'actions', index: 'actions', width: 70, align: "right", sortable: false, search: false}
            ],
            grouping: true,
            groupingView: {
                groupField: ['driverfullname'],
                groupColumnShow: [false],
                groupText: ['<div class="titleLeft"><b> {0} ({1})</b></div>'
                            + '<div class="titleRight"><a  class="selectbilling"><?php echo __('Facturar'); ?></a></div>'],
                groupCollapse: false
            },
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 450,
            pager: 'grid-listDriversBilling-paper',
            sortname: 'app_id',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('LISTA DE PAGOS PENDIENTES A CONDUCTOR'); ?>",
            gridComplete: function() {

                var aData = gridlistDriversBilling.getDataIDs();
                $.each(aData, function() {

                    check = "<input class=\"check\" ";
                    check += "data-id=\"" + gridlistDriversBilling.getCell(this, 'app_id') + "\" ";
                    check += "data-driver_id=\"" + gridlistDriversBilling.getCell(this, 'driver_id') + "\" ";
                    check += "name=\"check_" + gridlistDriversBilling.getCell(this, 'driver_id') + "[]\" ";
                    check += "id=\"check_" + this + "\" ";
                    check += "value=\"" + gridlistDriversBilling.getCell(this, 'app_id') + "\" ";
                    check += "type=\"checkbox\" />";
                    check += "<label for=\"check_" + this + "\"><?php echo __('Seleccionar'); ?></label>";
                    gridlistDriversBilling.setRowData(this
                            , {
                        actions: check
                    });

                });

                $('.check').button({
                    icons: {primary: 'ui-icon-check'},
                    text: false
                })
                $('.selectbilling').button({
                    icons: {primary: 'ui-icon-check'}
                });
            }
        });

        $("#grid-listDriversBilling").on('click', 'a.selectbilling', function() {
            a_check_ids = [];
            $.ajax({
                type: "POST",
                url: getNewCodeInvoice,
                dataType: 'json',
                async: false,
                success: function(response) {

                    $("#billing_driver_number").text(response.newbilling);
                }

            });
            $("#grid-listDriversBilling input:checkbox").each(function() {

                if (this.checked) {
                    a_check_ids.push($(this).val());
                    $("#driver_id_hidden").val($(this).attr('data-driver_id'));
                    console.log($(this).attr('data-driver_id'));
           }
            });

            if (a_check_ids.length == 0) {
                msgBox('<?php echo __('Seleccionar una cita para facturar.'); ?>'
                        , '<?php echo __('Alerta'); ?>');
            } else {
                $('#pre_billing').hide();
                $('#div_billing').show();
                $('div.content_to_billing').html('');
                //$('input:hidden#driver_id_hidden').val(i_driver_id);
                $.ajax({
                    url: getAppointmentsToBilling,
                    type: "POST",
                    data: {a_check_ids: a_check_ids.toString(),
                        driver_id: $("#driver_id_hidden").val()},
                    dataType: "json",
                    success: function(j_response) {
                        i_count = 0;
                        $.each(j_response, function() {
                            i_count++;

                            var hr = '<div class="clear"></div><hr class="hr_separate"/>';
                            i_billing_transp_id = this.app_id;

                            s_no_show_driver_html = '';

                            if (this.app_no_show_driver == 1) {
                                s_no_show_driver_html = '<div class="no_show_label ui-corner-all ui-state-error" id="noShowDriver' + this.app_id + '">NO SHOW</div>';
                            }

                            switch (this.pat_region) {
                                case 'florida':
                                    s_region = 'Florida';
                                    break;
                                case 'georgia':
                                    s_region = 'Georgia';
                                    break;
                                case 'other':
                                    s_region = 'Out state'
                                    break;
                                default:
                                    s_region = '<b class="balert">' + this.pat_region + '</b>';
                                    break;
                            }

                            i_round_trip = this.app_type_travel == 'Round Trip' ? 2 : 1;
                            s_extra1_html = '';
                            s_extra2_html = '';
                            s_out_html = '';
                            //console.log(this);
                            if (this.app_lbase == 1) {
                                s_extra1_html = '<li class="liLiftBase">';
                                s_extra1_html += '<dl>';
                                s_extra1_html += '<dd>Lift Base:</dd>';
                                s_extra1_html += '<dd><input size="5" class="ui-widget ui-widget-content ui-corner-all" name="lbase' + this.app_id + '" id="lbase' + this.app_id + '" type="text" value="' + i_round_trip + '"/></dd>';
                                s_extra1_html += '</dl>';
                                s_extra1_html += '<div class="clear"></div>';
                                s_extra1_html += '</li>';
                            }

                            if (this.app_cassistance == 1) {
                                s_extra2_html = '<li class="liCassistance">';
                                s_extra2_html += '<dl>';
                                s_extra2_html += '<dd>[CAR]Assitence</dd>';
                                s_extra2_html += '</dl>';
                                s_extra2_html += '<div class="clear"></div>';
                                s_extra2_html += '</li>';
                            }

//                            switch(this.app_type_service){
//                                case 'A':
//                                    s_service_name = 'Ambulatory';
//                                    s_out_html = s_extra1_html;
//                                    break;
//                                case 'W':
//                                    s_service_name = 'Wheelchair';
//                                    s_out_html = s_extra1_html+s_extra2_html.replace('[CAR]', 'Car ');
//                                    break;
//                                case 'ST':
//                                    s_service_name = 'Stretcher';
//                                    s_out_html = s_extra1_html+s_extra2_html.replace('[CAR]', '');
//                                    break;
//                                default:
//                                    s_service_name = '';
//                                    break;
//                            }

                            s_service_name = this.app_type_service;
                            s_out_html = s_extra1_html;
//                                    
                            var html_miles_billing = '';

                            if (this.app_frate > 0) {
                                s_miles_rate = 'Flate Rate';
                                b_checked_flate_rate = ' checked ';
                                i_miles_flate_rate = this.app_frate;
                            } else {
                                s_miles_rate = 'Miles';
                                b_checked_flate_rate = '';
                                i_miles_flate_rate = 0;
//                                if(this.app_miles_billing > 0){
////                                  
//                                    html_miles_billing += '<li>';
//                                    html_miles_billing += '    <dl>';
//                                    html_miles_billing += '        <dt><label>Miles Billing:</label></dt>';
//                                    html_miles_billing += '        <dd>'+this.app_miles_billing+'</dd>';
//                                    html_miles_billing += '        <dd class="clear"></dd>';
//                                    html_miles_billing += '    </dl>';
//                                    html_miles_billing += '    <div class="clear"></div>';
//                                    html_miles_billing += '</li>';
//                                }
//                                //else{
//                                    i_miles_flate_rate = this.app_miles;
////                                }
                            }

                            $('#current_driver').html(this.dr_pick_up_name + ' ' + this.dr_pick_up_last_name);
                            $('#billing_driver_date').val(this.d_current_date);

                            i_app_travel_billing = this.app_travel_billing.split('|')[0];
                            s_app_travel_billing = this.app_travel_billing.split('|')[1];
                            html_item_li_driver_2 = '';
                            html_driver_1_span_bold_ini = '';
                            html_driver_1_span_bold_end = '';
                            s_label_driver_1 = 'Driver 1';
                            html_span_icon_driver_1 = '';

                            html_from_app = '<li>';
                            html_from_app += '    <dl>';
                            html_from_app += '        <dt><label><?php echo __('Desde'); ?>:</label></dt>';
                            html_from_app += '        <dd><span class="custom_icon ui-icon ui-icon-flag"></span>' + this.pl_alias + ': ' + this.pl_address + '</dd>';
                            html_from_app += '        <dd class="clear"></dd>';
                            html_from_app += '    </dl>';
                            html_from_app += '    <div class="clear"></div>';
                            html_from_app += '</li>';
                            html_to_app = '<li>';
                            html_to_app += '    <dl>';
                            html_to_app += '        <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                            html_to_app += '        <dd><span class="custom_icon ui-icon ui-icon-arrowreturnthick-1-e"></span>' + this.tl_contact + ': ' + this.tl_address + '</dd>';
                            html_to_app += '        <dd class="clear"></dd>';
                            html_to_app += '    </dl>';
                            html_to_app += '    <div class="clear"></div>';
                            html_to_app += '</li>';

                            if (i_app_travel_billing == 1) {
                                html_span_icon_driver_1 = '<span class="custom_icon ui-icon ui-icon-person"></span>';
                                html_driver_1_span_bold_ini = '<span class="highlight_app">';
                                html_driver_1_span_bold_end = '</span>';
                                if (i_round_trip == 1) {
                                    s_label_driver_1 = '<?php echo __('Conductor'); ?>'+':';
                                } else {
                                    s_label_driver_1 = '<?php echo __('Conductor'); ?> 1';

                                    html_item_li_driver_2 += '<li>';
                                    html_item_li_driver_2 += '    <dl style="width:100%;">';
                                    html_item_li_driver_2 += '        <dt><label><?php echo __('Conductor'); ?> 2:</label></dt>';
                                    html_item_li_driver_2 += '        <dd>';
                                    html_item_li_driver_2 += '            <a class="ratioDriver2" data-app_driver_drop_id="' + this.app_driver_drop_id + '">' + this.dr_drop_name + ' ' + this.dr_drop_last_name + '</a>';
                                    html_item_li_driver_2 += '        </dd>';
                                    html_item_li_driver_2 += '        <dd class="clear"></dd>';
                                    html_item_li_driver_2 += '    </dl>';
                                    html_item_li_driver_2 += '    <div class="clear"></div>';
                                    html_item_li_driver_2 += '</li>';
                                }
                            } else if (i_app_travel_billing == 2) {
                                s_label_driver_1 = '<?php echo __('Conductor'); ?> 1';
                                html_item_li_driver_2 += '<li>';
                                html_item_li_driver_2 += '    <dl style="width:100%;">';
                                html_item_li_driver_2 += '        <dt><label><?php echo __('Conductor'); ?> 2:</label></dt>';
                                html_item_li_driver_2 += '        <dd>';
                                html_item_li_driver_2 += '            <span class="custom_icon ui-icon ui-icon-person"></span>';
                                html_item_li_driver_2 += '            <span class="highlight_app">';
                                html_item_li_driver_2 += '               <a class="ratioDriver2" data-app_driver_drop_id="' + this.app_driver_drop_id + '">' + this.dr_drop_name + ' ' + this.dr_drop_last_name + '</a>';
                                html_item_li_driver_2 += '            </span>';
                                html_item_li_driver_2 += '        </dd>';
                                html_item_li_driver_2 += '        <dd class="clear"></dd>';
                                html_item_li_driver_2 += '    </dl>';
                                html_item_li_driver_2 += '    <div class="clear"></div>';
                                html_item_li_driver_2 += '</li>';

                                html_from_app = '<li>';
                                html_from_app += '    <dl>';
                                html_from_app += '        <dt><label><?php echo __('Desde'); ?>:</label></dt>';
                                html_from_app += '        <dd><span class="custom_icon ui-icon ui-icon-refresh"></span><span class="custom_icon ui-icon ui-icon-flag"></span>' + this.tl_contact + ': ' + this.tl_address + '</dd>';
                                html_from_app += '        <dd class="clear"></dd>';
                                html_from_app += '    </dl>';
                                html_from_app += '    <div class="clear"></div>';
                                html_from_app += '</li>';
                                html_to_app = '<li>';
                                html_to_app += '    <dl>';
                                html_to_app += '        <dt><label><?php echo __('Hasta'); ?>:</label></dt>';
                                html_to_app += '        <dd><span class="custom_icon ui-icon ui-icon-refresh"></span><span class="custom_icon ui-icon ui-icon-arrowreturnthick-1-e"></span>' + this.pl_alias + ': ' + this.pl_address + '</dd>';
                                html_to_app += '        <dd class="clear"></dd>';
                                html_to_app += '    </dl>';
                                html_to_app += '    <div class="clear"></div>';
                                html_to_app += '</li>';

                            } else if (i_app_travel_billing == 3) {
                                s_label_driver_1 = '<?php echo __('Conductor'); ?>';
                                html_span_icon_driver_1 = '<span class="custom_icon ui-icon ui-icon-person"></span>';
                                html_driver_1_span_bold_ini = '<span class="highlight_app">';
                                html_driver_1_span_bold_end = '</span>';
                            }
                            var html = hr;
                            html += '<div class="wrapeer_item">';
                            html += '    <input id="enableTransp' + this.app_id + '" name="enableTransp' + this.app_id + '" type="hidden" value="" />';
                            html += '    <div class="cont_app">';
                            html += '       <div class="menutop ui-widget ui-widget-content ui-corner-all">';
                            html += '           <div class="menutop2 menutop5 right ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">';
                            html += '               <h4><?php echo __('Información de la Cita'); ?> : <span>' + this.app_alias + '</span></h4>';
                            html += '               <div class="clear"></div>';
                            html += '           </div>';
                            html += '           <a data-app_id="' + this.app_id + '" class="editApps"><?php echo __('Editar Cita'); ?></a>';
                            html += '           <div class="clear"></div>';
                            html += '        </div>';
                            html += '        <div class="ui-widget ui-widget-content ui-corner-all contApp">';
                            html += '           <div class="item_num_value ui-state-hover ui-corner-all"><span>Item</span>' + i_count + '</div>';
                            html += '           <div class="middle">';
                            html += '                <ul>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Nombre de Paciente'); ?>:</label></dt>';
                            html += '                           <dd>' + this.pat_full_name + '</dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Autorizado por'); ?>:</label></dt>';
                            html += '                           <dd>' + this.app_auth + '</dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Fecha de Servicio'); ?>:</label></dt>';
                            html += '                           <dd><span class="highlight_app">' + this.app_date + '</span></dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Hora'); ?>:</label></dt>';
                            html += '                           <dd><span class="highlight_app">' + this.app_time + '</span></dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Llamado por'); ?>:</label></dt>';
                            html += '                           <dd>' + this.app_who_calls + '</dd>';
                            html += '                           <dd class="clear"></dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                   <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Nombre de la Compañia'); ?>:</label></dd>';
                            html += '                           <dd>' + this.app_company_name + '</dd>';
                            html += '                           <dd class="clear"></dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </li>';
                            html += '                </ul>';
                            html += '           </div>';
                            html += '           <div class="middle">';
                            html += '                <ul>';
                            html += '                    <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Región'); ?>:</label></dt>';
                            html += '                           <dd>' + s_region + '</dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                    </li>';
                            html += '                    <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Tipo de Cita'); ?>:</label></dt>';
                            html += '                           <dd>' + this.app_type + '</dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                    </li>';
                            html += '                    <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Descripción de la Cita'); ?>:</label></dt>';
                            html += '                           <dd>' + this.tapp_name + '</dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                    </li>';
                            html += '                    <li>';
                            html += '                       <dl>';
                            html += '                           <dt><label><?php echo __('Facturación de Viaje'); ?>:</label></dt>';
                            html += '                           <dd><span class="highlight_app">' + s_app_travel_billing + '</span></dd>';
                            html += '                       </dl>';
                            html += '                       <div class="clear"></div>';
                            html += '                    </li>';
                            html += '                </ul>';
                            html += '           </div>';
                            html += '           <div class="clear"></div>';
                            html += '           <div class="billing_item_summary">';
                            html += '               <div class="bis_amount" id="bis_amount' + this.app_id + '"></div>';
                            html += '               <div class="bis_label"><?php echo __('Pre Monto'); ?>:</div>';
                            html += '               <div class="clear"></div>';
                            html += '           </div>';
                            html += '           <div class="clear"></div>';
                            html += '        </div>';
                            html += '    </div>';
                            html += '    <div class="cont_form">';
                            html += '        <form name="formTransp' + this.app_id + '" id="formTransp' + this.app_id + '" action="" method="POST">';
                            html += '           <textarea class="none" name="commentServDri' + this.app_id + '" id="commentServDri' + this.app_id + '"></textarea>';
                            html += '           <input type="hidden" id="milesBilling' + this.app_id + '" class="milesBilling" name="milesBilling' + this.app_id + '" value="' + i_miles_flate_rate + '" />';
                            html += '           <input type="hidden" id="flateRateBilling' + this.idAppointment + '" class="flateRateBilling" name="flateRateBilling' + this.app_id + '" value="' + this.app_frate + '" />';
                            html += '           <div class="divTrans divTrans' + this.app_id + '">';
                            html += '               <div class="menutop ui-widget ui-widget-content  ui-corner-all">';
                            html += '                   <div class="menutop2 right menutop3 ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">';
                            html += '                       <h4 style="text-align: center"><?php echo __('Información de Transporte'); ?></h4>';
                            html += '                       <div class="clear"></div>';
                            html += '                   </div>';
                            html += '                   <button type="button" data-app_id="' + this.app_id + '" class="bCommentServDri"><?php echo __('Facturación de Transporte'); ?></button>';
                            html += '                   <div class="clear"></div>';
                            html += '               </div>';
                            html += '               <div class="ui-widget ui-widget-content ui-corner-all contTrans">';
                            html += '                   <ul class="option_list_pre_billing">';
                            html += '                       <li>';
                            html += '                           <dl style="width:100%;">';
                            html += '                               <dt><label>' + s_label_driver_1 + ':</label></dt>';
                            html += '                               <dd>';
                            html += '                                   ' + html_span_icon_driver_1 + html_driver_1_span_bold_ini + '<a class="ratioDriver1" data-app_driver_pickup_id="' + this.app_driver_pickup_id + '">' + this.dr_pick_up_name + ' ' + this.dr_pick_up_last_name + '</a>' + html_driver_1_span_bold_end;
                            html += '                               </dd>';
                            html += '                               <dd class="checkbox noShowLabel">';
                            html += '                                   ' + s_no_show_driver_html;
                            html += '                                   <input class="checkbox" type="checkbox" name="haveFlatRate' + this.app_id + '" id="haveFlatRate' + this.app_id + '"' + b_checked_flate_rate + '/><label for="haveFlatRate' + this.app_id + '">Flate Rate</label>';
                            html += '                               </dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       ' + html_item_li_driver_2;
                            html += '                       <li>';
                            html += '                           <dl>';
                            html += '                               <dt><label><?php echo __('Tipo de Transporte'); ?>:</label></dt>';
                            html += '                               <dd>' + s_service_name + '</dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += html_from_app;
                            html += html_to_app;
                            html += '                       <li>';
                            html += '                           <dl>';
                            html += '                               <dt><label>Travel:</label></dt>';
                            html += '                               <dd><span class="custom_icon ui-icon ui-icon-transferthick-e-w"></span>' + this.app_type_travel + '</dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       <li>';
                            html += '                           <dl>';
                            html += '                               <dt><label><?php echo __('Millas'); ?>:</label></dt>';
                            html += '                               <dd>' + this.app_miles + '</dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>' + html_miles_billing;
                            html += '                       <li>';
                            html += '                           <dl class="checkbox">';
                            html += '                               <dd><input  type="checkbox" name="extra' + this.app_id + '[]" id="fs' + this.app_id + '" value="fs"/><label for="fs' + this.app_id + '">Fuel Surcharge</label></dd>';
                            html += '                               <dd><input  type="checkbox" name="extra' + this.app_id + '[]" id="epu' + this.app_id + '" value="epu"/><label for="epu' + this.app_id + '">Early Pick Up</label></dd>';
                            html += '                               <dd><input  type="checkbox" name="extra' + this.app_id + '[]"  id="ldo' + this.app_id + '" value="ldo"/><label for="ldo' + this.app_id + '">Late Drop Off</label></dd>';
                            html += '                               <dd><input  type="checkbox" name="extra' + this.app_id + '[]"  id="wend' + this.app_id + '" value="wend"/><label for="wend' + this.app_id + '">WEnd</label></dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       <li class="billing_options_left">';
                            html += '                           <dl>';
                            html += '                               <dd class="checkbox"><input  type="checkbox" name="wtime' + this.app_id + '" id="wtime' + this.app_id + '"/><label for="wtime' + this.app_id + '"><?php echo __('Tiempo de Espera'); ?></label></dd>';
                            html += '                               <dd class="none wtime plusMarginLeft"><label class="wthour" for="wthour">Hours:</label></dd>';
                            html += '                               <dd class="none wtime"><input type="text" size="5" id="wthour' + this.app_id + '" name="wthour' + this.app_id + '" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                            html += '                               <dd class="none wtime"><label class="wtmin" for="wtmin">Minutes:</label></dd>';
                            html += '                               <dd class="none wtime"><input type="text" size="5" id="wtmin' + this.app_id + '" name="wtmin' + this.app_id + '" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       <li>';
                            html += '                           <dl>';
                            html += '                               <dd><label id="lMiles' + this.app_id + '" class="lInput lMiles">' + s_miles_rate + '</label></dd>';
                            html += '                               <dd><input value="' + i_miles_flate_rate + '" type="text" size="5" id="tmiles' + this.app_id + '" name="tmiles' + this.app_id + '" class="inputMiles ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       ' + s_out_html + '';
                            html += '                       <li class="billing_options_left">';
                            html += '                           <dl>';
                            html += '                               <dd class="checkbox"><input  type="checkbox" name="other' + this.app_id + '" id="other' + this.app_id + '"/><label for="other' + this.app_id + '"><?php echo __('Otro'); ?></label></dd>';
                            html += '                               <dd class="none ddOther"><input type="text" size="5" id="otherValue' + this.app_id + '" name="otherValue' + this.app_id + '" class="ui-widget ui-widget-content ui-corner-all text_update"/></dd>';
                            html += '                               <dd class="clear"></dd>';
                            html += '                           </dl>';
                            html += '                           <div class="clear"></div>';
                            html += '                       </li>';
                            html += '                       <li class="clear"></li>';
                            html += '                   </ul>';
                            html += '                   <label id="statustransp' + this.app_id + '"></label>';
                            html += '                   <div class="clear"></div>';
                            html += '               </div>';
                            html += '           </div>';
                            html += '        </form>';
                            html += '        <div class="clear"></div>';
                            html += '    </div>';
                            html += '    <div class="clear"></div>';
                            html += '</div>';

                            $('#formTransp' + this.app_id).validate({
//                                submitHandler: function(form) {
//                                    alert($(form).serialize());
//                                }
                            });

                            i_app_insurance_carriers_id = this.app_insurance_carriers_id;
                            i_app_driver_drop_id = this.app_driver_drop_id;
                            i_app_driver_pickup_id = this.app_driver_pickup_id;

                            i_pat_id = this.pat_id;
                            i_cl_id = this.cl_id;
                            d_app_date = this.app_date;

                            if (this.app_driver_pickup_id > 0) {
                                $('.divTrans' + this.app_id).css('display', 'block');
                            } else {
                                $('.divTrans' + this.app_id).css('display', 'none');
                            }
                            $('div.content_to_billing').append(html);

                            $('input[name=wtime' + this.app_id + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('.wtime').css('display', 'inline');
                                } else {
                                    $(this).parents('dl').find('.wtime').css('display', 'none');
                                }

                                fnUpdateAmount();

                                return false;
                            });

                            $('input[name=haveFlatRate' + this.app_id + ']').change(function() {
                                if ($(this).is(':checked')) {

                                    $(this).parents('form').find('.lMiles').text('Flate Rate');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.flateRateBilling').val()
                                            );
                                } else {
                                    $(this).parents('form').find('.lMiles').text('Miles');
                                    $(this).parents('form').find('.inputMiles').val(
                                            $(this).parents('form').find('.milesBilling').val()
                                            );
                                }
                                fnUpdateAmount();
                                return false;
                            });

                            $('input[name=other' + this.app_id + ']').change(function() {
                                if ($(this).is(':checked')) {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'block');
                                } else {
                                    $(this).parents('dl').find('dd.ddOther').css('display', 'none');
                                }
                                fnUpdateAmount();
                                return false;
                            });
//                            a_check_ids.push(this.app_id);
                            $("#statustransp" + this.app_id).text('');
                            $("#savebilltransp" + this.app_id).removeAttr('disabled');
                            $("#statusbillingtransp" + this.app_id).text('');
                            $(".divTrans" + this.app_id + " input").removeAttr('disabled');


                            //$("#driver1").html("");
                            if (this.app_driver_drop_id == this.app_driver_pickup_id) {
                                $("#driver2").html('---');
                            } else {
                                $("#driver2").html("<a id='ratioDriver2' data-app_driver_drop_id='" + this.app_driver_drop_id + "'>" + this.dr_drop_name + " " + this.dr_drop_last_name + "</a>");
                            }
                            //$("#languages").text(this.languages);
                            //$("#interpreter").html("");

                            if (this.app_type_service == "W") {
                                $("#w").show();
                                s_type_service = 'w';
                            } else if (this.app_type_service == "A") {
                                $("#a").show();
                                s_type_service = 'a';
                            } else if (this.app_type_service == "ST") {
                                $("#st").show();
                                s_type_service = 'st';
                            }

                            if (this.app_wtime == 1) {
                                $('#wtime' + this.app_id).attr('checked', true).trigger('change');
                                $('#wthour' + this.app_id).val(this.app_wtime_hours);
                                $('#wtmin' + this.app_id).val(this.app_wtime_minutes);
                            }

                            $('.checkbox').buttonset();
                            $('.checkbox input[type=checkbox]').live('click', function() {
                                fnUpdateAmount();
                            });
                            $('.bCommentServDri').button({
                                icons: {
                                    primary: 'ui-icon-comment'
                                },
                                text: false
                            });
                            $('.editApps').button({
                                icons: {
                                    primary: 'ui-icon-zoomin'
                                },
                                text: false
                            });
                            $('input.text_update').live('keydown', function(e) {
                                if (e.keyCode == 13)
                                    fnUpdateAmount();
                            });
                        });
                        $('.div_pre_billing').slideUp(500, function() {
                            $('.div_billing').slideDown(500)
                        });
                        fnUpdateAmount();

                    }
                });
            }

        });

        function fnUpdateAmount() {
            var o_response = fnGetPreBilling();

            if (o_response.hasOwnProperty('pre_sum')) {
                $.each(o_response.pre_sum, function(i_index_app, f_amount) {
                    $('#bis_amount' + i_index_app).html(f_amount);
                });
                $('#summary_amount_total').html(o_response.pre_total);
            }
        }

        function fnGetPreBilling() {
            var s_data_forms = '';
            for (i = 0; i < a_check_ids.length; i++) {
                s_data_forms += '&' + $('#formTransp' + a_check_ids[i]).serialize();
            }
            s_data_forms = 'a_apps_id=' + a_check_ids.join(',') + s_data_forms;

            var s_url_data = "billing_driver_date=" + $('#billing_driver_date').val() + '&driver_id=' + $("#driver_id_hidden").val() + '&' + s_data_forms + '&save=' + $("#save_hidden").val() + "&ajax=ajax";
            var o_return = new Object();
            $.ajax({
                type: "POST"
                        , url: saveBilling
                        , data: s_url_data
                        , async: false
                        , dataType: 'json'
                        , success: function(response) {
                    o_return = response;
                }
            });
            return o_return;
        }
        
          $('#process_payment').click(function(){
            var s_data_forms = '';
            for(i=0;i<a_check_ids.length;i++){                
                s_data_forms += '&'+$('#formTransp'+a_check_ids[i]).serialize();
            }
            s_data_forms = 'a_apps_id='+a_check_ids.join(',')+s_data_forms;

            var s_url_data = "billing_driver_date="+$('#billing_driver_date').val()+'&driver_id='+$("#driver_id_hidden").val()+'&'+s_data_forms +"&save=true&ajax=ajax";
            
            $.ajax({
                type: "POST",
                url: saveBilling,
                data: s_url_data,
                dataType:'json',
                beforeSend : function(){
                    $("#loading").dialog('open');
                },
                complete : function(){
                    $("#loading").dialog('close');
                },
                success: function(response) {
                    o_return = response;
                    if(o_return.billing_driver_id > 0){

                        $('.div_billing').slideUp(500, function(){$('.div_pre_billing').slideDown(500)});
                        gridlistDriversBilling.trigger('reloadGrid');
                        $("#dialog_pre_view").dialog('close');
                        $('#div_billing').hide();
                        $('#pre_billing').show();
                    }                   
                }
            });
        });
        
        $("#dialog_pre_view").dialog({
            autoOpen: false
                    , height: 'auto'
                    , width: '700'
                    , modal: true
                    , resizable: false
                    , draggable: false
        });

        $('.bCommentServDri').live('click', function() {
            var app_id = $(this).data('app_id');
            $('#commentServ').val($('#commentServDri' + app_id).val());
            $('#textareaid').val('#commentServDri' + app_id);
            $('#divComment').dialog('open');
        });

        $('#formCommentServ').validate({
            submitHandler: function(form) {
                $($('#textareaid').val()).val($('#commentServ').val());
                $('#divComment').dialog('close');
            }
        });

        $('.savComment').button({
            icons: {
                primary: 'ui-icon-disk'
            }
        });
        $('#process_payment').button({
            icons: {
                primary: 'ui-icon-check'
            }
        });
    });

</script>

<div id="show_content" class="show_content">
    <div id="pre_billing" >
        </br>
        </br>
        <table id="grid-listDriversBilling" ></table>
        <div id="grid-listDriversBilling-paper"></div>


       <div class="float_right cont_filter">
        <div class="block1_theme content_filter ui-corner-all">
            <div class="padding10">
                <div>
                    <div class="title_theme ui-corner-all">
                <h2><?php echo __('Filtro'); ?></h2>                
            </div>
            <div class="row_item">
                <span class="li_item"></span>
                <div class="item_1">
                    <label><?php echo __('Conductor'); ?>:</label>
                </div>
                <div class="item_2">
                    <select name="idDriver" id="idDriver" value="0" >
                        <option value=""><?php echo __('Seleccionar ...'); ?></option>                         
                        <?php foreach ($drivers as $d_id) { ?>
                            <option value="<?php echo $d_id['idDriver'] ?>"><?php echo $d_id['name'] ?><?php echo '  ' ?><?php echo $d_id['lastname'] ?></option>

                        <?php } ?>
                    </select>
                </div>

            </div>
            <div class="row_item">
                <button type="button" id="btn_clean"><?php echo __('Limpiar'); ?></button>
            </div>
        </div>
</div>
        </div>
           <div class="clear"></div>
    </div>
    </div>
        
    <div id="div_billing" >
        <br><br> 
        <button type="button" id="btn_back_billing"><?php echo __('Atrás'); ?></button>
        <input type="hidden" id="driver_id_hidden" name="driver_id_hidden"/>
        <input type="hidden" id="save_hidden" name="save_hidden" value="false"/>
        <div class="menutop ui-widget ui-widget-content ui-corner-all">
            <div class="menutop2 menutop6 left ui-widget-header ui-state-hover ui-corner-all ui-helper-clearfix">
                <h4><?php echo __('Pago a Conductor'); ?></h4>
            </div>
            <span class="billing_driver_number ui-state-active ui-priority-primary ui-corner-all"><?php echo __('Número de Pago a Conductor'); ?>:<label id="billing_driver_number"></label></span>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="ui-widget ui-widget-content ui-corner-all info_billing_wrapper">
            <div class="info_billing">
                <table width="50%" border="0" style="float: left;">
                    <tr>
                        <td>
                            <label for="billing_driver_date"><?php echo __('Fecha de Pago'); ?>:</label>
                        </td>
                        <td>
                            <input id="billing_driver_date"/>
                        </td>            
                        <td>
                            <label><?php echo __('Conductor'); ?>:</label>
                        </td>
                        <td id="current_driver">

                        </td>            

                    </tr>
                </table>   
                <div id="summary_amount_total" class="ui-state-error ui-corner-all"></div>
                <div id="summary_label_total"><?php echo __('TOTAL'); ?></div>
                <div class="clear"></div>            
            </div>
        </div>
        <div class="clear"></div>
        <div class="content_to_billing">

        </div>
        <div class="clear"></div>
        <hr class="hr_separate"/>
        <div class="buttons_container">

            <button type="button" id="preview_billing_driver"><?php echo __('Guardar'); ?></button>

        </div>
    </div>
    <div id="divComment" class="dialog" width="450px" title="<?php echo __('Notas'); ?>" resizable="false">
        <form id="formCommentServ">
            <input name="textareaid" id="textareaid" type="hidden"/>
            <label>Note:</label>
            <textarea name="commentServ" cols="30" rows="8" id="commentServ"></textarea></td>
            <button type="submit" class="savComment"><?php echo __('Guardar'); ?></button>

        </form>
    </div>
    <div id="dialog_pre_view" class="none">
    <div class="padding10">
        <div class="wrapper_pre_billing_summary">
            <button id="process_payment"><?php echo __('Procesar Pago'); ?></button>
            <div class="amount_pre_billing_summary ui-corner-all ui-state-active">

            </div>
            <div class="label_pre_billing_summary ui-corner-all">

            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <ul class="ul_items_pre_billing">
            <li class="pre_billing_header ui-widget-header ui-corner-all">
                <dl>
                    <dd class="pre_billing_date"><?php echo __('Fecha'); ?></dd>
                    <dd class="pre_billing_description"><?php echo __('Descripción'); ?></dd>
                    <dd class="pre_billing_quantity"><?php echo __('Cantidad'); ?></dd>
                    <dd class="pre_billing_ratio"><?php echo __('Proporción'); ?></dd>
                    <dd class="pre_billing_amount"><?php echo __('Monto'); ?></dd>
                </dl>
                <div class="clear"></div>
            </li>
        </ul>
    </div>
</div>
</div>
