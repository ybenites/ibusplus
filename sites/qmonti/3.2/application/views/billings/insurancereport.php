<style type="text/css">
    .claim{width: 50px;float: left;}
    .insurance{width: 100px;float: left;}
    .menutop{padding: 2px;}
    .menutop2{padding: 5px;}
    .formatTable{float: left;margin-right: 10px;margin-top: 10px;width: 100%;}
    .details .menutop{padding: 15px;}
    h1, h2, h3, h4 {margin: 0;padding: 0;}

    .divCont dl,.divCont dl dd{float: left;margin: 0;padding: 0;margin-right: 10px;}
    .divCont ul, .divCont ul li{list-style: none;margin: 0;padding: 0;}
    .divCont ul li{padding: 5px 0;}
    .divCont ul li label{width: 130px; float: left;font-weight: bold;line-height: 20px;}
    #gbox_list{margin: 0 auto;}
</style>
<script type="text/javascript">
    reportInsuranceInvoices = '/billings/insurancereport/reportInsuranceInvoices';
    $(document).ready(function() {
        $("#idInsuranceCarriers").combobox();
        $("#dateIni,#dateEnd").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "mm/dd/yy",
            onSelect: function() {
                if ($('#dateFrom').val() != '' && $('#dateTo').val() != '') {
                }
            },
            onClose: function(selectedDate) {

                if ($("#dateIni").val() != '')
                    $("#dateEnd").datepicker("option", "minDate", selectedDate);
                if ($("#dateEnd").val() != '')
                    $("#dateIni").datepicker("option", "maxDate", selectedDate);
            }
        });

        $('#printInsurances').button({
            icons: {
                primary: 'ui-icon-print'
            }
        }).click(function() {

            if ($("#idInsuranceCarriers").val() > 0) {

                var data = new Object();
                data.dateFrom = $("#dateIni").val();
                data.dateTo = $("#dateEnd").val();
                data.idInsurance = $("#idInsuranceCarriers").val();

                $('#divReport').iReportingPlus('option', 'dynamicParams', data);
                $('#divReport').iReportingPlus('generateReport', 'pdf');
            } else
                msgBox('Select Insurance Carrier', 'Alert');
        });

        $('#divReport').iReportingPlus({
            domain: domain,
            repopt: ['xls'],
            clientFolder: 'qmonti',
            clientFile: 'MONTI_BILLING_INSURANCE_PAYMENT',
            htmlVisor: false,
            jqUI: true,
            urlData: reportInsuranceInvoices,
            xpath: '/report/response/row',
            orientation: 'horizontal',
            staticParams: rParams,
            reportZoom: 1.2,
            responseType: 'jsonp',
            jqCallBack: jquery_params,
            waitMessage: '<?php echo __('Generando su Reporte') ?>...'
        });
    });
</script><center>
    <br>
    <div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all"><?php echo __('Reporte de Aseguradoras') ?></div>
</center>
<div class="show_content" >


    <ul>
        <li>

            <label class="long_label"><?php echo __('Filtro por Rango de Fechas') ?></label>

            <div class="clear"></div>
        </li>
        <li>
            <table >
                <tr>
                    <td width="200px" ><label class="short_label" ><?php echo __('Desde') ?>:</label>
                        <input name="dateIni" id="dateIni" type="text"/></td>
                    <td width="200px">  <label class="short_label"><?php echo __('Hasta') ?>:</label>
                        <input name="dateEnd" id="dateEnd" type="text"/></td>
                    <td width="300px">    <label class="short_label"><?php echo __('Compañía de Seguros') ?>:</label>

                        <select id="idInsuranceCarriers" name="idInsuranceCarriers">
                            <option value=""></option>
                            <?php foreach ($insurance_carrier as $ins_id) { ?>
                                <option value="<?php echo $ins_id['idInsurancecarrier'] ?>"><?php echo $ins_id['name'] ?></option>

                            <?php } ?>
                        </select>                  
                    </td> 
                    <td >      <a href="#" id="printInsurances">
<?php echo __('Imprimir') ?>
                        </a>
                    </td> </tr> </table>
            <div class="clear"></div>
        </li>
    </ul>
</div>
<div id="divReport">

</div>