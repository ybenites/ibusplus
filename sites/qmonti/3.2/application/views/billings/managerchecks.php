<script type="text/javascript">

    var listChecks = "/billings/managerchecks/listChecks";
    var getCheckById = "/billings/managerchecks/getCheckById";
    var saveCheck = "/billings/managerchecks/saveCheck";
    var deleteCheckById = '/billings/managerchecks/deleteCheckById';
    
    $(document).ready(function() {

        $('#idInsuranceCarriers').combobox();
        $('#dateCheck').datepicker({dateFormat: 'mm/dd/yy'});

        $("#grid-listChecks").jqGrid({
            url: listChecks,
            datatype: "json",
            postData: {},
            colNames: ['CheckID', 
                       'InsuranceID', 
                       '<?php echo __('Compañía de Seguros') ?>', 
                       '<?php echo __('Número de Cheque') ?>', 
                       '<?php echo __('Monto') ?>', 
                       'paid', 
                       '<?php echo __('Nota') ?>', 
                       '<?php echo __('Fecha de Cheque') ?>', 
                       '<?php echo __('Fecha de Registro') ?>', 
                       '<?php echo __('Acciones') ?>'],
            colModel: [
                {name: 'check_id', index: 'billing_id', hidden: true, width: 70},
                {name: 'insurance_id', index: 'driver_id', hidden: true, width: 70},
                {name: 'insurance', index: 'driverfullname', width: 200},
                {name: 'numbercheck', index: 'numbercheck', width: 150},
                {name: 'amount', index: 'amount', search: true, width: 150},
                {name: 'paid', index: 'paid', hidden: true,search: true, width: 150},
                {name: 'memo', index: 'memo', search: true, width: 200},
                {name: 'datecheck', index: 'datecheck', search: false, width: 150},
                {name: 'dateregistration', index: 'dateregistration', search: false, width: 150},
                {name: 'actions', index: 'actions', width: 100, align: "right", sortable: false, search: false}
            ],
            rowNum: 50,
            rowList: [30, 40, 50],
            height: 450,
            pager: 'grid-listChecks-paper',
            sortname: 'check_id',
            viewrecords: true,
            sortorder: "asc",
            caption: "<?php echo __('Lista de Cheques') ?>",
//            toolbar: [true, "top"],
            afterInsertRow: function(row_id, data_id) {
                    
                edit = "<a class=\"edit\" data-idCheck=\"" + row_id + "\" title=\"<?php echo __('Editar Cheque'); ?>\" ><?php echo __('Editar Cheque'); ?></a>";
                if(data_id.paid==0)    
                delet = "<a class=\"delete\" data-idCheck=\"" + row_id + "\" title=\"<?php echo __('Eliminar Cheque'); ?>\" ><?php echo __('Eliminar Cheque'); ?></a>";
                else delet='';
                $("#grid-listChecks").jqGrid('setRowData', row_id, {actions: edit + delet});
            },
            gridComplete: function() {

                $('.edit').button({
                    icons: {primary: 'ui-icon-pencil'},
                    text: false
                });
                $('.delete').button({
                    icons: {primary: 'ui-icon-close'},
                    text: false
                });

            }
        });

        $('#grid-listChecks').on('click', '.edit', function() {
            $("#dialogCheck").dialog('open');
            
            $.ajax({
                type: "POST",
                url: getCheckById,
                data: 'idCheck='+$(this).attr("data-idCheck"),
                dataType: 'json',
                success: function(response)
                {
                    data = response.data;
                    $("#idCheck").val(data.idCheck);
                    $("#idInsuranceCarriers").val(data.idInsuranceCarrier);
                    $('#idInsuranceCarriers-combobox').val($("#idInsuranceCarriers option:selected").text());
                    $("#numberCheck").val(data.numbercheck);
                    $("#amount").val(data.amount);
                    $("#dateCheck").val(data.datecheck);
                    $("#memo").val(data.memo);

                }
            });
        });

         $('#grid-listChecks').on('click', '.delete', function() {

            $.ajax({
                type: "POST",
                url: deleteCheckById,
                data: 'idCheck='+$(this).attr("data-idCheck"),
                dataType: 'json',
                success: function(response)
                {
                    
                    $("#grid-listChecks").trigger('reloadGrid');

                }
            });
        });
        
        $('#dialogCheck').dialog({
            modal: true,
            autoOpen: false,
            resizable: false,
            draggable: false,
            height: 330,
            width: 490,
            buttons: {
                "<?php echo __('Guardar') ?>": function() {
                    if (formCheck.form()) {
                        $('#formCheck').submit();
                       
                    }
                },
                "<?php echo __('Cancelar') ?>": function() {
                    $(this).dialog("close");
                }
            },
            close: function() {

            }
        });

        formCheck = $('#formCheck').validate({
            rules: {
                idInsuranceCarriers: {
                    required: true
                },
                numberCheck: {
                    required: true
                },
                amount: {
                    required: true
                },
                dateCheck: {
                    required: true
                }
            },
            messages:{
                idInsuranceCarriers: {
                    required: "<?php echo __('La Compañia de Seguros es requerida'); ?>"
                },
                numberCheck: {
                    required: "<?php echo __('El Número de Cheque es requerido'); ?>"
                },
                amount: {
                    required: "<?php echo __('El Monto del Cheque es requerido'); ?>"
                },
                dateCheck: {
                    required: "<?php echo __('La Fecha del Cheque es requerida'); ?>"
                }
            },        
            errorElement: "em",
            submitHandler: function(form) {
                dataString = $(form).serialize();
                $.ajax({
                    type: "POST",
                    url: saveCheck,
                    data: dataString,
                    dataType: 'json',
                    beforeSend: function() {
                        $("#loading").dialog('open');
                    },
                    complete: function() {
                        $("#loading").dialog('close');
                    },
                    success: function(response) {
                        
                        if (response.msg == 'OK') {
                                $('#dialogCheck').dialog("close");
                                $("#grid-listChecks").trigger('reloadGrid');
                            } else {
                                msgBox(response.response, '<?php echo __('Alerta') ?>');
                            }

                        }
                    
                });
            }
        });


    });
</script>
 <center>
<div class="titleTables ui-state-default ui-helper-clearfix ui-corner-all">
            <?php echo __('Administrador de Cheques') ?>
        </div> </center>
<div id="show_content" class="show_content">
    <center>
        <table id="grid-listChecks" ></table>
        <div id="grid-listChecks-paper"></div>
    </center>
    <div id="dialogCheck" class="divCont none" title="<?php echo __('Edición de Cheque') ?>">
        <form id="formCheck" action="#">
            <input type="hidden" id="idCheck" name="idCheck" />

            <dl>
                <dd><label class="frmlbl" ><?php echo __('Número de Cheque') ?>:</label></dd>
                <dd>
                    <input type="text" id="numberCheck" name="numberCheck"  />
                </dd>
            </dl>
            <div class="clear"></div>
<br>
            <dl>
                <dd>
                    <label class="frmlbl"><?php echo __('Compañia de Seguros') ?>:</label>
                </dd>
                <dd>
                    <select id="idInsuranceCarriers" name="idInsuranceCarriers">
                        <option value=""></option>
                        <?php foreach ($insurance_carrier as $ins_id) { ?>
                            <option value="<?php echo $ins_id['idInsurancecarrier'] ?>"><?php echo $ins_id['name'] ?></option>

                        <?php } ?>
                    </select>
                </dd>
            </dl>
            <div class="clear"></div>
<br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Monto') ?>:</label></dd>
                <dd>
                    <input type="text" id="amount" name="amount" />
                </dd>
            </dl>
            <div class="clear"></div>
<br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Fecha de Cheque') ?>:</label></dd>
                <dd>
                    <input id="dateCheck" type="text" name="dateCheck" />
                </dd>
            </dl>
            <div class="clear"></div>
<br>
            <dl>
                <dd><label  class="frmlbl"><?php echo __('Nota') ?>:</label></dd>
                <dd>
                     <textarea name="memo" id="memo" cols="27" rows="6" ></textarea>
                </dd>
            </dl>
            <div class="clear"></div>

        </form>
    </div>    
</div>        