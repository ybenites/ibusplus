<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body style="background-color: whitesmoke; font-size: 16px;">
        <div style="background-color: white; border-radius: 4px; font-family: Arial; max-width:600px; margin: 0 auto;">
            <table style="margin: 2px; width: 99%;">
                <tr>
                    <td style="text-align: center;">
                           <img src="http://:website/media/images/:skin/logo.png" style="display: block;"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <h3 style="font-style: italic;">Confirmation of Assignment</h3>
                        <p style="text-align: justify; font-size: 0.8em;">
                         Se le notifica que ha habido un cambio en el Itineario de viaje el cual ud compro un ticket.
                         a continuacion se detalla los cambios en el itinerario.  
                        </p>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <p><label>Patient Name:</label> :patientname</p>
                        <p><label>Claim Number: </label> :claimnumber</p>
                        <p><label>Appointment Date:</label> :appdate</p
                        <p><label>Appointment Time:</label> :apptime</p>

                    </td>
                </tr>
            </table>           
            <table style="margin: 2px; width: 99%; color: gray; font-size: 0.7em;">
                <tr>
                    <td style="text-align: justify;">
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                        <?php echo __("Para contactar con nuestro servicio de atención al cliente, visite nuestro Centro de Asistencia o llame al 877-815-1531 (dentro de los EE.UU.)."); ?>
                        :contactCompany, :contactAddress - :contactPhone. <a href=":website">http://:website</a>
                        <hr style="border-style: solid; height: 2px; color: #DAE4E5;"/>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
