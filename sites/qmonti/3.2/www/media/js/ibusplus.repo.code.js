
var menuLeaveClick ;
var menuLeaveTimeOut ;
var profileHover = false;
var profileClick = false;
$(document).ready(function() {    
    $('div#iMenu').addClass('ui-state-default');
    $('div#iMenu li ul').addClass('ui-state-default');
    $('ul.iMainMenu > li').on('click','p',function(){
        if(!$.isEmptyObject($(this).data())) location.href = $(this).data('link');
    });
    $('ul.iMainMenu > li p').mouseenter(function(){
        //$('ul.iMainMenu > li ul').removeClass("iMenu-visible");
        //console.log();
        $(this).parents('ul:first').find('li > ul.iMenu-visible').removeClass("iMenu-visible");
        menuLeaveClick = false;
    });
    $('ul.iMainMenu > li').mouseenter(function(){
        clearTimeout(menuLeaveTimeOut);
        $(this).find('p:first').addClass('ui-state-active');
        var subMenu = $(this).children('ul');
        subMenu.addClass("iMenu-visible");
        menuLeaveClick = false;
    });
    $('ul.iMainMenu > li').mouseleave(function(){
        menuLeaveTimeOut = setTimeout(function(){
            $('ul.iMainMenu > li ul').removeClass("iMenu-visible");
        },2000);
        $(this).find('p:first').removeClass('ui-state-active');
        menuLeaveClick = true;
    });
    $('ul.iMainMenu > li ul > li').mouseenter(function(){
        $(this).addClass('ui-state-active');
        var subMenu = $(this).children('ul');
        subMenu.addClass("iMenu-visible iSubMenu-visible");
        subMenu.css("left",$(this).outerWidth()+"px");
        menuLeaveClick = false;
    });
    $('ul.iMainMenu > li ul > li').mouseleave(function(event,a,b,c){
        $(this).removeClass('ui-state-active');
        var subMenu = $(this).children('ul');
        subMenu.removeClass("iMenu-visible iSubMenu-visible");
        menuLeaveClick = true;
    });
    
    /*$('li.ibus-user-profile').click(function(){
        console.log(profileClick);
        $(this).find('div.ibus-user-profile-options').show();
        profileClick = false;
    });*/
    $('html').click(function(){
        if(typeof menuLeaveClick !='undefined')
            if(menuLeaveClick) $('ul.iMainMenu > li ul').removeClass("iMenu-visible");
        if(typeof profileClick !='undefined')
            if(!profileHover && profileClick) {
                $('div.ibus-profile-btn').removeClass('ibus-profile-btn-hover');
                $('div.ibus-user-profile-options').hide();
                profileHover = false;
                profileClick = false;
            }
    });
    $('div.ibus-profile-btn').mouseenter(function(){
        profileHover = true;
    });
    $('div.ibus-profile-btn').mouseleave(function(){
        profileHover = false;
    });
    $('div.ibus-profile-btn').click(function(){
        if(!profileClick){
            $(this).addClass('ibus-profile-btn-hover');
            $(this).parent().find('div.ibus-user-profile-options').show();
        }else if(profileHover && profileClick){
            $('div.ibus-profile-btn').removeClass('ibus-profile-btn-hover');
            $('div.ibus-user-profile-options').hide();
        }
        profileClick = !profileClick
        
    });
    
});