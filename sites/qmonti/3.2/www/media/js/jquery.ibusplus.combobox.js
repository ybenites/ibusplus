/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var iComboboxCount = 0;
var ibusplus_combobox_input_focus = [];
var ibusplus_combobox_last_click = [1,0];
(function( $ ) {
    var aGlobalSettings = [];
    var instanceSettings;
    var aInstanceDefaultSettings = {
        width : 200,
        fullTextSelectOnClick:false,
        source:null,
        nextComboboxLoad:null,
        beforeSelect:function(){},
        selected:function(){},
        afterSelect:function(){}
    };
    var methods = {
        init : function(options) {
            var cElment = $(this);
            cElment.hide();
            cElment.change(function(){
                //console.log('change');
                })
            aGlobalSettings[cElment.attr('id')] = $.extend({},aInstanceDefaultSettings,options);
            //aGlobalSettings.push(cElment.attr('id'));
            //console.log(aGlobalSettings[cElment.attr('id')]);
            instanceSettings = aGlobalSettings[cElment.attr('id')];
            if($('#'+cElment.attr("id")+"-icombobox").length>0) $('#'+cElment.attr("id")+"-icombobox").remove();
            var icElement = $(document.createElement('div'))
            .attr('id',cElment.attr("id")+"-icombobox")
            .attr('name',cElment.attr("id")+"-icombobox")
            .attr('style',"width:"+instanceSettings.width+"px;")
            .addClass('ibusplus-combobox ibusplus-combobox-corner-all');
            $.data(icElement.get(0),'for',cElment.attr('id'));
            icElement.click(function() {
                $('div.ibusplus-combobox').filter(':not(#'+$(this).attr('id')+')').removeClass('ibusplus-combobox-border-active');
                if(aGlobalSettings[cElment.attr('id')].fullTextSelectOnClick)
                    $(this).find('input.ibusplus-combobox-input').select();
            });
            
            icElement.mouseenter(function(){
                $(this).addClass("ibusplus-combobox-border-active");
                $(this).find('.ibusplus-combobox-options').addClass("ibusplus-combobox-border-active");
            });
                
            icElement.mouseleave(function(){
                if(!isComboboxInputFocus($(this).find('input.ibusplus-combobox-input').data('id'))){
                    $(this).removeClass("ibusplus-combobox-border-active");
                    $(this).find('.ibusplus-combobox-options').removeClass("ibusplus-combobox-border-active");
                }
            });
            
            var icMargin = $(document.createElement('div'));
             
            var icInput = $(document.createElement('input'))
            .attr("id",cElment.attr("id")+"-icombobox-input")
            .attr("autocomplete","off")
            .attr("type","text")
            .attr("style","width:"+eval(instanceSettings.width-40)+"px;")
            .addClass('ibusplus-combobox-input');
            $.data(icInput.get(0),"id",iComboboxCount);
            
            icInput.focus(function(){
                var parent = $(this).parents('div.ibusplus-combobox');
                if(!parent.hasClass('ibusplus-combobox-shadow ibusplus-combobox-border-active'))
                    parent.addClass("ibusplus-combobox-shadow ibusplus-combobox-border-active");
                if(!isComboboxInputFocus($(this).data('id')))
                    ibusplus_combobox_input_focus.push($(this).data('id'));
                
            });
            
            icInput.blur(function(){
                if($(this).data('id')!=ibusplus_combobox_input_focus[0]){
                    var parent = $(this).closest('div.ibusplus-combobox');
                    parent.find('div.ibusplus-combobox-border-active').removeClass("ibusplus-combobox-border-active");
                    parent.removeClass("ibusplus-combobox-shadow ibusplus-combobox-border-active");
                }
                if(isComboboxInputFocus($(this).data('id'))){
                    ibusplus_combobox_input_focus.splice(ibusplus_combobox_input_focus.indexOf($(this).data('id')),1);
                }
            });
            
            icInput.keydown(function(event){
                var option_list = $(this).closest('div.ibusplus-combobox').children('ul.ibusplus-combobox-options');
                var selected = option_list.find('a.ioption-hover');
                var focus = true;
                if(event.which==13){
                    focus = false;
                    selected.parent().trigger('click');
                }else if(event.which==40){
                    if(selected.length>0){
                        var nxt = selected.parent().next('li:not(:hidden)');
                        if(nxt.length>0)
                            nxt.find('a').addClass('ioption-hover').focus();
                        else
                            option_list.children('li:not(:hidden)').first().find('a').addClass('ioption-hover').focus();
                    } else
                        option_list.find('a:first').addClass('ioption-hover').focus();
                } else if(event.which==38){
                    if(selected.length>0){
                        var pre = selected.parent().prev('li:not(:hidden)');
                        if(pre.length>0)
                            pre.find('a').addClass('ioption-hover').focus();
                        else
                            option_list.children('li:not(:hidden)').last().find('a').addClass('ioption-hover').focus();
                    }else
                        option_list.find('a:last').addClass('ioption-hover').focus();
                }
                if(focus)
                    $(this).trigger('focus');
                selected.removeClass('ioption-hover');
            });
            
            icInput.keyup(function(event){
                var term = $(this).val().toString().toLowerCase();
                var option_list = $(this).closest('div.ibusplus-combobox').children('ul.ibusplus-combobox-options');
                var options = option_list.children();
                if(event.which==27){
                    cElment.val('');
                    $(this).val('');
                    options.show();
                    return false;  
                } else if(event.which==40 || event.which==38 || event.which==37 || event.which==39 || event.which==13){
                    return false;  
                }
                option_list.addClass('ibusplus-combobox-options-active ibusplus-combobox-border-active');
                
                var cResult=0;
                var options_filter = options.filter(function(index){
                    var response =($(this).text().toLowerCase().indexOf(term) == -1 || $(this).text().toLowerCase().indexOf(term) >0);
                    if(response) $(this).hide();
                    else {
                        $(this).show();
                        cResult++;
                    }
                    return response;
                });
                
                if(options.not(":hidden").find('a.ioption-hover').length==0)
                    options.not(":hidden").filter(':first').find('a').addClass('ioption-hover');
                if(cResult==0 || $.trim(term)==''){ 
                    cElment.val('');
                    option_list.removeClass('ibusplus-combobox-options-active ibusplus-combobox-border-active');
                }
            });
            
            
            
            icInput.click(function() {
                $(this).next('div.ibusplus-combobox-button').trigger('click');
            });
            
            var icButton = $(document.createElement('div'))
            .addClass('ibusplus-combobox-button ibusplus-combobox-corner-all')
            .append($(document.createElement('div')).addClass("cbx-arrow"));
            
            icButton.click(function() {
                var parent = $(this).closest('.ibusplus-combobox');
                var options = parent.children('ul.ibusplus-combobox-options');
                ibusplus_combobox_last_click[0] = 1;
                if(ibusplus_combobox_last_click[1]!=options.data('id')){
                    $("div.ibusplus-combobox ul.ibusplus-combobox-options").removeClass('ibusplus-combobox-options-active');
                }
                ibusplus_combobox_last_click[1]=options.data('id');
                if(options.hasClass('ibusplus-combobox-options-active')){
                    options.removeClass('ibusplus-combobox-options-active');
                }else{
                    options.addClass('ibusplus-combobox-options-active ibusplus-combobox-border-active');
                }
            });
            
            icMargin
            .append(icInput)
            .append(icButton)
            .append($(document.createElement('div')).attr("style","clear:both;"));
            
            icElement.append(icMargin);
            
            var ulOptions = $(document.createElement('ul'))
            .attr("style","top:35px;left:-2px;")
            .addClass('ibusplus-combobox-options');
            $.data(ulOptions.get(0),"id",iComboboxCount);
            if(aGlobalSettings[cElment.attr('id')].source!=null){
                $.each(aGlobalSettings[cElment.attr('id')].source,function(idx,obj){
                    var opt = $(document.createElement('option')).attr("value",obj.value).text(obj.label);
                    cElment.append(opt);
                });
            }
            $.each(cElment.children(),function(idx,obj){
                if($(obj).attr('value')!=''){
                    var li =  $(document.createElement('li')).append($(document.createElement('a')).text($(obj).text()).attr('tabindex', "-1"));
                    $.data(li.get(0),'value',$(obj).attr('value'));
                    $.data(li.get(0),'label',$(obj).text());
                    li.click(function(event){
                        aGlobalSettings[cElment.attr('id')].beforeSelect($(obj).attr('value'),$(obj).text());
                        var label = $(this).data('label');
                        var input = $(this).parents('.ibusplus-combobox').find('.ibusplus-combobox-input');
                        input.val(label);
                        $.data(input.get(0),'lst',label);
                        $(this).parent().find('li').show();
                        cElment.val($(this).data('value'));
                        aGlobalSettings[cElment.attr('id')].selected(event,{
                            value:$(obj).attr('value'),
                            label:$(obj).text(),
                            item:obj
                        });
                    });
                    li.hover(function(){
                        var parent = $(this).parent();
                        parent.find('a.ioption-hover').removeClass('ioption-hover');
                        $(this).children('a').addClass('ioption-hover');
                    },function(){
                        $(this).children('a').removeClass('ioption-hover');
                    });
                    ulOptions.append(li);
                }
            });
            ulOptions.keydown(function(){
                return false;
            });
            icElement.append(ulOptions);
            
            cElment.before(icElement);
            
            iComboboxCount++;
        }
        
    }
    function isComboboxInputFocus(value){
        return ($.inArray(value, ibusplus_combobox_input_focus)>-1);
    }
    $.fn.icombobox =  function(method) {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jquery.ibusplus.combobox' );
        }        
    };
})(jQuery);
$('html').click(function(){
    if(!Boolean(eval(ibusplus_combobox_last_click[0]))){
        $("div.ibusplus-combobox ul.ibusplus-combobox-options").removeClass('ibusplus-combobox-options-active');
        $.each($("div.ibusplus-combobox input"),function(idx,obj){
            var input = $(obj);
            var parent = input.closest("div.ibusplus-combobox"); 
            if(input.data('lst')!=input.val()){
                var options = parent.children('ul.ibusplus-combobox-options').children('li');
                input.val('');
                options.show().removeClass('ioption-hover');
                $("#"+parent.data('for')).val('');
            }
        });
    }
    ibusplus_combobox_last_click[0] = 0;
});