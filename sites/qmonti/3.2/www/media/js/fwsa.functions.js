$(document).ready(function(){
    
    $.generateFixture = function(){        
        $.ajax({
            url:'/registration/division/getScortsByDivision'+jquery_params,
            data:{
                idd:$('#cmbDivision').val()
            },
            async:false,
            success:function(response){
                if(evalResponse(response)){
                    var resp = response.data;
                    $.fillCmbJourney();
                    $.fillTableOfPositions();
                    $("#createFixture").remove();
                    var e = new Array();
                    var f = new Array();
                    var sp = "<div id='createFixture'>";
                    $.each(total_teams, function(idx, obj){
                        e.push(obj.idt);
                        f[obj.idt] = obj.name;
                    });        
                    var todo = new Array();        
                    for(var m=0; m<e.length-1; m++){
                        var dd = new Array();            
                        var se = "<div style='background-color: #EDF3F9; padding: 10px; margin-bottom: 5px;' class='div"+(m+1)+"'>";
                        se += "<span>Jornada "+(m+1)+" :</span>";
                        se += "<div class='selectGame' style='text-align: center; margin-bottom: 3px;'>";
                        for(var j=0; j<e.length; j++){
                            if(!($.inArray(e[j], dd) > -1)){
                                var p = j+1;                    
                                for(var k=p; k<e.length; k++){
                                    var y = e[j]+e[k];
                                    if(!($.inArray(e[j], dd) > -1)){
                                        if(!($.inArray(e[k], dd) > -1)){
                                            if(!($.inArray(y, todo) > -1)){                                    
                                                dd.push(e[j]);
                                                dd.push(e[k]);
                                                todo.push(y);
                                                array_for_fixture.push(e[j]);
                                                array_for_fixture.push(e[k]);                                    
                                                var di = "<div class='divfix' data-aa='"+e[j]+"' data-bb='"+e[k]+"'>";
                                                di += "<div id='zz' style='background-color: burlywood; width: 150px; height: 20px; float: left;  padding: 4px;'>02/12/2012 - 03:00 PM</div>";
                                                di += "<div id='aa' style='background-color: tomato; width: 100px; height: 20px; float: left;  padding: 4px;'>"+f[e[j]]+"</div>";
                                                var result = "vs";
                                                $.each(resp, function(idx, obj){
                                                    if(obj.idot == e[j] && obj.idtt == e[k]){
                                                        if(obj.sot != null){
                                                            result = obj.sot+" - "+obj.stt;
                                                        }
                                                    }
                                                });
                                                di += "<div id='bb"+e[j]+e[k]+"' style='background-color: yellowgreen; width: 50px; height: 20px; float: left;  padding: 4px;'>"+result+"</div>";
                                                di += "<div id='cc' style='background-color: tomato; width: 100px; height: 20px;  float: left;  padding: 4px;'>"+f[e[k]]+"</div>";
                                                di += "<div id='zz' style='background-color: burlywood; width: 150px; height: 20px; float: left;  padding: 4px;'>Estadio</div>";
                                                di += "<div class='clear'></div>";
                                                di += "</div>";
                                                se += di;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        se += "</div>";
                        se += "</div>";
                        sp += se;
                    }
                    $('div#setFixture').append(sp);
                }
            }
        });                
    };
    
    $.listDivisions = function(label, v){
        $.ajax({
            url:'/registration/team/listDivisions'+jquery_params,
            //                data:{t:'s'},
            async:false,
            success:function(response){
                $('option', label).remove();
                if(evalResponse(response)){
                    if(response.data.length>0){
                        $(label).append(option_template.replace('[VALUE]', '0','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                        $.each(response.data, function(index, array) {
                            $(label).append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                        });
                    } else {
                        $(label).append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                    }
                }
                $(label).combobox({
                    selected:function(event,ui){
                        var opt = $(this).val();
                        switch(v){
                            case "vd":
                                if(opt > 0){
                                    $.ajax({
                                        url:'/registration/division/getCheckingFixture'+jquery_params,
                                        data:{
                                            idd:opt
                                        },
                                        success:function(response){
                                            if(evalResponse(response)){
                                                $('#cmbJourney').hide('slow');                                                
                                                $.each(response.data, function(idx, obj) {
                                                    $("#createFixture").remove();
                                                    count_fixture = obj.c;
                                                    count_journey = obj.j;
                                                    array_for_fixture = new Array();
                                                    $("#divGridTeam, #divFixture").show("slow");
                                                    $.paramGrid(opt);                                                  
                                                    
                                                });                                                                                                
                                            } 
                                        }
                                    });                                    
                                }else{
                                    $("#divGridTeam, #divFixture").hide("slow")
                                }                                
                                break;
                            case "vt":
                                $("#teamDivision").val(opt);
                                if(opt > 0){
                                    $("div#tabs").show("slow");
                                    $.listTeamsByDivision();
                                }else{
                                    $("div#tabs").hide("slow");
                                }                                    
                                break;
                        }                                                        
                    }
                });
            }
        });
    }
    
    $.listTeamsByDivision = function(){
        $.ajax({
            url:'/registration/team/listTeamsByDivision'+jquery_params,
            data:{
                d:$('#cmbDivision').val()
            },
            async:false,
            success:function(response){
                $('option', '#cmbTeams').remove();
                if(evalResponse(response)){
                    if(response.data.length>0){
                        $('#cmbTeams').append(option_template.replace('[VALUE]', '0','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                        $.each(response.data, function(index, array) {
                            $('#cmbTeams').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                        });
                    } else {
                        $('#cmbTeams').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                    }
                }
                $('#cmbTeams').combobox({
                    selected:function(event,ui){
                        showLoading=1;
                        $.selectedTeam($(this).val());
                    }
                });
            }
        });
    }
      
    $.listCoachs = function(){
        $.ajax({
            url:'/registration/team/listCoachs'+jquery_params,
            //                data:{t:'s'},
            async:false,
            success:function(response){
                $('option', '#cmbCoachs').remove();
                if(evalResponse(response)){
                    if(response.data.length>0){
                        $('#cmbCoachs').append(option_template.replace('[VALUE]', '0','g').replace('[LABEL]',msgSelectDefaultOption, 'g'));
                        $.each(response.data, function(index, array) {
                            $('#cmbCoachs').append(option_template.replace('[VALUE]', array.id,'g').replace('[LABEL]',array.value, 'g'));
                        });
                    } else {
                        $('#cmbCoachs').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g'));
                    }
                }
                $('#cmbCoachs').combobox({
                    selected:function(event,ui){
                        //                            showLoading=1;
                        console.log($(this).val());
                    }
                });
            }
        });
    }    
    
    $.selectedTeam = function(idt){
        $.ajax({
            url:'/registration/team/getPlayerByTeam'+jquery_params,
            data:{
                idtd:idt
            },
            success:function(response){
                $('#out').html('');
                $('#ut').html('');
                if(evalResponse(response)){
                    var rd = response.data;
                    newElements = new Array();
                    trashElements = new Array();
                    if(rd.sFiles.length>0){
                        var ut=""
                        $.each(rd.sFiles,function(){
                            ut+=draggable_element.replace('[VALUE]', this.idp, 'g').replace('[LABEL]', this.fname, 'g') 
                        });
                        $('#ut').append(ut);
                    }
                    if(rd.oFiles.length>0){
                        var out=""
                        $.each(rd.oFiles,function(){                                               
                            out+=draggable_element_ut.replace('[VALUE]', this.idp, 'g').replace('[LABEL]', this.fname, 'g')
                        });
                        $('#out').append(out);
                    } 
                }
            }
        });
    }      
      
    $.paramGrid = function(d){
        $('#team_jqGrid').setGridParam({
            postData:{
                div:function(){
                    return d;
                }
            }
        }).trigger("reloadGrid");      
    }

    /*Reordenamiento aleatorio de una raiz*/
    $.randOrd = function(){
        return (Math.round(Math.random())-0.5); 
    }
    
    $.fillCmbJourney = function(){
        var op = "";
        $('option', '#cmbJourney').remove();
        op += "<option value='0'>Todas las jornadas</option>";
        for(var i = 0; i<count_journey; i++){
            op += "<option value='"+(i+1)+"'>Jornada"+(i+1)+"</option>";
        }
        $('#cmbJourney').append(op).show('slow');        
    }
    
    $.fillTableOfPositions = function(){
        $('tr', '#tableOfPositions').remove();
        var mm = "<tr>";
        mm += "<td style='width: 50px;'>No</td>";
        mm += "<td style='width: 150px;'>Equipo</td>";
        mm += "<td style='width: 50px;'>PJ</td>";
        mm += "<td style='width: 50px;'>PG</td>";
        mm += "<td style='width: 50px;'>PE</td>";
        mm += "<td style='width: 50px;'>PP</td>";
        mm += "<td style='width: 50px;'>GF</td>";
        mm += "<td style='width: 50px;'>GC</td>";
        mm += "<td style='width: 50px;'>GD</td>";
        mm += "<td style='width: 50px;'>PTOS</td>";
        var k = 1;
        $.each(total_teams, function(idx, obj){
            var tr = "<tr>";
            tr += "<td>"+k+"</td>";
            tr += "<td>"+obj.name+"</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "<td>0</td>";
            tr += "</tr>";
            mm += tr;
            k++;
        });
        $("#tableOfPositions").append(mm);
    }

});


