/***
 * Requiere jquery.autocomplete.combobox
 * script reutilizable para el index de cada empresa
 **/
$(document).ready(function(){
    /** INICIO ORIGEN-DESTINO-TIPO-VIAJE**/
    $('.btnPrePurchase').button({
        icons:{
            primary: 'ui-icon-cart'
        },
        text: true
    });
    function loadDepartures(){
        $('#cmbDepartures').icombobox({width:270});
        $('#cmbArrivals').icombobox({width:270});
        $.ajax({
            url:'/public/welcome/listDeparturesToExternal'+jquery_params,
            data:objWsid,
            dataType: 'json',
            type: "POST",
            beforeSend:function(){
              $('#bx_loader').show();  
            },
            complete:function(){
              $('#bx_loader').hide();  
            },
            success:function(response){
                objWsid.wsid = response.wsid;
                $('option', '#cmbDepartures').remove();
                if(response.code==code_success){
                    if(response.data.length>0){
                        $('#cmbDepartures').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g').replace('[CITY_NEAR]','', 'g'));
                        $.each(response.data, function(index, obj) {
                            $('#cmbDepartures').append(option_template.replace('[VALUE]', obj.id,'g').replace('[LABEL]',obj.value, 'g').replace('[CITY_NEAR]',obj.cityNear, 'g'));
                        });
                        $('#cmbDepartures').icombobox({
                            width:270,
                            selected:function(event,ui){
                                /*$('#idDeparture').val($(ui.item).val());
                                $('#idCityNearArrival').val('');
                                $('#idArrivalMultiroute').val('');
                                $('#idMultiroute').val('');
                                if( $(ui.item).data('cn')!=null){
                                    $('#idCityNearDeparture').val($(ui.item).data('cn'));
                                }else{
                                    $('#idCityNearDeparture').val('');
                                }
                                loadArrivals($(ui.item).val(),$(ui.item).data('cn'));
                                $('#idArrival').val('');
                                $('#cmbArrivals').val('');
                                $('#cmbArrivals-combobox').val('');*/
                                //console.log(ui);
 
                                $('#idDeparture').val(ui.value);
                                $('#idCityNearArrival').val('');
                                $('#idArrivalMultiroute').val('');
                                $('#idMultiroute').val('');
                                if( $(ui.item).data('cn')!=null){
                                    $('#idCityNearDeparture').val($(ui.item).data('cn'));
                                }else{
                                    $('#idCityNearDeparture').val('');
                                }
                                loadArrivals($(ui.item).val(),$(ui.item).data('cn'));
                                $('#idArrival').val('');
                                $('#cmbArrivals').val('');
                                $('#cmbArrivals-icombobox-input').val('');
                                //.focus();
                                $('#cmbArrivals-icombobox-input').focus();
                            }
                        });
                         $('#cmbDepartures-icombobox-input').focus();
                        if($('#idDeparture').length>0){
                            $('#cmbDepartures').val($('#idDeparture').val());
                            $('#cmbDepartures-icombobox-input').val($('#cmbDepartures option:selected').text());
                            loadArrivals($('#idDeparture').val(),$('#idCityNearDeparture').val());
                        }
                        if($('#dDate').length>0){
                            $('#dDate').val($('#departureDate').val());
                        }
                        if($('#aDate').length>0){
                            $('#aDate').val($('#returnDate').val());
                        }
                        if($('input#travelway').length>0){
                            if($('input#travelway').val()=="") {
                                $('input#travelway').val(0);
                                $('p.step_1').show();
                            }
                            if(!Boolean(eval($('input#travelway').val()))){
                                $('.returnDateGroup').hide();                                
                            }else {
                                $('.returnDateGroup').show();
                            }
                        }
                    } else {
                        $('#cmbDepartures').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g').replace('[CITY_NEAR]','', 'g'));
                        $('#cmbDepartures').icombobox({width:270});
                    }
                }else {
                    msgBox(response.msg,response.code);
                }
            }
        });
    }
    loadDepartures();
    //$('#cmbArrivals').combobox();
    //    reloadInputUI();
    $('div#travelway').buttonset();
    fnLoadCheck();
    function loadArrivals(idd,cn){
        var params = new Object();
        params.idd = idd;
        params.wsid = objWsid.wsid;
        params.idx = objWsid.idx;
        if(cn!=null){
            params.cn = cn;
        }
        $.ajax({
            url:'/public/welcome/listArrivalsToExternal'+jquery_params,
            async:false,
            data:params,
            dataType: 'json',
            type: "POST",
            beforeSend:function(){
              $('#bx_loader').show();  
            },
            complete:function(){
              $('#bx_loader').hide();  
            },
            success:function(response){
                objWsid.wsid = response.wsid;
                $('option', '#cmbArrivals').remove();
                if(response.code==code_success){
                    if(response.data.length>0){
                        $('#cmbArrivals').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectDefaultOption, 'g').replace('[CITY_NEAR]','', 'g').replace('[MULROU]','', 'g').replace('[IDMULROU]','', 'g').replace('[PRICE]','', 'g').replace('[RTPRICE]','', 'g'));
                        $.each(response.data, function(index, obj) {
//                            $('#cmbArrivals').append(option_template.replace('[VALUE]', obj.id,'g').replace('[LABEL]',obj.label, 'g').replace('[CITY_NEAR]',obj.cityNear, 'g').replace('[MULROU]',obj.multiroute, 'g').replace('[IDMULROU]',obj.idMultiroute, 'g').replace('[PRICE]',obj.amount, 'g').replace('[RTPRICE]',obj.roundAmount, 'g'));
                            $('#cmbArrivals').append(option_template.replace('[VALUE]', obj.id,'g').replace('[LABEL]',obj.value, 'g').replace('[CITY_NEAR]',obj.cityNear, 'g'));
                        });
                        //$('#cmbArrivals').combobox('refresh');
                        $('#cmbArrivals').icombobox({
                            width:270,
                            selected:function(event,ui){
                                $('#idArrival').val($(ui.item).val());
                                if( $(ui.item).data('cn')!=null){
                                    $('#idCityNearArrival').val($(ui.item).data('cn'));
                                }
                                if( $(ui.item).data('mr')!=null){
                                    $('#idArrivalMultiroute').val($(ui.item).data('mr'));
                                }
                                if( $(ui.item).data('imr')!=null){
                                    $('#idMultiroute').val($(ui.item).data('imr'));
                                }
                                $('#departureDate').focus();
                            }
                        });
                        if($('#idArrival').length>0){
                            $('#cmbArrivals').val($('#idArrival').val());
                            $('#cmbArrivals-icombobox-input').val($('#cmbArrivals option:selected').text());
                        }
                        
                    } else {
                        $('#cmbArrivals').append(option_template.replace('[VALUE]', '','g').replace('[LABEL]',msgSelectEmpty, 'g').replace('[CITY_NEAR]','', 'g').replace('[MULROU]','', 'g').replace('[IDMULROU]','', 'g'));
                    }
                }else {
                    msgBox(response.msg,response.code);
                }
            }
        });
    }
    $('div#travelway input[type=radio],input[name=rbtnTravelway]').change(function() {
        if(Boolean(eval($('div#travelway ').data('qws')))){
            /*$.each($('div#travelway').find('label'),function(){
                if($(this).hasClass('ui-state-active')){
                    $(this).css('font-size','14px');
                } else{
                    $(this).css('font-size','12px');
                }
            });*/
        }else{
            $('input#travelway').val($(this).val());
        }
        if(Boolean(eval($(this).val()))){
            $('.returnDateGroup').show();
        } else{
            $('.returnDateGroup').hide();
        }
    });
    
    $('#cmbDepartures').change(function(){
        $('#cmbDepartures-icombobox-input').val($(this).find('option:selected').text());
        $('#idDeparture').val($(this).val());
    });
    $('#cmbArrivals').change(function(){
        $('#cmbArrivals-icombobox-input').val($(this).find('option:selected').text());
        $('#idArrival').val($(this).val());
    });
    
    $('#saleWebSearchFrm').validate({
        submitHandler:function(form){
            var data = $(form).serializeObject();
            if(data.idDeparture=='') return false;
            if(data.idArrival=='') return false;
            if(data.rbtnTravelway=='') return false;
            if(data.idArrivalMultiroute=='') return false;
            else{
                if(Boolean(eval(data.rbtnTravelway))){
                    if(data.departureDate=='') return false;       
                    if(data.returnDate=='') return false;       
                } else{
                    if(data.departureDate=='') return false; 
                }
            }
            form.submit();
        }
    });
});
