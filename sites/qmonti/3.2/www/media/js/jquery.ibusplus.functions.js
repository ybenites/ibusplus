
function setCSSUploader(){
    $('.qq-upload-button').addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only');
    $('.qq-upload-button').hover(function(){
        if($(this).hasClass('ui-state-active')){
            $(this).removeClass('ui-state-active');
        }else {
            $(this).addClass('ui-state-active');
        }

    });
}

function uploaderUserInfo(){
    var uploader = new qq.FileUploader({
        element:document.getElementById('btnImgUpload_ui'),
        action: '/private/welcome/getUploadPhotoTMP',
        allowedExtensions: ['jpg','jpeg','png'],
        onSubmit: function(id, fileName){
            $('#imgProfile_file').val('');
            $('#imgProfile_ui').css('background-position','center');
            $('#imgProfile_ui').css('background-repeat','none');
            $('#imgProfile_ui').css('background-image','url(/media/images/'+skin+'/img_loading.gif)');
        },  
        onComplete: function(id, fileName, responseJSON){
            if(responseJSON.code==code_success && responseJSON.success=='true'){
                $('#imgProfile_file').val(responseJSON.data.tmpfn);
                $('#imgProfile_ui').css('background-repeat','none');
                $('#imgProfile_ui').css('background-image','url('+responseJSON.data.path+')');
            } else{
                msgBox(responseJSON.msg, responseJSON.code);
            }
        }
    });
    setCSSUploader();
}

function setRadio(id) {
    var radio = $('#' + id);
    radio[0].checked = true;
    radio.button("refresh");
}

function confirmBox(message,title,fn)
{
    $("div.confirmBox").remove();
    var confirmBoxDialog = $(document.createElement('div')).addClass("confirmBox").text(message).appendTo($("body"));
    confirmBoxDialog.dialog({
        autoOpen: false,
        modal:true,
        title:title,
        buttons:[{
            text: okButtonText,
            click: function() {
                $(this).remove();
                fn(true);
            }
        },{
            text: cancelButtonText,
            click: function() {
                $(this).remove();
                fn(false);
            }
        }],
        close: function(event, ui)
        {
            fn(false);
            $(this).remove();
        },

        open: function(event, ui)
        {
            $(this).focus();
        }
    });

    $("div.confirmBox").dialog('open');
}

function msgBox(text, title, after)
{
    $("div.msgbox").remove();
    var msgBoxDialog = $(document.createElement('div')).addClass("msgbox").html(text).appendTo($("body"));
    msgBoxDialog.dialog({
        autoOpen: true,
        modal:true,
        title:title,
        position:'center',
        minWidth:400,
        buttons:[{
            text: okButtonText,
            click: function() {
                $(msgBoxDialog).remove();
                if ( typeof(after) === 'function' ) after();   
            }
        }],
        close: function(event, ui)
        {
            $(msgBoxDialog).remove();
            if ( typeof(after) === 'function' ) after();
        },
        open: function(event, ui)
        {
            $(msgBoxDialog).find("button").focus();
        }
    });       
}

function maximizeClientArea(){
    if ( !Boolean(full_screen_mode) )
    {   
        $('#header').show();
        $('#content').removeAttr('style');
    } else {
        $('#header').hide();
        $('#content').attr('style','top:0px;');                    
    }
}

function maximizeTrueClientArea(){
    if ( !Boolean(full_true_screen_mode) )
    {   
        $('#header,#wrapper,#subfooter').show();
        $('#content').removeAttr('style');
    } else {
        $('#header,#wrapper,#subfooter').hide();
        $('#content').attr('style','top:0px;');
    }
}

function switch2FullScreen(){
    $.ajax({
        dataType:'json',
        url:'/private/welcome/getFullScreenMode'+jquery_params,
        success:function(response){
            if(response.code == code_success){
                full_screen_mode = parseInt(response.data.value);
                maximizeClientArea();
            } else {
                msgBox(response.msg,response.code);
            }
        }
    });
}

function switch2TrueFullScreen(){
    $.ajax({
        dataType:'json',
        url:'/private/welcome/getTrueFullScreenMode'+jquery_params,
        success:function(response){
            if(response.code == code_success){
                full_true_screen_mode = parseInt(response.data.value);
                maximizeTrueClientArea();
            } else {
                msgBox(response.msg,response.code);
            }
        }
    });

}

function evalResponse(response){   
    var evalResponse=false;
    if(response.code == code_success){
        evalResponse= true;
    } else{
        evalResponse =false;
        var error = response.msg ;
        var errorText = $(document.createElement('div'));
        if(typeof error ==='string'){
            errorText = error;
        }else if(error.eEnviroment==10){
            errorText.append($(document.createElement('p')).text(error.eText).attr("style","text-align:left;"));
            errorText.append($(document.createElement('div')).attr("style","line-height:30px; text-align:center;").append($(document.createElement('b')).text(sendSupportEmail)).append($(document.createElement('br'))));
            var imgLink = $(document.createElement('img')).attr("src","/media/ico/ico_send_mail.png").attr("title",sendSupportEmail).attr("style","cursor:pointer;");
            $.data(imgLink.get(0),error);
            errorText.find("div").append(imgLink);
            imgLink.click(function(){
                showLoading = 1;
                $('div.msgBox').dialog('close');
                $.ajax(
                {
                    url: supportURL,
                    data:$(this).data(),
                    success: function(response)
                    { 
                        if (response.code == code_success) msgBox(sendSupportEmailSuccess);
                        else msgBox(response.msg,response.code);          
                    }
                });
            });
        }else{            
            errorText.append($(document.createElement('div')).append($(document.createElement('b')).text('Código:')).append($(document.createElement('br'))).append(error.eCode).append($(document.createElement('br'))));
            errorText.append($(document.createElement('div')).append($(document.createElement('b')).text('Mensaje:')).append($(document.createElement('br'))).append(error.eText).append($(document.createElement('br'))));
            errorText.append($(document.createElement('div')).append($(document.createElement('b')).text('Línea:')).append($(document.createElement('br'))).append(error.eLine).append($(document.createElement('br'))));
            errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('Archivo:')).append($(document.createElement('br'))).append(error.eFile).append($(document.createElement('br'))));
            errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('Pila Error:')).append($(document.createElement('br'))).append(error.eTrace).append($(document.createElement('br'))));
            errorText.append($(document.createElement('pre')).append($(document.createElement('b')).text('Parámetros:')).append($(document.createElement('br'))).append(error.rParams).append($(document.createElement('br'))));
        }
        msgBox(errorText,response.code);
    }
    return evalResponse;
}


function fnLoadCheck(){    
    $.each($('input:checkbox.ui-helper-hidden-accessible, input:radio.ui-helper-hidden-accessible'), function(){
        if($(this).is(':checked')){
            $(this).button('option',{
                icons:{
                    primary: 'ui-icon-check'
                }
            }); 
        }else{                
            $(this).button('option',{
                icons:{
                    primary: 'ui-icon-minus'
                }
            });
        }
            
    });
    $('input:checkbox.ui-helper-hidden-accessible, input:radio.ui-helper-hidden-accessible').change(function(){        
        var input_type = $(this).attr('type');
        if(input_type == 'checkbox'){
            if($(this).is(':checked')){
                $(this).button('option', {
                    icons:{
                        primary:'ui-icon-check'
                    }
                });
            }else{
                $(this).button('option', {
                    icons:{
                        primary:'ui-icon-minus'
                    }
                });
            }
        }else {
            if(input_type == 'radio') {
                var input_name = $(this).attr('name');
               
                $.each($('input:radio[name='+input_name+']'), function(){
                    $(this).button('option', {
                        icons:{
                            primary:'ui-icon-minus'
                        }
                    });
                    $('label[for='+$(this).attr('id')+']').removeClass('ui-state-hover');
                });
               
                $(this).button('option', {
                    icons:{
                        primary:'ui-icon-check'
                    }
                });
               
            }
        }        
    });
}
