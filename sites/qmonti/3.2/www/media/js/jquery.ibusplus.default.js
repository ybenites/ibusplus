
$(document).ready(function(){
     $('#btnCleaningCache').live('click',function(e){
           e.preventDefault();
                    $.ajax({
                    url:'/private/messages/cleaningCache'+jquery_params,
                    data: {},
                    success: function(r){
                         if(evalResponse(r)) {
                            msgBox(txtprocessOk,txtprocessOk, function(){
                                location.reload();
                            });
                        }
                    }
               });   
    });
    $('#btnRegenerateLanguages').live('click',function(e){
           e.preventDefault();
                    $.ajax({
                    url:'/private/messages/cleaningLanguages'+jquery_params,
                    data: {},
                    success: function(r){
                         if(evalResponse(r)) {     
                            msgBox(txtprocessOk,txtprocessOk, function(){
                                location.reload();
                            });
                        }
                    }
               });   
    });
    $('#btncleaningSessionUsers').live('click',function(e){
           e.preventDefault();
                    $.ajax({
                    url:'/private/messages/cleaningSessionsUsers'+jquery_params,
                    data: {},
                    success: function(r){
                         if(evalResponse(r)) {     
                            msgBox(txtprocessOk,txtprocessOk, function(){
                                location.reload();
                            });
                        }
                    }
               });   
    });
    /**
     * Configuraciones jQuery
     **/
    
    $.ajaxSetup({
        type:'POST',
        dataType:'json',
        beforeSend : function(){
            if (Boolean(eval(showLoading))) showLoader();
        },
        complete: function(){                
            if (Boolean(eval(showLoading))) hideLoader();
            showLoading=0;
        },
        error: function(jqXHR, textStatus, errorThrown){
            msgBox(errorMessage);
        }
    });  
    
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    
    $.fn.serializeDisabled = function() {
        var obj = {};

        $(':disabled', this).each(function () { 
            if($(this).is(':radio')){
                $.each($(this), function(){
                    if($(this).is(':checked'))
                        obj[this.name] = $(this).val();
                });
            }else{
                obj[this.name] = $(this).val();
            }
            
             
        });
        return obj;
    };
    
    jQuery.validator.setDefaults({      
        errorElement: "em"
    });
     
    jQuery.extend(jQuery.validator.messages, {
        required: fieldRequired,
        remote: 'fieldRequired'
    });
    
    /**
     * Serializa un formulario a un objeto javascript
     * cada elemento debe tener definido el atributo name
     */
    $.fn.serializeObject = function() {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    
    /*
    * Elimina un elemento de un arreglo por su valor
    **/
    $.removeFromArray = function(value, arr) {
        return $.grep(arr, function(elem, index) {
            return elem !== value;
        });
    };
    
    /**
     * Centra una elemento html en la pantalla
     */
    $.fn.center = function () {
        this.css("position","absolute");
        this.css("top", (($(window).height() - this.outerHeight()) / 2) + 
            $(window).scrollTop() + "px");
        this.css("left", (($(window).width() - this.outerWidth()) / 2) + 
            $(window).scrollLeft() + "px");
        return this;
    }
    
    function showLoader(){
        var loader = $(document.createElement('div')).attr("id","loading").attr("style","z-index:9999;")
        .addClass('ui-corner-all ui-widget-content')
        .append($(document.createElement('p')).attr("style","line-height: 60px;").attr("align","center").text(txtLoading).append($(document.createElement('img')).attr("src",imgLoading).attr("style","float: right;")));
        loader.appendTo($("body"));
        loader.center();
        $(document.createElement('div')).attr("id","loading_overlay").addClass("ui-widget-overlay").appendTo($("body")).attr("style","z-index:9998;");
    }
    function hideLoader(){
        $("#loading,#loading_overlay").remove();
    }
    
    
    
});

function getCurrentCorrelative(documentType){
    return getCorrelative(documentType);
}

function getNextCorrelative(documentType){
    return getCorrelative(documentType,true);
}
function getCorrelative(documentType,next){
    var tmp_url ='/private/orderform/getCurrentCorrelative'+jquery_params; 
    if(next){
        tmp_url ='/private/orderform/getNextCorrelative'+jquery_params; 
    }
    var current = null;
    $.ajax({
        url:tmp_url,
        data:{
            dt:documentType
        },
        async:false,
        success:function(response){
                 if(evalResponse(response)) {                               
                    current = response.data;
                }
            
        }    
    });
    return current;
}
