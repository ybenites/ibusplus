<?php

defined('SYSPATH') or die('No direct script access.');
// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;
if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

include_once (APPPATH . 'config' . DIRECTORY_SEPARATOR . SITE_DOMAIN . '_db' . EXT);
/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('es-bts');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
Kohana::$environment = db_config::$db_conection_config[SITE_DOMAIN]['environment'];

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
register_shutdown_function(function() {
            Kohana::shutdown_handler();
        });
Kohana::init(array(
    'base_url' => '/',
    'index_file' => ''
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
$modules = array(
    'helpers' => DEVMODPATH . '3.2' . DIRECTORY_SEPARATOR . 'helpers'
    , 'libraries' => DEVMODPATH . '3.2' . DIRECTORY_SEPARATOR . 'libraries'
    , 'bts' => DEVMODPATH . '3.2' . DIRECTORY_SEPARATOR . 'bts'
    , 'cache' => MODPATH . 'cache'      // Caching with multiple backends
    , 'database' => MODPATH . 'database'   // Database access
    , 'image' => MODPATH . 'image'      // Image manipulation
    , 'orm' => MODPATH . 'orm'        // Object Relationship Mapping
    , 'ehandler' => DEVMODPATH . '3.2' . DIRECTORY_SEPARATOR . 'ehandler'
    , 'logviewer' => DEVMODPATH . '3.2' . DIRECTORY_SEPARATOR . 'logviewer'
);

if (Kohana::$environment == Kohana::DEVELOPMENT) {
    if (!defined('KOHANA_START_TIME')) {
        define('KOHANA_START_TIME', microtime(TRUE));
    }
    if (!defined('KOHANA_START_MEMORY')) {
        define('KOHANA_START_MEMORY', memory_get_usage());
    }
    unset($modules['ehandler']);
}
Kohana::modules($modules);
Cache::$default = 'apc';
Cookie::$salt = 'salt_' . SITE_DOMAIN;
//Cache::$default = 'apc';
//==================================IBUS+=======================================
/*
  /**
 * Configuración para generar el idioma y la traducción
 */
$url_part = explode('/', db_config::$db_conection_config[SITE_DOMAIN]['defaultIndex']);
$directory = $url_part[0];
$controller = $url_part[1];
$action = $url_part[2];
$timeZone = db_config::$db_conection_config[SITE_DOMAIN]['timeZone'];
date_default_timezone_set($timeZone);

//==================================IBUS+=======================================

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
Route::set(
                'default'
                , '(<directory>/<controller>(/<action>(/<id>)))(.<format>)(/<params>)'
                , array(
            'id' => '[0-9]+',
            'format' => '(html|json|xml)'
                )
        )
        ->defaults(
                array(
                    'directory' => $directory,
                    'controller' => $controller,
                    'action' => $action,
                    'format' => 'html'
                )
);