function fnLoadScript(s_script_src, o_callback) {
    var o_head = document.getElementsByTagName('head')[0];
    var o_script = document.createElement('script');
    o_script.type = 'text/javascript';
    o_script.src = s_script_src;
    // most browsers    
    o_script.onload = o_callback;
    // IE 6 & 7
    o_script.onreadystatechange = function() {           
        if (this.readyState == 'loaded' || this.readyState == 'complete') {        
            o_callback();
        }
    };
    o_head.appendChild(o_script);
}

function fnExecuteYepnope(){
    yepnope.errorTimeout = 5000;    
    var a_yepnope_scripts = [];    
    if(IBUS_SALE_WEB.config.load_jquery_css_ui){
        a_yepnope_scripts.push({
            load: [                 
                'http://static.css.bts.mcets-inc.com/kernel/css/bts/ui/jquery-ui-1.10.3.min.css'
            ]
        });        
    }
    if(IBUS_SALE_WEB.config.load_boostrap_css){
        a_yepnope_scripts.push({
            load: [
                'http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap.2.3.1.min.css',
                'http://static.css.bts.mcets-inc.com/kernel/css/bts/bootstrap/bootstrap-responsive.2.3.1.min.css'
            ]
        });        
    }
    if(IBUS_SALE_WEB.config.load_jquery){
        a_yepnope_scripts.push({
            load: 'http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-2.0.0.min.js'
        });        
    }
     if(IBUS_SALE_WEB.config.load_jquery_ui){
        a_yepnope_scripts.push({
            load: [
                'http://static.js.bts.mcets-inc.com/kernel/js/vendor/jquery-ui-1.10.3.min.js'
            ]
        });
    }
    if(IBUS_SALE_WEB.config.load_boostrap){
        a_yepnope_scripts.push({
            load: [                
                'http://static.js.bts.mcets-inc.com/kernel/js/vendor/bootstrap.2.3.1.min.js'
            ]
        });        
    }
    if(IBUS_SALE_WEB.config.load_select2){
        a_yepnope_scripts.push({
            load: [
                'http://static.css.bts.mcets-inc.com/kernel/css/bts/select2/select2.min.css',
                'http://static.js.bts.mcets-inc.com/kernel/js/bts/core/select2.min.js'
            ]
        });        
    }
    if(IBUS_SALE_WEB.config.load_datetimepicker){
        a_yepnope_scripts.push({
            load: [
                'http://static.css.bts.mcets-inc.com/kernel/css/bts/datetimepicker/bootstrap-datetimepicker.min.css',
                'http://static.js.bts.mcets-inc.com/kernel/js/bts/core/bootstrap-datetimepicker.min.js'        
            ]
        });        
    }
    a_yepnope_scripts.push({
        load: [
           'http://static.travelynx.bts.mcets-inc.com/js/public/travelynx.script.web.sale.min.js', 
           'http://static.travelynx.bts.mcets-inc.com/css/public/default.web.sale.min.css'
        ], 
        complete: function(){
        }
    });
    yepnope(a_yepnope_scripts);
}

IBUS_SALE_WEB = new Object();
IBUS_SALE_WEB.init = function(o_settings){        
    fnExtend(IBUS_SALE_WEB.config , o_settings);    
    console.log(IBUS_SALE_WEB.config.local);
    if (IBUS_SALE_WEB.config.local == true){
    IBUS_SALE_WEB.config.host = IBUS_SALE_WEB.config.protocol+'://'+IBUS_SALE_WEB.config.client+'/';
    }else{        
        IBUS_SALE_WEB.config.host = 'http://travelynx.mcets-inc.com/';
    }
    
    IBUS_SALE_WEB.config.url = 'public/service/webSaleForm.html';    
    if(this.config.load_yepnope){
        fnLoadScript('http://static.travelynx.bts.mcets-inc.com/js/public/yepnope.1.5.4.min.js', function(){
            fnExecuteYepnope();
        });    
    }else{
        fnExecuteYepnope();
    }
};

IBUS_SALE_WEB.config = {
    load_jquery : true , 
    load_jquery_ui: true , 
    load_jquery_css_ui: true , 
    load_boostrap: true , 
    load_boostrap_css: true , 
    load_select2: true , 
    load_datetimepicker: true , 
    load_yepnope: true ,
    load_css: true ,
    protocol: 'http'
};
fnExtend = function(o_first, o_second){    
    for (var prop in o_second){               
        o_first[prop] = o_second[prop];
    }    
};
window.ibusAsyncInit();