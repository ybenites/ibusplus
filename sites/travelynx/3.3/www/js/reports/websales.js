var d = '';
var sd = '';
var ed = '';
var myDate = new Date();
var rer = false;
var typeSearch = 'tbd';

$(document).ready(function() {

    function fnGetCurrentDate() {
        var y = myDate.getFullYear();
        var m = (myDate.getMonth() + 1).toString();
        var d = (myDate.getDate()).toString();
        if (m.length === 1)
            m = '0' + m;
        if (d.length === 1)
            d = '0' + d;
        return m + '-' + d + '-' + y;
    }

    function fnSelect2(etiq, placeholder) {
        etiq.select2({
            placeholder: placeholder,
            width: 'resolve',
            allowClear: true
        });
    }
    fnSelect2($("#listMonth"), s_place_holder_month);
    fnSelect2($("#listYear"), s_place_holder_year);
    $('#date,#sdate,#edate').datetimepicker({
        format: 'mm-dd-yyyy'
                , viewSelect: 'month'
                , minView: 'month'
                , autoclose: true

    });

    $("#tbd").click(function() {
        $('#show_date').removeClass('hide');
        $('#show_dowloand').removeClass('hide');
        $("#date").val(fnGetCurrentDate());
        $('#show_rdate').addClass('hide');
        $('#show_mdate').addClass('hide');
        typeSearch = 'tbd';
    });
    $("#tbr").click(function() {
        $('#show_rdate').removeClass('hide');
        $('#show_dowloand').removeClass('hide');
        $("#sdate").val(fnGetCurrentDate());
        $("#edate").val(fnGetCurrentDate());
        $('#show_date').addClass('hide');
        $('#show_mdate').addClass('hide');
        typeSearch = 'tbr';
    });
    $("#tbm").click(function() {
        $('#show_mdate').removeClass('hide');
        $('#show_dowloand').removeClass('hide');
        fnSelect2($("#listMonth"), s_place_holder_month);
        fnSelect2($("#listYear"), s_place_holder_year);
        $('#show_date').addClass('hide');
        $('#show_rdate').addClass('hide');
        typeSearch = 'tbm';
    });

    //Busqueda del Reporte
    $('#btnSearch').click(function() {
        rer = true;
        d = $("#date").val();
        sd = $("#sdate").val();
        ed = $("#edate").val();
        a = $("#listYear").val();
        m = $("#listMonth").val();

        var params = new Object();
        if (typeSearch === "tbd")
            params.d = d;
        if (typeSearch === "tbr") {
            params.sd = sd;
            params.ed = ed;
        }
        if (typeSearch === "tbm") {
            if (m === 0 || a === 0) {
                alert('<?php echo __("Seleccione Mes y Año"); ?>');
                return false;
            } else {
                params.m = m;
                params.a = a;
            }
        }
        $('#report_options').iReportingPlus('option', 'dynamicParams', params);
        $('#report_options').iReportingPlus('generateReport', 'html');

    });
    //fin boton buscar

    $('#report_options').iReportingPlus({
        domain: s_domain,
        staticParams: rParams,
        repopt: ['html', 'pdf', 'xlsx'],
        clientFolder: 'travelynx',
        clientFile: 'TRAVELYNX_WEB_SALES',
        htmlVisor: true,
        urlData: '/reports/websales/reportWebSales',
        xpath: '/report/response/row',
        orientation: 'vertical',
        jqUI: true,
        responseType: 'jsonp',
        reportZoom: 1.2,
        jqCallBack: s_jsonp_callback
    });

});

