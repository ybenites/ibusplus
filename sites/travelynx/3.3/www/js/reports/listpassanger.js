var d = '';
var sd = '';
var ed = '';
var myDate = new Date();
var rer = false;
var typeSearch = 'tbd';

$(document).ready(function() {

    function fnGetCurrentDate() {
        var y = myDate.getFullYear();
        var m = (myDate.getMonth() + 1).toString();
        var d = (myDate.getDate()).toString();
        if (m.length === 1)
            m = '0' + m;
        if (d.length === 1)
            d = '0' + d;
        return m + '-' + d + '-' + y;
    }

    function fnSelect2(etiq, placeholder) {
        etiq.select2({
            placeholder: placeholder
                    ,
            width: 'resolve'
                    ,
            allowClear: true
        });
    }

    $('#date,#edate,#sdate').datetimepicker({
        format: 'mm-dd-yyyy'
                , viewSelect: 'month'
                , minView: 'month'
                , autoclose: true

    });
    $("#tbm").hide();
    fnSelect2($("#listRoute"), s_place_holder_route);

    $("#tbd").click(function() {
        $('#show_date').removeClass('hide');
        $('#show_dowloand').removeClass('hide');
        $("#date").val(fnGetCurrentDate());
        $('#show_rdate').addClass('hide');
        $('#show_mdate').addClass('hide');
        typeSearch = 'tbd';
    });
    $("#tbr").click(function() {
        $('#show_rdate').removeClass('hide');
        $('#show_dowloand').removeClass('hide');
        $("#sdate").val(fnGetCurrentDate());
        $("#edate").val(fnGetCurrentDate());
        $('#show_date').addClass('hide');
        $('#show_mdate').addClass('hide');
        typeSearch = 'tbr';
    });

    //Busqueda del Reporte
    $('#btnSearch').click(function() {
        rer = true;
        d = $("#date").val();
        sd = $("#sdate").val();
        ed = $("#edate").val();
        idRoute = $("#listRoute").val();

        var params = new Object();
        params.idRoute = idRoute;
        if (typeSearch === "tbd")
            params.d = d;
        if (typeSearch === "tbr") {
            params.sd = sd;
            params.ed = ed;
        }
        $('#report_options').iReportingPlus('option', 'dynamicParams', params);
        $('#report_options').iReportingPlus('generateReport', 'html');

    });
    //fin boton buscar

    $('#report_options').iReportingPlus({
        domain: s_domain,
        staticParams: rParams,
        repopt: ['html', 'pdf', 'xlsx'],
        clientFolder: 'travelynx',
        clientFile: 'TRAVELYNX_MANIFEST_PASSANGER',
        htmlVisor: true,
        urlData: '/reports/listpassanger/reportManifestPassanger',
        xpath: '/report/response/row',
        orientation: 'vertical',
        jqUI: true,
        responseType: 'jsonp',
        reportZoom: 1.2,
        jqCallBack: s_jsonp_callback
    });

});
