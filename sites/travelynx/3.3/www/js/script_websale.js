window.ibusAsyncInit = function() {
    IBUS_SALE_WEB.init({
        lang: 'ES',
        load_yepnope: true,
        load_jquery: true,
        load_jquery_ui: true,
        load_jquery_css_ui: true,
        load_boostrap: true,
        load_boostrap_css: true,
        load_select2: true,
        load_datetimepicker: true,
        date_format: 'US',
        local: true,
        client: 'travelynx'
    });
};

(function(d) {
    var js, id = 'travelynx.sdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "/js/public/travelynx.sdk.js";
    ref.parentNode.insertBefore(js, ref);
}(document));       