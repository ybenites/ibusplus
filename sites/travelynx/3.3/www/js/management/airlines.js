$(document).ready(function(){    
    fnListAirlinesGrid();
    form_airlines=$("#form_airlines").validate({
        rules: {
            fairlines_name: {
                required: true
            }
        },
        messages: {
            fairlines_name: {
                required: s_validate_required_fairlines_name
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {            
            if (b_ajax) {
                var s_url = s_url_airlines_save;
                if($('#fairlines_id').val() != ''){
                    s_url = s_url_airlines_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_airlines_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_airlines_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_airlines').modal('hide');
                            bootbox.alert(s_airlines_saved);
                            jqgrid_airlines.trigger('reloadGrid');
                        }                       
                    }
                });
            }
        }
    });
    $("#modal_airlines_btn_save").on('click',function(){
        $("#form_airlines").submit();
    });
    $('#modal_airlines').on('hidden', function() {       
        form_airlines.currentForm.reset();
        form_airlines.resetForm();
        $('input[type=hidden]', form_airlines.currentForm).val('');
        $('input, select, textarea', form_airlines.currentForm).parents('.control-group').removeClass('error');
    });
     $("table#jqgrid_airlines").on('click','.airlines_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_airlines_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_airlines = j_response.data;                        
                        $("#fairlines_id").val(o_airlines.airlines_id);
                        $("#fairlines_name").val(o_airlines.airlines_name);                        
                        $('#modal_airlines').modal('show');
                    }                    
                }
            });
        }
    });
    $("table#jqgrid_airlines").on('click','.airlines_delete',function(){
        var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_airlines_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_airlines_delete);
                                jqgrid_airlines.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });        
    });
});
function fnListAirlinesGrid(){
    jqgrid_airlines=$("#jqgrid_airlines").jqGrid({
        url: s_url_airlines_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_airlines_col_names,
        colModel: a_jqgrid_airlines_col_model,
        pager: "#jqgrid_airlines_pager",          
        height: 'auto',              
        viewrecords: true,   
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idAirlines',
        afterInsertRow: function(row_id, row_data){
            var s_html_airlines_edit = '<a data-id=' + row_id + ' class="btn btn-mini airlines_edit"><i class="icon-pencil"></i></a>';
            var s_html_airlines_delete = '<a data-id=' + row_id + ' class="btn btn-mini airlines_delete"><i class="icon-trash"></i></a>';
            jqgrid_airlines.setRowData(row_id, {                
                actions: s_html_airlines_edit+' '+s_html_airlines_delete
            });
        }
    });
    jQuery("#jqgrid_airlines").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
}