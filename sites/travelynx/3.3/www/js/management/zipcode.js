$(document).ready(function(){
    var form_zipcode=$("#form_zipcode").validate({
        rules: {
            fzipcode_number: {
                required: true
            }
        },
        messages: {
            fzipcode_number: {
                required: s_validate_required_fzipcode_number
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {            
            if (b_ajax) {
                var s_url = s_url_zipcode_save;
                if($('#fzipcode_id').val() != ''){
                    s_url = s_url_zipcode_update;
                }
                var data=$(o_form).serialize();
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: data
                    , beforeSend: function() {
                        $('#modal_zipcode_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_zipcode_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            bootbox.alert(s_zipcode_saved);                            
                            $("#modal_zipcode").modal('hide');
                            jqgrid_zipcode.trigger('reloadGrid');
                        }                        
                    }
                });
            }
        }
    });
    $('#modal_zipcode_btn_save').click(function() {
          $('#form_zipcode').submit(); 
    });
    $('#modal_zipcode').on('hidden', function() {       
        form_zipcode.currentForm.reset();
        form_zipcode.resetForm();
        $('input[type=hidden]', form_zipcode.currentForm).val('');
        $('input, select, textarea', form_zipcode.currentForm).parents('.control-group').removeClass('error');
    });
    fnListZipcodeGrid();
    $("table#jqgrid_zipcode").on('click','.zipcode_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_zipcode_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_zipcode = j_response.data;                        
                        $("#fzipcode_id").val(o_zipcode.zipcode_id);
                        $("#fzipcode_number").val(o_zipcode.zipcode_number);                        
                        $('#modal_zipcode').modal('show');
                    }                    
                }
            });
        }
    });
    $("table#jqgrid_zipcode").on('click','.zipcode_delete',function(){
        var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_zipcode_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_zipcode_delete);
                                jqgrid_zipcode.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });        
    });
});
function fnListZipcodeGrid(){
    jqgrid_zipcode=$("#jqgrid_zipcode").jqGrid({
        url: s_url_zipcode_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_zipcode_col_names,
        colModel: a_jqgrid_zipcode_col_model,
        pager: "#jqgrid_zipcode_pager",          
        height: 'auto',              
        viewrecords: true,   
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idZipCode',
        afterInsertRow: function(row_id, row_data){
            var s_html_zipcode_edit = '<a data-id=' + row_id + ' class="btn btn-mini zipcode_edit"><i class="icon-pencil"></i></a>';
            var s_html_zipcode_delete = '<a data-id=' + row_id + ' class="btn btn-mini zipcode_delete"><i class="icon-trash"></i></a>';
            jqgrid_zipcode.setRowData(row_id, {                
                actions: s_html_zipcode_edit+' '+s_html_zipcode_delete
            });
        }
    });
    jQuery("#jqgrid_zipcode").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
}