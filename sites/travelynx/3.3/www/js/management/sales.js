var btn_type = true;
var g_id_location_departure = 0;
var g_id_location_arrival = 0;
var array_sales_cart = new Array();
var myDate = new Date();
var g_route_id = 0;
var g_route_desc = "";
var g_route_departure = "";
var g_route_arrival = "";
var g_dep_serv_id = 0;
var g_arr_serv_id = 0;
var g_dep_sed_id = 0;
var g_arr_sed_id = 0;

var g_id_loc_dep = "";
var g_id_ser_dep = "";
var g_hq_dep = "";
var g_dt_start = "";
var g_id_loc_arr = "";
var g_id_ser_arr = "";
var g_hq_arr = "";
var g_dt_end = "";
var g_address_location = "";
var g_address_arrival = "";

var control_search_departure = true;
var control_search_arrival = true;

$(document).ready(function() {

//    var o_param = new Object();
//    o_param.id = 10;

//    $('#manufacturer').typeahead({
//        source: function(typeahead, query) {
//            console.log(typeahead);
//            console.log(query);
//            $.ajax({
//                url: s_url_get_user_paypal + jq_jsonp_callback,
//                type: "POST",
//                data: {th: query},
//                dataType: "JSON",
//                async: false,
//                success: function(results) {
//                    console.log(results.data);
//                    var manufacturers = new Array;
//                    $.map(results.data, function(data, item) {
//                        console.log(data);
//                        console.log(item);
//                        console.log(data.idsede);
//                        var group;
//                        group = {
//                            manufacturer_id: data.idsede,
//                            manufacturer: data.namesede
//                        };
//                        manufacturers.push(group);
//                    });
//                    console.log(manufacturers);
//                    typeahead.process(manufacturers);
//                }
//            });
//        },
//        property: 'name',
//        items: 11,
//        onselect: function(obj) {
//
//        }
//    });

    if (opt_all_travelers)
        $("#div_accordion_all_travelers").show();
    else
        $("#div_accordion_owner").show();

    $("#tooltip_help_schedule").tooltip({
        title: s_info_help_select_schedule,
        show: 500,
        hide: 100,
        placement: 'right'
    });

    if (typeof(array_search) !== 'undefined') {
        g_id_loc_dep = array_search['id_loc_dep'];
        g_id_ser_dep = array_search['id_ser_dep'];
        g_hq_dep = array_search['id_hq_dep'];
        g_dt_start = array_search['id_dt_start'];
        g_id_loc_arr = array_search['id_loc_arr'];
        g_id_ser_arr = array_search['id_ser_arr'];
        g_hq_arr = array_search['id_hq_arr'];
        g_dt_end = array_search['id_dt_end'];
    }

    $("#full_content").show();

    $("#modal_pay,#modal_print").modal({
        "backdrop": "static",
        "keyboard": true,
        "show": false
    });
    fnSelectToSelect2("#cmb_location_departure", s_txt_location, g_id_loc_dep);
    fnSelectToSelect2("#cmb_service_departure", s_txt_service, g_id_ser_dep);
    fnSelectToSelect2("#cmb_headquarter_departure", s_txt_headquarter, g_hq_dep);

    fnSelectToSelect2("#cmb_location_arrival", s_txt_location, g_id_loc_arr);
    fnSelectToSelect2("#cmb_service_arrival", s_txt_service, g_id_ser_arr);
    fnSelectToSelect2("#cmb_headquarter_arrival", s_txt_headquarter, g_hq_arr);

    fnSelectToSelect2("#cmb_type_card", '');
    fnSelectToSelect2("#cmb_state", s_txt_department);
    fnSelectToSelect2("#cmb_expiration_year", '');
    fnSelectToSelect2("#cmb_country", '');
    fnSelectToSelect2("#cmb_expiration_month", '');
    fnSelectToSelect2("#select_hour_travel_ow", '');
    fnSelectToSelect2("#select_hour_travel_rt", '');

    fnSelectToSelect2("#cmb_location_departure_", s_txt_personalized);
    fnSelectToSelect2("#cmb_airline", s_txt_airline);

    if (typeof(array_search) !== 'undefined') {
        fnGetAllItinerary();
    }

    $("#continue_pd").click(function() {
        fnChekValNotEmptyInput("#personal_details");
    });

    $("#continue_owner").click(function() {
        fnChekValNotEmptyInput("#dates_owner");
    });

    function fnChekValNotEmptyInput(div) {
        var inpreq = $(div).find('table .required');
        var t = 0;
        $.each(inpreq, function(i, o) {
            var val = $(o).val();
            if (val == "") {
                t = 1;
                $(o).focus();
                return false;
            }
        });
        if (t == 0)
            $("#div_collapse_two").trigger("click");
    }

    $("#continue_od").click(function() {
        $("#div_collapse_three").trigger("click");
    });

    $('#div_datetime_travel_ow').datetimepicker({
        format: 'mm-dd-yyyy HH:ii:ss',
        //format: 'mm-dd-yyyy',
        viewSelect: 'month',
        minView: 'month',
        autoclose: true,
        startDate: date_now
    }).on('changeDate', function(ev) {
        var date_select = new Date(ev.date);
        var new_date = date_select.getDate();
        date_select.setDate(new_date);
        $('#div_datetime_travel_rt').datetimepicker('setStartDate', date_select);
        $('#input_datetime_travel_rt').val($("#input_datetime_travel_ow").val());
    });

    $('#div_datetime_travel_rt').datetimepicker({
        //format: 'yyyy-mm-dd',
        format: 'mm-dd-yyyy',
        viewSelect: 'month',
        minView: 'month',
        autoclose: true,
        startDate: date_now
    });

    $("#cmb_location_departure").on('change', function() {
        fnLocationDepartureArrival(this, 'departure');
    });

    $("#cmb_location_arrival").on('change', function() {
        fnLocationDepartureArrival(this, 'arrival');
    });

    $("#cmb_service_departure").on('change', function() {
        fnServiceDepartureAndArrival(this, "departure");
    });

    $("#cmb_service_arrival").on('change', function() {
        fnServiceDepartureAndArrival(this, "arrival");
    });

    $("#cmb_headquarter_departure").on('change', function() {
        fnSedeDepartureArrival(this, "departure");
    });

    $("#cmb_headquarter_arrival").on('change', function() {
        fnSedeDepartureArrival(this, "arrival");
    });
    $("button#btn_cleanMyShoppingCart").on('click', function() {
        showloadingDialog = 1;
        $.ajax({
            url: s_url_delete_my_shopping_cart + jq_jsonp_callback,
            data: [],
            success: function(r) {
                if (evalResponse(r)) {

                }
            }
        });
    });

    var frm_sale = $("#frm_sale").validate({
        rules: {
            fsales_service: {
                required: true
            },
            fsales_departure: {
                required: true
            },
            fsales_arrival: {
                required: true
            }
        },
        messages: {
            fsales_service: {
                required: s_validate_required_fsale_service
            },
            fsales_departure: {
                required: s_validate_required_fsale_departure
            },
            fsales_arrival: {
                required: s_validate_required_fsale_arrival
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        },
        submitHandler: function(o_form) {
        }
    });

    var form_paypal_payment = $("#paypalPayment").validate({
        rules: {
            txt_firstName: {
                required: true
            },
            txt_lastName: {
                required: true
            },
            txt_address1: {
                required: true
            },
            txt_phone1: {
                required: true
            },
            txt_city: {
                required: true
            },
            cmb_state: {
                required: true
            },
            txt_zip: {
                required: true
            },
            cmb_type_card: {
                required: true
            },
            cmb_expiration_month: {
                required: true
            },
            cmb_expiration_year: {
                required: true
            },
            txt_creditCardNumber: {
                required: true,
                creditcard: true
            },
            txt_cvv2Number: {
                required: true
            },
            txt_email: {
                required: true
            }
        },
        messages: {
            txt_firstName: {
                required: s_validate_required_paypal_firstname
            },
            txt_lastName: {
                required: s_validate_required_paypal_lastname
            },
            txt_address1: {
                required: s_validate_required_paypal_address
            },
            txt_phone1: {
                required: s_validate_required_paypal_phone
            },
            txt_city: {
                required: s_validate_required_paypal_city
            },
            cmb_state: {
                required: s_validate_required_paypal_state
            },
            txt_zip: {
                required: s_validate_required_paypal_zipcode
            },
            cmb_type_card: {
                required: s_validate_required_paypal_typecard
            },
            cmb_expiration_month: {
                required: s_validate_required_expiration_month
            },
            cmb_expiration_year: {
                required: s_validate_required_expiration_year
            },
            txt_creditCardNumber: {
                required: s_validate_required_creditCardNumber
            },
            txt_cvv2Number: {
                required: s_validate_required_cvv2Number
            },
            txt_email: {
                required: s_validate_required_email
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        ,
        submitHandler: function(o_form) {
            var o_param = $('form#paypalPayment').serializeObject();
            if (opt_all_travelers)
                o_param.formtd = $('form#listPersonDetail').serializeObject();
            else
                o_param.formtd = $('form#listOwnerDetail').serializeObject();
            o_param.formod = $('form#optionalDates').serializeObject();
            o_param.idtsc = array_sales_cart;
            o_param.optalltraveler = opt_all_travelers;
            if (b_ajax) {
                $.ajax({
                    url: s_url_save_sales + jq_jsonp_callback,
                    async: false,
                    data: o_param,
                    beforeSend: function() {
                        $('#btn_sales_cart_save').button('loading');
                    },
                    complete: function() {
                        $('#btn_sales_cart_save').button('reset');
                    },
                    success: function(r) {
                        if (evalResponse(r)) {
                            $("#btn_option_search_filter_departure").trigger('click');
                            $("#btn_option_search_filter_arrival").trigger('click');
                            $('#div_route_by_param').addClass('hide');
                            sc = new Object();
                            sc.id = r.data.id;
                            sc.email = r.data.email;
                            bootbox.alert(s_info_was_saved_correctly, function() {
                                $('#modal_print').modal('show');
                                $("#btn_clean_all_payment").trigger('click');
                            });
                            $('#tbody tr').each(function(i, o) {
                                $($(o).remove());
                            });
                            array_sales_cart = [];
                            fnResetFormPayal();
                            if (opt_all_travelers)
                                $("#div_collapse_one").trigger("click");
                            else
                                $("#div_collapse_zero").trigger("click");
                        }
                    }
                });
            }
        }
    });

    $('#modal_print').on('show', function() {
        $("ul#printTickets").on('click', 'a#btn_printTicketOne', function() {
            fnDirectPrint(sc.id, 0, 0, 0);
            $('#modal_print').modal('hide');
        });
        $("ul#printTickets").on('click', 'a#btn_printTicketTwo', function() {
            fnDirectPrint(sc.id, 1, 0, sc.email);
            $('#modal_print').modal('hide');
        });
        $("ul#printTickets").on('click', 'a#btn_printTicketThree', function() {
            fnDirectPrint(sc.id, 1, 1, sc.email);
            $('#modal_print').modal('hide');
        });
        $("ul#printTickets").on('click', 'a#btn_printTicketFour', function() {
            $('#modal_print').modal('hide');
        });
    });
    $('#modal_print').on('hidden', function() {
        $("ul#printTickets").off('click', 'a#btn_printTicketOne');
        $("ul#printTickets").off('click', 'a#btn_printTicketTwo');
        $("ul#printTickets").off('click', 'a#btn_printTicketThree');
        $("ul#printTickets").off('click', 'a#btn_printTicketFour');
    });

    $("#btn_search_location").click(function() {
        fnGetAllItinerary();
    });

    $("#btn_option_search_zipcode_departure, #btn_option_search_zipcode_arrival").click(function() {
        var tt = $(this).attr('id').split('_')[4];
        if (tt == 'departure')
            control_search_departure = false;
        else
            control_search_arrival = false;
        $('#txt_zipcode_' + tt).val("").prop('disabled', false);
        $("#div_location_" + tt).addClass('hide');
        $("#div_input_zipcode_" + tt + ", #div_btn_search_zipcode_" + tt).removeClass('hide');
        if (!control_search_departure && control_search_arrival) {
            $("#div_location_arrival").removeClass('mleft15').addClass('mleft60');
            $("#div_btn_search_route").removeClass('mleft15').addClass('mleft40');
        } else if (control_search_departure && !control_search_arrival) {
            $("#div_btn_search_route").removeClass('mleft15').addClass('mleft40');
        }
    });

    $("#btn_option_search_filter_departure, #btn_option_search_filter_arrival").click(function() {
        var tt = $(this).attr('id').split('_')[4];
        if (tt == 'departure')
            control_search_departure = true;
        else
            control_search_arrival = true;
        $("#cmb_location_" + tt + ", #cmb_service_" + tt + ", #cmb_headquarter_" + tt).select2("val", "");
        $("#div_location_" + tt).removeClass('hide');
        $("#div_input_zipcode_" + tt + ", #div_btn_search_zipcode_" + tt + ", #div_msg_zip_code_no_found_" + tt + ", #div_address_by_zipcode_" + tt).addClass('hide');
        if (control_search_departure && control_search_arrival) {
            $("#div_location_arrival").removeClass('mleft60').addClass('mleft15');
            $("#div_btn_search_route").removeClass('mleft40').addClass('mleft15');
        }
    });

    function fnGetAllItinerary() {
        showloadingDialog = 1;
        $('#tooltip_help_schedule').tooltip('hide');
        var o_param = new Object();
        o_param.filter = 0;
        o_param.privat = 0;
        if (control_search_departure) {
            o_param.locdep = $("select#cmb_location_departure").val();
            o_param.serdep = $("select#cmb_service_departure").val();
            o_param.seddep = $("select#cmb_headquarter_departure").val();
        } else {
            if ($("#txt_zipcode_departure").val() == "") {
                bootbox.alert(s_info_entry_an_zipcode_departure);
                return false;
            }
            if ($("#txt_address_departure").val() == "") {
                bootbox.alert(s_info_entry_an_address_departure);
                return false;
            }
            o_param.privat = 1;
            o_param.locdep = g_id_location_departure;
            o_param.serdep = "";
            o_param.seddep = "";
        }
        if (control_search_arrival) {
            o_param.locarr = $("select#cmb_location_arrival").val();
            o_param.serarr = $("select#cmb_service_arrival").val();
            o_param.sedarr = $("select#cmb_headquarter_arrival").val();
        } else {
            if ($("#txt_zipcode_arrival").val() == "") {
                bootbox.alert(s_info_entry_an_zipcode_departure);
                return false;
            }
            if ($("#txt_address_arrival").val() == "") {
                bootbox.alert(s_info_entry_an_zipcode_arrival);
                return false;
            }
            o_param.privat = 1;
            o_param.locarr = g_id_location_arrival;
            o_param.serarr = "";
            o_param.sedarr = "";
        }
        if (b_ajax) {
            $.ajax({
                url: s_url_sale_getRoutesByParameters + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var data = r.data;
                        if (data.length > 0) {
                            $("#msg_no_found_route").addClass('hide');
                            $("#div_route_by_param").addClass('hide');
                            $("#div_all_routes").html('').removeClass('hide');
                            $.each(data, function(i, o) {
                                var xdiv = $(document.createElement('div')).addClass('found_route alert info').attr('id', 'found_route_' + o.route_id).attr('rel', o.route_id).attr('idseddep', $("#cmb_headquarter_departure").val()).attr('idsedarr', $("#cmb_headquarter_arrival").val())
                                        .append($(document.createElement('div')).addClass('fleft width95p paddr1p found_route_div').attr('rel', o.route_id)
                                        .append($(document.createElement('span')).text(o.route_departure))
                                        .append($(document.createElement('i')).addClass('icon-chevron-right').addClass('mleft5 mright5'))
                                        .append($(document.createElement('span')).text(o.route_arrival))
                                        .append($(document.createElement('br')).css('margin-bottom', '3px'))
                                        .append($(document.createElement('i')).addClass('icon-list-alt mright2 opacity06'))
                                        .append($(document.createElement('span')).text(o.route_description).addClass('color-desc-all'))
                                        .append($(document.createElement('i')).addClass('icon-hand-right mleft20 mright3 opacity06'))
                                        .append($(document.createElement('span')).text(o.route_type.toLowerCase()).addClass('color-desc-all')))
                                        .append($(document.createElement('div')).addClass('fleft width3p')
                                        .append($(document.createElement('span')).addClass('fright mleft3')
                                        .append($(document.createElement('i')).attr('title', s_txt_schedules).addClass('icon-time').attr('id', 'itime_' + o.route_id).attr('rel', o.route_id)))
                                        .append($(document.createElement('span')).addClass('fright')
                                        .append($(document.createElement('i')).attr('title', s_txt_description).addClass('icon-calendar').attr('id', 'ilist_' + o.route_id))))
                                        .append($(document.createElement('div')).addClass('fleft width1p')
                                        .append($(document.createElement('button')).attr('id', 'btn-close-found').attr('type', 'button').addClass('close').html('&times;')))
                                        .append($(document.createElement('div')).addClass('cboth'));
                                $("#div_all_routes").append(xdiv);
                                //$('#ilist_'+o.route_id).popover({
                                //animation: true,
                                //placement: 'left',
                                //content: o.route_description
                                //});
                                $('#itime_' + o.route_id).popover({
                                    animation: true,
                                    trigger: 'hover',
                                    placement: 'left',
                                    content: 'List of schedules'
                                });
                            });
                        } else {
                            $("#div_all_routes").addClass('hide');
                            fnShowDivWarning(msg1);
                            return false;
                        }
                    }
                }
            });
        }
        return true;
    }

    $("#div_all_routes").on('mouseenter', '.found_route', function() {
        g_route_id = $(this).attr('rel');
        g_dep_sed_id = $("#cmb_headquarter_departure").val();
        g_arr_sed_id = $("#cmb_headquarter_arrival").val();
        $("#found_route_" + $(this).attr('rel')).addClass('hover-route');
    }).on('mouseleave', '.found_route:not(.view-schedule)', function() {
        $("#found_route_" + $(this).attr('rel')).removeClass('hover-route');
    });

    $("#div_all_routes").on('click', '.found_route_div', function() {
        showloadingDialog = 1;
        var o_param = new Object();
        o_param.filter = 1;
        o_param.idroute = g_route_id;
        if (control_search_departure)
            g_address_location = "";
        if (control_search_arrival)
            g_address_arrival = "";
        if (b_ajax) {
            $.ajax({
                url: s_url_sale_getRoutesByParameters + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var data = r.data;
                        if (data.length > 0) {
                            $("#msg_no_found_route, #div_all_routes").addClass('hide');
                            $("#div_route_by_param, #msg_route_collapse").removeClass('hide');
                            $("#icon-upload, .icon-download").trigger('click');
                            $.each(data, function(i, o) {
                                g_route_desc = o.route_description;
                                g_route_departure = o.route_departure;
                                g_route_arrival = o.route_arrival;
                                g_dep_serv_id = o.departure_service_id;
                                g_arr_serv_id = o.arrival_service_id;
                                $("button#btn-one-way").trigger('click');
                                $("button#btn-round-trip").addClass('hide');
                                var text_dep = o.route_departure;
                                var text_arr = o.route_arrival;
                                if (!control_search_departure && control_search_arrival) {
                                    text_dep = g_address_location + ' (' + g_route_departure + ')';
                                    text_arr = g_route_arrival;
                                } else if (!control_search_departure && !control_search_arrival) {
                                    text_dep = g_address_location + ' (' + g_route_departure + ')';
                                    text_arr = g_address_arrival + ' (' + g_route_arrival + ')';
                                } else if (control_search_departure && !control_search_arrival) {
                                    text_dep = g_route_departure;
                                    text_arr = g_address_arrival + ' (' + g_route_arrival + ')';
                                }
                                $("span#span_selected_route").html('')
                                        .append($(document.createElement('span')).text(text_dep))
                                        .append($(document.createElement('i')).addClass('icon-chevron-right mleft5 mright5 opacity'))
                                        .append($(document.createElement('span')).text(text_arr))
                                        .append($(document.createElement('i')).attr('title', s_txt_description).addClass('icon-list-alt mleft10 mright2'))
                                        .append($(document.createElement('span')).text('(' + o.route_description + ')').addClass('color-desc'));
                                $("div#td_available_schedule_ow, div#td_available_schedule_rt").html('');
                                if (o.route_type === 'ROUNDTRIP')
                                    $("button#btn-round-trip").removeClass('hide');
                                fnCreateSchedule(o.route_hour, "td_available_schedule_ow", "rdo_schd_ow", 1);
                                fnCreateSchedule(o.route_hour, "td_available_schedule_rt", "rdo_schd_rt", 2);
                                $("#cmb_pax_oneway").select2("val", "");
                                $("#info-1, #info-2, #info-4").removeClass('hide');
                                $('#tooltip_help_schedule').tooltip('show');
                            });
                        } else {

                        }
                    }
                }
            });
        }
    });

    $("#div_all_routes").on('mouseenter', '.found_route_div', function() {
        g_route_id = $(this).attr('rel');
        $("#found_route_" + $(this).attr('rel')).addClass('hover-route');
    }).on('mouseleave', '.found_route:not(.view-schedule)', function() {
        $("#found_route_" + $(this).attr('rel')).removeClass('hover-route');
    }).click(function() {

    });

    $("#div_all_routes").on("click", ".icon-calendar", function() {

    });

    $("#msg_route_collapse").on("click", ".icon-upload, .icon-download", function() {
        var thes = this;
        var icon = $(thes).attr('class');
        if (icon == 'icon-upload') {
            $('#tooltip_help_schedule').tooltip('hide');
            $(thes).removeClass('icon-upload').addClass('icon-download');
            $("#div_all_routes").removeClass('hide');
            $("#div_route_by_param").addClass('hide');
        } else {
            $('#tooltip_help_schedule').tooltip('show');
            $(thes).removeClass('icon-download').addClass('icon-upload');
            $("#div_all_routes").addClass('hide');
            $("#div_route_by_param").removeClass('hide');
        }
    });

    $("#btn-close-msg").click(function() {
        $(this).parent().addClass('hide');
    });

    function fnShowDivWarning(msg) {
        $("div#msg_no_found_route").removeClass('hide');
        $("div#msg_no_found_route span").html('').append(msg);
    }

    $("#div_rate_card").on('mouseenter', '.show-card', function() {
        $(this).children('i').removeClass('hide');
    }).on('mouseleave', '.show-card', function() {
        $(this).children('i').addClass('hide');
    });

    $("#div_rate_card").on('click', '.icon-remove', function() {
        showloadingDialog = 1;
        var id = $(this).parent().attr('id').split('_')[1];
        $(this).parent().remove();
        var idtsc = $(this).attr('idtsc');
        fnRemoveCard(id, idtsc);
    });

    $("#tbody").on('click', '.icon-trash', function() {
        $(this).parent().parent().remove();
        fnOtherHide();
        fnCalcultateTotalPreCard(true);
    });

    $("#td_available_schedule_ow").on("change", "input[name='rdo_schd_ow']", function() {
        $('#tooltip_help_schedule').tooltip('hide');
        fnFillCmbSchedule(this, "cmb_pax_oneway");
    });

    $("#td_available_schedule_rt").on("change", "input[name='rdo_schd_rt']", function() {
        fnFillCmbSchedule(this, "cmb_pax_roundtrip");
    });
    $("#btn_hand_departure, #btn_hand_arrival").click(function() {
        var txt = $(this).attr('id').split('_')[2];
        $("#txt_zipcode_" + txt).val("").prop('disabled', false);
        $("#div_input_zipcode_" + txt + ", #div_btn_search_zipcode_" + txt).removeClass('hide');
        $("#div_address_by_zipcode_" + txt).val("").addClass('hide');
        $("#txt_address_" + txt).val("");
    });

    $("#cmb_pax_oneway, #cmb_pax_roundtrip").select2({
        placeholder: s_txt_passanger,
        width: 'resolve',
        allowClear: true
    }).on('change', function() {

    });

    $("#btn_add_oneway, #btn_add_turn").click(function() {
        //oneway
        var pax_ow = $("#cmb_pax_oneway option:selected").data('pax');
        var price_ow = $("#cmb_pax_oneway option:selected").data('price');
        var id_raow = $("#cmb_pax_oneway option:selected").data('idra');
        var id_ro = $("#cmb_pax_oneway option:selected").data('idro');
        var desc = $("#cmb_pax_oneway option:selected").data('desc');
        //roundtrip
        var id_rart = $("#cmb_pax_roundtrip option:selected").data('idra');
        var pax_rt = $("#cmb_pax_roundtrip option:selected").data('pax');
        var price_rt = $("#cmb_pax_roundtrip option:selected").data('price');
        //ld
        var ld_ow = $("#input_datetime_travel_ow").val();
        var ld_rt = $("#input_datetime_travel_rt").val();
        var type = 'oneway';
        var text_type = s_txt_going;
        if (!btn_type) {
            type = 'roundtrip';
            text_type = s_txt_roundtrip;
        }
        if (typeof(id_raow) !== 'undefined') {
            var txt_dep_arr = g_route_departure + ' -> ' + g_route_arrival;
            if (!control_search_departure && control_search_arrival) {
                txt_dep_arr = g_address_location + ' (' + g_route_departure + ')' + ' -> ' + g_route_arrival;
            } else if (!control_search_departure && !control_search_arrival) {
                txt_dep_arr = g_address_location + ' (' + g_route_departure + ')' + ' -> ' + g_address_arrival + ' (' + g_route_arrival + ')';
            } else if (control_search_departure && !control_search_arrival) {
                txt_dep_arr = g_route_departure + ' - ' + g_address_arrival + ' (' + g_route_arrival + ')';
            }
            var tr = $(document.createElement('tr'))
                    .attr('id_ro', id_ro).attr('id_raow', id_raow).attr('id_rart', id_rart).attr('desc', desc).attr('type', type)
                    .attr('name_ro', txt_dep_arr).attr('desc_ro', g_route_desc)
                    .attr('id_dep_serv', g_dep_serv_id).attr('id_arr_serv', g_arr_serv_id)
                    .attr('id_dep_sede', g_dep_sed_id).attr('id_arr_sede', g_arr_sed_id)
                    .attr('address_location', g_address_location).attr('address_arrival', g_address_arrival);
            if (btn_type) {
                tr.attr('id', 'tr_' + id_raow.toString() + pax_ow.toString())
                        .attr('tp', pax_ow * price_ow);
            } else {
                tr.attr('id', 'tr_' + id_raow.toString() + pax_ow.toString() + pax_rt.toString())
                        .attr('tp', (pax_ow * price_ow + pax_rt * price_rt));
            }
            tr.attr('pax_ow', pax_ow).attr('up_ow', price_ow).attr('tp_ow', price_ow * pax_ow).attr('ld_ow', ld_ow)
                    .attr('pax_rt', pax_rt).attr('pu_rt', price_rt).attr('pt_rt', price_rt * pax_rt).attr('ld_rt', ld_rt);
            tr.append($(document.createElement('td')).text(txt_dep_arr))
                    //.append($(document.createElement('td')).text(g_route_desc))
                    .append($(document.createElement('td')).text(text_type));
            if (btn_type) {
                tr.append($(document.createElement('td')).text(ld_ow))
                        .append($(document.createElement('td')).text(pax_ow))
                        .append($(document.createElement('td')).text(price_ow + ' $'))
                        .append($(document.createElement('td')).text(pax_ow * price_ow + ' $'));
            } else {
                tr.append($(document.createElement('td'))
                        .append($(document.createElement('div'))
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_going)))
                        .append($(document.createElement('div')).text(ld_ow))
                        .append($(document.createElement('div')).addClass('mtop10')
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_turn)))
                        .append($(document.createElement('div')).text(ld_rt)))
                        .append($(document.createElement('td')).append($(document.createElement('div'))
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_going)))
                        .append($(document.createElement('div')).text(pax_ow))
                        .append($(document.createElement('div')).addClass('mtop10')
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_turn)))
                        .append($(document.createElement('div')).text(pax_rt)))
                        .append($(document.createElement('td')).append($(document.createElement('div'))
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_going)))
                        .append($(document.createElement('div')).text(price_ow + ' $'))
                        .append($(document.createElement('div')).addClass('mtop10')
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_turn)))
                        .append($(document.createElement('div')).text(price_rt + ' $')))
                        .append($(document.createElement('td')).append($(document.createElement('div'))
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_going)))
                        .append($(document.createElement('div')).text(pax_ow * price_ow + ' $'))
                        .append($(document.createElement('div')).addClass('mtop10')
                        .append($(document.createElement('i')).addClass('fontsize11').text(s_txt_turn)))
                        .append($(document.createElement('div')).text(pax_rt * price_rt + ' $'))
                        //.append($(document.createElement('div')).addClass('mtop10')
                        //.append($(document.createElement('i')).addClass('fontsize11').text('total')))
                        //.append($(document.createElement('div')).text((pax_ow*price_ow+pax_rt*price_rt) + ' $'))
                        );
            }
            tr.append($(document.createElement('td'))
                    .append($(document.createElement('i')).addClass('icon-trash').attr('title', s_txt_trash_travel))
                    .append($(document.createElement('i')).addClass('icon-hand-right').attr('title', s_txt_add_travel)));
            $("#tbody").append(tr);
            fnCalcultateTotalPreCard(true);
            fnHideOptSelectPax();
        }
    });

    $("#tbody").on("click", ".icon-hand-right", function() {
        var thes = $(this).parent().parent();
        showloadingDialog = 1;
        fnCreateCard(thes);
    });

    $("#btn_clear_all").click(function() {
        $('#tbody tr').each(function(i, o) {
            if ($(o).css('display') != 'none')
                $($(o).remove());
        });
        fnShowOptSelectPax();
    });

    $("#btn_add_all").click(function() {
        showloadingDialog = 1;
        $('#tbody tr').each(function(i, o) {
            if ($(o).css('display') != 'none') {
                fnCreateCard(o);
            }
        });
    });

    $("#btn_close_card").click(function() {
        showloadingDialog = 1;
        $('#div_rate_card div.alert').each(function(i, o) {
            var id = $(this).attr('id').split('_')[1];
            var idtsc = $(this).children('i').attr('idtsc');
            fnRemoveCard(id, idtsc);
            $(this).remove();
            $("#btn_confirm_payment, #btn_close_card, #div_amount_post_card").addClass('hide');
            $("#msg_empty_card").removeClass('hide');
        });
    });

    $("#btn_continue_step_traveler_detail").click(function() {
        $("#b").trigger("click");
    });

    $("#btn_confirm_payment").click(function() {
        showloadingDialog = 1;
        $("#step-1").addClass("hide");
        $("#step-2").removeClass("hide");
        $.each(array_sales_cart, function(n, m) {
            var idtsc = m;
            var o_param = new Object();
            o_param.idTsc = idtsc;
            if (b_ajax) {
                $.ajax({
                    url: s_url_get_temp_sales_cart + jq_jsonp_callback,
                    async: false,
                    data: o_param,
                    success: function(r) {
                        if (evalResponse(r)) {
                            var data = r.data;
                            var tbody = $(document.createElement('tbody'));
                            $.each(data, function(ik, ok) {
                                tbody.append(fnCreateTrSedeSelected(ok.idlocdep, ok.idserdep, ok.idseddep, s_txt_departure, 1, idtsc, 'idSedeDepartureSelected', ok.privat, ok.addLocation));
                                tbody.append(fnCreateTrSedeSelected(ok.idlocarr, ok.idserarr, ok.idsedarr, s_txt_arrival, 2, idtsc, 'idSedeArrivalSelected', ok.privat, ok.addArrival));
                                tbody.append(fnCreateTrSelectedHour('ow', s_txt_oneway_time, idtsc, ok.stOw, ok.etOw, ok.allTimeOw, ok.timeStOw, ok.timeEtOw));
                                if (ok.type === 'roundtrip')
                                    tbody.append(fnCreateTrSelectedHour('rt', s_txt_roundtrip_time, idtsc, ok.stRt, ok.etRt, ok.allTimeRt, ok.timeStRt, ok.timeEtRt));
                                var ndiv = $(document.createElement('div'))
                                        .append(fnGetLabelContinueProcess(ok.route_name, ok.route_description))
                                        .append($(document.createElement('table')).addClass('table table-hover mtop20')
                                        .append(tbody));
                                $("#airline_details").append(ndiv);
                                if (ok.idseddepSearch === null)
                                    ok.idseddepSearch = '';
                                if (ok.idsedarrSearch === null)
                                    ok.idsedarrSearch = '';
                                fnSelectToSelect2('#idSedeDepartureSelected_' + idtsc, ok.idseddepSearch);
                                fnSelectToSelect2('#idSedeArrivalSelected_' + idtsc, ok.idsedarrSearch);
                                if (ok.idserdep == 2) {
                                    fnSelectToSelect2('#idAirlineDeparture_' + idtsc, s_txt_airline);
                                    fnSelectToSelect2("#hourairportdeparture_" + idtsc, '');
                                    fnSelectToSelect2("#minutesairportdeparture_" + idtsc, '');
                                    fnSelectToSelect2("#formatairportdeparture_" + idtsc, '');
                                }
                                if (ok.idserarr == 2) {
                                    fnSelectToSelect2('#idAirlineArrival_' + idtsc, s_txt_airline);
                                    fnSelectToSelect2("#hourairportarrival_" + idtsc, '');
                                    fnSelectToSelect2("#minutesairportarrival_" + idtsc, '');
                                    fnSelectToSelect2("#formatairportarrival_" + idtsc, '');
                                }
                                var div = $(document.createElement('div'))
                                        .append(fnGetLabelContinueProcess(ok.route_name, ok.route_description));
                                fnCreateDivTable(div, ok.pow, s_txt_going, idtsc, 1, ok.type);
                                if (ok.type === 'roundtrip') {
                                    fnCreateDivTable(div, ok.prt, s_txt_turn, idtsc, 2, ok.type);
                                }
                                fnSelectToSelect2("#hourow_" + idtsc, '');
                                fnSelectToSelect2("#minutesow_" + idtsc, '');
                                if (ok.type === 'roundtrip') {
                                    fnSelectToSelect2("#hourrt_" + idtsc, '');
                                    fnSelectToSelect2("#minutesrt_" + idtsc, '');
                                }
                                $("#personal_details").append(div);
                                $('.form_datetime1').datetimepicker({
                                    format: 'mm-dd-yyyy',
                                    viewSelect: 'month',
                                    minView: 'month',
                                    autoclose: true
                                });
                            });
                        }
                    }
                });
            }
        });
        $("#hr_observations").removeClass('hide');
        if ($("#airline_details").text() === "") {
            $("#hr_observations").addClass('hide');
        }
    });

    function fnGetLabelContinueProcess(name, description) {
        return $(document.createElement('p')).addClass('mleft10 fweight')
                .append($(document.createElement('span')).text("* " + name))
                .append($(document.createElement('span'))
                .append($(document.createElement('i')).attr('title', s_txt_description).addClass('icon-list-alt mleft10 mright2')))
                .append($(document.createElement('span')).addClass('color-desc').text(description));
    }

    function fnCreateTrSedeSelected(idloc, idser, idsed, lbl, op, idtsc, txt_id, privat, address) {
        var tt = '';
        var airTxt = 'departure';
        var o_param = new Object();
        var txt_service = s_txt_no_service;
        o_param.idloc = idloc;
        o_param.idser = idser;
        o_param.idsed = idsed;
        var txtIdFlightNumber = 'flightNumberDeparture';
        if (op === 2) {
            airTxt = 'arrival';
            txtIdFlightNumber = 'flightNumberArrival';
        }
        if (privat === 1 && address != null) {
            tt = $(document.createElement('tr'))
                    .append($(document.createElement('td')).addClass('width230')
                    .append($(document.createElement('span')).text(lbl))
                    .append($(document.createElement('span')).addClass('txt-serv').text("(" + s_txt_sede_by_zipcode + ")")))
                    .append($(document.createElement('td')).addClass('width230')
                    .append($(document.createElement('span')).text(address)))
                    .append($(document.createElement('td')))
                    .append($(document.createElement('td')))
                    .append($(document.createElement('td')));
        } else {
            $.ajax({
                url: s_url_sale_getHotelParams + jq_jsonp_callback,
                async: false,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var data = r.data;
                        $.each(data, function(e, f) {
                            if (idser > 0 || idsed > 0) {
                                txt_service = f.service_name;
                            }
                            return false;
                        });
                        tt = $(document.createElement('tr'))
                                .append($(document.createElement('td')).addClass('width230')
                                .append($(document.createElement('span')).text(lbl))
                                .append($(document.createElement('span')).addClass('txt-serv').text("(" + txt_service + ")")))
                                .append($(document.createElement('td')).addClass('width300')
                                .append(fnFillCmbSede(data, op, idtsc, txt_id)));
                        if (idser == 2) {
                            tt.append($(document.createElement('td')).addClass('width230')
                                    .append(fnFillCmbAirport(op, idtsc)))
                                    .append($(document.createElement('td'))
                                    .append($(document.createElement('input')).css('width', '80px').attr('type', 'text').attr('id', txtIdFlightNumber + '_' + idtsc).attr('name', txtIdFlightNumber + '_' + idtsc).attr('placeholder', s_txt_flight_number)));
                            tt.append($(document.createElement('td')).addClass('width230')
                                    .append($(document.createElement('span')).addClass('txt-serv mright5').text(s_txt_time + ' :'))
                                    .append(fnFillCmbSelectedHour('airport' + airTxt, idtsc, 1, 0, 11))
                                    .append($(document.createElement('span')).addClass('mleft5 mright5 fweight lheight25').text(':'))
                                    .append(fnFillCmbSelectedMinutes('airport' + airTxt, idtsc, 1, 0, 23))
                                    .append(fnFillCmbSelectedFormat('airport' + airTxt, idtsc)));
                        } else {
                            tt.append($(document.createElement('td')))
                                    .append($(document.createElement('td')))
                                    .append($(document.createElement('td')));
                        }
                    }
                }
            });
        }
        return tt;
    }

    function fnCreateTrSelectedHour(type, txt, idtsc, st, et, alltime, timeSt, timeEt) {
        var lbl = timeSt + ' - ' + timeEt;
        if (alltime == 1) {
            st = 0;
            et = 23;
            lbl = 'All time';
        }
        return $(document.createElement('tr'))
                .append($(document.createElement('td'))
                .append($(document.createElement('span')).text(txt))
                .append($(document.createElement('span')).addClass('txt-serv').text('(' + lbl + ')')))
                .append($(document.createElement('td'))
                .append(fnFillCmbSelectedHour(type, idtsc, alltime, st, et))
                .append($(document.createElement('span')).addClass('mleft5 mright5 fweight lheight25').text(':'))
                .append(fnFillCmbSelectedMinutes(type, idtsc)))
                .append($(document.createElement('td')))
                .append($(document.createElement('td')))
                .append($(document.createElement('td')));
    }

    function fnFillCmbSelectedHour(type, idtsc, at, st, et) {
        var select = $(document.createElement('select')).attr("id", 'hour' + type + '_' + idtsc).attr("name", 'hour' + type + '_' + idtsc).addClass('fontsize11p5 width50');
        var j = et * 1;
        if (at == 0)
            j = et * 1 - 1;
        for (var e = st * 1; e <= j; e++) {
            select.append('<option value="' + e + '">' + fnFormatTwoDigitsNumber(e) + '</option>');
        }
        return select;
    }

    function fnFormatTwoDigitsNumber(number) {
        a = number;
        b = a.toString();
        var t = b;
        if (b.length === 1)
            t = '0' + b;
        return t;
    }

    function fnFillCmbSelectedMinutes(type, idtsc) {
        var select = $(document.createElement('select')).attr("id", 'minutes' + type + '_' + idtsc).attr("name", 'minutes' + type + '_' + idtsc).addClass('fontsize11p5 width50');
        select.append('<option value="00">00</option>');
        select.append('<option value="05">05</option>');
        select.append('<option value="10">10</option>');
        select.append('<option value="15">15</option>');
        select.append('<option value="20">20</option>');
        select.append('<option value="25">25</option>');
        select.append('<option value="30">30</option>');
        select.append('<option value="35">35</option>');
        select.append('<option value="40">40</option>');
        select.append('<option value="45">45</option>');
        select.append('<option value="50">50</option>');
        select.append('<option value="55">55</option>');
        return select;
    }

    function fnFillCmbSelectedFormat(type, idtsc) {
        var select = $(document.createElement('select')).attr("id", 'format' + type + '_' + idtsc).attr("name", 'format' + type + '_' + idtsc).addClass('width55 fontsize11p5 mleft10').css('margin-left', '11px');
        select.append('<option value="AM">AM</option>');
        select.append('<option value="PM">PM</option>');
        return select;
    }

    function fnFillCmbSede(data, op, idtsc, txt_id) {
        var select = $(document.createElement('select')).attr("id", txt_id + '_' + idtsc).attr("name", txt_id + '_' + idtsc).addClass('fontsize11p5 width285');
        $.each(data, function(i, o) {
            select.append('<option value="' + o['sede_id'] + '" style="font-size: 11.5px;">' + o['sede_name'] + ' (' + o['sede_address'] + ')' + '</option>');
        });
        return select;
    }

    function fnFillCmbAirport(op, idtsc) {
        var txtIdAirline = 'idAirlineDeparture';
        if (op === 2)
            txtIdAirline = 'idAirlineArrival';
        var select = $(document.createElement('select')).attr("id", txtIdAirline + '_' + idtsc).attr("name", txtIdAirline + '_' + idtsc).addClass('fontsize11p5')
                .append($(document.createElement('option')));
        $.each(array_airline, function(i, o) {
            select.append('<option value="' + o['idairline'] + '">' + o['nameairline'] + '</option>');
        });
        return select;
    }

    function fnCreateDivTable(div, p, txt, idtsc, type, texttype) {
        var opt = $(document.createElement('div')).addClass('label-info fright')
                .append($(document.createElement('i')).addClass('icon-trash').attr('title', s_txt_delete_all_row));
        if (texttype === 'roundtrip' && type === 1) {
            opt.append($(document.createElement('i')).addClass('icon-retweet mleft2 retweet-all').attr('id', idtsc + '_' + type).attr('title', s_txt_copy_all_rows));
        }
        var d = $(document.createElement('div')).addClass('mleft10')
                .append($(document.createElement('div'))
                .append($(document.createElement('i')).addClass('label-info fleft').text(txt))
                .append(opt))
                .append($(document.createElement('div')).addClass('cboth'))
                .append($(document.createElement('div'))
                .append(fnCreateTablePersonalDetail(p, idtsc, type, texttype)));
        return div.append(d);
    }

    function fnCreateTablePersonalDetail(pt, idtsc, type, texttype) {
        var table = $(document.createElement('table')).addClass('table table-hover').attr('id', 'table_' + idtsc + '_' + type)
                .append($(document.createElement('thead'))
                .append($(document.createElement('tr'))
                .append($(document.createElement('td')).text(''))
                .append($(document.createElement('td')).text(s_txt_title))
                .append($(document.createElement('td')).text(s_txt_firstname))
                .append($(document.createElement('td')).text(s_txt_middlename))
                .append($(document.createElement('td')).text(s_txt_lastname))
                .append($(document.createElement('td')).text(s_txt_dob))
                .append($(document.createElement('td')).text(s_txt_gender))
                .append($(document.createElement('td')).text(s_txt_options))))
                .append($(document.createElement('tbody')));
        for (var z = 1; z <= pt; z++) {
            var td = $(document.createElement('td'))
                    .append($(document.createElement('i')).addClass('icon-trash').attr('title', s_txt_delete_row));
            if (texttype == 'roundtrip' && type == 1) {
                td.append($(document.createElement('i')).addClass('icon-retweet mleft2 retweet-row').attr('id', z + '_' + idtsc + '_' + type).attr('title', s_txt_copy_row));
            }
            table.append($(document.createElement('tr'))
                    .append($(document.createElement('td')).text(z))
                    .append($(document.createElement('td')).append($(document.createElement('select')).addClass('span1')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'title').attr('id', z + '_' + idtsc + '_' + type + '_' + 'title')
                    .append($(document.createElement('option')).text('Mr'))
                    .append($(document.createElement('option')).text('Mrs'))
                    .append($(document.createElement('option')).text('Ms'))))
                    .append($(document.createElement('td'))
                    .append($(document.createElement('input')).addClass('input-medium').attr('type', 'text')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'firstname').attr('id', z + '_' + idtsc + '_' + type + '_' + 'firstname').addClass('required'))
                    .append($(document.createElement('span')).addClass('fweight mleft2 color-req cpointer').attr('title', s_txt_required).text('*')))
                    .append($(document.createElement('td'))
                    .append($(document.createElement('input')).addClass('input-medium').attr('type', 'text')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'middlename').attr('id', z + '_' + idtsc + '_' + type + '_' + 'middlename')))
                    .append($(document.createElement('td'))
                    .append($(document.createElement('input')).addClass('input-medium').attr('type', 'text')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'lastname').attr('id', z + '_' + idtsc + '_' + type + '_' + 'lastname').addClass('required'))
                    .append($(document.createElement('span')).addClass('fweight mleft2 color-req cpointer').attr('title', s_txt_required).text('*')))
                    .append($(document.createElement('td'))
                    .append($(document.createElement('input')).addClass('form_datetime1').attr('type', 'text')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'dob').attr('id', z + '_' + idtsc + '_' + type + '_' + 'dob')))
                    .append($(document.createElement('td'))
                    .append($(document.createElement('select')).addClass('span1')
                    .attr('name', z + '_' + idtsc + '_' + type + '_' + 'gender').attr('id', z + '_' + idtsc + '_' + type + '_' + 'gender')
                    .append($(document.createElement('option')).text('Male'))
                    .append($(document.createElement('option')).text('Female'))))
                    .append(td));
        }
        return table;
    }

    $("#personal_details").on('click', '.retweet-row', function() {
        var tr = $(this).parent().parent().find('input, select');
        fnCopyDataTraveler(tr);
    });

    $("#personal_details").on('click', '.retweet-all', function() {
        var tr = $("#table_" + $(this).attr('id')).find('input, select');
        fnCopyDataTraveler(tr);
    });

    function fnCopyDataTraveler(tr) {
        $.each(tr, function(i, o) {
            var id = $(o).attr('id');
            var aid = id.split('_');
            aid.splice(2, 1, '2');
            $("#" + aid.join('_')).val($('#' + id).val());
        });
    }

    $("#btn_return_to_select_travels_from_person_detail, #btn_return_to_select_travels_from_payment").click(function() {
        showloadingDialog = 1;
        $("#personal_details").html('');
        $("#airline_details").html('');
        $("#step-1").removeClass("hide");
        $("#step-2").addClass("hide");
    });

    $("#btn_clean_all_person_detail, #btn_clean_all_payment").click(function() {
        showloadingDialog = 1;
        $("#step-2").addClass("hide");
        $("#personal_details, #airline_details").html('');
        $("#btn_close_card").trigger("click");
        $("#btn_clear_all").trigger("click");
        $("#step-1").removeClass("hide");
    });

    $("#btn-one-way").click(function() {
        $("#div_show_roundtrip, #btn_add_turn, #hr_separator").addClass('hide');
        $("#btn_add_oneway").removeClass('hide');
        $("#cmb_pax_oneway, #cmb_pax_roundtrip").select2("val", "");
        $("#input_datetime_travel_ow, #input_datetime_travel_rt").val(fnGetCurrentDate());
        btn_type = true;
    });

    $("#btn-round-trip").click(function() {
        $("#div_show_roundtrip, #btn_add_turn, #hr_separator").removeClass("hide");
        $("#btn_add_oneway").addClass('hide');
        $("#cmb_pax_oneway, #cmb_pax_roundtrip").select2("val", "");
        $("#input_datetime_travel_ow, #input_datetime_travel_rt").val(fnGetCurrentDate());
        btn_type = false;
    });

    $("#btn_sales_cart_save").click(function() {
        var inpreq = $("#dates_owner").find('table .required');
        if (opt_all_travelers)
            inpreq = $("#personal_details").find('table .required');
        var t = 0;
        $.each(inpreq, function(i, o) {
            var val = $(o).val();
            if (val == "") {
                t = 1;
                if (opt_all_travelers)
                    $("#div_collapse_one").trigger("click");
                else
                    $("#div_collapse_zero").trigger("click");
                $(o).focus();
                return false;
            }
        });
        if (t == 0)
            $('#paypalPayment').submit();
    });

    $("#btn_search_zipcode_departure, #btn_search_zipcode_arrival").click(function() {
        var txt_zp = $(this).attr('id').split('_')[3];
        var o_param = new Object();
        o_param.zipcode = $('#txt_zipcode_' + txt_zp).val();
        if (b_ajax) {
            $.ajax({
                url: s_url_get_validate_zipcode + jq_jsonp_callback,
                async: false,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var data = r.data;
                        if (data.total >= '1') {
                            if (txt_zp === 'departure') {
                                g_id_location_departure = data.idlocation;
                            } else {
                                g_id_location_arrival = data.idlocation;
                            }
                            $('#txt_zipcode_' + txt_zp).val(data.name + ', ' + data.number).prop('disabled', true);
                            $("#div_msg_zip_code_no_found_" + txt_zp + ", #div_btn_search_zipcode_" + txt_zp).addClass('hide');
                            $("#div_address_by_zipcode_" + txt_zp).removeClass('hide');
                        } else {
                            if (txt_zp === 'departure') {
                                g_id_location_departure = 0;
                            } else {
                                g_id_location_arrival = 0;
                            }
                            $("#div_msg_zip_code_no_found_" + txt_zp).removeClass('hide');
                        }
                    }
                }
            });
        }
    });

    $("#clean_form_payment").click(function() {
        fnResetFormPayal();
    });

    function fnResetFormPayal() {
        form_paypal_payment.currentForm.reset();
        $("#cmb_country").select2("val", "US");
        $("#cmb_state").select2("val", "");
        $("#cmb_type_card").select2("val", "Visa");
        $("#cmb_expiration_month").select2("val", "00");
        $("#cmb_expiration_year").select2("val", "2013");
    }

    function fnCreateSchedule(route_hour, div_sch, name_rdo, type) {
        var form = $(document.createElement('form')).addClass('form-inline');
        if (typeof(route_hour) === 'object') {
            $.each(route_hour, function(ix, ox) {
                var label = $(document.createElement('label')).addClass('radio');
                var input = $(document.createElement('input'))
                        .attr('idro', ox.idr).attr('idra', ox.idra).attr('type', 'radio').attr('name', name_rdo).attr('value', '1').attr('st', ox.st).attr('et', ox.et);
                if (ix > 0)
                    input = input.css('margin-left', '20px');
                var span = $(document.createElement('span')).text(ox.description);
                label.append(input).append(span);
                form.append(label);
            });
        } else {
            var label = $(document.createElement('label')).addClass('radio');
            var input = $(document.createElement('input'))
                    .attr('idro', route_hour).attr('st', 0).attr('et', 0).attr('type', 'radio').attr('name', name_rdo).attr('value', '1');
            var span = $(document.createElement('span')).text('All time');
            label.append(input).append(span);
            form.append(label);
        }
        $("div#" + div_sch).append(form);
    }

    function fnFillCmbSchedule(thes, cmb) {
        var o_param = new Object();
        o_param.i = $(thes).attr('idro');
        o_param.ih = $(thes).attr('st');
        o_param.eh = $(thes).attr('et');
        if (b_ajax) {
            $.ajax({
                url: s_url_sale_get_price_by_route_and_schedule + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var data = r.data;
                        $("#" + cmb).html('').append('<option></option>').select2("val", "");
                        $.each(data, function(i, o) {
                            $("#" + cmb).append('<option value="' + (i * 1 + 1) + '" data-idra="' + o.idrate + '" data-idro = "' + o.idroute + '"  data-desc="' + o.description + '"  data-price="' + o.price + '" data-pax="' + o.numpax + '">' + o.numpax + '  -  ' + o.price + '$' + '</option>');
                        });
                        $("#info-2").removeClass('hide');
                    }
                }
            });
        }
    }

    function fnResetByTypeSale() {
        $("#btn_close_card").trigger('click');
        $("#btn_clear_all").trigger('click');
        $("#info-1, #info-2, #info-3, #info-4").addClass('hide');
    }

    function fnHideOptSelectPax() {
        $("#info-3, table.table, #btn_add_all, #btn_clear_all").removeClass('hide');
        $("#msg_empty_table").addClass('hide');
    }

    function fnShowOptSelectPax() {
        $(".table,#table.table, #btn_add_all, #btn_clear_all").addClass('hide');
        $("#msg_empty_table").removeClass('hide');
        $("#cmb_pax_oneway").select2("val", "");
    }

    function fnOtherHide() {
        var t = false;
        $('#tbody tr').each(function(i, o) {
            if ($(o).css('display') != 'none') {
                t = true;
                return false;
            }
        });
        if (!t) {
            $("#msg_empty_table").removeClass('hide');
            $("table.table, #btn_add_all, #btn_clear_all").addClass('hide');
        }
    }

    function fnCalcultateTotalPreCard(i) {
        var sumPreCard = 0
        var sumPostCard = 0
        $('#tbody tr').each(function(i, o) {
            if ($(o).css('display') != 'none')
                sumPreCard += $(o).attr('tp') * 1;
            else
                sumPostCard += $(o).attr('tp') * 1;
        });
        if (i)
            $("#span_amount_total_pre_card").text(sumPreCard);
        else
            $("#span_amount_total_post_card").text(sumPostCard);
        $("input#txt_total_amount").val(sumPostCard);
        $("input#txt_total_amounth").val(sumPostCard);
        $("span#span_amount_to_pay").text(sumPostCard);
    }

    function fnCreateCard(thes) {
        var tdesc = $(thes).find('td');
        $(thes).addClass('hide');
        var o_param = new Object();
        o_param.idro = $(thes).attr('id_ro');
        o_param.idraow = $(thes).attr('id_raow');
        o_param.idrart = $(thes).attr('id_rart');
        o_param.namero = $(thes).attr('name_ro');
        o_param.descro = $(thes).attr('desc_ro');
        o_param.type = $(thes).attr('type');
        o_param.seddepsearch = $(thes).attr('id_dep_sede');
        o_param.sedarrsearch = $(thes).attr('id_arr_sede');
        o_param.paxow = $(thes).attr('pax_ow');
        o_param.upow = $(thes).attr('up_ow');
        o_param.tpow = $(thes).attr('tp_ow');
        o_param.ldow = fnFormatDate($(thes).attr('ld_ow'));
        o_param.paxrt = $(thes).attr('pax_rt');
        o_param.uprt = $(thes).attr('pu_rt');
        o_param.tprt = $(thes).attr('pt_rt');
        o_param.ldrt = fnFormatDate($(thes).attr('ld_rt'));
        o_param.tp = $(thes).attr('tp');
        o_param.addloc = $(thes).attr('address_location');
        o_param.addarr = $(thes).attr('address_arrival');
        o_param.btype = btn_type;
        if (b_ajax) {
            $.ajax({
                url: s_url_save_temp_sales_cart + jq_jsonp_callback,
                async: false,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        var idtsc = r.data;
                        array_sales_cart.push(idtsc);
                        var text_type = s_txt_going;
                        if ($(thes).attr('type') === 'roundtrip')
                            text_type = s_txt_roundtrip;
                        var div = $(document.createElement('div')).addClass('alert borde navbar-inner-2 pos-rel show-card');
                        if ($(thes).attr('type') === 'oneway') {
                            div.attr('id', 'card_' + $(thes).attr('id_raow') + $(thes).attr('pax_ow'));
                        } else {
                            div.attr('id', 'card_' + $(thes).attr('id_raow') + $(thes).attr('pax_ow') + $(thes).attr('pax_rt'));
                        }
                        div.append($(document.createElement('i')).addClass('icon-remove pos-abs right8 hide').attr('idtsc', idtsc))
                                .append($(document.createElement('span'))
                                .append($(document.createElement('i')).addClass('pos-abs txt-type').text(text_type)))
                                //.append($(document.createElement('div')).text($(thes).attr('desc_ro')))
                                .append($(document.createElement('div')).text($(thes).attr('name_ro')))
                                .append($(document.createElement('div')).addClass('mtop10')
                                .append($(document.createElement('div')).addClass('tpax').attr('paxow', $(thes).attr('pax_ow'))
                                .append($(document.createElement('i')).addClass('price').text('Pax: ' + $(thes).attr('pax_ow')))
                                .append($(document.createElement('i')).addClass('price mleft10').text('PU: $' + $(thes).attr('up_ow')))
                                .append($(document.createElement('i')).addClass('price mleft10').text('PT: $' + $(thes).attr('tp_ow')))));
                        if ($(thes).attr('type') === 'roundtrip') {
                            div.append($(document.createElement('div')).addClass('mtop1')
                                    .append($(document.createElement('div')).addClass('tpax').attr('paxrt', $(thes).attr('pax_rt'))
                                    .append($(document.createElement('i')).addClass('price').text('Pax: ' + $(thes).attr('pax_rt')))
                                    .append($(document.createElement('i')).addClass('price mleft10').text('PU: $' + $(thes).attr('pu_rt')))
                                    .append($(document.createElement('i')).addClass('price mleft10').text('PT: $' + $(thes).attr('pt_rt')))));
                        }
                        div.append($(document.createElement('div')).addClass('mtop10').text(s_txt_total_to_pay + ": $" + $(thes).attr('tp')));
                        $("#div_rate_card").append(div);
                        $("#btn_confirm_payment,#btn_close_card,#div_amount_post_card").removeClass('hide');
                        $("#msg_empty_card").addClass('hide');
                        fnOtherHide();
                        fnCalcultateTotalPreCard(true);
                        fnCalcultateTotalPreCard(false);
                    }
                }
            });
        }
    }

    function fnRemoveCard(id, idtsc) {
        var o_param = new Object();
        o_param.idtsc = idtsc;
        if (b_ajax) {
            $.ajax({
                url: s_url_delete_temp_sales_cart + jq_jsonp_callback,
                async: false,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        $("#tr_" + id).removeClass('hide');
                        fnHideOptSelectPax();
                        if ($('div#div_rate_card').find('div').length === 0) {
                            $("#btn_confirm_payment, #btn_close_card, #div_amount_post_card").addClass('hide');
                            $("#msg_empty_card").removeClass('hide');
                        }
                        var pos = array_sales_cart.indexOf(idtsc * 1);
                        pos > -1 && array_sales_cart.splice(pos, 1);
                        fnCalcultateTotalPreCard(true);
                        fnCalcultateTotalPreCard(false);
                    }
                }
            });
        }
    }

    function fnSelectToSelect2(thes, label, val) {
        var qq = $(thes).select2({
            placeholder: label,
            width: 'resolve',
            allowClear: true
        });
        if (typeof(val) !== "undefined") {
            qq.select2("val", val);
        }
    }

    function fnServiceDepartureAndArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idloc = $("#cmb_location_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idloc == "") {
                if (o['idserv'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idserv'] == opt && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }

    function fnLocationDepartureArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idserv = $("#cmb_service_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idserv == "") {
                if (o['idloc'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idloc'] == opt && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }

    function fnSedeDepartureArrival(thes, cmb) {
        var id = "#cmb_headquarter_" + cmb + " option:selected";
        $("#cmb_location_" + cmb).select2("val", $(id).data('idloc'));
        $("#cmb_service_" + cmb).select2("val", $(id).data('idserv'));
    }

    function fnDirectPrint(sc, se, soe, email) {
        var printParams = new Object();
        printParams.sc = sc;
        printParams.se = se;
        printParams.soe = soe;
        printParams.email = email;
        url = '/management/sale/printTickets.html?' + $.param(printParams);
        if (websale)
            var url = '/public/websale/printTickets.html?' + $.param(printParams);
        window.open(url);
        return false;
    }

    function fnGetCurrentDate() {
        var y = myDate.getFullYear();
        var m = (myDate.getMonth() + 1).toString();
        var d = (myDate.getDate()).toString();
        if (m.length === 1)
            m = '0' + m;
        if (d.length === 1)
            d = '0' + d;
        //return y + '-' + m + '-' + d;
        return m + '-' + d + '-' + y;
    }

    function fnFormatDate(date) {
        var uu = date.split('-');
        return uu[2] + '-' + uu[0] + '-' + uu[1];
    }

});
