$(document).ready(function(){
    fnListLocationsGrid();
    fnSelect2($("#froute_departure"),s_place_holder_departure);
    fnSelect2($("#froute_service_departure"),s_place_holder_service_departure);
    fnSelect2($("#froute_sede_departure"),s_place_holder_sede_departure);
    fnSelect2($("#froute_arrival"),s_place_holder_arrival);
    fnSelect2($("#froute_service_arrival"),s_place_holder_service_arrival);
    fnSelect2($("#froute_sede_arrival"),s_place_holder_sede_arrival);      
       
    var form_route=$("#form_route").validate({
        rules: {
            froute_departure: {
                required: true
            }
            ,
            froute_arrival:{
                required: true
            }            
            ,
            froute_type:{
                required: true
            }            
            ,
            froute_description:{
                required: true
            }            
        },
        messages: {
            froute_departure: {
                required: s_validate_required_froute_departure
            }
            ,
            froute_arrival: {
                required: s_validate_required_froute_arrival
            }
            ,
            froute_type:{
                required: s_validate_required_froute_type
            }            
            ,
            froute_description:{
                required: s_validate_required_froute_description
            } 
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , 
        submitHandler: function(o_form) {            
            if (b_ajax) {
                var s_url = s_url_route_save;                
                if($('#froute_id').val() != ''){
                    s_url = s_url_route_update;
                }
                var data=$(o_form).serialize();
                var fsede_group=$('.fsede_group');
                if(fsede_group.length>0){
                    sede_group_id=[];
                    $.each(fsede_group,function(i,d){
                        sede_group_id[i]=$(d).data('id');
                    });
                    data+='&fsede_group_id='+sede_group_id;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , 
                    data: data
                    , 
                    beforeSend: function() {
                        $('#modal_route_btn_save').button('loading');
                    }
                    , 
                    complete: function() {
                        $('#modal_route_btn_save').button('reset');
                    }
                    , 
                    success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_route').modal('hide');
                            bootbox.alert(s_route_saved);
                            jqgrid_routes.trigger('reloadGrid');
                        }
                    }
                });
            }
        }
    });
    $('#modal_route_btn_save').click(function() {
        $('#form_route').submit(); 
    });
    $('#modal_route').on('hidden', function() { 
        $('#froute_service_departure').select2("val", "");
        $('#froute_sede_departure').select2("val", "");
        $('#froute_service_arrival').select2("val", "");
        $('#froute_sede_arrival').select2("val", "");
        $('#froute_departure').select2("val", "");
        $('#froute_arrival').select2("val", "");                             
        $("#group_select_sede").html('');
        
        form_route.currentForm.reset();
        form_route.resetForm();
        $('input[type=hidden]', form_route.currentForm).val('');
        $('input, select, textarea', form_route.currentForm).parents('.control-group').removeClass('error');
    });
    $("table#jqgrid_routes").on('click','.route_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_route_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , 
                success: function(j_response) {     
                    if(evalResponse(j_response)){
                        var o_route = j_response.data;
                        console.log(o_route);
                        $("#froute_id").val(o_route.route_id);                        
                        $("#froute_description").val(o_route.route_description);                                                
                        $("#froute_departure").select2("val",o_route.route_departure);
                        $("#"+o_route.route_type).prop('checked', true);
                        $("#froute_arrival").select2("val",o_route.route_arrival);                        
                        $("#froute_sede_departure").select2('val',o_route.route_sede_departure);
                        $("#froute_service_departure").select2('val',o_route.route_service_departure);
                        //$("#froute_sede_arrival").select2('val',o_route.route_sede_arrival);
                        o_route_sede=o_route.route_g_sede_arrival;
                        if(o_route_sede.length>0){
                            var div_group='';
                            $.each(o_route_sede,function(i,d){
                                div_group+='<div class="fsede_group alert pull-left" data-id="'+d.idSede+'"><span class="pull-left">'+d.nameSede+'</span><button data-dismiss="alert" class="close pull-left" type="button">×</button></div>';
                            });
                            $("#group_select_sede").append(div_group);
                        }                        
                        $("#froute_service_arrival").select2('val',o_route.route_service_arrival);
                        $("#froute_private").val([o_route.route_private])                        
                        $('#modal_route').modal('show');
                    }                   
                }
            });
        }
    });
    $("table#jqgrid_routes").on('click','.route_delete',function(){
        var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
            if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_route_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , 
                        success: function(j_response) {    
                            if(evalResponse(j_response)){
                                bootbox.alert(s_route_delete);
                                jqgrid_routes.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });        
    });
    $("#froute_sede_arrival").on('change',function(){
        fnSedeDepartureArrival(this, "arrival");        
        if($(this).val()!=''){            
            var class_group=$('.fsede_group');
            var valor_select=$('#froute_sede_arrival option:selected').val();
            var text_select=$('#froute_sede_arrival option:selected').text();
            var have=false;
            $.each(class_group,function(i,d){
                if($(d).data('id')==valor_select)
                    have=true;
            });
            if(have===false){
                var div_group='<div class="fsede_group alert pull-left" data-id="'+valor_select+'"><span class="pull-left">'+text_select+'</span><button data-dismiss="alert" class="close pull-left" type="button">×</button></div>';
                $("#group_select_sede").append(div_group);
            }
        }else{
            $("#froute_sede_arrival").select2('val','');
        }
    });
    
    /*START CODE HAMILTON*/
    $("#froute_departure").on('change', function() {
        fnLocationDepartureArrival(this, 'departure');
    });
    
    $("#froute_service_departure").on('change', function() {
        fnServiceDepartureAndArrival(this, 'departure');
    });
    
    $("#froute_sede_departure").on('change', function() {
        fnSedeDepartureArrival(this, "departure");
    });
    
    $("#froute_arrival").on('change', function() {
        $("#group_select_sede").html('');
        fnLocationDepartureArrival(this, "arrival");
    });
    
    $("#froute_service_arrival").on('change', function() {
        $("#group_select_sede").html('');
        fnServiceDepartureAndArrival(this, "arrival");
    });

    function fnLocationDepartureArrival(thes, cmb) {
        var opt = $(thes).val();        
        $("#froute_sede_" + cmb).html('').append('<option></option>').select2("val", "");
        var idserv = $("#froute_service_" + cmb).val();        
        $.each(array_sede, function(i, o) {
            if (idserv == "") {
                if (o['idloc'] == opt) {
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }else if(opt == ""){
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idloc'] == opt && o['idserv'] == idserv) {
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }else if (opt == "" && o['idserv'] == idserv) {
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }
    
    function fnServiceDepartureAndArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#froute_sede_" + cmb).html('').append('<option></option>').select2("val", "");
        var idloc = $("#froute_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idloc == "") {
                if (o['idserv'] == opt) {
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }else if(opt == ""){
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idserv'] == opt && o['idloc'] == idloc) {
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if(opt == "" && o['idloc'] == idloc){
                    $('#froute_sede_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }

    function fnSedeDepartureArrival(thes, cmb) {
        //var opt = $(thes).val();
        var id = "#froute_sede_" + cmb + " option:selected";
        $("#froute_" + cmb).select2("val", $(id).data('idloc'));        
        $("#froute_service_" + cmb).select2("val", $(id).data('idserv'));
    }

/*END CODE HAMILTON*/
    
    
});
function fnSelect2(etiq,placeholder){    
    etiq.select2({
        placeholder: placeholder
        ,
        width: 'resolve'
        ,
        allowClear: true
    });
}
function fnListLocationsGrid(){
    jqgrid_routes=$("#jqgrid_routes").jqGrid({
        url: s_url_route_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_routes_col_names,
        colModel: a_jqgrid_routes_col_model,
        pager: "#jqgrid_routes_pager",          
        height: 'auto',    
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idRoute',
        afterInsertRow: function(row_id, row_data){
            var s_html_route_edit = '<a data-id=' + row_id + ' class="btn btn-mini route_edit"><i class="icon-pencil"></i></a>';
            var s_html_route_delete = '<a data-id=' + row_id + ' class="btn btn-mini route_delete"><i class="icon-trash"></i></a>';
            jqgrid_routes.setRowData(row_id, {                
                actions: s_html_route_edit+' '+s_html_route_delete
            });
        }
    });
    jQuery("#jqgrid_routes").jqGrid('filterToolbar',{
        stringResult: true,
        searchOnEnter :false
    });
//    $("#jqgrid_routes").jqGrid('navGrid','#jqgrid_routes_pager',{add:false,edit:false,del:false,search:false,refresh:false});
//    $("#jqgrid_routes").jqGrid('navButtonAdd','#jqgrid_routes_pager',{ caption:"Seleccione Columnas", title: "Seleccione las columnas que desea ver", onClickButton : function (){ jQuery("#jqgrid_routes").jqGrid('columnChooser'); } });
}