$(document).ready(function(){
    fnListLocationsGrid();
     var form_location=$("#form_location").validate({
        rules: {
            flocation_name: {
                required: true
            }
        },
        messages: {
            flocation_name: {
                required: s_validate_required_flocation_name
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {            
            if (b_ajax) {
                var s_url = s_url_location_save;
                if($('#flocation_id').val() != ''){
                    s_url = s_url_location_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_location_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_location_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_location').modal('hide');
                            bootbox.alert(s_location_saved);
                            jqgrid_locations.trigger('reloadGrid');
                        }                        
                    }
                });
            }
        }
    });
    $('#modal_location_btn_save').click(function() {
          $('#form_location').submit(); 
    });
    $('#modal_location').on('hidden', function() {       
        form_location.currentForm.reset();
        form_location.resetForm();
        $('input[type=hidden]', form_location.currentForm).val('');
        $('input, select, textarea', form_location.currentForm).parents('.control-group').removeClass('error');
    });
    $("table#jqgrid_locations").on('click','.location_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_location_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_location = j_response.data;                        
                        $("#flocation_id").val(o_location.location_id);
                        $("#flocation_name").val(o_location.location_name);                        
                        $('#modal_location').modal('show');
                    }     
                }
            });
        }
    });
    $("table#jqgrid_locations").on('click','.location_delete',function(){
        var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_location_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_location_delete);
                                jqgrid_locations.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });        
    });
    $("table#jqgrid_locations").on('click','.location_add',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_location_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_location = j_response.data;                        
                        $("#flocation_id_2").val(o_location.location_id);
                        $("#modal_zipcode_sede .modal-header").append('<h3>'+o_location.location_name+'</h3>');
                        $('#myTab a:first').tab('show');
                        
                        $("#form_sede").find('input').attr('disabled',false);
                        $("#modal_zipcode_btn_save").attr('disabled','enabled');
                        $("#form_sede").find('select').select2('enable');
                        $("#form_zipcode").find('input').attr('disabled',false);
                        
                        $('#modal_zipcode_sede').modal('show');
                    }     
                }
            });
        }                
    });
    var form_sede=$("#form_sede").validate({
        rules: {
            fsede_service: {
                required: true
            }
            ,fsede_name: {
                required: true
            }
            ,fsede_address: {
                required: true
            }
            ,fsede_zipcode: {
                required: true
            }
        },
        messages: {
            fsede_service: {
                required: s_validate_required_sede_service
            }
            ,fsede_name: {
                required: s_validate_required_sede_name
            }
            ,fsede_address: {
                required: s_validate_required_sede_address
            }
            ,fsede_zipcode: {
                required: s_validate_required_sede_zipcode
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_sede_save;
                if($('#fsede_id').val() != ''){
                    s_url = s_url_sede_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_sede_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_sede_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_zipcode_sede').modal('hide');
                            bootbox.alert(s_sede_saved);
                            
                            $("#form_sede").find('input').attr('disabled',false);                                                       
                        
                            jqgrid_sede.trigger('reloadGrid');
                        }                        
                    }
                });
            }
        }
    });
    var form_zipcode=$("#form_zipcode").validate({
        rules: {
            fzipcode_number: {
                required: true
            }
        },
        messages: {
            fzipcode_number: {
                required: s_validate_required_fzipcode_number
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {            
            if (b_ajax) {
                var s_url = s_url_zipcode_save;
                if($('#fzipcode_id').val() != ''){
                    s_url = s_url_zipcode_update;
                }
                var data=$(o_form).serialize()+'&flocation_id='+$("#flocation_id_2").val();
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: data
                    , beforeSend: function() {
                        $('#modal_zipcode_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_zipcode_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            bootbox.alert(s_zipcode_saved);
                            if($('#fzipcode_id').val()==''){
                                form_zipcode.currentForm.reset();
                                form_zipcode.resetForm();
                                $('input[type=hidden]', form_zipcode.currentForm).val('');
                                $('input, select, textarea', form_zipcode.currentForm).parents('.control-group').removeClass('error');                                
                                fnListZipCodeByLocation($("#flocation_id_2").val());
                            }else{
                                $("#modal_zipcode_sede").modal('hide');
                            }
                            jqgrid_zipcode.trigger('reloadGrid');
                        }                        
                    }
                });
            }
        }
    });
    $("#modal_zipcode_btn_save").on('click',function(){
        $('#form_zipcode').submit();
    });
    $("#modal_sede_btn_save").on('click',function(){
        $('#form_sede').submit();
    });
    $('#modal_zipcode_sede').on('hidden', function() { 
        $("#modal_zipcode_sede .modal-header button").next().remove();
        form_zipcode.currentForm.reset();
        form_zipcode.resetForm();
        $('input[type=hidden]', form_zipcode.currentForm).val('');
        $('input, select, textarea', form_zipcode.currentForm).parents('.control-group').removeClass('error');
        
        form_sede.currentForm.reset();
        form_sede.resetForm();
        $('input[type=hidden]', form_sede.currentForm).val('');
        $('input, select, textarea', form_sede.currentForm).parents('.control-group').removeClass('error');
        $("#flocation_id_2").val('');
        $("#fsede_service").select2('val','');
        $("#fsede_zipcode").select2('val','');
        
        $("#form_sede").find('input').attr('disabled',false);
        $("#modal_zipcode_btn_save").attr('disabled','enabled');
        $("#form_sede").find('select').select2('enable');
        $("#form_zipcode").find('input').attr('disabled',false);
    });
    
   select2($('#fsede_service'),s_place_holder_service);
   select2($('#fsede_zipcode'),s_place_holder_zipcode);
   fnListZipCodeGrid();
   fnListSedeGrid();  
   $("#myTab a:last").on('click',function(e){
       e.preventDefault();
       if($('#fsede_id').val()=='')
           fnListZipCodeByLocation($("#flocation_id_2").val());       
   });  
   $("table#jqgrid_zipcode").on('click','.zipcode_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_zipcode_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_zipcode = j_response.data;                        
                        $("#fzipcode_id").val(o_zipcode.zipcode_id);
                        $("#fzipcode_number").val(o_zipcode.zipcode_number);
                        $("#form_sede").find('input,button').attr('disabled','diabled');
                        $("#modal_zipcode_btn_save").attr('disabled','diabled');
                        $("#form_sede").find('select').select2('disable');
                        var row_zipcode=$("#jqgrid_zipcode").jqGrid('getRowData',id);                        
                        $("#flocation_id_2").val(row_zipcode.location_id);
                        $("#modal_zipcode_sede .modal-header").append('<h3>'+row_zipcode.location_name+'</h3>');
                        
                        $('#myTab a:first').tab('show');
                        $('#modal_zipcode_sede').modal('show');
                    }     
                }
            });
        }                
    });
   $("table#jqgrid_zipcode").on('click','.zipcode_delete',function(){
       var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_zipcode_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_zipcode_delete);
                                jqgrid_zipcode.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });              
    });
   $("table#jqgrid_sede").on('click','.sede_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_sede_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var row_sede=$("#jqgrid_sede").jqGrid('getRowData',id);
                        $("#flocation_id_2").val(row_sede.location_id);
                        $("#modal_zipcode_sede .modal-header").append('<h3>'+row_sede.location_name+'</h3>');
                        $("#myTab a :last").trigger('click');
                        
                        var o_sede = j_response.data; 
                        $("#fsede_id").val(o_sede.sede_id);                        
                        $("#fsede_service").select2('val',o_sede.sede_service_id);
                        $("#fsede_name").val(o_sede.sede_name);
                        $("#fsede_address").val(o_sede.sede_address);                        
                        $("#fsede_phone").val(o_sede.sede_phone);
                        $("#fsede_fax").val(o_sede.sede_fax);
                                       
                        $("#form_zipcode").find('input,button').attr('disabled','diabled');
                        $("#modal_sede_btn_save").attr('disabled','diabled');                                                                                                                                                
                        
                        $("#fsede_zipcode").select2('val',o_sede.sede_zipcode_id);
                        
                        $('#myTab a:last').tab('show');
                        $('#modal_zipcode_sede').modal('show');
                    }     
                }
            });
        }                
    });
   $("table#jqgrid_sede").on('click','.sede_delete',function(){
       var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_sede_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_sede_delete);
                                jqgrid_sede.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });              
    });
});
function fnListZipCodeByLocation(idLocation){
    $('#fsede_zipcode').select2("val","").select2('close');
    $('#fsede_zipcode').empty();
    $('#fsede_zipcode').append('<option></option>'); 
    if(idLocation!=''){
        $.ajax({
            url: s_url_zipcode_listZipCodeByLocation + jq_jsonp_callback
            ,async:false
            , data:{
                idLocation:idLocation
            }
            , beforeSend: function() {            
            }
            , complete: function() {
            }
            , success: function(j_response) {
                if(evalResponse(j_response)){
                    $.each(j_response.data, function(i,o_value){                        
                        var i_zipcode_id = o_value.zipcode_id == null?'':o_value.zipcode_id;                                
                        $('#fsede_zipcode').append('<option value="'+i_zipcode_id+'">'+o_value.zipcode_number+'</option>');
                    });
                }                  
            }
        });
    }    
}
function fnListZipCodeGrid(){
    jqgrid_zipcode=$("#jqgrid_zipcode").jqGrid({
        url: s_url_zipcode_list+'?q=0',
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_zipcode_col_names,
        colModel: a_jqgrid_zipcode_col_model,
        pager: "#jqgrid_zipcode_pager",          
        height: 'auto',    
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idZipCode',
        afterInsertRow: function(row_id, row_data){
            var s_html_zipcode_edit = '<a data-id=' + row_id + ' class="btn btn-mini zipcode_edit"><i class="icon-pencil"></i></a>';
            var s_html_zipcode_delete = '<a data-id=' + row_id + ' class="btn btn-mini zipcode_delete"><i class="icon-trash"></i></a>';            
            jqgrid_zipcode.setRowData(row_id, {                
                actions: s_html_zipcode_edit+' '+s_html_zipcode_delete
            });
        }
        ,onSelectRow: function(ids) { 
            $("table#jqgrid_zipcode tr").removeClass('success');
            $("table#jqgrid_zipcode").addClass('table table-condensed');            
            $("#jqgrid_zipcode tr#"+ids).addClass('success');
            
            var row_codezip=jQuery("#jqgrid_zipcode").jqGrid('getRowData',ids);            
            jQuery("#jqgrid_sede").jqGrid('setGridParam',{url:s_url_sede_list+"?q=2&idZipCode="+ids});            
            jQuery("#jqgrid_sede").jqGrid('setCaption',row_codezip.location).trigger('reloadGrid');
        }
    });
    jQuery("#jqgrid_zipcode").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
}
function fnListSedeGrid(){
    jqgrid_sede=$("#jqgrid_sede").jqGrid({
        url: s_url_sede_list+'?q=0',
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_sede_col_names,
        colModel: a_jqgrid_sede_col_model,
        pager: "#jqgrid_sede_pager",          
        height: 'auto',    
        viewrecords: true,
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idSede',
        afterInsertRow: function(row_id, row_data){
            var s_html_sede_edit = '<a data-id=' + row_id + ' class="btn btn-mini sede_edit"><i class="icon-pencil"></i></a>';
            var s_html_sede_delete = '<a data-id=' + row_id + ' class="btn btn-mini sede_delete"><i class="icon-trash"></i></a>';            
            jqgrid_sede.setRowData(row_id, {                
                actions: s_html_sede_edit+' '+s_html_sede_delete
            });
        }
        ,onSelectRow: function(ids) { 
            $("table#jqgrid_sede tr").removeClass('success');
            $("table#jqgrid_sede").addClass('table table-condensed');            
            $("#jqgrid_sede tr#"+ids).addClass('success');
        }
    });
    jQuery("#jqgrid_sede").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
}
function select2(etiq,placeholder){    
    etiq.select2({
       placeholder: placeholder
       ,width: 'resolve'
       ,allowClear: true
   });
}
function fnListLocationsGrid(){
    jqgrid_locations=$("#jqgrid_locations").jqGrid({
        url: s_url_locations_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_locations_col_names,
        colModel: a_jqgrid_locations_col_model,
        pager: "#jqgrid_locations_pager",          
        height: 'auto',    
        viewrecords: true,
        rownumbers: true,
        rowList:[10,20,30],
        multiselect: false,
        rowNum: 50,
        width:'auto',
        sortname: 'idLocation',
        afterInsertRow: function(row_id, row_data){
            var s_html_location_edit = '<a data-id=' + row_id + ' class="btn btn-mini location_edit"><i class="icon-pencil"></i></a>';
            var s_html_location_delete = '<a data-id=' + row_id + ' class="btn btn-mini location_delete"><i class="icon-trash"></i></a>';
            var s_html_location_add= '<a data-id=' + row_id + ' class="btn btn-mini location_add"><i class="icon-plus"></i></a>';
            jqgrid_locations.setRowData(row_id, {                
                actions: s_html_location_edit+' '+s_html_location_delete+' '+s_html_location_add
            });
        }
        ,onSelectRow: function(ids) { 
            $("table#jqgrid_locations tr").removeClass('success');
            $("table#jqgrid_locations").addClass('table table-condensed');
            $("table#jqgrid_locations tr#"+ids).addClass('success');
            var row_location=jQuery("#jqgrid_locations").jqGrid('getRowData',ids);            
            jQuery("#jqgrid_zipcode").jqGrid('setGridParam',{url:s_url_zipcode_list+"?q=1&idLocation="+ids});
            jQuery("#jqgrid_zipcode").jqGrid('setCaption',row_location.name).trigger('reloadGrid');
            
            jQuery("#jqgrid_sede").jqGrid('setGridParam',{url:s_url_sede_list+"?q=1&idLocation="+ids});
            jQuery("#jqgrid_sede").jqGrid('setCaption',row_location.name).trigger('reloadGrid');
        }
    });
    jQuery("#jqgrid_locations").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});    
}