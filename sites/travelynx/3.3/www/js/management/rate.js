$(document).ready(function() {
    //SELECT2    
    fnSelectToSelect2("#cmb_location_departure", s_txt_location);
    fnSelectToSelect2("#cmb_service_departure", s_txt_service);
    fnSelectToSelect2("#cmb_headquarter_departure", s_txt_headquarter);

    fnSelectToSelect2("#cmb_location_arrival", s_txt_location);
    fnSelectToSelect2("#cmb_service_arrival", s_txt_service);
    fnSelectToSelect2("#cmb_headquarter_arrival", s_txt_headquarter);

    $("#cmb_location_departure").on('change', function() {
        fnLocationDepartureArrival(this, 'departure');
    });

    $("#cmb_location_arrival").on('change', function() {
        fnLocationDepartureArrival(this, 'arrival');
    });

    $("#cmb_service_departure").on('change', function() {
        fnServiceDepartureAndArrival(this, "departure");
    });

    $("#cmb_service_arrival").on('change', function() {
        fnServiceDepartureAndArrival(this, "arrival");
    });

    $("#cmb_headquarter_departure").on('change', function() {
        fnSedeDepartureArrival(this, "departure");
    });

    $("#cmb_headquarter_arrival").on('change', function() {
        fnSedeDepartureArrival(this, "arrival");
    });

    var frm_rate = $("#frm_rate").validate({
        rules: {
            cmb_service: {
                required: true
            },
            cmb_departure: {
                required: true
            },
            cmb_arrival: {
                required: true
            }
        },
        messages: {
            cmb_service: {
                required: s_validate_required_froute_service
            },
            cmb_departure: {
                required: s_validate_required_froute_departure
            },
            cmb_arrival: {
                required: s_validate_required_froute_arrival
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        },
        submitHandler: function(o_form) {

        }
    });

    $("#btn_search_location").click(function() {
        fnGetAllItinerary(0, 0);
    });

    function fnGetAllItinerary(idro, filter) {
        var o_param = new Object();
        o_param.filter = filter;
        if (idro > 0) {
            o_param.idroute = idro
        } else {
            o_param.locdep = $("select#cmb_location_departure").val();
            o_param.serdep = $("select#cmb_service_departure").val();
            o_param.seddep = $("select#cmb_headquarter_departure").val();
            o_param.locarr = $("select#cmb_location_arrival").val();
            o_param.serarr = $("select#cmb_service_arrival").val();
            o_param.sedarr = $("select#cmb_headquarter_arrival").val();
        }
        $("#frm_rate").children().children().children('.help-inline').css('display', 'none');
        if (b_ajax) {
            $.ajax({
                url: s_url_rate_getRoutesByParameters + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        $("#div_service").html('');
                        if (r.data.length > 0) {
                            $("#msg_no_found_route").addClass('hide');
                            $.each(r.data, function(t, o) {
                                var i = o.route_id;
                                var active = 'color-active';
                                var cmb = 'undisabled';
                                var dis = 'disabled';
                                var chk = 'checked';
                                if (o.alltime == 1) {
                                    cmb = 'disabled';
                                    active = 'color-deactive';
                                }
                                if (o.numrate == 0)
                                    dis = 'undisabled';
                                if (o.alltime == 0 || o.alltime == null)
                                    chk = 'unchecked';
                                var div = $(document.createElement('div')).addClass('row-fluid show-grid alert cblack navbar-inner-2 div-service').attr('id', "idservice_" + i)
                                        .append($(document.createElement('div')).addClass('span12')
                                        .append($(document.createElement('div')).css("width", "102%")
                                        .append($(document.createElement('div')).addClass('left mtop10').css("width", "63%")
                                        .append($(document.createElement('i')).attr('rel', i).addClass('i-txt').text(o.route_departure + ' - ' + o.route_arrival + ' (' + o.route_description + ')')))
                                        .append($(document.createElement('div')).addClass('right mtop5')
                                        .append($(document.createElement('span')).attr('id', 'span_totalschd' + i).text('( ' + o.numrate + ' ' + s_txt_schedule + ' )').addClass('mright10'))
                                        .append($(document.createElement('input')).attr(dis, '').attr(chk, '').attr('class', 'chk_all_times').attr('id', 'chk_all_times_' + i).attr('rel', i).attr('type', 'checkbox'))
                                        .append($(document.createElement('span')).text('All times'))
                                        .append($(document.createElement('select')).addClass('width70 selecth mleft10').attr(cmb, '').attr("id", "select_init_hour_" + i)
                                        .append(fnFillSelectHourArray()))
                                        .append($(document.createElement('select')).addClass('width70 selecth mleft5').attr(cmb, '').attr("id", "select_end_hour_" + i)
                                        .append(fnFillSelectHourArray()))
                                        .append($(document.createElement('div')).addClass('circle').attr('id', 'div_task_' + i)
                                        .append($(document.createElement('i')).addClass('i-text')
                                        .append($(document.createElement('i')).addClass('icon-tasks').attr("rel", i).attr("id", "id_icon_task_" + i).attr("title", s_jqgrid_title_show_schedule))))
                                        .append($(document.createElement('div')).addClass('circle ' + active).attr('id', 'div_search_' + i)
                                        .append($(document.createElement('i')).addClass('i-text')
                                        .append($(document.createElement('i')).addClass('icon-search').prop(cmb, true).attr("rel", i).attr("id", "id_icon_search_" + i).attr("title", s_jqgrid_title_show_filter))))
                                        .append($(document.createElement('div')).addClass('circle mleft5 ' + active).attr('id', 'div_plus_' + i)
                                        .append($(document.createElement('i')).addClass('i-text')
                                        .append($(document.createElement('i')).addClass('icon-plus').prop(cmb, true).attr("rel", i).attr("id", "id_icon_plus_" + i).attr("title", s_jqgrid_title_add_schedule)))))
                                        .append($(document.createElement('div')).addClass('both')))
                                        .append($(document.createElement('div')).attr("id", "div_schedule_" + i).addClass('div_schedule hide')
                                        .append($(document.createElement('table')).addClass('table mtop10')
                                        .append($(document.createElement('thead')).attr("id", "thead_schedule_" + i)
                                        .append(fnCreateTrFromPax()))
                                        .append($(document.createElement('tbody')).attr("id", "tbody_schedule_" + i)))
                                        .append($(document.createElement('hr')).addClass('fhr'))
                                        ));
                                $("#div_service").append(div);
                            });
                        } else {
                            $("#msg_no_found_route").removeClass('hide');
                        }
                    }
                }
            });
        }
    }

    $("#div_service").on('mouseenter', '.div-service', function() {
        $(this).find('.i-txt').addClass('text-dec-under-color');
    }).on('mouseleave', '.div-service', function() {
        $(this).find('.i-txt').removeClass('text-dec-under-color');
    });

    $("#div_service").on('click', '.i-txt', function() {
        fnGetAllItinerary($(this).attr('rel'), 1);
    });

    $("#div_service").on("click", ".icon-plus", function() {
        var i = $(this).attr('rel');
        if (!fnValidateSchd(i))
            return false;
        if (b_ajax) {
            $.ajax({
                url: s_url_get_price + jq_jsonp_callback,
                data: fnGetParamsForAjax(i, 0),
                success: function(r) {
                    if (evalResponse(r)) {
                        var icontm = $("#id_icon_task_" + i).attr('class');
                        if (icontm == 'icon-tasks') {
                            $("#tbody_schedule_" + i).html('');
                            fnCreateTrSchedule(i, true, r.data, 1);
                            if (r.data.length == 0) {
                                fnDisabledInputPax(i, false);
                                $("#chk_all_times_" + i).prop('disabled', true);
                                $("#id_icon_task_" + i).removeClass('icon-tasks').addClass('icon-minus');
                            }
                        } else if (icontm == 'icon-minus') {
                            if (r.data.length == 0) {
                                var tr_schd = $("#tbody_schedule_" + i).find('tr');
                                var id_schd = $("#select_init_hour_" + i).val() + $("#select_end_hour_" + i).val() + i;
                                var bool = true;
                                $.each(tr_schd, function(a, b) {
                                    if (id_schd == $(b).attr('id').split('_')[1]) {
                                        if ($(b).attr('class') == 'hide')
                                            $(this).removeClass('hide')
                                        bool = false;
                                        return false;
                                    }
                                });
                                if (bool) {
                                    fnCreateTrSchedule(i, true, r.data, 1);
                                    fnDisabledInputPax(i, false);
                                    $("#chk_all_times_" + i).prop('disabled', true);
                                    $("#id_icon_task_" + i).removeClass('icon-tasks').addClass('icon-minus');
                                }
                            }
                        }
                    }
                }
            });
        }
    });

    $("#div_service").on("click", ".icon-search", function() {
        var thes = this;
        var i = $(thes).attr('rel');
        if (!fnValidateSchd(i))
            return false;
        if (b_ajax) {
            $.ajax({
                url: s_url_get_price + jq_jsonp_callback,
                data: fnGetParamsForAjax(i, 0),
                success: function(r) {
                    if (evalResponse(r)) {
                        if (r.data.length > 0) {
                            $("#tbody_schedule_" + i).html('');
                            fnCreateTrSchedule(i, true, r.data, 1);
                            $("#id_icon_task_" + i).removeClass('icon-tasks').addClass('icon-minus')
                        } else {
                            bootbox.alert(s_msg_warning_not_schedule_for_route);
                            return false;
                        }
                    }
                }
            });
        }
    });

    $("#div_service").on("click", ".icon-tasks, .icon-minus", function() {
        var thes = this;
        var i = $(thes).attr('rel');
        var icontm = $("#id_icon_task_" + i).attr('class');
        if (icontm == 'icon-tasks') {
            if (b_ajax) {
                $.ajax({
                    url: s_url_get_price + jq_jsonp_callback,
                    data: fnGetParamsForAjax(i, 1),
                    success: function(r) {
                        if (evalResponse(r)) {
                            $(thes).removeClass('icon-tasks');
                            $(thes).addClass('icon-minus');
                            var a_price_rt = r.tr;
                            var a_price_route = r.data;
                            $("#tbody_schedule_" + i).html('');
                            $.each(a_price_rt, function(ix, ox) {
                                fnCreateTrSchedule(i, ox, a_price_route, 2);
                            });
                        }
                    }
                });
            }
        } else if (icontm == 'icon-minus') {
            $(thes).addClass('icon-tasks');
            $(thes).removeClass('icon-minus');
            $("#div_schedule_" + i).addClass('hide');
            $("#tbody_schedule_" + i).html('');
            fnActivateOptAllTime(i);
        }
    });

    $("#div_service").on('click', '.icon-thumbs-up', function() {
        var thes = this;
        var id = $(thes).parent().parent().attr('id');
        var i = $(thes).attr('rel');
        $(".input_" + id.split("_")[1]).attr('disabled', true);
        var input = $('tr#' + id).find('input');
        var pr = new Array();
        $.each(input, function(i, o) {
            var oo = new Object();
            oo.i = i + 1;
            oo.v = $("#" + $(o).attr('id')).val();
            pr.push(oo);
        });
        var o_param = new Object();
        o_param.i = i;
        o_param.ir = $(thes).attr('ir');
        o_param.ih = $(thes).attr('ih');
        o_param.eh = $(thes).attr('eh');
        o_param.va = pr;
        if (b_ajax) {
            $.ajax({
                url: s_url_save_rate + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (evalResponse(r)) {
                        $($(thes).parent().children()[0]).attr('ir', r.data);
                        $($(thes).parent().children()[3]).attr('ir', r.data);
                        if (o_param.ir == 0) {
                            $("#span_totalschd" + i).text('( ' + ($("#span_totalschd" + i).text().split(' ')[1] * 1 + 1) + ' ' + s_txt_schedule + ' )');
                        }
                        bootbox.alert(s_msg_ok_save);
                    }
                }
            });
        }
    });

    $("#div_service").on('click', '.chk_all_times', function() {
        var i = $(this).attr('rel');
        if ($(this).is(':checked')) {
            $(this).prop('disabled', true);
            $($(this).parent().children('div')[1]).addClass('color-deactive');
            $($(this).parent().children('div')[1]).children('i').children('i').prop('disabled', true);
            $($(this).parent().children('div')[2]).addClass('color-deactive');
            $($(this).parent().children('div')[2]).children('i').children('i').prop('disabled', true);
            $("#select_init_hour_" + i + ", #select_end_hour_" + i).prop('disabled', true);
            $("#id_icon_task_" + i).removeClass("icon-tasks").addClass('icon-minus');
            fnCreateTrSchedule(i, 0, [], 3);
        }
    });

    $("#div_service").on('click', '.icon-pencil', function() {
        var id = $(this).parent().parent().attr('id');
        var i = $(this).attr('rel');
        $(".input_" + id.split("_")[1]).attr('disabled', false);
    });

    $("#div_service").on('click', '.icon-ban-circle', function() {
        var i = $(this).attr('rel');
        var id = $(this).parent().parent().attr('id');
        $("#" + id).addClass('hide');
        var len = $("#tbody_schedule_" + i).find('tr').not('.hide').length;
        if (len == 0) {
            $("#div_schedule_" + i).addClass('hide');
            $("#id_icon_task_" + i).removeClass('icon-minus').addClass("icon-tasks");
            fnActivateOptAllTime(i);
        }
    });

    $("#div_service").on('click', '.icon-trash', function() {
        var thes = this;
        bootbox.confirm(s_msg_really_remove_this_rate, function(result) {
            if (result) {
                var o_param = new Object();
                o_param.idrate = $(thes).attr('ir');
                if (b_ajax) {
                    $.ajax({
                        url: s_url_delete_rate + jq_jsonp_callback,
                        data: o_param,
                        success: function(r) {
                            if (evalResponse(r)) {
                                var id = $(thes).parent().parent().attr('id');
                                var i = $(thes).attr('rel');
                                $('#' + id).remove();
                                var len = $("#tbody_schedule_" + i).find('tr').length;
                                $("#span_totalschd" + i).text('( ' + ($("#span_totalschd" + i).text().split(' ')[1] * 1 - 1) + ' ' + s_txt_schedule + ' )');
                                if (len == 0) {
                                    $("#div_schedule_" + i).addClass('hide');
                                    $("#id_icon_task_" + i).removeClass('icon-minus').addClass("icon-tasks");
                                    fnActivateOptAllTime(i);
                                }
                                bootbox.alert(s_msg_ok_rate_delete);
                            }
                        }
                    });
                }
            }
        });
    });

    $("#table_hour").on('click', '.chk_hour', function() {
        var select = $('div#div_service').find('select');
        var sts = 0;
        var id = $(this).attr("id");
        if ($("#" + id).is(':checked'))
            sts = 1;
        var o_param = new Object();
        o_param.id = id.split("_")[2];
        o_param.sts = sts;
        if (b_ajax) {
            $.ajax({
                url: s_url_update_status_hour + jq_jsonp_callback,
                data: o_param,
                success: function(r) {
                    if (r) {
                        $.each(select, function(i, o) {
                            fnFillSelectHourObject("#" + $(o).attr('id'), r.data);
                        });
                    }
                }
            });
        }
    });

    function fnCreateTrSchedule(i, idh, data, x) {
        var valih = $("#select_init_hour_" + i).val();
        var valeh = $("#select_end_hour_" + i).val();
        var st = valih;
        var et = valeh;
        var id = 0;
        var txth = '';
        if (x == 1) {
            id = valih + valeh
            txth = $("#select_init_hour_" + i + " option:selected").html() + ' - ' + $("#select_end_hour_" + i + " option:selected").html()
        } else if (x == 2) {
            id = idh.idr;
            txth = idh.schd;
        } else if (x == 3) {
            id = '00';
            txth = 'All Times';
            st = 0;
            et = 0;
        }
        
        var row_tb = $(document.createElement('tr')).attr("id", 'tr_' + id + i);//.addClass('warning');
        row_tb.append($(document.createElement('td')).text(txth).addClass('width100 fblack'));        

        var ridRate = 0;        
        if (data.length > 0) {
            $.each(data, function(id, od) {
                ridRate = od.idrate;
                return false;
            });
        }

        for (var j = 1; j <= s_max_pax; j++) {
            var price = '';
            if (data.length > 0) {
                if (x === 1) {
                    $.each(data, function(p, q) {
                        if (q.rate == valih + valeh + i + j) {
                            price = q.price;
                        }
                    });
                } else if (x === 2) {
                    $.each(data, function(iz, oz) {
                        if (id + j == oz.idr) {
                            price = oz.price;
                            st = oz.st;
                            et = oz.et;
                            return false;
                        }
                    });
                }
            }
            row_tb.append($(document.createElement('td'))
                    .append($(document.createElement('input')).attr('id', 'input_' + id + j + i).attr('type', 'text').addClass('input_' + id + i).addClass('width35 textaligncenter').val(price)));
        }
        row_tb.append(fnCreateIconAction(i, st, et, ridRate));
        $("#tbody_schedule_" + i).append(row_tb);
        $("#div_schedule_" + i).removeClass('hide');
        if (x == 1) {
            fnDisabledInputPax(i, true);
        } else if (x == 2) {
            $(".input_" + id + i).attr('disabled', true);
        }

    }

    function fnCreateIconAction(i, valih, valeh, idrate) {
        return $(document.createElement('td'))
                .append($(document.createElement('i')).addClass('icon-thumbs-up').attr('title', s_jqgrid_title_save).attr('rel', i).attr('ih', valih).attr('eh', valeh).attr('ir', idrate))
                .append($(document.createElement('i')).addClass('icon-pencil mleft4').attr('title', s_jqgrid_title_update).attr('rel', i))
                .append($(document.createElement('i')).addClass('icon-ban-circle mleft4').attr('title', s_jqgrid_title_hide).attr('rel', i))
                .append($(document.createElement('i')).addClass('icon-trash mleft2').attr('title', s_jqgrid_title_delete).attr('rel', i).attr('ir', idrate))


    }

    function fnGetParamsForAjax(i, x) {
        var o_param = new Object();
        o_param.i = i;
        o_param.ih = $("#select_init_hour_" + i).val();
        o_param.eh = $("#select_end_hour_" + i).val();
        o_param.op = x;
        return o_param;
    }

    function fnCreateTrFromPax() {
        var row_th = $(document.createElement('tr'));
        row_th.append($(document.createElement('th'))
                .append($(document.createElement('span')).text("Pax"))
                .append($(document.createElement('i')).addClass("icon-hand-right mleft4")));
        for (var i = 1; i <= s_max_pax; i++) {
            //row_th.append($(document.createElement('th')).text(i+" Pax"));
            row_th.append($(document.createElement('th')).text(i));
        }
        row_th.append($(document.createElement('td')).text(s_jqgrid_actions).addClass('fblack'));
        return row_th;
    }

    function fnValidateSchd(i) {
        var sih = $("#select_init_hour_" + i).val() * 1;
        var seh = $("#select_end_hour_" + i).val() * 1;
        if (sih > seh || sih == seh) {
            bootbox.alert(s_msg_warning_schedule);
            return false;
        } else {
            return true;
        }
    }

    function fnFillSelectHourArray() {
        var opt = '';
        for (var j = 0; j < t.length; j++) {
            opt += "<option value='" + t[j]['id'] + "'>" + t[j]['desc'] + "</option>";
        }
        return opt;
    }

    function fnFillSelectHourObject(thes, data) {
        $(thes).html('');
        $.each(data, function(i, o) {
            var opt = "<option value='" + o.idHour + "'>" + o.description + "</option>";
            $(thes).append(opt);
        });
    }

    function fnSelectToSelect2(thes, label) {
        $(thes).select2({
            placeholder: label,
            width: 'resolve',
            allowClear: true
        });
    }

    function fnDisabledInputPax(i, act) {
        $(".input_" + $("#select_init_hour_" + i).val() + $("#select_end_hour_" + i).val() + i).attr('disabled', act);
    }

    function fnActivateOptAllTime(i) {
        var numrat = $("#span_totalschd" + i).text().split(' ')[1] * 1;
        if (numrat == 0) {
            $("#div_plus_" + i).removeClass('color-deactive');
            $("#id_icon_plus_" + i).prop('disabled', false);
            $("#div_search_" + i).removeClass('color-deactive');
            $("#id_icon_search_" + i).prop('disabled', false);
            $("#select_init_hour_" + i + ", #select_end_hour_" + i).prop('disabled', false);
            $("#chk_all_times_" + i).prop('disabled', false).prop('checked', false);
        }

    }

    function fnServiceDepartureAndArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idloc = $("#cmb_location_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idloc == "") {
                if (o['idserv'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idserv'] == opt && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }

    function fnLocationDepartureArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idserv = $("#cmb_service_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idserv == "") {
                if (o['idloc'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idloc'] == opt && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }

    function fnSedeDepartureArrival(thes, cmb) {
        //var opt = $(thes).val();
        var id = "#cmb_headquarter_" + cmb + " option:selected";
        $("#cmb_location_" + cmb).select2("val", $(id).data('idloc'));
        $("#cmb_service_" + cmb).select2("val", $(id).data('idserv'));
    }

});