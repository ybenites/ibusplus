$(document).ready(function(){
    fnListServicesGrid();    
    $("table#jqgrid_services").on('click','.service_edit',function(){
        if (b_ajax) {
            var id = $(this).data('id');
            $.ajax({
                url: s_url_service_get + jq_jsonp_callback,
                data: {
                    id:id
                }
                , success: function(j_response) { 
                    if(evalResponse(j_response)){
                        var o_service = j_response.data;                        
                        $("#fservice_id").val(o_service.service_id);
                        $("#fservice_name").val(o_service.service_name);
                        $("#fservice_description").val(o_service.service_description);
                        $('#modal_service').modal('show');
                    }                    
                }
            });
        }
    });
    $("table#jqgrid_services").on('click','.service_delete',function(){
        var id = $(this).data('id');
        bootbox.confirm(s_dialog2_button_label_confirmation_delete_text, function(result) {
                if (result) {
                if (b_ajax) {                    
                    $.ajax({
                        url: s_url_service_delete + jq_jsonp_callback,
                        data: {
                            id:id
                        }
                        , success: function(j_response) {  
                            if(evalResponse(j_response)){
                                bootbox.alert(s_service_delete);
                                jqgrid_services.trigger('reloadGrid');
                            }                            
                        }
                    });
                }
            }
        });        
    });
    $('#modal_service').on('hidden', function() {       
        form_service.currentForm.reset();
        form_service.resetForm();
        $('input[type=hidden]', form_service.currentForm).val('');
        $('input, select, textarea', form_service.currentForm).parents('.control-group').removeClass('error');
    });
    var form_service=$("#form_service").validate({
        rules: {
            fservice_name: {
                required: true
            }
            , fservice_description: {
                required: true
            }
        },
        messages: {
            fservice_name: {
                required: s_validate_required_fservice_name
            }
            , fservice_description: {
                required: s_validate_required_fservice_description
            }
        },
        errorClass: 'help-inline',
        highlight: function(element, errorClass) {
            $(element).parents('.control-group').addClass('error');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.control-group').removeClass('error');
        }
        , submitHandler: function(o_form) {
            if (b_ajax) {
                var s_url = s_url_service_save;
                if($('#fservice_id').val() != ''){
                    s_url = s_url_service_update;
                }
                $.ajax({
                    url: s_url + jq_jsonp_callback
                    , data: $(o_form).serialize()
                    , beforeSend: function() {
                        $('#modal_service_btn_save').button('loading');
                    }
                    , complete: function() {
                        $('#modal_service_btn_save').button('reset');
                    }
                    , success: function(j_response) {
                        if(evalResponse(j_response)){
                            $('#modal_service').modal('hide');
                            bootbox.alert(s_service_saved);
                            jqgrid_services.trigger('reloadGrid');
                        }                       
                    }
                });
            }
        }
    });
    $('#modal_service_btn_save').click(function() {
          $('#form_service').submit(); 
    });
});
function fnListServicesGrid(){
    jqgrid_services=$("#jqgrid_services").jqGrid({
        url: s_url_services_list,
        datatype: 'json',
        mtype: 'POST',
        colNames: a_jqgrid_services_col_names,
        colModel: a_jqgrid_services_col_model,
        pager: "#jqgrid_services_pager",          
        height: 'auto',              
        viewrecords: true,   
        rownumbers: true,
        rowNum: 50,
        width:'auto',
        sortname: 'idService',
        afterInsertRow: function(row_id, row_data){
            var s_html_service_edit = '<a data-id=' + row_id + ' class="btn btn-mini service_edit"><i class="icon-pencil"></i></a>';
            var s_html_service_delete = '<a data-id=' + row_id + ' class="btn btn-mini service_delete"><i class="icon-trash"></i></a>';
            jqgrid_services.setRowData(row_id, {                
                actions: s_html_service_edit+' '+s_html_service_delete
            });
        }
    });
    jQuery("#jqgrid_services").jqGrid('filterToolbar',{stringResult: true,searchOnEnter :false});
}