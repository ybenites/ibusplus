<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Public_Websale extends Controller_Private_Kernel {

    public $template = 'bts/public/index';

    public function before() {
        parent::before();
        if (!$this->request->is_ajax()) {
            $this->template->SYSTEM_A_SCRIPTS = $this->a_bts_scripts;
            $this->template->SYSTEM_A_STYLES = $this->a_bts_styles;
            $this->template->SYSTEM_A_PROJ_SCRIPTS = $this->a_proj_scripts;
            $this->template->SYSTEM_A_PROJ_STYLES = $this->a_proj_styles;
            $o_cache_instance = Cache::instance('apc');
            $s_request_url = 'CORE_PUBLIC';
            $s_cache_key_css = SITE_DOMAIN . $s_request_url . 'css';
            $s_cache_key_js = SITE_DOMAIN . $s_request_url . 'js';
            $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
            $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            if ($a_in_cache_object_css === null) {
                $o_cache_instance->set($s_cache_key_css, self::fnSYSGetFilePage('CORE_PUBLIC', self::BTS_FILE_TYPE_CSS));
                $o_cache_instance->set($s_cache_key_js, self::fnSYSGetFilePage('CORE_PUBLIC', self::BTS_FILE_TYPE_JS));
                $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
                $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            }
            $this->fnSYSAddScripts($a_in_cache_object_js);
            $this->fnSYSAddStyles($a_in_cache_object_css);
        }
    }

    public function action_index() {
        $o_model_service = new Model_Service();
        $o_model_location = new Model_Location();
        $o_model_sede = new Model_Sede();
        $o_model_airline = new Model_Airlines();
        $skin = 'websale';
        $welcome_view_file = DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . 'index_websale';

        $array = $this->request->post();
        $empty = "";
        $array_search = array(
            'id_loc_dep' => "",
            'id_ser_dep' => $empty,
            'id_hq_dep' => $empty,
            'id_dt_start' => $empty,
            'id_loc_arr' => $empty,
            'id_ser_arr' => $empty,
            'id_hq_arr' => $empty,
            'id_dt_end' => $empty
        );

        if (isset($array['cmb_location_departure'])) {
            $array_search['id_loc_dep'] = $array['cmb_location_departure'];
        }
        if (isset($array['cmb_service_departure'])) {
            $array_search['id_ser_dep'] = $array['cmb_service_departure'];
        }
        if (isset($array['cmb_headquarter_departure'])) {
            $array_search['id_hq_dep'] = $array['cmb_headquarter_departure'];
        }
        if (isset($array['input_datetime_travel_start'])) {
            $array_search['id_dt_start'] = $array['input_datetime_travel_start'];
        }
        if (isset($array['cmb_location_arrival'])) {
            $array_search['id_loc_arr'] = $array['cmb_location_arrival'];
        }
        if (isset($array['cmb_service_arrival'])) {
            $array_search['id_ser_arr'] = $array['cmb_service_arrival'];
        }
        if (isset($array['cmb_headquarter_arrival'])) {
            $array_search['id_hq_arr'] = $array['cmb_headquarter_arrival'];
        }
        if (isset($array['input_datetime_travel_end'])) {
            $array_search['id_dt_end'] = $array['input_datetime_travel_end'];
        }

        $index_websale_view = new View($welcome_view_file);
        $index_websale_view->SYSTEM_AJAX_DATA_TYPE = 'json';
        $index_websale_view->SYSTEM_JSONP_CALL_BACK = 'bts_cb';
        $index_websale_view->LOCATION = $o_model_location->getAllLocation();
        $index_websale_view->SERVICE = $o_model_service->listArray();
        $index_websale_view->SEDE = $o_model_sede->getAllSede();
        $index_websale_view->AIRLINE = $o_model_airline->getArray();
        $index_websale_view->SEARCH = $array_search;

        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('PUBLIC_WEBSALE', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('PUBLIC_WEBSALE', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/datepicker/bootstrap-datetimepicker.' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);
        $index_websale_view->SYSTEM_CONFIG = $this->template->SYSTEM_CONFIG;
        $this->template->SYSTEM_BODY = $index_websale_view;
    }

    public function action_getLocations() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        try {
            $o_model_location = new Model_Location();
            $a_response['data'] = $o_model_location->getAllLocation();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response, self::BTS_RESPONSE_TYPE_JSONP);
    }

    public function action_getServices() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        try {
            $o_model_service = new Model_Service();
            $a_response['data'] = $o_model_service->listArray();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response, self::BTS_RESPONSE_TYPE_JSONP);
    }

    public function action_getHeadquarter() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        try {
            $o_model_sede = new Model_Sede();
            $a_response['data'] = $o_model_sede->getAllSede();
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response, self::BTS_RESPONSE_TYPE_JSONP);
    }

    public function action_getLocationsDepartureByService() {
        $this->getLocationsDepartureByService();
    }

    public function action_getLocationsArrivalByDeparture() {
        $this->getLocationsArrivalByDeparture();
    }

    public function action_getPriceByRouteAndSchedule() {
        $this->getPriceByRouteAndSchedule();
    }

    public function action_saveSales() {
        $websale = 1;
        $this->saveSales($websale);
    }

    public function action_getTempSalesCart() {
        $this->getTempSalesCart();
    }

    public function action_getHotelParams() {
        $this->getHotelParams();
    }

    public function action_saveTempSalesCart() {
        $this->saveTempSalesCart();
    }

    public function action_deleteTempSalesCart() {
        $this->deleteTempSalesCart();
    }

    public function action_getRoutesByParameters() {
        $this->getRoutesByParameters();
    }

    public function action_getValidateZipcode() {
        $this->getValidateZipcode();
    }

    public function action_printTickets() {
        $this->printTickets();
    }

    public function action_deleteMyShoppingCart() {
        $this->deleteMyShoppingCart();
    }

}