<?php

class Controller_Public_Service extends Public_Common {

    const SO_DATE_FORMAT = 'DATE_FORMAT';
    
   public $DATE_FORMAT = array(
        'US' => array(
            'VALUE' => 'US',
            'JQUERY_DATE' => 'mm-dd-yy',
            'SQL_DATE' => '%m-%d-%Y',
            'SQL_TIME' => '%H:%i:%s',
            'PHP_DATE' => 'm-d-Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%m-%d-%Y %H:%i',
            'JQUERY_TIME_ENTRY_24_HORAS' => 1,
            'PHP_TIME_INPUT' => 'H:i',
            'SQL_TIME_OUTPUT' => '%H:%i'
        ),
        'MX' => array(
            'VALUE' => 'MX',
            'JQUERY_DATE' => 'dd/mm/yy',
            'SQL_DATE' => '%d/%m/%Y',
            'SQL_TIME' => '%h:%i %p',
            'PHP_DATE' => 'd/m/Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%d/%m/%Y %h:%i %p',
            'JQUERY_TIME_ENTRY_24_HORAS' => 1,
            'PHP_TIME_INPUT' => 'h:i A',
            'SQL_TIME_OUTPUT' => '%h:%i %p'
        ),
        'PE' => array(
            'VALUE' => 'PE',
            'JQUERY_DATE' => 'dd/mm/yy',
            'SQL_DATE' => '%d/%m/%Y',
            'SQL_TIME' => '%h:%i %p',
            'PHP_DATE' => 'd/m/Y',
            'SQL_TIME_RAW' => '%H:%i:%s',
            'SQL_TIME_LIMIT' => '23:59:59',
            'SQL_DATE_TIME' => '%d/%m/%Y %h:%i %p',
            'JQUERY_TIME_ENTRY_24_HORAS' => 1,
            'PHP_TIME_INPUT' => 'h:i A',
            'SQL_TIME_OUTPUT' => '%h:%i %p'
        ),
    );
    
  public function before() {
        parent::before();
        I18n::lang('es');   
    }

    public function action_index() {
        $skin = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
        $welcome_view_file = DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $skin . DIRECTORY_SEPARATOR . 'index_html';
        $index_view = new View($welcome_view_file);
        $index_view->skin = $skin;
        $this->template->SYSTEM_BODY = $index_view;        
    }
    public function action_webSaleForm() {
           $this->auto_render = FALSE;
           $a_response = $this->a_bts_json_response;
        try{
        
        $s_idiom_lang = 'ES';
        $o_view_web_sale_form = new View('public/service/web_sale_form');
        
        $o_view_web_sale_form->s_lang = $s_idiom_lang;
        $o_view_web_sale_form->skin =$this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);

        //$s_date_format = $this->request->query('date_format');
       // $o_date = $this->getOptionValueFromDatabase(self::SO_DATE_FORMAT);
        //$s_date_format = '';
        
        //$a_date_format = array_key_exists($s_date_format, $this->DATE_FORMAT)?$this->DATE_FORMAT[$s_date_format]:$this->DATE_FORMAT['US'];

        $o_view_web_sale_form->a_date_format = 'mm-dd-yy';
        $a_response['data'] = $o_view_web_sale_form->render();
                
        $this->fnSYSResponseFormat($a_response,self::BTS_RESPONSE_TYPE_JSONP);
        }catch(Exception $e){
            echo $e->getTraceAsString();
        }
    }
    
    public function getOptionValueFromDatabase($key) {
        try {
            $db = DB::select('options.value', 'options.suffix')->from('options')->where('options.key', '=', $key)->as_object()->execute()->current();
            return $db;
        } catch (Exception $exc) {
            throw new Exception($exc->getMessage(), $exc->getCode(), $exc);
        }
    }
}