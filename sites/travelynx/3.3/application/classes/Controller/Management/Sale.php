<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Sale extends Controller_Private_Admin implements TravelynxConstants {

    public function action_index() {

//        require_once "/var/www/websites/projects/sites/travelynx/3.3/www/excel_reader2.php";
//        $data = new Spreadsheet_Excel_Reader("prueba4.xls");
//        $qq = $data->sheets;
//        $aa = $qq[0]['cells'];
//        foreach ($aa as $key => $value) {
//            print_r($key);
//            echo '<pre>';
//            print_r($value);
//            echo '</pre>';
////            $m_sale = new Model_Sale();
////            $m_sale->idShoppingCart = $value[1];
////            $m_sale->save();
//        }
//        die();

        $model_tempsalescart = new Model_Tempsalescart();
        $model_tempsalescart->cleanTempSalesCartBySession($this->fnSYSGetSessionParameter(self::BTS_SESSION_ID));
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('MANAGEMENT_SALES', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('MANAGEMENT_SALES', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/datepicker/bootstrap-datetimepicker.' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);
        $o_model_service = new Model_Service();
        $o_model_location = new Model_Location();
        $o_model_sede = new Model_Sede();
        $o_model_airline = new Model_Airlines();

        $v_sales = View::factory('management/sales');
        $v_sales->LOCATION = $o_model_location->getAllLocation();
        $v_sales->SERVICE = $o_model_service->listArray();
        $v_sales->SEDE = $o_model_sede->getAllSede();
        $v_sales->AIRLINE = $o_model_airline->getArray();
        $v_sales->OPTION_RECORD_ALL_TRAVELERS = self::fnSYSGetOptionValue(self::OPTION_RECORD_ALL_TRAVELERS);
        $this->template->SYSTEM_BODY = $v_sales;
    }

    public function action_getLocationsDepartureByService() {
        $this->getLocationsDepartureByService();
    }

    public function action_getLocationsArrivalByDeparture() {
        $this->getLocationsArrivalByDeparture();
    }

    public function action_getPriceByRouteAndSchedule() {
        $this->getPriceByRouteAndSchedule();
    }

    public function action_saveSales() {
        $websale = 0;
        $this->saveSales($websale);
    }

    public function action_getTempSalesCart() {
        $this->getTempSalesCart();
    }

    public function action_getHotelParams() {
        $this->getHotelParams();
    }

    public function action_saveTempSalesCart() {
        $this->saveTempSalesCart();
    }

    public function action_deleteTempSalesCart() {
        $this->deleteTempSalesCart();
    }

    public function action_getRoutesByParameters() {
        $this->getRoutesByParameters();
    }

    public function action_getValidateZipcode() {
        $this->getValidateZipcode();
    }

    public function action_printTickets() {
        $this->printTickets();
    }

    public function action_deleteMyShoppingCart() {
        $this->deleteMyShoppingCart();
    }

    public function action_getUserPaypal() {
        $this->getUserPaypal();
    }

    public function action_testSendEmail() {
        $id_sc = 16;
        $o_sc = new Model_Shoppingcart($id_sc);
        $model_shoppingcart = new Model_Shoppingcart();
        $model_traveler = new Model_Traveler();

        global $a_db_config;
        $email = 'hugocasanovam@gmail.com';
        $subject = __('Confirmación de Compra en Travelynx.com');
        $nameRecipient = __('Cliente');
        $emailView = 'mail/mail_sendconfirmationTickets';
        $Msjcompany = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_COMPANY];
        $MsjAddress = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_ADDRESS];
        $MsjWebsite = $a_db_config[self::BTS_SYS_CONFIG_WEBSITE];
        $MsjPhone = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_PHONE];
        $emailArrayMsgData = array(
            ':code' => $o_sc->code,
            ':totalAmount' => $o_sc->totalAmount,
            ':contactCompany' => $Msjcompany,
            ':contactAddress' => $MsjAddress,
            ':contactPhone' => $MsjPhone,
            ':website' => $MsjWebsite
        );
        $emailArrayMsgDataObj = array(
            "contact" => $model_shoppingcart->getDatesToEmailSend($id_sc),
            "ticketdetail" => $model_traveler->getDataForTicketDetail($id_sc),
        );
        $emailFrom = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM];
        $nameFrom = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM_NAME];
        $this->fnSYSSendEmail($subject, $email, $nameRecipient, $emailView, $emailArrayMsgDataObj, $emailArrayMsgData, $emailFrom, $nameFrom);
    }

}
