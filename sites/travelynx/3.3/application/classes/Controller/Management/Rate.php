<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Rate extends Private_Admin implements TravelynxConstants {

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('MANAGEMENT_RATE', self::BTS_FILE_TYPE_CSS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('MANAGEMENT_RATE', self::BTS_FILE_TYPE_JS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);

        $o_model_service = new Model_Service();
        $o_model_location = new Model_Location();
        $o_model_sede = new Model_Sede();

        $view_rate = View::factory('management/rate');
        $view_rate->HOUR = DB::select()->from('tra_hour')->execute()->as_array();
        $view_rate->LOCATION = $o_model_location->getAllLocation();
        $view_rate->SERVICE = $o_model_service->listArray();
        $view_rate->SEDE = $o_model_sede->getAllSede();
        $view_rate->OPTION_MAXIMUM_PAX = self::fnSYSGetOptionValue(self::OPTION_MAXIMUM_NUMBER_PAX);
        $this->template->SYSTEM_BODY = $view_rate;
    }

    public function action_saveRate() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('i', 'not_empty')
                    ->rule('ih', 'not_empty')
                    ->rule('eh', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            Database::instance()->begin();
            if ($a_form_post_data['ir'] > 0) {
                $o_model_rate = new Model_Rate($a_form_post_data['ir']);
                if (!$o_model_rate->loaded())
                    throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
                $idrate = $a_form_post_data['ir'];
            } else {
                $o_model_rate = new Model_Rate();
                $idrate = $o_model_rate->saveArray($a_form_post_data);
            }
            foreach ($a_form_post_data['va'] as $value) {
                $o_model_price = new Model_Price();
                if ($a_form_post_data['ir'] > 0) {
                    $xx = $o_model_price->checkPrice($idrate, $value);
                    if ($xx['count'])
                        $o_model_price->updatePrice($idrate, $value);
                    else
                        $o_model_price->saveArray($idrate, $value);
                } else {
                    $o_model_price->saveArray($idrate, $value);
                }
            }
            Database::instance()->commit();
            $a_response['data'] = $idrate;
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getListPricesByRoute() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('i', 'not_empty')
                    ->rule('ih', 'not_empty')
                    ->rule('eh', 'not_empty')
                    ->rule('op', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $this->fnSYSCleaningAllCache();
            $o_model = new Model_Price();
            if ($a_form_post_data['op'] == 0) {
                $a_response['data'] = $o_model->getPricesBySchedule($a_form_post_data);
            } else {
                $a_response['tr'] = $o_model->getScheduleByRoute($a_form_post_data);
                $a_response['data'] = $o_model->getPricesByRoute($a_form_post_data);
            }
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_updateStatusHour() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty')
                    ->rule('sts', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        case 'sts':
                            $s_label_name = __("Estado");
                            break;
                        default:
                            break;
                    }

                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_model_hour = new Model_Hour($a_form_post_data['id']);
            if (!$o_model_hour->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
            $o_model_hour->updateStatus($a_form_post_data['sts']);
            $a_response['data'] = DB::select()->from('tra_hour')->where('status', '=', self::BTS_STATUS_ACTIVE)->execute()->as_array();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getLocationsArrival() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('frate_service', 'not_empty')
                    ->rule('frate_departure', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'frate_service':
                            $s_label_name = __("Servicio");
                            break;
                        case 'frate_departure':
                            $s_label_name = __("Salida");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_Service = new Model_Service($a_form_post_data['frate_service']);
            if (!$o_model_Service->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_location = new Model_Location($a_form_post_data['frate_departure']);
            if (!$o_model_location->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_route = new Model_Route();
            $a_response['data'] = $o_model_route->listArray($o_model_Service->idService, $o_model_location->idLocation, 0, TRUE);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getRoutesByParameters() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('locdep', 'not_empty')
                    ->rule('locarr', 'not_empty');
            if (!$o_check_post->check() && count($o_check_post->errors()) >= 3) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'locdep':
                            $s_label_name = __("Origen");
                            break;
                        case 'locarr':
                            $s_label_name = __("Destino");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            $this->fnSYSCleaningAllCache();

            $a_form_post_data = $o_check_post->data();

            $o_model_route = new Model_Route();

            if ($a_form_post_data['filter']) {
                $a_route = $o_model_route->getRouteArrayById($a_form_post_data['idroute']);
            } else {
                $locdep = $serdep = $seddep = $locarr = $serarr = $sedarr = 0;
                if ($a_form_post_data['locdep'] != '')
                    $locdep = $a_form_post_data['locdep'];
                if ($a_form_post_data['serdep'] != '')
                    $serdep = $a_form_post_data['serdep'];
                if ($a_form_post_data['seddep'] != '')
                    $seddep = $a_form_post_data['seddep'];
                if ($a_form_post_data['locarr'] != '')
                    $locarr = $a_form_post_data['locarr'];
                if ($a_form_post_data['serarr'] != '')
                    $serarr = $a_form_post_data['serarr'];
                if ($a_form_post_data['sedarr'] != '')
                    $sedarr = $a_form_post_data['sedarr'];
                $a_route = $o_model_route->getRouteArray($locdep, $serdep, $seddep, $locarr, $serarr, $sedarr, 'rate');
                if (count($a_route) == 0) {
                    $a_route = $o_model_route->getRouteArray($locdep, $serdep, 0, $locarr, 0, $sedarr, 'rate');
                    if (count($a_route) == 0) {
                        $a_route = $o_model_route->getRouteArray($locdep, 0, 0, $locarr, 0, 0, 'rate');
                    }
                }
            }
            $a_response['data'] = $a_route;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_getLocationsDepartureByService() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('frate_service', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'frate_service':
                            $s_label_name = __("Servicio");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_Service = new Model_Service($a_form_post_data['frate_service']);
            if (!$o_model_Service->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_route = new Model_Route();
            $a_response['data'] = $o_model_route->listArray($o_model_Service->idService, 0, 0, TRUE);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_deleteRate() {
        $a_response = $this->a_bts_json_response;
        //$a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idrate', 'not_empty');

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_rate = new Model_Rate($a_form_post_data['idrate']);
            if (!$o_rate->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
            $o_rate->updateStatusRate();
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        $this->fnSYSResponseFormat($a_response);
    }

}
