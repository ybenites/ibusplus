<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Route
 *
 * @author BENITES
 */
class Controller_Management_Route extends Private_Admin implements TravelynxConstants {

    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        $this->fnSYSLoadClientLanguage();
        $a_interface = Kohana::$config->load('uInterface');
        foreach ($a_interface as $s_className) {
            $this->imports(get_class($s_className));
        }
    }

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('MANAGEMENT_ROUTES', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('MANAGEMENT_ROUTES', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);

        $o_model_service = new Model_Service();
        $o_model_location = new Model_Location();
        $o_model_sede = new Model_Sede();
        $v_routes = View::factory('management/routes');
        $v_routes->type_route = $this->fnGetClassVariable(self::ROUTE_TYPE);

        $v_routes->LOCATION = $o_model_location->getAllLocation();
        $v_routes->SERVICE = $o_model_service->listArray();
        $v_routes->SEDE = $o_model_sede->getAllSede();

        $this->template->SYSTEM_BODY = $v_routes;
    }

    public function action_list() {
        $this->auto_render = FALSE;
        try {
            $o_cacheInstance = Cache::instance('apc');
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' se.idRoute ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $ida = __('Ida');
            $ida_vuelta = __('Ida y Vuelta');
            $a_array_cols = array(
                "route_id" => "ro.idRoute"
                , "route_description" => "ro.description"
                , "route_type" => "CASE WHEN ro.type ='ROUNDTRIP' THEN  '$ida_vuelta' 
                                    WHEN ro.type ='ONEWAY' THEN '$ida' 
                                    ELSE 'NO EXISTE' END"
                , "route_departure" => "lod.name"
                , "route_service_departure" => "serd.name"
                , "route_sede_departure" => "sed.name"
                , "route_arrival" => "loa.name"
                , "route_service_arrival" => "sera.name"
                , "route_sede_arrival" => "(SELECT GROUP_CONCAT(sea.name) FROM tra_sede sea WHERE sea.status=1 AND SUBSTRING_INDEX(ro.idSedeArrival,',',FIND_IN_SET(sea.idSede ,ro.idSedeArrival)))"
                , "route_status" => "ro.status"
            );

            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }
            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' tra_route ro 
                                INNER JOIN tra_location lod ON lod.idLocation = ro.idLocationDeparture
                                LEFT JOIN tra_service serd ON serd.idService = ro.idServiceDeparture
                                LEFT JOIN tra_sede sed ON sed.idSede = ro.idSedeDeparture
                                INNER JOIN tra_location loa ON loa.idLocation = ro.idLocationArrival              
                                LEFT JOIN tra_service sera ON sera.idService = ro.idServiceArrival';

            $s_where_conditions = "ro.status=" . self::BTS_STATUS_ACTIVE;
//            $s_where_conditions .=" and serd.status=".self::BTS_STATUS_ACTIVE;
//            $s_where_conditions .=" and sed.status=".self::BTS_STATUS_ACTIVE." and sera.status=".self::BTS_STATUS_ACTIVE." and sea.status=".self::BTS_STATUS_ACTIVE;
//            $s_where_conditions.=" and lod.status=".self::BTS_STATUS_ACTIVE." and loa.status=".self::BTS_STATUS_ACTIVE;
            $s_cacheKeyGlobalController = SITE_DOMAIN . $this->request->controller();
            $s_cacheKey = SITE_DOMAIN . $this->request->controller() . $this->request->action() . $i_page . $s_where_search . $s_idx . $s_ord . $i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject === NULL OR $a_incacheObjectController === NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
    }

    public function action_create() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            Database::instance()->begin();
            $this->fnSYSRouteSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post, $a_error_labels);
            Database::instance()->commit();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_update() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = SITE_DOMAIN . $this->request->controller();
        $o_cacheInstance->delete($s_cacheKey);
        try {
            $this->fnSYSRouteSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post, $a_error_labels);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_delete() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_route = new Model_Route($a_form_post_data['id']);

            if (!$o_model_route->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_route->status = self::BTS_STATUS_DEACTIVE;
            $o_model_route->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN . $this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_get() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_route = new Model_Route($a_form_post_data['id']);

            if (!$o_model_route->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $data_route = array(
                'route_id' => $o_model_route->idRoute
                , 'route_description' => $o_model_route->description
                , 'route_type' => $o_model_route->type
                , 'route_departure' => $o_model_route->idLocationDeparture
                , 'route_service_departure' => $o_model_route->idServiceDeparture
                , 'route_sede_departure' => $o_model_route->idSedeDeparture
                , 'route_arrival' => $o_model_route->idLocationArrival
                , 'route_sede_arrival' => $o_model_route->idSedeArrival
                , 'route_service_arrival' => $o_model_route->idServiceArrival
                ,'route_private'=>$o_model_route->private    
            );
            $a_sede = array();
            if ($o_model_route->idSedeArrival != NULL) {
                $a_sede = DB::select(array('idSede', 'idSede'), array('name', 'nameSede'))
                                ->from('tra_sede')->where('status', '=', self::BTS_STATUS_ACTIVE)
                                ->and_where('idSede', 'IN', explode(',', $o_model_route->idSedeArrival))
                                ->execute()->as_array();
            }
            $data_route['route_g_sede_arrival'] = $a_sede;
            $a_response['data'] = $data_route;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }

    public function fnSYSRouteSave($s_controller_type, $o_check_post, &$a_error_labels) {
        $o_check_post
                ->rule('froute_description', 'not_empty')
                ->rule('froute_type', 'not_empty')
                ->rule('froute_departure', 'not_empty')
                ->rule('froute_arrival', 'not_empty');

        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {
                switch ($s_key) {
                    case 'froute_description':
                        $s_label_name = __("Descripción");
                        break;
                    case 'froute_type':
                        $s_label_name = __("Tipo");
                        break;
                    case 'froute_departure':
                        $s_label_name = __("Salida");
                        break;
                    case 'froute_arrival':
                        $s_label_name = __("Llegada");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }
        $a_form_post_data = $o_check_post->data();
        if ($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE) {
            $o_model_route = new Model_Route();
        } elseif ($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE) {
            $o_model_route = new Model_Route($a_form_post_data['froute_id']);
            if (!$o_model_route->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else {
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
        $a_route_save = array(
            'froute_description' => $a_form_post_data['froute_description']
            , 'froute_arrival' => $a_form_post_data['froute_arrival']
            , 'froute_departure' => $a_form_post_data['froute_departure']
            , 'froute_type' => $a_form_post_data['froute_type']
            , 'froute_sede_departure' => NULL
            , 'froute_service_departure' => NULL
            , 'froute_sede_arrival' => NULL
            , 'froute_service_arrival' => NULL
            ,'froute_private'=>(array_key_exists('froute_private',$a_form_post_data))?$a_form_post_data['froute_private']:0
        );
        if (array_key_exists('froute_sede_departure', $a_form_post_data)) {
            $a_route_save['froute_sede_departure'] = $a_form_post_data['froute_sede_departure'];
        }
        if (array_key_exists('fsede_group_id', $a_form_post_data)) {
            $a_route_save['froute_sede_arrival'] = $a_form_post_data['fsede_group_id'];
        }
        if (array_key_exists('froute_service_departure', $a_form_post_data)) {
            $a_route_save['froute_service_departure'] = $a_form_post_data['froute_service_departure'];
        }
        if (array_key_exists('froute_service_arrival', $a_form_post_data)) {
            $a_route_save['froute_service_arrival'] = $a_form_post_data['froute_service_arrival'];
        }

        $o_model_route->saveArray($a_route_save);
    }

}