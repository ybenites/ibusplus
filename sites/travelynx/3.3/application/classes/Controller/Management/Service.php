<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Management_Service extends Private_Admin implements TravelynxConstants {
    public function action_index(){
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {            
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('MANAGEMENT_SERVICES', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('MANAGEMENT_SERVICES', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/jqGrid.i18n/grid.locale-' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);          
        
        $v_services= View::factory('management/services');         
        $this->template->SYSTEM_BODY = $v_services;
    }
    
    public function action_create(){        
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {                        
            $this->fnSYSServiceSave(self::BTS_CONTROLLER_TYPE_CREATE, $o_check_post,$a_error_labels);   
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN.$this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode(); 
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function fnSYSServiceSave($s_controller_type, $o_check_post, &$a_error_labels){
        $o_check_post                
                ->rule('fservice_name', 'not_empty')
                ->rule('fservice_description', 'not_empty');
        
        if (!$o_check_post->check()) {
            foreach ($o_check_post->errors() as $s_key => $a_item) {                    
                switch ($s_key) {
                    case 'fservice_name':
                        $s_label_name = __("Nombre");
                        break;
                    case 'fservice_description':
                        $s_label_name = __("Descripción");
                        break;
                    default:
                        break;
                }
                switch ($a_item[0]) {
                    case 'not_empty':
                        $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                        break;
                    default:
                        break;
                }
            }
            throw new Exception("", self::BTS_CODE_SUCCESS);
        }
        Database::instance()->begin();
        $a_form_post_data = $o_check_post->data();        
        if($s_controller_type == self::BTS_CONTROLLER_TYPE_CREATE){
            $o_model_service= new Model_Service();
        }elseif($s_controller_type == self::BTS_CONTROLLER_TYPE_UPDATE){
            $o_model_service= new Model_Service($a_form_post_data['fservice_id']);
            if (!$o_model_service->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);
        }else{
            throw new Exception(__("Accion no encontrada"), self::BTS_CODE_SUCCESS);
        }
        
        $a_service_save = array(
            'fservice_name' =>$a_form_post_data['fservice_name']
            , 'fservice_description' => $a_form_post_data['fservice_description']        
        );
        $o_model_service->saveArray($a_service_save);   
        Database::instance()->commit();
    }
    public function action_list(){
        $this->auto_render = FALSE;
        try {
            $o_cacheInstance = Cache::instance('apc');            
            $i_page = $this->request->post('page');
            $i_limit = $this->request->post('rows');
            $s_idx = $this->request->post('sidx');
            $s_ord = $this->request->post('sord');

            if (!$s_idx)
                $s_idx = ' se.idService ';

            $s_where_search = "";

            $b_search_on = HelperJQGrid::fnStrip($this->request->post('_search'));

            $a_array_cols = array(
                "services_id" => "se.idService"
                , "services_name" => "se.name"                
                , "services_description" => "se.description"
                , "services_status" => "se.status"
                , "services_userCreate" => "pe.fullName"
                , "services_dateCreate" => "se.creationDate"                
                , "services_userLastChange" => "per.fullName"                
                , "services_dateLastChange" => "se.lastChangeDate"                
            );
            
            //COLUMNAS DE BUSQUEDA SIMPLE
            $a_sercheable_cols = array_keys($a_array_cols);

            if ($b_search_on == 'true') {
                if ($this->request->post('filters')) {
                    //BÚSQUEDA AVANZADA
                    $s_search_str = HelperJQGrid::fnStrip($this->request->post('filters'));
                    $s_where_search = HelperJQGrid::fnConstructWhere($s_search_str);
                } else {
                    //BÚSQUEDA SIMPLE POR COLUMNA
                    foreach ($this->request->post() as $i_key => $s_value) {
                        if (
                                in_array(
                                        str_ireplace('_', '.', $i_key)
                                        , $a_sercheable_cols
                                )
                        ) {
                            $s_where_search .= " AND "
                                    . str_ireplace('_', '.', $i_key)
                                    . " LIKE '%" . $s_value . "%'";
                        }
                    }
                }
            }

            $s_cols = HelperJQGrid::fnArrayColsToSQL($a_array_cols);
            $s_tables_join = ' tra_service se INNER JOIN bts_person pe ON se.userCreate = pe.idPerson
                                INNER JOIN bts_person per ON se.userLastChange = per.idPerson';
        
            $s_where_conditions = "1=1 and se.status=".self::BTS_STATUS_ACTIVE;
            $s_cacheKeyGlobalController = SITE_DOMAIN.$this->request->controller();
            $s_cacheKey = SITE_DOMAIN.$this->request->controller().$this->request->action().$i_page.$s_where_search.$s_idx.$s_ord.$i_limit;
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            $a_incacheObjectController = $o_cacheInstance->get($s_cacheKeyGlobalController);
            if ($a_incacheObject ===NULL OR $a_incacheObjectController===NULL) {
                $i_count = HelperJQGrid::fnGetCount($s_cols, $s_tables_join, $s_where_conditions, $s_where_search);
                $i_total_pages = 0;
                $i_start = HelperJQGrid::fnCalculateStart($i_page, $i_count, $i_limit, $i_total_pages);
                $a_result = HelperJQGrid::fnGenerateSQL($s_cols, $s_tables_join, $s_where_conditions, $s_where_search, $s_idx, $s_ord, $i_limit, $i_start);
                $result = HelperJQGrid::fnJSON4jqGrid($i_page, $i_total_pages, $i_count, $a_result, $a_array_cols, true);
                $o_cacheInstance->set($s_cacheKey, $result);
                $o_cacheInstance->set($s_cacheKeyGlobalController, 1);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
            }                        
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        $this->fnSYSResponseFormat($a_incacheObject, self::BTS_RESPONSE_TYPE_JQGRID);
    }
    
    public function action_delete(){
         $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');

            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_service = new Model_Service($a_form_post_data['id']);

            if (!$o_model_service->loaded())
                throw new Exception(__("Registro no encontrado"), self::BTS_CODE_SUCCESS);

            $o_model_service->status = self::BTS_STATUS_DEACTIVE;
            $o_model_service->save();
            $o_cacheInstance = Cache::instance('apc');
            $s_cacheKey = SITE_DOMAIN.$this->request->controller();
            $o_cacheInstance->delete($s_cacheKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_get(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('id', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'id':
                            $s_label_name = __("ID");
                            break;
                        default:
                            break;
                    }                    
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_service = new Model_Service($a_form_post_data['id']);

            if (!$o_model_service->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
            
            $a_response['data'] = array(
                'service_id'=>$o_model_service->idService
                ,'service_name' => $o_model_service->name
                ,'service_description' => $o_model_service->description                
            );
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
    public function action_update(){
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = SITE_DOMAIN.$this->request->controller();
        $o_cacheInstance->delete($s_cacheKey);
        try {                        
            $this->fnSYSServiceSave(self::BTS_CONTROLLER_TYPE_UPDATE, $o_check_post,$a_error_labels);        
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        $this->fnSYSResponseFormat($a_response);
    }
}