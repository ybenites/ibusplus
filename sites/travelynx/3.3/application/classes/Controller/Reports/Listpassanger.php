<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of closebox
 *
 * @author Harby
 */
defined('SYSPATH') or die('No direct script access. ');

class Controller_Reports_Listpassanger extends Controller_Reports_Abstractreports {

    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        $this->fnSYSLoadClientLanguage();
        $a_interface = Kohana::$config->load('uInterface');
        foreach ($a_interface as $s_className) {
            $this->imports(get_class($s_className));
        }
    }

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('REPORTS_LISTPASSANGER', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('REPORTS_LISTPASSANGER', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/datepicker/bootstrap-datetimepicker.' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);
        $view_listpassanger = View::factory('reports/listpassanger');
        $o_model_route = new Model_Route;
        $view_listpassanger->list_AllRoute = $o_model_route->getAllRoute();
        $view_listpassanger->a_year_report = $this->fnGetClassVariable(self::YEAR_NAME);
        $view_listpassanger->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $this->template->SYSTEM_BODY = $view_listpassanger;
    }

    public function action_reportManifestPassanger() {
        $this->auto_render = false;
        try {
            $newArray = array();
            $d = $this->request->query('d');
            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $idRoute = $this->request->query('idRoute');
           
            $newArray['qlang'] = $this->request->query(self::BTS_REPORT_LANG);
                        if ($d != "") {
                $newArray['date'] = $d;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $newArray['date'] = $sd . " - " . $ed;
                } else {
                    $newArray['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $newArray['date'] = $sd;
                }
            }
            $listPassanger = $this->getListPassanger($idRoute,$d, $sd, $ed);
           
            $this->fnSYSResponseFormat($listPassanger, self::BTS_RESPONSE_TYPE_XML, $newArray);
        } catch (Exception $e_exc) {
          $k= $this->fnSYSErrorHandling($e_exc);
          print_r($k);
          die();
        }
    }
}
