<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of abstractReports
 *
 * @author harby
 */
defined('SYSPATH') or die('No direct script access. ');

class Controller_Reports_Abstractreports extends Controller_Private_Admin implements TravelynxConstants {

    // Reportes de Ventas (Cierre Diario, Terminal, Web, Paypal)
    public function getSalesByType($type, $idOffice, $d, $sd, $ed, $m, $a) {

        $rDateFormat = $this->request->query(self::BTS_REPORT_DATE_FORMAT);
        $date_format = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL, $rDateFormat);
        $date_time = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_TIME_SQL, $rDateFormat);
        $time_raw = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_RAW, $rDateFormat);
        $time_limit = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_LIMIT, $rDateFormat);

        $parameters = array(
            ':ROUTE_ONEWAY' => self::ROUTE_ONEWAY,
            ':ROUTE_ROUNDTRIP' => self::ROUTE_ROUNDTRIP,
            ':CASH_PAYMENT' => self::SALES_PAYMENT_CASH,
            ':PAYPAL_PAYMENT' => self::SALES_PAYMENT_PAYPAL,
            ':STATUS_ACTIVE' => self::BTS_STATUS_ACTIVE,
            ':STATUS_DEACTIVE' => self::BTS_STATUS_DEACTIVE,
            ':date_time' => $date_time,
            ':date_format' => $date_format,
            ':time_raw' => $time_raw,
            ':time_limit' => $time_limit,
            ':sd' => $sd,
            ':ed' => $ed,
            ':a' => $a,
            ':m' => $m,
            ':d' => $d
        );

        if (in_array($type, array(self::REPORT_SALE_TYPE_ALL_TICKETS, self::REPORT_SALE_TYPE_SALES_OFFICE))) {
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idShoppingCart` AS idShoppingCart');
            $aColumns[] = DB::expr('`office`.`name` AS office');
            $aColumns[] = DB::expr('`person`.`fullName` AS user');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`code` AS codeShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`methodPayment` AS methodPayment');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`typeSale` AS salesType');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_shoppingcart`.`listingDate`,'$date_time') AS transactionDate");
            $aColumns[] = DB::expr('`tra_shoppingcart`.`status` AS statusShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idPaymentLog` AS idPaymentPaypal');
            $aColumns[] = DB::expr('`tra_sale`.`idSale` AS idSale');
            $aColumns[] = DB::expr('`tra_sale`.`codeSale` AS codeSale');
            $aColumns[] = DB::expr('`tra_sale`.`travelTypeSelected` AS travelType');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_sale`.`travelDateOw`,'$date_format') AS travelDateOw");
            $aColumns[] = DB::expr('`tra_sale`.`numpaxOw` AS numpaxOw');
            $aColumns[] = DB::expr('`tra_sale`.`unitPriceOw` AS unitPriceOw');
            $aColumns[] = DB::expr('`tra_sale`.`totalPriceOw` AS totalPriceOw');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_sale`.`travelDateRt`,'$date_format') AS travelDateRt");
            $aColumns[] = DB::expr('`tra_sale`.`numpaxRt` AS numpaxRt');
            $aColumns[] = DB::expr('`tra_sale`.`unitPriceRt` AS unitPriceRt');
            $aColumns[] = DB::expr('`tra_sale`.`totalPriceRt` AS totalPriceRt');
            $aColumns[] = DB::expr('`tra_sale`.`totalPrice` AS totalPrice');
            $aColumns[] = DB::expr("IF(`tra_sale`.`status`=:STATUS_DEACTIVE,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCancel");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`websale`=:STATUS_ACTIVE,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isWebSale");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`methodPayment`=:CASH_PAYMENT,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCash");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`methodPayment`=:PAYPAL_PAYMENT,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCreditCard");
            $aColumns[] = DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = tra_route.idLocationDeparture) AS dLocation");
            $aColumns[] = DB::expr('`sedeDeparture`.`name` AS dSede');
            $aColumns[] = DB::expr('`serviceDeparture`.`name` AS dService');
            $aColumns[] = DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = tra_route.idLocationArrival) AS aLocation");
            $aColumns[] = DB::expr('`sedeArrival`.`name` AS aSede');
            $aColumns[] = DB::expr('`serviceArrival`.`name` AS aService');
            $aColumns[] = DB::expr("IF(tra_sale.idSedeArrivalSelected IS NULL,'tra_sale.addressArrival','') as addressArrival");
        } elseif (in_array($type, array(self::REPORT_SALE_TYPE_SALES_WEBSALE))) {
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idShoppingCart` AS idShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`code` AS codeShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`methodPayment` AS methodPayment');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`typeSale` AS salesType');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_shoppingcart`.`listingDate`,'$date_format $date_time') AS transactionDate");
            $aColumns[] = DB::expr('`tra_shoppingcart`.`status` AS statusShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idPaymentLog` AS idPaymentPaypal');
            $aColumns[] = DB::expr('`tra_sale`.`idSale` AS idSale');
            $aColumns[] = DB::expr('`tra_sale`.`codeSale` AS codeSale');
            $aColumns[] = DB::expr('`tra_sale`.`travelTypeSelected` AS travelType');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_sale`.`travelDateOw`,'$date_format') AS travelDateOw");
            $aColumns[] = DB::expr('`tra_sale`.`numpaxOw` AS numpaxOw');
            $aColumns[] = DB::expr('`tra_sale`.`unitPriceOw` AS unitPriceOw');
            $aColumns[] = DB::expr('`tra_sale`.`totalPriceOw` AS totalPriceOw');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_sale`.`travelDateRt`,'$date_format') AS travelDateRt");
            $aColumns[] = DB::expr('`tra_sale`.`numpaxRt` AS numpaxRt');
            $aColumns[] = DB::expr('`tra_sale`.`unitPriceRt` AS unitPriceRt');
            $aColumns[] = DB::expr('`tra_sale`.`totalPriceRt` AS totalPriceRt');
            $aColumns[] = DB::expr('`tra_sale`.`totalPrice` AS totalPrice');
            $aColumns[] = DB::expr("IF(`tra_sale`.`status`=:STATUS_DEACTIVE,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCancel");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`websale`=:STATUS_ACTIVE,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isWebSale");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`methodPayment`=:CASH_PAYMENT,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCash");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`methodPayment`=:PAYPAL_PAYMENT,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCreditCard");
            $aColumns[] = DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = tra_route.idLocationDeparture) AS dLocation");
            $aColumns[] = DB::expr('`sedeDeparture`.`name` AS dSede');
            $aColumns[] = DB::expr('`serviceDeparture`.`name` AS dService');
            $aColumns[] = DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = tra_route.idLocationArrival) AS aLocation");
            $aColumns[] = DB::expr('`sedeArrival`.`name` AS aSede');
            $aColumns[] = DB::expr('`serviceArrival`.`name` AS aService');
            $aColumns[] = DB::expr("IF(tra_sale.idSedeArrivalSelected IS NULL,'tra_sale.addressArrival','') as addressArrival");
        } elseif (in_array($type, array(self::REPORT_SALE_TYPE_SALES_PAYPAL_TICKETS))) {
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idShoppingCart` AS idShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`code` AS codeShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`methodPayment` AS methodPayment');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`typeSale` AS salesType');
            $aColumns[] = DB::expr("DATE_FORMAT(`tra_shoppingcart`.`listingDate`,'$date_time') AS transactionDate");
            $aColumns[] = DB::expr('`tra_shoppingcart`.`status` AS statusShoppingCart');
            $aColumns[] = DB::expr('`tra_shoppingcart`.`idPaymentLog` AS idPaymentPaypal');
            $aColumns[] = DB::expr('`sale`.`idSale` AS idSale');
            $aColumns[] = DB::expr('`sale`.`totalPrice` AS totalPrice');
            $aColumns[] = DB::expr("IF(`sale`.`status`=:STATUS_DEACTIVE,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCancel");
            $aColumns[] = DB::expr("IF(`tra_shoppingcart`.`methodPayment`=:PAYPAL_PAYMENT,:STATUS_ACTIVE,:STATUS_DEACTIVE) AS isCreditCard");
            $aColumns[] = DB::expr("CONCAT(traveler.firstName,' ',traveler.lastName) AS traveler");
            $aColumns[] = DB::expr('`paypal`.`id` AS idPaypal');
            $aColumns[] = DB::expr('`paypal`.`amount` AS amountPaypal');
            $aColumns[] = DB::expr('`paypal`.`txn_id` AS transaction');
            $aColumns[] = DB::expr('`paypal`.`creditcardtype` AS cardType');
            $aColumns[] = DB::expr('`paypal`.`action` AS actionPaypal');
            $aColumns[] = DB::expr('`paypal`.`success` AS successPaypal');
        }
        $result = DB::select_array($aColumns);

        if (in_array($type, array(self::REPORT_SALE_TYPE_ALL_TICKETS, self::REPORT_SALE_TYPE_SALES_OFFICE))) {

            $sql = $result->from('tra_shoppingcart')
                    ->join('tra_sale')->on('tra_sale.idShoppingCart', '=', 'tra_shoppingcart.idShoppingCart')
                    ->join('tra_route')->on('tra_route.idRoute', '=', 'tra_sale.idRoute')
                    ->join(array('tra_sede', 'sedeDeparture'))->on('sedeDeparture.idSede', '=', 'tra_sale.idSedeDepartureSelected')
                    ->join(array('tra_sede', 'sedeArrival'), 'LEFT')->on('sedeArrival.idSede', '=', 'tra_sale.idSedeArrivalSelected')
                    ->join(array('tra_service', 'serviceDeparture'))->on('serviceDeparture.idService', '=', 'sedeDeparture.idService')
                    ->join(array('tra_service', 'serviceArrival'), 'LEFT')->on('serviceArrival.idService', '=', 'sedeArrival.idService')
                    ->join(array('bts_user', 'user'))->on('user.idUser', '=', 'tra_sale.userCreate')
                    ->join(array('bts_person', 'person'))->on('person.idPerson', '=', 'user.idUser')
                    ->join(array('bts_office', 'office'))->on('office.idOffice', '=', 'user.idOffice')
                    ->where('tra_shoppingcart.webSale', '=', ':STATUS_DEACTIVE');

            if ($idOffice != NULL) {
                $sql->and_where('office.idOffice', 'IN', DB::expr("(" . $idOffice . ")"));
            }

            if ($d != "") {
                $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql->and_where(DB::expr("MONTH(tra_shoppingcart.listingDate)"), '=', DB::expr(":m AND YEAR(tra_shoppingcart.listingDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql->and_where('tra_shoppingcart.listingDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql->parameters($parameters);
        } elseif (in_array($type, array(self::REPORT_SALE_TYPE_SALES_WEBSALE))) {
            $sql = $result->from('tra_shoppingcart')
                    ->join('tra_sale')->on('tra_sale.idShoppingCart', '=', 'tra_shoppingcart.idShoppingCart')
                    ->join('tra_route')->on('tra_route.idRoute', '=', 'tra_sale.idRoute')
                    ->join(array('tra_sede', 'sedeDeparture'))->on('sedeDeparture.idSede', '=', 'tra_sale.idSedeDepartureSelected')
                    ->join(array('tra_sede', 'sedeArrival'), 'LEFT')->on('sedeArrival.idSede', '=', 'tra_sale.idSedeArrivalSelected')
                    ->join(array('tra_service', 'serviceDeparture'))->on('serviceDeparture.idService', '=', 'sedeDeparture.idService')
                    ->join(array('tra_service', 'serviceArrival'), 'LEFT')->on('serviceArrival.idService', '=', 'sedeArrival.idService')
                    ->where('tra_shoppingcart.webSale', '=', ':STATUS_ACTIVE');
            if ($d != "") {
                $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }

            if ($m != 0) {
                $sql->and_where(DB::expr("MONTH(tra_shoppingcart.listingDate)"), '=', DB::expr(":m AND YEAR(tra_shoppingcart.listingDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql->and_where('tra_shoppingcart.listingDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql->parameters($parameters);
        } elseif (in_array($type, array(self::REPORT_SALE_TYPE_SALES_PAYPAL_TICKETS))) {
            $sql = $result->from('tra_shoppingcart')
                    ->join(array('bts_payment_logs', 'paypal'))->on('paypal.id', '=', 'tra_shoppingcart.idPaymentLog')
                    ->join(array('tra_sale', 'sale'))->on('sale.idShoppingCart', '=', 'tra_shoppingcart.idShoppingCart')
                    ->join(array('tra_travelersale', 'tsale'))->on('tsale.idSale', '=', 'sale.idSale')
                    ->join(array('tra_traveler', 'traveler'))->on('traveler.idTraveler', '=', 'tsale.idTraveler')
                    ->where('tra_shoppingcart.methodPayment', 'LIKE', ':PAYPAL_PAYMENT');
            if ($d != "") {
                $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql->and_where(DB::expr("MONTH(tra_shoppingcart.listingDate)"), '=', DB::expr(":m AND YEAR(tra_shoppingcart.listingDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql->and_where('tra_shoppingcart.listingDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql->and_where(DB::expr("DATE_FORMAT(tra_shoppingcart.listingDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql->parameters($parameters);
        }
        return $sql->execute()->as_array();
    }

    //Reporte de Manifiesto de Pasajeros
    public function getListPassanger($idRoute, $d, $sd, $ed) {
        try {
            $rDateFormat = $this->request->query(self::BTS_REPORT_DATE_FORMAT);
            $date_format = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL, $rDateFormat);
            $date_time = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL, $rDateFormat);
            $time_raw = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_RAW, $rDateFormat);
            $time_limit = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_LIMIT, $rDateFormat);

            $parameters = array(
                ':ROUTE_ONEWAY' => self::ROUTE_ONEWAY,
                ':ROUTE_ROUNDTRIP' => self::ROUTE_ROUNDTRIP,
                ':STATUS_ACTIVE' => self::BTS_STATUS_ACTIVE,
                ':STATUS_DEACTIVE' => self::BTS_STATUS_DEACTIVE,
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':sd' => $sd,
                ':ed' => $ed,
                ':d' => $d
            );
            $cols_Ow = array(
                array('shopping.idShoppingCart', 'idShoppingCart')
                , array('shopping.code', 'codeShoppingCart')
                , array('shopping.typeSale', 'salesType')
                , array('shopping.status', 'statusShopping')
                , array('shopping.observations', 'observations')
                , DB::expr("DATE_FORMAT(shopping.listingDate,:date_format) as transactionDate")
                , array('route.idRoute', 'idRoute')
                , array('route.description', 'descriptionRoute')
                , array('route.type', 'routeType')
                , array('sale.idSale', 'idSale')
                , array('sale.codeSale', 'codeSale')
                , array('sale.travelTypeSelected', 'travelType')
                , DB::expr("IF(rate.allTime = :STATUS_DEACTIVE,(SELECT description FROM tra_hour WHERE idHour = rate.startTime),'AllTime') AS startTime")
                , DB::expr("IF(rate.allTime = :STATUS_DEACTIVE,(SELECT description FROM tra_hour WHERE idHour = rate.endTime),'AllTime') AS endTime")
                , DB::expr("DATE_FORMAT(sale.travelDateOw,:date_format) AS travelDateOw")
                , array('sale.numpaxOw', 'numpaxOw')
                , array('sale.unitPriceOw', 'unitPriceOw')
                , array('sale.totalPriceOw', 'totalPriceOw')
                , DB::expr("DATE_FORMAT(sale.travelDateRt,:date_format) AS travelDateRt")
                , array('sale.numpaxRt', 'numpaxRt')
                , array('sale.unitPriceRt', 'unitPriceRt')
                , array('sale.totalPriceRt', 'totalPriceRt')
                , array('sale.totalPrice', 'totalAmount')
                , DB::expr("CONCAT(traveler.firstName,' ',traveler.lastName) as traveler")
                , DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = route.idLocationDeparture) AS dLocation")
                , array('sedeDeparture.name', 'dSede')
                , array('serviceDeparture.name', 'dService')
                , DB::expr("(SELECT tra_location.name FROM tra_location WHERE idLocation = route.idLocationArrival) AS aLocation")
                , array('sedeArrival.name', 'aSede')
                , array('serviceArrival.name', 'aService')
                , DB::expr("'OneWay' as typeTrip")
                , array('sale.flightNumberDeparture', 'flightNumber')
                , DB::expr("(SELECT tra_airlines.name FROM tra_airlines WHERE idAirlines = sale.idAirlineDeparture) AS airline")
            );
            $sql_listPassangerOw = DB::select_array($cols_Ow)
                    ->from(array('tra_shoppingcart', 'shopping'))
                    ->join(array('tra_sale', 'sale'))->on('sale.idShoppingCart', '=', 'shopping.idShoppingCart')
                    ->join(array('tra_route', 'route'))->on('route.idRoute', '=', 'sale.idRoute')
                    ->join(array('tra_rate', 'rate'))->on('rate.idRate', '=', 'sale.idRateOw')
                    ->join(array('tra_travelersale', 'tsale'))->on('tsale.idSale', '=', 'sale.idSale')
                    ->join(array('tra_traveler', 'traveler'))->on('traveler.idTraveler', '=', 'tsale.idTraveler')
                    ->join(array('tra_sede', 'sedeDeparture'))->on('sedeDeparture.idSede', '=', 'sale.idSedeDepartureSelected')
                    ->join(array('tra_sede', 'sedeArrival'), 'LEFT')->on('sedeArrival.idSede', '=', 'sale.idSedeArrivalSelected')
                    ->join(array('tra_service', 'serviceDeparture'))->on('serviceDeparture.idService', '=', 'sedeDeparture.idService')
                    ->join(array('tra_service', 'serviceArrival'), 'LEFT')->on('serviceArrival.idService', '=', 'sedeArrival.idService')
                    ->where('shopping.status', '=', ':STATUS_ACTIVE');

            if ($idRoute != NULL) {
                $sql_listPassangerOw->and_where('sale.idRoute', 'IN', DB::expr("(" . $idRoute . ")"));
            }
            if ($d != "") {
                $sql_listPassangerOw->and_where(DB::expr("DATE_FORMAT(sale.travelDateOw,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_listPassangerOw->and_where('sale.travelDateOw', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_listPassangerOw->and_where(DB::expr("DATE_FORMAT(sale.travelDateOw,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_listPassangerOw->and_where(DB::expr("DATE_FORMAT(sale.travelDateOw,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_listPassangerOw->parameters($parameters)->group_by('sale.codeSale');

            $cols_Rt = $cols_Ow;
            $cols_Rt[30] = DB::expr("'RoundTrip' as typeTrip");
            $cols_Rt[31] = array('sale.flightNumberArrival', 'flightNumber');
            $cols_Rt[32] = DB::expr("(SELECT tra_airlines.name FROM tra_airlines WHERE idAirlines = sale.idAirlineArrival) AS airline");

            $sql_listPassangerRt = DB::select_array($cols_Rt)
                    ->union($sql_listPassangerOw, TRUE)
                    ->from(array('tra_shoppingcart', 'shopping'))
                    ->join(array('tra_sale', 'sale'))->on('sale.idShoppingCart', '=', 'shopping.idShoppingCart')
                    ->join(array('tra_route', 'route'))->on('route.idRoute', '=', 'sale.idRoute')
                    ->join(array('tra_rate', 'rate'))->on('rate.idRate', '=', 'sale.idRateRt')
                    ->join(array('tra_travelersale', 'tsale'))->on('tsale.idSale', '=', 'sale.idSale')
                    ->join(array('tra_traveler', 'traveler'))->on('traveler.idTraveler', '=', 'tsale.idTraveler')
                    ->join(array('tra_sede', 'sedeDeparture'))->on('sedeDeparture.idSede', '=', 'sale.idSedeDepartureSelected')
                    ->join(array('tra_sede', 'sedeArrival'), 'LEFT')->on('sedeArrival.idSede', '=', 'sale.idSedeArrivalSelected')
                    ->join(array('tra_service', 'serviceDeparture'))->on('serviceDeparture.idService', '=', 'sedeDeparture.idService')
                    ->join(array('tra_service', 'serviceArrival'), 'LEFT')->on('serviceArrival.idService', '=', 'sedeArrival.idService')
                    ->where('shopping.status', '=', ':STATUS_ACTIVE');

            if ($idRoute != NULL) {
                $sql_listPassangerRt->and_where('sale.idRoute', 'IN', DB::expr("(" . $idRoute . ")"));
            }
            if ($d != "") {
                $sql_listPassangerRt->and_where(DB::expr("DATE_FORMAT(sale.travelDateRt,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_listPassangerRt->and_where('sale.travelDateRt', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_listPassangerRt->and_where(DB::expr("DATE_FORMAT(sale.travelDateRt,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_listPassangerRt->and_where(DB::expr("DATE_FORMAT(sale.travelDateRt,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_listPassangerRt->parameters($parameters)->group_by('sale.codeSale');

            $sql_listPassanger = $sql_listPassangerRt;
        } catch (Exception $e_exc) {
            echo $e_exc->getMessage();
        }
        return $sql_listPassanger->execute()->as_array();
    }

    //Reporte de Ventas (Mensuales)

    public function getMonthlySales($d, $sd, $ed, $m, $a) {
        try {
            $rDateFormat = $this->request->query(self::BTS_REPORT_DATE_FORMAT);
            $date_format = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_DATE_SQL, $rDateFormat);
            $date_time = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL, $rDateFormat);
            $time_raw = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_RAW, $rDateFormat);
            $time_limit = $this->fnSYSGetDATE_FORMAT_FOR(self::BTS_FORMAT_TIME_SQL_LIMIT, $rDateFormat);
            
            $parameters = array(
                ':ROUTE_ONEWAY' => self::ROUTE_ONEWAY,
                ':ROUTE_ROUNDTRIP' => self::ROUTE_ROUNDTRIP,
                ':STATUS_ACTIVE' => self::BTS_STATUS_ACTIVE,
                ':STATUS_DEACTIVE' => self::BTS_STATUS_DEACTIVE,
                ':date_time' => $date_time,
                ':date_format' => $date_format,
                ':time_raw' => $time_raw,
                ':time_limit' => $time_limit,
                ':sd' => $sd,
                ':ed' => $ed,
                ':d' => $d,
                ':m' => $m,
                ':a' => $a
            );
            
             $cols_one= array(
                 DB::expr("SUM(shopping.totalAmount) as totalAmount")
                , array('office.idOffice','idOffice')
                , array('office.name','office')
                , array('route.idRoute','idRoute')
                , array('route.description','route')
            );
            $sql_terminalSales = DB::select_array($cols_one)
                    ->from(array('tra_shoppingcart', 'shopping'))
                    ->join(array('tra_sale', 'sale'))->on('sale.idShoppingCart', '=', 'shopping.idShoppingCart')
                    ->join(array('tra_route', 'route'))->on('route.idRoute', '=', 'sale.idRoute')
                    ->join(array('bts_user', 'user'))->on('user.idUser', '=', 'shopping.userCreate')
                    ->join(array('bts_office', 'office'))->on('office.idOffice', '=', 'user.idOffice')
                    ->where('shopping.status', '=', ':STATUS_ACTIVE')
                    ->where('shopping.websale', '=', ':STATUS_DEACTIVE');

            
            if ($d != "") {
                $sql_terminalSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_terminalSales->and_where(DB::expr("MONTH(shopping.listingDate)"), '=', DB::expr(":m AND YEAR(shopping.listingDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_terminalSales->and_where('shopping.listingDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_terminalSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_terminalSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_terminalSales->parameters($parameters)->group_by('office.name')->group_by('route.idRoute');
            
            $cols_two = $cols_one;
            
            $cols_two[1] = DB::expr("0 as idOffice");
            $cols_two[2] = array('WebSale','office');
            
            $sql_webSales = DB::select_array($cols_two)
                    ->union($sql_terminalSales, TRUE)
                    ->from(array('tra_shoppingcart', 'shopping'))
                    ->join(array('tra_sale', 'sale'))->on('sale.idShoppingCart', '=', 'shopping.idShoppingCart')
                    ->join(array('tra_route', 'route'))->on('route.idRoute', '=', 'sale.idRoute')
                    ->where('shopping.status', '=', ':STATUS_ACTIVE')
                    ->where('shopping.websale', '=', ':STATUS_ACTIVE');

            if ($d != "") {
                $sql_webSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':d'));
            }
            if ($m != 0) {
                $sql_webSales->and_where(DB::expr("MONTH(shopping.listingDate)"), '=', DB::expr(":m AND YEAR(shopping.listingDate) = :a"));
            }
            if ($ed != "") {
                if ($sd != "") {
                    $sql_webSales->and_where('shopping.listingDate', 'BETWEEN', DB::expr("STR_TO_DATE(:sd , :date_format) AND STR_TO_DATE(:ed :time_limit, :date_format :time_raw)"));
                } else {
                    $sql_webSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':ed'));
                }
            } else {
                if ($sd != "") {
                    $sql_webSales->and_where(DB::expr("DATE_FORMAT(shopping.listingDate,:date_format)"), 'LIKE', DB::expr(':sd'));
                }
            }
            $sql_webSales->parameters($parameters)->group_by('route.idRoute');
            
        } catch (Expection $e_exc) {
            echo $e_exc->getMessage();
        }
         return $sql_webSales->execute()->as_array();
    }

}
