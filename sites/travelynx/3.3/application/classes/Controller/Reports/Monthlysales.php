<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of closebox
 *
 * @author Harby
 */
defined('SYSPATH') or die('No direct script access. ');

class Controller_Reports_Monthlysales extends Controller_Reports_Abstractreports {

    public function __construct(Request $o_request, Response $o_response) {
        parent::__construct($o_request, $o_response);
        $this->a_imported_class = array();
        $this->a_imported_functions = array();
        $this->fnSYSLoadClientLanguage();
        $a_interface = Kohana::$config->load('uInterface');
        foreach ($a_interface as $s_className) {
            $this->imports(get_class($s_className));
        }
    }

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('REPORTS_MONTHLYSALES', self::BTS_FILE_TYPE_CSS, 'bts/'));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('REPORTS_MONTHLYSALES', self::BTS_FILE_TYPE_JS, 'bts/'));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $lang = $this->template->SYSTEM_LANGUAGE;
        $a_more_scripts = array(
            array(
                'file_name' => '/kernel/js/bts/core/select2.i18n/select2_locale_' . $lang . '.js',
                'file_type' => 'js'
            )
            , array(
                'file_name' => '/kernel/js/bts/core/datepicker/bootstrap-datetimepicker.' . $lang . '.js',
                'file_type' => 'js'
            )
        );
        $this->fnSYSProjAddStyles($incacheObjectCss);
        $this->fnSYSProjAddScripts($incacheObjectJs, $a_more_scripts);
        $view_monthlysales = View::factory('reports/monthlysales');
        $view_monthlysales->a_year_report = $this->fnGetClassVariable(self::YEAR_NAME);
        $view_monthlysales->a_month_report = $this->fnGetClassVariable(self::MONTH_NAME);
        $this->template->SYSTEM_BODY = $view_monthlysales;
    }

    public function action_reportMonthlySales() {
        $this->auto_render = false;
        try {
            $newArray = array();
            $d = $this->request->query('d');
            $sd = $this->request->query('sd');
            $ed = $this->request->query('ed');
            $m = $this->request->query('m');
            $a = $this->request->query('a');

            $newArray['qlang'] = $this->request->query(self::BTS_REPORT_LANG);


            if ($d != "") {
                $newArray['date'] = $d;
            }

            if ($m != 0) {
                $newArray['date'] = $this->getMonthName($m) . " - " . $a;
            }

            if ($ed != "") {
                if ($sd != "") {
                    $newArray['date'] = $sd . " - " . $ed;
                } else {
                    $newArray['date'] = $ed;
                }
            } else {
                if ($sd != "") {
                    $newArray['date'] = $sd;
                }
            }

            $sql_allRoutes = DB::select(array('tra_route.description', 'route'), array('tra_route.idRoute', 'idRoute'))
                            ->from('tra_route')
                            ->execute()->as_array();
            
            $office =array(array('office'=>'WebSale','idOffice'=>0));
            $allOffices = DB::select(array('bts_office.name', 'office'), array('bts_office.idOffice', 'idOffice'))
                            ->from('bts_office')
                            ->execute()->as_array();
            $sql_allOffices = array_merge($allOffices,$office);
           
            
            $allMonthlySales = $this->getMonthlySales($d, $sd, $ed, $m, $a);

            $totalRoutes = array();
            $p = 0;

            for ($i = 0; $i < count($sql_allRoutes); $i++) {
                for ($j = 0; $j < count($sql_allOffices); $j++) {
                    $totalRoutes[$p]['route'] = $sql_allRoutes[$i]['route'];
                    $totalRoutes[$p]['office'] = $sql_allOffices[$j]['office'];
                    $totalAmount = 0;
                    for ($k = 0; $k < count($allMonthlySales); $k++) {
                        if ($sql_allRoutes[$i]['idRoute'] == $allMonthlySales[$k]['idRoute'] AND $sql_allOffices[$j]['idOffice'] == $allMonthlySales[$k]['idOffice']) {
                            $totalAmount = $allMonthlySales[$k]['totalAmount'];
                            break;
                        }
                    }
                    $totalRoutes[$p]['totalAmount'] = $totalAmount;
                    $p++;
                }
            }

            $this->fnSYSResponseFormat($totalRoutes, self::BTS_RESPONSE_TYPE_XML, $newArray);
            //  print_r(Database::instance()->last_query);
            // die(); 
        } catch (Exception $e_exc) {
            $k = $this->fnSYSErrorHandling($e_exc);
            print_r($k);
            die();
        }
    }

}
