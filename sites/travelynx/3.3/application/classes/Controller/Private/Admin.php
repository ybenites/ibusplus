<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Admin extends Controller_Private_Kernel {
    
     public function before() {
        parent::before();
        $isReportAccessAlowed = FALSE;
        if (!$this->request->is_ajax()) {
            $isReportAccessAlowed = $this->fnSYSHaveReportActiveSession();
            if (!$this->fnSYSHaveLoginAccess() AND !$isReportAccessAlowed) {
                $this->auto_render = false;
                $v_denied = View::factory('bts/public/sesion_expired');
                echo $v_denied;
                die();
            }
        }
        if (!$this->fnSYSHavePrivilegeAccess() AND !$isReportAccessAlowed) {
            if (!$this->request->is_ajax()) {
                $this->auto_render = false;
                $v_denied = View::factory('bts/public/442');
                echo $v_denied;
            } else {
                $this->fnSYSLoadClientLanguage();
                $a_response = $this->a_bts_json_response;
                $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_NO_AUTHORIZED;
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = __("Acceso Denegado");
                $this->fnSYSResponseFormat($a_response);
            }
            die();
        }
        if (!$this->request->is_ajax() AND !$isReportAccessAlowed) {
            $this->template->SYSTEM_A_SCRIPTS = $this->a_bts_scripts;
            $this->template->SYSTEM_A_STYLES = $this->a_bts_styles;
            $this->template->SYSTEM_A_PROJ_SCRIPTS = $this->a_proj_scripts;
            $this->template->SYSTEM_A_PROJ_STYLES = $this->a_proj_styles;
            $o_cache_instance = Cache::instance('apc');
            $s_request_url = 'CORE';
            $s_cache_key_css = SITE_DOMAIN . $s_request_url . 'css';
            $s_cache_key_js = SITE_DOMAIN . $s_request_url . 'js';
            $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
            $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            if ($a_in_cache_object_css === null) {
                $o_cache_instance->set($s_cache_key_css, self::fnSYSGetFilePage('CORE', self::BTS_FILE_TYPE_CSS));
                $o_cache_instance->set($s_cache_key_js, self::fnSYSGetFilePage('CORE', self::BTS_FILE_TYPE_JS));
                $a_in_cache_object_css = $o_cache_instance->get($s_cache_key_css);
                $a_in_cache_object_js = $o_cache_instance->get($s_cache_key_js);
            }
            $this->fnSYSAddScripts($a_in_cache_object_js);
            $this->fnSYSAddStyles($a_in_cache_object_css);
            $this->template->SYSTEM_A_MENU = self::fnSYSGenMenuArray();
            $this->template->SYSTEM_A_MENU_ACTIVE = $this->fnSYSGetArrayMenuActive();
            $this->template->SYSTEM_BREADCRUMBS = $this->fnSYSGetBreadcrumbs();
            $this->template->BTS_A_ACCESS = self::fnSYSGetSessionParameter(self::BTS_SESSION_ACTIONS_ACCESS);
            $this->template->BTS_A_SYSTEM_OPTIONS = self::fnSYSGetSessionParameter(self::BTS_SESSION_SYSTEM_OPTIONS);
            $this->template->BTS_USER_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
            $this->template->BTS_USER_FULL_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_FULL_NAME);
            $this->template->BTS_SESSION_USER_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_ID);
            $this->template->BTS_SESSION_OFFICE_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_ID);
            $this->template->BTS_SESSION_OFFICE_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_OFFICE_NAME);
            $this->template->BTS_SESSION_CITY_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_ID);
            $this->template->BTS_SESSION_CITY_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_CITY_NAME);
            $this->template->BTS_GROUP_NAME = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_NAME);
            $this->template->BTS_SESSION_GROUP_ID = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID);
            $this->template->BTS_SESSION_USER_EMAIL = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_EMAIL);
            $this->template->BTS_SESSION_DEFAULT_HOME = self::fnSYSGetSessionParameter(self::BTS_SESSION_DEFAULT_HOME);
            $this->template->BTS_OPTION_LANGUAGE = $this->fnSYSGetOptionValue(self::BTS_LANGUAGE);
            $this->template->ROOT_MIMIC_OPTION = (self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION) OR self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_MIMIC_OPTION));
            $this->template->USER_MIMIC_OPTION = self::fnSYSGetSessionParameter(self::BTS_SESSION_ROOT_OPTION_MIMIC);
            $this->template->b_BTS_CHAT =(bool) self::fnSYSGetOptionValue(self::BTS_SO_BTS_CHAT);
        }
    }

    public function action_index() {
        $cacheInstance = Cache::instance('apc');
        $v_request_url = $this->request->controller() . $this->request->action();
        $cacheKeyCss = SITE_DOMAIN . $v_request_url . 'css';
        $cacheKeyJs = SITE_DOMAIN . $v_request_url . 'js';
        $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
        $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        if ($incacheObjectCss === null) {
            $cacheInstance->set($cacheKeyCss, self::fnSYSGetFilePage('ADMIN', self::BTS_FILE_TYPE_JS));
            $cacheInstance->set($cacheKeyJs, self::fnSYSGetFilePage('ADMIN', self::BTS_FILE_TYPE_CSS));
            $incacheObjectCss = $cacheInstance->get($cacheKeyCss);
            $incacheObjectJs = $cacheInstance->get($cacheKeyJs);
        }
        $this->fnSYSAddScripts($incacheObjectCss);
        $this->fnSYSAddStyles($incacheObjectJs);
        $browser_version = HelperBrowser::getBrowser();
        $v_admin = View::factory('bts/private/admin/index');
        $this->template->SYSTEM_BODY = $v_admin;
        $v_admin->s_user_full_name = self::fnSYSGetSessionParameter(self::BTS_SESSION_USER_NAME);
        $v_admin->user_group_id = self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID);
        $v_admin->user_group_root = self::BTS_GROUP_ROOT;
        $v_admin->is_admin = (self::BTS_GROUP_ADMIN == self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID) OR self::BTS_GROUP_ROOT == self::fnSYSGetSessionParameter(self::BTS_SESSION_GROUP_ID));
        $v_admin->s_browser_name = $browser_version['browser_name'];
        $v_admin->s_browser_version = $browser_version['browser_version'];
    }

    public function action_cleaningCache() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $this->fnSYSCleaningAllCache();
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_cleaningLanguages() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $this->fnSYSCleaningLanguages();
        $this->fnSYSResponseFormat($a_response);
    }

    public function action_cleaningSessionsUsers() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $this->fnSYSCleaningSessionsUsers();
        $this->fnSYSResponseFormat($a_response);
    }
        public function action_activateDeactivatedSystem() {
        $this->auto_render = FALSE;
        $a_response = $this->a_bts_json_response;
        $activateDeactivated = $this->request->post('activateDeactivated');
        $this->fnSYSActivateDeactivatedSystem($activateDeactivated);
        $this->fnSYSResponseFormat($a_response);
    }

    public function getEnumValuesFromTable($tableName, $columName) {
        $db = DB::query(Database::SELECT, "SHOW COLUMNS FROM `$tableName` LIKE '$columName'")->as_object()->execute()->current();
        if ($db != NULL) {
            $raw_values = str_replace("enum(", "", $db->Type);
            $raw_values = str_replace(")", "", $raw_values);
            $raw_values = str_replace("'", "", $raw_values);
            $raw_values = explode(',', $raw_values);
            return $raw_values;
        } else {
            return array('NO_DATA');
        }
    }

    public function alterEnumValuesForTable($tableName, $columName, $values) {
        if ($columName == 'component') {
            $string_values = "GENERAL";
            $default_value = "GENERAL";
        } elseif ($columName == 'dataType') {
            $default_values = array('boolean', 'decimal', 'percentage', 'color', 'integer', 'string');
            if ($values[0] == '') {
                $values = $default_values;
            }
        }
        if (is_array($values) && $values[0] != '') {
            sort($values, SORT_STRING);
            $default_value = "'" . $values[0] . "'";
            $string_values = implode(',', $values);
            $string_values = "'" . str_replace(',', "','", $string_values) . "'";
        } else {
            $default_value = "'" . $string_values . "'";
            $string_values = $default_value;
        }
        DB::query(Database::UPDATE, "ALTER TABLE `$tableName` CHANGE `$columName` `$columName` enum($string_values) DEFAULT $default_value NOT NULL")->execute();
    }

    public function action_sendSupportEmail() {
        $this->auto_render = FALSE;
        try {
            $a_response = $this->a_bts_json_response;
            $CODE = $this->request->post('eCode');
            $DATE_TIME = $this->request->post('eTime');
            $DOMAIN = $this->request->post('eDomain');
            $USER = $this->request->post('eUserName');
            $TEXT = $this->request->post('eText');

            $emailArrayMsgData = array(
                ':CLIENT_DOMAIN' => $DOMAIN
                , ':CODE_ERROR' => $CODE
                , ':DATE_ERROR' => $DATE_TIME
                , ':USER_ERROR' => $USER
            );
            $subject = 'BUG ' . $CODE . ' ' . $DOMAIN;
            $emailFrom = 'noreply@angryuser.com';
            $nameFrom = 'Angry User';
            //$mailRecipient = 'wcumpa@ibusplus.com';
            $nameRecipient = 'Walter Cumpa';
            $emailView = "emails/bug_support_email";
            $mailRecipient = Kohana::$config->load('support_users.' . Kohana_BtsConstants::BTS_SUPORT_SYS_EMAILS);
            $config = Kohana::$config->load('support_users');
            $config->set('var', 'new_value');
            //$this->fnSYSSendEmail($subject, $mailRecipient, $nameRecipient, $emailView, $emailArrayMsgData, $emailFrom, $nameFrom);
        } catch (Exception $exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($exc);
            print_r($exc->getMessage());
        }
        $this->fnSYSResponseFormat($a_response);
    }
}