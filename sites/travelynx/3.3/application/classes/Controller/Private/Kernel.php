<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Private_Kernel extends Private_Kernel implements TravelynxConstants {

    public function getRoutesByParameters() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            $this->fnSYSCleaningAllCache();
            $a_form_post_data = $o_check_post->data();
            $o_model_route = new Model_Route();
            $t_route = array();
            $a_route = array();
            if ($a_form_post_data['filter']) {
                $a_route = $o_model_route->getRouteArrayById($a_form_post_data['idroute']);
            } else {
                $locdep = $serdep = $seddep = $locarr = $serarr = $sedarr = 0;
                if ($a_form_post_data['locdep'] != '')
                    $locdep = $a_form_post_data['locdep'];
                if ($a_form_post_data['serdep'] != '')
                    $serdep = $a_form_post_data['serdep'];
                if ($a_form_post_data['seddep'] != '')
                    $seddep = $a_form_post_data['seddep'];
                if ($a_form_post_data['locarr'] != '')
                    $locarr = $a_form_post_data['locarr'];
                if ($a_form_post_data['serarr'] != '')
                    $serarr = $a_form_post_data['serarr'];
                if ($a_form_post_data['sedarr'] != '')
                    $sedarr = $a_form_post_data['sedarr'];
                $a_route = $o_model_route->getRouteArray($locdep, $serdep, $seddep, $locarr, $serarr, $sedarr, 'sale', $a_form_post_data['privat']);
                if (count($a_route) == 0) {
                    $a_route = $o_model_route->getRouteArray($locdep, $serdep, 0, $locarr, 0, $sedarr, 'sale', $a_form_post_data['privat']);
                    if (count($a_route) == 0) {
                        $a_route = $o_model_route->getRouteArray($locdep, 0, 0, $locarr, 0, 0, 'sale', $a_form_post_data['privat']);
                    }
                }
            }
            foreach ($a_route as $value) {
                if ($value['alltime'] == 0) {
                    $value['route_hour'] = $o_model_route->listHourByRoute($value['route_id']);
                } else {
                    $value['route_hour'] = $value['route_id'];
                }
                array_push($t_route, $value);
            }
            $a_response['data'] = $t_route;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getLocationsDepartureByService() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('fsale_service', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'fsale_service':
                            $s_label_name = __("Servicio");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_Service = new Model_Service($a_form_post_data['fsale_service']);
            if (!$o_model_Service->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_route = new Model_Route();
            $a_response['data'] = $o_model_route->listArray($o_model_Service->idService, 0, 0, TRUE);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getLocationsArrivalByDeparture() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('fsale_service', 'not_empty')
                    ->rule('fsale_departure', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'fsale_service':
                            $s_label_name = __("Servicio");
                            break;
                        case 'fsale_departure':
                            $s_label_name = __("Salida");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();

            $o_model_Service = new Model_Service($a_form_post_data['fsale_service']);
            if (!$o_model_Service->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_location = new Model_Location($a_form_post_data['fsale_departure']);
            if (!$o_model_location->loaded())
                throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);

            $o_model_route = new Model_Route();
            $a_response['data'] = $o_model_route->listArray($o_model_Service->idService, $o_model_location->idLocation, 0, TRUE);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getPriceByRouteAndSchedule() {
        $a_response = $this->a_bts_json_response;
        $a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('i', 'not_empty')
                    ->rule('ih', 'not_empty')
                    ->rule('eh', 'not_empty');
            if (!$o_check_post->check()) {
                foreach ($o_check_post->errors() as $s_key => $a_item) {
                    switch ($s_key) {
                        case 'i':
                            $s_label_name = __("Ruta");
                            break;
                        case 'ih':
                            $s_label_name = __("Hora Inicio");
                            break;
                        case 'eh':
                            $s_label_name = __("Hora Final");
                            break;
                        default:
                            break;
                    }
                    switch ($a_item[0]) {
                        case 'not_empty':
                            $a_error_labels[] = $s_label_name . ' ' . __("requerido");
                            break;
                        default:
                            break;
                    }
                }
                throw new Exception("", self::BTS_CODE_SUCCESS);
            }

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $this->fnSYSCleaningAllCache();
            $o_model_price = new Model_Price();
            $a_response['data'] = $o_model_price->getPricesBySchedule($a_form_post_data, self::fnSYSGetOptionValue('MAXIMUM_NUMBER_PAX'));
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);

            if (!$o_check_post->check()) {
                $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = implode('<br/>', $a_error_labels);
            }
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getTempSalesCart() {
        $a_response = $this->a_bts_json_response;
        //$a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idTsc', 'not_empty');

            //PARTE VALIDA
            $this->fnSYSCleaningAllCache();
            $a_form_post_data = $o_check_post->data();
            $o_temp = new Model_Tempsalescart();

            $a_response['data'] = $o_temp->getTempSalesCartById($a_form_post_data['idTsc']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getHotelParams() {
        $a_response = $this->a_bts_json_response;
        //$a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idloc', 'not_empty');

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_service = new Model_Service();
            $a_response['data'] = $o_service->getHotelServiceById($a_form_post_data);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function saveTempSalesCart() {
        $a_response = $this->a_bts_json_response;
        //$a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idr', 'not_empty')
                    ->rule('locdep', 'not_empty')
                    ->rule('locarr', 'not_empty');

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $sessionKey = $this->fnSYSGetSessionParameter(self::BTS_SESSION_ID);
            $o_temp = new Model_Tempsalescart();
            $a_response['data'] = $o_temp->saveArray($a_form_post_data, $sessionKey);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function saveSales($websale) {
        $a_response = $this->a_bts_json_response;
        $methodPayment = $this->request->post('methodPayment');
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idtsc', 'not_empty');

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            Database::instance()->begin();
            $responseSale = null;
            $idPaymentLog = null;
            if ($methodPayment == 'paypal') {
                $hAmount = $a_form_post_data['txt_total_amounth'];
                $amount = $a_form_post_data['txt_total_amount'];
                if ($amount == $hAmount) {
                    //PAYPAL DATA
                    $cardNumber = str_replace(" ", "", $a_form_post_data['txt_creditCardNumber']);
                    $cardExpYear = $a_form_post_data['cmb_expiration_year'];
                    $cardExpMonth = $a_form_post_data['cmb_expiration_month'];
                    $cardCvv = $a_form_post_data['txt_cvv2Number'];
                    $userId = '';
                    $itemId = '';
                    $billingFname = $a_form_post_data['txt_firstName'];
                    $billingLname = $a_form_post_data['txt_lastName'];
                    $billingState = $a_form_post_data['cmb_state'];
                    $billingCity = $a_form_post_data['txt_city'];
                    $billingAddress = $a_form_post_data['txt_address1'];
                    $billingCountry = $a_form_post_data['cmb_country'];
                    $billingZip = $a_form_post_data['txt_zip'];
                    $creditCardType = $a_form_post_data['cmb_type_card'];
                    $billingCompany = 'Travelynx.com';
                    $billingEmail = $a_form_post_data['txt_email'];
                    $customHtml = __('Sale Type: Shuttle Service ');
                    $customHtml .= __(' Sale From: Terminal ');
                    if ($websale) {
                        $customHtml .= __(' Sale From: Web');
                    }
                    $responseSale = $this->fnSYSPaypalInstantPayment($cardNumber, $cardExpYear, $cardExpMonth, $amount, $cardCvv, $userId, $itemId, $billingFname, $billingLname, $billingState, $billingCity, $billingAddress, $billingCountry, $billingZip, $billingCompany, $billingEmail, $customHtml, null, self::PAYMENT_CREDIT_CARD_TICKET, null, null, $creditCardType);
                    $idPaymentLog = self::fnSYSGetSessionParameter('log_id');
                } else {
                    throw new Exception(__('Monto Abonado con la tarjeta no coincide con el monto a cobrar'), self::CODE_SUCCESS);
                }
            }
            $txt_phone1 = null;
            $txt_phone2 = null;
            if (isset($a_form_post_data['txt_phone1'])) {
                $txt_phone1 = $a_form_post_data['txt_phone1'];
            }
            if (isset($a_form_post_data['txt_phone2'])) {
                $txt_phone2 = $a_form_post_data['txt_phone2'];
            }
            $model_shopping_cart = new Model_Shoppingcart();
            $idSc = $model_shopping_cart->saveArray($a_form_post_data['txt_email'], $a_form_post_data['txt_total_amount'], HelperText::generateAlphanumCode('SC'), $websale, $methodPayment, $idPaymentLog, $txt_phone1, $txt_phone2, $a_form_post_data['formod']['text_observations']);
            foreach ($a_form_post_data['idtsc'] as $value) {
                $o_temp = new Model_Tempsalescart();
                $arrayTsc = $o_temp->getArray($value);
                $arrayOd = array();
                foreach ($a_form_post_data['formod'] as $keyx => $valuex) {
                    if ($keyx != 'text_observations') {
                        list($f, $h) = explode('_', $keyx);
                        if ($value == $h) {
                            if (!empty($valuex)) {
                                $arrayOd[$f] = $valuex;
                            }
                        }
                    }
                }
                $o_sale = new Model_Sale();
                $codeSale = HelperText::generateAlphanumCode('T');
                $id_sale = $o_sale->saveArray($arrayTsc, $idSc, $codeSale, $arrayOd, $a_form_post_data['optalltraveler']);
                if (!is_null($arrayTsc['numpaxOw']) AND $arrayTsc['numpaxOw'] > 0) {
                    $this->saveTraveler($arrayTsc['numpaxOw'], $a_form_post_data['optalltraveler'], $a_form_post_data['formtd'], $value, $id_sale->idSale, 'oneway', 1);
                }
                if (!is_null($arrayTsc['numpaxRt']) AND $arrayTsc['numpaxRt'] > 0) {
                    $this->saveTraveler($arrayTsc['numpaxRt'], $a_form_post_data['optalltraveler'], $a_form_post_data['formtd'], $value, $id_sale->idSale, 'roundtrip', 2);
                }
                $o_temp->deleteTempSalesCart($value);
            }
            Database::instance()->commit();
            if ($methodPayment == 'paypal' AND self::fnSYSGetSessionParameter('log_id')) {
                self::fnSYSDeleteVariableSession('log_id');
            } elseif ($methodPayment == 'paypal' AND self::fnSYSGetSessionParameter('paypalLogID')) {
                self::fnSYSDeleteVariableSession('paypalLogID');
            }
            $a_result['email'] = $a_form_post_data['txt_email'];
            $a_result['id'] = $id_sale->idShoppingCart;
            $a_response['data'] = $a_result;
        } catch (Exception $e_exc) {
            if ($methodPayment == 'paypal' AND self::fnSYSGetSessionParameter('log_id')) {
                $paypalRefund = new Model_Paymentpaypallog(self::fnSYSGetSessionParameter('log_id'));
                if (!$paypalRefund->loaded())
                    throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
                $this->fnSYSPaypalInstantCancel($paypalRefund->txn_id);
            } elseif ($methodPayment == 'paypal' AND self::fnSYSGetSessionParameter('paypalLogID')) {
                $paypalRefund = new Model_Paymentpaypallog(self::fnSYSGetSessionParameter('paypalLogID'));
                if (!$paypalRefund->loaded())
                    throw new Exception(__('Registro no encontrado'), self::BTS_CODE_SUCCESS);
                $this->fnSYSPaypalInstantCancel($paypalRefund->txn_id);
            }
            Database::instance()->rollback();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function saveTraveler($pax, $opttrav, $form, $value, $idsale, $label, $x) {
        $arrayTraveler = array();
        if ($opttrav) {
            for ($t = 1; $t <= $pax; $t++) {
                foreach ($form as $key => $td) {
                    list($a, $b, $c, $d) = explode('_', $key);
                    if ($t == $a AND $value == $b AND $c == $x) {
                        $arrayTraveler[$d] = $td;
                    }
                }
                $m_traveler = new Model_Traveler();
                $m_traveler_sale = new Model_Travelersale();
                $idTraveler = $m_traveler->saveArray($arrayTraveler);
                $m_traveler_sale->saveArray($idTraveler, $idsale, $label);
            }
        } else {
            foreach ($form as $key => $td) {
                list($a, $b) = explode('_', $key);
                $arrayTraveler[$b] = $td;
            }
            $m_traveler = new Model_Traveler();
            $m_traveler_sale = new Model_Travelersale();
            $idTraveler = $m_traveler->saveArray($arrayTraveler);
            $m_traveler_sale->saveArray($idTraveler, $idsale, $label);
        }
    }

    public function deleteTempSalesCart() {
        $a_response = $this->a_bts_json_response;
        //$a_error_labels = array();
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('idtsc', 'not_empty');

            //PARTE VALIDA
            $a_form_post_data = $o_check_post->data();
            $o_temp = new Model_Tempsalescart();
            $a_response['data'] = $o_temp->deleteTempSalesCart($a_form_post_data['idtsc']);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = self::BTS_CODE_ERROR;
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getValidateZipcode() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('zipcode', 'not_empty');
            $a_form_post_data = $o_check_post->data();
            $o_model_zipcode = new Model_Zipcode();
            $zc = $o_model_zipcode->getZipcode($a_form_post_data['zipcode']);
            if ($zc['total'] >= 1) {
                $o_model_route = new Model_Route();
                $a_response['locarr'] = $o_model_route->getArrivalByDeparture($zc['idlocation']);
            }
            $a_response['data'] = $zc;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function printTickets() {
        $id_sc = $this->request->query('sc'); // id del shopping Cart;
        $se = $this->request->query('se'); // 1= enviar email;
        $soe = $this->request->query('soe'); // 1= enviar solo email;
        $email = $this->request->query('email');
        //$this->fnSYSCleaningAllCache();
        $o_sc = new Model_Shoppingcart($id_sc);
        $model_shoppingcart = new Model_Shoppingcart();
        $model_traveler = new Model_Traveler();
        if ($o_sc->loaded()) {
            if ($soe == 0) {
                $o_sc->numberPrint = (int) $o_sc->numberPrint + 1;
                $skin = $this->fnSYSGetConfig(self::BTS_SYS_CONFIG_SKIN);
                $this->fnSYSPrintDocument(
                        self::DOC_TYPE_TICKET, array(
                    'ticketdetail' => $model_traveler->getDataForTicketDetail($id_sc)
                    , "contact" => $model_shoppingcart->getDatesToEmailSend($id_sc)
                    , 'skin' => $skin
                        )
                );
            }
            //mandar email;            
            if ($se == 1) {
                if (HelperText::checkEmail($email) == false) {
                    throw new Exception(__("El Email ingresado no es valido"), self::BTS_CODE_SUCCESS);
                } else {
                    global $a_db_config;
                    $opt_email = $this->getEmailForConfirmation(array($this->request->query('email')), self::OPTION_SEND_EMAIL_ALL_SALES);
                    if ($o_sc->websale) {
                        $opt_email = $this->getEmailForConfirmation($opt_email, self::OPTION_SEND_EMAIL_WEBSALE);
                    };
                    $email = $opt_email;
                    $subject = __('Confirmación de Compra en Travelynx.com');
                    $nameRecipient = $email;
                    $emailView = 'mail/mail_sendconfirmationTickets';
                    $imgHeader = 'http://travelynx/img/travelynx/print/footer_print_ticket.png';
                    $Msjcompany = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_COMPANY];
                    $MsjAddress = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_ADDRESS];
                    $MsjWebsite = $a_db_config[self::BTS_SYS_CONFIG_WEBSITE];
                    $MsjPhone = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_PHONE];
                    $emailArrayMsgData = array(
                        ':code' => $o_sc->code,
                        ':totalAmount' => $o_sc->totalAmount,
                        ':contactCompany' => $Msjcompany,
                        ':contactAddress' => $MsjAddress,
                        ':contactPhone' => $MsjPhone,
                        ':website' => $MsjWebsite,
                        ':imgHeader' => $imgHeader
                    );
                    $emailArrayMsgDataObj = array(
                        "contact" => $model_shoppingcart->getDatesToEmailSend($id_sc),
                        "ticketdetail" => $model_traveler->getDataForTicketDetail($id_sc),
                    );
                    $emailFrom = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM];
                    $nameFrom = $a_db_config[self::BTS_SYS_CONFIG_CONTACT_EMAIL_FROM_NAME];
                    $this->fnSYSSendEmail($subject, $email, $nameRecipient, $emailView, $emailArrayMsgDataObj, $emailArrayMsgData, $emailFrom, $nameFrom);
                }
            }
            $o_sc->update();
        } else {
            throw new Exception(__("No se ha podido realizar la solicitud, Por favor realizar nuevamente el proceso."), self::BTS_CODE_SUCCESS);
        }
    }

    public function getEmailForConfirmation($opt_email, $const) {
        $opt = ORM::factory('Bts_Option')->where('key', '=', $const)->find();
        if ($opt->loaded()) {
            $a_email = explode(',', $opt->value);
            foreach ($a_email as $value) {
                array_push($opt_email, trim($value));
            }
        }
        return $opt_email;
    }

    public function deleteMyShoppingCart() {
        $a_response = $this->a_bts_json_response;
        try {
            $sessionKey = self::fnSYSGetSessionParameter(self::BTS_SESSION_ID);
            $tsc = new Model_Tempsalescart();
            $maxTimeSelectTicked = (int) self::fnSYSGetOptionValue('MAX_TIME_TEMP_SHOPPING_CART') * 60;
            $date = HelperDate::fnGetMysqlCurrentDateTime();
            $tsc->deleteMyAllTempSalesCart($sessionKey, $date, $maxTimeSelectTicked);
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getScheduleByRoute() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('i', 'not_empty');
            $a_form_post_data = $o_check_post->data();
            $o_model_price = new Model_Price();
            $schd = $o_model_price->getScheduleByRoute($a_form_post_data);
            $a_response['data'] = $schd;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

    public function getUserPaypal() {
        $a_response = $this->a_bts_json_response;
        $o_check_post = Validation::factory($this->request->post());
        try {
            $o_check_post
                    ->rule('th', 'not_empty');
            $a_form_post_data = $o_check_post->data();

            $a_parameter = array(
                array('se.idSede', 'idsede')
                , array('se.name', 'namesede')
            );
            $a_result = DB::select_array($a_parameter)
                            ->from(array('tra_sede', 'se'))
                            ->where('se.name', 'LIKE', '%' . $a_form_post_data['th'] . '%')
                            ->and_where('se.status', '=', self::BTS_STATUS_ACTIVE)
                            ->execute()->as_array();

//            $schd = $o_model_price->getScheduleByRoute($a_form_post_data);
            $a_response['data'] = $a_result;
        } catch (Exception $e_exc) {
            $a_response[self::BTS_RESPONSE_TYPE_JSON_CODE] = $e_exc->getCode();
            $a_response[self::BTS_RESPONSE_TYPE_JSON_MSG] = $this->fnSYSErrorHandling($e_exc);
        }
        return $this->fnSYSResponseFormat($a_response);
    }

}
