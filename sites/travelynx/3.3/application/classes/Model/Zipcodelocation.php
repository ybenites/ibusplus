<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zipcodelocation
 *
 * @author BENITES
 */
class Model_Zipcodelocation extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_zipcodelocation';
    protected $_primary_key = 'idZipCodeLocation';
    
    protected $_belongs_to = array(
        'location' => array('model' => 'Location','foreign_key' => 'idLocation'),
        'zipcode' => array('model' => 'Zipcode','foreign_key' => 'idZipCode')
    );  
    
    public function saveArray($a_params) {        
        $this->idZipCode = $a_params['idZipCode'];
        $this->idLocation = $a_params['idLocation'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }
    public function listArray($idLocation) {
        $a_parameters = array(
            array('zil.idZipCodeLocation', 'zipcode_id')
            , array('zi.number', 'zipcode_number')
        );
        $a_result = DB::select_array(
                        $a_parameters
                )->from(array('tra_zipcodelocation', 'zil'))
                ->join(array('tra_zipcode','zi'))->on('zil.idZipCode','=','zi.idZipCode')
                ->where('zil.idLocation','=',$idLocation)
                ->and_where('zi.status', '=', self::BTS_STATUS_ACTIVE);        
        return $a_result->execute()->as_array();
    }
}