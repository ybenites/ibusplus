<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Hour extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_hour';
    protected $_primary_key = 'idHour';

    public function updateStatus($sts) {
        $this->status = $sts;
        parent::save();
    }

}