<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Traveler extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_traveler';
    protected $_primary_key = 'idTraveler';

    public function saveArray($a_params) {
        $this->title = $a_params['title'];
        $this->firstname = $a_params['firstname'];
        $this->middlename = $a_params['middlename'];
        $this->lastname = $a_params['lastname'];
        $this->dob = $a_params['dob'];
        $this->gender = $a_params['gender'];
        parent::save();
        return $this->idTraveler;
    }

    public function getDataForTicketDetail($idtsc) {
        $a_parameters = array(
            array(DB::expr("CONCAT(tv.`firstname`, ' ', tv.`lastname`)"), 'nametraveler')
            , array(DB::expr("DATE_FORMAT(sa.`travelDateOw`,'%m-%d-%Y')"), 'dateow')
            , array(DB::expr("IF(`ser`.`name` IS NOT NULL, `ser`.`name`, '--')"), 'service')
            , array(DB::expr("CONCAT('$ ', sa.unitPriceOw, '.00')"), 'price')
            , array(DB::expr("IF(sa.`addressLocation` IS NULL, sed.`name`, sa.`addressLocation`)"), 'departure')
            , array(DB::expr("IF(sa.`addressArrival` IS NULL, sea.`name`, sa.`addressArrival`)"), 'arrival')
            , array('sa.numpaxOw', 'numpaxow')
            , array('air.name', 'airname')
            , array('sa.flightNumberDeparture', 'airnumber')
            , array(DB::expr("DATE_FORMAT(sa.flightTimeDeparture, '%r')"), 'airtime')
            , array('sa.optAllTravelers', 'opttra')
            , array(DB::expr("'NORMAL'"), 'faretype')
        );
        $a_result = DB::select_array($a_parameters)
                ->from(array('tra_travelersale', 'ts'))
                ->join(array('tra_sale', 'sa'))->on('ts.idSale', '=', 'sa.idSale')
                ->join(array('tra_traveler', 'tv'))->on('ts.idTraveler', '=', 'tv.idTraveler')
                ->join(array('tra_sede', 'sed'), 'LEFT')->on('sa.idSedeDepartureSelected', '=', 'sed.idSede')
                ->join(array('tra_sede', 'sea'), 'LEFT')->on('sa.idSedeArrivalSelected', '=', 'sea.idSede')
                ->join(array('tra_service', 'ser'), 'LEFT')->on('sea.idService', '=', 'ser.idService')
                ->join(array('tra_airlines', 'air'), 'LEFT')->on('sa.idAirlineDeparture', '=', 'air.idAirlines')
                ->where('sa.idShoppingCart', '=', $idtsc);

        return $a_result->execute()->as_array();
    }

}