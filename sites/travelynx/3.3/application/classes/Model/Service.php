<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author BENITES
 */
class Model_Service extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_service';
    protected $_primary_key = 'idService';

    public function saveArray($a_params) {
        $this->name = $a_params['fservice_name'];
        $this->description = $a_params['fservice_description'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }

    public function listArray() {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_parameter = array(
                array('se.idService', 'service_id')
                , array('se.name', 'service_name')
                , array('se.description', 'service_description')
            );

            $a_result = DB::select_array(
                                    $a_parameter
                            )
                            ->from(array('tra_service', 'se'))
                            ->where('se.status', '=', self::BTS_STATUS_ACTIVE)
                            ->execute()->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getHotelServiceById($param) {
        $a_parameter = array(
            array('sed.idSede', 'sede_id')
            , array('sed.name', 'sede_name')
            , array('sed.address', 'sede_address')
            , array('ser.name', 'service_name')
        );

        $a_result = DB::select_array($a_parameter)
                ->from(array('tra_sede', 'sed'))
                ->join(array('tra_zipcode', 'zc'))->on('sed.idZipCode', '=', 'zc.idZipCode')
                ->join(array('tra_location', 'lo'))->on('zc.idLocation', '=', 'lo.idLocation')
                ->join(array('tra_service', 'ser'))->on('sed.idService', '=', 'ser.idService')
                ->where('sed.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('zc.idLocation', '=', $param['idloc']);
        if ($param['idser'] > 0 AND !is_null($param['idser']))
            $a_result->and_where('sed.idService', '=', $param['idser']);
        if ($param['idsed'] > 0 AND !is_null($param['idsed']))
            $a_result->and_where('sed.idSede', 'IN', DB::expr("(" . $param['idsed'] . ")"));

        return $a_result->execute()->as_array();
    }

}