<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Price extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_price';
    protected $_primary_key = 'idPrice';

    public function checkPrice($r, $array) {
        return DB::select(array(DB::expr('COUNT(*)'), 'count'))
                        ->from(array('tra_price', 'pr'))
                        ->where('idRate', '=', $r)
                        ->and_where('numPax', '=', $array['i'])
                        ->execute()->current();
    }

    public function saveArray($r, $array) {
        $this->idRate = $r;
        $this->numPax = $array['i'];
        $this->price = $array['v'];
        parent::save();
    }

    public function updatePrice($r, $array) {
        DB::update('tra_price')
                ->set(array('price' => $array['v']))
                ->where('idRate', '=', $r)
                ->and_where('numPax', '=', $array['i'])
                ->execute();
    }

    public function getScheduleByRoute($a_param) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__ . $a_param['i'];
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_result = DB::select(
                            array('ra.idRate', 'idrate')
                            , array(DB::expr('CONCAT(`startTime`, `endTime`)'), 'idr')
                            , array(DB::expr("IF(ra.alltime = 0, CONCAT(ih.`description`,' - ',eh.`description`), 'All Time')"), 'schd')
                    )
                    ->from(array('tra_rate', 'ra'))
                    ->join(array('tra_hour', 'ih'), 'LEFT')->on('ra.startTime', '=', 'ih.idHour')
                    ->join(array('tra_hour', 'eh'), 'LEFT')->on('ra.endTime', '=', 'eh.idHour')
                    ->where('ra.idRoute', '=', $a_param['i'])
                    ->and_where('ra.status', '=', self::BTS_STATUS_ACTIVE)
                    ->execute()
                    ->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getPricesByRoute($a_param) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__ . $a_param['i'];
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_result = DB::select(
                            array('ra.idRate', 'idrate')
                            , array('ra.startTime', 'st')
                            , array('ra.endTime', 'et')
                            , array(DB::expr('CONCAT(`startTime`, `endTime`, `numPax`)'), 'idr')
                            , array('pr.price', 'price'))
                    ->from(array('tra_price', 'pr'))
                    ->join(array('tra_rate', 'ra'))->on('pr.idRate', '=', 'ra.idRate')
                    ->where('ra.idRoute', '=', $a_param['i'])
                    ->and_where('ra.status', '=', self::BTS_STATUS_ACTIVE)
                    ->order_by('ra.startTime')
                    ->order_by('ra.endTime')
                    ->order_by('pr.numPax')
                    ->execute()
                    ->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getPricesBySchedule($a_param, $numpax) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__ . $a_param['i'] . $a_param['ih'] . $a_param['eh'];
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_result = DB::select(
                            array('ra.idRate', 'idrate')
                            , array('ro.idRoute', 'idroute')
                            , array('ro.description', 'description')
                            , array(DB::expr('CONCAT(ra.`startTime`, ra.`endTime`, ra.`idRoute`, pr.`numPax`)'), 'rate')
                            , array('pr.price', 'price')
                            , array('pr.numPax', 'numpax')
                    )
                    ->from(array('tra_price', 'pr'))
                    ->join(array('tra_rate', 'ra'))->on('pr.idRate', '=', 'ra.idRate')
                    ->join(array('tra_route', 'ro'))->on('ra.idRoute', '=', 'ro.idRoute')
                    ->where('ra.idRoute', '=', $a_param['i'])
                    ->and_where('ra.startTime', '=', $a_param['ih'])
                    ->and_where('ra.endTime', '=', $a_param['eh'])
                    ->and_where('ra.status', '=', self::BTS_STATUS_ACTIVE)
                    ->order_by('pr.numPax')
                    ->limit($numpax)
                    ->execute()
                    ->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

}