<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Travelersale extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_travelersale';
    protected $_primary_key = 'idTravelerSale';

    public function saveArray($idtrav, $idsale, $type) {
        $this->idTraveler = $idtrav;
        $this->idSale = $idsale;
        $this->type = $type;
        parent::save();
    }

}