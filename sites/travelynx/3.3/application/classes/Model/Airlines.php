<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of airlines
 *
 * @author BENITES
 */
defined('SYSPATH') or die('No direct script access.');

class Model_Airlines extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_airlines';
    protected $_primary_key = 'idAirlines';

    public function saveArray($a_params) {
        $this->name = $a_params['fairlines_name'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }

    public function getArray() {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class().__METHOD__;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
        $a_result = DB::select(array('air.idAirlines', 'idairline'), array('air.name', 'name'))
                ->from(array('tra_airlines', 'air'))
                ->where('air.status', '=', self::BTS_STATUS_ACTIVE);
        return $a_result->execute()->as_array();
             $o_cacheInstance->set($s_cacheKey, $a_result);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

}
