<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of services
 *
 * @author BENITES
 */
class Model_Location extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_location';
    protected $_primary_key = 'idLocation';

    public function saveArray($a_params) {
        $this->name = $a_params['flocation_name'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }

    public function listArray($param_location_departure = 0) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class().__METHOD__.$param_location_departure;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
        $a_parameter = array(
            array('lo.idLocation', 'location_id')
            , array('lo.name', 'location_name')
        );

        $a_result = DB::select_array(
                                $a_parameter
                        )
                        ->from(array('tra_location', 'lo'))
                        ->where('lo.status', '=', self::BTS_STATUS_ACTIVE)
                        ->and_where('lo.idLocation', '<>', $param_location_departure)
                        ->execute()->as_array();
          $o_cacheInstance->set($s_cacheKey, $a_result);
          $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getAllLocation() {        
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class().__METHOD__;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
        $a_parameter = array(
            array('lo.idLocation', 'idloc')
            , array('lo.name', 'nameloc')
        );
        $a_result = DB::select_array($a_parameter)
                        ->from(array('tra_location', 'lo'))
                        ->where('lo.status', '=', self::BTS_STATUS_ACTIVE)
                        ->execute()->as_array();
                $o_cacheInstance->set($s_cacheKey, $a_result);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

}