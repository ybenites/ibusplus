<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sale extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_sale';
    protected $_primary_key = 'idSale';

    public function saveArray($a_params, $idSc, $codeSale, $arrayOd, $optTrav) {
        $this->idShoppingCart = $idSc;
        $this->idRoute = $a_params['idRoute'];
        $this->travelTypeSelected = $a_params['travelTypeSelected'];
        $this->travelDateOw = $a_params['travelDateOw'];
        $this->idRateOw = $a_params['idRateOw'];
        $this->numpaxOw = $a_params['numpaxOw'];
        $this->unitPriceOw = $a_params['unitPriceOw'];
        $this->totalPriceOw = $a_params['totalPriceOw'];
        $this->travelDateRt = $a_params['travelDateRt'];
        $this->idRateRt = $a_params['idRateRt'];
        $this->numpaxRt = $a_params['numpaxRt'];
        $this->unitPriceRt = $a_params['unitPriceRt'];
        $this->totalPriceRt = $a_params['totalPriceRt'];
        $this->totalPrice = $a_params['totalPrice'];
        $this->addressLocation = $a_params['addressLocation'];
        $this->addressArrival = $a_params['addressArrival'];
        $this->status = self::BTS_STATUS_ACTIVE;
        $this->optAllTravelers = $optTrav;
        if (isset($arrayOd['idSedeDepartureSelected']))
            $this->idSedeDepartureSelected = $arrayOd['idSedeDepartureSelected'];
        if (isset($arrayOd['idSedeArrivalSelected']))
            $this->idSedeArrivalSelected = $arrayOd['idSedeArrivalSelected'];
        if (isset($arrayOd['idAirlineDeparture']))
            $this->idAirlineDeparture = $arrayOd['idAirlineDeparture'];
        if (isset($arrayOd['flightNumberDeparture']))
            $this->flightNumberDeparture = $arrayOd['flightNumberDeparture'];
        if (isset($arrayOd['hourairportdeparture']) AND isset($arrayOd['minutesairportdeparture'])) {
            $hour_dep = ($arrayOd['formatairportdeparture'] == 'AM') ? $arrayOd['hourairportdeparture'] : $arrayOd['hourairportdeparture'] * 1 + 12;
            $this->flightTimeDeparture = $hour_dep . ':' . $arrayOd['minutesairportdeparture'];
        }
        if (isset($arrayOd['idAirlineArrival']))
            $this->idAirlineArrival = $arrayOd['idAirlineArrival'];
        if (isset($arrayOd['flightNumberArrival']))
            $this->flightNumberArrival = $arrayOd['flightNumberArrival'];
        if (isset($arrayOd['hourairportarrival']) AND isset($arrayOd['minutesairportarrival'])) {
            $hour_arr = ($arrayOd['formatairportarrival'] == 'AM') ? $arrayOd['hourairportarrival'] : $arrayOd['hourairportarrival'] * 1 + 12;
            $this->flightTimeArrival = $hour_arr . ':' . $arrayOd['minutesairportarrival'];
        }
        if (isset($arrayOd['hourow']) AND isset($arrayOd['minutesow']))
            $this->timeSelectedOw = $arrayOd['hourow'] . ':' . $arrayOd['minutesow'];
        if (isset($arrayOd['hourrt']) AND isset($arrayOd['minutesrt']))
            $this->timeSelectedRt = $arrayOd['hourrt'] . ':' . $arrayOd['minutesrt'];
        $this->codeSale = $codeSale;
        parent::save();

        return $this;
    }

    public function fnGetAllByShoppingCart($id) {
        $a_result = DB::select()->from(array('tra_sale', 'ts'))
                        ->and_where('ts.idShoppingCart', '=', $id)
                        ->execute()->as_array();
        if (empty($a_result)) {
            throw new Exception(__("No se ha podido cargar la información, Valores no encontrados."), self::BTS_CODE_SUCCESS);
        }
        return $a_result;
    }

}
