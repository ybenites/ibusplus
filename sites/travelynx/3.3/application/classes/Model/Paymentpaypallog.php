<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Paymentpaypallog extends ORM {

    protected $_table_names_plural = false;
    protected $_table_name = 'bts_payment_logs';
    protected $_primary_key = 'id';    
}