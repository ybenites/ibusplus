<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rate extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_rate';
    protected $_primary_key = 'idRate';

    public function saveArray($a_params) {
        $this->idRoute = $a_params['i'] * 1;
        if ($a_params['ih'] == 0 and $a_params['eh'] == 0)
            $this->alltime = 1;
        $this->startTime = $a_params['ih'];
        $this->endTime = $a_params['eh'];
        parent::save();
        return $this->idRate;
    }

    public function deleteRate($id) {
        DB::query(Database::DELETE, 'DELETE FROM tra_rate WHERE idRate = ' . $id)->execute();
    }

    public function updateStatusRate() {
        $this->status = 0;
        parent::save();
    }

}