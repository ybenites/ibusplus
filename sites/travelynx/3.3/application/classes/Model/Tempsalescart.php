<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Tempsalescart extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_tempsalescart';
    protected $_primary_key = 'idTempSalesCart';

    public function getArray($idtsc) {
        $a_result = DB::select('*')
                ->from(array('tra_tempsalescart', 'tsc'))
                ->where('tsc.idTempSalesCart', '=', $idtsc);
        return $a_result->execute()->current();
    }

    public function getTempSalesCartById($idtsc) {

        $params = DB::expr("
  CONCAT(
    IF(
      ro.idSedeDeparture IS NULL 
      OR ro.idSedeDeparture = 0,
      IF(addressLocation IS NULL, lod.`name`, CONCAT(addressLocation, ' (',lod.`name`,')')),
      IF(addressLocation IS NULL, sed.`name`, CONCAT(addressLocation, ' (',sed.`name`,')'))   
    ),
    ' - ',
    IF(
      ro.idSedeArrival IS NULL 
      OR ro.idSedeArrival = 0,
      IF(addressArrival IS NULL, loa.`name`, CONCAT(addressArrival, ' (',loa.`name`,')')),
      (SELECT 
        GROUP_CONCAT(IF(addressArrival IS NULL, sea.`name`, CONCAT(addressArrival, ' (',sea.`name`,')'))) 
      FROM
        tra_sede sea 
      WHERE sea.status = 1 
        AND SUBSTRING_INDEX(
          ro.idSedeArrival,
          ',',
          FIND_IN_SET(sea.idSede, ro.idSedeArrival)
        ))
    )
  )            
");

        $a_parameters = array(
            array('ro.idRoute', 'route_id')
            , array('tsc.travelTypeSelected', 'type')
            , array('ro.idLocationDeparture', 'idlocdep')
            , array('ro.idServiceDeparture', 'idserdep')
            , array('ro.idSedeDeparture', 'idseddep')
            , array('ro.idLocationArrival', 'idlocarr')
            , array('ro.idServiceArrival', 'idserarr')
            , array('ro.idSedeArrival', 'idsedarr')
            , array('ro.private', 'privat')
            , array('tsc.addressLocation', 'addLocation')
            , array('tsc.addressArrival', 'addArrival')
            , array('tsc.numpaxRt', 'prt')
            , array('tsc.numpaxOw', 'pow')
            , array('tsc.idSedeDepartureSearch', 'idseddepSearch')
            , array('tsc.idSedeArrivalSearch', 'idsedarrSearch')
            , array($params, 'route_name')
            , array('ro.description', 'route_description')
            , array('raow.startTime', 'stOw')
            , array('raow.endTime', 'etOw')
            , array('rart.startTime', 'stRt')
            , array('rart.endTime', 'etRt')
            , array(DB::expr("CONCAT(IF(LENGTH(`raow`.`startTime`) = 1, CONCAT('0', `raow`.`startTime`), `raow`.`startTime`), ':', '00')"), 'timeStOw')
            , array(DB::expr("CONCAT(IF(LENGTH(`raow`.`endTime`) = 1, CONCAT('0', `raow`.`endTime`), `raow`.`endTime`), ':', '00')"), 'timeEtOw')
            , array(DB::expr("CONCAT(IF(LENGTH(`rart`.`startTime`) = 1, CONCAT('0', `rart`.`startTime`), `rart`.`startTime`), ':', '00')"), 'timeStRt')
            , array(DB::expr("CONCAT(IF(LENGTH(`rart`.`endTime`) = 1, CONCAT('0', `rart`.`endTime`), `rart`.`endTime`), ':', '00')"), 'timeEtRt')
            , array(DB::expr("IF(raow.`alltime` IS NULL, 0, raow.`alltime`)"), 'allTimeOw')
            , array(DB::expr("IF(rart.`alltime` IS NULL, 0, rart.`alltime`)"), 'allTimeRt')
        );
        $a_result = DB::select_array($a_parameters)
                ->from(array('tra_tempsalescart', 'tsc'))
                ->join(array('tra_route', 'ro'))->on('ro.idRoute', '=', 'tsc.idRoute')
                ->join(array('tra_location', 'lod'))->on('ro.idLocationDeparture', '=', 'lod.idLocation')
                ->join(array('tra_location', 'loa'))->on('ro.idLocationArrival', '=', 'loa.idLocation')
                ->join(array('tra_sede', 'sed'), 'LEFT')->on('ro.idSedeDeparture', '=', 'sed.idSede')
                ->join(array('tra_sede', 'sea'), 'LEFT')->on('ro.idSedeArrival', '=', 'sea.idSede')
                ->join(array('tra_rate', 'raow'), 'LEFT')->on('raow.idRate', '=', 'tsc.idrateOw')
                ->join(array('tra_rate', 'rart'), 'LEFT')->on('rart.idRate', '=', 'tsc.idRateRt')
                ->where('tsc.idTempSalesCart', '=', $idtsc);

        return $a_result->execute()->as_array();
    }

    public function saveArray($a_params, $sessionKey) {
        $this->idRoute = $a_params['idro'];
        $this->travelTypeSelected = $a_params['type'];
        $this->travelDateOw = $a_params['ldow'];
        $this->idRateOw = $a_params['idraow'];
        $this->unitPriceOw = $a_params['upow'];
        $this->totalPriceOw = $a_params['tpow'];
        $this->numpaxOw = $a_params['paxow'];
        if ($a_params['type'] == 'roundtrip') {
            $this->travelDateRt = $a_params['ldrt'];
            $this->idRateRt = $a_params['idrart'];
            $this->unitPriceRt = $a_params['uprt'];
            $this->totalPriceRt = $a_params['tprt'];
            $this->numpaxRt = $a_params['paxrt'];
        }
        $this->totalPrice = $a_params['tp'];
        $this->status = self::BTS_STATUS_ACTIVE;
        $this->sessionKey = $sessionKey;
        if ($a_params['addloc'] != "")
            $this->addressLocation = $a_params['addloc'];
        if ($a_params['addarr'] != "")
            $this->addressArrival = $a_params['addarr'];
        if ($a_params['seddepsearch'] != "")
            $this->idSedeDepartureSearch = $a_params['seddepsearch'];
        if ($a_params['sedarrsearch'] != "")
            $this->idSedeArrivalSearch = $a_params['sedarrsearch'];
        parent::save();
        return $this->idTempSalesCart;
    }

    public function deleteTempSalesCart($idtsc) {
        DB::query(Database::DELETE, 'DELETE FROM tra_tempsalescart WHERE idTempSalesCart = ' . $idtsc)->execute();
    }

    public function cleanTempSalesCartBySession($sessionKey) {
        DB::query(Database::DELETE, 'DELETE FROM tra_tempsalescart WHERE sessionKey =' . "'$sessionKey'")->execute();
    }

    public function deleteMyAllTempSalesCart($sessionKey, $date, $maxTimeSelectTicked) {
        DB::query(Database::DELETE, 'DELETE FROM tra_tempsalescartx WHERE sessionKey =' . "'$sessionKey'")->execute();
        $sql = "DELETE FROM tra_tempsalescart           	
            WHERE TIME_TO_SEC(TIMEDIFF(STR_TO_DATE('" . $date . "','%Y-%m-%d %H:%i:%s') ,creationDate)) >=:maxTimeSelectTicked";
        DB::query(Database::DELETE, $sql)
                ->parameters(
                        array(
                            ':maxTimeSelectTicked' => $maxTimeSelectTicked
                ))
                ->execute();
    }

}
