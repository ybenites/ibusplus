<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of route
 *
 * @author BENITES
 */
class Model_Route extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_route';
    protected $_primary_key = 'idRoute';
    protected $_belongs_to = array(
        'route_sede_departure' => array('model' => 'Sede', 'foreign_key' => 'idSedeDeparture'),
        'route_sede_arrival' => array('model' => 'Sede', 'foreign_key' => 'idSedeArrival'),
    );

    public function saveArray($a_params) {
        $this->description = $a_params['froute_description'];
        $this->type = $a_params['froute_type'];
        $this->idLocationDeparture = $a_params['froute_departure'];
        $this->idLocationArrival = $a_params['froute_arrival'];
        $this->idSedeDeparture = $a_params['froute_sede_departure'];
        $this->idServiceDeparture = $a_params['froute_service_departure'];
        $this->idSedeArrival = $a_params['froute_sede_arrival'];
        $this->idServiceArrival = $a_params['froute_service_arrival'];
        $this->private = $a_params['froute_private'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }

    public function listArray($service_id = 0, $location_departure = 0, $location_arrival = 0, $group_by = FALSE) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__ . $service_id . $location_departure . $location_arrival . ($group_by ? 'true' : 'false');
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_parameters = array(
                array('ro.idRoute', 'route_id')
                , array('ro.description', 'route_description')
                , array('ro.type', 'route_type')
                , array('se.idService', 'route_service_id')
                , array('se.name', 'route_service')
                , array('lod.idLocation', 'route_departure_id')
                , array('lod.name', 'route_departure')
                , array('loa.idLocation', 'route_arrival_id')
                , array('loa.name', 'route_arrival')
            );
            $a_result = DB::select_array(
                                    $a_parameters
                            )
                            ->from(array('tra_route', 'ro'))
                            ->join(array('tra_service', 'se'))->on('ro.idService', '=', 'se.idService')
                            ->join(array('tra_location', 'lod'))->on('ro.idLocationDeparture', '=', 'lod.idLocation')
                            ->join(array('tra_location', 'loa'))->on('ro.idLocationArrival', '=', 'loa.idLocation');
            if ($group_by) {
                if ($location_departure == 0) {
                    $a_result->group_by('route_departure_id');
                } else {
                    $a_result->group_by('route_arrival_id');
                }
            }
            $a_result->where('ro.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('se.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('lod.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('loa.status', '=', self::BTS_STATUS_ACTIVE);
            if ($service_id != 0) {
                $a_result->and_where('ro.idService', '=', $service_id);
            }
            if ($location_departure != 0) {
                $a_result->and_where('ro.idLocationDeparture', '=', $location_departure);
            }
            if ($location_arrival != 0) {
                $a_result->and_where('ro.idLocationArrival', '=', $location_arrival);
            }
            $a_result_final = $a_result->execute()->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result_final);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getRouteArray($locdep, $serdep, $seddep, $locarr, $serarr, $sedarr, $type, $private = 0) {
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class() . __METHOD__ . $locdep . $seddep . $locarr . $sedarr;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
            $a_result = DB::select_array($this->getFieldForRoute())
                            ->from(array('tra_route', 'ro'))
                            ->join(array('tra_location', 'lod'))->on('ro.idLocationDeparture', '=', 'lod.idLocation')
                            ->join(array('tra_location', 'loa'))->on('ro.idLocationArrival', '=', 'loa.idLocation')
                            ->join(array('tra_sede', 'sed'), 'LEFT')->on('ro.idSedeDeparture', '=', 'sed.idSede')
                            ->join(array('tra_sede', 'sea'), 'LEFT')->on('ro.idSedeArrival', '=', 'sea.idSede')
                            ->join(array('tra_rate', 'ra'), 'LEFT')->on('ro.idRoute', '=', 'ra.idRate');
            $a_result->where('ro.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('lod.status', '=', self::BTS_STATUS_ACTIVE)
                    ->and_where('loa.status', '=', self::BTS_STATUS_ACTIVE);
            if ($type == 'sale') {
                if ($private == self::BTS_STATUS_DEACTIVE)
                    $a_result->and_where('ro.private', '=', self::BTS_STATUS_DEACTIVE);
                else
                    $a_result->and_where('ro.private', '=', self::BTS_STATUS_ACTIVE);
            } else if ($type == 'rate') {
                $a_result->where_open()
                        ->where('ro.private', '=', self::BTS_STATUS_ACTIVE)
                        ->or_where('ro.private', '=', self::BTS_STATUS_DEACTIVE)
                        ->where_close();
            }
            if ($locdep != self::BTS_STATUS_DEACTIVE)
                $a_result->and_where('ro.idLocationDeparture', '=', $locdep);
            if ($serdep != self::BTS_STATUS_DEACTIVE)
                $a_result->and_where('ro.idServiceDeparture', '=', $serdep);            
            if ($seddep != self::BTS_STATUS_DEACTIVE)
                $a_result->and_where('ro.idSedeDeparture', '=', $seddep);
            if ($locarr != self::BTS_STATUS_DEACTIVE)
                $a_result->and_where('ro.idLocationArrival', '=', $locarr);
            if ($serarr != self::BTS_STATUS_DEACTIVE)
                $a_result->and_where('ro.idServiceArrival', '=', $serarr);
            if ($sedarr != self::BTS_STATUS_DEACTIVE) {
                $a_result->where(DB::expr('FIND_IN_SET(' . $sedarr . ',ro.`idSedeArrival`)'), '>', self::BTS_STATUS_DEACTIVE);
            }
            $a_result->order_by('sed.name');
            $a_result->order_by('lod.name');

            $a_result_final = $a_result->execute()->as_array();
            $o_cacheInstance->set($s_cacheKey, $a_result_final);
            $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

    public function getRouteArrayById($idroute) {

        $a_result = DB::select_array($this->getFieldForRoute())
                ->from(array('tra_route', 'ro'))
                ->join(array('tra_location', 'lod'))->on('ro.idLocationDeparture', '=', 'lod.idLocation')
                ->join(array('tra_location', 'loa'))->on('ro.idLocationArrival', '=', 'loa.idLocation')
                ->join(array('tra_sede', 'sed'), 'LEFT')->on('ro.idSedeDeparture', '=', 'sed.idSede')
                ->join(array('tra_sede', 'sea'), 'LEFT')->on('ro.idSedeArrival', '=', 'sea.idSede')
                ->join(array('tra_rate', 'ra'), 'LEFT')->on('ro.idRoute', '=', 'ra.idRate')
                ->where('ro.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('lod.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('loa.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('ro.idRoute', '=', $idroute);

        return $a_result->execute()->as_array();
    }

    public function getArrivalByDeparture($dep) {
        $a_parameters = array(
            array('ro.idLocationArrival', 'idloa')
            , array('loa.name', 'nameloa')
        );
        $a_result = DB::select_array($a_parameters)
                ->distinct(TRUE)
                ->from(array('tra_route', 'ro'))
                ->join(array('tra_location', 'loa'))->on('ro.idLocationArrival', '=', 'loa.idLocation')
                ->where('ro.idLocationDeparture', '=', $dep)
                ->and_where('ro.status', '=', 1)
                ->order_by('ro.idLocationArrival');

        return $a_result->execute()->as_array();
    }

    public function listHourByRoute($idroute) {
        $a_parameters = array(
            array('r.idRoute', 'idr')
            , array('r.idRate', 'idra')
            , array('r.startTime', 'st')
            , array('r.endTime', 'et')
            , array(DB::expr("CONCAT(st.`description`, ' - ', et.`description`)"), 'description')
        );
        $a_result = DB::select_array($a_parameters)
                ->from(array('tra_rate', 'r'))
                ->join(array('tra_hour', 'st'))->on('r.startTime', '=', 'st.idHour')
                ->join(array('tra_hour', 'et'))->on('r.endTime', '=', 'et.idHour')
                ->where('r.idRoute', '=', $idroute)
                ->and_where('r.status', '=', self::BTS_STATUS_ACTIVE);

        return $a_result->execute()->as_array();
    }

    public function getFieldForRoute() {

        $field_arrival = DB::expr("
    IF(
      ro.idSedeArrival IS NULL 
      OR ro.idSedeArrival = 0,
      loa.`name`,
      (SELECT 
        GROUP_CONCAT(sea.name) 
      FROM
        tra_sede sea 
      WHERE sea.status = 1 
        AND SUBSTRING_INDEX(
          ro.idSedeArrival,
          ',',
          FIND_IN_SET(sea.idSede, ro.idSedeArrival)
        ))
    )            
");

        return array(
            array('ro.idRoute', 'route_id')
            , array(DB::expr('IF(ro.idSedeDeparture IS NULL OR ro.idSedeDeparture = 0, lod.`name`, sed.`name`)'), 'route_departure')
            , array($field_arrival, 'route_arrival')
            , array('ro.description', 'route_description')
            , array('ro.type', 'route_type')
            , array('lod.idLocation', 'route_departure_id')
            , array('loa.idLocation', 'route_arrival_id')
            , array('ra.alltime', 'alltime')
            , array('ro.idSedeDeparture', 'route_sede_dep')
            , array('ro.idSedeArrival', 'route_sede_arr')
            , array(DB::expr("(SELECT COUNT(ra.idRate) FROM tra_rate ra WHERE ra.idRoute = ro.`idRoute` AND ra.`status` = 1)"), 'numrate')
            , array(DB::expr("(SELECT SUM(ra.alltime) FROM tra_rate ra WHERE ra.idRoute = ro.`idRoute` AND ra.`status` = 1)"), 'alltime')
        );
    }
    
        public function getAllRoute() {        
        $o_cacheInstance = Cache::instance('apc');
        $s_cacheKey = get_class().__METHOD__;
        $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        if ($a_incacheObject === null) {
        $a_parameter = array(
            array('route.idRoute', 'route_id')
            , array('route.description', 'route_description')
        );
        $a_result = DB::select_array($a_parameter)
                        ->from(array('tra_route', 'route'))
                        ->where('route.status', '=', self::BTS_STATUS_ACTIVE)
                        ->execute()->as_array();
                $o_cacheInstance->set($s_cacheKey, $a_result);
                $a_incacheObject = $o_cacheInstance->get($s_cacheKey);
        }
        return $a_incacheObject;
    }

}
