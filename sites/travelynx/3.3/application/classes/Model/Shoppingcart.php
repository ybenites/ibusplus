<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Shoppingcart extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_shoppingcart';
    protected $_primary_key = 'idShoppingCart';

    public function saveArray($email, $amount, $code, $websale, $methodPayment, $idPaymentLog = null, $txt_phone1 = null, $txt_phone2 = null, $observations = null) {
        $this->totalAmount = $amount;
        $this->status = self::BTS_STATUS_ACTIVE;
        $this->email = $email;
        $this->code = $code;
        $this->websale = $websale;
        $this->methodPayment = $methodPayment;
        $this->numberPrint = 0;
        $this->contactPhone1 = $txt_phone1;
        $this->contactPhone2 = $txt_phone2;
        $this->observations = $observations;
        $this->listingDate = HelperDate::fnGetMysqlCurrentDateTime();
        if ($methodPayment == 'paypal') {
            $this->idPaymentLog = $idPaymentLog;
        }
        $this->typeSale = 'sale';
        if($websale == self::BTS_STATUS_ACTIVE)
            $this->typeSale = 'websale';
        parent::save();
        return $this->idShoppingCart;
    }

    public function getDatesToEmailSend($idsc) {
        $a_parameter = array(
            array('sc.code', 'code')
            , array(DB::expr("DATE_FORMAT(sc.`listingDate`,'%m-%d-%Y %h:%i:%s %p')"), 'datereservation')
            , array('pl.firstName', 'firstname')
            , array('pl.lastName', 'lastname')
            , array('sc.email', 'email')
            , array(DB::expr("CONCAT('$ ', sc.totalAmount, '.00')"), 'amount')
        );

        $a_result = DB::select_array(
                                $a_parameter
                        )
                        ->from(array('tra_shoppingcart', 'sc'))
                        ->join(array('bts_payment_logs', 'pl'))->on('sc.idPaymentLog', '=', 'pl.id')
                        ->where('sc.idShoppingCart', '=', $idsc)
                        ->execute()->current();
        return $a_result;
    }

}