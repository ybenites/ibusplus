<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Zipcode
 *
 * @author BENITES
 */
class Model_Zipcode extends Kohana_BtsOrm implements TravelynxConstants, Kohana_BtsConstants {

    protected $_table_names_plural = false;
    protected $_table_name = 'tra_zipcode';
    protected $_primary_key = 'idZipCode';
    protected $_belongs_to = array(
        'zipcode_location' => array('model' => 'Location', 'foreign_key' => 'idLocation'),
    );

    public function saveArray($a_params) {
        $this->idLocation = $a_params['idLocation'];
        $this->number = $a_params['number'];
        $this->status = self::BTS_STATUS_ACTIVE;
        parent::save();
    }

    public function listArray($idLocation) {
        $a_parameters = array(
            array('zi.idZipCode', 'zipcode_id')
            , array('zi.number', 'zipcode_number')
        );
        $a_result = DB::select_array(
                        $a_parameters
                )->from(array('tra_zipcode', 'zi'))
                ->where('zi.idLocation', '=', $idLocation)
                ->and_where('zi.status', '=', self::BTS_STATUS_ACTIVE);
        return $a_result->execute()->as_array();
    }

    public function getZipcode($zc) {
        $a_parameters = array(
            array(DB::expr("COUNT(idZipCode)"), 'total')
            , array('zc.number', 'number')
            , array('lo.idLocation', 'idlocation')
            , array('lo.name', 'name')
        );
        $a_result = DB::select_array($a_parameters)
                ->from(array('tra_zipcode', 'zc'))
                ->join(array('tra_location', 'lo'))
                ->on('zc.idLocation', '=', 'lo.idLocation')
                ->where('zc.number', '=', $zc)
                ->and_where('zc.status', '=', self::BTS_STATUS_ACTIVE)
                ->and_where('lo.status', '=', self::BTS_STATUS_ACTIVE);
        return $a_result->execute()->current();
    }

}