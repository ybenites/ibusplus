<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/Kohana/Core' . EXT;

if (is_file(APPPATH . 'classes/Kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/Kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/Kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));
/**
 * Optionally, you can enable a compatibility auto-loader for use with
 * older modules that have not been updated for PSR-0.
 *
 * It is recommended to not enable this unless absolutely necessary.
 */
//spl_autoload_register(array('Kohana', 'auto_load_lowercase'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
//I18n::lang('en-us');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => ''
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'cache' => MODPATH . 'cache', // Caching with multiple backends
    'database' => MODPATH . 'database', // Database access
    'image' => MODPATH . 'image', // Image manipulation
    'orm' => MODPATH . 'orm', // Object Relationship Mapping
    'helpers' => DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'helpers'
    , 'bts3' => DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'bts3'
    , 'logviewer' => DEVMODPATH . '3.3' . DIRECTORY_SEPARATOR . 'logviewer'    
));
//--------
global $a_db_config;
$a_db_config = Kohana::$config->load('config_' . str_replace(".", "", SITE_DOMAIN));
Kohana::$environment = $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_ENVIRONMENT];

if (Kohana::$environment == Kohana::DEVELOPMENT) {
    if (!defined('KOHANA_START_TIME')) {
        define('KOHANA_START_TIME', microtime(TRUE));
    }
    if (!defined('KOHANA_START_MEMORY')) {
        define('KOHANA_START_MEMORY', memory_get_usage());
    }
}

if (isset($a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_LANGUAGE]))
    I18n::lang($a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_LANGUAGE]);

Cache::$default = 'apc';
Cookie::$salt = 'salt_' . SITE_DOMAIN;
Cookie::$expiration = Date::WEEK;

$url_part = explode('/', $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DEFAULT_INDEX]);
$directory = $url_part[0];
$controller = $url_part[1];
$action = $url_part[2];
$timeZone = $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_TIME_ZONE];
date_default_timezone_set($timeZone);
Route::set(
                'default'
                , '(<directory>/<controller>(/<action>(/<id>)))(.<format>)(/<params>)'
                , array(
            'id' => '[0-9]+',
            'format' => '(html|json|xml)'
                )
        )
        ->defaults(array(
            'directory' => $directory,
            'controller' => $controller,
            'action' => $action,
            'format' => 'html'
                )
);