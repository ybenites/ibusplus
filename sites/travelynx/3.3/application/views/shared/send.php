<div style="margin-top: 20px;">
    <p style="color: #1F4381">RESERVATION DETAIL</p>
    <div>
        <div>
            <table>
                <tr>
                    <td style="width: 300px">SHUTTLE TICKET NUMBER:</td>
                    <td><?php echo $contact['code'] ?></td>
                </tr>                
                <tr>
                    <td>DATE OF RESERVATION:</td>
                    <td><?php echo $contact['datereservation'] ?></td>
                </tr>
                <tr>
                    <td>STATUS:</td>
                    <td>SOLD</td>
                </tr>	            
            </table>		
        </div>
    </div>        
</div>
<div style="margin-top: 20px;">
    <p style="color: #1F4381">CUSTOMER INFORMATION</p>
    <div>
        <div>
            <table>
                <tr>
                    <td style="width: 300px">FIRST NAME:</td>
                    <td><?php echo $contact['firstname'] ?></td>
                </tr>                
                <tr>
                    <td>LAST NAME:</td>
                    <td><?php echo $contact['lastname'] ?></td>
                </tr>
                <tr>
                    <td>EMAIL:</td>
                    <td><?php echo $contact['email'] ?></td>
                </tr> 
                <tr>
                    <td>TOTAL PURCHASE AMOUNT:</td>
                    <td><?php echo $contact['amount'] ?></td>
                </tr>        
            </table>
        </div>
    </div>        
</div>
<div style="margin-top: 20px;">
    <p style="color: #1F4381">TRAVEL INFORMATION</p>
    <div>
        <div>
            <?php foreach ($ticketdetail as $key => $value): ?>
                <div style="margin-bottom: 25px;">
                    <table>    
                        <?php if (count($ticketdetail) > 1): ?>
                            <tr>
                                <td style="width: 300px"><i><b><u>Travel <?php echo $key + 1; ?></u></b></i></td>
                                <td></td>
                            </tr>                        
                        <?php endif; ?>
                        <tr>
                            <td style="width: 300px">TOTAL TRAVELERS</td>
                            <td><?php echo $value['numpaxow'] ?></td>
                        </tr>                
                        <tr>
                            <td>REPRESENTATIVE</td>
                            <td><?php echo $value['nametraveler'] ?></td>
                        </tr>                
                        <tr>
                            <td>DATE</td>
                            <td><?php echo $value['dateow'] ?></td>
                        </tr>                
                        <tr>
                            <td>SERVICE</td>
                            <td><?php echo $value['service'] ?></td>
                        </tr>                
                        <tr>
                            <td>PRICE</td>
                            <td><?php echo $value['price'] ?></td>
                        </tr>                
                        <tr>
                            <td>FROM</td>
                            <td><?php echo $value['departure'] ?></td>
                        </tr>
                        <?php if (!is_null($value['airname'])): ?>
                            <tr>
                                <td>AIRLINE</td>
                                <td><?php echo $value['airname'] ?></td>
                            </tr>
                        <?php endif; ?>     
                        <?php if (!is_null($value['airnumber'])): ?>
                            <tr>
                                <td>FLIGHT NUMBER</td>
                                <td><?php echo $value['airnumber'] ?></td>
                            </tr>
                        <?php endif; ?>     
                        <?php if (!is_null($value['airtime'])): ?>
                            <tr>
                                <td>FLIGHT TIME</td>
                                <td><?php echo $value['airtime'] ?></td>
                            </tr>
                        <?php endif; ?>                          
                        <tr>
                            <td>TO</td>
                            <td><?php echo $value['arrival'] ?></td>
                        </tr>                
                        <tr>
                            <td>TYPE</td>
                            <td><?php echo $value['faretype'] ?></td>
                        </tr>   
                    </table>
                </div>
            <?php endforeach ?>        

        </div>
        <!--        <div style="display: none">
                    <table>
                        <tr>
                            <th style="width: 25px;"></th>
                            <th style="width: 220px; text-align: left">NAME:</th>
                            <th style="width: 180px; text-align: left">DATE</th>
                            <th style="width: 220px; text-align: left">SERVICE</th>
                            <th style="width: 100px; text-align: left">PRICE</th>
                            <th style="width: 180px; text-align: left">FROM</th>
                            <th style="width: 180px; text-align: left">TO</th>
                            <th style="width: 150px; text-align: left">TYPE</th>
                        </tr>
                        <tr>
        <?php // foreach ($ticketdetail as $key => $value): ?>
                            <tr>
                                <td><?php // echo $key + 1  ?></td>
                                <td><?php // echo $value['nametraveler']  ?></td>
                                <td><?php // echo $value['dateow']  ?></td>
                                <td><?php // echo $value['service']  ?></td>
                                <td><?php // echo $value['price']  ?></td>
                                <td><?php // echo $value['departure']  ?></td>
                                <td><?php // echo $value['arrival']  ?></td>
                                <td><?php // echo $value['faretype']  ?></td>
                            </tr>                
        <?php // endforeach ?>
                        </tr> 
                    </table>
                </div>-->
    </div>        
</div>

<div style="margin-top: 70px;">
    <p style="text-align: center"><b>
            Gmail users please check Spam/Junk Folder for the Email Confirmation. A Photo ID is required to
            board the shuttle. Online promotions are non refundable and can not be changed.</b>
    </p>
    <p style="text-align: justify; margin-top: 20px;">
        <b style="color: #1F4381">Travelynx Terms & Conditions:</b>
        Unused Tickets, if you miss your trip, cannot be refunded.
        Changes of Fare prices: If you purchased your shuttle ticket at a certain price, and learn it might
        have decreased after purchase, Travelynx is not liable to supply the differences. Trip fares may
        vary slightly according to time of day and circumstances.
        Applicable discounts become invalid once the shuttle ticket is purchased.
        Shuttle Tickets may be changed for another date and/or time; however, 30% fee of the value of
        ticket purchased will be applied (up to 24 hours before departure of the shuttle). If your new
        shuttle trip has a different price, you must also pay the fare difference.
        Shuttle tickets purchased within 24 hours of the departure date are not refundable.
        Refunds will be made only through refund@travelynx.net.
        Changes have to be made over the phone by calling 800-226-5969.
        All cancellations must be made 24 hours in advance of the departure time to be eligable for a
        refund. (A 30% service charge will be applied.)
    </p>
</div>

<div style="text-align: center">
    <h1>
        Thank you for choosing Travelynx.net         
    </h1>
    <h3>
        <i>Travelynx is committed to offering our customers a wide-array of chartered and transit
            options utilizing a late model fleet with an unwavering commitment to safety and service.</i>
    </h3>
</div>