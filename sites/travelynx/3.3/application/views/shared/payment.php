<div id="modal_pay">
    <div style="margin: 0 auto; padding: 0px;">
        <div class="row" style="margin-left: 40px;">
            <form class="form-horizontal span10" id="paypalPayment">
                <input type="hidden" class="span12 hide" id="txt_total_amount" name="txt_total_amount" placeholder="">
                <input type="hidden" class="span12 hide" id="txt_total_amounth" name="txt_total_amounth" placeholder="">
                <input type="hidden" class="span12 hide" id="methodPayment" name="methodPayment" placeholder="" value="paypal"> 
                <fieldset>
                    <legend style="font-size: 17.5px;">
                        <span><?php echo __("Monto a pagar") ?>: </span>
                        <span id="span_amount_to_pay"></span><span> $</span>
                        <span style="float: right"><i class="icon-info-sign" title="<?php echo __("Informacion") ?>"></i></span>
                        <span style="float: right"><i class="icon-file" id="clean_form_payment" title="<?php echo __("limpiar Formulario") ?>"></i></span>
                    </legend>

                    <div class="control-group" style="padding-top: 0">
                        <label class="control-label"><?php echo __("Nombres y Apellidos") ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_firstName" id="txt_firstName" autocomplete="off" pattern="\d{4}" title="<?php echo __("Nombres") ?>" placeholder="<?php echo __("Nombres") ?>">
                                </div>
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_lastName" id="txt_lastName" autocomplete="off" pattern="\d{4}" title="<?php echo __("Apellidos") ?>" placeholder="<?php echo __("Apellidos") ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo __("Direccion") ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_address1" id="txt_address1" autocomplete="off" pattern="\d{4}" title="<?php echo __("Direccion 1") ?>" placeholder="<?php echo __("Direccion 1") ?>" required>
                                </div>
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_address2" id="txt_address2" autocomplete="off" pattern="\d{4}" title="<?php echo __("Direccion 2") ?>" placeholder="<?php echo __("Direccion 2 (opcional)") ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo __("Procedencia"); ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span3">
                                    <input type="text" class="input-block-level" name="txt_city" autocomplete="off" pattern="\d{4}" title="<?php echo __("Ciudad") ?>" placeholder="<?php echo __("ciudad") ?>" required>
                                </div>
                                <div class="span3">
                                    <select class="input-block-level" name="cmb_state" id="cmb_state" style="width: 190px;">
                                        <option value="" selected></option>
                                        <option value="AK">AK</option>
                                        <option value="AL">AL</option>
                                        <option value="AR">AR</option>
                                        <option value="AZ">AZ</option>
                                        <option value="CA">CA</option>
                                        <option value="CO">CO</option>
                                        <option value="CT">CT</option>
                                        <option value="DC">DC</option>
                                        <option value="DE">DE</option>
                                        <option value="FL">FL</option>
                                        <option value="GA">GA</option>
                                        <option value="HI">HI</option>
                                        <option value="IA">IA</option>
                                        <option value="ID">ID</option>
                                        <option value="IL">IL</option>
                                        <option value="IN">IN</option>
                                        <option value="KS">KS</option>
                                        <option value="KY">KY</option>
                                        <option value="LA">LA</option>
                                        <option value="MA">MA</option>
                                        <option value="MD">MD</option>
                                        <option value="ME">ME</option>
                                        <option value="MI">MI</option>
                                        <option value="MN">MN</option>
                                        <option value="MO">MO</option>
                                        <option value="MS">MS</option>
                                        <option value="MT">MT</option>
                                        <option value="NC">NC</option>
                                        <option value="ND">ND</option>
                                        <option value="NE">NE</option>
                                        <option value="NH">NH</option>
                                        <option value="NJ">NJ</option>
                                        <option value="NM">NM</option>
                                        <option value="NV">NV</option>
                                        <option value="NY">NY</option>
                                        <option value="OH">OH</option>
                                        <option value="OK">OK</option>
                                        <option value="OR">OR</option>
                                        <option value="PA">PA</option>
                                        <option value="RI">RI</option>
                                        <option value="SC">SC</option>
                                        <option value="SD">SD</option>
                                        <option value="TN">TN</option>
                                        <option value="TX">TX</option>
                                        <option value="UT">UT</option>
                                        <option value="VA">VA</option>
                                        <option value="VT">VT</option>
                                        <option value="WA">WA</option>
                                        <option value="WI">WI</option>
                                        <option value="WV">WV</option>
                                        <option value="WY">WY</option>
                                        <option value="AA">AA</option>
                                        <option value="AE">AE</option>
                                        <option value="AP">AP</option>
                                        <option value="AS">AS</option>
                                        <option value="FM">FM</option>
                                        <option value="GU">GU</option>
                                        <option value="MH">MH</option>
                                        <option value="MP">MP</option>
                                        <option value="PR">PR</option>
                                        <option value="PW">PW</option>
                                        <option value="VI">VI</option>
                                    </select>                                    
                                </div>
                                <div class="span3">
                                    <input type="text" class="input-block-level" name="txt_zip" pattern="\w+ \w+.*" title="<?php echo __("Codigo Postal") ?>" placeholder="<?php echo __("Codigo Postal") ?>" required>
                                </div>                                
                                <div class="span3">
                                    <select id="cmb_country" name="cmb_country" class="input-block-level" style="width: 190px;">
                                        <option value="AC">Ascension Island</option><option value="AD">Andorra</option><option value="AE">United Arab Emirates</option><option value="AF">Afghanistan</option><option value="AG">Antigua and Barbuda</option><option value="AI">Anguilla</option><option value="AL">Albania</option><option value="AM">Armenia</option><option value="AN">Netherlands Antilles</option><option value="AO">Angola</option><option value="AQ">Antarctica</option><option value="AR">Argentina</option><option value="AS">American Samoa</option><option value="AT">Austria</option><option value="AU">Australia</option><option value="AW">Aruba</option><option value="AX">Åland Islands</option><option value="AZ">Azerbaijan</option><option value="BA">Bosnia and Herzegovina</option><option value="BB">Barbados</option><option value="BD">Bangladesh</option><option value="BE">Belgium</option><option value="BF">Burkina Faso</option><option value="BG">Bulgaria</option><option value="BH">Bahrain</option><option value="BI">Burundi</option><option value="BJ">Benin</option><option value="BL">Saint Barthélemy</option><option value="BM">Bermuda</option><option value="BN">Brunei</option><option value="BO">Bolivia</option><option value="BQ">British Antarctic Territory</option><option value="BR">Brazil</option><option value="BS">Bahamas</option><option value="BT">Bhutan</option><option value="BV">Bouvet Island</option><option value="BW">Botswana</option><option value="BY">Belarus</option><option value="BZ">Belize</option><option value="CA">Canada</option><option value="CC">Cocos [Keeling] Islands</option><option value="CD">Congo - Kinshasa</option><option value="CF">Central African Republic</option><option value="CG">Congo - Brazzaville</option><option value="CH">Switzerland</option><option value="CI">Côte d’Ivoire</option><option value="CK">Cook Islands</option><option value="CL">Chile</option><option value="CM">Cameroon</option><option value="CN">China</option><option value="CO">Colombia</option><option value="CP">Clipperton Island</option><option value="CR">Costa Rica</option><option value="CS">Serbia and Montenegro</option><option value="CT">Canton and Enderbury Islands</option><option value="CU">Cuba</option><option value="CV">Cape Verde</option><option value="CX">Christmas Island</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DD">East Germany</option><option value="DE">Germany</option><option value="DG">Diego Garcia</option><option value="DJ">Djibouti</option><option value="DK">Denmark</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="DZ">Algeria</option><option value="EA">Ceuta and Melilla</option><option value="EC">Ecuador</option><option value="EE">Estonia</option><option value="EG">Egypt</option><option value="EH">Western Sahara</option><option value="ER">Eritrea</option><option value="ES">Spain</option><option value="ET">Ethiopia</option><option value="EU">European Union</option><option value="FI">Finland</option><option value="FJ">Fiji</option><option value="FK">Falkland Islands</option><option value="FM">Micronesia</option><option value="FO">Faroe Islands</option><option value="FQ">French Southern and Antarctic Territories</option><option value="FR">France</option><option value="FX">Metropolitan France</option><option value="GA">Gabon</option><option value="GB">United Kingdom</option><option value="GD">Grenada</option><option value="GE">Georgia</option><option value="GF">French Guiana</option><option value="GG">Guernsey</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GL">Greenland</option><option value="GM">Gambia</option><option value="GN">Guinea</option><option value="GP">Guadeloupe</option><option value="GQ">Equatorial Guinea</option><option value="GR">Greece</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="GT">Guatemala</option><option value="GU">Guam</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HK">Hong Kong SAR China</option><option value="HM">Heard Island and McDonald Islands</option><option value="HN">Honduras</option><option value="HR">Croatia</option><option value="HT">Haiti</option><option value="HU">Hungary</option><option value="IC">Canary Islands</option><option value="ID">Indonesia</option><option value="IE">Ireland</option><option value="IL">Israel</option><option value="IM">Isle of Man</option><option value="IN">India</option><option value="IO">British Indian Ocean Territory</option><option value="IQ">Iraq</option><option value="IR">Iran</option><option value="IS">Iceland</option><option value="IT">Italy</option><option value="JE">Jersey</option><option value="JM">Jamaica</option><option value="JO">Jordan</option><option value="JP">Japan</option><option value="JT">Johnston Island</option><option value="KE">Kenya</option><option value="KG">Kyrgyzstan</option><option value="KH">Cambodia</option><option value="KI">Kiribati</option><option value="KM">Comoros</option><option value="KN">Saint Kitts and Nevis</option><option value="KP">North Korea</option><option value="KR">South Korea</option><option value="KW">Kuwait</option><option value="KY">Cayman Islands</option><option value="KZ">Kazakhstan</option><option value="LA">Laos</option><option value="LB">Lebanon</option><option value="LC">Saint Lucia</option><option value="LI">Liechtenstein</option><option value="LK">Sri Lanka</option><option value="LR">Liberia</option><option value="LS">Lesotho</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="LV">Latvia</option><option value="LY">Libya</option><option value="MA">Morocco</option><option value="MC">Monaco</option><option value="MD">Moldova</option><option value="ME">Montenegro</option><option value="MF">Saint Martin</option><option value="MG">Madagascar</option><option value="MH">Marshall Islands</option><option value="MI">Midway Islands</option><option value="MK">Macedonia</option><option value="ML">Mali</option><option value="MM">Myanmar [Burma]</option><option value="MN">Mongolia</option><option value="MO">Macau SAR China</option><option value="MP">Northern Mariana Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MS">Montserrat</option><option value="MT">Malta</option><option value="MU">Mauritius</option><option value="MV">Maldives</option><option value="MW">Malawi</option><option value="MX">Mexico</option><option value="MY">Malaysia</option><option value="MZ">Mozambique</option><option value="NA">Namibia</option><option value="NC">New Caledonia</option><option value="NE">Niger</option><option value="NF">Norfolk Island</option><option value="NG">Nigeria</option><option value="NI">Nicaragua</option><option value="NL">Netherlands</option><option value="NO">Norway</option><option value="NP">Nepal</option><option value="NQ">Dronning Maud Land</option><option value="NR">Nauru</option><option value="NT">Neutral Zone</option><option value="NU">Niue</option><option value="NZ">New Zealand</option><option value="OM">Oman</option><option value="PA">Panama</option><option value="PC">Pacific Islands Trust Territory</option><option value="PE">Peru</option><option value="PF">French Polynesia</option><option value="PG">Papua New Guinea</option><option value="PH">Philippines</option><option value="PK">Pakistan</option><option value="PL">Poland</option><option value="PM">Saint Pierre and Miquelon</option><option value="PN">Pitcairn Islands</option><option value="PR">Puerto Rico</option><option value="PS">Palestinian Territories</option><option value="PT">Portugal</option><option value="PU">U.S. Miscellaneous Pacific Islands</option><option value="PW">Palau</option><option value="PY">Paraguay</option><option value="PZ">Panama Canal Zone</option><option value="QA">Qatar</option><option value="QO">Outlying Oceania</option><option value="RE">Réunion</option><option value="RO">Romania</option><option value="RS">Serbia</option><option value="RU">Russia</option><option value="RW">Rwanda</option><option value="SA">Saudi Arabia</option><option value="SB">Solomon Islands</option><option value="SC">Seychelles</option><option value="SD">Sudan</option><option value="SE">Sweden</option><option value="SG">Singapore</option><option value="SH">Saint Helena</option><option value="SI">Slovenia</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SK">Slovakia</option><option value="SL">Sierra Leone</option><option value="SM">San Marino</option><option value="SN">Senegal</option><option value="SO">Somalia</option><option value="SR">Suriname</option><option value="ST">São Tomé and Príncipe</option><option value="SU">Union of Soviet Socialist Republics</option><option value="SV">El Salvador</option><option value="SY">Syria</option><option value="SZ">Swaziland</option><option value="TA">Tristan da Cunha</option><option value="TC">Turks and Caicos Islands</option><option value="TD">Chad</option><option value="TF">French Southern Territories</option><option value="TG">Togo</option><option value="TH">Thailand</option><option value="TJ">Tajikistan</option><option value="TK">Tokelau</option><option value="TL">Timor-Leste</option><option value="TM">Turkmenistan</option><option value="TN">Tunisia</option><option value="TO">Tonga</option><option value="TR">Turkey</option><option value="TT">Trinidad and Tobago</option><option value="TV">Tuvalu</option><option value="TW">Taiwan</option><option value="TZ">Tanzania</option><option value="UA">Ukraine</option><option value="UG">Uganda</option><option value="UM">U.S. Minor Outlying Islands</option><option value="US" selected="selected">United States</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VA">Vatican City</option><option value="VC">Saint Vincent and the Grenadines</option><option value="VD">North Vietnam</option><option value="VE">Venezuela</option><option value="VG">British Virgin Islands</option><option value="VI">U.S. Virgin Islands</option><option value="VN">Vietnam</option><option value="VU">Vanuatu</option><option value="WF">Wallis and Futuna</option><option value="WK">Wake Island</option><option value="WS">Samoa</option><option value="YD">People's Democratic Republic of Yemen</option><option value="YE">Yemen</option><option value="YT">Mayotte</option><option value="ZA">South Africa</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option><option value="ZZ">Unknown Region</option>
                                    </select>                                    
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label"><?php echo __("Telefono") ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_phone1" id="txt_phone1" autocomplete="off" pattern="\d{4}" title="<?php echo __("Telefono 1") ?>" placeholder="<?php echo __('Telefono 1') ?>" required>
                                </div>
                                <div class="span6">
                                    <input type="text" class="input-block-level" name="txt_phone2" id="txt_phone2" autocomplete="off" pattern="\d{4}" title="<?php echo __("Telefono 2") ?>" placeholder="<?php echo __("Telefono 2 (opcional)") ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label"><?php echo __("Datos de Tarjeta") ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span3">
                                    <select class="input-block-level" name="cmb_type_card" id="cmb_type_card" style="width: 190px;">
                                        <option value="Visa">Visa</option>
                                        <option value="MasterCard">MasterCard</option>
                                        <option value="Discover">Discover</option>
                                        <option value="Amex">American Express</option>
                                    </select>
                                </div>
                                <div class="span6">
                                    <input type="text" name="txt_creditCardNumber" id="txt_creditCardNumber" class="input-block-level" autocomplete="off" pattern="\d{4}" placeholder="<?php echo __("Numero de Tarjeta") ?>" title="<?php echo __("Numero de Tarjeta") ?>" data-mask="9999 9999 9999 9999" required>
                                </div>
                                <div class="span3">
                                    <input type="text" class="input-block-level" name="txt_cvv2Number" id="txt_cvv2Number" autocomplete="off" pattern="\d{4}" title="<?php echo __("Codigo de verificacion") ?>" placeholder="<?php echo __("Codigo de verificacion") ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>                    

                    <div class="control-group">
                        <label class="control-label"><?php echo __("Expiracion de tarjeta") ?></label>
                        <div class="controls">
                            <div class="row-fluid">
                                <div class="span3">
                                    <select class="input-block-level" name="cmb_expiration_month" id="cmb_expiration_month" style="width: 190px;">
                                        <option value="00">00</option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                    </select>
                                </div>
                                <div class="span3">
                                    <select class="input-block-level" id="cmb_expiration_year" name="cmb_expiration_year" style="width: 190px;">
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                        <option value="2018">2018</option>
                                        <option value="2019">2019</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                        <option value="2026">2026</option>
                                        <option value="2027">2027</option>
                                        <option value="2028">2028</option>
                                        <option value="2029">2029</option>
                                        <option value="2030">2030</option>
                                    </select>                                    
                                </div>
                                <div class="span6 input-prepend">
                                    <span class="add-on"><i class="icon-envelope"></i></span>
                                    <input type="email" style="width: 343px;" id="txt_email" name="txt_email" pattern="\w+ \w+.*" title="Email" placeholder="Email: test@example.com">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" style="text-align: center;">
                        <a id="btn_sales_cart_save" class="btn btn-info" style="color: #222222" data-loading-text="<?php echo __("Procesando") ?>...">
                            <i class="icon-shopping-cart"></i>
                            <?php echo __("Pagar y Finalizar venta") ?>
                        </a>
                    </div>                      

                </fieldset>
            </form>
        </div>
    </div>
</div>