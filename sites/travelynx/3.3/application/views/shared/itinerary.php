<script type="text/javascript">
    var s_validate_required_fsale_service = '<?php echo __("Servicio") ?>';
    var s_validate_required_fsale_departure = '<?php echo __("Salida") ?>';
    var s_validate_required_fsale_arrival = '<?php echo __("Llegada") ?>';

    var s_validate_required_paypal_firstname = '<?php echo __("Nombre requerido") ?>';
    var s_validate_required_paypal_lastname = '<?php echo __("Apellido requerido") ?>';
    var s_validate_required_paypal_address = '<?php echo __("Direccion requerida") ?>';
    var s_validate_required_paypal_phone = '<?php echo __("Telefono requerido") ?>';
    var s_validate_required_paypal_city = '<?php echo __("Ciudad requerida") ?>';
    var s_validate_required_paypal_state = '<?php echo __("Estado requerido") ?>';
    var s_validate_required_paypal_zipcode = '<?php echo __("Codigo Zip requerido") ?>';
    var s_validate_required_paypal_typecard = '<?php echo __("Tipo de tarjeta requerido") ?>';
    var s_validate_required_expiration_month = '<?php echo __("Mes de expiracion requerido") ?>';
    var s_validate_required_expiration_year = '<?php echo __("Anio de expiracion requerido") ?>';
    var s_validate_required_creditCardNumber = '<?php echo __("Numero de tarjeta requerido") ?>';
    var s_validate_required_cvv2Number = '<?php echo __("Codigo de verificacion requerido") ?>';
    var s_validate_required_email = '<?php echo __("Email requerido") ?>';

    var s_txt_location = '<?php echo __("Locacion") ?>';
    var s_txt_service = '<?php echo __("Servicio") ?>';
    var s_txt_headquarter = '<?php echo __("Sede") ?>';
    var s_txt_state = '<?php echo __("Estado") ?>';
    var s_txt_department = '<?php echo __("Departamento") ?>';
    var s_txt_personalized = '<?php echo __("Personalizado") ?>';
    var s_txt_passanger = '<?php echo __("Pasajeros") ?>';

    var s_txt_service_cruise = '<?php echo __("Servicio de Crucero") ?>';
    var s_txt_service_airport = '<?php echo __("Servicio de Aeropuerto") ?>';
    var s_txt_service_hotel = '<?php echo __("Servicio de Hotel") ?>';
    var s_txt_service_attraction = '<?php echo __("Servicio de Atraccion") ?>';
    var s_txt_sede_by_zipcode = '<?php echo __("Sede por Zipcode") ?>';
    var s_txt_copy_all_rows = '<?php echo __("Copiar todas las filas") ?>';
    var s_txt_copy_row = '<?php echo __("Copiar fila") ?>';
    var s_txt_delete_all_row = '<?php echo __("Eliminar todas las filas") ?>';
    var s_txt_delete_row = '<?php echo __("Eliminar fila") ?>';
    var s_txt_departure = '<?php echo __("Origen") ?>';
    var s_txt_arrival = '<?php echo __("Destino") ?>';
    var s_txt_airline = '<?php echo __("Linea Area") ?>';
    var s_txt_hotel = '<?php echo __("Hotel") ?>';
    var s_txt_attraction = '<?php echo __("Atraccion") ?>';
    var s_txt_airport = '<?php echo __("Aeropuerto") ?>';
    var s_txt_cruise = '<?php echo __("Crucero") ?>';
    var s_txt_cruise_name = '<?php echo __("Nombre de crucero") ?>';
    var s_txt_flight_number = '<?php echo __("Numero de vuelo") ?>';
    var s_txt_hotel_name = '<?php echo __("Nombre de Hotel") ?>';
    var s_txt_title = '<?php echo __("Titulo") ?>';
    var s_txt_firstname = '<?php echo __("Primer Nombre") ?>';
    var s_txt_middlename = '<?php echo __("Segundo Nombre") ?>';
    var s_txt_lastname = '<?php echo __("Apellidos") ?>';
    var s_txt_dob = '<?php echo __("Fecha Nacimiento") ?>';
    var s_txt_gender = '<?php echo __("Genero") ?>';
    var s_txt_options = '<?php echo __("Opciones") ?>';
    var s_txt_going = '<?php echo __("ida") ?>';
    var s_txt_turn = '<?php echo __("vuelta") ?>';
    var s_txt_roundtrip = '<?php echo __("ida y vuelta") ?>';
    var s_txt_description = '<?php echo __("Descripcion") ?>';
    var s_txt_total_to_pay = '<?php echo __("Total a pagar") ?>';
    var s_txt_schedules = '<?php echo __("Horarios") ?>';
    var s_txt_required = '<?php echo __("Requerido") ?>';
    var s_txt_no_service = '<?php echo __("Sin Servicio") ?>';
    var s_txt_oneway_time = '<?php echo __("Hora de viaje de Ida") ?>';
    var s_txt_roundtrip_time = '<?php echo __("Hora de viaje de Vuelta") ?>';
    var s_txt_time = '<?php echo __("Hora") ?>';

    var s_info_was_saved_correctly = '<?php echo __("Se guardo correctamente...") ?>';
    var s_info_entry_an_address_departure = '<?php echo __("Ingrese una dirección de origen...") ?>';
    var s_info_entry_an_zipcode_departure = '<?php echo __("Ingrese un zip code de origen...") ?>';
    var s_info_entry_an_address_arrival = '<?php echo __("Ingrese una dirección de destino...") ?>';
    var s_info_entry_an_zipcode_arrival = '<?php echo __("Ingrese un zip code de destino...") ?>';
    var s_info_change_to_normal_sale = '<?php echo __("Si cambia a venta normal, se perderan todos sus cambios que no hayan sido procesados, desea proseguir?") ?>';
    var s_info_change_to_personalized_sale = '<?php echo __("Sicambia a venta personalizada, se perderan todos sus cambios que no hayan sido procesados, desea proseguir?") ?>';
    var s_info_help_select_schedule = '<?php echo __("Seleccione un horario para mostrar los precios por pasajero") ?>';

    var s_txt_trash_travel = '<?php echo __("Elimina viaje seleccionado") ?>'
    var s_txt_add_travel = '<?php echo __("Agrega viaje al carrito de compras") ?>'

    var msg1 = "<strong>" + '<?php echo __("No se encontraron rutas!") ?>' + "</strong>" + ' ' + '<?php echo __("Porfavor vuelva a intentarlo de nuevo con otros parametros") ?>' + ".";
</script>

<div id="step-1">
    <div class="page-header well" style="font-size: 12px; padding: 4px 0 0 12px;">
        <form id="frm_rate_l_">
            <div id="div_input_zipcode_departure" class="fleft hide" style="margin-top: 2px;">
                <div style="margin-bottom: 3px;"><?php echo __("ZIP CODE ORIGEN") ?></div>                
                <div class="fleft" style="margin-bottom: -4px;">
                    <input type="text" id="txt_zipcode_departure" name="txt_zipcode_departure" class="input-medium" placeholder="<?php echo __('ingrese su zip code') ?>">
                </div>
                <div style="clear: both"></div>
                <div style="color: #0088CC; font-size: 11px; cursor: pointer">
                    <span id="btn_option_search_filter_departure" style="margin-left: 1px;"><?php echo __("Cambiar a busqueda por filtro") ?></span>
                </div>
            </div>
            <div id="div_btn_search_zipcode_departure" class="fleft hide" style="margin-left: 5px;margin-top: 24px;">
                <div>
                    <a href="#" role="button" id="btn_search_zipcode_departure" class="btn btn-small" style="padding: 2px 10px;">
                        <i class="icon-circle-arrow-right"></i>
                    </a>                    
                </div>
            </div>
            <div id="div_msg_zip_code_no_found_departure" class="fleft hide" style="margin-top: 31px; margin-left: 7px;">
                <div class="alert error" style="margin-top: -10px; padding: 5px 17px 5px 14px;">
                    <strong><?php echo __('Zip Code no encontrado') ?>!</strong>                    
                </div>
            </div>
            <div id="div_address_by_zipcode_departure" class="fleft hide" style="margin-left: 5px;margin-top: 2px;">
                <div style="margin-bottom: 3px;">
                    <span><?php echo __("DIRECCION DE ORIGEN") ?></span>
                    <span><i class="icon-hand-left fright" id="btn_hand_departure" style="margin-right: 2px;"></i></span>
                </div>
                <div class="fleft">
                    <input type="text" id="txt_address_departure" name="txt_address_departure" class="input-xlarge" placeholder="<?php echo __("ingrese direccion de origen") ?> ">
                </div>
            </div>
            <div id="div_location_departure" class="fleft">
                <div style="margin-bottom: 3px;">
                    <span><?php echo __("ORIGEN") ?></span>
                </div>                
                <div>
                    <div class="fleft">
                        <select id="cmb_headquarter_departure" name="cmb_headquarter_departure" class="width172">
                            <option></option>                
                            <?php foreach ($SEDE as $key => $sede): ?>
                                <option value="<?php echo $sede['idsede'] ?>" data-idloc="<?php echo $sede['idloc'] ?>" data-idserv="<?php echo $sede['idserv'] ?>"><?php echo $sede['namesede'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>                    
                    <div class="fleft mleft8">
                        <select id="cmb_service_departure" name="cmb_service_departure" class="width172">
                            <option></option>
                            <?php foreach ($SERVICE as $key => $ser): ?>
                                <option value="<?php echo $ser['service_id'] ?>"><?php echo $ser['service_name'] ?></option>
                            <?php endforeach; ?>                            
                        </select>
                    </div>
                    <div class="fleft mleft8">
                        <select id="cmb_location_departure" name="cmb_location_departure" class="width172">
                            <option></option>
                            <?php foreach ($LOCATION as $key => $loc): ?>
                                <option value="<?php echo $loc['idloc'] ?>"><?php echo $loc['nameloc'] ?></option>
                            <?php endforeach; ?>
                        </select>                
                    </div>
                    <div style="clear: both"></div>
                    <div style="margin-top: 4px; color: #0088CC; font-size: 11px; cursor: pointer">
                        <span id="btn_option_search_zipcode_departure" style="margin-left: 1px;"><?php echo __("Cambiar a busqueda por zip code") ?></span>
                    </div>
                </div>                
            </div>
            <div id="div_input_zipcode_arrival" class="fleft hide" style="margin-top: 2px; margin-left: 60px;">
                <div style="margin-bottom: 3px;"><?php echo __("ZIP CODE DESTINO") ?></div>                
                <div class="fleft" style="margin-bottom: -4px;">
                    <input type="text" id="txt_zipcode_arrival" name="txt_zipcode_arrival" class="input-medium" placeholder="<?php echo __('ingrese su zip code') ?>">
                </div>
                <div style="clear: both"></div>
                <div style="color: #0088CC; font-size: 11px; cursor: pointer">
                    <span id="btn_option_search_filter_arrival" style="margin-left: 1px;"><?php echo __("Cambiar a busqueda por filtro") ?></span>
                </div>                
            </div>
            <div id="div_btn_search_zipcode_arrival" class="fleft hide" style="margin-left: 5px;margin-top: 24px;">
                <div>
                    <a href="#" role="button" id="btn_search_zipcode_arrival" class="btn btn-small" style="padding: 2px 10px;">
                        <i class="icon-circle-arrow-right"></i>
                    </a>                    
                </div>
            </div>
            <div id="div_msg_zip_code_no_found_arrival" class="fleft hide" style="margin-top: 31px; margin-left: 7px;">
                <div class="alert error" style="margin-top: -10px; padding: 5px 17px 5px 14px;">
                    <strong><?php echo __('Zip Code no encontrado') ?>!</strong>                    
                </div>
            </div>            
            <div id="div_address_by_zipcode_arrival" class="fleft hide" style="margin-left: 5px;margin-top: 2px;">
                <div style="margin-bottom: 3px;">
                    <span><?php echo __("DIRECCION DE DESTINO") ?></span>
                    <span><i class="fright icon-hand-left" id="btn_hand_arrival" style="margin-right: 2px;"></i></span>
                </div>
                <div class="fleft">
                    <input type="text" id="txt_address_arrival" name="txt_address_arrival" class="input-xlarge" placeholder="<?php echo __("ingrese direccion de destino") ?> ">
                </div>
            </div>
            <div id="div_location_arrival" class="fleft mleft15">
                <div style="margin-bottom: 3px;">
                    <span><?php echo __("DESTINO") ?></span>
                </div>
                <div>
                    <div class="fleft">
                        <select id="cmb_headquarter_arrival" name="cmb_headquarter_arrival" class="width172">
                            <option></option>
                            <?php foreach ($SEDE as $key => $sede): ?>
                                <option value="<?php echo $sede['idsede'] ?>" data-idloc="<?php echo $sede['idloc'] ?>" data-idserv="<?php echo $sede['idserv'] ?>"><?php echo $sede['namesede'] ?></option>
                            <?php endforeach; ?>                            
                        </select>
                    </div>                    
                    <div class="fleft mleft8">
                        <select id="cmb_service_arrival" name="cmb_service_arrival" class="width172">
                            <option></option>      
                            <?php foreach ($SERVICE as $key => $ser): ?>
                                <option value="<?php echo $ser['service_id'] ?>"><?php echo $ser['service_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="fleft mleft8">
                        <select id="cmb_location_arrival" name="cmb_location_arrival" class="width172">
                            <option></option>
                            <?php foreach ($LOCATION as $key => $loc): ?>
                                <option value="<?php echo $loc['idloc'] ?>"><?php echo $loc['nameloc'] ?></option>
                            <?php endforeach; ?>
                        </select>                
                    </div>                    
                </div>
                <div style="clear: both"></div>
                <div style="margin-top: 4px; color: #0088CC; font-size: 11px; cursor: pointer">
                    <span id="btn_option_search_zipcode_arrival" style="margin-left: 1px;"><?php echo __("Cambiar a busqueda por zip code") ?></span>
                </div>
            </div>
            <div id="div_btn_search_route" style="margin-top: 19px;" class="fleft mleft15">
                <div>
                    <a href="#" role="button" id="btn_search_location" class="btn btn-small" data-toggle="modal" style="padding: 5px 16px;" title="<?php echo __("Buscar Viajes") ?>">
                        <i class="icon-play-circle"></i>
                    </a>
                </div>
            </div>
            <div style="clear: both"></div>
        </form>
    </div>
    <div id="msg_no_found_route" class="alert error hide">
        <button id="btn-close-msg" type="button" class="close">&times;</button>
        <span></span>
    </div>
    <div id="msg_route_collapse" class="alert chevrom hide">
        <span><i class="icon-upload"></i></span>
    </div>
    <div id="div_all_routes" class="" style="margin-top: 10px;"></div>
    <div id="div_route_by_param" class="row" style="margin-top: 15px;">
        <div class="span9 columns">
            <div class="bs-docs-grid">
                <div id="info-1" class="alert navbar-inner-2 hide">
                    <div class="fleft mtop5">
                        <span id="span_selected_route" style="font-weight: bold;"></span>
                    </div>
                    <div class="fright">
                        <div class="btn-group" data-toggle="buttons-radio">                            
                            <button id="btn-one-way" class="btn"><?php echo __("ida") ?></button>
                            <button id="btn-round-trip" class="btn"><?php echo __("ida y vuelta") ?></button>
                        </div>                        
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div id="info-2" class="alert navbar-inner-2 hide" style="padding: 10px 16px 10px 14px;">
                    <div>
                        <div style="color: #0088CC; font-size: 11px">
                            <i class="fleft" style="width: 45px;"><?php echo __("ida") ?></i>
                            <div class="fleft" style="margin-left: 20px;" id="td_available_schedule_ow">
                            </div>
                            <div class="fright">
                                <span><i id="tooltip_help_schedule" class="icon-info-sign"></i></span>
                            </div>
                        </div>
                        <div style="clear: both"></div>
                        <div style="margin-top: 5px;">
                            <div class="fleft">
                                <h5 style="font-weight: normal" class="fleft mright10"><?php echo __("Numero de Pasajeros") ?>: </h5>
                                <select id="cmb_pax_oneway" name="cmb_pax_oneway">
                                    <option></option>
                                </select>                                
                                <span class="hide"><i class="icon-info-sign" title="<?php echo __("El precio referido es por persona") ?>"></i></span>
                            </div>
                            <div class="fleft" style="margin-left: 25px;">
                                <h5 style="width: 98px; font-weight: normal" class="fleft mright10"><?php echo __("Fecha de ida") ?>: </h5>
                                <div id="div_datetime_travel_ow" class="input-append date">
                                    <input id="input_datetime_travel_ow" type="text">
                                    <span class="add-on"><i class="icon-th"></i></span>                                
                                </div>
                            </div>
                            <div class="fright" style="margin-right: 7px; margin-top: 2px">
                                <a href="#" role="button" id="btn_add_oneway" class="btn btn-small" title="<?php echo __("Enviar información") ?>">
                                    <i class="icon-share"></i><?php echo __("Enviar") ?>
                                </a>                    
                            </div>                        
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <hr id="hr_separator" class="hide">
                    <div id="div_show_roundtrip" class="hide">
                        <div style="color: #0088CC; font-size: 11px">
                            <i class="fleft" style="width: 45px;"><?php echo __("vuelta") ?></i>
                            <div class="fleft" style="margin-left: 20px;" id="td_available_schedule_rt"></div>
                        </div>
                        <div style="clear: both"></div>
                        <div class="fleft">
                            <h5 style="font-weight: normal" class="fleft mright10"><?php echo __("Numero de Pasajeros") ?>: </h5>
                            <select id="cmb_pax_roundtrip" name="cmb_pax_oneway">
                                <option></option>
                            </select>
                            <span class="hide"><i class="icon-info-sign" title="<?php echo __("El precio referido es por persona") ?>"></i></span>
                        </div>                        
                        <div class="fleft" style="margin-left: 25px;">
                            <h5 style="width: 98px;font-weight: normal" class="fleft mright10"><?php echo __("Fecha de vuelta") ?>: </h5>
                            <div id="div_datetime_travel_rt" class="input-append date">
                                <input id="input_datetime_travel_rt" type="text">
                                <span class="add-on"><i class="icon-th"></i></span>                                
                            </div>
                        </div>
                        <div class="fright" style="margin-right: 7px; margin-top: 2px">
                            <a href="#" role="button" id="btn_add_turn" class="btn btn-small" title="<?php echo __("Enviar información") ?>">
                                <i class="icon-share"></i><?php echo __("Enviar") ?>
                            </a>                    
                        </div>                        
                    </div>
                    <div style="clear: both;"></div>                    
                </div>
                <div id="info-3" class="hide" style="margin-top: 10px;">
                    <div id="msg_empty_table" class="alert borde hide">
                        <span><?php echo __("Por favor seleccione una cantidad de pasajeros") ?></span>
                    </div>                
                    <table class="table navbar-inner-2 table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo __("Origen - Destino") ?></th>
                                <!--<th><?php //echo __("Descripcion")       ?></th>-->
                                <th><?php echo __("Tipo") ?></th>
                                <th><?php echo __("Fecha") ?></th>
                                <th><?php echo __("Pax") ?></th>
                                <th><?php echo __("P. U") ?></th>
                                <th><?php echo __("P. T") ?></th>
                                <th><?php echo __("Acciones") ?></th>
                            </tr>
                        </thead>
                        <tbody id="tbody"></tbody>
                    </table>
                    <a style="margin-left: 5px;" href="#" role="button" id="btn_add_all" class="fright btn btn-small" data-toggle="modal" title="<?php echo __("Agrega al carrito todos los viajes seleccionados") ?>">
                        <i class="icon-cog"></i><?php echo __("Agregar Todo") ?>
                    </a>
                    <div style="margin-right: 300px;" class="fright fweight" id="btn_add_all">
                        <span>Total :</span><span class="mleft10" id="span_amount_total_pre_card"></span><span style="margin-left: 3px;">$</span>
                    </div>
                    <a href="#" role="button" id="btn_clear_all" class="fleft btn btn-small" data-toggle="modal" title="<?php echo __("Elimina todos los viajes seleccionados") ?>">
                        <i class="icon-cog"></i><?php echo __("Limpiar Todo") ?>
                    </a>
                </div>
            </div>
        </div>
        <div id="info-4" class="span3 columns hide">
            <h5 class="price" style="margin-top: 0px;"><?php echo __("Carrito de Compras") ?></h5>
            <div id="msg_empty_card" class="alert borde">
                <span><?php echo __("Aun no se ha añadido ninguna compra") ?></span>
            </div>
            <div id="div_rate_card" style="font-size: 12.5px; line-height: 17px;"></div>
            <a id="btn_confirm_payment" href="#" role="button" class="fright btn btn-small btn-info hide" style="margin-left: 2px; color: #222222" data-toggle="modal" title="<?php echo __("Continuar con la venta") ?>">
                <i class="icon-check mleft3"></i><?php echo __("Continuar") ?>
            </a>
            <a id="btn_close_card" href="#modal_hour" role="button" class="fright btn btn-small hide" data-toggle="modal" title="<?php echo __("Eliminar carrito de compras") ?>">
                <i class="icon-remove-circle mleft3"></i><?php echo __("Cerrar") ?>
            </a>            
            <div id="div_amount_post_card" class="hide fweight mleft6">
                <span><?php echo __("Total") ?> :</span><span class="mleft10" id="span_amount_total_post_card"></span><span class="mleft3">$</span>
            </div>
        </div>        
    </div>    
</div>

<div id="step-2" class="hide">
    <div class="page-header well" style="font-size: 12px; padding: 5px;">
        <span style="margin-left: 10px; line-height: 20px;">
            <i><?php echo __("Para concretar la venta, por favor registrar los siguientes datos..."); ?></i>
            <i class="fright icon-question-sign" style="margin-right: 12px; margin-left: 3px;" title="<?php echo __("Ayuda") ?>"></i>
            <i class="fright icon-calendar" style="margin-left: 3px;" title="<?php echo __("Calendario") ?>"></i>
            <i class="fright icon-wrench" style="margin-left: 3px;" title="<?php echo __("Configuracion") ?>"></i>
            <i id="btn_clean_all_payment" class="fright icon-certificate" style="margin-left: 3px;" title="<?php echo __("Eliminar carrito de compras") ?>"></i>
            <i id="btn_return_to_select_travels_from_payment" class="fright icon-tags" title="<?php echo __("Volver a seleccion de viajes") ?>"></i>
        </span>
    </div>
    <div class="accordion" id="accordion2" style="margin-top: 8px;">
        <div class="accordion-group hide" id="div_accordion_owner">
            <div class="accordion-heading">
                <a class="accordion-toggle" id="div_collapse_zero" data-toggle="collapse" data-parent="#accordion2" href="#collapseZero">
                    <i class="icon-user" style="margin-right: 2px;"></i>
                    <?php echo __("Datos de la Reserva"); ?>
                </a>
            </div>
            <div id="collapseZero" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <div id="dates_owner">
                        <form id="listOwnerDetail">
                            <div id="owner_details">
                                <div>
                                    <p class="mleft10 fweight">
                                        * <span><?php echo __("Nombre del representante de la venta");?> </span>
                                        <!--<span class="color-desc">MCO to Hotels in Cocoa Beach</span>-->
                                    </p>
                                    <div class="mleft10">
                                        <div>
                                            <div class="cboth"></div>
                                            <div>
                                                <table id="table_owner" class="table table-hover" style="display: block">
                                                    <thead>
                                                        <tr>
                                                            <!--<td></td>-->
                                                            <td class="width100">Title</td>
                                                            <td class="width210">First Name</td>
                                                            <td class="width210">Initial</td>
                                                            <td class="width210">Last Name</td>
                                                            <td class="width210">D.O.B.</td>
                                                            <td class="width100">Gender</td>
                                                            <td>Options</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <!--<td></td>-->
                                                            <td>
                                                                <select id="owner_title" class="span1" name="owner_title">
                                                                    <option>Mr</option>
                                                                    <option>Mrs</option>
                                                                    <option>Ms</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input id="owner_firstname" class="input-medium required" type="text" name="owner_firstname">
                                                                <span class="fweight mleft2 color-req cpointer" title="Required">*</span>
                                                            </td>
                                                            <td>
                                                                <input id="owner_middlename" class="input-medium" type="text" name="owner_middlename">
                                                            </td>
                                                            <td>
                                                                <input id="owner_lastname" class="input-medium required" type="text" name="owner_lastname">
                                                                <span class="fweight mleft2 color-req cpointer" title="Required">*</span>
                                                            </td>
                                                            <td>
                                                                <input id="owner_dob" class="form_datetime1" type="text" name="owner_dob">
                                                            </td>
                                                            <td>
                                                                <select id="owner_gender" class="span1" name="owner_gender">
                                                                    <option>Male</option>
                                                                    <option>Female</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <i class="icon-trash" title="Delete Row"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="text-align: center;">
                                    <a id="continue_owner" class="btn btn-info" style="color: #222222" role="button">
                                        <i class="icon-check"></i>
                                        Continue
                                    </a>
                                </div>
                            </div>
                        </form>                        
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-group hide"  id="div_accordion_all_travelers">
            <div class="accordion-heading">
                <a class="accordion-toggle" id="div_collapse_one" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                    <i class="icon-user" style="margin-right: 2px;"></i>
                    <?php echo __("Datos Personales"); ?>
                </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
                <div class="accordion-inner">
                    <form id="listPersonDetail">                
                        <div id="personal_details"></div>
                        <div style="text-align: center;">
                            <a id="continue_pd" role="button" class="btn btn-info" style="color: #222222">
                                <i class="icon-check"></i>
                                <?php echo __("Continuar") ?>
                            </a>
                        </div>
                    </form>                                
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" id="div_collapse_two" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                    <i class="icon-exclamation-sign" style="margin-right: 2px;"></i>
                    <?php echo __("Datos Opcionales"); ?>
                </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                    <form id="optionalDates">
                        <div id="airline_details"></div>
                        <hr id="hr_observations" class="hide" style="margin: -18px 0 25px;"/>
                        <div class="mleft10">
                            <div class="fleft fweight width230 mleft10"><span style="margin-right: 3px;">*</span><?php echo __("Observaciones") ?></div>
                            <div class="fleft">
                                <textarea name="text_observations" id="text_observations" cols="50" rows="5" style="width: 150%; height: 50px" placeholder="<?php echo __("Ingrese su observacion aqui...") ?>"></textarea>
                            </div>
                            <div class="cboth"></div>
                        </div>
                        <div style="text-align: center;margin-bottom: 10px; margin-top: 10px;">
                            <a id="continue_od" class="btn btn-info" style="color: #222222">
                                <i class="icon-check"></i>
                                <?php echo __("Continuar") ?>
                            </a>
                        </div>                         
                    </form>
                </div>               
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <a class="accordion-toggle" id="div_collapse_three" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                    <i class="icon-shopping-cart" style="margin-right: 2px;"></i>
                    <?php echo __("Datos del pago") ?>
                </a>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                    <?php include Kohana::find_file('views', 'shared/payment') ?>
                </div>
            </div>  
        </div>
    </div>
</div>