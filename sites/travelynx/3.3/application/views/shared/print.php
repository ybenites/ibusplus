<div id="modal_print" class="modal hide">    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Confirmacion de Impresion") ?></h3>
    </div>
    <div class="modal-body">
        <p><i><?php echo __("Desea Imprimir los Tickets Vendidos"); ?>:</i></p>
        <ul id="printTickets">
            <li><a style="cursor: pointer;" id="btn_printTicketOne"><?php echo __("Si, Imprimir"); ?></a></li>
            <li><a style="cursor: pointer;" id="btn_printTicketTwo"><?php echo __("Si, Imprimir y enviar Email"); ?></a></li>
            <li><a style="cursor: pointer;" id="btn_printTicketThree"><?php echo __("Solo enviar Email"); ?></a></li>
            <li><a style="cursor: pointer;" id="btn_printTicketFour"><?php echo __("No, Cancelar Impresión"); ?></a></li>  
        </ul>
    </div>    
</div>