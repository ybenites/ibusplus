<script type="text/javascript">
    var q_web_sale_code_success = 441;
    var q_web_sale_jquery_params = '?jsonp_callback=?';
    var date_now = new Date();
    var array_sede = new Array();
    function fnTravelynxLoadDepartures() {
        $.ajax({
            url: 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/public/websale/getLocations' + q_web_sale_jquery_params,
            async: false,
            type: 'GET',
            dataType: 'jsonp',
            success: function(j_response) {
                if (j_response.code == q_web_sale_code_success) {
                    $.each(j_response.data, function(i, value) {
                        $('#cmb_location_departure').append('<option value="' + value.idloc + '">' + value.nameloc + '</option>');
                        $('#cmb_location_arrival').append('<option value="' + value.idloc + '">' + value.nameloc + '</option>');
                    });
                }
            }
        });
    }
    function fnTravelynxLoadServices() {
        $.ajax({
            url: 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/public/websale/getServices' + q_web_sale_jquery_params,
            async: false,
            type: 'GET',
            dataType: 'jsonp',
            success: function(j_response) {
                if (j_response.code == q_web_sale_code_success) {
                    $.each(j_response.data, function(i, value) {
                        $('#cmb_service_departure').append('<option value="' + value.service_id + '">' + value.service_name + '</option>');
                        $('#cmb_service_arrival').append('<option value="' + value.service_id + '">' + value.service_name + '</option>');
                    });
                }
            }
        });
    }
    function fnTravelynxLoadHeadquarter() {
        $.ajax({
            url: 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/public/websale/getHeadquarter' + q_web_sale_jquery_params,
            async: false,
            type: 'GET',
            dataType: 'jsonp',
            success: function(j_response) {
                if (j_response.code == q_web_sale_code_success) {
                    $.each(j_response.data, function(i, value) {
                        var as = new Array();
                        as['idsede'] = value.idsede;
                        as['namesede'] = value.namesede;
                        as['idloc'] = value.idloc;
                        as['idserv'] = value.idserv;
                        array_sede.push(as);
                        $('#cmb_headquarter_departure').append('<option data-idserv="' + value.idserv + '" data-idloc="' + value.idloc + '" value="' + value.idsede + '">' + value.namesede + '</option>');
                        $('#cmb_headquarter_arrival').append('<option data-idserv="' + value.idserv + '" data-idloc="' + value.idloc + '" value="' + value.idsede + '">' + value.namesede + '</option>');
                    });
                }
            }
        });
    }
    function fnSelectToSelect2(thes, label) {
        $(thes).select2({
            placeholder: label,
            width: 'resolve',
            allowClear: true
        });
    }
    function fnSedeDepartureArrival(thes, cmb) {
        var id = "#cmb_headquarter_" + cmb + " option:selected";
        console.log($(id).data('idloc'));
        $("#cmb_location_" + cmb).select2("val", $(id).data('idloc'));
        //$("#cmb_location_" + cmb).val($(id).data('idloc'));
        $("#cmb_service_" + cmb).select2("val", $(id).data('idserv'));
        //$("#cmb_service_" + cmb).val($(id).data('idserv'));
    }
    function fnLocationDepartureArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idserv = $("#cmb_service_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idserv == "") {
                if (o['idloc'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idloc'] == opt && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idserv'] == idserv) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }
    function fnServiceDepartureAndArrival(thes, cmb) {
        var opt = $(thes).val();
        $("#cmb_headquarter_" + cmb).html('').append('<option></option>').select2("val", "");
        var idloc = $("#cmb_location_" + cmb).val();
        $.each(array_sede, function(i, o) {
            if (idloc == "") {
                if (o['idserv'] == opt) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "") {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            } else {
                if (o['idserv'] == opt && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                } else if (opt == "" && o['idloc'] == idloc) {
                    $('#cmb_headquarter_' + cmb).append('<option value="' + o['idsede'] + '" data-idloc="' + o['idloc'] + '" data-idserv="' + o['idserv'] + '">' + o['namesede'] + '</option>')
                }
            }
        });
    }
    $(document).ready(function() {
        fnTravelynxLoadDepartures();
        fnTravelynxLoadServices();
        fnTravelynxLoadHeadquarter();
        fnSelectToSelect2("#cmb_location_departure", 'Location departure');
        fnSelectToSelect2("#cmb_service_departure", 'Service departure');
        fnSelectToSelect2("#cmb_headquarter_departure", 'Pickup departure');

        fnSelectToSelect2("#cmb_location_arrival", 'Location arrival');
        fnSelectToSelect2("#cmb_service_arrival", 'Service arrival');
        fnSelectToSelect2("#cmb_headquarter_arrival", 'Pickup arrival');
        $("#cmb_location_departure").on('change', function() {
            fnLocationDepartureArrival(this, 'departure');
        });
        $("#cmb_location_arrival").on('change', function() {
            fnLocationDepartureArrival(this, 'arrival');
        });

        $("#cmb_service_departure").on('change', function() {
            fnServiceDepartureAndArrival(this, "departure");
        });

        $("#cmb_service_arrival").on('change', function() {
            fnServiceDepartureAndArrival(this, "arrival");
        });
        $("#cmb_headquarter_departure").on('change', function() {
            fnSedeDepartureArrival(this, "departure");
        });
        $("#cmb_headquarter_arrival").on('change', function() {
            fnSedeDepartureArrival(this, "arrival");
        });
        
        $("#frm_rate_l_").show();
    });
</script>
<style>
    select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"], .uneditable-input {
        font-size: 12px;
        padding: 0px;
    }
    .format_input{
        width: 210px;
        height: 15px;
    }

    div.select2-result-label{
        font-size: 11px;
    }

    .btn_format {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        background-color: #07345D;
        background-repeat: repeat-x;
        border-color: #CCCCCC #CCCCCC #B3B3B3;
        border-image: none;
        border-radius: 5px 5px 5px 5px;
        border-style: solid;
        border-width: 1px;
        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
        color: #FFFFFF;
        cursor: pointer;
        display: inline-block;
        float: right;
        font-size: 13px;
        font-weight: bold;
        line-height: 15px;
        margin-bottom: 0;
        margin-right: 20px;
        padding: 8px 20px;
        text-align: center;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
        vertical-align: middle;
    }    

</style>

<style>
    .quoteformprincipal {
        width: 500px;
        height: 152px;
        font-family: 'PT Sans', Verdana, Geneva, sans-serif;
        font-size: 13px;        
    }
    .left {
        float: left;
    }
</style>
<div id="quoteformprincipal" class="left quoteformprincipal">
    <form id="frm_rate_l_" method="POST" class="form-horizontal" action="http://<?php echo $_SERVER['SERVER_NAME'] ?>/public/websale/index" target="_blank" style="display: none;">          
        <div id="div_location_departure" style="float: left; margin-right: 60px; font-size: 11px;">
            <!--<div style="margin-bottom: 3px;">DEPARTURE</div>-->
            <div>
                <div>
                    <select id="cmb_location_departure" name="cmb_location_departure" class="format_input">
                        <option></option>
                    </select>                
                </div>
                <div style="margin-top: 5px;">
                    <select id="cmb_service_departure" name="cmb_service_departure" class="format_input">
                        <option></option>
                    </select>
                </div>
                <div style="margin-top: 5px; padding-bottom: 10px;">
                    <select id="cmb_headquarter_departure" name="cmb_headquarter_departure" class="format_input">
                        <option></option>
                    </select>
                </div>
                <div style="clear: both"></div>
            </div>                
        </div>
        <div id="div_arrival_departure" style="font-size: 11px;">            
            <!--<div style="margin-bottom: 3px;">ARRIVAL</div>-->
            <div>
                <div>
                    <select id="cmb_location_arrival" name="cmb_location_arrival" class="format_input">
                        <option></option>
                    </select>                
                </div>
                <div style="margin-top: 5px;">
                    <select id="cmb_service_arrival" name="cmb_service_arrival" class="format_input">
                        <option></option>
                    </select>
                </div>
                <div style="padding-bottom: 10px; margin-top: 5px;">
                    <select id="cmb_headquarter_arrival" name="cmb_headquarter_arrival" class="format_input">
                        <option></option>
                    </select>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
        <div>
            <button type="submit" class="btn_format">SUBMIT QUOTE</button>
        </div>
    </form>
</div>