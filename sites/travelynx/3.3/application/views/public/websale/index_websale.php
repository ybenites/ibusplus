<script type="text/javascript">
    var websale = 1;
    // Lenguaje
    var l_spanish = "<?php echo __("Español") ?>";
    var l_english = "<?php echo __("Ingles") ?>";
    var cacheUsers = {}, lastXhrUsers = null;
    var requestUsers = 0;

    var s_not_result_found = '<?php echo __("Sin Resultados"); ?>';

    var s_url_sale_getLocationsDepartureByService = '/public/websale/getLocationsDepartureByService.json';
    var s_url_sale_getLocationsArrivalByDeparture = '/public/websale/getLocationsArrivalByDeparture.json';
    var s_url_sale_getRoutesByParameters = '/public/websale/getRoutesByParameters.json';
    var s_url_sale_get_price_by_route_and_schedule = '/public/websale/getPriceByRouteAndSchedule.json';
    var s_url_get_temp_sales_cart = '/public/websale/getTempSalesCart.json';
    var s_url_save_temp_sales_cart = '/public/websale/saveTempSalesCart.json';
    var s_url_delete_temp_sales_cart = '/public/websale/deleteTempSalesCart.json';
    var s_url_save_sales = '/public/websale/saveSales.json';
    var s_url_get_validate_zipcode = '/public/websale/getValidateZipcode.json';
    var s_url_delete_my_shopping_cart = '/public/websale/deleteMyShoppingCart.json';
    var s_url_sale_getHotelParams = '/public/websale/getHotelParams.json';

    var websale = 1;
    var array_sede = new Array();
    var array_airline = new Array();
    var array_search = new Array();
    var date_now = new Date();

<?php foreach ($SEDE as $key => $value) : ?>
        var as = new Array();
        as['idsede'] = "<?php echo $value['idsede'] ?>";
        as['namesede'] = "<?php echo $value['namesede'] ?>";
        as['idloc'] = "<?php echo $value['idloc'] ?>";
        as['idserv'] = "<?php echo $value['idserv'] ?>";
        array_sede.push(as);
<?php endforeach; ?>
<?php foreach ($AIRLINE as $air) : ?>
        var al = new Array();
        al['idairline'] = "<?php echo $air['idairline'] ?>";
        al['nameairline'] = "<?php echo $air['name'] ?>";
        array_airline.push(al);
<?php endforeach; ?>
<?php foreach ($SEARCH as $key => $sch) : ?>
        array_search['<?php echo $key ?>'] = "<?php echo $sch ?>";
<?php endforeach; ?>

</script>                          

<div id="full_content" style="display: none">
    <!--    <div class="btn-group">
            <button id="btn_cleanMyShoppingCart" type="button" class="btn" title="<?php //echo __("Limpiar Carrito de Compras")                              ?>"><i class="icon-refresh"></i></button>
            <button type="button" class="btn"><i class="icon-remove"></i></button>
        </div>-->
    <div style="position: relative;">
        <?php include Kohana::find_file('views', 'shared/itinerary') ?>
    </div>
</div>
<?php include Kohana::find_file('views', 'shared/print') ?>