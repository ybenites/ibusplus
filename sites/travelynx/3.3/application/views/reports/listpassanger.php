<script type="text/javascript">
    var s_place_holder_route = "<?php echo __('Seleccione una Ruta') ?>";
</script>
<div id="show_route">
    <h2 class="h2" style="float: left; margin-right: 15px ;"><small><?php echo __('Ruta') ?>:</small></h2>
    <div class="controls" style="float: left;margin-left:257px;">
        <select id="listRoute" name="listRoute" style="width: 250px;">
            <option></option>
            <?php foreach ($list_AllRoute as $key => $route): ?>
                <option value="<?php echo $route['route_id'] ?>"><?php echo $route['route_description'] ?></option>
            <?php endforeach; ?>                        
        </select>
    </div>
    <div style="clear: both"></div>
</div>
<div class="control-group">
    <div>
        <?php include Kohana::find_file('views', 'shared/abstractreport') ?>
    </div>
</div>


