<script type="text/javascript">
    var s_place_holder_office = "<?php echo __('Seleccione una Oficina') ?>";
    var s_office = "<?php echo __('Debe seleccionar una Oficina') ?>";
    
</script>
<div>
    <h2 class="h2" style="float: left; margin-right: 10px ;"><small><?php echo __('Oficina') ?>:</small></h2>
    <div class="controls" style="float: left;margin-left:260px;">
        <select id="listOffice" name="listOffice" style="width: 245px;">
            <option></option>
           <?php foreach($BTS_A_OFFICE_LIST as $o_office):?>
                        <option value="<?php echo $o_office->idOffice?>"><?php echo __($o_office->name)?></option>
                        <?php endforeach?>                     
        </select>
    </div>
    <div style="clear: both"></div>
</div>
<div class="control-group">
    <div>
        <?php include Kohana::find_file('views', 'shared/abstractreport') ?>
    </div>
</div>

