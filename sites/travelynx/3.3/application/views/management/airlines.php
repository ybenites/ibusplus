<script type="text/javascript">
    var a_jqgrid_airlines_col_names = [
        'ID'
                , '<?php echo __("Nombre") ?>'                  
                , '<?php echo __("Estado") ?>'  
                , '<?php echo __("Usuario Creo") ?>'  
                , '<?php echo __("Fecha Craeacion") ?>'  
                , '<?php echo __("Usuario Edito") ?>'  
                , '<?php echo __("Fecha Edicion") ?>'  
                , '<?php echo __("Acciones") ?>'
    ];
    
    var a_jqgrid_airlines_col_model = [
        {name: 'idAirlines', index: 'idAirlines', width: 1, hidden: true, key: true},
        {name: 'name', index: 'name', width:200,searchoptions:{sopt:['cn']}},        
        {name: 'status', index: 'status',hidden:true,search:false},
        {name: 'userCreate', index: 'userCreate', width:150,search:false},
        {name: 'creationDate', index: 'creationDate', width:100,search:false},
        {name: 'userLastChange', index: 'userLastChange', width:200,search:false},
        {name: 'lastChangeDate', index: 'lastChangeDate', width:100,search:false},
        {name: 'actions', index: 'actions', width: 160, sortable: false, align: 'center',search:false}
    ];
    var s_url_airlines_list='list.json';
    var s_url_airlines_save='create.json';
    var s_url_airlines_update='update.json';
    var s_url_airlines_get='get.json';
    var s_url_airlines_delete='delete.json';
    var s_validate_required_fairlines_name="<?php echo __('Nombre Requerido')?>";
    var s_airlines_saved='<?php echo __("Linea Aerea Guardada")?>';
    var s_airlines_delete='<?php echo __("Linea Aerea Eliminada")?>';
</script>
<div>
    <div class="control-group controls-row">
        <a href="#modal_airlines" role="button" class="btn btn-small" data-toggle="modal">
            <i class="icon-plus"></i><?php echo __("Agregar") ?>
        </a>        
    </div>
    <div class="control-group controls-row">
        <table id="jqgrid_airlines"></table>
        <div id="jqgrid_airlines_pager"></div>
    </div>
</div>

<div id="modal_airlines" class="modal hide" data-backdrop="static">
    <div class="modal-header" style="text-align: center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Lineas Aereas") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_airlines" class="form-horizontal">
            <input id="fairlines_id" name="fairlines_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fairlines_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fairlines_name" name="fairlines_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_airlines_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>