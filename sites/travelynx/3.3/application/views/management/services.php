<script type="text/javascript">
    var a_jqgrid_services_col_names = [
        'ID'
                , '<?php echo __("Nombre") ?>'  
                , '<?php echo __("Descripcion") ?>'  
                , '<?php echo __("Estado") ?>'  
                , '<?php echo __("Usuario Creo") ?>'  
                , '<?php echo __("Fecha Craeacion") ?>'  
                , '<?php echo __("Usuario Edito") ?>'  
                , '<?php echo __("Fecha Edicion") ?>'  
                , '<?php echo __("Acciones") ?>'
    ];
    
    var a_jqgrid_services_col_model = [
        {name: 'idServices', index: 'idServices', width: 1, hidden: true, key: true},
        {name: 'name', index: 'name', width:200,searchoptions:{sopt:['cn']}},
        {name: 'description', index: 'description', width: 200,searchoptions:{sopt:['cn']}},
        {name: 'status', index: 'status',hidden:true,search:false},
        {name: 'userCreate', index: 'userCreate', width:150,search:false},
        {name: 'creationDate', index: 'creationDate', width:100,search:false},
        {name: 'userLastChange', index: 'userLastChange', width:200,search:false},
        {name: 'lastChangeDate', index: 'lastChangeDate', width:100,search:false},
        {name: 'actions', index: 'actions', width: 160, sortable: false, align: 'center',search:false}
    ];
    
    var s_url_service_save = 'create.json';
    var s_url_services_list = 'list.json';
    var s_url_service_update = 'update.json';
    var s_url_service_get = 'get.json';
    var s_url_service_delete= 'delete.json';
    
    var s_validate_required_fservice_name="<?php echo __('Nombre Requerido')?>";
    var s_validate_required_fservice_description="<?php echo __('Descripción Requerido')?>";
    var s_service_saved='<?php echo __("Servicio Guardado")?>';
    var s_service_delete='<?php echo __("Servicio Eliminado")?>';
    var s_dialog2_button_label_confirmation_delete_text='<?php echo __("Desea Eliminar Registro?") ?>';
</script>
<style type="text/css">    
</style>
<div>
    <div class="control-group controls-row">
        <a href="#modal_service" role="button" class="btn btn-small" data-toggle="modal">
            <i class="icon-plus"></i><?php echo __("Agregar") ?>
        </a>        
    </div>
    <div class="control-group controls-row">
        <table id="jqgrid_services"></table>
        <div id="jqgrid_services_pager"></div>
    </div>
</div>
<div id="modal_service" class="modal hide" data-backdrop="static">
    <div class="modal-header" style="text-align: center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Servicio") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_service" class="form-horizontal">
            <input id="fservice_id" name="fservice_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fservice_name"><?php echo __("Nombre")?></label>
                <div class="controls">
                    <input type="text" id="fservice_name" name="fservice_name" placeholder="<?php echo __("Nombre")?>"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fservice_description"><?php echo __("Descripción")?></label>
                <div class="controls">
                    <input type="text" id="fservice_description" name="fservice_description" placeholder="<?php echo __("Descripción")?>"/>
                </div>
            </div>
           
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_service_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>