<script type="text/javascript">
    var s_url_sale_getLocationsDepartureByService = 'getLocationsDepartureByService.json';
    var s_url_sale_getLocationsArrivalByDeparture = 'getLocationsArrivalByDeparture.json';
    var s_url_sale_getRoutesByParameters = 'getRoutesByParameters.json';
    var s_url_sale_get_price_by_route_and_schedule = '/management/sale/getPriceByRouteAndSchedule.json';
    var s_url_get_temp_sales_cart = '/management/sale/getTempSalesCart.json';
    var s_url_save_temp_sales_cart = '/management/sale/saveTempSalesCart.json';
    var s_url_delete_temp_sales_cart = '/management/sale/deleteTempSalesCart.json';
    var s_url_save_sales = '/management/sale/saveSales.json';
    var s_url_get_validate_zipcode = '/management/sale/getValidateZipcode.json';
    var s_url_delete_my_shopping_cart = '/management/sale/deleteMyShoppingCart.json';
    var s_url_sale_getHotelParams = '/management/sale/getHotelParams.json';
    var s_url_get_schedule_by_route = '/management/sale/getScheduleByRoute.json';
    var s_url_get_user_paypal = '/management/sale/getUserPaypal.json';

    var websale = 0;
    var array_sede = new Array();
    var array_airline = new Array();
    var date_now = new Date();
    var opt_all_travelers = <?php echo $OPTION_RECORD_ALL_TRAVELERS ?>;
<?php foreach ($SEDE as $key => $value) : ?>
        var as = new Array();
        as['idsede'] = "<?php echo $value['idsede'] ?>";
        as['namesede'] = "<?php echo $value['namesede'] ?>";
        as['idloc'] = "<?php echo $value['idloc'] ?>";
        as['idserv'] = "<?php echo $value['idserv'] ?>";
        array_sede.push(as);
<?php endforeach; ?>
<?php foreach ($AIRLINE as $air) : ?>
        var al = new Array();
        al['idairline'] = "<?php echo $air['idairline'] ?>";
        al['nameairline'] = "<?php echo $air['name'] ?>";
        array_airline.push(al);
<?php endforeach; ?>
</script>

<div id="full_content" style="display: none">
<!--        <div class="btn-group">
            <button id="btn_cleanMyShoppingCart" type="button" class="btn" title="<?php //echo __("Limpiar Carrito de Compras")                        ?>"><i class="icon-refresh"></i></button>
            <button type="button" class="btn"><i class="icon-remove"></i></button>
        </div>-->
    <div style="position: relative;">
        <?php include Kohana::find_file('views', 'shared/itinerary') ?>
    </div>
</div>
<?php include Kohana::find_file('views', 'shared/print') ?>


<!--<div class="well">
    <input type="text" class="span3" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>
</div>
<div class="well">
    <input  id="manufacturer" type="text" class="span3" style="margin: 0 auto;" data-provide="typeahead" data-items="4" data-source=''>
</div>-->