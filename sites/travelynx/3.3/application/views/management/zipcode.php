<script type="text/javascript">
    var a_jqgrid_zipcode_col_names=[
        'ID'
        , '<?php echo __("number") ?>'   
        , '<?php echo __("Usuario Creo") ?>'   
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_zipcode_col_model=[
        {name: 'idZipCode', index: 'idZipCode', width: 1, hidden: true, key: true},
        {name: 'number', index: 'number',width:150,align: 'center',searchoptions:{sopt:['cn']}},       
        {name: 'userCreate', index: 'userCreate', width:150,search:false},     
        {name: 'actions', index: 'actions', width:150, sortable: false, align: 'center',search:false}
    ];
    var s_url_zipcode_save = 'create.json';   
    var s_url_zipcode_list = 'list.json';
    var s_url_zipcode_update = 'update.json';
    var s_url_zipcode_get = 'get.json';
    var s_url_zipcode_delete= 'delete.json';
    
    var s_validate_required_fzipcode_number="<?php echo __('Numero Zip Code Requerido') ?>";
    var s_zipcode_delete='<?php echo __("Zip Code Eliminado") ?>';
    var s_zipcode_saved='<?php echo __("Zip Code Guardado") ?>';
</script>
<style type="text/css"></style>
<div class="controls-row">
    <div class="control-group controls-row">
        <a href="#modal_zipcode" role="button" class="btn btn-small" data-toggle="modal">
            <i class="icon-plus"></i><?php echo __("Agregar") ?>
        </a>        
    </div>
    <div class="control-group controls-row">
        <table id="jqgrid_zipcode"></table>
        <div id="jqgrid_zipcode_pager"></div>
    </div>
</div>
<div id="modal_zipcode" class="modal hide" data-backdrop="static">
    <div class="modal-header" style="text-align: center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Zip Code") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_zipcode" class="form-horizontal">
            <input id="fzipcode_id" name="fzipcode_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="fzipcode_number"><?php echo __("Numero")?></label>
                <div class="controls">
                    <input type="text" id="fzipcode_number" name="fzipcode_number" placeholder="<?php echo __("Numero")?>"/>
                </div>
            </div>           
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_zipcode_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>