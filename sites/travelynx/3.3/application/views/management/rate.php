<script type="text/javascript">
    var s_url_update_status_hour = '/management/rate/updateStatusHour.json';   
    var s_url_rate_getLocationsArrival='getLocationsArrival.json';
    var s_url_rate_getRoutesByParameters='getRoutesByParameters.json';
    var s_url_save_rate = '/management/rate/saveRate.json';   
    var s_url_get_price = '/management/rate/getListPricesByRoute.json';
    var s_url_route_getLocationsArrival='getLocationsArrival.json';    
    var s_url_sale_getLocationsDepartureByService='getLocationsDepartureByService.json';    
    var s_url_delete_rate = '/management/rate/deleteRate.json';
    
    var s_validate_required_froute_service="<?php echo __("Seleccione Servicio") ?>";
    var s_validate_required_froute_departure="<?php echo __("Seleccione Salida") ?>";
    var s_validate_required_froute_arrival="<?php echo __("Seleccione Llegada") ?>";
    
    var s_txt_schedule="<?php echo __("Horarios") ?>";
    
    var s_jqgrid_actions = "<?php echo __("Acciones"); ?>";
    var s_jqgrid_title_save = "<?php echo __("Guardar"); ?>";
    var s_jqgrid_title_update = "<?php echo __("Actualizar"); ?>";
    var s_jqgrid_title_hide = "<?php echo __("Esconder"); ?>";
    var s_jqgrid_title_delete = "<?php echo __("Eliminar"); ?>";
    var s_jqgrid_title_show_schedule = "<?php echo __("Mostrar Horarios"); ?>";
    var s_jqgrid_title_show_filter = "<?php echo __("Mostrar Busqueda"); ?>";
    var s_jqgrid_title_add_schedule = "<?php echo __("Agregar Horarios"); ?>";
    
    var s_msg_warning_schedule = "<?php echo __("La hora de inicio no puede ser igual o menor a la hora final"); ?>";
    var s_msg_warning_not_schedule_for_route = "<?php echo __("No existe este horario para la presente ruta"); ?>";    
    
    var s_txt_location = '<?php echo __("Locacion") ?>';
    var s_txt_service = '<?php echo __("Servicio") ?>';
    var s_txt_headquarter = '<?php echo __("Sede") ?>';    
    
    var s_msg_ok_save = "<?php echo __("Los precios se guardaron correctamente"); ?>";
    var s_msg_ok_rate_delete = "<?php echo __("Las tarifas se eliminaron correctamente"); ?>";
    var s_msg_really_remove_this_rate = "<?php echo __("Esta seguro de eliminar esta tarifa?"); ?>";
    
    var s_max_pax = '<?php echo $OPTION_MAXIMUM_PAX; ?>';
    
    var t = new Array();
    var array_sede = new Array();
    
<?php foreach ($HOUR as $key => $value) : ?>
    <?php if ($value['status']): ?>
                var e = new Array();
                e['id'] = '<?php echo $value['idHour'] ?>';
                e['desc'] = '<?php echo $value['description'] ?>';
                t.push(e);
    <?php endif; ?>
<?php endforeach; ?>
    
<?php foreach ($SEDE as $key => $value) : ?>
        var as = new Array();
        as['idsede'] = "<?php echo $value['idsede'] ?>";
        as['namesede'] = "<?php echo $value['namesede'] ?>";
        as['idloc'] = "<?php echo $value['idloc'] ?>";
        as['idserv'] = "<?php echo $value['idserv'] ?>";
        array_sede.push(as);
<?php endforeach; ?>            

</script>
<style type="text/css">
    .icon-plus,.icon-tasks,.icon-minus,.icon-thumbs-up,.icon-pencil,.icon-trash,.icon-ban-circle,.icon-search,.i-txt{cursor: pointer}
    .width50{width: 50px}
    .width35{width: 35px}
    .width70{width: 70px}
    .width100{width: 100px}
    .mright10{margin-right: 10px;}
    .mleft5{margin-left: 5px}
    .mleft4{margin-left: 4px}
    .mleft2{margin-left: 2px}
    .mleft10{margin-left: 10px}
    .mtop5{margin-top: 5px}
    .mtop10{margin-top: 10px}
    .mtop15{margin-top: 15px}
    .div_ser{background-color: pink; margin-top: 10px}
    .left{float: left}
    .right{float: right}
    .margin0{margin: 0px;}
    .both{clear: both}
    .cblack{color: #525252}
    .fblack{font-weight: bold}
    .fhr{margin-top: -18px; color: #DDDDDD;background-color: #DDDDDD;height: 1px;border:none}
    .row-fluid {width: 95.5%}
    .circle{border-radius: 50%;width: 22px;height: 22px;background-color: #CFEAF2;float: right;margin-right: 5px}
    .i-text{line-height: 22px; margin-left: 4px}
    .table th, .table td {text-align: center}
    .selecth {background-color: #CFEAF2;border: 0 none}
    .color-active {background-color: #CFEAF2;}
    .color-deactive {background-color: #EEEEEE;}
    .text-dec-under {text-decoration: underline}
    .text-dec-under-color {color: #0088CC}
    .textaligncenter{text-align: center;}
    .alert {margin-bottom: 10px;background-color: #F5F5F5;border: 1px solid #DDDDDD}
    /*hr {margin: 15px 0;}*/ 
    .navbar-inner-2 {background-color: #FDFDFD;/*background-image: linear-gradient(to bottom, #FFFFFF, #FDFDFD);*/box-shadow: 0 1px 4px rgba(0, 0, 0, 0.067)}
    .well {padding: 10px}
    form {margin: 0 0 5px}
    .fweight{font-weight: bold;}
    .width172{width: 172px}
    .mleft8{margin-left: 8px}
    input[type="radio"], input[type="checkbox"] {margin: 0px 0 0;}
    .hover-div{
        background-color: #F5F5F5;
    }

</style>

<section>
    <div class="page-header well" style="font-size: 12px;">
        <form id="frm_rate_l">
            <div style="float: left;">
                <div style="margin-bottom: 3px;"><?php echo __("ORIGEN") ?></div>                
                <div>
                    <div style="float: left;">
                        <select id="cmb_location_departure" name="cmb_location_departure" class="width172">
                            <option></option>
                            <?php foreach ($LOCATION as $key => $loc): ?>
                                <option value="<?php echo $loc['idloc'] ?>"><?php echo $loc['nameloc'] ?></option>
                            <?php endforeach; ?>
                        </select>                
                    </div>
                    <div style="float: left;" class="mleft8">
                        <select id="cmb_service_departure" name="cmb_service_departure" class="width172">
                            <option></option>
                            <?php foreach ($SERVICE as $key => $ser): ?>
                                <option value="<?php echo $ser['service_id'] ?>"><?php echo $ser['service_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div style="float: left;" class="mleft8">
                        <select id="cmb_headquarter_departure" name="cmb_headquarter_departure" class="width172">
                            <option></option>                
                            <?php foreach ($SEDE as $key => $sede): ?>
                                <option value="<?php echo $sede['idsede'] ?>" data-idloc="<?php echo $sede['idloc'] ?>" data-idserv="<?php echo $sede['idserv'] ?>"><?php echo $sede['namesede'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div style="clear: both"></div>                    
                </div>                
            </div>
            <div style="float: left; margin-left: 15px;">            
                <div style="margin-bottom: 3px;"><?php echo __("DESTINO") ?></div>
                <div>
                    <div style="float: left;">
                        <select id="cmb_location_arrival" name="cmb_location_arrival" class="width172">
                            <option></option>
                            <?php foreach ($LOCATION as $key => $loc): ?>
                                <option value="<?php echo $loc['idloc'] ?>"><?php echo $loc['nameloc'] ?></option>
                            <?php endforeach; ?>
                        </select>                
                    </div>
                    <div style="float: left;" class="mleft8">
                        <select id="cmb_service_arrival" name="cmb_service_arrival" class="width172">
                            <option></option>      
                            <?php foreach ($SERVICE as $key => $ser): ?>
                                <option value="<?php echo $ser['service_id'] ?>"><?php echo $ser['service_name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div style="float: left;" class="mleft8">
                        <select id="cmb_headquarter_arrival" name="cmb_headquarter_arrival" class="width172">
                            <option></option>
                            <?php foreach ($SEDE as $key => $sede): ?>
                                <option value="<?php echo $sede['idsede'] ?>" data-idloc="<?php echo $sede['idloc'] ?>" data-idserv="<?php echo $sede['idserv'] ?>"><?php echo $sede['namesede'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div style="float: left; margin-left: 15px;">
                <div>
                    <a href="#" role="button" id="btn_search_location" class="btn btn-small" data-toggle="modal" style="padding: 5px 16px;">
                        <i class="icon-play-circle" style="margin-right: 3px;" title="<?php echo __("Buscar Itinerarios")?>"></i>
                    </a>
                </div>
                <div>
                    <a href="#modal_hour" role="button" id="btn_activate_schedule" class="btn btn-small" data-toggle="modal" style="padding: 5px 16px;">
                        <i class="icon-upload" style="margin-right: 3px;" title="<?php echo __("Activar Horarios") ?>"></i>
                    </a>
                </div>
            </div>
            <div style="clear: both"></div>
        </form>
    </div>
    <div id="div_service"></div>
    <div id="msg_no_found_route" class="alert alert-info hide" style="margin-top: -10px;">
        <strong><?php echo __("No se encontraron rutas!") ?></strong>
        <?php echo __("Porfavor vuelva a intentarlo de nuevo con otros parametros") ?>.
    </div>
</section>

<div id="modal_hour" class="modal hide" data-backdrop="static">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Activar Horario") ?></h3>
    </div>
    <div class="modal-body">
        <div class="bs-docs-example">
            <table id="table_hour" class="table table-striped">
                <thead>
                    <tr>
                        <th><?php echo __("Hora") ?></th>
                        <th><?php echo __("Estado") ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($HOUR as $key => $value): ?>
                        <tr>
                            <td class="hide"><?php echo $value['idHour'] ?></td>
                            <td><?php echo $value['description'] ?></td>
                            <td><input class="chk_hour" id="chk_hour_<?php echo $value['idHour'] ?>" type="checkbox" <?php if ($value['status']) echo "checked" ?>></td>
                        </tr>                    
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>      
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
    </div>
</div>    