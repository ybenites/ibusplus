<script type="text/javascript">
    var a_jqgrid_locations_col_names=[
        'ID'
        , '<?php echo __("Nombre") ?>'
        , '<?php echo __("Estado") ?>'  
        , '<?php echo __("Usuario Creo") ?>'  
        , '<?php echo __("Fecha Craeacion") ?>'  
        , '<?php echo __("Usuario Edito") ?>'  
        , '<?php echo __("Fecha Edicion") ?>'  
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_locations_col_model=[
        {name: 'idLocation', index: 'idLocation', width: 1, hidden: true, key: true},
        {name: 'name', index: 'lo.name', width:200,searchoptions:{sopt:['cn']}},        
        {name: 'status', index: 'status',hidden:true,search:false},
        {name: 'userCreate', index: 'userCreate', width:150,search:false},
        {name: 'creationDate', index: 'creationDate', width:100,search:false},
        {name: 'userLastChange', index: 'userLastChange', width:200,search:false},
        {name: 'lastChangeDate', index: 'lastChangeDate', width:100,search:false},
        {name: 'actions', index: 'actions', width: 160, sortable: false, align: 'center',search:false}
    ];
    
    var a_jqgrid_zipcode_col_names=[
        'ID'
        , '<?php echo __("number") ?>'  
        , '<?php echo __("idLocation") ?>'  
        , '<?php echo __("locacion") ?>'
        , '<?php echo __("Usuario Creo") ?>'   
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_zipcode_col_model=[
        {name: 'idZipCode', index: 'idZipCode', width: 1, hidden: true, key: true},
        {name: 'number', index: 'number',width:100,searchoptions:{sopt:['cn']}},
        {name: 'location_id', index: 'location_id',width:100,hidden: true},
        {name: 'location_name', index: 'lo.name', width:150,search:false},        
        {name: 'userCreate', index: 'userCreate', width:150,hidden: true,search:false},     
        {name: 'actions', index: 'actions', width:80, sortable: false, align: 'center',search:false}
    ];
    var a_jqgrid_sede_col_names=[
        'ID'
        , '<?php echo __("Nombre") ?>'  
        , '<?php echo __("idLocation") ?>'  
        , '<?php echo __("location_name") ?>'  
        , '<?php echo __("Locación") ?>'
        , '<?php echo __("Zip Code") ?>'
        , '<?php echo __("Servicio") ?>'
        , '<?php echo __("Dirección") ?>'
        , '<?php echo __("Telefono") ?>'
        , '<?php echo __("Fax") ?>'
        , '<?php echo __("Usuario Creo") ?>'   
        , '<?php echo __("Acciones") ?>'
    ];
    var a_jqgrid_sede_col_model=[
        {name: 'idSede', index: 'idSede', width: 1, hidden: true, key: true},
        {name: 'sede_name', index: 'se.name',width:100,searchoptions:{sopt:['cn']}},
        {name: 'location_id', index: 'location_id',width:100,hidden: true,search:false},
        {name: 'location_name', index: 'location_name',width:100,hidden: true,search:false},
        {name: 'sede_location', index: 'se.location', width:120,search:false},
        {name: 'sede_zipcode_number', index: 'zi.number', width:80,search:false},
        {name: 'sede_service_name', index: 'ser.name', width:100,search:false},
        {name: 'sede_address', index: 'se.address', width:120,search:false},
        {name: 'sede_phone', index: 'se.phone', width:70,search:false,hidden:true},
        {name: 'sede_fax', index: 'se.fax', width:80,search:false,hidden:false},        
        {name: 'userCreate', index: 'se.userCreate', width:150,search:false,hidden: true},     
        {name: 'actions', index: 'actions', width:80, sortable: false, align: 'center',search:false}
    ];
    var s_url_location_save = 'create.json';   
    var s_url_locations_list = 'list.json';
    var s_url_location_update = 'update.json';
    var s_url_location_get = 'get.json';
    var s_url_location_delete= 'delete.json';
    
    var s_url_zipcode_save='createzipcode.json';
    var s_url_zipcode_update='updateZipcode.json';
    var s_url_zipcode_delete='deleteZipcode.json';
    var s_url_zipcode_list='listZipcode.json';
    var s_url_zipcode_get='getZipcode.json';
    var s_url_zipcode_listZipCodeByLocation='listZipCodeByLocation.json';
    var s_url_sede_save='createSede.json';
    var s_url_sede_update='updateSede.json';
    var s_url_sede_list='listSede.json';
    var s_url_sede_get='getSede.json';
    var s_url_sede_delete='deleteSede.json';
    
    var s_validate_required_flocation_name="<?php echo __('Nombre Requerido') ?>";    
    var s_validate_required_fzipcode_number="<?php echo __('Numero Zip Code Requerido') ?>";    
    var s_validate_required_sede_service="<?php echo __('Servicio Requerido') ?>";    
    var s_validate_required_sede_name="<?php echo __('Nombre Requerido') ?>";    
    var s_validate_required_sede_zipcode="<?php echo __('Zip Code Requerido') ?>";    
    var s_validate_required_sede_address="<?php echo __('Dirección Requerido') ?>";    
    var s_location_saved='<?php echo __("Locación Guardado") ?>';
    var s_zipcode_saved='<?php echo __("Zip Code Guardado") ?>';
    var s_sede_saved='<?php echo __("Sede Guardado") ?>';
    var s_location_delete='<?php echo __("Locación Eliminado") ?>';    
    var s_zipcode_delete='<?php echo __("Zip Code Eliminado") ?>';    
    var s_sede_delete='<?php echo __("Sede Eliminado") ?>';    
    var s_place_holder_service='<?php echo __("Servicio") ?>';    
    var s_place_holder_zipcode='<?php echo __("Zip Code") ?>';        
</script>
<style type="text/css">    
</style>
<div class="controls-row">
    <div class="control-group controls-row">
        <a href="#modal_location" role="button" class="btn btn-small" data-toggle="modal">
            <i class="icon-plus"></i><?php echo __("Agregar") ?>
        </a>        
    </div>
    <div class="control-group controls-row">
        <table id="jqgrid_locations"></table>
        <div id="jqgrid_locations_pager"></div>
    </div>
    <div class="control-group row-fluid">
        <div class="span4">
            <table id="jqgrid_zipcode"></table>
            <div id="jqgrid_zipcode_pager"></div>
        </div>
        <div class="span8">
            <table id="jqgrid_sede"></table>
            <div id="jqgrid_sede_pager"></div>
        </div>
    </div>
</div>
<div id="modal_location" class="modal hide" data-backdrop="static">
    <div class="modal-header" style="text-align: center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3><?php echo __("Locación") ?></h3>
    </div>
    <div class="modal-body">
        <form id="form_location" class="form-horizontal">
            <input id="flocation_id" name="flocation_id" type="hidden"/>
            <div class="control-group">
                <label class="control-label" for="flocation_name"><?php echo __("Nombre") ?></label>
                <div class="controls">
                    <input type="text" id="flocation_name" name="flocation_name" placeholder="<?php echo __("Nombre") ?>"/>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
        <a id="modal_location_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
    </div>
</div>
<div id="modal_zipcode_sede" class="modal hide" data-backdrop="static">
    <div class="modal-header" style="text-align: center">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>        
    </div>
    <div class="modal-body">
        <div class="control-group">
            <input type="hidden" id="flocation_id_2" name="flocation_id_2" />
        </div>
        <div class="tabbable control-group"> <!-- Only required for left/right tabs -->
            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#tab1" data-toggle="tab"><?php echo __('Zip Code') ?></a></li>
                <li><a href="#tab2" data-toggle="tab"><?php echo __('Sede') ?></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <form id="form_zipcode" class="form-horizontal">
                        <input type="hidden" id="fzipcode_id" name="fzipcode_id" />
                        <div class="control-group">
                            <label class="control-label" for="fzipcode_number"><?php echo __("Zip Code") ?></label>
                            <div class="controls">
                                <input type="text" id="fzipcode_number" name="fzipcode_number" placeholder="<?php echo __("Numero") ?>"/>
                            </div>
                        </div> 
                    </form>                   
                    <div class="modal-footer">
                        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
                        <a id="modal_zipcode_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
                    </div>
                </div>
                <div class="tab-pane" id="tab2">
                    <form id="form_sede" class="form-horizontal">
                        <input type="hidden" id="fsede_id" name="fsede_id" />
                        <div class="control-group">
                            <label for="fsede_service" class="control-label"><?php echo __('Servicio') ?>: </label>
                            <div class="controls">
                                <select id="fsede_service" name="fsede_service">
                                    <option></option>
                                    <?php foreach ($list_service as $a_service): ?>
                                        <option value="<?php echo $a_service['service_id'] ?>" ><?php echo $a_service['service_name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="fsede_name"><?php echo __("Nombre") ?>: </label>
                            <div class="controls">
                                <input type="text" id="fsede_name" name="fsede_name" placeholder="<?php echo __("Nombre") ?>"/>
                            </div>
                        </div>       
                        <div class="control-group">
                            <label class="control-label" for="fsede_address"><?php echo __("Dirección") ?>: </label>
                            <div class="controls">
                                <input type="text" id="fsede_address" name="fsede_address" placeholder="<?php echo __("Dirección") ?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="fsede_zipcode" class="control-label"><?php echo __('Zip Code') ?>: </label>
                            <div class="controls">
                                <select id="fsede_zipcode" name="fsede_zipcode">
                                    <option></option>               
                                </select>
                            </div>                            
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="fsede_phone"><?php echo __("Telefono") ?>: </label>
                            <div class="controls">
                                <input type="text" id="fsede_phone" name="fsede_phone" placeholder="<?php echo __("Telefono") ?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="fsede_fax"><?php echo __("Fax") ?>: </label>
                            <div class="controls">
                                <input type="text" id="fsede_fax" name="fsede_fax" placeholder="<?php echo __("Fax") ?>"/>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <a href="#" class="btn" data-dismiss="modal"><?php echo __("Cerrar") ?></a>
                        <a id="modal_sede_btn_save" href="#" class="btn btn-primary"><?php echo __("Guardar") ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>