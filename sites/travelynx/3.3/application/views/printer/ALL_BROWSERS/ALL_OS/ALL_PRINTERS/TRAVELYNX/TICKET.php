<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            .head_print{
                background-image: url('/img/<?php echo $skin ?>/print/head_print_ticket.png');
                height: 66px;background-repeat: no-repeat;
            }
            .footer_print{
                background-image: url('/img/<?php echo $skin ?>/print/footer_print_ticket.png');
                height: 66px;background-repeat: no-repeat;
            }
        </style>
    </head>
    <body>
        <div style="width: 38.90em;">
            <div class="head_print">                
            </div>
            <div style="font-size: 12px;">
                <?php include Kohana::find_file('views', 'shared/send') ?>
            </div>
            <div class="footer_print">                
            </div>
        </div>
    </body>
</html>
