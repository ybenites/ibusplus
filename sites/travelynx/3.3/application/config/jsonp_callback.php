<?php

defined('SYSPATH') or die('No direct script access.');

return array(
    'jsonp_callback' => 'jsonp_callback',
    'ajax_data_type' => 'json'
);