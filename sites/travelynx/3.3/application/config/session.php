<?php

defined('SYSPATH') or die('No direct script access.');

global $a_db_config;

return array(
    Kohana_BtsConstants::KOHANA_SESSION_NAME_NATIVE => array(
        'name' => 'bts_' . md5($a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SKIN]),
        'lifetime' => 31536000 //NO CADUCA EN UN AÑO
    ),
    Kohana_BtsConstants::KOHANA_SESSION_NAME_DATABASE => array(
        /**
         * Database settings for session storage.
         *
         * string   group  configuation group name
         * string   table  session table name
         * integer  gc     number of requests before gc is invoked
         * columns  array  custom column names
         */
        'group' => 'default',
        'table' => 'bts_sessions',
        'lifetime' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SESSION_DB_LIFE_TIME],//
        'gc' => 500,
        'columns' => array(
            /**
             * session_id:  session identifier
             * last_active: timestamp of the last activity
             * contents:    serialized session data
             */
            'session_id' => 'session_id',
            'last_active' => 'last_active',
            'contents' => 'contents'
        ),
    ),
);