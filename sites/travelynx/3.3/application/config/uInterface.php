<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of uInterface
 *
 * @author BENITES
 */
class TravelynxInterface implements Kohana_BtsConstants, TravelynxConstants {

    public $ROUTE_TYPE = array(
        self::ROUTE_ONEWAY => array(
            'display' => "Ida"
            , 'value' => self::ROUTE_ONEWAY
        )
        , self::ROUTE_ROUNDTRIP => array(
            'display' => "Ida y vuelta"
            , 'value' => self::ROUTE_ROUNDTRIP
        )
    );

   public $a_year_report =
        array(
            '2013' => array(
                'display' => '2013'
                , 'value' => '2013'
            ),
            '2014' => array(
                'display' => '2014'
                , 'value' => '2014'
            )
        );

    public $a_month_report =
        array(
            'Enero' => array(
                'display' => 'Enero'
                , 'value' => '1'
            ),
            'Febrero' => array(
                'display' => 'Febrero'
                , 'value' => '2'
            ),
            'Marzo' => array(
                'display' => 'Marzo'
                , 'value' => '3'
            ),
            'Abril' => array(
                'display' => 'Abril'
                , 'value' => '4'
            ),
            'Mayo' => array(
                'display' => 'Mayo'
                , 'value' => '5'
            ),
            'Junio' => array(
                'display' => 'Junio'
                , 'value' => '6'
            ),
            'Julio' => array(
                'display' => 'Julio'
                , 'value' => '7'
            ),
            'Agosto' => array(
                'display' => 'Agosto'
                , 'value' => '8'
            ),
            'Septiembre' => array(
                'display' => 'Septiembre'
                , 'value' => '9'
            ),
            'Octubre' => array(
                'display' => 'Octubre'
                , 'value' => '10'
            ),
            'Noviembre' => array(
                'display' => 'Noviembre'
                , 'value' => '11'
            ),
            'Diciembre' => array(
                'display' => 'Diciembre'
                , 'value' => '12'
            )
        );
    

    public function getMonthName($index_value) {
        foreach ($this->a_month_report as $key => $value) {
            if ($value['value'] == $index_value) {
                return $key;
            }
        }
    }
    public function fnGetClassVariable($s_variable_name) {
        return $this->{$s_variable_name};
    }

}

return array(
    'uInterface' => new TravelynxInterface()
);