<?php

defined('SYSPATH') or die('No direct access allowed.');

global $a_db_config;
$a_all_db = Kohana_Controller_Private_Kernel::fnSYSGetArrayDataBaseConfigs();

$a_db_default = array
    (
    'default' => array
        (
        'type' => 'MySQLi',
        'connection' => array(
            'hostname' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_SERVER],
            'database' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_NAME],
            'username' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_USER],
            'password' => $a_db_config[Kohana_BtsConstants::BTS_SYS_CONFIG_DB_PASSWORD],
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
        'profiling' => TRUE,
    ),
    'alternate' => array(
        'type' => 'pdo',
        'connection' => array(
            'dsn' => 'mysql:host=localhost;dbname=kohana',
            'username' => 'root',
            'password' => 'r00tdb',
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
        'profiling' => TRUE,
    ),
    Kohana_BtsConstants::BTS_HELP_TASK_INSTANCE_CONNECTION=>array(
        'type'=>'MySQLi',
        'connection'=>array(
            'hostname' =>'192.168.1.37',
            'database' =>'ptms',
            'username' =>'root',
            'password' =>'2010Dallas09',
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => FALSE,
        'profiling' => TRUE,
    )
);

foreach ($a_all_db as $a_item) {
    $a_db_default = array_merge(
            $a_db_default
            , $a_item
    );
}

return $a_db_default;